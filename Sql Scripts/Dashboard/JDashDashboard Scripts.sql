
GO
/****** Object:  Table [dbo].[Dashboard]    Script Date: 10/24/2013 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dashboard](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](250) NULL,
	[layout] [ntext] NOT NULL,
	[config] [ntext] NULL,
	[paneConfig] [ntext] NULL,
	[style] [ntext] NULL,
	[cls] [ntext] NULL,
	[attr] [ntext] NULL,
	[groupName] [nvarchar](150) NULL,
	[description] [nvarchar](max) NULL,
	[createdBy] [nvarchar](150) NOT NULL,
	[modifiedBy] [nvarchar](150) NULL,
	[sharedBy] [nvarchar](150) NULL,
	[created] [datetime] NOT NULL,
	[modified] [datetime] NULL,
	[shared] [datetime] NULL,
	[userData] [ntext] NULL,
	[viewOrder] [int] NULL,
	[groupOrder] [int] NULL,
	[UserProperty1] [nvarchar](250) NULL,
	[UserProperty2] [nvarchar](250) NULL,
	[UserProperty3] [nvarchar](250) NULL,
 CONSTRAINT [PK_Dashboard] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Dashboard] ON
INSERT [dbo].[Dashboard] ([id], [title], [layout], [config], [paneConfig], [style], [cls], [attr], [groupName], [description], [createdBy], [modifiedBy], [sharedBy], [created], [modified], [shared], [userData], [viewOrder], [groupOrder], [UserProperty1], [UserProperty2], [UserProperty3]) VALUES (1, N'Main Dashboard', N'{"type":"grid","sections":{"section1":{"zones":{"zone1":{}}},"section2":{"zones":{"zone1":{},"zone2":{}}}},"addFirst":true}', N'null', N'null', NULL, NULL, NULL, N'', N'This is default dashboard', N'administrator', NULL, NULL, CAST(0x0000A26001146E18 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Dashboard] OFF
/****** Object:  Table [dbo].[Tag]    Script Date: 10/24/2013 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tag](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tagName] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_Tags] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EntityAuth]    Script Date: 10/24/2013 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EntityAuth](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[entityType] [nvarchar](50) NOT NULL,
	[entityId] [int] NOT NULL,
	[roleOrUser] [nvarchar](50) NOT NULL,
	[permission] [nvarchar](50) NOT NULL,
	[authType] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_EntityAuth] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[EntityAuth] ON
INSERT [dbo].[EntityAuth] ([id], [entityType], [entityId], [roleOrUser], [permission], [authType]) VALUES (17, N'dashboard', 1, N'test', N'view', N'userName')
INSERT [dbo].[EntityAuth] ([id], [entityType], [entityId], [roleOrUser], [permission], [authType]) VALUES (18, N'dashboard', 2, N'test', N'view', N'userName')
SET IDENTITY_INSERT [dbo].[EntityAuth] OFF
/****** Object:  Table [dbo].[DashletModule]    Script Date: 10/24/2013 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DashletModule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[path] [nvarchar](max) NOT NULL,
	[title] [nvarchar](255) NOT NULL,
	[config] [ntext] NOT NULL,
	[paneConfig] [ntext] NOT NULL,
	[dashletConfig] [ntext] NOT NULL,
	[style] [ntext] NULL,
	[cls] [ntext] NULL,
	[attr] [ntext] NULL,
	[description] [nvarchar](max) NULL,
	[createdBy] [nvarchar](150) NOT NULL,
	[modifiedBy] [nvarchar](150) NULL,
	[sharedBy] [nvarchar](150) NULL,
	[created] [datetime] NOT NULL,
	[modified] [datetime] NULL,
	[shared] [datetime] NULL,
	[groupName] [nvarchar](150) NULL,
	[userData] [ntext] NULL,
	[viewOrder] [int] NULL,
	[groupOrder] [int] NULL,
	[UserProperty1] [nvarchar](250) NULL,
	[UserProperty2] [nvarchar](250) NULL,
	[UserProperty3] [nvarchar](250) NULL,
 CONSTRAINT [PK_DashletModules] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DashletModule] ON
INSERT [dbo].[DashletModule] ([id], [path], [title], [config], [paneConfig], [dashletConfig], [style], [cls], [attr], [description], [createdBy], [modifiedBy], [sharedBy], [created], [modified], [shared], [groupName], [userData], [viewOrder], [groupOrder], [UserProperty1], [UserProperty2], [UserProperty3]) VALUES (1, N'[WfDefault]', N'Alerts', N'{"wfConfig":{"uc":"JDash/Dashlets/Alerts.ascx","ec":"~/JDash/Dashlets/HtmlDashletEditor.ascx","autoEdit":false,"notifications":{"commands":["maximize","restore","refresh"]}},"editor":{"paneConfig":{"cssClass":null,"width":null,"height":null},"useWindow":false}}', N'{"cssClass":null,"builtInCommands":["maximize","restore","refresh"],"customCommands":[]}', N'{}', NULL, NULL, NULL, N'', N'NOUMAN-PC\Adil Ata', NULL, NULL, CAST(0x0000A26000C37C8F AS DateTime), NULL, NULL, N'', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DashletModule] ([id], [path], [title], [config], [paneConfig], [dashletConfig], [style], [cls], [attr], [description], [createdBy], [modifiedBy], [sharedBy], [created], [modified], [shared], [groupName], [userData], [viewOrder], [groupOrder], [UserProperty1], [UserProperty2], [UserProperty3]) VALUES (2, N'[WfDefault]', N'Best Rates', N'{"wfConfig":{"uc":"~/JDash/Dashlets/BestRates.ascx","ec":"~/JDash/Dashlets/HtmlDashletEditor.ascx","autoEdit":false,"notifications":{"commands":["maximize","restore","refresh"]}},"editor":{"paneConfig":{"cssClass":null,"width":null,"height":null},"useWindow":false}}', N'{"cssClass":null,"builtInCommands":["maximize","restore","refresh"],"customCommands":[]}', N'{}', NULL, NULL, NULL, N'', N'NOUMAN-PC\Adil Ata', NULL, NULL, CAST(0x0000A2600071B11C AS DateTime), NULL, NULL, N'', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DashletModule] ([id], [path], [title], [config], [paneConfig], [dashletConfig], [style], [cls], [attr], [description], [createdBy], [modifiedBy], [sharedBy], [created], [modified], [shared], [groupName], [userData], [viewOrder], [groupOrder], [UserProperty1], [UserProperty2], [UserProperty3]) VALUES (3, N'[WfDefault]', N'Client List', N'{"wfConfig":{"uc":"~/JDash/Dashlets/ClientList.ascx","ec":"~/JDash/Dashlets/HtmlDashletEditor.ascx","autoEdit":false,"notifications":{"commands":["maximize","restore","refresh"]}},"editor":{"paneConfig":{"cssClass":null,"width":null,"height":null},"useWindow":false}}', N'{"cssClass":null,"builtInCommands":["maximize","restore","refresh"],"customCommands":[]}', N'{}', NULL, NULL, NULL, N'', N'NOUMAN-PC\Adil Ata', NULL, NULL, CAST(0x0000A2600071C5E8 AS DateTime), NULL, NULL, N'', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DashletModule] ([id], [path], [title], [config], [paneConfig], [dashletConfig], [style], [cls], [attr], [description], [createdBy], [modifiedBy], [sharedBy], [created], [modified], [shared], [groupName], [userData], [viewOrder], [groupOrder], [UserProperty1], [UserProperty2], [UserProperty3]) VALUES (4, N'[WfDefault]', N'Holdings', N'{"wfConfig":{"uc":"~/JDash/Dashlets/Holdings.ascx","ec":"~/JDash/Dashlets/HtmlDashletEditor.ascx","autoEdit":false,"notifications":{"commands":["maximize","restore","refresh"]}},"editor":{"paneConfig":{"cssClass":null,"width":null,"height":null},"useWindow":false}}', N'{"cssClass":null,"builtInCommands":["maximize","restore","refresh"],"customCommands":[]}', N'{}', NULL, NULL, NULL, N'', N'NOUMAN-PC\Adil Ata', NULL, NULL, CAST(0x0000A2600071DC7A AS DateTime), NULL, NULL, N'', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DashletModule] ([id], [path], [title], [config], [paneConfig], [dashletConfig], [style], [cls], [attr], [description], [createdBy], [modifiedBy], [sharedBy], [created], [modified], [shared], [groupName], [userData], [viewOrder], [groupOrder], [UserProperty1], [UserProperty2], [UserProperty3]) VALUES (5, N'[WfDefault]', N'Notifications', N'{"wfConfig":{"uc":"~/JDash/Dashlets/Notifications.ascx","ec":"~/JDash/Dashlets/HtmlDashletEditor.ascx","autoEdit":false,"notifications":{"commands":["maximize","restore","refresh"]}},"editor":{"paneConfig":{"cssClass":null,"width":null,"height":null},"useWindow":false}}', N'{"cssClass":null,"builtInCommands":["maximize","restore","refresh"],"customCommands":[]}', N'{}', NULL, NULL, NULL, N'', N'NOUMAN-PC\Adil Ata', NULL, NULL, CAST(0x0000A260001F889F AS DateTime), NULL, NULL, N'', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DashletModule] ([id], [path], [title], [config], [paneConfig], [dashletConfig], [style], [cls], [attr], [description], [createdBy], [modifiedBy], [sharedBy], [created], [modified], [shared], [groupName], [userData], [viewOrder], [groupOrder], [UserProperty1], [UserProperty2], [UserProperty3]) VALUES (6, N'[WfDefault]', N'TechnicalDocuments', N'{"wfConfig":{"uc":"~/JDash/Dashlets/TechnicalDocuments.ascx","ec":"~/JDash/Dashlets/HtmlDashletEditor.ascx","autoEdit":false,"notifications":{"commands":["maximize","restore","refresh"]}},"editor":{"paneConfig":{"cssClass":null,"width":null,"height":null},"useWindow":false}}', N'{"cssClass":null,"builtInCommands":["maximize","restore","refresh"],"customCommands":[]}', N'{}', NULL, NULL, NULL, N'', N'NOUMAN-PC\Adil Ata', NULL, NULL, CAST(0x0000A2600071FFFA AS DateTime), NULL, NULL, N'', NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[DashletModule] OFF
/****** Object:  Table [dbo].[Dashlet]    Script Date: 10/24/2013 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dashlet](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dashboardId] [int] NOT NULL,
	[dashletModuleId] [int] NOT NULL,
	[title] [nvarchar](255) NULL,
	[config] [ntext] NULL,
	[paneConfig] [ntext] NULL,
	[position] [ntext] NOT NULL,
	[style] [ntext] NULL,
	[cls] [ntext] NULL,
	[attr] [ntext] NULL,
	[groupName] [nvarchar](150) NULL,
	[description] [nvarchar](max) NULL,
	[createdBy] [nvarchar](150) NOT NULL,
	[modifiedBy] [nvarchar](150) NULL,
	[sharedBy] [nvarchar](150) NULL,
	[created] [datetime] NOT NULL,
	[modified] [datetime] NULL,
	[shared] [datetime] NULL,
	[userData] [ntext] NULL,
	[UserProperty1] [nvarchar](250) NULL,
	[UserProperty2] [nvarchar](250) NULL,
	[UserProperty3] [nvarchar](250) NULL,
 CONSTRAINT [PK_Dashlets] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Dashlet] ON
INSERT [dbo].[Dashlet] ([id], [dashboardId], [dashletModuleId], [title], [config], [paneConfig], [position], [style], [cls], [attr], [groupName], [description], [createdBy], [modifiedBy], [sharedBy], [created], [modified], [shared], [userData], [UserProperty1], [UserProperty2], [UserProperty3]) VALUES (1, 1, 1, N'Alerts', N'{}', N'{"builtInCommands":["remove"],"customCommands":[],"themeStyleId":"d","disableTheming":true}', N'{"section":"section1","zone":"zone1","pos":2}', NULL, NULL, NULL, N'', N'', N'administrator', NULL, NULL, CAST(0x0000A26001161D4E AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Dashlet] ([id], [dashboardId], [dashletModuleId], [title], [config], [paneConfig], [position], [style], [cls], [attr], [groupName], [description], [createdBy], [modifiedBy], [sharedBy], [created], [modified], [shared], [userData], [UserProperty1], [UserProperty2], [UserProperty3]) VALUES (2, 1, 2, N'Best Rates', N'{}', N'{"builtInCommands":["maximize","restore","refresh"],"customCommands":[],"themeStyleId":"d","disableTheming":true}', N'{"section":"section1","zone":"zone1","pos":3}', NULL, NULL, NULL, N'', N'', N'administrator', NULL, NULL, CAST(0x0000A26001178E49 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Dashlet] ([id], [dashboardId], [dashletModuleId], [title], [config], [paneConfig], [position], [style], [cls], [attr], [groupName], [description], [createdBy], [modifiedBy], [sharedBy], [created], [modified], [shared], [userData], [UserProperty1], [UserProperty2], [UserProperty3]) VALUES (3, 1, 3, N'Client List', N'{}', N'{"builtInCommands":["maximize","restore","refresh"],"customCommands":[],"themeStyleId":"d","disableTheming":true}', N'{"section":"section1","zone":"zone1","pos":4}', NULL, NULL, NULL, N'', N'', N'administrator', NULL, NULL, CAST(0x0000A260011797F4 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Dashlet] ([id], [dashboardId], [dashletModuleId], [title], [config], [paneConfig], [position], [style], [cls], [attr], [groupName], [description], [createdBy], [modifiedBy], [sharedBy], [created], [modified], [shared], [userData], [UserProperty1], [UserProperty2], [UserProperty3]) VALUES (4, 1, 4, N'Holdings', N'{}', N'{"builtInCommands":["maximize","restore","refresh"],"customCommands":[],"themeStyleId":"d","disableTheming":true}', N'{"section":"section1","zone":"zone1","pos":1}', NULL, NULL, NULL, N'', N'', N'administrator', NULL, NULL, CAST(0x0000A2600117BACE AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Dashlet] ([id], [dashboardId], [dashletModuleId], [title], [config], [paneConfig], [position], [style], [cls], [attr], [groupName], [description], [createdBy], [modifiedBy], [sharedBy], [created], [modified], [shared], [userData], [UserProperty1], [UserProperty2], [UserProperty3]) VALUES (5, 1, 5, N'Notifications', N'{}', N'{"builtInCommands":["maximize","restore","refresh"],"customCommands":[],"themeStyleId":"d","disableTheming":true}', N'{"section":"section1","zone":"zone1","pos":0}', NULL, NULL, NULL, N'', N'', N'administrator', NULL, NULL, CAST(0x0000A2600117C046 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Dashlet] ([id], [dashboardId], [dashletModuleId], [title], [config], [paneConfig], [position], [style], [cls], [attr], [groupName], [description], [createdBy], [modifiedBy], [sharedBy], [created], [modified], [shared], [userData], [UserProperty1], [UserProperty2], [UserProperty3]) VALUES (6, 1, 6, N'TechnicalDocuments', N'{}', N'{"builtInCommands":["maximize","restore","refresh"],"customCommands":[],"themeStyleId":"d","disableTheming":true}', N'{"section":"section1","zone":"zone1","pos":2}', NULL, NULL, NULL, N'', N'', N'administrator', NULL, NULL, CAST(0x0000A2600117C6B2 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Dashlet] OFF
/****** Object:  Table [dbo].[TagRelation]    Script Date: 10/24/2013 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TagRelation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tagId] [int] NOT NULL,
	[controlId] [int] NOT NULL,
	[controller] [nvarchar](15) NOT NULL,
 CONSTRAINT [PK_TagRelation] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_Dashlets_Dashboard]    Script Date: 10/24/2013 13:57:09 ******/
ALTER TABLE [dbo].[Dashlet]  WITH CHECK ADD  CONSTRAINT [FK_Dashlets_Dashboard] FOREIGN KEY([dashboardId])
REFERENCES [dbo].[Dashboard] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Dashlet] CHECK CONSTRAINT [FK_Dashlets_Dashboard]
GO
/****** Object:  ForeignKey [FK_Dashlets_DashletModules]    Script Date: 10/24/2013 13:57:09 ******/
ALTER TABLE [dbo].[Dashlet]  WITH CHECK ADD  CONSTRAINT [FK_Dashlets_DashletModules] FOREIGN KEY([dashletModuleId])
REFERENCES [dbo].[DashletModule] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Dashlet] CHECK CONSTRAINT [FK_Dashlets_DashletModules]
GO
/****** Object:  ForeignKey [FK_TagRelation_Dashboard]    Script Date: 10/24/2013 13:57:09 ******/
ALTER TABLE [dbo].[TagRelation]  WITH NOCHECK ADD  CONSTRAINT [FK_TagRelation_Dashboard] FOREIGN KEY([controlId])
REFERENCES [dbo].[Dashboard] ([id])
GO
ALTER TABLE [dbo].[TagRelation] NOCHECK CONSTRAINT [FK_TagRelation_Dashboard]
GO
/****** Object:  ForeignKey [FK_TagRelation_DashletModule]    Script Date: 10/24/2013 13:57:09 ******/
ALTER TABLE [dbo].[TagRelation]  WITH NOCHECK ADD  CONSTRAINT [FK_TagRelation_DashletModule] FOREIGN KEY([controlId])
REFERENCES [dbo].[DashletModule] ([id])
GO
ALTER TABLE [dbo].[TagRelation] NOCHECK CONSTRAINT [FK_TagRelation_DashletModule]
GO
/****** Object:  ForeignKey [FK_TagRelation_Dashlets]    Script Date: 10/24/2013 13:57:09 ******/
ALTER TABLE [dbo].[TagRelation]  WITH NOCHECK ADD  CONSTRAINT [FK_TagRelation_Dashlets] FOREIGN KEY([controlId])
REFERENCES [dbo].[Dashlet] ([id])
GO
ALTER TABLE [dbo].[TagRelation] NOCHECK CONSTRAINT [FK_TagRelation_Dashlets]
GO
/****** Object:  ForeignKey [FK_TagRelation_Tags]    Script Date: 10/24/2013 13:57:09 ******/
ALTER TABLE [dbo].[TagRelation]  WITH CHECK ADD  CONSTRAINT [FK_TagRelation_Tags] FOREIGN KEY([tagId])
REFERENCES [dbo].[Tag] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TagRelation] CHECK CONSTRAINT [FK_TagRelation_Tags]
GO
