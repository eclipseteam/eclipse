 

DROP TABLE [dbo].[Actions];

CREATE TABLE [dbo].[Actions] (
    [ActionID] uniqueidentifier  NOT NULL,
    [NotificationID] uniqueidentifier  NULL,
    [Message] varchar(max)  NULL,
    [IsExpired] bit  NOT NULL,
    [ExpiryDate] datetime  NOT NULL,
    [DueDate] datetime  NOT NULL,
    [ActionType] varchar(255)  NULL,
    [Status] varchar(255)  NULL,
    [CreationDate] datetime  NOT NULL,
    [CreatedBy] varchar(255)  NULL,
    [UpdatedDate] datetime  NOT NULL,
    [UpdatedBy] varchar(255)  NULL,
    [CreationMode] varchar(255)  NULL,
    [ActionTypeDetail] int  NOT NULL,
    [IsVisibleToClient] bit  NOT NULL,
    [IsVisibleToAdmin] bit  NOT NULL,
    [IsVisibleToAdviser] bit  NOT NULL
);
GO
