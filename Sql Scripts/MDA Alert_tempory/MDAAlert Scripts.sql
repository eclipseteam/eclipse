-- Create Action Table

IF object_id('Actions', 'U') is null
begin
CREATE TABLE [Actions] (
    [ActionID] uniqueidentifier  NOT NULL,
    [NotificationID] uniqueidentifier  NULL,
    [Message] varchar(max)  NULL,
    [IsExpired] bit  NOT NULL,
    [ExpiryDate] datetime  NOT NULL,
    [DueDate] datetime  NOT NULL,
    [ActionType] varchar(255)  NULL,
    [Status] varchar(255)  NULL,
    [IsAssignedTo] uniqueidentifier  NULL,
    [CreationDate] datetime  NOT NULL,
    [CreatedBy] varchar(255)  NULL,
    [UpdatedDate] datetime  NOT NULL,
    [UpdatedBy] varchar(255)  NULL,
    [CreationMode] varchar(255)  NULL
);
end

--Insert workflow


declare @WFID uniqueidentifier;
set @WFID = NEWID();

INSERT INTO [Workflows]
           ([WorkflowID]
           ,[WorkflowDesc]
           ,[WorkflowName]
           ,[CreatedBy]
           ,[Comments]
           ,[WorkflowType]
           ,[CreatedDate]
           ,[LastRunDate])
     VALUES(
            @WFID ,
           'MDA Workflow',
           'MDA Workflow',
           'Administrator',
           'WF for MDA',
           2,
           GETDATE(),
           GETDATE()
           )

----------------------------------------------------------------
declare @NotificationID1 uniqueidentifier;
set @NotificationID1 = NEWID();

INSERT INTO [Notifications]
           ([NotificationID]
           ,[WorkflowID]
           ,[NotificationType]
           ,[NotificationName]
           ,[NotificationDescription]
           ,[NotificationDetails]
           ,[AttachmentID]
           ,[ClientCID]
           ,[ClientID]
           ,[ClientName]
           ,[CreatedDate]
           ,[ModifiedDate]
           ,[FileDownLoadURLDownload])
     VALUES
           (
            @NotificationID1,
            @WFID,
            2,
            'MDA renewal',
            'MDA renewal',
            'MDA renewal',
            (SELECT CAST(0x0 AS UNIQUEIDENTIFIER)),
            'cfaa0531-ef5f-4c9f-90f0-48f4e78f7e5f',
            'IVACCCCCC',
            'J & K Heysen Family Superannuation Fund',
            GETDATE(),
            GETDATE(),
            'FileImport_1_1.aspx?FileID=3f9225ab-dee9-4b2a-a223-50f1a5af5093&CID=cfaa0531-ef5f-4c9f-90f0-48f4e78f7e5f'
           
           )

INSERT [dbo].[Actions] 
([ActionID], 
[NotificationID], 
[Message], 
[IsExpired], 
[ExpiryDate], 
[DueDate], 
[ActionType],
 [Status], 
 [IsAssignedTo], 
 [CreationDate], 
 [CreatedBy], 
 [UpdatedDate], 
 [UpdatedBy], 
 [CreationMode]) 
 
 VALUES 
 (
 (select NEWID()) , 
 @NotificationID1, 
 'MDA renewal', 
 0,
  GETDATE(), 
  GETDATE(),
  'MDA', 
  'Active',   
  (SELECT CAST(0x0 AS UNIQUEIDENTIFIER)),
  GETDATE(),
  '', 
  GETDATE(),
  '',
  'SystemGenerated')
  
  ----------------------------------------------------------------
  
  
declare @NotificationID2 uniqueidentifier;
set @NotificationID2 = NEWID();

INSERT INTO [Notifications]
           ([NotificationID]
           ,[WorkflowID]
           ,[NotificationType]
           ,[NotificationName]
           ,[NotificationDescription]
           ,[NotificationDetails]
           ,[AttachmentID]
           ,[ClientCID]
           ,[ClientID]
           ,[ClientName]
           ,[CreatedDate]
           ,[ModifiedDate]
           ,[FileDownLoadURLDownload])
     VALUES
           (
            @NotificationID2,
            @WFID,
            2,
            'MDA renewal',
            'MDA renewal',
            'MDA renewal',
            (SELECT CAST(0x0 AS UNIQUEIDENTIFIER)),
            'cfaa0531-ef5f-4c9f-90f0-48f4e78f7e5f',
            'IVACCCCCC',
            'J & K Heysen Family Superannuation Fund',
            GETDATE(),
            GETDATE(),
            'FileImport_1_1.aspx?FileID=3f9225ab-dee9-4b2a-a223-50f1a5af5093&CID=cfaa0531-ef5f-4c9f-90f0-48f4e78f7e5f'
           
           )

INSERT [dbo].[Actions] 
([ActionID], 
[NotificationID], 
[Message], 
[IsExpired], 
[ExpiryDate], 
[DueDate], 
[ActionType],
 [Status], 
 [IsAssignedTo], 
 [CreationDate], 
 [CreatedBy], 
 [UpdatedDate], 
 [UpdatedBy], 
 [CreationMode]) 
 
 VALUES 
 (
 (select NEWID()) , 
 @NotificationID2, 
 'MDA renewal', 
 0,
  GETDATE(), 
  GETDATE(),
  'MDA', 
  'Active',   
  (SELECT CAST(0x0 AS UNIQUEIDENTIFIER)),
  GETDATE(),
  '', 
  GETDATE(),
  '',
  'SystemGenerated')
  
 --------------------------------------------------------------
 
 
declare @NotificationID3 uniqueidentifier;
set @NotificationID3 = NEWID();

INSERT INTO [Notifications]
           ([NotificationID]
           ,[WorkflowID]
           ,[NotificationType]
           ,[NotificationName]
           ,[NotificationDescription]
           ,[NotificationDetails]
           ,[AttachmentID]
           ,[ClientCID]
           ,[ClientID]
           ,[ClientName]
           ,[CreatedDate]
           ,[ModifiedDate]
           ,[FileDownLoadURLDownload])
     VALUES
           (
            @NotificationID3,
            @WFID,
            2,
            'MDA renewal',
            'MDA renewal',
            'MDA renewal',
            (SELECT CAST(0x0 AS UNIQUEIDENTIFIER)),
            'cfaa0531-ef5f-4c9f-90f0-48f4e78f7e5f',
            'IVACCCCCC',
            'J & K Heysen Family Superannuation Fund',
            GETDATE(),
            GETDATE(),
            'FileImport_1_1.aspx?FileID=3f9225ab-dee9-4b2a-a223-50f1a5af5093&CID=cfaa0531-ef5f-4c9f-90f0-48f4e78f7e5f'
           
           )

INSERT [dbo].[Actions] 
([ActionID], 
[NotificationID], 
[Message], 
[IsExpired], 
[ExpiryDate], 
[DueDate], 
[ActionType],
 [Status], 
 [IsAssignedTo], 
 [CreationDate], 
 [CreatedBy], 
 [UpdatedDate], 
 [UpdatedBy], 
 [CreationMode]) 
 
 VALUES 
 (
 (select NEWID()) , 
 @NotificationID3, 
 'MDA renewal', 
 0,
  GETDATE(), 
  GETDATE(),
  'MDA', 
  'Active',   
  (SELECT CAST(0x0 AS UNIQUEIDENTIFIER)),
  GETDATE(),
  '', 
  GETDATE(),
  '',
  'SystemGenerated') 

------------------------------------------------------


declare @NotificationID4 uniqueidentifier;
set @NotificationID4 = NEWID();

INSERT INTO [Notifications]
           ([NotificationID]
           ,[WorkflowID]
           ,[NotificationType]
           ,[NotificationName]
           ,[NotificationDescription]
           ,[NotificationDetails]
           ,[AttachmentID]
           ,[ClientCID]
           ,[ClientID]
           ,[ClientName]
           ,[CreatedDate]
           ,[ModifiedDate]
           ,[FileDownLoadURLDownload])
     VALUES
           (
            @NotificationID4,
            @WFID,
            2,
            'MDA renewal',
            'MDA renewal',
            'MDA renewal',
            (SELECT CAST(0x0 AS UNIQUEIDENTIFIER)),
            'cfaa0531-ef5f-4c9f-90f0-48f4e78f7e5f',
            'IVACCCCCC',
            'J & K Heysen Family Superannuation Fund',
            GETDATE(),
            GETDATE(),
            'FileImport_1_1.aspx?FileID=3f9225ab-dee9-4b2a-a223-50f1a5af5093&CID=cfaa0531-ef5f-4c9f-90f0-48f4e78f7e5f'
           
           )

INSERT [dbo].[Actions] 
([ActionID], 
[NotificationID], 
[Message], 
[IsExpired], 
[ExpiryDate], 
[DueDate], 
[ActionType],
 [Status], 
 [IsAssignedTo], 
 [CreationDate], 
 [CreatedBy], 
 [UpdatedDate], 
 [UpdatedBy], 
 [CreationMode]) 
 
 VALUES 
 (
 (select NEWID()) , 
 @NotificationID4, 
 'MDA renewal', 
 0,
  GETDATE(), 
  GETDATE(),
  'MDA', 
  'Active',   
  (SELECT CAST(0x0 AS UNIQUEIDENTIFIER)),
  GETDATE(),
  '', 
  GETDATE(),
  '',
  'SystemGenerated')
  
  --------------------------------------------------------------
  
  
declare @NotificationID5 uniqueidentifier;
set @NotificationID5 = NEWID();

INSERT INTO [Notifications]
           ([NotificationID]
           ,[WorkflowID]
           ,[NotificationType]
           ,[NotificationName]
           ,[NotificationDescription]
           ,[NotificationDetails]
           ,[AttachmentID]
           ,[ClientCID]
           ,[ClientID]
           ,[ClientName]
           ,[CreatedDate]
           ,[ModifiedDate]
           ,[FileDownLoadURLDownload])
     VALUES
           (
            @NotificationID5,
            @WFID,
            2,
            'MDA renewal',
            'MDA renewal',
            'MDA renewal',
            (SELECT CAST(0x0 AS UNIQUEIDENTIFIER)),
            'cfaa0531-ef5f-4c9f-90f0-48f4e78f7e5f',
            'IVACCCCCC',
            'J & K Heysen Family Superannuation Fund',
            GETDATE(),
            GETDATE(),
            'FileImport_1_1.aspx?FileID=3f9225ab-dee9-4b2a-a223-50f1a5af5093&CID=cfaa0531-ef5f-4c9f-90f0-48f4e78f7e5f'
           
           )

INSERT [dbo].[Actions] 
([ActionID], 
[NotificationID], 
[Message], 
[IsExpired], 
[ExpiryDate], 
[DueDate], 
[ActionType],
 [Status], 
 [IsAssignedTo], 
 [CreationDate], 
 [CreatedBy], 
 [UpdatedDate], 
 [UpdatedBy], 
 [CreationMode]) 
 
 VALUES 
 (
 (select NEWID()) , 
 @NotificationID5, 
 'MDA renewal', 
 0,
  GETDATE(), 
  GETDATE(),
  'MDA', 
  'Active',   
  (SELECT CAST(0x0 AS UNIQUEIDENTIFIER)),
  GETDATE(),
  '', 
  GETDATE(),
  '',
  'SystemGenerated')