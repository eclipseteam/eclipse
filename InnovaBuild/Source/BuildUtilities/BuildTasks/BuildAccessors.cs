using System;
using System.Collections.Generic;
using System.Text;
namespace NableBuildTasks
{
    public class BuildAccessors
    {
        [System.Diagnostics.DebuggerStepThrough()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.TestTools.UnitTestGeneration", "1.0.0.0")]
        internal class BaseAccessor
        {
          protected Microsoft.VisualStudio.TestTools.UnitTesting.PrivateObject m_privateObject;
          protected BaseAccessor(object target, Microsoft.VisualStudio.TestTools.UnitTesting.PrivateType type) {
          m_privateObject = new Microsoft.VisualStudio.TestTools.UnitTesting.PrivateObject(target, type);
    }
    
    protected BaseAccessor(Microsoft.VisualStudio.TestTools.UnitTesting.PrivateType type) : 
            this(null, type) {
    }
    
    internal virtual object Target {
        get {
            return m_privateObject.Target;
        }
    }
    
    public override string ToString() {
        return this.Target.ToString();
    }
    
    public override bool Equals(object obj) {
        if (typeof(BaseAccessor).IsInstanceOfType(obj)) {
            obj = ((BaseAccessor)(obj)).Target;
        }
        return this.Target.Equals(obj);
    }
    
    public override int GetHashCode() {
        return this.Target.GetHashCode();
    }
}


[System.Diagnostics.DebuggerStepThrough()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.TestTools.UnitTestGeneration", "1.0.0.0")]
internal class Microsoft_Build_BuildEngine_EngineProxyAccessor : BaseAccessor {
    
    protected static Microsoft.VisualStudio.TestTools.UnitTesting.PrivateType m_privateType = new Microsoft.VisualStudio.TestTools.UnitTesting.PrivateType("Microsoft.Build.Engine", "Microsoft.Build.BuildEngine.EngineProxy");
    
    internal Microsoft_Build_BuildEngine_EngineProxyAccessor(object target) : 
            base(target, m_privateType) {
    }
    
    internal int ColumnNumberOfTaskNode {
        get {
            int ret = ((int)(m_privateObject.GetProperty("ColumnNumberOfTaskNode")));
            return ret;
        }
    }
    
    internal bool ContinueOnError {
        get {
            bool ret = ((bool)(m_privateObject.GetProperty("ContinueOnError")));
            return ret;
        }
    }
    
    internal int LineNumberOfTaskNode {
        get {
            int ret = ((int)(m_privateObject.GetProperty("LineNumberOfTaskNode")));
            return ret;
        }
    }
    
    internal string ProjectFileOfTaskNode {
        get {
            string ret = ((string)(m_privateObject.GetProperty("ProjectFileOfTaskNode")));
            return ret;
        }
    }
    
    internal int columnNumber {
        get {
            int ret = ((int)(m_privateObject.GetField("columnNumber")));
            return ret;
        }
        set {
            m_privateObject.SetField("columnNumber", value);
        }
    }
    
    internal global::Microsoft.Build.BuildEngine.Engine engine {
        get {
            global::Microsoft.Build.BuildEngine.Engine ret = ((global::Microsoft.Build.BuildEngine.Engine)(m_privateObject.GetField("engine")));
            return ret;
        }
        set {
            m_privateObject.SetField("engine", value);
        }
    }
    
    internal bool haveProjectFileLocation {
        get {
            bool ret = ((bool)(m_privateObject.GetField("haveProjectFileLocation")));
            return ret;
        }
        set {
            m_privateObject.SetField("haveProjectFileLocation", value);
        }
    }
    
    internal int lineNumber {
        get {
            int ret = ((int)(m_privateObject.GetField("lineNumber")));
            return ret;
        }
        set {
            m_privateObject.SetField("lineNumber", value);
        }
    }
    
    internal global::Microsoft.Build.BuildEngine.Project project {
        get {
            global::Microsoft.Build.BuildEngine.Project ret = ((global::Microsoft.Build.BuildEngine.Project)(m_privateObject.GetField("project")));
            return ret;
        }
        set {
            m_privateObject.SetField("project", value);
        }
    }
    
    internal static global::System.MarshalByRefObject CreatePrivate() {
        object[] args = new object[0];
        Microsoft.VisualStudio.TestTools.UnitTesting.PrivateObject priv_obj = new Microsoft.VisualStudio.TestTools.UnitTesting.PrivateObject("Microsoft.Build.Engine", "Microsoft.Build.BuildEngine.EngineProxy", new System.Type[0], args);
        return ((global::System.MarshalByRefObject)(priv_obj.Target));
    }
    
    internal bool BuildProjectFile(string projectFileName, string[] targetNames, global::System.Collections.IDictionary globalProperties, global::System.Collections.IDictionary targetOutputs) {
        object[] args = new object[] {
                projectFileName,
                targetNames,
                globalProperties,
                targetOutputs};
        bool ret = ((bool)(m_privateObject.Invoke("BuildProjectFile", new System.Type[] {
                    typeof(string),
                    typeof(string).MakeArrayType(),
                    typeof(global::System.Collections.IDictionary),
                    typeof(global::System.Collections.IDictionary)}, args)));
        return ret;
    }
    
    internal void ComputeProjectFileLocationOfTaskNode() {
        object[] args = new object[0];
        m_privateObject.Invoke("ComputeProjectFileLocationOfTaskNode", new System.Type[0], args);
    }
    
    internal object InitializeLifetimeService() {
        object[] args = new object[0];
        object ret = ((object)(m_privateObject.Invoke("InitializeLifetimeService", new System.Type[0], args)));
        return ret;
    }
    
    internal void LogCustomEvent(global::Microsoft.Build.Framework.CustomBuildEventArgs e) {
        object[] args = new object[] {
                e};
        m_privateObject.Invoke("LogCustomEvent", new System.Type[] {
                    typeof(global::Microsoft.Build.Framework.CustomBuildEventArgs)}, args);
    }
    
    internal void LogErrorEvent(global::Microsoft.Build.Framework.BuildErrorEventArgs e) {
        object[] args = new object[] {
                e};
        m_privateObject.Invoke("LogErrorEvent", new System.Type[] {
                    typeof(global::Microsoft.Build.Framework.BuildErrorEventArgs)}, args);
    }
    
    internal void LogMessageEvent(global::Microsoft.Build.Framework.BuildMessageEventArgs e) {
        object[] args = new object[] {
                e};
        m_privateObject.Invoke("LogMessageEvent", new System.Type[] {
                    typeof(global::Microsoft.Build.Framework.BuildMessageEventArgs)}, args);
    }
    
    internal void LogWarningEvent(global::Microsoft.Build.Framework.BuildWarningEventArgs e) {
        object[] args = new object[] {
                e};
        m_privateObject.Invoke("LogWarningEvent", new System.Type[] {
                    typeof(global::Microsoft.Build.Framework.BuildWarningEventArgs)}, args);
    }
}
    }
}
