//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Xml;
//using System.Xml.Schema;
//using System.Xml.Xsl;
//using System.IO;
//using System.Reflection;
//using System.Xml.XPath;
//using System.Collections;
//using Microsoft.VisualStudio.TestTools.Common;
//using System.Data;

//namespace NableBuildTasks
//{
//    /// <summary>
//    /// Merges two <see cref="XmlNode"/> objects.
//    /// </summary>
//    public class XmlMerge
//    {
//        const string First = "first";
//        const string Second = "second";
//        static XslTransform Transformation;
//        static IXPathNavigable Input;

//        static XmlMerge()
//        {
         
//            // Dummy input document for the merge stylesheet.
//            Input = new XPathDocument(new StringReader(String.Format(
//              @"<?xml version='1.0'?>
//      <merge xmlns='http://informatik.hu-berlin.de/merge'>
//        <file1>mem://{0}</file1>
//        <file2>mem://{1}</file2>
//      </merge>", First, Second)));
//        }

//        private XmlMerge() { }

//        /// <summary>
//        /// Merges the first XML with the second.
//        /// </summary>
//        /// <param name="first">First XML.</param>
//        /// <param name="second">Second XML.</param>
//        /// <param name="replace">If set to <see langword="true"/> replaces 
//        /// text values from <paramref name="first"/> with the ones in 
//        /// <paramref name="second"/> if nodes are equal.</param>
//        /// <returns>The merged XML.</returns>
//        public static XmlDocument Merge(IXPathNavigable first, IXPathNavigable second, bool replace)
//        {
//            // Holds the merged results.
//            StringBuilder sb = new StringBuilder();
//            XmlTextWriter tw = new XmlTextWriter(new StringWriter(sb));
//            tw.Formatting = Formatting.None;

//            // Specify whether second node replaces text from first one.
//            XsltArgumentList args = new XsltArgumentList();
//            args.AddParam("replace", String.Empty, replace);

//            Transformation.Transform(Input, args, tw, new MsTestXmlNodeResolver(first, second));
//            tw.Flush();

//            XmlDocument doc = new XmlDocument();
//            doc.LoadXml(sb.ToString());

//            return doc;
//        }

//        public static XmlDocument Merge(string testResultLocations, bool replace)
//        {
//            ArrayList resultFiles = new ArrayList(); 
//            ArrayList resultFiles2 = new ArrayList(); 

//                  }
//    }

//    public class MsTestXmlNodeResolver : XmlResolver
//    {
//        IXPathNavigable _first;
//        IXPathNavigable _second;

//        public MsTestXmlNodeResolver(IXPathNavigable first, IXPathNavigable second)
//        {
//            _first = first;
//            _second = second;
//        }

//        public override object GetEntity(Uri absoluteUri, string role, Type ofObjectToReturn)
//        {
//            if (absoluteUri.Authority == "First")
//                return _first.CreateNavigator();
//            else if (absoluteUri.Authority == "Second")
//                return _second.CreateNavigator();
//            return null;
//        }

//        public override System.Net.ICredentials Credentials
//        {
//            set { }
//        }
//    }
//    public class TestResultReader : XmlTestReader
//    {
//        public TestResultReader()
//        { }

//        public DataTable GetTable()
//        {
//            DataTable resultTable = null;
//            PropertyInfo[] properties = this.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.GetProperty);

//            foreach (PropertyInfo property in properties)
//            {
//                if (property.Name == "Table")
//                    resultTable = (DataTable)property.GetValue(this, null);
//            }

//            return resultTable;
//        }
//    }

//}
