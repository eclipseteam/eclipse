////using System;
////using System.Collections.Generic;
////using System.Text;
////using System.Data;
////using System.Reflection;

////using Microsoft.VisualStudio.TestTools.Common;

////namespace NableBuildTasks
////{
////    public class TestResultReader : XmlTestReader
////    {
////        public TestResultReader()
////        { }

////        public DataTable GetTable()
////        {
////            DataTable resultTable = null; 
////            PropertyInfo [] properties = this.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.GetProperty);
            
////            foreach (PropertyInfo property in properties)
////            {
////                if(property.Name == "Table")
////                    resultTable = (DataTable)property.GetValue(this, null);
////            }

////            return resultTable;
////        }
////    }
////}
//namespace BuildMsbuildProject
//{
//    using Microsoft.VisualStudio.TestTools.Diagnostics;
//    using Microsoft.VisualStudio.TestTools.Exceptions;
//    using Microsoft.VisualStudio.TestTools.Resources;
//    using System;
//    using System.Collections;
//    using System.Collections.Generic;
//    using System.ComponentModel;
//    using System.Globalization;
//    using System.IO;
//    using System.Reflection;
//    using System.Runtime.InteropServices;
//    using System.Text;
//    using System.Xml;
//    using Microsoft.VisualStudio.TestTools.Common;

//    public class XmlTestReader
//    {
//        static XmlTestReader()
//        {
//            XmlTestReader.StringBuilderType = typeof(StringBuilder);
//            XmlTestReader.GuidType = typeof(Guid);
//            XmlTestReader.StringType = typeof(string);
//            XmlTestReader.TimeSpanType = typeof(TimeSpan);
//            XmlTestReader.SimpleArrayElementSeparatorArray = new char[] { ',' };
//        }

//        public XmlTestReader()
//        {
//            this.m_typeNameToTypeMap = new Dictionary<string, Type>();
//        }

//        private bool AdvanceReader()
//        {
//            try
//            {
//                while (this.reader.Read())
//                {
//                    switch (this.reader.NodeType)
//                    {
//                        case XmlNodeType.Element:
//                            {
//                                EqtTrace.Verbose("<NAME> " + this.reader.Name);
//                                if (this.reader.Name != "Tests")
//                                {
//                                    return true;
//                                }
//                                continue;
//                            }
//                        case XmlNodeType.Text:
//                            EqtTrace.Verbose("<TEXT> " + this.reader.Value);
//                            return true;

//                        case XmlNodeType.CDATA:
//                            {
//                                this.Error((string)Messages.TMI_XMLUnexpectedNodeType("XmlNodeType.CDATA"));
//                                continue;
//                            }
//                        case XmlNodeType.EntityReference:
//                            {
//                                this.Error((string)Messages.TMI_XMLUnexpectedNodeType("XmlNodeType.EntityReference"));
//                                continue;
//                            }
//                        case XmlNodeType.ProcessingInstruction:
//                            {
//                                this.Error((string)Messages.TMI_XMLUnexpectedNodeType("XmlNodeType.ProcessingInstruction"));
//                                continue;
//                            }
//                        case XmlNodeType.Comment:
//                        case XmlNodeType.XmlDeclaration:
//                            {
//                                continue;
//                            }
//                        case XmlNodeType.Document:
//                            {
//                                this.Error((string)Messages.TMI_XMLUnexpectedNodeType("XmlNodeType.Document"));
//                                continue;
//                            }
//                        case XmlNodeType.DocumentType:
//                            {
//                                this.Error((string)Messages.TMI_XMLUnexpectedNodeType("XmlNodeType.DocumentType"));
//                                continue;
//                            }
//                        case XmlNodeType.EndElement:
//                            {
//                                EqtTrace.Verbose("</NAME> " + this.reader.Name);
//                                if (this.reader.Name != "Tests")
//                                {
//                                    return true;
//                                }
//                                continue;
//                            }
//                    }
//                    this.Error((string)Messages.TMI_XMLUnexpectedNodeType(this.reader.NodeType.ToString()));
//                }
//            }
//            catch (Exception exception1)
//            {
//                this.Error(exception1.Message);
//            }
//            catch
//            {
//                this.Error((string)Messages.get_Common_NonClsException());
//            }
//            return false;
//        }

//        internal static bool CanUseSimpleArray(Type elementType)
//        {
//            if ((elementType != null) && elementType.IsPrimitive)
//            {
//                return !elementType.Equals(typeof(char));
//            }
//            return false;
//        }

//        private void CheckElementName(string expectedName)
//        {
//            if (this.reader.Name != expectedName)
//            {
//                this.Error((string)Messages.TMI_XMLUnexpectedElementName(expectedName));
//            }
//        }

//        private void CheckNodeType(XmlNodeType expected)
//        {
//            if (this.reader.NodeType != expected)
//            {
//                this.Error((string)Messages.TMI_XMLUnexpectedNodeTypeExpecting(this.reader.NodeType.ToString(), expected.ToString()));
//            }
//        }

//        private void Error(string what)
//        {
//            this.Error(what, null);
//        }

//        private void Error(string what, Exception e)
//        {
//            throw new ErrorReadingStorageException((string)Messages.TMI_XMLError(what, this.fileName, this.reader.LineNumber, this.reader.LinePosition), e);
//        }

//        internal static string GetPersistenceNameForField(FieldInfo field)
//        {
//            object[] objArray1 = field.GetCustomAttributes(typeof(PersistenceElementNameAttribute), true);
//            if ((objArray1 != null) && (objArray1.Length > 0))
//            {
//                return ((PersistenceElementNameAttribute)objArray1[0]).Name;
//            }
//            return field.Name;
//        }

//        private Type InstantiateType(string typeName)
//        {
//            Type type1 = null;
//            if (!this.m_typeNameToTypeMap.TryGetValue(typeName, out type1))
//            {
//                try
//                {
//                    type1 = this.InstantiateTypeImpl(typeName);
//                }
//                catch (Exception)
//                {
//                    string text1;
//                    if (!this.Table.TryConvertStoredTypeName(typeName, out text1))
//                    {
//                        throw;
//                    }
//                    type1 = this.InstantiateTypeImpl(text1);
//                }
//                this.m_typeNameToTypeMap.Add(typeName, type1);
//            }
//            return type1;
//        }

//        private Type InstantiateTypeImpl(string typeName)
//        {
//            if (typeName.Contains(",") && !typeName.Contains("["))
//            {
//                try
//                {
//                    string text1 = typeName.Substring(0, typeName.IndexOf(','));
//                    Type type1 = Type.GetType(text1, false);
//                    if (type1 != null)
//                    {
//                        return type1;
//                    }
//                }
//                catch
//                {
//                }
//            }
//            try
//            {
//                Type type2 = Type.GetType(typeName, false);
//                if (type2 == null)
//                {
//                    return Type.GetType(XmlTestReader.RemoveVersionCulturePkInfo(typeName), true);
//                }
//                return type2;
//            }
//            catch
//            {
//            }
//        }

//        internal static bool IsASimpleType(Type fieldType)
//        {
//            if ((!fieldType.IsPrimitive && (fieldType != XmlTestReader.GuidType)) && ((fieldType != XmlTestReader.StringType) && (fieldType != XmlTestReader.StringBuilderType)))
//            {
//                return (fieldType == XmlTestReader.TimeSpanType);
//            }
//            return true;
//        }

//        public object[] Read(string location)
//        {
//            object[] objArray1;
//            EqtAssert.StringNotNullOrEmpty(location, "location");
//            if (!File.Exists(location))
//            {
//                throw new FileNotFoundException((string)Messages.TMI_InputFileNotFound(location));
//            }
//            this.fileName = location;
//            ArrayList list1 = new ArrayList();
//            try
//            {
//                using (XmlTextReader reader2 = (this.reader = new XmlTextReader(this.fileName)))
//                {
//                    this.reader.WhitespaceHandling = WhitespaceHandling.None;
//                    if (!this.ReadVersionTag())
//                    {
//                        return list1.ToArray();
//                    }
//                    string text1 = null;
//                    do
//                    {
//                        this.CheckNodeType(XmlNodeType.Element);
//                        text1 = this.reader.Name;
//                        Type type1 = this.ReadTypeAttribute(true);
//                        object obj1 = this.ReadOneClass(text1, type1);
//                        list1.Add(obj1);
//                    }
//                    while (this.AdvanceReader());
//                }
//                objArray1 = list1.ToArray();
//            }
//            catch (EqtException)
//            {
//                throw;
//            }
//            catch (Exception exception1)
//            {
//                this.Error(exception1.Message, exception1);
//                objArray1 = null;
//            }
//            catch
//            {
//                this.Error((string)Messages.get_Common_NonClsException());
//                objArray1 = null;
//            }
//            finally
//            {
//                this.reader = null;
//                this.fileName = null;
//            }
//            return objArray1;
//        }

//        private ArrayList ReadArray(string arrayTagName)
//        {
//            ArrayList list1 = new ArrayList();
//            Type type1 = this.ReadTypeAttribute(false);
//            if (XmlTestReader.CanUseSimpleArray(type1))
//            {
//                if (!this.AdvanceReader())
//                {
//                    this.Error((string)Messages.get_TMI_XMLCantReadElementValue());
//                }
//                this.CheckNodeType(XmlNodeType.Text);
//                return this.ReadSimpleArrayValues(list1, type1);
//            }
//        Label_003E:
//            if (!this.AdvanceReader())
//            {
//                this.Error((string)Messages.get_TMI_CantReadElementTag());
//            }
//            if ((this.reader.NodeType == XmlNodeType.EndElement) && (this.reader.Name == arrayTagName))
//            {
//                return list1;
//            }
//            this.CheckNodeType(XmlNodeType.Element);
//            this.CheckElementName("element");
//            if (!this.reader.IsEmptyElement)
//            {
//                object obj1 = this.ReadOneClass("element", this.ReadTypeAttribute(true));
//                list1.Add(obj1);
//                goto Label_003E;
//            }
//            list1.Add(null);
//            goto Label_003E;
//        }

//        private Hashtable ReadHashtable(string hashTagName)
//        {
//            Hashtable hashtable1 = new Hashtable();
//            while (true)
//            {
//                if (!this.AdvanceReader())
//                {
//                    this.Error((string)Messages.get_TMI_CantReadKeyTag());
//                }
//                if ((this.reader.NodeType == XmlNodeType.EndElement) && (this.reader.Name == hashTagName))
//                {
//                    return hashtable1;
//                }
//                this.CheckNodeType(XmlNodeType.Element);
//                this.CheckElementName("key");
//                object obj1 = this.ReadOneClass("key", this.ReadTypeAttribute(true));
//                if (!this.AdvanceReader())
//                {
//                    this.Error((string)Messages.get_TMI_CantReadValueTag());
//                }
//                this.CheckNodeType(XmlNodeType.Element);
//                this.CheckElementName("value");
//                Type type1 = this.ReadTypeAttribute(false);
//                object obj2 = null;
//                if (this.reader.IsEmptyElement)
//                {
//                    if ((type1 == null) || !type1.Equals(typeof(string)))
//                    {
//                        obj2 = null;
//                    }
//                    else
//                    {
//                        obj2 = string.Empty;
//                    }
//                }
//                else
//                {
//                    obj2 = this.ReadOneClass("value", type1);
//                }
//                hashtable1.Add(obj1, obj2);
//            }
//        }

//        private object ReadOneClass(string classElementName, Type type)
//        {
//            if (this.reader.IsEmptyElement)
//            {
//                if (type.Equals(typeof(string)))
//                {
//                    return string.Empty;
//                }
//                try
//                {
//                    return Activator.CreateInstance(type, true);
//                }
//                catch
//                {
//                    return null;
//                }
//            }
//            if (type.IsArray)
//            {
//                return this.ReadArray(classElementName);
//            }
//            object obj1 = null;
//            if (XmlTestReader.IsASimpleType(type))
//            {
//                obj1 = this.ReadSimpleType(type);
//                if (!this.AdvanceReader())
//                {
//                    this.Error((string)Messages.get_TMI_XMLCantReadElementName());
//                }
//                this.CheckNodeType(XmlNodeType.EndElement);
//                this.CheckElementName(classElementName);
//                return obj1;
//            }
//            try
//            {
//                obj1 = Activator.CreateInstance(type, true);
//            }
//            catch (Exception exception1)
//            {
//                this.Error((string)Messages.TMI_FailedToCreateInstanceOf(type.FullName), exception1);
//            }
//            catch
//            {
//                this.Error((string)Messages.TMI_FailedToCreateInstanceOf(type.FullName));
//            }
//            BindingFlags flags1 = BindingFlags.FlattenHierarchy | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;
//            FieldInfo[] infoArray1 = type.GetFields(flags1);
//            Hashtable hashtable1 = new Hashtable();
//            foreach (FieldInfo info1 in infoArray1)
//            {
//                try
//                {
//                    hashtable1.Add(XmlTestReader.GetPersistenceNameForField(info1), info1);
//                }
//                catch (ArgumentException)
//                {
//                    throw;
//                }
//            }
//            int num1 = 1;
//            while (((this.reader.NodeType != XmlNodeType.EndElement) || (this.reader.Name != classElementName)) || (num1 != 0))
//            {
//                if (!this.AdvanceReader())
//                {
//                    this.Error((string)Messages.get_TMI_XMLCantReadElementName());
//                }
//                if (this.reader.NodeType == XmlNodeType.EndElement)
//                {
//                    if (this.reader.Name == classElementName)
//                    {
//                        EqtTrace.Verbose("--" + this.reader.Name);
//                        num1--;
//                    }
//                }
//                else
//                {
//                    object obj3;
//                    this.CheckNodeType(XmlNodeType.Element);
//                    string text1 = this.reader.Name;
//                    if (text1 == classElementName)
//                    {
//                        EqtTrace.Verbose("++" + this.reader.Name);
//                        num1++;
//                    }
//                    FieldInfo info2 = (FieldInfo)hashtable1[text1];
//                    if (info2 == null)
//                    {
//                        ConversionEntry entry1;
//                        bool flag1 = false;
//                        if (this.Table.TryGetConversionEntry(type, text1, out entry1))
//                        {
//                            if (entry1.ConversionType == ConversionType.Delete)
//                            {
//                                this.SkipCurrentElement();
//                                continue;
//                            }
//                            if (entry1.ConversionType == ConversionType.Change)
//                            {
//                                info2 = (FieldInfo)hashtable1[entry1.NewPersistanceName];
//                                flag1 = info2 != null;
//                            }
//                        }
//                        if (!flag1)
//                        {
//                            this.Error((string)Messages.TMI_DoesNotKnowWhatToDoWithElement(type.FullName, text1));
//                        }
//                    }
//                    Type type1 = this.ReadTypeAttribute(false);
//                    if (type1 == null)
//                    {
//                        type1 = info2.FieldType;
//                    }
//                    object obj2 = this.ReadOneElement(text1, type1);
//                    if ((type1.IsEnum && (this.m_upgradeType != UpgradeType.None)) && this.Table.TryConvertEnumValue(this.m_upgradeType, type1, obj2, out obj3))
//                    {
//                        obj2 = obj3;
//                    }
//                    try
//                    {
//                        info2.SetValue(obj1, obj2, flags1, null, null);
//                        continue;
//                    }
//                    catch (ArgumentException)
//                    {
//                        ConversionEntry entry2;
//                        if (this.Table.TryGetConversionEntry(type, text1, out entry2) && (entry2.ConversionType == ConversionType.ChangeType))
//                        {
//                            if (entry2.TryConvertType(obj1.GetType(), obj1, obj2, out obj2))
//                            {
//                                info2.SetValue(obj1, obj2, flags1, null, null);
//                            }
//                            continue;
//                        }
//                        throw;
//                    }
//                }
//            }
//            IPersistable persistable1 = obj1 as IPersistable;
//            if (persistable1 != null)
//            {
//                try
//                {
//                    persistable1.RecoverFromPersistence();
//                }
//                catch (Exception exception2)
//                {
//                    this.Error((string)Messages.TMI_XMLRecoverFromPersistenceFailure(obj1.GetType().FullName, exception2.GetType().FullName, exception2.Message), exception2);
//                }
//                catch
//                {
//                    this.Error((string)Messages.TMI_XMLRecoverFromPersistenceFailure(obj1.GetType().FullName, Messages.get_Common_NonClsException(), string.Empty));
//                }
//            }
//            return obj1;
//        }

//        private object ReadOneElement(string elementName, Type fieldType)
//        {
//            if (XmlTestReader.IsASimpleType(fieldType))
//            {
//                return this.ReadSimpleType(fieldType);
//            }
//            if (fieldType.Equals(typeof(Type)))
//            {
//                if (!this.AdvanceReader())
//                {
//                    this.Error((string)Messages.get_TMI_XMLCantReadElementValue());
//                }
//                this.CheckNodeType(XmlNodeType.Text);
//                return this.InstantiateType(this.reader.Value);
//            }
//            if (fieldType.IsArray)
//            {
//                Type type1 = fieldType.GetElementType();
//                ArrayList list1 = this.ReadArray(elementName);
//                Array array1 = Array.CreateInstance(type1, list1.Count);
//                list1.CopyTo(0, array1, 0, list1.Count);
//                return array1;
//            }
//            if (typeof(ArrayList).IsAssignableFrom(fieldType))
//            {
//                return this.ReadArray(elementName);
//            }
//            if (typeof(Hashtable).IsAssignableFrom(fieldType))
//            {
//                return this.ReadHashtable(elementName);
//            }
//            Type type2 = this.ReadTypeAttribute(false);
//            if (type2 == null)
//            {
//                type2 = fieldType.UnderlyingSystemType;
//            }
//            return this.ReadOneClass(elementName, type2);
//        }

//        private ArrayList ReadSimpleArrayValues(ArrayList list, Type elementType)
//        {
//            string[] textArray1 = this.reader.Value.Split(XmlTestReader.SimpleArrayElementSeparatorArray, StringSplitOptions.RemoveEmptyEntries);
//            TypeConverter converter1 = TypeDescriptor.GetConverter(elementType);
//            for (int num1 = 0; num1 < textArray1.Length; num1++)
//            {
//                try
//                {
//                    list.Add(converter1.ConvertFrom(null, CultureInfo.InvariantCulture, textArray1[num1]));
//                }
//                catch (FormatException exception1)
//                {
//                    this.Error(exception1.Message);
//                }
//            }
//            return list;
//        }

//        private object ReadSimpleType(Type type)
//        {
//            if (this.reader.IsEmptyElement)
//            {
//                if (type.Equals(typeof(string)))
//                {
//                    return string.Empty;
//                }
//                if (type.Equals(typeof(StringBuilder)))
//                {
//                    return new StringBuilder();
//                }
//                return null;
//            }
//            if (!this.AdvanceReader())
//            {
//                this.Error((string)Messages.get_TMI_XMLCantReadElementValue());
//            }
//            this.CheckNodeType(XmlNodeType.Text);
//            try
//            {
//                if (type == typeof(StringBuilder))
//                {
//                    return new StringBuilder(this.reader.Value);
//                }
//                if (type == typeof(IntPtr))
//                {
//                    return new IntPtr((int)TypeDescriptor.GetConverter(typeof(int)).ConvertFrom(null, CultureInfo.InvariantCulture, this.reader.Value));
//                }
//                return TypeDescriptor.GetConverter(type).ConvertFrom(null, CultureInfo.InvariantCulture, this.reader.Value);
//            }
//            catch (FormatException exception1)
//            {
//                this.Error(exception1.Message);
//            }
//            catch (Exception)
//            {
//                char ch1 = ',';
//                if (!this.reader.Value.Contains(ch1.ToString()))
//                {
//                    throw;
//                }
//                return this.ReadSimpleArrayValues(new ArrayList(), type).ToArray(type);
//            }
//            catch
//            {
//                EqtTrace.Error("Got non-CLS-compliant exception in XmlTestReader.ReadSimpleType.");
//            }
//            return null;
//        }

//        public TestRun ReadTestRunSummary(string location)
//        {
//            TestRun run2;
//            EqtAssert.StringNotNullOrEmpty(location, "location");
//            if (!File.Exists(location))
//            {
//                EqtTrace.Warning("XmlTestReader.ReadTestRunSummary: result file not found at {0}", new object[] { location });
//                throw new FileNotFoundException((string)Messages.TMI_InputFileNotFound(location));
//            }
//            this.fileName = location;
//            TestRun run1 = null;
//            try
//            {
//                using (XmlTextReader reader2 = (this.reader = new XmlTextReader(this.fileName)))
//                {
//                    this.reader.WhitespaceHandling = WhitespaceHandling.None;
//                    if (!this.ReadVersionTag())
//                    {
//                        return null;
//                    }
//                    do
//                    {
//                        if (this.reader.NodeType == XmlNodeType.Element)
//                        {
//                            Type type1 = this.ReadTypeAttribute(false);
//                            if (type1 == typeof(TestRun))
//                            {
//                                break;
//                            }
//                        }
//                    }
//                    while (this.AdvanceReader());
//                    string text1 = this.reader.Name;
//                    if (!this.AdvanceReader())
//                    {
//                        this.Error((string)Messages.get_TMI_XMLCantReadElementValue());
//                    }
//                    run1 = Activator.CreateInstance(typeof(TestRun), true) as TestRun;
//                    if (run1 == null)
//                    {
//                        this.Error((string)Messages.TMI_FailedToCreateInstanceOf(typeof(TestRun).FullName));
//                    }
//                    BindingFlags flags1 = BindingFlags.FlattenHierarchy | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;
//                    FieldInfo[] infoArray1 = typeof(TestRun).GetFields(flags1);
//                    Dictionary<string, FieldInfo> dictionary1 = new Dictionary<string, FieldInfo>();
//                    foreach (FieldInfo info1 in infoArray1)
//                    {
//                        dictionary1.Add(XmlTestReader.GetPersistenceNameForField(info1), info1);
//                    }
//                    do
//                    {
//                        if ((this.reader.NodeType == XmlNodeType.EndElement) && (text1 == this.reader.Name))
//                        {
//                            goto Label_0252;
//                        }
//                        if (this.reader.NodeType == XmlNodeType.Element)
//                        {
//                            Type type2 = this.ReadTypeAttribute(true);
//                            if ((type2 == typeof(Hashtable)) || type2.IsArray)
//                            {
//                                this.SkipCurrentElement();
//                            }
//                            else
//                            {
//                                FieldInfo info2;
//                                if (!dictionary1.TryGetValue(this.reader.Name, out info2))
//                                {
//                                    this.Error((string)Messages.TMI_DoesNotKnowWhatToDoWithElement(type2.FullName, this.reader.Name));
//                                }
//                                object obj1 = this.ReadOneElement(this.reader.Name, type2);
//                                info2.SetValue(run1, obj1, flags1, null, null);
//                                Guid guid1 = run1.Id;
//                                if (((run1.Name != null) && (run1.RunUser != null)) && ((run1.RunConfiguration != null) && (run1.Result != null)))
//                                {
//                                    EqtTrace.Verbose("XmlTestReader.ReadTestRunSummary: All summary fields set for TestRun");
//                                    goto Label_0252;
//                                }
//                            }
//                        }
//                    }
//                    while (this.AdvanceReader());
//                }
//            Label_0252:
//                EqtTrace.Verbose("XmlTestReader.ReadTestRunSummary finished successfully");
//                run2 = run1;
//            }
//            catch (EqtException)
//            {
//                throw;
//            }
//            catch (Exception exception1)
//            {
//                this.Error(exception1.Message, exception1);
//                run2 = null;
//            }
//            catch
//            {
//                this.Error((string)Messages.get_Common_NonClsException());
//                run2 = null;
//            }
//            finally
//            {
//                this.reader = null;
//                this.fileName = null;
//            }
//            return run2;
//        }

//        private Type ReadTypeAttribute(bool mustExist)
//        {
//            string text1 = null;
//            if (this.reader.HasAttributes)
//            {
//                text1 = this.reader.GetAttribute("type");
//                if (text1 == null)
//                {
//                    this.Error((string)Messages.get_TMI_ExpectedTypeAttributeNotFound());
//                }
//            }
//            else if (mustExist)
//            {
//                this.Error((string)Messages.get_TMI_XMLExpectingTypeFoundNothing());
//            }
//            else
//            {
//                return null;
//            }
//            Type type1 = null;
//            try
//            {
//                type1 = this.InstantiateType(text1);
//            }
//            catch (Exception exception1)
//            {
//                throw new FailedToInstantiateTypeException((string)Messages.TMI_XmlFailedToInstantiateType(text1, exception1.Message), text1, exception1);
//            }
//            catch
//            {
//                throw new FailedToInstantiateTypeException((string)Messages.TMI_XmlFailedToInstantiateType(text1, Messages.get_Common_NullInMessages()), text1, new EqtException(Messages.get_Common_NonClsException()));
//            }
//            return type1;
//        }

//        private bool ReadVersionTag()
//        {
//            if (!this.AdvanceReader())
//            {
//                return false;
//            }
//            if (string.Compare("edtdocversion", this.reader.Name, StringComparison.OrdinalIgnoreCase) == 0)
//            {
//                bool flag1 = true;
//                int num1 = 0;
//                int num2 = 0;
//                string text1 = this.reader.GetAttribute("build");
//                if ((text1 == null) || !int.TryParse(text1, out num1))
//                {
//                    flag1 = false;
//                }
//                text1 = this.reader.GetAttribute("revision");
//                if ((text1 == null) || !int.TryParse(text1, out num2))
//                {
//                    flag1 = false;
//                }
//                string text2 = this.reader.GetAttribute("branch");
//                if (text2 == null)
//                {
//                    flag1 = false;
//                }
//                if (!flag1)
//                {
//                    throw new Exception((string)Messages.get_Common_InvalidVersion());
//                }
//                if (string.Compare(text2, "retail", StringComparison.OrdinalIgnoreCase) == 0)
//                {
//                    if ((num1 == Environment.Version.Build) && (num2 == Environment.Version.Revision))
//                    {
//                        this.m_upgradeType = UpgradeType.None;
//                    }
//                    else
//                    {
//                        this.m_upgradeType = UpgradeType.Lab23DevToRetail;
//                    }
//                }
//                else
//                {
//                    if (string.Compare(text2, "lab23df", StringComparison.OrdinalIgnoreCase) != 0)
//                    {
//                        throw new Exception((string)Messages.get_Common_InvalidVersion());
//                    }
//                    this.m_upgradeType = UpgradeType.DogfoodToRetail;
//                }
//                this.SkipCurrentElement();
//                if (!this.AdvanceReader())
//                {
//                    return false;
//                }
//            }
//            else
//            {
//                this.m_upgradeType = UpgradeType.Beta2ToRetail;
//            }
//            return true;
//        }

//        private static string RemoveVersionCulturePkInfo(string typeName)
//        {
//            if (typeName.IndexOf("[", StringComparison.Ordinal) >= 0)
//            {
//                return typeName;
//            }
//            int num1 = typeName.IndexOf(",", StringComparison.Ordinal);
//            if (num1 == -1)
//            {
//                return typeName;
//            }
//            num1 = typeName.IndexOf(",", num1 + 1, StringComparison.Ordinal);
//            if (num1 < 0)
//            {
//                return typeName;
//            }
//            return typeName.Substring(0, num1);
//        }

//        private void SkipCurrentElement()
//        {
//            string text1 = this.reader.Name;
//            int num1 = 1;
//            bool flag1 = false;
//            if (!this.reader.IsEmptyElement)
//            {
//                do
//                {
//                    if (!this.AdvanceReader())
//                    {
//                        return;
//                    }
//                    if (XmlNodeType.Element == this.reader.NodeType)
//                    {
//                        if (string.CompareOrdinal(text1, this.reader.Name) == 0)
//                        {
//                            num1++;
//                        }
//                    }
//                    else if ((XmlNodeType.EndElement == this.reader.NodeType) && (string.CompareOrdinal(text1, this.reader.Name) == 0))
//                    {
//                        num1--;
//                        flag1 = 0 == num1;
//                    }
//                }
//                while (!flag1);
//            }
//        }


//        private ConversionTable Table
//        {
//            get
//            {
//                if (this.m_table == null)
//                {
//                    this.m_table = new ConversionTable();
//                }
//                return this.m_table;
//            }
//        }


//        internal const string ArrayElementElementName = "element";
//        internal const string Branch = "retail";
//        private string fileName;
//        private static readonly Type GuidType;
//        internal const string KeyElementName = "key";
//        private ConversionTable m_table;
//        private Dictionary<string, Type> m_typeNameToTypeMap;
//        private UpgradeType m_upgradeType;
//        private XmlTextReader reader;
//        internal const string RootElementName = "Tests";
//        internal const char SimpleArrayElementSeparator = ',';
//        private static char[] SimpleArrayElementSeparatorArray;
//        private static readonly Type StringBuilderType;
//        private static readonly Type StringType;
//        private static readonly Type TimeSpanType;
//        internal const string TypeAttrName = "type";
//        internal const string ValueElementName = "value";
//        internal const string VersionBranchAttributeName = "branch";
//        internal const string VersionBuildAttributeName = "build";
//        internal const string VersionElementName = "edtdocversion";
//        internal const string VersionRevisionAttributeName = "revision";


//        public sealed class ConversionEntry
//        {
//            private ConversionEntry()
//            {
//            }

//            internal static XmlTestReader.ConversionEntry CreateChangeEntry(string oldFieldName, string newFieldName)
//            {
//                return XmlTestReader.ConversionEntry.CreateChangeEntry(oldFieldName, oldFieldName, newFieldName, newFieldName);
//            }

//            internal static XmlTestReader.ConversionEntry CreateChangeEntry(string oldFieldName, string oldPersistanceName, string newFieldName, string newPersistanceName)
//            {
//                XmlTestReader.ConversionEntry entry1 = new XmlTestReader.ConversionEntry();
//                entry1.m_oldPersistanceName = oldPersistanceName;
//                entry1.m_oldFieldName = oldFieldName;
//                entry1.m_newPersistanceName = newPersistanceName;
//                entry1.m_newFieldName = newFieldName;
//                entry1.m_conversionType = Microsoft.VisualStudio.TestTools.Common.XmlTestReader.ConversionType.Change;
//                return entry1;
//            }

//            internal static XmlTestReader.ConversionEntry CreateChangeTypeEntry(string oldFieldName)
//            {
//                return XmlTestReader.ConversionEntry.CreateChangeTypeEntry(oldFieldName, oldFieldName);
//            }

//            internal static XmlTestReader.ConversionEntry CreateChangeTypeEntry(string oldFieldName, string oldPersistanceName)
//            {
//                XmlTestReader.ConversionEntry entry1 = new XmlTestReader.ConversionEntry();
//                entry1.m_oldPersistanceName = oldPersistanceName;
//                entry1.m_oldFieldName = oldFieldName;
//                entry1.m_conversionType = Microsoft.VisualStudio.TestTools.Common.XmlTestReader.ConversionType.ChangeType;
//                return entry1;
//            }

//            internal static XmlTestReader.ConversionEntry CreateDeleteEntry(string oldFieldName)
//            {
//                return XmlTestReader.ConversionEntry.CreateDeleteEntry(oldFieldName, oldFieldName);
//            }

//            internal static XmlTestReader.ConversionEntry CreateDeleteEntry(string oldFieldName, string oldPersistanceName)
//            {
//                XmlTestReader.ConversionEntry entry1 = new XmlTestReader.ConversionEntry();
//                entry1.m_oldPersistanceName = oldPersistanceName;
//                entry1.m_oldFieldName = oldFieldName;
//                entry1.m_conversionType = Microsoft.VisualStudio.TestTools.Common.XmlTestReader.ConversionType.Delete;
//                return entry1;
//            }

//            internal bool TryConvertType(Type type, object target, object oldValue, out object newValue)
//            {
//                newValue = null;
//                try
//                {
//                    MethodInfo info1 = type.GetMethod("ChangedTypeOfPersistedValue", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
//                    if (info1 == null)
//                    {
//                        return false;
//                    }
//                    newValue = info1.Invoke(target, new object[] { this.OldFieldName, oldValue });
//                    return true;
//                }
//                catch (TargetInvocationException)
//                {
//                    return false;
//                }
//            }


//            internal ConversionType ConversionType
//            {
//                get
//                {
//                    return this.m_conversionType;
//                }
//            }

//            internal string NewFieldName
//            {
//                get
//                {
//                    return this.m_newFieldName;
//                }
//            }

//            internal string NewPersistanceName
//            {
//                get
//                {
//                    return this.m_newPersistanceName;
//                }
//            }

//            internal string OldFieldName
//            {
//                get
//                {
//                    return this.m_oldFieldName;
//                }
//            }

//            internal string OldPersistanceName
//            {
//                get
//                {
//                    return this.m_oldPersistanceName;
//                }
//            }


//            private ConversionType m_conversionType;
//            private string m_newFieldName;
//            private string m_newPersistanceName;
//            private string m_oldFieldName;
//            private string m_oldPersistanceName;
//        }

//        public sealed class ConversionTable
//        {
//            internal ConversionTable()
//            {
//                this.m_table = new Dictionary<string, Dictionary<string, XmlTestReader.ConversionEntry>>();
//                this.m_namespaceMap = new Dictionary<string, string>();
//                this.m_enumFromBetaTwo = new Dictionary<string, Dictionary<int, object>>();
//                this.m_enumFromDogfood = new Dictionary<string, Dictionary<int, object>>();
//                this.m_enumFromLab23Dev = new Dictionary<string, Dictionary<int, object>>();
//                this.BuildFieldChangeTable();
//                this.BuildNamespaceChangeTable();
//                this.BuildEnumValueChangeTables();
//            }

//            private void AddEntry(Dictionary<string, XmlTestReader.ConversionEntry> typeTable, XmlTestReader.ConversionEntry entry)
//            {
//                typeTable.Add(entry.OldPersistanceName, entry);
//            }

//            private void BuildEnumValueChangeTables()
//            {
//                Dictionary<int, object> dictionary1 = new Dictionary<int, object>();
//                dictionary1.Add(0, TestOutcome.Aborted);
//                dictionary1.Add(1, TestOutcome.Error);
//                dictionary1.Add(2, TestOutcome.Inconclusive);
//                dictionary1.Add(3, TestOutcome.Failed);
//                dictionary1.Add(4, TestOutcome.NotRunnable);
//                dictionary1.Add(5, TestOutcome.NotExecuted);
//                dictionary1.Add(6, TestOutcome.Disconnected);
//                dictionary1.Add(7, TestOutcome.Warning);
//                dictionary1.Add(8, TestOutcome.InProgress);
//                dictionary1.Add(9, TestOutcome.Pending);
//                dictionary1.Add(10, TestOutcome.PassedButRunAborted);
//                dictionary1.Add(11, TestOutcome.Completed);
//                dictionary1.Add(12, TestOutcome.Passed);
//                this.m_enumFromBetaTwo.Add(typeof(TestOutcome).FullName, (Dictionary<int, object>)dictionary1);
//            }

//            private void BuildFieldChangeTable()
//            {
//                Dictionary<string, XmlTestReader.ConversionEntry> dictionary1 = new Dictionary<string, XmlTestReader.ConversionEntry>();
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateDeleteEntry("m_manualTests", "manualTestCount"));
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateDeleteEntry("m_executedTests", "executedManualTestCount"));
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateDeleteEntry("m_failedManualTests", "failedManualTests"));
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateDeleteEntry("m_manualTestOutcomeMap", "manualTestOutcomeMap"));
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateDeleteEntry("m_notManualTestFailure", "notManualTestFailure"));
//                this.m_table.Add(typeof(RunResultAndStatistics).FullName, (Dictionary<string, XmlTestReader.ConversionEntry>)dictionary1);
//                dictionary1 = new Dictionary<string, XmlTestReader.ConversionEntry>();
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateChangeEntry("executionTimeout", "agentNotRespondingTimeout"));
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateDeleteEntry("m_relativePathRoot", "relativePathRoot"));
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateDeleteEntry("m_isExecutedOutOfProc", "isExecutedOutOfProc"));
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateDeleteEntry("m_ignorePerTestAgentProperties", "ignorePerTestAgentProperties"));
//                this.m_table.Add(typeof(TestRunConfiguration).FullName, (Dictionary<string, XmlTestReader.ConversionEntry>)dictionary1);
//                dictionary1 = new Dictionary<string, XmlTestReader.ConversionEntry>();
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateDeleteEntry("m_refreshRate"));
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateDeleteEntry("m_includeDetails"));
//                this.m_table.Add("Microsoft.VisualStudio.TestTools.WebStress.RunConfig", (Dictionary<string, XmlTestReader.ConversionEntry>)dictionary1);
//                dictionary1 = new Dictionary<string, XmlTestReader.ConversionEntry>();
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateChangeTypeEntry("m_headers"));
//                this.m_table.Add("Microsoft.VisualStudio.TestTools.WebStress.WebTestBrowser", (Dictionary<string, XmlTestReader.ConversionEntry>)dictionary1);
//                dictionary1 = new Dictionary<string, XmlTestReader.ConversionEntry>();
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateDeleteEntry("m_webTest"));
//                this.m_table.Add("Microsoft.VisualStudio.TestTools.WebStress.DeclarativeWebTestElement", (Dictionary<string, XmlTestReader.ConversionEntry>)dictionary1);
//                dictionary1 = new Dictionary<string, XmlTestReader.ConversionEntry>();
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateDeleteEntry("m_scope"));
//                this.m_table.Add("Microsoft.VisualStudio.TestTools.WebTestFramework.ContextParameter", (Dictionary<string, XmlTestReader.ConversionEntry>)dictionary1);
//                dictionary1 = new Dictionary<string, XmlTestReader.ConversionEntry>();
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateDeleteEntry("m_webServerType", "webServerType"));
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateDeleteEntry("m_webSiteRootPath", "webSiteRootPath"));
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateDeleteEntry("m_webAppRoot", "webAppRoot"));
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateDeleteEntry("m_deploymentItemsOnClasses", "deploymentItemsOnClasses"));
//                this.m_table.Add("Microsoft.VisualStudio.TestTools.TestTypes.Unit.UnitTestElement", (Dictionary<string, XmlTestReader.ConversionEntry>)dictionary1);
//                dictionary1 = new Dictionary<string, XmlTestReader.ConversionEntry>();
//                this.AddEntry(dictionary1, XmlTestReader.ConversionEntry.CreateDeleteEntry("m_showWindow", "ShowWindow"));
//                this.m_table.Add("Microsoft.VisualStudio.TestTools.TestTypes.Generic.GenericTest", (Dictionary<string, XmlTestReader.ConversionEntry>)dictionary1);
//            }

//            private void BuildNamespaceChangeTable()
//            {
//                this.m_namespaceMap.Add("Microsoft.VisualStudio.QualityTools.Common.", "Microsoft.VisualStudio.TestTools.Common.");
//                this.m_namespaceMap.Add("Microsoft.VisualStudio.QualityTools.WebStress.", "Microsoft.VisualStudio.TestTools.WebStress.");
//                this.m_namespaceMap.Add("Microsoft.VisualStudio.QualityTools.WebTestFramework.", "Microsoft.VisualStudio.TestTools.WebTesting.");
//                this.m_namespaceMap.Add("Microsoft.VisualStudio.QualityTools.Tips.UnitTest.", "Microsoft.VisualStudio.TestTools.TestTypes.Unit.");
//                this.m_namespaceMap.Add("Microsoft.VisualStudio.QualityTools.Tips.GenericTest.", "Microsoft.VisualStudio.TestTools.TestTypes.Generic.");
//                this.m_namespaceMap.Add("Microsoft.VisualStudio.QualityTools.Tips.ManualTest.", "Microsoft.VisualStudio.TestTools.TestTypes.Manual.");
//                this.m_namespaceMap.Add("Microsoft.VisualStudio.QualityTools.LoadTestFramework.", "Microsoft.VisualStudio.TestTools.LoadTesting.");
//                this.m_namespaceMap.Add("Microsoft.VisualStudio.QualityTools.UnitTesting.Framework.", "Microsoft.VisualStudio.TestTools.UnitTesting.");
//                this.m_namespaceMap.Add("Microsoft.VisualStudio.QualityTools.Tips.TestSuite.", "Microsoft.VisualStudio.TestTools.TestTypes.Ordered.");
//            }

//            private Dictionary<string, Dictionary<int, object>> GetEnumConversionTable(XmlTestReader.UpgradeType type)
//            {
//                switch (type)
//                {
//                    case XmlTestReader.UpgradeType.DogfoodToRetail:
//                        return this.m_enumFromDogfood;

//                    case XmlTestReader.UpgradeType.Lab23DevToRetail:
//                        return this.m_enumFromLab23Dev;

//                    case XmlTestReader.UpgradeType.Beta2ToRetail:
//                        return this.m_enumFromBetaTwo;
//                }
//                return null;
//            }

//            private bool TryConvertComplexTypeName(string oldTypeNameFormat, out string newTypeNameFormat)
//            {
//                ParseState state1 = ParseState.Normal;
//                StringBuilder builder1 = new StringBuilder(oldTypeNameFormat.Length);
//                StringBuilder builder2 = null;
//                int num1 = 0;
//                for (int num2 = 0; num2 < oldTypeNameFormat.Length; num2++)
//                {
//                    string text1;
//                    char ch1 = oldTypeNameFormat[num2];
//                    switch (state1)
//                    {
//                        case ParseState.Normal:
//                            if ('[' == ch1)
//                            {
//                                state1 = ParseState.TypeGroup;
//                            }
//                            builder1.Append(ch1);
//                            goto Label_00DC;

//                        case ParseState.TypeGroup:
//                            if (']' != ch1)
//                            {
//                                break;
//                            }
//                            state1 = ParseState.Normal;
//                            goto Label_006F;

//                        case ParseState.NestedType:
//                            string text2;
//                            if (']' != ch1)
//                            {
//                                goto Label_00BE;
//                            }
//                            num1--;
//                            if (num1 != 0)
//                            {
//                                goto Label_00DC;
//                            }
//                            text1 = builder2.ToString();
//                            if (!this.TryConvertStoredTypeName(text1, out text2))
//                            {
//                                goto Label_00A6;
//                            }
//                            builder1.Append(text2);
//                            goto Label_00AF;

//                        default:
//                            goto Label_00DC;
//                    }
//                    if ('[' == ch1)
//                    {
//                        state1 = ParseState.NestedType;
//                        builder2 = new StringBuilder();
//                        num1 = 1;
//                    }
//                Label_006F:
//                    builder1.Append(ch1);
//                    goto Label_00DC;
//                Label_00A6:
//                    builder1.Append(text1);
//                Label_00AF:
//                    builder1.Append(']');
//                    state1 = ParseState.TypeGroup;
//                    builder2 = null;
//                    goto Label_00DC;
//                Label_00BE:
//                    if ('[' == ch1)
//                    {
//                        num1++;
//                        builder2.Append(ch1);
//                    }
//                    else
//                    {
//                        builder2.Append(ch1);
//                    }
//                Label_00DC: ;
//                }
//                newTypeNameFormat = builder1.ToString();
//                int num3 = newTypeNameFormat.IndexOf('[');
//                int num4 = newTypeNameFormat.LastIndexOf('.', num3);
//                if ((num4 >= 0) && (num4 != newTypeNameFormat.Length))
//                {
//                    string text4;
//                    string text3 = newTypeNameFormat.Substring(0, num4 + 1);
//                    if (this.m_namespaceMap.TryGetValue(text3, out text4))
//                    {
//                        newTypeNameFormat = text4 + newTypeNameFormat.Substring(num4 + 1);
//                    }
//                }
//                return true;
//            }

//            private bool TryConvertSimpleTypeName(string oldTypeNameFormat, out string newTypeNameFormat)
//            {
//                string text3;
//                newTypeNameFormat = null;
//                string[] textArray1 = oldTypeNameFormat.Split(new char[] { ',' }, 2, StringSplitOptions.None);
//                if (textArray1.Length != 2)
//                {
//                    return false;
//                }
//                StringBuilder builder1 = new StringBuilder(oldTypeNameFormat.Length);
//                string text1 = textArray1[0];
//                int num1 = text1.LastIndexOf('.');
//                if ((num1 < 0) || (num1 == text1.Length))
//                {
//                    return false;
//                }
//                string text2 = text1.Substring(0, num1 + 1);
//                if (!this.m_namespaceMap.TryGetValue(text2, out text3))
//                {
//                    return false;
//                }
//                builder1.Append(text3);
//                builder1.Append(text1.Substring(num1 + 1));
//                builder1.Append(',');
//                builder1.Append(textArray1[1]);
//                newTypeNameFormat = builder1.ToString();
//                return true;
//            }

//            internal bool TryConvertStoredTypeName(string oldTypeNameFormat, out string newTypeNameFormat)
//            {
//                if (oldTypeNameFormat.IndexOf('[') < 0)
//                {
//                    return this.TryConvertSimpleTypeName(oldTypeNameFormat, out newTypeNameFormat);
//                }
//                return this.TryConvertComplexTypeName(oldTypeNameFormat, out newTypeNameFormat);
//            }

//            private Dictionary<string, Dictionary<int, object>> m_enumFromBetaTwo;
//            private Dictionary<string, Dictionary<int, object>> m_enumFromDogfood;
//            private Dictionary<string, Dictionary<int, object>> m_enumFromLab23Dev;
//            private Dictionary<string, string> m_namespaceMap;
//            private Dictionary<string, Dictionary<string, XmlTestReader.ConversionEntry>> m_table;


//            public enum ParseState
//            {
//                Normal,
//                TypeGroup,
//                NestedType
//            }
//        }

//        public enum ConversionType
//        {
//            Change,
//            Delete,
//            ChangeType
//        }

//        public enum UpgradeType
//        {
//            None,
//            DogfoodToRetail,
//            Lab23DevToRetail,
//            Beta2ToRetail
//        }
//    }
//}

