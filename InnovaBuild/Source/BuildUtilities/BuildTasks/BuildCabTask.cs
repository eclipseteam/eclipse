using System;
using System.Diagnostics;
using System.Text;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Security.AccessControl;
using Microsoft.Build.BuildEngine;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace NableBuildTasks
{
    public class BuildCabTask : Task
    {
        static readonly string cabFileName = "CABARC.EXE ";
        static readonly string cabSourcesFileName = "CabSourcesFile.txt";

        StreamWriter cabSourcesFileWriter = null;

        string assemblyDirectory = null;
        string solutionDirectory = null;
        string projectDirectory = null;
        string cabCommandPath = null;
        string destinationFile = null;
        string contentProjectFiles = null;
        string outputProjectFiles = null;
        ArrayList filesAttributeChanged = new ArrayList();

        StringBuilder argumentStringBuilder = null;

        public string AssemblyDirectory
        {
            set { assemblyDirectory = value; }
        }

        public string SolutionDirectory
        {
            set { solutionDirectory = value; }
        }

        public string ProjectDirectory
        {
            set { projectDirectory = value; }
        }

        public string CabCommandPath
        {
            set { cabCommandPath = value; }
        }

        public string DestinationFile
        {
            set { destinationFile = value; }
        }

        public string ContentProjectFiles
        {
            set { contentProjectFiles = value; }
        }

        public string OutputProjectFiles
        {
            set { outputProjectFiles = value; }
        }

        private bool areNamesValid()
        {

            if (assemblyDirectory == null)
            {
                Log.LogError("Assembly path not specified");
                return false;
            }

            if (solutionDirectory == null)
            {
                Log.LogError("Solution path not specified");
                return false;
            }

            if (projectDirectory == null)
            {
                Log.LogError("Project path not specified");
                return false;
            }

            if (cabCommandPath == null)
            {
                Log.LogError("Cab file path not specified");
                return false;
            }

            if (destinationFile == null)
            {
                Log.LogError("Destination file not specified");
                return false;
            }

            return true;
        }

        private void processOutputProjectFiles()
        {
             if (outputProjectFiles == null)
            {
                return;
            }

            // Add content files with full path name
            string[] projectFileArray = outputProjectFiles.Split(';');
            if (projectFileArray != null || projectFileArray.Length > 0)
            {
                List<string> sourceFileArray = new List<string>();
                foreach (string projectFileName in projectFileArray)
                {
                    FileInfo projectFileInfo = new FileInfo(@projectFileName);
                    string directory = projectFileInfo.DirectoryName;
                    projectFileInfo.Attributes = FileAttributes.Normal;

                    Project project = new Project();
                    project.Load(projectFileName);
                    string outputItem = project.GetEvaluatedProperty("AssemblyName");
                    string outputFilePath = string.Empty;
                    string outputType = project.GetEvaluatedProperty("OutputType").ToLower();
                    
                    if (outputType == "winexe")
                        outputFilePath = assemblyDirectory + "\\" + outputItem + ".exe";
                    else
                    {
                        string silverlightApp = project.GetEvaluatedProperty("SilverlightApplication");
                        if (!string.IsNullOrEmpty(silverlightApp) && (string.Compare(silverlightApp, "true", true) == 0))
                            outputFilePath = assemblyDirectory + "\\" + outputItem + ".xap";
                        else
                            outputFilePath = assemblyDirectory + "\\" + outputItem + ".dll";
                    }

                    cabSourcesFileWriter.WriteLine(outputFilePath);
                }
            }
        }

        private void processContentProjectFiles()
        {
            if (contentProjectFiles == null)
            {
                return;
            }

            // Add content files with full path name
            string[] projectFileArray = contentProjectFiles.Split(';');
            foreach (string projectFileName in projectFileArray)
            {
                FileInfo projectFileInfo = new FileInfo(@projectFileName);
                string directory = projectFileInfo.DirectoryName;
                projectFileInfo.Attributes = FileAttributes.Normal;
                Project project = new Project();
                project.Load(projectFileName);
                BuildItemGroup contentItems = project.GetEvaluatedItemsByName("Content");

                foreach (BuildItem item in contentItems)
                {
                    string contentFilePath = directory + "\\" + item.Include;
                    FileInfo contentFileInfo = new FileInfo(contentFilePath);

                    if ((contentFileInfo.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    {
                        contentFileInfo.Attributes = FileAttributes.Normal;
                        filesAttributeChanged.Add(contentFileInfo);
                    }

                    cabSourcesFileWriter.WriteLine(contentFilePath);
                }

               
            }
        }

        private bool isCabFileBuilt()
        {
            return File.Exists(destinationFile);
        }

        private bool callCabArcProcess(string argumentString)
        {
            StreamReader processOutput = null;
            try
            {
                Process process = new Process();
                string cabCommand = cabCommandPath + cabFileName;
                process.StartInfo.FileName = cabCommand;
                Log.LogMessage(process.StartInfo.FileName);
                process.StartInfo.Arguments = argumentString;
                Log.LogMessage(process.StartInfo.Arguments);

                process.StartInfo.CreateNoWindow = true;

                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.Start();
                processOutput = process.StandardOutput;
                // process.WaitForExit();
                while (!process.HasExited)
                {
                    string outputString = processOutput.ReadToEnd();
                    System.Threading.Thread.Sleep(1000);
                }
                Log.LogMessage(processOutput.ReadToEnd());
            }
            catch (Exception e)
            {
                Log.LogError("Build Cab --- An error has occurred!!!");
                Log.LogError(e.Message);
                if (processOutput != null)
                {
                    Log.LogMessage(processOutput.ReadToEnd());
                }
                return false;
            }

            bool success = true;
            if (!File.Exists(destinationFile))
            {
                success = false;
                Log.LogError("Cab File Failed to build", null);
            }
            return success;
        }

        public override bool Execute()
        {
            Log.LogMessage("Executing Build Cab");

            if (!areNamesValid())
            {
                return false;
            }

            Engine.GlobalEngine.BinPath = @"C:\Windows\Microsoft.NET\Framework\v2.0.50727";

            // Create file specifying cab source files
            string cabSourcesFullFileName = projectDirectory + "\\" + cabSourcesFileName;
            cabSourcesFileWriter = 
                new StreamWriter(cabSourcesFullFileName, false, Encoding.ASCII);
            processOutputProjectFiles();
            processContentProjectFiles();
            cabSourcesFileWriter.Close();

            // Build arugment for "CabArc.exe" specifying
            // - options 
            // - name of cab file
            // - text file specifying cab source files
            argumentStringBuilder = new StringBuilder("n ");
            argumentStringBuilder.Append(destinationFile);
            argumentStringBuilder.Append(" ");
            argumentStringBuilder.Append("@");
            argumentStringBuilder.Append(cabSourcesFullFileName);
            string argumentString = argumentStringBuilder.ToString();

            bool success = callCabArcProcess(argumentString);

            for (int i = 0; i <= filesAttributeChanged.Count - 1; i++)
            {
                ((FileInfo)filesAttributeChanged[i]).Attributes = FileAttributes.ReadOnly;
            }

            return true;
        }
    }
}
