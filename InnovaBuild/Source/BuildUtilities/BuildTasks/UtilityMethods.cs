using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Security;
using System.Security.Permissions;
using System.Security.AccessControl;
using System.Reflection;

namespace NableBuildTasks
{
	public class Utilities
	{
		public static void RestartIIS()
		{
			Process iisResetProcess = new Process();
			iisResetProcess.StartInfo.FileName = "c:\\windows\\system32\\iisreset.exe";
			iisResetProcess.StartInfo.Arguments = " /restart";
			//iisResetProcess.StartInfo.CreateNoWindow = true;
			iisResetProcess.StartInfo.UseShellExecute = false;
			iisResetProcess.StartInfo.RedirectStandardOutput = true;

			iisResetProcess.Start();

			iisResetProcess.WaitForExit();

			Thread.Sleep(10000);

			if (iisResetProcess.ExitCode != 0)
				Console.WriteLine("unable to restart IIS");
			else
				Console.WriteLine("IIS was reset");
		}
		public static string GetEmbeddedResource(string resourceName)
		{
			resourceName = String.Format("NableBuildTasks.{0}", resourceName);

			// get the contents of the config file
			Assembly runningAssembly = Assembly.GetExecutingAssembly();
			StreamReader streamReader = new StreamReader(runningAssembly.GetManifestResourceStream(resourceName));
			string resourceContent = streamReader.ReadToEnd();
			streamReader.Close();

			return resourceContent;
		}

		/// <summary>
		/// Create file with the given content 
		/// </summary>
		/// <param name="fileName">name of the file to create</param>
		/// <param name="fileContent">Content of the file to write</param>
		public static void CreateTextFile(string fileName, string fileContent)
		{
			Console.WriteLine(String.Format("Creating file {0}", fileName));
			StreamWriter streamWriter = new StreamWriter(fileName);
			streamWriter.Write(fileContent);
			streamWriter.Close();
		}
	}
	public class DirectoryMethods
	{

		/// <summary>
		/// Default Overwrite Value - Change to Preference.
		/// </summary>
		private static bool _DefaultOverwrite = false;

		/// <summary>
		/// Default Folder Iteration Limit - Change to Preference.
		/// </summary>
		private static int _DefaultIterationLimit = 1000000;

		///////////////////////////////////////////////////////////
		/////////////////// String Copy Methods ///////////////////
		///////////////////////////////////////////////////////////

		/// <summary>
		/// xDirectory.Copy() - Copy a Source Directory and it's SubDirectories/Files
		/// </summary>
		/// <param name="sSource">The Source Directory</param>
		/// <param name="sDestination">The Destination Directory</param>
		public static void Copy(string sSource, string sDestination)
		{
			Copy(new DirectoryInfo(sSource), new DirectoryInfo(sDestination), null, null, _DefaultOverwrite, _DefaultIterationLimit);
		}

		/// <summary>
		/// xDirectory.Copy() - Copy a Source Directory and it's SubDirectories/Files
		/// </summary>
		/// <param name="sSource">The Source Directory</param>
		/// <param name="sDestination">The Destination Directory</param>
		/// <param name="Overwrite">Whether or not to Overwrite a Destination File if it Exists.</param>
		public static void Copy(string sSource, string sDestination, bool Overwrite)
		{
			Copy(new DirectoryInfo(sSource), new DirectoryInfo(sDestination), null, null, Overwrite, _DefaultIterationLimit);
		}

		/// <summary>
		/// xDirectory.Copy() - Copy a Source Directory and it's SubDirectories/Files
		/// </summary>
		/// <param name="sSource">The Source Directory</param>
		/// <param name="sDestination">The Destination Directory</param>
		/// <param name="FileFilter">The File Filter (Standard Windows Filter Parameter, Wildcards: "*" and "?")</param>
		public static void Copy(string sSource, string sDestination, string FileFilter)
		{
			Copy(new DirectoryInfo(sSource), new DirectoryInfo(sDestination), FileFilter, null, _DefaultOverwrite, _DefaultIterationLimit);
		}

		/// <summary>
		/// xDirectory.Copy() - Copy a Source Directory and it's SubDirectories/Files
		/// </summary>
		/// <param name="sSource">The Source Directory</param>
		/// <param name="sDestination">The Destination Directory</param>
		/// <param name="FileFilter">The File Filter (Standard Windows Filter Parameter, Wildcards: "*" and "?")</param>
		/// <param name="Overwrite">Whether or not to Overwrite a Destination File if it Exists.</param>
		public static void Copy(string sSource, string sDestination, string FileFilter, bool Overwrite)
		{
			Copy(new DirectoryInfo(sSource), new DirectoryInfo(sDestination), FileFilter, null, Overwrite, _DefaultIterationLimit);
		}

		/// <summary>
		/// xDirectory.Copy() - Copy a Source Directory and it's SubDirectories/Files
		/// </summary>
		/// <param name="sSource">The Source Directory</param>
		/// <param name="sDestination">The Destination Directory</param>
		/// <param name="FileFilter">The File Filter (Standard Windows Filter Parameter, Wildcards: "*" and "?")</param>
		/// <param name="DirectoryFilter">The Directory Filter (Standard Windows Filter Parameter, Wildcards: "*" and "?")</param>
		public static void Copy(string sSource, string sDestination, string FileFilter, string DirectoryFilter)
		{
			Copy(new DirectoryInfo(sSource), new DirectoryInfo(sDestination), FileFilter, DirectoryFilter, _DefaultOverwrite, _DefaultIterationLimit);
		}

		/// <summary>
		/// xDirectory.Copy() - Copy a Source Directory and it's SubDirectories/Files
		/// </summary>
		/// <param name="sSource">The Source Directory</param>
		/// <param name="sDestination">The Destination Directory</param>
		/// <param name="FileFilter">The File Filter (Standard Windows Filter Parameter, Wildcards: "*" and "?")</param>
		/// <param name="DirectoryFilter">The Directory Filter (Standard Windows Filter Parameter, Wildcards: "*" and "?")</param>
		/// <param name="Overwrite">Whether or not to Overwrite a Destination File if it Exists.</param>
		public static void Copy(string sSource, string sDestination, string FileFilter, string DirectoryFilter, bool Overwrite)
		{
			Copy(new DirectoryInfo(sSource), new DirectoryInfo(sDestination), FileFilter, DirectoryFilter, Overwrite, _DefaultIterationLimit);
		}

		/// <summary>
		/// xDirectory.Copy() - Copy a Source Directory and it's SubDirectories/Files
		/// </summary>
		/// <param name="sSource">The Source Directory</param>
		/// <param name="sDestination">The Destination Directory</param>
		/// <param name="FileFilter">The File Filter (Standard Windows Filter Parameter, Wildcards: "*" and "?")</param>
		/// <param name="DirectoryFilter">The Directory Filter (Standard Windows Filter Parameter, Wildcards: "*" and "?")</param>
		/// <param name="Overwrite">Whether or not to Overwrite a Destination File if it Exists.</param>
		/// <param name="FolderLimit">Iteration Limit - Total Number of Folders/SubFolders to Copy</param>
		public static void Copy(string sSource, string sDestination, string FileFilter, string DirectoryFilter, bool Overwrite, int FolderLimit)
		{
			Copy(new DirectoryInfo(sSource), new DirectoryInfo(sDestination), FileFilter, DirectoryFilter, Overwrite, FolderLimit);
		}

		//////////////////////////////////////////////////////////////////
		/////////////////// DirectoryInfo Copy Methods ///////////////////
		//////////////////////////////////////////////////////////////////

		/// <summary>
		/// xDirectory.Copy() - Copy a Source Directory and it's SubDirectories/Files
		/// </summary>
		/// <param name="diSource">The Source Directory</param>
		/// <param name="diDestination">The Destination Directory</param>
		public static void Copy(DirectoryInfo diSource, DirectoryInfo diDestination)
		{
			Copy(diSource, diDestination, null, null, _DefaultOverwrite, _DefaultIterationLimit);
		}

		/// <summary>
		/// xDirectory.Copy() - Copy a Source Directory and it's SubDirectories/Files
		/// </summary>
		/// <param name="diSource">The Source Directory</param>
		/// <param name="diDestination">The Destination Directory</param>
		/// <param name="Overwrite">Whether or not to Overwrite a Destination File if it Exists.</param>
		public static void Copy(DirectoryInfo diSource, DirectoryInfo diDestination, bool Overwrite)
		{
			Copy(diSource, diDestination, null, null, Overwrite, _DefaultIterationLimit);
		}

		/// <summary>
		/// xDirectory.Copy() - Copy a Source Directory and it's SubDirectories/Files
		/// </summary>
		/// <param name="diSource">The Source Directory</param>
		/// <param name="diDestination">The Destination Directory</param>
		/// <param name="FileFilter">The File Filter (Standard Windows Filter Parameter, Wildcards: "*" and "?")</param>
		public static void Copy(DirectoryInfo diSource, DirectoryInfo diDestination, string FileFilter)
		{
			Copy(diSource, diDestination, FileFilter, null, _DefaultOverwrite, _DefaultIterationLimit);
		}

		/// <summary>
		/// xDirectory.Copy() - Copy a Source Directory and it's SubDirectories/Files
		/// </summary>
		/// <param name="diSource">The Source Directory</param>
		/// <param name="diDestination">The Destination Directory</param>
		/// <param name="FileFilter">The File Filter (Standard Windows Filter Parameter, Wildcards: "*" and "?")</param>
		/// <param name="Overwrite">Whether or not to Overwrite a Destination File if it Exists.</param>
		public static void Copy(DirectoryInfo diSource, DirectoryInfo diDestination, string FileFilter, bool Overwrite)
		{
			Copy(diSource, diDestination, FileFilter, null, Overwrite, _DefaultIterationLimit);
		}

		/// <summary>
		/// xDirectory.Copy() - Copy a Source Directory and it's SubDirectories/Files
		/// </summary>
		/// <param name="diSource">The Source Directory</param>
		/// <param name="diDestination">The Destination Directory</param>
		/// <param name="FileFilter">The File Filter (Standard Windows Filter Parameter, Wildcards: "*" and "?")</param>
		/// <param name="DirectoryFilter">The Directory Filter (Standard Windows Filter Parameter, Wildcards: "*" and "?")</param>
		public static void Copy(DirectoryInfo diSource, DirectoryInfo diDestination, string FileFilter, string DirectoryFilter)
		{
			Copy(diSource, diDestination, FileFilter, DirectoryFilter, _DefaultOverwrite, _DefaultIterationLimit);
		}

		/// <summary>
		/// xDirectory.Copy() - Copy a Source Directory and it's SubDirectories/Files
		/// </summary>
		/// <param name="diSource">The Source Directory</param>
		/// <param name="diDestination">The Destination Directory</param>
		/// <param name="FileFilter">The File Filter (Standard Windows Filter Parameter, Wildcards: "*" and "?")</param>
		/// <param name="DirectoryFilter">The Directory Filter (Standard Windows Filter Parameter, Wildcards: "*" and "?")</param>
		/// <param name="Overwrite">Whether or not to Overwrite a Destination File if it Exists.</param>
		public static void Copy(DirectoryInfo diSource, DirectoryInfo diDestination, string FileFilter, string DirectoryFilter, bool Overwrite)
		{
			Copy(diSource, diDestination, FileFilter, DirectoryFilter, Overwrite, _DefaultIterationLimit);
		}


		/////////////////////////////////////////////////////////////////////
		/////////////////// The xDirectory.Copy() Method! ///////////////////
		/////////////////////////////////////////////////////////////////////


		/// <summary>
		/// xDirectory.Copy() - Copy a Source Directory and it's SubDirectories/Files
		/// </summary>
		/// <param name="diSource">The Source Directory</param>
		/// <param name="diDestination">The Destination Directory</param>
		/// <param name="FileFilter">The File Filter (Standard Windows Filter Parameter, Wildcards: "*" and "?")</param>
		/// <param name="DirectoryFilter">The Directory Filter (Standard Windows Filter Parameter, Wildcards: "*" and "?")</param>
		/// <param name="Overwrite">Whether or not to Overwrite a Destination File if it Exists.</param>
		/// <param name="FolderLimit">Iteration Limit - Total Number of Folders/SubFolders to Copy</param>
		public static void Copy(DirectoryInfo diSource, DirectoryInfo diDestination, string FileFilter, string DirectoryFilter, bool Overwrite, int FolderLimit)
		{
			int iterator = 0;
			List<DirectoryInfo> diSourceList = new List<DirectoryInfo>();
			List<FileInfo> fiSourceList = new List<FileInfo>();

			try
			{
				///// Error Checking /////
				if (diSource == null)
					throw new ArgumentException("Source Directory: NULL");
				if (diDestination == null)
					throw new ArgumentException("Destination Directory: NULL");
				if (!diSource.Exists)
					throw new IOException("Source Directory: Does Not Exist");
				if (!(FolderLimit > 0))
					throw new ArgumentException("Folder Limit: Less Than 1");
				if (DirectoryFilter == null || DirectoryFilter == string.Empty)
					DirectoryFilter = "*";
				if (FileFilter == null || FileFilter == string.Empty)
					FileFilter = "*";

				///// Add Source Directory to List /////
				diSourceList.Add(diSource);

				///// First Section: Get Folder/File Listing /////
				while (iterator < diSourceList.Count && iterator < FolderLimit)
				{
					foreach (DirectoryInfo di in diSourceList[iterator].GetDirectories(DirectoryFilter))
						diSourceList.Add(di);

					foreach (FileInfo fi in diSourceList[iterator].GetFiles(FileFilter))
						fiSourceList.Add(fi);

					iterator++;
				}

				///// Second Section: Create Folders from Listing /////
				foreach (DirectoryInfo di in diSourceList)
				{
					if (di.Exists)
					{
						string sFolderPath = diDestination.FullName + @"\" + di.FullName.Remove(0, diSource.FullName.Length);

						///// Prevent Silly IOException /////
						if (!Directory.Exists(sFolderPath))
							Directory.CreateDirectory(sFolderPath);
					}
				}

				///// Third Section: Copy Files from Listing /////
				foreach (FileInfo fi in fiSourceList)
				{
					if (fi.Exists)
					{
						string sFilePath = diDestination.FullName + @"\" + fi.FullName.Remove(0, diSource.FullName.Length);

						///// Better Overwrite Test W/O IOException from CopyTo() /////
						if (Overwrite)
							fi.CopyTo(sFilePath, true);
						else
						{
							///// Prevent Silly IOException /////
							if (!File.Exists(sFilePath))
								fi.CopyTo(sFilePath, true);
						}
					}
				}
			}
			catch
			{ throw; }
		}
		/// <summary>
		/// Create directory with full access
		/// </summary>
		/// <param name="name">name of the directory</param>
		public static void CreateDirectoryWithFullAccessControl(string name)
		{
			if (name == null || name == "")
				throw new System.ArgumentNullException("Can not create directory without name.");
			else if (Directory.Exists(name))
				throw new System.ArgumentException("Can not create directory [" + name + "], because it exists.");
			else
			{
				DirectoryInfo di = Directory.CreateDirectory(name);
				System.Security.AccessControl.DirectorySecurity dirSecurity = new System.Security.AccessControl.DirectorySecurity(name, System.Security.AccessControl.AccessControlSections.All);
				di.SetAccessControl(dirSecurity);
			}
		}
		/// <summary>
		/// Delete directory even if no permissions - set full access to the directory and delete it.
		/// </summary>
		/// <param name="name">name of the directory</param>
		/// <param name="deleteRecursively">wthether to delete subdirectory or not.</param>
		public static void DeleteDirectory(string name, bool deleteRecursively)
		{
			if (name == null || name == "")
				throw new System.ArgumentNullException("DeleteDirectory: can not delete directory without name.");
			else if (Directory.Exists(name))
			{
				DirectoryInfo di = new DirectoryInfo(name);
				System.Security.AccessControl.DirectorySecurity dirSecurity = di.GetAccessControl();
				string identity = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
				dirSecurity.AddAccessRule(new FileSystemAccessRule(identity, FileSystemRights.FullControl | FileSystemRights.DeleteSubdirectoriesAndFiles, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.InheritOnly, AccessControlType.Allow));
				di.SetAccessControl(dirSecurity);

				UpdateFileAttributes(name, FileAttributes.Normal);
				di.Delete(deleteRecursively);
			}
		}
		/// <summary>
		/// Recursively sett attributes on all files and subdirectories for
		/// the given directory
		/// </summary>
		/// <param name="dirName">Full path to the directory</param>
		/// <param name="attrib">attributes to set</param>
		public static void UpdateFileAttributes(string dirName, FileAttributes attrib)
		{
			if (dirName == null || dirName == "")
				throw new System.ArgumentNullException("UpdateFileAttributes: Name of the directory required.");
			else if (Directory.Exists(dirName))
			{
				DirectoryInfo di = new DirectoryInfo(dirName);

				FileInfo[] fileList = di.GetFiles();
				foreach (FileInfo fileInfo in fileList)
					fileInfo.Attributes = attrib;

				foreach (DirectoryInfo childDi in di.GetDirectories())
				{
					childDi.Attributes = attrib;
					UpdateFileAttributes(childDi.FullName, attrib);
				}
			}
		}
	}
}
