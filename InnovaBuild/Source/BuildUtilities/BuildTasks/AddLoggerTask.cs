using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Build.BuildEngine;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace NableBuildTasks
{
    public class AddLoggerTask : Task
    {

        public override bool Execute()
        {
            Engine.GlobalEngine.BinPath = @"C:\Windows\Microsoft.NET\Framework\v2.0.50727";

            // Instantiate a new FileLogger to generate build log
            FileLogger logger = new FileLogger();

            // Set the logfile parameter to indicate the log destination
            logger.Parameters = @"logfile=build.log";

            // Register the logger with the engine
            Engine.GlobalEngine.UnregisterAllLoggers();
            // Engine.GlobalEngine.RegisterLogger(logger);

            return true;
        }
    }
}
