using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.CodeCoverage;

namespace NableBuildTasks
{
    class ModuleTotal : CoverageDSPriv.ModuleDataTable
    {
        private uint blocksCovered;
        private uint blocksNotCovered;
        private uint imageLinkTime;
        private uint imageSize;
        private uint linesCovered;
        private uint linesNotCovered;
        private uint linesPartiallyCovered;
        private string moduleName;

        public uint BlocksNotCovered
        {
            get
            {
                return blocksNotCovered;
            }
            set
            {
                blocksNotCovered = value;
            }
        }
        public uint ImageLinkTime
        {
            get
            {
                return imageLinkTime;
            }
            set
            {
                imageLinkTime = value;
            }
        }
        public uint ImageSize
        {
            get
            {
                return imageSize;
            }
            set
            {
                imageSize = value;
            }
        }
        public uint LinesCovered
        {
            get
            {
                return linesCovered;
            }
            set
            {
                linesCovered = value;
            }
        }
        public uint LinesNotCovered
        {
            get
            {
                return linesNotCovered;
            }
            set
            {
                linesNotCovered = value;
            }
        }

        public uint LinesPartiallyCovered
        {
            get
            {
                return linesPartiallyCovered;
            }
            set
            {
                linesPartiallyCovered = value;
            }
        }
        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
                moduleName = value;
            }
        }
        public uint BlocksCovered
        {
            get
            {
                return blocksCovered;
            }
            set 
            {
                blocksCovered = value;
            }
        }

        public ModuleTotal()
        {
            this.moduleName = "Total Summary";
            this.TableName = "ModuleTotal";
        }

        public void CalculateTotal(CoverageDSPriv.ModuleDataTable module)
        {
            foreach(CoverageDSPriv.ModuleRow moduleRow in module)
            {
                this.BlocksCovered += moduleRow.BlocksCovered;
                this.BlocksNotCovered += moduleRow.BlocksNotCovered;
                this.ImageLinkTime += moduleRow.ImageLinkTime;
                this.ImageSize += moduleRow.ImageSize;
                this.LinesCovered += moduleRow.LinesCovered;
                this.LinesNotCovered += moduleRow.LinesNotCovered;
                this.LinesPartiallyCovered += moduleRow.LinesPartiallyCovered;
            }
            this.AddModuleRow(this.moduleName, this.imageSize, this.imageLinkTime, this.linesCovered, this.linesPartiallyCovered, this.linesNotCovered, this.blocksCovered, this.blocksNotCovered);
        }
    }
}
