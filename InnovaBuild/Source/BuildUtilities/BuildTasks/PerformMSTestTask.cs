using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Xsl;
using System.Security.AccessControl;
using Microsoft.Build.BuildEngine;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using Microsoft.VisualStudio.CodeCoverage;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Xml.XPath;
using SHDocVw;
using System.Configuration;
using System.Collections.Generic;
using FMLibrary;
namespace NableBuildTasks
{
    public class PerformNunitTestTask : Task
    {
        static string NABLE_DIRECTORY = System.Environment.GetEnvironmentVariable("ECLIPSEHOME");

        static string NUNIT_CONSOLE_EXECUTABLE_FILE = "nunit-console.exe";
        static string NUNIT_ERROR_OUTPUT_OPTION = "/err:NunitErrors.txt";
        static string NUNIT_OUTPUT_OPTION = "/out:NunitOuput.txt";
        string NABLE_KEY_PATH = Path.Combine(System.Environment.GetEnvironmentVariable("ECLIPSEHOME"), @"Keys\TX360Enterprise.snk");
        static string NABLE_CODE_COVERAGE_XSL = Path.Combine(System.Environment.GetEnvironmentVariable("NABLEBUILDHOME"), @"MsBuildCommon\CodeCoverage.xsl");
        static string NABLE_CODE_COVERAGE_XML = Path.Combine(System.Environment.GetEnvironmentVariable("NABLEBUILDHOME"), @"MsBuildCommon\CodeCoverage.xml");
        static string NABLE_CODE_COVERAGE_XML_SUMMARY = Path.Combine(System.Environment.GetEnvironmentVariable("NABLEBUILDHOME"), @"MsBuildCommon\CodeCoverageSummary.xml");
        static string NABLE_CODE_COVERAGE_XML_DETAIL = Path.Combine(System.Environment.GetEnvironmentVariable("NABLEBUILDHOME"), @"MsBuildCommon\CodeCoverageDetail.xml");
        static string NABLE_CODE_COVERAGE_LOCAL_LOCATION = Path.Combine(System.Environment.GetEnvironmentVariable("NABLEBUILDHOME"), @"MsBuildCommon\CodeCoverageMsTest.coverage");
        static string NABLE_CODE_COVERAGE_HTML = Path.Combine(System.Environment.GetEnvironmentVariable("NABLEBUILDHOME"), @"MsBuildCommon\CodeCoverage.html");
        static string NABLE_TEST_ASSEMBLY_DIR = Path.Combine(System.Environment.GetEnvironmentVariable("ECLIPSEHOME"), "AssemblyTest");
        static string NABLE_COVERAGEFILE = Path.Combine(System.Environment.GetEnvironmentVariable("ECLIPSEHOME"), @"AssemblyTest\CodeCoverage.coverage");
        static string NunitDLLPath = Path.Combine(NABLE_DIRECTORY, @"Tools\External\NUnit");

        string nunitCommandPath = null;
        string nunitTestFile = null;
        string nunitOutputXmlFile = null;

        int exitCode = -1;

        [Required]
        public string NunitCommandPath
        {
            set { nunitCommandPath = value; }
        }

        [Required]
        public string NunitTestFile
        {
            set { nunitTestFile = value; }
        }

        [Required]
        public string NunitOutputXmlFile
        {
            set { nunitOutputXmlFile = value; }
        }

        [Output]
        public string ExitCodeString
        {
            get
            {
                Console.WriteLine("Exit code int is " + exitCode);
                Console.WriteLine("Exit code string is " + exitCode.ToString());
                return exitCode.ToString();
            }
        }

        private bool areNamesValid()
        {
            if (nunitCommandPath == null)
            {
                Log.LogError("NUnit command path not specified");
                return false;
            }

            if (nunitTestFile == null)
            {
                Log.LogError("NUnit test file not specified");
                return false;
            }

            if (nunitOutputXmlFile == null)
            {
                Log.LogError("NUnit XML output file not specified");
                return false;
            }

            return true;
        }

        private void ExecuteMSTestConsole()
        {

            // Build arugment for "nunit-console.exe" specifying
            // - options 
            // - name of nunit file file
            // - text file specifying cab source files
            StringBuilder argumentStringBuilder = new StringBuilder();
            argumentStringBuilder.Append("/xml:" + nunitOutputXmlFile + " ");
            argumentStringBuilder.Append(nunitTestFile + " ");
            argumentStringBuilder.Append(NUNIT_ERROR_OUTPUT_OPTION + " ");
            argumentStringBuilder.Append(NUNIT_OUTPUT_OPTION);
            string argumentString = argumentStringBuilder.ToString();

            StreamReader processOutput = null;
            try
            {
                Process process = new Process();
                string nunitConsoleCommand = nunitCommandPath + NUNIT_CONSOLE_EXECUTABLE_FILE;
                process.StartInfo.FileName = nunitConsoleCommand;
                Log.LogMessage(process.StartInfo.FileName);
                process.StartInfo.Arguments = argumentString;
                Log.LogMessage(process.StartInfo.Arguments);

                process.StartInfo.CreateNoWindow = true;

                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.Start();
                processOutput = process.StandardOutput;
                // process.WaitForExit();
                while (!process.HasExited)
                {
                    string outputString = processOutput.ReadToEnd();
                    System.Threading.Thread.Sleep(1000);
                }
                Log.LogMessage(processOutput.ReadToEnd());

                exitCode = process.ExitCode;
                process.Close();
            }
            catch (Exception e)
            {
                Log.LogError("NUnit --- An error has occurred!!!");
                Log.LogError(e.Message);
                if (processOutput != null)
                {
                    Log.LogMessage(processOutput.ReadToEnd());
                }
            }
        }

        public override bool Execute()
        {
            Console.WriteLine("*****************************************************************************");
            Console.WriteLine("*****************************************************************************");
            Console.WriteLine("*****************************************************************************");
            Console.WriteLine("Setting up temp direcotry for ms test assemblies......");
            SetUpTestDirectory();
            Process instrumentProcess = new Process();
            Console.WriteLine("*****************************************************************************");
            Console.WriteLine("*****************************************************************************");
            Console.WriteLine("*****************************************************************************");
            Console.WriteLine("Intrumenting dlls and files in AssemblyTest......");

            foreach (DictionaryEntry assemblyEntry in this.CoverageDLLS)
            {
                StringBuilder sb = new StringBuilder("/COVERAGE ");
                instrumentProcess.StartInfo.FileName = @"C:\Program Files\Microsoft Visual Studio 8\Team Tools\Performance Tools\vsinstr.exe";
                sb.Append(assemblyEntry.Value.ToString());
                instrumentProcess.StartInfo.Arguments = sb.ToString();
                instrumentProcess.StartInfo.CreateNoWindow = true;
                instrumentProcess.StartInfo.UseShellExecute = false;
                instrumentProcess.Start();
                instrumentProcess.WaitForExit();

                instrumentProcess.StartInfo.FileName = @"C:\Program Files\Microsoft Visual Studio 8\SDK\v2.0\Bin\sn.exe ";
                sb = new StringBuilder();
                sb.Append(" -R " + assemblyEntry.Value.ToString() + " " + NABLE_KEY_PATH);
                instrumentProcess.StartInfo.Arguments = sb.ToString();
                instrumentProcess.StartInfo.CreateNoWindow = true;
                instrumentProcess.StartInfo.UseShellExecute = false;
                instrumentProcess.Start();
                instrumentProcess.WaitForExit();
            }
            Guid myrunguid = Guid.NewGuid();

            Console.WriteLine("*****************************************************************************");
            Console.WriteLine("*****************************************************************************");
            Console.WriteLine("*****************************************************************************");
            Console.WriteLine("Running Monitor and executing Nunit test cases......");

            Monitor m = new Monitor();
            m.StartRunCoverage(myrunguid, NABLE_CODE_COVERAGE_LOCAL_LOCATION);
            ExecuteNunitConsole();

            m.FinishRunCoverage(myrunguid);

            DirectoryMethods.Copy(Path.Combine(System.Environment.GetEnvironmentVariable("NABLEBUILDHOME"), "MsBuildCommon"), NABLE_TEST_ASSEMBLY_DIR);
            CoverageInfoManager.SymPath = NABLE_TEST_ASSEMBLY_DIR;
            CoverageInfo ci = CoverageInfoManager.CreateInfoFromFile(NABLE_COVERAGEFILE);
            CoverageDS data = ci.BuildDataSet(null);
            foreach (DictionaryEntry coveregeDLL in this.CoverageDLLS)
            {
                CoverageDSPriv.ModuleRow moduleRow = data.Module.FindByModuleName(coveregeDLL.Key.ToString());
                if (moduleRow == null)
                {
                    string docPath = coveregeDLL.Value.ToString();
                    int charCount = 0;
                    uint lineNotCovered = (uint)FMLibrary.FileUtilities.CodeLineCount(docPath, false, false, true, ref charCount);
                    data.Module.AddModuleRow(coveregeDLL.Key.ToString(), 0, 0, 0, 0, lineNotCovered, 0, 0);
                }
            }

            Console.WriteLine("......");
            Console.WriteLine("Building summary table......");

            ModuleTotal total = new ModuleTotal();
            total.CalculateTotal(data.Module);
            data.Tables.Add(total);

            data.WriteXml("CodeCoverage.xml");
            data.WriteXmlSchema("CodeCoverage.xsd");

            Console.WriteLine("......");
            Console.WriteLine("Generating HTML report......");

            TransformXML();

            DirectoryMethods.Copy(Path.Combine(System.Environment.GetEnvironmentVariable("NABLEBUILDHOME"), "MsBuildCommon"), NABLE_TEST_ASSEMBLY_DIR);

            Console.WriteLine("......");
            Console.WriteLine("The End......");
            if (exitCode == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void SetUpTestDirectory()
        {
            string assemblyPath = Path.Combine(NABLE_DIRECTORY, "Assembly");
            string NABLE_TEST_ASSEMBLY_DIR = Path.Combine(NABLE_DIRECTORY, "AssemblyMsTest");

            DirectoryInfo assemblyDir = new DirectoryInfo(assemblyPath);
            DirectoryInfo assemblyDirCopy = new DirectoryInfo(NABLE_TEST_ASSEMBLY_DIR);

            if (Directory.Exists(NABLE_TEST_ASSEMBLY_DIR))
            {
                DirectoryInfo resourceDir = new DirectoryInfo(NABLE_TEST_ASSEMBLY_DIR);
                System.Security.AccessControl.DirectorySecurity dirSecurity = new System.Security.AccessControl.DirectorySecurity(resourceDir.FullName, System.Security.AccessControl.AccessControlSections.All);
                resourceDir.SetAccessControl(dirSecurity);
                FileInfo[] fileList = resourceDir.GetFiles();
                foreach (FileInfo fileInfo in fileList)
                {
                    fileInfo.Attributes = FileAttributes.Normal;
                }
                Directory.Delete(NABLE_TEST_ASSEMBLY_DIR, true);
            }

            if (!Directory.Exists(NABLE_TEST_ASSEMBLY_DIR))
                Directory.CreateDirectory(NABLE_TEST_ASSEMBLY_DIR);
            Console.WriteLine("*****************************************************************************");
            Console.WriteLine("*****************************************************************************");
            Console.WriteLine("*****************************************************************************");
            Console.WriteLine("Coying dlls and files from Assembly ---> AssemblyMsTest......");
            DirectoryMethods.Copy(assemblyDir, assemblyDirCopy);
        }
        public static void TransformXML()
        {
            // Create a resolver with default credentials.

            XmlUrlResolver resolver = new XmlUrlResolver();

            resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;

            // transform the personnel.xml file to html

            XslTransform transform = new XslTransform();

            // load up the stylesheet

            transform.Load(NABLE_CODE_COVERAGE_XSL, resolver);

            // perform the transformation

            transform.Transform(NABLE_CODE_COVERAGE_XML, NABLE_CODE_COVERAGE_HTML, resolver);

        }

        public Hashtable CoverageDLLS
        {
            get
            {
                Hashtable ht = new Hashtable();
                ht.Add("BrokerManagedComponent_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "BrokerManagedComponent_1_1.dll"));
                ht.Add("TaxTopicBase_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "TaxTopicBase_1_1.dll"));
                ht.Add("AccountingBase_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AccountingBase_1_1.dll"));
                ht.Add("AccountingExceptions_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AccountingExceptions_1_1.dll"));
                ht.Add("ATOForms_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "ATOForms_1_1.dll"));
                ht.Add("ATOForms_2_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "ATOForms_2_1.dll"));
                ht.Add("ATOForms_3_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "ATOForms_3_1.dll"));
                ht.Add("ATOForms_4_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "ATOForms_4_1.dll"));
                ht.Add("ATOFormsConstants_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "ATOFormsConstants_1_1.dll"));
                ht.Add("ATOFormsConstants_2_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "ATOFormsConstants_2_1.dll"));
                ht.Add("ATOFormsConstants_3_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "ATOFormsConstants_3_1.dll"));
                ht.Add("ATOFormsConstants_4_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "ATOFormsConstants_4_1.dll"));
                ht.Add("AustralianBorrowingCosts_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianBorrowingCosts_1_1.dll"));
                ht.Add("AustralianBusinessRelatedCosts_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianBusinessRelatedCosts_1_1.dll"));
                ht.Add("AustralianBusinessUtilities_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianBusinessUtilities_1_1.dll"));
                ht.Add("AustralianEntertainment_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianEntertainment_1_1.dll"));
                ht.Add("AustralianEntity_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianEntity_1_1.dll"));
                ht.Add("AustralianEntityCompany_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianEntityCompany_1_1.dll"));
                ht.Add("AustralianEntityDivision_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianEntityDivision_1_1.dll"));
                ht.Add("AustralianEntityIJV_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianEntityIJV_1_1.dll"));
                ht.Add("AustralianEntityIJVTest.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianEntityIJVTest.dll"));
                ht.Add("AustralianEntityPartnership_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianEntityPartnership_1_1.dll"));
                ht.Add("AustralianEntityTrust_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianEntityTrust_1_1.dll"));
                ht.Add("AustralianEntityUJVSOO_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianEntityUJVSOO_1_1.dll"));
                ht.Add("AustralianEntityUJVSOP_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianEntityUJVSOP_1_1.dll"));
                ht.Add("AustralianExemptIncome_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianExemptIncome_1_1.dll"));
                ht.Add("AustralianGroup_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianGroup_1_1.dll"));
                ht.Add("AustralianGroupDivisionalGroup_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianGroupDivisionalGroup_1_1.dll"));
                ht.Add("AustralianGroupSubGroup_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianGroupSubGroup_1_1.dll"));
                ht.Add("AustralianGroupUltimateTaxGroup_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianGroupUltimateTaxGroup_1_1.dll"));
                ht.Add("AustralianIntangibles_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianIntangibles_1_1.dll"));
                ht.Add("AustralianITL_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianITL_1_1.dll"));
                ht.Add("AustralianLegals_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianLegals_1_1.dll"));
                ht.Add("AustralianRepairsandMaintenances_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "AustralianRepairsandMaintenances_1_1.dll"));
                ht.Add("BrokerWeb_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "BrokerWeb_1_1.dll"));
                ht.Add("BS10TradingStock_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "BS10TradingStock_1_1.dll"));
                ht.Add("BS20Consumables_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "BS20Consumables_1_1.dll"));
                ht.Add("BS40AccruedIncome_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "BS40AccruedIncome_1_1.dll"));
                ht.Add("BS50AccruedExpenses_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "BS50AccruedExpenses_1_1.dll"));
                ht.Add("BS60Provisions_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "BS60Provisions_1_1.dll"));
                ht.Add("BS70UnearnedRevenue_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "BS70UnearnedRevenue_1_1.dll"));
                ht.Add("BS110Investments_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "BS110Investments_1_1.dll"));
                ht.Add("BusinessRelatedCostsTest.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "BusinessRelatedCostsTest.dll"));
                ht.Add("BusinessStructureBase_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "BusinessStructureBase_1_1.dll"));
                ht.Add("BusinessStructureUtilities_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "BusinessStructureUtilities_1_1.dll"));
                ht.Add("CacheSizeCacheTrigger_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "CacheSizeCacheTrigger_1_1.dll"));
                ht.Add("Caching_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "Caching_1_1.dll"));
                ht.Add("CalculationModule_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "CalculationModule_1_1.dll"));
                ht.Add("ChartOfAccounts_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "ChartOfAccounts_1_1.dll"));
                ht.Add("CLMapTemplate_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "CLMapTemplate_1_1.dll"));
                ht.Add("CMBroker_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "CMBroker_1_1.dll"));
                ht.Add("CommonPersisters_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "CommonPersisters_1_1.dll"));
                ht.Add("ConsolAdjustmentEntity_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "ConsolAdjustmentEntity_1_1.dll"));
                ht.Add("ConsolidatedTB_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "ConsolidatedTB_1_1.dll"));
                ht.Add("ConsolidationUtilities_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "ConsolidationUtilities_1_1.dll"));
                ht.Add("DBSecurityGroup_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "DBSecurityGroup_1_1.dll"));
                ht.Add("DBUser_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "DBUser_1_1.dll"));
                ht.Add("Donations_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "Donations_1_1.dll"));
                ht.Add("Entity_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "Entity_1_1.dll"));
                ht.Add("ErrorHandler_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "ErrorHandler_1_1.dll"));
                ht.Add("Exceptions_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "Exceptions_1_1.dll"));
                ht.Add("EXP30ForeignExchangeGainsAndLosses_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "EXP30ForeignExchangeGainsAndLosses_1_1.dll"));
                ht.Add("EXP40BadDebts_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "EXP40BadDebts_1_1.dll"));
                ht.Add("EXP70Subscriptions_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "EXP70Subscriptions_1_1.dll"));
                ht.Add("EXP80ConsultancyFees_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "EXP80ConsultancyFees_1_1.dll"));
                ht.Add("EXP110SuperannuationContributions_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "EXP110SuperannuationContributions_1_1.dll"));
                ht.Add("EXP120VariousAdjPermanent_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "EXP120VariousAdjPermanent_1_1.dll"));
                ht.Add("EXP130VariousAdjTiming_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "EXP130VariousAdjTiming_1_1.dll"));
                ht.Add("EXP130VariousAdjTimingUnitTest.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "EXP130VariousAdjTimingUnitTest.dll"));
                ht.Add("FA10UCARD_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "FA10UCARD_1_1.dll"));
                ht.Add("FA100CGL_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "FA100CGL_1_1.dll"));
                ht.Add("FileImport_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "FileImport_1_1.dll"));
                ht.Add("FixedAssets_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "FixedAssets_1_1.dll"));
                ht.Add("FixedAssetsLeasedOut_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "FixedAssetsLeasedOut_1_1.dll"));
                ht.Add("FOR10NFSI_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "FOR10NFSI_1_1.dll"));
                ht.Add("Group_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "Group_1_1.dll"));
                ht.Add("HelpUtilities_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "HelpUtilities_1_1.dll"));
                ht.Add("HitCountCacheToken_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "HitCountCacheToken_1_1.dll"));
                ht.Add("IBrokerManagedComponent_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "IBrokerManagedComponent_1_1.dll"));
                ht.Add("IBrokerManagedComponentWorpaper_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "IBrokerManagedComponentWorpaper_1_1.dll"));
                ht.Add("IBusinessStructureBase_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "IBusinessStructureBase_1_1.dll"));
                ht.Add("IConsolidationUtilities_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "IConsolidationUtilities_1_1.dll"));
                ht.Add("INC10DR_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "INC10DR_1_1.dll"));
                ht.Add("InstallationSecurity.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "InstallationSecurity.dll"));
                ht.Add("InstallInfoAttribute.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "InstallInfoAttribute.dll"));
                ht.Add("InstallUtilities_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "InstallUtilities_1_1.dll"));
                ht.Add("InterestExpense_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "InterestExpense_1_1.dll"));
                ht.Add("IOrganizationUnit_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "IOrganizationUnit_1_1.dll"));
                ht.Add("ISecurity_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "ISecurity_1_1.dll"));
                ht.Add("Ledger_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "Ledger_1_1.dll"));
                ht.Add("LedgerMapTemplate_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "LedgerMapTemplate_1_1.dll"));
                ht.Add("Licence_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "Licence_1_1.dll"));
                ht.Add("LogicalModule_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "LogicalModule_1_1.dll"));
                ht.Add("MapTarget_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "MapTarget_1_1.dll"));
                ht.Add("MemorySizeCacheTrigger_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "MemorySizeCacheTrigger_1_1.dll"));
                ht.Add("Organization_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "Organization_1_1.dll"));
                ht.Add("OrganizationUnit_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "OrganizationUnit_1_1.dll"));
                ht.Add("Period_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "Period_1_1.dll"));
                ht.Add("Persistence_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "Persistence_1_1.dll"));
                ht.Add("Prepayments_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "Prepayments_1_1.dll"));
                ht.Add("PresentationControls_1_1.dll  ", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "PresentationControls_1_1.dll"));
                ht.Add("PresentationTestFixtureBase_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "PresentationTestFixtureBase_1_1.dll"));
                ht.Add("PrivTemplate_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "PrivTemplate_1_1.dll"));
                ht.Add("RandD10_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "RandD10_1_1.dll"));
                ht.Add("SAPBase_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "SAPBase_1_1.dll"));
                ht.Add("SAS70Password_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "SAS70Password_1_1.dll"));
                ht.Add("SecurityUtilities_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "SecurityUtilities_1_1.dll"));
                ht.Add("SystemModule_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "SystemModule_1_1.dll"));
                ht.Add("TAX10OffsetsCreditsInstalments_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "TAX10OffsetsCreditsInstalments_1_1.dll"));
                ht.Add("TaxEffectAccounting_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "TaxEffectAccounting_1_1.dll"));
                ht.Add("TemperatureCacheToken_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "TemperatureCacheToken_1_1.dll"));
                ht.Add("TrialBalance_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "TrialBalance_1_1.dll"));
                ht.Add("Utilities_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "Utilities_1_1.dll"));
                ht.Add("ValidationModule_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "ValidationModule_1_1.dll"));
                ht.Add("VariousAdjTemporary_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "VariousAdjTemporary_1_1.dll"));
                ht.Add("WorkpaperBase_1_1.dll", Path.Combine(NABLE_TEST_ASSEMBLY_DIR, "WorkpaperBase_1_1.dll"));

                return ht;
            }
        }
    }
}







