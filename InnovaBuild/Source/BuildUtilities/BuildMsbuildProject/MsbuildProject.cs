using System;
using System.Collections.Generic;
using System.Text;
using BuildUtilities;
using Microsoft.Build.BuildEngine;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.IO;
using System.Xml;
using System.Resources;
using System.Collections;
using System.Diagnostics;


namespace BuildMsbuildProject
{
    public class MsbuildProject
    {
        static readonly string LOGGER_PARAMETERS = "logfile=" +
            BuildUtilities.Configuration.BuildLogFilename;
        static readonly string BIN_PATH = @"C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319";
        static readonly string ECLIPSE_BUILD_DIRECTORY = System.Environment.GetEnvironmentVariable("ECLIPSEBUILDHOME");
        static readonly string ECLIPSE_DIRECTORY = System.Environment.GetEnvironmentVariable("ECLIPSEHOME");
        static readonly string BUILD_ECLIPSE_DEVELOPMENT = "BuildEclipseDevelopment.proj";
        static readonly string BUILD_TEST_ECLIPSE_DEVELOPMENT = "BuildTestEclipseDevelopment.proj";
        static readonly string BUILD_ECLIPSE_INTEGRATION = "BuildEclipseIntegration.proj";

        static readonly string CLEAN_ECLIPSE = "CleanEclipse.proj";
        static readonly string CONFIGURATION_KEY = "Configuration";
        static readonly string DEBUG_CONFIGURATION = "Debug";
        static readonly string RELEASE_CONFIGURATION = "Release";

        private Engine engine = new Engine();
        internal BuildVersion Version;
        private Project project = new Project();
        private BuildPropertyGroup propertyGroup;
        private string buildProjectFilename = BUILD_ECLIPSE_DEVELOPMENT;
        private string configurationType = DEBUG_CONFIGURATION;

        private enum ArgumentStateType
        {
            None,
            Project
        }

        private ArgumentStateType argumentStateType = ArgumentStateType.None;

        public enum BuildType
        {
            DevelopmentBuild,
            DevelopmentBuildTest,
            IntegrationBuild,
            Clean
        }

        public BuildType buildType;

        MsbuildProject(Engine engine)
        {
            this.engine = engine;
            buildType = BuildType.DevelopmentBuild;
            project = engine.CreateNewProject();
        }

        /// <summary>
        /// 
        /// Use the argument switch to set debug/release mode
        /// <list type="">
        /// <item>
        /// /d - Debug
        /// </item>
        /// <item>
        /// /D - Debug
        /// </item>
        /// <item>
        /// /l - Release
        /// </item>
        /// <item>
        /// /L - Release
        /// </item>
        /// <item>
        /// Default is Debug.
        /// </item>
        /// </list>
        /// 
        /// </summary>
        /// 
        /// <param name="args">Argument strings supplied to executable</param>
        private void processArgument(string argument)
        {
            switch (argument)
            {
                case "/d":
                case "/D":
                    configurationType = DEBUG_CONFIGURATION;
                    argumentStateType = ArgumentStateType.None;
                    break;

                case "/l":
                case "/L":
                    configurationType = RELEASE_CONFIGURATION;
                    argumentStateType = ArgumentStateType.None;
                    break;

                case "/c":
                case "/C":
                    buildType = BuildType.Clean;
                    argumentStateType = ArgumentStateType.None;
                    break;

                case "/i":
                case "/I":
                    buildType = BuildType.IntegrationBuild;
                    argumentStateType = ArgumentStateType.None;
                    break;

                case "/u":
                case "/U":
                    if (buildType == BuildType.DevelopmentBuild)
                    {
                        buildType = BuildType.DevelopmentBuildTest;
                    }
                    argumentStateType = ArgumentStateType.None;
                    break;

                case "/p":
                case "/P":
                    argumentStateType = ArgumentStateType.Project;
                    break;

                default:
                    if (argumentStateType == ArgumentStateType.Project)
                        argumentStateType = ArgumentStateType.None;
                    break;
            }
        }

        private void processArguments(string[] args)
        {
            foreach (string argument in args)
            {
                processArgument(argument);
            }

            switch (buildType)
            {
                case BuildType.Clean:
                    buildProjectFilename = CLEAN_ECLIPSE;
                    break;

                case BuildType.DevelopmentBuild:
                    buildProjectFilename = BUILD_ECLIPSE_DEVELOPMENT;
                    break;

                case BuildType.DevelopmentBuildTest:
                    buildProjectFilename = BUILD_TEST_ECLIPSE_DEVELOPMENT;
                    break;

                case BuildType.IntegrationBuild:
                    buildProjectFilename = BUILD_ECLIPSE_INTEGRATION;
                    break;

                default:
                    break;
            }
        }

        private bool build()
        {
            project = engine.CreateNewProject();
            string projectFilePath = Path.Combine(ECLIPSE_BUILD_DIRECTORY, buildProjectFilename);
            project.Load(projectFilePath);
            propertyGroup = project.AddNewPropertyGroup(true);
            propertyGroup.AddNewProperty(CONFIGURATION_KEY, configurationType);
            if (this.Version != null)
                propertyGroup.AddNewProperty("BuildNumber", this.Version.ToString());
            return engine.BuildProject(project);
        }

        /// <summary>
        /// Excutable that:
        /// <list type="">
        /// <item>
        /// Builds an MSBUILD project specified by the argument supplied to it
        /// </item>
        /// <item>
        /// Logs the output of the build in an HTML file
        /// </item>
        /// <item>
        /// Sets the screen color based on the success of the build
        /// <list type="">
        /// <item>
        /// Green - Sucessful
        /// </item>
        /// <item>
        /// Red   - Unsucessful
        /// </item>
        /// </list>
        /// </item>
        /// </list>
        /// </summary>
        /// <param name="args">
        /// Argument strings supplied to executable include:
        ///   MSBUILD project file for project to be build
        ///   Configuration option for release or debug
        /// </param>
        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("**********************************************************************");
            // Instantiate a new Engine object
            Engine engine = new Engine();

            // Point to the path that contains the .NET Framework 2.0 CLR and tools
            engine.BinPath = @BIN_PATH;

            // Instantiate a new FileLogger to generate build log
            FileLogger logger = new FileLogger();
            logger.Parameters = @LOGGER_PARAMETERS;
            engine.RegisterLogger(logger);

            // Add console logger
            ConsoleLogger consoleLogger = new ConsoleLogger();
            consoleLogger.Verbosity = LoggerVerbosity.Minimal;
            engine.RegisterLogger(consoleLogger);

            BuildWarningsProcessor buildWarningsProcessor = new BuildWarningsProcessor();
            engine.RegisterLogger(buildWarningsProcessor);

            // Instantiate Msbuild Project and build it
            MsbuildProject msbuildNableProject = new MsbuildProject(engine);
            msbuildNableProject.processArguments(args);

          
            if (msbuildNableProject.buildType == BuildType.IntegrationBuild &&  msbuildNableProject.configurationType == RELEASE_CONFIGURATION)
            {
                Console.WriteLine("Deleting temp files");
                deleteReportFiles();

                //msbuildNableProject.Version = IncrementBuildNumber(currentBuildVersion);
                //Console.WriteLine("Old Version: " + currentBuildVersion.ToString() + ", New Version: " + msbuildNableProject.Version.ToString());
            }

            bool success = msbuildNableProject.build();

            success &= !buildWarningsProcessor.WarningTriggered;

            if (msbuildNableProject.buildType == BuildType.IntegrationBuild  && msbuildNableProject.configurationType == RELEASE_CONFIGURATION)
            {
                createCD();

                System.Text.StringBuilder deploymentPath = new System.Text.StringBuilder(Configuration.DeploymentPath);
                deploymentPath.Replace("{Version}", msbuildNableProject.Version.ToString());
                Deploy(Configuration.OutputFiles, deploymentPath.ToString());

            }

            // Unregister all loggers to close the log file
            engine.UnregisterAllLoggers();

            BuildUtilities.ControlScreenDisplay.setScreenColor(success);

            string logHtmlFileName = BuildUtilities.LogFilesProcessing.transformBuildLogToHtml(success);
            BuildUtilities.LogFilesProcessing.launchBrowserWithBuildLog(logHtmlFileName);
        }

        public static bool Deploy(string folder, string destinationPath)
        {
            bool success = true;

            Console.WriteLine(string.Format(": Deploying build output to folder '{0}'.", destinationPath));
            if (!Directory.Exists(destinationPath))
            {
                Console.WriteLine(string.Format(": \tCreating folder '{0}'.", destinationPath));
                Directory.CreateDirectory(destinationPath);
            }

            try
            {
                if (Directory.Exists(folder))
                {
                    copyDirectory(folder, destinationPath);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format(": Copying output file(s) {0} failed. {1}", folder, ex.Message));
                success = false;
            }

            return success;
        }

        private static void copyDirectory(string sourceDirectory, string targetDirectory)
        {
            if (!Directory.Exists(targetDirectory))
            {
                Directory.CreateDirectory(targetDirectory);
            }

            string[] sourceFiles = Directory.GetFileSystemEntries(sourceDirectory);
            for (int index = 0; index < sourceFiles.Length; index++)
            {
                string sourceFile = sourceFiles[index];
                if (Directory.Exists(sourceFile))
                {
                    // Copy sub-directories.
                    copyDirectory(sourceFile, Path.Combine(targetDirectory, Path.GetFileName(sourceFile)));
                }
                else
                {
                    // Copy files.
                    try
                    {
                        File.Copy(sourceFile, Path.Combine(targetDirectory, Path.GetFileName(sourceFile)), true);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(string.Format("Copying file(s) '{0}' failed. {1}", sourceFile, ex.Message));
                    }
                }
            }
        }

        /// <summary>
        ///		Get the current version.
        /// </summary>
        /// <param name ="productSourceSafeProject">
        ///		The SourceSafe project that contains the Version.txt file.
        ///	</param>
        /// <returns>
        ///		Current version from $/<project>/Version.txt
        ///	</returns>
        static private BuildVersion GetCurrentVersion(string productSourceSafeProject)
        {
            BuildVersion buildVersion = new BuildVersion("");
            return buildVersion;
        }

        static private BuildVersion IncrementBuildNumber(BuildVersion version)
        {
            BuildVersion newBuildVersion = new BuildVersion(version.Major, version.Minor,
                version.MainProjectName, version.RevisionNumber + 1);
            return newBuildVersion;
        }

        /// <summary>
        ///		Check in the new buildnumber.
        /// </summary>
        /// <param name="codename">
        ///		Project codename.
        ///	</param>
        /// <param name="buildNumber">
        ///		New build number to check in.
        ///	</param>
        static private void UpdateVersionFile(string projectFolder, BuildVersion newVersion)
        {
            
        }

        private static string getResourceValue(string resourceFileName, string settingName)
        {
            ResXResourceReader reader = new ResXResourceReader(resourceFileName);

            try
            {
                string resourceValue = String.Empty;

                foreach (DictionaryEntry entry in reader)
                {
                    if (entry.Key.ToString() == settingName)
                        return entry.Value.ToString();
                }

                return String.Empty;
            }
            finally
            {
                reader.Close();
            }
        }

        private static void setResourceValue(string resourceFileName, string settingName, string newValue)
        {
            FileInfo resourceFile = new FileInfo(resourceFileName);
            System.Security.AccessControl.FileSecurity fileSecurity = new System.Security.AccessControl.FileSecurity(resourceFileName, System.Security.AccessControl.AccessControlSections.All);
            resourceFile.SetAccessControl(fileSecurity);

            ResXResourceWriter writer = new ResXResourceWriter(resourceFile.FullName);
            writer.AddResource(settingName, newValue);
            writer.Close();
        }

        public static Hashtable ReportFileList
        {

            get
            {
                Hashtable list = new Hashtable();
                list.Add("CodeCoverage.coverage", new FileInfo("CodeCoverage.coverage"));
                list.Add("CodeCoverage.html", new FileInfo("CodeCoverage.html"));
                list.Add("CodeCoverage.xml", new FileInfo("CodeCoverage.xml"));
                list.Add("CodeCoverage.xsd", new FileInfo("CodeCoverage.xsd"));
                list.Add("console-test.html", new FileInfo("console-test.html"));
                return list;
            }

        }

        private static void createCD()
        {
            //Step 1. Delete existing directory
            if (Directory.Exists(@"c:\Setup\bin"))
                Directory.Delete(@"c:\Setup\bin", true);

            //Create Directory 
            Directory.CreateDirectory(@"c:\Setup\bin");

            DirectoryInfo cabSourceDirectory = new DirectoryInfo(Configuration.ECLIPSEHOMEDirectory + @"\cab");
            FileInfo[] cabFiles = cabSourceDirectory.GetFiles("*_1.cab");
            ArrayList go = new ArrayList();

            DirectoryInfo assemblySourceDirectory = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + @"\Assembly");
            ArrayList assemblyFiles = new ArrayList();
            string[] assemblyFilesList = Configuration.BinFiles.Split(';');
            foreach (string assemblyName in assemblyFilesList)
            {
                assemblyFiles.Add(new FileInfo(assemblyName));
            }


            assemblyFiles.ToArray(typeof(System.IO.FileInfo));

            FileInfo[] dllFiles = new FileInfo[] { new FileInfo(cabSourceDirectory.FullName) };

            DirectoryInfo[] destinationDirectory = new DirectoryInfo[] { new DirectoryInfo(@"c:\Setup\bin") };

            Process process = new Process();
            process.StartInfo.FileName = "CreateCD";
            process.StartInfo.WorkingDirectory = Configuration.ECLIPSEHOMEDirectory;
            process.StartInfo.CreateNoWindow = true;
            process.Start();

            while (!process.HasExited)
            {
                System.Threading.Thread.Sleep(1000);
            }
        }

        private static void deleteReportFiles()
        {
            foreach (FileInfo file in ReportFileList.Values)
                File.Delete(Path.Combine(Configuration.EclipseBuildCommonDirectory, file.Name));
        }
    }
}
