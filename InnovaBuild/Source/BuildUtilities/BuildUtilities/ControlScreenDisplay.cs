using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace BuildUtilities
{
    public class ControlScreenDisplay
    {

        static readonly string[] SCREENS = {
            Configuration.BuildFailedScreenColors,
            Configuration.BuildSucceededScreenColors
        };

        public static void setScreenColor(int success)
        {
            try
            {
                Process process = new Process();
                process.StartInfo.FileName = Configuration.DosCommandFileName;
                process.StartInfo.Arguments = Configuration.DosColorCommand + SCREENS[success];

                // process.StartInfo.CreateNoWindow = true;

                process.StartInfo.UseShellExecute = false;
                process.Start();
                while (!process.HasExited)
                {
                    System.Threading.Thread.Sleep(1000);
                }

                process.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void setScreenColor(bool success)
        {
            try
            {
                Process process = new Process();
                process.StartInfo.FileName = Configuration.DosCommandFileName;

                if (success)
                {
                    process.StartInfo.Arguments = Configuration.DosColorCommand +
                        Configuration.BuildSucceededScreenColors;
                }
                else
                {
                    process.StartInfo.Arguments = Configuration.DosColorCommand +
                        Configuration.BuildFailedScreenColors;
                }

                // process.StartInfo.CreateNoWindow = true;

                process.StartInfo.UseShellExecute = false;
                process.Start();
                while (!process.HasExited)
                {
                    System.Threading.Thread.Sleep(1000);
                }

                process.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
