using System;

namespace BuildUtilities
{
	/// <summary>
	/// Summary description for BuildVersion.
	/// </summary>
	public class BuildVersion 
	{
		//Private members
		string mainProjectName = String.Empty; 
		int revisionNumber = 0;
		int minorNumber = 0;
		int majorNumber = 0;

		public override string ToString()
		{
			return this.mainProjectName+"."+this.majorNumber.ToString()+"."+this.minorNumber.ToString()+"."+this.revisionNumber.ToString();
		}

		/// <summary>
		/// Project name: this is where it is different from Version class from .Net Framework. We can use project name as string 
		/// value rather than having year
		/// </summary>
		public string MainProjectName
		{
			get
			{
				return this.mainProjectName;  
			}
//			set
//			{
//				this.mainProjectName = value; 
//			}
		}
		/// <summary>
		/// Build revision number
		/// </summary>
		public int RevisionNumber 
		{
			get 
			{
				return this.revisionNumber; 
			}
//			set
//			{
//				this.revisionNumber = value; 
//			}
		}
		/// <summary>
		/// Build minor version number. 
		/// </summary>
		public int Minor
		{
			get
			{
				return this.minorNumber; 
			}
			
//			set
//			{
//				this.minorNumber = value; 
//			}
		}
		/// <summary>
		/// Build major version number. 
		/// </summary>
		public int Major
		{
			get
			{
				return this.majorNumber; 
			}
//			set
//			{
//				this.majorNumber = value;
//			}
		}
		/// <summary>
		/// Default constructor, accepts version string format xx.xx.xx.xx. 
		/// </summary>
		/// <param name="version"></param>
		public BuildVersion(string version)
		{
			string[] versionInformationArray = version.Split('.'); 

			//Set Project name
			this.mainProjectName = versionInformationArray[0];
			try
			{
				//Set revision number
				this.revisionNumber = Int32.Parse(versionInformationArray[3]);
			}
			catch(Exception ex)
			{
				throw new Exception("Issue with revision numnber....." + ex.Message); 
			}
			try
			{
				//Set minor number
				this.minorNumber = Int32.Parse(versionInformationArray[2]);
			}
			catch(Exception ex)
			{
				throw new Exception("Issue with minor number numnber....." + ex.Message); 
			}
			try
			{
				//Set major number
				this.majorNumber = Int32.Parse(versionInformationArray[1]);
			}
			catch(Exception ex)
			{
				throw new Exception("Issue with major numnber....." + ex.Message); 
			}
		}
		/// <summary>
		/// Default constructor, accepts int parameter for each version information. 
		/// </summary>
		/// <param name="version"></param>
		public BuildVersion(int major, int minor, int projectNameOrNumber, int revision)
		{
			this.mainProjectName = projectNameOrNumber.ToString();
			this.revisionNumber = revision;
			this.minorNumber = minor;
			this.majorNumber = major;
		}
		/// <summary>
		/// Default constructor, accepts int parameter for each version information except projectname, it accepts only string value. 
		/// </summary>
		/// <param name="version"></param>
		public BuildVersion(int major, int minor, string projectNameOrNumber, int revision)
		{
			this.mainProjectName = projectNameOrNumber;
			this.revisionNumber = revision;
			this.minorNumber = minor;
			this.majorNumber = major;
		}
	}
}
