using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using Microsoft.Build.Utilities;
using Microsoft.Build.Tasks;

namespace BuildUtilities
{
	public class LogFilesProcessing
	{
		public static string transformBuildLogToHtml(bool success)
		{
			// Create html file
			StreamWriter buildLogHtmlFileWriter =
				new StreamWriter(Configuration.BuildLogHtmlFilename, false, Encoding.ASCII);

			// Write html preamble
			buildLogHtmlFileWriter.WriteLine("<html>");
			buildLogHtmlFileWriter.WriteLine("<head><title>BUILD LOG</title></head>");

			// Set body parameters
			if (success)
			{
                buildLogHtmlFileWriter.WriteLine("<body style=\"font-family:Calibri\" bgcolor=\"#003311\" text=\"white\" >");
			}
			else
			{
                buildLogHtmlFileWriter.WriteLine("<body style=\"font-family:Calibri\" bgcolor=\"#771100\" text=\"white\" >");
			}

			// Copy log contents to html
			StreamReader buildLog = new StreamReader(Configuration.BuildLogFilename);
			while (!buildLog.EndOfStream)
			{
				string logLine = buildLog.ReadLine();
				buildLogHtmlFileWriter.WriteLine(logLine);
				buildLogHtmlFileWriter.WriteLine("<br />");
			}
			buildLog.Close();

			// Write end of body
			buildLogHtmlFileWriter.WriteLine("</body>");

			// Write end of file
			buildLogHtmlFileWriter.WriteLine("</html>");

			buildLogHtmlFileWriter.Close();

			return Configuration.BuildLogHtmlFilename;
		}

		public static void launchBrowserWithBuildLog(string buildLogHtmlFileName)
		{
			try
			{
				Process process = new Process();
				process.StartInfo.FileName = Configuration.DosCommandFileName;
				process.StartInfo.Arguments = Configuration.DosStartAppCommand +
					Configuration.DosStartPathOption +
					Configuration.IexplorerAppFilePath + buildLogHtmlFileName;
				Console.WriteLine(process.StartInfo.Arguments);

				process.StartInfo.UseShellExecute = true;
				process.Start();
				while (!process.HasExited)
				{
					System.Threading.Thread.Sleep(1000);
				}

				process.Close();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}
	}
}
