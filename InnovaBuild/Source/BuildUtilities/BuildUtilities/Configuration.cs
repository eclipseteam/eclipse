using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace BuildUtilities
{
    public class Configuration
    {
        //
        // Readonly declaration
        //
        static readonly string BIN_FILES = "cabarc.exe"; 
        static readonly string DEPLOYMENT_PATH = "";
        static readonly string ECLIPSE_HOME_ENVIRONMENT_VARIABLE = "ECLIPSEHOME";
        static readonly string ECLIPSE_BUILD_HOME_ENVIRONMENT_VARIABLE = "ECLIPSEBUILDHOME";
        static readonly string ECLIPSE_HOME_DIRECTORY =
        System.Environment.GetEnvironmentVariable(ECLIPSE_HOME_ENVIRONMENT_VARIABLE);
        static readonly string ECLIPSE_BUILD_HOME_DIRECTORY =
            System.Environment.GetEnvironmentVariable(ECLIPSE_BUILD_HOME_ENVIRONMENT_VARIABLE);
        static readonly string DOS_COMMAND_FILENAME = "C:\\WINDOWS\\SYSTEM32\\CMD.EXE";
        static readonly string DOS_COLOR_COMMAND = "/C COLOR ";
        static readonly string BUILD_FAILED_SCREEN_COLORS = "4F";
        static readonly string BUILD_SUCCEEDED_SCREEN_COLORS = "2F";
        static readonly string DOS_START_APP_COMMAND = "/C START ";
        static readonly string DOS_START_PATH_OPTION = "/Dpath ";
        static readonly string IEXPLORER_APP_DIRECTORY = "\"C:\\Program Files\\Internet Explorer\"";
        static readonly string IEXPLORER_APP_FILENAME = "IEXPLORE.EXE ";
        static readonly string IEXPLORER_APP_FILEPATH = IEXPLORER_APP_DIRECTORY + 
            IEXPLORER_APP_FILENAME;
        static readonly string MSBUILD_TOOLS_DIRECTORY = 
            ECLIPSE_BUILD_HOME_DIRECTORY + "\\MsBuildCommon\\";
        static readonly string BUILD_LOG_FILENAME = ECLIPSE_BUILD_HOME_DIRECTORY + "\\build.log";
        static readonly string BUILD_LOG_HTML_FILENAME = ECLIPSE_BUILD_HOME_DIRECTORY + "\\BuildLog.html";
        static readonly string DOS_REMOVE_DIRECTORY_COMMAND = "/C RMDIR /S /Q ";
        static readonly string DOS_MAKE_DIRECTORY_COMMAND = "/C MKDIR ";
        static readonly string VERSION_RESOURCE_PATH = @"Services\Broker\Broker_1_1\Broker\Resources";
        static readonly string HOST_NAME = System.Net.Dns.GetHostByName("LocalHost").HostName;
        static readonly string OUTPUT_FILES = "";

        public static string BinFiles
        {
            get
            {
                return BIN_FILES;
            }
        }

        public static string OutputFiles
        {
            get
            {
                return OUTPUT_FILES;
            }
        }

        public static string DeploymentPath
        {
            get
            {
                return DEPLOYMENT_PATH;
            }
        }

        public static string ECLIPSEHOMEDirectory
        {
            get
            {
                return ECLIPSE_HOME_DIRECTORY;
            }
        }

        public static string ECLIPSEBUILDHOMEDirectory
        {
            get
            {
                return ECLIPSE_BUILD_HOME_DIRECTORY;
            }
        }

        public static string DosCommandFileName
        {
            get
            {
                return DOS_COMMAND_FILENAME;
            }
        }

        public static string DosColorCommand
        {
            get
            {
                return DOS_COLOR_COMMAND;
            }
        }

        public static string BuildFailedScreenColors
        {
            get
            {
                return BUILD_FAILED_SCREEN_COLORS;
            }
        }

        public static string BuildSucceededScreenColors
        {
            get
            {
                return BUILD_SUCCEEDED_SCREEN_COLORS;
            }
        }

        public static string DosStartAppCommand
        {
            get
            {
                return DOS_START_APP_COMMAND;
            }
        }

        public static string DosStartPathOption
        {
            get
            {
                return DOS_START_PATH_OPTION;
            }
        }

        public static string IexplorerAppFilePath
        {
            get
            {
                return IEXPLORER_APP_FILEPATH;
            }
        }

        public static string MsbuildToolsDirectory
        {
            get
            {
                return MSBUILD_TOOLS_DIRECTORY;
            }
        }

        public static string BuildLogFilename
        {
            get
            {
                return BUILD_LOG_FILENAME;
            }
        }

        public static string BuildLogHtmlFilename
        {
            get
            {
                return BUILD_LOG_HTML_FILENAME;
            }
        }

        public static string DosRemoveDirectoryCommand
        {
            get
            {
                return DOS_REMOVE_DIRECTORY_COMMAND;
            }
        }

        public static string DosMakeDirectoryCommand
        {
            get
            {
                return DOS_MAKE_DIRECTORY_COMMAND;
            }
        }

        public static string VersionPath
        {
            get
            {
                return VERSION_RESOURCE_PATH;
            }
        }

        public static string VersionResourceName
        {
            get
            {
                return "Version.resx";
            }
        }

        public static string BuildPath
        {
            get
            {
                return @"C:\Products";
            }
        }

        public static string ActualHostName
        {
            get
            {
                return HOST_NAME;
            }
        }
    
        public static string EclipseBuildCommonDirectory
        {
            get
            {
                return Path.Combine(System.Environment.GetEnvironmentVariable("ECLIPSEBUILDHOME"), "MsBuildCommon");
            }
        }
    }
}
