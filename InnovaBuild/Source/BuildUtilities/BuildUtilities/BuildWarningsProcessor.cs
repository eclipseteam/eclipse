using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BuildUtilities
{
    public class BuildWarningsProcessor : Logger
    {
        #region Fields

        private bool warningTriggered;

        #endregion // Fields

        #region Constructors

        public BuildWarningsProcessor()
        {
            this.warningTriggered = false;
        }

        #endregion // Constructors

        #region Public Properties

        public bool WarningTriggered
        {
            get
            {
                return this.warningTriggered;
            }
        }

        #endregion // Public Properties

        #region Public Methods

        public override void Initialize(IEventSource eventSource)
        {
            eventSource.WarningRaised += new BuildWarningEventHandler(this.ProcessBuildWarningEvent);
        }

        public void ProcessBuildWarningEvent(object sender, BuildWarningEventArgs buildWarningEventArgs)
        {
            this.warningTriggered = true;
        }

        #endregion // Public Methods
    }
}
