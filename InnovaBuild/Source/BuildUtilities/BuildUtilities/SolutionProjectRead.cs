using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using Microsoft.Build.Utilities;


namespace BuildUtilities
{
	public class SolutionProjectRead
	{
		private TaskLoggingHelper loggingHelper = null;
		/// <summary>
		/// Optional propery to log
		/// If this property is set, SolutionProjectRead will log errors and messages
		/// </summary>
		public TaskLoggingHelper LoggingHelper
		{
			get
			{
				return loggingHelper;
			}
			set
			{
				loggingHelper = value;
			}
		}

		/// <summary>
		/// Get list of target assemblies of a certain type from solution file
		/// (Solution file contains a list of included projects files, 
		/// every project file contains its target assembly name) 
		/// </summary>
		/// <param name="path">path to the directory containg solution</param>
		/// <param name="fileName">solution file name</param>
		/// <param name="extention">assembly extention</param>
		/// <returns>target assemblies (path is not included)</returns>
		public ArrayList GetSolutionAssemblies(string path, string fileName, string type)
		{
			ArrayList list = GetSolutionAssemblies(path, fileName);
			ArrayList typeList = new ArrayList();
			foreach (string name in list)
			{
				if (name.EndsWith(type, StringComparison.OrdinalIgnoreCase))
					typeList.Add(name);
			}
			return typeList;
		}
		/// <summary>
		/// Get list of target assemblies from solution file
		/// (Solution file contains a list of included projects files, 
		/// every project file contains its target assembly name) 
		/// </summary>
		/// <param name="path">path to the directory containg solution</param>
		/// <param name="fileName">solution file name</param>
		/// <returns>target assemblies (path is not included)</returns>
		public ArrayList GetSolutionAssemblies(string path, string fileName)
		{
			ArrayList list = new ArrayList();

			string fullName = Path.Combine(path, fileName);
			if (!File.Exists(fullName))
				LogError("GetSolutionAssemblies: Can not find solution file " + fullName);
			else
			{
				string attrib = "";
				XmlDocument solution = new XmlDocument();
				solution.Load(fullName);
				//XmlNode node = solution.SelectSingleNode("Project/Target[@Name='Build']");
				//XPath expr does not work with this xml file, so we have to use GetNodeWithAttribute()

				XmlNode node = GetNodeWithAttribute(solution.DocumentElement, "Target", "Name", "Build");

				if (node != null && node.FirstChild != null)
					attrib = node.FirstChild.Attributes["Targets"].Value;
				if (attrib == String.Empty)
					LogError("GetSolutionAssemblies: Can not find Build Targets in" + fullName);
				else
				{
					string[] names = attrib.Split(';');
					foreach (string name in names)
					{
						node = GetNodeWithAttribute(solution.DocumentElement, "Target", "Name", name);
						if ((node != null) && (node.FirstChild != null) &&
								(node.FirstChild.Attributes["Projects"] != null))
						{
							string projectName = node.FirstChild.Attributes["Projects"].Value;
							string fullProjectPath = Path.Combine(path, projectName);
							string assemblyName = GetAssemblyName(fullProjectPath);
							list.Add(assemblyName);
							LogMessage("GetSolutionAssemblies in " + fullName + ": " + assemblyName);
						}
						else
							LogMessage("GetSolutionAssemblies: Could not find project details for build target " + name);
					}
				}
			}
			return list;
		}
		/// <summary>
		/// Find 1st child XmlElement with the Given name, with its attribute set to the given value
		/// </summary>
		/// <param name="parentNode">start node for the search</param>
		/// <param name="nodeName">XmlElement name to look for</param>
		/// <param name="attrName">XmlAttribute name to check in the found XmlElement</param>
		/// <param name="attrValue">XmlAttribute value to check in the found XmlElement</param>
		/// <returns>child XmlElement, null if not found</returns>
		/// throws ArgumentNullException if parentNode == null
		private XmlNode GetNodeWithAttribute(XmlNode parentNode, string nodeName, string attrName, string attrValue)
		{
			if (parentNode == null)
				throw new System.ArgumentNullException("ParentNode can not be null");
			foreach (XmlNode node in parentNode.ChildNodes)
			{
				if (node.Name == nodeName)
				{
					string name = node.Attributes[attrName].Value;
					if (name == attrValue)
						return node;
				}
			}
			return null;
		}
		/// <summary>
		/// Find 1st child XmlElement with the Given name
		/// </summary>
		/// <param name="parentNode">start node for the search</param>
		/// <param name="nodeName">XmlElement name to look for</param>
		/// <returns>first child XmlElement with the matching name, null if not found</returns>
		/// throws ArgumentNullException if parentNode == null
		private XmlNode GetFirstNode(XmlNode parentNode, string nodeName)
		{
			if (parentNode == null)
				throw new System.ArgumentNullException("GetFirstNode: ParentNode can not be null");
			foreach (XmlNode node in parentNode.ChildNodes)
			{
				if (node.Name == nodeName)
					return node;
			}
			return null;
		}
		/// <summary>
		/// Get assembly name from a project file
		/// </summary>
		/// <param name="fullName"> full path to the project file</param>
		/// <returns>target assembly name for this project</returns>
		private string GetAssemblyName(string fullName)
		{
			string name = "";
			if (!File.Exists(fullName))
				LogError("GetAssemblyName: Can not find file " + fullName);
			else
			{
				XmlDocument doc = new XmlDocument();
				doc.Load(fullName);
				XmlNode node = GetFirstNode(doc.DocumentElement.FirstChild, "AssemblyName");
				if (node != null)
				{
					name = node.InnerXml;
					node = GetFirstNode(doc.DocumentElement.FirstChild, "OutputType");
					if (node != null)
					{
						string type = node.InnerXml;
						name = name + GetAssemblyFileExtention(type);
					}
					else
						LogError("GetAssemblyName: Can not find OutputType in [" + fullName + "]");
				}
				else
					LogError("GetAssemblyName: Can not find AssemblyName in [" + fullName + "]");
			}
			return name;
		}

		/// <summary>
		/// Derive assembly extention based on OutputType in the project file
		/// (i.e 'exe', 'dll', etc)
		/// </summary>
		/// <param name="fullName"> full path to the project file</param>
		/// <returns>file extention</returns>
		private string GetAssemblyFileExtention(string type)
		{
			string ext = "";
			switch (type.ToLower())
			{
				case "exe":
				case "application":
					ext = ".exe";
					break;
				case "dll":
				case "library":
					ext = ".dll";
					break;
				default:
					break;
			}
			return ext;
		}


		private void LogMessage(string msg)
		{
			if (loggingHelper != null)
				loggingHelper.LogMessage(msg);
		}
		private void LogError(string msg)
		{
			if (loggingHelper != null)
				loggingHelper.LogError(msg);
		}
	}
}
