<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/TR/xhtml1/strict">

<xsl:output method="text"/>

<xsl:template match="/">
	<xsl:apply-templates select="//Issue" />
	<xsl:apply-templates select="//Exception" />
</xsl:template>

<xsl:template match="Exception"><xsl:variable name="kind" select="@Kind" /><xsl:choose><xsl:when test="@Keyword='CA0001'"><xsl:value-of select="/FxCopReport/Localized/String[@Key='InternalError']/text()" /></xsl:when><xsl:otherwise><xsl:value-of select="/FxCopReport/Localized/String[@Key='Error']/text()" /></xsl:otherwise></xsl:choose> : <xsl:value-of select="@Keyword" /> : <xsl:if test="@CheckId"><xsl:value-of select="/FxCopReport/Localized/String[@Key='Rule']/text()" />=<xsl:value-of select="@Category" />#<xsl:value-of select="@CheckId" />, <xsl:value-of select="/FxCopReport/Localized/String[@Key='Target']/text()" />=<xsl:value-of select="@Target" /> : </xsl:if><xsl:value-of select="ExceptionMessage/text()" /><xsl:text disable-output-escaping="yes">&#xD;&#xA;</xsl:text>
</xsl:template>

<xsl:template match="Issue">
	<xsl:if test="@Path"><xsl:value-of select="@Path"/>\<xsl:value-of select="@File"/>(<xsl:value-of select="@Line"/>) : </xsl:if><xsl:value-of select="@Level" /> : <xsl:apply-templates select=".." mode="parentMessage" /><xsl:value-of select="translate(normalize-space(text()),':','')" /><xsl:text disable-output-escaping="yes">&#xD;&#xA;</xsl:text>
</xsl:template>

<xsl:template match="Message" mode="parentMessage">	
<xsl:value-of select="@CheckId"/> : <xsl:value-of select="@Category"/> : <xsl:value-of select="@TypeName"/> : <xsl:apply-templates select=".." mode="signature"/> : </xsl:template>

<xsl:template match="Text">
	<xsl:value-of select="translate(normalize-space(text()),':','')"/>
</xsl:template>

<xsl:template match="Rules/Rule"/>
<xsl:template match="Note"/>

<xsl:template match="*" mode="signature">
  <xsl:choose>			
    <xsl:when test="self::Module"><xsl:value-of select="@Name" />, </xsl:when>
    <xsl:when test="self::Messages"><xsl:apply-templates select=".." mode="signature" /></xsl:when>
    <xsl:when test="self::Namespace"><xsl:apply-templates select=".." mode="parent" /><xsl:value-of select="@Name" /></xsl:when>
    <xsl:when test="self::Namespaces"><xsl:if test="not(name(..)='FxCopReport')"><xsl:apply-templates select=".." mode="parent"/></xsl:if></xsl:when>
    <xsl:when test="self::Target"><xsl:value-of select="@Name" />, </xsl:when>  
    <xsl:when test="@Name"><xsl:apply-templates select=".." mode="parent" /><xsl:value-of select="translate(@Name,':', '#')"/></xsl:when>  
    <xsl:otherwise><xsl:apply-templates select=".." mode="signature" /></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="*" mode="parent">
  <xsl:choose>			
    <xsl:when test="self::Module"><xsl:value-of select="@Name" />, </xsl:when>
    <xsl:when test="self::Messages"><xsl:apply-templates select=".." mode="parent" /></xsl:when>
    <xsl:when test="self::Namespace"><xsl:apply-templates select=".." mode="parent" /><xsl:value-of select="@Name" />.</xsl:when>
    <xsl:when test="self::Namespaces"><xsl:if test="not(name(..)='FxCopReport')"><xsl:apply-templates select=".." mode="parent"/></xsl:if></xsl:when>
    <xsl:when test="self::Target"><xsl:value-of select="@Name" />, </xsl:when>    
    <xsl:otherwise><xsl:apply-templates select=".." mode="parent" /><xsl:if test="@Name"><xsl:value-of select="translate(@Name,':', '#')"/>.</xsl:if></xsl:otherwise>
  </xsl:choose>
</xsl:template>
			
</xsl:stylesheet>


