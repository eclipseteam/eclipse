REM argsuments case insensitive: "/d" - Debug, "/l" - release , "/c" - clean, "/i" - IntegrationBuild


@ECHO OFF

net stop "TAXSIMP BMC Service"

set myMSBUILD="C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319\MSBuild"
set FXCOPDIR="%ECLIPSEBUILDHOME%\bin"
set myDV="C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\devenv.exe"
set config=Debug
set buildType=/build

set config=""

set buildType=/build

color F

echo.
echo %time%
echo.

Echo --- Build BuildMsbuildProject.exe ---
%myMSBUILD% BuildUtilities\EclipseBuildUtilitiesSolution.proj

echo.
Echo --- Building NextGen ---

bin\BuildMsbuildProject.exe /p NextGenRel05 %1 %2


%myDV% %buildType% %config% "%ECLIPSEHOME%\Services\RIAService\RIAService.sln"

echo.
echo %time%
echo.