Installation Guide for Eclipse v 2.3===========================
This is the installation section of Innova project. The installtion can be done by using following steps

1. Change regional settings
-----------------------------------
Change DateTime-Regional-setting of the system to English(Australia). To change follow

ControlPanel -> Region and Language -> Format = English(Australia)

2. Setting Environment Variables
----------------------------------------
We need to create following Environmental variables. The values of these variables should be change according to source code physical path.

Innova2011 		---> 	..\trunk\InnovaMain\Source\Innova2011
ECLIPSEBUILDHOME 	---> 	..\Innova\trunk\InnovaBuild\Source
ECLIPSEHOME 		---> 	..\Innova\trunk\InnovaMain\Source

The '..' should replace by the physical path of the source code. To create the environmental variables follow

Computer -> Properties -> Advance System Settings -> Environmental Variables -> New (User variables for Administrator)

3. Application Build
------------------------ 
The application is build using MSBuild. 

	a. Before starting, we need to just rename the following folder to any name, otherwise it fails the build process.
		"C:\Program Files (x86)\MSBuild\Microsoft\VisualStudio\v10.0\CodeAnalysis"
		change to (for example)
		"C:\Program Files (x86)\MSBuild\Microsoft\VisualStudio\v10.0\CodeAnalysis_Backup"

	b. To build the application, we need to run %ECLIPSEBUILDHOME%\EclipseBuildAll.bat. 
		The Green build means success whereas Red Build mean fail.

4. Deploy WebSite
-------------------
	a. We need to the following values according to the installation machine in %ECLIPSEHOME%\DevInstall.bat file i.e.
		/DBsERVER:database_server /dbname:Innova2011 /dbadminusername:sa /dbadminpassword:sa /DBSYSUSERNAME:Innova /dbsysuserpassword:kathmandu

	b. Delete the database(dbname) from database_server, otherwise it throws an exception. 
	c. Verify the 'Admin User (dbadminusername)' and 'System User (DBSYSUSERNAME)' account exists in database_server with corresponding passwords.
	d. After changing the values we need to run %ECLIPSEHOME%\DevInstall.bat.

5. Enable & Start net. tcp port sharing service.
------------------------------------------------
    a. Make sure that the Net. TCP Port Sharing service is enabled and started. 
		i. on Run menu, type Services.msc to launch service control manager and enable & start the service.
	
6. Update BMC Service
--------------------------
	
	a. Update the key "NableDBConnectionString" value from %ECLIPSEHOME%\Innova2011\Web.Config to %ECLIPSEHOME%\BMCServiceHost.exe.config. 
		The value can be find in following section
		<appSettings>
		....
		<add key="NableDBConnectionString" value="server=shamil-pc;database=Innova2011;uid=/DKbGq4Xx5/n0OdudPd1Jg==;pwd=7NsvoAdkoO/ukKBi3qiN1yNJloAOhM6c" />
		....
		</appSettings>
	
		Also update the same value  in %ECLIPSEHOME%\Services\BMCContainerService\BMCContainerService_2_1\ServiceHost\App.config for debugging.
	
	b. Start/Stop the BMCService using %ECLIPSEHOME%\StartStopBMCService.bat. 
	
7. Update Ria Service
-------------------------
Open solution %ECLIPSEHOME%\InnovaSolution.sln and compile the following projects in the given sequence.
	a. BMCServiceProxy
	b. RIAServicesLibrary.Web
	c. RIAServicesLibrary

8. Silverlight Integration
-----------------------------
The silverlight projects are not integrated with MSBuild right now. We need to build the following projects.
	
	a. %ECLIPSEHOME%\InnovaSolution.sln
		i.		SilverlightUtilities
		ii.		SilverlightControls
	
	b. %ECLIPSEHOME%\Components\Administration\Administration.sln
		i.		SilverlightTaxSimpMain
		ii.		SilverlightReportingUnit
		iii.		SilverlightOrganization
		iv.		SilverlightDBUser

9. Login
-----------------------
	a.	Open following page and use Administrator as username with password $Admin#1
		http://localhost:81/Innova2011/Login_1_1.aspx

	b. Application will open silverlight version by default. To open asp.net version open following page.
		http://localhost:81/Innova2011/Entities_1_1.aspx
