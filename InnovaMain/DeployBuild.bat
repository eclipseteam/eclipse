REM argsuments case insensitive: "/d" - Debug, "/l" - release , "/c" - clean, "/i" - IntegrationBuild


@ECHO OFF

set myMSBUILD="C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319\MSBuild"
set FXCOPDIR="%NABLEBUILDHOME%\bin"
set myDV="C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe"
set config=Debug
set buildType=/build

set config=""

set buildType=/build

color F

echo.
echo %time%
echo.

Echo --- Building .NET Projects ---
Echo --- Building Utilities Projects ---
%myMSBUILD% "%ECLIPSEHOME%\Framework\Utilities\Utilities_1_1\Utilities\Utilities.csproj"
echo.
echo %time%
echo.

Echo --- Building BMCServiceProxy Projects ---
%myMSBUILD% "%ECLIPSEHOME%\Services\BMCContainerService\BMCContainerService_2_1\BMCServiceProxy\BMCServiceProxy.csproj"
echo.
echo %time%
echo.

Echo --- Building RIAServicesLibrary.Web Projects ---
%myMSBUILD% "%ECLIPSEHOME%\Services\RIAService\RIAServicesLibrary.Web\RIAServicesLibrary.Web.csproj"
echo.
echo %time%
echo.

Echo --- Building RIAServicesLibrary Projects ---
%myMSBUILD% "%ECLIPSEHOME%\Services\RIAService\RIAServicesLibrary\RIAServicesLibrary.csproj"
echo.
echo %time%
echo.

Echo --- Building Organization Projects ---
%myMSBUILD% "%ECLIPSEHOME%\Components\Administration\Organization\Organization_1_1\Organization.csproj"
echo.
echo %time%
echo.

Echo --- Building SharepointLibrary Projects ---
%myMSBUILD% "%ECLIPSEHOME%\Framework\SharepointLibrary\SharepointLibrary.csproj"
echo.
echo %time%
echo.

Echo --- Building Silverlight Projects ---
Echo --- Building SilverlightUtilities Projects ---
%myMSBUILD% "%ECLIPSEHOME%\Framework\SilverlightUtilities\SilverlightUtilities.csproj"
echo.
echo %time%
echo.

Echo --- Building SilverlightControls Projects ---
%myMSBUILD% "%ECLIPSEHOME%\FrameworkGUI\SilverlightControls\SilverlightControls.csproj"
echo.
echo %time%
echo.

Echo --- Building SilverlightDBUser Projects ---
%myMSBUILD% "%ECLIPSEHOME%\Components\Administration\SilverlightDBUser\SilverlightDBUser.csproj"
echo.
echo %time%
echo.

Echo --- Building SilverlightOrganization Projects ---
%myMSBUILD% "%ECLIPSEHOME%\Components\Administration\SilverlightEntity\SilverlightOrganization.csproj"
echo.
echo %time%
echo.

Echo --- Building SilverlightDashboard.Web Projects ---
%myMSBUILD% "%ECLIPSEHOME%\Components\Administration\SilverlightDashboard.Web\SilverlightDashboard.Web.csproj"
echo.
echo %time%
echo.

Echo --- Building SilverlightDashboard Projects ---
%myMSBUILD% "%ECLIPSEHOME%\Components\Administration\SilverlightDashboard\SilverlightDashboard.csproj"
echo.
echo %time%
echo.

