﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using QuickFix.Fields;
using TradeClient.DAL;
using TradeClient.Common;
using System.Transactions;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace TradeClient
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            FIXInitiator fix = new FIXInitiator();

            while (true)
            {
                fix.Process();
                System.Threading.Thread.Sleep(1000);
            }
        }

        [STAThread]
        static void Main1(string[] args)
        {
            try
            {
                string configFileName = DBHelper.GetConfigFileName();
                
                // just for testing
                //configFileName = "desktopBroker_ssl.cfg";
                configFileName = "desktopBroker.cfg";

                QuickFix.SessionSettings settings = new QuickFix.SessionSettings(configFileName);
                
                TradeClientApp application = new TradeClientApp();
                QuickFix.IMessageStoreFactory storeFactory = new QuickFix.FileStoreFactory(settings);
                QuickFix.ILogFactory logFactory = new QuickFix.FileLogFactory(settings);
                QuickFix.Transport.SocketInitiator initiator = new QuickFix.Transport.SocketInitiator(application, storeFactory, settings, logFactory);

                application.MyInitiator = initiator;

                initiator.Start();
                
                while (true)
                {
                    Thread.Sleep(500);

                    List<FIXTradeRequest> notProcessedList = FIXTradeRequest_DAL.GetNotProcessedOrders();

                    foreach (FIXTradeRequest orderRequest in notProcessedList)
                    {
                        NewOrderByShareAmount request = new NewOrderByShareAmount();

                        request.ClientOrderId = orderRequest.ClientOrderId.ToString();
                        request.SecurityName = orderRequest.SecurityName;
                        request.BuySell = orderRequest.BuySell.ToFIXField(typeof(Side));
                        request.OrderType = orderRequest.OrderType.ToFIXField(typeof(OrdType));
                        request.HandleInstruction = orderRequest.HandleInstruction.ToFIXField(typeof(HandlInst));
                        request.Quantity = orderRequest.Quantity;
                        request.ValidDuration = orderRequest.ValidDuration.ToFIXField(typeof(TimeInForce));
                        request.TPPClientID = orderRequest.TPPClientID;
                        request.TPPPersonID = orderRequest.TPPPersonID;
                        request.CommissionType = orderRequest.CommissionType.ToFIXField(typeof(CommType));

                        if (application._session == null)
                            continue;

                        if (!request.Validate())
                            continue;

                        request.Session = application._session;
                        request.InitializeRequestMessage();
                        request.SetFIXFields();

                        using (TransactionScope scope = new TransactionScope())
                        {
                            request.SendRequestMessage();
                            FIXTradeRequest_DAL.SetProcessingStatus(orderRequest.ClientOrderId, ProcessingStatus.SENT);

                            scope.Complete();
                        }
                        
                        Thread.Sleep(300);
                    }

                }
            }
            catch (System.Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }

            Environment.Exit(1);
        }
    }
}
