﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using TradeClient;

namespace EclipseFIXClient
{
    public partial class EclipseFIXService : ServiceBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private Timer serviceTimer;
        private static bool InProcessing = false;
        private static FIXInitiator fix = new FIXInitiator();

        public EclipseFIXService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            log.Info("EclipseFIXService OnStart");
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);

            serviceTimer = new System.Timers.Timer(5000);
            serviceTimer.Elapsed += new ElapsedEventHandler(DoWork);
            serviceTimer.Enabled = true;

            GC.KeepAlive(serviceTimer);
        }

        protected override void OnStop()
        {
            fix.Stop();

            log.Info("EclipseFIXService OnStop");
        }

        private static void DoWork(object source, ElapsedEventArgs e)
        {
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);

            try
            {
                if (!InProcessing)
                {
                    InProcessing = true;
                    fix.Process();
                    InProcessing = false;
                }

            }
            catch (System.Exception ex)
            {
                log.Error("EclipseFIXService DoWork: " + ex.Message);
            }

        }
    }
}
