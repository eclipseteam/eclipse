﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeClient.DAL;

namespace TradeClient
{
    public class DBHelper
    {
        public static string GetConfigFileName()
        {
            string key = Constants.CONFIG_FILENAME.ToString();
            string configFileName = string.Empty;

            using (var db = new Innova2011())
            {
                configFileName = db.FIXConfigs.Where(s => s.Name == key).FirstOrDefault().Value;
            }

            return configFileName;

        }


    }
}
