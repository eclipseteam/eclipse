﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeClient.Common;

namespace TradeClient.DAL
{
    public static class FIXTradeRequest_DAL
    {

        public static void Insert(FIXTradeRequest request)
        {
            using (var db = new Innova2011())
            {
                db.FIXTradeRequests.AddObject(request);
                db.SaveChanges();
            }

        }

        // Inside tuple. 1: ClOrdID, 2: OrderID, 3: ExecType, 4: Text
        public static void UpdateServerResponse(Tuple<int, string, string, string> serverResponse)
        {
            using (var db = new Innova2011())
            {
                FIXTradeRequest request = db.FIXTradeRequests.Where(s => s.ClientOrderId == serverResponse.Item1).First();

                if (request != null)
                {
                    request.ServerOrderID = serverResponse.Item2;
                    request.ExecutionType = serverResponse.Item3;
                    request.ServerResponse = serverResponse.Item4.LimitTo(500);
                    request.ProcessingStatus = ProcessingStatus.RESPONSE_RECEIVED.ToString();
                    request.LastUpdateTime = DateTime.Now;
                }

                db.SaveChanges();
            }

        }

        public static List<FIXTradeRequest> GetNotProcessedOrders()
        {
            List<FIXTradeRequest> notProcessedList;

            string forceCancel = ProcessingStatus.FORCE_CANCEL.ToString();

            using (var db = new Innova2011())
            {
                notProcessedList = db.FIXTradeRequests.Where(s => string.IsNullOrEmpty(s.ProcessingStatus) || s.ProcessingStatus == forceCancel).ToList();
            }

            return notProcessedList;

        }

        public static void SetProcessingStatus(int clientOrderId, ProcessingStatus status)
        {
            using (var db = new Innova2011())
            {
                FIXTradeRequest request = db.FIXTradeRequests.Where(s => s.ClientOrderId == clientOrderId).First();

                if (request != null)
                {
                    request.ProcessingStatus = status.ToString();
                    request.LastUpdateTime = DateTime.Now;
                }

                db.SaveChanges();
            }

        }

    }
}
