﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuickFix.FIX44;

namespace TradeClient
{
    public abstract class Response
    {
        protected Message ServerResponse;

        #region FIX fields
        public string ClientOrderId { get; set; }
        public string ServerOrderId { get; set; }
        public string FreeText { get; set; }
        public string ExectionType { get; set; }

        public string TPPClientID { get; set; }
        public string TPPPersonID { get; set; }
        #endregion

        public abstract void AnalyzeResponse();
        public abstract void PersistResponse();

    }
}
