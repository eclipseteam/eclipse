﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuickFix.FIX44;
using QuickFix.Fields;

namespace TradeClient
{
    public class NewOrderSingleRequest : Request
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public NewOrderSingleRequest()
            : base()
        {
            HashSet<string> tempMandatoryFields = new HashSet<string>
            {
                "CommissionType",
                "ExternalID",
            };
            this.SetOptionalProperties.UnionWith(tempMandatoryFields);

        }

        public override void InitializeRequestMessage()
        {
            QuickFix.FIX44.NewOrderSingle newOrderSingle = new QuickFix.FIX44.NewOrderSingle(
                new ClOrdID(ClientOrderId),
                new Symbol(SecurityName),
                new Side(BuySell.Value),
                new TransactTime(DateTime.Now),
                new OrdType(OrderType.Value)
                );

            log.Info("Info logging");

            this.RequestMessage = newOrderSingle;
        }

    }
}
