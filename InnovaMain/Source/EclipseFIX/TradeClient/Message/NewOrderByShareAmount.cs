﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TradeClient
{
    public class NewOrderByShareAmount : NewOrderSingleRequest
    {
        public NewOrderByShareAmount() : base()
        {

            HashSet<string> tempMandatoryFields = new HashSet<string>
            {
                "ClientOrderId",
                "SecurityName",
                "BuySell",
                "OrderType",
                "HandleInstruction",
                "Quantity",
                "ValidDuration",
                "TPPClientID",
                "TPPPersonID",
            };
            this.SetMandatoryProperties.UnionWith(tempMandatoryFields);

        }

    }
}
