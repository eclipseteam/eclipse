﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TradeClient
{
    public class NewOrderByMoneyAmount : NewOrderSingleRequest
    {
        public NewOrderByMoneyAmount() : base()
        {
            HashSet<string> tempMandatoryFields = new HashSet<string>
            {
                "ClientOrderId",
                "SecurityName",
                "BuySell",
                "OrderType",
                "HandleInstruction",
                "CashOrderQty",
                "ValidDuration",
                "TPPClientID",
                "TPPPersonID",
            };
            this.SetMandatoryProperties.UnionWith(tempMandatoryFields);

        }

    }
}
