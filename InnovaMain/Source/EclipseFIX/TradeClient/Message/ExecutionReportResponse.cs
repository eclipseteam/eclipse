﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuickFix.FIX44;
using QuickFix.Fields;
using TradeClient.DAL;
using TradeClient.Common;

namespace TradeClient
{
    public class ExecutionReportResponse : Response
    {
        public ExecutionReportResponse(Message response)
        {
            this.ServerResponse = response;
        }

        public override void AnalyzeResponse()
        {
            ClOrdID clOrderId = new ClOrdID();
            if (ServerResponse.IsSetField(clOrderId))
                this.ClientOrderId = ServerResponse.GetField(clOrderId).ToString();

            OrderID orderId = new OrderID();
            if (ServerResponse.IsSetField(orderId))
                this.ServerOrderId = ServerResponse.GetField(orderId).ToString();

            ExecType execType = new ExecType();
            if (ServerResponse.IsSetField(execType))
                this.ExectionType = ServerResponse.GetField(execType).ToString().ToDescription(typeof(ExecType));

            Text text = new Text();
            if (ServerResponse.IsSetField(text))
                this.FreeText = ServerResponse.GetField(text).ToString();

            this.PersistResponse();
        }

        public override void PersistResponse()
        {
            if (!string.IsNullOrEmpty(this.ClientOrderId))
            {
                // Inside tuple. 1: ClOrdID, 2: OrderID, 3: ExecType, 4: Text
                Tuple<int, string, string, string> serverResponse = Tuple.Create(Convert.ToInt32(ClientOrderId), this.ServerOrderId, this.ExectionType, this.FreeText);

                FIXTradeRequest_DAL.UpdateServerResponse(serverResponse);
            }
        }


    }
}
