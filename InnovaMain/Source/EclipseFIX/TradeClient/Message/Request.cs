﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuickFix.FIX44;
using QuickFix.Fields;
using System.Reflection;

namespace TradeClient
{
    public abstract class Request
    {

        #region fields other than FIX ones
        public HashSet<string> SetMandatoryProperties = new HashSet<string>();
        public HashSet<string> SetOptionalProperties = new HashSet<string>();
        public QuickFix.Session Session = null;
        public Message RequestMessage;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region FIX fields
        public string ClientOrderId { get; set; }
        public string SecurityName { get; set; }
        public char? BuySell { get; set; }
        public char? OrderType { get; set; }
        public char? HandleInstruction { get; set; }
        public int? Quantity { get; set; }
        public decimal? CashOrderQty { get; set; }
        public char? ValidDuration { get; set; }
        public decimal? PricePerShare { get; set; }
        public decimal? StopPrice { get; set; }
        public string TPPClientID { get; set; }
        public string TPPPersonID { get; set; }
        public char? CommissionType { get; set; }
        public decimal? CommissionField { get; set; } // "Commission" has already been used by FIXField, so use "CommissionField" instead.

        // I might don't need following fileds.
        public int? SeqNum { get; set; }
        public int? Msgtype { get; set; }
        public int? OrdStatus { get; set; }
        public int? PossDupFlag { get; set; }
        public int? Side { get; set; }
        public string Text { get; set; }
        public int? ExecType { get; set; }
        public int? OrderID { get; set; }

        public string OrigClOrdID { get; set; }
        #endregion

        #region FIX Custom Fields
        public string ExternalID { get; set; }
        #endregion

        #region methods
        public abstract void InitializeRequestMessage();

        public Request()
        {
            HashSet<string> setMandatory = new HashSet<string>() 
            {
                "TPPClientID",
                "TPPPersonID",
            };

            SetMandatoryProperties.UnionWith(setMandatory);

        }

        public virtual bool SendRequestMessage()
        {
            if (Session == null)
                return false;
            else
                return Session.Send(RequestMessage);
        }

        public bool Validate()
        {
            List<string> listError = new List<string>();

            //  if mandatory fields are not set, then return.
            foreach (string property in this.SetMandatoryProperties)
            {
                PropertyInfo fi = this.GetType().GetProperty(property);
                object o = fi.GetValue(this, null);

                if (o == null)
                    log.Error(string.Format("There's no value for mandatory field - {0}", property));
            }

            if (listError.Count > 0)
                return false;

            return true;
        }

        public virtual void SetFIXFields()
        {
            HashSet<string> allFields = new HashSet<string>();
            allFields.UnionWith(this.SetMandatoryProperties);
            allFields.UnionWith(this.SetOptionalProperties);

            foreach (string field in allFields)
            {
                if (field == "ExternalID" && !string.IsNullOrEmpty(ExternalID))
                    RequestMessage.SetField(new StringField((int)FIXCustomTags.ExternalID, this.ExternalID));

                if (field == "HandleInstruction" && HandleInstruction.HasValue)
                    RequestMessage.SetField(new HandlInst(this.HandleInstruction.Value));

                if (field == "Quantity" && Quantity.HasValue)
                    RequestMessage.SetField(new OrderQty(Quantity.Value));

                if (field == "CashOrderQty" && CashOrderQty.HasValue)
                    RequestMessage.SetField(new CashOrderQty(CashOrderQty.Value));

                if (field == "ValidDuration" && ValidDuration.HasValue)
                    RequestMessage.SetField(new TimeInForce(this.ValidDuration.Value));

                if (field == "PricePerShare" && PricePerShare.HasValue)
                    RequestMessage.SetField(new Price(this.PricePerShare.Value));

                if (field == "StopPrice" && StopPrice.HasValue)
                    RequestMessage.SetField(new StopPx(this.StopPrice.Value));

                if (field == "TPPClientID" && !string.IsNullOrEmpty(TPPClientID))
                    RequestMessage.SetField(new Account(TPPClientID));    // tag 1

                if (field == "TPPPersonID" && !string.IsNullOrEmpty(TPPPersonID))
                    RequestMessage.SetField(new Text(this.TPPPersonID));  // tag 58

                if (field == "CommissionType" && CommissionType.HasValue)
                    RequestMessage.SetField(new CommType(this.CommissionType.Value));  // tag 13

            }
            
        }

        #endregion

    }
}
