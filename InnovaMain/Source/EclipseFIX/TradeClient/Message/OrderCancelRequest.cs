﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuickFix.FIX44;
using QuickFix.Fields;

namespace TradeClient
{
    public class OrderCancelRequest : Request
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public OrderCancelRequest()
            : base()
        {
            // CancelRequest is iteself a new request with OrigClOrdID linking to the original client order.
            // ProcessingStatus starts from "FORCE_CANCEL" instead of NULL
            HashSet<string> tempMandatoryFields = new HashSet<string>
            {
                "OrigClOrdID",
            };
            this.SetOptionalProperties.UnionWith(tempMandatoryFields);

            HashSet<string> tempOptionalFields = new HashSet<string>
            {
                "ExternalID",
            };
            this.SetOptionalProperties.UnionWith(tempOptionalFields);

        }

        public override void InitializeRequestMessage()
        {
            QuickFix.FIX44.OrderCancelRequest newOrderCancel = new QuickFix.FIX44.OrderCancelRequest(
                new OrigClOrdID(OrigClOrdID),
                new ClOrdID(ClientOrderId),
                new Symbol(SecurityName),
                new Side(BuySell.Value),
                new TransactTime(DateTime.Now)
                );

            log.Info("OrderCancelRequest created.");

            this.RequestMessage = newOrderCancel;
        }

    }
}
