-------------------------------------------------------------------

if exists (select 1 from FIXConfig where Name = 'CONFIG_FILENAME')
	delete FIXConfig where Name = 'CONFIG_FILENAME';

insert FIXConfig (Name, Value) values ('CONFIG_FILENAME', 'desktopBroker.cfg');
go

-------------------------------------------------------------------
-- Sample Data
-------------------------------------------------------------------
delete FIXTradeRequest;

INSERT FIXTradeRequest(
	SecurityName	,
	BuySell			,
	OrderType		,
	Quantity		,
	CashOrderQty	,
	ValidDuration	,
	PricePerShare	,
	StopPrice		,
	TPPClientID		,
	TPPPersonID		,
	CommissionType	,
	Commission	,
	ExternalID	,
	HandleInstruction,
	CreateTime,
	ServerResponse
) values (
'NAB',	--SecurityName	
'BUY',	--BuySell			
'MARKET',		--OrderType		
1000,		--Quantity		
5000,		--CashOrderQty	
'GOOD_TILL_CANCEL',		--ValidDuration	
2.34,		--PricePerShare	
3.56,		--StopPrice		
'116647',		--TPPClientID		
'25398',		--TPPPersonID		
'ABSOLUTE',		--CommissionType	
1.00,		--[Commission]	
100,		--[ExternalID]	
'AUTOMATED_EXECUTION_ORDER_PUBLIC',		--HandleInstruction
GETDATE(),		--CreateTime		
NULL		--ServerResponse	
);

INSERT FIXTradeRequest(
	SecurityName	,
	BuySell			,
	OrderType		,
	Quantity		,
	CashOrderQty	,
	ValidDuration	,
	PricePerShare	,
	StopPrice		,
	TPPClientID		,
	TPPPersonID		,
	CommissionType	,
	Commission	,
	ExternalID	,
	HandleInstruction,
	CreateTime,
	ServerResponse
) values (
'CBA',	--SecurityName	
'BUY',	--BuySell			
'MARKET',		--OrderType		
1000,		--Quantity		
500,		--CashOrderQty	
'GOOD_TILL_CANCEL',		--ValidDuration	
2.34,		--PricePerShare	
3.56,		--StopPrice		
'116647',		--TPPClientID		
'25398',		--TPPPersonID		
'ABSOLUTE',		--CommissionType	
1.00,		--[Commission]	
100,		--[ExternalID]	
'AUTOMATED_EXECUTION_ORDER_PUBLIC',		--HandleInstruction
GETDATE(),		--CreateTime		
NULL		--ServerResponse	
);



select ServerResponse, ProcessingStatus, * from FIXTradeRequest
-------------------------------------------------------------------


	
