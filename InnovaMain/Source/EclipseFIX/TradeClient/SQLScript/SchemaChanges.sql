-------------------------------------------------------------------
if exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'FIXConfig')
	drop table FIXConfig;
go	

CREATE TABLE [dbo].[FIXConfig]
(
	[Name] [nvarchar](50) NOT NULL,
	[Value] [nvarchar](50) NOT NULL,
	 CONSTRAINT [PK_FIXConfig] PRIMARY KEY CLUSTERED 
	(
		[Name] ASC
	)
)

-------------------------------------------------------------------
if exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'FIXTradeRequest')
	drop table FIXTradeRequest;
go	

CREATE TABLE [dbo].[FIXTradeRequest](
	ClientOrderId [int] IDENTITY(1,1) NOT NULL,
	SecurityName [nvarchar](50) NULL,
	BuySell [nvarchar](100) NULL,
	OrderType [nvarchar](50) NULL,
	Quantity int NULL,
	CashOrderQty decimal(12,2) null,
	ValidDuration [nvarchar](50) NULL,
	PricePerShare decimal(12,2) null,
	StopPrice decimal(12,2) null,
	TPPClientID nvarchar(50) null,
	TPPPersonID nvarchar(50) null,
	CommissionType nvarchar(50) null,
	[Commission] [nvarchar](50) NULL,
	[ExternalID] [varchar](200) NULL,
	HandleInstruction varchar(50) NULL,
	CreateTime	datetime null,
	LastUpdateTime	datetime null,
	ProcessingStatus	nvarchar(50) NULL,
	ServerOrderID	nvarchar(50) NULL,
	ExecutionType	nvarchar(50) NULL,
	ServerResponse varchar(500),
	 CONSTRAINT [PK_FIXTradeRequest] PRIMARY KEY CLUSTERED 
	(
		ClientOrderId ASC
	)
)

go
-------------------------------------------------------------------


