﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using QuickFix.Fields;
using TradeClient.DAL;
using TradeClient.Common;
using System.Transactions;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace TradeClient
{
    public class FIXInitiator
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private TradeClientApp application;
        private QuickFix.Transport.SocketInitiator initiator;
        private bool isInitialized = false;

        public void Initialize()
        {
            try
            {
                QuickFix.SessionSettings settings = new QuickFix.SessionSettings(DBHelper.GetConfigFileName());
                application = new TradeClientApp();
                QuickFix.IMessageStoreFactory storeFactory = new QuickFix.FileStoreFactory(settings);
                QuickFix.ILogFactory logFactory = new QuickFix.FileLogFactory(settings);
                initiator = new QuickFix.Transport.SocketInitiator(application, storeFactory, settings, logFactory);

                application.MyInitiator = initiator;

                initiator.Start();
            }
            catch (System.Exception e)
            {
                log.Error(e.Message);
                log.Error(e.StackTrace);
            }
        }

        public void Process()
        {
            // dor debugging purpose, attaching process
            //Thread.Sleep(1000 * 30);

            // I can't do Initialize() during timer starting, as it could take long time.
            if (!this.isInitialized)
            {
                this.Initialize();
                this.isInitialized = true;
            }

            try
            {
                List<FIXTradeRequest> notProcessedList = FIXTradeRequest_DAL.GetNotProcessedOrders();

                foreach (FIXTradeRequest orderRequest in notProcessedList)
                {
                    if (application._session == null)
                    {
                        log.Info(string.Format("Session not ready, wait..."));
                        Thread.Sleep(300);
                        continue;
                    }

                    if (!application.IsLoggedOn)
                    {
                        log.Info(string.Format("FIX client hasn't logged on, wait..."));
                        Thread.Sleep(300);
                        continue;
                    }

                    // Get different requests according to DB data
                    Request request = GetRequest(orderRequest);
                    if (!request.Validate())
                    {
                        log.Info(string.Format("Request valudation failed, security: {0}", request.SecurityName));
                        continue;
                    }

                    request.Session = application._session;
                    request.InitializeRequestMessage();
                    request.SetFIXFields();

                    using (TransactionScope scope = new TransactionScope())
                    {
                        request.SendRequestMessage();
                        log.Info("SendRequestMessage security: " + request.SecurityName);
                        FIXTradeRequest_DAL.SetProcessingStatus(orderRequest.ClientOrderId, ProcessingStatus.SENT);
                        log.Info(string.Format("SendRequestMessage security: {0}, status: sent",request.SecurityName ));

                        scope.Complete();
                    }

                    // wait 300 ms before sending another request.
                    Thread.Sleep(300);
                }


            }
            catch (System.Exception e)
            {
                log.Error(e.Message);
                log.Error(e.StackTrace);
            }
        }

        private static Request GetRequest(FIXTradeRequest orderRequest)
        {
            Request request;

            if (orderRequest.ProcessingStatus == ProcessingStatus.FORCE_CANCEL.ToString())
            {
                request = new OrderCancelRequest();
                request.OrigClOrdID = orderRequest.OrigClOrdID.ToString();
            }
            else if (orderRequest.Quantity == null || orderRequest.Quantity == 0)
            {
                request = new NewOrderByMoneyAmount();
                request.CashOrderQty = orderRequest.CashOrderQty;
            }
            else
            {
                request = new NewOrderByShareAmount();
                request.Quantity = orderRequest.Quantity;
            }

            request.ClientOrderId = orderRequest.ClientOrderId.ToString();
            request.SecurityName = orderRequest.SecurityName;
            request.BuySell = orderRequest.BuySell.ToFIXField(typeof(Side));
            request.OrderType = orderRequest.OrderType.ToFIXField(typeof(OrdType));
            request.HandleInstruction = orderRequest.HandleInstruction.ToFIXField(typeof(HandlInst));
            request.ValidDuration = orderRequest.ValidDuration.ToFIXField(typeof(TimeInForce));
            request.TPPClientID = orderRequest.TPPClientID;
            request.TPPPersonID = orderRequest.TPPPersonID;
            request.CommissionType = orderRequest.CommissionType.ToFIXField(typeof(CommType));
            request.ExternalID = orderRequest.ExternalID;

            return request;
        }

        public void Stop()
        {
            try
            {
                initiator.Stop();
            }
            catch (System.Exception e)
            {
                log.Error(e.Message);
                log.Error(e.StackTrace);
            }
        }
    }
}
