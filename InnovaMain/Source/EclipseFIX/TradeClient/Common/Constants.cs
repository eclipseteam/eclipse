﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Reflection;

namespace TradeClient
{
    public enum Constants
    {
        [Description("Config File Name")]
        CONFIG_FILENAME,
    }

    public enum ProcessingStatus
    {
        SENT,
        RESPONSE_RECEIVED,
        SUCCESS,
        FAILED,
        FORCE_CANCEL,
    }

    public enum FIXCustomTags
    {
        ExternalID = 5100,
    }

}
