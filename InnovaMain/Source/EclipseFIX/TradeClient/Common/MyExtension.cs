﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;

namespace TradeClient.Common
{
    public static class MyExtension
    {
        public static string ToDescription(this System.Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }


        public static string LimitTo(this string data, int length)
        {
            if (string.IsNullOrEmpty(data))
                return data;

            return data.Substring(0, Math.Min(data.Length, length));
        }


    }
}
