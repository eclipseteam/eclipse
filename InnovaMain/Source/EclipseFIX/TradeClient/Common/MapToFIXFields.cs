﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using QuickFix.Fields;

namespace TradeClient.Common
{
    public static class MapToFIXFields
    {

        // change description to FIX field value of char
        public static char? ToFIXField(this string description, Type type)
        {
            if (description == null)
                return null;

            char result = '0';

            List<FieldInfo> listFI = type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy).
                Where(s => s.IsLiteral && !s.IsInitOnly).ToList();

            foreach (FieldInfo fi in listFI)
            {
                if (string.Equals(fi.Name, description, StringComparison.OrdinalIgnoreCase))
                    result = Convert.ToChar(fi.GetRawConstantValue());
            }

            return result;
        }

        // change FIX field value of char to description
        public static string ToDescription(this string filedValue, Type type)
        {
            string result = string.Empty;

            List<FieldInfo> listFI = type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy).
                Where(s => s.IsLiteral && !s.IsInitOnly).ToList();

            foreach (FieldInfo fi in listFI)
            {
                if (string.Equals(fi.GetRawConstantValue().ToString(), filedValue, StringComparison.OrdinalIgnoreCase))
                    result = fi.Name;
            }

            return result;
        }


    }
}
