echo on
copy *.dll %ECLIPSEHOME%\Assembly
copy *.xml %ECLIPSEHOME%\Assembly
IF EXIST %1.pdb copy %1.pdb %ECLIPSEHOME%\Assembly
IF EXIST %1.xap copy %1.xap %ECLIPSEHOME%\Assembly
IF EXIST %1.exe copy %1.exe %ECLIPSEHOME%\Assembly
IF EXIST %1.exe.config copy %1.exe.config %ECLIPSEHOME%\Assembly

xcopy *.dll %ECLIPSEHOME%\eclipseonlineweb\eclipseonlineweb\bin\ /Y /I /R
xcopy *.xml %ECLIPSEHOME%\eclipseonlineweb\eclipseonlineweb\bin\ /Y /I /R
IF EXIST %1.pdb xcopy %1.pdb %ECLIPSEHOME%\eclipseonlineweb\eclipseonlineweb\bin\ /Y /I /R
IF EXIST %1.xap xcopy %1.xap %ECLIPSEHOME%\eclipseonlineweb\eclipseonlineweb\ClientBin\ /Y /I /R
IF EXIST %1.exe xcopy %1.exe %ECLIPSEHOME%\eclipseonlineweb\eclipseonlineweb\bin\ /Y /I /R
IF EXIST %1.exe.config xcopy %1.exe.config %ECLIPSEHOME%\eclipseonlineweb\eclipseonlineweb\bin\ /Y /I /R
