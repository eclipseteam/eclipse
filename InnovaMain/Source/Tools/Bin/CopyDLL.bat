echo on
copy %1.dll %ECLIPSEHOME%\Assembly
IF EXIST %1.pdb copy %1.pdb %ECLIPSEHOME%\Assembly
IF EXIST %1.xap copy %1.xap %ECLIPSEHOME%\Assembly

xcopy %1.dll %ECLIPSEHOME%\eclipseonlineweb\eclipseonlineweb\bin\ /Y /I /R
xcopy %1.dll %ECLIPSEHOME%\eclipseonlineweb\eclipseonlineweb\ClientBin\ /Y /I /R
xcopy *.dll %ECLIPSEHOME%\eclipseonlineweb\eclipseonlineweb\ClientBin\ /Y /I /R
IF EXIST %1.pdb xcopy %1.pdb %ECLIPSEHOME%\eclipseonlineweb\eclipseonlineweb\bin\ /Y /I /R
IF EXIST %1.xap xcopy %1.xap %ECLIPSEHOME%\eclipseonlineweb\eclipseonlineweb\ClientBin\ /Y /I /R
IF EXIST AppManifest.xaml copy AppManifest.xaml %ECLIPSEHOME%\eclipseonlineweb\eclipseonlineweb\ClientBin\%1.AppManifest.xaml 


xcopy %1.dll %ECLIPSEHOME%\eclipseonlineweb\eclipseonlineweb\bin\ /Y /I /R
IF EXIST %1.pdb xcopy %1.pdb %ECLIPSEHOME%\eclipseonlineweb\eclipseonlineweb\bin\ /Y /I /R