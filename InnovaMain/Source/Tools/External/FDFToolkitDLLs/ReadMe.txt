Programmer's Reference for the FDF ActiveX Server Component V2.0
----------------------------------------------------------------

Copyright (C) 1997 by Adobe Systems Incorporated.
All rights reserved.

For more information about FDF, check out APPENDIX H, titled
"Forms Data Format", of the "PDF 1.2 Reference Manual"
<http://www.adobe.com/supportservice/devrelations/PDFS/TN/PDFSPEC.PDF>



Introduction:
-------------

FdfAcX.dll is a Win32 "in-process" ActiveX server component (formerly known as
OLE automation server) that can also be used in conjunction with Internet
Information Server (IIS) and Active Server Pages (ASP).



Installation:
-------------

FdfAcX.dll should be installed in some directory that has "execute" permissions.
In the ASP case running on WinNT, a good location can be
\WINNT\system32\inetsrv\ASP\Cmpnts. Also, if running on an NTFS, the DLL
themselves (see next paragraph) need to have proper execute permissions.

FdfAcX.dll uses FdfTk.dll, which can be placed either in the same directory
as FdfAcX.dll, or in the "System" directory (e.g. c:\WINNT\system32).

FdfAcX.dll needs to be registered. This can be accomplished by typing at the
command prompt:

cd \WINNT\system32\inetsrv\ASP\Cmpnts
                    (assuming this is where you placed FdfAcX.dll)
regsvr32 FdfAcX.dll



Use:
----

FdfAcX.dll exposes one main object: "FdfApp.FdfApp".

From Visual Basic (VB), you might create it like this:

    Dim FdfAcX As FDFACXLib.FdfApp
    Set FdfAcX = CreateObject("FdfApp.FdfApp")

From an ASP document using VBScript, you would use:

    <% Set FdfAcX = Server.CreateObject("FdfApp.FdfApp") %>

The following 5 methods are available on the "FdfApp.FdfApp" object:

    FDFGetVersion
    FDFCreate
    FDFOpenFromFile
    FDFOpenFromBuf
    FDFOpenFromStr

The last 4 methods above return an object of type FDFACXLib.FdfDoc, which has
37 methods:

    FDFClose
    FDFSaveToFile
    FDFSaveToBuf
    FDFSaveToStr
    FDFNextFieldName
    FDFGetValue
    FDFSetValue
    FDFGetStatus
    FDFSetStatus
    FDFGetFile
    FDFSetFile
    FDFGetID
    FDFSetID
    FDFGetEncoding
    FDFSetEncoding
    FDFGetOpt
    FDFGetOptNumElem
    FDFSetOpt
    FDFGetFlags
    FDFSetFlags
    FDFGetAP
    FDFSetAP
    FDFSetAPRef
    FDFSetIF
    FDFSetAS
    FDFSetSubmitFormAction
    FDFSetResetFormAction
    FDFSetImportDataAction
    FDFSetJavaScriptAction
    FDFSetGoToAction
    FDFSetGoToRAction
    FDFSetURIAction
    FDFSetNamedAction
    FDFSetHideAction
    FDFSetStateAction
    FDFRemoveItem
    FDFAddTemplate



Exceptions:
-----------

All methods may raise an exception. Some of the possible ones are standard OLE
exceptions, such as E_OUTOFMEMORY (0x8007000E). Others are FdfAcX-specific. 
For these, the numeric value, as well as the description, are derived from
FDFErc, defined (and briefly described) in the file FdfTk.h. For example, one
possible exception has a numeric value = 3 and description = "FDFErcFileSysErr".
The actual numeric value of the returned exception is assembled as an HRESULT,
uses the FACILITY_ITF, and starts with decimal 512 (hex 0x0200), as recommended
by Microsoft. Therefore, the numeric value of the exception in the example above
is hex 0x80040202. The important part is the rightmost hex 202, which is the
third error (200 is the first), and the third error's description is
"FDFErcFileSysErr" (see FdfTk.h).



Methods:
--------

The examples below use either VB or VBScript (the latter assumes an ASP
environment).



FDFGetVersion: Returns a string with the current version of FdfTk.dll
    (e.g. "4.0"). No parameters.

    Example:    strVersion = FdfAcX.FDFGetVersion

    Exceptions: None



FDFCreate: Creates a new FDF and returns an object of type FDFACXLib.FdfDoc.
    Client should call the method FDFClose on the FdfDoc object when the FdfDoc is
    no longer needed. No parameters.
    
    Example:    Dim objFdf As FDFACXLib.FdfDoc  'not in VBScript, which supports
                                    'every data type, as long as it is a Variant
                Set objFdf = FdfAcX.FDFCreate

    Exceptions: FDFErcInternalError



FDFOpenFromFile: Opens an existing FDF file and returns an object of type
    FDFACXLib.FdfDoc. Client should call the method FDFClose on the FdfDoc
    object when the FdfDoc is no longer needed. Parameter:

    - bstrFileName: The pathname for the FDF file to be opened.

    Example:    Dim objFdf As FDFACXLib.FdfDoc  'not in VBScript, which supports
                                    'every data type, as long as it is a Variant
                Set objFdf = FdfAcX.FDFOpenFromFile("c:\Temp\inFdf.fdf")

    Exceptions: FDFErcFileSysErr, FDFErcBadFDF, FDFErcInternalError



FDFOpenFromBuf: Takes as parameter an array of unsigned bytes (i.e. a Variant
    of type VT_ARRAY | VT_UI1) which holds the contents of an FDF, and returns
    an object of type FDFACXLib.FdfDoc. Client should call the method FDFClose
    on the FdfDoc object when the FdfDoc is no longer needed. Parameters:

    - varFdfData: An array of bytes holding the contents of the FDF.

    Example:    Dim objFdf As FDFACXLib.FdfDoc  'not in VBScript, which supports
                                    'every data type, as long as it is a Variant
                Set objFdf = FdfAcX.FDFOpenFromBuf(Request.BinaryRead(Request.TotalBytes))

    Note: You need to have Windows NT Service Pack 3 (SP3) installed for this
    to work. SP3 includes version 1.0b of ASP, which adds Request.BinaryRead
    and Request.TotalBytes.

    Exceptions: FDFErcFileSysErr (explanation: this method uses a temporary file),
        FDFErcBadFDF, FDFErcInternalError, E_OUTOFMEMORY



FDFOpenFromStr: Takes as parameter a string which holds the contents of an FDF,
    and returns an object of type FDFACXLib.FdfDoc. Client should call the
    method FDFClose on the FdfDoc object when the FdfDoc is no longer needed.
    Note: the preferred way to open an FDF from a stream of bytes is by using
    FDFOpenFromBuf, since FDF contains binary data. However, FDFOpenFromStr is
    provided for the benefit of some environments (e.g. some versions of Visual
    Basic, such as used by HAHTSite) that do not support the data type "Variant
    of type VT_ARRAY | VT_UI1".  Parameters:

    - bstrFdfData: A string holding the contents of the FDF.

    Example:    Dim objFdf As FDFACXLib.FdfDoc  'not in VBScript, which supports
                                    'every data type, as long as it is a Variant
                Dim strFdfData As String
                'Get the FDF data into strFdfData somehow, and then:
                Set objFdf = FdfAcX.FDFOpenFromStr(strFdfData)

    Exceptions: FDFErcFileSysErr (explanation: this method uses a temporary file),
        FDFErcBadFDF, FDFErcInternalError, E_OUTOFMEMORY



FDFClose: Frees resources used by the FDF. Before exiting, client should
    call FDFClose for each open FDF. Client should not use the FdfDoc
    after the FDF has been closed. No parameters.

    Example:    objFdf.FDFClose

    Exceptions: None



FDFSaveToFile: Saves an FDF file to a file. Parameter:

    - fileName: The pathname where the FDF file should be saved. The FDF cannot
        be saved to a file that was opened with FDFOpenFromFile and hasn't yet
        been closed with FDFClose.
    
    Example:    objFdf.FDFSaveToFile "c:\Temp\outFdf.fdf"

    Exceptions: FDFErcFileSysErr, FDFErcInternalError



FDFSaveToBuf: Returns an array of unsigned bytes (i.e. a Variant of type
    VT_ARRAY | VT_UI1) containing the FDF. No parameters.

    Example:    Response.ContentType = "application/vnd.fdf"
                Response.BinaryWrite objFdf.FDFSaveToBuf

    Exceptions: FDFErcFileSysErr (explanation: this method uses a temporary
        file), FDFErcInternalError



FDFSaveToStr: Returns a string containing the FDF. Note: the preferred way to
    get the FDF as a stream of bytes is by using FDFSaveToBuf, since FDF
    contains binary data. However, FDFSaveToStr is provided for the benefit of
    some environments (e.g. some versions of Visual Basic, such as used by
    HAHTSite) that do not support a return value of type "Variant of type
    VT_ARRAY | VT_UI1".  No parameters.

    Example:    Dim strFDF As String
                strFDF = objFdf.FDFSaveToStr

    Exceptions: FDFErcFileSysErr (explanation: this method uses a temporary
        file), FDFErcInternalError, E_OUTOFMEMORY



FDFNextFieldName: Used to enumerate the field names in the FDF, this function
    returns a string containing the fully qualified name of the field that
    comes after the one passed as parameter (or the first one, if none is
    passed). The client can then use the returned field name as parameter to
    many of the methods of the FdfDoc object. Parameter:

    - fieldName: String representing the fully qualified name of the previous
        field, such that FDFNextFieldName will return the one that comes after
        it in the FDF. If fieldName is the empty string, the first field name
        in the FDF is returned.

    Example:    strFirstField = objFdf.FDFNextFieldName("")
                strSecondField = objFdf.FDFNextFieldName(strFirstField)

    Exceptions: FDFErcBadFDF, FDFErcFieldNotFound, FDFErcIncompatibleFDF,
        FDFErcInternalError



FDFGetValue: Returns a string containing the value of a field (i.e. the value
    of /V). Parameter:

    - fieldName: String representing the fully qualified name of the field.
    
    Example:    strLastName = objFdf.FDFGetValue("employee.name.last")

    Exceptions: FDFErcBadFDF, FDFErcFieldNotFound, FDFErcNoValue,
        FDFErcIncompatibleFDF, FDFErcInternalError, E_OUTOFMEMORY



FDFSetValue: Sets the value of a field (i.e. the value of /V). If the field
    does not yet exist in the FDF, it is created. If it does, and already has a
    value, that old value is replaced. If the FdfDoc object is a "template-
    based" FDF (i.e. an FDF for which FDFAddTemplate has been called), then
    FDFSetValue acts on the most recently added template. Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - newValue: String to use as new value.
    - isName: Boolean. If true, then newValue is converted to a PDF Name before
        making it the value of the field. If false, newValue is set to a PDF
        String.
        Note that radio buttons and checkboxes take PDF Names as values. The other
        field types take PDF Strings. For the former, newValue must be either "Off",
        or a value that was entered as the "Export Value" when defining the
        properties of the field in Acrobat.

    Example:    objFdf.FDFSetValue "employee.name.last", "Jones", False

    Exceptions: FDFErcBadFDF, FDFErcCantInsertField, FDFErcInternalError



FDFGetStatus: Returns a string containing the status of the FDF (i.e. the
    value of /Status). 
    Note: FDF exported from an Acrobat Form does not contain a /Status. However,
    FDFGetStatus can still be useful when parsing an FDF produced some other
    way. For example, the FDF may have previously been produced with this very
    toolkit, and saved to a file. No parameters.

    Example:    strStatus = objFdf.FDFGetStatus

    Exceptions: FDFErcIncompatibleFDF, FDFErcInternalError, E_OUTOFMEMORY



FDFSetStatus: Sets the status of the FDF (i.e. the value of /Status). If the
    FDF already has a /Status key, its old value is replaced. Parameter:

    - newStatus: String to use as new status.

    Example:    objFdf.FDFSetStatus "Your order has been entered"

    Exceptions: FDFErcIncompatibleFDF, FDFErcInternalError



FDFGetFile: Returns a string containing the value of the FDF's /F key (which is
    assumed to be a PDF String, since that is what Acrobat produces when
    exporting FDF). No parameters.

    Example:    strFormFileName = objFdf.FDFGetFile

    Exceptions: FDFErcIncompatibleFDF, FDFErcInternalError, E_OUTOFMEMORY



FDFSetFile: Sets the value of the FDF's /F key. If the FDF already has an /F
    key, its old value is replaced. Parameter:

    - newFile: String to use as the new value of the /F key.

    Example:    objFdf.FDFSetFile "http://myserver/TheForm.pdf"

    Exceptions: FDFErcIncompatibleFDF, FDFErcInternalError



FDFGetID: Returns a string containing the value of one element in the FDF's /ID
    key. Parameter:

    - nElemNum: Integer representing the element number to get from the FDF�s
        /ID key. Must be one of the following: 0: the permanent ID; 1: the
        changing ID.

    Example:    strFirstID = objFdf.FDFGetID(0)

    Exceptions: FDFErcBadParameter, FDFErcIncompatibleFDF, FDFErcInternalError,
        E_OUTOFMEMORY



FDFSetID: Sets the value of one element in the FDF's /ID key. If the FDF
    already has an /ID key, the requested element is replaced. Parameters:

    - nElemNum: Integer representing the element number to set in the FDF�s /ID
        key. Must be one of the following: 0: the permanent ID; 1: the changing
        ID.
    - elem: Buffer containing the ID element to set.

    Example:    objFdf.FDFSetID 1, anotherFdf.FDFGetID(1)

    Exceptions: FDFErcBadParameter, FDFErcIncompatibleFDF, FDFErcInternalError



FDFGetEncoding: Returns a string containing the value of the FDF's /Encoding key.
    No parameters.

    Example:    strEncoding = objFdf.FDFGetEncoding

    Exceptions: FDFErcIncompatibleFDF, FDFErcInternalError, E_OUTOFMEMORY



FDFSetEncoding: Sets the value of the FDF's /Encoding key. If the FDF already has
    an /Encoding key, its old value is replaced. Parameter:

    - newEncoding: String that will be converted to a PDF Name before making it
        the value of the /Encoding key. As of Acrobat 4.0, the only value that
        is allowed is: "Shift-JIS" (exact spelling required).

    Example:    objFdf.FDFSetEncoding "Shift-JIS"

    Exceptions: FDFErcIncompatibleFDF, FDFErcInternalError



FDFGetOpt: Returns a string containing the value of one element in a field's
    /Opt key. The /Opt key for a field of type listbox or combobox consists
    of an array. Each element in this array may be a single string, or an
    array of two strings. In the former case, this single string is used as
    both the item name, and the export value. In the latter case, the first
    string is the export value, and the second is the item name.
    Note: FDF exported from an Acrobat Form does not contain an /Opt. However,
    FDFGetOpt can still be useful when parsing an FDF produced some other
    way. For example, the FDF may have previously been produced with this very
    toolkit, and saved to a file. Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - nElemNum: An integer representing the element number to get from the
        field�s /Opt array. The first element in the /Opt array has an index of
        zero.
    - bFirstString: Boolean that determines which of the two strings in the
        desired element should be returned. True gets you the first one, False
        the second one. If the element contains only one string, and
        bFirstString is False, an empty string is returned.

    Example:    strOption = objFdf.FDFGetOpt("Credit Card", 2, True)

    Exceptions: FDFErcBadFDF, FDFErcIncompatibleFDF, FDFErcFieldNotFound,
        FDFErcNoOption, FDFErcInternalError, E_OUTOFMEMORY



FDFGetOptNumElem: Returns a short (Integer) holding the number of elements in
    the field's /Opt array. See also FDFGetOpt. Parameters:

    - fieldName: String representing the fully qualified name of the field.

    Example:    sngNumOptions = objFdf.FDFGetOpt("Credit Card")

    Exceptions: FDFErcBadFDF, FDFErcIncompatibleFDF, FDFErcFieldNotFound,
        FDFErcNoOption, FDFErcInternalError



FDFSetOpt: Sets the value of one element in a field's /Opt key (see also
    FDFGetOpt). If the field does not yet exist in the FDF, it is created.
    If it does, and already has an /Opt key, the requested element in the
    array is replaced. If the FdfDoc object is a "template-based" FDF (i.e.
    an FDF for which FDFAddTemplate has been called), then FDFSetOpt acts on
    the most recently added template.
    Note that an FDF used to dynamically  change the /Opt of a field in an Acrobat
    Form typically needs to also change the /V of that same field (i.e. use
    FDFSetValue when creating the FDF), particularly if the old /V of the
    field is not a member of the new /Opt being imported into the Form.
    Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - nElemNum: The index of the element to set in the field�s /Opt array. The
        first element in the /Opt array has an index of zero.
    - string1: If the /Opt array element to be set will be composed of an array
        containing two strings, then string1 contains the first one (which is
        the export value). If, on the other hand, the /Opt array element will be
        a single string, then string1 contains it (which is the item name).
    - string2: If the /Opt array element to be set will be composed of an array
        containing two strings, then string2 contains the second one (which is
        the item name).  Otherwise, string2 should be the empty string.

    Example:    objFdf.FDFSetOpt "Credit Card", 2, "Visa", ""

    Exceptions: FDFErcBadFDF, FDFErcCantInsertField, FDFErcInternalError



FDFGetFlags: Returns a Long containing the flags of a field, i.e. the value of
    one of: /Ff (field-specific flags), /F (flags common to all annotation
    types).
    Note: FDF exported from an Acrobat Form does not contain flags. However,
    FDFGetFlags can still be useful when parsing an FDF produced some other
    way. For example, the FDF may have previously been produced with this very
    toolkit, and saved to a file. Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - whichFlags: Must be one of: 5 (i.e. /Ff) or 8(i.e. /F) (see FDFItem in
        FdfTk.h)
 
    Example:    Const FDFFf    = 5
                Const FDFFlags = 8
                lngAnnotFlags = objFdf.FDFGetFlags("employee.name.last", FDFFlags)

    Exceptions: FDFErcBadParameter, FDFErcBadFDF, FDFErcIncompatibleFDF,
        FDFErcFieldNotFound, FDFErcNoFlags, FDFErcInternalError



FDFSetFlags: Sets the flags of a field (i.e. the value of one of: /Ff, /F,
    /SetFf, /ClrFf, /SetF, /ClrF). If the field does not yet exist in the FDF,
    it is created. If it does, and already has flags, the old value is
    replaced. If the FdfDoc object is a "template-based" FDF (i.e. an FDF for
    which FDFAddTemplate has been called), then FDFSetFlags acts on the most
    recently added template. Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - whichFlags: Must be one of: 5 (i.e. /Ff), 6 (i.e. /SetFf), 7 (i.e.
        /ClearFf), 8 (i.e. /F), 9 (i.e. /SetF), 10 (i.e. /ClrF) (see FDFItem
        in FdfTk.h)
    - newFlags: A Long holding the new value for flags.
 
    Examples:   Const FDFFf      = 5
                Const FDFSetFf   = 6
                Const FDFClearFf = 7
                Const FDFFlags   = 8
                Const FDFSetF    = 9
                Const FDFClrF    = 10

                    'bit 2 of the annotation flags is the "hidden" bit. The
                    'following statement hides the field "employee.name.last"
                    '(and leaves the other annotation flags untouched). See
                    'the Flags description in the table describing "Annotation
                    'attributes" in the "PDF 1.2 Reference Manual":
                    '<http://www.adobe.com/supportservice/devrelations/PDFS/TN/PDFSPEC.PDF>
                objFdf.FDFSetFlags "employee.name.last", FDFSetF, 2

                    'Make "employee.name.last" visible
                objFdf.FDFSetFlags "employee.name.last", FDFClrF, 2

                    'bit 1 of the field flags is the "read only" flag. The
                    'following statement makes "ssn" read-only (but leaves the
                    'other field flags untouched). See the "Attributes common
                    'to all types of fields" table in the "PDF 1.2 Reference
                    'Manual".
                objFdf.FDFSetFlags "ssn", FDFSetFf, 1

    Exceptions: FDFErcBadParameter, FDFErcBadFDF, FDFErcCantInsertField,
        FDFErcInternalError



FDFGetAP: Gets the appearance of a field (i.e. the value of one of the faces
    of the /AP key), and creates a PDF document out of it. Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - whichFace: Must be one of: 0 (i.e. /N), 1 (i.e. /R), 2 (i.e. /D) (see
        FDFAppFace in FdfTk.h)
    - fileName: String containing the pathname of the PDF file to create.
 
    Example:    Const FDFNormalAP   = 0
                Const FDFRolloverAP = 1
                Const FDFDownAP     = 2
                objFdf.FDFGetAP "my button", FDFNormalAP, "c:\temp\button.pdf" 

    Exceptions: FDFErcBadParameter, FDFErcBadFDF, FDFErcIncompatibleFDF,
        FDFErcFieldNotFound, FDFErcFileSysErr, FDFErcNoAP, FDFErcInternalError



FDFSetAP: Sets the appearance of a field (i.e. the value of one of the faces
    of the /AP key) from a PDF document. If the field does not yet exist in the
    FDF, it is created. If it does, the requested face in the /AP is replaced.
    If the FdfDoc object is a "template-based" FDF (i.e. an FDF for which
    FDFAddTemplate has been called), then FDFSetAP acts on the most recently
    added template.
    Notes: Acrobat will only import an /AP from FDF into fields of type "Button".
    In addition, only the FDFNormalAP (see parameter "whichFace" below) will be
    imported (and FDFDownAP, FDFRolloverAP will be ignored) unless the button that
    the /AP is being imported into has a "Highlight" of type "Push". Finally,
    be aware that the new imported /AP will not show if the "Layout" for the
    button is of type "Text only".
    Tip: If, once the FDF containing the new /AP is imported into the Acrobat
    Form, the picture looks too small inside the button field, with too much white
    space around it, you may want to crop (using Acrobat Exchange) the PDF page
    used as the source of the /AP (i.e. the one identified by parameters "fileName"
    and "pageNum" below). Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - whichFace: Must be one of: 0 (i.e. /N), 1 (i.e. /R), 2 (i.e. /D) (see
        FDFAppFace in FdfTk.h)
    - fileName: String containing the pathname of the PDF file.
    - pageNum: Short containing the page number within the PDF file to be used
        for the appearance (the first page is 1).
 
    Example:    Const FDFNormalAP   = 0
                Const FDFRolloverAP = 1
                Const FDFDownAP     = 2
                objFdf.FDFSetAP "my button", FDFNormalAP, _ 
                                "d:\Acrobat3\Exchange\Clipart\Funstuff.pdf", 1

    Exceptions: FDFErcBadParameter, FDFErcBadFDF, FDFErcBadPDF,
        FDFErcFileSysErr, FDFErcCantInsertField, FDFErcInternalError



FDFSetAPRef: Sets a reference to the PDF document to use for the appearance
    of a field (i.e. the value of one of the faces within the /APRef key). If the
    field does not yet exist in the FDF, it is created. If it does, the requested
    face in the /APRef is replaced. If the FdfDoc object is a "template-based"
    FDF (i.e. an FDF for which FDFAddTemplate has been called), then FDFSetAPRef
    acts on the most recently added template.
    See additional notes for FDFSetAP.
    Note that /AP and /APRef are mutually exclusive. If both appear inside an FDF,
    /AP takes precedence and /APRef is ignored. Note that while /AP inside an FDF
    corresponds to the /AP for a button field in an AcroForm, /APRef is an FDF-only
    key, and is resolved to an /AP during FDF import. In other words, /APRef never
    shows up inside PDF.
    The value of /F within /APRef (i.e. parameter fileName) can be a URL if the PDF
    document that the FDF will be imported into (if any) is being viewed inside a
    web browser. In addition, if the FDF will in fact be imported into an existing
    PDF (as opposed to being used to construct a brand new document), then the value
    of /F can be a relative file specification. /APRef doesn't have to contain an /F
    if the appearance resides (presumably as invisible template) in the PDF that the
    FDF will be imported into. Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - whichFace: Must be one of: 0 (i.e. /N), 1 (i.e. /R), 2 (i.e. /D) (see
        FDFAppFace in FdfTk.h)
    - fileName: String containing the pathname of the PDF file. May be an empty
        string if no /F is needed.
    - templateName: String containing the name of the page ("template") within the
        PDF file specified by fileName, to be used for the appearance.
 
    Example:    Const FDFNormalAP   = 0
                Const FDFRolloverAP = 1
                Const FDFDownAP     = 2
                objFdf.FDFSetAPRef "my button", FDFNormalAP, _ 
                                                "MyTemplates.pdf", "logo"

    Exceptions: FDFErcBadParameter, FDFErcBadFDF, FDFErcBadPDF,
        FDFErcFileSysErr, FDFErcCantInsertField, FDFErcInternalError



FDFSetIF: Sets the Icon Fit that should be used for the appearance of a field
    (i.e. the value of the /IF key). If the field does not yet exist in the FDF,
    it is created. If it does, and already has an /IF key, its old value is
    replaced. The /IF key determines the placement of the PDF icon used as the
    appearance of a button (see also FDFSetAP and FDFSetAPRef). If the FDF does
    not include an /IF, then the icon is placed according to the /IF already in
    the Form (or the default, if that field in the Form does not have an /IF).
    If the FdfDoc object is a "template-based" FDF (i.e. an FDF for which
    FDFAddTemplate has been called), then FDFSetIF acts on the most recently
    added template. Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - scaleWhen: When to scale the icon to fit within the rectangle of the
        annotation that represents the field. Must be one of: 0 (i.e. FDFAlways),
        1 (i.e. FDFTooSmall), 2 (i.e. FDFTooBig), 3  (i.e. FDFNever) (see
        FDFScaleWhen in FdfTk.h)
    - bProportional: Boolean that determines how to do the scaling (if at all):
        proportionally (if True) or not (if False).
    - x: A floating-point value between 0 and 1. 0 places the icon at the left
        edge, and 1 at the right edge, of the annotation rectangle.
    - y: A floating-point value between 0 and 1. 0 places the icon at the bottom
        edge, and 1 at the upper edge, of the annotation rectangle.
 
    Example:    Const FDFAlways   = 0
                Const FDFTooSmall = 1
                Const FDFTooBig   = 2
                Const FDFNever    = 3
                objFdf.FDFSetIF "my button", FDFTooSmall, True, .5, .5 

    Exceptions: FDFErcBadParameter, FDFErcBadFDF,  FDFErcCantInsertField,
        FDFErcInternalError



FDFSetAS: Sets the value of the /AS key for a field. If the field does not
    yet exist in the FDF, it is created. If it does, and already has an /AS
    key, its old value is replaced. 
    Note: do not use FDFSetAS for fields of type checkbox or radio button. Use
    FDFSetValue instead. When the value (i.e. /V) of a field of one of these two
    types changes (to a valid value, i.e. either "Off", or a value that was
    entered as the "Export Value" when defining the properties of the field), the
    /AS key automatically gets set to the correct value by Acrobat. You can use
    FDFSetAS for fields of type button, but only if they have subfaces defined.
    Currently Acrobat does not have a user interface that allows for defining
    subfaces for buttons. This could be accomplished using pdfmark instead (see
    <http://beta1.adobe.com/ada/acrosdk/DOCS/PDFMARK.PDF>).
    Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - newAS: String that will be converted to a PDF Name to use as the new
        value of /AS.

    Example:    objFdf.FDFSetAS "multi-face button", "subface1"

    Exceptions: FDFErcBadFDF, FDFErcIncompatibleFDF, FDFErcCantInsertField,
        FDFErcInternalError



FDFSetSubmitFormAction: Sets the value of either the /A or /AA keys of a
    field to an action of type SubmitForm, using the passed URL and field
    names (which is an optional parameter). If the field does not yet exist in
    the FDF, it is created. If it does, and already has an /A (or /AA) key, its
    old value is replaced. If the FdfDoc object is a "template-based" FDF (i.e.
    an FDF for which FDFAddTemplate has been called), then FDFSetSubmitFormAction
    acts on the most recently added template. Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - whichTrigger: Must be one of: 0 (i.e. /E), 1 (i.e. /X), 2 (i.e. /D),
        3 (i.e. /U) (see FDFActionTrigger in FdfTk.h)
    - theURL: String to be used as value for the /F key in the SubmitForm
        action.
    - theFlags: Long to use as value for the /Flags key in the SubmitForm
        action. If zero, since the meaning of each individual flag was
        carefully defined to mean the same as if the /Flags key wasn't there
        at all, no /Flags key is inserted.  Please check "SubmitForm Action"
        in the "PDF 1.2 Reference Manual" for the meaning of each bit in /Flags,
        including how to interpret the "rgFields" parameter (below) as either
        fields to submit, or to exclude from submitting.
    - rgFields: Optional parameter. If not passed, then the created action
        will not include a /Fields key. Otherwise, it should be an array of
        strings, representing the names of the fields to submit (or exclude
        from submitting) when the action gets executed.
 
    Example:    Const FDFEnter  = 0
                Const FDFExit   = 1
                Const FDFDown   = 2
                Const FDFUp     = 3
                Dim rgFlds(1)
                rgFlds(0)       = "field1"
                rgFlds(1)       = "field2"

                objFdf.FDFSetSubmitFormAction "my button", FDFUp, _ 
                        "http://myserver/ASPSamp/Samples/testfdf.asp#FDF", _
                        4, rgFlds 'Set flags for submit in HTML Form format

    Exceptions: FDFErcBadParameter, FDFErcBadFDF, FDFErcCantInsertField,
        FDFErcInternalError, E_OUTOFMEMORY



FDFSetResetFormAction: Sets the value of either the /A or /AA keys of a
    field to an action of type ResetForm, using the passed field names (which
    is an optional parameter). If the field does not yet exist in
    the FDF, it is created. If it does, and already has an /A (or /AA) key,
    its old value is replaced. If the FdfDoc object is a "template-based" FDF
    (i.e. an FDF for which FDFAddTemplate has been called), then
    FDFSetResetFormAction acts on the most recently added template. Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - whichTrigger: Must be one of: 0 (i.e. /E), 1 (i.e. /X), 2 (i.e. /D),
        3 (i.e. /U) (see FDFActionTrigger in FdfTk.h)
    - theFlags: Long to use as value for the /Flags key in the ResetForm
        action. If zero, since the meaning of each individual flag was
        carefully defined to mean the same as if the /Flags key wasn't there
        at all, no /Flags key is inserted. Please check "ResetForm Action" in
        the "PDF 1.2 Reference Manual" for the meaning of each bit in /Flags,
        including how to interpret the "rgFields" parameter (below) as either
        fields to reset, or to exclude from resetting.
    - rgFields: Optional parameter. If not passed, then the created action
        will not include a /Fields key. Otherwise, it should be an array of
        strings, representing the names of the fields to reset (or exclude
        from resetting) when the action gets executed.
 
    Example:    Const FDFExit = 1
                objFdf.FDFSetResetFormAction "my button", FDFExit, 0

    Exceptions: FDFErcBadParameter, FDFErcBadFDF, FDFErcCantInsertField,
        FDFErcInternalError, E_OUTOFMEMORY



FDFSetImportDataAction: Sets the value of either the /A or /AA keys of a
    field to an action of type ImportData. If the field does not yet exist in
    the FDF, it is created. If it does, and already has an /A (or /AA) key, its
    old value is replaced. If the FdfDoc object is a "template-based" FDF
    (i.e. an FDF for which FDFAddTemplate has been called), then
    FDFSetImportDataAction acts on the most recently added template. Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - whichTrigger: Must be one of: 0 (i.e. /E), 1 (i.e. /X), 2 (i.e. /D),
        3 (i.e. /U) (see FDFActionTrigger in FdfTk.h)
    - theFile: String to be used as value for the /F key in the ImportData
        action.
 
    Example:    Const FDFUp = 3
                objFdf.FDFSetImportDataAction "my button", FDFUp, "myprof.fdf"

    Exceptions: FDFErcBadParameter, FDFErcBadFDF, FDFErcCantInsertField,
        FDFErcInternalError



FDFSetJavaScriptAction: Sets the value of either the /A or /AA keys of a
    field to an action of type JavaScript. If the field does not yet exist in
    the FDF, it is created. If it does, and already has an /A (or /AA) key, its
    old value is replaced. If the FdfDoc object is a "template-based" FDF (i.e.
    an FDF for which FDFAddTemplate has been called), then FDFSetJavaScriptAction
    acts on the most recently added template. Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - whichTrigger: Must be one of: 0 (i.e. /E), 1 (i.e. /X), 2 (i.e. /D),
        3 (i.e. /U) (see FDFActionTrigger in FdfTk.h)
    - theScript: String containing the text of the script.
 
    Example:    Const FDFUp = 3
                'Use Chr(13) to add a CR, Chr(9) for tabs
                objFdf.FDFSetJavaScriptAction "my button", FDFUp, _
                            "var f = this.getField(""Approved"");" & _
                            Chr(13) & "f.hidden = false;"

    Exceptions: FDFErcBadParameter, FDFErcBadFDF, FDFErcCantInsertField,
        FDFErcInternalError



FDFSetGoToAction: Sets the value of either the /A or /AA keys of a
    field to an action of type GoTo using a named destination. If the field
    does not yet exist in the FDF, it is created. If it does, and already has an
    /A (or /AA) key, its  old value is replaced. If the FdfDoc object is a
    "template-based" FDF (i.e. an FDF for which FDFAddTemplate has been called),
    then FDFSetGoToAction acts on the most recently added template. Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - whichTrigger: Must be one of: 0 (i.e. /E), 1 (i.e. /X), 2 (i.e. /D),
        3 (i.e. /U) (see FDFActionTrigger in FdfTk.h)
    - theDest: String to be used as value for the /D key in the GoTo action.
    - isName: Boolean. If true, then theDest is converted to a PDF Name before
        making it the value of /D. If false, theDest is set to a PDF String.

    Example:    Const FDFDown = 2
                objFdf.FDFSetGoToAction "my button", FDFDown, "Chap6.begin", True

    Exceptions: FDFErcBadParameter, FDFErcBadFDF, FDFErcCantInsertField,
        FDFErcInternalError



FDFSetGoToRAction: Sets the value of either the /A or /AA keys of a field
    to an action of type GoToR using a named destination. If the field
    does not yet exist in the FDF, it is created. If it does, and already has an
    /A (or /AA) key, its  old value is replaced. If the FdfDoc object is a
    "template-based" FDF (i.e. an FDF for which FDFAddTemplate has been called),
    then FDFSetGoToRAction acts on the most recently added template. Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - whichTrigger: Must be one of: 0 (i.e. /E), 1 (i.e. /X), 2 (i.e. /D),
        3 (i.e. /U) (see FDFActionTrigger in FdfTk.h)
    - theDest: String to be used as value for the /D key in the GoTo action.
    - isName: Boolean. If true, then theDest is converted to a PDF Name before
        making it the value of /D. If false, theDest is set to a PDF String.
    - theFile: String to be used as value for the /F key in the GoToR action.
    - addNewWindow: Boolean. If True, will add a /NewWindow key to the GoToR
        action, set to the value passed in newWindow. If False, will not add
        it, and newWindow is ignored.
    - newWindow: Boolean. Value for the /NewWindow key in the GoToR action.
        Only used if addNewWindow is True.

    Example:    Const FDFUp = 3
                objFdf.FDFSetGoToRAction "my button", FDFUp, "Appendix H", _
                                         False, "pdfspec.pdf", True, False

    Exceptions: FDFErcBadParameter, FDFErcBadFDF, FDFErcCantInsertField,
        FDFErcInternalError



FDFSetURIAction: Sets the value of either the /A or /AA keys of a field to
    an action of type URI. If the field does not yet exist in the FDF, it is
    created. If it does, and already has an /A (or /AA) key, its  old value is
    replaced. If the FdfDoc object is a "template-based" FDF (i.e. an FDF for
    which FDFAddTemplate has been called), then FDFSetURIAction acts on the most
    recently added template. Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - whichTrigger: Must be one of: 0 (i.e. /E), 1 (i.e. /X), 2 (i.e. /D),
        3 (i.e. /U) (see FDFActionTrigger in FdfTk.h)
    - theURI: String to be used as value for the /URI key in the URI action.
    - isMap: Boolean. Value for the /IsMap key in the URI action. If False, no
        /IsMap key is added.

    Example:    Const FDFUp = 3
                objFdf.FDFSetURIAction "my button", FDFUp, _
                                       "http://myserver/TheForm.pdf", False

    Exceptions: FDFErcBadParameter, FDFErcBadFDF, FDFErcCantInsertField,
        FDFErcInternalError



FDFSetNamedAction: Sets the value of either the /A or /AA keys of a field
    to a Named action. If the field does not yet exist in the FDF, it is
    created. If it does, and already has an /A (or /AA) key, its  old value is
    replaced. If the FdfDoc object is a "template-based" FDF (i.e. an FDF for
    which FDFAddTemplate has been called), then FDFSetNamedAction acts on the
    most recently added template. Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - whichTrigger: Must be one of: 0 (i.e. /E), 1 (i.e. /X), 2 (i.e. /D),
        3 (i.e. /U) (see FDFActionTrigger in FdfTk.h)
    - theName: String that will be converted to a PDF Name to use as the value
        of /N in the Named action.

    Example:    Const FDFUp = 3
                objFdf.FDFSetNamedAction "my button", FDFUp, "FirstPage"

    Exceptions: FDFErcBadParameter, FDFErcBadFDF, FDFErcCantInsertField,
        FDFErcInternalError



FDFSetHideAction: Sets the value of either the /A or /AA keys of a field to
    an action of type Hide. If the field does not yet exist in the FDF, it is
    created. If it does, and already has an /A (or /AA) key, its  old value is
    replaced. If the FdfDoc object is a "template-based" FDF (i.e. an FDF for
    which FDFAddTemplate has been called), then FDFSetHideAction acts on the
    most recently added template. Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - whichTrigger: Must be one of: 0 (i.e. /E), 1 (i.e. /X), 2 (i.e. /D),
        3 (i.e. /U) (see FDFActionTrigger in FdfTk.h)
    - theTarget: String to be used as value for the /T key in the Hide action.
    - isHide: Boolean. Value for the /H key in the Hide action. If True, no /H
        key is added.

    Example:    Const FDFUp = 3
                objFdf.FDFSetHideAction "my button", FDFUp, "hideable field", True

    Exceptions: FDFErcBadParameter, FDFErcBadFDF, FDFErcCantInsertField,
        FDFErcInternalError



FDFSetStateAction: Sets the value of either the /A or /AA keys of a field to
    an action of type SetState. If the field does not yet exist in the FDF, it is
    created. If it does, and already has an /A (or /AA) key, its  old value is
    replaced. If the FdfDoc object is a "template-based" FDF (i.e. an FDF for
    which FDFAddTemplate has been called), then FDFSetStateAction acts on the
    most recently added template. Parameters:

    - fieldName: String representing the fully qualified name of the field.
    - whichTrigger: Must be one of: 0 (i.e. /E), 1 (i.e. /X), 2 (i.e. /D),
        3 (i.e. /U) (see FDFActionTrigger in FdfTk.h)
    - theTarget: String to be used as value for the /T key in the SetState action.
    - theAS: String that will be converted to a PDF Name to use as the value of
        /AS in the SetState action.

    Example:    Const FDFUp = 3
                objFdf.FDFSetStateAction "my button", FDFUp, "Is New Address", _
                                         "yes"

    Exceptions: FDFErcBadParameter, FDFErcBadFDF, FDFErcCantInsertField,
        FDFErcInternalError



FDFRemoveItem: Removes an item (i.e. a key-value pair) from the FDF.
    If the item to be removed is not present, no error occurs (the function
    returns FDFErcOK). Parameters:

    - fieldName: String representing the fully qualified name of the field
        from which the key-value pair is to be removed. If the item to be
        removed is not specific to one particular field (i.e. /Status, /F, /ID),
        then pass an empty string.
    - whichItem: Item to be removed. Must be one of: 0 (i.e. /V),
        1 (i.e. /Status), 2 (i.e. /F(ile)), 3 (i.e. /ID), 4 (i.e. /Opt),
        5 (i.e. /Ff), 6 (i.e. /SetFf), 7 (i.e. /ClearFf), 8 (i.e. /F(lags)),
        9 (i.e. /SetF), 10 (i.e. /ClrF), 11 (i.e. /AP), 12 (i.e. /AS),
        13 (i.e. /A), 14 (i.e. /AA), 15 (i.e. /APRef), 16 (i.e. /IF) (see
        FDFItem in FdfTk.h)

    Example:    Const FDFFile = 2
                objFdf.FDFRemoveItem "", FDFFile

    Exceptions: FDFErcBadParameter, FDFErcBadFDF, FDFErcIncompatibleFDF,
        FDFErcFieldNotFound, FDFErcInternalError



FDFAddTemplate: Adds a template to the FDF. There are 2 types of FDF: the
    "classic" kind first introduced with Acrobat 3.0, and the "template-based"
    kind that directs the construction of a brand new PDF document from templates
    found inside specified PDF documents. You cannot add templates to a "classic"
    FDF: they are mutually exclusive (and you will get FDFErcIncompatibleFDF if
    you try). Many of the calls in the FDF Library (e.g. FDFSetFile) are
    incompatible with a "template-based" FDF (i.e. an FDF for which FDFAddTemplate
    has been called), and will as well return FDFErcIncompatibleFDF if called with
    such an FDF. Other calls (e.g. FDFSetValue) are ok with either kind of FDF.
    If called with a "template-based" FDF, they act on the most recently added
    template.  Parameters:

    - bNewPage: Boolean. If True, the template is used to start a new page,
        otherwise, it is appended to the last page (if any: if this is the very
        first template added to the FDF, this parameter is ignored). Note that
        when a "template- based" FDF is used to construct a brand new PDF
        document, each page can be the result of overlaying multiple templates
        one after another.
    - fileName: String containing the pathname of the PDF file to be used as the
        value for the /F key within the /TRef key, which is the file specification
        for the PDF where the template is. May be an empty
        string (in which case when the FDF gets imported into Acrobat, the
        template is expected to reside inside the PDF file currently being viewed,
        which will get replaced as the top most document with the new PDF
        constructed as a result of importing the FDF).
    - templateName: String containing the name of the template within the PDF
        file specified by fileName.
    - bRename: Boolean. If False, will add a /Rename key whose value is false,
        otherwise it won't (since true is the default). When renaming is
        indicated, all fields in the template modified by the FDF (for example,
        because new values for them are included) get renamed. The purpose of
        the /Rename key is to provide flexibility concerning whether fields get
        renamed as templates get spawned. The default is to do the renaming, in
        order to prevent potential conflicts between field names originating in
        different templates. The renaming scheme consists of prepending
        "P" & page # & ".template name_." & template # before the field name.
        For example, if a template called "flower" gets spawned into page 3, and
        this is the second template for that page in the FDF, then field "color"
        on that template gets renamed to "P3.flower_1.color". However, the
        application designer can choose to include /Rename false if she knows
        that there are no name conflicts in the templates being used. Not doing the
        renaming simplifies her application, since she knows what the field names
        are, and can use them in cgi-scripts, JavaScripts, etc. without worrying
        about what they get renamed to.

    Example:    objFdf.FDFAddTemplate True, "MyTemplates.pdf", "logo", True

    Exceptions: FDFErcBadParameter, FDFErcBadFDF, FDFErcIncompatibleFDF,
        FDFErcInternalError
