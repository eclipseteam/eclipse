#region References
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Threading;
using System.Reflection; 
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Runtime.InteropServices;
#endregion

namespace Oritax.TaxSimp.Licence
{
	public class LicenceStamper
	{
		private const string LICENCE_STAMPER_VERSION = "1.0.1";
		private const string LICENCE_FILE	= "f";
		private const string LICENCE_VERIFY = "v";
		private const string LICENCE_SIGN	= "s";
		private const string LICENCE_HELP	= "?";



		public LicenceStamper()
		{

		}

		[STAThread]
		static void Main(string[] args) 
		{
			// Allow to accept parameters
			Hashtable htargs = new Hashtable();


			if(args.Length > 0)
			{
				htargs = parseArgs(args);

				if (!htargs[LICENCE_HELP].Equals(string.Empty))
				{
					ShowCommandLineHelp();
				}
				else if (htargs[LICENCE_FILE].Equals(string.Empty))
				{
					string errorMessage = string.Format("Option /f requires a filename to be passed in. Please view the help /?.");
					Console.WriteLine(errorMessage);
					DebuggerWait();
				}
				else if (!htargs[LICENCE_FILE].Equals(string.Empty))
				{
					if(!File.Exists(htargs[LICENCE_FILE].ToString()))
					{
						string errorMessage = string.Format("The file {0} provided does not exist.",new string[] {htargs[LICENCE_FILE].ToString()});
						Console.WriteLine(errorMessage);
						DebuggerWait();
					}
					else if((htargs[LICENCE_SIGN].Equals(string.Empty)) && (htargs[LICENCE_VERIFY].Equals(string.Empty)))
					{
						string errorMessage = string.Format("A valid switch must be provided. /v or /s.");
						Console.WriteLine(errorMessage);
						DebuggerWait();
					}
					else if(!(htargs[LICENCE_SIGN].Equals(string.Empty)))
					{
						SignLicenceFile(htargs[LICENCE_FILE].ToString());
					}
					else if(!(htargs[LICENCE_VERIFY].Equals(string.Empty)))
					{
						VerifyLicenceFile(htargs[LICENCE_FILE].ToString());
					}
					else
					{
						Console.WriteLine("Could not sign or verify the licence file.");
						DebuggerWait();
					}

				}

			}	
			else
			{
				Application.Run(new Login());
			}
		}

		#region Sign  or Verify Mode
		private static void SignLicenceFile(string fileName)
		{
			string messages = string.Empty;
			DigitallySign ds = new DigitallySign();

			ds.SignFile(fileName,out messages);
			Console.WriteLine(messages);
			DebuggerWait();

		}

		private static void VerifyLicenceFile(string fileName)
		{
			string messages = string.Empty;
			DigitallySign ds = new DigitallySign();

			ds.VerifyFile(fileName,out messages);
			Console.WriteLine(messages);
			DebuggerWait();

		}

		#endregion

		#region Command Line Help
		private static void ShowCommandLineHelp()
		{
			string CommandHelp = string.Empty;

			CommandHelp  ="__________________________________________________________________\n";
			CommandHelp += "Usage: LicenceStamper.exe /f:c:\\temp\\MyDemoLicenceFile.xml /s\n\n";
			CommandHelp += "Options:\n";
			CommandHelp += "/f:<Filename.xml> - Location of (Path and Filename) of the nable licence file.\n";
			CommandHelp += "/s - To sign the file.\n";
			CommandHelp += "/v - To verify a Signed file.\n\n\nLicence Stamper Version "+LICENCE_STAMPER_VERSION;
			CommandHelp +="\n__________________________________________________________________\n";

			Console.WriteLine(CommandHelp);
			DebuggerWait();

		}
		#endregion


		#region Parse Arguments
		private static Hashtable parseArgs(string[] args)
		{
			Hashtable ht = new Hashtable();
			int argNum=0;

			#region Parameter Listing
			/*
			 *  /F: <filename of Licence>
			 *  /S: is to stamp it
			 *  /V: is to verify it 
			 *  /? is the see help 
			 * 
			 */
			#endregion

			#region Argument Defaults
			//pump in hard coded Defaults;
			
			ht[LICENCE_FILE] = string.Empty;
			ht[LICENCE_VERIFY]	= string.Empty;
			ht[LICENCE_SIGN] = string.Empty;
			ht[LICENCE_HELP] = string.Empty;

			#endregion

			#region Collate the Arguments
			for(int i=0; i<args.Length; i++)
			{
				//check to see if the argument is an optional switch or absolute argument
				if(args[i][0] == '/' || args[i][0] == '-' || args[i][0] == '\\')
				{
					//Check to see if the switch is compound in that it has some text after a colon
					int colonPos = args[i].IndexOf(":");
					if(colonPos >= 0)
					{
						//compound switch
						//suck out the text before the colon
						string swName = args[i].Substring(1,colonPos-1).Trim().ToLower();
						//suck out the text after the colon
						string swValue = args[i].Substring(colonPos+1).Trim().ToLower();

						ht[swName] = swValue;
					}
					else
					{
						//the switch is a simple switch
						//check to see that it has a least one extra charater rather that the dash or slash
						if(args[i].Length > 1)
						{
							string swName = args[i].Substring(1).Trim().ToLower();
							ht[swName] = swName;
						}
					}
				}
				else
				{
					//put the mandatory field into ht with key argX
					//where x is a number
					ht["arg"+argNum.ToString()] = args[i];
					argNum++;
				}
			}

			return ht;
			#endregion
		}
		private static void DebuggerWait()
		{
			if(Debugger.IsAttached)
			{
				Console.Write("Press Enter Key to Continue");
				Console.ReadLine();
			}
		}
		#endregion

	}
}
