using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace Oritax.TaxSimp.Licence
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button cmdCancel;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label lblMessage;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Button cmdCreateLicenceFiles;
		private System.Windows.Forms.Button CmdSignFiles;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label lblCreate;
		private System.Windows.Forms.Label lblViewLicence;
		private System.Windows.Forms.Label lblSign;
		private System.Windows.Forms.Button cmdSign;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			Init();
			
		}

		private void Init()
		{
			string VERSION = NableConstants.LICENCE_STAMPER_VERSION;  
			this.lblCreate.Text = "Create N-ABLE XML licence file.";
			this.lblSign.Text = "Digitally sign the XML licence file or\n sign the chart of accounts CSV files.";
			this.lblViewLicence.Text = "Open existing N-ABLE XML licence file and view content."; 
			this.Text  = "N-ABLE Licence Stamper";
			this.lblMessage.Text = "N-ABLE Licence Stamper ("+VERSION+")";
			
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmMain));
			this.panel1 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.lblMessage = new System.Windows.Forms.Label();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.cmdCreateLicenceFiles = new System.Windows.Forms.Button();
			this.CmdSignFiles = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.cmdSign = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.lblCreate = new System.Windows.Forms.Label();
			this.lblViewLicence = new System.Windows.Forms.Label();
			this.lblSign = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(576, 48);
			this.panel1.TabIndex = 4;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(200, 32);
			this.label1.TabIndex = 0;
			this.label1.Text = "Licence Stamper";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.cmdCancel);
			this.panel2.Controls.Add(this.groupBox1);
			this.panel2.Controls.Add(this.lblMessage);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(0, 333);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(576, 40);
			this.panel2.TabIndex = 21;
			// 
			// cmdCancel
			// 
			this.cmdCancel.Location = new System.Drawing.Point(496, 16);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.TabIndex = 3;
			this.cmdCancel.Text = "Close";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(576, 8);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			// 
			// lblMessage
			// 
			this.lblMessage.ForeColor = System.Drawing.Color.Gray;
			this.lblMessage.Location = new System.Drawing.Point(16, 16);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(472, 16);
			this.lblMessage.TabIndex = 20;
			this.lblMessage.Text = "N-ABLE Licence Stamper (1.0.4)";
			this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// pictureBox2
			// 
			this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(77)), ((System.Byte)(90)));
			this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.Location = new System.Drawing.Point(0, 48);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(90, 285);
			this.pictureBox2.TabIndex = 22;
			this.pictureBox2.TabStop = false;
			// 
			// cmdCreateLicenceFiles
			// 
			this.cmdCreateLicenceFiles.Image = ((System.Drawing.Image)(resources.GetObject("cmdCreateLicenceFiles.Image")));
			this.cmdCreateLicenceFiles.Location = new System.Drawing.Point(120, 72);
			this.cmdCreateLicenceFiles.Name = "cmdCreateLicenceFiles";
			this.cmdCreateLicenceFiles.Size = new System.Drawing.Size(80, 72);
			this.cmdCreateLicenceFiles.TabIndex = 24;
			this.cmdCreateLicenceFiles.Click += new System.EventHandler(this.cmdCreateLicenceFiles_Click);
			// 
			// CmdSignFiles
			// 
			this.CmdSignFiles.Enabled = false;
			this.CmdSignFiles.Image = ((System.Drawing.Image)(resources.GetObject("CmdSignFiles.Image")));
			this.CmdSignFiles.Location = new System.Drawing.Point(120, 160);
			this.CmdSignFiles.Name = "CmdSignFiles";
			this.CmdSignFiles.Size = new System.Drawing.Size(80, 72);
			this.CmdSignFiles.TabIndex = 25;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(208, 72);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(256, 16);
			this.label2.TabIndex = 26;
			this.label2.Text = "Create Licence File";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(208, 248);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(344, 16);
			this.label3.TabIndex = 27;
			this.label3.Text = "Sign / Verify Licence File or COA csv files";
			// 
			// cmdSign
			// 
			this.cmdSign.Image = ((System.Drawing.Image)(resources.GetObject("cmdSign.Image")));
			this.cmdSign.Location = new System.Drawing.Point(120, 248);
			this.cmdSign.Name = "cmdSign";
			this.cmdSign.Size = new System.Drawing.Size(80, 72);
			this.cmdSign.TabIndex = 28;
			this.cmdSign.Click += new System.EventHandler(this.cmdSign_Click);
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label4.Location = new System.Drawing.Point(208, 160);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(256, 16);
			this.label4.TabIndex = 29;
			this.label4.Text = "View Licence File";
			// 
			// lblCreate
			// 
			this.lblCreate.Location = new System.Drawing.Point(216, 88);
			this.lblCreate.Name = "lblCreate";
			this.lblCreate.Size = new System.Drawing.Size(344, 40);
			this.lblCreate.TabIndex = 30;
			this.lblCreate.Text = "<< Info >>";
			// 
			// lblViewLicence
			// 
			this.lblViewLicence.Location = new System.Drawing.Point(216, 176);
			this.lblViewLicence.Name = "lblViewLicence";
			this.lblViewLicence.Size = new System.Drawing.Size(344, 40);
			this.lblViewLicence.TabIndex = 31;
			this.lblViewLicence.Text = "<< Info >>";
			// 
			// lblSign
			// 
			this.lblSign.Location = new System.Drawing.Point(216, 264);
			this.lblSign.Name = "lblSign";
			this.lblSign.Size = new System.Drawing.Size(344, 40);
			this.lblSign.TabIndex = 32;
			this.lblSign.Text = "<< Info >>";
			// 
			// frmMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(576, 373);
			this.ControlBox = false;
			this.Controls.Add(this.lblSign);
			this.Controls.Add(this.lblViewLicence);
			this.Controls.Add(this.lblCreate);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.cmdSign);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.CmdSignFiles);
			this.Controls.Add(this.cmdCreateLicenceFiles);
			this.Controls.Add(this.pictureBox2);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "frmMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "N-ABLE Licence Stamper";
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
	

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void cmdSign_Click(object sender, System.EventArgs e)
		{
			this.Hide();
			frmSign signForm  = new frmSign();
			signForm.ShowDialog(this);
			this.Show();
		}

		private void cmdCreateLicenceFiles_Click(object sender, System.EventArgs e)
		{
			this.Hide();
			frmCreate createForm  = new frmCreate();
			createForm.ShowDialog(this);
			this.Show();
		}

	}
}
