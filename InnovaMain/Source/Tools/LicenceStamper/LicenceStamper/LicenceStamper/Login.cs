using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Runtime.CompilerServices;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Licence
{
	public class Login : System.Windows.Forms.Form
	{
		
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.TextBox txtPassword;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.LinkLabel lnkLogin;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Label lblVersion;
		private System.ComponentModel.Container components = null;

		#region Constants --------------------------------------------------------------
		
		
		

		#endregion

		public Login()
		{
			InitializeComponent();
			lblVersion.Text ="Version "+NableConstants.LICENCE_STAMPER_VERSION;
			
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Login));
			this.label1 = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.txtPassword = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.lblVersion = new System.Windows.Forms.Label();
			this.lnkLogin = new System.Windows.Forms.LinkLabel();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(64)), ((System.Byte)(64)));
			this.label1.Location = new System.Drawing.Point(8, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(208, 24);
			this.label1.TabIndex = 0;
			this.label1.Text = "N-ABLE Licence Stamper Login";
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackColor = System.Drawing.Color.YellowGreen;
			this.pictureBox1.Location = new System.Drawing.Point(8, 40);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(328, 1);
			this.pictureBox1.TabIndex = 1;
			this.pictureBox1.TabStop = false;
			// 
			// txtPassword
			// 
			this.txtPassword.BackColor = System.Drawing.Color.White;
			this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtPassword.ForeColor = System.Drawing.Color.Gray;
			this.txtPassword.Location = new System.Drawing.Point(80, 96);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.PasswordChar = '*';
			this.txtPassword.Size = new System.Drawing.Size(240, 20);
			this.txtPassword.TabIndex = 3;
			this.txtPassword.Text = "";
			this.txtPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPassword_KeyPress);
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.Gray;
			this.label2.Location = new System.Drawing.Point(16, 56);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(320, 24);
			this.label2.TabIndex = 4;
			this.label2.Text = "Please enter the password for the stamper for current release.";
			// 
			// lblVersion
			// 
			this.lblVersion.AutoSize = true;
			this.lblVersion.ForeColor = System.Drawing.Color.Silver;
			this.lblVersion.Location = new System.Drawing.Point(16, 144);
			this.lblVersion.Name = "lblVersion";
			this.lblVersion.Size = new System.Drawing.Size(82, 16);
			this.lblVersion.TabIndex = 5;
			this.lblVersion.Text = "Version <<...>>";
			this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lnkLogin
			// 
			this.lnkLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
			this.lnkLogin.LinkArea = new System.Windows.Forms.LinkArea(0, 10);
			this.lnkLogin.LinkColor = System.Drawing.Color.DimGray;
			this.lnkLogin.Location = new System.Drawing.Point(264, 120);
			this.lnkLogin.Name = "lnkLogin";
			this.lnkLogin.Size = new System.Drawing.Size(56, 24);
			this.lnkLogin.TabIndex = 7;
			this.lnkLogin.TabStop = true;
			this.lnkLogin.Text = "Login";
			this.lnkLogin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.lnkLogin.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLogin_LinkClicked);
			// 
			// pictureBox2
			// 
			this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.Location = new System.Drawing.Point(16, 88);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(51, 46);
			this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox2.TabIndex = 8;
			this.pictureBox2.TabStop = false;
			// 
			// Login
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(346, 170);
			this.Controls.Add(this.pictureBox2);
			this.Controls.Add(this.lnkLogin);
			this.Controls.Add(this.lblVersion);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.txtPassword);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Login";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "N-ABLE Licence Stamper Login";
			this.ResumeLayout(false);

		}
		#endregion

		private void lnkLogin_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			DoLogin();
		}

		private void DoLogin()
		{
			string message = string.Empty ;

			if(validateLogin(out message))
			{
				this.Hide();
				frmMain mainMenuScreen = new frmMain();
				mainMenuScreen.ShowDialog(this);
			}
			else
			{
				NableMessageBox nableMSGBox = new NableMessageBox("Licence Stamper Login",message,true);
				nableMSGBox.ShowDialog(this);
				txtPassword.Focus();
				txtPassword.SelectAll();
			
			}
		}

		private bool validateLogin(out string Message)
		{
			bool LoginOK = true;

			Message = string.Empty;
			string OritaxNABLE = NableConstants.LICENCE_STAMPER_VERSION.Replace(".",string.Empty);
			string actualPassword = NableConstants.NABLE+OritaxNABLE;
			if(txtPassword.Text != actualPassword)
			{
				Message ="Incorrect password for this release.";
				LoginOK=false;
			}
	
			return LoginOK;
		}

		private void txtPassword_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar==13)
			{
				if(!txtPassword.Text.Trim().Equals(string.Empty))
					DoLogin();
					
			}
		}
	}
}
