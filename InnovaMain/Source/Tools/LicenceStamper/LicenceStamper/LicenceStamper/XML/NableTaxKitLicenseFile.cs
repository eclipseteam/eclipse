using System;
using Oritax.TaxSimp.TaxSimpLicence; 

namespace Oritax.TaxSimp.Licence
{
	internal class NableTaxKitLicenseFile: NableLicenseFile
	{
		#region Main Constructor -------------------------------------------------------
		public NableTaxKitLicenseFile()
		{
		
		}
		public NableTaxKitLicenseFile(out LicenceDS Licence):base(out Licence)
		{
			
		}
		#endregion 
		#region Component Licensing ----------------------------------------------------
		
		protected override void AddComponentsList()
		{
			#region Modules Section
			// Setup Contants
			const string GroupName		= "Modules";
			const string GroupInstance	= "-1";

			// Add the Modules Group
			this.licenceDS.AddComponentGroupRow(GroupName,GroupInstance);  

			#endregion
			#region Tax Kit Required Components
			
			// ATO Forms 2003
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("5B144E3D-EE2F-4804-A01B-D219A7F227E5"),
				"ATO Forms",
				GroupInstance,
				"1.1.0.2"
				);

			// ATO Forms 2004
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("5B144E3D-EE2F-4804-A01B-D219A7F227E5"),
				"ATO Forms",
				"-1",
				"2.1.0.2"
				);

			// ATO Forms 2005
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("5B144E3D-EE2F-4804-A01B-D219A7F227E5"),
				"ATO Forms",
				"-1",
				"3.1.0.0"
				);

			// ATO Forms 2006
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("5B144E3D-EE2F-4804-A01B-D219A7F227E5"),
				"ATO Forms",
				"-1",
				"4.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("B16CDF66-8AAD-428F-9F5B-D455770EC516"),
				"Various adjustments - temporary differences",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("09168B23-A85A-43bc-8DED-14580DE89149"),
				"Borrowing costs",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("{1003D369-F9DF-4cb9-8654-2D47C02B61D2}"),
				"Business related costs",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("FA541B84-3896-4504-84B1-C36A290E7404"),
				"Australian Entertainment",
				GroupInstance,
				"1.1.1.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("2C81CFA2-16BD-4879-B2EA-412E1296A202"),
				"Australian Exempt Income",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("7BC5C5C8-397D-4ac3-9E15-53F4488406D8"),
				"Intangibles",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("4E93E44C-B609-4b33-86C6-D778AFBDFBA1"),
				"Income tax calculation",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("04F630BF-F59C-45e4-8856-30D2913732D4"),
				"Australian Legals",
				GroupInstance,
				"1.1.1.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("489D1006-A332-4193-A0C3-62836CCDE104"),
				"Repair and maintenance",
				GroupInstance,
				"1.1.1.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("A41CD257-CE8A-46ab-BCFD-FBDE02A85F34"),
				"Trading stock",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("{F3491EE8-E354-4c8a-90A9-2836AD0EC125}"),
				"Investments",
				GroupInstance,
				"1.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("A7A2B69D-181A-458c-8221-8A0B497EAF2F"),
				"Consumables",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("C8032301-B42F-453a-8AE1-B4AE7E6D85A5"),
				"Accrued income",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("E887589E-D574-49b5-86AD-4FA66C3B33EB"),
				"Accrued expenses",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("8C3F8BB9-7EA1-4567-A691-89A80E2415CE"),
				"Provisions",
				GroupInstance,
				"1.1.1.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("B6FCD85A-B289-411e-B48A-A80B7848D8C0"),
				"Unearned revenue",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("17762042-5FC3-432a-B2EC-B8FD52F957A6"),
				"Donations",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("724713C0-2AB7-4e9c-8220-9EFCFD8DC362"),
				"Superannuation contributions",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("0330C3E7-A3CA-4767-B03C-1A7402973E85"),
				"Various adjustments - permanent",
				"-1",
				"1.1.1.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("ECB88EBB-0FD2-4255-9B11-FB1A2577B858"),
				"Various adjustments - timing",
				"-1",
				"1.1.1.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("48654912-2C61-473d-92CE-BD086A89F149"),
				"Foreign exchange gains and losses",
				GroupInstance,
				"1.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("EB5E3368-90BE-4483-A22A-872F91CE8D1E"),
				"Bad debts",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("EC96B495-DC01-4d22-8D1E-23F71DDFA02B"),
				"Tax offsets credits and instalments",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("F5BF6358-A6B7-4e92-81DF-6DCCFF6E12D5"),
				"Subscriptions",
				GroupInstance,
				"1.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("{01768745-06DF-4c84-873A-812129C686AB}"),
				"Consultancy fees",
				GroupInstance,
				"1.1.0.0"
				);

// Obselete Components
//			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
//				new Guid("5588C9FB-44B6-4559-A205-FF7A79BA791C"),
//				"Capital gains/losses",
//				GroupInstance,
//				"1.1.0.2"
//				);
//
//			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
//				new Guid("F3D730D6-3DCC-46be-8AD4-8F346F536551"),
//				"Uniform capital allowance",
//				GroupInstance,
//				"1.1.0.2"
//				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("3E2F8113-25AA-451b-8C33-74DF33FE5F59"),
				"Fixed assets",
				GroupInstance,
				"1.1.1.3"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("D2EF2D41-EC29-437c-95F1-CA0D8E0FA3C5"),
				"Foreign income",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("5A198D5F-EB0F-4e3d-93D1-E88B57810E83"),
				"Distributions received",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("5DE96952-FB8A-4561-9207-C74D32176BC7"),
				"Interest Expense",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("B19BCED6-9F26-4e2e-A1D5-47E8DBF6E7D0"),
				"Prepayments",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("2E0A06D3-2B32-40da-8056-F45D2D0FBDC8"),
				"Privilege Template",
				GroupInstance,
				"1.1.0.1"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("06EE8B65-EBE7-43bd-A404-288D5A00BA9D"),
				"Research and development",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("D3FA6258-5833-45e3-999E-C8B5FDFD0989"),
				"Validation Module",
				GroupInstance,
				"1.1.0.2"
				);
			
			#endregion
			#region Common Components

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("94F0B06B-505E-4194-9FA6-79E5244D779F"),
				"Broker Managed Component",
				GroupInstance,
				"1.1.1.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("3981982D-3D81-4419-B831-90B1C8254976"),
				"Calculation Module",
				GroupInstance,
				"1.1.0.3"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("17141A0D-42E2-4346-A9F7-16BCF76C193A"),
				"Chart Of Accounts",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("391B2230-F504-4f8a-8FA6-DFDFFA608E24"),
				"Tax Map",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("D5D34D91-5BD5-4ca5-8353-9478299E4188"),
				"Consolidation adjustment entity",
				GroupInstance,
				"1.1.0.1"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("CA65CF34-3EAC-40a7-B3BD-0B6E0FDA6892"),
				"Accounting records - Group",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("1EF5BD90-8999-4bd2-A976-79DD864FA332"),
				"Error handler",
				GroupInstance,
				"1.1.0.1"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("815C19AA-D48B-4ea4-BAAC-D045973AEA3F"),
				"Event",
				GroupInstance,
				"1.1.0.1"
				);
		
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("9E6A9C5F-A9B4-4d0e-AF98-7D0029B62421"),
				"File Import",
				GroupInstance,
				"1.1.0.0"
				);
		
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("196EED92-D604-4b07-BF49-C5C322400A05"),
				"Group",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("E15E5928-DC89-40f3-A047-73232A1608EE"),
				"Ledger Map",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("F9402117-5B8C-471f-B4B6-13DE7406BC91"),
				"Accounting records",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("9E6870FD-3F53-45c6-A398-FA5C20A3CB42"),
				"Nable Licence",
				GroupInstance,
				"1.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("DB489B76-49E4-4a6d-942C-9F59061080D8"),
				"Logical Module",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("E7A63C80-7FA8-4875-82E4-3643DF9AE6B3"),
				"Map Target",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("030EC843-59FA-488c-B75B-A82D52FCBA93"),
				"Organization Unit",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("59DFAAA8-FB81-492c-964D-967A2847ED83"),
				"Organization",
				GroupInstance,
				"1.1.1.1"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("FD5D3AFD-3745-40b5-B46F-9048C3EE4BA4"),
				"Period",
				GroupInstance,
				"1.1.0.3"
				);
		
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("939CC890-FA84-4b24-AC5C-3D178D7E7FB8"),
				"SAP Base",
				GroupInstance,
				"1.1.0.1"
				);
			#endregion
		
		}
		#endregion
	}
}
