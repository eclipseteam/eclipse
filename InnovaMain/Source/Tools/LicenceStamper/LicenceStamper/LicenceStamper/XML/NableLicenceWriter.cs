using System;
using System.IO;
using System.Xml;
using System.Diagnostics;
using System.Data;
using System.Text;
using System.Collections;
using Allume.Nable.NableLicence;


namespace Allume.Nable.Licence
{
	/// <summary>
	/// Summary description for XmlLicense.
	/// </summary>
	public class NableLicenceWriter
	{
		#region Member Variables -------------------------------------------------------
		private XmlDocument xmlDoc;
		private string NewLicenseFile;
		private string NableLicenseFile;
		private string nableProductType = string.Empty;

		#endregion

		#region Constants --------------------------------------------------------------
		private const string COMPONENT_LIST = "COMPONENT_LIST";
		private const string ALLUME = "Allume";
		private const string COMPONENT = "COMPONENT";
		private const string GROUPCOMPONENT = "GroupCOMPONENT";
		private const string COMPONENT_ID = "ComponentID";
		private const string DESCRIPTION  = "Description";
		private const string ALLOWED_INSTANCES = "AllowedInstances";
		private const string COMPONENT_VERSION = "ComponentVersion";
		private const string GROUP_NAME = "GroupName";
		private const string COMPONENT_GROUP = "Componentgroup";
		private const string GLOBAL = "GLOBAL";
		private const string NABLE_LICENCE = "NableLicense";
		private const string NABLE_PRODUCT_TYPE = "ProductType";
		private LicenceDS newLicenceDS;
		#endregion

		#region Main Constructor -------------------------------------------------------
		public NableLicenceWriter()
		{
			xmlDoc = new XmlDocument();
			NewLicenseFile = string.Empty;
			NableLicenseFile  = string.Empty;
			newLicenceDS = new LicenceDS();
		}

		public NableLicenceWriter(LicenceDS licenceds)
		{
			xmlDoc = new XmlDocument();
			NewLicenseFile = string.Empty;
			NableLicenseFile  = string.Empty;
			newLicenceDS = new LicenceDS();

			newLicenceDS = licenceds;
		}
		#endregion

		#region Public methods - Create XML Document -----------------------------------
		public LicenceDS LicenceContent
		{
			get
			{
				return this.newLicenceDS;
			}
			set
			{
				this.newLicenceDS = value;
			}
		}
		
		public string ProductType
		{
			get
			{
				return this.nableProductType;
			}
			set
			{
				this.nableProductType = value;
			}
		}
		
		public void CreateLicenseContent(string XMLFileName)
		{

			// Create XML File
			XmlTextWriter xw = new XmlTextWriter( XMLFileName, Encoding.UTF8  );

			// Write the opening xml
			xw.WriteStartDocument();
			
			xw.Indentation =5;
			xw.Formatting = Formatting.Indented;
			xw.WriteStartElement(NABLE_LICENCE);

			this.CreateAllumeSection(xw);
			
			// End of NableLicense
			xw.WriteEndElement();

			// End of Document
			xw.WriteEndDocument();

			if ( xw != null )
			{
				xw.Close();
			}
		}
		
		private XmlTextWriter CreateAllumeSection(XmlTextWriter xw)
		{
			LicenceDS licenceds = this.LicenceContent;
			// Allume Section
			xw.WriteStartElement(ALLUME);
			
			xw = this.CreateGlobalInformaton(xw,licenceds); 
			xw = this.CreateAllumeLicensedComponentList(xw,licenceds);
            		
			// Close the Allume node
			xw.WriteEndElement();

			return xw;
		}
		

		private XmlTextWriter CreateAllumeLicensedIndividualComponent(XmlTextWriter xw, LicenceDS licenceds)
		{

			foreach(DataRow drow  in licenceds.Tables[LicenceDS.CG_TABLE].Rows)
			{
				// Write the XML Name/Value Node to the xml file
				xw.WriteStartElement(COMPONENT);
				xw.WriteAttributeString(COMPONENT_ID, drow[LicenceDS.CG_NAME_FLD].ToString());
				xw.WriteAttributeString(DESCRIPTION, "-1");
				xw.WriteAttributeString(ALLOWED_INSTANCES, drow[LicenceDS.CG_ALLOWEDINSTANCES_FLD].ToString());
				xw.WriteAttributeString(COMPONENT_VERSION, "-1");
				// End COMPONENT
				xw.WriteEndElement();
			}
			return xw;
		}

		private XmlTextWriter CreateAllumeLicensedComponentList(XmlTextWriter xw, LicenceDS licenceds )
		{
			// Write the XML Name/Value Node to the xml file
			xw.WriteStartElement(COMPONENT_LIST);

			foreach(DataRow drow  in licenceds.Tables[LicenceDS.CG_TABLE].Rows)
			{
				xw.WriteStartElement(COMPONENT_GROUP);
				xw.WriteAttributeString(GROUP_NAME, drow[LicenceDS.CG_NAME_FLD].ToString());
				xw.WriteAttributeString(ALLOWED_INSTANCES, drow[LicenceDS.CG_ALLOWEDINSTANCES_FLD].ToString());

				DataRow[] filteredRowByGroup = licenceds.Tables[LicenceDS.LICENSECOMPONENT_GROUPEDTABLE].Select(LicenceDS.LICENSECOMPONENT_GROUPNAME_FLD+"='"+drow[LicenceDS.CG_NAME_FLD].ToString()+"'");
				
				foreach(DataRow drow2  in filteredRowByGroup)
				{
					xw.WriteStartElement(GROUPCOMPONENT);

					xw.WriteAttributeString(COMPONENT_ID, drow2[LicenceDS.LICENSECOMPONENT_COMPONENTID_FLD].ToString());
					xw.WriteAttributeString(DESCRIPTION, drow2[LicenceDS.LICENSECOMPONENT_DISCRIPTION_FLD].ToString());
					xw.WriteAttributeString(ALLOWED_INSTANCES, drow2[LicenceDS.LICENSECOMPONENT_ALLOWEDINSTANCES_FLD].ToString());
					xw.WriteAttributeString(COMPONENT_VERSION, drow2[LicenceDS.LICENSECOMPONENT_COMPONENTVERSION_FLD].ToString());
					// End Componentgroup
					xw.WriteEndElement();
				}
				
				// End Componentgroup
				xw.WriteEndElement();
			}
			// End COMPONENT_LIST
			xw.WriteEndElement();
			return xw;
		}

		private XmlTextWriter CreateGlobalInformaton(XmlTextWriter xw, LicenceDS licenceds )
		{
			
			DataRow drow = licenceds.Tables[LicenceDS.GLOBAL_TABLE].Rows[0];

			// Write the XML Name/Value Node to the xml file
			xw.WriteStartElement(GLOBAL);

			// Product Type
			xw.WriteStartElement(NABLE_PRODUCT_TYPE);
			xw.WriteString(this.ProductType);
			xw.WriteEndElement();

			if (drow[LicenceDS.GLOBAL_LICENCE_GUID_FLD].Equals(Guid.Empty))
			{
				xw.WriteStartElement(LicenceDS.GLOBAL_LICENCE_GUID_FLD);
				xw.WriteString(string.Empty);
				xw.WriteEndElement();
			}
			else
			{
				xw.WriteStartElement(LicenceDS.GLOBAL_LICENCE_GUID_FLD);
				xw.WriteString(drow[LicenceDS.GLOBAL_LICENCE_GUID_FLD].ToString() );
				xw.WriteEndElement();
			}
			
			xw.WriteStartElement(LicenceDS.GLOBAL_NABLE_VERSION_FLD);
			xw.WriteString(drow[LicenceDS.GLOBAL_NABLE_VERSION_FLD].ToString() );
			xw.WriteEndElement();
			
			xw.WriteStartElement(LicenceDS.GLOBAL_NAME_FLD);
			xw.WriteString(drow[LicenceDS.GLOBAL_NAME_FLD].ToString());
			xw.WriteEndElement();

			xw.WriteStartElement(LicenceDS.GLOBAL_ADDRESS_FLD);
			xw.WriteString(drow[LicenceDS.GLOBAL_ADDRESS_FLD].ToString());
			xw.WriteEndElement();

			xw.WriteStartElement(LicenceDS.GLOBAL_ABN_FLD);
			xw.WriteString(drow[LicenceDS.GLOBAL_ABN_FLD].ToString());
			xw.WriteEndElement();

			xw.WriteStartElement(LicenceDS.GLOBAL_ACN_FLD);
			xw.WriteString(drow[LicenceDS.GLOBAL_ACN_FLD].ToString());
			xw.WriteEndElement();

			xw.WriteStartElement(LicenceDS.GLOBAL_TIMELENGTH_FLD);
			xw.WriteString(drow[LicenceDS.GLOBAL_TIMELENGTH_FLD].ToString());
			xw.WriteEndElement();

			xw.WriteStartElement(LicenceDS.GLOBAL_LICENSEDATE_FLD);
			xw.WriteString(drow[LicenceDS.GLOBAL_LICENSEDATE_FLD].ToString());
			xw.WriteEndElement();

			xw.WriteStartElement(LicenceDS.GLOBAL_PERIOD_ENDDATE_FLD);
			xw.WriteString(drow[LicenceDS.GLOBAL_PERIOD_ENDDATE_FLD].ToString());
			xw.WriteEndElement();

			xw.WriteStartElement(LicenceDS.GLOBAL_EXPIRYDATE_FLD);
			xw.WriteString(drow[LicenceDS.GLOBAL_EXPIRYDATE_FLD].ToString());
			xw.WriteEndElement();

			xw.WriteStartElement(LicenceDS.GLOBAL_MAXORGANISATION_FLD);
			xw.WriteString(drow[LicenceDS.GLOBAL_MAXORGANISATION_FLD].ToString());
			xw.WriteEndElement();

			xw.WriteStartElement(LicenceDS.GLOBAL_LICENCE_SCENARIOS_FLD );
			xw.WriteString(drow[LicenceDS.GLOBAL_LICENCE_SCENARIOS_FLD].ToString());
			xw.WriteEndElement();

			xw.WriteStartElement(LicenceDS.GLOBAL_LICENCE_IMPORT_LEDGER_FLD);
			xw.WriteString(drow[LicenceDS.GLOBAL_LICENCE_IMPORT_LEDGER_FLD].ToString());
			xw.WriteEndElement();
			
			// End Global Seciton
			xw.WriteEndElement();

			return xw;
		}

		public XmlDocument LoadNableLicenseFile(string LicenseFileName)
		{
			// 

			return null;
		}

		#endregion
		
		#region Private methods - XML Section ------------------------------------------
		

		private XmlAttribute XmlAttribute(string attributeName, string attributeValue)
		{
			XmlAttribute xmlAttrib = xmlDoc.CreateAttribute(attributeName);
			xmlAttrib.Value = FilterXMLString(attributeValue);
			return xmlAttrib;
		}

		private XmlElement XmlElement(string elementName, string elementValue)
		{
			XmlElement xmlElement = xmlDoc.CreateElement(elementName);
			xmlElement.Attributes.Append(XmlAttribute("name", FilterXMLString(elementValue)));
			return xmlElement;
		}

		private string FilterXMLString(string inputString)
		{
			string returnString = inputString;
			if (inputString.IndexOf("&") > 0)
				returnString = inputString.Replace("&","&amp;");

			if (inputString.IndexOf("'") > 0)
				returnString = inputString.Replace("'","&apos;");
		
			return returnString;
		}	

		#endregion

		#region Digitally Sign the License File ----------------------------------------
		
		public string DigitallySignLicenseFile(string NableLicenseFile, out string HashKey)
		{
			string _messageResult = string.Empty;
			DigitallySign digitallySign = new DigitallySign();
			digitallySign.SignFile(NableLicenseFile,out _messageResult);
			HashKey = digitallySign.GetHashKey;
			return _messageResult;
		}
		#endregion
	}
}
