using System;
using Oritax.TaxSimp.TaxSimpLicence; 

namespace Oritax.TaxSimp.Licence
{
	internal class NableFormsKitLicenseFile: NableLicenseFile
	{
		
		#region Main Constructor -------------------------------------------------------
		public NableFormsKitLicenseFile()
		{
		
		}
		public NableFormsKitLicenseFile(out LicenceDS Licence):base(out Licence)
		{
		
		}
		#endregion 
		#region Component Licensing ----------------------------------------------------
		
		protected override void AddComponentsList()
		{
			#region Modules Section
			// Setup Contants
			const string GroupName		= "Modules";
			const string GroupInstance	= "-1";

			// Add the Modules Group
			this.licenceDS.AddComponentGroupRow(GroupName,GroupInstance);  

			#endregion
			#region Forms Kit Required Components
			
			// ATO Forms 2003
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("5B144E3D-EE2F-4804-A01B-D219A7F227E5"),
				"ATO Forms",
				GroupInstance,
				"1.1.0.2"
				);

			// ATO Forms 2004
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("5B144E3D-EE2F-4804-A01B-D219A7F227E5"),
				"ATO Forms",
				"-1",
				"2.1.0.2"
				);

			// ATO Forms 2005
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("5B144E3D-EE2F-4804-A01B-D219A7F227E5"),
				"ATO Forms",
				"-1",
				"3.1.0.0"
				);

			// ATO Forms 2006
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("5B144E3D-EE2F-4804-A01B-D219A7F227E5"),
				"ATO Forms",
				"-1",
				"4.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("0330C3E7-A3CA-4767-B03C-1A7402973E85"),
				"Various adjustments - permanent",
				GroupInstance,
				"1.1.1.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("4E93E44C-B609-4b33-86C6-D778AFBDFBA1"),
				"Income tax calculation",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("EC96B495-DC01-4d22-8D1E-23F71DDFA02B"),
				"Tax offsets credits and instalments",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("D3FA6258-5833-45e3-999E-C8B5FDFD0989"),
				"Validation Module",
				GroupInstance,
				"1.1.0.2"
				);

			#endregion
			#region Common Components

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("94F0B06B-505E-4194-9FA6-79E5244D779F"),
				"Broker Managed Component",
				GroupInstance,
				"1.1.1.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("3981982D-3D81-4419-B831-90B1C8254976"),
				"Calculation Module",
				GroupInstance,
				"1.1.0.3"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("17141A0D-42E2-4346-A9F7-16BCF76C193A"),
				"Chart Of Accounts",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("391B2230-F504-4f8a-8FA6-DFDFFA608E24"),
				"Tax Map",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("D5D34D91-5BD5-4ca5-8353-9478299E4188"),
				"Consolidation adjustment entity",
				GroupInstance,
				"1.1.0.1"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("CA65CF34-3EAC-40a7-B3BD-0B6E0FDA6892"),
				"Accounting records - Group",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("1EF5BD90-8999-4bd2-A976-79DD864FA332"),
				"Error handler",
				GroupInstance,
				"1.1.0.1"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("815C19AA-D48B-4ea4-BAAC-D045973AEA3F"),
				"Event",
				GroupInstance,
				"1.1.0.1"
				);
			
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("9E6A9C5F-A9B4-4d0e-AF98-7D0029B62421"),
				"File Import",
				GroupInstance,
				"1.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("196EED92-D604-4b07-BF49-C5C322400A05"),
				"Group",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("E15E5928-DC89-40f3-A047-73232A1608EE"),
				"Ledger Map",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("F9402117-5B8C-471f-B4B6-13DE7406BC91"),
				"Accounting records",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("9E6870FD-3F53-45c6-A398-FA5C20A3CB42"),
				"Nable Licence",
				GroupInstance,
				"1.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("DB489B76-49E4-4a6d-942C-9F59061080D8"),
				"Logical Module",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("E7A63C80-7FA8-4875-82E4-3643DF9AE6B3"),
				"Map Target",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("030EC843-59FA-488c-B75B-A82D52FCBA93"),
				"Organization Unit",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("59DFAAA8-FB81-492c-964D-967A2847ED83"),
				"Organization",
				GroupInstance,
				"1.1.1.1"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("FD5D3AFD-3745-40b5-B46F-9048C3EE4BA4"),
				"Period",
				GroupInstance,
				"1.1.0.3"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("939CC890-FA84-4b24-AC5C-3D178D7E7FB8"),
				"SAP Base",
				GroupInstance,
				"1.1.0.1"
				);
			
			#endregion
		
		}
		#endregion
	}
}
