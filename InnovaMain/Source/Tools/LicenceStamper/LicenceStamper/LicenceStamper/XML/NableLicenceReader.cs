using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Collections;
using System.Security.Cryptography; 
using Allume.Nable.NableLicence;
using DTT.TX360Enterprise.Utilities;

namespace Allume.Nable.Licence
{
	public class NableLicenceReader
	{
		#region Member Variables -------------------------------------------------------
		private XmlDocument xmlDoc;
		private LicenceDS licenceds;
		private string NewLicenseFile;
		private string NableLicenseFile;
		private string nableProductType = string.Empty;

		#endregion

		#region Constants --------------------------------------------------------------
		private const string COMPONENT_LIST = "COMPONENT_LIST";
		private const string ALLUME = "Allume";
		private const string COMPONENT = "COMPONENT";
		private const string GROUPCOMPONENT = "GroupCOMPONENT";
		private const string COMPONENT_ID = "ComponentID";
		private const string DESCRIPTION  = "Description";
		private const string ALLOWED_INSTANCES = "AllowedInstances";
		private const string COMPONENT_VERSION = "ComponentVersion";
		private const string GROUP_NAME = "GroupName";
		private const string COMPONENT_GROUP = "Componentgroup";
		private const string GLOBAL = "GLOBAL";
		private const string NABLE_LICENCE = "NableLicense";
		private const int	 DEMO_PERIOD		= 15;
		private const string ENTITY			= "Entity";
		private const string GROUP			= "Group";
		private const string SCENARIOS		= "Scenarios";
		private const string USERACCOUNTS	= "UserAccounts";
		private const string MODULES = "Modules";
		private const string TEA = "TEA";
		private const string LEDGER = "Ledger";
		private const string PERIODS = "Periods";
		private const int	 UNLIMITED = -1;
		private const int	 NOTALLOWED = 0;
		private const string LICENCE = "Licence";
		private const string NABLE_PRODUCT_TYPE = "ProductType";
		private LicenceDS newLicenceDS;
		#endregion

		#region Main Constructor -------------------------------------------------------
		public NableLicenceReader()
		{
			xmlDoc = new XmlDocument();
			NewLicenseFile = string.Empty;
			NableLicenseFile  = string.Empty;
			newLicenceDS = new LicenceDS();
		}

		public NableLicenceReader(LicenceDS licenceds)
		{
			xmlDoc = new XmlDocument();
			NewLicenseFile = string.Empty;
			NableLicenseFile  = string.Empty;
			newLicenceDS = new LicenceDS();

			newLicenceDS = licenceds;
		}
		#endregion

		#region Public Methods ---------------------------------------------------------

		public LicenceDS LicenceContent
		{
			get{ return this.licenceds; }
			set{ this.licenceds = value; }
		}
		
		public string ProductType
		{
			get
			{
				return this.nableProductType;
			}
			set
			{
				this.nableProductType = value;
			}
		}
	
		public void ReadLicenseContent(string XMLFileName)
		{
			StreamReader streamReader = new StreamReader(XMLFileName,true);
			string fileContent =  validateLicenceHash(streamReader.ReadToEnd().ToString());

			// Check for <md5> ... tags in the file content

			int licenseStart = fileContent.IndexOf("<md5>");
			int licenseFinish = fileContent.IndexOf("</md5>", 0);

			if (!(licenseStart == -1 || licenseFinish == -1))
				fileContent =validateLicenceHash(fileContent);


			licenceds = new LicenceDS();
			XmlDocument xmldoc = new XmlDocument();
			xmldoc.LoadXml(fileContent); 
				
			PopulateLicenceFromXML(xmldoc.DocumentElement); 

			streamReader.Close();
		}

		#endregion

		#region Private Methods and properties -----------------------------------------

		#region Populate Licence from xml node - recursively
		private void PopulateLicenceFromXML(XmlNode node)
		{
			// End recursion if the node is a text type
			if(node == null || node.NodeType == XmlNodeType.Text || node.NodeType == XmlNodeType.CDATA)
				return;

			// Load the Global Section
			if(node.Name.Equals(GLOBAL))
				GlobalInformation( node );

			// Load the Component Groups and Component lists
			if(node.Name.Equals(COMPONENT_LIST))
				GroupedComponents(node); 

			// Add all the children of the current node to the treeview
			foreach(XmlNode childnode in node.ChildNodes)
				PopulateLicenceFromXML(childnode);
		}

		#endregion

		#region Add grouped component
		private void AddGroupedComponent(XmlNode node)
		{
			string GroupName =string.Empty;
			string GroupInstance =  string.Empty;

			if(node.Attributes.Count >0)
			{
				XmlAttributeCollection theAttrib = node.Attributes;

				foreach(XmlAttribute attrib in theAttrib)
				{
					if(attrib.Name.Equals(LicenceDS.LICENSECOMPONENT_GROUPNAME_FLD))
						GroupName = attrib.InnerText.ToString();
					
					if(attrib.Name.Equals(LicenceDS.LICENSECOMPONENT_ALLOWEDINSTANCES_FLD))
						GroupInstance = attrib.InnerText.ToString();

				}
				// Add the Group Information
				licenceds.AddComponentGroupRow(GroupName,GroupInstance); 
			}

			if(node.ChildNodes.Count>0)
			{
				foreach(XmlNode childnode in node.ChildNodes)
				{

					if (childnode.Attributes.Count >0)
					{
						XmlAttributeCollection theAttrib = childnode.Attributes;//xn.ChildNodes[0].Attributes;

						Hashtable ht = new Hashtable();

						foreach(XmlAttribute attrib in theAttrib)
							ht.Add(attrib.Name.ToString(),attrib.InnerText.ToString());

						string Desription = ht[LicenceDS.LICENSECOMPONENT_DISCRIPTION_FLD].ToString();
						Guid ComponentID = new Guid(ht[LicenceDS.LICENSECOMPONENT_COMPONENTID_FLD].ToString());
						string AllowedInstances = ht[LicenceDS.LICENSECOMPONENT_ALLOWEDINSTANCES_FLD].ToString();
						string ComponentVersion = ht[LicenceDS.LICENSECOMPONENT_COMPONENTVERSION_FLD].ToString();

						licenceds.AddGroupedComponentInfoRow(
							GroupName
							,ComponentID
							,Desription
							,AllowedInstances
							,ComponentVersion);

					}
				}
			}
		}
		
		#endregion

		#region Add Component List

		private void AddComponentList(XmlNode node)
		{
			if(node.Attributes.Count >0)
			{
				XmlAttributeCollection theAttrib = node.Attributes;

				Hashtable ht = new Hashtable();

				foreach(XmlAttribute attrib in theAttrib)
					ht.Add(attrib.Name.ToString(),attrib.InnerText.ToString());

				string Description = ht[LicenceDS.LICENSECOMPONENT_DISCRIPTION_FLD].ToString();
				Guid ComponentID = new Guid(ht[LicenceDS.LICENSECOMPONENT_COMPONENTID_FLD].ToString());
				string AllowedInstances = ht[LicenceDS.LICENSECOMPONENT_ALLOWEDINSTANCES_FLD].ToString();
				string ComponentVersion = ht[LicenceDS.LICENSECOMPONENT_COMPONENTVERSION_FLD].ToString();

				licenceds.AddComponentListRow(ComponentID,Description,AllowedInstances ,ComponentVersion); 
			}
		}

		#endregion

		#region Grouped Components
		private void GroupedComponents(XmlNode node)
		{
			foreach( XmlNode childnode in node.ChildNodes)
			{
				if(childnode.Name.Equals(COMPONENT_GROUP))
					AddGroupedComponent(childnode);
				
				if(childnode.Name.Equals(COMPONENT))
					AddComponentList(childnode);
			}
		}
		#endregion

		#region Private Global Information
		private void GlobalInformation(XmlNode node)
		{
			Hashtable ht = new Hashtable();

			foreach( XmlNode childnode in node.ChildNodes)
			{
				ht.Add(childnode.Name.ToString(),string.Empty);

				if(childnode.ChildNodes.Count>0)
				{
					XmlNode tmpChildNode =  childnode.ChildNodes[0];
					ht[childnode.Name.ToString()] = tmpChildNode.InnerText.ToString();
				}
			}
		
			// Get the Product Type
			if(ht.ContainsKey(NABLE_PRODUCT_TYPE))
			{
				if(!ht[NABLE_PRODUCT_TYPE].ToString().Equals(null))
				{
					ProductType = ht[NABLE_PRODUCT_TYPE].ToString();
				}
				else
				{
					ProductType = string.Empty;
				}
			}
			else
			{
				ProductType = string.Empty;
			}

			// Determine the licence Type
			
			LicenceDS.LicenceType licencetype;
			if (!ht[LicenceDS.GLOBAL_LICENCE_GUID_FLD].ToString().Equals(string.Empty))
			{
				licenceds.LicenceGUID = new Guid(ht[LicenceDS.GLOBAL_LICENCE_GUID_FLD].ToString());
				licencetype = LicenceDS.LicenceType.Registered;
			}
			else
			{
				licencetype = LicenceDS.LicenceType.Demo;  
			}

			licenceds.AddGlobalInfoRow(
				ht[LicenceDS.GLOBAL_NAME_FLD].ToString(),
				ht[LicenceDS.GLOBAL_ADDRESS_FLD].ToString(),
				ht[LicenceDS.GLOBAL_ABN_FLD].ToString(),
				ht[LicenceDS.GLOBAL_ACN_FLD].ToString(),
				ht[LicenceDS.GLOBAL_TIMELENGTH_FLD].ToString(),
				ht[LicenceDS.GLOBAL_LICENSEDATE_FLD].ToString(),
				ht[LicenceDS.GLOBAL_EXPIRYDATE_FLD].ToString(),
				ht[LicenceDS.GLOBAL_MAXORGANISATION_FLD].ToString(),
				ht[LicenceDS.GLOBAL_PERIOD_ENDDATE_FLD].ToString(),
				licencetype.ToString(),
				ht[LicenceDS.GLOBAL_NABLE_VERSION_FLD].ToString(),
				ht[LicenceDS.GLOBAL_LICENCE_SCENARIOS_FLD].ToString(),
				bool.Parse(ht[LicenceDS.GLOBAL_LICENCE_IMPORT_LEDGER_FLD].ToString())
				
				);
		}

		#endregion

		#region MD5 Hash validation
		private string validateLicenceHash(string fileText)
		{
			validateHash(fileText,LICENCE);
			
			int licenseStart = fileText.IndexOf("<md5>");
			int licenseFinish = fileText.IndexOf("</md5>", 0);

			if (!(licenseStart == -1 || licenseFinish == -1))
				fileText = fileText.Substring(0, licenseStart);
			
			
			return fileText;

		}

		private void validateHash(string fileText, string type)
		{
			MessageBoxDefinition licenceValidationMessage = null;

			int licenseStart = fileText.IndexOf("<md5>");
			int licenseFinish = fileText.IndexOf("</md5>", 0);
		
			// Make sure that the MD5 tags are found
			if (!(licenseStart == -1 || licenseFinish == -1))
			{
				string md5String = fileText.Substring(licenseStart + 5, licenseFinish - licenseStart - 5);
				fileText = fileText.Substring(0, licenseStart);
				
				try
				{
					md5String = Encryption.DecryptData(md5String);
					
					if(!createMD5(fileText.Trim()).Equals(md5String))
							licenceValidationMessage = new  MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_INVALID);
				}
				catch
				{
					// put some error handling code
				}
			}
		
		}

		private string createMD5(string text)
		{
			MD5 md5 = new MD5CryptoServiceProvider();
			Encoding encoding = Encoding.ASCII;
			
			// Generate MD5 Hash
			byte[] result = md5.ComputeHash(encoding.GetBytes(text.Trim()));
			return encoding.GetString(result);
		}
		#endregion

		#endregion

	}
}
