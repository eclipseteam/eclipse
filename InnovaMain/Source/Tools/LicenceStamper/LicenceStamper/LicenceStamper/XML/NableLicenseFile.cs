using System;
using Oritax.TaxSimp.TaxSimpLicence; 

namespace Oritax.TaxSimp.Licence
{
	
	public enum LicenceType
	{
		Demo,
		Registered
	}

	internal class NableLicenseFile
	{

		#region Constants --------------------------------------------------------------
		private const string GLOBAL_INFO_ORGANISATIONMAX	= "1";
		private const string GLOBAL_INFO_SCENARIOS			= "-1"; // Unlimited
		private const bool   GLOBAL_INFO_IMPORT_LEDGER		= true;
		#endregion

		#region Member Variables -------------------------------------------------------
		protected LicenceDS licenceDS = new LicenceDS();
		private string GLOBAL_INFO_LICENSEDATE				= DateTime.Now.ToString();
		private string GLOBAL_INFO_LICENSEEXPIRYDATE		= DateTime.Now.AddDays(16).ToString();


		private string _companyName = string.Empty;
		private string _companyAddress = string.Empty;
		private string _companyABN = string.Empty;
		private string _companyACN = string.Empty;
		private bool   _importLedger = false;
		private bool   _tea = false;
		private string _organisationMax = "1";
		private string _licenceExpiryDate = string.Empty;
		private string _periodEndDate = string.Empty;
		private string _licenceDate = string.Empty;
		private bool   _isDemoLicence = false;
		private Guid _installationID = Guid.Empty;
		private int _scenarios = 0;
		private string _licencetype = string.Empty;
		private int _numberofentities = 0;
		private int _numberofgroups = 0;
		private int _numberofuseraccounts = 0;
		private int _numberofStrandardFiles = -1;
		private string productType = string.Empty; 


		#endregion

		#region Main Constructor -------------------------------------------------------
		public NableLicenseFile()
		{
		}
		public NableLicenseFile(out LicenceDS Licence)
		{
			Licence=this.NableLicenseData; 
		}

		public LicenceDS NableLicenseData
		{
			get
			{
				this.CreateNableLicenseData();
				return this.licenceDS;
			}
		}

	
		#endregion

		#region Public properties ------------------------------------------------------
		public string ProductType{get{return productType;}set{productType = value; }} 
		public  string _NableVersion{get { return NableConstants.GLOBAL_INFO_NABLEVERSION;}}
		public  string _CompanyName {get{return this._companyName;}set{this._companyName =value;}} 
		public  string _CompanyABN {get{return this._companyABN;}set{this._companyABN =value;}} 
		public  string _CompanyACN {get{return this._companyACN;}set{this._companyACN =value;}} 
		public  string _CompanyAddress {get{return this._companyAddress;}set{this._companyAddress =value;}} 
		public  bool _ImportLedger {get{return this._importLedger;}set{this._importLedger =value;}} 
		public  bool _TEA {get{return this._tea;}set{this._tea =value;}} 
		public  string _OrganisationMAX {get{return this._organisationMax;}set{this._organisationMax =value;}} 
		public  string _PeriodEndDate {get{return this._periodEndDate;}set{this._periodEndDate =value;}} 
		public  string _LicenceExpireDate {get{return this._licenceExpiryDate;}set{this._licenceExpiryDate =value;}} 
		public  string _LicenceDate {get{return this._licenceDate;}set{this._licenceDate =value;}} 
		public  Guid _InstallationID {get{return this._installationID;}set{this._installationID =value;}} 
		public  bool _IsDEMO{get{return this._isDemoLicence;}set{this._isDemoLicence =value;}} 
		public  int _NumberScenarios {get{return this._scenarios;}set{this._scenarios =value;}} 
		public  string _LicenceType {get{return this._licencetype;}set{this._licencetype =value;}} 

		public  int _NumberOfEntities {get{return this._numberofentities;}set{this._numberofentities =value;}} 
		public  int _NumberOfGroups {get{return this._numberofgroups;}set{this._numberofgroups =value;}} 
		public  int _NumberOfUserAccounts {get{return this._numberofuseraccounts;}set{this._numberofuseraccounts =value;}} 

		#endregion

		#region Nable License File -----------------------------------------------------

		protected virtual void CreateNableLicenseData()
		{
			// for now this release 2005.1
			this._OrganisationMAX  = GLOBAL_INFO_ORGANISATIONMAX;

			if(_IsDEMO)
			{
				this.licenceDS.LicenceGUID  = Guid.Empty;
				this._LicenceType = LicenceType.Demo.ToString(); 
			}
			else
			{
				this.licenceDS.LicenceGUID = new Guid(this._InstallationID.ToString());
				this._LicenceType = LicenceType.Registered.ToString(); 
			}


			// the Global Information - Nable being Licensed to
			this.licenceDS.AddGlobalInfoRow(
				this._CompanyName,
				this._CompanyAddress,
				this._CompanyABN,
				this._CompanyACN,
				string.Empty,
				this._LicenceDate,
				this._LicenceExpireDate,
				this._OrganisationMAX ,
				this._PeriodEndDate ,
				this._LicenceType,
				NableConstants.GLOBAL_INFO_NABLEVERSION,
				this._NumberScenarios.ToString() ,
				this._ImportLedger
				);

			this.AddEntityGroup();
			this.AddGroupStructureGroup();
			this.AddTeaComponents(); 
			this.AddUserAccounts();
			this.AddComponentsList();
			this.AddStandardFileComponent();
		}
		#endregion

		#region Add Component Groups to the license file -------------------------------

		protected virtual void AddStandardFileComponent()
		{
			// Setup Contants
			const string StandardFilesName	= "StandardFiles";
			
			// Add the Entity Group
			this.licenceDS.AddComponentGroupRow(StandardFilesName,this._numberofStrandardFiles.ToString() );  

			if(this.ProductType == frmCreate.PRODUCT_TYPE_NTMS)
			//Add NTMS license component for StandardFiles
				AddStandardFilesForNTMS(StandardFilesName);
			//Add NTRS/NTPS license component for StanddardFiles
			else
				AddStandardFilesForNTRSNTCSNTPS(StandardFilesName); 
		}

		private void AddStandardFilesForNTRSNTCSNTPS(string StandardFilesName)
		{
			this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
				new Guid("8906B440-E389-429a-9F5D-2BD0DEF8C4E2"),
				"Short CL",
				"-1",
				"1.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
				new Guid("63650340-74A5-41d5-AEF6-A8C796B4FAD5"),
				"Full CL",
				"0",
				"1.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
				new Guid("236BB323-A73D-41a9-A15B-B5324A2B0997"),
				"IL",
				"-1",
				"1.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
                new Guid("DE856ADF-36BB-4b85-9FE8-4D18321F830F"),
				"Tax Short Map 2007.2",
				"-1",
				"1.1.0.0"
				);

            this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
                new Guid("17F17EB7-227B-4823-8720-210C9253D190"),
                "Tax Short Map 2008.1",
                "-1",
                "1.1.0.0"
                );

            this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
            new Guid("1C5D67A9-EF6B-4d67-9F15-BEB1E478E83B"),
            "Tax Short Map",
            "-1",
            "1.1.0.0"
            );

			this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
				new Guid("5689277D-D301-4090-B9B9-0D205670D067"),
				"Tax Map",
				"0",
				"1.1.0.0"
				);

            this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
            new Guid("1DA33DD1-60D8-463d-ABD0-8AD4315AC4B6"),
            "Tax Map 2007.2",
            "0",
            "1.1.0.0"
            );

             this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
            new Guid("391C8728-CCAE-473c-A651-741178F3E533"),
            "Tax Map 2008.1",
            "0",
            "1.1.0.0"
            );

           	this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
				new Guid("4AD8035C-224B-474a-9DC3-D3C002637268"),
				"Ledger Map",
				"-1",
				"1.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
				new Guid("F33A2142-9CAA-4aa6-B6A3-07AB68FD96A8"),
				"Target Set",
				"-1",
				"1.1.0.0"
				);
           
           this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
            new Guid("4440DFD7-CC67-4051-AE17-5F54419EAB54"),
            "Target Set 2007.2",
            "-1",
            "1.1.0.0"
            );

           this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
           new Guid("8BBDB4D7-A179-49f7-8D9B-207751188817"),
           "Target Set 2008.1",
           "-1",
           "1.1.0.0"
           );

		}


        private void AddStandardFilesForNTMS(string StandardFilesName)
        {
            this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
                new Guid("8906B440-E389-429a-9F5D-2BD0DEF8C4E2"),
                "Short CL",
                "0",
                "1.1.0.0"
                );

            this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
                new Guid("63650340-74A5-41d5-AEF6-A8C796B4FAD5"),
                "Full CL",
                "-1",
                "1.1.0.0"
                );

            this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
                new Guid("236BB323-A73D-41a9-A15B-B5324A2B0997"),
                "IL",
                "0",
                "1.1.0.0"
                );

            this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
                new Guid("1C5D67A9-EF6B-4d67-9F15-BEB1E478E83B"),
                "Tax Short Map",
                "0",
                "1.1.0.0"
                );

            this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
            new Guid("DE856ADF-36BB-4b85-9FE8-4D18321F830F"),
            "Tax Short Map 2007.2",
            "0",
            "1.1.0.0"
            );

            this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
                new Guid("5689277D-D301-4090-B9B9-0D205670D067"),
                "Tax Map",
                "-1",
                "1.1.0.0"
                );

            this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
                new Guid("4AD8035C-224B-474a-9DC3-D3C002637268"),
                "Ledger Map",
                "0",
                "1.1.0.0"
                );

            this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
                new Guid("F33A2142-9CAA-4aa6-B6A3-07AB68FD96A8"),
                "Target Set",
                "-1",
                "1.1.0.0"
                );

            this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
           new Guid("4440DFD7-CC67-4051-AE17-5F54419EAB54"),
           "Target Set 2007.2",
           "-1",
           "1.1.0.0"
           );

            this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
          new Guid("1DA33DD1-60D8-463d-ABD0-8AD4315AC4B6"),
          "Tax Map 2007.2",
          "-1",
          "1.1.0.0"
          );
            this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
         new Guid("8BBDB4D7-A179-49f7-8D9B-207751188817"),
         "Target Set 2008.1",
         "-1",
         "1.1.0.0"
         );

            this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
            new Guid("391C8728-CCAE-473c-A651-741178F3E533"),
            "Tax Map 2008.1",
            "-1",
            "1.1.0.0"
            );

            this.licenceDS.AddGroupedComponentInfoRow(StandardFilesName,
                new Guid("17F17EB7-227B-4823-8720-210C9253D190"),
                "Tax Short Map 2008.1",
                "0",
                "1.1.0.0"
                );
        }


		protected virtual void AddGroupStructureGroup()
		{
			// Setup Contants
			const string GroupName		= "Group";
			const string GroupInstance	= "-1";

			// Add the Entity Group
			this.licenceDS.AddComponentGroupRow(GroupName,this._NumberOfGroups.ToString() );  


			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("900EBA20-CB50-4797-BCBD-2D3D42D1D993"),
				"Australian group",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("A69D1EA1-10B6-498a-A9A7-BB23CEB4C7CE"),
				"Australian Sub-group",
				GroupInstance,
				"1.1.0.1"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("AE9FE16C-F731-4d04-8CF2-F6A18C362EDE"),
				"Australian Ultimate tax group" ,
				GroupInstance,
				"1.1.0.1"
				);		

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("ED763091-228C-48e6-94A4-487CA66F3743"),
				"Australian Entity - Divisional group",
				GroupInstance,
				"1.1.0.2"
				);

		}

		private void AddEntityGroup()
		{
			// Setup Contants
			const string GroupName		= "Entity";
			string GroupInstance	= "-1";

			// Add the Entity Group
			this.licenceDS.AddComponentGroupRow(GroupName,this._NumberOfEntities.ToString());  

			
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
						new Guid("4C0456B3-C6B8-4560-9D17-B33E65F7F528"),
						"Australian Company",
						GroupInstance,
						"1.1.0.2"
						);
							
				this.licenceDS.AddGroupedComponentInfoRow(GroupName,
						new Guid("44A99034-79F6-453a-89F6-1E47BBAFB24F"),
						"Australian Division of an entity",
						GroupInstance,
						"1.1.0.2"
						);
						
				this.licenceDS.AddGroupedComponentInfoRow(GroupName,
						new Guid("{E5921418-7F69-4dd2-8CA8-EAA8909F17F3}"),
						"Australian Incorporated Joint Venture",
						GroupInstance,
						"1.1.0.2"
						);
						
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
						new Guid("{47507E76-BF4D-491a-9783-6DCA7E5811D9}"),
						"Australian Partnership",
						GroupInstance,
						"1.1.0.2"
						);
						
				this.licenceDS.AddGroupedComponentInfoRow(GroupName,
						new Guid("3C1C57F7-ECA1-4f14-8A44-99E9EC98F947"),
						"Australian Trust",
						GroupInstance,
						"1.1.0.2"
						);
						
				this.licenceDS.AddGroupedComponentInfoRow(GroupName,
						new Guid("44240F44-76ED-458f-AF16-21CA6D3B0802"),
						"Australian Unincorporated Joint Venture (share of outputs)",
						GroupInstance,
						"1.1.0.2"
						);
						
				this.licenceDS.AddGroupedComponentInfoRow(GroupName,
						new Guid("E06EC08A-452A-4223-B5D1-1C307862A241"),
						"Australian Unincorporated Joint Venture (share of profits)",
						GroupInstance,
						"1.1.0.2"
						);
						
				this.licenceDS.AddGroupedComponentInfoRow(GroupName,
						new Guid("0AA37352-3610-4ebb-9C15-856F4E2B7E2A"),
						"Australian entity",
						GroupInstance,
						"1.1.0.2"
						);
						
				this.licenceDS.AddGroupedComponentInfoRow(GroupName,
						new Guid("B361D993-DEC0-4442-9312-5608BF19F370"),
						"Entity",
						GroupInstance,
						"1.1.0.1"
						);
					
		}
		#endregion

		#region Add User Accounts
		protected virtual void AddUserAccounts()
		{
			// Setup Contants
			const string GroupName		= "UserAccounts";
			const string GroupInstance	= "-1";

			// Add the Modules Group
			this.licenceDS.AddComponentGroupRow(GroupName,this._NumberOfUserAccounts.ToString());  

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("2B4FFB46-E5C1-4b99-BD9D-EAD2B4A666ED"),
				"DB Security Group",
				GroupInstance,
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("3D98A779-ADF0-4BF5-940D-220EE57ACBD9"),
				"DB User",
				GroupInstance,
				"1.1.0.2"
				);
		}


		#endregion

		#region TEA
		protected  virtual void AddTeaComponents()
		{
			// Setup Contants
			const string GroupName		= "TEA";
			string GroupInstance	= string.Empty;

			if(this._TEA)
				GroupInstance = "-1";
			else
				GroupInstance = "0";

			// Add the TEA Group
			this.licenceDS.AddComponentGroupRow(GroupName,GroupInstance);  
			
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("69C2186E-9549-42ba-A621-2B5F17C75356"),
				"Tax effect accounting",
				GroupInstance,
				"1.1.0.3"
				);

		}


		#endregion

		#region Components List to the License File ------------------------------------

		protected  virtual void AddComponentsList()
		{
			// Setup Contants
			const string GroupName		= "Modules";
			const string GroupInstance	= "-1";

			// Add the Modules Group
			this.licenceDS.AddComponentGroupRow(GroupName,GroupInstance);  

			// ATO Forms 2003
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("5B144E3D-EE2F-4804-A01B-D219A7F227E5"),
				"ATO Forms",
				GroupInstance,
				"1.1.0.2"
				);

			// ATO Forms 2004
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("5B144E3D-EE2F-4804-A01B-D219A7F227E5"),
				"ATO Forms",
				"-1",
				"2.1.0.2"
				);

			// ATO Forms 2005
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("5B144E3D-EE2F-4804-A01B-D219A7F227E5"),
				"ATO Forms",
				"-1",
				"3.1.0.0"
				);

			// ATO Forms 2006
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("5B144E3D-EE2F-4804-A01B-D219A7F227E5"),
				"ATO Forms",
				"-1",
				"4.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("B16CDF66-8AAD-428F-9F5B-D455770EC516"),
				"Various adjustments - temporary differences",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("09168B23-A85A-43bc-8DED-14580DE89149"),
				"Borrowing costs",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("{1003D369-F9DF-4cb9-8654-2D47C02B61D2}"),
				"Business related costs",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("FA541B84-3896-4504-84B1-C36A290E7404"),
				"Australian Entertainment",
				"-1",
				"1.1.1.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("2C81CFA2-16BD-4879-B2EA-412E1296A202"),
				"Australian Exempt Income",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("7BC5C5C8-397D-4ac3-9E15-53F4488406D8"),
				"Intangibles",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("4E93E44C-B609-4b33-86C6-D778AFBDFBA1"),
				"Income tax calculation",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("04F630BF-F59C-45e4-8856-30D2913732D4"),
				"Australian Legals",
				"-1",
				"1.1.1.2"
				);


			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("489D1006-A332-4193-A0C3-62836CCDE104"),
				"Repair and maintenance",
				"-1",
				"1.1.1.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("94F0B06B-505E-4194-9FA6-79E5244D779F"),
				"Broker Managed Component",
				"-1",
				"1.1.1.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("A41CD257-CE8A-46ab-BCFD-FBDE02A85F34"),
				"Trading stock",
				"-1",
				"1.1.0.2"
				);


			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("{F3491EE8-E354-4c8a-90A9-2836AD0EC125}"),
				"Investments",
				"-1",
				"1.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("A7A2B69D-181A-458c-8221-8A0B497EAF2F"),
				"Consumables",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("C8032301-B42F-453a-8AE1-B4AE7E6D85A5"),
				"Accrued income",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("E887589E-D574-49b5-86AD-4FA66C3B33EB"),
				"Accrued expenses",
				"-1",
				"1.1.0.2"
				);


			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("8C3F8BB9-7EA1-4567-A691-89A80E2415CE"),
				"Provisions",
				"-1",
				"1.1.1.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("B6FCD85A-B289-411e-B48A-A80B7848D8C0"),
				"Unearned revenue",
				"-1",
				"1.1.0.2"
				);


			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("3981982D-3D81-4419-B831-90B1C8254976"),
				"Calculation Module",
				"-1",
				"1.1.0.3"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("17141A0D-42E2-4346-A9F7-16BCF76C193A"),
				"Chart Of Accounts",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("391B2230-F504-4f8a-8FA6-DFDFFA608E24"),
				"Tax Map",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("D5D34D91-5BD5-4ca5-8353-9478299E4188"),
				"Consolidation adjustment entity",
				"-1",
				"1.1.0.1"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("CA65CF34-3EAC-40a7-B3BD-0B6E0FDA6892"),
				"Accounting records - Group",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("17762042-5FC3-432a-B2EC-B8FD52F957A6"),
				"Donations",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("1EF5BD90-8999-4bd2-A976-79DD864FA332"),
				"Error handler",
				"-1",
				"1.1.0.1"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("815C19AA-D48B-4ea4-BAAC-D045973AEA3F"),
				"Event",
				"-1",
				"1.1.0.1"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("724713C0-2AB7-4e9c-8220-9EFCFD8DC362"),
				"Superannuation contributions",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("0330C3E7-A3CA-4767-B03C-1A7402973E85"),
				"Various adjustments - permanent",
				"-1",
				"1.1.1.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("ECB88EBB-0FD2-4255-9B11-FB1A2577B858"),
				"Various adjustments - timing",
				"-1",
				"1.1.1.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("48654912-2C61-473d-92CE-BD086A89F149"),
				"Foreign exchange gains and losses",
				"-1",
				"1.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("EB5E3368-90BE-4483-A22A-872F91CE8D1E"),
				"Bad debts",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("EC96B495-DC01-4d22-8D1E-23F71DDFA02B"),
				"Tax offsets credits and instalments",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("F5BF6358-A6B7-4e92-81DF-6DCCFF6E12D5"),
				"Subscriptions",
				"-1",
				"1.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("{01768745-06DF-4c84-873A-812129C686AB}"),
				"Consultancy fees",
				"-1",
				"1.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("5588C9FB-44B6-4559-A205-FF7A79BA791C"),
				"Capital gains/losses",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("F3D730D6-3DCC-46be-8AD4-8F346F536551"),
				"Uniform capital allowance",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("9E6A9C5F-A9B4-4d0e-AF98-7D0029B62421"),
				"File Import",
				"-1",
				"1.1.0.0"
				);
//
//			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
//				new Guid("1BE15D05-DF54-483f-A556-27992915B6DC"),
//				"Lessor assets",
//				"-1",
//				"1.1.0.2"
//				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("3E2F8113-25AA-451b-8C33-74DF33FE5F59"),
				"Fixed assets",
				"-1",
				"1.1.2.4"
				);


			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("D2EF2D41-EC29-437c-95F1-CA0D8E0FA3C5"),
				"Foreign income",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("196EED92-D604-4b07-BF49-C5C322400A05"),
				"Group",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("5A198D5F-EB0F-4e3d-93D1-E88B57810E83"),
				"Distributions received",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("5DE96952-FB8A-4561-9207-C74D32176BC7"),
				"Interest Expense",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("E15E5928-DC89-40f3-A047-73232A1608EE"),
				"Ledger Map",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("F9402117-5B8C-471f-B4B6-13DE7406BC91"),
				"Accounting records",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("9E6870FD-3F53-45c6-A398-FA5C20A3CB42"),
				"Nable Licence",
				"-1",
				"1.1.0.0"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("DB489B76-49E4-4a6d-942C-9F59061080D8"),
				"Logical Module",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("E7A63C80-7FA8-4875-82E4-3643DF9AE6B3"),
				"Map Target",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("030EC843-59FA-488c-B75B-A82D52FCBA93"),
				"Organization Unit",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("59DFAAA8-FB81-492c-964D-967A2847ED83"),
				"Organization",
				"-1",
				"1.1.1.1"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("FD5D3AFD-3745-40b5-B46F-9048C3EE4BA4"),
				"Period",
				"-1",
				"1.1.0.3"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("B19BCED6-9F26-4e2e-A1D5-47E8DBF6E7D0"),
				"Prepayments",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("2E0A06D3-2B32-40da-8056-F45D2D0FBDC8"),
				"Privilege Template",
				"-1",
				"1.1.0.1"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("06EE8B65-EBE7-43bd-A404-288D5A00BA9D"),
				"Research and development",
				"-1",
				"1.1.0.2"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("939CC890-FA84-4b24-AC5C-3D178D7E7FB8"),
				"SAP Base",
				"-1",
				"1.1.0.1"
				);

			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("D3FA6258-5833-45e3-999E-C8B5FDFD0989"),
				"Validation Module",
				"-1",
				"1.1.0.2"
				);

		}
		#endregion
	}
}
