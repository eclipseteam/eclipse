using System;
using System.Drawing;
using System.Collections;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Licence
{
	public class DigitallySign
	{
		private string _hashKey = string.Empty;
		#region Main Contructor --------------------------------------------------------
		public DigitallySign()
		{
		
		}
		#endregion
		

		#region Sign File

		public bool SignFile(string fileName,out string messages)
		{
		
			try
			{
				StreamReader reader = new StreamReader(fileName);
				string file = reader.ReadToEnd();

				int licenseStart = file.IndexOf("<md5>");
				int licenseFinish = file.IndexOf("</md5>", 0);
			
				if (licenseStart != -1)
					file = file.Substring(0, licenseStart);
			
				string md5 = CreateMD5(file.Trim());
				string md5Encryt = Encryption.EncryptData(md5);
				_hashKey =md5Encryt;
				reader.Close();

				WriteToFile(file, md5Encryt, fileName);
				messages = "File signed and saved successfully!";
				return true;
			}
			catch(Exception ex)
			{
				messages = ex.Message.ToString();
				return false;
			}
		}

		public string GetHashKey
		{
			get
			{
				return _hashKey;
			}
		}

		private string CreateMD5(string text)
		{
			MD5 md5 = new MD5CryptoServiceProvider();
			Encoding encoding = Encoding.ASCII;
			
			// Generate MD5 Hash of the COA
			byte[] result = md5.ComputeHash(encoding.GetBytes(text));
			Debug.WriteLine("In Create ="+encoding.GetString(result).ToString());
			return encoding.GetString(result);
		}

		private void WriteToFile(string mainText, string md5Encryt, string fileName)
		{
			StreamWriter writer = new StreamWriter(fileName);
			writer.WriteLine(mainText);
			writer.WriteLine("<md5>" + md5Encryt + "</md5>");
			writer.Close();
		}

		#endregion

		#region Verify File
		public bool VerifyFile(string fileName, out string messages)
		{
			bool verified = true;

			StreamReader reader = new StreamReader(fileName);
			string fileText = reader.ReadToEnd();
			reader.Close();

			int licenseStart = fileText.IndexOf("<md5>");
			int licenseFinish = fileText.IndexOf("</md5>", 0);
		
			// Make sure that the MD5 tags are found
			if (!(licenseStart == -1 || licenseFinish == -1))
			{
				string md5String = fileText.Substring(licenseStart + 5, licenseFinish - licenseStart - 5);
				fileText = fileText.Substring(0, licenseStart);
				
				try
				{
					Debug.WriteLine("before="+md5String);
					md5String = Encryption.DecryptData(md5String);
					Debug.WriteLine("After="+md5String);
					
					if(!CreateMD5(fileText.Trim()).Equals(md5String))
					{
						messages = "File signature stamp does not match.";
						verified=false;
					}
					else
					{
						messages = "Signature verified!";
						verified=true;
					}
				}
				catch
				{
					messages = "Signature not found in file of file tampered with.";
					verified=false;
				}
			}
			else
			{
				messages = "Signature not found in file of file tampered with.";
				verified=false;
			}

			return verified;
		}
		#endregion
	}
}
