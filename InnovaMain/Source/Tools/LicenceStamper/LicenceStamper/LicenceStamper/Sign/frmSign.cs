using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Oritax.TaxSimp.Licence
{
	/// <summary>
	/// Summary description for frmSign.
	/// </summary>
	public class frmSign : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button cmdCancel;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label lblMessage;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnBrowse;
		private System.Windows.Forms.TextBox txtFileName;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button cmdSign;
		private System.Windows.Forms.Button btnVerify;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmSign()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			Init();
			
		}

		private void Init()
		{
			this.Text  = "N-ABLE Licence Stamper";
			this.lblMessage.Text=string.Empty;
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmSign));
			this.panel2 = new System.Windows.Forms.Panel();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.lblMessage = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.label2 = new System.Windows.Forms.Label();
			this.btnBrowse = new System.Windows.Forms.Button();
			this.txtFileName = new System.Windows.Forms.TextBox();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.label4 = new System.Windows.Forms.Label();
			this.cmdSign = new System.Windows.Forms.Button();
			this.btnVerify = new System.Windows.Forms.Button();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.cmdCancel);
			this.panel2.Controls.Add(this.groupBox1);
			this.panel2.Controls.Add(this.lblMessage);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(0, 287);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(578, 40);
			this.panel2.TabIndex = 22;
			// 
			// cmdCancel
			// 
			this.cmdCancel.Location = new System.Drawing.Point(496, 16);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.TabIndex = 3;
			this.cmdCancel.Text = "Close";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(578, 8);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			// 
			// lblMessage
			// 
			this.lblMessage.ForeColor = System.Drawing.Color.Gray;
			this.lblMessage.Location = new System.Drawing.Point(16, 16);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(472, 16);
			this.lblMessage.TabIndex = 20;
			this.lblMessage.Text = "<< OS Detected >>";
			this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(578, 48);
			this.panel1.TabIndex = 23;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(512, 32);
			this.label1.TabIndex = 0;
			this.label1.Text = "Licence Stamper - Sign  or Verify files";
			// 
			// pictureBox2
			// 
			this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(77)), ((System.Byte)(90)));
			this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.Location = new System.Drawing.Point(0, 48);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(90, 239);
			this.pictureBox2.TabIndex = 24;
			this.pictureBox2.TabStop = false;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(104, 88);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(96, 16);
			this.label2.TabIndex = 27;
			this.label2.Text = "File Name";
			// 
			// btnBrowse
			// 
			this.btnBrowse.Location = new System.Drawing.Point(472, 104);
			this.btnBrowse.Name = "btnBrowse";
			this.btnBrowse.Size = new System.Drawing.Size(32, 23);
			this.btnBrowse.TabIndex = 26;
			this.btnBrowse.Text = "...";
			this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
			// 
			// txtFileName
			// 
			this.txtFileName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtFileName.Location = new System.Drawing.Point(104, 104);
			this.txtFileName.Name = "txtFileName";
			this.txtFileName.Size = new System.Drawing.Size(360, 20);
			this.txtFileName.TabIndex = 25;
			this.txtFileName.Text = "";
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label4.Location = new System.Drawing.Point(104, 56);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(392, 16);
			this.label4.TabIndex = 30;
			this.label4.Text = "Sign  or verify xml  licence file or chart  of accounts CSV files";
			// 
			// cmdSign
			// 
			this.cmdSign.Image = ((System.Drawing.Image)(resources.GetObject("cmdSign.Image")));
			this.cmdSign.Location = new System.Drawing.Point(104, 136);
			this.cmdSign.Name = "cmdSign";
			this.cmdSign.Size = new System.Drawing.Size(80, 72);
			this.cmdSign.TabIndex = 31;
			this.cmdSign.Text = "Sign";
			this.cmdSign.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.cmdSign.Click += new System.EventHandler(this.cmdSign_Click);
			// 
			// btnVerify
			// 
			this.btnVerify.Image = ((System.Drawing.Image)(resources.GetObject("btnVerify.Image")));
			this.btnVerify.Location = new System.Drawing.Point(200, 136);
			this.btnVerify.Name = "btnVerify";
			this.btnVerify.Size = new System.Drawing.Size(80, 72);
			this.btnVerify.TabIndex = 32;
			this.btnVerify.Text = "Verify";
			this.btnVerify.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnVerify.Click += new System.EventHandler(this.btnVerify_Click);
			// 
			// frmSign
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(578, 327);
			this.ControlBox = false;
			this.Controls.Add(this.btnVerify);
			this.Controls.Add(this.cmdSign);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.btnBrowse);
			this.Controls.Add(this.txtFileName);
			this.Controls.Add(this.pictureBox2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel2);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "frmSign";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "frmSign";
			this.panel2.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void btnBrowse_Click(object sender, System.EventArgs e)
		{
			try
			{
				openFileDialog1.AddExtension = true; 
				openFileDialog1.Filter = "N-ABLE Licence File |*.xml";
				openFileDialog1.ShowDialog();
				txtFileName.Text = openFileDialog1.FileName;
				
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private void cmdSign_Click(object sender, System.EventArgs e)
		{
			try
			{
				if (txtFileName.Text != "")
				{
					string messages = string.Empty;
					DigitallySign sign = new DigitallySign();

					if(sign.SignFile(txtFileName.Text.ToString(),out messages))
					{
						lblMessage.Text=messages;
						MessageBox.Show (this,messages,"Operation Successfull",MessageBoxButtons.OK,MessageBoxIcon.Information );
					}
					else
					{
						lblMessage.Text=messages;
						MessageBox.Show (this,messages,"Operation Failed",MessageBoxButtons.OK,MessageBoxIcon.Error);
	
					}

				}
				else
					MessageBox.Show (this,"No file provided to sign.","No file to sign",MessageBoxButtons.OK,MessageBoxIcon.Error );
			}
			catch (Exception ex)
			{
				MessageBox.Show (this,ex.ToString(),"Error occurred",MessageBoxButtons.OK,MessageBoxIcon.Error );
			}
		}

		private void btnVerify_Click(object sender, System.EventArgs e)
		{
			try
			{
				if (txtFileName.Text != "")
				{
					string messages = string.Empty;
					DigitallySign sign = new DigitallySign();

					if(sign.VerifyFile(txtFileName.Text.ToString(),out messages))
					{
						lblMessage.Text=messages;
						MessageBox.Show (this,messages,"Operation Successfull",MessageBoxButtons.OK,MessageBoxIcon.Information );
					}
					else
					{
						lblMessage.Text=messages;
						MessageBox.Show (this,messages,"Operation Failed",MessageBoxButtons.OK,MessageBoxIcon.Error);
	
					}

				}
				else
					MessageBox.Show (this,"No file provided to verify.","No file to verify",MessageBoxButtons.OK,MessageBoxIcon.Error );
			}
			catch (Exception ex)
			{
				MessageBox.Show (this,ex.ToString(),"Error occurred",MessageBoxButtons.OK,MessageBoxIcon.Error );
			}
		}
	}
}
