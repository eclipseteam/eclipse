using System;

namespace Oritax.TaxSimp.Licence
{
	public class  NableConstants
	{
		#region Public constants - Nable Licence ---------------------------------------
        public const string GLOBAL_INFO_NABLEVERSION = "NABLE 2008.2";
		public const string GLOBAL_INFO_PERIODEXPIRYDATE	= "30/06/2009";
		public const string FORMSKIT_PERIOD_END = "31/12/2008";

		#endregion

		#region Public constants - Licence Stamper -------------------------------------
		public const string LICENCE_STAMPER_VERSION = "2.0.0";
		public const string NABLE="nable";
		#endregion

	}
}
