using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Oritax.TaxSimp.Licence
{
	/// <summary>
	/// Summary description for NableMessageBox.
	/// </summary>
	public class NableMessageBox : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.TextBox txtMessage;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Label lblCaption;
		public string MessageCaption = string.Empty;
		public string MessageText = string.Empty;
		public bool	ErrorMode = false;
		private System.Windows.Forms.PictureBox PicOK;
		private System.Windows.Forms.PictureBox picError;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public NableMessageBox()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			
			
		}

		public NableMessageBox(string Title, string Message, bool errorMode)
		{
			InitializeComponent();
			this.MessageCaption= Title;
			this.MessageText = Message;
			this.ErrorMode = errorMode;
			picError.Visible=false;
			PicOK.Visible=false;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(NableMessageBox));
			this.txtMessage = new System.Windows.Forms.TextBox();
			this.btnOK = new System.Windows.Forms.Button();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.lblCaption = new System.Windows.Forms.Label();
			this.PicOK = new System.Windows.Forms.PictureBox();
			this.picError = new System.Windows.Forms.PictureBox();
			this.SuspendLayout();
			// 
			// txtMessage
			// 
			this.txtMessage.AcceptsReturn = true;
			this.txtMessage.BackColor = System.Drawing.SystemColors.InactiveBorder;
			this.txtMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtMessage.Location = new System.Drawing.Point(152, 40);
			this.txtMessage.Multiline = true;
			this.txtMessage.Name = "txtMessage";
			this.txtMessage.ReadOnly = true;
			this.txtMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtMessage.Size = new System.Drawing.Size(288, 176);
			this.txtMessage.TabIndex = 0;
			this.txtMessage.Text = "";
			// 
			// btnOK
			// 
			this.btnOK.Location = new System.Drawing.Point(344, 224);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(96, 24);
			this.btnOK.TabIndex = 1;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// pictureBox2
			// 
			this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(77)), ((System.Byte)(90)));
			this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.Location = new System.Drawing.Point(0, 0);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(88, 258);
			this.pictureBox2.TabIndex = 26;
			this.pictureBox2.TabStop = false;
			// 
			// lblCaption
			// 
			this.lblCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblCaption.Location = new System.Drawing.Point(96, 8);
			this.lblCaption.Name = "lblCaption";
			this.lblCaption.Size = new System.Drawing.Size(344, 24);
			this.lblCaption.TabIndex = 27;
			// 
			// PicOK
			// 
			this.PicOK.Image = ((System.Drawing.Image)(resources.GetObject("PicOK.Image")));
			this.PicOK.Location = new System.Drawing.Point(96, 40);
			this.PicOK.Name = "PicOK";
			this.PicOK.Size = new System.Drawing.Size(48, 48);
			this.PicOK.TabIndex = 28;
			this.PicOK.TabStop = false;
			// 
			// picError
			// 
			this.picError.Image = ((System.Drawing.Image)(resources.GetObject("picError.Image")));
			this.picError.Location = new System.Drawing.Point(96, 40);
			this.picError.Name = "picError";
			this.picError.Size = new System.Drawing.Size(56, 48);
			this.picError.TabIndex = 29;
			this.picError.TabStop = false;
			// 
			// NableMessageBox
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(450, 258);
			this.Controls.Add(this.picError);
			this.Controls.Add(this.PicOK);
			this.Controls.Add(this.lblCaption);
			this.Controls.Add(this.pictureBox2);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.txtMessage);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "NableMessageBox";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Load += new System.EventHandler(this.NableMessageBox_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void NableMessageBox_Load(object sender, System.EventArgs e)
		{
			this.Text = MessageCaption.ToString();
			txtMessage.Text = MessageText.ToString();
			lblCaption.Text = MessageCaption.ToString();
			if(ErrorMode)
			{
				lblCaption.ForeColor = Color.Red;
				picError.Visible=true;
				PicOK.Visible=false;
			}
			else	
			{
				lblCaption.ForeColor = Color.Black;
				picError.Visible=false;
				PicOK.Visible=true;
			}

			
		}
	}
}
