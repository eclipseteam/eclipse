using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using System.IO;
using Oritax.TaxSimp.TaxSimpLicence;

namespace Oritax.TaxSimp.Licence
{
	/// <summary>
	/// Summary description for frmCreate.
	/// </summary>
	public class frmCreate : System.Windows.Forms.Form
	{
		#region System variables / objects ---------------------------------------------
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button cmdCancel;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox cmbLicenceType;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.ComboBox cmbLedgerImport;
		private System.Windows.Forms.TextBox txtInstallationID;
		private System.Windows.Forms.TextBox txtUserAccounts;
		private System.Windows.Forms.TextBox txtEntities;
		private System.Windows.Forms.TextBox txtGroups;
		private System.Windows.Forms.TextBox txtScenarios;
		private System.Windows.Forms.DateTimePicker dateReportingPeriod;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.DateTimePicker DateLicenceExpire;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabLicenceInfo;
		private System.Windows.Forms.TabPage tabCompanyInfo;
		private System.Windows.Forms.ImageList imageList;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Button btnBrowse;
		private System.Windows.Forms.TextBox txtFileName;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Button cmdSave;
		private System.Windows.Forms.ComboBox cmbTea;
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.TextBox txtCompanyName;
		private System.Windows.Forms.TextBox txtAddress;
		private System.Windows.Forms.TextBox txtABN;
		private System.Windows.Forms.TextBox txtACN;
		private bool InitFormLoad = true;
		#endregion

		#region Constants --------------------------------------------------------------
		private const string GROUPS ="Group";
		private const string ENTITY ="Entity";
		private const string USER_ACCOUNT ="UserAccounts";
		private const string TEA ="TEA";
		private const string YES = "Yes";
		private const string NO = "No";
		private const string DEMONSTRATION = "Demonstration";
		private const string REGISTERED = "Registered";
		private const string UNLIMITED = "Unlimited";
		private const string PERIOD_END = "30/06/2006";  
		private const string UNLICENSED = "(unlicensed)";
		private const int SELECTED = 1;
		private const int UNSELECTED = 0;
		private const int TEA_SELECTED = 0;
		private const int DEMO_DAYS = 16;  
		private const int DEMO_MODE = 0;
		private const int REG_MODE = 1;
		public const string PRODUCT_TYPE_NTMS = "N-ABLE TMS";
		public const string PRODUCT_TYPE_NTPS = "N-ABLE TPS � DTT / KPMG";
		public const string PRODUCT_TYPE_NTPS_OTHER = "N-ABLE TPS - OTHER";
		public const string PRODUCT_TYPE_NTRS = "N-ABLE TRS";
		public const string PRODUCT_TYPE_TAXKIT = "N-ABLE TCS (NTK)";
		public const string PRODUCT_TYPE_FORMSKIT = "Nable Forms Kit";
		private const int NTMS = 0;
		private const int NTPS = 3;
		private const int NTRS = 4;
		private const int NTCS = 5;
		private const int NTK = 2;
		private const int NFK = 1;

		private System.Windows.Forms.Button cmdVerify;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
		private System.Windows.Forms.CheckBox chkUserAccounts;
		private System.Windows.Forms.CheckBox chkEntities;
		private System.Windows.Forms.CheckBox chkgroups;
		private System.Windows.Forms.CheckBox chkScenarios;
		private System.Windows.Forms.TextBox lblMessage;
		private System.Windows.Forms.LinkLabel bntOpenLicenceFile;
		private System.Windows.Forms.CheckBox chkDateOverride;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox cmbProductType;
		private System.Windows.Forms.ToolTip toolTip1;
		private string nableProductType = string.Empty;

		#endregion

		public frmCreate()
		{
			InitializeComponent();
			Init();
		}

		private void Init()
		{
			
			this.Text  = "N-ABLE Licence Stamper";
			this.cmbLedgerImport.Items.Clear();
			this.cmbLicenceType.Items.Clear();
			this.cmbTea.Items.Clear();

			// Add the text in
			this.cmbTea.Items.Add(YES);
			this.cmbTea.Items.Add(NO);
			this.cmbLedgerImport.Items.Add(YES);
			this.cmbLedgerImport.Items.Add(NO);
			this.cmbLicenceType.Items.Add(DEMONSTRATION);
			this.cmbLicenceType.Items.Add(REGISTERED);

			// Add the product Type
			this.cmbProductType.Items.Clear();
			this.cmbProductType.Items.Add(PRODUCT_TYPE_NTMS);
			this.cmbProductType.Items.Add(PRODUCT_TYPE_NTPS);
			this.cmbProductType.Items.Add(PRODUCT_TYPE_NTRS);
			this.cmbProductType.Items.Add(PRODUCT_TYPE_FORMSKIT);
			this.cmbProductType.Items.Add(PRODUCT_TYPE_NTPS_OTHER);
			this.cmbProductType.Items.Add(PRODUCT_TYPE_TAXKIT);
			this.cmbProductType.SelectedIndex = 0;
			
			this.cmbLedgerImport.SelectedIndex = 0;
			this.cmbLicenceType.SelectedIndex = 0;
			this.cmbTea.SelectedIndex=SELECTED;

			DateLicenceExpire.Text=DateTime.Now.AddDays(DEMO_DAYS).ToShortDateString();
			
			enableDisbleButtons();
			InitFormLoad=false;
  
		}
	
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmCreate));
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.cmbLicenceType = new System.Windows.Forms.ComboBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.cmbLedgerImport = new System.Windows.Forms.ComboBox();
			this.cmbTea = new System.Windows.Forms.ComboBox();
			this.txtInstallationID = new System.Windows.Forms.TextBox();
			this.txtUserAccounts = new System.Windows.Forms.TextBox();
			this.txtEntities = new System.Windows.Forms.TextBox();
			this.txtGroups = new System.Windows.Forms.TextBox();
			this.txtScenarios = new System.Windows.Forms.TextBox();
			this.dateReportingPeriod = new System.Windows.Forms.DateTimePicker();
			this.panel3 = new System.Windows.Forms.Panel();
			this.chkDateOverride = new System.Windows.Forms.CheckBox();
			this.bntOpenLicenceFile = new System.Windows.Forms.LinkLabel();
			this.label7 = new System.Windows.Forms.Label();
			this.DateLicenceExpire = new System.Windows.Forms.DateTimePicker();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabLicenceInfo = new System.Windows.Forms.TabPage();
			this.chkScenarios = new System.Windows.Forms.CheckBox();
			this.chkgroups = new System.Windows.Forms.CheckBox();
			this.chkEntities = new System.Windows.Forms.CheckBox();
			this.chkUserAccounts = new System.Windows.Forms.CheckBox();
			this.tabCompanyInfo = new System.Windows.Forms.TabPage();
			this.label17 = new System.Windows.Forms.Label();
			this.txtACN = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.txtAddress = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.txtABN = new System.Windows.Forms.TextBox();
			this.txtCompanyName = new System.Windows.Forms.TextBox();
			this.imageList = new System.Windows.Forms.ImageList(this.components);
			this.label18 = new System.Windows.Forms.Label();
			this.btnBrowse = new System.Windows.Forms.Button();
			this.txtFileName = new System.Windows.Forms.TextBox();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.panel4 = new System.Windows.Forms.Panel();
			this.cmdSave = new System.Windows.Forms.Button();
			this.cmdVerify = new System.Windows.Forms.Button();
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			this.lblMessage = new System.Windows.Forms.TextBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.label2 = new System.Windows.Forms.Label();
			this.cmbProductType = new System.Windows.Forms.ComboBox();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.panel3.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabLicenceInfo.SuspendLayout();
			this.tabCompanyInfo.SuspendLayout();
			this.SuspendLayout();
			// 
			// pictureBox2
			// 
			this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(77)), ((System.Byte)(90)));
			this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.Location = new System.Drawing.Point(0, 48);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(88, 517);
			this.pictureBox2.TabIndex = 25;
			this.pictureBox2.TabStop = false;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.cmdCancel);
			this.panel2.Controls.Add(this.groupBox1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(0, 565);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(616, 40);
			this.panel2.TabIndex = 24;
			// 
			// cmdCancel
			// 
			this.cmdCancel.Location = new System.Drawing.Point(528, 16);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(80, 23);
			this.cmdCancel.TabIndex = 3;
			this.cmdCancel.Text = "Close";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(616, 8);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(616, 48);
			this.panel1.TabIndex = 23;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(512, 32);
			this.label1.TabIndex = 0;
			this.label1.Text = "Licence Stamper - Create/Update Licence";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(64)), ((System.Byte)(64)), ((System.Byte)(64)));
			this.label3.Location = new System.Drawing.Point(24, 16);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(73, 16);
			this.label3.TabIndex = 27;
			this.label3.Text = "Installation ID";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(64)), ((System.Byte)(64)), ((System.Byte)(64)));
			this.label4.Location = new System.Drawing.Point(24, 48);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(130, 16);
			this.label4.TabIndex = 28;
			this.label4.Text = "Number of user accounts";
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label5.Location = new System.Drawing.Point(8, 32);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(120, 16);
			this.label5.TabIndex = 29;
			this.label5.Text = "Licence Type";
			// 
			// cmbLicenceType
			// 
			this.cmbLicenceType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbLicenceType.Items.AddRange(new object[] {
																"Demonstration",
																"Registered"});
			this.cmbLicenceType.Location = new System.Drawing.Point(176, 32);
			this.cmbLicenceType.Name = "cmbLicenceType";
			this.cmbLicenceType.Size = new System.Drawing.Size(160, 21);
			this.cmbLicenceType.TabIndex = 30;
			this.cmbLicenceType.SelectedIndexChanged += new System.EventHandler(this.cmbLicenceType_SelectedIndexChanged);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(64)), ((System.Byte)(64)), ((System.Byte)(64)));
			this.label6.Location = new System.Drawing.Point(24, 72);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(96, 16);
			this.label6.TabIndex = 31;
			this.label6.Text = "Number of entities";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(64)), ((System.Byte)(64)), ((System.Byte)(64)));
			this.label8.Location = new System.Drawing.Point(24, 96);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(95, 16);
			this.label8.TabIndex = 33;
			this.label8.Text = "Number of groups";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(64)), ((System.Byte)(64)), ((System.Byte)(64)));
			this.label9.Location = new System.Drawing.Point(24, 152);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(122, 16);
			this.label9.TabIndex = 34;
			this.label9.Text = "Reporting periods up to";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(64)), ((System.Byte)(64)), ((System.Byte)(64)));
			this.label10.Location = new System.Drawing.Point(24, 120);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(108, 16);
			this.label10.TabIndex = 35;
			this.label10.Text = "Number of scenarios";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(64)), ((System.Byte)(64)), ((System.Byte)(64)));
			this.label11.Location = new System.Drawing.Point(24, 184);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(117, 16);
			this.label11.TabIndex = 36;
			this.label11.Text = "Ledger Import Allowed";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(64)), ((System.Byte)(64)), ((System.Byte)(64)));
			this.label12.Location = new System.Drawing.Point(24, 208);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(69, 16);
			this.label12.TabIndex = 37;
			this.label12.Text = "TEA Allowed";
			// 
			// cmbLedgerImport
			// 
			this.cmbLedgerImport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbLedgerImport.Items.AddRange(new object[] {
																 "Yes",
																 "No"});
			this.cmbLedgerImport.Location = new System.Drawing.Point(176, 184);
			this.cmbLedgerImport.Name = "cmbLedgerImport";
			this.cmbLedgerImport.Size = new System.Drawing.Size(96, 21);
			this.cmbLedgerImport.TabIndex = 38;
			// 
			// cmbTea
			// 
			this.cmbTea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbTea.Items.AddRange(new object[] {
														"Yes",
														"No"});
			this.cmbTea.Location = new System.Drawing.Point(176, 208);
			this.cmbTea.Name = "cmbTea";
			this.cmbTea.Size = new System.Drawing.Size(96, 21);
			this.cmbTea.TabIndex = 39;
			this.cmbTea.SelectedValueChanged += new System.EventHandler(this.cmbTea_SelectedIndexChanged);
			// 
			// txtInstallationID
			// 
			this.txtInstallationID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtInstallationID.Location = new System.Drawing.Point(176, 16);
			this.txtInstallationID.Name = "txtInstallationID";
			this.txtInstallationID.Size = new System.Drawing.Size(288, 20);
			this.txtInstallationID.TabIndex = 40;
			this.txtInstallationID.Text = "";
			// 
			// txtUserAccounts
			// 
			this.txtUserAccounts.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtUserAccounts.Location = new System.Drawing.Point(176, 48);
			this.txtUserAccounts.Name = "txtUserAccounts";
			this.txtUserAccounts.Size = new System.Drawing.Size(92, 20);
			this.txtUserAccounts.TabIndex = 41;
			this.txtUserAccounts.Text = "";
			this.txtUserAccounts.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.processNumericKeys);
			// 
			// txtEntities
			// 
			this.txtEntities.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtEntities.Location = new System.Drawing.Point(176, 72);
			this.txtEntities.Name = "txtEntities";
			this.txtEntities.Size = new System.Drawing.Size(92, 20);
			this.txtEntities.TabIndex = 42;
			this.txtEntities.Text = "";
			this.txtEntities.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.processNumericKeys);
			// 
			// txtGroups
			// 
			this.txtGroups.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtGroups.Location = new System.Drawing.Point(176, 96);
			this.txtGroups.Name = "txtGroups";
			this.txtGroups.Size = new System.Drawing.Size(92, 20);
			this.txtGroups.TabIndex = 43;
			this.txtGroups.Text = "";
			this.txtGroups.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.processNumericKeys);
			// 
			// txtScenarios
			// 
			this.txtScenarios.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtScenarios.Location = new System.Drawing.Point(176, 120);
			this.txtScenarios.Name = "txtScenarios";
			this.txtScenarios.Size = new System.Drawing.Size(92, 20);
			this.txtScenarios.TabIndex = 44;
			this.txtScenarios.Text = "";
			this.txtScenarios.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.processNumericKeys);
			// 
			// dateReportingPeriod
			// 
			this.dateReportingPeriod.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateReportingPeriod.Location = new System.Drawing.Point(176, 152);
			this.dateReportingPeriod.Name = "dateReportingPeriod";
			this.dateReportingPeriod.Size = new System.Drawing.Size(152, 20);
			this.dateReportingPeriod.TabIndex = 47;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.cmbProductType);
			this.panel3.Controls.Add(this.label2);
			this.panel3.Controls.Add(this.chkDateOverride);
			this.panel3.Controls.Add(this.bntOpenLicenceFile);
			this.panel3.Controls.Add(this.label7);
			this.panel3.Controls.Add(this.DateLicenceExpire);
			this.panel3.Controls.Add(this.label5);
			this.panel3.Controls.Add(this.cmbLicenceType);
			this.panel3.Controls.Add(this.tabControl1);
			this.panel3.Location = new System.Drawing.Point(96, 56);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(512, 368);
			this.panel3.TabIndex = 48;
			// 
			// chkDateOverride
			// 
			this.chkDateOverride.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.chkDateOverride.ForeColor = System.Drawing.Color.Gray;
			this.chkDateOverride.Location = new System.Drawing.Point(384, 56);
			this.chkDateOverride.Name = "chkDateOverride";
			this.chkDateOverride.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.chkDateOverride.Size = new System.Drawing.Size(112, 16);
			this.chkDateOverride.TabIndex = 54;
			this.chkDateOverride.Text = "Date override";
			this.chkDateOverride.CheckedChanged += new System.EventHandler(this.chkDateOverride_CheckedChanged);
			// 
			// bntOpenLicenceFile
			// 
			this.bntOpenLicenceFile.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bntOpenLicenceFile.Image = ((System.Drawing.Image)(resources.GetObject("bntOpenLicenceFile.Image")));
			this.bntOpenLicenceFile.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.bntOpenLicenceFile.LinkArea = new System.Windows.Forms.LinkArea(0, 25);
			this.bntOpenLicenceFile.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
			this.bntOpenLicenceFile.LinkColor = System.Drawing.Color.Gray;
			this.bntOpenLicenceFile.Location = new System.Drawing.Point(384, 8);
			this.bntOpenLicenceFile.Name = "bntOpenLicenceFile";
			this.bntOpenLicenceFile.Size = new System.Drawing.Size(120, 24);
			this.bntOpenLicenceFile.TabIndex = 53;
			this.bntOpenLicenceFile.TabStop = true;
			this.bntOpenLicenceFile.Text = "Open licence file              ";
			this.bntOpenLicenceFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.bntOpenLicenceFile.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.bntOpenLicenceFile_LinkClicked);
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label7.Location = new System.Drawing.Point(8, 56);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(112, 16);
			this.label7.TabIndex = 50;
			this.label7.Text = "Licence Expiry Date";
			// 
			// DateLicenceExpire
			// 
			this.DateLicenceExpire.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.DateLicenceExpire.Location = new System.Drawing.Point(176, 56);
			this.DateLicenceExpire.Name = "DateLicenceExpire";
			this.DateLicenceExpire.Size = new System.Drawing.Size(160, 20);
			this.DateLicenceExpire.TabIndex = 49;
			this.DateLicenceExpire.ValueChanged += new System.EventHandler(this.DateLicenceExpire_ValueChanged);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabLicenceInfo);
			this.tabControl1.Controls.Add(this.tabCompanyInfo);
			this.tabControl1.Location = new System.Drawing.Point(8, 96);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(496, 264);
			this.tabControl1.TabIndex = 49;
			// 
			// tabLicenceInfo
			// 
			this.tabLicenceInfo.Controls.Add(this.chkScenarios);
			this.tabLicenceInfo.Controls.Add(this.chkgroups);
			this.tabLicenceInfo.Controls.Add(this.chkEntities);
			this.tabLicenceInfo.Controls.Add(this.chkUserAccounts);
			this.tabLicenceInfo.Controls.Add(this.dateReportingPeriod);
			this.tabLicenceInfo.Controls.Add(this.label4);
			this.tabLicenceInfo.Controls.Add(this.txtUserAccounts);
			this.tabLicenceInfo.Controls.Add(this.cmbLedgerImport);
			this.tabLicenceInfo.Controls.Add(this.label9);
			this.tabLicenceInfo.Controls.Add(this.label11);
			this.tabLicenceInfo.Controls.Add(this.label6);
			this.tabLicenceInfo.Controls.Add(this.txtGroups);
			this.tabLicenceInfo.Controls.Add(this.txtScenarios);
			this.tabLicenceInfo.Controls.Add(this.label12);
			this.tabLicenceInfo.Controls.Add(this.label8);
			this.tabLicenceInfo.Controls.Add(this.cmbTea);
			this.tabLicenceInfo.Controls.Add(this.txtEntities);
			this.tabLicenceInfo.Controls.Add(this.txtInstallationID);
			this.tabLicenceInfo.Controls.Add(this.label3);
			this.tabLicenceInfo.Controls.Add(this.label10);
			this.tabLicenceInfo.ImageIndex = 1;
			this.tabLicenceInfo.Location = new System.Drawing.Point(4, 22);
			this.tabLicenceInfo.Name = "tabLicenceInfo";
			this.tabLicenceInfo.Size = new System.Drawing.Size(488, 238);
			this.tabLicenceInfo.TabIndex = 0;
			this.tabLicenceInfo.Text = "Licence Information";
			// 
			// chkScenarios
			// 
			this.chkScenarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.chkScenarios.ForeColor = System.Drawing.Color.Gray;
			this.chkScenarios.Location = new System.Drawing.Point(288, 120);
			this.chkScenarios.Name = "chkScenarios";
			this.chkScenarios.Size = new System.Drawing.Size(136, 16);
			this.chkScenarios.TabIndex = 51;
			this.chkScenarios.Text = "Unlimited";
			this.chkScenarios.CheckedChanged += new System.EventHandler(this.chkScenarios_CheckedChanged);
			// 
			// chkgroups
			// 
			this.chkgroups.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.chkgroups.ForeColor = System.Drawing.Color.Gray;
			this.chkgroups.Location = new System.Drawing.Point(288, 96);
			this.chkgroups.Name = "chkgroups";
			this.chkgroups.Size = new System.Drawing.Size(136, 16);
			this.chkgroups.TabIndex = 50;
			this.chkgroups.Text = "Unlimited";
			this.chkgroups.CheckedChanged += new System.EventHandler(this.chkgroups_CheckedChanged);
			// 
			// chkEntities
			// 
			this.chkEntities.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.chkEntities.ForeColor = System.Drawing.Color.Gray;
			this.chkEntities.Location = new System.Drawing.Point(288, 72);
			this.chkEntities.Name = "chkEntities";
			this.chkEntities.Size = new System.Drawing.Size(136, 16);
			this.chkEntities.TabIndex = 49;
			this.chkEntities.Text = "Unlimited";
			this.chkEntities.CheckedChanged += new System.EventHandler(this.chkEntities_CheckedChanged);
			// 
			// chkUserAccounts
			// 
			this.chkUserAccounts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.chkUserAccounts.ForeColor = System.Drawing.Color.Gray;
			this.chkUserAccounts.Location = new System.Drawing.Point(288, 48);
			this.chkUserAccounts.Name = "chkUserAccounts";
			this.chkUserAccounts.Size = new System.Drawing.Size(136, 16);
			this.chkUserAccounts.TabIndex = 48;
			this.chkUserAccounts.Text = "Unlimited";
			this.chkUserAccounts.CheckedChanged += new System.EventHandler(this.chkUserAccounts_CheckedChanged);
			// 
			// tabCompanyInfo
			// 
			this.tabCompanyInfo.Controls.Add(this.label17);
			this.tabCompanyInfo.Controls.Add(this.txtACN);
			this.tabCompanyInfo.Controls.Add(this.label13);
			this.tabCompanyInfo.Controls.Add(this.txtAddress);
			this.tabCompanyInfo.Controls.Add(this.label14);
			this.tabCompanyInfo.Controls.Add(this.label15);
			this.tabCompanyInfo.Controls.Add(this.txtABN);
			this.tabCompanyInfo.Controls.Add(this.txtCompanyName);
			this.tabCompanyInfo.Location = new System.Drawing.Point(4, 22);
			this.tabCompanyInfo.Name = "tabCompanyInfo";
			this.tabCompanyInfo.Size = new System.Drawing.Size(488, 262);
			this.tabCompanyInfo.TabIndex = 1;
			this.tabCompanyInfo.Text = "Company Information";
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(64)), ((System.Byte)(64)), ((System.Byte)(64)));
			this.label17.Location = new System.Drawing.Point(24, 120);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(28, 16);
			this.label17.TabIndex = 50;
			this.label17.Text = "ACN";
			// 
			// txtACN
			// 
			this.txtACN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtACN.Location = new System.Drawing.Point(176, 120);
			this.txtACN.Name = "txtACN";
			this.txtACN.Size = new System.Drawing.Size(200, 20);
			this.txtACN.TabIndex = 51;
			this.txtACN.Text = "";
			this.txtACN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.processNumericKeys);
			this.txtACN.TextChanged += new System.EventHandler(this.txtACN_TextChanged);
			this.txtACN.Leave += new System.EventHandler(this.txtACN_Leave);
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(64)), ((System.Byte)(64)), ((System.Byte)(64)));
			this.label13.Location = new System.Drawing.Point(24, 40);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(46, 16);
			this.label13.TabIndex = 45;
			this.label13.Text = "Address";
			// 
			// txtAddress
			// 
			this.txtAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtAddress.Location = new System.Drawing.Point(176, 40);
			this.txtAddress.Multiline = true;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtAddress.Size = new System.Drawing.Size(296, 48);
			this.txtAddress.TabIndex = 48;
			this.txtAddress.Text = "";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(64)), ((System.Byte)(64)), ((System.Byte)(64)));
			this.label14.Location = new System.Drawing.Point(24, 96);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(27, 16);
			this.label14.TabIndex = 46;
			this.label14.Text = "ABN";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(64)), ((System.Byte)(64)), ((System.Byte)(64)));
			this.label15.Location = new System.Drawing.Point(24, 16);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(86, 16);
			this.label15.TabIndex = 44;
			this.label15.Text = "Company Name";
			// 
			// txtABN
			// 
			this.txtABN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtABN.Location = new System.Drawing.Point(176, 96);
			this.txtABN.Name = "txtABN";
			this.txtABN.Size = new System.Drawing.Size(200, 20);
			this.txtABN.TabIndex = 49;
			this.txtABN.Text = "";
			this.txtABN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.processNumericKeys);
			this.txtABN.TextChanged += new System.EventHandler(this.txtABN_TextChanged);
			this.txtABN.Leave += new System.EventHandler(this.txtABN_Leave);
			// 
			// txtCompanyName
			// 
			this.txtCompanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtCompanyName.Location = new System.Drawing.Point(176, 16);
			this.txtCompanyName.Name = "txtCompanyName";
			this.txtCompanyName.Size = new System.Drawing.Size(296, 20);
			this.txtCompanyName.TabIndex = 47;
			this.txtCompanyName.Text = "";
			// 
			// imageList
			// 
			this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
			this.imageList.ImageSize = new System.Drawing.Size(16, 16);
			this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
			this.imageList.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(120, 440);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(164, 16);
			this.label18.TabIndex = 52;
			this.label18.Text = "Licence Filename and Location ";
			// 
			// btnBrowse
			// 
			this.btnBrowse.Location = new System.Drawing.Point(544, 456);
			this.btnBrowse.Name = "btnBrowse";
			this.btnBrowse.Size = new System.Drawing.Size(32, 23);
			this.btnBrowse.TabIndex = 51;
			this.btnBrowse.Text = "...";
			this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
			// 
			// txtFileName
			// 
			this.txtFileName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtFileName.ForeColor = System.Drawing.Color.Gray;
			this.txtFileName.Location = new System.Drawing.Point(120, 456);
			this.txtFileName.Name = "txtFileName";
			this.txtFileName.Size = new System.Drawing.Size(416, 20);
			this.txtFileName.TabIndex = 50;
			this.txtFileName.Text = "";
			this.txtFileName.TextChanged += new System.EventHandler(this.txtFileName_TextChanged);
			this.txtFileName.Leave += new System.EventHandler(this.txtFileName_Leave);
			// 
			// panel4
			// 
			this.panel4.BackColor = System.Drawing.Color.Gray;
			this.panel4.ForeColor = System.Drawing.Color.Gray;
			this.panel4.Location = new System.Drawing.Point(96, 432);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(512, 1);
			this.panel4.TabIndex = 53;
			// 
			// cmdSave
			// 
			this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
			this.cmdSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.cmdSave.Location = new System.Drawing.Point(120, 488);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Size = new System.Drawing.Size(104, 72);
			this.cmdSave.TabIndex = 54;
			this.cmdSave.Text = "Save and Sign";
			this.cmdSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// cmdVerify
			// 
			this.cmdVerify.Image = ((System.Drawing.Image)(resources.GetObject("cmdVerify.Image")));
			this.cmdVerify.Location = new System.Drawing.Point(232, 488);
			this.cmdVerify.Name = "cmdVerify";
			this.cmdVerify.Size = new System.Drawing.Size(80, 72);
			this.cmdVerify.TabIndex = 55;
			this.cmdVerify.Text = "Verify";
			this.cmdVerify.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.cmdVerify.Visible = false;
			// 
			// lblMessage
			// 
			this.lblMessage.AcceptsTab = true;
			this.lblMessage.BackColor = System.Drawing.SystemColors.InactiveBorder;
			this.lblMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lblMessage.ForeColor = System.Drawing.Color.Gray;
			this.lblMessage.Location = new System.Drawing.Point(232, 488);
			this.lblMessage.MaxLength = 0;
			this.lblMessage.Multiline = true;
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.ReadOnly = true;
			this.lblMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.lblMessage.Size = new System.Drawing.Size(344, 72);
			this.lblMessage.TabIndex = 56;
			this.lblMessage.Text = "";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(8, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(112, 16);
			this.label2.TabIndex = 55;
			this.label2.Text = "Product Type";
			// 
			// cmbProductType
			// 
			this.cmbProductType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbProductType.Items.AddRange(new object[] {
																"NABLE TMS",
																"Forms Kit",
																"Tax Kit"});
			this.cmbProductType.Location = new System.Drawing.Point(176, 8);
			this.cmbProductType.Name = "cmbProductType";
			this.cmbProductType.Size = new System.Drawing.Size(160, 21);
			this.cmbProductType.TabIndex = 56;
			this.cmbProductType.SelectedIndexChanged += new System.EventHandler(this.cmbProductType_SelectedIndexChanged);
			// 
			// frmCreate
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(616, 605);
			this.Controls.Add(this.lblMessage);
			this.Controls.Add(this.cmdVerify);
			this.Controls.Add(this.cmdSave);
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.label18);
			this.Controls.Add(this.btnBrowse);
			this.Controls.Add(this.txtFileName);
			this.Controls.Add(this.pictureBox2);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel3);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MaximumSize = new System.Drawing.Size(622, 630);
			this.MinimumSize = new System.Drawing.Size(622, 630);
			this.Name = "frmCreate";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "frmCreate";
			this.panel2.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabLicenceInfo.ResumeLayout(false);
			this.tabCompanyInfo.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion


		private void cmbProductType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string userData = string.Empty;
			string unlicensed = string.Empty;

			if((cmbProductType.SelectedItem.Equals(PRODUCT_TYPE_FORMSKIT)) || (cmbProductType.SelectedItem.Equals(PRODUCT_TYPE_TAXKIT)))
			{
				// Common to both Tax Kit and Forms Kit
				this.cmbLicenceType.SelectedIndex = 1; 
				this.cmbLicenceType.Enabled=false; 
				this.cmbLedgerImport.SelectedItem = NO;
				this.cmbTea.SelectedItem=NO;
				this.cmbTea.Enabled= false;
				this.cmbLedgerImport.Enabled= false;
				chkScenarios.Enabled=false; 
			}
			else
			{	
				this.cmbLicenceType.SelectedIndex = 0;
				this.cmbLicenceType.Enabled=true;
			}

			if(cmbProductType.SelectedItem.Equals(PRODUCT_TYPE_FORMSKIT))
			{
				this.txtScenarios.Text = "1";
				chkEntities.Checked=true; 
				chkUserAccounts.Checked=true; 
				chkgroups.Checked=true; 
				dateReportingPeriod.Text = NableConstants.FORMSKIT_PERIOD_END; 
			
			}

			if(cmbProductType.SelectedItem.Equals(PRODUCT_TYPE_TAXKIT))
				this.txtScenarios.Text = "2";

		}

		private void cmbLicenceType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string userData = string.Empty;
			string unlicensed = string.Empty;
			bool enable = false;

			if(cmbLicenceType.SelectedItem.Equals(DEMONSTRATION))
			{
				userData = UNLIMITED;
				enable = false;
				cmbLedgerImport.SelectedItem=YES;
				cmbTea.SelectedItem=YES;
				dateReportingPeriod.Text=NableConstants.GLOBAL_INFO_PERIODEXPIRYDATE; 
				unlicensed=UNLICENSED;
				DateLicenceExpire.Text=DateTime.Now.AddDays(DEMO_DAYS).ToShortDateString();
				chkDateOverride.Enabled=false; 
			}
			else
			{
				cmbTea.SelectedItem=NO;
				userData = string.Empty;
				unlicensed=string.Empty;
				enable = true;
				DateLicenceExpire.Text=DateTime.MaxValue.AddYears(-1).ToShortDateString();
				chkDateOverride.Enabled=true;
			}

			chkEntities.Checked = !enable;
			chkgroups.Checked = !enable;
			chkUserAccounts.Checked = !enable;
			chkScenarios.Checked = !enable;
			
			chkEntities.Enabled = enable;
			chkgroups.Enabled =enable;
			chkUserAccounts.Enabled = enable;
			chkScenarios.Enabled = enable;
			DateLicenceExpire.Enabled = !enable;
  
			txtInstallationID.Text = userData;
			
			txtCompanyName.Text = unlicensed;
			txtABN.Text = unlicensed;
			txtACN.Text = unlicensed;
			txtAddress.Text = unlicensed;
			
			txtEntities.Enabled  = enable; 
			txtGroups.Enabled = enable;
			txtUserAccounts.Enabled = enable;
			txtScenarios.Enabled = enable;
			txtInstallationID.Enabled=enable;
			cmbLedgerImport.Enabled = enable;
			cmbTea.Enabled=enable;
			dateReportingPeriod.Enabled=enable;
				
			enableDisbleButtons();

			if((cmbProductType.SelectedItem.Equals(PRODUCT_TYPE_NTPS)))
			{
				chkUserAccounts.Checked = true; 
				this.txtEntities.Text = "100"; 
				this.txtScenarios.Text = "10";
				this.cmbLedgerImport.SelectedItem = NO; 
				this.cmbTea.SelectedItem = NO;
			}
			else if((cmbProductType.SelectedItem.Equals(PRODUCT_TYPE_NTPS_OTHER)))
			{
				this.txtGroups.Text = "1"; 
				this.txtScenarios.Text = "3";
				this.cmbLedgerImport.SelectedItem = YES; 
				this.cmbTea.SelectedItem=YES;
			}
			else if((cmbProductType.SelectedItem.Equals(PRODUCT_TYPE_NTRS)))
			{
				chkUserAccounts.Checked = true; 
				this.txtEntities.Text = "100"; 
				this.txtScenarios.Text = "10";
				this.cmbLedgerImport.SelectedItem = NO; 
				this.cmbTea.SelectedItem = YES;
			}
			else if((cmbProductType.SelectedItem.Equals(PRODUCT_TYPE_TAXKIT)))
			{
				this.txtScenarios.Text = "2";
				this.cmbLedgerImport.SelectedItem = NO; 
				this.cmbTea.SelectedItem = NO;
			}
			else if((cmbProductType.SelectedItem.Equals(PRODUCT_TYPE_FORMSKIT)))
			{
				chkUserAccounts.Checked = true; 
				this.chkEntities.Checked = true; 
				this.chkgroups.Checked = true;
				this.txtScenarios.Text = "1";
				this.cmbLedgerImport.SelectedItem = NO; 
				this.cmbTea.SelectedItem = NO;
			}
		}

		private void enableDisbleButtons()
		{
			cmdSave.Enabled=enablebuttons();
			cmdVerify.Enabled=enablebuttons();
			cmdSave.Enabled=enablebuttons();
		}

		private void btnBrowse_Click(object sender, System.EventArgs e)
		{
			try
			{
				saveFileDialog1.AddExtension = true; 
				saveFileDialog1.Filter = "N-ABLE Licence File |*.xml";

				saveFileDialog1.ShowDialog();
				txtFileName.Text = saveFileDialog1.FileName;
				
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}

			enableDisbleButtons();
		}

		private bool enablebuttons()
		{
			bool result = false;
			if(!txtFileName.Text.Trim().Equals(string.Empty) && 
				!cmbLicenceType.Text.Trim().Equals(string.Empty) && 
				!DateLicenceExpire.Text.Trim().Equals(string.Empty))
				result = true;												   
			else
				result = false;

			return result;
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void DateLicenceExpire_ValueChanged(object sender, System.EventArgs e)
		{
			enableDisbleButtons();
		}

		private void txtFileName_TextChanged(object sender, System.EventArgs e)
		{
			enableDisbleButtons();
		}

		private void txtFileName_Leave(object sender, System.EventArgs e)
		{
			enableDisbleButtons();
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			// Save and Sign
			string licenceType = string.Empty; 
			string productType = string.Empty;

			LicenceDS  licence = new LicenceDS();
			LicencePDF licencePDF = new LicencePDF();
			NableLicenseFile TaxSimpLicenceFile;

			try
			{
				#region Product Selection Type
				validateEntries();
				
				if (cmbProductType.SelectedItem.Equals(PRODUCT_TYPE_NTMS))
				{
					TaxSimpLicenceFile = new NableLicenseFile();
					licencePDF.lblProductType = PRODUCT_TYPE_NTMS;
					productType = PRODUCT_TYPE_NTMS;
				}
				else if (cmbProductType.SelectedItem.Equals(PRODUCT_TYPE_FORMSKIT))
				{
					TaxSimpLicenceFile = new NableFormsKitLicenseFile();
					licencePDF.lblProductType = PRODUCT_TYPE_FORMSKIT;
					productType = PRODUCT_TYPE_FORMSKIT;
				}
				else if (cmbProductType.SelectedItem.Equals(PRODUCT_TYPE_TAXKIT))
				{
					TaxSimpLicenceFile = new NableTaxKitLicenseFile();
					licencePDF.lblProductType = PRODUCT_TYPE_TAXKIT;
					productType = PRODUCT_TYPE_TAXKIT;
				}
				else
					TaxSimpLicenceFile = new NableLicenseFile();

				TaxSimpLicenceFile.ProductType = (String)cmbProductType.SelectedItem; 
				#endregion

				#region Variable Assignment
				if(cmbLedgerImport.SelectedIndex.Equals(0))
					TaxSimpLicenceFile._ImportLedger = true;
				else
					TaxSimpLicenceFile._ImportLedger = false;

				licencePDF.lblGLImport =cmbLedgerImport.Text.ToString();

				if(cmbTea.SelectedIndex.Equals(0))
					TaxSimpLicenceFile._TEA = true;
				else
					TaxSimpLicenceFile._TEA =false;

				licencePDF.lblTEA = cmbTea.Text.ToString();

				if(cmbLicenceType.Text.Equals(REGISTERED))
				{
					TaxSimpLicenceFile._IsDEMO = false;
					TaxSimpLicenceFile._InstallationID =new Guid(txtInstallationID.Text.ToString()); 
					licencePDF.lblLicenceID =  txtInstallationID.Text.ToString(); 
					licencePDF.lblLicenceType=REGISTERED;
				}
				else if(cmbLicenceType.Text.Equals(DEMONSTRATION))
				{
					TaxSimpLicenceFile._IsDEMO = true; 
					licencePDF.lblLicenceID = UNLIMITED;
					licencePDF.lblLicenceType=DEMONSTRATION;
				}
				
				if(txtScenarios.Text.Equals(UNLIMITED))
				{
					TaxSimpLicenceFile._NumberScenarios = -1;
					licencePDF.lblScenarios = UNLIMITED;  
				}
				else
				{
					TaxSimpLicenceFile._NumberScenarios = int.Parse(txtScenarios.Text.ToString());
					licencePDF.lblScenarios = txtScenarios.Text.ToString();
				}

				if(txtEntities.Text.Equals(UNLIMITED))
				{
					TaxSimpLicenceFile._NumberOfEntities = -1;
					licencePDF.lblEntities=UNLIMITED; 
				}
				else
				{
					TaxSimpLicenceFile._NumberOfEntities = int.Parse(txtEntities.Text.ToString());
					licencePDF.lblEntities= txtEntities.Text.ToString();
				}

				if(txtGroups.Text.Equals(UNLIMITED))
				{
					TaxSimpLicenceFile._NumberOfGroups = -1;
					licencePDF.lblGroups=  UNLIMITED;
				}
				else
				{
					TaxSimpLicenceFile._NumberOfGroups = int.Parse(txtGroups.Text.ToString());
					licencePDF.lblGroups=txtGroups.Text.ToString();
				}

				if(txtUserAccounts.Text.Equals(UNLIMITED))
				{
					TaxSimpLicenceFile._NumberOfUserAccounts = -1;
					licencePDF.lblUserAcounts = UNLIMITED;
				}
				else
				{
					TaxSimpLicenceFile._NumberOfUserAccounts = int.Parse(txtUserAccounts.Text.ToString());
					licencePDF.lblUserAcounts = txtUserAccounts.Text.ToString();
				}


				TaxSimpLicenceFile._CompanyName  = txtCompanyName.Text.ToString();
				licencePDF.lblCompanyName = txtCompanyName.Text.ToString();
				TaxSimpLicenceFile._CompanyAddress = txtAddress.Text.ToString();
				licencePDF.lblCompanyAddress = txtAddress.Text.ToString();
				TaxSimpLicenceFile._CompanyABN = txtABN.Text.ToString();
				licencePDF.lblCompanyABN = txtABN.Text.ToString();
				TaxSimpLicenceFile._CompanyACN = txtACN.Text.ToString();
				licencePDF.lblCompanyACN = txtACN.Text.ToString();
				TaxSimpLicenceFile._LicenceDate = DateTime.Now.ToShortDateString();
				licencePDF.lblLicenceDateCreated = DateTime.Now.ToShortDateString();
				TaxSimpLicenceFile._LicenceExpireDate = DateTime.Parse(DateLicenceExpire.Text.ToString()).ToShortDateString(); 
				licencePDF.lblLicenceExpireDate = DateTime.Parse(DateLicenceExpire.Text.ToString()).ToShortDateString(); 
				TaxSimpLicenceFile._PeriodEndDate = DateTime.Parse(dateReportingPeriod.Text.ToString()).ToShortDateString(); 
				licencePDF.lblPeriodEndDate = DateTime.Parse(dateReportingPeriod.Text.ToString()).ToShortDateString(); 
				#endregion

				#region Write and Digitally sign licence file
				licence = TaxSimpLicenceFile.NableLicenseData; 
				TaxSimpLicenceWriter licenceWriter = new TaxSimpLicenceWriter(licence);

				string result = string.Empty;
				string hashKey = string.Empty;

				licenceWriter.ProductType = productType; 
				licenceWriter.CreateLicenseContent(txtFileName.Text.ToString());
				result = licenceWriter.DigitallySignLicenseFile(txtFileName.Text.ToString(),out hashKey);
				#endregion

				#region Create Licence PDF Output
				licencePDF.lblLicenceHASHKEY = hashKey;
				licencePDF.WritePDF(txtFileName.Text.ToString()+".pdf");
				#endregion

				#region Finalise
				string SuccessMessage = result+Environment.NewLine+Environment.NewLine+"File save at location "+txtFileName.Text.ToString()+Environment.NewLine+Environment.NewLine+"and the PDF printout version is saved at "+txtFileName.Text.ToString()+".pdf";
				lblMessage.Text = result+Environment.NewLine+Environment.NewLine+"File save at location "+txtFileName.Text.ToString()+Environment.NewLine+Environment.NewLine+"and the PDF printout version is saved at "+txtFileName.Text.ToString()+".pdf";
				NableMessageBox nableMSGBox = new NableMessageBox("Licence Creation",SuccessMessage,false);
				nableMSGBox.ShowDialog(this);
				
				txtFileName.Text = string.Empty; 
				#endregion

			}
			catch(Exception ex)
			{	
				lblMessage.Text = "Last error message"+Environment.NewLine+Environment.NewLine;
				lblMessage.Text += ex.Message.ToString();
				NableMessageBox nableMSGBox = new NableMessageBox("Validation Message",ex.Message.ToString(),true);
				nableMSGBox.ShowDialog(this);
			}
		}

		private void validateEntries()
		{
			string _validationMessage  = string.Empty;

			// test Licence type
			if(cmbLicenceType.Text.Equals(REGISTERED))
			{
				//  test the Company details.

				if(txtCompanyName.Text.Trim().Equals(string.Empty))
					_validationMessage += Environment.NewLine+"Company name cannot be blank.";

				if(!txtABN.Text.Trim().Equals(string.Empty))
				{
					if(!ValidateABN(txtABN.Text))
						_validationMessage += Environment.NewLine+"ABN is not correct";
				}

				if(!txtACN.Text.Trim().Equals(string.Empty))
				{
					if(!ValidateACN_ACN(txtACN.Text))
						_validationMessage += Environment.NewLine+"ACN is not correct";
				}

				if(txtAddress.Text.Trim().Equals(string.Empty))
					_validationMessage += Environment.NewLine+"Company Address cannot be blank.";

				if(txtInstallationID.Text.Trim().Equals(string.Empty))
					_validationMessage += Environment.NewLine+"Installation ID cannot be blank.";
				
				if(txtUserAccounts.Text.Trim().Equals(string.Empty))
					_validationMessage += Environment.NewLine+"Number of user account cannot be blank.";

				if(txtEntities.Text.Trim().Equals(string.Empty))
					_validationMessage += Environment.NewLine+"Number of Entities cannot be blank.";

				if(txtGroups.Text.Trim().Equals(string.Empty))
					_validationMessage += Environment.NewLine+"Number of groups cannot be blank.";

				if(txtScenarios.Text.Trim().Equals(string.Empty))
					_validationMessage += Environment.NewLine+"Number of scenarios cannot be blank.";

				if(cmbLedgerImport.SelectedIndex==-1 )
					_validationMessage += Environment.NewLine+"Ledger import must be set.";

				if(cmbTea.SelectedIndex==-1 )
					_validationMessage += Environment.NewLine+"Tea must be set.";

				if(_validationMessage.Trim().Length>0)
					_validationMessage += Environment.NewLine+"These are required when licence type is of \"Registered\"";
					
			}

			if(!(txtFileName.Text.IndexOf(":")>0))
			{
				if( (!(txtFileName.Text.StartsWith("\\"))) && (!(txtFileName.Text.StartsWith("//"))))
					_validationMessage += Environment.NewLine+"Invalid save file path.("+txtFileName.Text.ToString()+")";
			}

			// throw exception
			if(_validationMessage.Trim().Length>0)
				throw new Exception(_validationMessage);

		}

		private void chkUserAccounts_CheckedChanged(object sender, System.EventArgs e)
		{
			txtUserAccounts.Text = ((chkUserAccounts.Checked) ?  UNLIMITED : string.Empty);
			txtUserAccounts.Enabled = !chkUserAccounts.Checked; 
		}

		private void chkEntities_CheckedChanged(object sender, System.EventArgs e)
		{
			txtEntities.Text = ((chkEntities.Checked) ?  UNLIMITED : string.Empty);
			txtEntities.Enabled = !chkEntities.Checked; 
		}

		private void chkgroups_CheckedChanged(object sender, System.EventArgs e)
		{
			txtGroups.Text = ((chkgroups.Checked) ?  UNLIMITED : string.Empty);
			txtGroups.Enabled = !chkgroups.Checked; 
		}

		private void chkScenarios_CheckedChanged(object sender, System.EventArgs e)
		{
			txtScenarios.Text = ((chkScenarios.Checked) ?  UNLIMITED : string.Empty);
			txtScenarios.Enabled = !chkScenarios.Checked; 
		}

		private void cmbTea_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			validateTEA();
		}

		private void validateTEA()
		{
			if(!InitFormLoad)
			{
				if(cmbTea.SelectedIndex ==TEA_SELECTED)
				{
					DialogResult result = MessageBox.Show(this,"Have you checked that this company is NOT a SEC listed company?","TEA Check",MessageBoxButtons.YesNo,MessageBoxIcon.Question);
 
					if(result== DialogResult.No)
						cmbTea.SelectedIndex = 1;

				}
			}
		}

		private void loadDataFromLicenceFile(LicenceDS licenceds)
		{
			#region Load global information
			int AllowedInstance = 0;

			DataRow drow = licenceds.Tables[LicenceDS.GLOBAL_TABLE].Rows[0];


			if ((nableProductType.Equals(PRODUCT_TYPE_NTMS)) || (nableProductType.Equals(string.Empty)))
			{
				cmbProductType.SelectedIndex = NTMS; 
			}
			else if(nableProductType.Equals(PRODUCT_TYPE_FORMSKIT))
			{
				cmbProductType.SelectedIndex = NFK; 
			}
			else if(nableProductType.Equals(PRODUCT_TYPE_TAXKIT))
			{
				cmbProductType.SelectedIndex = NTK; 
			}
			else
			{
				cmbProductType.SelectedIndex = NTMS; 
			}


			if (drow[LicenceDS.GLOBAL_EXPIRYDATE_FLD].ToString().Equals(DateTime.MaxValue.AddYears(-1).ToShortDateString()))
				DateLicenceExpire.Enabled=false;
			else
				DateLicenceExpire.Enabled=true;

			if (licenceds.LicenceGUID.Equals(Guid.Empty))
				cmbLicenceType.SelectedIndex=DEMO_MODE;
			else
			{
				cmbLicenceType.SelectedIndex=REG_MODE;

				txtInstallationID.Text = drow[LicenceDS.GLOBAL_LICENCE_GUID_FLD].ToString();

				if(drow[LicenceDS.GLOBAL_LICENCE_SCENARIOS_FLD].ToString().Equals("-1"))
					chkScenarios.Checked=true;
				else
					txtScenarios.Text =drow[LicenceDS.GLOBAL_LICENCE_SCENARIOS_FLD].ToString();

				if(drow[LicenceDS.GLOBAL_LICENCE_IMPORT_LEDGER_FLD].ToString().Equals("True"))
					cmbLedgerImport.SelectedItem=YES;
				else
					cmbLedgerImport.SelectedItem=NO;
			}

			txtCompanyName.Text = drow[LicenceDS.GLOBAL_NAME_FLD].ToString();
			txtAddress.Text = drow[LicenceDS.GLOBAL_ADDRESS_FLD].ToString();
			txtABN.Text =  drow[LicenceDS.GLOBAL_ABN_FLD].ToString();
			txtACN.Text = drow[LicenceDS.GLOBAL_ACN_FLD].ToString();

			DateLicenceExpire.Text = drow[LicenceDS.GLOBAL_EXPIRYDATE_FLD].ToString();
			dateReportingPeriod.Text=drow[LicenceDS.GLOBAL_PERIOD_ENDDATE_FLD].ToString();

			AllowedInstance =  getSectionInstanceCount(GROUPS,licenceds);
			if(AllowedInstance.Equals(-1))
				chkgroups.Checked=true;
			else
				txtGroups.Text = AllowedInstance.ToString();
 
			AllowedInstance =  getSectionInstanceCount(USER_ACCOUNT,licenceds);
			if(AllowedInstance.Equals(-1))
				chkUserAccounts.Checked=true;
			else
				txtUserAccounts.Text = AllowedInstance.ToString();


			AllowedInstance =  getSectionInstanceCount(ENTITY,licenceds);
			if(AllowedInstance.Equals(-1))
				chkEntities.Checked=true;
			else
				txtEntities.Text = AllowedInstance.ToString();

			AllowedInstance =  getSectionInstanceCount(TEA,licenceds);
			if(AllowedInstance.Equals(-1))
				cmbTea.SelectedItem=YES;
			else
				cmbTea.SelectedItem=NO;

			#endregion
		}

		private void bntOpenLicenceFile_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			try
			{
				openFileDialog1.AddExtension = true; 
				openFileDialog1.Filter = "N-ABLE Licence File |*.xml";

				openFileDialog1.ShowDialog();
				txtFileName.Text = openFileDialog1.FileName;

				string fileName = txtFileName.Text;

				if(File.Exists(fileName))
				{
					TaxSimpLicenceReader readLicence = new TaxSimpLicenceReader();

					readLicence.ReadLicenseContent(fileName);
					InitFormLoad = true;
					nableProductType = readLicence.ProductType;
					loadDataFromLicenceFile(readLicence.LicenceContent);
					InitFormLoad = false;
				}
			}
			catch (Exception ex)
			{
				InitFormLoad = false;
				lblMessage.Text = ex.Message.ToString();
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				lblMessage.Text="File successfully loaded!";
			}
		
		}

		private void processNumericKeys(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			char c = e.KeyChar;

			if (Char.IsDigit(c)|| c==8|| e.KeyChar == (char)3 || e.KeyChar == (char)22 || e.KeyChar == (char)24 || e.KeyChar ==(char)26)
			{
				e.Handled = false;
			}
			else
				e.Handled = true;

		}

		private int getSectionInstanceCount(string section, LicenceDS licence)
		{
			DataTable sections = licence.Tables[LicenceDS.CG_TABLE];

			if(sections!=null)
			{
				DataView sectionsView = new DataView(sections);
				sectionsView.RowFilter = LicenceDS.CG_NAME_FLD+"='"+section+"'";
				if (sectionsView.Count>0)
				{
					return int.Parse(sectionsView[0].Row[LicenceDS.CG_ALLOWEDINSTANCES_FLD].ToString());
				}
				else
					return  0;
			}

			return  0;
		}
	
	
		private bool ValidateACN_ACN( string ACN)
		{
			if ( ACN.Equals(string.Empty) )
				return false;


			int intACNLength = ACN.Length;
			int intAddition = 0;
	
			// check that the ACN is 9 characters long
			if ( intACNLength != 9 )
			{
				return false;
			}
		
			// loop through each element in the string and validate it	
			for ( int intElementPosition = 0; intElementPosition < intACNLength; intElementPosition++ )
			{
				char Element = ACN[intElementPosition];
				
				if(!Char.IsNumber(Element))
					return false;

				int intElement = int.Parse(Element.ToString());
		
				switch ( intElementPosition )
				{
					case 0:
						intAddition += ( intElement * 8 );
						break;
			
					case 1:
						intAddition += ( intElement * 7 );
						break;
			
					case 2:
						intAddition += ( intElement * 6 );
						break;
			
					case 3:
						intAddition += ( intElement * 5 );
						break;
			
					case 4:
						intAddition += ( intElement * 4 );
						break;
			
					case 5:
						intAddition += ( intElement * 3 );
						break;
			
					case 6:
						intAddition += ( intElement * 2 );
						break;
			
					case 7:
						intAddition += ( intElement * 1 );
						break;
				}
			}
	
			int intModTen = intAddition % 10;
			int intComplement = 10 - intModTen;
			if(intComplement == 10)
				intComplement = 0;
		 
		 
			char cElement = ACN[8];
		
			if ( !Char.IsNumber(cElement))
				return false;
		
			
			int iElement = int.Parse(cElement.ToString());
	
			if (iElement == intComplement)
				return true;
			else
				return false;
		}

		private bool ValidateABN(string ABN)
		{
			if ( ABN.Equals(string.Empty))
				return false;
		
			
			int intABNLength = ABN.Length;
			int intAddition = 0;
	
			// check that the ABN is 11 characters long
			if ( intABNLength != 11 )
				return false;
		
			// loop through each element in the string and validate it	
			for ( int intElementPosition = 0; intElementPosition < intABNLength; intElementPosition++ )
			{
				char cElement = ABN[intElementPosition];
		
				if ( !Char.IsNumber(cElement))
					return false;
		
				int intElement = int.Parse(cElement.ToString());
		
				switch ( intElementPosition )
				{
					case 0:
						intAddition += ( ( intElement -1 ) * 10 );
						break;
			
					case 1:
						intAddition += ( intElement * 1 );
						break;
			
					case 2:
						intAddition += ( intElement * 3 );
						break;
			
					case 3:
						intAddition += ( intElement * 5 );
						break;
			
					case 4:
						intAddition += ( intElement * 7 );
						break;
			
					case 5:
						intAddition += ( intElement * 9 );
						break;
			
					case 6:
						intAddition += ( intElement * 11 );
						break;
			
					case 7:
						intAddition += ( intElement * 13 );
						break;
			
					case 8:
						intAddition += ( intElement * 15 );
						break;

					case 9:
						intAddition += ( intElement * 17 );
						break;

					case 10:
						intAddition += ( intElement * 19 );
						break;
				}
			}
	
			if ( ( intAddition % 89 ) == 0 )
				return true;
			else
				return false;
		}




		private void txtABN_Leave(object sender, System.EventArgs e)
		{
			ValidateABN_ACN(txtABN,"ABN"); 
		}

		private void txtACN_Leave(object sender, System.EventArgs e)
		{
			ValidateABN_ACN(txtACN,"ACN"); 
		}

		private void txtABN_TextChanged(object sender, System.EventArgs e)
		{
			ValidateABN_ACN(txtABN,"ABN"); 
		}

		private void txtACN_TextChanged(object sender, System.EventArgs e)
		{
			ValidateABN_ACN(txtACN,"ACN"); 
		}

		private void ValidateABN_ACN(TextBox txt,string type)
		{
			if (type =="ABN")
			{
				if(!ValidateABN (txt.Text))
					txt.ForeColor = Color.Red;
				else
					txt.ForeColor = Color.Black;
			}
			else if (type =="ACN")
			{
				if(!ValidateACN_ACN(txt.Text))
					txt.ForeColor = Color.Red;
				else
					txt.ForeColor = Color.Black;
			}
		}

		private void chkDateOverride_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cmbLicenceType.SelectedItem.Equals(REGISTERED))
			{
				if (chkDateOverride.Checked)
					DateLicenceExpire.Text=DateTime.Now.AddDays(DEMO_DAYS).ToShortDateString();
				else
					DateLicenceExpire.Text=DateTime.MaxValue.AddYears(-1).ToShortDateString();
				
				DateLicenceExpire.Enabled = chkDateOverride.Checked; 
			}
		}

		

	}

	
}
