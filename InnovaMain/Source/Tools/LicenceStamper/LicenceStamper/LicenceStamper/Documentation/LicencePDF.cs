using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using Oritax.TaxSimp.TaxSimpLicence;
using WebSupergoo.ABCpdf7;
using WebSupergoo.ABCpdf7.Objects;
using WebSupergoo.ABCpdf7.Atoms;

namespace Oritax.TaxSimp.Licence
{

	#region Licence PDF ----------------------------------------------------------------
	public class LicencePDF
	{
		#region  Public Constants
		public const string LBL_NABLEVERSION		= "lblNableVersion";
		public const string LBL_COMPANYNAME			= "lblCompanyName";
		public const string LBL_COMPANYADDRESS		= "lblCompanyAddress";
		public const string LBL_COMPANYAB			= "lblCompanyAB";
		public const string LBL_COMPANYACN			= "lblCompanyACN";
		public const string LBL_LICENCEID			= "lblLicenceID";
		public const string LBL_LICENCESTATUS		= "lblLicenceStatus";
		public const string LBL_LICENCETYPE			= "lblLicenceType";
		public const string LBL_LICENCESTATUSMESSAGE= "lblLicenceStatusMessage";
		public const string LBL_USERACOUNTS			= "lblUserAcounts";
		public const string LBL_ENTITIES			= "lblEntities";
		public const string LBL_GROUPS				= "lblGroups";
		public const string LBL_PERIODENDDATE		= "lblPeriodEndDate";
		public const string LBL_SCENARIOS			= "lblScenarios";
		public const string LBL_GLIMPORT			= "lblGLImport";
		public const string LBL_TEA					= "lblTEA";
		public const string LBL_LICENCEDATECREATED	= "lblLicenceDateCreated";
		public const string LBL_LICENCEHASHKEY		= "lblLicenceHASHKEY";
		public const string LBL_LICENCECREATEDBY	= "lblLicenceCreatedBy";
		#endregion

		#region Private variables
		private string _lblNableVersion = string.Empty;
		private string _lblCompanyName = string.Empty;
		private string _lblCompanyAddress = string.Empty;
		private string _lblCompanyABN = string.Empty;
		private string _lblCompanyACN = string.Empty;
		private string _lblLicenceID = string.Empty;
		private string _lblLicenceStatus = string.Empty;
		private string _lblLicenceType = string.Empty;
		private string _lblLicenceStatusMessage = string.Empty;
		private string _lblUserAcounts = string.Empty;
		private string _lblEntities = string.Empty;
		private string _lblGroups = string.Empty;
		private string _lblPeriodEndDate = string.Empty;
		private string _lblScenarios = string.Empty;
		private string _lblGLImport = string.Empty;
		private string _lblTEA = string.Empty;
		private string _lblLicenceDateCreated = string.Empty;
		private string _lblLicenceHASHKEY = string.Empty;
		private string _lblLicenceCreatedBy = string.Empty;
		private string _lblLicenceExpireDate = string.Empty;
		private string _lblProductType = string.Empty;
		private NAblePdf pdfDocument;
		private int TotalPDFPageCount;
		private ArrayList alWorkpaperDetails;
		private StringBuilder stringBuilder;
		private string pdfFilename = string.Empty;
		private string WPTitle = string.Empty;
		private string WPFooter = string.Empty;

		#endregion

		#region Main Constructor
		public LicencePDF()
		{
			TotalPDFPageCount = 0;
			this.pdfDocument = new NAblePdf();
			alWorkpaperDetails = new ArrayList();
			this.stringBuilder = new StringBuilder();
		}
		#endregion

		#region Write Licence PDF
		public void WritePDF(string filename)
		{
			pdfFilename=filename;
			AddWorkpaper();
			pdfDocument.Save(filename);
		}
		#endregion

		#region Private methods
		private void AddWorkpaper()
		{
			int		id;
			int		width = 1024;
			string	WPTitle		= string.Empty;
			string  WPFooter	= string.Empty;

			NAblePdf newWorkpaper = new NAblePdf();
			PrerenderedPages newPrerenderedPages = new PrerenderedPages();
			this.stringBuilder = PrintLicenceHTML();
	
			#region Header HTML Section ------------------------------------------------
			WPTitle =@"<html>
					<head>
					<title>Untitled Document</title>
					<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
					</head>
					<body>
					<table width='100%' border='0' cellspacing='0' cellpadding='0'>
					<tr>
						<td width='0%'><font color='#666666' face='Verdana, Arial, Helvetica, sans-serif'><strong>Oritax</strong></font></td>
						<td width='0%'>&nbsp;</td>
						<td width='100%'><div align='right'><font color='#999999' size='-1' face='Verdana, Arial, Helvetica, sans-serif'>Tax Management Solution</font></div></td>
					</tr>
					</table><hr size='1'>
					</body>
					</html>";
			#endregion

			#region Footer HTML Section ------------------------------------------------
			WPFooter =@"<html>
					<head>
					<title>Untitled Document</title>
					<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
					</head>
					<body>
					<hr size='1'>
					<table width='100%' border='0' cellspacing='0' cellpadding='0'>
					<tr>
						<td width='0%'><font color='#666666' face='Verdana, Arial, Helvetica, sans-serif'><strong>CONFIDENTIAL</strong></font></td>
						<td width='0%'>&nbsp;</td>
						<td width='100%'><div align='right'><font color='#999999' size='-1' face='Verdana, Arial, Helvetica, sans-serif'>Page 
							1 of 1</font></div></td>
					</tr>
					</table>
					</body>
					</html>";
			
			#endregion


			SetMargins(newWorkpaper, false);
			// write the HTML File
			StreamWriter sw = new StreamWriter(@pdfFilename+".html",false,System.Text.Encoding.ASCII);
			sw.WriteLine(this.stringBuilder.ToString());
			
			sw.Flush();
			sw.Close();

			//id = newWorkpaper.AddImageHtml(stringBuilder.ToString(),true,800,false);
			id = newWorkpaper.AddImageUrl ("file:///"+@pdfFilename+".html",true,800,false);
			File.Delete(@pdfFilename+".html");
			this.TotalPDFPageCount++;

			// BookMark IN PDF. Hyperlink to Page section
			AddBookmark(newWorkpaper,"N-ABLE Client Licence");

			newPrerenderedPages.PageNumber = this.TotalPDFPageCount; 
			newPrerenderedPages.PageTitleContent = WPTitle.ToString();
			newPrerenderedPages.PageFooterContent = WPFooter.ToString();
			newPrerenderedPages.PageWidth = width;
			newPrerenderedPages.isLandscape = false; 

			alWorkpaperDetails.Add(newPrerenderedPages);
			
			for(;;)
			{
				PrerenderedPages newPrerenderedMainPages = new PrerenderedPages();
				newWorkpaper.FrameRect();
				if(newWorkpaper.GetInfo(id, "Truncated") != "1") break;

				newWorkpaper.Page = newWorkpaper.AddPage();
				this.TotalPDFPageCount++;

				id = newWorkpaper.AddImageToChain(id);

				newPrerenderedMainPages.PageNumber = this.TotalPDFPageCount; 
				newPrerenderedMainPages.PageTitleContent = WPTitle.ToString();
				newPrerenderedMainPages.PageFooterContent = WPFooter.ToString();
				newPrerenderedMainPages.PageWidth = width;
				newPrerenderedMainPages.isLandscape = false; 

				alWorkpaperDetails.Add(newPrerenderedMainPages);
			}

			this.pdfDocument.Append(newWorkpaper);

			stringBuilder.Length = 0;
		}

		private void SetLandscape(NAblePdf pdf)
		{
			// adjust the default rotation and save
			int id = pdf.GetInfoInt(pdf.Root, "Pages");

			pdf.SetInfo(id, "/Rotate", "90");

			double w = pdf.MediaBox.Width;
			double h = pdf.MediaBox.Height;

			pdf.MediaBox.Width = h;
			pdf.MediaBox.Height = w;

			// rotate our rectangle
			pdf.Rect.Width = h;
			pdf.Rect.Height = w;
			pdf.Rect.Top = w;
			pdf.Rect.Left = 0;
		}

		private void SetMargins(NAblePdf pdf)
		{
			double w = pdf.Rect.Width;
			double h = pdf.Rect.Height;

			pdf.Color.String = "255 255 255";
			pdf.Rect.String = "20 41 590 749";

		}

		private void SetMargins(NAblePdf pdf, bool isLandscape)
		{
			double w = pdf.Rect.Width;
			double h = pdf.Rect.Height;

			// Section Color Red/ Blue/ Green
			pdf.Color.String = "255 255 255";

			if (isLandscape)
			{
				// left /bottom/ width/ top
				pdf.Rect.String = "20 40 770 538";
			}
			else
			{
				// left /bottom/ width/ top
				pdf.Rect.String = "20 41 590 727";
			}
		}
		
		private void AddBookmark(NAblePdf theDoc, string BookmarkText)
		{
			theDoc.AddBookmark(BookmarkText,true); 
		}

		
		private void AddHeadersFooter(NAblePdf theDoc, int PageCount)
		{
			#region PDF Page Header ----------------------------------------------------

			foreach(PrerenderedPages pre in alWorkpaperDetails)
			{
				if(pre.isLandscape)
				{

					theDoc.Rect.String = "20 590 770 540";
				}
				else
				{
					// left/bottom/width/top
					theDoc.Rect.String = "20 730 590 785";
				}

				theDoc.HPos = 0.5;
				theDoc.VPos = 0.5;
				theDoc.Color.String = "255 255 255";
				theDoc.FontSize = 8;
				theDoc.PageNumber = pre.PageNumber;

				theDoc.AddImageHtml(pre.PageTitleContent.ToString(),true,pre.PageWidth,true); 

				theDoc.FrameRect();

			}

			#endregion

			#region PDF Page Footer ----------------------------------------------------
	
			foreach(PrerenderedPages pre in alWorkpaperDetails)
			{
				string WPFooter = string.Empty;

				if(pre.isLandscape)
				{
					theDoc.Rect.String = "20 10 770 40";
				}
				else
				{
					theDoc.Rect.String = "20 10 590 40";
				}

				theDoc.HPos = 0.5;
				theDoc.VPos = 0.5;
				theDoc.Color.String = "255 255 255";
				theDoc.FontSize = 8;
			
				theDoc.PageNumber = pre.PageNumber;

				WPFooter +="<td width=\"33%\" align=\"right\" valign=\"top\">";
				WPFooter += string.Format("<div id=\"breadcrumb\" align=\"right\"><span id=\"BreadCrumbName\">Page {0} of {1}</span></div>", pre.PageNumber.ToString(), PageCount.ToString());
				WPFooter +="</td></tr></table>";
				WPFooter +="</body></html>";

				theDoc.AddImageHtml(pre.PageFooterContent+WPFooter.ToString()  ,true,1024,true);
				theDoc.FrameRect();

			}
			#endregion
		}

		#endregion

		#region Public Properties
		
		public string lblNableVersion { get{return _lblNableVersion;} set{ _lblNableVersion = value;} } 
		public string lblCompanyName { get{return _lblCompanyName;} set{ _lblCompanyName = value;} } 
		public string lblCompanyAddress { get{return _lblCompanyAddress;} set{ _lblCompanyAddress = value;} } 
		public string lblCompanyABN { get{return _lblCompanyABN;} set{ _lblCompanyABN = value;} } 
		public string lblCompanyACN { get{return _lblCompanyACN;} set{ _lblCompanyACN = value;} } 
		public string lblLicenceID { get{return _lblLicenceID;} set{ _lblLicenceID = value;} } 
		public string lblLicenceStatus { get{return _lblLicenceStatus;} set{ _lblLicenceStatus = value;} } 
		public string lblLicenceType { get{return _lblLicenceType;} set{ _lblLicenceType = value;} } 
		public string lblLicenceStatusMessage { get{return _lblLicenceStatusMessage;} set{ _lblLicenceStatusMessage = value;} } 
		public string lblUserAcounts { get{return _lblUserAcounts;} set{ _lblUserAcounts = value;} } 
		public string lblEntities { get{return _lblEntities;} set{ _lblEntities = value;} } 
		public string lblGroups { get{return _lblGroups;} set{ _lblGroups = value;} } 
		public string lblPeriodEndDate { get{return _lblPeriodEndDate;} set{ _lblPeriodEndDate = value;} } 
		public string lblScenarios { get{return _lblScenarios;} set{ _lblScenarios = value;} } 
		public string lblGLImport { get{return _lblGLImport;} set{ _lblGLImport = value;} } 
		public string lblTEA { get{return _lblTEA;} set{ _lblTEA = value;} } 
		public string lblLicenceDateCreated { get{return _lblLicenceDateCreated;} set{ _lblLicenceDateCreated = value;} } 
		public string lblLicenceHASHKEY { get{return _lblLicenceHASHKEY;} set{ _lblLicenceHASHKEY = value;} } 
		public string lblLicenceCreatedBy { get{return _lblLicenceCreatedBy;} set{ _lblLicenceCreatedBy = value;} } 
		public string lblLicenceExpireDate { get{return _lblLicenceExpireDate;} set{ _lblLicenceExpireDate = value;} } 
		public string lblProductType { get{return _lblProductType;} set{ _lblProductType = value;} } 




		
		#endregion

		#region Internal Class PreRendered Pages ---------------------------------------
		private class PrerenderedPages
		{
			private string      	WPPageTitleContent;
			private int				WPPageNumber;
			private bool			WPisLandscape;
			private int				WPPageWidth;
			private string			WPPageFooterContent;

			public PrerenderedPages()
			{
				WPPageTitleContent	= string.Empty;
				WPPageNumber		= 0;
				WPisLandscape		= false;
				WPPageWidth			= 0;
			}
		
			public string PageFooterContent
			{
				get
				{
					return this.WPPageFooterContent;
				}
				set
				{
					this.WPPageFooterContent=value;
				}
			}

			public string PageTitleContent
			{
				get
				{
					return this.WPPageTitleContent;
				}
				set
				{

					this.WPPageTitleContent = value;
				}
			}

			public int PageWidth
			{
				get
				{
					return this.WPPageWidth;
				}
				set
				{
					this.WPPageWidth = value;
				}
			}

			public bool isLandscape
			{
				get
				{
					return this.WPisLandscape;
				}
				set
				{
					this.WPisLandscape = value;
				}
			}

			public int PageNumber
			{
				get
				{
					return this.WPPageNumber;
				}
				set
				{
					this.WPPageNumber = value;
				}
			}

			

		}

		#endregion

		#region HTML Print out
		

		private StringBuilder PrintLicenceHTML()
		{
			this.lblLicenceStatusMessage = "The licence expiry date set for this client is "+this.lblLicenceExpireDate;  
			this.lblNableVersion = NableConstants.GLOBAL_INFO_NABLEVERSION;

			#region CSS
						string Style =@"<style type='text/css'>
									<!--
									a{
										border-right: medium none;
										border-top: medium none;
										font-weight: normal;
										font-size: 11px;
										border-left: medium none;
										color: #666666;
										border-bottom: medium none;
										font-family: Tahoma;
									}
									.leftquestions {font-family: Tahoma;font-size: 12px;font-weight: normal;color: #666666;	width: 180;}
									body{font-family: Tahoma;font-size: 10px;color: #666;}
									.NoteHeading{font-family: Tahoma;font-weight: bold;color: #014b59;font-size: 11px;}
									.body{font-family: Tahoma;font-size: 10px;}
			
									-->
									</style>";
			#endregion
			#region HTML Header
			StringBuilder HtmlStringBuilder = new StringBuilder(); 
			HtmlStringBuilder.AppendFormat(@"<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
								<html>
								<head>
								<title>2005 N-ABLE Tax Management Solution - Client Licence</title>
								<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>{0}</head>",Style);
			#endregion
			#region Body and Main title
			HtmlStringBuilder.AppendFormat(@"<body>
								<div class='NoteHeading'><font size='+1'>N-ABLE Tax Management Solution - Client Licence [{0}]</font></div>
								<hr size='1'>
								<br>",this.lblProductType);
			#endregion
			#region Company Information
			HtmlStringBuilder.AppendFormat(@"<font style='font-family: Tahoma;font-weight: bold;color: #014b59;font-size: 11px;' >Company information</font>
					<TABLE id='Table3' cellSpacing='1' cellPadding='1' border='0'>
							<TR>
								<TD class='leftquestions'>N-ABLE Version</TD>
								<TD>{0}</TD>
							</TR>
							<TR>
								<TD class='leftquestions'>Company Name</TD>
								<TD>{1}</TD>
							</TR>
							<TR>
								<TD class='leftquestions'>Company Address </TD>
								<TD>{2}</TD>
							</TR>
							<TR>
								<TD class='leftquestions'>Company ABN </TD>
								<TD>{3}</TD>
							</TR>
							<TR>
								<TD class='leftquestions'>Company ACN</TD>
								<TD>{4}</TD>
							</TR>
							
							
					</TABLE>
					<br>",new string[]{
										  this.lblNableVersion.ToString(),
										  this.lblCompanyName.ToString(),
										  this.lblCompanyAddress.ToString(),
										  this.lblCompanyABN.ToString(),
										  this.lblCompanyACN.ToString() 
									  });





			#endregion
			#region Licence Information
			HtmlStringBuilder.AppendFormat(@"<div class='NoteHeading'>Licence information</div>
<TABLE id='Table3' cellSpacing='1' cellPadding='1' border='0'>
				<TR>
					<TD class='leftquestions'>Installation ID</TD>
					<TD>{0}</TD>
					
				</TR>
				<TR>
					<TD class='leftquestions'>Current licence status</TD>
					<TD colspan='2'>{1}</TD>
				</TR>
				<TR>
					<TD class='leftquestions'>Licence Type</TD>
					<TD colspan='2'>{2}</TD>
				</TR>
				<TR>
					<TD class='leftquestions'></TD>
					<TD colspan='2'>{3}</TD>
				</TR>
				<TR>
					<TD class='leftquestions'>No. of user accounts</TD>
					<TD colspan='2'>{4}</TD>
				</TR>
				<TR>
					<TD class='leftquestions'>No. of entities</TD>
					<TD colspan='2'>{5}</TD>
				</TR>
				<TR>
					<TD class='leftquestions'>No. of groups</TD>
					<TD colspan='2'>{6}</TD>
				</TR>
				<TR>
					<TD class='leftquestions'>Reporting periods up to</TD>
					<TD colspan='2'>{7}</TD>
				</TR>
				<TR>
					<TD class='leftquestions'>No. of scenarios</TD>
					<TD colspan='2'>{8}</TD>
				</TR>
				<TR>
					<TD class='leftquestions'>General ledger import</TD>
					<TD colspan='2'>{9}</TD>
				</TR>
				<TR>
					<TD class='leftquestions'>Tax effect accounting</TD>
					<TD colspan='2'>{10}</TD>
				</TR>
	</TABLE>
	<br>",new string[]{
				this.lblLicenceID.ToString(),
				this.lblLicenceStatusMessage.ToString(),
				this.lblLicenceType.ToString(),
				this.lblLicenceStatus.ToString(),
				this.lblUserAcounts.ToString(),
				this.lblEntities.ToString(),
				this.lblGroups.ToString(),
				this.lblPeriodEndDate.ToString(),
				this.lblScenarios.ToString(),
				this.lblGLImport.ToString(),
				this.lblTEA.ToString()});

			#endregion
			#region Licence Creation information
			HtmlStringBuilder.AppendFormat(@"<div class='NoteHeading'>Licence creation information</div>
				<TABLE id='Table3' cellSpacing='1' cellPadding='1' border='0'>
				<TR> 
					<TD class='leftquestions'>Date Created </TD>
					<TD>{0}</TD>
				</TR>
				<TR> 
					<TD class='leftquestions'>Licence hash key</TD>
					<TD>{1}</TD>
				</TR>
				<TR> 
					<TD class='leftquestions'>Created by</TD>
					<TD align='left' valign='top'>{2}</TD>
				</TR>
				<TR> 
					<TD class='leftquestions'><div class='NoteHeading'>Authorisation</div></TD>
					<TD>&nbsp;</TD>
				</TR>
				<TR> 
					<TD class='leftquestions'>Authorised name </TD>
					<TD>________________________________</TD>
				</TR>
				<TR> 
					<TD class='leftquestions'>Authorised signature </TD>
					<TD>________________________________</TD>
				</TR>
				<TR>
					<TD class='leftquestions'>Authorised date</TD>
					<TD>___/___/___</TD>
				</TR>
				</TABLE>
				<p><br>
				</p>
				</body>
				</html>
				",new string[]{
							this.lblLicenceDateCreated.ToString(),
							this.lblLicenceHASHKEY.ToString(),
							this.lblLicenceCreatedBy.ToString()});

			#endregion

			return HtmlStringBuilder;
		}
		#endregion

	}
	#endregion

	#region License Information --------------------------------------------------------
	public class NAblePdf : Doc
	{
		public NAblePdf() : base()
		{
			this.SetInfo(0, "License", "810-031-225-276-7825-321");
		}
	}
	#endregion

	
}
