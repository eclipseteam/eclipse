using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace InstallGUI
{
	/// <summary>
	/// Summary description for MigrationFinishedControl.
	/// </summary>
	public class MigrationFinishedControl : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label successfulCompleteLabel;
		private System.Windows.Forms.Label errorLoglabel;

		private string informationMessage = "IMPORTANT: Errors in Tax/Accounting Data\nMessage: Please READ this message carefully and take appropriate action.\n"
        + "Certain tax and accounting figures were incorrectly calculated in the previous version of N-ABLE and have been recomputed and corrected during the upgrade to N-ABLE 2008.2.\n"  
		+"A full report of the old errors and corrections made is available here {0}.\n\n"
		+"Please immediately forward this report to your Tax Manager and NOTIFY Oritax Technology Partners Pty Ltd on 1800 774 467 of this issue.";

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MigrationFinishedControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
	
			label1.Hide();
			label2.Hide();
			label3.Hide();
			errorLoglabel.Hide();
			successfulCompleteLabel.Location = new Point(8, 20);
			successfulCompleteLabel.Show();
		
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.successfulCompleteLabel = new System.Windows.Forms.Label();
			this.errorLoglabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(424, 32);
			this.label1.TabIndex = 0;
			this.label1.Text = "Unfortunately the upgrade has encounted a problem in upgrading to\n N-ABLE " +
                "Release 2008.2";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(424, 48);
			this.label2.TabIndex = 1;
			this.label2.Text = "Please contact Oritax Helpdesk on 1800 774 467 for assistance in resolving this i" +
				"ssue. It will assist our enquiries if you can e-mail the following error log to " +
				"support@Oritax.com.au: ";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 126);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(416, 64);
			this.label3.TabIndex = 2;
			this.label3.Text = "We apologise for the inconvenience you are experiencing and will attempt to resol" +
				"ve this issue as quickly as possible. Please note that you are able to continue " +
                "using N-ABLE 2007.2 in the meantime.";
			// 
			// successfulCompleteLabel
			// 
			this.successfulCompleteLabel.Location = new System.Drawing.Point(8, 168);
			this.successfulCompleteLabel.Name = "successfulCompleteLabel";
			this.successfulCompleteLabel.Size = new System.Drawing.Size(416, 23);
			this.successfulCompleteLabel.TabIndex = 3;
            this.successfulCompleteLabel.Text = "Congratulations, your system has been successfully upgraded to N-ABLE 2008.2";
			// 
			// errorLoglabel
			// 
			this.errorLoglabel.Location = new System.Drawing.Point(8, 104);
			this.errorLoglabel.Name = "errorLoglabel";
			this.errorLoglabel.Size = new System.Drawing.Size(416, 64);
			this.errorLoglabel.TabIndex = 4;
			// 
			// MigrationFinishedControl
			// 
			this.Controls.Add(this.errorLoglabel);
			this.Controls.Add(this.successfulCompleteLabel);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "MigrationFinishedControl";
			this.Size = new System.Drawing.Size(436, 192);
			this.ResumeLayout(false);

		}
		#endregion

		public void MigrationFailed(string errorLogPath)
		{
			successfulCompleteLabel.Hide();
			label1.Show();
			label2.Show();
            label3.Text = "We apologise for the inconvenience you are experiencing and will attempt to resolve this issue as quickly as possible. Please note that you are able to continue using N-ABLE 2007.2 in the meantime.";
			label3.Location = new Point(8, 128);
			label3.Show();
			errorLoglabel.Text = errorLogPath;
			errorLoglabel.Show();
		}

		public void MigrationNotifiableChanges(string changeLogPath)
		{
			label3.Text = string.Format(informationMessage, changeLogPath);
			label3.Location = new Point(8, 50);
			label3.Size = new Size(416, 170);
			label3.Show();
		}
	}
}
