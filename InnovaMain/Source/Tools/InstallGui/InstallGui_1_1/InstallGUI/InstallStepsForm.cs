using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using System.Data;

using Oritax.TaxSimp.SAS70Password;

using Oritax.TaxSimp.Administration.SystemUpdate;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.Utilities;


namespace InstallGUI
{ 
	
	/// <summary>
	///		Creates an instance of the InstallSteps form. This takes the user through 
	///		the necessary steps to install the N-Able product.
	/// </summary>
	public class InstallStepsForm : System.Windows.Forms.Form
	{
		public InstallMode IMode = InstallMode.Full;

		#region Private Members ------------------------------------------------------

		private bool installed = false;

		private enum CurrentSelection
		{
			InstallStep1,
			InstallStep2,
			InstallStep3,
			InstallStep4,
			InstallProgress,
			Installed
		}

		private InstallParameters installParameters;
		private Install install;
		private InstallLog installLog = null;

		private string step1Label = InstallConstants.INSTALL_STEP1; 
		private string step2Label = InstallConstants.INSTALL_STEP2; 
		private string step3Label = InstallConstants.INSTALL_STEP3; 
		private string step4Label = InstallConstants.INSTALL_STEP4; 
		private string installLabel = InstallConstants.INSTALL_LABEL; 

		private CurrentSelection currentSelection;
		private InstallSQLInfoControl installSQLInfo;
		private InstallGeneralInfoControl installGeneralInfo;
		private InstallAdministratorInfoControl installAdminInfo;
		private InstallDatabaseAdminInfoControl installDatabaseInfo;
		private InstallProgressControl installProgress;
		
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Button nextButton;
		private System.Windows.Forms.Button previousButton;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Label installInformationLabel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		#endregion //Private Members

		#region Contructors ----------------------------------------------------------

		public InstallStepsForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			initializeInstallParameters();

			installGeneralInfo = new InstallGeneralInfoControl();
			installGeneralInfo.Parent = this;
			installGeneralInfo.SetBounds(88,48, installGeneralInfo.Size.Width, installGeneralInfo.Size.Height);
		
			installSQLInfo = new InstallSQLInfoControl();
			installSQLInfo.Parent = this;
			installSQLInfo.SetBounds(88,48, installSQLInfo.Size.Width, installSQLInfo.Size.Height);

			installAdminInfo = new InstallAdministratorInfoControl();
			installAdminInfo.Parent = this;
			installAdminInfo.SetBounds(88,48, installAdminInfo.Size.Width, installAdminInfo.Size.Height);

			installDatabaseInfo = new InstallDatabaseAdminInfoControl();
			installDatabaseInfo.Parent = this;
			installDatabaseInfo.SetBounds(88,48, installDatabaseInfo.Size.Width, installDatabaseInfo.Size.Height);
		
			installProgress = new InstallProgressControl();
			installProgress.Parent = this;
			installProgress.SetBounds(88,48, installProgress.Size.Width, installProgress.Size.Height);

			install = new Install();
			install.InstallActionProgressUpdate += new InstallProgressUpdateHandler(install_InstallActionProgressUpdate);
			install.InstallStepProgressUpdate += new InstallProgressUpdateHandler(install_InstallStepProgressUpdate);
			install.InstallError += new InstallErrorHandler(install_InstallError);

			installGeneralInfo.Show();
			installAdminInfo.Hide();
			installDatabaseInfo.Hide();
			installSQLInfo.Hide();
			installProgress.Hide();
		}

		#endregion //Contructors

		#region Protected Methods ----------------------------------------------------

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#endregion //Protected Methods

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(InstallStepsForm));
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.previousButton = new System.Windows.Forms.Button();
			this.nextButton = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.installInformationLabel = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cancelButton = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pictureBox2
			// 
			this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(65)), ((System.Byte)(65)));
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.Location = new System.Drawing.Point(0, 0);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(88, 288);
			this.pictureBox2.TabIndex = 5;
			this.pictureBox2.TabStop = false;
			// 
			// previousButton
			// 
			this.previousButton.Enabled = false;
			this.previousButton.Location = new System.Drawing.Point(260, 256);
			this.previousButton.Name = "previousButton";
			this.previousButton.TabIndex = 1;
			this.previousButton.Text = "< Previous";
			this.previousButton.Click += new System.EventHandler(this.previousButton_Click);
			// 
			// nextButton
			// 
			this.nextButton.Location = new System.Drawing.Point(348, 256);
			this.nextButton.Name = "nextButton";
			this.nextButton.TabIndex = 0;
			this.nextButton.Text = "Next >";
			this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.panel1.Controls.Add(this.installInformationLabel);
			this.panel1.Location = new System.Drawing.Point(88, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(440, 40);
			this.panel1.TabIndex = 4;
			// 
			// installInformationLabel
			// 
			this.installInformationLabel.Location = new System.Drawing.Point(8, 8);
			this.installInformationLabel.Name = "installInformationLabel";
			this.installInformationLabel.Size = new System.Drawing.Size(416, 32);
			this.installInformationLabel.TabIndex = 0;
			this.installInformationLabel.Text = "Step 1 of 4 - Installation location";
			// 
			// groupBox1
			// 
			this.groupBox1.Location = new System.Drawing.Point(88, 240);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(424, 8);
			this.groupBox1.TabIndex = 3;
			this.groupBox1.TabStop = false;
			// 
			// cancelButton
			// 
			this.cancelButton.Location = new System.Drawing.Point(436, 256);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.TabIndex = 2;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
			// 
			// InstallStepsForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(520, 285);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.nextButton);
			this.Controls.Add(this.previousButton);
			this.Controls.Add(this.pictureBox2);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "InstallStepsForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "N-ABLE Install";
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		#region Private Methods ------------------------------------------------------

		private void nextButton_Click(object sender, System.EventArgs e)
		{
			installInformationLabel.ForeColor = Color.Black;
			
			switch (currentSelection)
			{
				case CurrentSelection.InstallStep1:
				{
					previousButton.Enabled = true;
					string errorText = "";
					if (checkGeneralInfo(out errorText))
					{
						installGeneralInfo.Hide();
						installSQLInfo.Show();
						installInformationLabel.Text = step2Label;
					}
					else
					{
						installInformationLabel.Text = errorText;
						installInformationLabel.ForeColor = Color.Red;
						currentSelection--;
					}
					break;
				}
				case CurrentSelection.InstallStep2:
				{
					string errorText = "";
					if (checkSQLInfo(out errorText))
					{
						installSQLInfo.Hide();
						installAdminInfo.Show();
						installInformationLabel.Text = step3Label;
					}
					else
					{
						installInformationLabel.Text = errorText;
						installInformationLabel.ForeColor = Color.Red;
						currentSelection--;
					}
					break;
				}
				case CurrentSelection.InstallStep3:
				{
					string errorText = "";
					if (checkAdministratorInfo(out errorText))
					{
						installAdminInfo.Hide();
						installDatabaseInfo.Show();
						installInformationLabel.Text = step4Label;
					}
					else
					{
						installInformationLabel.Text = errorText;
						installInformationLabel.ForeColor = Color.Red;
						currentSelection--;
					}
					break;
				}
				case CurrentSelection.InstallStep4:
				{
					string errorText = "";
					if (checkDatabaseAdminInfo(out errorText))
					{
						resetInstallProgressControl();
						installDatabaseInfo.Hide();
						installProgress.Show();
						nextButton.Text = "Install";
						installInformationLabel.Text = installLabel;
						installProgress.CurrentInstallStep = InstallConstants.INSTALL_READY; 
					}
					else
					{
						installInformationLabel.Text = errorText;
						installInformationLabel.ForeColor = Color.Red;
						currentSelection--;
					}
					break;
				}
				case CurrentSelection.InstallProgress:
				{
					nextButton.Enabled = false;
					previousButton.Enabled = false;

					getAllInstallSettings();
				
					if (checkInstallParameters())
					{
						installProgress.ProgressBarScale = 7;
						createInstallLog();

						bool ret;
                        string errorText = string.Empty;
						if(IMode == InstallMode.Full)
						{
							ret = install.InstallFullProduct(installParameters, out errorText);
						}
						else
						{
							ret = install.UpdateFullProduct(installParameters);
						}

						if (ret)
						{
							nextButton.Enabled = true;
							nextButton.Text = "Finished";
							installed = true;	
						}
						else
						{
							nextButton.Enabled = true;
							previousButton.Enabled = true;
                            installInformationLabel.Text = errorText;
                            installInformationLabel.ForeColor = Color.Red;
                            currentSelection--;
						}
						closeInstallLog();
					}
					break;
				}
				case CurrentSelection.Installed:
				{
					this.Close();
					break;
				}
			}
			currentSelection++;
		}

		private void cancelButton_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void previousButton_Click(object sender, System.EventArgs e)
		{
			currentSelection--;
			nextButton.Text = "Next >";
			installInformationLabel.ForeColor = Color.Black;

			installInformationLabel.ForeColor = Color.Black;
			
			switch (currentSelection)
			{
				case CurrentSelection.InstallStep1:
				{
					installSQLInfo.Hide();
					installGeneralInfo.Show();
					previousButton.Enabled = false;
					installInformationLabel.Text = step1Label;
					break;
				}
				case CurrentSelection.InstallStep2:
				{
					installAdminInfo.Hide();
					installSQLInfo.Show();
					installInformationLabel.Text = step2Label;
					break;
				}
				case CurrentSelection.InstallStep3:
				{
					installDatabaseInfo.Hide();
					installAdminInfo.Show();
					installInformationLabel.Text = step3Label;
					break;
				}
				case CurrentSelection.InstallStep4:
				{
					installProgress.Hide();
					installDatabaseInfo.Show();
					installInformationLabel.Text = step4Label;
					break;
				}
			}
		}

		private void getAllInstallSettings()
		{
			DirectoryInfo dirInfo = new DirectoryInfo(Directory.GetCurrentDirectory());

			if (string.Compare(dirInfo.Name, "assembly", true) == 0)
			{
				installParameters.installBase = Directory.GetParent(Directory.GetCurrentDirectory()).FullName;
				installParameters.installCode = Directory.GetCurrentDirectory();
			}
			else
			{
				installParameters.installBase = Directory.GetCurrentDirectory();
				installParameters.installCode = Path.Combine(installParameters.installBase, "bin");
			}

			installParameters.installPackageLocation = Path.Combine(installParameters.installBase, "Cab");
			installParameters.workingLocation = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "AllumeTemp");
			installParameters.installLocation = installGeneralInfo.InstallFolder;
			installParameters.websiteLocation = Path.Combine(installParameters.installLocation, installGeneralInfo.InstallName);
			
			installParameters.databaseName = installGeneralInfo.InstallName;
			installParameters.databaseServer = installSQLInfo.SQLServerName;
			installParameters.dbSysUserName = installDatabaseInfo.NableSystemAccountName;
			installParameters.dbSysPassword = installDatabaseInfo.NableSystemPassword;
			installParameters.dbAdminUserName = installSQLInfo.SQLAdminAccountName;
			installParameters.dbAdminPassword = installSQLInfo.SQLAdminPassword;
			installParameters.nableAdminUserName = installAdminInfo.NableAdminName;
			installParameters.nableAdminPassword = installAdminInfo.NableAdminPassword;


			installParameters.connectionString = "server=" + installParameters.databaseServer + ";database=" + installParameters.databaseName + ";uid=" + installParameters.dbSysUserName + ";pwd=" + installParameters.dbSysPassword;
			installParameters.adminConnectionString = "server=" + installParameters.databaseServer + ";database=" + installParameters.databaseName + ";uid=" + installParameters.dbAdminUserName +";pwd=" + installParameters.dbAdminPassword;
			installParameters.encryptedConnectionString = "server=" + installParameters.databaseServer + ";database=" + installParameters.databaseName + ";uid=" + Encryption.EncryptData(installParameters.dbSysUserName) + ";pwd=" + Encryption.EncryptData(installParameters.dbSysPassword);

            installParameters.commandLineInstall = false;

		}

		private bool checkGeneralInfo(out string errorText)
		{
			errorText = "";
			bool infoCorrectlyEntered = true;

			if (installGeneralInfo.InstallName.Length == 0)
			{
				errorText = "Please specify an Installation Name.";
				infoCorrectlyEntered = false;
			}
			if (!Utilities.CheckInstallName(installGeneralInfo.InstallName))
			{
				errorText = "Please specify an Installation Name that contains characters, numbers or '_' '#' only.";
				infoCorrectlyEntered = false;
			}
			if (installGeneralInfo.InstallFolder.Length == 0)
			{
				errorText = "Please specify an Install Location.";
				infoCorrectlyEntered = false;
			}

			return infoCorrectlyEntered;
		}

		private bool checkAdministratorInfo(out string errorText)
		{
			errorText = "";
			bool infoCorrectlyEntered = true;
			
			if (installAdminInfo.NableAdminName.Length == 0)
			{
				errorText = "Please specify an N-ABLE Administrator account.";
				infoCorrectlyEntered = false;
			}
			if (installAdminInfo.NableAdminPassword.Length == 0)
			{
				errorText = "Please specify a N-ABLE Administrator password.";
				infoCorrectlyEntered = false;
			}
			if (!installAdminInfo.ValidatePassword())
			{
				errorText = "The re-entered password does not match the original password.";
				infoCorrectlyEntered = false;
			}

            PasswordDS data = new PasswordDS();
            SAS70PasswordSM pwdSM = new SAS70PasswordSM();
            if (!pwdSM.CheckRulesForNewUser(installAdminInfo.NableAdminName,
                installAdminInfo.NableAdminPassword, data.Tables[PasswordDS.PasswordRulesTable]))
            {
                errorText = "The passwords that you have entered for the N-ABLE Administrator Account " +
                    "do not conform to N-ABLE password creation rules";
                infoCorrectlyEntered = false;
            }

			return infoCorrectlyEntered;
		}

		private bool checkSQLInfo(out string errorText)
		{
			bool infoCorrectlyEntered = true;
			errorText = "";

			if (installSQLInfo.SQLServerName.Length == 0)
			{
				errorText = "Please specify a SQLServer.";
				infoCorrectlyEntered = false;
			}
			if (installSQLInfo.SQLAdminAccountName.Length == 0)
			{
				errorText = "Please specify a SQL administrator account.";
				infoCorrectlyEntered = false;
			}
			
			
			return infoCorrectlyEntered;
		}

		private bool checkDatabaseAdminInfo(out string errorText)
		{
			bool infoCorrectlyEntered = true;
			errorText = "";

			if (installDatabaseInfo.NableSystemAccountName.Length == 0)
			{
				errorText = "Please specify a N-ABLE Database account.";
				infoCorrectlyEntered = false;
			}
			if (installDatabaseInfo.NableSystemPassword.Length == 0)
			{
				errorText = "Please specify a N-ABLE Database password.";
				infoCorrectlyEntered = false;
			}
			if (!installDatabaseInfo.ValidatePassword())
			{
				errorText = "The re-entered password does not match the original password for the N-ABLE Database Account .";
				infoCorrectlyEntered = false;
			}

			return infoCorrectlyEntered;
		}

		private bool checkInstallParameters()
		{
			bool installCanProceed = true;
			string errorText = "";

			if(!Directory.Exists(installGeneralInfo.InstallFolder))
			{
				try
				{
					Directory.CreateDirectory(installGeneralInfo.InstallFolder);
				}
				catch
				{
					errorText = "The install directory could not be created."; 
					installCanProceed = false;
				}
			}

            //if (!SqlLocator.SQLServerExists(installSQLInfo.SQLServerName, installSQLInfo.SQLAdminAccountName, installSQLInfo.SQLAdminPassword))
            //{
            //    errorText = "The SQL server instance could not be found";
            //    installCanProceed = false;
            //}

			if (!installCanProceed)
			{
				installProgress.CurrentInstallStep = errorText;
				installProgress.CurrentInstallStepForeColor = Color.Red;
			}

			return installCanProceed;
		}

		private void resetInstallProgressControl()
		{
			installProgress.CurrentInstallStep = "";
			installProgress.CurrentInstallAction = "";
			installProgress.CurrentInstallStepForeColor = Color.Black;
		}

		private void install_InstallActionProgressUpdate(object sender, InstallProgressEventArgs e)
		{
			writeToLog(e.Message);
			installProgress.CurrentInstallAction = e.Message;
			installProgress.Refresh();
			this.Refresh();
			Application.DoEvents();
		}

		private void install_InstallStepProgressUpdate(object sender, InstallProgressEventArgs e)
		{
			writeToLog(e.Message);
			installProgress.CurrentInstallStep = e.Message;
			installProgress.CurrentInstallStepForeColor = Color.Black;
			installProgress.CurrentInstallAction = "";
			installProgress.IncrementProgressBar(1);
			installProgress.Refresh();
			this.Refresh();
			Application.DoEvents();
		}

		private void install_InstallError(object sender, InstallErrorEventArgs e)
		{
			writeToLog(e.Message);
			installProgress.CurrentInstallStep = e.Message;
			installProgress.CurrentInstallStepForeColor = Color.Red;
			installProgress.CurrentInstallAction = "";
			installProgress.Refresh();
			this.Refresh();
			Application.DoEvents();
		}

		private void initializeInstallParameters()
		{
			installParameters.installBase = "";
			installParameters.installPackageLocation = "";
			installParameters.workingLocation = "";
			installParameters.websiteLocation = "";
			installParameters.installCode = "";
			installParameters.installLocation = "";

			installParameters.databaseName = "";
			installParameters.databaseServer = "";
			installParameters.dbSysUserName = "";
			installParameters.dbSysPassword = "";
			installParameters.dbAdminUserName = "";
			installParameters.dbAdminPassword = "";
			installParameters.nableAdminUserName = "";
			installParameters.nableAdminPassword = "";

			installParameters.changedbSysUser = false;
			installParameters.changeNableAdminUser = false;

			installParameters.connectionString = "";
			installParameters.adminConnectionString = "";
			installParameters.encryptedConnectionString = "";
		}

		private void createInstallLog()
		{
			try
			{
				installLog = new InstallLog(Path.Combine(installParameters.installLocation, installParameters.databaseName + "Installlog.txt"));
			}
			catch
			{
			}
		}

		private void closeInstallLog()
		{
			if (installLog != null)
			{
				installLog.Dispose();
			}
		}

		private void writeToLog(string message)
		{
			if (installLog != null)
			{
				installLog.AddtoLog(message);
			}
		}

		#endregion //Private Methods

		/// <summary>
		///		Gets whether the install procedure completed sucessfully.
		/// </summary>
		public bool Installed
		{
			get
			{
				return installed;
			}
		}

		
	}
}
