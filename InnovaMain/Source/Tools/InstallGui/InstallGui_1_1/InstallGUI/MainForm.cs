using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Windows.Forms;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
 
namespace InstallGUI
{
	public enum InstallMode
	{
		Full,
		Update,
		Repair,
		Migrate,
		ChangePermissions
	}
	
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		#region Private Members ------------------------------------------------------

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label2;

		// Other Variables
		private bool ShowCustomiseButton = false;
		private System.Windows.Forms.Label panelLabel;
		private System.Windows.Forms.Button fullInstallButton;
		private System.Windows.Forms.Label fullInstallInfoLabel;
		private System.Windows.Forms.PictureBox NABLELogo;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Label permissionsLabel;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button changePermissionsButton;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button updateButton;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		#endregion //Private Members

		#region Constructors ---------------------------------------------------------

		public MainForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		public MainForm(bool pShowCustomiseButton)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			ShowCustomiseButton = pShowCustomiseButton;

		}

		#endregion //Constructors

		#region Protected Methods ----------------------------------------------------

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#endregion //Protected Methods

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MainForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.panelLabel = new System.Windows.Forms.Label();
			this.fullInstallButton = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label2 = new System.Windows.Forms.Label();
			this.fullInstallInfoLabel = new System.Windows.Forms.Label();
			this.NABLELogo = new System.Windows.Forms.PictureBox();
			this.cancelButton = new System.Windows.Forms.Button();
			this.permissionsLabel = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.changePermissionsButton = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.updateButton = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.panelLabel);
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(488, 48);
			this.panel1.TabIndex = 3;
			// 
			// panelLabel
			// 
			this.panelLabel.Font = new System.Drawing.Font("Tahoma", 13.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.panelLabel.Location = new System.Drawing.Point(8, 8);
			this.panelLabel.Name = "panelLabel";
			this.panelLabel.Size = new System.Drawing.Size(488, 32);
			this.panelLabel.TabIndex = 0;
            this.panelLabel.Text = "N-ABLE Tax Management Solution Release 2008.2";
			// 
			// fullInstallButton
			// 
			this.fullInstallButton.Image = ((System.Drawing.Image)(resources.GetObject("fullInstallButton.Image")));
			this.fullInstallButton.Location = new System.Drawing.Point(112, 80);
			this.fullInstallButton.Name = "fullInstallButton";
			this.fullInstallButton.Size = new System.Drawing.Size(80, 72);
			this.fullInstallButton.TabIndex = 1;
			this.fullInstallButton.Click += new System.EventHandler(this.fullInstall_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Location = new System.Drawing.Point(0, 380);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(494, 8);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(200, 80);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(256, 16);
			this.label2.TabIndex = 14;
			this.label2.Text = "Product Installation";
			// 
			// fullInstallInfoLabel
			// 
			this.fullInstallInfoLabel.Location = new System.Drawing.Point(200, 104);
			this.fullInstallInfoLabel.Name = "fullInstallInfoLabel";
			this.fullInstallInfoLabel.Size = new System.Drawing.Size(280, 40);
			this.fullInstallInfoLabel.TabIndex = 16;
			this.fullInstallInfoLabel.Text = "<< Info >>";
			// 
			// NABLELogo
			// 
			this.NABLELogo.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(77)), ((System.Byte)(90)));
			this.NABLELogo.Image = ((System.Drawing.Image)(resources.GetObject("NABLELogo.Image")));
			this.NABLELogo.Location = new System.Drawing.Point(0, 48);
			this.NABLELogo.Name = "NABLELogo";
			this.NABLELogo.Size = new System.Drawing.Size(90, 440);
			this.NABLELogo.TabIndex = 19;
			this.NABLELogo.TabStop = false;
			// 
			// cancelButton
			// 
			this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cancelButton.Location = new System.Drawing.Point(408, 396);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.TabIndex = 0;
			this.cancelButton.Text = "Close";
			this.cancelButton.Click += new System.EventHandler(this.cancel_Click);
			// 
			// permissionsLabel
			// 
			this.permissionsLabel.Location = new System.Drawing.Point(200, 302);
			this.permissionsLabel.Name = "permissionsLabel";
			this.permissionsLabel.Size = new System.Drawing.Size(280, 40);
			this.permissionsLabel.TabIndex = 22;
			this.permissionsLabel.Text = "<< Info >>";
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label4.Location = new System.Drawing.Point(200, 280);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(256, 16);
			this.label4.TabIndex = 21;
			this.label4.Text = "Change Permissions";
			// 
			// changePermissionsButton
			// 
			this.changePermissionsButton.Image = ((System.Drawing.Image)(resources.GetObject("changePermissionsButton.Image")));
			this.changePermissionsButton.Location = new System.Drawing.Point(112, 280);
			this.changePermissionsButton.Name = "changePermissionsButton";
			this.changePermissionsButton.Size = new System.Drawing.Size(80, 72);
			this.changePermissionsButton.TabIndex = 3;
			this.changePermissionsButton.Click += new System.EventHandler(this.changePermissionsButton_Click);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(200, 204);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(280, 40);
			this.label3.TabIndex = 28;
			this.label3.Text = "label3";
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label5.Location = new System.Drawing.Point(200, 180);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(264, 16);
			this.label5.TabIndex = 27;
			this.label5.Text = "Product Upgrade";
			// 
			// updateButton
			// 
			this.updateButton.Image = ((System.Drawing.Image)(resources.GetObject("updateButton.Image")));
			this.updateButton.Location = new System.Drawing.Point(112, 180);
			this.updateButton.Name = "updateButton";
			this.updateButton.Size = new System.Drawing.Size(80, 72);
			this.updateButton.TabIndex = 26;
			this.updateButton.Click += new System.EventHandler(this.updateInstall_Click);
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(488, 423);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.updateButton);
			this.Controls.Add(this.permissionsLabel);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.changePermissionsButton);
			this.Controls.Add(this.NABLELogo);
			this.Controls.Add(this.fullInstallInfoLabel);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.fullInstallButton);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.cancelButton);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "N-ABLE Installation";
			this.Load += new System.EventHandler(this.mainForm_Load);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		#region Private Methods ------------------------------------------------------

		private void mainForm_Load(object sender, System.EventArgs e)
		{
			InitialiseForm();
		}
		private void InitialiseForm()
		{
			panelLabel.Text = InstallConstants.MAIN_TITLE;
			fullInstallInfoLabel.Text = InstallConstants.FULL_INSTALL_INFO;
			permissionsLabel.Text = InstallConstants.PERMISSIONS;
			label3.Text = InstallConstants.UPGRADE_INSTALL_INFO;

			if (!iisInstalled())
			{
				DialogResult result = MessageBox.Show(this, InstallConstants.IISNOTDETECTED_MESSAGE,InstallConstants.IISNOTDETECTED_TITLE,MessageBoxButtons.YesNo,MessageBoxIcon.Stop);
				if (result == DialogResult.No)
				{
					this.Close();
				}
			}
		}

		/// <summary>
		/// Test 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fullInstall_Click(object sender, System.EventArgs e)
		{
			this.Hide();
			InstallStepsForm installForm = new InstallStepsForm();
			installForm.ShowDialog();

			if (installForm.Installed)
			{
				this.Close();
			}
			else
			{
				this.Show();
			}
		}

		private void updateInstall_Click(object sender, System.EventArgs e)
		{
			this.Hide();
			UpdateStepsForm installForm = new UpdateStepsForm();
			installForm.ShowDialog();
			cancelButton.Text = "Finished";
			
			this.Show();
		}

		private void changePermissionsButton_Click(object sender, System.EventArgs e)
		{
			this.Hide();
			PermissionsForm permissionsForm = new PermissionsForm();
			permissionsForm.ShowDialog();
			
			this.Show();
		}

		private void cancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private bool iisInstalled()
		{
			return findRunningService("W3SVC");
		}

		private bool findRunningService(string serviceName)
		{
			bool serviceFound = false;

			ServiceController[] services= ServiceController.GetServices();
			
			for (int i = 0; i <  services.Length; i++)
			{
				if(services[i].ServiceName == serviceName)
				{
					serviceFound = true;
					break;
				}
			}

			return serviceFound;
		}

		#endregion // Private methods

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new MainForm());
		}
	}

}
