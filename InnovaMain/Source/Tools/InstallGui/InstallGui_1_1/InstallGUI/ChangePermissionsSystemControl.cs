using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Windows.Forms;

using Oritax.TaxSimp.Administration.SystemUpdate;

namespace InstallGUI
{
	/// <summary>
	/// Summary description for ChangePermissionsControl.
	/// </summary>
	public class ChangePermissionsSystemControl : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.CheckBox changeSystemAccountCheckBox;
		private System.Windows.Forms.TextBox reenterSystemPasswordTextBox;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
		private System.Windows.Forms.TextBox nableSystemAccountTextBox;
		private System.Windows.Forms.TextBox nableSystemPasswordTextBox;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ChangePermissionsSystemControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.changeSystemAccountCheckBox = new System.Windows.Forms.CheckBox();
			this.reenterSystemPasswordTextBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.nableSystemAccountTextBox = new System.Windows.Forms.TextBox();
			this.nableSystemPasswordTextBox = new System.Windows.Forms.TextBox();
			this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label1.Enabled = false;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 104);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(88, 16);
			this.label1.TabIndex = 68;
			this.label1.Text = "Password";
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label2.Enabled = false;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(16, 64);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(88, 16);
			this.label2.TabIndex = 67;
			this.label2.Text = "Login name";
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.changeSystemAccountCheckBox);
			this.groupBox1.Controls.Add(this.reenterSystemPasswordTextBox);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.nableSystemAccountTextBox);
			this.groupBox1.Controls.Add(this.nableSystemPasswordTextBox);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(8, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(416, 216);
			this.groupBox1.TabIndex = 7;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "N-ABLE Database Administrator";
			// 
			// changeSystemAccountCheckBox
			// 
			this.changeSystemAccountCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.changeSystemAccountCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.changeSystemAccountCheckBox.Location = new System.Drawing.Point(16, 24);
			this.changeSystemAccountCheckBox.Name = "changeSystemAccountCheckBox";
			this.changeSystemAccountCheckBox.Size = new System.Drawing.Size(176, 24);
			this.changeSystemAccountCheckBox.TabIndex = 4;
			this.changeSystemAccountCheckBox.Text = "Change System Account";
			this.changeSystemAccountCheckBox.CheckedChanged += new System.EventHandler(this.changeSystemAccountCheckBox_CheckedChanged);
			// 
			// reenterSystemPasswordTextBox
			// 
			this.reenterSystemPasswordTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.reenterSystemPasswordTextBox.Enabled = false;
			this.reenterSystemPasswordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.reenterSystemPasswordTextBox.Location = new System.Drawing.Point(176, 144);
			this.reenterSystemPasswordTextBox.Name = "reenterSystemPasswordTextBox";
			this.reenterSystemPasswordTextBox.PasswordChar = '*';
			this.reenterSystemPasswordTextBox.Size = new System.Drawing.Size(192, 20);
			this.reenterSystemPasswordTextBox.TabIndex = 7;
			this.reenterSystemPasswordTextBox.Text = "";
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label3.Enabled = false;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(16, 144);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(136, 16);
			this.label3.TabIndex = 61;
			this.label3.Text = "Re-enter password";
			// 
			// nableSystemAccountTextBox
			// 
			this.nableSystemAccountTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.nableSystemAccountTextBox.Enabled = false;
			this.nableSystemAccountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.nableSystemAccountTextBox.Location = new System.Drawing.Point(176, 64);
			this.nableSystemAccountTextBox.Name = "nableSystemAccountTextBox";
			this.nableSystemAccountTextBox.Size = new System.Drawing.Size(192, 20);
			this.nableSystemAccountTextBox.TabIndex = 5;
			this.nableSystemAccountTextBox.Text = "";
			// 
			// nableSystemPasswordTextBox
			// 
			this.nableSystemPasswordTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.nableSystemPasswordTextBox.Enabled = false;
			this.nableSystemPasswordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.nableSystemPasswordTextBox.Location = new System.Drawing.Point(176, 104);
			this.nableSystemPasswordTextBox.Name = "nableSystemPasswordTextBox";
			this.nableSystemPasswordTextBox.PasswordChar = '*';
			this.nableSystemPasswordTextBox.Size = new System.Drawing.Size(192, 20);
			this.nableSystemPasswordTextBox.TabIndex = 6;
			this.nableSystemPasswordTextBox.Text = "";
			// 
			// ChangePermissionsSystemControl
			// 
			this.Controls.Add(this.groupBox1);
			this.Name = "ChangePermissionsSystemControl";
			this.Size = new System.Drawing.Size(432, 224);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		#region Properties -----------------------------------------------------------

		public string NableSystemDatabaseAccount
		{
			get
			{
				return nableSystemAccountTextBox.Text;
			}
		}

		public string NableSystemDatabasePassword
		{
			get
			{
				return nableSystemPasswordTextBox.Text;
			}
		}

		public string NableSystemDatabaseReenteredPassword
		{
			get
			{
				return reenterSystemPasswordTextBox.Text;
			}
		}
		public bool ChangeNableSystemDatbaseAccount
		{
			get
			{
				return changeSystemAccountCheckBox.Checked;
			}
		}

		#endregion //Properties

		#region Private Methods ------------------------------------------------------

		private void changeSystemAccountCheckBox_CheckedChanged(object sender, System.EventArgs e)
		{
			nableSystemAccountTextBox.Enabled = changeSystemAccountCheckBox.Checked;
			nableSystemPasswordTextBox.Enabled = changeSystemAccountCheckBox.Checked;
			reenterSystemPasswordTextBox.Enabled = changeSystemAccountCheckBox.Checked;
			label1.Enabled = changeSystemAccountCheckBox.Checked;
			label2.Enabled = changeSystemAccountCheckBox.Checked;
			label3.Enabled = changeSystemAccountCheckBox.Checked;
		}

		#endregion //Private Methods
	}
}
