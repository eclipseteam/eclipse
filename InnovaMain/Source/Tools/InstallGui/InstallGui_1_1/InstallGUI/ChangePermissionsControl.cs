using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Windows.Forms;

using Oritax.TaxSimp.Administration.SystemUpdate;

namespace InstallGUI
{
	/// <summary>
	/// Summary description for ChangePermissionsControl.
	/// </summary>
	public class ChangePermissionsControl : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox sqlAdminTextBox;
		private System.Windows.Forms.TextBox sqlAdminPassword;
		private System.Windows.Forms.TextBox sqlServerNameTextBox;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Button browseButton;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox installFolderTextBox;
		private System.Windows.Forms.Button browseFolderButton;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
		private System.Windows.Forms.GroupBox groupBox1;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ChangePermissionsControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			sqlServerNameTextBox.Text =InstallConstants.SQL_SERVER_NAME;
			installFolderTextBox.Text = determineDefaultInstallLocation();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.sqlAdminTextBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.sqlAdminPassword = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.sqlServerNameTextBox = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.browseButton = new System.Windows.Forms.Button();
			this.label9 = new System.Windows.Forms.Label();
			this.installFolderTextBox = new System.Windows.Forms.TextBox();
			this.browseFolderButton = new System.Windows.Forms.Button();
			this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// sqlAdminTextBox
			// 
			this.sqlAdminTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.sqlAdminTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.sqlAdminTextBox.Location = new System.Drawing.Point(144, 24);
			this.sqlAdminTextBox.Name = "sqlAdminTextBox";
			this.sqlAdminTextBox.Size = new System.Drawing.Size(216, 20);
			this.sqlAdminTextBox.TabIndex = 5;
			this.sqlAdminTextBox.Text = "";
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label4.Location = new System.Drawing.Point(16, 24);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(104, 16);
			this.label4.TabIndex = 71;
			this.label4.Text = "Login name";
			// 
			// sqlAdminPassword
			// 
			this.sqlAdminPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.sqlAdminPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.sqlAdminPassword.Location = new System.Drawing.Point(144, 64);
			this.sqlAdminPassword.Name = "sqlAdminPassword";
			this.sqlAdminPassword.PasswordChar = '*';
			this.sqlAdminPassword.Size = new System.Drawing.Size(216, 20);
			this.sqlAdminPassword.TabIndex = 6;
			this.sqlAdminPassword.Text = "";
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label5.Location = new System.Drawing.Point(16, 64);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(104, 16);
			this.label5.TabIndex = 73;
			this.label5.Text = "Password";
			// 
			// sqlServerNameTextBox
			// 
			this.sqlServerNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.sqlServerNameTextBox.Location = new System.Drawing.Point(8, 64);
			this.sqlServerNameTextBox.Name = "sqlServerNameTextBox";
			this.sqlServerNameTextBox.Size = new System.Drawing.Size(312, 20);
			this.sqlServerNameTextBox.TabIndex = 3;
			this.sqlServerNameTextBox.Text = "";
			// 
			// label8
			// 
			this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label8.Location = new System.Drawing.Point(8, 48);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(184, 16);
			this.label8.TabIndex = 77;
			this.label8.Text = "SQL Server Name";
			// 
			// browseButton
			// 
			this.browseButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.browseButton.Location = new System.Drawing.Point(352, 64);
			this.browseButton.Name = "browseButton";
			this.browseButton.TabIndex = 4;
			this.browseButton.Text = "Browse..";
			this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
			// 
			// label9
			// 
			this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label9.Location = new System.Drawing.Point(8, 8);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(328, 16);
			this.label9.TabIndex = 80;
			this.label9.Text = "Installation Location";
			// 
			// installFolderTextBox
			// 
			this.installFolderTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.installFolderTextBox.Location = new System.Drawing.Point(8, 24);
			this.installFolderTextBox.Name = "installFolderTextBox";
			this.installFolderTextBox.Size = new System.Drawing.Size(312, 20);
			this.installFolderTextBox.TabIndex = 1;
			this.installFolderTextBox.Text = "";
			// 
			// browseFolderButton
			// 
			this.browseFolderButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.browseFolderButton.Location = new System.Drawing.Point(352, 24);
			this.browseFolderButton.Name = "browseFolderButton";
			this.browseFolderButton.TabIndex = 2;
			this.browseFolderButton.Text = "Browse ...";
			this.browseFolderButton.Click += new System.EventHandler(this.browseFolderButton_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.sqlAdminPassword);
			this.groupBox1.Controls.Add(this.sqlAdminTextBox);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(8, 96);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(416, 112);
			this.groupBox1.TabIndex = 81;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "SQL Server System Administrator";
			// 
			// ChangePermissionsControl
			// 
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.installFolderTextBox);
			this.Controls.Add(this.browseFolderButton);
			this.Controls.Add(this.browseButton);
			this.Controls.Add(this.sqlServerNameTextBox);
			this.Controls.Add(this.label8);
			this.Name = "ChangePermissionsControl";
			this.Size = new System.Drawing.Size(432, 224);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		#region Properties -----------------------------------------------------------

		public string SQLServerName 
		{
			get
			{
				return sqlServerNameTextBox.Text;
			}
		}

		public string SQLAdminAccount
		{
			get
			{
				return sqlAdminTextBox.Text;
			}
		}

		public string SQLAdminPassword
		{
			get
			{
				return sqlAdminPassword.Text;
			}
		}

		public string InstallationLocation
		{
			get
			{
				return installFolderTextBox.Text;
			}
		}

		#endregion //Properties

		#region Private Methods ------------------------------------------------------

		private void browseButton_Click(object sender, System.EventArgs e)
		{
			string[] sqls = SqlLocator.GetServers();
			SQLSelectorForm selectorForm = new SQLSelectorForm(sqls);

			selectorForm.ShowDialog();
			if (!selectorForm.DialogCancelled)
			{
				sqlServerNameTextBox.Text = selectorForm.SelectedSQLServer;
			}
		}

		private void browseFolderButton_Click(object sender, System.EventArgs e)
		{
			folderBrowserDialog.SelectedPath = installFolderTextBox.Text;
			folderBrowserDialog.ShowDialog();

			string selectedFolder = folderBrowserDialog.SelectedPath;

			if (!Directory.Exists(selectedFolder))
			{
				Directory.CreateDirectory(selectedFolder);
			}
			installFolderTextBox.Text = selectedFolder;	
		}

		private string determineDefaultInstallLocation()
		{	string programFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
			string defaultInstallLocation = Path.Combine(programFiles, InstallConstants.PROGRAM_FILES_ALLUME+"\\"+InstallConstants.INSTALL_FOLDER_NAME);
			return defaultInstallLocation;
		}
		
		#endregion //Private Methods
	}
}
