using System;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using System.Data;

using Oritax.TaxSimp.Administration.SystemUpdate;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;

using Oritax.TaxSimp.UpdateUtilities;


namespace InstallGUI
{
	/// <summary>
	///		Creates an instance of the InstallSteps form. This takes the user through 
	///		the necessary steps to install the N-Able product.
	/// </summary>
	public class UpdateStepsForm : System.Windows.Forms.Form
	{
		#region Private Members ------------------------------------------------------

		public enum CurrentSelection
		{
			Welcome,
			InstallStep1,
			InstallStep2,
			InstallProgress,
			Finished
		}

	
		private bool migrationFailed = false;
		private bool migrationNotifiableChanges = false;

		private string migrationErrorText = InstallConstants.UPGRADE_MIGRATION_ERROR; 

		private string welcomeLabel = InstallConstants.UPGRADE_WELCOME; 
		private string step1Label =InstallConstants.UPGRADE_STEP1; 
		private string step2Label = InstallConstants.UPGRADE_STEP2; 
		private string step3Label = InstallConstants.UPGRADE_STEP3; 
		private string successfulLabel = InstallConstants.UPGRADE_SUCCESSFUL; 
		private string migrationFailedLabel = InstallConstants.UPGRADE_MIGRATION_FAILED; 

		private CurrentSelection currentSelection;
		private WelcomeControl introduction;
		private UpgradeInfoControl generalInfo;
		private UpgradeSQLInfoControl sqlInfo;
		private MigrateProgressControl migrateProgress;
		private MigrationFinishedControl migrateFinished;
		
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Button nextButton;
		private System.Windows.Forms.Button previousButton;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Label updateInformationLabel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		#endregion //Private Members

		#region Contructors ----------------------------------------------------------

		public UpdateStepsForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			introduction = new WelcomeControl();
			introduction.Parent = this;
			introduction.SetBounds(88,48, introduction.Size.Width, introduction.Size.Height);

			generalInfo = new UpgradeInfoControl();
			generalInfo.Parent = this;
			generalInfo.SetBounds(88,48, generalInfo.Size.Width, generalInfo.Size.Height);

			sqlInfo = new UpgradeSQLInfoControl();
			sqlInfo.Parent = this;
			sqlInfo.SetBounds(88,48, sqlInfo.Size.Width, sqlInfo.Size.Height);
	
			migrateProgress = new MigrateProgressControl();
			migrateProgress.Parent = this;
			migrateProgress.SetBounds(88,48, migrateProgress.Size.Width, migrateProgress.Size.Height);
			migrateProgress.UpdateCompleted += new UpdateProgressStepUpdateHandler(migrateProgress_UpdateCompleted);

			migrateFinished = new MigrationFinishedControl();
			migrateFinished.Parent = this;
			migrateFinished.SetBounds(88,48, migrateFinished.Size.Width, migrateFinished.Size.Height);

			updateInformationLabel.Text = welcomeLabel;
			introduction.Show();
			sqlInfo.Hide();
			generalInfo.Hide();
			migrateProgress.Hide();
			migrateFinished.Hide();
		}

		#endregion //Contructors

		#region Protected Methods ----------------------------------------------------

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#endregion //Protected Methods

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(UpdateStepsForm));
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.previousButton = new System.Windows.Forms.Button();
			this.nextButton = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.updateInformationLabel = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cancelButton = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pictureBox2
			// 
			this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left)));
			this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(65)), ((System.Byte)(65)));
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.Location = new System.Drawing.Point(0, 0);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(88, 288);
			this.pictureBox2.TabIndex = 22;
			this.pictureBox2.TabStop = false;
			// 
			// previousButton
			// 
			this.previousButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.previousButton.Enabled = false;
			this.previousButton.Location = new System.Drawing.Point(264, 256);
			this.previousButton.Name = "previousButton";
			this.previousButton.TabIndex = 1;
			this.previousButton.Text = "< Previous";
			this.previousButton.Click += new System.EventHandler(this.previousButton_Click);
			// 
			// nextButton
			// 
			this.nextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.nextButton.Location = new System.Drawing.Point(352, 256);
			this.nextButton.Name = "nextButton";
			this.nextButton.TabIndex = 0;
			this.nextButton.Text = "Next >";
			this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.Controls.Add(this.updateInformationLabel);
			this.panel1.Location = new System.Drawing.Point(88, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(440, 40);
			this.panel1.TabIndex = 40;
			// 
			// updateInformationLabel
			// 
			this.updateInformationLabel.BackColor = System.Drawing.Color.White;
			this.updateInformationLabel.Location = new System.Drawing.Point(8, 12);
			this.updateInformationLabel.Name = "updateInformationLabel";
			this.updateInformationLabel.Size = new System.Drawing.Size(384, 28);
			this.updateInformationLabel.TabIndex = 0;
			this.updateInformationLabel.Text = "General product installation information";
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Location = new System.Drawing.Point(88, 240);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(428, 8);
			this.groupBox1.TabIndex = 41;
			this.groupBox1.TabStop = false;
			// 
			// cancelButton
			// 
			this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cancelButton.Location = new System.Drawing.Point(440, 256);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.TabIndex = 2;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
			// 
			// UpdateStepsForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(524, 285);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.nextButton);
			this.Controls.Add(this.previousButton);
			this.Controls.Add(this.pictureBox2);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "UpdateStepsForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "N-ABLE Upgrade";
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		#region Private Methods ------------------------------------------------------

		private void nextButton_Click(object sender, System.EventArgs e)
		{
			updateInformationLabel.ForeColor = Color.Black;
			switch (currentSelection)
			{
				case CurrentSelection.Welcome:
				{
					introduction.Hide();
					updateInformationLabel.Text = step1Label;
					generalInfo.Show();
					previousButton.Enabled = true;
					break;
				}
				case CurrentSelection.InstallStep1:
				{
					string errorText;
					if (checkUpgradeInfo(out errorText))
					{
						generalInfo.Hide();
						updateInformationLabel.Text = step2Label;
						sqlInfo.Show();
						nextButton.Text = "Upgrade";
					}
					else
					{
						updateInformationLabel.Text = errorText;
						updateInformationLabel.ForeColor = Color.Red;
						currentSelection--;
					}
					break;
				}
				case CurrentSelection.InstallStep2:
				{
					string errorText;
					if (checkSQLInfo(out errorText))
					{
						sqlInfo.Hide();
						updateInformationLabel.Text = step3Label;
						migrateProgress.Show();
						this.Refresh();

						previousButton.Enabled = false;
						nextButton.Enabled = false;

						migrateProgress.InstallParameters = getInstallationArguments();
						this.Cursor = Cursors.WaitCursor;
						bool updateStarted = true;
						try
						{
							updateStarted = migrateProgress.UpdateInstallation();	
						}
						catch (Exception ex)
						{
							updateInformationLabel.Text = ex.Message;
							updateInformationLabel.ForeColor = Color.Red;
							updateStarted = false;
						}

						if (!updateStarted)
						{
							this.Cursor = Cursors.Default;
							nextButton.Enabled = true;
							previousButton.Enabled = true;
							currentSelection--;
						}
					}
					else
					{
						updateInformationLabel.Text = errorText;
						updateInformationLabel.ForeColor = Color.Red;
						currentSelection--;
					}

					break;
				}
				case CurrentSelection.InstallProgress:
				{
					migrateProgress.Hide();
					updateInformationLabel.Text = successfulLabel;
					if (migrationFailed)
					{
						migrateFinished.MigrationFailed(Path.Combine(generalInfo.TargetLocation, "ErrorLog.txt"));
						updateInformationLabel.Text = migrationFailedLabel;
					}
					else if (migrationNotifiableChanges)
					{
						migrateFinished.MigrationNotifiableChanges(Path.Combine(generalInfo.TargetLocation, "Upgrade Diagnostic Report.pdf"));
					}
					migrateFinished.Show();

					nextButton.Text = "Finished";
					break;
				}
				case CurrentSelection.Finished:
				{
					this.Close();
					break;
				}
			}
			currentSelection++;
		}

		private void previousButton_Click(object sender, System.EventArgs e)
		{
			switch (currentSelection)
			{
				case CurrentSelection.Welcome:
				{
					break;
				}
				case CurrentSelection.InstallStep1:
				{
					generalInfo.Hide();
					updateInformationLabel.Text = welcomeLabel;
					introduction.Show();
					previousButton.Enabled = false;
					break;
				}
				case CurrentSelection.InstallStep2:
				{
					sqlInfo.Hide();
					updateInformationLabel.Text = step1Label;
					generalInfo.Show();
					nextButton.Text = "Next >";
					break;
				}
			}
			currentSelection--;
		}

		private bool checkUpgradeInfo(out string errorText)
		{
			errorText = "";
			bool infoCorrectlyEntered = true;

			if (!File.Exists(Path.Combine(generalInfo.SourceLocation, "web.config")))
			{
				errorText = "Unable to locate a web.config file. Please specify a source location that corresponds to an N-ABLE installation";
				infoCorrectlyEntered = false;
			}

			if (generalInfo.SourceLocation.Length == 0)
			{
				errorText = "Please specify a source Location.";
				infoCorrectlyEntered = false;
			}

			if (generalInfo.TargetLocation.Length == 0)
			{
				errorText = "Please specify a target Location.";
				infoCorrectlyEntered = false;
			}

			return infoCorrectlyEntered;
		}

		private bool checkSQLInfo(out string errorText)
		{
			bool infoCorrectlyEntered = true;
			errorText = "";

			if (sqlInfo.Server.Length == 0)
			{
				errorText = "Please specify a SQLServer.";
				infoCorrectlyEntered = false;
			}
			if (sqlInfo.AdminAccount.Length == 0)
			{
				errorText = "Please specify a SQL administrator account.";
				infoCorrectlyEntered = false;
			}
			
			return infoCorrectlyEntered;
		}

		private InstallParameters getInstallationArguments()
		{
			InstallParameters args = new InstallParameters();
			DirectoryInfo dirInfo = new DirectoryInfo(Directory.GetCurrentDirectory());

			if (string.Compare(dirInfo.Name, "assembly", true) == 0)
			{
				args.installBase = Directory.GetParent(Directory.GetCurrentDirectory()).FullName;
				args.installCode = Directory.GetCurrentDirectory();
			}
			else
			{
				args.installBase = Directory.GetCurrentDirectory();
				args.installCode = Path.Combine(args.installBase, "bin");
			}
			
			args.installPackageLocation = Path.Combine(args.installBase, "Cab");
			args.workingLocation = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "AllumeTemp");

			args.dbAdminUserName = sqlInfo.AdminAccount;
			args.dbAdminPassword = sqlInfo.AdminPassword;
			args.databaseServer = sqlInfo.Server;
			args.websiteLocation = generalInfo.SourceLocation;
			args.installLocation = Directory.GetParent(args.websiteLocation).FullName;

			args.sourceLocation = generalInfo.SourceLocation;
			args.targetLocation = generalInfo.TargetLocation;
			args.sourceDataBaseName = DatabaseManager.GetDatabaseNameFromPath(generalInfo.SourceLocation);
			args.targetDataBaseName = DatabaseManager.GetDatabaseNameFromPath(generalInfo.TargetLocation);

			getConnectionStringAndUserDetails(ref args);
			return args;
		}

		private void cancelButton_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void getConnectionStringAndUserDetails(ref InstallParameters args)
		{
			string webConfigFile = Path.Combine(args.websiteLocation, "web.config");

			args.encryptedConnectionString = WebDeployment.GetWebConfigValue(webConfigFile, "", "DBConnectionString");
			
			string[] connectionProperties = args.encryptedConnectionString.Split( ';' );

			// Locate the encrypted uid and pwd in the configuration file.
			string userName = "";
			string password = "";
			string databaseName = "";

			foreach(string property in connectionProperties)
			{
				if(property.Trim().StartsWith("uid"))
				{
					userName = property.Substring(property.IndexOf("=") + 1);
					if(userName != "")
						userName = Encryption.DecryptData(userName);
				}
				else if (property.Trim().StartsWith("pwd"))
				{
					password = property.Substring(property.IndexOf("=") + 1);
					if(password != "")
						password = Encryption.DecryptData(password);
				}
				else if (property.Trim().StartsWith("database"))
				{
					databaseName = property.Substring(property.IndexOf("=") + 1);
				}
			}
			
			args.databaseName = databaseName;
			args.dbSysUserName = userName;
			args.dbSysPassword = password;
			args.databaseName = databaseName;
			// Build an unencrypted version of the connection string for system usage.
			string connection = null;
			foreach(string property in connectionProperties)
			{
				if(property.Trim().StartsWith("uid"))
				{
					connection += "uid=" + userName + ";";
				}
				else if(property.Trim().StartsWith("pwd"))
				{
					connection += "pwd=" + password + ";";
				}
				else
					connection += property + ";";
			}

			args.connectionString = connection;
		}

		private void migrateProgress_UpdateCompleted(object sender, UpdateProgressStepEventArgs e)
		{
			//migrationInProgress = false;

			if (migrateProgress.ErrorOccurred)
			{
				updateInformationLabel.Text = migrationErrorText;
				updateInformationLabel.ForeColor = Color.Red;
				migrationFailed = true;
			}

			if (migrateProgress.MigrationNotifiableChanges)
			{
				migrationNotifiableChanges = true;
			}

			this.Cursor = Cursors.Default;
			nextButton.Text = "Next >";
			nextButton.Enabled = true;
		}
		#endregion //Private Methods

	}
}
