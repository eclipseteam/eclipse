using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace InstallGUI
{
	/// <summary>
	/// Summary description for UpgradeInfoControl.
	/// </summary>
	public class UpgradeInfoControl : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button V3BrowseButton;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button V51BrowseButton;
		private System.Windows.Forms.TextBox sourceLocationTextBox;
		private System.Windows.Forms.TextBox targetLocationTextBox;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public UpgradeInfoControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.sourceLocationTextBox = new System.Windows.Forms.TextBox();
			this.V3BrowseButton = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.label1 = new System.Windows.Forms.Label();
			this.targetLocationTextBox = new System.Windows.Forms.TextBox();
			this.V51BrowseButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// sourceLocationTextBox
			// 
			this.sourceLocationTextBox.Location = new System.Drawing.Point(8, 48);
			this.sourceLocationTextBox.Name = "sourceLocationTextBox";
			this.sourceLocationTextBox.Size = new System.Drawing.Size(320, 20);
			this.sourceLocationTextBox.TabIndex = 0;
			this.sourceLocationTextBox.Text =InstallConstants.UPGRADE_INSTALL_FROM_PATH; 
			// 
			// V3BrowseButton
			// 
			this.V3BrowseButton.Location = new System.Drawing.Point(344, 48);
			this.V3BrowseButton.Name = "V3BrowseButton";
			this.V3BrowseButton.TabIndex = 1;
			this.V3BrowseButton.Text = "Browse...";
			this.V3BrowseButton.Click += new System.EventHandler(this.SourceBrowseButton_Click);
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(8, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(208, 16);
			this.label2.TabIndex = 7;
			this.label2.Text = InstallConstants.UPGRADE_INSTALL_FROM_LOCATION_TEXT; 
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 96);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(208, 16);
			this.label1.TabIndex = 8;
			this.label1.Text = InstallConstants.UPGRADE_INSTALL_TO_LOCATION_TEXT ;
			// 
			// targetLocationTextBox
			// 
			this.targetLocationTextBox.Location = new System.Drawing.Point(8, 128);
			this.targetLocationTextBox.Name = "targetLocationTextBox";
			this.targetLocationTextBox.Size = new System.Drawing.Size(320, 20);
			this.targetLocationTextBox.TabIndex = 2;
			this.targetLocationTextBox.Text = InstallConstants.UPGRADE_INSTALL_TO_PATH; 
			// 
			// V51BrowseButton
			// 
			this.V51BrowseButton.Location = new System.Drawing.Point(344, 128);
			this.V51BrowseButton.Name = "V51BrowseButton";
			this.V51BrowseButton.TabIndex = 3;
			this.V51BrowseButton.Text = "Browse...";
			this.V51BrowseButton.Click += new System.EventHandler(this.TargetBrowseButton_Click);
			// 
			// UpgradeInfoControl
			// 
			this.Controls.Add(this.V51BrowseButton);
			this.Controls.Add(this.targetLocationTextBox);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.V3BrowseButton);
			this.Controls.Add(this.sourceLocationTextBox);
			this.Name = "UpgradeInfoControl";
			this.Size = new System.Drawing.Size(432, 192);
			this.ResumeLayout(false);

		}
		#endregion

		private void SourceBrowseButton_Click(object sender, System.EventArgs e)
		{
			folderBrowserDialog.SelectedPath = sourceLocationTextBox.Text;
			folderBrowserDialog.ShowDialog();

			sourceLocationTextBox.Text = folderBrowserDialog.SelectedPath;
		}

		private void TargetBrowseButton_Click(object sender, System.EventArgs e)
		{
			folderBrowserDialog.SelectedPath = targetLocationTextBox.Text;
			folderBrowserDialog.ShowDialog();

			targetLocationTextBox.Text = folderBrowserDialog.SelectedPath;
		}

		public string SourceLocation
		{
			get
			{
				return sourceLocationTextBox.Text;
			}
		}

		public string TargetLocation
		{
			get
			{
				return targetLocationTextBox.Text;
			}
		}
	}
}
