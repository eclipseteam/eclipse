using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace InstallGUI
{
	/// <summary>
	/// Summary description for SQLSelectorForm.
	/// </summary>
	public class SQLSelectorForm : System.Windows.Forms.Form
	{
		private bool cancelled = false;
		
		private System.Windows.Forms.ListBox SQLlistBox;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button OKButton;
		private System.Windows.Forms.Button cancelButton;
		private Size originalFormSize;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public SQLSelectorForm(string[] sqls)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.originalFormSize = this.Size;

			if (sqls != null)
			{
				SQLlistBox.Items.AddRange(sqls);
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SQLSelectorForm));
			this.SQLlistBox = new System.Windows.Forms.ListBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.OKButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// SQLlistBox
			// 
			this.SQLlistBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.SQLlistBox.Location = new System.Drawing.Point(8, 8);
			this.SQLlistBox.Name = "SQLlistBox";
			this.SQLlistBox.Size = new System.Drawing.Size(224, 225);
			this.SQLlistBox.TabIndex = 0;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Location = new System.Drawing.Point(0, 240);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(232, 8);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			// 
			// OKButton
			// 
			this.OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.OKButton.Location = new System.Drawing.Point(76, 260);
			this.OKButton.Name = "OKButton";
			this.OKButton.TabIndex = 1;
			this.OKButton.Text = "OK";
			this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
			// 
			// cancelButton
			// 
			this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cancelButton.Location = new System.Drawing.Point(156, 260);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.TabIndex = 2;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
			// 
			// SQLSelectorForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(240, 293);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.OKButton);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.SQLlistBox);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SQLSelectorForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "SQL Server Selector";
			this.Resize += new System.EventHandler(this.SQLSelectorForm_Resize);
			this.ResumeLayout(false);

		}
		#endregion

		private void cancelButton_Click(object sender, System.EventArgs e)
		{
			cancelled = true;
			this.Close();
		}

		private void OKButton_Click(object sender, System.EventArgs e)
		{
			if (SQLlistBox.SelectedIndex == -1)
			{
				MessageBox.Show(this, "Please select a SQL server");
			}
			else
			{
				this.Close();
			}
		}

		private void SQLSelectorForm_Resize(object sender, System.EventArgs e)
		{
			if (Size.Height <= originalFormSize.Height || Size.Width <= originalFormSize.Width)
			{
				Size = originalFormSize;
			}
		}

		public bool DialogCancelled
		{
			get
			{
				return cancelled;
			}
		}

		public string SelectedSQLServer
		{
			get
			{
				string selectedSQL = "";

				if (SQLlistBox.SelectedItem != null)
				{
					selectedSQL = SQLlistBox.SelectedItem.ToString();
				}

				return selectedSQL;
			}
		}

	
	}
}
