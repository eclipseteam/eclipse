using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace InstallGUI
{
	/// <summary>
	/// Summary description for InstallStep1.
	/// </summary>
	public class InstallAdministratorInfoControl : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
		private System.Windows.Forms.TextBox nableAdminPasswordTextBox;
		private System.Windows.Forms.TextBox nableAdminTextBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox reenterPasswordTextBox;

		private string installFullSystem = InstallConstants.ADMIN_INFORMATION_MESSAGE; 
		private System.Windows.Forms.Label lblinfoMessage;

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public InstallAdministratorInfoControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			//Defaults
			nableAdminTextBox.Text = InstallConstants.ADMIN_SYSTEM_ACCOUNT_NAME;
			nableAdminPasswordTextBox.Text = "";
			reenterPasswordTextBox.Text = "";
			lblinfoMessage.Text = installFullSystem;

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.lblinfoMessage = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.reenterPasswordTextBox = new System.Windows.Forms.TextBox();
			this.label20 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.nableAdminPasswordTextBox = new System.Windows.Forms.TextBox();
			this.nableAdminTextBox = new System.Windows.Forms.TextBox();
			this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.lblinfoMessage);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.reenterPasswordTextBox);
			this.groupBox2.Controls.Add(this.label20);
			this.groupBox2.Controls.Add(this.label13);
			this.groupBox2.Controls.Add(this.nableAdminPasswordTextBox);
			this.groupBox2.Controls.Add(this.nableAdminTextBox);
			this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox2.Location = new System.Drawing.Point(8, 8);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(416, 184);
			this.groupBox2.TabIndex = 47;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "N-ABLE System Administrator";
			// 
			// lblinfoMessage
			// 
			this.lblinfoMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblinfoMessage.Location = new System.Drawing.Point(10, 120);
			this.lblinfoMessage.Name = "lblinfoMessage";
			this.lblinfoMessage.Size = new System.Drawing.Size(390, 48);
			this.lblinfoMessage.TabIndex = 0;
			this.lblinfoMessage.Text = "Important: The N-ABLE System Administrator has full access to the N-ABLE applicat" +
				"ion, including organisation-wide tax and financial data. It is recommended you c" +
				"hoose a secure password for the N-ABLE System Administrator.";
			// 
			// label3
			// 
			this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(10, 88);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(136, 16);
			this.label3.TabIndex = 1;
			this.label3.Text = "Re-enter Password";
			// 
			// reenterPasswordTextBox
			// 
			this.reenterPasswordTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.reenterPasswordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.reenterPasswordTextBox.Location = new System.Drawing.Point(190, 88);
			this.reenterPasswordTextBox.Name = "reenterPasswordTextBox";
			this.reenterPasswordTextBox.PasswordChar = '*';
			this.reenterPasswordTextBox.Size = new System.Drawing.Size(192, 20);
			this.reenterPasswordTextBox.TabIndex = 2;
			this.reenterPasswordTextBox.Text = "";
			// 
			// label20
			// 
			this.label20.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label20.Location = new System.Drawing.Point(10, 24);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(96, 16);
			this.label20.TabIndex = 2;
			this.label20.Text = "Login name";
			// 
			// label13
			// 
			this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label13.Location = new System.Drawing.Point(8, 56);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(104, 16);
			this.label13.TabIndex = 3;
			this.label13.Text = "Password";
			// 
			// nableAdminPasswordTextBox
			// 
			this.nableAdminPasswordTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.nableAdminPasswordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.nableAdminPasswordTextBox.Location = new System.Drawing.Point(190, 56);
			this.nableAdminPasswordTextBox.Name = "nableAdminPasswordTextBox";
			this.nableAdminPasswordTextBox.PasswordChar = '*';
			this.nableAdminPasswordTextBox.Size = new System.Drawing.Size(192, 20);
			this.nableAdminPasswordTextBox.TabIndex = 1;
			this.nableAdminPasswordTextBox.Text = "";
			// 
			// nableAdminTextBox
			// 
			this.nableAdminTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.nableAdminTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.nableAdminTextBox.Location = new System.Drawing.Point(190, 24);
			this.nableAdminTextBox.Name = "nableAdminTextBox";
			this.nableAdminTextBox.ReadOnly = true;
			this.nableAdminTextBox.Size = new System.Drawing.Size(192, 20);
			this.nableAdminTextBox.TabIndex = 4;
			this.nableAdminTextBox.TabStop = false;
			this.nableAdminTextBox.Text = "";
			// 
			// InstallAdministratorInfoControl
			// 
			this.Controls.Add(this.groupBox2);
			this.Name = "InstallAdministratorInfoControl";
			this.Size = new System.Drawing.Size(432, 192);
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		#region Private Methods ------------------------------------------------------

		private string determineDefaultInstallLocation()
		{
			string[] drives = Environment.GetLogicalDrives();
			string logicalDrive = "";
			foreach (string drive in drives)
			{
				if (string.Compare(drive, @"a:\", true) != 0)
				{
					logicalDrive = drive;
					break;
				}
			}
			
			return logicalDrive;
		}

		#endregion //Private Methods

		#region Properties -----------------------------------------------------------

		public string NableAdminName
		{
			get
			{
				return nableAdminTextBox.Text;
			}
		}

		public string NableAdminPassword
		{
			get
			{
				return nableAdminPasswordTextBox.Text;
			}
		}

		#endregion //Properties

		public bool ValidatePassword()
		{
			return (nableAdminPasswordTextBox.Text == reenterPasswordTextBox.Text);
		}

	}
}
