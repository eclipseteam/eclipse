using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace InstallGUI
{
	/// <summary>
	/// Summary description for Progress.
	/// </summary>
	public class InstallProgressControl : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label currentInstallAction;
		private System.Windows.Forms.Label currentInstallStep;
		private System.Windows.Forms.ProgressBar progressBar;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public InstallProgressControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			this.TabStop = false;
			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected override void OnVisibleChanged(EventArgs e)
		{
			base.OnVisibleChanged(e);
			progressBar.Value = 0;
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.currentInstallAction = new System.Windows.Forms.Label();
			this.currentInstallStep = new System.Windows.Forms.Label();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.SuspendLayout();
			// 
			// currentInstallAction
			// 
			this.currentInstallAction.Location = new System.Drawing.Point(8, 72);
			this.currentInstallAction.Name = "currentInstallAction";
			this.currentInstallAction.Size = new System.Drawing.Size(408, 48);
			this.currentInstallAction.TabIndex = 0;
			// 
			// currentInstallStep
			// 
			this.currentInstallStep.Location = new System.Drawing.Point(8, 16);
			this.currentInstallStep.Name = "currentInstallStep";
			this.currentInstallStep.Size = new System.Drawing.Size(408, 48);
			this.currentInstallStep.TabIndex = 0;
			// 
			// progressBar
			// 
			this.progressBar.Location = new System.Drawing.Point(16, 136);
			this.progressBar.Maximum = 6;
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(394, 23);
			this.progressBar.Step = 1;
			this.progressBar.TabIndex = 2;
			this.progressBar.Value = 5;
			// 
			// InstallProgressControl
			// 
			this.Controls.Add(this.currentInstallAction);
			this.Controls.Add(this.currentInstallStep);
			this.Controls.Add(this.progressBar);
			this.Name = "InstallProgressControl";
			this.Size = new System.Drawing.Size(432, 192);
			this.ResumeLayout(false);

		}
		#endregion

		#region Properties -----------------------------------------------------------
		
		public Color CurrentInstallStepForeColor
		{
			set
			{
				currentInstallStep.ForeColor = value;
			}
		}

		public string CurrentInstallStep
		{
			set
			{
				currentInstallStep.Text = value;
			}
		}

		public string CurrentInstallAction
		{
			set
			{
				currentInstallAction.Text = value;
			}
		}

		public int ProgressBarScale
		{
			set
			{
				progressBar.Maximum = value;
			}
		}

		#endregion //Properties

		public void IncrementProgressBar(int incrementValue)
		{
			progressBar.Increment(incrementValue); 
		}

	}
}
