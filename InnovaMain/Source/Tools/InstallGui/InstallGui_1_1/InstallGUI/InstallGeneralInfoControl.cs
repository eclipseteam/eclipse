using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace InstallGUI
{
	/// <summary>
	/// Summary description for InstallStep1.
	/// </summary>
	public class InstallGeneralInfoControl : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox installFolderTextBox;
		private System.Windows.Forms.TextBox installNameTextBox;
		private System.Windows.Forms.Button browseButton;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
		private System.Windows.Forms.Label label3;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public InstallGeneralInfoControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			//Defaults
			installNameTextBox.Text = InstallConstants.INSTALL_FOLDER_NAME ;
			installFolderTextBox.Text = determineDefaultInstallLocation();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.installFolderTextBox = new System.Windows.Forms.TextBox();
			this.installNameTextBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.browseButton = new System.Windows.Forms.Button();
			this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.label3 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 74);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(328, 16);
			this.label1.TabIndex = 1;
			this.label1.Text = "Installation path";
			// 
			// installFolderTextBox
			// 
			this.installFolderTextBox.Location = new System.Drawing.Point(8, 98);
			this.installFolderTextBox.Name = "installFolderTextBox";
			this.installFolderTextBox.Size = new System.Drawing.Size(312, 20);
			this.installFolderTextBox.TabIndex = 1;
			this.installFolderTextBox.Text = "";
			// 
			// installNameTextBox
			// 
			this.installNameTextBox.Location = new System.Drawing.Point(8, 34);
			this.installNameTextBox.Name = "installNameTextBox";
			this.installNameTextBox.Size = new System.Drawing.Size(312, 20);
			this.installNameTextBox.TabIndex = 0;
			this.installNameTextBox.Text = "";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(8, 10);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(328, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "Application name";
			// 
			// browseButton
			// 
			this.browseButton.Location = new System.Drawing.Point(344, 98);
			this.browseButton.Name = "browseButton";
			this.browseButton.TabIndex = 2;
			this.browseButton.Text = "Browse ...";
			this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 152);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(392, 32);
			this.label3.TabIndex = 0;
			this.label3.Text = "Note: The application name will be used as the virtual directory name and SQL Ser" +
				"ver database name.";
			// 
			// InstallGeneralInfoControl
			// 
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.installFolderTextBox);
			this.Controls.Add(this.installNameTextBox);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.browseButton);
			this.Name = "InstallGeneralInfoControl";
			this.Size = new System.Drawing.Size(432, 192);
			this.ResumeLayout(false);

		}
		#endregion

		#region Private Methods ------------------------------------------------------

		private string determineDefaultInstallLocation()
		{
			string programFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
			string defaultInstallLocation = Path.Combine(programFiles, InstallConstants.PROGRAM_FILES_ALLUME);
			
			return defaultInstallLocation;
		}

		private void browseButton_Click(object sender, System.EventArgs e)
		{
			folderBrowserDialog.SelectedPath = installFolderTextBox.Text;
			folderBrowserDialog.ShowDialog();

			string selectedFolder = folderBrowserDialog.SelectedPath;

			if (!Directory.Exists(selectedFolder))
			{
				Directory.CreateDirectory(selectedFolder);
			}
			installFolderTextBox.Text = selectedFolder;	
		}

		#endregion //Private Methods

		#region Properties -----------------------------------------------------------

		public string InstallName
		{
			get
			{
				return installNameTextBox.Text;
			}
		}

		public string InstallFolder
		{
			get
			{
				return installFolderTextBox.Text;
			}
		}

		#endregion //Properties

	}
}
