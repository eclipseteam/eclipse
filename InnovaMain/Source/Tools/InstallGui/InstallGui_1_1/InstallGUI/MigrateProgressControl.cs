using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Windows.Forms;
using System.Threading;

using Oritax.TaxSimp.UpdateUtilities;
using Oritax.TaxSimp.Administration.SystemUpdate;

namespace InstallGUI
{
	/// <summary>
	/// Summary description for InstallProgressControl.
	/// </summary>
	public class MigrateProgressControl : System.Windows.Forms.UserControl
	{
		public event UpdateProgressStepUpdateHandler UpdateCompleted;

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label step2label;
		private System.Windows.Forms.Label step3label;

		Install install = null;
		InstallParameters installArguments;
		
		private bool errorOccured = false;
		private bool migrationNotifiableChanges = false;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label steplabel;
		private System.Windows.Forms.Label stepDescriptionlabel;
		private System.Windows.Forms.Label componentLabel;
		private System.Windows.Forms.Label progressLabel;
		private System.Windows.Forms.Label elapsedTimelabel;

		private DateTime startTime;
		private string errorLog;
		StreamWriter stream;
		Thread myThread = null;

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MigrateProgressControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					stream.Close();
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.elapsedTimelabel = new System.Windows.Forms.Label();
			this.progressLabel = new System.Windows.Forms.Label();
			this.componentLabel = new System.Windows.Forms.Label();
			this.stepDescriptionlabel = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.step3label = new System.Windows.Forms.Label();
			this.step2label = new System.Windows.Forms.Label();
			this.steplabel = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.elapsedTimelabel);
			this.groupBox1.Controls.Add(this.progressLabel);
			this.groupBox1.Controls.Add(this.componentLabel);
			this.groupBox1.Controls.Add(this.stepDescriptionlabel);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.step3label);
			this.groupBox1.Controls.Add(this.step2label);
			this.groupBox1.Controls.Add(this.steplabel);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(416, 136);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Product Upgrade Progress";
			// 
			// elapsedTimelabel
			// 
			this.elapsedTimelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.elapsedTimelabel.Location = new System.Drawing.Point(96, 112);
			this.elapsedTimelabel.Name = "elapsedTimelabel";
			this.elapsedTimelabel.Size = new System.Drawing.Size(296, 16);
			this.elapsedTimelabel.TabIndex = 7;
			// 
			// progressLabel
			// 
			this.progressLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.progressLabel.Location = new System.Drawing.Point(96, 86);
			this.progressLabel.Name = "progressLabel";
			this.progressLabel.Size = new System.Drawing.Size(312, 16);
			this.progressLabel.TabIndex = 6;
			// 
			// componentLabel
			// 
			this.componentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.componentLabel.Location = new System.Drawing.Point(96, 50);
			this.componentLabel.Name = "componentLabel";
			this.componentLabel.Size = new System.Drawing.Size(304, 30);
			this.componentLabel.TabIndex = 5;
			// 
			// stepDescriptionlabel
			// 
			this.stepDescriptionlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.stepDescriptionlabel.Location = new System.Drawing.Point(96, 24);
			this.stepDescriptionlabel.Name = "stepDescriptionlabel";
			this.stepDescriptionlabel.Size = new System.Drawing.Size(304, 16);
			this.stepDescriptionlabel.TabIndex = 4;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(8, 112);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(88, 16);
			this.label2.TabIndex = 3;
			this.label2.Text = "Elapsed time: ";
			// 
			// step3label
			// 
			this.step3label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.step3label.Location = new System.Drawing.Point(8, 86);
			this.step3label.Name = "step3label";
			this.step3label.Size = new System.Drawing.Size(56, 16);
			this.step3label.TabIndex = 2;
			this.step3label.Text = "Progress:";
			// 
			// step2label
			// 
			this.step2label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.step2label.Location = new System.Drawing.Point(8, 50);
			this.step2label.Name = "step2label";
			this.step2label.Size = new System.Drawing.Size(72, 16);
			this.step2label.TabIndex = 1;
			this.step2label.Text = "Component:";
			// 
			// steplabel
			// 
			this.steplabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.steplabel.Location = new System.Drawing.Point(8, 24);
			this.steplabel.Name = "steplabel";
			this.steplabel.Size = new System.Drawing.Size(72, 16);
			this.steplabel.TabIndex = 0;
			this.steplabel.Text = "Step 1 of 3:";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 160);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(416, 23);
			this.label1.TabIndex = 1;
			this.label1.Text = "Note: The upgrade process may take several hours to complete.";
			// 
			// MigrateProgressControl
			// 
			this.Controls.Add(this.label1);
			this.Controls.Add(this.groupBox1);
			this.Name = "MigrateProgressControl";
			this.Size = new System.Drawing.Size(432, 192);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		public InstallParameters InstallParameters
		{
			set
			{
				this.installArguments = value;
			}
		}

		public bool UpdateInstallation()
		{
			string updateFolder = installArguments.targetLocation; 
			errorLog = Path.Combine(updateFolder, "ErrorLog.txt");
			
			if (Directory.Exists(updateFolder))
			{
				DialogResult result = MessageBox.Show(this, string.Format("The upgrade process is attempting to clone the existing system and requires the path name {0}. \n Do you want to delete this folder and continue? ", updateFolder), "Delete existing folder?" , MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
				if (result == DialogResult.Cancel)
				{
                    throw new Exception("The user has canceled the update. Attempting to delete the N-ABLE 2008.2 folder");
				}
				else
				{
					try
					{
						purgeFolder(updateFolder, false);
					}
					catch
					{
						MessageBox.Show(this, string.Format("Unable to delete the following directory {0} or one of its subdirectories. This is required for updating. The update process has been cancelled.", updateFolder), "Nable Update", MessageBoxButtons.OK, MessageBoxIcon.Error);
						return false;
					}
				}
			}

			if (!Directory.Exists(updateFolder))
			{
				try
				{
					Directory.CreateDirectory(updateFolder);
				}
				catch
				{
					MessageBox.Show(this, string.Format("Unable to create the following directory {0}. This is required for updating. The update process has been cancelled.", updateFolder), "Nable Update", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return false;
				}
			}

			try
			{
				stream = new StreamWriter(errorLog, true);
			}
			catch
			{
				MessageBox.Show(this, "The upgrade log file could not be created.", "Nable Update", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
					
			ThreadStart myThreadDelegate = new ThreadStart(updateData);
			myThread = new Thread(myThreadDelegate);
			myThread.IsBackground = true;
			startTime = DateTime.Now;
			myThread.Start();
			return true;
		}

		public void Cancel()
		{
			if (stream != null)
			{
				stream.Close();
				stream = null;
			}

			if (myThread != null)
			{
				myThread.Abort();
			}
		}

		private void updateData()
		{
			install = new Install();
			install.UpdateComponentProgressUpdate += new UpdateProgressComponentUpdateHandler(install_UpdateComponentProgressUpdate);
			install.LogMigrationInformation += new UpdateMessageHandler(migration_MigrationInformation);
			install.UpdateStepProgressUpdate += new UpdateProgressStepUpdateHandler(install_UpdateStepProgressUpdate);
			install.InstallError += new InstallErrorHandler(install_InstallError);
			install.MigrationInformation +=new MigrationInformationHandler(install_MigrationInformation);
		
			try
			{
				install.UpdateFullProduct(installArguments); 
			}
			catch (Exception ex)
			{
				errorOccured = true;
				writeToErrorFile(ex.Message);
				this.onMigrationCompleted(null);
			}
		}

		private void install_UpdateComponentProgressUpdate(object sender, UpdateProgressComponentEventArgs e)
		{
			componentLabel.Text = e.ComponentName;
			progressLabel.Text = e.ProgressPercentage;
			elapsedTimelabel.Text = calculateElapsedTime();
			writeToErrorFile("\t" +e.ComponentName + " " + e.Progress);
			this.Refresh();
			this.ParentForm.Refresh();
			Application.DoEvents();
		}

		private void migration_MigrationInformation(object sender, UpdateMessageEventArgs e)
		{
			writeToErrorFile(e.Message);
		}

		private void writeToErrorFile(string message)
		{
			if (stream != null)
			{
				string[] errorMessages = message.Split('\n');
				foreach(string errorMessage in errorMessages)
				{
					stream.WriteLine(DateTime.Now.ToShortTimeString() +": " +errorMessage);
				}
				stream.Flush();
			}
		}

		private string calculateElapsedTime()
		{
			TimeSpan deltaTime = DateTime.Now - startTime;
			string elapsedTime;
			if (deltaTime.Hours == 0)
			{
				elapsedTime = deltaTime.Minutes + " mins " +deltaTime.Seconds +" secs"; 
			}
			else
			{
				elapsedTime = deltaTime.Hours + " hours " +deltaTime.Minutes +" mins";
			}
			return elapsedTime;
		}

		public bool ErrorOccurred
		{
			get
			{
				return errorOccured;
			}
		}

		public bool MigrationNotifiableChanges
		{
			get
			{
				return migrationNotifiableChanges;
			}
		}

		private void onMigrationCompleted(UpdateProgressStepEventArgs e)
		{
			if (this.UpdateCompleted != null)
				this.UpdateCompleted(this, e);
		}

		private void install_UpdateStepProgressUpdate(object sender, UpdateProgressStepEventArgs e)
		{
			switch (e.StepProgress)
			{
				case StepProgress.Cloning:
				{
					steplabel.Text = "Step 1 of 3";
					stepDescriptionlabel.Text = "Cloning System";
					break;
				}
				case StepProgress.Updating:
				{
					steplabel.Text = "Step 2 of 3";
					stepDescriptionlabel.Text = "Updating Components";
					break;
				}
				case StepProgress.InstallingSystemComponents:
				{
					steplabel.Text = "Step 3 of 3";
					stepDescriptionlabel.Text = "Installing System Components";
					break;
				}
				case StepProgress.Finished:
				{
					stepDescriptionlabel.Text	= "System Upgrade complete";
					componentLabel.Text = "";
					progressLabel.Text = "100 %";
					elapsedTimelabel.Text = calculateElapsedTime();
					onMigrationCompleted(e);
					break;
				}
			}
			
			writeToErrorFile("");
			writeToErrorFile("Performing " +e.StepProgress.ToString() + " step");
			writeToErrorFile("");
			this.Refresh();
			this.ParentForm.Refresh();
			Application.DoEvents();
		}

		private void install_InstallError(object sender, InstallErrorEventArgs e)
		{
			errorOccured = true;
			writeToErrorFile(e.Message);
			Cancel();
		}

		private void purgeFolder(string path, bool subDirectory)
		{
			string[] directories = Directory.GetDirectories(path);

			foreach(string directory in directories)
			{
				purgeFolder(directory, true);
			}

			string[] fileNames = Directory.GetFiles(path);

			foreach(string file in fileNames)
			{
				File.Delete(file);
			}
			
			if (subDirectory)
			{
				Directory.Delete(path);
			}
		}

		private void install_MigrationInformation(object sender, MigrationInformationEventArgs e)
		{
			migrationNotifiableChanges = true;
			writeToErrorFile(e.Message);
		}
	}
}
