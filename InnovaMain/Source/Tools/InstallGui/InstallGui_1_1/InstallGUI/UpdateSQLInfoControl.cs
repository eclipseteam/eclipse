using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Windows.Forms;

using Oritax.TaxSimp.Administration.SystemUpdate;

namespace InstallGUI
{
	/// <summary>
	/// Summary description for InstallStep1.
	/// </summary>
	public class UpdateSQLInfoControl : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
		private System.Windows.Forms.TextBox nableSystemPasswordTextBox;
		private System.Windows.Forms.TextBox nableSystemAccountTextBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button browseSQLButton;
		private System.Windows.Forms.TextBox sqlServerNameTextBox;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label13;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public UpdateSQLInfoControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			//Defaults
			nableSystemAccountTextBox.Text = "nable";
			nableSystemPasswordTextBox.Text = "";
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.nableSystemPasswordTextBox = new System.Windows.Forms.TextBox();
			this.nableSystemAccountTextBox = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.label3 = new System.Windows.Forms.Label();
			this.sqlServerNameTextBox = new System.Windows.Forms.TextBox();
			this.browseSQLButton = new System.Windows.Forms.Button();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.nableSystemPasswordTextBox);
			this.groupBox2.Controls.Add(this.nableSystemAccountTextBox);
			this.groupBox2.Controls.Add(this.label13);
			this.groupBox2.Controls.Add(this.label20);
			this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox2.Location = new System.Drawing.Point(8, 64);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(416, 128);
			this.groupBox2.TabIndex = 51;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "N-ABLE Database Administrator";
			// 
			// nableSystemPasswordTextBox
			// 
			this.nableSystemPasswordTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.nableSystemPasswordTextBox.Location = new System.Drawing.Point(190, 56);
			this.nableSystemPasswordTextBox.Name = "nableSystemPasswordTextBox";
			this.nableSystemPasswordTextBox.PasswordChar = '*';
			this.nableSystemPasswordTextBox.Size = new System.Drawing.Size(192, 20);
			this.nableSystemPasswordTextBox.TabIndex = 1;
			this.nableSystemPasswordTextBox.Text = "";
			// 
			// nableSystemAccountTextBox
			// 
			this.nableSystemAccountTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.nableSystemAccountTextBox.Location = new System.Drawing.Point(190, 24);
			this.nableSystemAccountTextBox.Name = "nableSystemAccountTextBox";
			this.nableSystemAccountTextBox.Size = new System.Drawing.Size(192, 20);
			this.nableSystemAccountTextBox.TabIndex = 0;
			this.nableSystemAccountTextBox.Text = "";
			// 
			// label13
			// 
			this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label13.Location = new System.Drawing.Point(10, 24);
			this.label13.Name = "label13";
			this.label13.TabIndex = 0;
			this.label13.Text = "Login Name";
			// 
			// label20
			// 
			this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label20.Location = new System.Drawing.Point(10, 56);
			this.label20.Name = "label20";
			this.label20.TabIndex = 1;
			this.label20.Text = "Password";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(8, 8);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(328, 16);
			this.label3.TabIndex = 50;
			this.label3.Text = "SQL Server";
			// 
			// sqlServerNameTextBox
			// 
			this.sqlServerNameTextBox.Location = new System.Drawing.Point(8, 30);
			this.sqlServerNameTextBox.Name = "sqlServerNameTextBox";
			this.sqlServerNameTextBox.Size = new System.Drawing.Size(312, 20);
			this.sqlServerNameTextBox.TabIndex = 3;
			this.sqlServerNameTextBox.Text = "";
			// 
			// browseSQLButton
			// 
			this.browseSQLButton.Location = new System.Drawing.Point(349, 30);
			this.browseSQLButton.Name = "browseSQLButton";
			this.browseSQLButton.TabIndex = 4;
			this.browseSQLButton.Text = "Browse ...";
			this.browseSQLButton.Click += new System.EventHandler(this.browseSQLButton_Click);
			// 
			// UpdateSQLInfoControl
			// 
			this.Controls.Add(this.sqlServerNameTextBox);
			this.Controls.Add(this.browseSQLButton);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.label3);
			this.Name = "UpdateSQLInfoControl";
			this.Size = new System.Drawing.Size(432, 192);
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		#region Private Methods ------------------------------------------------------

		private string determineDefaultInstallLocation()
		{
			string[] drives = Environment.GetLogicalDrives();
			string logicalDrive = "";
			foreach (string drive in drives)
			{
				if (string.Compare(drive, @"a:\", true) != 0)
				{
					logicalDrive = drive;
					break;
				}
			}
			
			return logicalDrive;
		}

		private void browseSQLButton_Click(object sender, System.EventArgs e)
		{
			string[] sqls = SqlLocator.GetServers();
			SQLSelectorForm selectorForm = new SQLSelectorForm(sqls);

			selectorForm.ShowDialog();
			if (!selectorForm.DialogCancelled)
			{
				sqlServerNameTextBox.Text = selectorForm.SelectedSQLServer;
			}
		}
		
		#endregion //Private Methods

		

		#region Properties -----------------------------------------------------------

		public string SQLServerName
		{
			get
			{
				return sqlServerNameTextBox.Text;
			}
		}

		public string NableSystemName
		{
			get
			{
				return nableSystemAccountTextBox.Text;
			}
		}

		public string NableSystemPassword
		{
			get
			{
				return nableSystemPasswordTextBox.Text;
			}
		}

		#endregion //Properties

	}
}
