using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Windows.Forms;

using Oritax.TaxSimp.Administration.SystemUpdate;

namespace InstallGUI
{
	/// <summary>
	/// Summary description for ChangePermissionsControl.
	/// </summary>
	public class ChangePermissionsAdminControl : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox reenterAdminPassword;
		private System.Windows.Forms.CheckBox changeNableAdminCheckBox;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
		private System.Windows.Forms.TextBox adminAccountTextBox;
		private System.Windows.Forms.TextBox adminPasswordTextBox;
		private System.Windows.Forms.Label label1;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ChangePermissionsAdminControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label17 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.changeNableAdminCheckBox = new System.Windows.Forms.CheckBox();
			this.reenterAdminPassword = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.adminAccountTextBox = new System.Windows.Forms.TextBox();
			this.adminPasswordTextBox = new System.Windows.Forms.TextBox();
			this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label17
			// 
			this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label17.Enabled = false;
			this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label17.Location = new System.Drawing.Point(16, 104);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(104, 16);
			this.label17.TabIndex = 5;
			this.label17.Text = "Password";
			// 
			// label12
			// 
			this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label12.Enabled = false;
			this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label12.Location = new System.Drawing.Point(16, 64);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(88, 16);
			this.label12.TabIndex = 4;
			this.label12.Text = "Login name";
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.changeNableAdminCheckBox);
			this.groupBox1.Controls.Add(this.reenterAdminPassword);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.adminAccountTextBox);
			this.groupBox1.Controls.Add(this.adminPasswordTextBox);
			this.groupBox1.Controls.Add(this.label12);
			this.groupBox1.Controls.Add(this.label17);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(8, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(416, 216);
			this.groupBox1.TabIndex = 7;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "N-ABLE System Administrator";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 184);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(392, 16);
			this.label1.TabIndex = 6;
			this.label1.Text = "Note: IIS will need to be restarted for the permission change to take effect.";
			// 
			// changeNableAdminCheckBox
			// 
			this.changeNableAdminCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.changeNableAdminCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.changeNableAdminCheckBox.Location = new System.Drawing.Point(16, 24);
			this.changeNableAdminCheckBox.Name = "changeNableAdminCheckBox";
			this.changeNableAdminCheckBox.Size = new System.Drawing.Size(216, 24);
			this.changeNableAdminCheckBox.TabIndex = 0;
			this.changeNableAdminCheckBox.Text = "Change  Administrator Account";
			this.changeNableAdminCheckBox.CheckedChanged += new System.EventHandler(this.changeNableAdmin_CheckedChanged);
			// 
			// reenterAdminPassword
			// 
			this.reenterAdminPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.reenterAdminPassword.Enabled = false;
			this.reenterAdminPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.reenterAdminPassword.Location = new System.Drawing.Point(176, 144);
			this.reenterAdminPassword.Name = "reenterAdminPassword";
			this.reenterAdminPassword.PasswordChar = '*';
			this.reenterAdminPassword.Size = new System.Drawing.Size(192, 20);
			this.reenterAdminPassword.TabIndex = 2;
			this.reenterAdminPassword.Text = "";
			// 
			// label6
			// 
			this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label6.Enabled = false;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label6.Location = new System.Drawing.Point(16, 144);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(120, 16);
			this.label6.TabIndex = 2;
			this.label6.Text = "Re-enter password";
			// 
			// adminAccountTextBox
			// 
			this.adminAccountTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.adminAccountTextBox.Enabled = false;
			this.adminAccountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.adminAccountTextBox.Location = new System.Drawing.Point(176, 64);
			this.adminAccountTextBox.Name = "adminAccountTextBox";
			this.adminAccountTextBox.ReadOnly = true;
			this.adminAccountTextBox.Size = new System.Drawing.Size(192, 20);
			this.adminAccountTextBox.TabIndex = 3;
			this.adminAccountTextBox.TabStop = false;
			this.adminAccountTextBox.Text = "Administrator";
			// 
			// adminPasswordTextBox
			// 
			this.adminPasswordTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.adminPasswordTextBox.Enabled = false;
			this.adminPasswordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.adminPasswordTextBox.Location = new System.Drawing.Point(176, 104);
			this.adminPasswordTextBox.Name = "adminPasswordTextBox";
			this.adminPasswordTextBox.PasswordChar = '*';
			this.adminPasswordTextBox.Size = new System.Drawing.Size(192, 20);
			this.adminPasswordTextBox.TabIndex = 1;
			this.adminPasswordTextBox.Text = "";
			// 
			// ChangePermissionsAdminControl
			// 
			this.Controls.Add(this.groupBox1);
			this.Name = "ChangePermissionsAdminControl";
			this.Size = new System.Drawing.Size(432, 224);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		#region Properties -----------------------------------------------------------

		public string NableAdminAccount
		{
			get
			{
				return adminAccountTextBox.Text;
			}
		}

		public string NableAdminPassword
		{
			get
			{
				return adminPasswordTextBox.Text;
			}
		}
		
		public string NableAdminReeneteredPassword
		{
			get
			{
				return reenterAdminPassword.Text;
			}
		}

		public bool ChangeNableAdminAccount
		{
			get
			{
				return changeNableAdminCheckBox.Checked;
			}
		}

		#endregion //Properties

		#region Private Methods ------------------------------------------------------

		private void changeNableAdmin_CheckedChanged(object sender, System.EventArgs e)
		{
			adminAccountTextBox.Enabled = changeNableAdminCheckBox.Checked;
			adminPasswordTextBox.Enabled = changeNableAdminCheckBox.Checked;
			reenterAdminPassword.Enabled = changeNableAdminCheckBox.Checked;
			label6.Enabled = changeNableAdminCheckBox.Checked;
			label12.Enabled = changeNableAdminCheckBox.Checked;
			label17.Enabled = changeNableAdminCheckBox.Checked;
		}

		#endregion //Private Methods
	}
}
