using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace InstallGUI
{
	/// <summary>
	/// Summary description for InstallStep2.
	/// </summary>
	public class InstallDatabaseAdminInfoControl : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox nableSystemPasswordTextBox;
		private System.Windows.Forms.TextBox nableSystemAccountTextBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox reenterPasswordTextBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public InstallDatabaseAdminInfoControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			//defaults
			nableSystemAccountTextBox.Text = InstallConstants.NABLE_SYSTEM_ACCOUNT_NAME;
			nableSystemPasswordTextBox.Text = "";
			reenterPasswordTextBox.Text = "";
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label17 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.nableSystemPasswordTextBox = new System.Windows.Forms.TextBox();
			this.nableSystemAccountTextBox = new System.Windows.Forms.TextBox();
			this.reenterPasswordTextBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label17
			// 
			this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label17.Location = new System.Drawing.Point(10, 56);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(128, 16);
			this.label17.TabIndex = 55;
			this.label17.Text = "Password";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(10, 120);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(392, 56);
			this.label1.TabIndex = 59;
			this.label1.Text = @"Note: The N-ABLE Database Administrator account is used by the N-ABLE application to access the N-ABLE SQL Server database. Choose a different login name from the SQL Server Administrator account.";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.nableSystemPasswordTextBox);
			this.groupBox1.Controls.Add(this.nableSystemAccountTextBox);
			this.groupBox1.Controls.Add(this.reenterPasswordTextBox);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.label17);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(416, 184);
			this.groupBox1.TabIndex = 60;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "N-ABLE Database Administrator";
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label5.Location = new System.Drawing.Point(10, 24);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(100, 24);
			this.label5.TabIndex = 61;
			this.label5.Text = "Login name";
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label4.Location = new System.Drawing.Point(10, 88);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(136, 16);
			this.label4.TabIndex = 60;
			this.label4.Text = "Re-enter password";
			// 
			// nableSystemPasswordTextBox
			// 
			this.nableSystemPasswordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.nableSystemPasswordTextBox.Location = new System.Drawing.Point(190, 56);
			this.nableSystemPasswordTextBox.Name = "nableSystemPasswordTextBox";
			this.nableSystemPasswordTextBox.PasswordChar = '*';
			this.nableSystemPasswordTextBox.Size = new System.Drawing.Size(192, 20);
			this.nableSystemPasswordTextBox.TabIndex = 1;
			this.nableSystemPasswordTextBox.Text = "";
			// 
			// nableSystemAccountTextBox
			// 
			this.nableSystemAccountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.nableSystemAccountTextBox.Location = new System.Drawing.Point(190, 24);
			this.nableSystemAccountTextBox.Name = "nableSystemAccountTextBox";
			this.nableSystemAccountTextBox.Size = new System.Drawing.Size(192, 20);
			this.nableSystemAccountTextBox.TabIndex = 0;
			this.nableSystemAccountTextBox.Text = "";
			// 
			// reenterPasswordTextBox
			// 
			this.reenterPasswordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.reenterPasswordTextBox.Location = new System.Drawing.Point(190, 88);
			this.reenterPasswordTextBox.Name = "reenterPasswordTextBox";
			this.reenterPasswordTextBox.PasswordChar = '*';
			this.reenterPasswordTextBox.Size = new System.Drawing.Size(192, 20);
			this.reenterPasswordTextBox.TabIndex = 2;
			this.reenterPasswordTextBox.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Name = "label2";
			this.label2.TabIndex = 0;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(0, 0);
			this.label3.Name = "label3";
			this.label3.TabIndex = 0;
			// 
			// InstallDatabaseAdminInfoControl
			// 
			this.Controls.Add(this.groupBox1);
			this.Name = "InstallDatabaseAdminInfoControl";
			this.Size = new System.Drawing.Size(432, 192);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		#region Properties -----------------------------------------------------------
		
		public string NableSystemAccountName
		{
			get
			{
				return nableSystemAccountTextBox.Text;
			}
		}

		public string NableSystemPassword
		{
			get
			{
				return nableSystemPasswordTextBox.Text;
			}
		}

		#endregion //Properties

		public bool ValidatePassword()
		{
			return (nableSystemPasswordTextBox.Text == reenterPasswordTextBox.Text);
		}
	}
}
