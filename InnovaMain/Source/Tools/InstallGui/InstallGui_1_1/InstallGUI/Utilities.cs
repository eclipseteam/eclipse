using System;

namespace InstallGUI
{
	/// <summary>
	/// Summary description for Utilities.
	/// </summary>
	public class Utilities
	{
		public static bool CheckInstallName(string installName)
		{
			bool validInstallName = false; 
			char[] legalCharacters = new char[]{'_', '#'};
	
			foreach(char character in installName)
			{
				validInstallName = false;
				foreach(char legal in legalCharacters)
				{
					if (character == legal)
					{
						validInstallName = true;
						break;
					}
				}

				if (char.IsLetterOrDigit(character))
				{
					validInstallName = true;
				}

				if (!validInstallName)
				{
					break;
				}
			}

			return validInstallName;
		}
	}
}
