using System.Reflection;
using System.Runtime.CompilerServices;
using InstallGUI;

[assembly: AssemblyTitle("N-ABLE Installer")]
[assembly: AssemblyDescription("N-ABLE Tax Management Solution application installer.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Allume Technology Partners")]
[assembly: AssemblyProduct("N-ABLE Tax Management Solution")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]		
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyDelaySign(false)]
