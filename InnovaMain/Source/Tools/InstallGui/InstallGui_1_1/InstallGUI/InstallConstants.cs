using System;

namespace InstallGUI
{
	/// <summary>
	/// Summary description for InstallConstants.
	/// </summary>
	public class InstallConstants
	{
		#region Main Form --------------------------------------------------------------
        public const string MAIN_TITLE = "N-ABLE Tax Management Solution Release 2008.2";
		public const string FULL_INSTALL_INFO = "Select this option to install a new version of the product.";
		public const string PERMISSIONS ="Select this option to change system administration passwords for an existing product installation.";
		public const string UPGRADE_INSTALL_INFO ="Select this option to upgrade a version of the product.";
		public const string IISNOTDETECTED_MESSAGE = "N-ABLE Installer is unable to detect Windows IIS (Internet Information Services) on the application server.  \nPlease ensure that IIS is installed and configured before continuing with the installation of N-ABLE.\nNote: N-ABLE Installer may be unable to detect IIS on a Windows 2003 server configured with security lock-down.\n\nDo you want to continue installing N-ABLE?";
		public const string IISNOTDETECTED_TITLE = "IIS not detected";
		#endregion

		#region General Info -----------------------------------------------------------
        public const string INSTALL_PRODUCT_VERSION = "Release 0.5";
        public const string INSTALL_FOLDER_NAME = "ONE_SOURCE_REL_05";
		public const string PROGRAM_FILES_ALLUME = "Allume";
		#endregion

		#region Database Admin Info ----------------------------------------------------
        public const string NABLE_SYSTEM_ACCOUNT_NAME = "ONE_SOURCE_REL_05";
		#endregion

		#region Administrator Info -----------------------------------------------------
		public const string ADMIN_SYSTEM_ACCOUNT_NAME = "Administrator";
		public const string ADMIN_INFORMATION_MESSAGE = "Important: The One Source System Administrator has full access to the One Source application, including organisation-wide tax and financial data. It is recommended you choose a secure password for the One Source System Administrator.";
		#endregion

		#region Change Permission ------------------------------------------------------
		public const string SQL_SERVER_NAME =  "localhost";
		#endregion

		#region Install steps ----------------------------------------------------------
		public const string INSTALL_STEP1 = "Step 1 of 4 - Installation location";
		public const string INSTALL_STEP2 = "Step 2 of 4 - SQL Server login";
		public const string INSTALL_STEP3 = "Step 3 of 4 - One Source System Administrator";
		public const string INSTALL_STEP4 = "Step 4 of 4 - One Source Database Administrator";
        public const string INSTALL_LABEL = "Installing One Source Version 0.5";
		public const string INSTALL_READY = "Ready to install. Click Install to proceed.";

		#endregion

		#region Upgrade Installation ---------------------------------------------------
		public const string UPGRADE_MIGRATION_ERROR = "Errors have occured during the update. Check the error log in the installation directory for details";
		public const string UPGRADE_WELCOME = "Introduction";
		public const string UPGRADE_STEP1 = "Step 1 of 3 - N-ABLE locations";
		public const string UPGRADE_STEP2 = "Step 2 of 3 - SQL Server information.";
		public const string UPGRADE_STEP3 = "Step 3 of 3 - Upgrading Components and Data";
		public const string UPGRADE_SUCCESSFUL = "Upgrade successful";
		public const string UPGRADE_MIGRATION_FAILED = "Upgrade incomplete";
		public const string UPGRADE_INSTALL_TO_PATH ="C:\\Program Files\\Allume\\NABLE_V2008_2";
		public const string UPGRADE_INSTALL_FROM_PATH  = "C:\\Program Files\\Allume\\NABLE_V2008_1";
        public const string UPGRADE_INSTALL_TO_LOCATION_TEXT = "N-ABLE 2008.2 installation location";
		public const string UPGRADE_INSTALL_FROM_LOCATION_TEXT ="N-ABLE 2008.1 installation location";
		#endregion

		#region Upgrade Welcome --------------------------------------------------------
        public const string UPGRADE_WELCOME_TEXT = "This process will upgrade your system from N-ABLE 2008.1 to N-ABLE 2008.2.";
		public const string UPGRADE_WELCOME_LOGOUT_TEXT = 	"- You have an installation of N-ABLE 2008.1 \n- All users are logged out of N-ABLE 2008.1 \n- You have System Administrator access rights to the N-ABLE application and servers ";
		
		#endregion

		
	}
}
