using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Windows.Forms;

using Oritax.TaxSimp.Administration.SystemUpdate;

namespace InstallGUI
{
	/// <summary>
	/// Summary description for InstallStep1.
	/// </summary>
	public class UpdateGeneralInfoControl : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox installFolderTextBox;
		private System.Windows.Forms.Button browseButton;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
		private System.Windows.Forms.ComboBox installNameComboBox;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public UpdateGeneralInfoControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			//Defaults
			installFolderTextBox.Text = determineDefaultInstallLocation();
			installNameComboBox.Items.AddRange(WebDeployment.GetNAbleWebSites(null));
			if (installNameComboBox.Items.Count > 0)
			{
				installNameComboBox.SelectedIndex = 0;
			}
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.installFolderTextBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.browseButton = new System.Windows.Forms.Button();
			this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.installNameComboBox = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 80);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(328, 16);
			this.label1.TabIndex = 44;
			this.label1.Text = "Installation Location";
			// 
			// installFolderTextBox
			// 
			this.installFolderTextBox.Location = new System.Drawing.Point(8, 104);
			this.installFolderTextBox.Name = "installFolderTextBox";
			this.installFolderTextBox.Size = new System.Drawing.Size(312, 20);
			this.installFolderTextBox.TabIndex = 1;
			this.installFolderTextBox.Text = "";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(8, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(328, 16);
			this.label2.TabIndex = 45;
			this.label2.Text = "Installation Name";
			// 
			// browseButton
			// 
			this.browseButton.Location = new System.Drawing.Point(344, 104);
			this.browseButton.Name = "browseButton";
			this.browseButton.TabIndex = 2;
			this.browseButton.Text = "Browse ...";
			this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
			// 
			// installNameComboBox
			// 
			this.installNameComboBox.Location = new System.Drawing.Point(8, 40);
			this.installNameComboBox.Name = "installNameComboBox";
			this.installNameComboBox.Size = new System.Drawing.Size(232, 21);
			this.installNameComboBox.TabIndex = 0;
			// 
			// UpdateGeneralInfoControl
			// 
			this.Controls.Add(this.installNameComboBox);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.installFolderTextBox);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.browseButton);
			this.Name = "UpdateGeneralInfoControl";
			this.Size = new System.Drawing.Size(432, 192);
			this.ResumeLayout(false);

		}
		#endregion

		#region Private Methods ------------------------------------------------------

		private string determineDefaultInstallLocation()
		{
			string[] drives = Environment.GetLogicalDrives();
			string logicalDrive = "";
			foreach (string drive in drives)
			{
				if (string.Compare(drive, @"a:\", true) != 0)
				{
					logicalDrive = drive;
					break;
				}
			}
			
			return logicalDrive;
		}

		private void browseButton_Click(object sender, System.EventArgs e)
		{
			folderBrowserDialog.SelectedPath = installFolderTextBox.Text;
			folderBrowserDialog.ShowDialog();

			string selectedFolder = folderBrowserDialog.SelectedPath;

			if (!Directory.Exists(selectedFolder))
			{
				Directory.CreateDirectory(selectedFolder);
			}
			installFolderTextBox.Text = selectedFolder;	
		}

		#endregion //Private Methods

		#region Properties -----------------------------------------------------------

		public string InstallName
		{
			get
			{
				return installNameComboBox.Text;
			}
		}

		public string InstallFolder
		{
			get
			{
				return installFolderTextBox.Text;
			}
		}

		#endregion //Properties

	}
}
