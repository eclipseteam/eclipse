using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace InstallGUI
{
	/// <summary>
	/// Summary description for InstallStep2.
	/// </summary>
	public class InstallSQLInfoControl : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox sqlServerNameTextBox;
		private System.Windows.Forms.Button browseButton;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox sqlAdminAccountTextBox;
		private System.Windows.Forms.TextBox sqlAdminPasswordTextBox;
		private System.Windows.Forms.Label label1;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public InstallSQLInfoControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			//defaults
			sqlServerNameTextBox.Text = InstallConstants.SQL_SERVER_NAME;
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label17 = new System.Windows.Forms.Label();
			this.sqlAdminAccountTextBox = new System.Windows.Forms.TextBox();
			this.sqlServerNameTextBox = new System.Windows.Forms.TextBox();
			this.sqlAdminPasswordTextBox = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.browseButton = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label17
			// 
			this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label17.Location = new System.Drawing.Point(10, 56);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(72, 16);
			this.label17.TabIndex = 55;
			this.label17.Text = "Password";
			// 
			// sqlAdminAccountTextBox
			// 
			this.sqlAdminAccountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.sqlAdminAccountTextBox.Location = new System.Drawing.Point(190, 24);
			this.sqlAdminAccountTextBox.Name = "sqlAdminAccountTextBox";
			this.sqlAdminAccountTextBox.Size = new System.Drawing.Size(192, 20);
			this.sqlAdminAccountTextBox.TabIndex = 2;
			this.sqlAdminAccountTextBox.Text = "";
			// 
			// sqlServerNameTextBox
			// 
			this.sqlServerNameTextBox.Location = new System.Drawing.Point(8, 30);
			this.sqlServerNameTextBox.Name = "sqlServerNameTextBox";
			this.sqlServerNameTextBox.Size = new System.Drawing.Size(312, 20);
			this.sqlServerNameTextBox.TabIndex = 0;
			this.sqlServerNameTextBox.Text = "";
			// 
			// sqlAdminPasswordTextBox
			// 
			this.sqlAdminPasswordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.sqlAdminPasswordTextBox.Location = new System.Drawing.Point(190, 56);
			this.sqlAdminPasswordTextBox.Name = "sqlAdminPasswordTextBox";
			this.sqlAdminPasswordTextBox.PasswordChar = '*';
			this.sqlAdminPasswordTextBox.Size = new System.Drawing.Size(192, 20);
			this.sqlAdminPasswordTextBox.TabIndex = 3;
			this.sqlAdminPasswordTextBox.Text = "";
			// 
			// label12
			// 
			this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label12.Location = new System.Drawing.Point(10, 24);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(128, 16);
			this.label12.TabIndex = 53;
			this.label12.Text = "Login name";
			// 
			// label11
			// 
			this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label11.Location = new System.Drawing.Point(8, 8);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(328, 16);
			this.label11.TabIndex = 54;
			this.label11.Text = "SQL Server";
			// 
			// browseButton
			// 
			this.browseButton.Location = new System.Drawing.Point(349, 30);
			this.browseButton.Name = "browseButton";
			this.browseButton.TabIndex = 1;
			this.browseButton.Text = "Browse ...";
			this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.sqlAdminPasswordTextBox);
			this.groupBox1.Controls.Add(this.sqlAdminAccountTextBox);
			this.groupBox1.Controls.Add(this.label17);
			this.groupBox1.Controls.Add(this.label12);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(8, 64);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(416, 128);
			this.groupBox1.TabIndex = 60;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "SQL Server System Administrator";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(10, 88);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(376, 32);
			this.label1.TabIndex = 56;
			this.label1.Text = "Note: The SQL Server System Administrator Login details are not retained after in" +
				"stallation.";
			// 
			// InstallSQLInfoControl
			// 
			this.Controls.Add(this.sqlServerNameTextBox);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.browseButton);
			this.Controls.Add(this.groupBox1);
			this.Name = "InstallSQLInfoControl";
			this.Size = new System.Drawing.Size(432, 192);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void browseButton_Click(object sender, System.EventArgs e)
		{
			string[] sqls = SqlLocator.GetServers();
			SQLSelectorForm selectorForm = new SQLSelectorForm(sqls);

			selectorForm.ShowDialog();
			if (!selectorForm.DialogCancelled)
			{
				sqlServerNameTextBox.Text = selectorForm.SelectedSQLServer;
			}
		}

		#region Properties -----------------------------------------------------------
		
		public string SQLServerName
		{
			get
			{
				return sqlServerNameTextBox.Text;
			}
		}

		public string SQLAdminAccountName
		{
			get
			{
				return sqlAdminAccountTextBox.Text;
			}
		}

		public string SQLAdminPassword
		{
			get
			{
				return sqlAdminPasswordTextBox.Text;
			}
		}

		#endregion //Properties
	}
}
