using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using System.Data;

using Oritax.TaxSimp.SAS70Password;

using Oritax.TaxSimp.Administration.SystemUpdate;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.Utilities;


namespace InstallGUI
{
	/// <summary>
	///		Creates an instance of the InstallSteps form. This takes the user through 
	///		the necessary steps to install the N-Able product.
	/// </summary>
	public class PermissionsForm : System.Windows.Forms.Form
	{
		#region Private Members ------------------------------------------------------

		private enum CurrentSelection
		{
			InstallStep1,
			InstallStep2,
			InstallStep3,
			InstallProgress,
			Installed
		}

		private InstallParameters installParameters;
		private Oritax.TaxSimp.Administration.SystemUpdate.Install install;

		private string step1Label = "Step 1 of 3 N-ABLE Installation Details";
		private string step2Label = "Step 2 of 3 N-ABLE System Administrator";
		private string step3Label = "Step 3 of 3 N-ABLE Database Administrator";
		private string updateLabel = "Updating permissions";

		private CurrentSelection currentSelection;
		private ChangePermissionsControl permissionsInfo;
		private ChangePermissionsAdminControl adminInfo;
		private ChangePermissionsSystemControl systemInfo;
		private InstallProgressControl installProgress;
		
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Button nextButton;
		private System.Windows.Forms.Button previousButton;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Label installInformationLabel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		#endregion //Private Members

		#region Contructors ----------------------------------------------------------

		public PermissionsForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			permissionsInfo = new ChangePermissionsControl();
			permissionsInfo.Parent = this;
			permissionsInfo.SetBounds(88,40, permissionsInfo.Size.Width, permissionsInfo.Size.Height);

			adminInfo = new ChangePermissionsAdminControl();
			adminInfo.Parent = this;
			adminInfo.SetBounds(88,40, adminInfo.Size.Width, adminInfo.Size.Height);

			systemInfo = new ChangePermissionsSystemControl();
			systemInfo.Parent = this;
			systemInfo.SetBounds(88,40, systemInfo.Size.Width, systemInfo.Size.Height);
	
			installProgress = new InstallProgressControl();
			installProgress.Parent = this;
			installProgress.SetBounds(88,48, installProgress.Size.Width, installProgress.Size.Height);

            install = new Oritax.TaxSimp.Administration.SystemUpdate.Install();
			install.InstallActionProgressUpdate += new InstallProgressUpdateHandler(install_InstallActionProgressUpdate);
			install.InstallStepProgressUpdate += new InstallProgressUpdateHandler(install_InstallStepProgressUpdate);
			install.InstallError += new InstallErrorHandler(install_InstallError);

			installInformationLabel.Text = step1Label;
			permissionsInfo.Show();
			adminInfo.Hide();
			systemInfo.Hide();
			installProgress.Hide();
		}

		#endregion //Contructors

		#region Protected Methods ----------------------------------------------------

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#endregion //Protected Methods

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(PermissionsForm));
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.previousButton = new System.Windows.Forms.Button();
			this.nextButton = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.installInformationLabel = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cancelButton = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pictureBox2
			// 
			this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left)));
			this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(77)), ((System.Byte)(90)));
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.Location = new System.Drawing.Point(0, 0);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(88, 322);
			this.pictureBox2.TabIndex = 22;
			this.pictureBox2.TabStop = false;
			// 
			// previousButton
			// 
			this.previousButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.previousButton.Enabled = false;
			this.previousButton.Location = new System.Drawing.Point(260, 290);
			this.previousButton.Name = "previousButton";
			this.previousButton.TabIndex = 1;
			this.previousButton.Text = "< Previous";
			this.previousButton.Click += new System.EventHandler(this.previousButton_Click);
			// 
			// nextButton
			// 
			this.nextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.nextButton.Location = new System.Drawing.Point(348, 290);
			this.nextButton.Name = "nextButton";
			this.nextButton.TabIndex = 0;
			this.nextButton.Text = "Next >";
			this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.panel1.Controls.Add(this.installInformationLabel);
			this.panel1.Location = new System.Drawing.Point(88, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(440, 40);
			this.panel1.TabIndex = 40;
			// 
			// installInformationLabel
			// 
			this.installInformationLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.installInformationLabel.Location = new System.Drawing.Point(8, 8);
			this.installInformationLabel.Name = "installInformationLabel";
			this.installInformationLabel.Size = new System.Drawing.Size(384, 32);
			this.installInformationLabel.TabIndex = 0;
			this.installInformationLabel.Text = "General product installation information";
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.groupBox1.Location = new System.Drawing.Point(88, 274);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(424, 8);
			this.groupBox1.TabIndex = 41;
			this.groupBox1.TabStop = false;
			// 
			// cancelButton
			// 
			this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cancelButton.Location = new System.Drawing.Point(436, 290);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.TabIndex = 2;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
			// 
			// PermissionsForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(520, 319);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.nextButton);
			this.Controls.Add(this.previousButton);
			this.Controls.Add(this.pictureBox2);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "PermissionsForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "N-ABLE Install";
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		#region Private Methods ------------------------------------------------------

		private void nextButton_Click(object sender, System.EventArgs e)
		{
			installInformationLabel.ForeColor = Color.Black;

			switch (currentSelection)
			{
				case CurrentSelection.InstallStep1:
				{
					string errorText = "";
					if (getAllChangeSettings(out errorText))
					{
						previousButton.Enabled = true;
						permissionsInfo.Hide();
						adminInfo.Show();
						installInformationLabel.Text = step2Label;
					}
					else
					{
						installInformationLabel.Text = errorText;
						installInformationLabel.ForeColor = Color.Red;
						currentSelection--;
					}

					break;
				}
				case CurrentSelection.InstallStep2:
				{
					string errorText = "";
					if (getAdminChangeSettings(out errorText))
					{
						adminInfo.Hide();
						systemInfo.Show();
						installInformationLabel.Text = step3Label;
					}
					else
					{
						installInformationLabel.Text = errorText;
						installInformationLabel.ForeColor = Color.Red;
						currentSelection--;
					}
					break;
				}
				case CurrentSelection.InstallStep3:
				{
					string errorText = "";
					if (getSystemChangeSettings(out errorText))
					{
						resetInstallProgressControl();
						systemInfo.Hide();
						installProgress.Show();
						nextButton.Text = "Update";
						installInformationLabel.Text = updateLabel;
						installProgress.CurrentInstallStep = "Ready to Update. Click Update to proceed.";
					}
					else
					{
						installInformationLabel.Text = errorText;
						installInformationLabel.ForeColor = Color.Red;
						currentSelection--;
					}
					break;
				}
					
				case CurrentSelection.InstallProgress:
				{
					nextButton.Enabled = false;
					previousButton.Enabled = false;
					installProgress.ProgressBarScale = 2;
					if (install.ChangePermissions(installParameters))
					{
						nextButton.Enabled = true;
						nextButton.Text = "Finished";
					}
					else
					{
						nextButton.Enabled = true;
						previousButton.Enabled = true;
						currentSelection--;
					}
					
					break;
				}
				case CurrentSelection.Installed:
				{
					this.Close();
					break;
				}
			}

			currentSelection++;
		}

		private void cancelButton_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void previousButton_Click(object sender, System.EventArgs e)
		{
			currentSelection--;
			nextButton.Text = "Next >";
			installInformationLabel.ForeColor = Color.Black;

			switch (currentSelection)
			{
				case CurrentSelection.InstallStep1:
				{
					adminInfo.Hide();
					permissionsInfo.Show();
					previousButton.Enabled = false;
					installInformationLabel.Text = step1Label;
					break;
				}
				case CurrentSelection.InstallStep2:
				{
					systemInfo.Hide();
					adminInfo.Show();
					installInformationLabel.Text = step2Label;
					break;
				}
				case CurrentSelection.InstallStep3:
				{
					installProgress.Hide();
					systemInfo.Show();
					installInformationLabel.Text = step3Label;
					break;
				}

			}
		}

		private bool getAllChangeSettings(out string errorText)
		{
			bool settingsOK = true;
			errorText = "";

			if (permissionsInfo.SQLAdminAccount.Length != 0)
			{
				installParameters.dbAdminUserName = permissionsInfo.SQLAdminAccount;
				installParameters.dbAdminPassword = permissionsInfo.SQLAdminPassword;
			}
			else
			{
				settingsOK = false;
				errorText = "Please specify a SQL administrator account";
			}

			if (permissionsInfo.InstallationLocation.Length != 0)
			{
				installParameters.installLocation = permissionsInfo.InstallationLocation;
				installParameters.databaseName = Path.GetFileName(installParameters.installLocation);
			}
			else
			{
				settingsOK = false;
				errorText = "Please specify the installation location";
			}

			if (permissionsInfo.SQLServerName.Length != 0)
			{
				installParameters.databaseServer = permissionsInfo.SQLServerName;
			}
			else
			{
				settingsOK = false;
				errorText = "Please specify a SQL Server Name";
			}
			return settingsOK;
		}

		private bool getAdminChangeSettings(out string errorText)
		{
			bool settingsOK = true;
			errorText = "";

			if (adminInfo.ChangeNableAdminAccount)
			{
				if (adminInfo.NableAdminAccount.Length != 0)
				{
					installParameters.changeNableAdminUser = true;
					installParameters.nableAdminUserName = adminInfo.NableAdminAccount;	
				}
				else
				{
					settingsOK = false;
					errorText = "You have selected to change the N-ABLE Administrator Account. Please specify an account name.";
				}

				if (adminInfo.NableAdminPassword == adminInfo.NableAdminReeneteredPassword)
				{
					installParameters.nableAdminPassword = adminInfo.NableAdminPassword;
				}
				else
				{
					settingsOK = false;
					errorText = "The passwords that you have entered for the N-ABLE Administrator Account do not match.";
				}

				if (adminInfo.NableAdminPassword.Length == 0)
				{
					settingsOK = false;
					errorText = "A password is required for the N-ABLE Administrator Account.";
				}

                PasswordDS data = new PasswordDS();
                SAS70PasswordSM pwdSM = new SAS70PasswordSM();
                if (!pwdSM.CheckRulesForNewUser(adminInfo.NableAdminAccount,
                    adminInfo.NableAdminPassword, data.Tables[PasswordDS.PasswordRulesTable]))
                {
                    settingsOK = false;
                    errorText = "The passwords that you have entered for the N-ABLE Administrator Account " +
                        "do not conform to N-ABLE password creation rules";
                }
            }
			else
			{
				installParameters.changeNableAdminUser = false;
			}

			return settingsOK;
		}

		private bool getSystemChangeSettings(out string errorText)
		{
			bool settingsOK = true;
			errorText = "";
			
			if (systemInfo.ChangeNableSystemDatbaseAccount)
			{
				if (systemInfo.NableSystemDatabaseAccount.Length != 0)
				{
					installParameters.changedbSysUser = true;
					installParameters.dbSysUserName = systemInfo.NableSystemDatabaseAccount;	
				}
				else
				{
					settingsOK = false;
					errorText = "You have selected to change the N-ABLE Database System Account. Please specify an account name.";
				}

				if (systemInfo.NableSystemDatabasePassword == systemInfo.NableSystemDatabaseReenteredPassword)
				{
					installParameters.dbSysPassword = systemInfo.NableSystemDatabasePassword;
				}
				else
				{
					settingsOK = false;
					errorText = "The passwords that you have entered for the N-ABLE System Database Account do not match.";
				}

				if (systemInfo.NableSystemDatabasePassword.Length == 0)
				{
					settingsOK = false;
					errorText = "A password is required for the N-ABLE Database System Account.";
				}

				installParameters.websiteLocation = installParameters.installLocation;

				if (settingsOK)
				{
					installParameters.encryptedConnectionString = "server=" + installParameters.databaseServer + ";database=" + installParameters.databaseName + ";uid=" + Encryption.EncryptData(installParameters.dbSysUserName) + ";pwd=" + Encryption.EncryptData(installParameters.dbSysPassword); 
				}
			}
			else
			{
				installParameters.changedbSysUser = false;
			}

			return settingsOK;
		}

		private void initializeInstallParameters()
		{
			installParameters.installBase = "";
			installParameters.installPackageLocation = "";
			installParameters.workingLocation = "";
			installParameters.websiteLocation = "";
			installParameters.installCode = "";
			installParameters.installLocation = "";

			installParameters.databaseName = "";
			installParameters.databaseServer = "";
			installParameters.dbSysUserName = "";
			installParameters.dbSysPassword = "";
			installParameters.dbAdminUserName = "";
			installParameters.dbAdminPassword = "";
			installParameters.nableAdminUserName = "";
			installParameters.nableAdminPassword = "";

			installParameters.changedbSysUser = false;
			installParameters.changeNableAdminUser = false;

			installParameters.connectionString = "";
			installParameters.adminConnectionString = "";
			installParameters.encryptedConnectionString = "";
		}

		private void install_InstallActionProgressUpdate(object sender, InstallProgressEventArgs e)
		{
			installProgress.CurrentInstallAction = e.Message;
			installProgress.Refresh();
		}

		private void install_InstallStepProgressUpdate(object sender, InstallProgressEventArgs e)
		{
			installProgress.CurrentInstallStep = e.Message;
			installProgress.CurrentInstallStepForeColor = Color.Black;
			installProgress.CurrentInstallAction = "";
			installProgress.IncrementProgressBar(1);
			installProgress.Refresh();
		}

		private void install_InstallError(object sender, InstallErrorEventArgs e)
		{
			installProgress.CurrentInstallStep = e.Message;
			installProgress.CurrentInstallStepForeColor = Color.Red;
			installProgress.CurrentInstallAction = "";
			installProgress.Refresh();
		}

		private void resetInstallProgressControl()
		{
			installProgress.CurrentInstallStep = "";
			installProgress.CurrentInstallAction = "";
			installProgress.CurrentInstallStepForeColor = Color.Black;
		}
		
		#endregion //Private Methods

	}
}
