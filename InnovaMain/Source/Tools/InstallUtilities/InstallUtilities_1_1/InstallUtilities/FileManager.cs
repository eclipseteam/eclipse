using System;
using System.Reflection;
using System.IO;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.InstallInfo;
using Oritax.TaxSimp.Services.CMBroker;
using System.Collections;
using System.Diagnostics;

namespace Oritax.TaxSimp.Administration.SystemUpdate
{
	/// <summary>
	/// Summary description for FileManager.
	/// </summary>
	public class FileManager
	{
		public FileManager()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public IAssembly ReadNAbleAssembly(string assemblyPathName)
		{
			IAssembly nAbleAssembly=new AssemblyDictionary.Assembly();
			PopulateNAbleAssembly(nAbleAssembly,assemblyPathName);
			return nAbleAssembly;
		}

		public DeploymentUnitInstallJob.NAbleAssemblyName[] FindNAbleAssembly(string installPath,string baseName)
		{
			string [] assemblyNames=Directory.GetFiles(installPath,baseName+"*.dll");

			DeploymentUnitInstallJob.NAbleAssemblyName[] nAbleAssemblyNames=new DeploymentUnitInstallJob.NAbleAssemblyName[assemblyNames.Length];
			for(int i=0;i<assemblyNames.Length;i++)
			{
				nAbleAssemblyNames[i]=new DeploymentUnitInstallJob.NAbleAssemblyName(assemblyNames[i]);
			}

			return nAbleAssemblyNames;
		}

		public IAssembly PopulateNAbleAssembly(IAssembly nAbleAssembly,string assemblyPathName)
		{
			int majorVersion;
			int minorVersion;
			int dataFormat;
			int revision;

			Assembly assembly=Assembly.LoadFile(assemblyPathName);
			object[] assemblyInstallInfoAttributes=assembly.GetCustomAttributes(typeof(AssemblyInstallInfoAttribute),false);
			if(null!=assemblyInstallInfoAttributes && assemblyInstallInfoAttributes.Length==1)
			{
				AssemblyInstallInfoAttribute assemblyInstallInfoAttribute=(AssemblyInstallInfoAttribute)(assemblyInstallInfoAttributes[0]);
				string ownerTypeIDStr=assemblyInstallInfoAttribute.AssemblyID;
				nAbleAssembly.ID=new Guid(ownerTypeIDStr);
				nAbleAssembly.Name=assemblyInstallInfoAttribute.AssemblyName;
				nAbleAssembly.DisplayName=assemblyInstallInfoAttribute.AssemblyDisplayName;
			}
			else
				throw new Exception("AssemblyInstallInfo Attribute was not present on "+assembly.FullName+" file");

			AssemblyName assemblyName=assembly.GetName();
			Version version=assemblyName.Version;
			majorVersion=version.Major;
			minorVersion=version.Minor;

			object[] AssemblyInformationalVersionAttributes=assembly.GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute),false);
			if(null!=AssemblyInformationalVersionAttributes && AssemblyInformationalVersionAttributes.Length==1)
			{
				AssemblyInformationalVersionAttribute assemblyInformationalVersionAttribute=(AssemblyInformationalVersionAttribute)(AssemblyInformationalVersionAttributes[0]);
				Version infoVersion=new Version(assemblyInformationalVersionAttribute.InformationalVersion);
				dataFormat=infoVersion.Build;
				revision=infoVersion.Revision;
			}
			else
				throw new Exception("AssemblyVersion Attribute was not present on "+assembly.FullName+" file");

			nAbleAssembly.VersionNumber=new AssemblyDictionary.AssemblyVersionNumber(majorVersion,minorVersion,dataFormat,revision);

			nAbleAssembly.StrongName=assembly.FullName;

			return nAbleAssembly;
		}

		public IComponent ReadComponent(string assemblyPathName)
		{
			Assembly assembly=Assembly.LoadFile(assemblyPathName);
			IComponent component=new ComponentDictionary.Component();

			object[] componentObsoleteAttributes = assembly.GetCustomAttributes(typeof(ComponentObsoleteAttribute),false);
			object[] componentInstallInfoAttributes=assembly.GetCustomAttributes(typeof(ComponentInstallInfoAttribute),false);

			if(null!=componentInstallInfoAttributes && componentInstallInfoAttributes.Length==1)
			{
				ComponentInstallInfoAttribute componentInstallInfoAttribute=(ComponentInstallInfoAttribute)(componentInstallInfoAttributes[0]);
				component.ID=new Guid(componentInstallInfoAttribute.ComponentID);
				component.Name=componentInstallInfoAttribute.ComponentName;
				component.DisplayName=componentInstallInfoAttribute.DisplayName;
				component.Category=componentInstallInfoAttribute.Category;
				component.Applicability=componentInstallInfoAttribute.Applicability;
			}
			else
				throw new Exception("ComponentInstallInfo Attribute was not present on "+assembly.FullName+" file");

			// if the obsolete attribute is present then mark the component as obsolete
			component.Obsolete = (componentObsoleteAttributes != null && componentObsoleteAttributes.Length > 0);

			return component;
		}

        public IEnumerable GetComponentWorkpapers(string name)
        {
            Assembly assembly = Assembly.LoadFrom(name);
            Module[] modules = assembly.GetLoadedModules();
            foreach (Module module in modules)
            {
                Debug.WriteLine(module.Name);           
                Type[] types = module.GetTypes();
                foreach (Type type in types)
                {
                    if (IsTypeOf(type, "CalculationModuleBase"))
                    {
                        PropertyInfo info = type.GetProperty("WorkPapers");
                        if (info != null)
                        {
                            object instance = Activator.CreateInstance(type);
                            IEnumerable wp = (IEnumerable)info.GetValue(instance, null);
                            return wp;
                        }
                    }
                }
            }
            return null;
        }

        private bool IsTypeOf(Type type, string name)
        {
            if (type.Name == name)
                return true;
            else if (type.BaseType != null)
                return IsTypeOf(type.BaseType, name);
            else
                return false;
        }
		public IComponentVersion ReadComponentVersion(string assemblyPathName)
		{
			IComponentVersion componentVersion=new ComponentDictionary.ComponentVersion();
			PopulateNAbleAssembly(componentVersion,assemblyPathName);

			Assembly assembly=Assembly.LoadFile(assemblyPathName);

			object[] componentVersionObsoleteAttributes = assembly.GetCustomAttributes(typeof(ComponentVersionObsoleteAttribute),false);
			object[] componentVersionInstallInfoAttributes=assembly.GetCustomAttributes(typeof(ComponentVersionInstallInfoAttribute),false);

			if(null!=componentVersionInstallInfoAttributes && componentVersionInstallInfoAttributes.Length==1)
			{
				ComponentVersionInstallInfoAttribute componentVersionInstallInfoAttribute=(ComponentVersionInstallInfoAttribute)(componentVersionInstallInfoAttributes[0]);
				componentVersion.StartDate=DateTime.Parse(componentVersionInstallInfoAttribute.StartDate);
				componentVersion.EndDate=DateTime.Parse(componentVersionInstallInfoAttribute.EndDate);
				componentVersion.PersisterAssemblyStrongName=componentVersionInstallInfoAttribute.PersisterAssembly;
				componentVersion.PersisterClass=componentVersionInstallInfoAttribute.PersisterClass;
				componentVersion.ImplementationClass=componentVersionInstallInfoAttribute.ImplementationClass;
			}
			else
				throw new Exception("ComponentVersionInstallInfo Attribute was not present on "+assembly.FullName+" file");

			// if the obsolete attribute is present then mark the component as obsolete
			componentVersion.Obsolete = (componentVersionObsoleteAttributes != null && componentVersionObsoleteAttributes.Length > 0);

			object[] componentInstallInfoAttributes=assembly.GetCustomAttributes(typeof(ComponentInstallInfoAttribute),false);
			if(null!=componentInstallInfoAttributes && componentInstallInfoAttributes.Length==1)
			{
				ComponentInstallInfoAttribute componentInstallInfoAttribute=(ComponentInstallInfoAttribute)(componentInstallInfoAttributes[0]);
				componentVersion.ComponentID=new Guid(componentInstallInfoAttribute.ComponentID);
			}
			else
				throw new Exception("ComponentInstallInfo Attribute was not present on "+assembly.FullName+" file");


			return componentVersion;
		}


		public DeploymentUnitInstallJob.InstallStateOption CheckAssemblyInstallFilesByName(string targetBinFolder,IAssembly newAssembly)
		{
			// Cases:
			// (1) File exists with the same name but with different DataModel and/or revision attribute values.
			// Determine if an assembly with the same name exists in the installation as a file.
			// (2) File exists with the same base name but with different version suffix and different major
			// and/or minor versions in the version attributes.
			// (All file names are of the form AAAAAAAAA_X_Y.BBB where AAAAAAAAA is the base name X is the major version integer, Y is
			// the minor version integer and BBB is the file extension. The base name and suffix fields may be any length and the
			// format is not relevant to the operation of the install and version and update mechanism.

			// Get the assembly versions that have the same name as the new assembly each assembly will be represented by a dll file
			// with a name of the above format.

			DeploymentUnitInstallJob.NAbleAssemblyName newNAbleAssemblyName=new DeploymentUnitInstallJob.NAbleAssemblyName(newAssembly.Name);

			foreach(string installedFileName in Directory.GetFiles(targetBinFolder,newNAbleAssemblyName.BaseName+@"_*.dll"))
			{
				DeploymentUnitInstallJob.NAbleAssemblyName installedNAbleAssemblyName=new DeploymentUnitInstallJob.NAbleAssemblyName(installedFileName);
				//				string archivePathName=this.archiveFolder+@"\"+installedNAbleAssemblyName.Name;

				//				File.Copy(installedFileName,archivePathName,true);

				//				Assembly installedAssembly=Assembly.LoadFile(archivePathName);

				Assembly installedAssembly=Assembly.LoadFile(installedFileName);

				AssemblyDictionary.AssemblyVersionNumber installedVersionNumber=ExtractVersionFromAssembly(installedAssembly);

				if(installedVersionNumber.MajorVersion!=installedNAbleAssemblyName.MajorVersion
					|| installedVersionNumber.MinorVersion!=installedNAbleAssemblyName.MinorVersion)
					throw new Exception("An installed dll file name: "+installedFileName+" does not match it's internal version attributes");

				AssemblyDictionary.AssemblyVersionNumber newVersionNumber=(AssemblyDictionary.AssemblyVersionNumber)newAssembly.VersionNumber;

				AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption differenceColumn=
					(installedVersionNumber<newVersionNumber).DifferenceColumn;

				switch(differenceColumn)
				{
					case AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption.NONE:
						return DeploymentUnitInstallJob.InstallStateOption.INSTALLED_OK;
					case AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption.MAJORVERSION:
					case AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption.MINORVERSION:
						break;
					case AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption.DATAFORMAT:
						return DeploymentUnitInstallJob.InstallStateOption.INSTALLED_EARLIER_DATAMODEL;
					case AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption.REVISION:
						return DeploymentUnitInstallJob.InstallStateOption.INSTALLED_EARLIER_REVISION;
					default:
						return DeploymentUnitInstallJob.InstallStateOption.UNDETERMINED;
				}
			}

			return DeploymentUnitInstallJob.InstallStateOption.NOT_INSTALLED;
		}

		AssemblyDictionary.AssemblyVersionNumber ExtractVersionFromAssembly(Assembly assembly)
		{
			int majorVersion,minorVersion,dataFormat,revision;

			AssemblyName installedAssemblyName=assembly.GetName();
			Version version=installedAssemblyName.Version;

			majorVersion=version.Major;
			minorVersion=version.Minor;

			object[] assemblyInformationalVersionAttributes=assembly.GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute),false);
			if(null==assemblyInformationalVersionAttributes || assemblyInformationalVersionAttributes.Length!=1)
				throw new Exception("An installed dll file: does not have informational version attribute.");

			AssemblyInformationalVersionAttribute assemblyInformationalVersionAttribute=(AssemblyInformationalVersionAttribute)assemblyInformationalVersionAttributes[0];
			Version informationalVersion=new Version(assemblyInformationalVersionAttribute.InformationalVersion);
			dataFormat=informationalVersion.Build;
			revision=informationalVersion.Revision;
			return new AssemblyDictionary.AssemblyVersionNumber(majorVersion,minorVersion,dataFormat,revision);
		}


		public Attribute GetCustomAttribute(string assemblyDllName,Type attributeClass)
		{
			Assembly assembly=Assembly.LoadFile(assemblyDllName);

			object[] dataModelInstallInfoAttributes=assembly.GetCustomAttributes(attributeClass,false);

			if(1==dataModelInstallInfoAttributes.Length)
			{
				DataModelInstallInfoAttribute dataModelInstallInfoAttribute=(DataModelInstallInfoAttribute)dataModelInstallInfoAttributes[0];
				return dataModelInstallInfoAttribute;
			}
			else
				throw new Exception("Custom Attribute does not exist in specified assembly");
		}


		public bool IsAttributeDefined(string attributeDllName,Type attributeClass)
		{
            if (IsSilverlightControl(attributeDllName))
                return false;
            else
            {
                Assembly assembly = Assembly.LoadFile(attributeDllName);
                return assembly.IsDefined(attributeClass, false);
            }
		}

        public static bool IsSilverlightControl(string fileName)
        {
            if (!string.IsNullOrEmpty(fileName) && fileName.EndsWith(".xap"))
                return true;
            else
                return false;
        }
		public void InstallFile(string targetBinFolder,string tempFolder,string relativePathName)
		{
			if(!Directory.Exists(targetBinFolder))
				Directory.CreateDirectory(targetBinFolder);

			string fileName=relativePathName;

			int lastSlashIndex=relativePathName.LastIndexOf(@"\");
			if(lastSlashIndex!=-1)
				fileName=relativePathName.Substring(lastSlashIndex+1);

            try
            {
                File.Copy(tempFolder + @"\" + relativePathName, targetBinFolder + @"\" + fileName, true);
            }
            catch
            { }
            SetDirectoryFilesToNormal(new DirectoryInfo(targetBinFolder));
		}

		public void InstallAssociatedFiles(string targetWebFolder,string tempFolder,string dLLFileName)
		{
			Assembly newAssembly=Assembly.LoadFile(tempFolder+@"\"+dLLFileName);

			object[] associatedFileInstallInfoAttributes=newAssembly.GetCustomAttributes(typeof(AssociatedFileInstallInfoAttribute),false);

			if(null!=associatedFileInstallInfoAttributes && associatedFileInstallInfoAttributes.Length==1)
			{
				AssociatedFileInstallInfoAttribute associatedFileInstallInfoAttribute=(AssociatedFileInstallInfoAttribute)associatedFileInstallInfoAttributes[0];

				foreach(AssociatedFileInstallInfoAttribute.AssociatedFileEntry associatedFileEntry in associatedFileInstallInfoAttribute.AssociatedFileEntries)
				{
					string fileName=associatedFileEntry.FileName;
					string installFolderPathName=targetWebFolder;

					if(!File.Exists(tempFolder+@"\"+fileName))
						throw new Exception("Dll file "+fileName+" was not present");

					if(null!=associatedFileEntry.RelativeInstallLocation)
					{
						installFolderPathName+=@"\"+associatedFileEntry.RelativeInstallLocation;
					}

					if(!Directory.Exists(installFolderPathName))
						Directory.CreateDirectory(installFolderPathName);

					// Copy the associated file to the target folder
                    try
                    {
                        File.Copy(tempFolder + @"\" + fileName, installFolderPathName + @"\" + fileName, true);
                    }
                    catch
                    { }

                    SetDirectoryFilesToNormal(new DirectoryInfo(installFolderPathName));
				}
			}
		}

		public void RegisterComFiles(string targetWebFolder,string tempFolder,string dLLFileName)
		{
			Assembly newAssembly = Assembly.LoadFile(tempFolder+@"\"+dLLFileName);

			object[] comFileRegisterInfoAttributes = newAssembly.GetCustomAttributes(typeof(ComFileRegisterInfoAttribute),false);

			if(comFileRegisterInfoAttributes != null && comFileRegisterInfoAttributes.Length == 1)
			{
				ComFileRegisterInfoAttribute registerComAttribute = (ComFileRegisterInfoAttribute)comFileRegisterInfoAttributes[0];

				foreach(ComFileRegisterInfoAttribute.ComFileEntry comFileEntry in registerComAttribute.ComFileEntries)
				{
					string fileName = comFileEntry.FileName;
					string installFolderPathName = targetWebFolder;

					if(comFileEntry.RelativeInstallLocation != null)
					{
						installFolderPathName += @"\" + comFileEntry.RelativeInstallLocation;
					}

					registerComFile(Path.Combine(installFolderPathName, fileName));
				}
			}
		}
		
		public static void SetDirectoryFilesToNormal(DirectoryInfo info)
        {
            if (info.Exists)
            {
                FileInfo[] fileInfos = info.GetFiles();
                foreach (FileInfo fileInfo in fileInfos)
                {
                    fileInfo.Attributes = FileAttributes.Normal;
                }

                DirectoryInfo[] directoryInfos = info.GetDirectories();
                foreach (DirectoryInfo directoryInfo in directoryInfos)
                {
                    SetDirectoryFilesToNormal(directoryInfo);
                }
            }
		}

		private void registerComFile(string comFileName)
		{
			if(!File.Exists(comFileName))
			{
				throw new Exception("Dll file "+comFileName+" was not present");
			}
			
			string output;
			DeploymentUnitInstallJob.RunProcess("regsvr32", null, new string[]{"/s", comFileName}, out output, true);
		}

		public void UpdateDeployedVersion(string tempLocation, string deployedPath)
		{
			cleanAssemblyFolder();

			string legacyFolder = Path.Combine(tempLocation, "legacy_1_1");
 
			if (!Directory.Exists(legacyFolder))
			{
				//OnMigrationError(new MigrationMessageEventArgs("The legacy folder does not exist in the target directory"));
				return;
			}

			foreach (string filePath in Directory.GetFiles(legacyFolder, "*.dll"))
			{
				string fileName = Path.GetFileName(filePath);
				string destFilePath = Path.Combine(deployedPath, fileName);
				
				try
				{
					File.Copy(filePath, destFilePath, true);
                    SetDirectoryFilesToNormal(new DirectoryInfo(legacyFolder));
				}
				catch //(Exception ex)
				{
				}
			}
		}

		private void cleanAssemblyFolder()
		{
			string appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
			string assemblyPath = Path.Combine(appDataPath, "assembly");

			if (Directory.Exists(assemblyPath))
			{
				Directory.Delete(assemblyPath, true);
			}
		}
	}
}
