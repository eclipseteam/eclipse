using System;
using System.IO;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;

using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CalculationInterface;
using System.Collections;

namespace Oritax.TaxSimp.Administration.SystemUpdate
{
	/// <summary>
	/// This assembly acts as a proxy assembly so that Migrate executables can load up AppDomains
	/// with the Base Directory of the AppDomain set to a particular directory.
	/// 
	/// This assembly should always be weakly named and always have a version of 1.0.0.0
	/// so it can be resolved by the loader
	/// 
	/// This assembly should never, never, ever have a reference set to any DLL as it would then
	/// become version dependent. The whole reason for using reflection below is to isolate the
	/// code from version dependencies.
	/// </summary>
	public class FileManagerProxy: MarshalByRefObject
	{
		private AppDomain appDomain;
		private object fileManager;

		//entry point for CreateInstanceAndUnwrap
		public FileManagerProxy()
		{
			//Dynamically load the broker and create an instance
			appDomain=AppDomain.CurrentDomain;
			Assembly ass=Assembly.LoadFrom(Path.Combine(appDomain.BaseDirectory, "InstallUtilities_1_1.dll"));
			Type tType=ass.GetType("Oritax.TaxSimp.Administration.SystemUpdate.FileManager");
			fileManager=Activator.CreateInstance(tType);
			
		}

		public AppDomain AppDomain{get{return this.appDomain;}}

		public IAssembly ReadNAbleAssembly(string assemblyPathName)
		{
			IAssembly assembly = null;
			object[] args=new object[]{assemblyPathName};

			try
			{
				assembly = (IAssembly)fileManager.GetType().InvokeMember("ReadNAbleAssembly",BindingFlags.InvokeMethod,null,fileManager,args);
			}
			catch (Exception ex)
			{
				throw ex.InnerException;
			}
			return assembly;
		}

		public IComponent ReadComponent(string assemblyPathName)
		{
			IComponent component = null;
			object[] args=new object[]{assemblyPathName};

			try
			{
				component = (IComponent)fileManager.GetType().InvokeMember("ReadComponent",BindingFlags.InvokeMethod,null,fileManager,args);
			}
			catch (Exception ex)
			{
				throw ex.InnerException;
			}
			return component;
		}

		public IComponentVersion ReadComponentVersion(string assemblyPathName)
		{
			IComponentVersion componentVersion = null;
			object[] args=new object[]{assemblyPathName};

			try
			{
				componentVersion = (IComponentVersion)fileManager.GetType().InvokeMember("ReadComponentVersion",BindingFlags.InvokeMethod,null,fileManager,args);
			}
			catch (Exception ex)
			{
				throw ex.InnerException;
			}
			return componentVersion;
		}
        public IEnumerable GetComponentWorkpapers(string assemblyPathName)
        {
            IEnumerable wps = null;
            object[] args = new object[] { assemblyPathName };

            try
            {
                wps = (IEnumerable)fileManager.GetType().InvokeMember("GetComponentWorkpapers", BindingFlags.InvokeMethod, null, fileManager, args);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
            return wps;
        }
		public DeploymentUnitInstallJob.NAbleAssemblyName[] FindNAbleAssembly(string installPath,string baseName)
		{
			DeploymentUnitInstallJob.NAbleAssemblyName[] assemblyNames = null;
			object[] args=new object[]{installPath,baseName};

			try
			{
				assemblyNames = (DeploymentUnitInstallJob.NAbleAssemblyName[])fileManager.GetType().InvokeMember("FindNAbleAssembly",BindingFlags.InvokeMethod,null,fileManager,args);
			}
			catch (Exception ex)
			{
				throw ex.InnerException;
			}
			return assemblyNames;
		}

		public DeploymentUnitInstallJob.InstallStateOption CheckAssemblyInstallFilesByName(string targetBinFolder,IAssembly newAssembly)
		{
			DeploymentUnitInstallJob.InstallStateOption installOption;
			object[] args=new object[]{targetBinFolder,newAssembly};
			
			try
			{
				installOption = (DeploymentUnitInstallJob.InstallStateOption)fileManager.GetType().InvokeMember("CheckAssemblyInstallFilesByName",BindingFlags.InvokeMethod,null,fileManager,args);
			}
			catch (Exception ex)
			{
				throw ex.InnerException;
			}
			return installOption;
		}

		public void InstallFile(string targetBinFolder,string tempFolder,string dLLFileName)
		{
			object[] args=new object[]{targetBinFolder,tempFolder,dLLFileName};

			try
			{
				fileManager.GetType().InvokeMember("InstallFile",BindingFlags.InvokeMethod,null,fileManager,args);
			}
			catch (Exception ex)
			{
				throw ex.InnerException;
			}
		}

		public void InstallAssociatedFiles(string targetWebFolder,string tempFolder,string dLLFileName)
		{
			object[] args=new object[]{targetWebFolder,tempFolder,dLLFileName};
			try
			{
				fileManager.GetType().InvokeMember("InstallAssociatedFiles",BindingFlags.InvokeMethod,null,fileManager,args);
			}
			catch (Exception ex)
			{
                Exception newEx = new Exception("Processing DLL " + dLLFileName, ex.InnerException);
                throw newEx;
			}
		}

		public void RegisterComFiles(string targetWebFolder,string tempFolder,string dLLFileName)
		{
			object[] args=new object[]{targetWebFolder,tempFolder,dLLFileName};
			try
			{
				fileManager.GetType().InvokeMember("RegisterComFiles",BindingFlags.InvokeMethod,null,fileManager,args);
			}
			catch (Exception ex)
			{
				throw ex.InnerException;
			}
		}

		public Attribute GetCustomAttribute(string assemblyDllName,Type attributeClass)
		{
			Attribute attribute = null;
			object[] args=new object[]{assemblyDllName,attributeClass};

			try
			{
				attribute = (Attribute)fileManager.GetType().InvokeMember("GetCustomAttribute",BindingFlags.InvokeMethod,null,fileManager,args);
			}
			catch (Exception ex)
			{
				throw ex.InnerException; 
			}
			return attribute;
		}

		public bool IsAttributeDefined(string assemblyDllName,Type attributeClass)
		{
			bool isAttributeDefined = false;
			object[] args=new object[]{assemblyDllName,attributeClass};
			
			try
			{
				isAttributeDefined = (bool)fileManager.GetType().InvokeMember("IsAttributeDefined",BindingFlags.InvokeMethod,null,fileManager,args);
			}
			catch (Exception ex)
			{
				throw ex.InnerException; 
			}
			return isAttributeDefined; 
		}

		public static FileManagerProxy CreateFileManager(string installFolderPath)
		{
			FileManagerProxy fileManagerProxy=null;

			AppDomain appDomain = AppDomain.CreateDomain("InstallDomain",null,installFolderPath,"bin",false);
			appDomain.InitializeLifetimeService();

            //fileManagerProxy=(FileManagerProxy)appDomain.CreateInstanceAndUnwrap(
            //    "InstallUtilities_1_1",
            //    "Oritax.TaxSimp.Administration.SystemUpdate.FileManagerProxy",
            //    false,
            //    System.Reflection.BindingFlags.CreateInstance,
            //    null,
            //    null,
            //    null,
            //    null,
            //    null);

            fileManagerProxy = (FileManagerProxy)appDomain.CreateInstanceAndUnwrap(
                "InstallUtilities_1_1",
                "Oritax.TaxSimp.Administration.SystemUpdate.FileManagerProxy",
                false,
                System.Reflection.BindingFlags.CreateInstance,
                null,
                null,
                null,
                null);

			return fileManagerProxy;
		}

		public static void DestroyFileManager(FileManagerProxy fileManager)
		{
			AppDomain.Unload(fileManager.AppDomain);
		}

		public override object InitializeLifetimeService( )
		{
			// note: do not remove this method as it will drop the connection after a period of time
			// this will keep the object alive until the app domain shuts down
			return null;
		}
	}
}
