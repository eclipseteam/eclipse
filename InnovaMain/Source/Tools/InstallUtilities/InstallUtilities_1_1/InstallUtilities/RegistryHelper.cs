using System;
using Microsoft.Win32;

namespace Oritax.TaxSimp.Administration.SystemUpdate
{
	/// <summary>
	/// Summary description for RegistryHelper.
	/// </summary>
	public class RegistryHelper
	{
		#region Fields ---------------------------------------------------------------

		private string companyName;
		private string productName;
		private string parentKey;

		#endregion // Fields

		#region Constructors ---------------------------------------------------------

		/// <summary>
		///		Initializes a new instance of the RegistrySettings class.
		/// </summary>
		/// <param name="companyName">
		///		The key name that appears directly beneath the SOFTWARE key.
		/// </param>
		/// <param name="productName">
		///		The key name that appears directly beneath the company name key.
		/// </param>
		/// <remarks>
		///		In the following key:
		///			HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\.NetFramework
		///		The company name is "Microsoft" and the product name is ".NetFramework".
		/// </remarks>
		public RegistryHelper(string companyName, string productName)
		{
			if (companyName == null)
			{
				throw new ArgumentNullException("companyName");
			}
			if (companyName.Length == 0)
			{
				throw new ArgumentException("companyName");
			}
			if (productName == null)
			{
				throw new ArgumentNullException("productName");
			}
			if (productName.Length == 0)
			{
				throw new ArgumentException("productName");
			}

			this.companyName = companyName;
			this.productName = productName;
			this.parentKey = string.Format(@"SOFTWARE\{0}\{1}\", companyName, productName);
		}

		#endregion // Constructors

		#region Public Properties ----------------------------------------------------
		
		/// <summary>
		///		Gets the registry key that the RegistrySettings class uses as the parent key
		///		used for reading and writing settings.
		/// </summary>
		public string ParentKey
		{
			get
			{
				return this.parentKey;
			}
		}

		#endregion // Public Properties

		#region Public Methods -------------------------------------------------------

		/// <summary>
		///		Sets a Windows registry value.
		/// </summary>
		/// <param name="name">
		///		The name of the registry value.
		/// </param>
		/// <param name="regValue">
		///		The value to set.
		/// </param>
		public void SetRegistryValue(string name, object regValue)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("name");
			}

			RegistryKey key = Registry.LocalMachine.CreateSubKey(parentKey);
			if (key != null)
			{
				key.SetValue(name, regValue);
			}
		}

		/// <summary>
		///		Gets a Windows registry value by providing a default value.
		/// </summary>
		/// <param name="name">
		///		The name of the registry value.
		/// </param>
		/// <param name="defaultValue">
		///		The value to be returned if the registry value does not exist.
		/// </param>
		/// <returns>
		///		An Object.
		/// </returns>
		public object GetRegistryValue(string name, object defaultValue)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("name");
			}

			object returnObject = null;

			RegistryKey key = Registry.LocalMachine.OpenSubKey(parentKey);

			if (key == null)
			{
				returnObject = defaultValue; 
			}
			else
			{
				returnObject = key.GetValue(name, defaultValue);
			}

			return returnObject;
		}

		/// <summary>
		///		Gets a Windows registry value.
		/// </summary>
		/// <param name="name">
		///		The name of the registry value.
		/// </param>
		/// <returns>
		///		An Object that is the registry value requested.
		/// </returns>
		public object GetRegistryValue(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("name");
			}

			object returnObject = null;

			RegistryKey key = Registry.LocalMachine.OpenSubKey(parentKey);

			if (key != null)
			{
				returnObject = key.GetValue(name);
			}

			return returnObject;
		}

		/// <summary>
		///		Gets a value from the Windows registry.
		/// </summary>
		/// <param name="name">
		///		The name of the registry value.
		/// </param>
		/// <returns>
		///		The value from the registry as an object. A null reference is returned if the value does not exist.
		/// </returns>
		public object GetRegistryValue(string subKeyName, string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("name");
			}

			object returnObject = null;

			RegistryKey key = null;

			if (subKeyName == null)
			{
				key = Registry.LocalMachine.OpenSubKey(parentKey);
			}
			else
			{
				key = Registry.LocalMachine.OpenSubKey(parentKey +@"\" +subKeyName);
			}

			if (key != null)
			{
				returnObject = key.GetValue(name);
			}

			return returnObject;
		}

		#endregion // Public Methods

		#region Private and Protected Methods ----------------------------------------

		#endregion // Private and Protected Methods
	}
}
