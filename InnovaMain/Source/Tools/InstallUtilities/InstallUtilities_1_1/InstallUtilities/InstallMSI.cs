using System;
using System.Diagnostics;
using System.Text;

namespace Oritax.TaxSimp.Administration.SystemUpdate
{
	/// <summary>
	///		Provides functionality to install MSI files.
	/// </summary>
	public class InstallMSI
	{
		/// <summary>
		///		This method will install the specified MSI file
		/// </summary>
		/// <param name="msiFileName">
		///		Full path and filename of the MSi to install.
		/// </param>
		public static void Install(string msiFileName, ProgressUpdate progressUpdate)
		{
			try
			{
				Process process = new Process();
				process.StartInfo.FileName ="msiexec";
				process.StartInfo.Arguments = "/i" + '"' +msiFileName + '"' +" /qn REBOOT=ReallySuppress";
				
				process.Start();
				while(!process.HasExited)
				{
					System.Threading.Thread.Sleep(1000);
				}
			
				if(process.ExitCode == 1)
					progressUpdate("NOTE: Machine needs to be rebooted for install to take effect.");
			
			}
			catch(Exception e)
			{
				progressUpdate(e.Message);
			}
		}

        public static void InstallExe(string exeFileName, string args,  ProgressUpdate progressUpdate)
        {
            try
            {
                StringBuilder arguments = new StringBuilder();
                Process process = new Process();
                process.StartInfo.FileName = exeFileName;
                arguments.Append(args);
                process.StartInfo.Arguments = arguments.ToString();
                //process.StartInfo.CreateNoWindow = true;
                process.StartInfo.UseShellExecute = false;
                process.Start();
               
                while (!process.HasExited)
                {
                    System.Threading.Thread.Sleep(1000);
                }

                if (process.ExitCode == 1)
                    progressUpdate("NOTE: Machine needs to be rebooted for install to take effect.");

            }
            catch (Exception e)
            {
                progressUpdate(e.Message);
            }
        }

	}
}
