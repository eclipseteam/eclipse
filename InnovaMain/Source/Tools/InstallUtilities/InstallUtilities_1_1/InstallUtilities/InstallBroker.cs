using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using System.Threading;

using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.Administration.SystemUpdate
{
	/// <summary>
	/// This assembly acts as a proxy assembly so that Migrate executables can load up AppDomains
	/// with the Base Directory of the AppDomain set to a particular directory.
	/// 
	/// This assembly should always be weakly named and always have a version of 1.0.0.0
	/// so it can be resolved by the loader
	/// 
	/// This assembly should never, never, ever have a reference set to any DLL as it would then
	/// become version dependent. The whole reason for using reflection below is to isolate the
	/// code from version dependencies.
	/// </summary>
	public class InstallBroker: MarshalByRefObject
	{
		private object broker=null;	//must always be an object
		private string basePath = string.Empty;
        
		private AppDomain appDomain;

		//entry point for CreateInstanceAndUnwrap
		public InstallBroker(string brokerAssemblyName,string db)
		{
			//Dynamically load the broker and create an instance
			
			appDomain=AppDomain.CurrentDomain;
			Assembly ass = null;
			try
			{
				ass = Assembly.LoadFrom(Path.ChangeExtension(Path.Combine(appDomain.BaseDirectory, brokerAssemblyName), ".dll"));
			}
			catch
			{
				ass = Assembly.LoadFrom(Path.ChangeExtension(Path.Combine(Path.Combine(appDomain.BaseDirectory, "bin"), brokerAssemblyName), ".dll"));
			}
				
			Type tType=ass.GetType("Oritax.TaxSimp.Services.CMBroker.CMBroker");
				
			object [] args = {db};
			broker = Activator.CreateInstance(tType, args);
			
			LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");
				
			if(brokerSlot == null)
				brokerSlot = Thread.AllocateNamedDataSlot("Broker");

			Thread.SetData(brokerSlot, broker);
		}

		/// <summary>
		/// InstallBroker constructor.
		/// </summary>
		/// <param name="brokerAssemblyName"></param>   // name of assembly
		/// <param name="db"></param>                   // db connection string
		/// <param name="assemblyPath"></param>         // path where the app's assemblies are being installed to
		public InstallBroker(string brokerAssemblyName, string db, string assemblyPath)
		{
			//Dynamically load the broker and create an instance
			appDomain=AppDomain.CurrentDomain;

			// might need this guy later in the assembly resolver
			basePath = assemblyPath;
			// new handler for unresolved assemblies. As the default app domain is where setup is run from
			// .NET will attempt to load assembies from there. We need to provide a handler here 
			// so the assemblies can be loaded from the specified install path. 
			// (i.e. the new assemblies used to restore data)
			appDomain.AssemblyResolve += new ResolveEventHandler(AssemblyResolveEventHandler);

			// now load the broker
			Assembly ass = null;
			ass = Assembly.LoadFrom(Path.ChangeExtension(Path.Combine(assemblyPath, brokerAssemblyName), ".dll"));
			Type tType=ass.GetType("Oritax.TaxSimp.Services.CMBroker.CMBroker");
			object [] args = {db};
			broker = Activator.CreateInstance(tType, args);
			LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");
				
			if(brokerSlot == null)
				brokerSlot = Thread.AllocateNamedDataSlot("Broker");

			Thread.SetData(brokerSlot, broker);
		}

		public void Dispose()
		{
			Thread.FreeNamedDataSlot("Broker");
		}
		
		/// <summary>
		/// Event handler for resolving assemblies.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		private Assembly AssemblyResolveEventHandler(object sender, ResolveEventArgs args) 
		{
			Assembly ass = null;
			// parse the fully qualified assembly name argument
			string fullName = args.Name;
			string [] nameParts = fullName.Split( ',' );
			if ( nameParts.Length > 0 )
			{
				// now load the dll from the install directory
				string assName = Path.GetFileName( nameParts[0] ) + ".dll";
				ass = Assembly.LoadFrom( Path.Combine( basePath, assName ));
			}
			return ass;	
		}
		
		public AppDomain AppDomain{get{return this.appDomain;}}

		/// <summary>
		/// Finds the Brokers Assembly and Type by inspecting the CMTypes Table
		/// </summary>
		/// <param name="db">Connection String to the database</param>
		/// <param name="sAssembly">returns the Assembly Name</param>
		/// <param name="sType">returns the Assembly type</param>
		private void GetBrokerAssemblyAndType(string db, out string sAssembly, out string sType)
		{

			SqlConnection conn = new SqlConnection(db);
			conn.Open();
			SqlCommand cmdCMTypes = DBCommandFactory.Instance.NewSqlCommand("SELECT ASSEMBLY, TYPE from CMTYPES WHERE TYPENAME='CMBroker'", conn);
			SqlDataReader dtrCMTypes = cmdCMTypes.ExecuteReader();
			if(!dtrCMTypes.Read())
			{
				dtrCMTypes.Close();
				conn.Close();
				throw new Exception("Could not find Broker Type in CMTypes Table");
			}
			sAssembly = (string)dtrCMTypes["ASSEMBLY"];
			sType = (string)dtrCMTypes["TYPE"];
			dtrCMTypes.Close();
			conn.Close();
		}

		/// <summary>
		/// Call the broker using reflection to extract an xml string that is the state of a BMC
		/// </summary>
		/// <param name="bmcID">The ID of the Broker Managed Component</param>
		/// <returns>An XML string representation of the data within a Broker Managed Component</returns>
		public string ExtractXML(Guid bmcID)
		{
			string sXML;
			object [] args = {bmcID};

			sXML = (string)broker.GetType().InvokeMember("ExtractXML",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,args);

			return sXML;
		}

		public void ExportXMLToFile(Guid bmcID, string fileName)
		{
			object [] args = {bmcID, fileName};

			broker.GetType().InvokeMember("ExportXMLToFile",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,args);
		}

		public void DeleteCMInstance(Guid bmcID)
		{
			object [] args = {bmcID};

			broker.GetType().InvokeMember("DeleteCMInstance",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,args);
		}

		public string GetProductVersion()
		{
			PropertyInfo propertyInfo = broker.GetType().GetProperty("ProductVersion", BindingFlags.Public|BindingFlags.Instance);
			return propertyInfo.GetValue(broker, null).ToString();
		}

		/// <summary>
		/// Call the broker using reflection to deliver an xml string that is the state of a BMC
		/// </summary>
		/// <param name="bmcID">The ID of the Broker Managed Component</param>
		/// <param name="sourceXML">An XML string representation of the data to be delivered to the Broker Managed Component</param>
		/// <returns></returns>
		public string DeliverXML(Guid bmcID, string sourceXML)
		{
            System.Diagnostics.Debugger.Break();
			string sXML;
			object [] args = {bmcID,sourceXML,false};
			sXML = (string)broker.GetType().InvokeMember("DeliverXML",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,args);

			return sXML;
		}

		public void DeliverXMLFromFile(Guid bmcID, string fileName)
		{
			object [] args = {bmcID,fileName};
			broker.GetType().InvokeMember("DeliverXMLFromFile",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,args);
		}

		/// <summary>
		/// Call the broker using reflection to check to see that this Broker Managed Component has been custom serialized
		/// </summary>
		/// <param name="bmcID">The ID of the Broker Managed Component</param>
		/// <returns>true if Cuom Serialized</returns>
		public bool IsBMCCustomSerialized(Guid bmcID)
		{
			bool isCustomSerialized=false;
			object [] args = {bmcID};
			try
			{
				//broker.SetStart();
				broker.GetType().InvokeMember("SetStart",BindingFlags.InvokeMethod,null,broker,null);

				//sXML = broker.IsBMCCustomSerialized(Guid cIID)
				isCustomSerialized = (bool)broker.GetType().InvokeMember("IsBMCCustomSerialized",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,args);
			}
			catch(Exception ex)
			{
				Console.WriteLine("IsBMCCustomeSerialized Failed. Reason:{0}",ex.InnerException.Message);
				return false;
			}
			finally
			{
				//Throw away everything so we have no chance of corrupting the source
				//broker.SetAbort();
				broker.GetType().InvokeMember("SetAbort",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,null);
			}
			return isCustomSerialized;
		}

		/// <summary>
		/// Call the Broker Maneaged Component to inform it that all Migration is complete 
		/// and that republishing can commence if needed
		/// </summary>
		/// <param name="bmcID">The ID of the Broker Managed Component</param>
		public void MigrationCompleted(Guid bmcID)
		{
			object [] args = {bmcID};
			broker.GetType().InvokeMember("MigrationCompleted",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,args);
		}

		/// <summary>
		/// Call the Broker Managed Component to inform it resubscribe.
		/// </summary>
		/// <param name="bmcID">The ID of the Broker Managed Component</param>
		public void Resubscribe(Guid bmcID)
		{
			object [] args = {bmcID};
			broker.GetType().InvokeMember("Resubscribe",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,args);
		}
		public void SetStart()
		{
			this.SetStart(false);
		}
		public void SetStart(bool setMigrationInProgress)
		{
			MethodInfo setWrite = broker.GetType().GetMethod("SetWriteStart");
			if (setWrite == null)
			{
				setWrite = broker.GetType().GetMethod("SetWrite");
			}
			setWrite.Invoke(broker, null);
			//broker.GetType().InvokeMember("SetWriteStart",BindingFlags.InvokeMethod,null,broker,null);
			broker.GetType().InvokeMember("MigrationInProgress", BindingFlags.SetProperty, null, broker, new object[]{setMigrationInProgress});
		}
		public void SetComplete()
		{
			this.SetComplete(false);
		}

		//		public void SetComplete(bool setMigrationInProgress)
		//		{
		//			broker.GetType().InvokeMember("SetComplete",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,null);
		//			broker.GetType().InvokeMember("MigrationInProgress", BindingFlags.SetProperty, null, broker, new object[]{setMigrationInProgress});
		//		}

		public void SetComplete(bool installationsChanged)
		{
			object[] args=new object[]{installationsChanged};
			broker.GetType().InvokeMember("SetComplete",BindingFlags.InvokeMethod,null,broker,args);
		}

		public void SetAbort()
		{
			broker.GetType().InvokeMember("SetAbort",BindingFlags.InvokeMethod,null,broker,null);
		}

		public void ClearCache()
		{
			broker.GetType().InvokeMember("ClearCache", BindingFlags.InvokeMethod, null, broker, null);
		}

		public Guid[] GetBMCIDList(Guid componentVersionID)
		{
			object[] args=new object[]{componentVersionID};
			DataRowCollection bMCInstances=(DataRowCollection)broker.GetType().InvokeMember("GetBMCList",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,args);

			Guid[] bMCIDs=new Guid[bMCInstances.Count];

			int index=0;
			foreach(DataRow bMCRow in bMCInstances)
			{
				bMCIDs[index]=(Guid)bMCRow[DSBMCInfo.CMID_FIELD];
				index++;
			}

			return bMCIDs;
		}

		public void EstablishDatabase(string modelManagerAssemblyPath,string modelManagerClass)
		{
			Assembly modelManagerAssembly=Assembly.LoadFrom(modelManagerAssemblyPath);

			Type modelManagerType=modelManagerAssembly.GetType(modelManagerClass);
			object [] args = {((ICMBroker)broker).Transaction.SQLConnection, ((ICMBroker)broker).Transaction.SQLTransaction};
			IDataModelManagement modelManager=(IDataModelManagement)Activator.CreateInstance(modelManagerType,args);

			modelManager.EstablishDatabase();
		}

		public void DropOldDatabaseTables(string modelManagerAssemblyPath,string modelManagerClass)
		{
			Assembly modelManagerAssembly=Assembly.LoadFrom(modelManagerAssemblyPath);

			Type modelManagerType=modelManagerAssembly.GetType(modelManagerClass);
			object [] args = {((ICMBroker)broker).Transaction.SQLConnection, ((ICMBroker)broker).Transaction.SQLTransaction};
			IDataModelManagement modelManager=(IDataModelManagement)Activator.CreateInstance(modelManagerType,args);

			modelManager.RemoveOldDatabase();
		}
		public SqlCommand GetSqlCommand(string cmdText)
		{
			SqlCommand command = DBCommandFactory.Instance.NewSqlCommand();
			command.Connection = ((ICMBroker)broker).Transaction.SQLConnection;
			command.Transaction = ((ICMBroker)broker).Transaction.SQLTransaction;
			command.CommandText = cmdText;
			return command;
		}

		public void InstallComponentVersion(string componentName,IComponentVersion componentVersion)
		{
			IComponentManagement componentDictionary=(IComponentManagement)broker.GetType().InvokeMember("GetComponentDictionary",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,null);

			componentDictionary.Add(componentName,componentVersion);
		}

		public void ReplaceComponentVersion(string componentName,IComponentVersion componentVersion)
		{
			IComponentManagement componentDictionary=(IComponentManagement)broker.GetType().InvokeMember("GetComponentDictionary",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,null);

			componentDictionary.Replace(componentName,componentVersion);
		}

		public bool AssemblyExists(Guid assemblyID)
		{
			IAssemblyManagement assemblyDictionary=(IAssemblyManagement)broker.GetType().InvokeMember("GetAssemblyDictionary",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,null);

			return assemblyDictionary.Contains(assemblyID);
		}

		public void InstallAssembly(IAssembly nAbleAssembly)
		{
			IAssemblyManagement assemblyDictionary=(IAssemblyManagement)broker.GetType().InvokeMember("GetAssemblyDictionary",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,null);

			assemblyDictionary.Add(nAbleAssembly);
		}

		public void ReplaceAssembly(IAssembly nAbleAssembly)
		{
			IAssemblyManagement assemblyDictionary=(IAssemblyManagement)broker.GetType().InvokeMember("GetAssemblyDictionary",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,null);

			assemblyDictionary.Replace(nAbleAssembly);
		}

		public bool ComponentExists(Guid componentID)
		{
			IComponentManagement componentDictionary=(IComponentManagement)broker.GetType().InvokeMember("GetComponentDictionary",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,null);

			return componentDictionary.ContainsComponent(componentID);
		}

		public bool ComponentVersionExists(Guid componentVersionID)
		{
			IComponentManagement componentDictionary=(IComponentManagement)broker.GetType().InvokeMember("GetComponentDictionary",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,null);

			return componentDictionary.ContainsComponentVersion(componentVersionID);
		}

		public IBrokerManagedComponent CreateComponentInstance(Guid assemblyID, string name)
		{
			object[] args=new object[]{assemblyID,String.Empty};
			return (IBrokerManagedComponent) broker.GetType().InvokeMember("CreateComponentInstance",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,args);
		}

        
		public void ReleaseBrokerManagedComponent(IBrokerManagedComponent iBMC)
		{
			object[] args=new object[]{iBMC};
			broker.GetType().InvokeMember("ReleaseBrokerManagedComponent",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,args);
		}

		public void InstallComponent(IComponent component)
		{
			IComponentManagement componentDictionary=(IComponentManagement)broker.GetType().InvokeMember("GetComponentDictionary",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,null);

			componentDictionary.Add(component);
		}

		public void ReplaceComponent(IComponent component)
		{
			IComponentManagement componentDictionary=(IComponentManagement)broker.GetType().InvokeMember("GetComponentDictionary",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,null);

			componentDictionary.Replace(component);
		}

		public static InstallBroker CreateBrokerProxy(string brokerAssemblyName,string installFolderPath,string connectionString)
		{
			InstallBroker broker=null;

            if (brokerAssemblyName == null || brokerAssemblyName == string.Empty)
                brokerAssemblyName = "CMBroker_1_1";

			AppDomainSetup appDomainSetupInfo = new AppDomainSetup();
			appDomainSetupInfo.ApplicationBase = installFolderPath;
			appDomainSetupInfo.PrivateBinPath = "bin";
			appDomainSetupInfo.ShadowCopyFiles = "false";
			appDomainSetupInfo.ConfigurationFile = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
			AppDomain appDomain = AppDomain.CreateDomain("InstallDomain",null,appDomainSetupInfo);
			appDomain.InitializeLifetimeService();

            //broker = (InstallBroker)appDomain.CreateInstanceAndUnwrap(
            //    "InstallUtilities_1_1",
            //    "Oritax.TaxSimp.Administration.SystemUpdate.InstallBroker",
            //    false,
            //    System.Reflection.BindingFlags.CreateInstance,
            //    null,
            //    new object [] {brokerAssemblyName,connectionString},
            //    null,
            //    null,
            //    null);

            broker = (InstallBroker)appDomain.CreateInstanceAndUnwrap(
                "InstallUtilities_1_1",
                "Oritax.TaxSimp.Administration.SystemUpdate.InstallBroker",
                false,
                System.Reflection.BindingFlags.CreateInstance,
                null,
                new object[] { brokerAssemblyName, connectionString },
                null,
                null);

			return broker;
		}

		public static void DestroyBrokerProxy(InstallBroker installBroker)
		{
			AppDomain.Unload(installBroker.AppDomain);
		}

		public override object InitializeLifetimeService( )
		{
			// note: do not remove this method as it will drop the connection after a period of time
			// this will keep the object alive until the app domain shuts down
			return null;
		}

		public ICalculationModule GetOrganisation()
		{
			return (ICalculationModule) this.broker.GetType().InvokeMember("GetWellKnownCM",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,new object[]{WellKnownCM.Organization});
		}

		public IEnumerable GetLCMsByParent(Guid parentCLID, Guid parentCSID)
		{
			return (IEnumerable) this.broker.GetType().InvokeMember("GetLCMsByParent",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,new object[]{parentCLID, parentCSID});
		}

        public IEnumerable GetLCMsByParent(Guid parentCLID)
        {
            return (IEnumerable)this.broker.GetType().InvokeMember("GetLCMsByParent", BindingFlags.InvokeMethod | BindingFlags.Default, null, broker, new object[] { parentCLID});
        }

		public ICalculationModule GetCMImplementation(Guid cLID, Guid cSID)
		{
			return (ICalculationModule) this.broker.GetType().InvokeMember("GetCMImplementation",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,new object[]{cLID, cSID});
		}

		public ILogicalModule GetLogicalCM(Guid cLID)
		{
			return (ILogicalModule) this.broker.GetType().InvokeMember("GetLogicalCM",BindingFlags.InvokeMethod|BindingFlags.Default,null,broker,new object[]{cLID});
		}
	}
}
