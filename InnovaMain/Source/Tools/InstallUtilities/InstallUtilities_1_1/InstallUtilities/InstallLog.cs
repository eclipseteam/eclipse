using System;
using System.IO;

namespace Oritax.TaxSimp.Administration.SystemUpdate
{
	/// <summary>
	///		Provides functionality to write to a install log file.
	/// </summary>
	public class InstallLog
	{
		StreamWriter installLog;
		bool isDisposed = false;

		public InstallLog(string fileName)
		{
			if (File.Exists(fileName))
			{
				File.Delete(fileName);
			}
			
			if (!Directory.Exists(Path.GetDirectoryName(fileName)))
			{
				Directory.CreateDirectory(Path.GetDirectoryName(fileName));
			}

			installLog = new StreamWriter(fileName);
		}

		public void Dispose()
		{
			if (!isDisposed)
			{
				isDisposed = true;
				installLog.Flush();
				installLog.Close();
			}
		}

		public void AddtoLog(string logentry)
		{
			if (!isDisposed)
			{
				installLog.WriteLine(logentry);
				installLog.Flush();
			}
		}
	}
}
