using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Oritax.TaxSimp.Administration.SystemUpdate
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public struct MEMORYSTATUSEX
    {
        private uint Length;
        public uint MemoryLoad;
        public ulong TotalPhysical;
        public ulong AvailablePhysical;
        public ulong TotalPageFile;
        public ulong AvailablePageFile;
        public ulong TotalVirtual;
        public ulong AvailableVirtual;
        public ulong AvailableExtendedVirtual;

        public MEMORYSTATUSEX(bool dummy)
        {
            this.Length = 0;
            this.MemoryLoad = 0;
            this.TotalPhysical = 0;
            this.AvailablePhysical = 0;
            this.TotalPageFile = 0;
            this.AvailablePageFile = 0;
            this.TotalVirtual = 0;
            this.AvailableVirtual = 0;
            this.AvailableExtendedVirtual = 0;
            this.Length = (uint)Marshal.SizeOf(this);
        }
    }
}
