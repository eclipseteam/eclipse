using System;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Security.Principal;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Administration.SystemUpdate;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.Installation.Security;
using Oritax.TaxSimp.UpdateUtilities;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;
using Oritax.TaxSimp.CM.Organization;

namespace Oritax.TaxSimp.Administration.SystemUpdate
{
	public struct InstallParameters
	{
		public string installBase;
		public string installPackageLocation;
		public string workingLocation;
		public string websiteLocation;
		public string installCode;
		public string installLocation;

		public string databaseName;
		public string databaseServer;
		public string dbSysUserName;
		public string dbSysPassword;
		public string dbAdminUserName;
		public string dbAdminPassword;
		public string nableAdminUserName;
		public string nableAdminPassword;

		public bool changedbSysUser;
		public bool changeNableAdminUser;

		public string connectionString;
		public string adminConnectionString;
		public string encryptedConnectionString;

		public string sourceLocation;
		public string targetLocation;
		public string sourceDataBaseName;
		public string targetDataBaseName;
        public bool commandLineInstall;

	}

	/// <summary>
	///		Summary description for Install.
	/// </summary>
	public class Install
	{
        #region Fields ---------------------------------------------------------------

        private const string currentFullCL = "N-ABLE V2.0 Full CL";
        private const string currentShortCL = "N-ABLE V2.0 Short CL";

		private string importLedgerID = String.Empty; 
		private string importCOAID = String.Empty; 
		private string importShortCOAID = String.Empty;
        private string importCOAID30 = String.Empty;
        private string importShortCOAID30 = String.Empty; 
        private string importTaxTargetID = String.Empty; 
		 
        private const string incorrectSqlServerInstalledMessage = 
            "N-ABLE requires SQL Server 2000 Standard Edition SP3 or later. " + 
            "Please install to a server with the correct version of SQL Server installed.";
        private const string bypassSystemRequirementsCheckKey = "BypassSystemRequirementsCheck";
        private const int earliestSupportedDbVersion = 8;
        private const int earliestSupportedSqlServer8Release = 760;
        private static readonly string[] supportedDatabaseEditions = {
            "Enterprise Edition",
            "64-bit Edition",
            "Standard Edition",
            "Workgroup Edition",
            "Developer Edition",
            "Enterprise Edition (64-bit)",
            "Standard Edition (64-bit)",
            "Workgroup Edition (64-bit)",
            "Developer Edition (64-bit)",
       };

        private const string masterDatabaseString = "master";

        private const string JQueryLibraryFileName = "jquery-1.3.2.min.js";

        #endregion // Fields

        #region Private Members ------------------------------------------------------

        private bool abort = false;
		private string backupLocation = "";

		#endregion //Private Members

		#region Events ---------------------------------------------------------------

		public event InstallProgressUpdateHandler InstallStepProgressUpdate;
		public event InstallProgressUpdateHandler InstallActionProgressUpdate;
		public event InstallErrorHandler InstallError;
		public event MigrationInformationHandler MigrationInformation;
		public event UpdateProgressStepUpdateHandler UpdateStepProgressUpdate;
		public event UpdateProgressComponentUpdateHandler UpdateComponentProgressUpdate;
		public event UpdateMessageHandler LogMigrationInformation;

		#endregion //Events

		#region Public Methods -------------------------------------------------------
        public string GetCompleteExceptionDetails(Exception ex)
        {
            string message = ex.Message;
            message += ex.StackTrace;

            if (ex.InnerException != null)
            {
                Exception tempEx = ex.InnerException;

                while (tempEx.InnerException != null)
                {
                    message += "\n" + tempEx.InnerException.Message;
                    message += "\n" + tempEx.InnerException.StackTrace;
                    tempEx = tempEx.InnerException;
                }
            }
            return message;
        }
	

        public bool InstallFullProduct(InstallParameters parameters, out string errorMessage)
		{
			abort = false;

            if (!IsCorrectDatabaseReleaseInstalled(parameters.databaseServer,
                    parameters.dbAdminUserName, parameters.dbAdminPassword,
                    out errorMessage))
            {
                return false;
            }


            if (!abort)
            {
                OnInstallStepProgressUpdate(new InstallProgressEventArgs("Installing Database"));
                installDatabase(parameters);
            }

			if (!abort)
			{
				this.CreateTargetDirectory(parameters, DeploymentPackageInstallJob.InstallJobType.NEW_INSTALLATION);
			}

			DatabaseManager databaseManager = new DatabaseManager(parameters.databaseName, parameters.databaseServer,parameters.dbAdminUserName, parameters.dbAdminPassword);

            if (!abort)
            {
                databaseManager.CreateProductVersionTable();
            }

			if (!abort)
			{
				OnInstallStepProgressUpdate(new InstallProgressEventArgs("Installing Microsoft Report Viewer"));
                installSSRS(parameters);
			}

			if (!abort)
			{
				OnInstallStepProgressUpdate(new InstallProgressEventArgs("Deploying Cabinet files"));				
				installCabs(parameters, DeploymentPackageInstallJob.InstallJobType.NEW_INSTALLATION);
				databaseManager.ApplyPostInstallScriptsToDatabase(parameters.dbSysUserName, parameters.dbSysPassword);
			}

            if (!abort)
            {
                this.UpdateZippedJavascriptFiles(parameters);
            }

            if (!abort)
            {
                this.UnZip_SkinFile(parameters);
            }

            if (!abort)
            {
                string buildNumber = GetBuildNumber(parameters, parameters.websiteLocation);
                databaseManager.UpdateBuildVersionInTheDatabase(buildNumber);
            }
            
			if (!abort)
			{
				OnInstallStepProgressUpdate(new InstallProgressEventArgs("Updating Web config file"));
				updateWebConfigFile(parameters);
			}
		
			if (!abort)
			{
				OnInstallStepProgressUpdate(new InstallProgressEventArgs("Adding NAble Admin User Account"));
				createNableUserAccount(parameters);
			}

			if (!abort)
			{
				OnInstallStepProgressUpdate(new InstallProgressEventArgs("Creating Organisation"));
				createOrganisation(parameters);
			}

            if (!abort)
            {
                OnInstallStepProgressUpdate(new InstallProgressEventArgs("Assigning Admin User to organisation"));
                UpdateNableUserAccounts(parameters);
            }

			if (!abort)
			{
				OnInstallStepProgressUpdate(new InstallProgressEventArgs("Creating Licence"));
				createLicence(parameters);
			}
		
			if (!abort)
			{
				OnInstallStepProgressUpdate(new InstallProgressEventArgs("Creating Security provider"));
				this.createSecurityProvider(parameters);
			}
				
			if(!abort)
			{
				OnInstallStepProgressUpdate(new InstallProgressEventArgs("Creating standard files for General Ledger"));
                //InstallStandardFiles(parameters);
			}

			if (!abort)
			{
				OnInstallStepProgressUpdate(new InstallProgressEventArgs("Creating Virtual Directory"));
				createVirtualDirectory(parameters);
			}
			
			try
			{
				FileManager.SetDirectoryFilesToNormal(new DirectoryInfo(parameters.workingLocation));
				Directory.Delete(parameters.workingLocation, true);
			}
			catch
			{
                // intentionally left blank - if we couldn't delete everything
                // don't care and carry on.
    		}


			if (!abort)
			{
				OnInstallStepProgressUpdate(new InstallProgressEventArgs("Installation Complete. Click Finished to exit."));
			}

           
			return !abort;
		}

        private void UpdateZippedJavascriptFiles(InstallParameters parameters)
        {
            OnInstallStepProgressUpdate(new InstallProgressEventArgs("Updating zipped Javascript files"));
            string scriptsDirectory = string.Format(@"{0}\Scripts", parameters.websiteLocation);
            string compressedFilename = string.Format(@"{0}\{1}.zip", scriptsDirectory, JQueryLibraryFileName);
            string unCompressedFilename = string.Format(@"{0}\{1}", scriptsDirectory, JQueryLibraryFileName);
            if (!File.Exists(compressedFilename))
                return;
            try
            {
                if (File.Exists(unCompressedFilename))
                    File.Delete(unCompressedFilename);
                File.Copy(compressedFilename, unCompressedFilename);
            }
            catch (Exception exception)
            {
                OnInstallStepProgressUpdate(new InstallProgressEventArgs("Updating zipped Javascript files - Exception raised"));
                OnInstallStepProgressUpdate(new InstallProgressEventArgs(exception.ToString()));
            }
        }

        private void UnZip_SkinFile(InstallParameters parameters)
        {
            String zipPathAndFile = string.Format(@"{0}\Scripts\grid\skin.zip", parameters.websiteLocation);
            String outputFolder = string.Format(@"{0}\Scripts\grid", parameters.websiteLocation);

            FastZip fastZip = new FastZip();
            fastZip.ExtractZip(zipPathAndFile, outputFolder, "");
        }
			
		/// <summary>
		/// Return true is check is successful and there is no issues else false is for failure
		/// </summary>
		/// <param name="c"></param>
		/// <returns></returns>
		private bool PreUpdateChecks(InstallParameters parameters)
		{
            return true;
		}

        public bool UpdateFullProduct(InstallParameters parameters)
        {
            abort = false;
            MigrationIssueXMLTraceListener migrationXMLIssues = new MigrationIssueXMLTraceListener();
            Trace.Listeners.Add(migrationXMLIssues);

            OnUpdateStepProgress(new UpdateProgressStepEventArgs(StepProgress.Cloning));
            cloneSystem(parameters);

            DatabaseManager newDatabaseManager = new DatabaseManager(parameters.targetDataBaseName, parameters.databaseServer, parameters.dbSysUserName, parameters.dbSysPassword);

            ArrayList ids = newDatabaseManager.GetLockedScenearioIDs(parameters.dbSysUserName, parameters.dbSysPassword);
            if (ids.Count > 0)
            {
                newDatabaseManager.ChangeScenarioLockedStatus(ids, "0", parameters.dbSysUserName, parameters.dbSysPassword);
            }

            parameters = updateParameters(parameters);

            newDatabaseManager.ApplyPreInstallScriptsToDatabase(parameters.dbAdminUserName, parameters.dbAdminPassword);
            OnUpdateStepProgress(new UpdateProgressStepEventArgs(StepProgress.Updating));
            installCabs(parameters, DeploymentPackageInstallJob.InstallJobType.UPDATE);


            if (ids.Count > 0)
            {
                newDatabaseManager.ChangeScenarioLockedStatus(ids, "1", parameters.dbSysUserName, parameters.dbSysPassword);
            }

            updateWebConfigFile(parameters);

            string buildNumber = string.Empty;// GetBuildNumber(parameters);

            newDatabaseManager.UpdateBuildVersionInTheDatabaseForUpgrade(buildNumber);

            OnUpdateStepProgress(new UpdateProgressStepEventArgs(StepProgress.InstallingSystemComponents));
            createLicence(parameters);
            createSecurityProvider(parameters);

            newDatabaseManager.ApplyPostInstallScriptsToDatabase(parameters.dbSysUserName, parameters.dbSysPassword);

            // write out any migration issues
            Trace.Listeners.Remove(migrationXMLIssues);

            OnUpdateStepProgress(new UpdateProgressStepEventArgs(StepProgress.Finished));

            return !abort;
        }

		public bool ChangePermissions(InstallParameters parameters)
		{
			abort = false;

			if (parameters.changedbSysUser)
			{
				OnInstallStepProgressUpdate(new InstallProgressEventArgs("Changing Database System User Account"));
				try
				{
					DatabaseManager databaseManager = new DatabaseManager(parameters.databaseName,
						parameters.databaseServer,
						parameters.dbAdminUserName,
						parameters.dbAdminPassword);
			
					databaseManager.RemoveDBOwnerUserFromDatabase();
					if (!databaseManager.CheckSystemLogin(parameters.dbSysUserName))
					{
						databaseManager.CreateSystemLogin(parameters.dbSysUserName, parameters.dbSysPassword);
					}
					else
					{
						databaseManager.ChangeDatabaseUserPassword(parameters.dbSysUserName, parameters.dbSysPassword);
						databaseManager.AddUserToDatabase(parameters.dbSysUserName, parameters.dbSysPassword);
					}

					updateWebConfigFile(parameters);
				}
				catch (Exception ex)
				{
					HandleException(ex);
				}
			}

			if (parameters.changeNableAdminUser)
			{
				OnInstallStepProgressUpdate(new InstallProgressEventArgs("Changing NAble Admin User Account"));
				createNableUserAccount(parameters);
			}

			if (!abort)
			{
				OnInstallStepProgressUpdate(new InstallProgressEventArgs("Update Complete. Click Finished to exit."));
			}
			return !abort;
		}

		#endregion Public Methods

		#region Protected Methods ----------------------------------------------------
		
		protected void OnInstallStepProgressUpdate(InstallProgressEventArgs e)
		{
			if (InstallStepProgressUpdate != null)
			{
				InstallStepProgressUpdate(this, e);
			}
		}

		protected void OnInstallActionProgressUpdate(InstallProgressEventArgs e)
		{
			if (InstallActionProgressUpdate != null)
			{
				InstallActionProgressUpdate(this, e);
			}
		}

		protected void OnInstallError(InstallErrorEventArgs e)
		{
			if (InstallError != null)
			{
				InstallError(this, e);
			}
		}

		protected void OnUpdateStepProgress(UpdateProgressStepEventArgs e)
		{
			if (UpdateStepProgressUpdate != null)
			{
				UpdateStepProgressUpdate(this, e);
			}
		}

		protected void OnUpdateComponentProgressUpdate(UpdateProgressComponentEventArgs e)
		{
			if (UpdateComponentProgressUpdate != null)
			{
				UpdateComponentProgressUpdate(this, e);
			}
		}
		protected void OnLogMigrationInformation(UpdateMessageEventArgs e)
		{
			if (LogMigrationInformation != null)
			{
				LogMigrationInformation(this, e);
			}
		}
		protected void OnMigrationInformation(MigrationInformationEventArgs e)
		{
			if (MigrationInformation != null)
			{
				MigrationInformation(this, e);
			}
		}


		#endregion //Protected Methods

		#region Private Methods ------------------------------------------------------

		private void cloneSystem(InstallParameters parameters)
		{
			try
			{
				string newInstallLocation = parameters.targetLocation;
			
				int progressAsPercentage = progressAsAPercentage(1, 5, 1);
				OnUpdateComponentProgressUpdate(new UpdateProgressComponentEventArgs("Copying directories", "", progressAsPercentage.ToString() +" %"));

				if (File.Exists(Path.Combine(parameters.websiteLocation, "ErrorLog.txt")))
				{
					File.Delete(Path.Combine(parameters.websiteLocation, "ErrorLog.txt"));
				}

				copyDirectoryContents(parameters.websiteLocation, newInstallLocation);

				string legacyFolder = Path.Combine(newInstallLocation, "legacy");
				if (Directory.Exists(legacyFolder))
				{
					Directory.Delete(legacyFolder, true);
				}
				
				DatabaseManager databaseManager = new DatabaseManager(parameters.databaseName,
					parameters.databaseServer,parameters.dbAdminUserName, parameters.dbAdminPassword);

				progressAsPercentage = progressAsAPercentage(2, 5, 1);
				OnUpdateComponentProgressUpdate(new UpdateProgressComponentEventArgs("Copying database", "", progressAsPercentage.ToString() +" %"));

				databaseManager.CloneDatabase(parameters.targetDataBaseName);

				databaseManager = new DatabaseManager(parameters.targetDataBaseName,
					parameters.databaseServer,parameters.dbAdminUserName, parameters.dbAdminPassword);

				if (databaseManager.DatabaseExists(parameters.dbSysUserName, parameters.dbSysPassword))
				{
					databaseManager.CleanDatabaseTable("VersionHistory", parameters.dbSysUserName, parameters.dbSysPassword);
				}

				progressAsPercentage = progressAsAPercentage(4, 5, 1);
				OnUpdateComponentProgressUpdate(new UpdateProgressComponentEventArgs("Creating website", "", progressAsPercentage.ToString() +" %"));

				InstallParameters paramss = new InstallParameters();
				paramss.websiteLocation = parameters.targetLocation;
                paramss.databaseName = parameters.targetDataBaseName;
				createVirtualDirectory(paramss);

				progressAsPercentage = progressAsAPercentage(5, 5, 1);
				OnUpdateComponentProgressUpdate(new UpdateProgressComponentEventArgs("Cloning complete", "", progressAsPercentage.ToString() +" %"));

			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		private void copyDirectoryContents(string directorySourcePath, string directoryTargetPath)
		{
			if (!Directory.Exists(directoryTargetPath))
			{
				Directory.CreateDirectory(directoryTargetPath);
				setFolderPermissions(directoryTargetPath);
			}

			foreach (string directory in Directory.GetDirectories(directorySourcePath))
			{
				copyDirectoryContents(directory, Path.Combine(directoryTargetPath, Path.GetFileName(directory)));
			}

			string[] files = Directory.GetFiles(directorySourcePath);
			foreach (string file in files)
			{
				string targetFile = Path.Combine(directoryTargetPath, Path.GetFileName(file));
				File.Copy(file, targetFile, true);
			}
		}

		private void installDatabase(InstallParameters parameters)
		{
			try
			{
				DatabaseManager databaseManager = new DatabaseManager(parameters.databaseName,
					parameters.databaseServer,
					parameters.dbAdminUserName,
					parameters.dbAdminPassword);
			
				databaseManager.CreateDB();
				if (!databaseManager.CheckSystemLogin(parameters.dbSysUserName))
				{
					databaseManager.CreateSystemLogin(parameters.dbSysUserName, parameters.dbSysPassword);
				}
				else
				{
					databaseManager.AddUserToDatabase(parameters.dbSysUserName, parameters.dbSysPassword);
				}
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

	    private void installSSRS(InstallParameters parameters)
        {
            OnInstallActionProgressUpdate(new InstallProgressEventArgs("Installing ReportViewer.exe"));
            string filePath = Path.Combine(parameters.installBase, "MicrosoftReportViewerInstall\\ReportViewer.exe");
            InstallMSI.InstallExe(filePath, "/q:a /c:\"install.exe /q\"", new ProgressUpdate(progressUpdateHandler));
        }

		private void installCabs(InstallParameters parameters, DeploymentPackageInstallJob.InstallJobType installType)
		{
			try
			{
				if (Directory.Exists(parameters.workingLocation))
				{
					FileManager.SetDirectoryFilesToNormal(new DirectoryInfo(parameters.workingLocation));
					Directory.Delete(parameters.workingLocation, true);
				}
				Directory.CreateDirectory(parameters.workingLocation);
				File.Copy(Path.Combine(parameters.installCode, "cabarc.exe"), 
					Path.Combine(parameters.workingLocation, "cabarc.exe"));
			}
			catch (Exception ex)
			{
				HandleException(ex);
				return;
			}
		
			setFolderPermissions(parameters.websiteLocation);
			
			DeploymentPackageInstallJob deploymentPackageInstallJob = new DeploymentPackageInstallJob(
				parameters.installPackageLocation,
				parameters.workingLocation,
				parameters.websiteLocation,
				parameters.installCode,
				parameters.connectionString,
				null
				);
			deploymentPackageInstallJob.ComponentUpdated += new UpdateProgressHandler(deploymentPackageInstallJob_ComponentUpdated);
			deploymentPackageInstallJob.LogMigrationInformation += new UpdateMessageHandler(deploymentPackageInstallJob_LogMigrationInformation);

			try
			{
				deploymentPackageInstallJob.Install(installType);
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		private void CreateTargetDirectory(InstallParameters parameters, DeploymentPackageInstallJob.InstallJobType installType)
		{
			if (!Directory.Exists(parameters.websiteLocation))
			{
				try
				{
					Directory.CreateDirectory(parameters.websiteLocation);

					if(!Directory.Exists(parameters.websiteLocation + "\\bin"))
						Directory.CreateDirectory(parameters.websiteLocation + "\\bin");

                    if (!Directory.Exists(parameters.websiteLocation + "\\ClientBin"))
                        Directory.CreateDirectory(parameters.websiteLocation + "\\ClientBin");

				}
				catch (Exception ex)
				{
					HandleException(ex);
					return;
				}
			}
		}

		private void updateWebConfigFile(InstallParameters parameters)
		{
			try
			{
				WebDeployment.UpdateWebConfigFile(Path.Combine(parameters.websiteLocation, "web.config"), parameters.encryptedConnectionString, new ProgressUpdate(progressUpdateHandler));
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		private void createVirtualDirectory(InstallParameters parameters)
		{
            try
			{
				WebDeployment webDeploy = new WebDeployment();
				WebDeployment.CreateVirtualDirectory(parameters.websiteLocation, parameters.databaseName, "login_1_1.aspx", new ProgressUpdate(progressUpdateHandler));
				webDeploy.RegisterWebsites(parameters.databaseName, new ProgressUpdate(progressUpdateHandler));
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		private void createOrganisation(InstallParameters parameters)
		{
			ICMBroker broker = null;
			LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

			try
			{
				broker = new CMBroker(parameters.adminConnectionString);
				Thread.SetData(brokerSlot, broker);

				broker.SetWriteStart();
				ICalculationModule objOrganisation = broker.GetWellKnownCM(WellKnownCM.Organization);

				if (objOrganisation == null)
					broker.CreateLogicalCM("Organization_1_1", "Organization", Guid.NewGuid(), null);
				else
					OnInstallActionProgressUpdate(new InstallProgressEventArgs("Located pre-existing Organisation, aborting creation of new Organisation"));
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
			finally
			{
				if(broker != null)
					broker.SetComplete();

				Thread.SetData(brokerSlot, null);
			}
		}

        private void UpdateNableUserAccounts(InstallParameters parameters)
        {
            try
            {
                DatabaseManager databaseManager = new DatabaseManager(parameters.databaseName,
                    parameters.databaseServer,
                    parameters.dbAdminUserName,
                    parameters.dbAdminPassword);

                ICMBroker broker = new CMBroker(parameters.adminConnectionString);

                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (brokerSlot == null)
                    brokerSlot = Thread.AllocateNamedDataSlot("Broker");

                Thread.SetData(brokerSlot, broker);

                broker.SetWriteStart();
                ICalculationModule objOrganisation = broker.GetWellKnownCM(WellKnownCM.Organization);
                IOrganization organizationCM = (IOrganization)objOrganisation;

                databaseManager.UpdateNableUserAccounts(objOrganisation.CID, organizationCM.OrganisationName, parameters.dbAdminUserName, parameters.dbAdminPassword);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

		private void createLicence(InstallParameters parameters)
		{
			ICMBroker broker = null;
			LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

			try
			{
				broker = new CMBroker(parameters.connectionString);
				Thread.SetData(brokerSlot, broker);

				broker.SetWriteStart();
				IBrokerManagedComponent objLicence = broker.GetWellKnownBMC(WellKnownCM.Licencing);

				if (objLicence == null)
					broker.CreateComponentInstance(new Guid("26437F7E-10D8-4887-A462-E92A7EF3F05C"),"Licence");
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
			finally
			{
				if(broker != null)
					broker.SetComplete();

				Thread.SetData(brokerSlot, null);
			}
		}
	
		private void createSecurityProvider(InstallParameters parameters)
		{
			ICMBroker broker = null;
			LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

			try
			{
				broker = new CMBroker(parameters.connectionString);
				Thread.SetData(brokerSlot, broker);

				broker.SetWriteStart();
				IBrokerManagedComponent objSecurityProvider = broker.GetWellKnownBMC(WellKnownCM.SecurityProvider);

				if (objSecurityProvider == null)
					broker.CreateComponentInstance(new Guid("6D0500F5-B1F9-4053-88BC-FF4004D9E9D0"),"SAS70Password");
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
			finally
			{
				if(broker != null)
					broker.SetComplete();

				Thread.SetData(brokerSlot, null);
			}
		}
		

		private void createNableUserAccount(InstallParameters parameters)
		{
			try
			{
				DatabaseManager databaseManager = new DatabaseManager(parameters.databaseName,
					parameters.databaseServer,
					parameters.dbAdminUserName,
					parameters.dbAdminPassword);
			
				if (databaseManager.CheckNAbleAccount(parameters.nableAdminUserName))
				{
					databaseManager.ChangeNableAccountPassword(parameters.nableAdminUserName, parameters.nableAdminPassword);
				}
				else
				{
					databaseManager.AddNableUserAccount(parameters.nableAdminUserName, parameters.nableAdminPassword,
						parameters.dbAdminUserName, parameters.dbAdminPassword);
					databaseManager.UpdateCINSTANCERecords(parameters.dbAdminUserName, parameters.dbAdminPassword);
				}
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		private void HandleException(Exception ex)
		{
			string message = GetCompleteExceptionDetails(ex);

			OnInstallError(new InstallErrorEventArgs(message));
			abort = true;
		}

		private void progressUpdateHandler(string message)
		{
			OnInstallActionProgressUpdate(new InstallProgressEventArgs(message));
		}

		private void setFolderPermissions(string folder)
		{
			try 
			{
				SecurityDescriptor secDesc = SecurityDescriptor.GetFileSecurity(folder, SECURITY_INFORMATION.DACL_SECURITY_INFORMATION);
				Dacl dacl = secDesc.Dacl;
				Sid sidUser = new Sid ("ASPNET");

				// allow: folder, subfolder and files modify
				dacl.AddAce (new AceAccessAllowed (sidUser, AccessType.GENERIC_WRITE | AccessType.GENERIC_READ | AccessType.DELETE | AccessType.GENERIC_EXECUTE , AceFlags.OBJECT_INHERIT_ACE | AceFlags.CONTAINER_INHERIT_ACE));

				if (WebDeployment.IsWin2K3Server())
				{
					Sid ntAuthority = new Sid(@"NT AUTHORITY\NETWORK SERVICE");
					dacl.AddAce (new AceAccessAllowed (ntAuthority, AccessType.GENERIC_WRITE | AccessType.GENERIC_READ | AccessType.DELETE | AccessType.GENERIC_EXECUTE , AceFlags.OBJECT_INHERIT_ACE | AceFlags.CONTAINER_INHERIT_ACE));

				}
				secDesc.SetDacl(dacl);
				secDesc.SetFileSecurity(folder, SECURITY_INFORMATION.DACL_SECURITY_INFORMATION);

			} 
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		private void markArchiveAsSuccessful()
		{
			string successfulArchive = backupLocation + "-SuccessfulUpdate";
			Directory.Move(backupLocation, successfulArchive);
		}

		private bool checkForFailedUpdates(string installName)
		{
            string backupPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), @"DBTempBackup\" + installName);

			foreach(string directory in Directory.GetDirectories(backupPath))
			{
				if (!directory.EndsWith("SuccessfulUpdate"))
				{
					return true;
				}
			}

			return false;
		}

		private ArrayList checkForOldUpdates(string installName)
		{
			ArrayList oldUpdates = new ArrayList();
            string backupPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), @"DBTempBackup\" + installName);

			foreach(string directory in Directory.GetDirectories(backupPath))
			{
				if (directory.EndsWith("SuccessfulUpdate"))
				{
					string updateTime = Path.GetFileName(directory);
					string[] dateTimeInfo = updateTime.Split('_');

					if (dateTimeInfo.Length == 5)
					{
						DateTime folderTime = new DateTime(int.Parse(dateTimeInfo[2]), int.Parse(dateTimeInfo[1]), int.Parse(dateTimeInfo[0]), int.Parse(dateTimeInfo[3]), int.Parse(dateTimeInfo[4]), 0);
						TimeSpan age = folderTime - DateTime.Now;

						if (age.Days >= 90)
						{
							oldUpdates.Add(directory);
						}
					}
				}
			}
			return oldUpdates;
		}

		private InstallParameters updateParameters(InstallParameters parameters)
		{
			parameters.websiteLocation = parameters.targetLocation;	
			parameters.connectionString = replaceDatabaseName(parameters.connectionString, parameters.targetDataBaseName);
			parameters.encryptedConnectionString = replaceDatabaseName(parameters.encryptedConnectionString, parameters.targetDataBaseName);
			parameters.databaseName = parameters.targetDataBaseName;

			return parameters;
		}

		private string replaceDatabaseName(string connectionString, string newDatabaseName)
		{
			string[] connectionProperties = connectionString.Split( ';' );

			string databaseName = "";

			foreach(string property in connectionProperties)
			{
				if (property.Trim().StartsWith("database"))
				{
					databaseName = property.Substring(property.IndexOf("=") + 1);
				}
			}
			
			return connectionString.Replace(databaseName, newDatabaseName);
		}

		private int progressAsAPercentage(int count, int totalCount, int updateStep)
		{
			float progress = ((float)count/((float)totalCount));
			
			if (updateStep == 1)
			{
				progress = progress * 10;
			}
			if (updateStep == 2)
			{
				progress = progress * 85;
				progress += 10;
			}
			else if (updateStep == 3)
			{
				progress = progress * 5;
				progress += 95;
			}

			return (int)Math.Floor(progress);
		}

		public string GetBuildNumber(InstallParameters parameters, string location)
		{
			string build = "";

			FileManagerProxy installFileManager=FileManagerProxy.CreateFileManager(parameters.installCode);
			DeploymentUnitInstallJob.NAbleAssemblyName[] cmBrokerAssemblies=installFileManager.FindNAbleAssembly(Path.Combine(parameters.websiteLocation, "bin"),"CMBroker");
	
			if(cmBrokerAssemblies.Length==1)
			{
				string installedBrokerAssemblyName=cmBrokerAssemblies[0].VersionedName;
                InstallBroker broker = new InstallBroker(installedBrokerAssemblyName, parameters.connectionString, Path.Combine(location, "bin"));
				build = broker.GetProductVersion();
				broker.Dispose();
				broker = null;			
			}
			
			return build; 
		}

        public string GetBuildNumber(InstallParameters parameters)
        {
            string build = "";

            FileManagerProxy installFileManager = FileManagerProxy.CreateFileManager(parameters.installCode);
            DeploymentUnitInstallJob.NAbleAssemblyName[] cmBrokerAssemblies = installFileManager.FindNAbleAssembly(Path.Combine(parameters.websiteLocation, "bin"), "CMBroker");

            if (cmBrokerAssemblies.Length == 1)
            {
                string installedBrokerAssemblyName = cmBrokerAssemblies[0].VersionedName;
                InstallBroker broker = new InstallBroker(installedBrokerAssemblyName, parameters.connectionString, Path.Combine(parameters.targetLocation, "bin"));
                build = broker.GetProductVersion();
                broker.Dispose();
                broker = null;
            }

            return build;
        }

        private static void SearchForDatabaseRelease(string connectionString, out string version,
            out string level, out string edition)
        {
            version = string.Empty;
            level = string.Empty;
            edition = string.Empty;

            string commandString = "SELECT  SERVERPROPERTY('productversion'), " +
                "SERVERPROPERTY ('productlevel'), SERVERPROPERTY ('edition')";
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(commandString, connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    version = (string)reader[0];
                    level = (string)reader[1];
                    edition = (string)reader[2];
                }
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                connection.Close();
            }
        }

        private static string ProcessDatabaseVersionString(string databaseVersionString)
        {
            int versionNumberStart = databaseVersionString.IndexOf('-') + 1;
            databaseVersionString = databaseVersionString.Substring(versionNumberStart);
            int versionNumberEnd = databaseVersionString.IndexOf('(') - 1;
            databaseVersionString = databaseVersionString.Remove(versionNumberEnd);
            databaseVersionString = databaseVersionString.Trim();
            return databaseVersionString;
        }

        private static string GetDatabaseVersion(string connectionString)
        {
            string commandString = "SELECT @@VERSION";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(commandString, connection);

                connection.Open();
                string versionString = (string)command.ExecuteScalar();
                return ProcessDatabaseVersionString(versionString);
           }
        }

        private static bool IsCorrectDatabaseReleaseInstalled(string serverString, 
            string dbAdministratorName, string dbAdministratorPassword, out string errorMessage)
        {
            errorMessage = string.Empty;
            string connectionString = "server=" + serverString + ";database=" + 
                masterDatabaseString + ";uid=" + dbAdministratorName + ";pwd=" +
                dbAdministratorPassword;

            string bypassSystemRequirementsCheckString = ConfigurationManager.AppSettings[
                bypassSystemRequirementsCheckKey];
            bool bypassSystemRequirements = bool.Parse(bypassSystemRequirementsCheckString);
            if (bypassSystemRequirements)
            {
                return true;
            }

            // Check current backend database version
            string databaseVersionString = GetDatabaseVersion(connectionString);
            Version version = new Version(databaseVersionString);
            if (version.Major < earliestSupportedDbVersion)
            {
                errorMessage = incorrectSqlServerInstalledMessage;
                return false;
            }

            string versionString = string.Empty;
            string levelString = string.Empty;
            string editionString = string.Empty;
            SearchForDatabaseRelease(connectionString, out versionString, out levelString,
                out editionString);

            // Check release for SQL Server 2000
            if (version.Major == earliestSupportedDbVersion)
            {
                string[] versionParameters = databaseVersionString.Split(new char[] { '.' });
                int release = int.Parse(versionParameters[2]);
                if (release < earliestSupportedSqlServer8Release)
                {
                    errorMessage = incorrectSqlServerInstalledMessage;
                    return false;
                }
            }

            // Check edition
            bool editionSupported = false;
            for (int supportedEditionIndex = 0; supportedEditionIndex <
                supportedDatabaseEditions.Length; supportedEditionIndex++)
            {
                string supportedDatabaseEdition = supportedDatabaseEditions[
                    supportedEditionIndex];
                if (editionString == supportedDatabaseEdition)
                {
                    editionSupported = true;
                }
            }
            if (!editionSupported)
            {
                errorMessage = incorrectSqlServerInstalledMessage;
                return false;
            }

            return true;
        }

		#endregion //Private Methods

		private void deploymentPackageInstallJob_ComponentUpdated(object sender, UpdateProgressEventArgs e)
		{
			int percentage = progressAsAPercentage(e.StepNumber, e.TotalSteps, 2);
			OnUpdateComponentProgressUpdate(new UpdateProgressComponentEventArgs(e.Description, "", percentage.ToString() +" %"));
		}
		private void deploymentPackageInstallJob_LogMigrationInformation(object sender, UpdateMessageEventArgs e)
		{
			OnLogMigrationInformation(e);
		}
	}
}
