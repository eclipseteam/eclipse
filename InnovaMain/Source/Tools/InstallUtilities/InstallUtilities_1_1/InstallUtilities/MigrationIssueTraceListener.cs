using System;
using System.Diagnostics;
using System.Text;
using System.Data;
using System.IO;
using System.Collections;
using System.Xml;
using System.Configuration;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Administration.SystemUpdate
{
	public class MigrationIssueTraceListener : TraceListener
	{
        private const string ISSUEINTRODUCTION = "N-ABLE Tax Management Solution\r\nRelease 2008.2\r\n \r\nUpgrade Correction Report\r\nIntroduction\r\n==========\r\n\r\nThis report details changes made to tax and accounting figures as a result of corrections to certain calculations during the upgrade from N-ABLE 2008.1 to 2008.2.\r\nPlease review this report carefully.\r\n\r\nDetails\r\n======\r\n";
		private const string ISSUEEND = "Summary\r\n=======\r\n\r\nA total of {0} Errors were found and corrected in {1} Calculation Modules";
		private string currentHeader;
		private StringBuilder errors = new StringBuilder();
		private int errorCount = 0;
		private int calculationModuleCount = 0;

		public MigrationIssueTraceListener()
		{

		}

		public override void Write(string message)
		{
			// ignore anything that doesn't have a category as we are looking for specific categories
		}

		public override void WriteLine(string message)
		{
			// ignore anything that doesn't have a category as we are looking for specific categories
		}

		public override void WriteLine(string message, string category)
		{
			if(category == "MigrationIssueHeaderInfo")
				this.currentHeader = message;
			else if(category == "MigrationCorrection")
			{
				if(this.currentHeader != String.Empty)
				{
					errors.Append(this.currentHeader);
					errors.Append(Environment.NewLine);
					this.currentHeader = String.Empty;
					calculationModuleCount++;
				}

				errors.Append(message);
				errors.Append(System.Environment.NewLine);
				errors.Append(System.Environment.NewLine);
				errors.Append("---");
				errors.Append(System.Environment.NewLine);
				errors.Append(System.Environment.NewLine);

				errorCount++;
			}

			// ignore if not one of these categories
		}
		
		public bool ErrorsFound
		{
			get
			{
				return this.errorCount > 0;
			}
		}

		public void WriteLog(string fileName)
		{
			if(this.ErrorsFound)
			{
				if(File.Exists(fileName))
					File.Delete(fileName);

				StreamWriter logFile = File.CreateText(fileName);

				try
				{
					logFile.Write(ISSUEINTRODUCTION);
					logFile.Write(this.errors.ToString());
					logFile.Write(String.Format(ISSUEEND, new object[]{this.errorCount,this.calculationModuleCount}));
					logFile.Flush();
				}
				finally
				{
					if(logFile != null)
						logFile.Close();
				}
			}
		}
	}
	
	public class MigrationIssueXMLTraceListener : TraceListener
	{
		public const string DSNAME = "MigrationIssueDataSet";
		public const string TEATABLENAME = "MigrationIssueTEATable";
		public const string ID_FIELD = "ID";
		public const string BREADCRUMB_FIELD = "BreadCrumb";
		public const string CALCULATIONMODULE_FIELD = "CalculationModule";
		public const string WORKPAPERSECTION_FIELD = "WorkpaperSection";
		public const string WORKPAPERNAME_FIELD = "WorkpaperName";
		public const string CONTRIBUTINGITEMDESCRIPTION_FIELD = "ContributingItemDescription";
		public const string OLDVALUE_FIELD = "OldValue";
		public const string CURRENTVALUE_FIELD = "CurrentValue";

		public const string ATOTABLENAME = "MigrationIssueATOTable";
		public const string ATOLABELDESCRIPTION_FIELD = "ATOFormLabelDesc";

		public const string EXP40TABLENAME = "MigrationIssueEXP40Table";		
		//ATO CONSTANT 
		public const string SCHEDULEHEADING_FIELD = "SCHEDULEHEADING";
		public const string SCHEDULESUBSECTION_FIELD	= "SCHEDULESUBSECTION";
		public const string SCHEDULESUBSECTIONTYPE_FIELD = "SCHEDULESUBSECTIONTYPE";
		public const string SCHEDULESUBSECTIONTYPEDESCRIPTION_FIELD = "SCHEDULESUBSECTIONTYPEDESCRIPTION";
		public const string POSITION_FIELD = "POSITION";
		public const string DIFF_FIELD = "DIFF";
		public const string YEAR_FIELD = "YEAR";

			
		private int errorCount = 0;
		private const string APPLICATIONSETTINGNAME = "MigrationReportName";
		public const string DEFAULTFILENAME = "MigrationReport.xml";
		private string reportPath = String.Empty;
		private DataSet migrationIssueDS = new DataSet(DSNAME);
		public bool ErrorsFound
		{
			get
			{
				return this.errorCount > 0;
			}
		}

		public string ReportPath
		{
			get
			{
				if ( this.reportPath.EndsWith("\\") )
					return this.reportPath + @"\MigrationReport\";
				else
					return this.reportPath + @"\MigrationReport\";
			}
			set
			{
				this.reportPath = value;
			}
		}
       
		public MigrationIssueXMLTraceListener()
		{
			this.CreateDataSet();
		}

		public override void Write(string message)
		{
			//			 ignore anything that doesn't have a category as we are looking for specific categories	
		}

		public override void WriteLine(string message)
		{
			//			 ignore anything that doesn't have a category as we are looking for specific categories
		}

		public override void WriteLine(object o)
		{
			try
			{
				if ( o is MigrationIssueTEAItem )
				{
					this.WriteLine((MigrationIssueTEAItem) o);
				}
				else if ( o is MigrationIssueATOItem )
				{
					this.WriteLine((MigrationIssueATOItem) o);
				}
				else if ( o is MigrationIssueEXP40Item )
				{
					this.WriteLine((MigrationIssueEXP40Item) o);
				}	
				else if ( o is ArrayList )
				{
					ArrayList listParameters = (ArrayList) o;
					if ( MigrationReportEntry.TEAENTRYKEY == listParameters[0].ToString() )
					{
						MigrationIssueTEAItem item = new MigrationIssueTEAItem();
						item.ID = new Guid(listParameters[0].ToString());
						item.BreadCrumb = listParameters[1].ToString();
						item.CalculationModule = listParameters[2].ToString();
						item.WorkpaperSection = listParameters[3].ToString();
						item.WorkpaperName = listParameters[4].ToString();
						item.ContributingItemDescription = listParameters[5].ToString();
						item.OldValue = listParameters[6].ToString();
						item.CurrentValue = listParameters[7].ToString();
						item.difference = listParameters[8].ToString();
						item.ExtractData(this.migrationIssueDS);
					}
					else if ( MigrationReportEntry.ATOENTRYKEY == listParameters[0].ToString() )
					{
						MigrationIssueATOItem item = new MigrationIssueATOItem();
						item.id = new Guid(listParameters[0].ToString());
						item.breadCrumb = listParameters[1].ToString();
						item.scheduleHeading = listParameters[2].ToString();
						item.scheduleSubSection = listParameters[3].ToString();
						item.atoLabelDescription = listParameters[4].ToString();
						item.scheduleSubSectionType = listParameters[5].ToString();
						item.contributingItemDescription = listParameters[6].ToString();
						item.oldValue = listParameters[7].ToString();
						item.currentValue = listParameters[8].ToString();
						item.difference = listParameters[9].ToString();
						item.position = listParameters[10].ToString();
						item.year = listParameters[11].ToString();
						item.ExtractData(this.migrationIssueDS);	
					}
					else if ( MigrationReportEntry.EXP40ENTRYKEY == listParameters[0].ToString())
					{
						MigrationIssueEXP40Item item = new MigrationIssueEXP40Item();
						item.ID = new Guid(listParameters[0].ToString());
						item.BreadCrumb = listParameters[1].ToString();
						item.CalculationModule = listParameters[2].ToString();
						item.WorkpaperName = listParameters[3].ToString();
						item.ContributingItemDescription = listParameters[4].ToString();
						item.OldValue = listParameters[5].ToString();
						item.CurrentValue = listParameters[6].ToString();
						item.difference = listParameters[7].ToString();
						item.ExtractData(this.migrationIssueDS);
					}
				}
				errorCount++;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public void WriteLine(MigrationIssueTEAItem item)
		{
			item.ExtractData(this.migrationIssueDS);
		}
		
		public void WriteLine(MigrationIssueATOItem item)
		{
			item.ExtractData(this.migrationIssueDS);
		} 

		public void WriteLine(MigrationIssueEXP40Item item)
		{
			item.ExtractData(this.migrationIssueDS);
		}       

		public void GetXML(string path, string fileName)
        {
            #pragma warning disable 618
            XmlDataDocument doc = new XmlDataDocument(this.migrationIssueDS);
			doc.CreateXmlDeclaration("1.0", Encoding.Unicode.ToString(), null);
			if ( fileName == null )
				fileName = DEFAULTFILENAME;
			if ( !Directory.Exists(path) )
			{
				Directory.CreateDirectory(path);
			}
			FileStream file = new FileStream(path + fileName, FileMode.Create);
			doc.Save(file);
			file.Flush();
			file.Close();			
		}	
        
		private void CreateDataSet()
		{
			this.AddTEATable(this.migrationIssueDS);
			this.AddATOTable(this.migrationIssueDS);
			this.AddEXP40Table(this.migrationIssueDS);	
		}

		private void AddTEATable(DataSet ds)
		{
			DataTable table = new DataTable(TEATABLENAME);
			table.Columns.Add(new DataColumn(ID_FIELD, typeof(System.Guid)));
			table.Columns.Add(new DataColumn(BREADCRUMB_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(CALCULATIONMODULE_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(WORKPAPERSECTION_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(WORKPAPERNAME_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(CONTRIBUTINGITEMDESCRIPTION_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(OLDVALUE_FIELD,typeof(System.String)));
			table.Columns.Add(new DataColumn(CURRENTVALUE_FIELD,typeof(System.String)));
			table.Columns.Add(new DataColumn(DIFF_FIELD, typeof(System.String)));
			ds.Tables.Add(table);
		}
       	
		private void AddATOTable(DataSet ds)
		{
			DataTable table = new DataTable(ATOTABLENAME);
			table.Columns.Add(new DataColumn(ID_FIELD, typeof(System.Guid)));
			table.Columns.Add(new DataColumn(BREADCRUMB_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(SCHEDULEHEADING_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(SCHEDULESUBSECTION_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(ATOLABELDESCRIPTION_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(SCHEDULESUBSECTIONTYPE_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(CONTRIBUTINGITEMDESCRIPTION_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(OLDVALUE_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(CURRENTVALUE_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(POSITION_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(DIFF_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(YEAR_FIELD, typeof(System.String)));
			ds.Tables.Add(table);
		}

		private void AddEXP40Table(DataSet ds)
		{
			DataTable table = new DataTable(EXP40TABLENAME);
			table.Columns.Add(new DataColumn(ID_FIELD, typeof(System.Guid)));
			table.Columns.Add(new DataColumn(BREADCRUMB_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(CALCULATIONMODULE_FIELD,typeof(System.String)));
			table.Columns.Add(new DataColumn(WORKPAPERNAME_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(CONTRIBUTINGITEMDESCRIPTION_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(OLDVALUE_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(CURRENTVALUE_FIELD, typeof(System.String)));
			table.Columns.Add(new DataColumn(DIFF_FIELD, typeof(System.String)));
			ds.Tables.Add(table);
		}
	}

	public class MigrationIssueTEAItem
	{
		#region Private fields
		private Guid id = Guid.Empty;
		private string breadCrumb = String.Empty;
		private string calculationModule = String.Empty;
		private string workpaperSection = String.Empty;
		private string workpaperName = String.Empty;
		private string contributingItemDescription = String.Empty;
		private string oldValue = String.Empty;
		private string currentValue = String.Empty;
		public string difference = String.Empty;
		#endregion

		#region Public properties
		public Guid ID
		{
			get
			{
				return this.id;
			}
			set
			{
				this.id = value;				
			}
		}

		public string BreadCrumb
		{
			get
			{
				return this.breadCrumb;
			}
			set
			{
				this.breadCrumb = value;				
			}
		}

		public string CalculationModule
		{
			get
			{
				return this.calculationModule;											
			}
			set
			{
				this.calculationModule = value;	
			}
		}	

		public string WorkpaperSection
		{
			get
			{
				return this.workpaperSection;
			}
			set
			{
				this.workpaperSection = value;	
			}
		}

		public string WorkpaperName
		{
			get
			{
				return this.workpaperName;
			}
			set
			{
				this.workpaperName = value;
			}
		}

		public string ContributingItemDescription
		{
			get
			{
				return this.contributingItemDescription;
			}
			set
			{
				this.contributingItemDescription = value;	
			}
		}

		public string OldValue
		{
			get
			{
				return this.oldValue;
			}
			set
			{
				this.oldValue = value;
			}
		}

		public string CurrentValue
		{
			get
			{
				return this.currentValue;
			}
			set
			{
				this.currentValue = value;	
			}
		}
		#endregion

		#region Constructors
		public MigrationIssueTEAItem() {}

		public MigrationIssueTEAItem(Guid id,
			string breadCrumb, 
			string calculationModule, 
			string workpaperSection,
			string workpaperName, 
			string contributingItemDescription,
			string oldValue,
			string currentValue,
			string difference)
		{
			this.id = id;
			this.breadCrumb = breadCrumb;
			this.calculationModule = calculationModule;
			this.workpaperSection = workpaperSection;
			this.workpaperName = workpaperName;
			this.contributingItemDescription = contributingItemDescription;
			this.oldValue = oldValue;
			this.currentValue = currentValue;
			this.difference = difference;

		}

		#endregion

		#region Member methods
		public void ExtractData(DataSet ds)
		{
			DataTable table = ds.Tables[MigrationIssueXMLTraceListener.TEATABLENAME];
			DataRow row = table.NewRow();
			row[MigrationIssueXMLTraceListener.ID_FIELD] = this.ID.ToString();
			row[MigrationIssueXMLTraceListener.BREADCRUMB_FIELD] = this.BreadCrumb;
			row[MigrationIssueXMLTraceListener.CALCULATIONMODULE_FIELD] = this.CalculationModule;
			row[MigrationIssueXMLTraceListener.WORKPAPERSECTION_FIELD] = this.WorkpaperSection;
			row[MigrationIssueXMLTraceListener.WORKPAPERNAME_FIELD] = this.WorkpaperName;
			row[MigrationIssueXMLTraceListener.CONTRIBUTINGITEMDESCRIPTION_FIELD] = this.ContributingItemDescription;
			row[MigrationIssueXMLTraceListener.OLDVALUE_FIELD] = this.OldValue;
			row[MigrationIssueXMLTraceListener.CURRENTVALUE_FIELD] = this.CurrentValue;
			row[MigrationIssueXMLTraceListener.DIFF_FIELD] = this.difference;
			table.Rows.Add(row);
		}
		
		#endregion
	}

	public class MigrationIssueATOItem
	{
		public  Guid id = Guid.Empty; // Unique ID
		public  string breadCrumb = String.Empty; //1 Consol Group >  01/01/2004-31/12/2004 [Initial]
		public	string scheduleHeading = String.Empty; //ATO10 - Company tax return 2005
		public	string scheduleSubSection = String.Empty; //Question 6 
		public  string atoLabelDescription = String.Empty; //Label A - Cost of sales
		public	string scheduleSubSectionType = String.Empty; //Ledger || Adjustment || Revised
		public	string contributingItemDescription = String.Empty; //Legder item 1
		public	string oldValue = String.Empty; //Old value
		public	string currentValue = String.Empty;// new value
		public	string position = String.Empty;	
		public	string difference = String.Empty;	
		public	string year = String.Empty;  

		public MigrationIssueATOItem(){}
		
		public MigrationIssueATOItem(Guid id,
			string breadCrumb, 
			string scheduleHeading, 
			string scheduleSubSection,
			string atoLabelDescription,
			string scheduleSubSectionType, 
			string contributingItemDescription,
			string oldValue,
			string currentValue,
			string position,
			string difference,
			string year)
		{
			this.id = id;
			this.breadCrumb = breadCrumb;
			this.scheduleHeading = scheduleHeading;
			this.scheduleSubSection = scheduleSubSection; 
			this.atoLabelDescription = atoLabelDescription;
			this.scheduleSubSectionType = scheduleSubSectionType;
			this.contributingItemDescription =contributingItemDescription; 
			this.oldValue = oldValue; 
			this.currentValue =currentValue; 
			this.position = position; 
			this.difference = difference;
			this.year = year; 
		}
		
		public void ExtractData(DataSet ds)
		{
			DataTable table = ds.Tables[MigrationIssueXMLTraceListener.ATOTABLENAME];
			DataRow row = table.NewRow();
			row[MigrationIssueXMLTraceListener.ID_FIELD] = this.id.ToString();
			row[MigrationIssueXMLTraceListener.BREADCRUMB_FIELD] = this.breadCrumb;
			row[MigrationIssueXMLTraceListener.SCHEDULEHEADING_FIELD] = this.scheduleHeading;
			row[MigrationIssueXMLTraceListener.SCHEDULESUBSECTION_FIELD] = this.scheduleSubSection;
			row[MigrationIssueXMLTraceListener.ATOLABELDESCRIPTION_FIELD] = this.atoLabelDescription;
			row[MigrationIssueXMLTraceListener.SCHEDULESUBSECTIONTYPE_FIELD] = this.scheduleSubSectionType; 
			row[MigrationIssueXMLTraceListener.CONTRIBUTINGITEMDESCRIPTION_FIELD] = this.contributingItemDescription;
			row[MigrationIssueXMLTraceListener.OLDVALUE_FIELD] = this.oldValue;
			row[MigrationIssueXMLTraceListener.CURRENTVALUE_FIELD] = this.currentValue;
			row[MigrationIssueXMLTraceListener.POSITION_FIELD] = this.position;
			row[MigrationIssueXMLTraceListener.DIFF_FIELD] = this.difference;
			row[MigrationIssueXMLTraceListener.YEAR_FIELD] = this.year;

			
			table.Rows.Add(row);
		}
	}


	public class MigrationIssueEXP40Item
	{
		#region Private members
		private Guid id = Guid.Empty;
		private string breadCrumb = String.Empty;
		private string calculationModule = String.Empty;
		private string workpaperName = String.Empty;
		private string contributingItemDescription = String.Empty;
		private string oldValue = String.Empty;
		private string currentValue = String.Empty;
		public string difference = String.Empty;

		#endregion

		#region Public properties
		public Guid ID
		{
			get
			{
				return this.id;
			}
			set
			{
				this.id = value;
			}
		}
        		
		public string BreadCrumb
		{
			get
			{
				return this.breadCrumb;
			}
			set
			{
				this.breadCrumb = value;
			}
		}
		
		public string CalculationModule
		{
			get
			{
				return this.calculationModule;
			}
			set
			{
				this.calculationModule = value;
			}
		}
	
		public string WorkpaperName
		{
			get
			{
				return this.workpaperName;
			}
			set
			{
				this.workpaperName = value;
			}
		}

		public string ContributingItemDescription
		{
			get
			{
				return this.contributingItemDescription;												
			}
			set
			{
				this.contributingItemDescription = value;
			}
		}
	
		public string OldValue
		{
			get
			{
				return this.oldValue;
			}
			set
			{
				this.oldValue = value;
			}
		}

		public string CurrentValue
		{
			get
			{
				return this.currentValue;
			}
			set
			{
				this.currentValue = value;
			}
		}
		#endregion

		#region Constructors
		public MigrationIssueEXP40Item() {}

		public MigrationIssueEXP40Item(Guid id, string breadCrumb, string calculationModule,
			string workpaperName, string contributingItemDescription, string oldValue, string currentValue,string difference)
		{
			this.difference = difference;
			this.id = id;
			this.breadCrumb = breadCrumb;
			this.calculationModule = calculationModule;
			this.workpaperName = workpaperName;
			this.contributingItemDescription = contributingItemDescription;
			this.oldValue = oldValue;
			this.currentValue = currentValue;	
		}
		#endregion

		#region Member methods
		public void ExtractData(DataSet ds)
		{
			DataTable table = ds.Tables[MigrationIssueXMLTraceListener.EXP40TABLENAME];
			DataRow row = table.NewRow();
			row[MigrationIssueXMLTraceListener.ID_FIELD] = this.ID.ToString();
			row[MigrationIssueXMLTraceListener.BREADCRUMB_FIELD] = this.BreadCrumb;
			row[MigrationIssueXMLTraceListener.CALCULATIONMODULE_FIELD] = this.CalculationModule;
			row[MigrationIssueXMLTraceListener.WORKPAPERNAME_FIELD] = this.WorkpaperName;
			row[MigrationIssueXMLTraceListener.CONTRIBUTINGITEMDESCRIPTION_FIELD] = this.ContributingItemDescription;
			row[MigrationIssueXMLTraceListener.OLDVALUE_FIELD] = this.OldValue;
			row[MigrationIssueXMLTraceListener.CURRENTVALUE_FIELD] = this.CurrentValue;
			row[MigrationIssueXMLTraceListener.DIFF_FIELD] = this.difference;
			table.Rows.Add(row);
		}
		#endregion
	}
}
