using System;
using System.Collections;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Data.SqlClient;
using System.Data;
using System.Text;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.UpdateUtilities;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CM.Period;
using Oritax.TaxSimp.DSCalculation;
using ICSharpCode.SharpZipLib.Zip;

namespace Oritax.TaxSimp.Administration.SystemUpdate
{
	public delegate void ProgressUpdate(string message); 
	public delegate void SaveDueToMemory();

	/// <summary>
	/// Represents the encapsulation of the job to install a package of deployment units.
	/// </summary>
	public class DeploymentPackageInstallJob
	{
		private const string ATOMIGRATIONISSUEHEADERMESSAGE = "{0} > {1} [{2}]\r\n\r\n{3}";
		private const string MIGRATION_PROGRESS_MESSAGE = "Processing {0} > {1} [{2}] : {3}";
        private const string MIGRATION_PROGRESS_CHECK_MESSAGE = "Checking {0} > {1} [{2}] : {3}";
        public event UpdateProgressHandler ComponentUpdated;
		public event UpdateMessageHandler LogMigrationInformation;

        private const string JQueryLibraryFileName = "jquery-1.3.2.min.js";
		
		public class DeploymentPackageInstallJobException : Exception
		{
			public DeploymentPackageInstallJobException(string message) : base(message){}
		}

        public string GetCompleteExceptionDetails(Exception ex)
        {
            string message = ex.Message;
            message += ex.StackTrace;

            if (ex.InnerException != null)
            {
                Exception tempEx = ex.InnerException;

                while (tempEx.InnerException != null)
                {
                    message += "\n" + tempEx.InnerException.Message;
                    message += "\n" + tempEx.InnerException.StackTrace;
                    tempEx = tempEx.InnerException;
                }
            }
            return message;
        }
		public enum InstallJobType
		{
			UNKNOWN				=	0,
			NEW_INSTALLATION	=	1,
			UPDATE				=	2,
			REPAIR				=	3
		}

		private string			installToolsFolderPath;
		private string			packageFolderPath;
		private string			tempFolderPath;
		private string			installFolderPath;
		private string			helpFolderPath;
		private InstallJobType	installJobType=InstallJobType.UNKNOWN;
        private string installedBrokerAssemblyName = "CMBroker_1_1";
		private string			connectionString;
		private ProgressUpdate	progressUpdate;
        private Hashtable       cmMigrated = new Hashtable();
        private string          installFolderMainPath;

		private long maximumMemorySize = Convert.ToInt64(ConfigurationManager.AppSettings["CachingMemoryTriggerMaxMemorySize"]);

		private DeploymentUnitInstallJob[] deploymentUnitInstallJobs;

		private void onComponentUpdated(UpdateProgressEventArgs e)
		{
			if (ComponentUpdated != null)
			{
				ComponentUpdated(this, e);
			}
		}
		private void onLogMigrationInformation(UpdateMessageEventArgs e)
		{
			if (LogMigrationInformation != null)
			{
				LogMigrationInformation(this, e);
			}
		}
		/// <summary>
		/// Constructor for the job in which all the deployment units in the package are cab files that
		/// are present in a flat folder.
		/// </summary>
		/// <param name="packageFolderPath">The absolute path name of the folder containing the cab file deployment units.</param>
		public DeploymentPackageInstallJob(string packageFolderPath,string tempFolderPath,string installFolderPath,string installToolsFolderPath,string connectionString) : 
			this(packageFolderPath, tempFolderPath, installFolderPath, installToolsFolderPath, connectionString, null)
		{
		}

		/// <summary>
		/// Constructor for the job in which all the deployment units in the package are cab files that
		/// are present in a flat folder.
		/// </summary>
		/// <param name="packageFolderPath">The absolute path name of the folder containing the cab file deployment units.</param>
		public DeploymentPackageInstallJob(string packageFolderPath,string tempFolderPath,string installFolderPath,string installToolsFolderPath,string connectionString, ProgressUpdate progressUpdate)
		{
			this.packageFolderPath=packageFolderPath;
			this.tempFolderPath=tempFolderPath;
            this.installFolderMainPath = installFolderPath; //this variable is used to write the InstallUtil.InstallLog file to the specified path
			this.installFolderPath=installFolderPath + @"\bin";
			this.helpFolderPath=installFolderPath + @"\help";
			this.installToolsFolderPath=installToolsFolderPath;
			this.connectionString=connectionString;
			this.progressUpdate = progressUpdate;

			if(!Directory.Exists(packageFolderPath))
			{
				throw new DeploymentPackageInstallJobException("The folder containing the deployment package does not exist");
			}

			if(!Directory.Exists(tempFolderPath))
			{
				throw new DeploymentPackageInstallJobException("The temporary folder does not exist");
			}

			if(!Directory.Exists(installFolderPath))
			{
				throw new DeploymentPackageInstallJobException("The installation folder does not exist");
			}

			installJobType=InstallJobType.UNKNOWN;
			installedBrokerAssemblyName=null;


            if(IntPtr.Size == 4) // only install the service from here when running in 32 bit mode
                StopExcelHostService();

			cleanAssemblyFolder();
		}

		public void Install(InstallJobType installJobType)
		{
			this.installJobType=installJobType;
			Install();
		}
		/// <summary>
		/// Installs the deployment package specified in the deployment package job.
		/// </summary>
		private void Install()
		{
			PopulateInstallJobs();
			
			foreach(DeploymentUnitInstallJob deploymentUnitInstallJob in deploymentUnitInstallJobs)
			{
				deploymentUnitInstallJob.Unpack();
			}

			AuditInstallJobs();

			//copy back legacy assemblies
			if(this.installJobType==InstallJobType.UPDATE)
			{
				FileManager fileManager = new FileManager();
				fileManager.UpdateDeployedVersion(this.tempFolderPath, this.installFolderPath);
			}

			BackupInstances();//At this stage we Export XML files if needed
			InstallFiles();
            UpdateZippedJavascriptFiles();
            InstallDataModel();
			InstallNewTypes();
			RestoreExistingData();//here we process exported xml files
            UpdateCMs(); //here we iterate through MigrationCompleted() methods of selected CMs
			DeleteDataModel();

            if (IntPtr.Size == 4) // only install the service from here when running in 32 bit mode
                InstallExcelHostService();
		}

        private void UpdateZippedJavascriptFiles()
        {
            string scriptsDirectory = string.Format(@"{0}\Scripts", this.installFolderMainPath);
            string compressedFilename = string.Format(@"{0}\{1}.zip", scriptsDirectory, JQueryLibraryFileName);
            string unCompressedFilename = string.Format(@"{0}\{1}", scriptsDirectory, JQueryLibraryFileName);
            if (!File.Exists(compressedFilename))
                return;
            try
            {
                File.Copy(compressedFilename, unCompressedFilename);
            }
            catch
            {
            }
        }

        private void UnZip_SkinFile()
        {
            String zipPathAndFile = string.Format(@"{0}\Scripts\grid\skin.zip", this.installFolderMainPath);
            String outputFolder = string.Format(@"{0}\Scripts\grid", this.installFolderMainPath);

            FastZip fastZip = new FastZip();
            fastZip.ExtractZip(zipPathAndFile, outputFolder, "");
        }

		private void PopulateInstallJobs()
		{
			// -------------------------------------------------------------------------------------------
			// Obtain the list of .CAB files from the deployment package folder
			string[] cabFileNames=Directory.GetFiles(this.packageFolderPath,"*.CAB");
			int totalDeployedUnits = cabFileNames.Length;
			// -------------------------------------------------------------------------------------------
			// create deployment unit jobs for each .CAB file in the deployment package
			onComponentUpdated(new UpdateProgressEventArgs(5, 100, "Deploying cabinet files"));
			deploymentUnitInstallJobs=new DeploymentUnitInstallJob[cabFileNames.Length];
			for(int i=0;i<cabFileNames.Length;i++)
			{		
				onProgressUpdate("Deploying " + cabFileNames[i]);
				onComponentUpdated(new UpdateProgressEventArgs(5, 100, "Deploying cabinet files - " +cabFileNames[i].ToString()));
				deploymentUnitInstallJobs[i]=
					new DeploymentUnitInstallJob(
					null,
					cabFileNames[i],
					this.tempFolderPath,
					this.helpFolderPath,
					Directory.GetParent(this.installFolderPath).FullName
					);
			}
		}

		private void AuditInstallJobs()
		{
			//--------------------------------------------------------------------------------------------
			// Audit the install state of the elements represented by the set of jobs that have been unpacked.
			onComponentUpdated(new UpdateProgressEventArgs(10, 100, "Checking deployed files"));
			if(this.installJobType==InstallJobType.UNKNOWN
				|| this.installJobType==InstallJobType.UPDATE)
			{
				FileManagerProxy fileManager=FileManagerProxy.CreateFileManager(this.installToolsFolderPath);
				try
				{
					foreach(DeploymentUnitInstallJob deploymentUnitInstallJob in deploymentUnitInstallJobs)
					{
						onComponentUpdated(new UpdateProgressEventArgs(10, 100, "Checking deployed files - " +deploymentUnitInstallJob.Name));
						deploymentUnitInstallJob.FileManager=fileManager;
						deploymentUnitInstallJob.AuditInstallStates();
					}

					DeploymentUnitInstallJob.NAbleAssemblyName[] cmBrokerAssemblies=fileManager.FindNAbleAssembly(installFolderPath,"CMBroker");
					if(cmBrokerAssemblies.Length>1)
						throw new Exception("More than one CMBroker exists on the system!  This is not permitted.");
					if(cmBrokerAssemblies.Length==1)
						installedBrokerAssemblyName=cmBrokerAssemblies[0].VersionedName;
					else
						installedBrokerAssemblyName=null;
				}
				finally
				{
					FileManagerProxy.DestroyFileManager(fileManager);
				}
			}
		}

		private void BackupInstances()
		{
			//--------------------------------------------------------------------------------------------
			// Perform any backups of instances of Broker Managed components that will require automatic
			// migration because replacement implementations have been installed.
			// *** Backup is only performed if the installation is an Update
			onComponentUpdated(new UpdateProgressEventArgs(1, 5, "Backing up "));
			if(this.installJobType==InstallJobType.UPDATE)
			{
				InstallBroker backupBroker=InstallBroker.CreateBrokerProxy(this.installedBrokerAssemblyName,installFolderPath,connectionString);
				
				try
				{

					backupBroker.SetStart(false);
					foreach(DeploymentUnitInstallJob deploymentUnitInstallJob in deploymentUnitInstallJobs)
					{

                        ReclaimInstallBrokers(ref backupBroker);
						
						onComponentUpdated(new UpdateProgressEventArgs(1, 5, "Backing up - " +deploymentUnitInstallJob.Name));
						deploymentUnitInstallJob.Broker=backupBroker;
						deploymentUnitInstallJob.InstanceBackup();
						    
					}
					//Table back up performed for testing couple issues: 
					backupBroker.SetComplete(false);
				}
				finally
				{
					InstallBroker.DestroyBrokerProxy(backupBroker);
				}
			}
		}

        private void StopExcelHostService()
        {
            onComponentUpdated(new UpdateProgressEventArgs(5, 20, "Stopping Excel Reader Host Service"));

            //stop the service: Again this will fail if the service does not already exist so just swallow the exception.

            string output;

            try
            {
                DeploymentUnitInstallJob.RunProcess("net", null, new string[] { "stop", "\"Nable Excel Reader Host\"" }, out output, false);
            }
            catch (Exception)
            {

            }
        }

        private void InstallExcelHostService()
        {
            string output = string.Empty;
            string installUtilExeName = "installUtil.exe";
            string excelHostServiceName = "ExcelReaderHost.exe";
            string frameworkDir = WebDeployment.DotNetFrameWorkPath;
            string assemblyPath = DeploymentUnitInstallJob.ServicesPath;

            string fullUtilPath = frameworkDir + @"\" + installUtilExeName;
            string fullHostExePath = assemblyPath + excelHostServiceName;

            string[] uninstallArgs = new string[] { @"/u", fullHostExePath };
            string[] installArgs = new string[] { fullHostExePath };

            string workingDirectory = installFolderMainPath;
            //first uninstall service if already installed, this will cause an exception if the service is not there.
            try
            {
                DeploymentUnitInstallJob.RunProcess(fullUtilPath, workingDirectory, uninstallArgs, out output, false);
            }
            catch (Exception)
            {

            }

            //Copy the dependancy file.
            string dependancyFile = DeploymentUnitInstallJob.ServicesPath + "ExcelReaderInterfaces.dll";
            string sourceOfDependancyFile = this.installFolderPath + @"\" + "ExcelReaderInterfaces.dll";
            File.Copy(sourceOfDependancyFile, dependancyFile, true);

            //install the service
            DeploymentUnitInstallJob.RunProcess(fullUtilPath, workingDirectory, installArgs, out output, false);

            //start the service
            DeploymentUnitInstallJob.RunProcess("net", workingDirectory, new string[] { "start", "\"Nable Excel Reader Host\"" }, out output, false);

        }


		private void InstallFiles()
		{
			//--------------------------------------------------------------------------------------------
			// Install new or replacement files
			onComponentUpdated(new UpdateProgressEventArgs(5, 20, "Installing new components"));
			FileManagerProxy installFileManager=FileManagerProxy.CreateFileManager(installToolsFolderPath);
			try
			{
				foreach(DeploymentUnitInstallJob deploymentUnitInstallJob in deploymentUnitInstallJobs)
				{
					onComponentUpdated(new UpdateProgressEventArgs(5, 20, "Installing new components - " +deploymentUnitInstallJob.Name));
					deploymentUnitInstallJob.FileManager=installFileManager;
					deploymentUnitInstallJob.InstallFiles();
				}

				DeploymentUnitInstallJob.NAbleAssemblyName[] cmBrokerAssemblies=installFileManager.FindNAbleAssembly(installFolderPath,"CMBroker");
				if(cmBrokerAssemblies.Length>1)
					throw new Exception("More than one CMBroker exists on the system!  This is not permitted.");
				if(cmBrokerAssemblies.Length==1)
					installedBrokerAssemblyName=cmBrokerAssemblies[0].VersionedName;
				else
					throw new Exception("No CMBroker was found in the deploy directory. This is required for successful installation.");
			}
			finally
			{
				FileManagerProxy.DestroyFileManager(installFileManager);
			}
		}

		private void InstallDataModel()
		{
			//--------------------------------------------------------------------------------------------
			// Install new or replacement data model
			onComponentUpdated(new UpdateProgressEventArgs(20, 25, "Installing new datamodel"));
			InstallBroker dataModelInstallBroker=InstallBroker.CreateBrokerProxy(this.installedBrokerAssemblyName,installFolderPath,connectionString);
			FileManagerProxy dataModelFileManager=FileManagerProxy.CreateFileManager(installFolderPath);
			try
			{
				dataModelInstallBroker.SetStart(false);
				foreach(DeploymentUnitInstallJob deploymentUnitInstallJob in deploymentUnitInstallJobs)
				{
					onComponentUpdated(new UpdateProgressEventArgs(20, 25, "Installing new datamodel - " +deploymentUnitInstallJob.Name));
					deploymentUnitInstallJob.FileManager=dataModelFileManager;
					deploymentUnitInstallJob.Broker=dataModelInstallBroker;
					deploymentUnitInstallJob.InstallDataModel();
				}
				dataModelInstallBroker.SetComplete(false);
			}
			catch
			{
				dataModelInstallBroker.SetAbort();
				throw;
			}
			finally
			{
				InstallBroker.DestroyBrokerProxy(dataModelInstallBroker);
				FileManagerProxy.DestroyFileManager(dataModelFileManager);
			}
		}

		private void InstallNewTypes()
		{
			//--------------------------------------------------------------------------------------------
			// Install new or replacement BMC types
			onComponentUpdated(new UpdateProgressEventArgs(25, 35, "Installing new types"));
			InstallBroker bmcTypeInstallBroker=InstallBroker.CreateBrokerProxy(this.installedBrokerAssemblyName,installFolderPath,connectionString);
			FileManagerProxy bmcTypeInstallFileManager=FileManagerProxy.CreateFileManager(installFolderPath);
			try
            {
                ArrayList workpapers = new ArrayList();
                ArrayList consolWorkpapers = new ArrayList();

				bmcTypeInstallBroker.SetStart(false);
				foreach(DeploymentUnitInstallJob deploymentUnitInstallJob in deploymentUnitInstallJobs)
				{
					onComponentUpdated(new UpdateProgressEventArgs(25, 35, "Installing new types - " + deploymentUnitInstallJob.Name));
					deploymentUnitInstallJob.FileManager=bmcTypeInstallFileManager;
					deploymentUnitInstallJob.Broker=bmcTypeInstallBroker;
					deploymentUnitInstallJob.InstallData(workpapers, consolWorkpapers);
				}
				bmcTypeInstallBroker.SetComplete(true);

                DeploymentUnitInstallJob.SerialiseWorkPapersList(workpapers, Path.Combine(installFolderMainPath, @"config\WorkpapersList.xml"));
                DeploymentUnitInstallJob.SerialiseWorkPapersList(consolWorkpapers, Path.Combine(installFolderMainPath, @"config\WorkpapersListConsol.xml"));
			}
			catch
			{
				bmcTypeInstallBroker.SetAbort();
				throw;
			}
			finally
			{
				InstallBroker.DestroyBrokerProxy(bmcTypeInstallBroker);
				FileManagerProxy.DestroyFileManager(bmcTypeInstallFileManager);
			}
		}
		
		private InstallBroker savedInstallBroker = null;

		private void RestoreExistingData()
		{
			//Change
			//--------------------------------------------------------------------------------------------
			// Restore backed-up BMC type instances
			onComponentUpdated(new UpdateProgressEventArgs(35, 40, "Restoring existing data"));
			if(this.installJobType==InstallJobType.UPDATE)
			{
                this.installedBrokerAssemblyName = "CMBroker_1_1";

				savedInstallBroker = new InstallBroker(this.installedBrokerAssemblyName, connectionString, installFolderPath);
				try
				{
                    SaveDueToMemory saveDelegate = new SaveDueToMemory(delegateSaveMigrationInProcess);

					ArrayList instanceGuids = new ArrayList();
					onComponentUpdated(new UpdateProgressEventArgs(35, 40, "Restoring existing data - restoring data"));
					savedInstallBroker.SetStart(true);
					int count = 0;
					foreach(DeploymentUnitInstallJob deploymentUnitInstallJob in deploymentUnitInstallJobs)
					{
						count++;
						int calcPercentage = CalcPercentage(count, deploymentUnitInstallJobs.Length, 2);
						int currPercentage = Math.Min(35 + calcPercentage, 40);
						onComponentUpdated(new UpdateProgressEventArgs(currPercentage, 40, string.Format("Restoring existing data - restoring data {0} of {1}", count, deploymentUnitInstallJobs.Length)));

						deploymentUnitInstallJob.Broker=savedInstallBroker;
						instanceGuids.AddRange(deploymentUnitInstallJob.InstanceRestore(saveDelegate));

						saveDueToMemory(ref savedInstallBroker, true);
					}
					
					savedInstallBroker.SetComplete(false);
					clearCache(ref savedInstallBroker);
					//					onComponentUpdated(new UpdateProgressEventArgs(45, 100, "Restoring existing data - data links repair"));
					//					
					//					savedInstallBroker.SetStart(false);
					//					count = 0;
					//					foreach(DeploymentUnitInstallJob deploymentUnitInstallJob in deploymentUnitInstallJobs)
					//					{
					//						count++;
					//						int calcPercentage = CalcPercentage(count, deploymentUnitInstallJobs.Length, 10); 
					//						onComponentUpdated(new UpdateProgressEventArgs(45 + calcPercentage, 100, string.Format("Restoring existing data - data links repair {0} of {1}", count, deploymentUnitInstallJobs.Length)));
					//						deploymentUnitInstallJob.Broker=savedInstallBroker;
					//						deploymentUnitInstallJob.Resubscribe();
					//
					//						saveDueToMemory(ref savedInstallBroker, false);
					//					}
					//					
					//					savedInstallBroker.SetComplete(false);
					//					clearCache(ref savedInstallBroker);
					//					onComponentUpdated(new UpdateProgressEventArgs(55, 100, "Restoring existing data - republishing data"));
					//					
					//					bmcRestoreBroker.SetStart(false);
					//					count = 0;
					//					foreach(Guid instanceGuid in instanceGuids)
					//					{
					//						count++;
					//						int calcPercentage = CalcPercentage(count, instanceGuids.Count, 10); 
					//						onComponentUpdated(new UpdateProgressEventArgs(55 + calcPercentage, 100, string.Format("Restoring existing data - republishing data {0} of {1}", count, instanceGuids.Count)));
					//						bmcRestoreBroker.MigrationCompleted(instanceGuid);
					//
					//						saveDueToMemory(ref bmcRestoreBroker, false);
					//					}
					//					bmcRestoreBroker.SetComplete(false);

					//					savedInstallBroker.Dispose();
					//					savedInstallBroker = null;
				}
				catch
				{
					savedInstallBroker.SetAbort();
					throw;
				}
			}
		}

        private void delegateSave()
        {
            if (maximumMemorySize < GC.GetTotalMemory(false))
            {
                savedInstallBroker.SetComplete(false);
                savedInstallBroker.SetStart(false);
            }
        }

        private void delegateSaveMigrationInProcess()
        {
            if (maximumMemorySize < GC.GetTotalMemory(false))
            {
                savedInstallBroker.SetComplete(false);
                savedInstallBroker.SetStart(true);
            }
        }

        private void saveDueToMemory(ref InstallBroker installBroker, bool migrationInProgress)
		{
			if (maximumMemorySize < GC.GetTotalMemory(false))
			{
				installBroker.SetComplete(false);

				clearCache(ref installBroker);

				installBroker.SetStart(migrationInProgress);
			}
		}

		private void clearCache(ref InstallBroker installBroker)
		{
			installBroker.SetStart(false);
			installBroker.ClearCache();
			installBroker.SetComplete(false);
		}

		private void UpdateCMs( )
		{
			if(this.installJobType==InstallJobType.UPDATE)
			{
				onComponentUpdated(new UpdateProgressEventArgs(40, 55, "Restoring existing data - updating data"));
				InstallBroker bmcRestoreBroker = new InstallBroker(this.installedBrokerAssemblyName,connectionString, installFolderPath);

				ArrayList requiredUpdateAssemblyIDs = new ArrayList();
                requiredUpdateAssemblyIDs.Add(new Guid("{3A775463-DD6F-40C3-9E3B-F83641D4B5BE}")); // Period

              	this.RecurseGroupStructure(requiredUpdateAssemblyIDs, new ProcessCM(this.MigrateCompleteProcess), null, bmcRestoreBroker, 55, 60);
			}
		}


        /// <summary>
        /// 2007.2 release only
        /// remove notes from TrueUp reports - Tea021, Tea031, Tea041, Tea042, Tea051
        /// </summary>
        private void DeleteTrueUpReportsNotes(InstallBroker bmcRestoreBroker)
        {
            try
            {
                onLogMigrationInformation(new UpdateMessageEventArgs("DeleteTrueUpReportsNotes: Start "));
                ArrayList wpIds = new ArrayList();
                wpIds.Add("{59EE2FCC-4FF0-4279-A633-84A3BAB3ACED}");
                wpIds.Add("{D57999F9-F3D1-4940-9E7C-799E07B715B5}");
                wpIds.Add("{33D910A0-38C3-4e68-8DB0-21CFD8916BAC}");
                wpIds.Add("{0F674C7B-90BF-440d-AFF8-39BF553189FF}");
                wpIds.Add("{6B9486FB-9F55-4174-83C5-E8074D7B9D8F}"); 
                
                bmcRestoreBroker.SetStart(false);
                bool changesMade = true;

                string cmdText = GetDeleteNotesCmdText(wpIds);
                onLogMigrationInformation(new UpdateMessageEventArgs("DeleteTrueUpReportsNotes: [" + cmdText + "]"));
                SqlCommand command = bmcRestoreBroker.GetSqlCommand(cmdText);
                command.ExecuteNonQuery();

                bmcRestoreBroker.SetComplete(changesMade);
                onLogMigrationInformation(new UpdateMessageEventArgs("DeleteTrueUpReportsNotes: End"));
            }
            catch (Exception ex)
            {
                bmcRestoreBroker.SetAbort();
                throw ex;
            }
        }
        /// <summary>
        /// Generate command text to Delete notes from several  workpapers
        /// </summary>
        /// <param name="wpIds">list of workpaper ids as strings</param>
        /// <returns>command text</returns>
        private string GetDeleteNotesCmdText(ArrayList wpIds)
        {
            StringBuilder expr = new StringBuilder();
            foreach (string id in wpIds)
            {
                if (expr.Length > 0)
                    expr.Append(" OR ");
                expr.Append(string.Format("{0} ='{1}'", NoteListDS.NOTES_WORKPAPERID_FIELD, id));
            }

            string strQuery = string.Format("DELETE FROM {0} WHERE {1}",
                                       NoteListDS.NOTES_TABLE, expr.ToString());
            return strQuery;
        }

		private void RecurseGroupStructure(ArrayList requiredUpdateAssemblyIDs, ProcessCM processCMDelegate, object data, InstallBroker bmcRestoreBroker, int minPercentage, int maxPercentage)
		{
			ArrayList reportingUnitsProcessed = new ArrayList();
			try
			{
				bmcRestoreBroker.SetStart(false);

				ICalculationModule organisation = bmcRestoreBroker.GetOrganisation();
				ILogicalModule organisationLM = bmcRestoreBroker.GetLogicalCM(organisation.CLID);
				ArrayList childCLIDs = organisationLM.ChildCLIDs;
				int count = 0;
				foreach(Guid childCLID in childCLIDs)
				{
					count++;
					int calcPercentage = CalcPercentage(count, childCLIDs.Count, 3); 
					int currPercentage = Math.Min(minPercentage + calcPercentage, maxPercentage);
					onComponentUpdated(new UpdateProgressEventArgs(currPercentage, maxPercentage, string.Format("Restoring existing data - republishing {0} of {1}", count, (childCLIDs.Count))));
						
					ILogicalModule logicalReportingUnit = bmcRestoreBroker.GetLogicalCM(childCLID);

					foreach(DictionaryEntry scenarioDE in logicalReportingUnit.Scenarios)
					{	
						CMScenario scenario = (CMScenario) scenarioDE.Value;

						IOrganizationUnit organisationUnit = (IOrganizationUnit) bmcRestoreBroker.GetCMImplementation(logicalReportingUnit.CLID, scenario.CSID);
						if(organisationUnit is IGroup)
						{
							if(!reportingUnitsProcessed.Contains(organisationUnit.CLID.ToString() + organisationUnit.CSID.ToString()))
							{
								this.RecurseThroughGroupToPerformAction(reportingUnitsProcessed, (IGroup) organisationUnit, bmcRestoreBroker, requiredUpdateAssemblyIDs, processCMDelegate, data);
								this.PerformActionOrganizationUnit(organisationUnit, bmcRestoreBroker, requiredUpdateAssemblyIDs, processCMDelegate, data);
								this.PerformActionInAllPeriods(organisationUnit, bmcRestoreBroker, requiredUpdateAssemblyIDs, processCMDelegate, data);
								reportingUnitsProcessed.Add(organisationUnit.CLID.ToString() + organisationUnit.CSID.ToString());
							}
						}
						else
						{
							if(!reportingUnitsProcessed.Contains(organisationUnit.CLID.ToString() + organisationUnit.CSID.ToString()))
							{
								this.PerformActionOrganizationUnit(organisationUnit, bmcRestoreBroker, requiredUpdateAssemblyIDs, processCMDelegate, data);
								this.PerformActionInAllPeriods(organisationUnit, bmcRestoreBroker, requiredUpdateAssemblyIDs, processCMDelegate, data);
								reportingUnitsProcessed.Add(organisationUnit.CLID.ToString() + organisationUnit.CSID.ToString());
							}
						}

						saveDueToMemory(ref bmcRestoreBroker, false);
					}
				}

				bmcRestoreBroker.SetComplete();
			}
            catch (Exception ex)
			{
                string msg = GetCompleteExceptionDetails(ex);
                Trace.WriteLine(msg);
                onLogMigrationInformation(new UpdateMessageEventArgs("PerformActionInAllPeriods: " + msg));

				bmcRestoreBroker.SetAbort();
				throw ex;
			}
		}

		private void RecurseThroughGroupToPerformAction(ArrayList groupsAlreadyUpgraded, IGroup groupToRecurse, InstallBroker broker, ArrayList requiredUpdateAssemblyIDs, ProcessCM processCMDelegate, object data)
		{
			ICalculationModule groupAsCM = (ICalculationModule) groupToRecurse;

			string name = groupAsCM.Name;
			onLogMigrationInformation(new UpdateMessageEventArgs("RecurseThroughGroupToPerformAction: Group: " + name));

			if(!groupsAlreadyUpgraded.Contains(groupAsCM.CLID.ToString() + groupAsCM.CSID.ToString()))
			{
				GroupMembers members = groupToRecurse.IncludedGroupMembers;

				foreach(DictionaryEntry memberDE in members)
				{
					GroupMember member = (GroupMember) memberDE.Value;

					IOrganizationUnit organisationUnit = (IOrganizationUnit) broker.GetCMImplementation(member.CLID, member.CSID);

					if(organisationUnit is IGroup)
					{
						if(!groupsAlreadyUpgraded.Contains(organisationUnit.CLID.ToString() + organisationUnit.CSID.ToString()))
						{
							this.RecurseThroughGroupToPerformAction(groupsAlreadyUpgraded, (IGroup) organisationUnit, broker, requiredUpdateAssemblyIDs, processCMDelegate, data);
							this.PerformActionOrganizationUnit(organisationUnit, broker, requiredUpdateAssemblyIDs, processCMDelegate, data);
							this.PerformActionInAllPeriods(organisationUnit, broker, requiredUpdateAssemblyIDs, processCMDelegate, data);
							groupsAlreadyUpgraded.Add(organisationUnit.CLID.ToString() + organisationUnit.CSID.ToString());
						}
					}
					else
					{
						if(!groupsAlreadyUpgraded.Contains(organisationUnit.CLID.ToString() + organisationUnit.CSID.ToString()))
						{
							this.PerformActionOrganizationUnit(organisationUnit, broker, requiredUpdateAssemblyIDs, processCMDelegate, data);
							this.PerformActionInAllPeriods(organisationUnit, broker, requiredUpdateAssemblyIDs, processCMDelegate, data);
							groupsAlreadyUpgraded.Add(organisationUnit.CLID.ToString() + organisationUnit.CSID.ToString());
						}
					}
 
					saveDueToMemory(ref broker, false);
				}
			}
		}

        private void PerformActionInAllPeriods(IOrganizationUnit organisationUnit, InstallBroker broker, ArrayList cmTypesToUpdate, ProcessCM processCMDelegate, object data)
        {
            ICalculationModule organisationUnitAsCM = (ICalculationModule)organisationUnit;
            ILogicalModule organisationUnitLM = broker.GetLogicalCM(organisationUnitAsCM.CLID);

            foreach (CMScenario periodCMS in organisationUnitLM.GetChildCMs(organisationUnitAsCM.CSID))
            {
                ILogicalModule periodLM = broker.GetLogicalCM(periodCMS.CLID);

                foreach (Guid typeID in cmTypesToUpdate)
                {
                    if (periodCMS.CMTypeID == typeID)
                    {
                        IBrokerManagedComponent migBMC = (IBrokerManagedComponent)broker.GetCMImplementation(periodCMS.CLID, periodCMS.CSID);

                        string ouName = organisationUnitAsCM.Name;
                        string scenarioName = periodCMS.Name;
                        string periodName = periodLM[periodCMS.CSID].Name;
                        string cmName = "Module is not found " + typeID.ToString();
                        if (migBMC != null)
                            cmName = migBMC.Name;

                        Trace.WriteLine(String.Format(ATOMIGRATIONISSUEHEADERMESSAGE, new object[] { ouName, scenarioName, periodName, cmName }), "MigrationIssueHeaderInfo");
                        string msg = String.Format(MIGRATION_PROGRESS_MESSAGE, new object[] { ouName, scenarioName, periodName, cmName });
                        onLogMigrationInformation(new UpdateMessageEventArgs("PerformActionInAllPeriods:" + msg));
                        processCMDelegate(migBMC, data);

                        broker.ReleaseBrokerManagedComponent(migBMC);
                    }
                    else
                    {
                        foreach (CMScenario atoCMS in periodLM.GetChildCMsByType(typeID, periodCMS.CSID))
                        {
                            IBrokerManagedComponent atoBMC = (IBrokerManagedComponent)broker.GetCMImplementation(atoCMS.CLID, atoCMS.CSID);

                            string ouName = organisationUnitAsCM.Name;
                            string scenarioName = periodCMS.Name;
                            string periodName = periodLM[periodCMS.CSID].Name;
                            string cmName = "Module is not found " + atoCMS.CMTypeName;
                            if (atoBMC != null)
                                cmName = atoBMC.Name;

                            Trace.WriteLine(String.Format(ATOMIGRATIONISSUEHEADERMESSAGE, new object[] { ouName, scenarioName, periodName, cmName }), "MigrationIssueHeaderInfo");
                            string msg = String.Format(MIGRATION_PROGRESS_MESSAGE, new object[] { ouName, scenarioName, periodName, cmName });
                            onLogMigrationInformation(new UpdateMessageEventArgs("PerformActionInAllPeriods: " + msg));

                            processCMDelegate(atoBMC, data);

                            if (atoBMC.Modified && atoBMC is ICalculationModule)
                            {
                                ILogicalModule moduleLM = broker.GetLogicalCM(atoCMS.CLID);
                                moduleLM.DivergeScenario(atoBMC as ICalculationModule);
                            }


                            broker.ReleaseBrokerManagedComponent(atoBMC);
                        }
                    }
                }
            }
        }

        //private void PerformActionInAllPeriods(IOrganizationUnit organisationUnit, InstallBroker broker, ArrayList cmTypesToUpdate, ProcessCM processCMDelegate, object data)
        //{
        //    ICalculationModule organisationUnitAsCM = (ICalculationModule)organisationUnit;
        //    ILogicalModule organisationUnitLM = broker.GetLogicalCM(organisationUnitAsCM.CLID);
        //    string ouName = organisationUnitAsCM.Name;

        //    foreach (CMScenario periodCMS in organisationUnitLM.GetChildCMs(organisationUnitAsCM.CSID))
        //    {
        //        ILogicalModule periodLM = broker.GetLogicalCM(periodCMS.CLID);
        //        string scenarioName = periodCMS.Name;
        //        string periodName = periodLM[periodCMS.CSID].Name;

        //        foreach (Guid typeID in cmTypesToUpdate)
        //        {
        //            if (periodCMS.CMTypeID == typeID)
        //            {
        //                ProcessComponent(periodCMS, ouName, scenarioName, periodName, broker, processCMDelegate, data, true);
        //            }
        //            else
        //            {
        //                foreach (CMScenario cmScenario in periodLM.GetChildCMsByType(typeID, periodCMS.CSID))
        //                {
        //                    ProcessComponent(cmScenario, ouName, scenarioName, periodName, broker, processCMDelegate, data, false);
        //                }
        //            }
        //        }
        //    }
        //}

        //private void ProcessComponent(CMScenario cmScenario, string ouName, string scenarioName, string periodName, InstallBroker broker, ProcessCM processCMDelegate, object data, bool isPeriod)
        //{
        //    string cmName = cmScenario.CMTypeName;
        //    IBrokerManagedComponent bmc = (IBrokerManagedComponent)broker.GetCMImplementation(cmScenario.CLID, cmScenario.CSID);

        //    if (bmc != null)
        //        cmName = bmc.Name;
        //    else
        //        cmName = "Component is not found " + cmScenario.CMTypeName;

        //    Trace.WriteLine(String.Format(ATOMIGRATIONISSUEHEADERMESSAGE, new object[] { ouName, scenarioName, periodName, cmName }), "MigrationIssueHeaderInfo");
        //    string msg = String.Format(MIGRATION_PROGRESS_MESSAGE, new object[] { ouName, scenarioName, periodName, cmName });
        //    onLogMigrationInformation(new UpdateMessageEventArgs("PerformActionInAllPeriods: " + msg));

        //    processCMDelegate(bmc, data);

        //    if (!isPeriod)//do not diverge period
        //    {
        //        if (bmc.Modified && bmc is ICalculationModule)
        //        {
        //            ILogicalModule moduleLM = broker.GetLogicalCM(cmScenario.CLID);
        //            moduleLM.DivergeScenario(bmc as ICalculationModule);
        //        }
        //    }
        //    broker.ReleaseBrokerManagedComponent(bmc);
        //}

		private void PerformActionOrganizationUnit(IOrganizationUnit organisationUnit, InstallBroker broker, ArrayList cmTypesToUpdate, ProcessCM processCMDelegate, object data)
		{
			ICalculationModule organisationUnitAsCM = (ICalculationModule) organisationUnit;
			ILogicalModule organisationUnitLM = broker.GetLogicalCM(organisationUnitAsCM.CLID);

			if(organisationUnit is IGroup)
			{
				IBrokerManagedComponent migBMC = (IBrokerManagedComponent) broker.GetCMImplementation(organisationUnit.CLID, organisationUnit.CSID);

				string ouName = organisationUnitAsCM.Name;
				string scenarioName = String.Empty;
				string periodName = String.Empty;
				string cmName = migBMC.Name;
						
				Trace.WriteLine(String.Format(ATOMIGRATIONISSUEHEADERMESSAGE, new object[]{ouName,scenarioName,periodName,cmName}),"MigrationIssueHeaderInfo");
				string msg = String.Format(MIGRATION_PROGRESS_MESSAGE, new object[]{ouName,scenarioName,periodName,cmName});
				onLogMigrationInformation(new UpdateMessageEventArgs("PerformActionInAllPeriods:"+ msg));
				processCMDelegate(migBMC, data);

				broker.ReleaseBrokerManagedComponent(migBMC); 
			}

		}
		
		private void PerformActionOrganizationCM(InstallBroker broker, ProcessCM processCMDelegate, object data)
		{

			broker.SetStart(false);

			IBrokerManagedComponent migBMC = (IBrokerManagedComponent)broker.GetOrganisation();


			string ouName = migBMC.Name;
			string scenarioName = String.Empty;
			string periodName = String.Empty;
			string cmName = migBMC.Name;
						
			Trace.WriteLine(String.Format(ATOMIGRATIONISSUEHEADERMESSAGE, new object[]{ouName,scenarioName,periodName,cmName}),"MigrationIssueHeaderInfo");
			string msg = String.Format(MIGRATION_PROGRESS_MESSAGE, new object[]{ouName,scenarioName,periodName,cmName});
			onLogMigrationInformation(new UpdateMessageEventArgs("PerformActionInAllPeriods:"+ msg));
			processCMDelegate(migBMC, data);

			broker.ReleaseBrokerManagedComponent(migBMC); 

			broker.SetComplete(); 
		}


		private delegate void ProcessCM(IBrokerManagedComponent bmc, object data);

		private void MigrateCompleteProcess(IBrokerManagedComponent bmc, object data)
		{
			bmc.MigrationCompleted();
		}

		private void StoreValuesBeforeMigrationFixup(IBrokerManagedComponent bmc, object data)
		{
			Hashtable bmcsValues = data as Hashtable;
			ICalculationModule bmcAsCM = bmc as ICalculationModule;
			string clcsid = bmcAsCM.CLID.ToString() + bmcAsCM.CSID.ToString();

			if(!bmcsValues.Contains(clcsid))
			{
				if(bmc.GetType().GetMethod("StoreValuesBeforeMigrationFixups") != null)
				{
					Hashtable bmcValues = (Hashtable) bmc.GetType().InvokeMember("StoreValuesBeforeMigrationFixups", BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.Public, null, bmc, new object[]{});
					bmcsValues.Add(clcsid, bmcValues);
				}
			}
		}

		private void CheckValuesAfterMigtationFixup(IBrokerManagedComponent bmc, object data)
		{
			Hashtable bmcsValues = data as Hashtable;
			ICalculationModule bmcAsCM = bmc as ICalculationModule;
			string clcsid = bmcAsCM.CLID.ToString() + bmcAsCM.CSID.ToString();

			if(bmcsValues == null)
				throw new ApplicationException("CheckValuesAfterMigtationFixup, hashtable is null or not a hashtable");

			Hashtable bmcValues = (Hashtable) bmcsValues[clcsid];

			if(bmc.GetType().GetMethod("CheckValuesAfterMigrationFixups") != null)
			{
				if(bmcValues == null)
					throw new ApplicationException("CheckValuesAfterMigtationFixup, values don't exist in data, bmc is probably missing a StoreValuesBeforeMigrationFixups method");

				bmc.GetType().InvokeMember("CheckValuesAfterMigrationFixups", BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.Public, null, bmc, new object[]{bmcValues});
			}
		}

		private void DeleteDataModel()
		{
			// Remove any data model elements from previous versions which are no longer required
			onComponentUpdated(new UpdateProgressEventArgs(97, 100, "Removing old data"));
			if(this.installJobType==InstallJobType.UPDATE)
			{
				InstallBroker installBroker = InstallBroker.CreateBrokerProxy(this.installedBrokerAssemblyName,installFolderPath,connectionString);
				FileManagerProxy fileManager = FileManagerProxy.CreateFileManager(installFolderPath);
				try
				{
					installBroker.SetStart(false);
					foreach(DeploymentUnitInstallJob deploymentUnitInstallJob in deploymentUnitInstallJobs)
					{
						onComponentUpdated(new UpdateProgressEventArgs(97, 100, "Removing old data - " + deploymentUnitInstallJob.Name));
						deploymentUnitInstallJob.FileManager = fileManager;
						deploymentUnitInstallJob.Broker = installBroker;
						deploymentUnitInstallJob.RemoveOldDataModel();
					}

					installBroker.SetComplete(false);
				}
                catch (Exception ex)
				{
					installBroker.SetAbort();
					throw ex;
				}
				finally
				{
					InstallBroker.DestroyBrokerProxy(installBroker);
					FileManagerProxy.DestroyFileManager(fileManager);
				}
			}
			onComponentUpdated(new UpdateProgressEventArgs(100, 100, "Removing old data"));
		}

		private void ReclaimBrokers(ref FileManagerProxy fileManagerProxy, ref InstallBroker installBroker, bool setStart)
		{
			if (maximumMemorySize < GC.GetTotalMemory(false))
			{
				installBroker.SetComplete(false);

				FileManagerProxy.DestroyFileManager(fileManagerProxy);
				fileManagerProxy = FileManagerProxy.CreateFileManager(installFolderPath);
				
				InstallBroker.DestroyBrokerProxy(installBroker);
				installBroker = InstallBroker.CreateBrokerProxy(this.installedBrokerAssemblyName,installFolderPath,connectionString);
				
				installBroker.SetStart(setStart);
			}
		}

		private void ReclaimInstallBrokers(ref InstallBroker installBroker)
		{
			if (maximumMemorySize < GC.GetTotalMemory(false))
			{
                installBroker.SetComplete(false);

                InstallBroker.DestroyBrokerProxy(installBroker);
				installBroker = InstallBroker.CreateBrokerProxy(this.installedBrokerAssemblyName,installFolderPath,connectionString);

                installBroker.SetStart(false);
			}
		}

		private InstallJobType DetermineInstallJobType()
		{
			if(null==this.installedBrokerAssemblyName)
				return InstallJobType.NEW_INSTALLATION;

			return InstallJobType.UPDATE;
		}

		// Returns true if there is an installation on the system sufficient to allow the Broker to
		// operate from the installed system.
		private string FindInstalledBrokerName()
		{
			foreach(DeploymentUnitInstallJob deploymentUnitInstallJob in this.deploymentUnitInstallJobs)
			{
				if(deploymentUnitInstallJob.HasBrokerAssembly())
					return deploymentUnitInstallJob.GetBrokerAssemblyName();
			}

			return null;
		}

		private void onProgressUpdate(string message)
		{
			if (progressUpdate != null)
			{
				progressUpdate(message);
			}
		}

		private void cleanAssemblyFolder()
		{
			string appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
			string assemblyPath = Path.Combine(appDataPath, "assembly");

			try
			{
				if (Directory.Exists(assemblyPath))
				{
					Directory.Delete(assemblyPath, true);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}
	
		private int CalcPercentage(int count, int totalCount, int percentageOf)
		{
			float percentage = (float)count/(float)totalCount;
			percentage = percentage * percentageOf;
			return (int)Math.Floor(percentage);
        }
        #region Obsolete migration code
        /// <summary>
        /// Conversion to fix problem #9681 and #8462. 
        /// Is needed for ATO2007 release, can be removed later
        /// </summary>
        /// <param name="bmcRestoreBroker"></param>
        private void FixCmScenarios(InstallBroker bmcRestoreBroker)
        {
            try
            {
                onLogMigrationInformation(new UpdateMessageEventArgs("FixCmScenarios: start checking"));

                bmcRestoreBroker.SetStart(false);
                bool changesMade = false;

                string cmdText = "SELECT CMSCENARIO.CLID,CMSCENARIO.CSID,CINSTANCES.CTID "
                    + "FROM CINSTANCES,CMSCENARIO "
                    + "WHERE CMSCENARIO.CIID=CINSTANCES.CID AND CMSCENARIO.CTID<>CINSTANCES.CTID ";

                SqlCommand command = bmcRestoreBroker.GetSqlCommand(cmdText);
                System.Data.DataTable myTable = new System.Data.DataTable();
                myTable.Columns.Add("CLID", typeof(Guid));
                myTable.Columns.Add("CSID", typeof(Guid));
                myTable.Columns.Add("CTID", typeof(Guid));

                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                dataAdapter.Fill(myTable);
                foreach (System.Data.DataRow row in myTable.Rows)
                {
                    Guid foundClid = (Guid)row["CLID"];
                    Guid foundCsid = (Guid)row["CSID"];
                    onLogMigrationInformation(new UpdateMessageEventArgs("FixCmScenarios: " + "CLID:" + foundClid.ToString() + ", CSID:" + foundCsid.ToString()));

                    LogicalModuleBase lmb = bmcRestoreBroker.GetLogicalCM(foundClid) as LogicalModuleBase;

                    LogicalModuleBase.CMScenarios scenarios = lmb.Scenarios as LogicalModuleBase.CMScenarios;
                    CMScenario corruptedScenario = scenarios[foundCsid];
                    corruptedScenario.CMTypeID = (Guid)row["CTID"];
                    changesMade = true;
                    lmb.CalculateToken(true);
                }
                bmcRestoreBroker.SetComplete(changesMade);
            }
            catch (Exception ex)
            {
                bmcRestoreBroker.SetAbort();
                throw ex;
            }
        }

        #endregion
    }
}
