using System;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Data;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.InstallInfo;
using Oritax.TaxSimp.Services.CMBroker;
using System.Text;
using System.Xml.Serialization;

namespace Oritax.TaxSimp.Administration.SystemUpdate
{
	/// <summary>
	/// Summary description for AssemblyInstallProcess.
	/// </summary>
	public class DeploymentUnitInstallJob : IDisposable
	{
		const string MANIFEST_FILE_NAME="manifest.xml";
        public const string ServicesPath = @"C:\NableServices\";

		public enum StatusOption
		{
			NOT_STARTED		=	0,
			UNPACKED		=	1,
			INSTALLED		=	2
		}

		public enum ElementTypeOption
		{
			NONVERSIONEDASSEMBLY	=	0,
			ASSEMBLY				=	1,
			SUPPORT					=	2,
			MONITOR					=	3,
			HELP					=	4,
			COM						=	5,
            SERVICE                 =   6
		}

		/// <summary>
		/// Enum that represents the install state of a particular DU.
		/// </summary>
		public enum InstallStateOption
		{
			UNDETERMINED					=	0,		// The install state has not been determined or cannot be determined.
			INSTALLED_OK					=	1,		// This DU is installed on the system and is complete
			INSTALLED_NOTOK					=	2,		// This DU is installed on the system but needs repair
			// The following 4 options imply that the DU is not installed at the requested version.
			// (Let the requested version be a.b.c.d)
			INSTALLED_EARLIER_REVISION		=	3,		// One installation exists at the version a.b.c.x where x < d.
			INSTALLED_EARLIER_DATAMODEL		=	4,		// One installation exists at the version a.b.x.y where x < c.
			INSTALLED_EARLIER_MINORVERSION	=	5,		// One installation exists at the version a.x.y.z where x < b.
			INSTALLED_EARLIER_MAJORVERSION	=	6,		// One installation exists at the version x.y.z.w where x < a.
			NOT_INSTALLED					=	7		// No installation exists.
		}

		public class DeploymentUnitInstallJobException : Exception
		{
			public DeploymentUnitInstallJobException(string message)
				: base(message){}
		}

		[Serializable]
		public class Element
		{
			public ElementTypeOption	ElementType;
			public string				TagName;
			public string				DllFile;
			public Guid					TypeID;
			public Assembly				ElementAssembly;
			public InstallStateOption	InstallState=InstallStateOption.UNDETERMINED;
			public String				Location;
			public string				ProjectName;
			public string				ChmFile;
			public string				XmlFile;

			public Element(ElementTypeOption elementType,string tagName,string dllFile,Assembly elementAssembly,Guid typeID)
			{
				ElementType=elementType;
				TagName=tagName;
				DllFile=dllFile;
				ElementAssembly=elementAssembly;
				TypeID=typeID;
				Location=null;
			}

			public Element(string file,string location)
			{
				ElementType=ElementTypeOption.SUPPORT;
				TagName="supportfile";
				DllFile=file;
				ElementAssembly=null;
				TypeID=Guid.Empty;
				Location=location;
			}

			public Element(string project, string chmFile, string xmlFile)
			{
				ElementType = ElementTypeOption.HELP;
				TagName="helpFile";
				TypeID = Guid.Empty;
				ElementAssembly = null;
				ProjectName = project;
				ChmFile = chmFile;
				XmlFile = xmlFile;
			}
		}

		public class Elements : ArrayList, IEnumerable, IEnumerator
		{
			private int index;

			public new Element this[int index]
			{
				get{return (Element)base[index];}
			}

			public void Add(Element element)
			{
				base.Add(element);
			}

			#region IEnumerable Members

			public override IEnumerator GetEnumerator()
			{
				index=-1;
				return (IEnumerator)this;
			}

			#endregion

			#region IEnumerator Members

			public void Reset()
			{
				index=-1;
			}

			public object Current
			{
				get
				{
					if(index<0 || index>=base.Count) throw new InvalidOperationException("Enumerator is out of range");
					return (Element)base[index];
				}
			}

			public bool MoveNext()
			{
				if(index>=base.Count) throw new InvalidOperationException("Enumerator is out of range");
				index++;
				return index<base.Count;
			}

			#endregion
		}

		[Serializable]
		public class NAbleAssemblyName
		{
			public class AssemblyNameException : Exception
			{
				public AssemblyNameException(string message):base(message){}
			}

			public string BaseName;
			public int MajorVersion;
			public int MinorVersion;
			public string Name;
			public string VersionedName;

			public NAbleAssemblyName(string name)
			{
                // first remove any path components of the name
                //int lastBackslashIndex=name.LastIndexOf('\\');
                //if(-1!=lastBackslashIndex)
                //{
                //    name=name.Substring(lastBackslashIndex+1);
                //}

                //Name=name;
                //int firstUnderScoreIndex=name.IndexOf('_');
                //if(-1==firstUnderScoreIndex || 0==firstUnderScoreIndex)
                //    throw new AssemblyNameException("Invalid NAble Assembly name in constructor");

                //int secondUnderScoreIndex=name.IndexOf('_',firstUnderScoreIndex+1);
                //if(-1==secondUnderScoreIndex || 0==secondUnderScoreIndex || secondUnderScoreIndex-firstUnderScoreIndex<2)
                //    throw new AssemblyNameException("Invalid NAble Assembly name in constructor");

                //int dotIndex=name.IndexOf('.',secondUnderScoreIndex+1);

                //if(-1==dotIndex)
                //    dotIndex=name.Length;

                //if(0==dotIndex || dotIndex-secondUnderScoreIndex<2)
                //    throw new AssemblyNameException("Invalid NAble Assembly name in constructor");
				

                //BaseName=name.Substring(0,firstUnderScoreIndex);
                //try
                //{
                //    MajorVersion=int.Parse(name.Substring(firstUnderScoreIndex+1,secondUnderScoreIndex-firstUnderScoreIndex-1));
                //    MinorVersion=int.Parse(name.Substring(secondUnderScoreIndex+1,dotIndex-secondUnderScoreIndex-1));
                //}
                //catch
                //{
                //    throw new AssemblyNameException("Invalid NAble Assembly name in constructor");
                //}

				VersionedName=Name;
			}
		}

		private InstallBroker broker;
		private FileManagerProxy fileManager;
		private string baseTempFolder;
		private string tempFolder;
		private string folderName;
		private string archiveFolder;
		private string backupFolder;
		private string baseCabFolder;
		private string cabFileName;
		private string name;
		private string targetHelpFolder;
		private string targetWebFolder;
		private string targetBinFolder;
		private string targetClientBinFolder;
		private StatusOption status;
		private Elements elements=new Elements();

		public string CabFilePathName{get{return baseCabFolder+@"\"+cabFileName;}}
		public string CabFileName{get{return cabFileName;}}
		public string Name{get{return name;}}
		public string TempFolder{get{return tempFolder;}}
		public string BaseTempFolder{get{return baseTempFolder;}}
		public StatusOption Status{get{return status;}}
		public InstallBroker Broker{get{return broker;}set{broker=value;}}
		public FileManagerProxy FileManager{get{return fileManager;}set{fileManager=value;}}
		public Element[] InstallElements
		{
			get
			{
				return (Element[]) this.elements.ToArray(typeof(Element));
			}
		}

		public bool HasBrokerAssembly()
		{
			foreach(Element element in this.elements)
			{
				NAbleAssemblyName nAbleAssemblyName=new NAbleAssemblyName(element.DllFile);
				if(nAbleAssemblyName.BaseName=="CMBroker")
				{
					return true;
				}
			}

			return false;
		}

		public string GetBrokerAssemblyName()
		{
			foreach(Element element in this.elements)
			{
				NAbleAssemblyName nAbleAssemblyName=new NAbleAssemblyName(element.DllFile);
				if(nAbleAssemblyName.BaseName=="CMBroker")
				{
					return element.DllFile;
				}
			}

			throw new Exception("Install job does not contain a broker.");
		}

        private string GetInstallFolder(string fileName)
        {
            if (SystemUpdate.FileManager.IsSilverlightControl(fileName))
                return this.targetClientBinFolder;
            else
                return this.targetBinFolder;
        }



		public DeploymentUnitInstallJob(
			InstallBroker broker,	// The transaction broker owning a transaction in which the job resides.
			string cabPathName,		// Full path name to the CAB file that is to be installed by this job.
			string baseTempFolder,	// Full path name to the base temporary working folder.
			string targetHelpFolder,// The target folder in the installation that is to contain the deployed help files
			string targetWebFolder	// The target web folder
			)
		{
			string cabFileName;
			string baseCabFolder;

			int startIndex=cabPathName.LastIndexOf(@"\");
			if(-1!=startIndex)
			{
				cabFileName=cabPathName.Substring(startIndex+1);
				baseCabFolder=cabPathName.Substring(0,startIndex);
			}
			else
				throw new DeploymentUnitInstallJobException(@"Invalid cab path name - does not include \");

			initialize(broker,cabFileName,baseCabFolder,baseTempFolder,targetHelpFolder,targetWebFolder);
		}

//		public DeploymentUnitInstallJob(
//			InstallBroker broker,	// The transaction broker owning a transaction in which the job resides.
//			string cabFileName,		// The name of the cab file to be installed
//			string baseCabFolder,	// Full path name to the folder in which the cab file resides
//			string baseTempFolder,	// Full path name to the base temporary working folder.
//			string targetHelpFolder,// The target folder in the installation that is to contain the deployed help files
//			string targetWebFolder	// The target web folder
//			)
//		{
//			Initialize(broker,cabFileName,baseCabFolder,baseTempFolder,targetHelpFolder,targetWebFolder);
//		}

		private void initialize(InstallBroker broker,string cabFileName,string baseCabFolder,string baseTempFolder,string targetHelpFolder,string targetWebFolder)
		{
			this.broker=broker;
			this.cabFileName=cabFileName;
			this.baseCabFolder=baseCabFolder;
			this.baseTempFolder=baseTempFolder;
			int index=cabFileName.IndexOf(@".");
			this.folderName=cabFileName.Substring(0,index);
			this.tempFolder=baseTempFolder+@"\"+folderName;
			this.archiveFolder=baseTempFolder+@"\Archive";
			this.backupFolder=baseTempFolder+@"\Backup";
			this.targetHelpFolder=targetHelpFolder;
			this.targetWebFolder=targetWebFolder;
			this.targetBinFolder=targetWebFolder+@"\bin";
			this.targetClientBinFolder=targetWebFolder+@"\ClientBin";
			this.name = Path.ChangeExtension(cabFileName, null);

			status=StatusOption.NOT_STARTED;

			if(!Directory.Exists(tempFolder))
				Directory.CreateDirectory(tempFolder);
			if(!Directory.Exists(archiveFolder))
				Directory.CreateDirectory(archiveFolder);
			if(!Directory.Exists(backupFolder))
				Directory.CreateDirectory(backupFolder);
		}

		#region IDisposable Members

		public void Dispose()
		{
			if(Directory.Exists(tempFolder))
				Directory.Delete(tempFolder,true);

			GC.SuppressFinalize(this);
		}

		#endregion

		public void Unpack()
		{
			string processStdOut;


			// Start a process to expand the cabinet file
			RunProcess(this.BaseTempFolder+@"\cabarc.exe",this.TempFolder,new string[]{"-p","-o","X",this.CabFilePathName},out processStdOut, true);

			if ( !File.Exists(this.TempFolder+@"\"+MANIFEST_FILE_NAME))
				throw new Exception( "DeploymentUnitInstallJob.Unpack, manifest file does not exist: "+this.TempFolder+@"\"+MANIFEST_FILE_NAME);

			Stream manifestStream=new FileStream(tempFolder+@"\"+MANIFEST_FILE_NAME,FileMode.Open,FileAccess.Read);

			this.elements.Clear();

			try
			{
				XmlReader xmlReader=new XmlTextReader(manifestStream);

				while(xmlReader.Read())
				{
					if(xmlReader.NodeType==XmlNodeType.Element)
					{
						switch( xmlReader.Name.ToLower() )
						{
							case "assembly":
								string assemblydllname=xmlReader.GetAttribute("dll");

								if(null!=assemblydllname)
								{
									string dllSourcePathName=this.TempFolder+@"\"+assemblydllname;

									this.elements.Add(new Element(ElementTypeOption.ASSEMBLY,"assembly",assemblydllname,null,Guid.Empty));
								}
								break;

							case "nonversionedassembly":
								string nonVersionedAssemblyDllName=xmlReader.GetAttribute("dll");

								if(null!=nonVersionedAssemblyDllName)
								{
									string dllSourcePathName=this.TempFolder+@"\"+nonVersionedAssemblyDllName;

									this.elements.Add(new Element(ElementTypeOption.NONVERSIONEDASSEMBLY,"nonversionedassembly",nonVersionedAssemblyDllName,null,Guid.Empty));
								}
								break;

							case "supportfile":
								string supportfileName=xmlReader.GetAttribute("file");
								string supportfileLocation=xmlReader.GetAttribute("location");

								if(null!=supportfileName)
								{
									string supportfilePathName=this.TempFolder+@"\"+supportfileName;

									this.elements.Add(new Element(supportfileName,supportfileLocation));
								}
								break;

							case "monitors":
								processMonitors(xmlReader);
								break;

							case "help":
								string project=xmlReader.GetAttribute("project");
								string chmFile=xmlReader.GetAttribute("chmFile");
								string xmlFile=xmlReader.GetAttribute("xmlFile");

								if (project != null)
								{
									elements.Add(new Element(project, chmFile, xmlFile));
								}

								break;
                            case "service":
                                string serviceName = xmlReader.GetAttribute("exe");

                                if (serviceName != null)
                                {
                                    string dllSourcePathName = this.TempFolder + @"\" + serviceName;

                                    this.elements.Add(new Element(ElementTypeOption.SERVICE, "service", serviceName, null, Guid.Empty));
                                }
                                break;
							case "com":
								break;
						}
					}
				}

			}
			catch
			{
				throw;
			}
			finally
			{
				manifestStream.Close();
			}

			status=StatusOption.UNPACKED;
		}

        private void processMonitors(XmlReader xmlReader)
        {
            string strCategoryName = xmlReader.GetAttribute("category");
            string strCategoryHelp = xmlReader.GetAttribute("categoryhelp");
            string strMonitors = xmlReader.ReadInnerXml();

            CounterCreationDataCollection objCCDCollection = null;

            XmlParserContext objXMLParser = new XmlParserContext(null, null, null, XmlSpace.None);
            XmlReader objXMLReader = new XmlTextReader(strMonitors, XmlNodeType.Element, objXMLParser);
            Console.Write("Installing Monitors...");

            objCCDCollection = new CounterCreationDataCollection();

            while (objXMLReader.Read())
            {
                if (objXMLReader.NodeType == XmlNodeType.Element && objXMLReader.Name.ToLower() == "monitor")
                {
                    string strMonitorName = objXMLReader.GetAttribute("name");
                    string strMonitorHelp = objXMLReader.GetAttribute("help");
                    string strMonitorType = objXMLReader.GetAttribute("type");

                    CounterCreationData objCCData = new CounterCreationData();

                    objCCData.CounterName = strMonitorName;
                    objCCData.CounterHelp = strMonitorHelp;
                    objCCData.CounterType = (PerformanceCounterType)PerformanceCounterType.Parse(typeof(PerformanceCounterType), strMonitorType);

                    objCCDCollection.Add(objCCData);
                }
            }

            string[] instanceNames = null;

            // Locate any existing category, and add the existing counters.
            if (PerformanceCounterCategory.Exists(strCategoryName))
            {
                PerformanceCounterCategory category = new PerformanceCounterCategory(strCategoryName);
                instanceNames = category.GetInstanceNames();

                foreach (string instanceName in instanceNames)
                {
                    PerformanceCounter[] counters = category.GetCounters(instanceName);

                    foreach (PerformanceCounter counter in counters)
                    {
                        if(!this.ContainsCounterData(counter.CounterName, objCCDCollection))
                        {
                            CounterCreationData ccData = new CounterCreationData();
                            ccData.CounterName = counter.CounterName;
                            ccData.CounterHelp = counter.CounterHelp;
                            ccData.CounterType = counter.CounterType;

                            objCCDCollection.Add(ccData);
                        }
                    }
                }
            }

            if (PerformanceCounterCategory.Exists(strCategoryName))
                PerformanceCounterCategory.Delete(strCategoryName);

            PerformanceCounterCategory newCategory = PerformanceCounterCategory.Create(strCategoryName, strCategoryHelp, PerformanceCounterCategoryType.MultiInstance, objCCDCollection);

            // put our new instance in the list
            if (instanceNames == null)
                instanceNames = new string[0];

            List<string> instanceNameList = new List<string>(instanceNames);
            instanceNameList.Add(this.GetInstanceName());
            instanceNames = instanceNameList.ToArray();

            // go through each instance and make sure the instance and counter exist for that instance
            foreach (string instanceName in instanceNames)
            {
                foreach (CounterCreationData ccData in objCCDCollection)
                {
                    PerformanceCounter counter = new PerformanceCounter(newCategory.CategoryName, ccData.CounterName, instanceName, false);
                    counter.Increment();
                }
            }

            // Delete the legacy counters if they exist
            if (PerformanceCounterCategory.Exists("TX360 Enterprise"))
                PerformanceCounterCategory.Delete("TX360 Enterprise");
        }

        private string GetInstanceName()
        {
            // get the last part of the directory which should be the install name
            // of the application
            string[] directoryParts = this.targetWebFolder.Split('\\');

            System.Text.StringBuilder instanceName = new System.Text.StringBuilder();

            if (directoryParts[directoryParts.Length - 1] != "\\")
                instanceName.Append(directoryParts[directoryParts.Length - 1]);
            else
                instanceName.Append(directoryParts[directoryParts.Length - 2]);

            // Remove any counter instance name invalid characters
            instanceName = instanceName.Replace("(", "");
            instanceName = instanceName.Replace(")", "");
            instanceName = instanceName.Replace("#", "");
            instanceName = instanceName.Replace("/", "");
            instanceName = instanceName.Replace("\\", "");

            return instanceName.ToString();
        }

        private bool ContainsCounterData(string counterName, CounterCreationDataCollection counterDataCollection)
        {
            foreach (CounterCreationData counterData in counterDataCollection)
            {
                if (counterData.CounterName == counterName)
                    return true;
            }

            return false;
        }

		/// <summary>
		/// Determines the Audit Sate of each DU represented by this job.
		/// </summary>
		/// <returns>The install state enum.</returns>
		public void AuditInstallStates()
		{
			foreach(Element element in this.elements)
			{
				switch(element.ElementType)
				{
					case ElementTypeOption.ASSEMBLY:
                        if (element.DllFile.Contains("ExcelReader") == false)
                        {
                            IAssembly newAssembly = this.fileManager.ReadNAbleAssembly(this.tempFolder + @"\" + element.DllFile);
                            element.TypeID = newAssembly.ID;
                            element.InstallState = this.fileManager.CheckAssemblyInstallFilesByName(this.targetBinFolder, newAssembly);
                            //element.InstallState = this.fileManager.CheckAssemblyInstallFilesByName(GetInstallFolder(element.DllFile), newAssembly);
                        }
                        else
                        {
                            element.InstallState = InstallStateOption.INSTALLED_OK;
                        }
					break;
				}
			}
		}

		public void InstanceBackup()
		{
			foreach(Element element in this.elements)
			{
				if(element.InstallState==InstallStateOption.INSTALLED_EARLIER_DATAMODEL)
				{
					// Process each of the instances of a Component Version represented by
					// the assembly.

					string componentVersionBackupFolder=this.backupFolder+@"\"+element.TypeID.ToString();
                    
                    if(!Directory.Exists(componentVersionBackupFolder))
						Directory.CreateDirectory(componentVersionBackupFolder);

					Guid[] bMCIDs=this.broker.GetBMCIDList(element.TypeID);

					foreach(Guid bMCID in bMCIDs)
					{
						string fileName = Path.Combine(componentVersionBackupFolder, bMCID.ToString());
						broker.ExportXMLToFile(bMCID, fileName);
						broker.DeleteCMInstance(bMCID);
					}
				}
			}
		}

		private InstallStateOption CheckAssemblyInstallFilesByName(IAssembly newAssembly)
		{
			// Cases:
			// (1) File exists with the same name but with different DataModel and/or revision attribute values.
			// Determine if an assembly with the same name exists in the installation as a file.
			// (2) File exists with the same base name but with different version suffix and different major
			// and/or minor versions in the version attributes.
			// (All file names are of the form AAAAAAAAA_X_Y.BBB where AAAAAAAAA is the base name X is the major version integer, Y is
			// the minor version integer and BBB is the file extension. The base name and suffix fields may be any length and the
			// format is not relevant to the operation of the install and version and update mechanism.

			// Get the assembly versions that have the same name as the new assembly each assembly will be represented by a dll file
			// with a name of the above format.

			NAbleAssemblyName newNAbleAssemblyName=new NAbleAssemblyName(newAssembly.Name);

			foreach(string installedFileName in Directory.GetFiles(this.targetBinFolder,newNAbleAssemblyName.BaseName+@"_*"))
			{
				NAbleAssemblyName installedNAbleAssemblyName=new NAbleAssemblyName(installedFileName);
				string archivePathName=this.archiveFolder+@"\"+installedNAbleAssemblyName.Name;

				File.Copy(installedFileName,archivePathName,true);

				Assembly installedAssembly=Assembly.LoadFile(archivePathName);
				AssemblyDictionary.AssemblyVersionNumber installedVersionNumber=ExtractVersionFromAssembly(installedAssembly);

				if(installedVersionNumber.MajorVersion!=installedNAbleAssemblyName.MajorVersion
					|| installedVersionNumber.MinorVersion!=installedNAbleAssemblyName.MinorVersion)
					throw new DeploymentUnitInstallJobException("An installed dll file name: "+installedFileName+" does not match it's internal version attributes");

				AssemblyDictionary.AssemblyVersionNumber newVersionNumber=(AssemblyDictionary.AssemblyVersionNumber)newAssembly.VersionNumber;

				AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption differenceColumn=
					(installedVersionNumber<newVersionNumber).DifferenceColumn;

				switch(differenceColumn)
				{
					case AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption.NONE:
						return InstallStateOption.INSTALLED_OK;
					case AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption.MAJORVERSION:
						return InstallStateOption.INSTALLED_EARLIER_MAJORVERSION;
					case AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption.MINORVERSION:
						return InstallStateOption.INSTALLED_EARLIER_MINORVERSION;
					case AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption.DATAFORMAT:
						return InstallStateOption.INSTALLED_EARLIER_DATAMODEL;
					case AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption.REVISION:
						return InstallStateOption.INSTALLED_EARLIER_REVISION;
					default:
						return InstallStateOption.UNDETERMINED;
				}
			}

			return InstallStateOption.NOT_INSTALLED;
		}

		AssemblyDictionary.AssemblyVersionNumber ExtractVersionFromAssembly(Assembly assembly)
		{
			int majorVersion,minorVersion,dataFormat,revision;

			AssemblyName installedAssemblyName=assembly.GetName();
			Version version=installedAssemblyName.Version;

			majorVersion=version.Major;
			minorVersion=version.Minor;

//			if(majorVersion!=installedNAbleAssemblyName.MajorVersion
//				|| minorVersion!=installedNAbleAssemblyName.MinorVersion)
//				throw new DeploymentUnitInstallJobException("An installed dll file: "+installedFileName+" does not match it's internal version attributes");

			object[] assemblyInformationalVersionAttributes=assembly.GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute),false);
			if(null==assemblyInformationalVersionAttributes || assemblyInformationalVersionAttributes.Length!=1)
				throw new DeploymentUnitInstallJobException("An installed dll file: does not have informational version attribute.");

			AssemblyInformationalVersionAttribute assemblyInformationalVersionAttribute=(AssemblyInformationalVersionAttribute)assemblyInformationalVersionAttributes[0];
			Version informationalVersion=new Version(assemblyInformationalVersionAttribute.InformationalVersion);
			dataFormat=informationalVersion.Build;
			revision=informationalVersion.Revision;
			return new AssemblyDictionary.AssemblyVersionNumber(majorVersion,minorVersion,dataFormat,revision);
		}

		private InstallStateOption CheckAssemblyInstallByName(IAssemblyManagement assemblyDictionary,IAssembly newAssembly)
		{
			// Get the assembly versions that have the same name as the new assembly
			IAssemblyVersionSet assemblyVersions=assemblyDictionary.GetAssemblyVersions(newAssembly.Name);
			if(assemblyVersions!=null)
			{
				foreach(IAssembly existingAssembly in assemblyVersions)
				{
					AssemblyDictionary.AssemblyVersionNumber newVersionNumber=(AssemblyDictionary.AssemblyVersionNumber)newAssembly.VersionNumber;
					AssemblyDictionary.AssemblyVersionNumber existingVersionNumber=(AssemblyDictionary.AssemblyVersionNumber)existingAssembly.VersionNumber;
					AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption differenceColumn=
						(existingVersionNumber<newVersionNumber).DifferenceColumn;

					switch(differenceColumn)
					{
						case AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption.NONE:
							return InstallStateOption.INSTALLED_OK;
						case AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption.MAJORVERSION:
							return InstallStateOption.INSTALLED_EARLIER_MAJORVERSION;
						case AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption.MINORVERSION:
							return InstallStateOption.INSTALLED_EARLIER_MINORVERSION;
						case AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption.DATAFORMAT:
							return InstallStateOption.INSTALLED_EARLIER_DATAMODEL;
						case AssemblyDictionary.AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption.REVISION:
							return InstallStateOption.INSTALLED_EARLIER_REVISION;
						default:
							return InstallStateOption.UNDETERMINED;
					}
				}
			}

			return InstallStateOption.NOT_INSTALLED;
		}

		/// <summary>
		/// Installs the files associated with each element.
		/// </summary>
		public void InstallFiles()
		{
			foreach(Element element in this.elements)
			{
                string installFolder = GetInstallFolder(element.DllFile);
				switch(element.ElementType)
				{
					case ElementTypeOption.ASSEMBLY:
                        SystemUpdate.FileManager.SetDirectoryFilesToNormal(new DirectoryInfo(TempFolder));
                        SystemUpdate.FileManager.SetDirectoryFilesToNormal(new DirectoryInfo(installFolder));
                        SystemUpdate.FileManager.SetDirectoryFilesToNormal(new DirectoryInfo(targetWebFolder));
                        this.fileManager.InstallFile(installFolder, this.TempFolder, element.DllFile);
                        if (!SystemUpdate.FileManager.IsSilverlightControl(element.DllFile))
                        {
                            this.fileManager.InstallAssociatedFiles(this.targetWebFolder, this.TempFolder, element.DllFile);
                            this.fileManager.RegisterComFiles(this.targetWebFolder, this.TempFolder, element.DllFile);
                        }
						break;
					case ElementTypeOption.NONVERSIONEDASSEMBLY:
                        SystemUpdate.FileManager.SetDirectoryFilesToNormal(new DirectoryInfo(TempFolder));
                        SystemUpdate.FileManager.SetDirectoryFilesToNormal(new DirectoryInfo(installFolder));
                        this.fileManager.InstallFile(installFolder, this.TempFolder, element.DllFile);
						break;
					case ElementTypeOption.SUPPORT:
                        SystemUpdate.FileManager.SetDirectoryFilesToNormal(new DirectoryInfo(TempFolder));
                        this.fileManager.InstallFile(this.targetWebFolder + @"\" + element.Location, this.TempFolder, element.DllFile);
						break;
					case ElementTypeOption.MONITOR:
						break;
				    case ElementTypeOption.SERVICE:
                        
                        if (!Directory.Exists(ServicesPath))
                            Directory.CreateDirectory(ServicesPath);

                        this.fileManager.InstallFile(ServicesPath, this.TempFolder, element.DllFile);
                        
                        break;
					case ElementTypeOption.COM:
						break;
				}
			}
		}

		public void InstallDataModel()
		{
			foreach(Element element in this.elements)
			{
				if(element.InstallState!=InstallStateOption.INSTALLED_OK)
				{
					switch(element.ElementType)
					{
						case ElementTypeOption.ASSEMBLY:

							// Perform Data Model Installations
                            string assemblyName = this.tempFolder + @"\" + element.DllFile;
							if(this.fileManager.IsAttributeDefined(assemblyName,typeof(DataModelInstallInfoAttribute)))
							{
								DataModelInstallInfoAttribute dataModelInstallInfoAttribute=(DataModelInstallInfoAttribute)this.fileManager.GetCustomAttribute(assemblyName,typeof(DataModelInstallInfoAttribute));
								string modelManagerAssemblyName=dataModelInstallInfoAttribute.ModelManagerAssembly;
								string modelManagerClass=dataModelInstallInfoAttribute.ModelManagerClass;
								string[] assemblyInfo = modelManagerAssemblyName.Split(',');
								broker.EstablishDatabase(Path.ChangeExtension(Path.Combine(this.targetBinFolder, assemblyInfo[0]), ".dll") ,modelManagerClass);
							}
							break;
					}
				}
			}
		}

		public void RemoveOldDataModel()
		{
			foreach(Element element in this.elements)
			{
				if(element.InstallState==InstallStateOption.INSTALLED_EARLIER_DATAMODEL)
				{
					switch(element.ElementType)
					{
						case ElementTypeOption.ASSEMBLY:

							string dllFileName = this.tempFolder+@"\"+element.DllFile;

							// Check that the file still exists before trying to load it
							if(File.Exists(dllFileName))
							{
								// Perform Data Model Installations
								if(this.fileManager.IsAttributeDefined(dllFileName,typeof(DataModelInstallInfoAttribute)))
								{
									DataModelInstallInfoAttribute dataModelInstallInfoAttribute=(DataModelInstallInfoAttribute)this.fileManager.GetCustomAttribute(dllFileName,typeof(DataModelInstallInfoAttribute));
									string modelManagerAssemblyName=dataModelInstallInfoAttribute.ModelManagerAssembly;
									string modelManagerClass=dataModelInstallInfoAttribute.ModelManagerClass;
									string[] assemblyInfo = modelManagerAssemblyName.Split(',');
									broker.DropOldDatabaseTables(Path.ChangeExtension(Path.Combine(this.targetBinFolder, assemblyInfo[0]), ".dll") ,modelManagerClass);
								}
							}
							break;
					}
				}
			}
		}

        public void InstallData(ArrayList workpapers, ArrayList consolWorkpapers)
		{
			foreach(Element element in this.elements)
			{
                if ((element.InstallState != InstallStateOption.INSTALLED_OK) && !SystemUpdate.FileManager.IsSilverlightControl(element.DllFile))
				{
					switch(element.ElementType)
					{
						case ElementTypeOption.ASSEMBLY:
							// Perform the installation of the assembly into the Assembly and Component managers as required
							// - First determine if the assembly is a component version or not
							//   (All component version assemblies have the ComponentVersionInstallInfoAttribute defined.)
                            string assemblyName = this.tempFolder+@"\"+element.DllFile;
                            if (this.FileManager.IsAttributeDefined(assemblyName, typeof(ComponentVersionInstallInfoAttribute)))
							{
								IComponentVersion componentVersion=this.FileManager.ReadComponentVersion(assemblyName);
								IComponent component=this.FileManager.ReadComponent(assemblyName);

								if(element.InstallState==InstallStateOption.INSTALLED_EARLIER_DATAMODEL
									|| element.InstallState==InstallStateOption.INSTALLED_EARLIER_REVISION)
								{
									broker.ReplaceComponent(component);
									broker.ReplaceComponentVersion(component.Name,componentVersion);
								}
								else
								{
									// If the component does not exist, it needs to be added before the new version can be added.
									if(!broker.ComponentExists(componentVersion.ComponentID))
									{
										broker.InstallComponent(component);
									}

									// If the component version already exists, replace it, otherwise install it
									if(!broker.ComponentVersionExists(componentVersion.ID))
									{
										broker.InstallComponentVersion(component.Name,componentVersion);
									}
									else
										broker.ReplaceComponentVersion(component.Name,componentVersion);
								}
                                if (!SkipAssemblyNoWorkpapers(assemblyName))
                                {
                                    IEnumerable compWorkpapers = this.FileManager.GetComponentWorkpapers(assemblyName);
                                    if (compWorkpapers != null && componentVersion != null)
                                        AddWithID(workpapers, consolWorkpapers, componentVersion.ID, compWorkpapers);
                                }
							}
							else
							{
								IAssembly nAbleAssembly=this.FileManager.ReadNAbleAssembly(assemblyName);

								if(element.InstallState==InstallStateOption.INSTALLED_EARLIER_DATAMODEL
									|| element.InstallState==InstallStateOption.INSTALLED_EARLIER_REVISION)
								{
									broker.ReplaceAssembly(nAbleAssembly);
								}
								else
								{
									if(!broker.AssemblyExists(nAbleAssembly.ID))
									{
										broker.InstallAssembly(nAbleAssembly);
									}
									else
									{
										broker.ReplaceAssembly(nAbleAssembly);
									}
								}
							}
							break;
					}
				}
			}
		}
        public static void AddWithID(ArrayList normalWokpapers, ArrayList consolWorkpapers, Guid id, IEnumerable compWorkpapers)
        {
            if (compWorkpapers != null)
            {
                if (compWorkpapers is Hashtable)
                {
                    Hashtable ht = (Hashtable)compWorkpapers;
                    foreach (object key in ht.Keys)
                    {
                        if (key is WorkPaperType)
                        {
                            if ((WorkPaperType)key == WorkPaperType.Supporting)
                                AddListWithID(normalWokpapers, id, (IEnumerable)ht[key]);
                            else if ((WorkPaperType)key == WorkPaperType.Consolidation)
                                AddListWithID(consolWorkpapers, id, (IEnumerable)ht[key]);
                        }
                    }
                }
                 else 
                    AddListWithID(normalWokpapers, id, compWorkpapers);
            }
        }
        public static void AddListWithID(ArrayList workpapers, Guid id, IEnumerable compWorkpapers)
        {
            if (compWorkpapers != null)
            {
                foreach (WorkPaper.WorkPaperData wpData in compWorkpapers)
                {
                    wpData.AssemblyID = id;
                    workpapers.Add(wpData);
                }
            }
        }
        public static void SerialiseWorkPapersList(ArrayList workpapers, string fileName)
        {
            if (workpapers != null && workpapers.Count > 0)
            {
                if (File.Exists(fileName))
                    File.Delete(fileName);
                XmlSerializer s = new XmlSerializer(typeof(ArrayList), new Type[] { typeof(WorkPaper.WorkPaperData), typeof(WorkPaper.WorkPaperView), typeof(WorkPaper.Argument),
                    typeof(WorkPaper.Argument[]) , typeof(WorkPaper.WorkPaperView[])});
                TextWriter w = new StreamWriter(fileName);
                s.Serialize(w, workpapers);
                w.Close();
            }
        }
        private bool SkipAssemblyNoWorkpapers(string name)
        {
            if (name.EndsWith("AustralianEntity_1_1.dll") || name.EndsWith("OrganizationUnit_1_1.dll")
                || name.EndsWith("TaxTopicBase_1_1.dll") || name.EndsWith("WorkBookBase_1_1.dll"))
                return true;
            else
                return false;
        }        
        public ArrayList InstanceRestore(SaveDueToMemory saveDelegate)
		{
			ArrayList restoreInstanceGuids = new ArrayList();
			foreach(Element element in this.elements)
			{
				if(element.InstallState==InstallStateOption.INSTALLED_EARLIER_DATAMODEL)
				{
					string componentVersionBackupFolder=this.backupFolder+@"\"+element.TypeID.ToString();
					string[] backupFileNames=Directory.GetFiles(componentVersionBackupFolder);

					foreach(string backupPathName in backupFileNames)
					{
						int lastSlashIndex=backupPathName.LastIndexOf(@"\");
						string backupFileName=backupPathName.Substring(lastSlashIndex+1);
						Guid instanceID=new Guid(backupFileName);
						restoreInstanceGuids.Add(instanceID);
						broker.DeliverXMLFromFile(instanceID,backupPathName);
						saveDelegate();
						
					}
				}
			}

			return restoreInstanceGuids;
		}

		public void Resubscribe()
		{
			foreach(Element element in this.elements)
			{
				if(element.InstallState==InstallStateOption.INSTALLED_EARLIER_DATAMODEL || element.InstallState==InstallStateOption.INSTALLED_EARLIER_REVISION)
				{
					Guid[] bMCIDs=this.broker.GetBMCIDList(element.TypeID);

					foreach(Guid bMCID in bMCIDs)
					{
						broker.Resubscribe(bMCID);	
					}
				}
			}
		}
		
		 
		/// <summary>
		/// Runs a process under controlled conditions so that the process is not allowed
		/// to hang.
		/// </summary>
		/// <param name="executable">The pathname of the process to run.</param>
		/// <param name="workingDirectory">The working directory assigned to the process.</param>
		/// <param name="arguments">The command line parameters to be attached.</param>
		/// <param name="output">The captured standard output of the process.</param>
		/// <returns>Exit code.</returns>
		public static int RunProcess(string executable,string workingDirectory,string[] arguments,out string output, bool quoteArgs)
		{
			Process process=new Process();
			int exitCode=-1;
			try
			{
				process.StartInfo.FileName = '"' +executable +'"';

                StringBuilder argsString = new StringBuilder();

                foreach (string argument in arguments)
                {
                    argsString.Append(" ");

                    if (quoteArgs)
                        argsString.Append('"');

                    argsString.Append(argument);

                    if (quoteArgs)
                        argsString.Append('"');
                }

                process.StartInfo.Arguments = argsString.ToString();

				process.StartInfo.UseShellExecute=false;
				process.StartInfo.RedirectStandardOutput=true;
				process.StartInfo.WorkingDirectory = workingDirectory;
				process.StartInfo.CreateNoWindow = true;

				process.Start();
				output = process.StandardOutput.ReadToEnd();
				process.WaitForExit(3000);
				exitCode=process.ExitCode;

				if( exitCode != 0)
					throw new Exception( "Exception: DeploymentUnitInstallJob.Runprocess : Non-zero error code returned : " + output );
					
			}
			catch
			{
				    throw;
			}
			finally
			{
				if(!process.HasExited)
				{
					process.Kill();
				}
			}
			return exitCode;
		}

		private void PopulateNAbleAssembly(IAssembly nAbleAssembly,Assembly assembly)
		{
			int majorVersion;
			int minorVersion;
			int dataFormat;
			int revision;

//			ArrayList assemblyInstallInfoAttributes=GetCustomAttributes(assembly,typeof(AssemblyInstallInfoAttribute).ToString());
			object[] assemblyInstallInfoAttributes=assembly.GetCustomAttributes(typeof(AssemblyInstallInfoAttribute),false);
			if(null!=assemblyInstallInfoAttributes && assemblyInstallInfoAttributes.Length==1)
			{
				AssemblyInstallInfoAttribute assemblyInstallInfoAttribute=(AssemblyInstallInfoAttribute)(assemblyInstallInfoAttributes[0]);
				string ownerTypeIDStr=assemblyInstallInfoAttribute.AssemblyID;
				nAbleAssembly.ID=new Guid(ownerTypeIDStr);
				nAbleAssembly.Name=assemblyInstallInfoAttribute.AssemblyName;
				nAbleAssembly.DisplayName=assemblyInstallInfoAttribute.AssemblyDisplayName;
			}
			else
				throw new DeploymentUnitInstallJobException("AssemblyInstallInfo Attribute was not present on "+assembly.FullName+" file");

			AssemblyName assemblyName=assembly.GetName();
			Version version=assemblyName.Version;
			majorVersion=version.Major;
			minorVersion=version.Minor;

			object[] AssemblyInformationalVersionAttributes=assembly.GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute),false);
			if(null!=AssemblyInformationalVersionAttributes && AssemblyInformationalVersionAttributes.Length==1)
			{
				AssemblyInformationalVersionAttribute assemblyInformationalVersionAttribute=(AssemblyInformationalVersionAttribute)(AssemblyInformationalVersionAttributes[0]);
				Version infoVersion=new Version(assemblyInformationalVersionAttribute.InformationalVersion);
				dataFormat=infoVersion.Build;
				revision=infoVersion.Revision;
			}
			else
				throw new DeploymentUnitInstallJobException("AssemblyVersion Attribute was not present on "+assembly.FullName+" file");

			nAbleAssembly.VersionNumber=new AssemblyDictionary.AssemblyVersionNumber(majorVersion,minorVersion,dataFormat,revision);

			nAbleAssembly.StrongName=assembly.FullName;
		}

		private void PopulateComponent(IComponent component,Assembly assembly)
		{
//			ArrayList componentInstallInfoAttributes=GetCustomAttributes(assembly,typeof(ComponentInstallInfoAttribute).ToString());
			object[] componentInstallInfoAttributes=assembly.GetCustomAttributes(typeof(ComponentInstallInfoAttribute),false);
			if(null!=componentInstallInfoAttributes && componentInstallInfoAttributes.Length==1)
			{
				ComponentInstallInfoAttribute componentInstallInfoAttribute=(ComponentInstallInfoAttribute)(componentInstallInfoAttributes[0]);
				component.ID=new Guid(componentInstallInfoAttribute.ComponentID);
				component.Name=componentInstallInfoAttribute.ComponentName;
				component.DisplayName=componentInstallInfoAttribute.DisplayName;
				component.Category=componentInstallInfoAttribute.Category;
			}
			else
				throw new DeploymentUnitInstallJobException("ComponentInstallInfo Attribute was not present on "+assembly.FullName+" file");
		}

		private void PopulateComponentVersion(IComponentVersion componentVersion,Assembly assembly)
		{
			object[] componentVersionInstallInfoAttributes=assembly.GetCustomAttributes(typeof(ComponentVersionInstallInfoAttribute),false);
			if(null!=componentVersionInstallInfoAttributes && componentVersionInstallInfoAttributes.Length==1)
			{
				ComponentVersionInstallInfoAttribute componentVersionInstallInfoAttribute=(ComponentVersionInstallInfoAttribute)(componentVersionInstallInfoAttributes[0]);
				componentVersion.StartDate=DateTime.Parse(componentVersionInstallInfoAttribute.StartDate);
				componentVersion.EndDate=DateTime.Parse(componentVersionInstallInfoAttribute.EndDate);
				componentVersion.PersisterAssemblyStrongName=componentVersionInstallInfoAttribute.PersisterAssembly;
				componentVersion.PersisterClass=componentVersionInstallInfoAttribute.PersisterClass;
				componentVersion.ImplementationClass=componentVersionInstallInfoAttribute.ImplementationClass;
			}
			else
				throw new DeploymentUnitInstallJobException("ComponentVersionInstallInfo Attribute was not present on "+assembly.FullName+" file");

			object[] componentInstallInfoAttributes=assembly.GetCustomAttributes(typeof(ComponentInstallInfoAttribute),false);
			if(null!=componentInstallInfoAttributes && componentInstallInfoAttributes.Length==1)
			{
				ComponentInstallInfoAttribute componentInstallInfoAttribute=(ComponentInstallInfoAttribute)(componentInstallInfoAttributes[0]);
				componentVersion.ComponentID=new Guid(componentInstallInfoAttribute.ComponentID);
			}
			else
				throw new DeploymentUnitInstallJobException("ComponentInstallInfo Attribute was not present on "+assembly.FullName+" file");
		}

		ArrayList GetCustomAttributes(Assembly assembly,string typeName)
		{
			ArrayList customAttributes=new ArrayList();

			foreach(Attribute attribute in assembly.GetCustomAttributes(false))
			{
				string attributeTypeName=attribute.GetType().ToString();
				if(attributeTypeName==typeName)
					customAttributes.Add(attribute);
			}

			return customAttributes;
		}
	}
}
