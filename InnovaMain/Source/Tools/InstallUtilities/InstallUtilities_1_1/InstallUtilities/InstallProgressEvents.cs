using System;

namespace Oritax.TaxSimp.Administration.SystemUpdate
{
	
	public class InstallProgressEventArgs : EventArgs
	{
		private string message;

		public InstallProgressEventArgs(string message)
		{
			this.message = message;
		}

		public string Message
		{
			get
			{
				return message;
			}
		}
	}

	public class InstallErrorEventArgs : EventArgs
	{
		private string message;

		public InstallErrorEventArgs(string message)
		{
			this.message = message;
		}

		public string Message
		{
			get
			{
				return message;
			}
		}
	}

	public class MigrationInformationEventArgs : EventArgs
	{
		private string message;
		private string filePath;

		public MigrationInformationEventArgs(string message, string filePath)
		{
			this.message = message;
			this.filePath = filePath;
		}

		public string Message
		{
			get
			{
				return message;
			}
		}

		public string FilePath
		{
			get
			{
				return filePath;
			}
		}
	}
	
	public delegate void InstallProgressUpdateHandler(object sender, InstallProgressEventArgs e);

	public delegate void InstallErrorHandler(object sender, InstallErrorEventArgs e);

	public delegate void MigrationInformationHandler(object sender, MigrationInformationEventArgs e);
}
