using System;
using System.IO;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.ServiceProcess;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Administration.SystemUpdate
{
    public class DBCreateException : Exception
    {
        public DBCreateException(string message) : base(message) { }
    }

    /// <summary>
    ///		Provides functionality for creating and adding users to a MS SQL database.
    /// </summary>
    public class DatabaseManager
    {
        #region Private Members ------------------------------------------------------

        private string databaseName;
        private string databaseServer;
        private string databaseAdminName;
        private string databaseAdminPassword;
        private int retry = 0;

        #endregion //Private Members

        public DatabaseManager(string databaseName, string serverName, string adminUserName, string adminPassword)
        {
            if (databaseName.Length == 0) throw new DBCreateException("Database managment services was requested but database name parameter was absent");
            if (serverName.Length == 0) throw new DBCreateException("Database managment services was requested but database server parameter was absent");
            if (adminUserName.Length == 0) throw new DBCreateException("Database managment services was requested but database admin user name was absent");
            if (adminPassword.Length == 0) throw new DBCreateException("Database managment services was requested but database admin user password was absent");

            this.databaseName = databaseName;
            this.databaseServer = serverName;
            this.databaseAdminName = adminUserName;
            this.databaseAdminPassword = adminPassword;
        }

        #region Public Methods --------------------------------------------------------------------

        /// <summary>
        ///		Create N-Able Database.
        /// </summary>
        public void CreateDB()
        {
            SqlConnection masterConnection = null;

            try
            {
                string connectionString = "server=" + databaseServer + ";database=master;uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
                masterConnection = new SqlConnection(connectionString);
                masterConnection.Open();

                SqlCommand objCreateCommand = DBCommandFactory.Instance.NewSqlCommand("CREATE DATABASE " + databaseName, masterConnection);
                objCreateCommand.ExecuteNonQuery();

                SqlCommand setRecovery = DBCommandFactory.Instance.NewSqlCommand(string.Format("ALTER DATABASE {0} SET RECOVERY SIMPLE", databaseName), masterConnection);
                setRecovery.ExecuteNonQuery();

                Console.WriteLine("Database created");
            }
            finally
            {
                if (masterConnection != null)
                {
                    masterConnection.Close();
                }
            }
        }

        public void CreateSystemLogin(string systemUserName, string systemUserPassword)
        {
            if (systemUserName.Length == 0) throw new DBCreateException("CreateSystemLogin was requested but database N-ABLE system user name was absent");
            if (systemUserPassword.Length == 0) throw new DBCreateException("CreateSystemLogin was requested but database N-ABLE system user password was absent");

            SqlConnection masterConnection = null;

            // Add the system login.
            try
            {
                Console.WriteLine("Adding N-ABLE system login ...\n");

                string strMasterConnection = "server=" + databaseServer + ";database=master;uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
                masterConnection = new SqlConnection(strMasterConnection);
                masterConnection.Open();

                SqlCommand loginCommand = DBCommandFactory.Instance.NewSqlCommand("sp_addlogin", masterConnection);
                loginCommand.CommandType = System.Data.CommandType.StoredProcedure;
                loginCommand.Parameters.AddWithValue("@loginame", systemUserName);
                loginCommand.Parameters.AddWithValue("@passwd", systemUserPassword);
                loginCommand.Parameters.AddWithValue("@defdb", databaseName);

                loginCommand.ExecuteNonQuery();

                AddUserToDatabase(systemUserName, systemUserPassword);
            }
            finally
            {
                if (masterConnection != null)
                {
                    masterConnection.Close();
                }
            }
        }

        public void ChangeUserInDatabase(string systemUserName, string systemUserPassword)
        {
            if (systemUserName.Length == 0) throw new DBCreateException("Database creation was requested but database N-ABLE system user name was absent");
            if (systemUserPassword.Length == 0) throw new DBCreateException("Database creation was requested but database N-ABLE system user password was absent");

        }

        public void RemoveDBOwnerUserFromDatabase()
        {
            SqlConnection connection = null;

            try
            {
                string systemUserName = getsystemUser();

                if (systemUserName.Length != 0)
                {
                    string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
                    connection = new SqlConnection(connectionString);
                    connection.Open();

                    // Add the database user.
                    SqlCommand accessCommand = DBCommandFactory.Instance.NewSqlCommand();
                    Console.Write("Adding user to N-ABLE database ...");
                    accessCommand.CommandText = "sp_dropuser";
                    accessCommand.Connection = connection;
                    accessCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    accessCommand.Parameters.AddWithValue("@name_in_db ", systemUserName);
                    accessCommand.ExecuteNonQuery();
                }

            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }

        public void AddUserToDatabase(string systemUserName, string systemUserPassword)
        {
            SqlConnection connection = null;

            try
            {
                string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
                connection = new SqlConnection(connectionString);
                connection.Open();

                // Add the database user.
                SqlCommand accessCommand = DBCommandFactory.Instance.NewSqlCommand();
                Console.Write("Adding user to N-ABLE database ...");
                accessCommand.CommandText = "sp_adduser";
                accessCommand.Connection = connection;
                accessCommand.CommandType = System.Data.CommandType.StoredProcedure;
                accessCommand.Parameters.AddWithValue("@loginame", systemUserName);
                accessCommand.Parameters.AddWithValue("@grpname", "db_owner");
                accessCommand.ExecuteNonQuery();

                // Add the user role.
                accessCommand = DBCommandFactory.Instance.NewSqlCommand();
                Console.WriteLine("Adding user role ...");
                accessCommand.CommandText = "sp_addrolemember";
                accessCommand.Connection = connection;
                accessCommand.CommandType = System.Data.CommandType.StoredProcedure;
                accessCommand.Parameters.AddWithValue("@rolename", "db_owner");
                accessCommand.Parameters.AddWithValue("@membername", systemUserName);
                accessCommand.ExecuteNonQuery();
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }

        public void ChangeDatabaseUserPassword(string userName, string newPassword)
        {
            SqlConnection connection = null;

            try
            {
                string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
                connection = new SqlConnection(connectionString);
                connection.Open();

                // Add the database user.
                SqlCommand accessCommand = DBCommandFactory.Instance.NewSqlCommand();
                Console.Write("Adding user to N-ABLE database ...");
                accessCommand.CommandText = "sp_password";
                accessCommand.Connection = connection;
                accessCommand.CommandType = System.Data.CommandType.StoredProcedure;
                accessCommand.Parameters.AddWithValue("@old", null);
                accessCommand.Parameters.AddWithValue("@new", newPassword);
                accessCommand.Parameters.AddWithValue("@loginame", userName);
                accessCommand.ExecuteNonQuery();
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }

        public void AddNableUserAccount(string nableUserName, string nableUserPassword, string systemUserName, string systemUserPassword)
        {
            AddNableUserAccount(nableUserName, nableUserPassword, systemUserName, systemUserPassword, false);
        }

        public void AddNableUserAccount(string nableUserName, string nableUserPassword, string systemUserName, string systemUserPassword, bool administrator)
        {
            SqlConnection nAbleConnection = null;
            string admin = "0";
            if (administrator)
                admin = "1";
            try
            {
                string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + systemUserName + ";pwd=" + systemUserPassword;
                nAbleConnection = new SqlConnection(connectionString);
                nAbleConnection.Open();

                string encryptedPassword = Encryption.EncryptData(nableUserPassword);
                string userName = nableUserName;

                string sqlString = "INSERT INTO [UserDetail] ([CID],[Username], [Password], [Active],[Administrator], [LastLoggedIn],"
                    + "[EmailAddress], [FailedAttempts], [Locked], [Firstname], [Lastname], [DisplayZeroPreference], [AdministratorType], [UserType])"
                    + " VALUES('" + Guid.NewGuid().ToString() + "','" + userName + "', '" + encryptedPassword + "', 1," + admin.ToString() + ", NULL, "
                    + " NULL, 0, 0, '" + userName + "', '" + userName + "', 0, 0, 1)";

                SqlCommand userCommand = DBCommandFactory.Instance.NewSqlCommand(sqlString, nAbleConnection);
                userCommand.ExecuteNonQuery();
                nAbleConnection.Close();

            }
            catch
            {
                nAbleConnection.Close();
                if (retry != 5)
                {
                    retry++;
                    AddNableUserAccount(nableUserName, nableUserPassword, systemUserName, systemUserPassword, administrator);
                }
                else
                {
                    retry = 0;
                    throw;
                }
            }
        }

        public void UpdateNableUserAccounts(Guid organisationCid, string organisationName, string systemUserName, string systemUserPassword)
        {
            SqlConnection nAbleConnection = null;
            try
            {
                string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + systemUserName + ";pwd=" + systemUserPassword;
                nAbleConnection = new SqlConnection(connectionString);
                nAbleConnection.Open();

                string sqlString = "UPDATE [UserDetail] SET [OrganisationCID] = '" + organisationCid.ToString() + 
                    "', [OrganisationName] = '" + organisationName + "'";

                SqlCommand userCommand = DBCommandFactory.Instance.NewSqlCommand(sqlString, nAbleConnection);
                userCommand.ExecuteNonQuery();
                nAbleConnection.Close();

            }
            catch
            {
                nAbleConnection.Close();
                if (retry != 5)
                {
                    retry++;
                    UpdateNableUserAccounts(organisationCid, organisationName, systemUserName, systemUserPassword);
                }
                else
                {
                    retry = 0;
                    throw;
                }
            }
        }

        public void RemoveNableUserAccount(string userName)
        {
            SqlConnection connection = null;

            try
            {
                if (userName.Length != 0)
                {
                    string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
                    connection = new SqlConnection(connectionString);
                    connection.Open();

                    string sqlString = "DELETE FROM UserDetail WHERE Username = '" + userName + "'";

                    SqlCommand userCommand = DBCommandFactory.Instance.NewSqlCommand(sqlString, connection);
                    userCommand.ExecuteNonQuery();

                    sqlString = "DELETE FROM " + BrokerManagedComponentDS.CINSTANCES_TABLE + " WHERE NAME = '" + userName + "'";

                    userCommand = DBCommandFactory.Instance.NewSqlCommand(sqlString, connection);
                    userCommand.ExecuteNonQuery();
                }

            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }

        public void ChangeNableAccountPassword(string userName, string password)
        {
            SqlConnection connection = null;

            try
            {
                if (userName.Length != 0)
                {
                    string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
                    connection = new SqlConnection(connectionString);
                    connection.Open();

                    string encryptedPassword = Encryption.EncryptData(password);

                    string sqlString = "UPDATE UserDetail set Active = 1, locked = 0, FailedAttempts = 0 ,Password='" + encryptedPassword + "' where Username='" + userName + "'";

                    SqlCommand userCommand = DBCommandFactory.Instance.NewSqlCommand(sqlString, connection);
                    userCommand.ExecuteNonQuery();
                }

            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }

        public bool CheckNAbleAccount(string userName)
        {
            if (userName == null) throw new ArgumentNullException("Database user remove was requested but the user name was no supplied");
            if (userName.Length == 0) throw new ArgumentException("Database user remove was requested but the user name was no supplied");

            bool nableUserExists = false;
            SqlConnection connection = null;

            try
            {
                string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
                connection = new SqlConnection(connectionString);
                connection.Open();

                SqlCommand checkLoginCommand = DBCommandFactory.Instance.NewSqlCommand("SELECT * FROM UserDetail WHERE Username='" + userName + "'", connection);
                checkLoginCommand.CommandType = System.Data.CommandType.Text;

                SqlDataReader reader = checkLoginCommand.ExecuteReader();

                nableUserExists = reader.HasRows;
                reader.Close();
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }

            return nableUserExists;
        }

        public void CreateCINSTANCERecords(string systemUserName, string systemUserPassword)
        {
            SqlConnection nAbleConnection = null;
            try
            {
                string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + systemUserName + ";pwd=" + systemUserPassword;
                nAbleConnection = new SqlConnection(connectionString);
                nAbleConnection.Open();

                string objSql = "INSERT INTO " + BrokerManagedComponentDS.CINSTANCES_TABLE + " (CID, CTID, NAME) "
                    + "SELECT CID, (SELECT COMPONENTVERSION.ID FROM COMPONENTVERSION,COMPONENT WHERE COMPONENTVERSION.COMPONENTID=COMPONENT.ID AND COMPONENT.NAME = 'DBUser'), USERNAME "
                    + "FROM UserDetail ";

                SqlCommand objUserCommand = DBCommandFactory.Instance.NewSqlCommand(objSql.ToString(), nAbleConnection);
                objUserCommand.ExecuteNonQuery();
            }
            finally
            {
                nAbleConnection.Close();
            }
        }

        public void UpdateCINSTANCERecords(string systemUserName, string systemUserPassword)
        {
            SqlConnection nAbleConnection = null;
            try
            {
                string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + systemUserName + ";pwd=" + systemUserPassword;
                nAbleConnection = new SqlConnection(connectionString);
                nAbleConnection.Open();

                SqlCommand ctidCommand = DBCommandFactory.Instance.NewSqlCommand("SELECT COMPONENTVERSION.ID FROM COMPONENTVERSION,COMPONENT WHERE COMPONENTVERSION.COMPONENTID=COMPONENT.ID AND COMPONENT.NAME = 'DBUser'", nAbleConnection);
                ctidCommand.CommandType = System.Data.CommandType.Text;
                string CTID = ctidCommand.ExecuteScalar().ToString();

                SqlCommand cInstanceCommand = DBCommandFactory.Instance.NewSqlCommand("SELECT * FROM " + BrokerManagedComponentDS.CINSTANCES_TABLE, nAbleConnection);
                cInstanceCommand.CommandType = System.Data.CommandType.Text;
                SqlDataReader cInstanceReader = cInstanceCommand.ExecuteReader();

                ArrayList ids = new ArrayList();

                while (cInstanceReader.Read())
                {
                    ids.Add(cInstanceReader.GetSqlGuid(0).ToString());
                }
                cInstanceReader.Close();

                if (ids.Count > 0)
                {
                    ArrayList commands = new ArrayList();
                    SqlCommand userDetailCommand = DBCommandFactory.Instance.NewSqlCommand("SELECT * FROM UserDetail", nAbleConnection);
                    userDetailCommand.CommandType = System.Data.CommandType.Text;
                    SqlDataReader userDetailReader = userDetailCommand.ExecuteReader();

                    while (userDetailReader.Read())
                    {
                        bool found = false;
                        foreach (string id in ids)
                        {
                            if (id == userDetailReader.GetSqlGuid(0).ToString())
                            {
                                found = true;
                                break;
                            }
                        }

                        if (!found)
                        {
                            commands.Add("INSERT INTO " + BrokerManagedComponentDS.CINSTANCES_TABLE + "(CID, CTID, NAME) VALUES ('"
                                + userDetailReader.GetSqlGuid(0).ToString() + "','" + CTID + "','" + userDetailReader.GetString(1).ToString() + "')");
                        }
                    }
                    userDetailReader.Close();

                    foreach (string command in commands)
                    {
                        SqlCommand objUserCommand = DBCommandFactory.Instance.NewSqlCommand(command, nAbleConnection);
                        objUserCommand.ExecuteNonQuery();
                    }
                }
                else
                {
                    CreateCINSTANCERecords(systemUserName, systemUserPassword);
                }


            }
            finally
            {
                nAbleConnection.Close();
            }
        }

        public void RemoveSystemLogin(string systemUserName)
        {
            if (systemUserName == null) throw new ArgumentNullException("Database user remove was requested but the user name was no supplied");
            if (systemUserName.Length == 0) throw new ArgumentException("Database user remove was requested but the user name was no supplied");

            SqlConnection masterConnection = null;

            try
            {
                string strMasterConnection = "server=" + databaseServer + ";database=master;uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
                masterConnection = new SqlConnection(strMasterConnection);
                masterConnection.Open();

                SqlCommand checkLoginCommand = DBCommandFactory.Instance.NewSqlCommand("sp_helplogins", masterConnection);
                checkLoginCommand.CommandType = System.Data.CommandType.StoredProcedure;
                checkLoginCommand.Parameters.AddWithValue("@LoginNamePattern", systemUserName);

                SqlDataReader reader = checkLoginCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Close();

                    SqlCommand deleteLoginCommand = DBCommandFactory.Instance.NewSqlCommand("sp_droplogin", masterConnection);
                    deleteLoginCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    deleteLoginCommand.Parameters.AddWithValue("@loginame", systemUserName);
                    deleteLoginCommand.ExecuteNonQuery();
                }
                else
                {
                    reader.Close();
                }

            }
            finally
            {
                if (masterConnection != null)
                {
                    masterConnection.Close();
                }
            }
        }

        public bool CheckSystemLogin(string systemUserName)
        {
            if (systemUserName == null) throw new ArgumentNullException("Database user remove was requested but the user name was no supplied");
            if (systemUserName.Length == 0) throw new ArgumentException("Database user remove was requested but the user name was no supplied");

            bool systemUserExists = false;
            SqlConnection masterConnection = null;

            try
            {
                string strMasterConnection = "server=" + databaseServer + ";database=master;uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
                masterConnection = new SqlConnection(strMasterConnection);
                masterConnection.Open();

                SqlCommand checkLoginCommand = DBCommandFactory.Instance.NewSqlCommand("sp_helplogins", masterConnection);
                checkLoginCommand.CommandType = System.Data.CommandType.StoredProcedure;
                checkLoginCommand.Parameters.AddWithValue("@LoginNamePattern", systemUserName);
                SqlDataReader reader = checkLoginCommand.ExecuteReader();

                systemUserExists = reader.HasRows;
                reader.Close();
            }
            finally
            {
                if (masterConnection != null)
                {
                    masterConnection.Close();
                }
            }

            return systemUserExists;
        }

        public bool DatabaseExists(string systemUserName, string systemUserPassword)
        {
            return databaseExists(databaseName, systemUserName, systemUserPassword);
        }

        public ArrayList GetLockedScenearioIDs(string systemUserName, string systemUserPassword)
        {
            SqlConnection connection = null;
            ArrayList ids = new ArrayList();

            string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + systemUserName + ";pwd=" + systemUserPassword;
            connection = new SqlConnection(connectionString);
            connection.Open();

            SqlCommand command = DBCommandFactory.Instance.NewSqlCommand("SELECT CSID FROM Scenario WHERE Locked='1'", connection);
            command.CommandType = System.Data.CommandType.Text;

            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    ids.Add(reader.GetValue(0).ToString());
                }
            }
            reader.Close();
            connection.Close();

            return ids;

        }

        public void ChangeScenarioLockedStatus(ArrayList ids, string status, string systemUserName, string systemUserPassword)
        {
            SqlConnection connection = null;

            string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + systemUserName + ";pwd=" + systemUserPassword;
            connection = new SqlConnection(connectionString);
            connection.Open();

            string sqlString = "";

            foreach (object csid in ids)
            {
                sqlString = sqlString + "UPDATE Scenario set locked = " + status + " where CSID='" + csid.ToString() + "';";
            }

            SqlCommand command = DBCommandFactory.Instance.NewSqlCommand(sqlString, connection);
            command.CommandType = System.Data.CommandType.Text;

            command.ExecuteNonQuery();

        }

        private bool databaseExists(string dbName, string systemUserName, string systemUserPassword)
        {
            bool exists = true;

            SqlConnection connection = null;
            try
            {
                string connectionString = "server=" + databaseServer + ";database=" + dbName + ";uid=" + systemUserName + ";pwd=" + systemUserPassword;
                connection = new SqlConnection(connectionString);
                connection.Open();
            }
            catch 
            {
                exists = false;
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return exists;
        }

        public void Delete()
        {
            delete(this.databaseName);
        }

        private void delete(string dbName)
        {
            killUsers(dbName);

            SqlConnection masterConnection = null;

            try
            {
                Console.WriteLine("Deleting Database : " + dbName + " on " + databaseServer + "...\n");

                string connectionString = "server=" + databaseServer + ";database=master;uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
                masterConnection = new SqlConnection(connectionString);
                masterConnection.Open();
                
                string sql = string.Format("ALTER DATABASE [{0}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE", dbName);
                SqlCommand command = DBCommandFactory.Instance.NewSqlCommand(sql , masterConnection);
                command.ExecuteNonQuery();

                SqlCommand objCreateCommand = DBCommandFactory.Instance.NewSqlCommand("DROP DATABASE " + dbName, masterConnection);
                objCreateCommand.ExecuteNonQuery();

                Console.WriteLine("Database Deleted Successfully.\n");
            }
            finally
            {
                if (masterConnection != null)
                {
                    masterConnection.Close();
                }
            }
        }

        public void BackupDatabase(string backupFileName)
        {
            string commandText = string.Format("BACKUP DATABASE [{0}] TO DISK = N'{1}' WITH INIT, NAME = N'{0} backup', SKIP, NOFORMAT", databaseName, backupFileName);
            string connectionString = "server=" + databaseServer + ";database=master;uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
            SqlConnection masterConnection = new SqlConnection(connectionString);
            masterConnection.Open();

            using (SqlCommand backupCommand = DBCommandFactory.Instance.NewSqlCommand(commandText, masterConnection))
            {
                backupCommand.ExecuteNonQuery();
            }

            masterConnection.Close();
        }

        public void RestoreDatabase(string backupFileName)
        {
            string commandText = string.Format("RESTORE DATABASE [{0}] FROM DISK = N'{1}'", databaseName, backupFileName);
            string connectionString = "server=" + databaseServer + ";database=master;uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
            SqlConnection masterConnection = new SqlConnection(connectionString);
            masterConnection.Open();

            using (SqlCommand backupCommand = DBCommandFactory.Instance.NewSqlCommand(commandText, masterConnection))
            {
                backupCommand.ExecuteNonQuery();
            }

            masterConnection.Close();
        }

        private void restoreDatabase(string backupFileName, string newFileName)
        {
            string connectionString = "server=" + databaseServer + ";database=master;uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
            SqlConnection masterConnection = new SqlConnection(connectionString);
            masterConnection.Open();

            string commandText = string.Format("RESTORE FILELISTONLY FROM DISK = N'{0}'", backupFileName);
            string dataBasePath = "";
            string logicalName = "";
            string logicalLogName = "";
            using (SqlCommand fileListCommand = DBCommandFactory.Instance.NewSqlCommand(commandText, masterConnection))
            {
                fileListCommand.CommandTimeout = 6000;
                SqlDataReader reader = fileListCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    int i = 0;
                    while (reader.Read())
                    {
                        if (i == 0)
                        {
                            logicalName = (string)reader.GetValue(0);
                            dataBasePath = (string)reader.GetValue(1);
                        }
                        if (i == 1)
                        {
                            logicalLogName = (string)reader.GetValue(0);
                        }
                        i++;
                    }
                }
                reader.Close();
            }

            string dataFile = Path.Combine(Directory.GetParent(dataBasePath).FullName, newFileName + ".mdf");
            string logFile = Path.Combine(Directory.GetParent(dataBasePath).FullName, newFileName + "_log.ldf");

            commandText = string.Format("RESTORE DATABASE [{0}] FROM DISK = N'{1}' WITH MOVE '{2}' TO '{3}', MOVE '{4}' TO '{5}'", newFileName, backupFileName, logicalName, dataFile, logicalLogName, logFile);

            using (SqlCommand restoreCommand = DBCommandFactory.Instance.NewSqlCommand(commandText, masterConnection))
            {
                restoreCommand.CommandTimeout = 6000;
                restoreCommand.ExecuteNonQuery();
            }

            alterDatabaseName(newFileName, logicalName, newFileName);
            alterDatabaseName(newFileName, logicalLogName, newFileName + "_log");

            masterConnection.Close();
        }


        public void CloneDatabase(string newDatabaseName)
        {
            string tempBackUpName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "DBTempBackup.bak");
            if (File.Exists(tempBackUpName))
            {
                File.Delete(tempBackUpName);
            }

            BackupDatabase(tempBackUpName);

            if (this.databaseExists(newDatabaseName, this.databaseAdminName, this.databaseAdminPassword))
            {
                DialogResult result = MessageBox.Show(string.Format("The upgrade process is attempting to clone the existing system and requires the database name '{0}'. \n Do you want to delete this database and continue? ", newDatabaseName), "Delete existing database?", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (result == DialogResult.Cancel)
                {
                    throw new Exception(string.Format("The user has canceled the update. Attempting to delete the {0} database", newDatabaseName));
                }
                else
                {
                    this.delete(newDatabaseName);
                }
            }

            restoreDatabase(tempBackUpName, newDatabaseName);

            killUsers(newDatabaseName);

            File.Delete(tempBackUpName);
        }

        private void alterDatabaseName(string targetDatabase, string oldDatabaseName, string newDatabaseName)
        {
            string commandText = string.Format("ALTER DATABASE [{0}] MODIFY FILE (NAME='{2}',NEWNAME='{1}')", targetDatabase, newDatabaseName, oldDatabaseName);
            string connectionString = "server=" + databaseServer + ";database=master;uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
            SqlConnection masterConnection = new SqlConnection(connectionString);
            masterConnection.Open();

            using (SqlCommand backupCommand = DBCommandFactory.Instance.NewSqlCommand(commandText, masterConnection))
            {
                backupCommand.ExecuteNonQuery();
            }
            masterConnection.Close();
        }

        public void CreateProductVersionTable()
        {
            string commandText = "if not exists(select * from dbo.sysobjects where id = object_id(N'[dbo].[ProductVersion]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
                                 + " CREATE TABLE [dbo].[ProductVersion] ([Version] [varchar] (255) NOT NULL, [Date] [varchar] (255) NOT NULL) ON [PRIMARY]";

            string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
            SqlConnection masterConnection = new SqlConnection(connectionString);
            masterConnection.Open();

            using (SqlCommand versionTableCommand = DBCommandFactory.Instance.NewSqlCommand(commandText, masterConnection))
            {
                versionTableCommand.CommandType = System.Data.CommandType.Text;
                versionTableCommand.ExecuteNonQuery();
            }

            masterConnection.Close();
        }

        public void UpdateBuildVersionInTheDatabase(string buildNumber)
        {
            string commandText = string.Format("INSERT INTO [ProductVersion] ([Version], [Date]) VALUES ('{0}', '{1}')", buildNumber, DateTime.Now.ToString());

            string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
            SqlConnection masterConnection = new SqlConnection(connectionString);
            masterConnection.Open();

            using (SqlCommand versionTableCommand = DBCommandFactory.Instance.NewSqlCommand(commandText, masterConnection))
            {
                versionTableCommand.CommandType = System.Data.CommandType.Text;
                versionTableCommand.ExecuteNonQuery();
            }

            masterConnection.Close();

        }

        public void UpdateBuildVersionInTheDatabaseForUpgrade(string buildNumber)
        {
            string commandText = string.Format("if not exists(select * from dbo.sysobjects where id = object_id(N'[dbo].[ProductVersion]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
                                + " CREATE TABLE [dbo].[ProductVersion] ([Version] [varchar] (255) NOT NULL, [Date] [varchar] (255) NOT NULL) ON [PRIMARY] INSERT INTO [ProductVersion] ([Version], [Date]) VALUES ('{0}', '{1}')", buildNumber, DateTime.Now.ToString());

            string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
            SqlConnection masterConnection = new SqlConnection(connectionString);
            masterConnection.Open();

            using (SqlCommand versionTableCommand = DBCommandFactory.Instance.NewSqlCommand(commandText, masterConnection))
            {
                versionTableCommand.CommandType = System.Data.CommandType.Text;
                versionTableCommand.ExecuteNonQuery();
            }

            masterConnection.Close();

        }

        public static string GetDatabaseNameFromPath(string path)
        {
            int lastSlash = path.LastIndexOf("\\");
            if (lastSlash > 0)
            {
                return path.Substring(lastSlash + 1);
            }
            else
            {
                //detect c:DIRECTORY
                if (path[1] == ':')
                {
                    return path.Substring(2);
                }
                else
                {
                    return path;
                }
            }
        }

        public void CleanDatabaseTable(string tableName, string systemUserName, string systemUserPassword)
        {
            string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + systemUserName + ";pwd=" + systemUserPassword;
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            string deleteCommandText = "DELETE FROM " + tableName;

            SqlCommand command = DBCommandFactory.Instance.NewSqlCommand(deleteCommandText, conn);
            try
            {
                command.CommandTimeout = 180;
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public void ApplyPreInstallScriptsToDatabase(string systemUserName, string systemUserPassword)
        {
            string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + systemUserName + ";pwd=" + systemUserPassword;

            SqlConnection.ClearAllPools();

        }

        public void ApplyPostInstallScriptsToDatabase(string systemUserName, string systemUserPassword)
        {

            string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + systemUserName + ";pwd=" + systemUserPassword;

        }

        public void ApplyScriptToDatabase(string systemUserName, string systemUserPassword, string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            Stream str = asm.GetManifestResourceStream(resourceName);
            StreamReader reader = new StreamReader(str);

            string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + systemUserName + ";pwd=" + systemUserPassword;

            parseStreamAndActionScript(reader, connectionString);
        }

        /// <summary>
        /// Parse the stream extracting 
        /// </summary>
        /// <param name="stream"></param>
        private void parseStreamAndActionScript(System.IO.StreamReader stream, string connectionString)
        {
            // Parses the stream looking for GO markers which 
            // delimit blocks of SQL.
            string line;
            StringBuilder currentBlock = new StringBuilder();
            int overallLine = 0;
            int blockLine = 0;
            while ((line = stream.ReadLine()) != null)
            {
                overallLine++;
                blockLine++;
                if (line == "GO")
                {
                    // GO marker found, Action the script block
                    // up to the GO marker
                    actionScript(currentBlock.ToString(), connectionString);

                    // Reset the script block
                    currentBlock = new StringBuilder();
                    blockLine = 0;
                }
                else
                {
                    // To assist debugging output the line of the stream (overallLine)
                    // and the line within the block (blockLine). SQL Server will report
                    // errors relative to the start of a block.
                    currentBlock.Append(line);
                    currentBlock.Append(Environment.NewLine);
                }
            }

            // Send the last block to be processed.
            actionScript(currentBlock.ToString(), connectionString);

        }

        /// <summary>
        /// Parse the stream extracting, and return script as string. 
        /// </summary>
        /// <param name="stream"></param>
        private string parseStreamAndActionScript(System.IO.StreamReader stream)
        {
            // Parses the stream looking for GO markers which 
            // delimit blocks of SQL.
            string line;
            StringBuilder currentBlock = new StringBuilder();
            int overallLine = 0;
            int blockLine = 0;
            while ((line = stream.ReadLine()) != null)
            {
                overallLine++;
                blockLine++;

                // To assist debugging output the line of the stream (overallLine)
                // and the line within the block (blockLine). SQL Server will report
                // errors relative to the start of a block.
                currentBlock.Append(line);
                currentBlock.Append(Environment.NewLine);
            }

            // return block.
            return currentBlock.ToString();
        }

        private void actionScript(string script, string connectionString)
        {
            if (script == String.Empty)
                return;

            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            string scriptsCommandText = script;
            SqlCommand command = DBCommandFactory.Instance.NewSqlCommand(scriptsCommandText, conn);
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }
       
        #endregion //Public Methods

        #region Private Methods -------------------------------------------------------------------\

        private string getsystemUser()
        {
            string systemUserName = "";
            SqlConnection connection = null;

            try
            {
                string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
                connection = new SqlConnection(connectionString);
                connection.Open();

                SqlCommand roleMemberCommand = DBCommandFactory.Instance.NewSqlCommand("sp_helprolemember", connection);
                roleMemberCommand.CommandType = System.Data.CommandType.StoredProcedure;
                roleMemberCommand.Parameters.AddWithValue("@rolename", "db_owner");
                roleMemberCommand.ExecuteNonQuery();

                SqlDataReader reader = roleMemberCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        if (reader.GetString(1) != "dbo")
                        {
                            systemUserName = reader.GetString(1);
                            break;
                        }
                    }

                }
                reader.Close();
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }

            return systemUserName;
        }

        private void stopStartSQLServer()
        {
            TimeSpan timeout = TimeSpan.FromSeconds(120);
            using (ServiceController serviceController = new ServiceController("MSSQLSERVER"))
            {
                if (serviceController.Status != ServiceControllerStatus.Stopped)
                {
                    serviceController.Stop();
                    serviceController.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                }

                serviceController.Start();
                serviceController.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
        }

        public void killUsers(string dbName)
        {
            string connectionString = "server=" + databaseServer + ";database=master;uid=" + databaseAdminName + ";pwd=" + databaseAdminPassword;
            SqlConnection masterConnection = new SqlConnection(connectionString);
            masterConnection.Open();

            SqlCommand loginCommand = DBCommandFactory.Instance.NewSqlCommand("sp_who", masterConnection);
            loginCommand.CommandType = System.Data.CommandType.StoredProcedure;
            loginCommand.ExecuteNonQuery();

            SqlDataReader reader = loginCommand.ExecuteReader();

            ArrayList spids = new ArrayList();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (reader.GetValue(6) != DBNull.Value)
                    {
                        if (string.Compare(reader.GetString(6), dbName, true) == 0)
                        {
                            spids.Add(reader.GetValue(0).ToString());
                        }
                    }
                }

            }
            reader.Close();

            foreach (string spid in spids)
            {
                SqlCommand command = DBCommandFactory.Instance.NewSqlCommand("kill " + spid, masterConnection);
                command.CommandType = System.Data.CommandType.Text;
                command.ExecuteNonQuery();
            }
            masterConnection.Close();
        }


        #endregion //Private Methods
    }
}
