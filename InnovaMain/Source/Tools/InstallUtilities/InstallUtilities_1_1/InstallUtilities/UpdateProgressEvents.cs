using System;

namespace Oritax.TaxSimp.UpdateUtilities
{
	public enum StepProgress
	{
		Cloning,
		Updating,
		InstallingSystemComponents,
		Finished
	}
	public class UpdateProgressComponentEventArgs : EventArgs
	{
		private string componentName;
		private string progress;
		private string progressPercentage;

		public UpdateProgressComponentEventArgs(string componentName, string progress, string progressPercentage)
		{
			this.componentName = componentName;
			this.progress = progress;
			this.progressPercentage = progressPercentage;
		}

		public string ComponentName
		{
			get
			{
				return componentName;
			}
		}

		public string Progress
		{
			get
			{
				return progress;
			}
		}

		public string ProgressPercentage
		{
			get
			{
				return progressPercentage;
			}
		}
	}

	public class UpdateProgressEventArgs : EventArgs
	{
		private int stepNumber;
		private int totalSteps;
		private string description;

		public UpdateProgressEventArgs(int stepNumber, int totalSteps, string description)
		{
			this.stepNumber = stepNumber;
			this.totalSteps = totalSteps;
			this.description = description;
		}

		public string Description
		{
			get
			{
				return description;
			}
		}

		public int TotalSteps
		{
			get
			{
				return totalSteps;
			}
		}

		public int StepNumber
		{
			get
			{
				return stepNumber;
			}
		}
	}

	public class UpdateProgressStepEventArgs : EventArgs
	{
		private StepProgress progress;

		public UpdateProgressStepEventArgs(StepProgress progress)
		{
			this.progress = progress;
		}

		public StepProgress StepProgress
		{
			get
			{
				return progress;
			}
		}
	}

	public class UpdateMessageEventArgs : EventArgs
	{
		private string message;

		public UpdateMessageEventArgs(string message)
		{
			this.message = message;
		}

		public string Message
		{
			get
			{
				return message;
			}
		}
	}
	
	public delegate void UpdateProgressStepUpdateHandler(object sender, UpdateProgressStepEventArgs e);

	public delegate void UpdateProgressComponentUpdateHandler(object sender, UpdateProgressComponentEventArgs e);

	public delegate void UpdateMessageHandler(object sender, UpdateMessageEventArgs e);

	public delegate void UpdateProgressHandler(object sender, UpdateProgressEventArgs e);

}
