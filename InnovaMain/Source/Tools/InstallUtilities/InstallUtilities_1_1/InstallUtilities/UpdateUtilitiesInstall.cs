using System;

namespace Oritax.TaxSimp.Administration.SystemUpdate
{
	/// <summary>
	/// Summary description for UpdateUtilitiesInstall.
	/// </summary>
	public class UpdateUtilitiesInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="47669779-7423-4411-BF8D-14A15E880809";
		public const string ASSEMBLY_NAME="InstallUtilities_1_1";
		public const string ASSEMBLY_DISPLAYNAME="InstallUtilities V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		//public const string ASSEMBLY_REVISION="0";
		public const string ASSEMBLY_REVISION="1";//2005.1

		#endregion
	}
}
