using System;
using System.Collections;
using System.DirectoryServices;
using System.IO;
using System.Xml;
using System.Runtime.InteropServices;
using System.Net;

namespace Oritax.TaxSimp.Administration.SystemUpdate
{
    /// <summary>
    ///		Provides functionality that assists in the deployment of a web site 
    ///		and associated config files.
    /// </summary>
    public class WebDeployment
    {
        private const int MAXOSRESERVEDMEMORY = 1073741824; // 1GB max reserved for OS
        private const int MINAPPCHUNKSIZE = 1073741824; // 1GB for each application as a minimum


        #region Public Methods -------------------------------------------------------

        /// <summary>
        ///		Updates the web config file with all the required attributes and paramaters for the product.
        /// </summary>
        /// <param name="webConfigFileName">
        ///		Full path name of the web config file to be updated.
        /// </param>
        /// <param name="nableConnectionString">
        ///		The connection string for the product.
        /// </param>
        public static void UpdateWebConfigFile(string webConfigFileName, string nableConnectionString, ProgressUpdate progressUpdate)
        {
            XmlDocument objXMLDocument = new XmlDocument();
            XmlNode objAppSettings = null;

            objXMLDocument.Load(webConfigFileName);

            // Generate the 'appSettings' element.
            objAppSettings = objXMLDocument.DocumentElement.LastChild;

            if (progressUpdate != null)
            {
                progressUpdate("UpdateWebConfigFile with connection string " + nableConnectionString);
            }
            foreach (XmlNode node in objAppSettings.ChildNodes)
            {
                if (node.Attributes["key"].Value == "DBConnectionString")
                {
                    node.Attributes["value"].Value = nableConnectionString;
                }
                if (node.Attributes["key"].Value == "CachingMemoryTriggerMaxMemorySize")
                {
                    node.Attributes["value"].Value = GetSuggestedCacheMemorySize(IsSQLInstalledLocally(nableConnectionString, progressUpdate)).ToString();
                }
            }

            // test is it exists.
            XmlNodeList xmlNodeList = objXMLDocument.GetElementsByTagName("identity");
            if (xmlNodeList.Count == 1)
            {
                XmlNode xmlNode = xmlNodeList[0];
                xmlNode.Attributes["impersonate"].Value = "false";
            }
            else
            {
                if (progressUpdate != null)
                {
                    progressUpdate("Error: Could not find identity node in web.config file.");
                }
            }

            XmlTextWriter objWriter = new XmlTextWriter(webConfigFileName, null);
            objWriter.Formatting = Formatting.Indented;
            objXMLDocument.WriteContentTo(objWriter);
            objWriter.Close();
        }


        /// <summary>
        /// Determines if SQL is installed on the local machine based on the connection string
        /// </summary>
        /// <param name="connectionString">The connection string to check</param>
        /// <returns>True if the connection string indicates SQL is installed locally, false otherwise</returns>
        /// <exception cref="System.ArgumentNullException">connectionString cannot be null or an empty string</exception>
        private static bool IsSQLInstalledLocally(string connectionString, ProgressUpdate progressUpdate)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException("connectionString");

            string serverName = System.Environment.MachineName;
            try
            {
                if (IsLocalSQLAddress(serverName) || IsLocalAddress(serverName))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                if (progressUpdate != null)
                {
                    progressUpdate("Exception occured within IsSQLInstalledLocally : DB serverName - " + serverName);
                    progressUpdate(ex.Message + ", " + ex.InnerException);
                    progressUpdate(ex.StackTrace);
                }
                return false;
            }
        }

        /// <summary>
        /// Gets the name of the sql server from a connection string
        /// </summary>
        /// <param name="connectionString">The connection string to extract the server name from</param>
        /// <returns>The server name</returns>
        /// <exception cref="System.ArgumentNullException">connectionString cannot be null or an empty string</exception>
        /// <exception cref="System.ArgumentException">connectionString does not contain a server name or is not well formed</exception>
        private static string ExtractServerNameFromConnectionString(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException("connectionString");

            string[] connectionStringParts = connectionString.Split(';');

            foreach (string connectionStringPart in connectionStringParts)
            {
                if (connectionStringPart.ToLowerInvariant().Contains("server"))
                {
                    string[] serverNameParts = connectionStringPart.Split('=');

                    if (serverNameParts.Length != 2)
                        throw new ArgumentException("Connection string, server part is not well formed");

                    return serverNameParts[1];
                }
            }

            throw new ArgumentException("Connection string does not contain a server name");
        }

        /// <summary>
        /// Determines if a server name is a "special" sql server address
        /// </summary>
        /// <param name="serverName">The server name to check</param>
        /// <returns>True if the server name is a "special" sql server address, false otherwise</returns>
        /// <remarks>"." and "(local)" are special sql addresses</remarks>
        /// <exception cref="System.ArgumentNullException">serverName cannot be null or an empty string</exception>
        private static bool IsLocalSQLAddress(string serverName)
        {
            if (string.IsNullOrEmpty(serverName))
                throw new ArgumentNullException("serverName");

            return (serverName == "." ||
                    serverName.ToLowerInvariant() == "(local)");
        }

        /// <summary>
        /// Determines if a server name is a local address by resolving the name and checking the IP address
        /// </summary>
        /// <param name="serverName">The server name to check</param>
        /// <returns>True if the server name is a local machine, false otherwise</returns>
        /// <exception cref="System.ArgumentNullException">serverName cannot be null or an empty string</exception>
        private static bool IsLocalAddress(string serverName)
        {
            if (string.IsNullOrEmpty(serverName))
                throw new ArgumentNullException("serverName");

            IPAddress[] ipAddresses = Dns.GetHostAddresses(serverName);

            foreach (IPAddress ipAddress in ipAddresses)
            {
                if (IPAddress.IsLoopback(ipAddress))
                    return true;
            }
            return false;
        }


        /// <summary>
        ///		Creates the virtual directory that publishes the web site.
        /// </summary>
        /// <param name="websiteLocation">
        ///		Directory where the website is installed.
        /// </param>
        /// <param name="projectName">
        ///		The name of the installation.
        /// </param>
        public static void CreateVirtualDirectory(string websiteLocation, string projectName, string defaultDocument, ProgressUpdate progressUpdate)
        {
            string schema = "IIsWebVirtualDir";
            string rootSubPath = "/W3SVC/1/Root";

            DirectoryEntry root = new DirectoryEntry("IIS://localhost" + rootSubPath);

            try
            {
                root.RefreshCache();

                DirectoryEntry dir = null;
                try
                {
                    dir = root.Children.Find(projectName, schema);

                    if (dir == null)
                    {
                        createWebsite(root, websiteLocation, projectName, schema, defaultDocument);
                    }
                    else
                    {
                        dir.Properties["Path"].Clear();
                        dir.CommitChanges();
                        dir.RefreshCache();
                        dir.Properties["Path"].Insert(0, websiteLocation);
                        dir.CommitChanges();

                        if (defaultDocument != null)
                        {
                            dir.Properties["DefaultDoc"].Clear();
                            dir.CommitChanges();
                            dir.RefreshCache();
                            dir.Properties["DefaultDoc"].Insert(0, defaultDocument);
                            dir.CommitChanges();
                            dir.Close();
                        }
                        else
                        {
                            dir.Close();
                        }
                    }
                }
                catch (DirectoryNotFoundException)
                {
                    createWebsite(root, websiteLocation, projectName, schema, defaultDocument);
                }

                root.CommitChanges();
                root.Close();
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                if (progressUpdate != null)
                {
                    progressUpdate("COM Error : " + ex.Message);
                }
            }
        }

        private static void createWebsite(DirectoryEntry root, string websiteLocation, string projectName, string schema, string defaultDocument)
        {
            DirectoryEntry dir = root.Children.Add(projectName, schema);

            dir.Properties["Path"].Insert(0, websiteLocation);
            if (defaultDocument != null)
            {
                dir.Properties["DefaultDoc"].Insert(0, defaultDocument);
            }
            dir.CommitChanges();
            root.CommitChanges();

            // Create a Application
            dir.Invoke("AppCreate", true);
            dir.CommitChanges();
            dir.Close();
        }

        public static string GetWebConfigValue(string webConfigFileName, string elementName, string keyName)
        {
            string value = "";
            XmlDocument objXMLDocument = new XmlDocument();

            objXMLDocument.Load(webConfigFileName);

            XmlNode xmlNode = objXMLDocument.DocumentElement.LastChild;

            foreach (XmlNode node in xmlNode.ChildNodes)
            {
                if (node.Attributes["key"].Value == keyName)
                {
                    value = node.Attributes["value"].Value;
                    break;
                }
            }

            return value;
        }

        public void RegisterWebsites(string installName, ProgressUpdate progressUpdate)
        {
            string output = "";
            registerASPNetWebsite(installName, out output);

            if (progressUpdate != null)
            {
                progressUpdate(output);
            }
        }

        /// <summary>
        ///		Checkes IIS and returns all web sites that are registered with the 
        ///		IIsWebVirtualDir schema
        /// </summary>
        /// <param name="progressUpdate">
        ///		Reports back progress.
        /// </param>
        /// <returns>
        ///		A string array of all the names of the registered web sites. 
        /// </returns>
        public static string[] GetDeployedWebSites(ProgressUpdate progressUpdate)
        {
            ArrayList webSites = new ArrayList();
            string schema = "IIsWebVirtualDir";
            string rootSubPath = "/W3SVC/1/Root";

            DirectoryEntry deRoot = new DirectoryEntry("IIS://localhost" + rootSubPath);

            try
            {
                deRoot.RefreshCache();

                foreach (DirectoryEntry entry in deRoot.Children)
                {
                    if (string.Compare(entry.SchemaClassName, schema, true) == 0)
                    {
                        string path = ((string)entry.Properties["Path"].Value).ToLower();
                        if (path.IndexOf("innova") != -1 || path.IndexOf("eclipse") != -1)
                        {
                            webSites.Add(entry.Name);
                        }
                    }
                }

                deRoot.Close();
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                if (progressUpdate != null)
                {
                    progressUpdate("COM Error : " + ex.Message);
                }
            }

            return (string[])webSites.ToArray(typeof(string));
        }

        /// <summary>
        ///		Checkes IIS and returns all web sites.
        /// </summary>
        /// <param name="progressUpdate">
        ///		Reports back progress.
        /// </param>
        /// <returns>
        ///		A string array of all the names of the registered web sites. 
        /// </returns>
        public static string[] GetAllWebSites(ProgressUpdate progressUpdate)
        {
            ArrayList webSites = new ArrayList();
            string rootSubPath = "/W3SVC/1/Root";

            DirectoryEntry deRoot = new DirectoryEntry("IIS://localhost" + rootSubPath);

            try
            {
                deRoot.RefreshCache();

                foreach (DirectoryEntry entry in deRoot.Children)
                {
                    webSites.Add(entry.Name);
                }

                deRoot.Close();
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                if (progressUpdate != null)
                {
                    progressUpdate("COM Error : " + ex.Message);
                }
            }

            return (string[])webSites.ToArray(typeof(string));
        }

        #endregion //Public Methods

        #region Private Methods ------------------------------------------------------

        //private bool IsDotNet20Installed()
        //{
        //    bool installed = false;
        //    RegistryHelper reg = new RegistryHelper("Microsoft", ".NETFramework");
        //    object regValue = reg.GetRegistryValue(@"policy\v2.0", "50727");

        //    if (regValue != null)
        //        installed = true;

        //    return installed;
        //}

        private bool IsDotNet40Installed()
        {
            bool installed = false;
            RegistryHelper reg = new RegistryHelper("Microsoft", ".NETFramework");
            object regValue = reg.GetRegistryValue(@"policy\v4.0", "30319");//v4.0.30319

            if (regValue != null)
                installed = true;

            return installed;
        }

        //public static string DotNetFrameWorkPath
        //{
        //    get
        //    {
        //        RegistryHelper reg = new RegistryHelper("Microsoft", ".NETFramework");
        //        object regValue = reg.GetRegistryValue(null, "InstallRoot");
        //        string dotNetDirectory = Path.Combine(regValue.ToString(), "v2.0.50727");
        //        return dotNetDirectory;                
        //    }
        //}

        public static string DotNetFrameWorkPath
        {
            get
            {
                RegistryHelper reg = new RegistryHelper("Microsoft", ".NETFramework");
                object regValue = reg.GetRegistryValue(null, "InstallRoot");//v4.0.30319
                string dotNetDirectory = Path.Combine(regValue.ToString(), "v4.0.30319");
                return dotNetDirectory;
            }
        }

        //private string dotNetFramework20Directory()
        //{
        //    return DotNetFrameWorkPath;
        //}

        private string dotNetFramework40Directory()
        {
            return DotNetFrameWorkPath;
        }

        public static bool IsWin2K3Server()
        {
            //	You need to look on MSDN. 
            //
            //	Numbers are xx.xx.xxxx which represents the major version, minor version and build numbers. 
            //
            //	Major version 5 = Windows 2000 or Windows XP. 
            //	Minor version 0 - Windows 2000 
            //	Minor version 1 = Windows XP 
            //	Minor version 2 = Windows 2003 Server 
            //
            //	Major version 4 - Windows NT, Windows 95 or 98. 
            //	Minor version 0 - Windows NT 
            //	Minor version 10 - Windows 98 
            //	Minor version 90 - Windows ME 
            //  ---------------------------------------------------------------------------------
            //	KB189249 HOW TO: Determine Which 32-Bit Windows Version Is Being Used 
            //	KB304283 HOW TO: Determine Windows Version by Using Visual C# .NET 
            //	KB304289 HOW TO: Determine Windows Version by Using Visual Basic .NET 
            //	KB307394 HOW TO: Determine Windows Version by Using Visual C++ .NET 
            //
            //	Furthermore, the programming instructions noted that variable information about the various OS's catagorized Windows into 2 groups as stated below: 
            //
            //	"...the enumerated value of the Win32Windows property indicates one of the following operating systems: 
            //	* Windows 95 
            //	* Windows 98 
            //	* Windows 98 Second Edition 
            //	* Windows Me 
            //
            //	Similarly, the WinNT property indicates one of the following operating systems: 
            //	* Windows NT 3.51 
            //	* Windows NT 4.0 
            //	* Windows 2000 
            //	* Windows XP 
            //	* Windows Server 2003" 

            System.OperatingSystem osInfo = System.Environment.OSVersion;

            if (osInfo.Version.Minor.ToString() == "2")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void registerASPNetWebsite(string installName, out string output)
        {
            string command = " -s W3SVC/1/ROOT/" + installName;
            string aspNetReg = "aspnet_regiis";
            output = "";

            if (this.IsDotNet40Installed())
            {
                string frameworkLocation = dotNetFramework40Directory();
                DeploymentUnitInstallJob.RunProcess(Path.Combine(frameworkLocation, aspNetReg), frameworkLocation, new string[] { command }, out output, false);
            }
            else
                throw new Exception("The required .NET Framework version 4.0 is not installed");//throw new Exception("The required .NET Framework version 2.0 is not installed");
        }

        /// <summary>
        /// Gets a suggested memory size for the cache based on
        /// the amount of memory on the machine, if SQL is installed locally
        /// and the virtual address space available on the machine
        /// </summary>
        /// <param name="isSQLInstalledLocally">Indicate if SQL is installed locally</param>
        /// <returns>The suggested cache size</returns>
        private static ulong GetSuggestedCacheMemorySize(bool isSQLInstalledLocally)
        {
            MEMORYSTATUSEX computerMemory = GetSystemMemoryStatus();

            // 2 systems e.g. N-ABLE and the OS require memory by default
            ulong numberOfSystemsRequiringMemory = 2;

            // if SQL is installed locally then this needs memory too
            if (isSQLInstalledLocally)
                numberOfSystemsRequiringMemory = 3;

            // get the total physical RAM in the machine
            ulong physicalMemory = computerMemory.TotalPhysical;

            // if dividing the memory between the systems requiring memory is
            // less than the allocated chunk size then just return the memory
            // divided equally between the applications
            if ((physicalMemory / (ulong)numberOfSystemsRequiringMemory) <= MINAPPCHUNKSIZE)
            {
                return EnsureCacheSizeDoesntExceedPermittedRAM(Convert.ToUInt64((physicalMemory / numberOfSystemsRequiringMemory)), computerMemory.TotalVirtual);
            }
            else
            {
                // otherwise, allocate max OS memory to OS and the rest divided between the other processes
                ulong remainingMemory = (physicalMemory - MAXOSRESERVEDMEMORY);
                ulong remainingSystems = numberOfSystemsRequiringMemory - 1;

                return EnsureCacheSizeDoesntExceedPermittedRAM(Convert.ToUInt64((remainingMemory / remainingSystems)), computerMemory.TotalVirtual);
            }
        }

        /// <summary>
        /// Ensures that the allocated memory does not exceed 80% of the virtual address space
        /// </summary>
        /// <param name="allocatedMemory">The memory that has been allocated to the cache</param>
        /// <param name="userSpaceVirtualMemory">The user virtual address space size</param>
        private static ulong EnsureCacheSizeDoesntExceedPermittedRAM(ulong allocatedMemory, ulong userSpaceVirtualMemory)
        {
            ulong eightyPercentOfUserAddressSpace = (ulong)(userSpaceVirtualMemory * 0.8);

            // allocate a maximim of 80% of the user virtual address space
            if (allocatedMemory > eightyPercentOfUserAddressSpace)
                return eightyPercentOfUserAddressSpace;
            else
                return allocatedMemory;
        }

        private static MEMORYSTATUSEX GetSystemMemoryStatus()
        {
            MEMORYSTATUSEX computerMemory = new MEMORYSTATUSEX(true);

            GlobalMemoryStatusEx(out computerMemory);

            return computerMemory;
        }

        [DllImport("kernel32.dll")]
        static extern void GlobalMemoryStatusEx(out MEMORYSTATUSEX lpBuffer);

        #endregion //Private Methods

    }
}
