namespace InstallBulkUpgrade
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnLoad = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnUpgrade = new System.Windows.Forms.Button();
            this.txtInstallBaseDirectory = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.btnInstallBase = new System.Windows.Forms.Button();
            this.cbxExecutableDirectory = new System.Windows.Forms.ComboBox();
            this.gbxCdImageDirectory = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbxCdImageDirectory.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLoad
            // 
            this.btnLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoad.Location = new System.Drawing.Point(21, 13);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(117, 65);
            this.btnLoad.TabIndex = 0;
            this.btnLoad.Text = "Load XML file ...";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // btnUpgrade
            // 
            this.btnUpgrade.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpgrade.Location = new System.Drawing.Point(21, 222);
            this.btnUpgrade.Name = "btnUpgrade";
            this.btnUpgrade.Size = new System.Drawing.Size(92, 59);
            this.btnUpgrade.TabIndex = 1;
            this.btnUpgrade.Text = "Bulk upgrade";
            this.btnUpgrade.UseVisualStyleBackColor = true;
            this.btnUpgrade.Click += new System.EventHandler(this.btnUpgrade_Click);
            // 
            // txtInstallBaseDirectory
            // 
            this.txtInstallBaseDirectory.Location = new System.Drawing.Point(27, 117);
            this.txtInstallBaseDirectory.Name = "txtInstallBaseDirectory";
            this.txtInstallBaseDirectory.Size = new System.Drawing.Size(288, 20);
            this.txtInstallBaseDirectory.TabIndex = 2;
            // 
            // btnInstallBase
            // 
            this.btnInstallBase.Location = new System.Drawing.Point(321, 117);
            this.btnInstallBase.Name = "btnInstallBase";
            this.btnInstallBase.Size = new System.Drawing.Size(123, 23);
            this.btnInstallBase.TabIndex = 3;
            this.btnInstallBase.Text = "Install base directory ...";
            this.btnInstallBase.UseVisualStyleBackColor = true;
            this.btnInstallBase.Click += new System.EventHandler(this.btnInstallBase_Click);
            // 
            // cbxExecutableDirectory
            // 
            this.cbxExecutableDirectory.FormattingEnabled = true;
            this.cbxExecutableDirectory.Items.AddRange(new object[] {
            "bin",
            "assembly"});
            this.cbxExecutableDirectory.Location = new System.Drawing.Point(27, 167);
            this.cbxExecutableDirectory.Name = "cbxExecutableDirectory";
            this.cbxExecutableDirectory.Size = new System.Drawing.Size(177, 21);
            this.cbxExecutableDirectory.TabIndex = 4;
            // 
            // gbxCdImageDirectory
            // 
            this.gbxCdImageDirectory.Controls.Add(this.label1);
            this.gbxCdImageDirectory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxCdImageDirectory.Location = new System.Drawing.Point(21, 97);
            this.gbxCdImageDirectory.Name = "gbxCdImageDirectory";
            this.gbxCdImageDirectory.Size = new System.Drawing.Size(430, 106);
            this.gbxCdImageDirectory.TabIndex = 5;
            this.gbxCdImageDirectory.TabStop = false;
            this.gbxCdImageDirectory.Text = "CD Image Directory";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Binaries directory";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 303);
            this.Controls.Add(this.cbxExecutableDirectory);
            this.Controls.Add(this.btnInstallBase);
            this.Controls.Add(this.txtInstallBaseDirectory);
            this.Controls.Add(this.btnUpgrade);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.gbxCdImageDirectory);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Bulk Upgrade";
            this.gbxCdImageDirectory.ResumeLayout(false);
            this.gbxCdImageDirectory.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button btnUpgrade;
        private System.Windows.Forms.TextBox txtInstallBaseDirectory;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Button btnInstallBase;
        private System.Windows.Forms.ComboBox cbxExecutableDirectory;
        private System.Windows.Forms.GroupBox gbxCdImageDirectory;
        private System.Windows.Forms.Label label1;
    }
}

