using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml;
using System.Windows.Forms;
using System.Reflection;

using log4net;
using log4net.Config;
using log4net.Appender;
using log4net.Layout;

using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Administration.SystemUpdate;
using Oritax.Administration.InstallUpgradeBase;

namespace InstallBulkUpgrade
{
    public partial class Form1 : Form
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Form1));
        private static FileStream fileStream = null;
        List<XMLStore> XMLStrings = new List<XMLStore>();
        List<SerializableInstallParameters> installParametersList = new List<SerializableInstallParameters>();
        XMLStore myXML = new XMLStore();
        XmlDocument doc = new XmlDocument();

        static Form1()
        {
            log4net.Layout.SimpleLayout simpleLayout = new SimpleLayout();
            log4net.Appender.TextWriterAppender textWriterAppender = new TextWriterAppender();
            textWriterAppender.Layout = simpleLayout;
            fileStream = new FileStream(string.Format(@"{0}\{1}", Program.bulkUpgradeLogDirectory, Program.loggingFilename), 
                FileMode.Create, FileAccess.Write, FileShare.Read);
            textWriterAppender.Writer = new StreamWriter(fileStream);
            BasicConfigurator.Configure(textWriterAppender);
        }

        public Form1()
        {
            InitializeComponent();
            this.cbxExecutableDirectory.SelectedIndex = 1;
            this.txtInstallBaseDirectory.Text = System.Environment.GetEnvironmentVariable("NABLEHOME");
            log.Info("Starting bulk installation upgrade");
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            string nableHomeDirectory = System.Environment.GetEnvironmentVariable("NABLEHOME");
            string assemblyDirectory = string.Format(@"{0}\Assembly", nableHomeDirectory);

            openFileDialog.AddExtension = true;
            openFileDialog.Filter = "XML files (*.xml)|*.xml";
            openFileDialog.FileName = "*.xml";
            openFileDialog.Title = "Open the XML file";

            //create settings
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            settings.IgnoreComments = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                XmlReader xmlIn = XmlReader.Create(openFileDialog.FileName, settings);

                System.IO.Directory.SetCurrentDirectory(assemblyDirectory);


                //read past all nodes to first instance node
                if (xmlIn.ReadToDescendant(StringValues.strXMLElement1))
                {
                    //create an instance object for each instance node
                    do
                    {
                        XMLStore xmlStore = new XMLStore();
                        //read to next element
                        xmlIn.ReadStartElement(StringValues.strXMLElement1);
                        //read info into XMLStore
                        xmlStore.InstanceClient = xmlIn.ReadContentAsString();
                        xmlIn.ReadEndElement();
                        xmlIn.ReadStartElement(StringValues.strXMLElement2);
                        xmlStore.ExistingDirectory = xmlIn.ReadContentAsString();
                        xmlIn.ReadEndElement();
                        xmlIn.ReadStartElement(StringValues.strXMLElement3);
                        xmlStore.NewDirectory = xmlIn.ReadContentAsString();
                        xmlIn.ReadEndElement();
                        xmlIn.ReadStartElement(StringValues.strXMLElement4);
                        xmlStore.SQLServerName = xmlIn.ReadContentAsString();
                        xmlIn.ReadEndElement();
                        xmlIn.ReadStartElement(StringValues.strXMLElement5);
                        xmlStore.SQLUserName = xmlIn.ReadContentAsString();
                        xmlIn.ReadEndElement();
                        xmlIn.ReadStartElement(StringValues.strXMLElement6);
                        xmlStore.SQLPassword = xmlIn.ReadContentAsString();
                        xmlIn.ReadEndElement();
                        xmlIn.ReadStartElement(StringValues.strXMLElement7);
                        xmlStore.VirtualDirectory = xmlIn.ReadContentAsString();
                        xmlIn.ReadEndElement();
                        xmlIn.ReadStartElement(StringValues.strXMLElement8);
                        xmlStore.ExistingDatabase = xmlIn.ReadContentAsString();
                        xmlIn.ReadEndElement();
                        xmlIn.ReadStartElement(StringValues.strXMLElement9);
                        xmlStore.NewDatabase = xmlIn.ReadContentAsString();
                        xmlIn.ReadEndElement();
                        xmlIn.ReadEndElement();
                        //add to list of XML items
                        XMLStrings.Add(xmlStore);
                        this.installParametersList.Add(CreateInstallParameters(xmlStore));
                    }
                    while (xmlIn.ReadToDescendant(StringValues.strXMLElement1));
                }
                xmlIn.Close();
            }
        }

        private SerializableInstallParameters CreateInstallParameters(XMLStore xmlStore)
        {
            SerializableInstallParameters installParameters = new SerializableInstallParameters();

            installParameters.installBase = this.txtInstallBaseDirectory.Text;
            installParameters.installCode = string.Format(@"{0}\{1}", this.txtInstallBaseDirectory.Text, 
                (string)this.cbxExecutableDirectory.SelectedItem); ;

            installParameters.installPackageLocation = Path.Combine(installParameters.installBase, "Cab");
            installParameters.workingLocation = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), 
                "OritaxTemp");

            installParameters.dbAdminUserName = xmlStore.SQLUserName;
            installParameters.dbAdminPassword = xmlStore.SQLPassword;
            installParameters.databaseServer = xmlStore.SQLServerName;

            installParameters.websiteLocation = xmlStore.ExistingDirectory;
            installParameters.installLocation = Directory.GetParent(installParameters.websiteLocation).FullName;

            installParameters.sourceLocation = xmlStore.ExistingDirectory;
            installParameters.targetLocation = xmlStore.NewDirectory;

            installParameters.sourceDataBaseName = xmlStore.ExistingDatabase;
            installParameters.targetDataBaseName = xmlStore.NewDatabase;

            getConnectionStringAndUserDetails(installParameters);

            installParameters.clientName = xmlStore.InstanceClient;

            return installParameters;
        }

        private void getConnectionStringAndUserDetails(SerializableInstallParameters args)
        {
            string webConfigFile = Path.Combine(args.websiteLocation, "web.config");

            args.encryptedConnectionString = WebDeployment.GetWebConfigValue(webConfigFile, "", "DBConnectionString");

            string[] connectionProperties = args.encryptedConnectionString.Split(';');

            // Locate the encrypted uid and pwd in the configuration file.
            string userName = "";
            string password = "";
            string databaseName = "";

            foreach (string property in connectionProperties)
            {
                if (property.Trim().StartsWith("uid"))
                {
                    userName = property.Substring(property.IndexOf("=") + 1);
                    if (userName != "")
                        userName = Encryption.DecryptData(userName);
                }
                else if (property.Trim().StartsWith("pwd"))
                {
                    password = property.Substring(property.IndexOf("=") + 1);
                    if (password != "")
                        password = Encryption.DecryptData(password);
                }
                else if (property.Trim().StartsWith("database"))
                {
                    databaseName = property.Substring(property.IndexOf("=") + 1);
                }
            }

            args.databaseName = databaseName;
            args.dbSysUserName = userName;
            args.dbSysPassword = password;
            args.databaseName = databaseName;
            // Build an unencrypted version of the connection string for system usage.
            string connection = null;
            foreach (string property in connectionProperties)
            {
                if (property.Trim().StartsWith("uid"))
                {
                    connection += "uid=" + userName + ";";
                }
                else if (property.Trim().StartsWith("pwd"))
                {
                    connection += "pwd=" + password + ";";
                }
                else
                    connection += property + ";";
            }

            args.connectionString = connection;
        }

        private void btnUpgrade_Click(object sender, EventArgs e)
        {
            CopyFileToInstallBase(Program.installUpgradeAssemblyFilename);
            CopyFileToInstallBase(Program.installUpgradeBaseAssemblyFilename);
            CopyFileToInstallBase(Program.log4netAssemblyFilename);
            CopyFileToInstallBase(Program.log4netConfigurationFilename);

            int clientIndex = 1;
            foreach (SerializableInstallParameters serializableInstallParameters in installParametersList)
            {
                AppDomain installerAppDomain = LoadAndExecuteInstaller(Program.installUpgradeAssemblyFilename,
                    Program.installUpgradeFullClassName, serializableInstallParameters, clientIndex);

                if (installerAppDomain != null)
                    AppDomain.Unload(installerAppDomain);

                clientIndex++;
            }
        }

        private void CopyFileToInstallBase(string installUpgradeAssemblyFilename)
        {
            string existingPathname = string.Format(@"{0}\{1}", Program.ProgramDirectory,
                installUpgradeAssemblyFilename);
            string targetPathname = string.Format(@"{0}\{1}\{2}", this.txtInstallBaseDirectory.Text,
                (string)this.cbxExecutableDirectory.SelectedItem, installUpgradeAssemblyFilename);
            if (!File.Exists(targetPathname))
                File.Copy(existingPathname, targetPathname);
        }

        private AppDomain LoadAndExecuteInstaller(string installerBin, string installerType,
            SerializableInstallParameters installParameters, int clientIndex)
        {
            log.Info("");
            log.Info("");
            log.Info(string.Format("Upgrading client: \"{0}\"", installParameters.clientName));

            InstallUpgradeBase installUpgrade = null;

            string assemblyDirectory = string.Format(@"{0}\{1}", this.txtInstallBaseDirectory.Text, 
                (string)this.cbxExecutableDirectory.SelectedItem);
            System.IO.Directory.SetCurrentDirectory(assemblyDirectory);
            log.Info(string.Format("Current directory is now: \"{0}\"", System.IO.Directory.GetCurrentDirectory()));

            AppDomainSetup appDomainSetupInfo = new AppDomainSetup();
            appDomainSetupInfo.ApplicationBase = this.txtInstallBaseDirectory.Text;
            appDomainSetupInfo.PrivateBinPath = (string)this.cbxExecutableDirectory.SelectedItem;
            appDomainSetupInfo.ShadowCopyFiles = "false";
            appDomainSetupInfo.ConfigurationFile = string.Format(@"{0}\setup.exe.config", assemblyDirectory);
            log.Info(string.Format("Configuration file is: \"{0}\"", appDomainSetupInfo.ConfigurationFile));

            AppDomain ad = AppDomain.CreateDomain(installParameters.clientName + " upgrade", null, appDomainSetupInfo);
           
            try
            {
                installUpgrade = (InstallUpgradeBase)ad.CreateInstanceFromAndUnwrap(
                    installerBin,
                    installerType,
                    false,
                    BindingFlags.CreateInstance,
                    null,
                    new object[] { Program.bulkUpgradeLogDirectory, clientIndex },
                    null,
                    null,
                    null);
            }
            catch (Exception e)
            {
                log.Error("Failed to create installation upgrade domain", e);
            }

            if (installUpgrade == null)
            {
                AppDomain.Unload(ad);
                ad = null;
            }
            else
            {
                try
                {
                    installUpgrade.Upgrade(installParameters);
                    log.Info(string.Format("Completed upgrading client: {0}", installParameters.clientName));
                }
                catch (Exception e)
                {
                    log.Error("Failed to perform upgrade", e);
                }
            }

            return ad;
        }

        private void btnInstallBase_Click(object sender, EventArgs e)
        {
            if (this.folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                this.txtInstallBaseDirectory.Text = this.folderBrowserDialog.SelectedPath;
            }
        }

    }
}