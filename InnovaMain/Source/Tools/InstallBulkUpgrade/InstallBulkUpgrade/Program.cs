using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

using log4net.Config;

namespace InstallBulkUpgrade
{
    static class Program
    {
        private static string programDirectory = string.Empty;

        public const string loggingFilename = "InstallBulkUpgradeLog.txt";

        public const string bulkUpgradeLogDirectory = @"c:\BulkUpgrade";

        public const string installUpgradeBaseAssemblyFilename = "InstallUpgradeBase_1_1.dll";

        public const string installUpgradeAssemblyFilename = "InstallUpgrade_1_1.dll";

        public const string installUpgradeFullClassName = "Oritax.Administration.InstallUpgrade.InstallUpgrade";

        public const string log4netAssemblyFilename = "log4net.dll";

        public const string log4netConfigurationFilename = "log4net.xml";

        public static string ProgramDirectory
        {
            get
            {
                return programDirectory;
            }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            programDirectory = Directory.GetCurrentDirectory();

            if (!Directory.Exists(bulkUpgradeLogDirectory))
                Directory.CreateDirectory(bulkUpgradeLogDirectory);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}