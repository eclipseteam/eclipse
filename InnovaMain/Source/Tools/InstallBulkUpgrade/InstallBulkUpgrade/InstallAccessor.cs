using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace InstallBulkUpgrade
{
    public class InstallAccessor
    {
        #region Fields

        private object installObject = null;

        #endregion // Fields

        #region Constructor

        public InstallAccessor(Assembly installUtilitiesAssembly)
        {
            Type installObjectType = installUtilitiesAssembly.GetType("Oritax.TaxSimp.Administration.SystemUpdate.Install");
            ConstructorInfo installConstructorInfo = installObjectType.GetConstructor(BindingFlags.Public | BindingFlags.Instance,
                null, new Type[] { }, null);
            installObject = installConstructorInfo.Invoke(new object[] { });
        }

        #endregion // Constructor
    }
}
