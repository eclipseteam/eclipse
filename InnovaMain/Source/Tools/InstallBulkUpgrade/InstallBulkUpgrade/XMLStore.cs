using System;
using System.Collections.Generic;
using System.Text;

namespace InstallBulkUpgrade
{
    class XMLStore
    {
        private string  newDirectory;

        public string  NewDirectory
        {
            get { return newDirectory; }
            set { newDirectory = value; }
        }
        
        private string existingDirectory;

        public string ExistingDirectory
        {
            get { return existingDirectory; }
            set { existingDirectory = value; }
        }

        private string sqlServerName;

        public string SQLServerName
        {
            get { return sqlServerName; }
            set { sqlServerName = value; }
        }
        
        private string instanceClient;

        public string InstanceClient
        {
            get { return instanceClient; }
            set { instanceClient = value; }
        }

        private string sqlUserName;

        public string SQLUserName
        {
            get { return sqlUserName; }

            set { sqlUserName = value; }
        }

        private string sqlPassword;

        public string SQLPassword
        {
            get { return sqlPassword; }
            set { sqlPassword = value; }
        }

        private string  virtualDirectory;

        public string  VirtualDirectory
        {
            get { return virtualDirectory; }
            set 
            { 
                {
                    string words = value;
                    string[] split = words.Split(new Char[] { '\\'});
                    int splitCount = split.Length;
                    virtualDirectory = split[splitCount-1];
                }           
            }
        }

        private string  existingDatabase;

        public string  ExistingDatabase
        {
            get { return existingDatabase; }
            set 
            {            
                string words = value;
                string[] split = words.Split(new Char[] { '\\'});
                int splitCount = split.Length;
                existingDatabase = split[splitCount - 1];       
            }
        }

        private string newDatabase;

        public string NewDatabase
        {
            get { return newDatabase; }
            set
            {
                string words = value;
                string[] split = words.Split(new Char[] { '\\' });
                int splitCount = split.Length;
                newDatabase = split[splitCount - 1];
            }
        }


        public XMLStore(string Instance, string ExistDirect, string NewDirect, string SQLName, string SQLUser, string SQLPass)
        {
            InstanceClient = Instance;
            ExistingDirectory = ExistDirect;
            newDirectory = NewDirect;
            SQLServerName = SQLName;
            SQLPassword = SQLPass;
            SQLUserName = SQLUser;
            VirtualDirectory = NewDirect;
            ExistingDatabase = ExistDirect;
            NewDatabase = NewDirect;
        }

        public XMLStore()
        {

        }

    }
}
