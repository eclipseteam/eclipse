using System;
using System.Collections.Generic;
using System.Text;

namespace InstallBulkUpgrade
{
    static class StringValues
    {
        //XML strings
        public static string strXMLBeginning = "Nable_Install_Start";
        public static string strXMLNode = "Instance";
        public static string strXMLElement1 = "InstanceClient";
        public static string strXMLElement2 = "Existing_Directory";
        public static string strXMLElement3 = "New_Directory";
        public static string strXMLElement4 = "SQLServerName";
        public static string strXMLElement5 = "SQLServerUser";
        public static string strXMLElement6 = "SQLServerPassword";
        public static string strXMLElement7 = "VirtualDirectory";
        public static string strXMLElement8 = "Existing_Database";
        public static string strXMLElement9 = "New_Database";

        //XML Data Strings
        public static string strXMLBeginning1 = "Config_Update_Start";
        public static string strXMLNode1 = "Instance";
        public static string strXMLElement11 = "InstanceClient";
        public static string strXMLElement21 = "DB_Connection_String";
        public static string strXMLElement31 = "Cache";
        public static string strXMLElement41 = "TimeOut";


        //label strings
        public static string strLabelInstance = "Instance/Client";
        public static string strExDirectory = "Existing Directory";
        public static string strNewDirectory = "New Directory";
        public static string strSQLName = "SQL Server Name";
        public static string strSQLUSer = "SQL User Name";
        public static string strSQLPassword = "SQL Password";
        public static string strTotals = "Total Instances";
        public static string strDBConn = "DB Connection String";
        public static string strTimeOut = "Time Out";
        public static string strCache = "Cache Size";
        public static string strMByte = "MByte";
        public static string strCacheSize = "Set Cache Size";
        public static string strNoWebConfig = "No Web.config loaded";
        public static string strWebConfig = "Web.config loaded";
        public static string strMinutes = "Minutes";
    }
}
