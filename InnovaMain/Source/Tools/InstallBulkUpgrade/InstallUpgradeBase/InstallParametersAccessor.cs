using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Oritax.Administration.InstallUpgradeBase
{
    public class InstallParametersAccessor
    {
        #region Fields

        private Type installObjectType = null;
        private Object installParametersObject = null;

        private FieldInfo installBaseField = null;
        private FieldInfo installPackageLocationField = null;
        private FieldInfo workingLocationField = null;
        private FieldInfo websiteLocationField = null;
        private FieldInfo installCodeField = null;
        private FieldInfo installLocationField = null;

        private FieldInfo databaseNameField = null;
        private FieldInfo databaseServerField = null;
        private FieldInfo dbSysUserNameField = null;
        private FieldInfo dbSysPasswordField = null;
        private FieldInfo dbAdminUserNameField = null;
        private FieldInfo dbAdminPasswordField = null;
        private FieldInfo nableAdminUserNameField = null;
        private FieldInfo nableAdminPasswordField = null;

        private FieldInfo changedbSysUserField = null;
        private FieldInfo changeNableAdminUserField = null;

        private FieldInfo connectionStringField = null;
        private FieldInfo adminConnectionStringField = null;
        private FieldInfo encryptedConnectionStringField = null;

        private FieldInfo sourceLocationField = null;
        private FieldInfo targetLocationField = null;
        private FieldInfo sourceDataBaseNameField = null;
        private FieldInfo targetDataBaseNameField = null;
        FieldInfo commandLineInstallField = null;

        #endregion // Fields

        #region Constructor

        public InstallParametersAccessor(Assembly installUtilitiesAssembly)
        {
            this.installObjectType = installUtilitiesAssembly.GetType("Oritax.TaxSimp.Administration.SystemUpdate.InstallParameters");

            installParametersObject = Activator.CreateInstance(this.installObjectType);

            installBaseField = installObjectType.GetField("installBase", BindingFlags.Instance | BindingFlags.Public);
            installPackageLocationField = installObjectType.GetField("installPackageLocation", BindingFlags.Instance | BindingFlags.Public);
            workingLocationField = installObjectType.GetField("workingLocation", BindingFlags.Instance | BindingFlags.Public);
            websiteLocationField = installObjectType.GetField("websiteLocation", BindingFlags.Instance | BindingFlags.Public);
            installCodeField = installObjectType.GetField("installCode", BindingFlags.Instance | BindingFlags.Public);
            installLocationField = installObjectType.GetField("installLocation", BindingFlags.Instance | BindingFlags.Public);

            databaseNameField = installObjectType.GetField("databaseName", BindingFlags.Instance | BindingFlags.Public);
            databaseServerField = installObjectType.GetField("databaseServer", BindingFlags.Instance | BindingFlags.Public);
            dbSysUserNameField = installObjectType.GetField("dbSysUserName", BindingFlags.Instance | BindingFlags.Public);
            dbSysPasswordField = installObjectType.GetField("dbSysPassword", BindingFlags.Instance | BindingFlags.Public);
            dbAdminUserNameField = installObjectType.GetField("dbAdminUserName", BindingFlags.Instance | BindingFlags.Public);
            dbAdminPasswordField = installObjectType.GetField("dbAdminPassword", BindingFlags.Instance | BindingFlags.Public);
            nableAdminUserNameField = installObjectType.GetField("nableAdminUserName", BindingFlags.Instance | BindingFlags.Public);
            nableAdminPasswordField = installObjectType.GetField("nableAdminPassword", BindingFlags.Instance | BindingFlags.Public);

            changedbSysUserField = installObjectType.GetField("changedbSysUser", BindingFlags.Instance | BindingFlags.Public);
            changeNableAdminUserField = installObjectType.GetField("changeNableAdminUser", BindingFlags.Instance | BindingFlags.Public);

            connectionStringField = installObjectType.GetField("connectionString", BindingFlags.Instance | BindingFlags.Public);
            adminConnectionStringField = installObjectType.GetField("adminConnectionString", BindingFlags.Instance | BindingFlags.Public);
            encryptedConnectionStringField = installObjectType.GetField("encryptedConnectionString", BindingFlags.Instance | BindingFlags.Public);

            sourceLocationField = installObjectType.GetField("sourceLocation", BindingFlags.Instance | BindingFlags.Public);
            targetLocationField = installObjectType.GetField("targetLocation", BindingFlags.Instance | BindingFlags.Public);
            sourceDataBaseNameField = installObjectType.GetField("sourceDataBaseName", BindingFlags.Instance | BindingFlags.Public);
            targetDataBaseNameField = installObjectType.GetField("targetDataBaseName", BindingFlags.Instance | BindingFlags.Public);
            commandLineInstallField = installObjectType.GetField("commandLineInstall", BindingFlags.Instance | BindingFlags.Public);
        }

        #endregion // Constructor

        #region Public Properties

        public object Target
        {
            get
            {
                return this.installParametersObject;
            }
        }

        public string InstallBase
        {
            get
            {
                return (string)this.installBaseField.GetValue(installParametersObject);
            }
            set
            {
                this.installBaseField.SetValue(installParametersObject, value);
            }
        }

        public string InstallPackageLocation
        {
            get
            {
                return (string)this.installPackageLocationField.GetValue(installParametersObject);
            }
            set
            {
                this.installPackageLocationField.SetValue(installParametersObject, value);
            }
        }

        public string WorkingLocation
        {
            get
            {
                return (string)this.workingLocationField.GetValue(installParametersObject);
            }
            set
            {
                this.workingLocationField.SetValue(installParametersObject, value);
            }
        }


        public string WebsiteLocation
        {
            get
            {
                return (string)this.websiteLocationField.GetValue(installParametersObject);
            }
            set
            {
                this.websiteLocationField.SetValue(installParametersObject, value);
            }
        }


        public string InstallCode
        {
            get
            {
                return (string)this.installCodeField.GetValue(installParametersObject);
            }
            set
            {
                this.installCodeField.SetValue(installParametersObject, value);
            }
        }


        public string InstallLocation
        {
            get
            {
                return (string)this.installLocationField.GetValue(installParametersObject);
            }
            set
            {
                this.installLocationField.SetValue(installParametersObject, value);
            }
        }



        public string DatabaseName
        {
            get
            {
                return (string)this.databaseNameField.GetValue(installParametersObject);
            }
            set
            {
                this.databaseNameField.SetValue(installParametersObject, value);
            }
        }


        public string DatabaseServer
        {
            get
            {
                return (string)this.databaseServerField.GetValue(installParametersObject);
            }
            set
            {
                this.databaseServerField.SetValue(installParametersObject, value);
            }
        }


        public string DbSysUserName
        {
            get
            {
                return (string)this.dbSysUserNameField.GetValue(installParametersObject);
            }
            set
            {
                this.dbSysUserNameField.SetValue(installParametersObject, value);
            }
        }


        public string DbSysPassword
        {
            get
            {
                return (string)this.dbSysPasswordField.GetValue(installParametersObject);
            }
            set
            {
                this.dbSysPasswordField.SetValue(installParametersObject, value);
            }
        }


        public string DbAdminUserName
        {
            get
            {
                return (string)this.dbAdminUserNameField.GetValue(installParametersObject);
            }
            set
            {
                this.dbAdminUserNameField.SetValue(installParametersObject, value);
            }
        }


        public string DbAdminPassword
        {
            get
            {
                return (string)this.dbAdminPasswordField.GetValue(installParametersObject);
            }
            set
            {
                this.dbAdminPasswordField.SetValue(installParametersObject, value);
            }
        }


        public string NableAdminUserName
        {
            get
            {
                return (string)this.nableAdminUserNameField.GetValue(installParametersObject);
            }
            set
            {
                this.nableAdminUserNameField.SetValue(installParametersObject, value);
            }
        }


        public string NableAdminPassword
        {
            get
            {
                return (string)this.nableAdminPasswordField.GetValue(installParametersObject);
            }
            set
            {
                this.nableAdminPasswordField.SetValue(installParametersObject, value);
            }
        }



        public bool ChangedbSysUser
        {
            get
            {
                return (bool)this.changedbSysUserField.GetValue(installParametersObject);
            }
            set
            {
                this.changedbSysUserField.SetValue(installParametersObject, value);
            }
        }


        public bool ChangeNableAdminUser
        {
            get
            {
                return (bool)this.changeNableAdminUserField.GetValue(installParametersObject);
            }
            set
            {
                this.changeNableAdminUserField.SetValue(installParametersObject, value);
            }
        }



        public string ConnectionString
        {
            get
            {
                return (string)this.connectionStringField.GetValue(installParametersObject);
            }
            set
            {
                this.connectionStringField.SetValue(installParametersObject, value);
            }
        }


        public string AdminConnectionString
        {
            get
            {
                return (string)this.adminConnectionStringField.GetValue(installParametersObject);
            }
            set
            {
                this.adminConnectionStringField.SetValue(installParametersObject, value);
            }
        }


        public string EncryptedConnectionString
        {
            get
            {
                return (string)this.encryptedConnectionStringField.GetValue(installParametersObject);
            }
            set
            {
                this.encryptedConnectionStringField.SetValue(installParametersObject, value);
            }
        }



        public string SourceLocation
        {
            get
            {
                return (string)this.sourceLocationField.GetValue(installParametersObject);
            }
            set
            {
                this.sourceLocationField.SetValue(installParametersObject, value);
            }
        }


        public string TargetLocation
        {
            get
            {
                return (string)this.targetLocationField.GetValue(installParametersObject);
            }
            set
            {
                this.targetLocationField.SetValue(installParametersObject, value);
            }
        }


        public string SourceDataBaseName
        {
            get
            {
                return (string)this.sourceDataBaseNameField.GetValue(installParametersObject);
            }
            set
            {
                this.sourceDataBaseNameField.SetValue(installParametersObject, value);
            }
        }


        public string TargetDataBaseName
        {
            get
            {
                return (string)this.targetDataBaseNameField.GetValue(installParametersObject);
            }
            set
            {
                this.targetDataBaseNameField.SetValue(installParametersObject, value);
            }
        }


        public bool CommandLineInstall
        {
            get
            {
                return (bool)this.commandLineInstallField.GetValue(installParametersObject);
            }
            set
            {
                this.commandLineInstallField.SetValue(installParametersObject, value);
            }
        }



        #endregion // Public Properties
    }
}
