using System;
using System.Collections.Generic;
using System.Text;

using Oritax.TaxSimp.Administration.SystemUpdate;

namespace Oritax.Administration.InstallUpgradeBase
{
    public abstract class InstallUpgradeBase : MarshalByRefObject
    {
        public abstract void Upgrade(SerializableInstallParameters serializableInstallParameters);

        public abstract bool Finished
        {
            get;
        }
    }
}
