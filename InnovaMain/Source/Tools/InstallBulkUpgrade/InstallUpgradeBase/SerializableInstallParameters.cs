using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;

using Oritax.TaxSimp.Administration.SystemUpdate;

namespace Oritax.Administration.InstallUpgradeBase
{
    [Serializable]
    public class SerializableInstallParameters
    {

        #region Fields

        public string clientName;

        public string installBase;
        public string installPackageLocation;
        public string workingLocation;
        public string websiteLocation;
        public string installCode;
        public string installLocation;

        public string databaseName;
        public string databaseServer;
        public string dbSysUserName;
        public string dbSysPassword;
        public string dbAdminUserName;
        public string dbAdminPassword;
        public string nableAdminUserName;
        public string nableAdminPassword;

        public bool changedbSysUser;
        public bool changeNableAdminUser;

        public string connectionString;
        public string adminConnectionString;
        public string encryptedConnectionString;

        public string sourceLocation;
        public string targetLocation;
        public string sourceDataBaseName;
        public string targetDataBaseName;
        public bool commandLineInstall;

        #endregion // Fields

        #region Constructors

        public SerializableInstallParameters()
        {
            this.installBase = string.Empty;
            this.installPackageLocation = string.Empty;
            this.workingLocation = string.Empty;
            this.websiteLocation = string.Empty;
            this.installCode = string.Empty;
            this.installLocation = string.Empty;

            this.databaseName = string.Empty;
            this.databaseServer = string.Empty;
            this.dbSysUserName = string.Empty;
            this.dbSysPassword = string.Empty;
            this.dbAdminUserName = string.Empty;
            this.dbAdminPassword = string.Empty;
            this.nableAdminUserName = string.Empty;
            this.nableAdminPassword = string.Empty;

            this.changedbSysUser = false;
            this.changeNableAdminUser = false;

            this.connectionString = string.Empty;
            this.adminConnectionString = string.Empty;
            this.encryptedConnectionString = string.Empty;

            this.sourceLocation = string.Empty;
            this.targetLocation = string.Empty;
            this.sourceDataBaseName = string.Empty;
            this.targetDataBaseName = string.Empty;
            this.commandLineInstall = false;
        }

        public SerializableInstallParameters(InstallParameters installParameters)
        {
            this.installBase = installParameters.installBase;
            this.installPackageLocation = installParameters.installPackageLocation;
            this.workingLocation = installParameters.workingLocation;
            this.websiteLocation = installParameters.websiteLocation;
            this.installCode = installParameters.installCode;
            this.installLocation = installParameters.installLocation;

            this.databaseName = installParameters.databaseName;
            this.databaseServer = installParameters.databaseServer;
            this.dbSysUserName = installParameters.dbSysUserName;
            this.dbSysPassword = installParameters.dbSysPassword;
            this.dbAdminUserName = installParameters.dbAdminUserName;
            this.dbAdminPassword = installParameters.dbAdminPassword;
            this.nableAdminUserName = installParameters.nableAdminUserName;
            this.nableAdminPassword = installParameters.nableAdminPassword;

            this.changedbSysUser = installParameters.changedbSysUser;
            this.changeNableAdminUser = installParameters.changeNableAdminUser;

            this.connectionString = installParameters.connectionString;
            this.adminConnectionString = installParameters.adminConnectionString;
            this.encryptedConnectionString = installParameters.encryptedConnectionString;

            this.sourceLocation = installParameters.sourceLocation;
            this.targetLocation = installParameters.targetLocation;
            this.sourceDataBaseName = installParameters.sourceDataBaseName;
            this.targetDataBaseName = installParameters.targetDataBaseName;
            this.commandLineInstall = installParameters.commandLineInstall;
        }

        #endregion // Constructors

        #region Public Methods
        public InstallParameters Convert()
        {
            InstallParameters installParameters = new InstallParameters();

            installParameters.installBase = this.installBase;
            installParameters.installPackageLocation = this.installPackageLocation;
            installParameters.workingLocation = this.workingLocation;
            installParameters.websiteLocation = this.websiteLocation;
            installParameters.installCode = this.installCode;
            installParameters.installLocation = this.installLocation;

            installParameters.databaseName = this.databaseName;
            installParameters.databaseServer = this.databaseServer;
            installParameters.dbSysUserName = this.dbSysUserName;
            installParameters.dbSysPassword = this.dbSysPassword;
            installParameters.dbAdminUserName = this.dbAdminUserName;
            installParameters.dbAdminPassword = this.dbAdminPassword;
            installParameters.nableAdminUserName = this.nableAdminUserName;
            installParameters.nableAdminPassword = this.nableAdminPassword;

            installParameters.changedbSysUser = this.changedbSysUser;
            installParameters.changeNableAdminUser = this.changeNableAdminUser;

            installParameters.connectionString = this.connectionString;
            installParameters.adminConnectionString = this.adminConnectionString;
            installParameters.encryptedConnectionString = this.encryptedConnectionString;

            installParameters.sourceLocation = this.sourceLocation;
            installParameters.targetLocation = this.targetLocation;
            installParameters.sourceDataBaseName = this.sourceDataBaseName;
            installParameters.targetDataBaseName = this.targetDataBaseName;
            installParameters.commandLineInstall = this.commandLineInstall;

            return installParameters;
        }

        public InstallParametersAccessor Convert(Assembly installAssembly)
        {
            InstallParametersAccessor installParameters = new InstallParametersAccessor(installAssembly);

            installParameters.InstallBase = this.installBase;
            installParameters.InstallPackageLocation = this.installPackageLocation;
            installParameters.WebsiteLocation = this.workingLocation;
            installParameters.WebsiteLocation = this.websiteLocation;
            installParameters.InstallCode = this.installCode;
            installParameters.InstallLocation = this.installLocation;

            installParameters.DatabaseName = this.databaseName;
            installParameters.DatabaseServer = this.databaseServer;
            installParameters.DbSysUserName = this.dbSysUserName;
            installParameters.DbSysPassword = this.dbSysPassword;
            installParameters.DbAdminUserName = this.dbAdminUserName;
            installParameters.DbAdminPassword = this.dbAdminPassword;
            installParameters.NableAdminUserName = this.nableAdminUserName;
            installParameters.NableAdminPassword = this.nableAdminPassword;

            installParameters.ChangedbSysUser = this.changedbSysUser;
            installParameters.ChangeNableAdminUser = this.changeNableAdminUser;

            installParameters.ConnectionString = this.connectionString;
            installParameters.AdminConnectionString = this.adminConnectionString;
            installParameters.EncryptedConnectionString = this.encryptedConnectionString;

            installParameters.SourceLocation = this.sourceLocation;
            installParameters.TargetLocation = this.targetLocation;
            installParameters.SourceDataBaseName = this.sourceDataBaseName;
            installParameters.TargetDataBaseName = this.targetDataBaseName;
            installParameters.CommandLineInstall = this.commandLineInstall;

            return installParameters;
        }

        #endregion // Public Methods
    }
}
