using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Oritax.Administration.InstallUpgrade
{
    public class InstallAccessor
    {
        #region Fields

        private Type installObjectType = null;
        private object installObject = null;

        EventInfo updateComponentProgressUpdateEvent = null;
        EventInfo logMigrationInformationEvent = null;
        EventInfo updateStepProgressUpdateEvent = null;
        EventInfo installErrorEvent = null;
        EventInfo migrationInformationEvent = null;

            //install.UpdateComponentProgressUpdate += new UpdateProgressComponentUpdateHandler(install_UpdateComponentProgressUpdate);
            //install.LogMigrationInformation += new UpdateMessageHandler(migration_MigrationInformation);
            //install.UpdateStepProgressUpdate += new UpdateProgressStepUpdateHandler(install_UpdateStepProgressUpdate);
            //install.InstallError += new InstallErrorHandler(install_InstallError);
            //install.MigrationInformation += new MigrationInformationHandler(install_MigrationInformation);

        #endregion // Fields

        #region Constructor

        public InstallAccessor(Assembly installUtilitiesAssembly)
        {
            installObjectType = installUtilitiesAssembly.GetType("Oritax.TaxSimp.Administration.SystemUpdate.Install");
            ConstructorInfo installConstructorInfo = installObjectType.GetConstructor(BindingFlags.Public | BindingFlags.Instance,
                null, new Type[] { }, null);
            installObject = installConstructorInfo.Invoke(new object[] { });

            this.updateComponentProgressUpdateEvent = installObjectType.GetEvent("UpdateComponentProgressUpdate",
                BindingFlags.Public | BindingFlags.Instance);
            this.logMigrationInformationEvent = installObjectType.GetEvent("LogMigrationInformation",
                BindingFlags.Public | BindingFlags.Instance);
            this.updateStepProgressUpdateEvent = installObjectType.GetEvent("UpdateStepProgressUpdate",
                BindingFlags.Public | BindingFlags.Instance);
            this.installErrorEvent = installObjectType.GetEvent("InstallError",
                BindingFlags.Public | BindingFlags.Instance);
            this.migrationInformationEvent = installObjectType.GetEvent("MigrationInformation",
                BindingFlags.Public | BindingFlags.Instance);
        }

        #endregion // Constructor

        #region Public Properties

        public object Target
        {
            get
            {
                return this.installObject;
            }
        }

        public EventInfo UpdateComponentProgressUpdateEvent
        {
            get
            {
                return this.updateComponentProgressUpdateEvent;
            }
        }

        public EventInfo UpdateStepProgressUpdateEvent
        {
            get
            {
                return this.updateStepProgressUpdateEvent;
            }
        }

        #endregion // Public Properties

        #region Public Methods

        public void UpdateFullProduct(Object parameters)
        {
            MethodInfo updateFullProductMethodsInfo = this.installObjectType.GetMethod("UpdateFullProduct",
                BindingFlags.Instance | BindingFlags.Public);
            updateFullProductMethodsInfo.Invoke(this.installObject, new object[] { parameters });
        }

        #endregion // Public Methods
    }
}
