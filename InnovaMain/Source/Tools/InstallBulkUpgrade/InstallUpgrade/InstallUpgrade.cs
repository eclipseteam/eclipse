using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

using log4net;
using log4net.Config;
using log4net.Appender;
using log4net.Layout;

using Oritax.Administration.InstallUpgradeBase;
using Oritax.Nable.UpdateUtilities;
using Oritax.TaxSimp.Administration.SystemUpdate;

namespace Oritax.Administration.InstallUpgrade
{
    public class InstallUpgrade : InstallUpgradeBase.InstallUpgradeBase
    {
        #region Fields

        private static readonly ILog log = LogManager.GetLogger(typeof(InstallUpgrade));
        private static FileStream fileStream = null;

        private bool finished = false;

        #endregion // Fields

        #region Constructors

        public InstallUpgrade(string logDirectory, int clientIndex)
        {
            log4net.Layout.SimpleLayout simpleLayout = new SimpleLayout();
            log4net.Appender.TextWriterAppender textWriterAppender = new TextWriterAppender();
            textWriterAppender.Layout = simpleLayout;
            string filename = string.Format(@"{0}\s{1:D2}_{2}_Log.txt", logDirectory, clientIndex, AppDomain.CurrentDomain.FriendlyName);
            fileStream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.Read);
            textWriterAppender.Writer = new StreamWriter(fileStream);
            BasicConfigurator.Configure(textWriterAppender);
        }

        #endregion // Constructors

        #region Public Properties

        public override bool Finished
        {
            get 
            {
                return finished;
            }
        }

        #endregion // Public Properties

        #region Public Methods

        public override void Upgrade(SerializableInstallParameters serializableInstallParameters)
        {
            Directory.SetCurrentDirectory(@"c:\Program Files\Oritax");

            Assembly installUtilitiesAssembly = Assembly.LoadFile(string.Format("{0}/{1}", serializableInstallParameters.installCode,
                "InstallUtilities_1_1.dll"));

            InstallParametersAccessor installParameters = serializableInstallParameters.Convert(installUtilitiesAssembly);
            log.Info(string.Format("{0} - Commencing upgrade of {1}\n", DateTime.Now.ToLongTimeString(), 
                serializableInstallParameters.clientName));

            Type installParametersType = installParameters.GetType();

            foreach (FieldInfo fieldInfo in installParametersType.GetFields(BindingFlags.Public | BindingFlags.Instance))
            {
                log.Info(string.Format("{0}: {1}\n", fieldInfo.Name, fieldInfo.GetValue(installParameters)));
            }

            InstallAccessor installAccessor = new InstallAccessor(installUtilitiesAssembly);

            EventInfo updateStepProgressUpdateEventInfo = installAccessor.UpdateStepProgressUpdateEvent;

            Type updateProgressStepUpdateHandlerType = installUtilitiesAssembly.GetType(
                "Oritax.Nable.UpdateUtilities.UpdateProgressComponentUpdateHandler");

            ConstructorInfo[] updateProgressStepUpdateConstructors = updateProgressStepUpdateHandlerType.GetConstructors();

            object updateProgressStepUpdateEventHandler = updateProgressStepUpdateConstructors[0].Invoke(new object[] { this, (IntPtr)install_UpdateStepProgressUpdate });

            updateStepProgressUpdateEventInfo.AddEventHandler(installAccessor.Target, (Delegate)updateProgressStepUpdateEventHandler);

            Install install = new Install();



            install.UpdateComponentProgressUpdate += new UpdateProgressComponentUpdateHandler(install_UpdateComponentProgressUpdate);
            install.LogMigrationInformation += new UpdateMessageHandler(migration_MigrationInformation);
            install.UpdateStepProgressUpdate += new UpdateProgressStepUpdateHandler(install_UpdateStepProgressUpdate);
            install.InstallError += new InstallErrorHandler(install_InstallError);
            install.MigrationInformation += new MigrationInformationHandler(install_MigrationInformation);

            installAccessor.UpdateFullProduct(installParameters.Target);
        }

        #endregion // Public Methods

        #region Private Methods

        private void install_InstallError(object sender, EventArgs e)
        {
            InstallErrorEventArgs eventArgs = (InstallErrorEventArgs)e;
            log.Error(eventArgs.Message);
            Cancel();
        }

        private void Cancel()
        {
            finished = true;
        }

        public void install_UpdateStepProgressUpdate(object sender, EventArgs e)
        {
            Type updateProgressStepEventArgsType = e.GetType();

            PropertyInfo stepProgressPropertyInfo = updateProgressStepEventArgsType.GetProperty("StepProgress",
                BindingFlags.Instance |BindingFlags.Public);

            object stepProgress = stepProgressPropertyInfo.GetValue(e, null);

            switch (stepProgress.ToString())
            {
                case "Cloning":
                    {
                        break;
                    }
                case "Updating":
                    {
                        break;
                    }
                case "InstallingSystemComponents":
                    {
                        break;
                    }
                case "Finished":
                    {
                        finished = true;
                        break;
                    }
                default:
                    break;
            }

            log.Info("");
            log.Info("Performing " + stepProgress.ToString() + " step");
        }

        private void migration_MigrationInformation(object sender, EventArgs e)
        {
            UpdateMessageEventArgs eventArgs = (UpdateMessageEventArgs)e;
            log.Info(eventArgs.Message);
        }

        private void install_UpdateComponentProgressUpdate(object sender, EventArgs e)
        {
            UpdateProgressComponentEventArgs eventArgs = (UpdateProgressComponentEventArgs)e;
            log.Info("");
            log.Info(string.Format("{0}", eventArgs.ComponentName));
            log.Info(string.Format("Progress - {0}", eventArgs.Progress));
            log.Info(string.Format("Progress percentage - {0}", eventArgs.ProgressPercentage));
        }

        private void install_MigrationInformation(object sender, EventArgs e)
        {
            MigrationInformationEventArgs eventArgs = (MigrationInformationEventArgs)e;
            log.Info(eventArgs.Message);
        }

        public delegate void UpdateHandler(object sender, EventArgs e);

        #endregion // Private Methods
    }
}
