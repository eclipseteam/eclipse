using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Data.SqlClient;
using System.Threading;

using Oritax.TaxSimp.Administration.SystemUpdate;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.TaxSimpLicence;
using Oritax.TaxSimp.CM.Organization;

namespace Oritax.TaxSimp.Install.InstallCommand
{
	/// <summary>
	/// Encapulates the command line driven install process.
	/// </summary>
	class InstallCommand
    {
        #region PUBLIC CONSTANTS

        public const string INSTALLFROM_PARAMETER		=	"INSTALLFROM";		// Source path to install from
		public const string DBNAME_PARAMETER			=	"DBNAME";			// Name of the NAble Database to be created
		public const string DBSERVER_PARAMETER			=	"DBSERVER";			// Host on which the SQL databse server exists
		public const string WEBSERVER_PARAMETER			=	"WEBSERVER";		// Host on which the NAble web site is to be created
		public const string WEBSITENAME_PARAMETER		=	"WEBSITENAME";		// Name of the NAble web site
		public const string WEBSITELOCATION_PARAMETER	=	"WEBSITELOCATION";	// Location of the virtual directory that will contain the site
		public const string DBSYSUSERNAME_PARAMETER		=	"DBSYSUSERNAME";	// The DB user name for the N-Able application to use in its connection string
		public const string DBSYSUSERPASSWORD_PARAMETER	=	"DBSYSUSERPASSWORD";// The DB user password for the N-Able application to use in its connection string
		public const string DBADMINUSERNAME_PARAMETER	=	"DBADMINUSERNAME";	// The DB user name for the N-Able application to use in its connection string
		public const string DBADMINPASSWORD_PARAMETER	=	"DBADMINPASSWORD";	// The DB user password for the N-Able application to use in its connection string
		public const string PARAMETERS_PARAMETER		=	"PARAMETERS";		// Take the parameters from a file

		public const string INSTALL_PARAMETER			=	"INSTALL";
		public const string REPAIR_PARAMETER			=	"REPAIR";
		public const string UPDATE_PARAMETER			=	"UPDATE";
		public const string CREATEDB_PARAMETER			=	"CREATEDB";
		public const string DEVUSERS_PARAMETER			=	"DEVUSERS";
		public const string TESTUSERS_PARAMETER			=	"TESTUSERS";
		public const string ADMINUSER_PARAMETER			=	"ADMINUSER";
		public const string CREATEORG_PARAMETER			=	"CREATEORG";
		public const string QM_PARAMETER				=	"?";
		public const string HELP_PARAMETER				=	"HELP";

		#endregion

		#region INTERNAL CLASSES
		
		
		public class DBCreateException : Exception
		{
			public DBCreateException(string message) : base(message){}
		}



		/// <summary>
		/// Implements a hash table for the rest of the program to look up arguments.
		/// </summary>
		public class CommandLineArguments : Hashtable
		{
			public string[] ParameterlessArgs=null;
			public string[] ParameterArgs=null;

			public bool Contains(string name){return this.ContainsKey(name);}

			public class CommandLineArgumentsException : Exception
			{
				public CommandLineArgumentsException(string message) : base(message){}
			}

			public string this[string key]
			{
				get{return (string)base[key.ToUpper()];}
			}

			// The following syntax is implemented:
			//  -XYZ -Y -ZX ...  where XYZ, Y and ZX are switch character (or /XYZ /Y /ZX)
			//  (switches may consist of only one character.)
			//  In addition switches may optionally have arguments which are presented in the form:
			//  -X:AAA where AAA is a string argument of any length deliminated by the following space or the end of the command line
			public CommandLineArguments(string[] args, string[] allowedParameterArgs,string[] allowedParameterlessArgs)
			{
				int unNamedArgIndex=1;
				string nameString;
				string valueString;

				ParameterlessArgs=allowedParameterlessArgs;
				ParameterArgs=allowedParameterArgs;

				foreach(string argString in args)
				{
					if(argString[0]!='-' && argString[0]!='/')
					{
						// Unnamed argument
						nameString="ARG"+unNamedArgIndex.ToString();
						this.Add(nameString,argString);
						unNamedArgIndex++;
					}
					else
					{
						string argString1=argString.Substring(1);

						string[] argStringNameValue=argString1.Split(new char[]{':'},2);

						if(argStringNameValue.Length==1)
						{
							nameString=argStringNameValue[0].ToUpper();
							valueString="PRESENT";

							if(!IsInAllowedList(ParameterlessArgs,nameString))
								throw new CommandLineArgumentsException("Unknown command line argument: "+nameString);
						}
						else
						{
							// Named argument
							nameString=argStringNameValue[0].ToUpper();
							valueString=argStringNameValue[1];

							if(!IsInAllowedList(ParameterArgs,nameString))
								throw new CommandLineArgumentsException("Unknown command line argument: "+nameString);
						}

						if(this.ContainsKey(nameString))
							throw new CommandLineArgumentsException("Invalid command line argument: "+argString);

						this.Add(nameString,valueString);
					}
				}
			}

			public CommandLineArguments(string[] args)
				: this(args,null,null){}

			private bool IsInAllowedList(string[] allowedArgs,string nameString)
			{
				if(null==allowedArgs)
					return true;

				foreach(string allowedArg in allowedArgs)
					if(nameString==allowedArg)
						return true;

				return false;
			}
		}

		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			string[] allowedParameterArguments=new string[]{
																INSTALLFROM_PARAMETER,		// Source path to install from
																DBNAME_PARAMETER,			// Name of the NAble Database to be created
																DBSERVER_PARAMETER,			// Host on which the SQL databse server exists
																WEBSERVER_PARAMETER,		// Host on which the NAble web site is to be created
																WEBSITENAME_PARAMETER,		// Name of the NAble web site
																WEBSITELOCATION_PARAMETER,	// Location of the virtual directory that will contain the site
																DBSYSUSERNAME_PARAMETER,	// The DB user name for the N-Able application to use in its connection string
																DBSYSUSERPASSWORD_PARAMETER,// The DB user password for the N-Able application to use in its connection string
																DBADMINUSERNAME_PARAMETER,	// The DB user name for the N-Able application to use in its connection string
																DBADMINPASSWORD_PARAMETER,	// The DB user password for the N-Able application to use in its connection string
																PARAMETERS_PARAMETER		// Take the parameters from a file
														   };

			string[] allowedParameterlessArguments=new string[]{
																INSTALL_PARAMETER,
																REPAIR_PARAMETER,
																UPDATE_PARAMETER,
																CREATEDB_PARAMETER,
																CREATEORG_PARAMETER,
																CREATEDB_PARAMETER,
																DEVUSERS_PARAMETER,
																TESTUSERS_PARAMETER,
																ADMINUSER_PARAMETER,
																QM_PARAMETER,
																HELP_PARAMETER
															   };

			CommandLineArguments commandLineArguments=new CommandLineArguments(args,allowedParameterArguments,allowedParameterlessArguments);

			string installPackageLocation=commandLineArguments[INSTALLFROM_PARAMETER];
			string workingLocation=Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "OritaxTemp");
			string websiteLocation=commandLineArguments[WEBSITELOCATION_PARAMETER];
            string installCode = Path.Combine(System.Environment.GetEnvironmentVariable("ECLIPSEHOME"), "assembly");

			string databaseName=commandLineArguments[DBNAME_PARAMETER];
			string databaseServer=commandLineArguments[DBSERVER_PARAMETER];
			string dbSysUserName=commandLineArguments[DBSYSUSERNAME_PARAMETER];
			string dbSysUserPassword=commandLineArguments[DBSYSUSERPASSWORD_PARAMETER];

			string databaseAdminName=commandLineArguments[DBADMINUSERNAME_PARAMETER];
			string databaseAdminPassword=commandLineArguments[DBADMINPASSWORD_PARAMETER];

			string connectionString="server="+databaseServer+";database="+databaseName+";uid="+dbSysUserName+";pwd="+dbSysUserPassword;
			SetupWorkingFolder(workingLocation, installCode);


			if(commandLineArguments.ContainsKey("?") || commandLineArguments.ContainsKey("HELP"))
			{
				DisplayHelp();
			}

			if(commandLineArguments.ContainsKey(CREATEDB_PARAMETER))
			{
				SetupDB(commandLineArguments);
			}

			if(commandLineArguments.ContainsKey(INSTALL_PARAMETER))
			{
				Console.WriteLine("Installing system ...\n");

				CreateTargetDirectory(websiteLocation);

				DatabaseManager databaseManager = new DatabaseManager(databaseName, databaseServer, databaseAdminName, databaseAdminPassword);

				string buildNumber = GetBuildNumber(installCode, websiteLocation, connectionString);
                databaseManager.CreateProductVersionTable();
                databaseManager.UpdateBuildVersionInTheDatabase(buildNumber);

				DeploymentPackageInstallJob deploymentPackageInstallJob=new DeploymentPackageInstallJob(
					installPackageLocation,
					workingLocation,
					websiteLocation,
					installCode,
					connectionString
					);

				deploymentPackageInstallJob.Install(DeploymentPackageInstallJob.InstallJobType.NEW_INSTALLATION);

				Console.WriteLine("System installed.\n");
			}

			if(commandLineArguments.ContainsKey(UPDATE_PARAMETER))
			{
				Console.WriteLine("Updating system ...\n");

				DeploymentPackageInstallJob deploymentPackageInstallJob=new DeploymentPackageInstallJob(
					installPackageLocation,
					workingLocation,
					websiteLocation,
					installCode,
					connectionString
					);

				deploymentPackageInstallJob.Install(DeploymentPackageInstallJob.InstallJobType.UPDATE);

				Console.WriteLine("System updated.\n");
			}

			if(commandLineArguments.ContainsKey(REPAIR_PARAMETER))
			{
				DeploymentPackageInstallJob deploymentPackageInstallJob=new DeploymentPackageInstallJob(
					installPackageLocation,
					workingLocation,
					websiteLocation,
					installCode,
					connectionString
					);

				deploymentPackageInstallJob.Install(DeploymentPackageInstallJob.InstallJobType.REPAIR);
				
				Console.WriteLine("System repaired.\n");
			}

			if(commandLineArguments.ContainsKey(DEVUSERS_PARAMETER) || commandLineArguments.ContainsKey(TESTUSERS_PARAMETER))
			{
				CreateDevelopmentUsers(commandLineArguments);
			}

			if(commandLineArguments.ContainsKey(CREATEORG_PARAMETER))
			{
				CreateOrganization(commandLineArguments);
				CreateLicence(commandLineArguments);
				CreateSecurityProvider(commandLineArguments);
			}

            UpdateNableUserAccounts(databaseAdminName, databaseAdminPassword, commandLineArguments);

	
			updateWebConfig(commandLineArguments);
			WebDeployment.CreateVirtualDirectory(commandLineArguments[WEBSITELOCATION_PARAMETER], commandLineArguments[DBNAME_PARAMETER], "login_1_1.aspx", null);

			ApplyScriptToDB(commandLineArguments);
			CleanUpworkingFolder(workingLocation);
		}

        private static void UpdateNableUserAccounts(string databaseAdminName, string databaseAdminPassword, CommandLineArguments commandLineArguments)
        {
            string databaseName = commandLineArguments[DBNAME_PARAMETER];
            string databaseServer = commandLineArguments[DBSERVER_PARAMETER];
            string dbSysUserName = commandLineArguments[DBSYSUSERNAME_PARAMETER];
            string dbSysUserPassword = commandLineArguments[DBSYSUSERPASSWORD_PARAMETER];

            DatabaseManager databaseManager = new DatabaseManager(databaseName,
                databaseServer,
                databaseAdminName,
                databaseAdminPassword);

            string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + dbSysUserName + ";pwd=" + dbSysUserPassword;
		
            ICMBroker broker = new CMBroker(connectionString);

            LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

            if (brokerSlot == null)
                brokerSlot = Thread.AllocateNamedDataSlot("Broker");

            Thread.SetData(brokerSlot, broker);

            broker.SetWriteStart();
            ICalculationModule objOrganisation = broker.GetWellKnownCM(WellKnownCM.Organization);
            IOrganization organizationCM = (IOrganization)objOrganisation;

            databaseManager.UpdateNableUserAccounts(objOrganisation.CID, organizationCM.OrganisationName, dbSysUserName, dbSysUserPassword);
        }

		private static void CreateTargetDirectory(string websiteLocation)
		{
			if (!Directory.Exists(websiteLocation))
				Directory.CreateDirectory(websiteLocation);

			if(!Directory.Exists(websiteLocation + "\\bin"))
				Directory.CreateDirectory(websiteLocation + "\\bin");

            if (!Directory.Exists(websiteLocation + "\\ClientBin"))
                Directory.CreateDirectory(websiteLocation + "\\ClientBin");
		}

		private static string GetBuildNumber(string installFolderPath, string websiteLocation, string connectionString)
		{
			string build = "";

			FileManagerProxy installFileManager=FileManagerProxy.CreateFileManager(installFolderPath);
			DeploymentUnitInstallJob.NAbleAssemblyName[] cmBrokerAssemblies=installFileManager.FindNAbleAssembly(Path.Combine(websiteLocation, "bin"),"CMBroker");
	
			if(cmBrokerAssemblies.Length==1)
			{
				string installedBrokerAssemblyName=cmBrokerAssemblies[0].VersionedName;
				InstallBroker broker = InstallBroker.CreateBrokerProxy(installedBrokerAssemblyName, Path.Combine(websiteLocation, "bin"), connectionString);
				build = broker.GetProductVersion();
			}
			
			return build; 
		}

		private static void updateWebConfig(CommandLineArguments commandLineArguments)
		{
			string databaseName=commandLineArguments[DBNAME_PARAMETER];
			string databaseServer=commandLineArguments[DBSERVER_PARAMETER];
			string dbSysUserName=commandLineArguments[DBSYSUSERNAME_PARAMETER];
			string dbSysUserPassword=commandLineArguments[DBSYSUSERPASSWORD_PARAMETER];
			string connectionString = "server=" + databaseServer + ";database=" + databaseName + ";uid=" + Encryption.EncryptData(dbSysUserName) + ";pwd=" + Encryption.EncryptData(dbSysUserPassword); 
			
			string webConfigFileName = Path.Combine(commandLineArguments[WEBSITELOCATION_PARAMETER], "web.config");

			WebDeployment.UpdateWebConfigFile(webConfigFileName, connectionString, null);
		}
		/// <summary>
		/// Create N-Able Database.
		/// </summary>
		/// <param name="objConnection"></param>
		private static void SetupDB(CommandLineArguments commandLineArguments)
		{
			if(!commandLineArguments.Contains(DBNAME_PARAMETER))
			{
				throw new DBCreateException("Database creation was requested but database name parameter was absent");
			}

			if(!commandLineArguments.Contains(DBSERVER_PARAMETER))
			{
				throw new DBCreateException("Database creation was requested but database server parameter was absent");
			}

			if(!commandLineArguments.Contains(DBADMINUSERNAME_PARAMETER))
			{
				throw new DBCreateException("Database creation was requested but database admin user name was absent");
			}

			if(!commandLineArguments.Contains(DBADMINPASSWORD_PARAMETER))
			{
				throw new DBCreateException("Database creation was requested but database admin user password was absent");
			}
			if(!commandLineArguments.Contains(DBSYSUSERNAME_PARAMETER))
			{
				throw new DBCreateException("Database creation was requested but database N-Able system user name was absent");
			}

			if(!commandLineArguments.Contains(DBSYSUSERPASSWORD_PARAMETER))
			{
				throw new DBCreateException("Database creation was requested but database N-Able system user password was absent");
			}

			string databaseName=commandLineArguments[DBNAME_PARAMETER];
			string databaseServer=commandLineArguments[DBSERVER_PARAMETER];
			string databaseAdminName=commandLineArguments[DBADMINUSERNAME_PARAMETER];
			string databaseAdminPassword=commandLineArguments[DBADMINPASSWORD_PARAMETER];
			string dbSysUserName=commandLineArguments[DBSYSUSERNAME_PARAMETER];
			string dbSysUserPassword=commandLineArguments[DBSYSUSERPASSWORD_PARAMETER];
			
			DatabaseManager databaseManager = new DatabaseManager(databaseName,
				databaseServer,
				databaseAdminName,
				databaseAdminPassword);
			
			if (databaseManager.DatabaseExists(dbSysUserName, dbSysUserPassword))
			{
				Console.WriteLine("Database exists so its going to be dropped");
				databaseManager.Delete();
			}
			databaseManager.CreateDB();
			if (!databaseManager.CheckSystemLogin(dbSysUserName))
			{
				databaseManager.CreateSystemLogin(dbSysUserName, dbSysUserPassword);
			}
			else
			{
				databaseManager.AddUserToDatabase(dbSysUserName, dbSysUserPassword);
			}	
		}

		public static void ApplyScriptToDB(CommandLineArguments commandLineArguments)
		{
			string databaseName=commandLineArguments[DBNAME_PARAMETER];
			string databaseServer=commandLineArguments[DBSERVER_PARAMETER];
			string databaseAdminName=commandLineArguments[DBADMINUSERNAME_PARAMETER];
			string databaseAdminPassword=commandLineArguments[DBADMINPASSWORD_PARAMETER];
			string dbSysUserName=commandLineArguments[DBSYSUSERNAME_PARAMETER];
			string dbSysUserPassword=commandLineArguments[DBSYSUSERPASSWORD_PARAMETER];
			
			DatabaseManager databaseManager = new DatabaseManager(databaseName,
				databaseServer,
				databaseAdminName,
				databaseAdminPassword);
			
			databaseManager.ApplyPostInstallScriptsToDatabase(dbSysUserName, dbSysUserPassword);
		}

		public static void CreateDevelopmentUsers(CommandLineArguments commandLineArguments)
		{
			Hashtable users = new Hashtable();

			if (commandLineArguments.ContainsKey(DEVUSERS_PARAMETER))
			{
				users.Add("Administrator", "$Admin#1" );
                users.Add("rprado", "$Admin#1");
                users.Add("darlowp", "$Admin#1");
    		}
			else if (commandLineArguments.ContainsKey(TESTUSERS_PARAMETER))
			{
                users.Add("Administrator", "$Admin#1");
                users.Add("testUser", "$Admin#1");
			}

			if(!commandLineArguments.Contains(DBNAME_PARAMETER))
			{
				throw new DBCreateException("Dev user creation was requested but database name parameter was absent");
			}

			if(!commandLineArguments.Contains(DBSERVER_PARAMETER))
			{
				throw new DBCreateException("Dev user creation was requested but database server parameter was absent");
			}

			if(!commandLineArguments.Contains(DBADMINUSERNAME_PARAMETER))
			{
				throw new DBCreateException("Database creation was requested but database admin user name was absent");
			}

			if(!commandLineArguments.Contains(DBADMINPASSWORD_PARAMETER))
			{
				throw new DBCreateException("Database creation was requested but database admin user password was absent");
			}

			if(!commandLineArguments.Contains(DBSYSUSERNAME_PARAMETER))
			{
				throw new DBCreateException("Dev user creation was requested but database N-Able system user name was absent");
			}

			if(!commandLineArguments.Contains(DBSYSUSERPASSWORD_PARAMETER))
			{
				throw new DBCreateException("Dev user creation was requested but database N-Able system user password was absent");
			}

			string databaseName=commandLineArguments[DBNAME_PARAMETER];
			string databaseServer=commandLineArguments[DBSERVER_PARAMETER];
			string databaseAdminName=commandLineArguments[DBADMINUSERNAME_PARAMETER];
			string databaseAdminPassword=commandLineArguments[DBADMINPASSWORD_PARAMETER];
			string dbSysUserName=commandLineArguments[DBSYSUSERNAME_PARAMETER];
			string dbSysUserPassword=commandLineArguments[DBSYSUSERPASSWORD_PARAMETER];

			DatabaseManager databaseManager = new DatabaseManager(databaseName,
				databaseServer,
				databaseAdminName,
				databaseAdminPassword);

			foreach(DictionaryEntry userEntry in users)
			{
				databaseManager.AddNableUserAccount(userEntry.Key.ToString(), userEntry.Value.ToString(), dbSysUserName, dbSysUserPassword);
			}
			databaseManager.CreateCINSTANCERecords(dbSysUserName, dbSysUserPassword);

			Console.WriteLine("Users successfully created.\n");

		}

		static void CreateOrganization(CommandLineArguments commandLineArguments)
		{
			ICMBroker broker = null;
			try
			{
				if(!commandLineArguments.Contains(DBNAME_PARAMETER))
				{
					throw new DBCreateException("Organization creation was requested but database name parameter was absent");
				}

				if(!commandLineArguments.Contains(DBSERVER_PARAMETER))
				{
					throw new DBCreateException("Organization creation was requested but database server parameter was absent");
				}

				if(!commandLineArguments.Contains(DBSYSUSERNAME_PARAMETER))
				{
					throw new DBCreateException("Organization creation was requested but database N-Able system user name was absent");
				}

				if(!commandLineArguments.Contains(DBSYSUSERPASSWORD_PARAMETER))
				{
					throw new DBCreateException("Organization creation was requested but database N-Able system user password was absent");
				}

				string databaseName=commandLineArguments[DBNAME_PARAMETER];
				string databaseServer=commandLineArguments[DBSERVER_PARAMETER];
				string dbSysUserName=commandLineArguments[DBSYSUSERNAME_PARAMETER];
				string dbSysUserPassword=commandLineArguments[DBSYSUSERPASSWORD_PARAMETER];

				string connectionString="server="+databaseServer+";database="+databaseName+";uid="+dbSysUserName+";pwd="+dbSysUserPassword;
				broker=new CMBroker( connectionString );

				LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");
				
				if(brokerSlot == null)
					brokerSlot = Thread.AllocateNamedDataSlot("Broker");

				Thread.SetData(brokerSlot, broker);

				broker.SetWriteStart();
				ICalculationModule objOrganisation = broker.GetWellKnownCM( WellKnownCM.Organization );
				if( objOrganisation == null )
				{
					Console.WriteLine( "Creating Organisation...\n" );

					Guid cSID=Guid.NewGuid();
					ILogicalModule lcmOrganization=broker.CreateLogicalCM("Organization_1_1","Organization",cSID, null);
				}
				else
				{
						Console.WriteLine( "Located pre-existing Organisation, aborting creation of new Organisation.\n" );
				}

			}
			catch(Exception ex)
			{
				Console.WriteLine("Creation of organization failed: "+ex.Message+"\n");
				throw;
			}
			finally
			{
				if( broker != null )
					broker.SetComplete();
			}
		}

		static void CreateLicence(CommandLineArguments commandLineArguments)
		{
			ICMBroker broker = null;
			try
			{
				if(!commandLineArguments.Contains(DBNAME_PARAMETER))
				{
					throw new DBCreateException("Organization creation was requested but database name parameter was absent");
				}

				if(!commandLineArguments.Contains(DBSERVER_PARAMETER))
				{
					throw new DBCreateException("Organization creation was requested but database server parameter was absent");
				}

				if(!commandLineArguments.Contains(DBSYSUSERNAME_PARAMETER))
				{
					throw new DBCreateException("Organization creation was requested but database N-Able system user name was absent");
				}

				if(!commandLineArguments.Contains(DBSYSUSERPASSWORD_PARAMETER))
				{
					throw new DBCreateException("Organization creation was requested but database N-Able system user password was absent");
				}

				string databaseName=commandLineArguments[DBNAME_PARAMETER];
				string databaseServer=commandLineArguments[DBSERVER_PARAMETER];
				string dbSysUserName=commandLineArguments[DBSYSUSERNAME_PARAMETER];
				string dbSysUserPassword=commandLineArguments[DBSYSUSERPASSWORD_PARAMETER];

				string connectionString="server="+databaseServer+";database="+databaseName+";uid="+dbSysUserName+";pwd="+dbSysUserPassword;
		
				broker=new CMBroker( connectionString );

				LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");
				
				if(brokerSlot == null)
					brokerSlot = Thread.AllocateNamedDataSlot("Broker");

				Thread.SetData(brokerSlot, broker);

				broker.SetWriteStart();
		
				// Create Licence
				IBrokerManagedComponent objLicence = broker.GetWellKnownBMC(WellKnownCM.Licencing);

				if( objLicence == null )
				{
					Console.WriteLine( "Creating N-ABLE Licence...\n" );

				IBrokerManagedComponent IBMCLicence = broker.CreateComponentInstance(new Guid("26437F7E-10D8-4887-A462-E92A7EF3F05C"),"Licence");
				}
				else
				{
					Console.WriteLine( "A Licence already exists..\n" );
				}
			}
			catch(Exception ex)
			{
				Console.WriteLine("Creation of Licence failed: "+ex.Message+"\n");
				throw;
			}
			finally
			{
				if( broker != null )
					broker.SetComplete();
			}
		}
		
		static void CreateSecurityProvider(CommandLineArguments commandLineArguments)
		{
			ICMBroker broker = null;
			try
			{
				if(!commandLineArguments.Contains(DBNAME_PARAMETER))
				{
					throw new DBCreateException("Security Provider creation was requested but database name parameter was absent");
				}

				if(!commandLineArguments.Contains(DBSERVER_PARAMETER))
				{
					throw new DBCreateException("Security Provider creation was requested but database server parameter was absent");
				}

				if(!commandLineArguments.Contains(DBSYSUSERNAME_PARAMETER))
				{
					throw new DBCreateException("Security Provider creation was requested but database N-Able system user name was absent");
				}

				if(!commandLineArguments.Contains(DBSYSUSERPASSWORD_PARAMETER))
				{
					throw new DBCreateException("Security Provider creation was requested but database N-Able system user password was absent");
				}

				string databaseName=commandLineArguments[DBNAME_PARAMETER];
				string databaseServer=commandLineArguments[DBSERVER_PARAMETER];
				string dbSysUserName=commandLineArguments[DBSYSUSERNAME_PARAMETER];
				string dbSysUserPassword=commandLineArguments[DBSYSUSERPASSWORD_PARAMETER];

				string connectionString="server="+databaseServer+";database="+databaseName+";uid="+dbSysUserName+";pwd="+dbSysUserPassword;
		
				broker=new CMBroker( connectionString );

				LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");
				
				if(brokerSlot == null)
					brokerSlot = Thread.AllocateNamedDataSlot("Broker");

				Thread.SetData(brokerSlot, broker);

				broker.SetWriteStart();
		
				// Create Security Provider
				IBrokerManagedComponent objSecurityProvider = broker.GetWellKnownBMC(WellKnownCM.SecurityProvider);

				if( objSecurityProvider == null )
				{
					Console.WriteLine( "Creating N-ABLE Security Provider...\n" );

					IBrokerManagedComponent IBMCSecurityProvider = broker.CreateComponentInstance(new Guid("6D0500F5-B1F9-4053-88BC-FF4004D9E9D0"),"SAS70Password");
				}
				else
				{
					Console.WriteLine( "A Security Provider already exists..\n" );
				}
			}
			catch(Exception ex)
			{
				Console.WriteLine("Creation of Security Provider failed: "+ex.Message+"\n");
				throw;
			}
			finally
			{
				if( broker != null )
					broker.SetComplete();
			}
		}

		static void CleanUpworkingFolder(string workingLocation)
		{
			if (Directory.Exists(workingLocation))
			{
				FileManager.SetDirectoryFilesToNormal(new DirectoryInfo(workingLocation));
				Directory.Delete(workingLocation, true);
			}
		}

		static void SetupWorkingFolder(string workingLocation, string installCode)
		{
			CleanUpworkingFolder(workingLocation);
			Directory.CreateDirectory(workingLocation);
			File.Copy(Path.Combine(installCode, "cabarc.exe"), Path.Combine(workingLocation, "cabarc.exe"));
		}

		static void DisplayHelp()
		{
			Version version=Assembly.GetExecutingAssembly().GetName().Version;

			Console.WriteLine( "N-Able Installer (Command Line Edition), Version: " + version.ToString() );
			Console.WriteLine( "\nUsage: install <parameters>" );
			Console.WriteLine( "parameters : " );
			Console.WriteLine( " /?	 - Help, displays this page" );
			Console.WriteLine( "/INSTALLFROM:<pathname>      Source path from which to install");
			Console.WriteLine( "/DBNAME:<dbname>             Name of the NAble Database to be created");
			Console.WriteLine( "/DBSERVER:<host name>        Host on which the SQL databse server exists");
			Console.WriteLine( "/WEBSERVER:<host name>		 Host on which the NAble web site is to be created");
			Console.WriteLine( "/WEBSITENAME:<site name>     Name of the NAble web site");
			Console.WriteLine( "/WEBSITELOCATION:<pathname>  Location of the virtual directory that will contain the site");
			Console.WriteLine( "/DBUSERNAME:<user name>      The DB user name for the N-Able application to use in its connection string");
			Console.WriteLine( "/DBUSERPASSWORD:<password>   The DB user password for the N-Able application to use in its connection string");
			Console.WriteLine( "/DBADMINNAME:<user name>     The DB user name for the N-Able application to use in its connection string");
			Console.WriteLine( "/DBADMINPASSWORD:<password>  The DB user password for the N-Able application to use in its connection string");
			Console.WriteLine( "/PARAMETERS:<file name>      Take parameters from a file");
			Console.WriteLine();
			Console.WriteLine( "/INSTALL                     Do a full install on a bare machine");
			Console.WriteLine( "/REPAIR                      Do a repair");
			Console.WriteLine( "/UPDATE                      Do an update");
			Console.WriteLine( "/CREATEDB                    Create the database");
			Console.WriteLine( "/CREATEUSER                  Create an initial user");
			Console.WriteLine( "/CREATEORG                   Create an organization");
		}
	}
}
