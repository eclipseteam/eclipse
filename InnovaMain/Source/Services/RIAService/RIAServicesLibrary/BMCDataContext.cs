﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.ServiceModel.DomainServices.Client;

namespace RIAServicesLibrary.Web
{
    public sealed partial class BMCDataContext
    {
        partial void OnCreated()
        {
            if (!DesignerProperties.IsInDesignTool)
            {
                ((WebDomainClient<RIAServicesLibrary.Web.BMCDataContext.IBMCDataServiceContract>)this.DomainClient)
                    .ChannelFactory.Endpoint.Binding.SendTimeout = new TimeSpan(0, 59, 0);
                ((WebDomainClient<RIAServicesLibrary.Web.BMCDataContext.IBMCDataServiceContract>)this.DomainClient)
                .ChannelFactory.Endpoint.Binding.ReceiveTimeout = new TimeSpan(0, 59, 0);
                ((WebDomainClient<RIAServicesLibrary.Web.BMCDataContext.IBMCDataServiceContract>)this.DomainClient)
                                .ChannelFactory.Endpoint.Binding.OpenTimeout = new TimeSpan(0, 59, 0);

            }
        }
    }
}
