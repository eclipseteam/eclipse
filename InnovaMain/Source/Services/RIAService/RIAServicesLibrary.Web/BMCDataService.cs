﻿using System;
using System.ServiceModel.DomainServices.Hosting;
using System.ServiceModel.DomainServices.Server;
using NLog;
using BMCServiceProxy.BMCServiceClient;
using System.Web;

namespace RIAServicesLibrary.Web
{

    [EnableClientAccess()]
    public partial class BMCDataService : DomainService
    {
        private static Logger _Logger = LogManager.GetCurrentClassLogger();

        #region ** By CSID, CLID

        public string UpdatePassword(string Password)
        {
            HttpContext.Current.Session["UserPassword"] = Password;
            return Password;
        }


        public string GetItem(string args)
        {
            return ProcessRequest("GetItem", (c, n, p) => c.GetItem(args, n, p));
        }       
        public string AddItem(string args)
        {
            return ProcessRequest("AddItem", (c, n, p) => c.AddItem(args, n, p));
        }       
        public string UpdateItem(string args)
        {
            return ProcessRequest("UpdateItem", (c, n, p) => c.UpdateItem(args, n, p));
        }
        public string DeleteItem(string args)
        {
            return ProcessRequest("DeleteItem", (c, n, p) => c.DeleteItem(args, n, p));
        }

        public string GetArray(string args)
        {
            return ProcessRequest("GetArray", (c, n, p) => c.GetArray(args, n, p));
        }
        public string AddArray(string args)
        {
            return ProcessRequest("AddArray", (c, n, p) => c.AddArray(args, n, p));
        }
        public string UpdateArray(string args)
        {
            return ProcessRequest("UpdateArray", (c, n, p) => c.UpdateArray(args, n, p));
        }
        public string DeleteArray(string args)
        {
            return ProcessRequest("DeleteArray", (c, n, p) => c.DeleteArray(args, n, p));
        }

        #endregion

        #region ** By Component Name

        public string GetItemByName(string args)
        {
            return ProcessRequest("GetItemByName", (c, n, p) => c.GetItemByName(args, n, p));
        }
        public string AddItemByName(string args)
        {
            return ProcessRequest("AddItemByName", (c, n, p) => c.AddItemByName(args, n, p));
        }       
        public string UpdateItemByName(string args)
        {
            return ProcessRequest("UpdateItemByName", (c, n, p) => c.UpdateItemByName(args, n, p));
        }
        public string DeleteItemByName(string args)
        {
            return ProcessRequest("DeleteItemByName", (c, n, p) => c.DeleteItemByName(args, n, p));
        }

        public string GetTransItemByName(string args)
        {
            return ProcessRequest("GetTransItemByName", (c, n, p) => c.GetTransItemByName(args, n, p));
        }

        public string GetArrayByName(string args)
        {
            return ProcessRequest("GetArrayByName", (c, n, p) => c.GetArrayByName(args, n, p));
        }
        public string AddArrayByName(string args)
        {
            return ProcessRequest("AddArrayByName", (c, n, p) => c.AddArrayByName(args, n, p));
        }
        public string UpdateArrayByName(string args)
        {
            return ProcessRequest("UpdateArrayByName", (c, n, p) => c.UpdateArrayByName(args, n, p));
        }
        public string DeleteArrayByName(string args)
        {
            return ProcessRequest("DeleteArrayByName", (c, n, p) => c.DeleteArrayByName(args, n, p));
        }

        #endregion

        #region ** By CID

        public string GetItemByCID(string args)
        {
            return ProcessRequest("GetItemByCID", (c, n, p) => c.GetItemByCID(args, n, p));
        }
        public string AddItemByCID(string args)
        {
            return ProcessRequest("AddItemByCID", (c, n, p) => c.AddItemByCID(args, n, p));
        }
        public string UpdateItemByCID(string args)
        {
            return ProcessRequest("UpdateItemByCID", (c, n, p) => c.UpdateItemByCID(args, n, p));
        }
        public string DeleteItemByCID(string args)
        {
            return ProcessRequest("DeleteItemByCID", (c, n, p) => c.DeleteItemByCID(args, n, p));
        }

        public string GetArrayByCID(string args)
        {
            return ProcessRequest("GetArrayByCID", (c, n, p) => c.GetArrayByCID(args, n, p));
        }
        public string AddArrayByCID(string args)
        {
            return ProcessRequest("AddArrayByCID", (c, n, p) => c.AddArrayByCID(args, n, p));
        }
        public string UpdateArrayByCID(string args)
        {
            return ProcessRequest("UpdateArrayByCID", (c, n, p) => c.UpdateArrayByCID(args, n, p));
        }
        public string DeleteArrayByCID(string args)
        {
            return ProcessRequest("DeleteArrayByCID", (c, n, p) => c.DeleteArrayByCID(args, n, p));
        }

        #endregion

        public string GetComponentInfo(string args)
        {
            return ProcessRequest("GetComponentInfo", (c, n, p) => c.GetComponentInfo(args, n, p));
        }

        public string GetComponentAssemblyInfo(string args)
        {
            return ProcessRequest("GetComponentAssemblyInfo", (c, n, p) => c.GetComponentAssemblyInfo(args, n, p));
        }

        private string ProcessRequest(string log, Func<BMCServiceClient, string, string, string> process)
        {
            string name = HttpContext.Current.Session["UserName"] as string;
            string password = HttpContext.Current.Session["UserPassword"] as string;
            _Logger.Info(log);
            string value = string.Empty;
            using (BMCServiceClient client = new BMCServiceClient())
            {
                value = process(client, name, password);
                client.Close();
            }
            return value;
        }

    }
}



