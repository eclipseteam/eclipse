﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using RIAServicesLibrary.Web;
using Oritax.TaxSimp.InstallInfo;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("RIAServicesLibrary.Web")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("TLR")]
[assembly: AssemblyProduct("RIAServicesLibrary.Web")]
[assembly: AssemblyCopyright("Copyright © TLR 2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("36979ACC-04B5-42F2-835F-3A9EC312B48B")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion(RIAServicesLibraryWebInstall.ASSEMBLY_MAJORVERSION + "." + RIAServicesLibraryWebInstall.ASSEMBLY_MINORVERSION)]
[assembly: AssemblyInformationalVersion(RIAServicesLibraryWebInstall.ASSEMBLY_MAJORVERSION + "." + RIAServicesLibraryWebInstall.ASSEMBLY_MINORVERSION + "." + RIAServicesLibraryWebInstall.ASSEMBLY_DATAFORMAT + "." + RIAServicesLibraryWebInstall.ASSEMBLY_REVISION)]
[assembly: AssemblyInstallInfo(RIAServicesLibraryWebInstall.ASSEMBLY_ID, RIAServicesLibraryWebInstall.ASSEMBLY_NAME, RIAServicesLibraryWebInstall.ASSEMBLY_DISPLAYNAME)]
