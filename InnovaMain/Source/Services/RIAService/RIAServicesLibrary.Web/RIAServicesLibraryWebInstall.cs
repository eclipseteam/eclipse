﻿using System;

namespace RIAServicesLibrary.Web
{
    /// <summary>
    /// Summary description for Install.
    /// </summary>
    public class RIAServicesLibraryWebInstall
    {
        #region INSTALLATION PROPERTIES

        public const string ASSEMBLY_ID = "36979ACC-04B5-42F2-835F-3A9EC312B48B";
        public const string ASSEMBLY_NAME = "RIAServicesLibraryWeb";
        public const string ASSEMBLY_DISPLAYNAME = "RIAServicesLibraryWeb";
        public const string ASSEMBLY_MAJORVERSION = "1";
        public const string ASSEMBLY_MINORVERSION = "1";
        public const string ASSEMBLY_DATAFORMAT = "0";
        public const string ASSEMBLY_REVISION = "0";

        #endregion
    }
}
