﻿
namespace RIAServicesLibrary.Web
{
    public partial class BMCDataService
    {
        public string GetSharepointFiles(string user, string library)
        {
            return ProcessRequest("GetSharepointFiles", (c, n, p) => c.GetSharepointFiles(user, library));
        }

        public string GetSharepointFile(string user, string library, string name)
        {
            return ProcessRequest("GetSharepointFile", (c, n, p) => c.GetSharepointFile(user, library, name));
        }

        public string UploadSharepointFile(string user, string library, string file)
        {
            return ProcessRequest("UpdateArray", (c, n, p) => c.UploadSharepointFile(user, library, file));
        }

        public string DeleteSharepointFile(string user, string library, string name)
        {
            return ProcessRequest("UpdateArray", (c, n, p) => c.DeleteSharepointFile(user, library, name));
        }
    }
}
