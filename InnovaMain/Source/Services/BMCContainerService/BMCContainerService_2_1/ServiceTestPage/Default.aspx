﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="ServiceTestPage._Default" ValidateRequest="false" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        WebService Test Page
    </h2>
    <p>   <div>

    <table>
    <tr>
    <td><asp:RadioButtonList ID="radiolist" runat=server>
    <asp:ListItem Text="Get By Name" Value="nyname"></asp:ListItem>
    <asp:ListItem Text="Get All Client" Selected=True Value="all"></asp:ListItem>
    </asp:RadioButtonList>
    </td>
    <td>
    <asp:TextBox ID=txtClientName runat=server></asp:TextBox>
    </td>
    <td><asp:Button ID="ButtonSelectGet" Text="Get Response by Selection" Width="200" D runat="server" onclick="ButtonSelectGet_Click" /></td>
    </tr>
    </table>

     <div>
        <asp:Label ID=labelResponseSelection Width="100%" Height="550px" runat="server" BorderStyle="Solid" ReadOnly="True" TextMode="MultiLine" Wrap="False" BackColor="#CCCCCC" > </asp:Label>
       </div>

    </p>

    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;<b>Url</b>&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="TextUri" Text="https://shamil-pc/Innova2011/Services/ClientName/Entity 1" Width="600" runat="server" />
        &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="ButtonGet" Text="Get Response" Width="120" D runat="server" onclick="ButtonGet_Click" />
        <br/>
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;<b>Xml Response</b>
        <br />
        <br />
    </div>
    <div>
        <asp:TextBox ID="TextResponse" Width="100%" Height="550px" runat="server" BorderStyle="Solid" ReadOnly="True" TextMode="MultiLine" Wrap="False" BackColor="#CCCCCC" />
    </div>
    </p>

</asp:Content>
