﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Xml.Linq;


namespace ServiceTestPage
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
             
        }

        protected override void OnUnload(EventArgs e)
        {
       
            base.OnUnload(e);

        }

        protected void ButtonSelectGet_Click(object sender, EventArgs e)
        {
            try
            {
                string response = string.Empty; 

                PublicServiceBMCClient.PublicServiceBMCClient client = new PublicServiceBMCClient.PublicServiceBMCClient();

                if (this.radiolist.SelectedValue == "all")
                {
                    

                    response = client.GetAllClients();
                   
                    //labelResponseSelection.Text = HttpUtility.HtmlEncode(response);
                    TextResponseSelection.Text = response;

                }
                else if (this.radiolist.SelectedValue == "nyname")
                {
                    response = client.GetClientByName(this.txtClientName.Text);
                    //labelResponseSelection.Text = HttpUtility.HtmlEncode(response);
                    TextResponseSelection.Text = response;
                }

                client.Close();
            }
            catch (Exception ex)
            {
                TextResponseSelection.Text = string.Format("Exception : {0}", ex.Message);
            }
        }
    }
}
