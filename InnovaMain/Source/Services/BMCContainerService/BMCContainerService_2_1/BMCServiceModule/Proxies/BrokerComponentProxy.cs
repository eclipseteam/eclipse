﻿using System;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using NLog;

namespace Oritax.TaxSimp.Proxies
{
    public class BrokerComponentProxy : IDisposable
    {
        private static Logger _Logger = LogManager.GetCurrentClassLogger();
        BrokerProxy _Proxy = null;
        BrokerManagedComponent _Component = null;
        private bool _CanWrite = false;
        Guid _Clid;
        Guid _Csid;
        Guid _Cid;
        string _CMname;
        Guid _TypeID;

        public Guid TypeID
        {
            get
            { return _TypeID;}

            set
            { _TypeID = value; }
        }

        public BrokerComponentProxy(BrokerProxy proxy)
        {
            _Proxy = proxy;
        }

        public BrokerComponentProxy(BrokerProxy proxy, Guid cid)
        {
            _Proxy = proxy;
            _Cid = cid;
        }

        public BrokerComponentProxy(BrokerProxy proxy, Guid cid, bool canWrite)
            : this(proxy, cid)
        {
            _CanWrite = canWrite;
        }

        public BrokerComponentProxy(BrokerProxy proxy, string cmname)
        {
            _Proxy = proxy;
            _CMname = cmname;            
        }

        public BrokerComponentProxy(BrokerProxy proxy, string cmname, bool canWrite)
            : this(proxy, cmname)
        {
            _CanWrite = canWrite;
        }

        public BrokerComponentProxy(BrokerProxy proxy, Guid clid, Guid csid)
        {
            _Proxy = proxy;
            _Clid = clid;
            _Csid = csid;
        }

        public BrokerComponentProxy(BrokerProxy proxy, Guid clid, Guid csid, bool canWrite)
            : this(proxy, clid, csid)
        {
            _CanWrite = canWrite;
        }

        public BrokerManagedComponent Component
        {
            get
            {
                if (_Component == null)
                {
                    _Component = (BrokerManagedComponent) _Proxy.Broker.GetCMImplementation(_Clid, _Csid);
                }
                return _Component;
            }
        }

        public BrokerManagedComponent ComponentByName
        {
            get
            {
                if (_Component == null)
                {
                    _Component = (BrokerManagedComponent)_Proxy.Broker.GetCMImplementation(_CMname, _CMname);
                }
                return _Component;
            }
        }

        public BrokerManagedComponent ComponentByCID
        {
            get
            {
                if (_Component == null)
                {
                    _Component = (BrokerManagedComponent)_Proxy.Broker.GetBMCInstance(_Cid);
                }
                return _Component;
            }
        }

        public BrokerManagedComponent ComponentByTypeID(Guid TypeID)
        {
            if (_Component == null)
            {
                _Component = (BrokerManagedComponent)_Proxy.Broker.CreateTransientComponentInstance(_TypeID);
            }
            return _Component;
        }

        public void Dispose()
        {
            if (_Component != null)
            {
                try
                {
                    if (!_Component.TransientInstance)
                    {
                        _Proxy.Broker.ReleaseBrokerManagedComponent(_Component);
                        _Component = null;
                    }
                }
                catch (Exception ex)
                {
                    _Logger.Error("ReleaseBrokerManagedComponent got error:{0}", ex.InnerException.ToString());
                }
                finally
                {
                    
                }
            }
            if (_Proxy.Broker != null && _CanWrite)
            {
                _Proxy.Broker.SetWriteStart();
                _Proxy.Broker.SetComplete();
            }

        }
    }
    
}
