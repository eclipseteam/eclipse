﻿using System;
using System.Threading;
using System.Diagnostics;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Proxies
{
    public class BrokerProxy : IDisposable
    {
        private BrokerProxy() { }

        public ICMBroker Broker
        {
            get
            {
                LocalDataStoreSlot slot = Thread.GetNamedDataSlot("Broker");
                if (slot == null)
                    return null;
                return (ICMBroker)Thread.GetData(slot);
            }
            set
            {
                LocalDataStoreSlot slot = Thread.GetNamedDataSlot("Broker");
                if (slot == null)
                    slot = Thread.AllocateNamedDataSlot("Broker");
                if (value == null)
                    Thread.FreeNamedDataSlot("Broker");
                Thread.SetData(slot, value);
            }
        }

        protected void Initialize()
        {
            this.Broker = new CMBroker(DBConnection.Connection.ConnectionString);
            this.Broker.SetStart();
        }

        protected IBrokerManagedComponent GetBrokerManagedComponent(string clid, string csid)
        {
            Guid module = Broker.GetCMInstanceID(clid.ToGuid(), csid.ToGuid());
            IBrokerManagedComponent component = Broker.GetBMCInstance(module);
            return component;
        }

        protected void InitialiseBrokerWriterStart()
        {
            Broker = new CMBroker(DBConnection.Connection.ConnectionString);
            Broker.SetWriteStart();
        }

        protected void Complete()
        {
            try
            {
                if (Broker.InTransaction)
                {
                    Broker.SetComplete();
                }
            }
            catch
            {
                Broker.SetAbort();
                throw;
            }
        }

        protected void Cleanup()
        {
            if (Broker != null)
            {
                if (Broker.InTransaction)
                {
                    Broker.SetAbort();
                    Debug.Assert(true, "BrokerManagedComponentWorkpaper.CleanupBroker(), transaction on the broker was not ended correctly");
                }
                Broker.Dispose();
            }
        }

        #region Dispose Method
        public void Dispose()
        {
            ICMBroker broker = Broker;
            if (broker != null) broker.SetAbort();
            this.Broker.SetComplete();
        }
        #endregion

        #region Factory Method
        public static BrokerProxy GetBrokerProxy()
        {
            BrokerProxy proxy = new BrokerProxy();
            proxy.Initialize();
            return proxy;
        }
        #endregion
    }


}
