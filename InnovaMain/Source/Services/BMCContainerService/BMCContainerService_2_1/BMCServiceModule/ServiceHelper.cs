﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BMCServiceModule
{
    /// <summary>
    /// The helper class of the service
    /// </summary>
    public class ServiceHelper
    {
        /// <summary>
        /// Get the exception information string
        /// </summary>
        /// <param name="ex"> the exception object</param>
        /// <returns>the exception information string</returns>
        static public string GetExceptionInfo(Exception ex)
        {

            string info = ex.Message.ToString();

            if (ex.InnerException != null)
            {
                info += "\r\n" + ex.InnerException.Message.ToString();
            }

            return info;
        }
    }
}
