﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Oritax.TaxSimp.InstallInfo;

using BMCServiceModule;
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("BMCServiceModule")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("TLR")]
[assembly: AssemblyProduct("BMCServiceModule")]
[assembly: AssemblyCopyright("Copyright © TLR 2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("4a1a9b01-7ff6-42fb-8d64-51b53a125a60")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion(BMCServiceModuleInstall.ASSEMBLY_MAJORVERSION + "." + BMCServiceModuleInstall.ASSEMBLY_MINORVERSION)]
[assembly: AssemblyInformationalVersion(BMCServiceModuleInstall.ASSEMBLY_MAJORVERSION + "." + BMCServiceModuleInstall.ASSEMBLY_MINORVERSION + "." + BMCServiceModuleInstall.ASSEMBLY_DATAFORMAT + "." + BMCServiceModuleInstall.ASSEMBLY_REVISION)]
[assembly: AssemblyInstallInfo(BMCServiceModuleInstall.ASSEMBLY_ID, BMCServiceModuleInstall.ASSEMBLY_NAME, BMCServiceModuleInstall.ASSEMBLY_DISPLAYNAME)]

