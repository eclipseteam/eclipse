﻿using System;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.Workers
{
    public static class BmcServiceWorkerFactory
    {
        public static IBmcServiceWorker GetInstance<T>(string xml, bool canWrite, Action<BMCServiceDataItem, BrokerManagedComponent> action)
            where T : class, IBmcServiceWorker, new()
        {
            IBmcServiceWorker worker = new T();
            worker.Xml = xml;
            worker.CanWrite = canWrite;
            worker.Action = action;
            return worker;
        }
    }

    public static class BmcServiceWorkerFactoryByName
    {
        public static IBmcServiceWorkerByName GetInstance<T>(string xml, bool canWrite, Action<BMCServiceDataItemByName, BrokerManagedComponent> action)
            where T : class, IBmcServiceWorkerByName, new()
        {
            IBmcServiceWorkerByName worker = new T();
            worker.Xml = xml;
            worker.CanWrite = canWrite;
            worker.Action = action;
            return worker;
        }
    }

    public static class BmcServiceWorkerFactoryByTrans
    {
        public static IBmcServiceWorkerByTrans GetInstance<T>(string xml, Action<BMCServiceDataItemByTrans, BrokerManagedComponent> action)
            where T : class, IBmcServiceWorkerByTrans, new()
        {
            IBmcServiceWorkerByTrans worker = new T();
            worker.Xml = xml;
            worker.Action = action;
            return worker;
        }
    }

    public static class BmcServiceWorkerFactoryByCID
    {
        public static IBmcServiceWorkerByCID GetInstance<T>(string xml, bool canWrite, Action<BMCServiceDataItemByCID, BrokerManagedComponent> action)
            where T : class, IBmcServiceWorkerByCID, new()
        {
            IBmcServiceWorkerByCID worker = new T();
            worker.Xml = xml;
            worker.CanWrite = canWrite;
            worker.Action = action;
            return worker;
        }
    }
}
