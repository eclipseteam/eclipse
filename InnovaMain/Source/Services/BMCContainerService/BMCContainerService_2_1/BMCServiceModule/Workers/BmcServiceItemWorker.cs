﻿using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Loggers;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.Workers
{
    public class BmcServiceItemWorker : BmcServiceWorkerBase
    {
        protected override string DoAction()
        {
            ServiceLogger.WriteRequest(Xml);
            BMCServiceDataItem item = Xml.ToData<BMCServiceDataItem>();
            DoComponentAction(item);
            string value = item.ToXmlString<BMCServiceDataItem>();
            return value;
        }
    }

    public class BmcServiceItemWorkerByName : BmcServiceWorkerBaseByName
    {
        protected override string DoAction()
        {
            ServiceLogger.WriteRequest(Xml);
            BMCServiceDataItemByName item = Xml.ToData<BMCServiceDataItemByName>();
            DoComponentAction(item);
            string value = item.ToXmlString<BMCServiceDataItemByName>();
            return value;
        }
    }

    public class BmcServiceItemWorkerByTrans : BmcServiceWorkerBaseByTrans
    {
        protected override string DoAction()
        {
            ServiceLogger.WriteRequest(Xml);
            BMCServiceDataItemByTrans item = Xml.ToData<BMCServiceDataItemByTrans>();
            DoComponentAction(item);
            string value = item.ToXmlString<BMCServiceDataItemByTrans>();
            return value;
        }
    }

    public class BmcServiceItemWorkerByCID : BmcServiceWorkerBaseByCID
    {
        protected override string DoAction()
        {
            ServiceLogger.WriteRequest(Xml);
            BMCServiceDataItemByCID item = Xml.ToData<BMCServiceDataItemByCID>();
            DoComponentAction(item);
            string value = item.ToXmlString<BMCServiceDataItemByCID>();
            return value;
        }
    }

}
