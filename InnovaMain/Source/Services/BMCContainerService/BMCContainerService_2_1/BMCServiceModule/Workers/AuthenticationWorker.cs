﻿using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Workers
{
    public class AuthenticationWorker
    {
        public static bool Authenticate(ICMBroker broker, string name, string password)
        {
            bool status = false;
            IBrokerManagedComponent component = null;
            try
            {
                component = broker.GetBMCInstance(name, "DBUser_1_1");
                if (component != null)
                {
                    status = (component as IAuthenticable).Authenticate(name, password);
                }
            }
            finally
            {
                if (component != null)
                {
                    broker.ReleaseBrokerManagedComponent(component);
                }
            }
            return status;
        }
    }
}
