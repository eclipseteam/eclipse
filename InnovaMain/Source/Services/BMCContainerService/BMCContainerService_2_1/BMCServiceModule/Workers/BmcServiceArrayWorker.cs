﻿using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Loggers;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.Workers
{
    public class BmcServiceArrayWorker : BmcServiceWorkerBase
    {
        protected override string DoAction()
        {
            ServiceLogger.WriteRequest(Xml);
            BMCServiceDataArray data = Xml.ToData<BMCServiceDataArray>();
            foreach (var item in data.Items)
            {
                DoComponentAction(item);
            }
            string value = data.ToXmlString<BMCServiceDataArray>();
            return value;
        }
    }

    public class BmcServiceArrayWorkerByName : BmcServiceWorkerBaseByName
    {
        protected override string DoAction()
        {
            ServiceLogger.WriteRequest(Xml);
            BMCServiceDataArrayByName data = Xml.ToData<BMCServiceDataArrayByName>();
            foreach (var item in data.Items)
            {
                DoComponentAction(item);
            }
            string value = data.ToXmlString<BMCServiceDataArrayByName>();
            return value;
        }
    }


    public class BmcServiceArrayWorkerByTrans : BmcServiceWorkerBaseByTrans
    {
        protected override string DoAction()
        {
            ServiceLogger.WriteRequest(Xml);
            BMCServiceDataArrayByTrans data = Xml.ToData<BMCServiceDataArrayByTrans>();
            foreach (var item in data.Items)
            {
                DoComponentAction(item);
            }
            string value = data.ToXmlString<BMCServiceDataArrayByTrans>();
            return value;
        }
    }

    public class BmcServiceArrayWorkerByCID : BmcServiceWorkerBaseByCID
    {
        protected override string DoAction()
        {
            ServiceLogger.WriteRequest(Xml);
            BMCServiceDataArrayByCID data = Xml.ToData<BMCServiceDataArrayByCID>();
            foreach (var item in data.Items)
            {
                DoComponentAction(item);
            }
            string value = data.ToXmlString<BMCServiceDataArrayByCID>();
            return value;
        }
    }
}
