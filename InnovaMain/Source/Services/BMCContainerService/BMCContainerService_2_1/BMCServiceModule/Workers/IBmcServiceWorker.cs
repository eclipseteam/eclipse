﻿using System;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.Workers
{

    public interface IBmcServiceWorker
    {
        string Xml { get; set; }
        bool CanWrite { get; set; }
        Action<BMCServiceDataItem, BrokerManagedComponent> Action { get; set; }
        string Process(string name, string password);
    }

    public interface IBmcServiceWorkerByName
    {
        string Xml { get; set; }
        bool CanWrite { get; set; }
        Action<BMCServiceDataItemByName, BrokerManagedComponent> Action { get; set; }
        string Process(string name, string password);
    }

    public interface IBmcServiceWorkerByTrans
    {
        string Xml { get; set; }
        bool CanWrite { get; set; }
        Action<BMCServiceDataItemByTrans, BrokerManagedComponent> Action { get; set; }
        string Process(string name, string password);
    }


    public interface IBmcServiceWorkerByCID
    {
        string Xml { get; set; }
        bool CanWrite { get; set; }
        Action<BMCServiceDataItemByCID, BrokerManagedComponent> Action { get; set; }
        string Process(string name, string password);
    }
}
