﻿using System;
using NLog;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Proxies;
using Oritax.TaxSimp.Loggers;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Extensions;
namespace Oritax.TaxSimp.Workers
{

    public abstract class BmcServiceWorkerBase : IBmcServiceWorker
    {
        private static Logger _Logger = LogManager.GetCurrentClassLogger();

        public string Xml { get; set; }
        public bool CanWrite { get; set; }
        public Action<BMCServiceDataItem, BrokerManagedComponent> Action { get; set; }
        private BrokerProxy _Broker;

        protected abstract string DoAction();

        public string Process(string name, string password)
        {
            string value = string.Empty;
            string compressedResponse = string.Empty;
            using (_Broker = BrokerProxy.GetBrokerProxy())
            {
                try
                {
                    if (AuthenticationWorker.Authenticate(_Broker.Broker, name, password))
                    {
                        value = DoAction();
                    }
                    else
                    {
                        value = string.Format("Authentication failed: User: {0}", name);
                    }

                    compressedResponse = value.Compress();

                    _Logger.Info("The result size: {0}", value.Length);
                    _Logger.Info("The Compressed respose size: {0}", compressedResponse.Length);
                }
                catch (Exception ex)
                {
                    _Logger.Error(ServiceLogger.GetExceptionInfo(ex));
                }

            }
            _Broker = null;
            ServiceLogger.WriteResponse(value);
          
            ServiceLogger.WriteZippedResponse(compressedResponse);

            return compressedResponse;

        }

        protected void DoComponentAction(BMCServiceDataItem item)
        {
            using (BrokerComponentProxy component = new BrokerComponentProxy(_Broker, item.Clid, item.Csid, CanWrite))
            {
                try
                {
                    if (component.Component == null)
                    {
                        _Logger.Error("Component not loaded CLID:{0}, CSID:{1}", item.Clid, item.Csid);
                    }
                    else
                    {
                        Action(item, component.Component);
                    }
                }
                catch (Exception ex)
                {
                    _Logger.Error(ServiceLogger.GetExceptionInfo(ex));
                }
            }

        }

    }

    public abstract class BmcServiceWorkerBaseByName : IBmcServiceWorkerByName
    {
        private static Logger _Logger = LogManager.GetCurrentClassLogger();

        public string Xml { get; set; }
        public bool CanWrite { get; set; }
        public Action<BMCServiceDataItemByName, BrokerManagedComponent> Action { get; set; }
        private BrokerProxy _Broker;

        protected abstract string DoAction();

        public string Process(string name, string password)
        {
            string value = string.Empty;
            string compressedResponse = string.Empty;
            using (_Broker = BrokerProxy.GetBrokerProxy())
            {
                try
                {
                    if (AuthenticationWorker.Authenticate(_Broker.Broker, name, password))
                    {
                        value = DoAction();
                    }
                    else
                    {
                        value = string.Format("Authentication failed: User: {0}", name);
                    }
                  
                    compressedResponse = value.Compress();

                    _Logger.Info("The result size: {0}", value.Length);
                    _Logger.Info("The Compressed respose size: {0}", compressedResponse.Length);
                }
                catch (Exception ex)
                {
                    _Logger.Error(ServiceLogger.GetExceptionInfo(ex));
                }

            }
            _Broker = null;
            ServiceLogger.WriteResponse(value);
            ServiceLogger.WriteZippedResponse(compressedResponse);

            return compressedResponse;

        }

        protected void DoComponentAction(BMCServiceDataItemByName item)
        {
            using (BrokerComponentProxy component = new BrokerComponentProxy(_Broker, item.ComponentName,CanWrite))
            {
                try
                {
                    if (component.ComponentByName == null)
                    {
                        _Logger.Error("Component not loaded ComponentName:{0}", item.ComponentName);
                    }
                    else
                    {
                        Action(item, component.ComponentByName);
                    }
                }
                catch (Exception ex)
                {
                    _Logger.Error(ServiceLogger.GetExceptionInfo(ex));
                }
            }

        }

    }

    public abstract class BmcServiceWorkerBaseByTrans : IBmcServiceWorkerByTrans
    {
        private static Logger _Logger = LogManager.GetCurrentClassLogger();

        public string Xml { get; set; }
        public bool CanWrite { get; set; }
        public Action<BMCServiceDataItemByTrans, BrokerManagedComponent> Action { get; set; }
        private BrokerProxy _Broker;

        protected abstract string DoAction();

        public string Process(string name, string password)
        {
            string value = string.Empty;
            string compressedResponse = string.Empty;
            using (_Broker = BrokerProxy.GetBrokerProxy())
            {
                try
                {
                    if (AuthenticationWorker.Authenticate(_Broker.Broker, name, password))
                    {
                        value = DoAction();
                    }
                    else
                    {
                        value = string.Format("Authentication failed: User: {0}", name);
                    }
                    compressedResponse = value.Compress();

                    _Logger.Info("The result size: {0}", value.Length);
                    _Logger.Info("The Compressed respose size: {0}", compressedResponse.Length);
                }
                catch (Exception ex)
                {
                    _Logger.Error(ServiceLogger.GetExceptionInfo(ex));
                }

            }
            _Broker = null;
            ServiceLogger.WriteResponse(value);
            ServiceLogger.WriteZippedResponse(compressedResponse);

            return compressedResponse;
        }

        protected void DoComponentAction(BMCServiceDataItemByTrans item)
        {
            using (BrokerComponentProxy brokerComponentProxy = new BrokerComponentProxy(_Broker))
            {
                try
                {
                    brokerComponentProxy.TypeID = item.TypeID; 

                    if (brokerComponentProxy.TypeID == null)
                    {
                        _Logger.Error("Component not loaded TypeID:{0}", item.TypeID);
                    }
                    else
                    {
                        BrokerManagedComponent bnrokerManagedComponent = brokerComponentProxy.ComponentByTypeID(item.TypeID);
                        Action(item, bnrokerManagedComponent);
                    }
                }
                catch (Exception ex)
                {
                    _Logger.Error(ServiceLogger.GetExceptionInfo(ex));
                }
            }

        }

    }

    public abstract class BmcServiceWorkerBaseByCID : IBmcServiceWorkerByCID
    {
        private static Logger _Logger = LogManager.GetCurrentClassLogger();

        public string Xml { get; set; }
        public bool CanWrite { get; set; }
        public Action<BMCServiceDataItemByCID, BrokerManagedComponent> Action { get; set; }
        private BrokerProxy _Broker;

        protected abstract string DoAction();

        public string Process(string name, string password)
        {
            string value = string.Empty;
            string compressedResponse = string.Empty;
            using (_Broker = BrokerProxy.GetBrokerProxy())
            {
                try
                {
                    if (AuthenticationWorker.Authenticate(_Broker.Broker, name, password))
                    {
                        value = DoAction();
                    }
                    else
                    {
                        value = string.Format("Authentication failed: User: {0}", name);
                    }
                    compressedResponse = value.Compress();

                    _Logger.Info("The result size: {0}", value.Length);
                    _Logger.Info("The Compressed respose size: {0}", compressedResponse.Length);
                }
                catch (Exception ex)
                {
                    _Logger.Error(ServiceLogger.GetExceptionInfo(ex));
                }

            }
            _Broker = null;
            ServiceLogger.WriteResponse(value);
            ServiceLogger.WriteZippedResponse(compressedResponse);

            return compressedResponse;
        }

        protected void DoComponentAction(BMCServiceDataItemByCID item)
        {
            using (BrokerComponentProxy component = new BrokerComponentProxy(_Broker, item.Cid, CanWrite))
            {
                try
                {
                    if (component.ComponentByCID == null)
                    {
                        _Logger.Error("Component not loaded CID:{0}", item.Cid);
                    }
                    else
                    {
                        Action(item, component.ComponentByCID);
                    }
                }
                catch (Exception ex)
                {
                    _Logger.Error(ServiceLogger.GetExceptionInfo(ex));
                }
            }

        }

    }
}
