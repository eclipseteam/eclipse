﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

using NLog;
using System.Collections.ObjectModel;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.Exceptions;

namespace BMCServiceModule
{
    /// <summary>
    /// The class implement IBMCService interface
    /// </summary>
    public class BMCService : ServiceManagedBase, IBMCService
    {
        
        public static bool LogRequest = false;
        public static bool LogResponse = false;
        public static string LogDir = "c:\\BMCServiceLog";

        public static int Counter = 0;

        private static Logger m_logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Get the log file name by specified prefix
        /// </summary>
        /// <param name="prefix">the prefix of the file name</param>
        /// <returns></returns>
        private string GetLogFileName(string prefix)
        {
            string ret = string.Format
                (
                    "{0}_{1:D4}_{2:D2}_{3:D2}_{4:D5}.xml", 
                    prefix, 
                    DateTime.Now.Year, 
                    DateTime.Now.Month, 
                    DateTime.Now.Day, 
                    ++(BMCService.Counter)
                );
            return ret;
        }

        /// <summary>
        /// write the log file 
        /// </summary>
        /// <param name="fileNamePrefix">the prefix of the file name</param>
        /// <param name="content">the file contents</param>
        private void WriteLog(string fileNamePrefix, string content)
        {
            try
            {
                string fullPath = Path.Combine(BMCService.LogDir, GetLogFileName(fileNamePrefix));

                //File.WriteAllBytes(fullPath, Encoding.GetBytes(content));

                XmlDocument doc = new XmlDocument();

                doc.LoadXml(content);

                doc.Save(fullPath);
                
            }
            catch (Exception ex)
            {
                m_logger.Error(ServiceHelper.GetExceptionInfo(ex));
            }

        }

        /// <summary>
        /// implement GetData method to retrieve one group of data
        /// </summary>
        /// <param name="clid">the CLID of a workpaper</param>
        /// <param name="csid">the CSID of a workpaper</param>
        /// <param name="type">the data type</param>
        /// <returns>return serialized xml string</returns>
        public string GetData(Guid clid, Guid csid, int type)
        {
            m_logger.Info("**********BMCService:GetData **********");

            m_logger.Trace("Parameters: type={0} clid={1} csid={2}", type, clid, csid);

            string ret = string.Empty;

            //intialise the broker
            BrokerManagedComponent bmc = null;
            InitialiseBroker();

            try
            {

                //get the data...
                bmc = (BrokerManagedComponent)CmBroker.GetCMImplementation(clid, csid);
                ret = bmc.GetDataStream(type);


            }
            catch (Exception ex)
            {
                m_logger.Error(ServiceHelper.GetExceptionInfo(ex));
                throw new Exception("Exception occured in the service", ex);
            }
            finally
            {
                if (bmc != null)
                    CmBroker.ReleaseBrokerManagedComponent(bmc);
                CmBroker.SetAbort();
            }

            m_logger.Info("Mesage size of type {0} is: {1} ", type, ret.Length);

            if (BMCService.LogResponse)
                WriteLog("Response", ret.ToString());

            return ret;
        }

        /// <summary>
        /// Update a group of data
        /// </summary>
        /// <param name="clid">the CLID of a workpaper</param>
        /// <param name="csid">the CSID of a workpaper</param>
        /// <param name="type">the data type</param>
        /// <param name="data">the actually to be updated</param>
        public void UpdateData(Guid clid, Guid csid, int type, string data)
        {
            m_logger.Info("********** BMCService:UpdateData **********");
            
            m_logger.Trace("Processing: type={0} clid={1} csid={2}", type, clid, csid);

            if (BMCService.LogRequest)
                WriteLog("Request", data);
            
            string ret = string.Empty;

            //initialise the broker
            BrokerManagedComponent bmc = null;
            InitialiseBroker();

            try
            {
                //update the data
                bmc = (BrokerManagedComponent)CmBroker.GetCMImplementation(clid, csid);
                bmc.UpdateDataStream(type, data);
                //CMCacheManager.CacheManager.UpdateCacheEntry(bmc);
                CmBroker.SetComplete();

            }
            catch (Exception ex)
            {
                m_logger.Error(ServiceHelper.GetExceptionInfo(ex));
                throw new Exception("Exception occured in the service", ex);
            }
            finally
            {
                if (bmc != null)
                    CmBroker.ReleaseBrokerManagedComponent(bmc);
                CmBroker.SetAbort();
            }

            m_logger.Info("Finish processing the request type of {0}", type);
        }


        /// <summary>
        /// Get multiple groups of data
        /// </summary>
        /// <param name="data">the data type</param>
        /// <returns>the xml string</returns>
        public string GetDataList(string requestString)
        {
            m_logger.Info("********** BMCService:GetDataList **********");

            if (BMCService.LogRequest)
                WriteLog("Request", requestString);

            StringBuilder response = new StringBuilder();

            //initialise the broker
            BrokerManagedComponent bmc = null;
            InitialiseBroker();

            try
            {
                //establish the serializer
                XmlSerializer serializer = new XmlSerializer(typeof(BMCServiceDataArray));

                //deserialize the data string to objects
                StringReader reader = new StringReader(requestString);
                BMCServiceDataArray request = serializer.Deserialize(reader) as BMCServiceDataArray;
                reader.Close();

                //process each type of request
                foreach (BMCServiceDataItem item in request.BMCServiceDataItem)
                {
                    try
                    {
                        m_logger.Trace("Processing: type={0} clid={1} csid={2}", item.Type, request.CLID, request.CSID);

                        DateTime dtStart = DateTime.Now;

                        bmc = (BrokerManagedComponent)CmBroker.GetCMImplementation(request.CLID, request.CSID);
                        item.Data = bmc.GetDataStream(item.Type);

                    }
                    catch (Exception ex)
                    {
                        //log the error
                        m_logger.Error(ServiceHelper.GetExceptionInfo(ex));
                    }
                    finally
                    {
                        if (bmc != null)
                            CmBroker.ReleaseBrokerManagedComponent(bmc);

                    }
                }

                //consolidate the result: serialize to a string
                StringWriter writer = new StringWriter(response);
                serializer.Serialize(writer, request);
                writer.Close();

                m_logger.Info("The result size: {0}", response.Length);

            }
            catch (Exception ex)
            {
                //log the error
                m_logger.Error(ServiceHelper.GetExceptionInfo(ex));

                throw ex;
            }
            finally
            {
                CmBroker.SetAbort();
            }

            if (BMCService.LogResponse)
                WriteLog("Response", response.ToString());

            return response.ToString();
        }

        /// <summary>
        /// Update multiple groups of data
        /// </summary>
        /// <param name="requestString">the xml string contains the serialized data</param>
        /// <returns>return xml string</returns>
        public string UpdateDataList(string requestString)
        {

            StringBuilder response = new StringBuilder();

            m_logger.Info("********** BMCService:UpdateDataList **********");

            if (BMCService.LogRequest)
                WriteLog("Request", requestString);

            BrokerManagedComponent bmc = null;
            try
            {
                //initialise the broker
                InitialiseBroker();
                
                //establish the serializer 
                XmlSerializer serializer = new XmlSerializer(typeof(BMCServiceDataArray));

                //deserialize the string to the request object
                StringReader reader = new StringReader(requestString);
                BMCServiceDataArray request = serializer.Deserialize(reader) as BMCServiceDataArray;
                reader.Close();

                bmc = (BrokerManagedComponent)CmBroker.GetCMImplementation(request.CLID, request.CSID);


                //process each type of request
                foreach (BMCServiceDataItem item in request.BMCServiceDataItem)
                {
                    try
                    {
                        m_logger.Trace("Processing: type={0} clid={1} csid={2}", item.Type, request.CLID, request.CSID);

                        bmc.UpdateDataStream(item.Type, item.Data);
                        item.Data = string.Empty;

                        m_logger.Info("Finish processing the request type of {0} ", item.Type);
                        
                    }
                    catch (Exception ex)
                    {
                        //log the error
                        m_logger.Error(ServiceHelper.GetExceptionInfo(ex));
                    }
                    
                }

                //serialize the result to a response string
                StringWriter writer = new StringWriter(response);
                serializer.Serialize(writer, request);
                writer.Close();

                

            }
            catch (Exception ex)
            {
                //log the error
                m_logger.Error(ServiceHelper.GetExceptionInfo(ex));
                CmBroker.SetAbort();
                throw ex;
            }
            finally
            {
                
                //CMCacheManager.CacheManager.UpdateCacheEntry(bmc);
                if (bmc != null)
                    CmBroker.ReleaseBrokerManagedComponent(bmc);
                CmBroker.SetWriteStart();
                CmBroker.SetComplete();


            }

            if (BMCService.LogResponse)
                WriteLog("Response", response.ToString());

            return response.ToString();
        }

        /// <summary>
        /// Get the component display name
        /// </summary>
        /// <param name="clid">the CLID of a workpaper</param>
        /// <param name="csid">the CSID of a workpaper</param>
        /// <returns>return the component name string</returns>
        public string GetComponentInfo(Guid clid, Guid csid)
        {

            m_logger.Info("********** BMCService:GetComponentInfo **********");
            
            string ret = string.Empty;

            BrokerManagedComponent bmc = null;
            InitialiseBroker();

            try
            {

                bmc = (BrokerManagedComponent)CmBroker.GetCMImplementation(clid, csid);
                
                IComponent component = bmc.ComponentInformation;

                ret = component.DisplayName;

            }
            catch (Exception ex)
            {
                throw new Exception("Exception occured in the service", ex);
            }
            finally
            {
                if (bmc != null)
                    CmBroker.ReleaseBrokerManagedComponent(bmc);
                CmBroker.SetAbort();
            }
            return ret;
        }

        /// <summary>
        /// To be used to check if the service is online
        /// </summary>
        public void IsOnline()
        {
        }
    }
}
