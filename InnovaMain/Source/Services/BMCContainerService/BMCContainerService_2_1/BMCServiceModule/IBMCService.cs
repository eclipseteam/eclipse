﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BMCServiceModule
{
    /// <summary>
    /// The interface of the service
    /// </summary>

    #region Service Interface
    [ServiceContract]
    public interface IBMCService
    {
        [OperationContract]
        string GetComponentInfo(Guid clid, Guid csid);

        [OperationContract]
        string GetData(Guid clid, Guid csid, int type);

        [OperationContract]
        void UpdateData(Guid clid, Guid csid, int type, string data);

        [OperationContract]
        string GetDataList(string requestString);

        [OperationContract]
        string UpdateDataList(string requestString);

        [OperationContract]
        void IsOnline();
    }
    #endregion

    /// <summary>
    /// The data contract for the request and the response of the service
    /// </summary>
    /// 
    #region Service Data Contract
    [DataContract]
    public class BMCRunTimeData
    {

        /// <summary>
        /// Constructor
        /// </summary>
        #region Constructors
        public BMCRunTimeData()
        {
        }

        public BMCRunTimeData(Guid clid, Guid csid, string data)
        {
            m_clid = clid;
            m_csid = csid;
            m_data = data;
        }

        public BMCRunTimeData(Guid clid, Guid csid, int type)
        {
            m_clid = clid;
            m_csid = csid;
            m_type = type;
        }

        #endregion

        #region Private varaibles

        Guid m_clid = Guid.Empty;
        Guid m_csid = Guid.Empty;
        int m_type = 0;
        string m_data = string.Empty;
        double m_serviceRunTime = 0.0;

        #endregion

        #region Data members
        [DataMember]
        public Guid CLID
        {
            get { return m_clid; }
            set { m_clid = value; }
        }

        [DataMember]
        public Guid CSID
        {
            get { return m_csid; }
            set { m_csid = value; }
        }

        [DataMember]
        public int Type
        {
            get { return m_type; }
            set { m_type = value; }
        }

        [DataMember]
        public double ServiceRunTime
        {
            get { return m_serviceRunTime; }
            set { m_serviceRunTime = value; }
        }

        [DataMember]
        public string Data
        {
            get { return m_data; }
            set { m_data = value; }
        }

        #endregion
    }

    #endregion

}
