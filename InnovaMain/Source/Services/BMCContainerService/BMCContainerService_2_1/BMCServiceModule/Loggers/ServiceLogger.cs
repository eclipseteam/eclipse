﻿using System;
using System.IO;
using System.Xml;
using NLog;

namespace Oritax.TaxSimp.Loggers
{
    #region ServiceLogger
    public static class ServiceLogger
    {
        public static bool LogRequest = false;
        public static bool LogResponse = false;

        private static int Counter = 0;
        public static string LogPath = "c:\\BMCServiceLog";
        
        private static Logger Logger
        {
            get { return LogManager.GetCurrentClassLogger(); }
        }

        private static string GetLogFileName(string prefix,string ext)
        {
            string value = string.Format
                (
                    "{0}_{1:D4}_{2:D2}_{3:D2}_{4:D5}.{5}",
                    prefix,
                    DateTime.Now.Year,
                    DateTime.Now.Month,
                    DateTime.Now.Day,
                    ++(ServiceLogger.Counter),
                    ext
                );
            return value;
        }
        
        private static void WriteLog(string file, string content)
        {
            try
            {
                string path = Path.Combine(ServiceLogger.LogPath, GetLogFileName(file,"xml"));
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(content);
                xml.Save(path);

            }
            catch (Exception ex)
            {
                Logger.Error(ServiceLogger.GetExceptionInfo(ex));
            }
        }

        private static void WriteZippedLog(string file, string content)
            {
            try
                {
                string path = Path.Combine(ServiceLogger.LogPath, GetLogFileName(file,"txt"));
                using (StreamWriter outfile =new StreamWriter(path))
                    {
                    outfile.Write(content.ToString());
                    }

                }
            catch (Exception ex)
                {
                Logger.Error(ServiceLogger.GetExceptionInfo(ex));
                }
            }


        public static void WriteRequest(string content)
        {
            if (ServiceLogger.LogRequest)
            {
                WriteLog("Request", content);
            }
        }

        public static void WriteResponse(string content)
        {
            if (ServiceLogger.LogResponse)
            {
                WriteLog("Reponse", content);
            }
        }
        public static void WriteZippedResponse(string content)
            {
            if (ServiceLogger.LogResponse)
                {
                WriteZippedLog("Reponse", content);
                }
            }

        public static string GetExceptionInfo(Exception ex)
        {

            string info = ex.Message.ToString();

            if (ex.InnerException != null)
            {
                info += "\r\n" + ex.InnerException.Message.ToString();
            }

            return info;
        }

    }
    #endregion
}
