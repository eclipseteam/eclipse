﻿using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Proxies;
using Oritax.TaxSimp.Workers;

namespace Oritax.TaxSimp.Services
{
    public partial class BMCService
    {
        public string GetQueryData(string xml, bool isCompressed)
        {
            string outa = string.Empty;
            BMCServiceDataItem item = xml.ToData<BMCServiceDataItem>();
            using (BrokerProxy proxy = BrokerProxy.GetBrokerProxy())
            {
                ICalculationModule cm = proxy.Broker.GetWellKnownCM(WellKnownCM.Organization);
                IBrokerManagedComponent bmc = proxy.Broker.GetCMImplementation(cm.CLID, cm.CSID);
                outa = bmc.GetDataStream(item.Type, item.Data);
                proxy.Broker.ReleaseBrokerManagedComponent(bmc);
                proxy.Broker.ReleaseBrokerManagedComponent(cm);
            }
    
            if (isCompressed)
                return  StringCompression.Compress(outa);
            else
                return outa;
        }

        public bool Authenticate(string name, string password)
        {
            bool status = false;
            using (BrokerProxy proxy = BrokerProxy.GetBrokerProxy())
            {
                status = AuthenticationWorker.Authenticate(proxy.Broker, name, password); 
            }
            return status;

        }
    }
}
