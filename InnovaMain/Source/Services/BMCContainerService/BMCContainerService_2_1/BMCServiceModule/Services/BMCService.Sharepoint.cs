﻿using System.Collections.Generic;
using Oritax.TaxSimp.Proxies;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.Services
{
    public partial class BMCService
    {
        public string GetSharepointFiles(string user, string library)
        {
            List<SharepointFileInfo> files = SharepointProxy.GetFiles(user, library);
            return files.ToXmlString();
        }

        public string GetSharepointFile(string user, string library, string name)
        {
            SerializedFile file = SharepointProxy.GetSerializedFile(user, library, name);
            return file.ToXmlString();
        }

        public string UploadSharepointFile(string user, string library, string file)
        {
            SerializedFile serialized = file.ToData<SerializedFile>();
            SharepointFileInfo info = SharepointProxy.UploadFile(user, library, serialized);
            return info.ToXmlString();
        }

        public string DeleteSharepointFile(string user, string library, string name)
        {
            SharepointFileInfo info = SharepointProxy.DeleteFile(user, library, name);
            return info.ToXmlString();
        }
    }
}
