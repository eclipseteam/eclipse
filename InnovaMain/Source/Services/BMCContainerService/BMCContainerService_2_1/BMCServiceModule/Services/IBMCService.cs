﻿using System.ServiceModel;

namespace Oritax.TaxSimp.Services
{
    #region Service Interface
    [ServiceContract]
    public interface IBMCService
    {
        [OperationContract]
        string GetQueryData(string args, bool isCompressed);

        [OperationContract]
        bool Authenticate(string name, string password);

        #region Sharepoint Service

        [OperationContract]
        string GetSharepointFiles(string user, string library);

        [OperationContract]
        string GetSharepointFile(string user, string library, string name);

        [OperationContract]
        string UploadSharepointFile(string user, string library, string file);

        [OperationContract]
        string DeleteSharepointFile(string user, string library, string name);

        #endregion

        [OperationContract]
        void IsOnline();

        [OperationContract]
        string GetComponentInfo(string args, string name, string password);

        [OperationContract]
        string GetComponentAssemblyInfo(string args, string name, string password);

        [OperationContract]
        string GetItem(string args, string name, string password);

        [OperationContract]
        string AddItem(string args, string name, string password);

        [OperationContract]
        string UpdateItem(string args, string name, string password);

        [OperationContract]
        string DeleteItem(string args, string name, string password);

        [OperationContract]
        string GetArray(string args, string name, string password);

        [OperationContract]
        string AddArray(string args, string name, string password);

        [OperationContract]
        string UpdateArray(string args, string name, string password);

        [OperationContract]
        string DeleteArray(string args, string name, string password);

        #region ** By TransientComponent

        [OperationContract]
        string GetTransItemByName(string args, string name, string password);

        #endregion 

         #region ** By Component Name

        [OperationContract]
        string GetItemByName(string args, string name, string password);

        [OperationContract]
        string AddItemByName(string args, string name, string password);

        [OperationContract]
        string UpdateItemByName(string args, string name, string password);

        [OperationContract]
        string DeleteItemByName(string args, string name, string password);

        [OperationContract]
        string GetArrayByName(string args, string name, string password);

        [OperationContract]
        string AddArrayByName(string args, string name, string password);

        [OperationContract]
        string UpdateArrayByName(string args, string name, string password);

        [OperationContract]
        string DeleteArrayByName(string args, string name, string password);
 
        #endregion

        #region ** By CID

        [OperationContract]
        string GetItemByCID(string args, string name, string password);

        [OperationContract]
        string AddItemByCID(string args, string name, string password);

        [OperationContract]
        string UpdateItemByCID(string args, string name, string password);

        [OperationContract]
        string DeleteItemByCID(string args, string name, string password);

        [OperationContract]
        string GetArrayByCID(string args, string name, string password);

        [OperationContract]
        string AddArrayByCID(string args, string name, string password);

        [OperationContract]
        string UpdateArrayByCID(string args, string name, string password);

        [OperationContract]
        string DeleteArrayByCID(string args, string name, string password);

        #endregion
    }
    #endregion
}
