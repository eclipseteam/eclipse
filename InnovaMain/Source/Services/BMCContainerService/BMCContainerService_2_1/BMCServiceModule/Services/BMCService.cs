﻿using System;
using NLog;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Workers;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Services
{
    public partial class BMCService : IBMCService
    {
        private static Logger _Logger = LogManager.GetCurrentClassLogger();

        public void IsOnline()
        {
            throw new NotImplementedException();
        }

        #region ** GetComponentInfo
        public string GetComponentInfo(string args, string name, string password)
        {
            string value = string.Empty;
            ProcessItem("GetComponentInfo", args, name, password, false, (data, component) =>
            {
                IComponent info = component.ComponentInformation;
                value = info.DisplayName;
                _Logger.Info("The component : {0}", value);
            });
            return value;
        }

        public string GetComponentAssemblyInfo(string args, string name, string password)
        {
            string value = string.Empty;
            ProcessItem("GetComponentInfo", args, name, password, false, (data, component) =>
            {
                value = component.TypeName;// for the need of assembly name.
                _Logger.Info("The component Assembly : {0}", value);
            });
            return value;
        }

        #endregion

        #region ** By CSID, CLID

        public string GetItem(string args, string name, string password)
        {
            return ProcessItem("GetItem", args, name, password, false, (data, component) =>
            {
                data.Data = component.GetDataStream(data.Type, data.Data);
            });
        }

        public string AddItem(string args, string name, string password)
        {
            return ProcessItem("AddItem", args, name, password, true, (data, component) =>
            {
                data.Data = component.AddDataStream(data.Type, data.Data);
            });
        }

        public string UpdateItem(string args, string name, string password)
        {
            return ProcessItem("UpdateItem", args, name, password, true, (data, component) =>
            {
                data.Data = component.UpdateDataStream(data.Type, data.Data);
            });
        }

        public string DeleteItem(string args, string name, string password)
        {
            return ProcessItem("DeleteItem", args, name, password, true, (data, component) =>
            {
                data.Data = component.DeleteDataStream(data.Type, data.Data);
            });
        }

        public string GetArray(string args, string name, string password)
        {
            return ProcessArray("GetArray", args, name, password, false, (data, component) =>
            {
                data.Data = component.GetAllDataStream(data.Type);
            });
        }

        public string AddArray(string args, string name, string password)
        {
            return ProcessArray("AddArray", args, name, password, true, (data, component) =>
            {
                data.Data = component.AddDataStream(data.Type, data.Data);
            });
        }

        public string UpdateArray(string args, string name, string password)
        {
            return ProcessArray("UpdateArray", args, name, password, true, (data, component) =>
            {
                data.Data = component.UpdateDataStream(data.Type, data.Data);
            });
        }

        public string DeleteArray(string args, string name, string password)
        {
            return ProcessArray("DeleteArray", args, name, password, true, (data, component) =>
            {
                data.Data = component.DeleteDataStream(data.Type, data.Data);
            });
        }

        private string ProcessItem(string message, string xml, string name, string password, bool canWrite, Action<BMCServiceDataItem, BrokerManagedComponent> action)
        {
            _Logger.Info(message);
            IBmcServiceWorker worker = BmcServiceWorkerFactory.GetInstance<BmcServiceItemWorker>(xml, canWrite, action);
            return worker.Process(name, password);
        }

        private string ProcessArray(string message, string xml, string name, string password, bool canWrite, Action<BMCServiceDataItem, BrokerManagedComponent> action)
        {
            _Logger.Info(message);
            IBmcServiceWorker worker = BmcServiceWorkerFactory.GetInstance<BmcServiceArrayWorker>(xml, canWrite, action);
            return worker.Process(name, password);
        }

        #endregion

        #region ** By Component Name

        #region Item

        public string GetItemByName(string args, string name, string password)
        {
            return ProcessItemByName("GetItemByName", args, name, password, false, (data, component) =>
            {
                data.Data = component.GetDataStream(data.Type, data.Data);
            });

        }

        public string AddItemByName(string args, string name, string password)
        {
            return ProcessItemByName("AddItemByName", args, name, password, true, (data, component) =>
            {
                data.Data = component.AddDataStream(data.Type, data.Data);
            });
        }

        public string UpdateItemByName(string args, string name, string password)
        {
            return ProcessItemByName("UpdateItemByName", args, name, password, true, (data, component) =>
            {
                data.Data = component.UpdateDataStream(data.Type, data.Data);
            });
        }

        public string DeleteItemByName(string args, string name, string password)
        {
            return ProcessItemByName("DeleteItemByName", args, name, password, true, (data, component) =>
            {
                data.Data = component.DeleteDataStream(data.Type, data.Data);
            });
        }

        private string ProcessItemByName(string message, string xml, string name, string password, bool canWrite, Action<BMCServiceDataItemByName, BrokerManagedComponent> action)
        {
            _Logger.Info(message);
            IBmcServiceWorkerByName worker = BmcServiceWorkerFactoryByName.GetInstance<BmcServiceItemWorkerByName>(xml, canWrite, action);
            return worker.Process(name, password);
        }

        #endregion

        #region   TransientComponent

        public string GetTransItemByName(string args, string name, string password)
        {
            return ProcessTransItemByName("GetTransItemByName", args, name, password, false, (data, component) =>
            {
                data.Data = component.GetDataStream(data.Type, data.Data);
            });

        }

        private string ProcessTransItemByName(string message, string xml, string name, string password, bool canWrite, Action<BMCServiceDataItemByTrans, BrokerManagedComponent> action)
        {
            _Logger.Info(message);
            IBmcServiceWorkerByTrans worker = BmcServiceWorkerFactoryByTrans.GetInstance<BmcServiceItemWorkerByTrans>(xml, action);
            return worker.Process(name, password);
        }

        #endregion

        #region Array

        public string GetArrayByName(string args, string name, string password)
        {
            return ProcessArrayByName("GetArrayByName", args, name, password, false, (data, component) =>
            {
                data.Data = component.GetAllDataStream(data.Type);
            });
        }

        public string AddArrayByName(string args, string name, string password)
        {
            return ProcessArrayByName("AddArrayByName", args, name, password, true, (data, component) =>
            {
                data.Data = component.AddDataStream(data.Type, data.Data);
            });
        }

        public string UpdateArrayByName(string args, string name, string password)
        {
            return ProcessArrayByName("UpdateArrayByName", args, name, password, true, (data, component) =>
            {
                data.Data = component.UpdateDataStream(data.Type, data.Data);
            });
        }

        public string DeleteArrayByName(string args, string name, string password)
        {
            return ProcessArrayByName("DeleteArrayByName", args, name, password, true, (data, component) =>
            {
                data.Data = component.DeleteDataStream(data.Type, data.Data);
            });
        }

        private string ProcessArrayByName(string message, string xml, string name, string password, bool canWrite, Action<BMCServiceDataItemByName, BrokerManagedComponent> action)
        {
            _Logger.Info(message);
            IBmcServiceWorkerByName worker = BmcServiceWorkerFactoryByName.GetInstance<BmcServiceArrayWorkerByName>(xml, canWrite, action);
            return worker.Process(name, password);
        }

        #endregion

        #endregion

        #region ** By CID

        #region Item

        public string GetItemByCID(string args, string name, string password)
        {
            return ProcessItemByCID("GetItemByCID", args, name, password, false, (data, component) =>
            {
                data.Data = component.GetDataStream(data.Type, data.Data);
            });

        }

        public string AddItemByCID(string args, string name, string password)
        {
            return ProcessItemByCID("AddItemByCID", args, name, password, true, (data, component) =>
            {
                data.Data = component.AddDataStream(data.Type, data.Data);
            });
        }

        public string UpdateItemByCID(string args, string name, string password)
        {
            return ProcessItemByCID("UpdateItemByCID", args, name, password, true, (data, component) =>
            {
                data.Data = component.UpdateDataStream(data.Type, data.Data);
            });
        }

        public string DeleteItemByCID(string args, string name, string password)
        {
            return ProcessItemByCID("DeleteItemByCID", args, name, password, true, (data, component) =>
            {
                data.Data = component.DeleteDataStream(data.Type, data.Data);
            });
        }

        private string ProcessItemByCID(string message, string xml, string name, string password, bool canWrite, Action<BMCServiceDataItemByCID, BrokerManagedComponent> action)
        {
            _Logger.Info(message);
            IBmcServiceWorkerByCID worker = BmcServiceWorkerFactoryByCID.GetInstance<BmcServiceItemWorkerByCID>(xml, canWrite, action);
            return worker.Process(name, password);
        }

        #endregion

        #region Array

        public string GetArrayByCID(string args, string name, string password)
        {
            return ProcessArrayByCID("GetArrayByCID", args, name, password, false, (data, component) =>
            {
                data.Data = component.GetAllDataStream(data.Type);
            });
        }

        public string AddArrayByCID(string args, string name, string password)
        {
            return ProcessArrayByCID("AddArrayByCID", args, name, password, true, (data, component) =>
            {
                data.Data = component.AddDataStream(data.Type, data.Data);
            });
        }

        public string UpdateArrayByCID(string args, string name, string password)
        {
            return ProcessArrayByCID("UpdateArrayByCID", args, name, password, true, (data, component) =>
            {
                data.Data = component.UpdateDataStream(data.Type, data.Data);
            });
        }

        public string DeleteArrayByCID(string args, string name, string password)
        {
            return ProcessArrayByCID("DeleteArrayByCID", args, name, password, true, (data, component) =>
            {
                data.Data = component.DeleteDataStream(data.Type, data.Data);
            });
        }

        private string ProcessArrayByCID(string message, string xml, string name, string password, bool canWrite, Action<BMCServiceDataItemByCID, BrokerManagedComponent> action)
        {
            _Logger.Info(message);
            IBmcServiceWorkerByCID worker = BmcServiceWorkerFactoryByCID.GetInstance<BmcServiceArrayWorkerByCID>(xml, canWrite, action);
            return worker.Process(name, password);
        }

        #endregion

        #endregion

    }
}
