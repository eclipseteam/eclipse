﻿using System;

namespace BMCServiceModule
{
    /// <summary>
    /// Summary description for Install.
    /// </summary>
    public class BMCServiceModuleInstall
    {
        #region INSTALLATION PROPERTIES

        public const string ASSEMBLY_ID = "3BDC058B-7C2B-40A4-ACC9-F584D447A21D";
        public const string ASSEMBLY_NAME = "BMCServiceModule";
        public const string ASSEMBLY_DISPLAYNAME = "BMCServiceModule";
        public const string ASSEMBLY_MAJORVERSION = "1";
        public const string ASSEMBLY_MINORVERSION = "1";
        public const string ASSEMBLY_DATAFORMAT = "0";
        public const string ASSEMBLY_REVISION = "0";

        #endregion
    }
}
