﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Oritax.TaxSimp.InstallInfo;
using BMCServiceProxy;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("BMCServiceProxy")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("TLR")]
[assembly: AssemblyProduct("BMCServiceProxy")]
[assembly: AssemblyCopyright("Copyright © TLR 2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("82233cfc-ec42-40f7-ac51-241ded3aeb32")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion(BMCServiceProxyInstall.ASSEMBLY_MAJORVERSION + "." + BMCServiceProxyInstall.ASSEMBLY_MINORVERSION)]
[assembly: AssemblyInformationalVersion(BMCServiceProxyInstall.ASSEMBLY_MAJORVERSION + "." + BMCServiceProxyInstall.ASSEMBLY_MINORVERSION + "." + BMCServiceProxyInstall.ASSEMBLY_DATAFORMAT + "." + BMCServiceProxyInstall.ASSEMBLY_REVISION)]
[assembly: AssemblyInstallInfo(BMCServiceProxyInstall.ASSEMBLY_ID, BMCServiceProxyInstall.ASSEMBLY_NAME, BMCServiceProxyInstall.ASSEMBLY_DISPLAYNAME)]

