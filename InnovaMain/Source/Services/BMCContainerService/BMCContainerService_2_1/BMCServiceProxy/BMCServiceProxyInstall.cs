﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BMCServiceProxy
{
    /// <summary>
    /// Summary description for Install.
    /// </summary>
    public class BMCServiceProxyInstall
    {
        #region INSTALLATION PROPERTIES

        public const string ASSEMBLY_ID = "139DD30E-60F1-4563-825E-631158862F7F";
        public const string ASSEMBLY_NAME = "BMCServiceProxy";
        public const string ASSEMBLY_DISPLAYNAME = "BMCServiceProxy";
        public const string ASSEMBLY_MAJORVERSION = "1";
        public const string ASSEMBLY_MINORVERSION = "1";
        public const string ASSEMBLY_DATAFORMAT = "0";
        public const string ASSEMBLY_REVISION = "0";

        #endregion
    }
}