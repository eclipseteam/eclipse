﻿using System;
using System.ServiceProcess;
using System.ServiceModel;
using System.Configuration;
using System.IO;
using NLog;

using Oritax.TaxSimp.Loggers;
using Oritax.TaxSimp.Services;

namespace BMCServiceHost
{
    public partial class BMCServiceHost : ServiceBase
    {

        private static Logger m_logger = LogManager.GetCurrentClassLogger();

        ServiceHost m_host = null;

        public BMCServiceHost()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                m_logger.Info("The service is started.");

                m_logger.Info("Load the service configuration settings.");
                
                string setting = ConfigurationManager.AppSettings["LogRequest"];

                if (setting.ToLower() == "true")
                    ServiceLogger.LogRequest = true;
                else
                    ServiceLogger.LogRequest = false;

                setting = ConfigurationManager.AppSettings["LogResponse"];

                if (setting.ToLower() == "true")
                    ServiceLogger.LogResponse = true;
                else
                    ServiceLogger.LogResponse = false;

                setting = ConfigurationManager.AppSettings["LogDir"];

                ServiceLogger.LogPath = setting;

                if (!Directory.Exists(ServiceLogger.LogPath))
                {
                    Directory.CreateDirectory(ServiceLogger.LogPath);
                }

                Aspose.Words.License license = new Aspose.Words.License();
                license.SetLicense("Aspose.Words.lic");
                
            }
            catch (Exception ex)
            {
                m_logger.Error(ServiceLogger.GetExceptionInfo(ex));
            }

            try
            {
                m_host = new ServiceHost(typeof(BMCService));
                m_host.Open();
            }
            catch (Exception ex)
            {
                m_logger.Error(ServiceLogger.GetExceptionInfo(ex));
            }
        }

        protected override void OnStop()
        {
            try
            {

                m_logger.Info("The service is stopped.");

                if (m_host != null)
                {
                    m_host.Close();
                }

            }
            catch (Exception ex)
            {
                m_logger.Error(ServiceLogger.GetExceptionInfo(ex));
            }
        }
    }
}
