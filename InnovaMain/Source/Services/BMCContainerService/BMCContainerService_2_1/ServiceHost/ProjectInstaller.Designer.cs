﻿namespace BMCServiceHost
{
	partial class ProjectInstaller
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.BMCServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.BMCServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // BMCServiceProcessInstaller
            // 
            this.BMCServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.BMCServiceProcessInstaller.Password = null;
            this.BMCServiceProcessInstaller.Username = null;
            // 
            // BMCServiceInstaller
            // 
            this.BMCServiceInstaller.DisplayName = "TAXSIMP BMC Service";
            this.BMCServiceInstaller.ServiceName = "BMCService";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.BMCServiceProcessInstaller,
            this.BMCServiceInstaller});

		}

		#endregion

        private System.ServiceProcess.ServiceProcessInstaller BMCServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller BMCServiceInstaller;
	}
}