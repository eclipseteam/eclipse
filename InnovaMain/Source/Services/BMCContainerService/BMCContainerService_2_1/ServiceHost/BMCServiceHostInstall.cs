﻿using System;

namespace BMCServiceHost
{
    /// <summary>
    /// Summary description for Install.
    /// </summary>
    public class BMCServiceHostInstall
    {
        #region INSTALLATION PROPERTIES

        public const string ASSEMBLY_ID = "10EC844E-F1FD-4E2C-B6A4-6EDFC85375DC";
        public const string ASSEMBLY_NAME = "BMCServiceHost";
        public const string ASSEMBLY_DISPLAYNAME = "BMCServiceHost";
        public const string ASSEMBLY_MAJORVERSION = "1";
        public const string ASSEMBLY_MINORVERSION = "1";
        public const string ASSEMBLY_DATAFORMAT = "0";
        public const string ASSEMBLY_REVISION = "0";

        #endregion
    }
}
