﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using BMCServiceProxy.BMCServiceClient;
using Oritax.TaxSimp.Data;
using System.Xml.Linq;
using System.ServiceModel.Channels;
using System.Xml;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Helpers;
using Oritax.TaxSimp.Extensions;

namespace PublicServiceBMC
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class PublicServiceBMC : IPublicServiceBMC
    {

        public string GetAllClients()
        {
            BMCServiceDataItem item = new BMCServiceDataItem { Type = (int)WellKnownRestCommand.AllClient, Data = string.Empty };
            return Process(item);
        }

        public string GetClientByType(string type)
        {
            BMCServiceDataItem item = new BMCServiceDataItem { Type = (int)WellKnownRestCommand.AllClientByType, Data = type };
            return Process(item);
        }

        public string GetClientByName(string name)
        {
            BMCServiceDataItem item = new BMCServiceDataItem { Type = (int)WellKnownRestCommand.ClientByName, Data = name };
            return Process(item);
        }


        private string Process(BMCServiceDataItem item)
        {
            string xml = string.Empty;
            using (BMCServiceClient client = new BMCServiceClient())
            {
                xml = client.GetQueryData(item.ToXmlString(), false);
                client.Close();
            }
            XDocument document = XDocument.Parse(xml);
            document.Root.RemoveAttributes();
            return document.ToString();
        }

    }
}
