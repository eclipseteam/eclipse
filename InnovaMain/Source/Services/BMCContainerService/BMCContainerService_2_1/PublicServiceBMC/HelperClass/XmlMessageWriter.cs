﻿using System.Xml;
using System.Xml.Linq;
using System.ServiceModel.Channels;

namespace Oritax.TaxSimp.Helpers
{
    public class XmlMessageWriter : BodyWriter
    {
        XDocument _Document;

        public XmlMessageWriter(XDocument document)
            : base(true)
        {
            _Document = document;
        }

        protected override void OnWriteBodyContents(XmlDictionaryWriter writer)
        {
            _Document.WriteTo(writer);
        }
    }
}
