﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.ServiceModel.Channels;

namespace PublicServiceBMC
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IPublicServiceBMC
    {

        [OperationContract]
        [WebGet(UriTemplate = "/AllClients/")]
        string GetAllClients();

        [OperationContract]
        [WebGet(UriTemplate = "ClientType/{type}")]
        string GetClientByType(string type);

        [OperationContract]
        [WebGet(UriTemplate = "ClientName/{name}")]
        string GetClientByName(string name);
    }
  
}
