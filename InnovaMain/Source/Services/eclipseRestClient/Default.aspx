﻿<%@ Page Title="e-Clipse Online - Rest Client Test Page" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <h2>
        e-Clipse Online Rest Client
    </h2>
    <br />
    <table width="100%">
        <tr>
            <td width="10%">
                Rest URLs:
            </td>
            <td width="70%">
                <telerik:RadComboBox runat="server"  ID="cmdRestURLS" Width="100%">
                <Items>
                    <telerik:RadComboBoxItem Text="Client Response XPLAN Live" Value="https://services.e-clipse.com.au/episervices/getdataresponseallclients/"/>
                    <telerik:RadComboBoxItem Text="Client Response XPLAN UAT V2" Value="https://services.e-clipse.com.au/episervices/GetAllSecuritiesDataResponse/"/>
                    <telerik:RadComboBoxItem Text="Super Managers (e-Clipse Super Accounts) Basic" Value="https://services.e-clipse.com.au/services/EclipseSuperBasic/" />
                </Items>
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td width="10%">
                Parameter:
            </td>
            <td width="70%">
                <asp:TextBox Width="50%" runat="server" ID="para"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="10%">
                User Name:
            </td>
            <td width="70%">
                <asp:TextBox Width="50%" runat="server" ID="User"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="10%">
                Password:
            </td>
            <td width="70%">
                <asp:TextBox TextMode="Password" Width="50%" runat="server" ID="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
            <td>
                <br />
                [<asp:LinkButton Font-Size="Smaller" ID="lnkDownload" Text="DOWNLOAD" runat="server"
                    OnClick="lnkDownload_Click"></asp:LinkButton>]  [<asp:LinkButton Font-Size="Smaller" ID="lnkDownloadExcel" Text="DOWNLOAD EXCEL" runat="server"
                    OnClick="lnkDownloadEclipseSuperBasicData_Click"></asp:LinkButton>] [<asp:LinkButton Font-Size="Smaller"
                        ID="lnkFetch" Text="GET DATA" runat="server" onclick="lnkFetch_Click"></asp:LinkButton>]
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="Red" ErrorMessage="*Please select REST Query"
                    ControlToValidate="cmdRestURLS"> </asp:RequiredFieldValidator><br />
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ForeColor="Red" ErrorMessage="*User Name is Required"
                    ControlToValidate="User"> </asp:RequiredFieldValidator><br />
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ForeColor="Red" ErrorMessage="*Password is Required"
                    ControlToValidate="Password"> </asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
    <div>
        <asp:TextBox ID="TextResponse" Width="100%" Height="300px" runat="server" BorderStyle="Solid"
            ReadOnly="True" TextMode="MultiLine" Wrap="False" BackColor="#CCCCCC" />
    </div>
    <br />
    <br />
    <p>
        2012 (&#169;) e-Clipse Online PTY. LTD.
    </p>
</asp:Content>
