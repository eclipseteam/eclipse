﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Xml.Linq;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_Click(object sender, EventArgs e)
    {
        try
        {
            if (this.cmdRestURLS.SelectedItem != null && this.cmdRestURLS.Text != string.Empty)
            {
                string uri = this.cmdRestURLS.SelectedValue;
                string fileNameIndex = this.cmdRestURLS.Text;
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    (s, certificate, chain, sslPolicyErrors) => true;

                HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;
                request.Timeout = 4000000;
                request.Credentials = new NetworkCredential(this.User.Text, this.Password.Text);
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    string strContents = null;
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    string xml = reader.ReadToEnd();
                    XDocument doc = XDocument.Parse(xml);
                    strContents = doc.ToString();

                    string attachment = "attachment; filename=" + fileNameIndex + " - " + DateTime.Now.ToString("dd/MMM/yyyy") + ".xml";
                    Response.ClearContent();
                    Response.ContentType = "application/xml";
                    Response.AddHeader("content-disposition", attachment);
                    Response.Write(strContents);
                    Response.End();
                }
            }
        }
        catch (Exception ex)
        {
            TextResponse.Text = string.Format("Exception : {0}", ex.Message);
        }
    }

    protected void lnkDownloadEclipseSuperBasicData_Click(object sender, EventArgs e)
    {
        try
        {
            if (this.cmdRestURLS.SelectedItem != null && this.cmdRestURLS.Text != string.Empty)
            {
                string uri = this.cmdRestURLS.SelectedValue;
                string fileNameIndex = this.cmdRestURLS.Text;
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    (s, certificate, chain, sslPolicyErrors) => true;

                HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;
                request.Timeout = 4000000;
                request.Credentials = new NetworkCredential(this.User.Text, this.Password.Text);
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    string strContents = null;
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    string xml = reader.ReadToEnd();
                    XDocument doc = XDocument.Parse(xml);
                    strContents = doc.ToString();

                    string attachment = "attachment; filename=" + fileNameIndex + " - " + DateTime.Now.ToString("dd/MMM/yyyy") + ".xml";
                    Response.ClearContent();
                    Response.ContentType = "application/xml";
                    Response.AddHeader("content-disposition", attachment);
                    Response.Write(strContents);
                    Response.End();
                }
            }
        }
        catch (Exception ex)
        {
            TextResponse.Text = string.Format("Exception : {0}", ex.Message);
        }
    }
    protected void lnkFetch_Click(object sender, EventArgs e)
    {
        try
        {
            if ((User.Text.ToLower() == "iress_access") || (User.Text.ToLower() == "administrator"))
            {
                if (this.cmdRestURLS.SelectedItem != null && this.cmdRestURLS.Text != string.Empty)
                {
                    string uri = this.cmdRestURLS.SelectedValue;
                    string fileNameIndex = this.cmdRestURLS.Text;
                    System.Net.ServicePointManager.ServerCertificateValidationCallback =
                        (s, certificate, chain, sslPolicyErrors) => true;

                    HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;
                    request.Credentials = new NetworkCredential(this.User.Text, this.Password.Text);
                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {
                        StreamReader reader = new StreamReader(response.GetResponseStream());
                        string xml = reader.ReadToEnd();
                        XDocument doc = XDocument.Parse(xml);
                        TextResponse.Text = doc.ToString();
                    }
                }
            }
            else
                TextResponse.Text = "Unauthorised Access";
        }
        catch (Exception ex)
        {
            TextResponse.Text = string.Format("Exception : {0}", ex.Message);
        }
    }
}
