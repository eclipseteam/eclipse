﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Xml.Linq;
using Downloader;



    public class XMLDownloader
    {
        private string _url = string.Empty;
        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }
        public string UserID = string.Empty;
        public string Password = string.Empty;
        public string ReponseAsString = string.Empty;
        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
        public void MakeRequest()
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(Url);
                request.Method = "GET";
                request.Headers.Add(GetAthentication());

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                    string xml = reader.ReadToEnd();
                    XDocument doc = XDocument.Parse(xml);
                    ReponseAsString = doc.ToString();
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                Console.Write("Access denied to the path " + ConfigurationSettings.AppSettings["SaveFilePath"].ToString());
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Logger.Log("\nXML Downloader failed." + "_" + ex.Message.ToString());
                SendEmail(false);
            }
        }

        private NameValueCollection GetAthentication()
        {
            NameValueCollection userCredential = new NameValueCollection();

            string inputText = this.UserID+":"+this.Password;
            byte [] bytesToEncode = Encoding.UTF8.GetBytes (inputText);
            string encodedText = Convert.ToBase64String (bytesToEncode);
            userCredential.Add("Password",this.Password);
            userCredential.Add("Username", this.UserID);
            //userCredential.Add("Authorization", "Basic " + encodedText);
            return userCredential;
        }
        private void SendEmail(bool isDownloaded)
        {
            try
            {
                MailMessage mail = new MailMessage();
                if (ConfigurationSettings.AppSettings != null)
                {
                    SmtpClient SmtpServer = new SmtpClient(ConfigurationSettings.AppSettings["SmtpClient"].ToString());
                    mail.From = new MailAddress(ConfigurationSettings.AppSettings["MailFrom"].ToString());
                    mail.To.Add(ConfigurationSettings.AppSettings["MailTo"].ToString());
                    mail.Subject = "XML Downloader";
                    if (isDownloaded)
                    {
                        mail.Body = "File Date " + DateTime.Now.ToString("dd-MM-yyyy") + " is successfully downloaded";
                    }
                    else
                    {
                        mail.Body = "File Date " + DateTime.Now.ToString("dd-MM-yyyy") + " is failed";
                    }
                    SmtpServer.Port = Convert.ToInt32(ConfigurationSettings.AppSettings["SmtpPort"]);
                    SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationSettings.AppSettings["SmptpUser"].ToString(), ConfigurationSettings.AppSettings["SmptpPassword"].ToString());
                    SmtpServer.EnableSsl = true;
                    SmtpServer.Send(mail);
                }
            }
            catch
            {
            }
        }

        string ConvertResponseToString(HttpWebResponse response)
        {
            string result = new StreamReader(response.GetResponseStream()).ReadToEnd();
            return result;
        }
    }

