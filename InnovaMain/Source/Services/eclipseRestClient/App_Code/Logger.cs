﻿using System;
using System.IO;
using System.Configuration;

namespace Downloader
{
    public class Logger
    {
        public static void Log(string line)
        {
            try
            {
                if (Directory.Exists(ConfigurationSettings.AppSettings["LogFilePath"].ToString()))
                {
                    if (File.Exists(ConfigurationSettings.AppSettings["LogFilePath"].ToString() + DateTime.Now.ToString("dd-MM-yyyy")))
                    {
                        using (StreamWriter writer = File.AppendText(ConfigurationSettings.AppSettings["LogFilePath"].ToString() + DateTime.Now.ToString("dd-MM-yyyy")))
                        {
                            writer.WriteLine(line);
                            writer.Close();
                        }
                    }
                    else
                    {
                        File.WriteAllText(ConfigurationSettings.AppSettings["LogFilePath"].ToString() + DateTime.Now.ToString("dd-MM-yyyy"), line);
                    }
                }
                else
                {
                    Directory.CreateDirectory(ConfigurationSettings.AppSettings["LogFilePath"].ToString());
                    if (File.Exists(ConfigurationSettings.AppSettings["LogFilePath"].ToString() + DateTime.Now.ToString("dd-MM-yyyy")))
                    {
                        using (StreamWriter writer = File.AppendText(ConfigurationSettings.AppSettings["LogFilePath"].ToString() + DateTime.Now.ToString("dd-MM-yyyy")))
                        {
                            writer.WriteLine(line);
                            writer.Close();
                        }
                    }
                    else
                    {
                        File.WriteAllText(ConfigurationSettings.AppSettings["LogFilePath"].ToString() + DateTime.Now.ToString("dd-MM-yyyy"), line);
                    }
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                Console.Write("Access denied to the path " + ConfigurationSettings.AppSettings["LogFilePath"].ToString());
                Console.ReadLine();
            }
            catch
            {
            }

        }
    }
}
