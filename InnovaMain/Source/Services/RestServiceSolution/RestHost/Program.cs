﻿using System;
using System.ServiceProcess;
using Oritax.TaxSimp.Hosts;
using Oritax.TaxSimp.Services;

namespace Oritax.TaxSimp
{
    class Program
    {
        static void Main(string[] args)
        {
            bool status = true;
            if (status)
            {
                ServiceBase.Run(new HostingService());
            }
            else
            {
                //RunFileEchoServiceHost();
                //RunInnovaRestServiceHost();
                RunHostingService();
            }

        }

        private static void RunHostingService()
        {
            HostingService service = new HostingService();
            service.StartX();
            PauseToStop();
            service.StopX();
        }

        private static void RunFileEchoServiceHost()
        {
            FileEchoServiceHost host = new FileEchoServiceHost();
            host.Start();
            PauseToStop();
            host.Stop();
        }

        private static void RunInnovaRestServiceHost()
        {
            InnovaRestServiceHost host = new InnovaRestServiceHost();
            host.Start();
            PauseToStop();
            host.Stop();
        }

        private static void PauseToStop()
        {
            Console.WriteLine("Press enter key to exit...");
            Console.ReadLine();
        }

    }
}
