﻿using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using System.ServiceModel;
using BMCServiceProxy.BMCServiceClient;

namespace Oritax.TaxSimp.Validators
{
    public class InnovaValidator : UserNamePasswordValidator
    {

        public override void Validate(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                throw new SecurityTokenException("Username and password required");

            bool status = false;
            using (BMCServiceClient client = new BMCServiceClient())
            {
                status = client.Authenticate(userName, password);
                client.Close();
            }

            if (!status)
            {
                throw new FaultException(string.Format("Authentication failed. User: {0} ", userName));
            }
        }

    }
}
