﻿using System.Configuration;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using System.ServiceModel;

namespace Oritax.TaxSimp.Validators
{
    public class AdministratorValidator : UserNamePasswordValidator
    {
        public static string UserName { get { return ConfigurationManager.AppSettings["UserName"]; } }
        public static string UserPassword { get { return ConfigurationManager.AppSettings["UserPassword"]; } }

        public override void Validate(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                throw new SecurityTokenException("Username and password required");

            bool status = false;

            //if (userName == "admin" && password == "admin") status = true;
            if (userName == UserName && password == UserPassword) status = true;

            if (!status)
            {
                throw new FaultException(string.Format("Authentication failed. User: {0} ", userName));
            }
        }
    }
}
