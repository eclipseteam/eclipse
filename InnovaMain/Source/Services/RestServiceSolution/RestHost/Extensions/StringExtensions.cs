﻿using System;
using System.Configuration;
using System.IO;

namespace Oritax.TaxSimp.Extensions
{
    public static class StringExtensions
    {
        private static string LogFileName { get { return ConfigurationManager.AppSettings["LogFileName"]; } }
        public static bool EnableLog { get { return Convert.ToBoolean(ConfigurationManager.AppSettings["EnableLog"]); } }

        public static void Log(this string value)
        {
            if (!EnableLog) return;

            using (StreamWriter writer = new StreamWriter(new FileStream(LogFileName, FileMode.Append, FileAccess.Write)))
            {
                writer.WriteLine(string.Format("{0:hh.mm.ssss} - {1}", DateTime.Now, value));
                writer.Close();
            }
        }
    }
}
