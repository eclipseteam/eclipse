﻿using System.ServiceModel.Channels;
using System.Xml;
using System.Xml.Linq;

namespace Oritax.TaxSimp.Writers
{
    public class XmlMessageWriter : BodyWriter
    {
        XDocument _Document;

        public XmlMessageWriter(XDocument document) : base(true)
        {
            _Document = document;
        }

        protected override void OnWriteBodyContents(XmlDictionaryWriter writer)
        {
            _Document.WriteTo(writer);
        }
    }
}
