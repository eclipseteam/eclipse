﻿using System;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Timers;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Services;
using Oritax.TaxSimp.Validators;

namespace Oritax.TaxSimp.Hosts
{
    public class FileEchoServiceHost
    {
        private Timer _Timer;
        private WebServiceHost _Host;
        public static string Uri { get { return ConfigurationManager.AppSettings["FileEchoServiceUri"]; } }
        public static bool EnableTimer { get { return Convert.ToBoolean(ConfigurationManager.AppSettings["EnableTimer"]); } }

        public FileEchoServiceHost()
        {
        }

        public void Start()
        {
            "Starting FileEchoServiceHost".Log();
            StartHost();
            StartTimer();
            "Started FileEchoServiceHost".Log();
        }

        public void Stop()
        {
            "Stoping FileEchoServiceHost".Log();
            StopHost();
            StopTimer();
            "Stopped FileEchoServiceHost".Log();
        }

        private void StartHost()
        {
            try
            {

                _Host = new WebServiceHost(typeof(FileEchoService));

                WebHttpBinding binding = new WebHttpBinding();
                binding.Security.Mode = WebHttpSecurityMode.Transport;
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

                ServiceCredentials credentials = new ServiceCredentials();
                credentials.UserNameAuthentication.UserNamePasswordValidationMode = System.ServiceModel.Security.UserNamePasswordValidationMode.Custom;
                credentials.UserNameAuthentication.CustomUserNamePasswordValidator = new AdministratorValidator();

                _Host.AddServiceEndpoint(typeof(IFileEchoService), binding, Uri);
                _Host.Description.Behaviors.Add(credentials);
                _Host.Open();
            }
            catch (Exception e)
            {
                e.Message.Log(); 
            }
        }

        private void StartTimer()
        {
            if (!EnableTimer) return;
            _Timer = new Timer(5 * 1000);
            _Timer.Elapsed += (s, a) => "Log by FileEchoServiceHost".Log();
            _Timer.Start();
        }

        private void StopHost()
        {
            _Host.Close();
            if (_Host is IDisposable) (_Host as IDisposable).Dispose();
            _Host = null;
        }

        private void StopTimer()
        {
            if (_Timer == null) return;
            _Timer.Stop();
            _Timer.Dispose();
            _Timer = null;
        }
    }
}
