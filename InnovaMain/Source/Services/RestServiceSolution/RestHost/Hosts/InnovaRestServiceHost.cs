﻿using System;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Services;
using Oritax.TaxSimp.Validators;

namespace Oritax.TaxSimp.Hosts
{
    public class InnovaRestServiceHost
    {
        private WebServiceHost _Host;
        public static string Uri { get { return ConfigurationManager.AppSettings["InnovaServiceUri"]; } }

        public InnovaRestServiceHost()
        {
        }

        public void Start()
        {
            "Starting InnovaRestServiceHost".Log();
            InitializeHost();
            "Started InnovaRestServiceHost".Log();
        }

        public void Stop()
        {
            "Stoping InnovaRestServiceHost".Log();
            _Host.Close();
            if (_Host is IDisposable) (_Host as IDisposable).Dispose();
            _Host = null;
            "Stopped InnovaRestServiceHost".Log();
        }

        private void InitializeHost()
        {
            try
            {

                _Host = new WebServiceHost(typeof(InnovaRestService));

                WebHttpBinding binding = new WebHttpBinding();
                binding.Security.Mode = WebHttpSecurityMode.Transport;

                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                
                //binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

                ServiceCredentials credentials = new ServiceCredentials();
       
                
                if (binding.Security.Transport.ClientCredentialType == HttpClientCredentialType.Certificate)
                {
                    credentials.ClientCertificate.Authentication.CertificateValidationMode = System.ServiceModel.Security.X509CertificateValidationMode.PeerOrChainTrust;
                    credentials.ClientCertificate.Authentication.TrustedStoreLocation = System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine;
                }
                else
                {
                    /// TODO: Following check should be removed  as soon as "Certificate" method is implemented 
                    credentials.UserNameAuthentication.UserNamePasswordValidationMode = System.ServiceModel.Security.UserNamePasswordValidationMode.Custom;
                    credentials.UserNameAuthentication.CustomUserNamePasswordValidator = new InnovaValidator();
                }

                _Host.AddServiceEndpoint(typeof(IInnovaRestService), binding, Uri);
                _Host.Description.Behaviors.Add(credentials);
                _Host.Open();
            }
            catch (Exception e)
            {
                e.Message.Log();
            }
        }

    }
}
