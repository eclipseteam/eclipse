﻿using System;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Services;
using Oritax.TaxSimp.Validators;

namespace Oritax.TaxSimp.Hosts
{
    public class InnovaEPIServiceHost
    {
        private WebServiceHost _Host;
        public static string Uri { get { return ConfigurationManager.AppSettings["EPIServiceUri"]; } }

        public InnovaEPIServiceHost()
        {
        }

        public void Start()
        {
        "Starting InnovaEPIServiceHost".Log();
            InitializeHost();
            "Started InnovaEPIServiceHost".Log();
        }

        public void Stop()
        {
        "Stoping InnovaEPIServiceHost".Log();
            _Host.Close();
            if (_Host is IDisposable) (_Host as IDisposable).Dispose();
            _Host = null;
            "Stopped InnovaEPIServiceHost".Log();
        }

        private void InitializeHost()
        {
            try
            {

                _Host = new WebServiceHost(typeof(InnovaEPIService));

                WebHttpBinding binding = new WebHttpBinding();
                binding.Security.Mode = WebHttpSecurityMode.Transport;

                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

                
                //binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

                ServiceCredentials credentials = new ServiceCredentials();
       
                
                if (binding.Security.Transport.ClientCredentialType == HttpClientCredentialType.Certificate)
                {
                    credentials.ClientCertificate.Authentication.CertificateValidationMode = System.ServiceModel.Security.X509CertificateValidationMode.PeerOrChainTrust;
                    credentials.ClientCertificate.Authentication.TrustedStoreLocation = System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine;
                }
                else
                {
                    /// TODO: Following check should be removed  as soon as "Certificate" method is implemented 
                    credentials.UserNameAuthentication.UserNamePasswordValidationMode = System.ServiceModel.Security.UserNamePasswordValidationMode.Custom;
                    credentials.UserNameAuthentication.CustomUserNamePasswordValidator = new InnovaValidator();
                }

                _Host.AddServiceEndpoint(typeof(IInnovaEPIService), binding, Uri);
                _Host.Description.Behaviors.Add(credentials);
                _Host.Open();
            }
            catch (Exception e)
            {
                e.Message.Log();
            }
        }

    }
}
