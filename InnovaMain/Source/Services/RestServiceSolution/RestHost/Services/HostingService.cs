﻿using System;
using System.Configuration;
using System.ServiceProcess;
using Oritax.TaxSimp.Hosts;

namespace Oritax.TaxSimp.Services
{
    public class HostingService : ServiceBase
    {
        public const string ServiceNameX = "TaxSimp Rest Service";
        FileEchoServiceHost _Echo;
        InnovaRestServiceHost _Innova;
         InnovaEPIServiceHost _epi;
        public static bool EnableEcho { get { return Convert.ToBoolean(ConfigurationManager.AppSettings["EnableFileEchoService"]); } }
        public static bool EnableRest { get { return Convert.ToBoolean(ConfigurationManager.AppSettings["EnableRestService"]); } }
        public static bool EnableEPI { get { return Convert.ToBoolean(ConfigurationManager.AppSettings["EnableEPIService"]); } }
        public HostingService()
        {
            base.ServiceName = HostingService.ServiceNameX;
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            StartX();
        }

        protected override void OnStop()
        {
            base.OnStop();
            StopX();
        }

        public void StartX()
        {
        if (EnableRest)
        {
            _Innova = new InnovaRestServiceHost();
            _Innova.Start();
        }
            if(EnableEPI)
            {
                _epi=new InnovaEPIServiceHost();
                _epi.Start();

            }

            if (EnableEcho) {
            _Echo = new FileEchoServiceHost();
            _Echo.Start();
            }
        }

        public void StopX()
            {
            if (_Innova != null) 
            _Innova.Stop();
            if (_epi != null)
                _epi.Stop();
            if(_Echo != null) _Echo.Stop();
        }
    }
}
