﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.Services
{
    [ServiceContract]
    public interface IFileEchoService
    {
        [OperationContract]
        [WebGet(UriTemplate = "name")]
        string GetAuthenticationName();

        [OperationContract]
        [WebGet(UriTemplate = "echo/{value}")]
        string Echo(string value);

    }

    public class FileEchoService : IFileEchoService
    {
        public string GetAuthenticationName()
        {
            ServiceSecurityContext context = OperationContext.Current.ServiceSecurityContext;
            string name = "None";
            if (context != null)
            {
                name = context.IsAnonymous ? "Anonymous" : context.PrimaryIdentity.Name;
            }
            return name;
        }

        public string Echo(string value)
        {
            string.Format("FileEchoService.Echo - {0}", value).Log();
            return string.Format("({0:hh.mm.ssss}) Echo : {1} ", DateTime.Now, value);
        }

    }
}

