﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;
using System.Xml;
using System.Xml.Linq;
using BMCServiceProxy.BMCServiceClient;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Writers;
using EPI.Data;
using System.Text;
namespace Oritax.TaxSimp.Services
{
   
   [ServiceContract]
    public interface IInnovaEPIService
    {
        [OperationContract]
        [WebGet(UriTemplate = "GetDataResponseAllClients/")]
        XElement GetDataResponseAllClients();

        [OperationContract]
        [WebGet(UriTemplate = "GetASXSecuritiesDataResponse/")]
        XElement GetASXSecuritiesDataResponseAllClients();

        [OperationContract]
        [WebGet(UriTemplate = "GetAllSecuritiesDataResponse/")]
        XElement GetAllSecuritiesDataResponse();


        

    }

    public class InnovaEPIService : IInnovaEPIService
    {
        public XElement GetAllSecuritiesDataResponse()
        {
            var item = new BMCServiceDataItem { Type = (int)WellKnownRestCommand.EPIDataReposneAllSecurities, Data = string.Empty };
            System.Console.WriteLine("Request Received >>GetDataReposneAllSecurities" + DateTime.Now);
            return Process(item);
        }

        public XElement GetDataResponseAllClients()
        {
            var item = new BMCServiceDataItem { Type = (int)WellKnownRestCommand.EPIDataReposneAllClient, Data = string.Empty };
            System.Console.WriteLine("Request Received >>GetDataResponseAllClients"+DateTime.Now);
            return Process(item);
        }


        public XElement GetASXSecuritiesDataResponseAllClients()
        {
            var item = new BMCServiceDataItem { Type = (int)WellKnownRestCommand.EPIASXDataReposneAllClient, Data = string.Empty };
            System.Console.WriteLine("Request Received >>GetASXSecuritiesDataResponseAllClients" + DateTime.Now);
            return Process(item);
        }

        
        private XElement Process(BMCServiceDataItem item) 
        {
        string xml =string.Empty;
        using (BMCServiceClient client = new BMCServiceClient())
        {
            xml = client.GetQueryData(item.ToXmlString(),true);
            
            client.Close();
        }
        System.Console.WriteLine("Response sent >>" + ((WellKnownRestCommand)item.Type).ToString() + DateTime.Now);
        

            return  XElement.Parse(StringCompression.Decompress(xml));
        }
    }





   
}

