﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Xml.Linq;
using BMCServiceProxy.BMCServiceClient;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Writers;

namespace Oritax.TaxSimp.Services
{
    [ServiceContract]
    public interface IInnovaRestService
    {
        [OperationContract]
        [WebGet(UriTemplate = "AllModels/")]
        Message GetAllModels();

        [OperationContract]
        [WebGet(UriTemplate = "EclipseSuperBasic/")]
        Message GetEclipseSuperBasic();

        [OperationContract]
        [WebGet(UriTemplate = "ModelDetailsById/{modelid}")]
        Message GetModelDetailsByID(string modelID);

        [OperationContract]
        [WebGet(UriTemplate = "AllClients/")]
        Message GetAllClients();

        [OperationContract]
        [WebGet(UriTemplate = "ClientType/{type}")]
        Message GetClientByType(string type);

        [OperationContract]
        [WebGet(UriTemplate = "ClientName/{name}")]
        Message GetClientByName(string name);

        [OperationContract]
        [WebGet(UriTemplate = "ClientFinancials/{clientid}")]
        Message ClientFinancials(string clientid);

        [OperationContract]
        [WebGet(UriTemplate = "AllClientsByAdviserID/{adviserid}")]
        Message ClientByAdvisers(string adviserID);

        [OperationContract]
        [WebGet(UriTemplate = "AllClientsByIFAID/{ifaID}")]
        Message ClientByIFA(string ifaID);


    }

    public class InnovaRestService : IInnovaRestService
    {
        public Message GetAllModels()
        {
            var item = new BMCServiceDataItem { Type = (int)WellKnownRestCommand.AllModels, Data = string.Empty };
            return Process(item);
        }

        public Message GetEclipseSuperBasic()
        {
            var item = new BMCServiceDataItem { Type = (int)WellKnownRestCommand.SuperManagersEclipseSuperBasic, Data = string.Empty };
            return Process(item);
        }

        public Message GetModelDetailsByID(string modelID)
        {
            var item = new BMCServiceDataItem { Type = (int)WellKnownRestCommand.ModelDetailsByID, Data = modelID };
            return Process(item);
        }

        public Message GetAllClients()
        {
            var item = new BMCServiceDataItem { Type = (int)WellKnownRestCommand.AllClientsMainData, Data = string.Empty };
            return Process(item);
        }

        public Message GetClientByType(string type)
        {
            var item = new BMCServiceDataItem { Type = (int)WellKnownRestCommand.AllClientByType, Data = type };
            return Process(item);
        }

        public Message GetClientByName(string name)
        {
            var item = new BMCServiceDataItem { Type = (int)WellKnownRestCommand.ClientByName, Data = name };
            return Process(item);
        }

        public Message ClientFinancials(string clientid)
        {
            var item = new BMCServiceDataItem { Type = (int)WellKnownRestCommand.ClientFinancialDetails, Data = clientid };
            return Process(item);
        }

        public Message ClientByAdvisers(string adviserID)
        {
            var item = new BMCServiceDataItem { Type = (int)WellKnownRestCommand.AllClientsByAdviserID, Data = adviserID };
            return Process(item);
        }

        public Message ClientByIFA(string ifaID)
        {
            var item = new BMCServiceDataItem { Type = (int)WellKnownRestCommand.AllClientsByIFAID, Data = ifaID };
            return Process(item);
        }

        private Message Process(BMCServiceDataItem item)
        {
            string xml = string.Empty;
            using (BMCServiceClient client = new BMCServiceClient())
            {
                xml = client.GetQueryData(item.ToXmlString(),true);
                client.Close();
            }
            XDocument document = XDocument.Parse(StringCompression.Decompress(xml));
            document.Root.RemoveAttributes();
            XmlMessageWriter writer = new XmlMessageWriter(document);
            Message message = Message.CreateMessage(MessageVersion.None, OperationContext.Current.OutgoingMessageHeaders.Action, writer);
            return message;
        }
    }
}

