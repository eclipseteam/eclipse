﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;
using Oritax.TaxSimp.Services;

namespace Oritax.TaxSimp.Installers
{
    [RunInstaller(true)]
    public class HostingServiceInstaller : Installer
    {
        public HostingServiceInstaller()
        {
            var process = new ServiceProcessInstaller();
            var service = new ServiceInstaller();

            process.Account = ServiceAccount.LocalSystem;

            service.ServiceName = HostingService.ServiceNameX;
            service.DisplayName = HostingService.ServiceNameX;
            service.StartType = ServiceStartMode.Manual;

            this.Installers.Add(process);
            this.Installers.Add(service);
        }
    }
}
