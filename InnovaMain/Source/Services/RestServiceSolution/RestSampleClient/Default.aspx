﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="RestSampleClient.Default" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="height: 675px">
    <form id="form1" runat="server" defaultbutton="ButtonGet" defaultfocus="TextUri">
    <div>
        <%--"https://shamil-pc/Innova2011/Services/ClientName/Entity 1"--%>
        &nbsp;&nbsp;&nbsp;&nbsp;<b>Url</b>&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="TextUri" Text="https://shamil-pc/Innova2011/Services/AllClients/" Width="600" runat="server" />
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;<b>User Id</b>&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="TextUserId" Text="Administrator" Width="150" runat="server" />
        &nbsp;&nbsp;&nbsp;&nbsp;<b>Password</b>&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="TextPassword" Text="$Admin#1" Width="150" runat="server" />
        <br />
        <br />
        <asp:Button ID="ButtonGet" Text="Get Response" Width="120" D runat="server" onclick="ButtonGet_Click" />
        <br/>
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;<b>Xml Response</b>
        <br />
        <br />
    </div>
    <div>
        <asp:TextBox ID="TextResponse" Width="100%" Height="550px" runat="server" BorderStyle="Solid" ReadOnly="True" TextMode="MultiLine" Wrap="False" BackColor="#CCCCCC" />
    </div>
    </form>
</body>
</html>
