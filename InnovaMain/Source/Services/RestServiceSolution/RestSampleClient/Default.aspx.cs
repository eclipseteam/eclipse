﻿using System;
using System.IO;
using System.Net;
using System.Web.UI;
using System.Xml.Linq;

namespace RestSampleClient
{
    public partial class Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonGet_Click(object sender, EventArgs e)
        {
            try
            {
                string uri = TextUri.Text;
                System.Net.ServicePointManager.ServerCertificateValidationCallback = 
                    (s, certificate, chain, sslPolicyErrors) => true;

                HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;
                request.Credentials = new NetworkCredential(TextUserId.Text, TextPassword.Text);
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    string xml = reader.ReadToEnd();
                    XDocument doc = XDocument.Parse(xml);
                    TextResponse.Text = doc.ToString();
                }
            }
            catch (Exception ex)
            {
                TextResponse.Text = string.Format("Exception : {0}", ex.Message);
            }
        }
    }
}