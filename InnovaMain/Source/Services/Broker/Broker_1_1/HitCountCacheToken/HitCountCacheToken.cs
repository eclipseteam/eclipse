using System;

using Oritax.TaxSimp.Services.Caching;

namespace Oritax.TaxSimp.Services.Caching.CacheTokens
{
	/// <summary>
	/// A legacy hit count method of caching
	/// </summary>
	public class HitCountCacheToken : CacheToken
	{
		#region Private Members
		private int hitCount;
		#endregion //Private Members
		#region Constructors
		/// <summary>
		/// Creates a HitCountCacheToken
		/// </summary>
		/// <param name="cacheabilityType"></param>
		public HitCountCacheToken(CacheabilityType cacheabilityType) : base(cacheabilityType)
		{
			this.hitCount = 0;
		}
		#endregion //Constructors
		#region Public Methods
		/// <summary>
		/// Increments the hitcount when the cache item is requested
		/// </summary>
		public override void CacheItemRequested()
		{
			this.hitCount++;
		}

		/// <summary>
		/// Tests if 2 HitCountCacheTokens are equal
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			if(!base.Equals(obj))
				return false;

			if(!(obj is HitCountCacheToken))
				return false;

			HitCountCacheToken cacheToken = (HitCountCacheToken) obj;
			return(cacheToken.hitCount == this.hitCount);
		}

		/// <summary>
		/// Gets the hash code for the HitCountCacheToken
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return base.GetHashCode ();
		}

		/// <summary>
		/// Tests if a HitCountCacheToken is less than another
		/// </summary>
		/// <param name="token1">The token to check is less than token2</param>
		/// <param name="token2">The token that is checked to be greater than token1</param>
		/// <returns>True if token1 is less than token 2, false otherwise</returns>
		public static bool operator < (HitCountCacheToken token1, HitCountCacheToken token2)
		{
			if(token1.CacheabilityType != token2.CacheabilityType)
				return((CacheToken)token1 < (CacheToken)token2);
			else
				return (token1.hitCount < token2.hitCount);
		}

		/// <summary>
		/// Tests if a HitCountCacheToken is greater than another
		/// </summary>
		/// <param name="token1">The token to check is greater than token2</param>
		/// <param name="token2">The token that is checked to be less than token1</param>
		/// <returns>True if token1 is greater than token 2, false otherwise</returns>
		public static bool operator > (HitCountCacheToken token1, HitCountCacheToken token2)
		{
			if(token1.CacheabilityType != token2.CacheabilityType)
				return((CacheToken)token1 > (CacheToken)token2);
			else
				return (token1.hitCount > token2.hitCount);
		}

		/// <summary>
		/// Tests if a HitCountCacheToken is equal to this
		/// </summary>
		/// <param name="obj">The HitCountCacheToken item to compare this against</param>
		/// <returns>0 if the objects are equal, -1 if this is less than obj, 1 otherwise</returns>
		public override int CompareTo(object obj)
		{
			HitCountCacheToken cacheToken = (HitCountCacheToken)obj;

			if(obj.Equals(this))
				return 0;
			if(this <cacheToken)
				return -1;
			else
				return 1;
		}
		#endregion //Public Methods
	}
}
