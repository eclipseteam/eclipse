using System;

namespace Oritax.TaxSimp.Services.Caching.CacheTokens
{
	/// <summary>
	/// 
	/// </summary>
	public class InstallHitCountCacheToken
	{
		#region INSTALLATION PROPERTIES
		// Assembly Installation Properties
		public const string ASSEMBLY_ID="C11F793C-F3FB-416d-B230-E12E9C4A1EF5";
		public const string ASSEMBLY_NAME="HitCountCacheToken_1_1";
		public const string ASSEMBLY_DISPLAYNAME="HitCountCacheToken";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
//		public const string ASSEMBLY_REVISION="0"; 
		public const string ASSEMBLY_REVISION="1"; //2005.1 
		#endregion
	}
}
