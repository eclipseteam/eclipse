﻿using Oritax.TaxSimp.Services.VelocityCache;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;

namespace VelocityTest
{
    
    
    /// <summary>
    ///This is a test class for VelocityDataCacheTest and is intended
    ///to contain all VelocityDataCacheTest Unit Tests
    ///</summary>
    [TestClass()]
    public class VelocityDataCacheTest
    {
        private const string VELOCITY_SLOT = "VelocitySlot";

        private VelocityDataCache _DataCache
        {
            get
            {
                VelocityDataCache dataCache;
                LocalDataStoreSlot _VelocitySlot = Thread.GetNamedDataSlot(VelocityDataCacheTest.VELOCITY_SLOT);

                if (_VelocitySlot == null)
                    _VelocitySlot = Thread.AllocateNamedDataSlot(VelocityDataCacheTest.VELOCITY_SLOT);

                dataCache = (VelocityDataCache)Thread.GetData(_VelocitySlot);

                if (dataCache == null)
                {
                    dataCache = new VelocityDataCache();
                    Thread.SetData(_VelocitySlot, dataCache);
                }

                return dataCache;
            }
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for PutObject
        ///</summary>
        [TestMethod()]
        public void PutObjectTest()
        {
            Guid id = Guid.NewGuid();
            Random r = new Random();

            int obj = r.Next();
            bool expected = true;
            bool actual;

            actual = _DataCache.PutObject(id, obj);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void GetObjectTest()
        {
            Guid id = Guid.NewGuid();
            Random r = new Random();

            int expected = r.Next();
            int actual;

            _DataCache.PutObject(id, expected);
            actual = (int)_DataCache.GetObejct(id);

            Assert.AreEqual(expected, actual);


        }

        [TestMethod()]
        public void ConcurrencyTest()
        {
            VelocityDataCache cache1 = new VelocityDataCache();
            VelocityDataCache cahce2 = new VelocityDataCache();

            Guid id = Guid.NewGuid();
            Random r = new Random();
            int obj1 = r.Next();
            int obj2 = r.Next();
            int obj3 = r.Next();

            bool actual1 = cache1.PutObject(id, obj1);
            bool actual2 = cahce2.PutObject(id, obj2);
            bool actual3 = cache1.PutObject(id, obj3);

            Assert.AreEqual(actual1, true);
            //!!! obj2 should be put successfully, as the value of id is never retrieved before in cache2.
            //!!! This situation should be avoided in rela situation.
            Assert.AreEqual(actual2, true);
            Assert.AreEqual(actual3, false, "Third update should fail as of version mismatch.");
        }

    }
}
