using System;

namespace Oritax.TaxSimp.Services.Caching.CacheTokens
{
	/// <summary>
	/// 
	/// </summary>
	public class InstallTemperatureCacheToken
	{
		#region INSTALLATION PROPERTIES
		// Assembly Installation Properties
		public const string ASSEMBLY_ID="0B2300C8-FD83-4602-B7BF-C24CFB837DA9";
		public const string ASSEMBLY_NAME="TemperatureCacheToken_1_1";
		public const string ASSEMBLY_DISPLAYNAME="TemperatureCacheToken";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
//		public const string ASSEMBLY_REVISION="0"; 
		public const string ASSEMBLY_REVISION="1"; //2005.1 

		#endregion
	}
}
