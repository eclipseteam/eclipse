using System;
using System.Runtime.Serialization;

using Oritax.TaxSimp.Services.Caching;

namespace Oritax.TaxSimp.Services.Caching.CacheTokens
{
	/// <summary>
	/// The temperature cache token caches elements based on the load time and the number of times that a cache item has been used
	/// </summary>
    [Serializable]
    public class TemperatureCacheToken : CacheToken
	{
		#region Private Members
		private const double LOADFACTOR = 100d;
        private DateTime creationTime;
        private DateTime startLoadTime;
        private DateTime endLoadTime;
        private TimeSpan loadTime;
        private int hitCount;
        private int temperature;
		#endregion //Private Members
		#region Constructors
		/// <summary>
		/// Constructs a TemperatureCacheToken
		/// </summary>
		/// <param name="cacheabilityType"></param>
		public TemperatureCacheToken(CacheabilityType cacheabilityType) : base(cacheabilityType)
		{
			this.creationTime = DateTime.Now;
			this.hitCount = 0;
			this.ComputeTemperature( );
		}
		#endregion //Constructors
		#region Public Methods
		/// <summary>
		/// Logs when the cache item started loading
		/// </summary>
        public override void CacheItemLoadStart()
		{
			this.startLoadTime = DateTime.Now;
			this.ComputeTemperature( );
		}

		/// <summary>
		/// Logs when the cache item was finished loading
		/// </summary>
		public override void CacheItemLoadEnd()
		{
			this.endLoadTime = DateTime.Now;
			this.loadTime = this.endLoadTime.Subtract(this.startLoadTime);
			this.ComputeTemperature( );
		}

		/// <summary>
		/// Logs when the cache item is requested
		/// </summary>
		public override void CacheItemRequested()
		{
			this.hitCount++;
			this.ComputeTemperature( );
		}

		/// <summary>
		/// Logs when the cache item is released
		/// </summary>
		public override void CacheItemReleased()
		{
			this.hitCount--;
			this.ComputeTemperature( );
		}

		/// <summary>
		/// Tests if a TemperatureCacheItem is the same as another
		/// </summary>
		/// <param name="obj">The TemperatureCacheItem to compare against</param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			if(!base.Equals(obj))
				return false;

			if(!(obj is TemperatureCacheToken))
				return false;

			TemperatureCacheToken cacheToken = (TemperatureCacheToken) obj;
			return(cacheToken.temperature == this.temperature);
		}

		/// <summary>
		/// Gets the hashcode for the TemperatureCacheToken
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return base.GetHashCode ();
		}

		/// <summary>
		/// Tests if a TemperatureCacheItem is less than another
		/// </summary>
		/// <param name="token1">The token to check is less than token2</param>
		/// <param name="token2">The token that is checked to be greater than token1</param>
		/// <returns>True if token1 is less than token 2, false otherwise</returns>
		public static bool operator < (TemperatureCacheToken token1, TemperatureCacheToken token2)
		{
			if(token1.CacheabilityType != token2.CacheabilityType)
				return((CacheToken)token1 < (CacheToken)token2);
			else
				return (token1.temperature < token2.temperature);
		}

		/// <summary>
		/// Tests if a TemperatureCacheItem is greater than another
		/// </summary>
		/// <param name="token1">The token to check is greater than token2</param>
		/// <param name="token2">The token that is checked to be less than token1</param>
		/// <returns>True if token1 is greater than token 2, false otherwise</returns>
		public static bool operator > (TemperatureCacheToken token1, TemperatureCacheToken token2)
		{
			if(token1.CacheabilityType != token2.CacheabilityType)
				return((CacheToken)token1 > (CacheToken)token2);
			else
				return (token1.temperature > token2.temperature);
		}

		/// <summary>
		/// Tests if a TemperatureCacheToken is equal to this
		/// </summary>
		/// <param name="obj">The TemperatureCacheToken item to compare this against</param>
		/// <returns>0 if the objects are equal, -1 if this is less than obj, 1 otherwise</returns>
		public override int CompareTo(object obj)
		{
			TemperatureCacheToken cacheToken = (TemperatureCacheToken)obj;

			if(obj.Equals(this))
				return 0;
			if(this <cacheToken)
				return -1;
			else
				return 1;
		}
		#endregion //Public Methods
		#region Properties
		/// <summary>
		/// The temperature of the cache token
		/// </summary>
        public int Temperature
		{
			get
			{
				return this.temperature;
			}
            set
            {
                this.temperature = value;
            }
		}
		#endregion //Properties
		#region Private Methods
		private void ComputeTemperature( )
		{
			int loadTimePoints = Convert.ToInt32( this.loadTime.TotalSeconds * LOADFACTOR );
			int adjustedLoadTimePoints = loadTimePoints + hitCount;
			this.temperature = adjustedLoadTimePoints;
		}
		#endregion //Private Methods
	}
}
