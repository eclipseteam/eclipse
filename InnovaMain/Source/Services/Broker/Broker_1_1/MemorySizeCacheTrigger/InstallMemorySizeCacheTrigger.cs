using System;

namespace Oritax.TaxSimp.Services.Caching.CacheTriggers
{
	/// <summary>
	/// 
	/// </summary>
	public class InstallMemorySizeCacheTrigger
	{
		#region INSTALLATION PROPERTIES
		// Assembly Installation Properties
		public const string ASSEMBLY_ID="5851E4B7-DD80-4927-94D8-AE543DA89FFF";
		public const string ASSEMBLY_NAME="MemorySizeCacheTrigger_1_1";
		public const string ASSEMBLY_DISPLAYNAME="MemorySizeCacheTrigger";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		//public const string ASSEMBLY_REVISION="0";	//2004.3
		public const string ASSEMBLY_REVISION="1";		//2005.1
		#endregion
	}
}
