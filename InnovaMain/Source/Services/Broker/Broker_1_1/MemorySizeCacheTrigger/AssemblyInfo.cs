using System.Reflection;
using System.Runtime.CompilerServices;

using Oritax.TaxSimp.Services.Caching.CacheTriggers;
using Oritax.TaxSimp.InstallInfo;

[assembly: AssemblyTitle("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]		
[assembly: AssemblyVersion(InstallMemorySizeCacheTrigger.ASSEMBLY_MAJORVERSION+"."+InstallMemorySizeCacheTrigger.ASSEMBLY_MINORVERSION)]
[assembly: AssemblyInformationalVersion(InstallMemorySizeCacheTrigger.ASSEMBLY_MAJORVERSION+"."+InstallMemorySizeCacheTrigger.ASSEMBLY_MINORVERSION+"."+InstallMemorySizeCacheTrigger.ASSEMBLY_DATAFORMAT+"."+InstallMemorySizeCacheTrigger.ASSEMBLY_REVISION)]
[assembly: AssemblyInstallInfo(InstallMemorySizeCacheTrigger.ASSEMBLY_ID,InstallMemorySizeCacheTrigger.ASSEMBLY_NAME,InstallMemorySizeCacheTrigger.ASSEMBLY_DISPLAYNAME)]
[assembly: AssemblyDelaySign(false)]
