using System;
using System.Configuration;

using Oritax.TaxSimp.Services.Caching;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Services.Caching.CacheTriggers
{
	/// <summary>
	/// Memory size cache trigger will cause collection when the memory used by the application exceeds a given threshold
	/// </summary>
	public class MemorySizeCacheTrigger : CacheCollectionTrigger
	{
		#region Private Members
		private const double COLLECTIONLOWERBOUND = 0.9d;
		private long maximumMemorySize;
		private int cacheReductionPercentage;
		private long collectStartSize;
		#endregion //Private Members
		#region Constructors
		/// <summary>
		/// Constructs a memory size cache trigger
		/// </summary>
		public MemorySizeCacheTrigger()
		{
            this.maximumMemorySize = Convert.ToInt64(ConfigurationManager.AppSettings["CachingMemoryTriggerMaxMemorySize"]);
            this.cacheReductionPercentage = Convert.ToInt32(ConfigurationManager.AppSettings["CachingMemoryTriggerCacheReductionPercentage"]);
			this.collectStartSize = Convert.ToInt64(((double)this.maximumMemorySize)*COLLECTIONLOWERBOUND);
		}
		#endregion
		#region Public Methods
		/// <summary>
		/// Checks if the memory size of the application has exceeded the threshold
		/// </summary>
		/// <returns>True if the memory size has exceeded the threshold</returns>
		public override bool CheckCacheCollectionTriggered()
		{
			return (GC.GetTotalMemory(false) > this.collectStartSize);
		}

		/// <summary>
		/// Checks if the memory use of the resized cache is now within the threshold
		/// </summary>
		/// <returns>True if the memory use of the resized cache is within the threshold, false otherwise</returns>
		public override bool IsResizedCacheWithinParameters()
		{
			return (GC.GetTotalMemory(false) <= this.collectStartSize);
		}

		/// <summary>
		/// Returns the percentage to reduce the cache by, as specified by the user in the web.config file 
		/// </summary>
		/// <returns>The reduction percentage specified by the web.config file</returns>
		public override int ReductionPercentage()
		{
			return this.cacheReductionPercentage;
		}
		#endregion //Public Methods
	}
}
