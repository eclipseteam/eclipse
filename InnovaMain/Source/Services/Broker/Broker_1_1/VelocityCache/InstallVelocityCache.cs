﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Services.VelocityCache
{
    public class InstallVelocityCache
    {
        public const string ASSEMBLY_ID = "5E51F478-9126-11DE-A14E-165A55D89593";
        public const string ASSEMBLY_NAME = "VelocityCache_1_1";
        public const string ASSEMBLY_DISPLAYNAME = "VelocityCache V1.1";
        public const string ASSEMBLY_MAJORVERSION = "1";
        public const string ASSEMBLY_MINORVERSION = "1";
        public const string ASSEMBLY_DATAFORMAT = "0";
        public const string ASSEMBLY_REVISION = "0";

        // Associated File Installation Properties
        // Workpapers
        public const string ASSOCIATED_FILE_VELOCITY_DLL_BASE = "CacheBaseLibrary.dll";
        public const string ASSOCIATED_FILE_VELOCITY_DLL_CLIENT = "ClientLibrary.dll";
        public const string ASSOCIATED_FILE_VELOCITY_DLL_FABRIC = "FabricCommon.dll";
        public const string ASSOCIATED_FILE_VELOCITY_DLL_CASBASE = "CASBase.dll";

    }
}
