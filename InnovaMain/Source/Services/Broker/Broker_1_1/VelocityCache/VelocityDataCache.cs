﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.Data.Caching;

namespace Oritax.TaxSimp.Services.VelocityCache
{
     /// <summary>
     /// Attention:-
    /// 1. Only create one instance VelocityDataCache for each thread, as Initialise() takes long time.
    /// 2. Don't make VelocityDataCache static, as there's no concurrency control inside VelocityDataCache.
     /// </summary>
    public class VelocityDataCache
    {
        protected Dictionary<Guid, DataCacheItemVersion> _VersionDictionary = new Dictionary<Guid, DataCacheItemVersion>();
        protected DataCache _DC;

        public VelocityDataCache()
        {
            Initialise();
        }

        public virtual object GetObejct(Guid id)
        {
            Object obj = null;

            obj = _DC.Get(id.ToString());
            
            UpdateVersionDict(id);

            return obj;
        }

        public virtual bool PutObject(Guid id, Object obj)
        {
            if (_VersionDictionary.ContainsKey(id))
                return this.UpdateExistingEntry(id, obj, _VersionDictionary[id]);
            else
                return this.InsertNewEntry(id, obj);

        }

        #region private fields & methods

        protected virtual bool InsertNewEntry(Guid id, Object obj)
        {
            DataCacheItemVersion newItemVersion = _DC.Put(id.ToString(), obj);
            
            if (newItemVersion == null)
                return false;
            else
            {
                _VersionDictionary[id] = newItemVersion;
                return true;
            }
        }

        protected virtual bool UpdateExistingEntry(Guid id, Object obj, DataCacheItemVersion oldVersion)
        {
            try
            {
                DataCacheItemVersion newItemVersion = _DC.Put(id.ToString(), obj, oldVersion);
                if (newItemVersion == null)
                    return false;
                else
                {
                    _VersionDictionary[id] = newItemVersion;
                    return true;
                }
            }
            catch (DataCacheException e)
            {
                if (e.ErrorCode == DataCacheErrorCode.CacheItemVersionMismatch)
                    return false;
                else
                    throw;
            }
        }

        protected virtual void UpdateVersionDict(Guid id)
        {
            DataCacheItem cacheItem = _DC.GetCacheItem(id.ToString());
            DataCacheItemVersion cacheItemVersion = cacheItem.Version;
            _VersionDictionary[id] = cacheItemVersion;

        }

        protected virtual void Initialise()
        {
            int serverCount = VelocityConfiguration.VelocityServers.GetLength(0);

            DataCacheServerEndpoint[] servers = new DataCacheServerEndpoint[serverCount];

            for (int i = 0; i < serverCount; i++)
            {
                servers[i] = new DataCacheServerEndpoint(
                    VelocityConfiguration.VelocityServers[i], 
                    VelocityConfiguration.CachePort,
                    VelocityConfiguration.CacheHostName
                    );
            }

            // specify cache client configuration
            DataCacheFactory cacheFactory = new DataCacheFactory(
                servers,
                VelocityConfiguration.IsReoutingClient,
                VelocityConfiguration.IsLocalCacheEnabled,
                (DataCacheLocalCacheSyncPolicy)Enum.Parse(typeof(DataCacheLocalCacheSyncPolicy), VelocityConfiguration.LocalCacheSyncPolicy),
                VelocityConfiguration.LocalCacheTimeOut,
                VelocityConfiguration.PollInterval
                );

            // get cache client for cache "FirstCache"
            _DC = cacheFactory.GetCache(VelocityConfiguration.VelocityCacheName);

        }

        #endregion


    }
}
