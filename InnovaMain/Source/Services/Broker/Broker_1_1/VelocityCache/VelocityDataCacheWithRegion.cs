﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Data.Caching;

namespace Oritax.TaxSimp.Services.VelocityCache
{
    public class VelocityDataCacheWithRegion : VelocityDataCache
    {
        protected string _RegionName;

        public void Remove(Guid id)
        {
            this._DC.Remove(id.ToString(), _RegionName);
        }

        public void RemoveAll()
        {
            this._DC.ClearRegion(_RegionName);
        }

        public Hashtable GetCollectionList()
        {
            Hashtable ht = new Hashtable();

            IEnumerable<KeyValuePair<string, Object>> keyValuePairs = this._DC.GetObjectsInRegion(_RegionName);
            foreach (KeyValuePair<string, Object> kvPair in keyValuePairs)
                ht.Add(kvPair.Key, kvPair.Value);

            return ht;
        }

        public VelocityDataCacheWithRegion(string regionName) : base()
        {
            _RegionName = regionName;
            this.CreateRegion();
        }

        public override object GetObejct(Guid id)
        {
            Object obj = null;

            obj = _DC.Get(id.ToString(), _RegionName);

            this.UpdateVersionDict(id);

            return obj;
        }

        public override bool PutObject(Guid id, Object obj)
        {
            if (_VersionDictionary.ContainsKey(id))
                return this.UpdateExistingEntry(id, obj, _VersionDictionary[id]);
            else
                return this.InsertNewEntry(id, obj);

        }

        protected override bool InsertNewEntry(Guid id, Object obj)
        {
            DataCacheItemVersion newItemVersion = _DC.Put(id.ToString(), obj, this._RegionName);

            if (newItemVersion == null)
                return false;
            else
            {
                _VersionDictionary[id] = newItemVersion;
                return true;
            }
        }

        protected override bool UpdateExistingEntry(Guid id, Object obj, DataCacheItemVersion oldVersion)
        {
            try
            {
                DataCacheItemVersion newItemVersion = _DC.Put(id.ToString(), obj, oldVersion, this._RegionName);
                if (newItemVersion == null)
                    return false;
                else
                {
                    _VersionDictionary[id] = newItemVersion;
                    return true;
                }
            }
            catch (DataCacheException e)// exception ERRCA001, optimistic concurrency model update
            {
                if (e.ErrorCode == DataCacheErrorCode.CacheItemVersionMismatch)
                    return false;
                else
                    throw;
            }
        }


        protected override void UpdateVersionDict(Guid id)
        {
            // Save a thread specific DataCacheItemVersion dictionary in "GetObject", which is to be used in "PutObject".
            DataCacheItem cacheItem = _DC.GetCacheItem(id.ToString(), _RegionName);
            DataCacheItemVersion cacheItemVersion = cacheItem.Version;
            _VersionDictionary[id] = cacheItemVersion;

        }


        protected void CreateRegion()
        {
            try
            {
                this._DC.CreateRegion(_RegionName, true);
            }
            catch (DataCacheException e)
            {
                if (e.ErrorCode != DataCacheErrorCode.RegionAlreadyExists)
                    throw;
            }
        }



    }
}
