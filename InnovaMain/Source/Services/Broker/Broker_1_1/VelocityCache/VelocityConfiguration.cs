﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Oritax.TaxSimp.Services.VelocityCache
{
    public static class VelocityConfiguration
    {
        private const string IS_VELOCITY_CONFIGURED = "IsVelocityConfigured";
        private const string IS_REOUTING_CLIENT = "IsReoutingClient";
        private const string IS_LOCAL_CACHE_ENABLED = "IsLocalCacheEnabled";
        private const string VELOCITY_SERVERS = "VelocityServers";
        private const string VELOCITY_CACHE_NAME = "VelocityCacheName";
        private const string VELOCITY_CACHE_PORT = "CachePort";
        private const string VELOCITY_CACHE_HOST_NAME = "CacheHostName";
        private const string LOCAL_CACHE_SYNC_POLICY = "LocalCacheSyncPolicy";
        private const string LOCAL_CACHE_TIME_OUT = "LocalCacheTimeOut";
        private const string POLL_INTERVAL = "PollInterval";

        public static int CachePort
        {
            get 
            {
                // default value 22233
                string configuredValue = ConfigurationSettings.AppSettings[VELOCITY_CACHE_PORT];
                return Convert.ToInt32(configuredValue);
            }
        }

        public static string CacheHostName
        {
            get 
            {
                // default value DistributedCacheService
                string configuredValue = ConfigurationSettings.AppSettings[VELOCITY_CACHE_HOST_NAME];
                return configuredValue;
            }
        }

        public static bool IsVelocityConfigured
        {
            get
            {
                string configuredValue = ConfigurationSettings.AppSettings[IS_VELOCITY_CONFIGURED].ToUpper();
                if (configuredValue == "TRUE" || configuredValue == "YES")
                    return true;
                else
                    return false;
            }
        }

        public static bool IsReoutingClient
        {
            get
            {
                string configuredValue = ConfigurationSettings.AppSettings[IS_REOUTING_CLIENT].ToUpper();
                if (configuredValue == "TRUE" || configuredValue == "YES")
                    return true;
                else
                    return false;
            }
        }

        public static bool IsLocalCacheEnabled
        {
            get
            {
                string configuredValue = ConfigurationSettings.AppSettings[IS_LOCAL_CACHE_ENABLED].ToUpper();
                if (configuredValue == "TRUE" || configuredValue == "YES")
                    return true;
                else
                    return false;
            }
        }

        

        public static string[] VelocityServers
        {
            get
            {
                string configuredValue = ConfigurationSettings.AppSettings[VELOCITY_SERVERS];

                return configuredValue.Split(new char[] { ';' });
            }
        }

        public static string VelocityCacheName
        {
            get
            {
                string configuredValue = ConfigurationSettings.AppSettings[VELOCITY_CACHE_NAME];

                return configuredValue;
            }
        }

        public static string LocalCacheSyncPolicy
        {
            get
            {
                string configuredValue = ConfigurationSettings.AppSettings[LOCAL_CACHE_SYNC_POLICY];

                return configuredValue;
            }
        }

        public static int LocalCacheTimeOut
        {
            get
            {
                string configuredValue = ConfigurationSettings.AppSettings[LOCAL_CACHE_TIME_OUT];

                return Convert.ToInt32(configuredValue);
            }
        }

        public static int PollInterval
        {
            get
            {
                string configuredValue = ConfigurationSettings.AppSettings[POLL_INTERVAL];

                return Convert.ToInt32(configuredValue);
            }
        }

        

    }
}
