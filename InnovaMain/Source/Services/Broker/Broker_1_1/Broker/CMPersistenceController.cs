using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CommonPersistence;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.VelocityCache;
using System.Reflection;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Collections.Generic;

namespace Oritax.TaxSimp.Services.CMBroker
{
    /// <summary>
    /// Provides persistence services of CM instances and types via the use of a persister, supplied externally.
    /// </summary>
    public class CMPersistenceController
    {
        //=============================================================================================================
        // CM Catalog Functions

        //---------------------------
        // BCM Summary list functions
        [Obsolete("ListBCMsOfTypeID is deprecated; use ListBMCsOfTypeID", false)]
        public static DataSet ListBCMsOfTypeID(CMListPersist persister, Guid typeId)
        {
            return ListBMCsOfTypeID(persister, typeId);
        }

        public static DataSet ListBMCsOfTypeID(CMListPersist persister, Guid typeId)
        {
            DSBMCInfo dSBMCInfo = (DSBMCInfo)persister.FindByTypeId(typeId);
            return dSBMCInfo;
        }


        [Obsolete("ListBCMsOfTypeID is deprecated; use ListBMCsOfTypeID", false)]
        public static DataSet ListBCMsOfTypeName(CMListPersist persister, string typeName)
        {
            return ListBMCsOfTypeName(persister, typeName);
        }

        public static DataSet ListBMCsOfTypeName(CMListPersist persister, string typeName)
        {
            DSBMCInfo dSBMCInfo = (DSBMCInfo)persister.FindByTypeName(typeName);
            return dSBMCInfo;
        }

        [Obsolete("ListBCMsOfName is deprecated; use ListBMCsOfName", false)]
        public static DataSet ListBCMsOfName(CMListPersist persister, string name)
        {
            return ListBMCsOfName(persister, name);
        }

        public static DataSet ListBMCsOfName(CMListPersist persister, string name)
        {
            DSBMCInfo dSBMCInfo = (DSBMCInfo)persister.FindByName(name);
            return dSBMCInfo;
        }

        public static DataSet ListBMCsOfName(CMListPersist persister, string name, string strType)
        {
            DSBMCInfo dSBMCInfo = (DSBMCInfo)persister.FindByName(name, strType);
            return dSBMCInfo;
        }

        [Obsolete("ListAllBCMs is deprecated; use ListAllBMCs", false)]
        public static DataSet ListAllBCMs(CMListPersist persister)
        {
            return ListAllBMCs(persister);
        }

        public static DataSet ListAllBMCs(CMListPersist persister)
        {
            DSBMCInfo dSBMCInfo = (DSBMCInfo)persister.FindAll();
            return dSBMCInfo;
        }


        //--------------------------
        // BMC Detail List Functions

        [Obsolete("ListBCMDetailsOfTypeName is deprecated; use ListBMCDetailsOfTypeName", false)]
        public static DataSet ListBCMDetailsOfTypeName(string typeName, object[] parameters, ITransaction transaction)
        {
            return ListBMCDetailsOfTypeName(typeName, parameters, transaction);
        }

        public static DataSet ListBMCDetailsOfTypeName(string typeName, object[] parameters, ITransaction transaction)
        {
            DataSet bmcDetail = null;

            using (CalculationTypePersist typePersister = new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction))
            {
                if (transaction.SQLTransaction != null)
                    typePersister.Transaction = transaction.SQLTransaction;
                DataRowCollection typeRows = (DataRowCollection)ListCMTypesByTypeName(typePersister, typeName);

                if (typeRows.Count == 1)
                {
                    string persistAssyName = typeRows[0][DSCalculationModuleType.CMPASSEMBLY_FIELD].ToString();
                    string persistTypeName = typeRows[0][DSCalculationModuleType.CMPCLASS_FIELD].ToString();

                    Assembly persisterAssy = System.Reflection.Assembly.Load(persistAssyName);
                    //Type persisterType=persisterAssy.GetType(persistTypeName);
                    Type persisterType = GetPersisterType(persisterAssy, persistTypeName);

                    object[] args = { transaction.SQLConnection, transaction.SQLTransaction };
                    ICPersister iPersister = (ICPersister)Activator.CreateInstance(persisterType, args);

                    Guid typeId = (Guid)typeRows[0][DSCalculationModuleType.CMTYPEID_FIELD];
                    bmcDetail = iPersister.FindDetailsByTypeId(typeId, parameters);
                }

                return bmcDetail;
            }
        }

        //---------------------
        // CM Listing functions
        public static DSLogicalModulesInfo ListLCMsByParent(LCMListPersist persister, Guid parentCLID)
        {
            DSLogicalModulesInfo dSLogicalModulesInfo = (DSLogicalModulesInfo)persister.FindByParentId(parentCLID);
            return dSLogicalModulesInfo;
        }

        public static DSLogicalModulesInfo ListLCMsByParent(LCMListPersist persister, Guid parentCLID, Guid parentCSID)
        {
            DSLogicalModulesInfo dSLogicalModulesInfo = (DSLogicalModulesInfo)persister.FindByParentId(parentCLID, parentCSID);
            return dSLogicalModulesInfo;
        }

        public static DataSet ListConsolidationParents(CMListPersist persister, Guid memberCLID)
        {
            DSBMCInfo dSBMCInfo = (DSBMCInfo)persister.FindByMember(memberCLID);
            return dSBMCInfo;
        }

        public static DataSet ListConsolidationMembers(CMListPersist persister, Guid parentCLID, Guid parentCSID)
        {
            DSBMCInfo dSBMCInfo = (DSBMCInfo)persister.FindByConsolidationParent(parentCLID, parentCSID);
            return dSBMCInfo;
        }

        public static DataSet ListConsolidationParents(CMListPersist persister, Guid memberCLID, Guid memberCSID)
        {
            DSBMCInfo dSBMCInfo = (DSBMCInfo)persister.FindByMember(memberCLID, memberCSID);
            return dSBMCInfo;
        }

        public static Guid GetCLID(LCMListPersist persister, Guid cIID)
        {
            DSLogicalModulesInfo dSLogicalModulesInfo = (DSLogicalModulesInfo)persister.FindLCMsByCIID(cIID);
            if (dSLogicalModulesInfo.Tables[DSLogicalModulesInfo.LCMLIST_TABLE].Rows.Count > 0)
                return (Guid)dSLogicalModulesInfo.Tables[DSLogicalModulesInfo.LCMLIST_TABLE].Rows[0][DSLogicalModulesInfo.LCM_CLID_FIELD];
            else
                return Guid.Empty;
        }

        private static Type GetPersisterType(Assembly persisterAssy, string typeName)
        {
            Type persisterType = null;

            if (typeName == "Oritax.TaxSimp.CommonPersistence.BlobPersister")
            {
                if (VelocityConfiguration.IsBlobPersistTableCached)
                    persisterType = persisterAssy.GetType("Oritax.TaxSimp.CommonPersistence.BlobPersisterVelocity");
                else
                    persisterType = persisterAssy.GetType(typeName);
            }
            else if (typeName == "Oritax.TaxSimp.Calculation.LogicalCMPersister")
            {
                if (VelocityConfiguration.IsLogicalCMPersisterTableCached)
                    persisterType = persisterAssy.GetType("Oritax.TaxSimp.Calculation.LogicalCMPersisterVelocity");
                else
                    persisterType = persisterAssy.GetType(typeName);
            }
            else
            {
                persisterType = persisterAssy.GetType(typeName);
            }

            return persisterType;
        }

        //=============================================================================================================
        // CM Load Functions

        public static IBrokerManagedComponent Load(Guid instanceId, ITransaction transaction, ref DataSet persistedPrimaryDS)
        {
            IBrokerManagedComponent iBMC = null;

            using (CMListPersist cMListPersist = new CMListPersist(transaction.SQLConnection))
            {
                if (null != transaction.SQLTransaction)
                    cMListPersist.Transaction = transaction.SQLTransaction;

                DSCalculationModulesInfo dsCalculationModule = (DSCalculationModulesInfo)cMListPersist.FindByInstanceID(instanceId);

                if (dsCalculationModule.Tables[DSCalculationModulesInfo.CMLIST_TABLE].Rows.Count == 1)
                {
                    Assembly persisterAssy = System.Reflection.Assembly.Load(dsCalculationModule.Tables[DSCalculationModulesInfo.CMLIST_TABLE].Rows[0][DSCalculationModulesInfo.CMPERSISTERASSY_FIELD].ToString());

                    Type persisterType = GetPersisterType(persisterAssy, dsCalculationModule.Tables[DSCalculationModulesInfo.CMLIST_TABLE].Rows[0][DSCalculationModulesInfo.CMPERSISTERCLASS_FIELD].ToString());

                    object[] args = { transaction.SQLConnection, transaction.SQLTransaction };
                    ICPersister iPersister = (ICPersister)Activator.CreateInstance(persisterType, args);

                    iPersister.Broker = transaction.Broker;

                    Guid typeId = (Guid)dsCalculationModule.Tables[DSCalculationModulesInfo.CMLIST_TABLE].Rows[0][DSCalculationModulesInfo.CMTYPEID_FIELD];

                    iBMC = iPersister.Get(instanceId, ref persistedPrimaryDS);

                    if (persistedPrimaryDS != null)
                        persistedPrimaryDS.AcceptChanges();
                }

                return iBMC;
            }
        }

        //=============================================================================================================
        // CM Save Functions

        public static void Save(IBrokerManagedComponent iBMC, ITransaction transaction, ref ArrayList invalidDependencies, ref DataSet persistedPrimaryDS)
        {
            if (iBMC.TransientInstance)
                throw new InvalidOperationException("CMPersistenceController.Save, component instance is marked as transient, it cannot be saved");

            // Get the type info for the BMC

            using (CalculationTypePersist persister = new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction))
            {
                if (transaction.SQLTransaction != null)
                    persister.Transaction = transaction.SQLTransaction;
                DataRowCollection typeRows = (DataRowCollection)ListCMTypesByTypeID(persister, iBMC.TypeID);

                string persistAssyName = typeRows[0][DSCalculationModuleType.CMPASSEMBLY_FIELD].ToString();
                string persistTypeName = typeRows[0][DSCalculationModuleType.CMPCLASS_FIELD].ToString();

                Assembly persisterAssy = System.Reflection.Assembly.Load(persistAssyName);

                Type persisterType = GetPersisterType(persisterAssy, persistTypeName);

                object[] args = { transaction.SQLConnection, transaction.SQLTransaction };
                ICPersister iPersister = (ICPersister)Activator.CreateInstance(persisterType, args);

                iPersister.Broker = transaction.Broker;

                // FindDependentBMCs is called before and after the save.  This is because the dependencies
                // may have changed as a result of the save, so both those dependent before and after the
                // save are invalidated in the cache.
                iPersister.FindDependentBMCs(iBMC.CID, ref invalidDependencies, PersisterActionType.Save);

                if (VelocityConfiguration.IsVelocityTransactionConfigured   &&
                    (   persisterType.FullName == "Oritax.TaxSimp.CommonPersistence.BlobPersisterVelocity" ||
                        persisterType.FullName == "Oritax.TaxSimp.Calculation.LogicalCMPersisterVelocity"
                    )
                    )
                {
                    ((ICPersisterVelocity)iPersister).Save(iBMC, ref persistedPrimaryDS, (ITransactionVelocity)transaction);
                }
                else
                {
                    iPersister.Save(iBMC, ref persistedPrimaryDS);
                }

                iPersister.FindDependentBMCs(iBMC.CID, ref invalidDependencies, PersisterActionType.Save);
            }
        }

        //==============================================================================================================
        // CM Creation Functions

        public static IBrokerManagedComponent Create(Guid instanceID, IComponentVersion componentVersion, string instanceName, bool transientInstance)
        {
            IAssembly implementationAssembly = componentVersion as IAssembly;
            IComponent component = componentVersion.ContainingComponentDictionary.GetComponent(componentVersion.ComponentID);

            Assembly implementationAssy = System.Reflection.Assembly.Load(implementationAssembly.StrongName);

            Type implementationType = implementationAssy.GetType(componentVersion.ImplementationClass);
            IBrokerManagedComponent iBMC = (IBrokerManagedComponent)Activator.CreateInstance(implementationType);

            // mark as transient if required
            FieldInfo transientInfoFieldInfo = typeof(BrokerManagedComponent).GetField("transientInstance", BindingFlags.NonPublic | BindingFlags.Instance);
            transientInfoFieldInfo.SetValue(iBMC, transientInstance);

            iBMC.CID = instanceID;
            iBMC.TypeID = implementationAssembly.ID;
            iBMC.TypeName = componentVersion.Name;
            iBMC.CategoryName = component.Category;

            iBMC.Initialize(instanceName);

            // need to reset these in case the initialize method has changed them
            iBMC.CID = instanceID;
            iBMC.TypeID = implementationAssembly.ID;
            iBMC.TypeName = componentVersion.Name;
            iBMC.CategoryName = component.Category;

            return iBMC;
        }

        public static IBrokerManagedComponent Create(CalculationTypePersist persister, Guid typeId, bool transientInstance)
        {
            DSCalculationModuleType dSCalculationModuleType = (DSCalculationModuleType)persister.FindByTypeId(typeId);
            return CreateCM(dSCalculationModuleType, transientInstance);
        }

        public static IBrokerManagedComponent Create(CalculationTypePersist persister, string typeName, bool transientInstance)
        {
            DSCalculationModuleType dSCalculationModuleType = (DSCalculationModuleType)persister.FindByTypeName(typeName);
            return CreateCM(dSCalculationModuleType, transientInstance);
        }

        public static IBrokerManagedComponent CreateCM(DSCalculationModuleType typeInfo, bool transientInstance)
        {
            if (1 == typeInfo.Tables[DSCalculationModuleType.TYPE_TABLE].Rows.Count)
            {
                Assembly cmAssy = System.Reflection.Assembly.Load(typeInfo.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0][DSCalculationModuleType.CMASSEMBLY_FIELD].ToString());
                Type cmType = cmAssy.GetType(typeInfo.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0][DSCalculationModuleType.CMTYPE_FIELD].ToString());
                IBrokerManagedComponent iCM = (IBrokerManagedComponent)Activator.CreateInstance(cmType);
                iCM.TypeID = (Guid)typeInfo.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0][DSCalculationModuleType.CMTYPEID_FIELD];
                iCM.TypeName = typeInfo.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0][DSCalculationModuleType.CMTYPENAME_FIELD].ToString().Trim();
                iCM.CategoryName = typeInfo.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0][DSCalculationModuleType.CMCATEGORYNAME_FIELD].ToString().Trim();

                // mark as transient if required
                FieldInfo transientInfoFieldInfo = typeof(BrokerManagedComponent).GetField("transientInstance", BindingFlags.NonPublic | BindingFlags.Instance);
                transientInfoFieldInfo.SetValue(iCM, transientInstance);

                return iCM;
            }
            return null;
        }

        //==============================================================================================================
        // Type Management Functions

        public static ICMType CreateCMType(CalculationTypePersist persister, CMTypeInfo cMTypeDetails, ITransaction transaction)
        {
            persister.EstablishDatabase();	//WSI Build the CMTYPES table if needed.
            DSCalculationModuleType dSCalculationModuleType = (DSCalculationModuleType)persister.FindByTypeId(cMTypeDetails.ID);

            // SJD 5/2/2002 Added pAssembly and pClass 
            if (dSCalculationModuleType.Tables[DSCalculationModuleType.TYPE_TABLE].Rows.Count == 0)
            {
                dSCalculationModuleType = new DSCalculationModuleType(
                    cMTypeDetails.ID,
                    cMTypeDetails.TypeName,
                    cMTypeDetails.DisplayName,
                    cMTypeDetails.Category,
                    cMTypeDetails.Applicability,
                    cMTypeDetails.AssemblyName,
                    cMTypeDetails.ClassName,
                    cMTypeDetails.majorVersion,
                    cMTypeDetails.minorVersion,
                    cMTypeDetails.release,
                    cMTypeDetails.pAssembly,
                    cMTypeDetails.pClass);
            }
            else
            {
                DataRow row = dSCalculationModuleType.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0];

                row[DSCalculationModuleType.CMTYPENAME_FIELD] = cMTypeDetails.TypeName;
                row[DSCalculationModuleType.CMTYPEDISPLAYNAME_FIELD] = cMTypeDetails.DisplayName;
                row[DSCalculationModuleType.CMCATEGORYNAME_FIELD] = cMTypeDetails.Category;
                row[DSCalculationModuleType.CMAPPLICABILITY_FIELD] = cMTypeDetails.Applicability;
                row[DSCalculationModuleType.CMASSEMBLY_FIELD] = cMTypeDetails.AssemblyName;
                row[DSCalculationModuleType.CMTYPE_FIELD] = cMTypeDetails.ClassName;
                row[DSCalculationModuleType.CMMAJORVERSION_FIELD] = cMTypeDetails.majorVersion;
                row[DSCalculationModuleType.CMMINORVERSION_FIELD] = cMTypeDetails.minorVersion;
                row[DSCalculationModuleType.CMRELEASE_FIELD] = cMTypeDetails.release;
                row[DSCalculationModuleType.CMPASSEMBLY_FIELD] = cMTypeDetails.pAssembly;
                row[DSCalculationModuleType.CMPCLASS_FIELD] = cMTypeDetails.pClass;
            }

            persister.Update(dSCalculationModuleType);

            if (cMTypeDetails.pAssembly != null)
            {
                // Create an instance of the persister class
                Assembly persisterAssy = System.Reflection.Assembly.Load(cMTypeDetails.pAssembly);
                //Type persisterType=persisterAssy.GetType(cMTypeDetails.pClass);
                Type persisterType = GetPersisterType(persisterAssy, cMTypeDetails.pClass);
                object[] args = { transaction.SQLConnection, transaction.SQLTransaction };
                ICPersister iPersister = (ICPersister)Activator.CreateInstance(persisterType, args);

                // Establish the Database for the persister of the new Type.
                iPersister.EstablishDatabase();
                iPersister.EstablishDatabase(cMTypeDetails);
            }

            return (ICMType)null;
        }

        public static IEnumerable ListAllCMTypes(CalculationTypePersist persister)
        {
            DSCalculationModuleType dSCalculationModulesInfo = (DSCalculationModuleType)persister.FindAll();
            return dSCalculationModulesInfo.Tables[DSCalculationModuleType.TYPE_TABLE].Rows;
        }

        public static IEnumerable ListCMTypes(CalculationTypePersist persister, string categoryName)
        {
            DSCalculationModuleType dSCalculationModulesInfo = (DSCalculationModuleType)persister.FindByCategory(categoryName);
            return dSCalculationModulesInfo.Tables[DSCalculationModuleType.TYPE_TABLE].Rows;
        }

        public static IEnumerable ListCMTypesByTypeID(CalculationTypePersist persister, Guid typeID)
        {
            DSCalculationModuleType dSCalculationModulesInfo = (DSCalculationModuleType)persister.FindByTypeId(typeID);
            return dSCalculationModulesInfo.Tables[DSCalculationModuleType.TYPE_TABLE].Rows;
        }

        public static IEnumerable ListCMTypesByTypeName(CalculationTypePersist persister, string typeName)
        {
            DSCalculationModuleType dSCalculationModulesInfo = (DSCalculationModuleType)persister.FindByTypeName(typeName);
            return dSCalculationModulesInfo.Tables[DSCalculationModuleType.TYPE_TABLE].Rows;
        }

        public static IEnumerable ListCMTypes(CalculationTypePersist persister, string[] categoryNames)
        {
            DSCalculationModuleType dSCalculationModulesInfo = (DSCalculationModuleType)persister.FindByCategory(categoryNames);
            return dSCalculationModulesInfo.Tables[DSCalculationModuleType.TYPE_TABLE].Rows;
        }

        public static void DeleteAllTypes(CalculationTypePersist calculationTypePersist)
        {
            calculationTypePersist.DeleteAll();
        }

        public static ICalculationModule CloneCMInstance(ICalculationModule iCM)
        {
            try
            {
                MemoryStream memoryStream = new MemoryStream(1000);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(memoryStream, iCM);

                MemoryStream memoryStream1 = new MemoryStream(memoryStream.ToArray());

                BinaryFormatter binaryFormatter1 = new BinaryFormatter();
                ICalculationModule newCM = (ICalculationModule)binaryFormatter1.Deserialize(memoryStream1);

                newCM.CIID = Guid.NewGuid();
                return newCM;
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                throw;
            }
        }

        //==============================================================================================================
        // CM Deletion Functions

        //public static void Delete(ICalculationModulePersist persister, Guid cmID)
        //{
        //    DSCalculationModule dsCalculationModule = (DSCalculationModule)persister.FindByInstanceId(cmID);

        //    if (dsCalculationModule.Tables[DSCalculationModule.INSTANCE_TABLE].Rows.Count != 0)
        //    {
        //        DataRow row = dsCalculationModule.Tables[DSCalculationModule.INSTANCE_TABLE].Rows[0];
        //        (new CMTypesIdentifier()).VerifyDeletionByComponentType( (Guid)row[DSCalculationModule.TYPE_FIELD]);
        //        row.Delete();
        //        persister.Update(dsCalculationModule);
        //    }
        //}

        public static void Delete(ICMBroker broker, IBrokerManagedComponent bmc, ITransaction transaction, ref ArrayList invalidDependencies)
        {
            (new CMTypesIdentifier()).VerifyDeletionByType(broker, bmc.TypeID);
            //(new CMTypesIdentifier()).VerifyDeletionByType(broker, bmc.ComponentInformation.ID);

            // Get the complete dataset for the component
            using (CMListPersist cMListPersist = new CMListPersist(transaction.SQLConnection))
            {
                if (transaction.SQLTransaction != null)
                    cMListPersist.Transaction = transaction.SQLTransaction;
                DSCalculationModulesInfo dsCalculationModuleInfo = (DSCalculationModulesInfo)cMListPersist.FindByInstanceID(bmc.CID);

                if (dsCalculationModuleInfo.Tables[DSCalculationModulesInfo.CMLIST_TABLE].Rows.Count != 0)
                {
                    string persistAssyName = dsCalculationModuleInfo.Tables[DSCalculationModulesInfo.CMLIST_TABLE].Rows[0][DSCalculationModulesInfo.CMPERSISTERASSY_FIELD].ToString();

                    Assembly persisterAssy = System.Reflection.Assembly.Load(persistAssyName);

                    Type persisterType = GetPersisterType(persisterAssy, dsCalculationModuleInfo.Tables[DSCalculationModulesInfo.CMLIST_TABLE].Rows[0][DSCalculationModulesInfo.CMPERSISTERCLASS_FIELD].ToString());

                    object[] args = { transaction.SQLConnection, transaction.SQLTransaction };
                    ICPersister iPersister = (ICPersister)Activator.CreateInstance(persisterType, args);

                    iPersister.Broker = broker;

                    iPersister.FindDependentBMCs(bmc.CID, ref invalidDependencies, PersisterActionType.Delete);

                    iPersister.Delete(bmc);
                }
            }
        }

        //public static void DeleteAll(ICalculationModulePersist persister, ITransaction transaction)
        //{
        //    using (CalculationTypePersist calculationTypePersist = new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction))
        //    {
        //        if (transaction.SQLTransaction != null)
        //            calculationTypePersist.Transaction = transaction.SQLTransaction;

        //        DataRowCollection typeRowSet = (DataRowCollection)ListAllCMTypes(calculationTypePersist);
        //        foreach (DataRow typeRow in typeRowSet)
        //        {
        //            String persisterAssyName = String.Empty;
        //            String persisterClassName = String.Empty;

        //            if (typeRow[DSCalculationModuleType.CMPASSEMBLY_FIELD] != System.DBNull.Value)
        //            {
        //                persisterAssyName = (string)typeRow[DSCalculationModuleType.CMPASSEMBLY_FIELD];
        //            }

        //            if (typeRow[DSCalculationModuleType.CMPCLASS_FIELD] != System.DBNull.Value)
        //            {
        //                persisterClassName = (string)typeRow[DSCalculationModuleType.CMPCLASS_FIELD];
        //            }

        //            if (persisterAssyName != String.Empty && persisterClassName != String.Empty)
        //            {
        //                Assembly persisterAssy = System.Reflection.Assembly.Load(persisterAssyName);
        //                //Type persisterType=persisterAssy.GetType(persisterClassName);
        //                Type persisterType = GetPersisterType(persisterAssy, persisterClassName);
        //                object[] args = { transaction.SQLConnection, transaction.SQLTransaction };
        //                ICPersister iPersister = (ICPersister)Activator.CreateInstance(persisterType, args);

        //                iPersister.ClearDatabase();
        //            }
        //        }
        //    }

        //}
        //==============================================================================================================
        // CM Qualifier Functions

        public static IBrokerManagedComponent GetQualifiedList(Guid typeId, object qualifiers, ITransaction transaction)
        {
            IBrokerManagedComponent iBMC = null;
            using (CalculationTypePersist persister = new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction))
            {
                if (transaction.SQLTransaction != null)
                    persister.Transaction = transaction.SQLTransaction;

                DSCalculationModuleType dsCalculationModule = (DSCalculationModuleType)persister.FindByTypeId(typeId);

                if (1 == dsCalculationModule.Tables[DSCalculationModuleType.TYPE_TABLE].Rows.Count)
                {
                    Assembly persisterAssy = System.Reflection.Assembly.Load(dsCalculationModule.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0][DSCalculationModuleType.CMPASSEMBLY_FIELD].ToString());
                    //Type persisterType=persisterAssy.GetType(dsCalculationModule.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0][DSCalculationModuleType.CMPCLASS_FIELD].ToString());
                    Type persisterType = GetPersisterType(persisterAssy, dsCalculationModule.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0][DSCalculationModuleType.CMPCLASS_FIELD].ToString());

                    object[] args = { transaction.SQLConnection, transaction.SQLTransaction };
                    ICPersister iPersister = (ICPersister)Activator.CreateInstance(persisterType, args);

                    iBMC = iPersister.GetQualifiedList(qualifiers);
                }
                return iBMC;
            }
        }

        public static DataTable ListBMCCollection(CollectionSpecifier bMCCollectionSpecifier, ITransaction transaction)
        {
            //ITransaction transaction=broker.Transaction;
            IComponentManagement componentDictionary = transaction.Broker.GetComponentDictionary();

            IComponentVersion componentVersion = componentDictionary[bMCCollectionSpecifier.TypeID];

            Assembly persisterAssy = System.Reflection.Assembly.Load(componentVersion.PersisterAssemblyStrongName);
            //Type persisterType=persisterAssy.GetType(componentVersion.PersisterClass);
            Type persisterType = GetPersisterType(persisterAssy, componentVersion.PersisterClass);

            object[] args = { transaction.SQLConnection, transaction.SQLTransaction };
            ICPersister iPersister = (ICPersister)Activator.CreateInstance(persisterType, args);

            DataTable collectionTable = iPersister.ListBMCCollection(bMCCollectionSpecifier);

            return collectionTable;
        }

        //public static void DeleteByQualifier(Guid typeId, object qualifiers, ITransaction transaction)
        //{
        //    using (CalculationTypePersist persister = new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction))
        //    {
        //        if (transaction.SQLTransaction != null)
        //            persister.Transaction = transaction.SQLTransaction;

        //        DSCalculationModuleType dsCalculationModule = (DSCalculationModuleType)persister.FindByTypeId(typeId);

        //        if (1 == dsCalculationModule.Tables[DSCalculationModuleType.TYPE_TABLE].Rows.Count)
        //        {
        //            Assembly persisterAssy = System.Reflection.Assembly.Load(dsCalculationModule.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0][DSCalculationModuleType.CMPASSEMBLY_FIELD].ToString());
        //            //Type persisterType=persisterAssy.GetType(dsCalculationModule.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0][DSCalculationModuleType.CMPCLASS_FIELD].ToString());
        //            Type persisterType = GetPersisterType(persisterAssy, dsCalculationModule.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0][DSCalculationModuleType.CMPCLASS_FIELD].ToString());

        //            object[] args = { transaction.SQLConnection, transaction.SQLTransaction };
        //            ICPersister iPersister = (ICPersister)Activator.CreateInstance(persisterType, args);

        //            iPersister.DeleteByQualifier(qualifiers);
        //        }
        //    }
        //}
   

}
    public class CMTypesIdentifier
    {
        private List<Guid> clientComponentTypes = new List<Guid>();
        public CMTypesIdentifier()
        {
            clientComponentTypes.Add(new Guid("E9445EE9-EAA9-48C3-AFD0-9F289CF0C9C5"));
            clientComponentTypes.Add(new Guid("253BBAB4-3243-4B79-A55B-50667BB148A1"));
            clientComponentTypes.Add(new Guid("EE2B9DD2-6B31-442E-ABD9-A1DA168770D1"));
            clientComponentTypes.Add(new Guid("CC2A8EE2-F93F-4C4B-9662-8468EFDD8EE4"));
            clientComponentTypes.Add(new Guid("E705E1E7-8B32-4241-B685-C610C56F7AA7"));
            clientComponentTypes.Add(new Guid("39759DC8-74CC-4559-9B00-FCA6E3D1E5B9"));
            clientComponentTypes.Add(new Guid("9F901897-040D-4F78-9825-683D8E974586"));
            clientComponentTypes.Add(new Guid("9CE651E3-1AED-4FD5-BD25-1F8C36A7B0B8"));
            clientComponentTypes.Add(new Guid("4CD0650A-4B8E-4547-ABAF-D410814AB98E"));
            clientComponentTypes.Add(new Guid("D5E72B84-EB01-4060-8746-80ADF576D750"));
            clientComponentTypes.Add(new Guid("88CFDC1D-12D9-4DBA-9287-815C94EDCD08"));
            clientComponentTypes.Add(new Guid("46581D50-6BF9-4AAF-B980-21E03CD76317"));
            clientComponentTypes.Add(new Guid("750819E8-3B01-4EB7-B894-E5CD0615482E"));
            clientComponentTypes.Add(new Guid("A6818F74-D0BF-4196-9F3E-964F003BC2EC"));       
        }
        public bool IsClient(Guid id)
        {
            return clientComponentTypes.Contains(id);
        }

        /// <summary>
        /// Some client information got deleted from the database
        /// THe purpoce of this method is to identify where adeletion was invoked from
        /// </summary>
        /// <param name="typeId"></param>
        public void VerifyDeletionByType(ICMBroker broker, Guid typeId)
        {
            VerifyDeletionByComponentType(typeId, broker.UserContext.Identity.Name);  //check whether component type was pased
            IComponentVersion compVersion = ((CMBroker)broker).GetComponentVersion(typeId); //if component version id was passed
            if (compVersion != null)
                VerifyDeletionByComponentType(compVersion.ComponentID, broker.UserContext.Identity.Name);
        }
        public void VerifyDeletionByComponentType(Guid typeId, string userName)
        {
            if ((new CMTypesIdentifier()).IsClient(typeId) && userName.ToLower() != "administrator")
            {
                throw new Exception("Deletion of this client type is temporarily disabled. Component Type " + typeId.ToString());
            }
        }
    }
}
