using System;
using System.Collections.Generic;
using System.Text;

namespace Oritax.TaxSimp.Services
{
    /// <summary>
    /// Represents a user logged into the server
    /// </summary>
    public class UserAuthenticationToken
    {
        private string userName;
        private DateTime sessionLastRefreshed;

        /// <summary>
        /// Constructs a new UserAuthenticationToken
        /// </summary>
        /// <param name="userName">The username to create a token for</param>
        /// <exception cref="System.ArgumentNullException">userName cannot be null or an empty string</exception>
        public UserAuthenticationToken(string userName)
        {
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException("userName");

            this.userName = userName;
            sessionLastRefreshed = DateTime.Now;
        }

        /// <summary>
        /// The username of the user logged in
        /// </summary>
        public string UserName
        {
            get
            {
                return userName;
            }
        }

        /// <summary>
        /// Indicates the last time the user 
        /// was connected to the server
        /// </summary>
        public DateTime SessionLastRefreshed
        {
            get
            {
                lock (this)
                {
                    return sessionLastRefreshed;
                }
            }
        }

        /// <summary>
        /// Indicates that the user has connected to the server
        /// </summary>
        public void RefreshSessionTime()
        {
            lock (this)
            {
                this.sessionLastRefreshed = DateTime.Now;
            }
        }

        /// <summary>
        /// Checks if the user should still be logged in
        /// </summary>
        /// <returns>True if the user has not timed out, false if not</returns>
        public bool IsUserStillLoggedIn()
        {
            DateTime logOutTime = this.sessionLastRefreshed.AddMinutes(UserAuthentication.AuthenticationTimeout);
            return (logOutTime > DateTime.Now);
        }
    }
}
