using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Oritax.TaxSimp.Services
{
    /// <summary>
    /// Represents user authentication actions
    /// </summary>
    public static class UserAuthentication
    {
        public static readonly int AuthenticationTimeout = int.Parse(ConfigurationManager.AppSettings["AuthenticationTimeout"]);
        private static Dictionary<string, UserAuthenticationToken> cache = new Dictionary<string, UserAuthenticationToken>();

        /// <summary>
        /// Log the user into the server
        /// </summary>
        /// <param name="userName">The user name of the user to login</param>
        /// <exception cref="System.ArgumentNullException">userName cannot be null or an empty string</exception>
        public static void LogUserIn(string userName)
        {
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException("userName");

            UserAuthenticationToken token = GetUserToken(userName, true);
            UserAuthentication.RefreshSessionTime(token);
        }

        /// <summary>
        /// Logs a user out from the server
        /// </summary>
        /// <param name="userName">The user name of the user to log out</param>
        /// <exception cref="System.ArgumentNullException">userName cannot be null or an empty string</exception>
        public static void LogUserOut(string userName)
        {
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException("userName");

            RemoveUserToken(userName);
        }

        /// <summary>
        /// Determines if a user is currently logging in to the server
        /// </summary>
        /// <param name="userName">The user to check if they are logged in</param>
        /// <returns>True if the user is logged in false otherwise</returns>
        /// <exception cref="System.ArgumentNullException">userName cannot be null or an empty string</exception>
        public static bool IsUserLoggedIn(string userName)
        {
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException("userName");

            UserAuthenticationToken token = GetUserToken(userName, false);

            if (token != null)
            {
                bool loggedIn = token.IsUserStillLoggedIn();

                if (!loggedIn)
                    RemoveUserToken(userName);

                return loggedIn;
            }
            else
                return false;
        }

        /// <summary>
        /// Gets a list of the users currently logged into the server
        /// </summary>
        /// <returns>A list of the currently logged in users</returns>
        public static string[] GetLoggedInUsers()
        {
            List<string> loggedInUsers = new List<string>();

            foreach (UserAuthenticationToken token in cache.Values)
            {
                if (token.IsUserStillLoggedIn())
                    loggedInUsers.Add(token.UserName);
            }

            return loggedInUsers.ToArray();
        }

        /// <summary>
        /// Logs out all users from the server
        /// </summary>
        /// <param name="exemptAdministrator">Indicates if the administrator account should be exempted from the logout</param>
        public static void LogOutAllUsers(bool exemptAdministrator)
        {
            List<UserAuthenticationToken> tokens = new List<UserAuthenticationToken>(cache.Values);
            foreach (UserAuthenticationToken token in tokens)
            {
                if (!exemptAdministrator || (token.UserName.ToLower() != "administrator") )
                    LogUserOut(token.UserName);
            }
        }

        /// <summary>
        /// Refreshes the user when they conect to the application to refresh
        /// their timeout so that they aren't automatically logged out
        /// </summary>
        /// <param name="userName">The user name of the user being refreshed</param>
        /// <exception cref="System.ArgumentNullException">userName cannot be null or an empty string</exception>
        public static void UserRefreshedSession(string userName)
        {
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException("userName");

            UserAuthenticationToken token = GetUserToken(userName, true);
            token.RefreshSessionTime();
        }

        /// <summary>
        /// Refreshes the session time for the user
        /// </summary>
        /// <param name="token">The user token</param>
        /// <exception cref="System.ArgumentNullException">token cannot be null</exception>
        private static void RefreshSessionTime(UserAuthenticationToken token)
        {
            if (token == null)
                throw new ArgumentNullException("token");

            token.RefreshSessionTime();
        }

        /// <summary>
        /// Gets the user token or creates it if it doesn't exist and
        /// creation is specified
        /// </summary>
        /// <param name="userName">The user name of the user to get</param>
        /// <param name="createTokenIfNotPresent">If the token isn't present then create it</param>
        /// <returns>The user token requested, null if it the user doesn't exist and creation isn't specified</returns>
        /// <exception cref="System.ArgumentNullException">userName cannot be null or an empty string</exception>
        private static UserAuthenticationToken GetUserToken(string userName, bool createTokenIfNotPresent)
        {
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException("userName");

            lock (cache)
            {
                if (cache.ContainsKey(userName))
                {
                    return cache[userName];
                }
                else if (createTokenIfNotPresent)
                {
                    UserAuthenticationToken token = new UserAuthenticationToken(userName);
                    cache.Add(userName, token);
                    return token;
                }
                else
                    return null;
            }
        }

        /// <summary>
        /// Removes a user token from the cache
        /// </summary>
        /// <param name="userName">The username to remove</param>
        /// <exception cref="System.ArgumentNullException">userName cannot be null or an empty string</exception>
        private static void RemoveUserToken(string userName)
        {
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException("userName");

            lock (cache)
            {
                if (cache.ContainsKey(userName))
                    cache.Remove(userName);
            }
        }
    }
}
