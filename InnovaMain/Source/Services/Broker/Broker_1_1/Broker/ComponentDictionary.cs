using System;
using System.Collections;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.BusinessStructureUtilities;

namespace Oritax.TaxSimp.Services.CMBroker
{
	/// <summary>
	/// Summary description for ComponentDictionary.
	/// </summary>
	[Serializable]
	public class ComponentDictionary : AssemblyDictionary, IComponentManagement
	{
		#region INTERNAL CLASSES

		[Serializable]
		public class ComponentDictionaryException : Exception
		{
			public ComponentDictionaryException(string message)
				: base(message){}
		}

		[Serializable]
		public class ComponentVersion : Assembly, IComponentVersion
		{
			private string versionName;
			private Guid componentID;
			private DateTime startDate;
			private DateTime endDate;
			private string persisterAssemblyStrongName;
			private string persisterClass;
			private string implementationClass;
			private IComponentManagement containingComponentDictionary;
			private bool obsolete;

			public int MajorVersion{get{return base.VersionNumber.MajorVersion;}}
			public int MinorVersion{get{return base.VersionNumber.MinorVersion;}}
			public string VersionName{get{return versionName;}set{versionName=value;}}
			public Guid ComponentID{get{return componentID;}set{componentID=value;}}
			public DateTime StartDate{get{return startDate;}set{startDate=value;}}
			public DateTime EndDate{get{return endDate;}set{endDate=value;}}
			public string PersisterAssemblyStrongName{get{return persisterAssemblyStrongName;}set{persisterAssemblyStrongName=value;}}
			public string PersisterClass{get{return persisterClass;}set{persisterClass=value;}}
			public string ImplementationClass{get{return implementationClass;}set{implementationClass=value;}}
			public IComponentManagement ContainingComponentDictionary{get{return containingComponentDictionary;}set{containingComponentDictionary=value;}}
			public bool Obsolete{get{return this.obsolete;}set{this.obsolete = value;}}

			public ComponentVersion()
				: base()
			{
				this.versionName="";
				this.componentID=Guid.Empty;
				this.startDate=DateTime.Now;
				this.endDate=DateTime.Now;
				this.persisterAssemblyStrongName="";
				this.persisterClass="";
				this.implementationClass="";
				this.containingComponentDictionary=null;
				this.obsolete = false;
			}

			public ComponentVersion(
				Guid					iD,
				AssemblyVersionNumber	versionNumber,
				string					name,
				string					displayName,
				string					strongName,
				string					versionName,
				Guid					componentID,
				DateTime				startDate,
				DateTime				endDate,
				string					persisterAssemblyStrongName,
				string					persisterClass,
				string					implementationClass,
				bool					obsolete
				)
				: base(iD,versionNumber,name,displayName,strongName)
			{
				this.versionName=versionName;
				this.componentID=componentID;
				this.startDate=startDate;
				this.endDate=endDate;
				this.persisterAssemblyStrongName=persisterAssemblyStrongName;
				this.persisterClass=persisterClass;
				this.implementationClass=implementationClass;
				this.containingComponentDictionary=null;
				this.obsolete = obsolete;
			}

			public override bool Equals(object object1)
			{
				if(object1 is ComponentVersion)
				{
					ComponentVersion componentVersion=object1 as ComponentVersion;
					return base.Equals(componentVersion)
						&& versionName==componentVersion.versionName
						&& componentID==componentVersion.componentID
						&& startDate==componentVersion.startDate
						&& endDate==componentVersion.endDate
						&& persisterAssemblyStrongName==componentVersion.persisterAssemblyStrongName
						&& persisterClass==componentVersion.persisterClass
						&& implementationClass==componentVersion.implementationClass
						&& obsolete==componentVersion.obsolete;
				}
				else
					return false;
			}

			public bool IsLaterVersion(IComponentVersion componentVersion)
			{
				return this.MajorVersion>componentVersion.MajorVersion
					|| (this.MajorVersion==componentVersion.MajorVersion
						&& this.MinorVersion>componentVersion.MinorVersion);
			}
			
			public bool IsSameVersion(IComponentVersion componentVersion)
			{
				return (this.MajorVersion==componentVersion.MajorVersion
					&& this.MinorVersion==componentVersion.MinorVersion);
			}

			public bool IsEarlierVersion(IComponentVersion componentVersion)
			{
				return this.MajorVersion<componentVersion.MajorVersion
					|| (this.MajorVersion==componentVersion.MajorVersion
					&& this.MinorVersion<componentVersion.MinorVersion);
			}

			/// <summary>
			/// Determines if a component version is the next version to this component version
			/// </summary>
			/// <param name="componentVersion">The component version to check</param>
			/// <returns>True if the component version is 1 major version greater than this component version, false otherwise</returns>
			public bool IsNextVersion(IComponentVersion componentVersion)
			{
				int expectedMajorVersion = componentVersion.MajorVersion + 1;

				return (this.MajorVersion == expectedMajorVersion);
			}

			public static bool operator == (ComponentVersion componentVersion1,ComponentVersion componentVersion2)
			{
				return componentVersion1.Equals(componentVersion2);
			}

			public static bool operator != (ComponentVersion componentVersion1,ComponentVersion componentVersion2)
			{
				return !componentVersion1.Equals(componentVersion2);
			}

			public override int GetHashCode()
			{
				return base.GetHashCode();
			}
		}


		[Serializable]
		public class ComponentVersionArray : IComponentVersionArray,IEnumerable
		{
			ArrayList arrayList;

			public int Count{get{return arrayList.Count;}}

			public IComponentVersion this[int index]
			{
				get
				{
					return (ComponentVersion)arrayList[index];
				}
			}

			public void Add(IComponentVersion componentVersion)
			{
				arrayList.Add(componentVersion);
			}

			public void Replace(IComponentVersion componentVersion)
			{
				for(int i=0;i<this.Count;i++)
				{
					if(((ComponentVersion)arrayList[i]).ID==componentVersion.ID)
					{
						arrayList[i]=componentVersion;
					}
				}
			}

			public ComponentVersionArray()
			{
				arrayList=new ArrayList();
			}

			public ComponentVersionArray(ArrayList arrayList)
			{
				this.arrayList=arrayList;
			}

			#region IEnumerable Members

			public IEnumerator GetEnumerator()
			{
				return arrayList.GetEnumerator();;
			}

			#endregion
		}


		[Serializable]
		public class Component : IComponent
		{
			private Guid iD;
			private string name;
			private string displayName;
			private string category;
			private CMApplicability cMApplicability;
			private bool obsolete;

			public Guid ID{get{return iD;}set{iD=value;}}
			public string Name{get{return name;}set{name=value;}}
			public string DisplayName{get{return displayName;}set{displayName=value;}}
			public string Category{get{return category;}set{category=value;}}
			public int Applicability{get{return (int)cMApplicability;}set{cMApplicability=(CMApplicability)value;}}
			public bool Obsolete{get{return this.obsolete;}set{this.obsolete = value;}}
            
			public Component(Guid iD,string name,string displayName,string category,CMApplicability cMApplicability, bool obsolete)
			{
				this.iD=iD;
				this.name=name;
				this.displayName=displayName;
				this.category=category;
				this.cMApplicability=cMApplicability;
				this.obsolete = obsolete;
			}

			public Component()
			{
				this.iD=Guid.Empty;
				this.name="";
				this.displayName="";
				this.category="";
				this.cMApplicability=CMApplicability.All;
				this.obsolete = false;
			}
		}

		[Serializable]
		public class ComponentArray : ArrayList, IComponentArray
		{
			public new IComponent this[int index]
			{
				get
				{
					return (Component)base[index];
				}
			}

			public void Add(IComponent component)
			{
				base.Add(component);
			}

			public void Replace(IComponent component)
			{
				for (int i=0; i < this.Count; i++)
				{
					if (((IComponent)this[i]).ID == component.ID)
					{
						this.RemoveAt(i);
						break;
					}
				}

				Add(component);
			}

			#region IEnumerable Members

			public new IEnumerator GetEnumerator()
			{
				return base.GetEnumerator();
			}

			#endregion
		}


		[Serializable]
		public class ComponentIDDictionary : DictionaryBase
		{
			public IComponent this[Guid iD]
			{
				get
				{
					return (Component)Dictionary[iD];
				}
			}

			public bool Contains(Guid iD)
			{
				return Dictionary.Contains(iD);
			}

			public void Add(Guid iD,IComponent component)
			{
                if(!Dictionary.Contains(iD))
    				Dictionary.Add(iD,component);
			}

			public void Replace(Guid id,IComponent component)
			{
				Dictionary[id] = component;
			}

			public void Delete(Guid iD)
			{
				Dictionary.Remove(iD);
			}
		}


		[Serializable]
		protected class ComponentVersionIDDictionary : AssemblyIDDictionary, IComponentVersionIDDictionary
		{
			public new IComponentVersion this[Guid iD]
			{
				get
				{
					return (IComponentVersion)base[iD];
				}
			}

			public void Add(Guid iD,IComponentVersion componentVersion)
			{
				base.Add(iD,componentVersion);
			}
		}


		[Serializable]
		public class ComponentNameDictionary : DictionaryBase
		{
			[Serializable]
			public class ComponentNameDictionaryException : Exception
			{
				public ComponentNameDictionaryException(string message)
					: base(message){}
			}

			public IComponent this[string name]
			{
				get
				{
					return (Component)Dictionary[name];
				}
			}

			public bool Contains(string name)
			{
				return Dictionary.Contains(name);
			}

			public void Replace(string name, IComponent component)
			{
				Dictionary[name] = component;
			}

			public void Add(string name,IComponent component)
			{
				Dictionary.Add(name,component);
			}

			public void Delete(string name)
			{
				Dictionary.Remove(name);	
			}
		}


		[Serializable]
		public abstract class ComponentVersionsFilter
		{
			public abstract bool IsIncluded(IComponentVersion componentVersion,IComponentManagement componentDictionary);
		}


		/// <summary>
		/// Filter class that includes only components that have their categories eqqual to at least one of a set of strings.
		/// </summary>
		[Serializable]
		public class ComponentCategoryFilter : ComponentVersionsFilter
		{
			string[] permittedCategories;

			public ComponentCategoryFilter(string[] permittedCategories)
			{
				this.permittedCategories=permittedCategories;
			}

			public override bool IsIncluded(IComponentVersion componentVersion,IComponentManagement componentDictionary)
			{
				IComponent component=componentDictionary.GetComponent(componentVersion.ComponentID);
				foreach(string permittedCategory in permittedCategories)
				{
					if(component.Category==permittedCategory)
						return true;
				}

				return false;
			}
		}


		/// <summary>
		/// Filter class that includes only components that have a date range that overlaps the
		/// date range supplied.
		/// </summary>
		[Serializable]
		public class ComponentDateOverlapFilter : ComponentVersionsFilter
		{
			DateTime startDate;
			DateTime endDate;

			public ComponentDateOverlapFilter(DateTime startDate,DateTime endDate)
			{
				this.startDate=startDate;
				this.endDate=endDate;
			}

			public override bool IsIncluded(IComponentVersion componentVersion,IComponentManagement componentDictionary)
			{
                IComponent component = componentDictionary.GetComponent(componentVersion.ComponentID);

                return CalculationModuleBase.CheckComponentIsIncluded(componentVersion.StartDate, componentVersion.EndDate, this.startDate, this.endDate, componentVersion.DisplayName);
			}
		}


		[Serializable]
		public class ComponentAllVersionOrderingFilter : ComponentVersionsFilter
		{
			IComponentManagement includedComponentVersionsDictionary;

			public ComponentAllVersionOrderingFilter(IComponentManagement includedComponentVersionsDictionary)
			{
				this.includedComponentVersionsDictionary=includedComponentVersionsDictionary;
			}

			public override bool IsIncluded(IComponentVersion componentVersion,IComponentManagement componentDictionary)
			{
				if(includedComponentVersionsDictionary.ContainsComponentVersion(componentVersion.ID))
					return false;
				else	
					return true;
			}
		}

		[Serializable]
		public class ComponentVersionOrderingFilter : ComponentVersionsFilter
		{
			IComponentManagement includedComponentVersionsDictionary;

			public ComponentVersionOrderingFilter(IComponentManagement includedComponentVersionsDictionary)
			{
				this.includedComponentVersionsDictionary=includedComponentVersionsDictionary;
			}

			public override bool IsIncluded(IComponentVersion componentVersion,IComponentManagement componentDictionary)
			{
				if(includedComponentVersionsDictionary.ContainsComponentVersion(componentVersion.ID))
					return false;

				if(includedComponentVersionsDictionary.ContainsComponent(componentVersion.ComponentID))
				{
					IComponent component=includedComponentVersionsDictionary.GetComponent(componentVersion.ComponentID);
					IAssemblyVersionSet includedComponentVersions=includedComponentVersionsDictionary.GetComponentVersions(component.Name);
					foreach(ComponentVersion includedComponentVersion in includedComponentVersions)
					{
						if(!includedComponentVersion.IsEarlierVersion(componentVersion))
							return false;
					}
				}

				return true;
			}
		}


		[Serializable]
		public class FilteredComponentVersionArray : IComponentVersionArray
		{
			private ComponentVersionArray componentVersionArray;
			private ComponentVersionsFilter[] componentVersionsFilters;
			private IComponentManagement componentDictionary;

			[Serializable]
			public class FilteredComponentVersionIterator : IEnumerator 
			{
				private int index=0;
				private bool validCurrent=false;
				ComponentVersionArray componentVersionArray;
				ComponentVersionsFilter[] componentVersionsFilters;
				IComponentManagement componentDictionary;

				public FilteredComponentVersionIterator(ComponentVersionArray componentVersionArray,
					ComponentVersionsFilter[] componentVersionsFilters,
					IComponentManagement componentDictionary)
				{
					this.componentVersionArray=componentVersionArray;
					this.componentVersionsFilters=componentVersionsFilters;
					this.componentDictionary=componentDictionary;

					Reset();
				}

				public object Current
				{
					get
					{
						if(!validCurrent)
							throw new InvalidOperationException("The iterator is not pointing to a valid object");
						return componentVersionArray[index];
					}
				}

				public bool MoveNext()
				{
					validCurrent=false;
					for(index++;index<componentVersionArray.Count;index++)
					{
						validCurrent=true;

						for(int i=0;i<componentVersionsFilters.Length;i++)
						{
							if(!componentVersionsFilters[i].IsIncluded(componentVersionArray[index],componentDictionary))
							{
								validCurrent=false;
							}
						}
						if(validCurrent)
							return true;
					}

					return false;
				}

				public void Reset()
				{
					validCurrent=false;
					index=-1;
				}
			}

			public int Count
			{
				get
				{
					throw new InvalidOperationException("FilteredComponentVersionArray does not implement the Count property.");
				}
			}

			public FilteredComponentVersionArray(ComponentVersionArray componentVersionArray,ComponentVersionsFilter[] componentVersionsFilter,
				IComponentManagement componentDictionary)
			{
				this.componentVersionArray=componentVersionArray;
				this.componentVersionsFilters=componentVersionsFilter;
				this.componentDictionary=componentDictionary;
			}

			public IComponentVersion this[int index]
			{
				get
				{
					throw new Exception("Indexer on FilteredComponentVersionArray is not implemented");
				}
			}

			public void Add(IComponentVersion componentVersion)
			{
				componentVersionArray.Add(componentVersion);
			}

			#region IEnumerable Members

			public IEnumerator GetEnumerator()
			{
				// TODO:  Add FilteredComponentVersionArray.GetEnumerator implementation
				return new FilteredComponentVersionIterator(componentVersionArray,componentVersionsFilters,componentDictionary);
			}

			#endregion

		}


		#endregion INTERNAL CLASSES

		#region FIELD VARIABLES

		static ComponentDictionary componentDictionary;
		ComponentArray componentArray;
		ComponentVersionArray componentVersionArray;
		ComponentIDDictionary componentIDDictionary;
		ComponentNameDictionary componentNameDictionary;

		#endregion FIELD VARIABLES

		#region PROPERTIES

		public static new ComponentDictionary Dictionary(ITransaction transaction)
		{
			if(null==componentDictionary)
			{
				componentDictionary=new ComponentDictionary();
				BrokerPersister brokerPersister=new BrokerPersister(transaction.SQLConnection, transaction.SQLTransaction);
				brokerPersister.Transaction=transaction.SQLTransaction;

				brokerPersister.LoadComponentDictionary(componentDictionary);
			}
			return componentDictionary;
		}

		public static void SetDictionary(ComponentDictionary componentDictionary)
		{
			ComponentDictionary.componentDictionary=componentDictionary;
		}

		public ComponentArray Components
		{
			get
			{
				return this.componentArray;
			}
		}

		public IComponentVersionArray ComponentVersions
		{
			get
			{
				return this.componentVersionArray;
			}
		}

		public IComponentVersionArray TaxTopicComponentsVersions
		{
			get
			{
				ComponentCategoryFilter taxTopicOnlyFilter=new ComponentCategoryFilter(new string[]{"Primary","Support"});
				return new FilteredComponentVersionArray(this.componentVersionArray,new ComponentVersionsFilter[]{taxTopicOnlyFilter},this);
			}
		}

		#endregion PROPERTIES

		#region CONSTRUCTORS

		public ComponentDictionary()
			: base()
		{
			componentArray=new ComponentArray();
			componentIDDictionary=new ComponentIDDictionary();
			componentNameDictionary=new ComponentNameDictionary();
			componentVersionArray=new ComponentVersionArray(this.assemblyArray);
		}

		#endregion CONSTRUCTORS

		#region MEMBER FUNCTIONS

		public void Add(IComponent component)
		{
			this.componentArray.Add(component);
			this.componentIDDictionary.Add(component.ID,component);
			this.componentNameDictionary.Add(component.Name,component);
		}

		public void Replace(IComponent component)
		{
			this.componentArray.Replace(component);
			this.componentIDDictionary.Replace(component.ID,component);
			this.componentNameDictionary.Replace(component.Name,component);
		}

		public void DeleteComponent(Guid iD)
		{
			IComponent component=this.componentIDDictionary[iD];
			this.componentArray.Remove(component);
			this.componentNameDictionary.Delete(component.Name);
			this.componentIDDictionary.Delete(iD);
		}

		public bool ContainsComponent(Guid iD)
		{
			return this.componentIDDictionary.Contains(iD);
		}

		public bool ContainsComponent(string name)
		{
			return this.componentNameDictionary.Contains(name);
		}

		public IComponent GetComponent(Guid iD)
		{
			return this.componentIDDictionary[iD];
		}

		public IComponent GetComponent(string name)
		{
			return this.componentNameDictionary[name];
		}

		//====================================================================================

		public void Add(IComponentVersion componentVersion)
		{
			if(this.componentIDDictionary.Contains(componentVersion.ComponentID))
			{
				componentVersion.ContainingComponentDictionary=this;
				base.Add((IAssembly)componentVersion);
			}
			else
				throw new ComponentDictionaryException("Attempt to add a component version referencing a non-existent component");
		}

		public void Add(string componentName,IComponentVersion componentVersion)
		{
			if(this.componentIDDictionary.Contains(componentVersion.ComponentID))
			{
				componentVersion.ContainingComponentDictionary=this;
				base.Add((IAssembly)componentVersion,componentName);
			}
			else
				throw new ComponentDictionaryException("Attempt to add a component version referencing a non-existent component");
		}

		public void Replace(IComponentVersion componentVersion)
		{
			if(this.componentIDDictionary.Contains(componentVersion.ComponentID))
			{
				componentVersion.ContainingComponentDictionary=this;
//				componentVersionArray.Replace(componentVersion);
				base.Replace((IAssembly)componentVersion);
			}
			else
				throw new ComponentDictionaryException("Attempt to replace a component version referencing a non-existent component");
		}

		public void Replace(string componentName,IComponentVersion componentVersion)
		{
			if(this.componentIDDictionary.Contains(componentVersion.ComponentID))
			{
				componentVersion.ContainingComponentDictionary=this;
				//				componentVersionArray.Replace(componentVersion);
				base.Replace((IAssembly)componentVersion,componentName);
			}
			else
				throw new ComponentDictionaryException("Attempt to replace a component version referencing a non-existent component");
		}

		public void DeleteComponentVersion(Guid iD)
		{
			base.Delete(iD);
		}

		public new IComponentVersion this[Guid iD]
		{
			get
			{
				return (ComponentVersion)base[iD];
			}
		}

		public Guid this[string typeName]
		{
			get
			{
				foreach (Assembly entry in Assemblies)
				{
					if (entry.Name == typeName)
					{
						return entry.ID;
					}
				}
				return Guid.Empty;
				//return (IComponent)componentNameDictionary[typeName];
			}
		}

		public bool ContainsComponentVersion(Guid iD)
		{
			return this.Contains(iD);
		}

		public IComponentVersion GetComponentVersion(string name,int majorVersion,int minorVersion)
		{
			AssemblyVersionNumber assemblyVersionNumber=new AssemblyVersionNumber(majorVersion,minorVersion,0,0);
			return (ComponentVersion)this.assemblyNameDictionary[name][assemblyVersionNumber];
		}

		public IAssemblyVersionSet GetComponentVersions(string name)
		{
			return this.assemblyNameDictionary[name];
		}

		public IComponentVersion[] GetComponentVersions(Guid componentID)
		{
			ArrayList componentVersions = new ArrayList();

			foreach(IComponentVersion componentVersion in this.componentVersionArray)
			{
				if(componentVersion.ComponentID == componentID)
					componentVersions.Add(componentVersion);
			}

			return (IComponentVersion[]) componentVersions.ToArray(typeof(IComponentVersion));
		}

		public bool ContainsComponentVersion(string name,int majorVersion,int minorVersion)
		{
			AssemblyVersionNumber assemblyVersionNumber=new AssemblyVersionNumber(majorVersion,minorVersion,0,0);
			return this.Contains(name,assemblyVersionNumber);
		}

//		public IComponentVersionIDDictionary GetVersionsForCMScenarios(ArrayList childCMScenarios)
//		{
//			ComponentVersionIDDictionary componentVersionIDDictionary=new ComponentVersionIDDictionary();
//			foreach(CMScenario cMScenario in childCMScenarios)
//			{
//				IComponentVersion componentVersion=this[cMScenario.CMTypeID];
//				componentVersionIDDictionary.Add(componentVersion.ID,componentVersion);
//			}
//
//			return componentVersionIDDictionary;
//		}

		public IComponentManagement GetDictionaryForCMScenarios(ArrayList childCMScenarios)
		{
			ComponentDictionary componentDictionary=new ComponentDictionary();
			foreach(CMScenario cMScenario in childCMScenarios)
			{
				IComponentVersion componentVersion=this[cMScenario.CMTypeID];
				IComponent component=this.GetComponent(componentVersion.ComponentID);
				if(!componentDictionary.ContainsComponent(componentVersion.ComponentID))
				{
					componentDictionary.Add(component);
				}
				componentDictionary.Add(componentVersion,component.Name);
			}

			return componentDictionary;
		}

		public new void Commit(ITransaction transaction)
		{
			BrokerPersister brokerPersister=new BrokerPersister(transaction.SQLConnection, transaction.SQLTransaction);

			brokerPersister.SaveComponentDictionary(this);
		}

		public new static void Delete()
		{
			AssemblyDictionary.Delete();
			dictionary=null;
		}

		public override void Clear()
		{
			base.Clear();

			this.componentArray.Clear();
			this.componentIDDictionary.Clear();
			this.componentNameDictionary.Clear();
		}

		public IComponentVersionArray GetDateQualifiedTaxTopicComponentVersions(DateTime startDate,DateTime endDate)
		{
			ComponentCategoryFilter taxTopicOnlyFilter=new ComponentCategoryFilter(new string[]{"Primary","Support"});
			ComponentDateOverlapFilter componentDateOverlapFilter=new ComponentDateOverlapFilter(startDate,endDate);
			return new FilteredComponentVersionArray(this.componentVersionArray,new ComponentVersionsFilter[]{taxTopicOnlyFilter,componentDateOverlapFilter},this);
		}

		public IComponentVersionArray GetQualifiedTaxTopicComponentVersions(DateTime startDate,DateTime endDate,IComponentManagement includedVersions)
		{
			ComponentCategoryFilter taxTopicOnlyFilter=new ComponentCategoryFilter(new string[]{"Primary","Support"});
			ComponentDateOverlapFilter componentDateOverlapFilter=new ComponentDateOverlapFilter(startDate,endDate);
			ComponentVersionOrderingFilter componentVersionOrderingFilter=new ComponentVersionOrderingFilter(includedVersions);
			return new FilteredComponentVersionArray(this.componentVersionArray,new ComponentVersionsFilter[]{taxTopicOnlyFilter,componentDateOverlapFilter,componentVersionOrderingFilter},this);
		}

		/// <summary>
		/// Retrieves all Primary and Supporting tax topic versions except ones which are in the includedVersions list.
		/// Will return all verions of a component even if they are ealier than a component which is in the includedVersions collection.
		/// </summary>
		/// <param name="includedVersions"></param>
		/// <returns></returns>
		public IComponentVersionArray GetTaxTopicComponentVersions(IComponentManagement includedVersions)
		{
			ComponentCategoryFilter taxTopicOnlyFilter=new ComponentCategoryFilter(new string[]{"Primary","Support"});
			ComponentAllVersionOrderingFilter componentVersionOrderingFilter=new ComponentAllVersionOrderingFilter(includedVersions);
			return new FilteredComponentVersionArray(this.componentVersionArray,new ComponentVersionsFilter[]{taxTopicOnlyFilter,componentVersionOrderingFilter},this);
		}

		public IComponentVersionArray GetQualifiedPrimaryTaxTopicComponentVersions(DateTime startDate,DateTime endDate,IComponentManagement includedVersions)
		{
			ComponentCategoryFilter taxTopicOnlyFilter=new ComponentCategoryFilter(new string[]{"Primary"});
			ComponentDateOverlapFilter componentDateOverlapFilter=new ComponentDateOverlapFilter(startDate,endDate);
			ComponentVersionOrderingFilter componentVersionOrderingFilter=new ComponentVersionOrderingFilter(includedVersions);
			return new FilteredComponentVersionArray(this.componentVersionArray,new ComponentVersionsFilter[]{taxTopicOnlyFilter,componentDateOverlapFilter,componentVersionOrderingFilter},this);
		}

        public IComponentVersionArray GetQualifiedPrimarySupportTaxTopicComponentVersions(DateTime startDate, DateTime endDate, IComponentManagement includedVersions)
        {
            ComponentCategoryFilter taxTopicOnlyFilter = new ComponentCategoryFilter(new string[] { "Primary", "Support" });
            ComponentDateOverlapFilter componentDateOverlapFilter = new ComponentDateOverlapFilter(startDate, endDate);
            ComponentVersionOrderingFilter componentVersionOrderingFilter = new ComponentVersionOrderingFilter(includedVersions);
            return new FilteredComponentVersionArray(this.componentVersionArray, new ComponentVersionsFilter[] { taxTopicOnlyFilter, componentDateOverlapFilter, componentVersionOrderingFilter }, this);
        }

		#endregion MEMBER FUNCTIONS
	}
}
