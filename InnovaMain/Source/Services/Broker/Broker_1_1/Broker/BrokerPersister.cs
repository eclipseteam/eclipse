using System;
using System.Data;
using System.Data.SqlClient;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Services.CMBroker
{
	/// <summary>
	/// Summary description for BrokerPersister.
	/// </summary>
	public class BrokerPersister : IDataModelManagement
	{
		SqlConnection connection;
		SqlTransaction transaction;

		public BrokerPersister(SqlConnection connection, SqlTransaction transaction)
		{
			this.connection = connection;
			this.transaction = transaction;
		}

		public SqlTransaction Transaction{get{return transaction;}set{transaction=value;}}
		
		public void RemoveOldDatabase()
		{
		}
		
		public void EstablishDatabase()
		{
            SqlCommand establishCommand = DBCommandFactory.Instance.NewSqlCommand();
            establishCommand.Connection = connection;
			establishCommand.Transaction = this.transaction;

			//========================================================================================================================
			// Create the ASSEMBLY table
			establishCommand.CommandText=
			"if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ASSEMBLY]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) "
			+"CREATE TABLE [dbo].[ASSEMBLY] ("
			+"[ID] [uniqueidentifier] NOT NULL ,"
			+"[NAME] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
			+"[DISPLAYNAME] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
			+"[STRONGNAME] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
			+"[MAJORVERSION] [int] NOT NULL ,"
			+"[MINORVERSION] [int] NOT NULL ,"
			+"[DATAFORMAT] [int] NOT NULL ,"
			+"[REVISION] [int] NOT NULL "
			+") ON [PRIMARY];";

			establishCommand.ExecuteNonQuery();

			//========================================================================================================================
			// Create the COMPONENT table
			establishCommand.CommandText=
			"if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COMPONENT]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
			+"CREATE TABLE [dbo].[COMPONENT] ("
			+"[ID] [uniqueidentifier] NOT NULL ,"
			+"[NAME] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
			+"[DISPLAYNAME] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
			+"[CATEGORY] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL, "
			+"[APPLICABILITY] [int] NOT NULL, "
			+"[OBSOLETE] [bit] NOT NULL "
			+") ON [PRIMARY];";

			establishCommand.ExecuteNonQuery();

			//========================================================================================================================
			// Create the COMPONENTVERSION table
			establishCommand.CommandText=
			"if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COMPONENTVERSION]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
			+"CREATE TABLE [dbo].[COMPONENTVERSION] ("
			+"[ID] [uniqueidentifier] NOT NULL ,"
			+"[VERSIONNAME] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
			+"[COMPONENTID] [uniqueidentifier] NOT NULL ,"
			+"[STARTDATE] [datetime] NOT NULL ,"
			+"[ENDDATE] [datetime] NOT NULL ,"
			+"[PERSISTERASSEMBLYSTRONGNAME] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
			+"[PERSISTERCLASS] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
			+"[IMPLEMENTATIONCLASS] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
			+"[OBSOLETE] [bit] NOT NULL "
			+") ON [PRIMARY];";

			establishCommand.ExecuteNonQuery();

			//=======================================================================================================================
			// Create the EVENTLOG table
			establishCommand.CommandText="if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[EVENTLOG]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
				+"CREATE TABLE [dbo].[EVENTLOG] ("
				+"[EVENTID] [uniqueidentifier] NOT NULL ,"
                +"[EVENTCID] [uniqueidentifier] NULL ,"
				+"[DATETIME] [datetime] NOT NULL ,"
				+"[USERNAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,"
				+"[NAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,"
				+"[TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,"
				+"[DETAIL] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL "
				+") ON [PRIMARY]";

			establishCommand.ExecuteNonQuery();

			UpdateDataModel(); 
		}
		private void UpdateDataModel()
		{
			//At this point we will check if the current database has Component table already to in that case will migrate add nessacary changes to it
			//rather then creating new table. 
			DataSet oldDS = new DataSet();
            SqlCommand checkComponentTables = DBCommandFactory.Instance.NewSqlCommand();
            checkComponentTables.Connection = connection;
			checkComponentTables.Transaction = transaction;

			checkComponentTables.CommandText = "SELECT * FROM COMPONENT SELECT * FROM COMPONENTVERSION"; 
			SqlDataAdapter tempDA = new SqlDataAdapter();
			
			tempDA.SelectCommand = checkComponentTables; 
			DataSet oldDataSet  = new DataSet(); 
			tempDA.Fill(oldDS,"COMPONENT");
			tempDA.Fill(oldDS,"COMPONENTVERSION");
 			
			if(oldDS.Tables["COMPONENT"].Columns["OBSOLETE"] == null)
			{
                SqlCommand columnsUpdate = DBCommandFactory.Instance.NewSqlCommand();
                columnsUpdate.Connection = connection;
				columnsUpdate.CommandText="ALTER TABLE dbo.COMPONENT ADD OBSOLETE  bit default '0' not null  ALTER TABLE dbo.COMPONENTVERSION ADD OBSOLETE  bit default '0' not null";
				columnsUpdate.ExecuteNonQuery(); 
			}
		}


		public void RemoveDatabase()
		{
            SqlCommand removeCommand = DBCommandFactory.Instance.NewSqlCommand();
            removeCommand.Connection = connection;
			removeCommand.Transaction = transaction;

			//====================================================================================================
			// Remove the ASSEMBLY table
			removeCommand.CommandText=
				"if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ASSEMBLY]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) "
				+"drop table [dbo].[ASSEMBLY]; ";

			removeCommand.ExecuteNonQuery();

			//====================================================================================================
			// Remove the COMPONENT table
			removeCommand.CommandText=
				"if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_COMPONENTVERSION_COMPONENT]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)"
				+"ALTER TABLE [dbo].[COMPONENTVERSION] DROP CONSTRAINT FK_COMPONENTVERSION_COMPONENT;"

				+"if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COMPONENT]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
				+"drop table [dbo].[COMPONENT];";

			removeCommand.ExecuteNonQuery();

			//====================================================================================================
			// Remove the COMPONENTVERSION table
			removeCommand.CommandText=
				"if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PK_ASSEMBLY_COMPONENTVERSION]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)"
				+"ALTER TABLE [dbo].[ASSEMBLY] DROP CONSTRAINT PK_ASSEMBLY_COMPONENTVERSION;"

				+"if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COMPONENTVERSION]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
				+"drop table [dbo].[COMPONENTVERSION];";

			removeCommand.ExecuteNonQuery();
		}

		public void LoadAssemblyDictionary(AssemblyDictionary assemblyDictionary, bool allRecords)
		{
            if (allRecords)
                LoadAssemblyDictionary(assemblyDictionary, 
                    "select assembly.ID,assembly.NAME,assembly.DISPLAYNAME,assembly.STRONGNAME,assembly.MAJORVERSION,assembly.MINORVERSION,assembly.DATAFORMAT,assembly.REVISION from assembly");
            else
                LoadAssemblyDictionary(assemblyDictionary,
                    "select assembly.ID,assembly.NAME,assembly.DISPLAYNAME,assembly.STRONGNAME,assembly.MAJORVERSION,assembly.MINORVERSION,assembly.DATAFORMAT,assembly.REVISION from assembly where not exists (SELECT componentversion.ID from componentversion where componentversion.id=assembly.ID)");
		}

        public void LoadAssemblyDictionary(AssemblyDictionary assemblyDictionary, string commandText)
        {
            SqlCommand loadCommand = DBCommandFactory.Instance.NewSqlCommand();
            loadCommand.Connection = connection;
            loadCommand.Transaction = transaction;

            loadCommand.CommandText = commandText;
            SqlDataReader loadReader = loadCommand.ExecuteReader();
            while (loadReader.Read())
            {
                Guid iD = loadReader.GetGuid(0);
                string nAME = loadReader.GetString(1);
                string dISPLAYNAME = loadReader.GetString(2);
                string sTRONGNAME = loadReader.GetString(3);
                int mAJORVERSION = loadReader.GetInt32(4);
                int mINORVERSION = loadReader.GetInt32(5);
                int dATAFORMAT = loadReader.GetInt32(6);
                int rEVISION = loadReader.GetInt32(7);

                AssemblyDictionary.AssemblyVersionNumber assemblyVersionNumber = new AssemblyDictionary.AssemblyVersionNumber(mAJORVERSION, mINORVERSION, dATAFORMAT, rEVISION);
                AssemblyDictionary.Assembly assembly = new AssemblyDictionary.Assembly(iD, assemblyVersionNumber, nAME, dISPLAYNAME, sTRONGNAME);

                assemblyDictionary.Add(assembly);
            }
            loadReader.Close();
        }

        /// <summary>
        /// only saves a subversion of assemblies
        /// </summary>
        /// <param name="assemblyDictionary"></param>
		public void SaveAssemblyDictionary(AssemblyDictionary assemblyDictionary)
		{
			AssemblyDictionary existingAssemblyDictionary=new AssemblyDictionary();
			LoadAssemblyDictionary(existingAssemblyDictionary, false);

			foreach(AssemblyDictionary.Assembly assembly in assemblyDictionary.Assemblies)
			{
				if(existingAssemblyDictionary.Contains(assembly.ID))
				{
					AssemblyDictionary.Assembly existingAssembly= (AssemblyDictionary.Assembly)existingAssemblyDictionary[assembly.ID];
					if(assembly!=existingAssembly)
					{
						UpdateAssemblyDBImage(assembly);
					}
				}
				else
				{
					InsertAssemblyDBImage(assembly);
				}
			}

			foreach(AssemblyDictionary.Assembly assembly in existingAssemblyDictionary.Assemblies)
			{
				if(!assemblyDictionary.Contains(assembly.ID))
				{
					DeleteAssemblyDBImage(assembly);
				}
			}
		}

		void UpdateAssemblyDBImage(AssemblyDictionary.Assembly assembly)
		{
            SqlCommand updateCommand = DBCommandFactory.Instance.NewSqlCommand();
            updateCommand.Connection = connection;
			updateCommand.Transaction = transaction;

			updateCommand.CommandText="UPDATE ASSEMBLY "
				+"SET NAME='"+assembly.Name+"',"
				+"DISPLAYNAME='"+assembly.DisplayName+"',"
				+"STRONGNAME='"+assembly.StrongName+"',"
				+"MAJORVERSION="+assembly.VersionNumber.MajorVersion+","
				+"MINORVERSION="+assembly.VersionNumber.MinorVersion+","
				+"DATAFORMAT="+assembly.VersionNumber.DataFormat+","
				+"REVISION="+assembly.VersionNumber.Revision+" WHERE "
				+"ID='"+assembly.ID.ToString()+"';";

			updateCommand.ExecuteNonQuery();
		}

		void InsertAssemblyDBImage(AssemblyDictionary.Assembly assembly)
		{
            SqlCommand insertCommand = DBCommandFactory.Instance.NewSqlCommand();
            insertCommand.Connection = connection;
			insertCommand.Transaction = transaction;

			insertCommand.CommandText="INSERT INTO ASSEMBLY "
				+"(ID,NAME,DISPLAYNAME,STRONGNAME,MAJORVERSION,MINORVERSION,DATAFORMAT,REVISION) "
				+"VALUES ("
				+"'"+assembly.ID.ToString()+"',"
				+"'"+assembly.Name+"',"
				+"'"+assembly.DisplayName+"',"
				+"'"+assembly.StrongName+"',"
				+assembly.VersionNumber.MajorVersion+", "
				+assembly.VersionNumber.MinorVersion+", "
				+assembly.VersionNumber.DataFormat+", "
				+assembly.VersionNumber.Revision+");";

			insertCommand.ExecuteNonQuery();
		}

		void DeleteAssemblyDBImage(AssemblyDictionary.Assembly assembly)
		{
            SqlCommand deleteCommand = DBCommandFactory.Instance.NewSqlCommand();
            deleteCommand.Connection = connection;
			deleteCommand.Transaction = transaction;

			deleteCommand.CommandText="DELETE ASSEMBLY WHERE "
				+"ID='"+assembly.ID.ToString()+"';";

			deleteCommand.ExecuteNonQuery();
		}


		public void LoadComponentDictionary(ComponentDictionary componentDictionary)
		{
            SqlCommand loadCommand = DBCommandFactory.Instance.NewSqlCommand();
            loadCommand.Connection = connection;
			loadCommand.Transaction=this.Transaction;
			loadCommand.CommandText="SELECT "
				+"COMPONENT.ID,"										// Field 0
				+"COMPONENT.NAME,"										// Field 1
				+"COMPONENT.DISPLAYNAME,"								// Field 2
				+"COMPONENT.CATEGORY,"									// Field 3
				+"COMPONENT.APPLICABILITY, "								// Field 4
				+"COMPONENT.OBSOLETE "									// Field 5
				+"FROM COMPONENT";
			SqlDataReader loadReader=loadCommand.ExecuteReader();
			while(loadReader.Read())
			{
				Guid iD=loadReader.GetGuid(0);
				string nAME=loadReader.GetString(1);
				string dISPLAYNAME=loadReader.GetString(2);
				string cATEGORY=loadReader.GetString(3);
				CMApplicability cMAPPLICABILITY=(CMApplicability)loadReader.GetInt32(4);
				bool obsolete = loadReader.GetBoolean(5);

				ComponentDictionary.Component component=new ComponentDictionary.Component(iD,nAME,dISPLAYNAME,cATEGORY,cMAPPLICABILITY,obsolete);

				componentDictionary.Add(component);
			}
			loadReader.Close();

            loadCommand = DBCommandFactory.Instance.NewSqlCommand();
            loadCommand.Connection = connection;
			loadCommand.Transaction=this.Transaction;
			loadCommand.CommandText="SELECT "
				+"ASSEMBLY.ID,"										// Field 0
				+"ASSEMBLY.NAME,"									// Field 1
				+"ASSEMBLY.DISPLAYNAME,"							// Field 2
				+"ASSEMBLY.STRONGNAME,"								// Field 3
				+"ASSEMBLY.MAJORVERSION,"							// Field 4
				+"ASSEMBLY.MINORVERSION,"							// Field 5
				+"ASSEMBLY.DATAFORMAT,"								// Field 6
				+"ASSEMBLY.REVISION,"								// Field 7
				+"COMPONENTVERSION.VERSIONNAME,"					// Field 8
				+"COMPONENTVERSION.COMPONENTID,"					// Field 9
				+"COMPONENTVERSION.STARTDATE,"						// Field 10
				+"COMPONENTVERSION.ENDDATE,"						// Field 11
				+"COMPONENTVERSION.PERSISTERASSEMBLYSTRONGNAME,"	// Field 12
				+"COMPONENTVERSION.PERSISTERCLASS,"					// Field 13
				+"COMPONENTVERSION.IMPLEMENTATIONCLASS, "			// Field 14
				+"COMPONENT.NAME, "									// Field 15
				+"COMPONENT.OBSOLETE "								// Field 16
				+"FROM ASSEMBLY,COMPONENTVERSION,COMPONENT WHERE ASSEMBLY.ID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID";
			loadReader=loadCommand.ExecuteReader();
			while(loadReader.Read())
			{
				Guid iD=loadReader.GetGuid(0);
				string nAME=loadReader.GetString(1);
				string dISPLAYNAME=loadReader.GetString(2);
				string sTRONGNAME=loadReader.GetString(3);
				int mAJORVERSION=loadReader.GetInt32(4);
				int mINORVERSION=loadReader.GetInt32(5);
				int dATAFORMAT=loadReader.GetInt32(6);
				int rEVISION=loadReader.GetInt32(7);
				string versionName=loadReader.GetString(8);
				Guid componentID=loadReader.GetGuid(9);
				DateTime startDate=loadReader.GetDateTime(10);
				DateTime endDate=loadReader.GetDateTime(11);
				string persisterAssemblyStrongName=loadReader.GetString(12);
				string persisterClass=loadReader.GetString(13);
				string implementationClass=loadReader.GetString(14);
				string componentName=loadReader.GetString(15);
				bool obsolete = loadReader.GetBoolean(16);

				AssemblyDictionary.AssemblyVersionNumber assemblyVersionNumber=new AssemblyDictionary.AssemblyVersionNumber(mAJORVERSION,mINORVERSION,dATAFORMAT,rEVISION);
				ComponentDictionary.ComponentVersion componentVersion=new ComponentDictionary.ComponentVersion(iD,assemblyVersionNumber,nAME,dISPLAYNAME,sTRONGNAME,
					versionName,componentID,startDate,endDate,persisterAssemblyStrongName,persisterClass,implementationClass, obsolete);

				componentDictionary.Add(componentName,componentVersion);
			}
			loadReader.Close();
		}

		public void SaveComponentDictionary(ComponentDictionary componentDictionary)
		{
			ComponentDictionary existingComponentDictionary=new ComponentDictionary();
			LoadComponentDictionary(existingComponentDictionary);

			foreach(ComponentDictionary.ComponentVersion componentVersion in componentDictionary.ComponentVersions)
			{
				if(existingComponentDictionary.Contains(componentVersion.ID))
				{
					ComponentDictionary.ComponentVersion existingComponentVersion=(ComponentDictionary.ComponentVersion)existingComponentDictionary[componentVersion.ID];
					if(componentVersion!=existingComponentVersion)
					{
						UpdateComponentVersionDBImage(componentVersion);
					}
				}
				else
				{
					InsertComponentVersionDBImage(componentVersion);
				}
			}

			foreach(ComponentDictionary.ComponentVersion componentVersion in existingComponentDictionary.ComponentVersions)
			{
				if(!componentDictionary.Contains(componentVersion.ID))
				{
					DeleteComponentVersionDBImage(componentVersion);
				}
			}

			foreach(ComponentDictionary.Component component in componentDictionary.Components)
			{
				if(existingComponentDictionary.ContainsComponent(component.ID))
				{
					IComponent existingComponent=existingComponentDictionary.GetComponent(component.ID);
					if(component!=existingComponent)
					{
						UpdateComponentDBImage(component);
					}
				}
				else
				{
					InsertComponentDBImage(component);
				}
			}

			foreach(ComponentDictionary.Component component in existingComponentDictionary.Components)
			{
				if(!componentDictionary.ContainsComponent(component.ID))
				{
					DeleteComponentDBImage(component);
				}
			}

		}

		void UpdateComponentVersionDBImage(ComponentDictionary.ComponentVersion componentVersion)
		{
			UpdateAssemblyDBImage(componentVersion);

            SqlCommand updateCommand = DBCommandFactory.Instance.NewSqlCommand();
            updateCommand.Connection = connection;
			updateCommand.Transaction = transaction;

			updateCommand.CommandText="UPDATE COMPONENTVERSION "
				+"SET VERSIONNAME='"+componentVersion.VersionName+"', "
				+"COMPONENTID='"+componentVersion.ComponentID.ToString()+"',"
				+"STARTDATE=@STARTDATE,"
				+"ENDDATE=@ENDDATE,"
				+"PERSISTERASSEMBLYSTRONGNAME='"+componentVersion.PersisterAssemblyStrongName+"',"
				+"PERSISTERCLASS='"+componentVersion.PersisterClass+"', "
				+"IMPLEMENTATIONCLASS='"+componentVersion.ImplementationClass+"', "
				+"OBSOLETE=" + (componentVersion.Obsolete ? 1:0) + " WHERE "
				+"ID='"+componentVersion.ID.ToString()+"';";

			SqlParameter startDate=new SqlParameter("@STARTDATE", SqlDbType.DateTime,8);
			startDate.Value=componentVersion.StartDate;
			updateCommand.Parameters.Add(startDate);
			SqlParameter endDate=new SqlParameter("@ENDDATE", SqlDbType.DateTime,8);
			endDate.Value=componentVersion.EndDate;
			updateCommand.Parameters.Add(endDate);

			updateCommand.ExecuteNonQuery();
		}

		void InsertComponentVersionDBImage(ComponentDictionary.ComponentVersion componentVersion)
		{
			InsertAssemblyDBImage(componentVersion);

            SqlCommand insertCommand = DBCommandFactory.Instance.NewSqlCommand();
            insertCommand.Connection = connection;
			insertCommand.Transaction = transaction;

			insertCommand.CommandText="INSERT INTO COMPONENTVERSION "
				+"(ID,VERSIONNAME,COMPONENTID,STARTDATE,ENDDATE,PERSISTERASSEMBLYSTRONGNAME,PERSISTERCLASS,IMPLEMENTATIONCLASS,OBSOLETE) "
				+"VALUES ("
				+"'"+componentVersion.ID.ToString()+"',"
				+"'"+componentVersion.VersionName+"', "
				+"'"+componentVersion.ComponentID.ToString()+"',"
				+"@STARTDATE, "
				+"@ENDDATE, "
				+"'"+componentVersion.PersisterAssemblyStrongName+"', "
				+"'"+componentVersion.PersisterClass+"', "
				+"'"+componentVersion.ImplementationClass+"', " 
				+ (componentVersion.Obsolete ? 1:0) + ");";

			SqlParameter startDate=new SqlParameter("@STARTDATE", SqlDbType.DateTime,8);
			startDate.Value=componentVersion.StartDate;
			insertCommand.Parameters.Add(startDate);
			SqlParameter endDate=new SqlParameter("@ENDDATE", SqlDbType.DateTime,8);
			endDate.Value=componentVersion.EndDate;
			insertCommand.Parameters.Add(endDate);

			insertCommand.ExecuteNonQuery();

		}

		void DeleteComponentVersionDBImage(ComponentDictionary.ComponentVersion componentVersion)
		{
			DeleteAssemblyDBImage(componentVersion);

            SqlCommand deleteCommand = DBCommandFactory.Instance.NewSqlCommand();
            deleteCommand.Connection = connection;
			deleteCommand.Transaction = transaction;
			deleteCommand.CommandText="DELETE COMPONENTVERSION WHERE "
				+"ID='"+componentVersion.ID.ToString()+"';";

			deleteCommand.ExecuteNonQuery();
		}

		void UpdateComponentDBImage(ComponentDictionary.Component component)
		{
            SqlCommand updateCommand = DBCommandFactory.Instance.NewSqlCommand();
            updateCommand.Connection = connection;
			updateCommand.Transaction = transaction;

			updateCommand.CommandText="UPDATE COMPONENT "
				+"SET NAME='"+component.Name+"', "
				+"DISPLAYNAME='"+component.DisplayName+"',"
				+"CATEGORY='"+component.Category+"',"
				+"APPLICABILITY='"+component.Applicability+"',"
				+"OBSOLETE="+(component.Obsolete ? 1:0)+" WHERE "
				+"ID='"+component.ID.ToString()+"';";

			updateCommand.ExecuteNonQuery();
		}

		void InsertComponentDBImage(ComponentDictionary.Component component)
		{
            SqlCommand insertCommand = DBCommandFactory.Instance.NewSqlCommand();
            insertCommand.Connection = connection;
			insertCommand.Transaction = transaction;

			insertCommand.CommandText="INSERT INTO COMPONENT "
				+"(ID,NAME,DISPLAYNAME,CATEGORY,APPLICABILITY,OBSOLETE) "
				+"VALUES ("
				+"'"+component.ID.ToString()+"',"
				+"'"+component.Name+"', "
				+"'"+component.DisplayName+"',"
				+"'"+component.Category+"',"
				+"'"+component.Applicability+"', " 
				+(component.Obsolete ? 1:0)+");";

			insertCommand.ExecuteNonQuery();
		}

		void DeleteComponentDBImage(ComponentDictionary.Component component)
		{
            SqlCommand deleteCommand = DBCommandFactory.Instance.NewSqlCommand();
            deleteCommand.Connection = connection;
			deleteCommand.Transaction = transaction;

			deleteCommand.CommandText="DELETE COMPONENT WHERE "
				+"ID='"+component.ID.ToString()+"';";

			deleteCommand.ExecuteNonQuery();
		}


    }
}
