using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Text;
using System.Security.Principal;
using System.Diagnostics;
using System.Xml;
using System.Resources;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CommonPersistence;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.TaxSimpLicence;
using Oritax.TaxSimp.CM.OrganizationUnit;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.VelocityCache;
using Oritax.TaxSimp.Services.CMBroker.EventLogging;

namespace Oritax.TaxSimp.Services.CMBroker
{
    public class CMBroker : MarshalByRefObject, ICMBroker, IDisposable
    {
        private Guid clientID;
        private static Transactions transactions = new Transactions();
        private IPrincipal userInfo;
        private bool disposed = false;
        private bool migrationInProgress = false;
        private bool saveOverride = false;
        private bool inPlaceUpgradeInProgress = false;
        private string systemWideMessage = "-1";
        private string productVersion = "";
        private RequestProcessStatusProvider requestStatusProvider = null;

        public bool MigrationInProgress
        {
            get
            {
                return migrationInProgress;
            }
            set
            {
                migrationInProgress = value;
            }
        }

        public bool InPlaceUpgradeInProgress
        {
            get
            {
                return inPlaceUpgradeInProgress;
            }
            set
            {
                inPlaceUpgradeInProgress = value;
            }
        }

        public bool SaveOverride
        {
            get
            {
                return saveOverride;
            }
            set
            {
                saveOverride = value;
            }
        }

        public string SystemWideDisplayMessage
        {
            get
            {
                if (systemWideMessage == "-1")
                {
                    IOrganization objOrganisation = (IOrganization)this.GetWellKnownCM(WellKnownCM.Organization);

                    if (objOrganisation != null)
                        systemWideMessage = objOrganisation.SystemWideDisplayMessage;

                    this.ReleaseBrokerManagedComponent(objOrganisation);
                }
                return systemWideMessage;
            }
            set
            {
                systemWideMessage = value;
            }
        }

        public string ProductVersion
        {
            get
            {
                if (productVersion == "")
                {
                    ResourceManager resourceManager = new ResourceManager("Oritax.TaxSimp.Services.CMBroker.Resources.Version", Assembly.GetExecutingAssembly());
                    productVersion = resourceManager.GetString("ProductVersion");
                }
                return productVersion;
            }
        }

        private static int securitySetting = -1;

        public IPrincipal UserContext { get { return userInfo; } }
        public bool InTransaction
        {
            get
            {
                ITransaction currentTransaction = this.Transaction;

                if (currentTransaction != null)
                    return currentTransaction.State == TransactionStateOption.Started;
                else
                    return false;
            }
        }

        internal Transactions Transactions
        {
            get
            {
                return transactions;
            }
        }

        public ITransaction Transaction
        {
            get
            {
                this.CheckIfUserCancelled();
                return (ITransaction)transactions[this.clientID];
            }
        }

        /// <summary>
        /// Indicates if the user has cancelled the operation
        /// </summary>
        public bool UserRequestCancelled
        {
            get
            {
                return this.requestStatusProvider.Cancelled;
            }
        }

        /// <summary>
        /// Sets the status message to be displayed to the user
        /// about the current status of the operation
        /// </summary>
        public void SetStatusDisplayMessage(string statusMessage)
        {
            this.requestStatusProvider.StatusInfoMessage = statusMessage;
        }

        /// <summary>
        /// Gets the status message that is currently being
        /// displayed to the user about the status of the operation
        /// </summary>
        /// <returns>The status message being displayed to the user</returns>
        public string GetStatusDisplayMessage()
        {
            return this.requestStatusProvider.StatusInfoMessage;
        }

        public int SecuritySetting
        {
            get
            {
                if (securitySetting == -1)
                {
                    IOrganization objOrganisation = (IOrganization)this.GetWellKnownCM(WellKnownCM.Organization);
                    if (objOrganisation != null)
                        securitySetting = objOrganisation.SecuritySetting;
                }
                return (securitySetting);
            }
            set
            {
                securitySetting = value;
            }
        }

        public CMBroker(string DBConnection)
        {
            clientID = Guid.NewGuid();

            ITransaction transaction;
            if (VelocityConfiguration.IsVelocityTransactionConfigured)
                transaction = new TransactionVelocity(this, clientID, DBConnection);
            else if (VelocityConfiguration.IsVelocityConfigured)
                transaction = new TransactionWithCache(this, clientID, DBConnection);
            else
                transaction = new Transaction(this, clientID, DBConnection);

            transactions[clientID] = transaction;

            CMCacheManager.CacheManager.LogTransactionStart(transaction);
        }

        public CMBroker()
            : this(ConfigurationManager.AppSettings["DBConnectionString"])
        {
        }

        public CMBroker(IPrincipal userContext)
            : this(userContext, ConfigurationManager.AppSettings["DBConnectionString"])
        {
        }

        public CMBroker(IPrincipal userContext, string DBConnection)
            : this(DBConnection)
        {
            this.userInfo = userContext;
        }

        public override object InitializeLifetimeService()
        {
            // note: do not remove this method as it will drop the connection after a period of time
            // this will keep the object alive until the app domain shuts down
            return null;
        }

        public void CheckTransationIsRunning()
        {
            if (!this.InTransaction)
                throw new TransactionNotRunningException("You cannot call this method when a transaction is not running");
        }

        /// <summary>
        /// Indicates if a long running transaction is in progress somewhere in the system
        /// </summary>
        /// <returns>True if there is a long running transaction in progress, false otherwise</returns>
        public bool IsLongRunningTransactionInProgress()
        {
            return transactions.IsLongRunningTransactionInProgress();
        }

        /// <summary>
        /// Log an event that has ocurred within the system
        /// </summary>
        /// <param name="eventType">The event type</param>
        /// <param name="eventDetails">The details of the event</param>
        public void LogEvent(int eventType, Guid cid, string eventDetails)
        {
            string strUserName = String.Empty;

            // check if the user information is present, if it is, use the username,
            // otherwise, use "Unknown user"
            if (this.userInfo != null)
                strUserName = this.userInfo.Identity.Name;
            else
                strUserName = "Unknown user";


            this.LogEvent(eventType, eventDetails, strUserName, cid);
        }

        public IEventLogQuery QueryEventLog(EventLogQueryType eventLogQueryType)
        {
            if (InTransaction)
                return new EventLogQuery(this.Transaction, eventLogQueryType);
            else
                throw new Exception("Attempting to read event log when not in transaction");
        }

        /// <summary>
        /// Log an event that has ocurred within the system
        /// </summary>
        /// <param name="eventType">The event type</param>
        /// <param name="eventDetails">The details of the event</param>
        /// <param name="userDetails">The name of the user</param>
        public void LogEvent(int eventType, string eventDetails, string userDetails, Guid cid)
        {
            if (InTransaction)
                EventLoggingFactory.LogEvent(eventType, eventDetails, userDetails, cid);
        }

        /// <summary>
        /// Assembly dictionary only contains asseblies not included in component dictionary
        /// </summary>
        /// <returns></returns>
        public IAssemblyManagement GetAssemblyDictionary()
        {
            if (InTransaction)
                return AssemblyDictionary.Dictionary(this.Transaction);
            else
                throw new TransactionNotRunningException("CreateCMInstance called without a running transaction, transaction needs to be started before calling this method");
        }
        /// <summary>
        /// This is the full dictionary of all assemblies
        /// </summary>
        /// <returns></returns>
        public IAssemblyManagement GetAssemblyFullDictionary()
        {
            if (InTransaction)
                return AssemblyFullDictionary.Dictionary(this.Transaction, true);
            else
                throw new TransactionNotRunningException("CreateCMInstance called without a running transaction, transaction needs to be started before calling this method");
        }
        public IComponentManagement GetComponentDictionary()
        {
            if (InTransaction)
                return ComponentDictionary.Dictionary(this.Transaction);
            else
                throw new TransactionNotRunningException("CreateCMInstance called without a running transaction, transaction needs to be started before calling this method");
        }

        public void SetComponentDictionary(IComponentManagement componentDictionary)
        {
            if (InTransaction)
                ComponentDictionary.SetDictionary((ComponentDictionary)componentDictionary);
            else
                throw new TransactionNotRunningException("CreateCMInstance called without a running transaction, transaction needs to be started before calling this method");
        }

        public IBrokerManagedComponent GetQualifiedList(Guid typeID, object qualifier)
        {
            IBrokerManagedComponent iBMC = CMCacheManager.CacheManager.GetQualifiedList(typeID, qualifier, this.Transaction);
            return iBMC;
        }

        public DataTable ListBMCCollection(CollectionSpecifier bMCCollectionSpecifier)
        {
            DataTable collectionTable = CMCacheManager.CacheManager.ListBMCCollection(bMCCollectionSpecifier, this.Transaction);
            return collectionTable;
        }

        public void SetStart()
        {
            CMCacheManager.CacheLock.AcquireReaderLock(-1);
            if (!InTransaction)
                this.Transaction.Start();
        }

        public void SetWriteStart()
        {
            if (VelocityConfiguration.IsITLPDSCached)
            {
                if (!VelocityCacheString.VelocityCache.GetAndLock(CMCacheManager.VELOCITY_LOCK_KEY))
                    throw new Exception("Unable to write as Velocity cache is currently locked. Please try again later.");
                else
                {
                    if (!CMCacheManager.CacheLock.IsReaderLockHeld)
                        CMCacheManager.CacheLock.AcquireWriterLock(-1);
                    else
                        CMCacheManager.CacheLock.UpgradeToWriterLock(-1);
                }
            }
            else
            {
                //Need to look at reader writer lock. 
                //if (!CMCacheManager.CacheLock.IsReaderLockHeld)
                //    CMCacheManager.CacheLock.AcquireWriterLock(-1);
                //else
                //    CMCacheManager.CacheLock.UpgradeToWriterLock(-1);
                this.SaveOverride = true;
            }

            if (!InTransaction)
                this.Transaction.Start();
        }

        public void SetComplete(bool installationsChanged)
        {
            if (InTransaction)
            {
                if (installationsChanged)
                {
                    AssemblyDictionary.Dictionary(this.Transaction).Commit(this.Transaction);
                    ComponentDictionary.Dictionary(this.Transaction).Commit(this.Transaction);
                }
            }

            SetComplete();
        }

        public void SetComplete()
        {
            if (InTransaction)
            {
                try
                {
                    this.LogEvent("Transaction: " + this.Transaction.TID.ToString() + " completed");

                    if (CMCacheManager.AmWriter() || this.saveOverride)
                        CMCacheManager.CacheManager.Commit(this.Transaction);
                    else
                    {
                        EventLoggingFactory.CommitLogEntries(this.Transaction);
                        //CMCacheManager.CacheManager.DebugCheckReadHasntModifiedCMS();
                    }
                    this.Transaction.Complete();
                }
                catch
                {
                    this.Transaction.Rollback();
                    throw;
                }
                finally
                {
                    CMCacheManager.CacheLock.ReleaseLock();
                    this.SaveOverride = false;
                    if (VelocityConfiguration.IsITLPDSCached)
                        VelocityCacheString.VelocityCache.Unlock(CMCacheManager.VELOCITY_LOCK_KEY);
                }

            }

        }

        internal void LogEvent(string eventString)
        {
            try
            {
                // Added 30MAY03 - SC,  12 Sept SI
                if (ConfigurationManager.AppSettings["LoggingMessage"] == "*"
                    || ConfigurationManager.AppSettings["LoggingMessage"] == "ON")
                {
                    if (Trace.Listeners["TX360EnterpriseListener"] == null)
                    {
                        Oritax.TaxSimp.Utilities.EventLogTraceListener objListener = new Oritax.TaxSimp.Utilities.EventLogTraceListener();
                        Trace.Listeners.Add(objListener);
                    }
                }
                Trace.WriteLine(eventString);
            }
            catch
            {
                //if this debugging logging stuff fails for any reason then don't bring down the system
                //carry on regardless even though it may cause a slowdown!
            }
        }

        public void SetAbort()
        {
            if (InTransaction)
            {
                try
                {
                    if (CMCacheManager.AmWriter() || saveOverride)
                        CMCacheManager.CacheManager.Abort(this.Transaction);

                    EventLoggingFactory.AbortLogEntries();
                }
                finally
                {
                    CMCacheManager.CacheLock.ReleaseLock();

                    if (VelocityConfiguration.IsITLPDSCached)
                        VelocityCacheString.VelocityCache.Unlock(CMCacheManager.VELOCITY_LOCK_KEY);
                }
            }
        }

        public void SetRequestStatusProvider(RequestProcessStatusProvider statusProvider)
        {
            this.requestStatusProvider = statusProvider;
        }

        private void CheckIfUserCancelled()
        {
            if (this.requestStatusProvider != null &&
                this.requestStatusProvider.Cancelled)
            {
                if (!this.requestStatusProvider.CancelRequestAwknowledged)
                {
                    this.requestStatusProvider.AwknowledgeCancelRequest();
                    throw new UserCancelledRequestException();
                }
            }
        }

        public void RegisterIDChange(Guid oldCID, Guid newCID)
        {
            if (InTransaction)
                CMCacheManager.CacheManager.RegisterIDChange(oldCID, newCID, this.Transaction);
            else
                throw new TransactionNotRunningException("RegisterIDChange called without a running transaction, transaction needs to be started before calling this method");
        }

        #region DEPRECATED BMC CREATION FUNCTIONS

        public ICalculationModule CreateCMInstance(Guid typeId, string cmName, BusinessStructureContext objContext, Guid objCLID, Guid objCSID)
        {
            ICalculationModule iCM = null;

            if (InTransaction)
                iCM = CMCacheManager.CacheManager.CreateCMInstance(typeId, cmName, this.Transaction, objContext, (ICMBroker)this, objCLID, objCSID);
            else
                throw new TransactionNotRunningException("CreateCMInstance called without a running transaction, transaction needs to be started before calling this method");

            return iCM;
        }

        public ICalculationModule CreateCMInstance(string typeName, string cmName, BusinessStructureContext objContext, Guid objCLID, Guid objCSID)
        {
            ICalculationModule iCM = null;

            if (InTransaction)
                iCM = CMCacheManager.CacheManager.CreateCMInstance(typeName, cmName, this.Transaction, objContext, (ICMBroker)this, objCLID, objCSID);
            else
                throw new TransactionNotRunningException("CreateCMInstance called without a running transaction, transaction needs to be started before calling this method");

            return iCM;
        }

        public IBrokerManagedComponent CreateBMCInstance(string typeName, string bmcName)
        {
            IBrokerManagedComponent iBMC = null;

            if (InTransaction)
                iBMC = CMCacheManager.CacheManager.CreateBMCInstance(Guid.NewGuid(), typeName, bmcName, this.Transaction);
            else
                throw new TransactionNotRunningException("CreateBMCInstance called without a running transaction, transaction needs to be started before calling this method");

            return iBMC;
        }

        public IBrokerManagedComponent CreateBMCInstance(Guid cIID, string typeName, string bmcName)
        {
            IBrokerManagedComponent iBMC = null;

            if (InTransaction)
                iBMC = CMCacheManager.CacheManager.CreateBMCInstance(cIID, typeName, bmcName, this.Transaction);
            else
                throw new TransactionNotRunningException("CreateBMCInstance called without a running transaction, transaction needs to be started before calling this method");

            return iBMC;
        }

        public IBrokerManagedComponent CreateTransientBMCInstance(string typeName, string name)
        {
            IBrokerManagedComponent iBMC = null;

            if (this.InTransaction)
            {
                using (CalculationTypePersist calculationTypePersist = new CalculationTypePersist(this.Transaction.SQLConnection, this.Transaction.SQLTransaction))
                {
                    iBMC = CMCacheManager.CacheManager.CreateTransientComponentInstance(typeName, this.Transaction);
                    if (null != iBMC)
                        iBMC.Initialize(name);
                }
            }
            else
                throw new TransactionNotRunningException("CreateBMCInstance called without a running transaction, transaction needs to be started before calling this method");

            return iBMC;
        }

        #endregion DEPRECATED BMC CREATION FUNCTIONS

        #region COMPONENT INSTANCE CREATION FUNCTIONS

        public IBrokerManagedComponent CreateComponentInstance(Guid typeID, string instanceName)
        {
            return CreateComponentInstance(Guid.NewGuid(), typeID, instanceName);
        }

        /// <summary>
        /// Create a persistent instance of a Broker Managed Component of a specified type.
        /// </summary>
        /// <param name="instanceID"></param>
        /// <param name="typeID">The Component Version Type ID of the component to be created.</param>
        /// <param name="instanceName"></param>
        /// <returns>An interface to the new component.</returns>
        public IBrokerManagedComponent CreateComponentInstance(Guid instanceID, Guid typeID, string instanceName)
        {
            if (InTransaction)
            {
                IComponentManagement componentDictionary = this.GetComponentDictionary();
                IComponentVersion componentVersion = componentDictionary[typeID];
                return CMCacheManager.CacheManager.CreateComponentInstance(instanceID, componentVersion, instanceName, this.Transaction);
            }
            else
                throw new TransactionNotRunningException("CreateBMCInstance called without a running transaction, transaction needs to be started before calling this method");
        }

        /// <summary>
        /// Find component Id from assembly ID (or Component Version Type ID)
        /// </summary>
        /// <param name="instanceName"></param>
        /// <returns>component version</returns>
        public IComponentVersion GetComponentVersion(Guid typeID)
        {
            if (InTransaction)
            {
                IComponentManagement componentDictionary = this.GetComponentDictionary();
                IComponentVersion componentVersion = componentDictionary[typeID];
                return componentVersion;
            }
            else
                throw new TransactionNotRunningException("GetComponentID called without a running transaction, transaction needs to be started before calling this method");
        }
        /// <summary>
        /// Creates a non-persisted instance of a Broker Managed Component of a specified type.
        /// </summary>
        /// <param name="typeID">The Component Version Type ID of the component to be created.</param>
        /// <returns>An interface to the new component.</returns>
        public IBrokerManagedComponent CreateTransientComponentInstance(Guid typeID)
        {
            if (this.InTransaction)
            {
                IComponentManagement componentDictionary = this.GetComponentDictionary();
                IComponentVersion componentVersion = componentDictionary[typeID];
                return CMCacheManager.CacheManager.CreateTransientComponentInstance(Guid.NewGuid(), componentVersion, string.Empty, this.Transaction);
            }
            else
                throw new TransactionNotRunningException("CreateBMCInstance called without a running transaction, transaction needs to be started before calling this method");
        }

        #endregion BMC CREATION FUNCTIONS

        /// <summary>
        /// Copies a CM implementation instance.
        /// </summary>
        /// <param name="cIID"></param>
        /// <param name="cSID"></param>
        /// <returns></returns>
        public ICalculationModule CopyCMInstance(Guid cIID, Guid cSID)
        {
            if (InTransaction)
                return CMCacheManager.CacheManager.CopyCMInstance(cIID, cSID, this.Transaction);
            else
                throw new TransactionNotRunningException("CopyCMInstance called without a running transaction, transaction needs to be started before calling this method");
        }

        /// <summary>
        /// Updates a CM instance from an old type to a new type, by returning a CM of the required new
        /// type populated with the data from the old instance.
        /// </summary>
        /// <param name="cIID">The implementation ID of the CM instance.</param>
        /// <param name="cTID">The required typeID of the new CM.</param>
        /// <returns>An interface to the new CM instance.</returns>
        public ICalculationModule UpdateCMInstance(Guid cIID, Guid cTID)
        {
            if (InTransaction)
            {
                ICalculationModule iCM = CMCacheManager.CacheManager.UpdateCMInstance(cIID, cTID, this.Transaction);
                this.LogEvent(EventType.UpdateComponentModule,Guid.Empty, "The component module '" + iCM.Name + "' Updated");
                return iCM;
            }
            else
                throw new TransactionNotRunningException("UpdateCMInstance called without a running transaction, transaction needs to be started before calling this method");
        }

        /// <summary>
        /// Gets a non-persistant instance of the specified Broker Managed Component
        /// </summary>
        /// <param name="typeId">The type id of the component version</param>
        /// <param name="cmName">The name to give to the instance</param>
        /// <returns>An interface to the new component.</returns>
        public IBrokerManagedComponent GetBMCInstance(Guid typeId, string cmName)
        {
            if (this.InTransaction)
            {
                IBrokerManagedComponent iBMC = CMCacheManager.CacheManager.CreateTransientComponentInstance(typeId, this.Transaction);

                if (null != iBMC)
                    iBMC.Initialize(cmName);

                return iBMC;
            }
            else
                throw new TransactionNotRunningException("CreateBMCInstance called without a running transaction, transaction needs to be started before calling this method");
        }

        public IBrokerManagedComponent GetBMCInstance(Guid cID)
        {
            if (InTransaction)
                return CMCacheManager.CacheManager.GetBMCInstance(cID, this.Transaction);
            else
                throw new TransactionNotRunningException("GetBMCInstance called without a running transaction, transaction needs to be started before calling this method");
        }

        public IBrokerManagedComponent GetBMCInstance(string bmcName, string strType)
        {
            if (InTransaction)
                return CMCacheManager.CacheManager.GetBMCInstance(bmcName, strType, this.Transaction);
            else
                throw new TransactionNotRunningException("GetBMCInstance called without a running transaction, transaction needs to be started before calling this method");
        }

        /// <summary>
        /// Releases a broker managed component from the transaction, this means
        /// that the object is not being used any more and can be reclaimed if the 
        /// cache is exhausted. If the object has been modified it will be saved
        /// before it is removed from the cache
        /// </summary>
        /// <param name="iBMC">The broker managed component that is no longer required</param>
        public void ReleaseBrokerManagedComponent(IBrokerManagedComponent iBMC)
        {
            this.CheckTransactionRunning();

            if (iBMC == null)
                return;

            this.Transaction.ReleaseBMC(iBMC.CID);
        }

        private void CheckTransactionRunning()
        {
            if (!this.InTransaction)
                throw new TransactionNotRunningException("Broker used without a running transaction, you must start a transaction by using SetStart before interacting with the broker");
        }

        /// <summary>
        /// Gets a temporary instance of a CMType that cannot be persisted
        /// </summary>
        /// <param name="typeId">The typeID of the CM to create</param>
        /// <param name="cmName">The name to give the CM when it is created</param>
        /// <returns>The transient instance of the CM</returns>
        public ICalculationModule GetCMInstance(Guid typeId, string cmName)
        {
            if (this.InTransaction)
            {
                ICalculationModule iCM = (ICalculationModule)CMCacheManager.CacheManager.CreateTransientComponentInstance(typeId, this.Transaction);

                if (null != iCM)
                    iCM.Initialize(cmName);

                return iCM;
            }
            else
                throw new TransactionNotRunningException("GetCMInstance called without a running transaction, transaction needs to be started before calling this method");
        }

        /// <summary>
        /// Gets a temporary instance of a CMType that cannot be persisted
        /// </summary>
        /// <param name="typeName">The type name of the CM to create</param>
        /// <param name="cmName">The name given to the CM when it is created</param>
        /// <returns>The CM that has been created</returns>
        public ICalculationModule GetCMInstance(string typeName, string cmName)
        {
            if (this.InTransaction)
            {
                ICalculationModule iCM = (ICalculationModule)CMCacheManager.CacheManager.CreateTransientComponentInstance(typeName, this.Transaction);

                if (null != iCM)
                    iCM.Initialize(cmName);

                return iCM;
            }
            else
                throw new TransactionNotRunningException("GetCMInstance called without a running transaction, transaction needs to be started before calling this method");
        }

        public Guid GetCMInstanceID(Guid objCLID, Guid objCSID)
        {
            ICalculationModule objCM = this.GetCMImplementation(objCLID, objCSID);

            try
            {
                if (objCM != null)
                    return objCM.CIID;
                else
                    return Guid.Empty;
            }
            finally
            {
                this.ReleaseBrokerManagedComponent(objCM);
            }
        }

        public ICalculationModule GetCMInstance(Guid cIID, Guid cSID)
        {
            if (InTransaction)
                return CMCacheManager.CacheManager.GetCMInstance(cIID, cSID, this.Transaction);
            else
                throw new TransactionNotRunningException("GetCMInstance called without a running transaction, transaction needs to be started before calling this method");
        }

        public ILogicalModule GetLogicalCM(Guid cLID)
        {
            if (InTransaction)
                return CMCacheManager.CacheManager.GetLogicalCM(cLID, this.Transaction);
            else
                throw new TransactionNotRunningException("GetLogicalCM called without a running transaction, transaction needs to be started before calling this method");
        }

        public ICalculationModule GetCMImplementation(Guid cLID, Guid cSID)
        {
            if (InTransaction)
            {
                ILogicalModule lCM = CMCacheManager.CacheManager.GetLogicalCM(cLID, this.Transaction);

                try
                {
                    if (lCM != null)
                    {
                        if (lCM[cSID] != null)
                            return lCM[cSID];
                        else
                        {
                            foreach (DictionaryEntry dictionaryEntry in lCM.Scenarios)
                            {
                                CMScenario cMScenario = (CMScenario)dictionaryEntry.Value;
                                if (cMScenario.CSID == cSID && cMScenario.CLID == cLID)
                                    return this.GetBMCInstance(cMScenario.CIID) as ICalculationModule;
                            }
                            return null;
                        }
                    }
                    else
                        return null;
                }
                finally
                {
                    this.ReleaseBrokerManagedComponent(lCM);
                }
            }
            else
                throw new TransactionNotRunningException("GetCMImplementation called without a running transaction, transaction needs to be started before calling this method");
        }

        public IBrokerManagedComponent GetCMImplementation(string typeName, string cmName)
        {
            if (this.InTransaction)
            {
                IBrokerManagedComponent iCM = CMCacheManager.CacheManager.CreateTransientComponentInstance(typeName, this.Transaction);

                if (null != iCM)
                    iCM.Initialize(cmName);

                return iCM;
            }
            else
                throw new TransactionNotRunningException("GetCMInstance called without a running transaction, transaction needs to be started before calling this method");
            //if (InTransaction)
            //{
            //    ILogicalModule lCM = CMCacheManager.CacheManager.GetLogicalCM(cLID, this.Transaction);

            //    try
            //    {
            //        if (lCM != null)
            //            return lCM[cSID];
            //        else
            //            return null;
            //    }
            //    finally
            //    {
            //        this.ReleaseBrokerManagedComponent(lCM);
            //    }
            //}
            //else
            //    throw new TransactionNotRunningException("GetCMImplementation called without a running transaction, transaction needs to be started before calling this method");
        }

        public ILogicalModule CreateLogicalCM(Guid typeId, string cmName, BusinessStructureContext objContext)
        {
            if (InTransaction)
                return CMCacheManager.CacheManager.CreateLogicalCM(typeId, cmName, Guid.NewGuid(), this.Transaction, objContext);
            else
                throw new TransactionNotRunningException("CreateLogicalCM called without a running transaction, transaction needs to be started before calling this method");
        }

        public ILogicalModule CreateLogicalCM(Guid typeId, string cmName, Guid initCSID, BusinessStructureContext objContext)
        {
            if (InTransaction)
                return CMCacheManager.CacheManager.CreateLogicalCM(typeId, cmName, initCSID, this.Transaction, objContext);
            else
                throw new TransactionNotRunningException("CreateLogicalCM called without a running transaction, transaction needs to be started before calling this method");
        }

        /// <summary>
        /// Creates a new logical CM with a default scenario of CSID initCSID.  If initCSID is null, no scenario is created.
        /// </summary>
        /// <param name="typeName">The type name of the logical CM to create</param>
        /// <param name="cmName">The name the CM is created with</param>
        /// <param name="initCSID">The initial scenario, if null, no scenario is created</param>
        /// <param name="objContext">The business structure context the CM is created with, e.g. entity, single period</param>
        /// <returns>The logical module that has been created</returns>
        public ILogicalModule CreateLogicalCM(string typeName, string cmName, Guid initCSID, BusinessStructureContext objContext)
        {
            if (InTransaction)
            {
                ILicence licence = (ILicence)GetWellKnownBMC(WellKnownCM.Licencing);

                if (licence != null)
                    licence.IsLicenced(typeName);

                this.ReleaseBrokerManagedComponent(licence);

                return CMCacheManager.CacheManager.CreateLogicalCM(typeName, cmName, initCSID, this.Transaction, objContext);
            }
            else
                throw new TransactionNotRunningException("CreateLogicalCM called without a running transaction, transaction needs to be started before calling this method");
        }

        /// <summary>
        /// Deletes a Logical CM specified by its CLID
        /// </summary>
        /// <param name="cLID">The CLID of the CM to delete</param>
        public void DeleteLogicalCM(Guid cLID)
        {
            if (InTransaction)
            {
                ILogicalModule lCM = this.GetLogicalCM(cLID);

                //foreach(Guid childCLID in lCM.ChildCLIDs)
                //    DeleteLogicalCM(childCLID);

                // Recursively delete all the children of the logical CM
                for (int i = lCM.ChildCLIDs.Count - 1; i >= 0; i--)
                {
                    Guid childCLID = (Guid)lCM.ChildCLIDs[i];
                    ILogicalModule childlCM = this.GetLogicalCM(childCLID);
                    lCM.RemoveChildLogicalCM(childlCM);
                    //DeleteLogicalCM(childCLID);
                    //lCM.RemoveChildLogicalCM.ChildCLIDs.RemoveAt(i);
                }

                // Delete all of the implementation CMs 
                foreach (DictionaryEntry dictionaryEntry in lCM.Scenarios)
                {
                    CMScenario cMScenario = (CMScenario)dictionaryEntry.Value;
                    CMCacheManager.CacheManager.DeleteCMInstance(lCM.CLID, cMScenario.CSID, this.Transaction);
                    cMScenario.CIID = Guid.Empty;
                }

                // Delete the logical CM itself
                CMCacheManager.CacheManager.DeleteCMInstance(cLID, this.Transaction);

                this.ReleaseBrokerManagedComponent(lCM);
            }
            else
                throw new TransactionNotRunningException("DeleteLogicalCM called without a running transaction, transaction needs to be started before calling this method");
        }


        public IBrokerManagedComponent GetWellKnownBMC(WellKnownCM wellKnownCM)
        {
            if (InTransaction)
                return CMCacheManager.CacheManager.GetWellKnownBMC(wellKnownCM, this.Transaction);
            else
                throw new TransactionNotRunningException("GetWellKnownBMC called without a running transaction, transaction needs to be started before calling this method");
        }

        /// <summary>
        /// Gets a system wide well known CM, e.g. the top level Organisation
        /// </summary>
        /// <param name="wellKnownCM">The well known CM to be retreived</param>
        /// <returns>The well known CM requested</returns>
        public ICalculationModule GetWellKnownCM(WellKnownCM wellKnownCM)
        {
            if (InTransaction)
            {
                ICalculationModule cM = (ICalculationModule)this.GetWellKnownBMC(wellKnownCM);

                if (cM != null)
                {
                    ILogicalModule iLM = this.GetLogicalCM(cM.CLID);
                    foreach (DictionaryEntry dictEntry in iLM.Scenarios)
                    {
                        CMScenario cMScenario = (CMScenario)dictEntry.Value;
                        if (cMScenario.CIID == cM.CIID)
                        {
                            cM.CSID = cMScenario.CSID;
                            break;
                        }
                    }

                    this.ReleaseBrokerManagedComponent(iLM);

                    if (cM.CSID == Guid.Empty)
                        throw new Exception("CMTransactionBroker->GetWellKnownCM, CSID is empty guid.");

                    return cM;
                }
                else
                    return null;
            }
            else
                throw new TransactionNotRunningException("GetWellKnownCM called without a running transaction, transaction needs to be started before calling this method");
        }

        //===============================================================================================
        // CM Catalog Functions

        // Note:  Functions that return a list of BMC instances require a transaction to exist and 
        // that no change has been made to the cache within the transaction, prior to the calling of the list fucntion.

        public IEnumerable GetBMCList()
        {
            if (InTransaction)
            {
                using (CMListPersist cMListPersist = new CMListPersist(this.Transaction.SQLConnection))
                {
                    if (this.Transaction.SQLTransaction != null)
                        cMListPersist.Transaction = this.Transaction.SQLTransaction;

                    return CMCacheManager.CacheManager.ListAllBMCs(cMListPersist, this.Transaction);
                }
            }
            else
                throw new TransactionNotRunningException("GetBMCList called without a running transaction, transaction needs to be started before calling this method");
        }

        public IEnumerable GetBMCList(Guid typeId)
        {
            if (InTransaction)
            {
                using (CMListPersist cMListPersist = new CMListPersist(this.Transaction.SQLConnection))
                {
                    if (this.Transaction.SQLTransaction != null)
                        cMListPersist.Transaction = this.Transaction.SQLTransaction;
                    return CMCacheManager.CacheManager.ListBMCsOfTypeID(cMListPersist, typeId, this.Transaction);
                }
            }
            else
                throw new TransactionNotRunningException("GetBMCList called without a running transaction, transaction needs to be started before calling this method");
        }

        public IEnumerable GetBMCList(string typeName)
        {
            if (InTransaction)
            {
                using (CMListPersist cMListPersist = new CMListPersist(this.Transaction.SQLConnection))
                {
                    if (this.Transaction.SQLTransaction != null)
                        cMListPersist.Transaction = this.Transaction.SQLTransaction;

                    return CMCacheManager.CacheManager.ListBMCsOfTypeName(cMListPersist, typeName, this.Transaction);
                }
            }
            else
                throw new TransactionNotRunningException("GetBMCList called without a running transaction, transaction needs to be started before calling this method");
        }

        public DataSet GetBMCDetailList(string typeName)
        {
            return GetBMCDetailList(typeName, null);
        }

        public DataSet GetBMCDetailList(string typeName, object[] parameters)
        {
            if (InTransaction)
                return CMCacheManager.CacheManager.ListBMCDetailsOfTypeName(typeName, parameters, this.Transaction);
            else
                throw new TransactionNotRunningException("GetBMCDetailList called without a running transaction, transaction needs to be started before calling this method");
        }


        public DataTable GetLCMsByParent(Guid parentCLID)
        {
            if (InTransaction)
            {
                using (LCMListPersist lCMListPersist = new LCMListPersist(this.Transaction.SQLConnection))
                {
                    if (this.Transaction.SQLTransaction != null)
                        lCMListPersist.Transaction = this.Transaction.SQLTransaction;
                    return CMCacheManager.CacheManager.ListLCMsByParent(lCMListPersist, parentCLID, this.Transaction);
                }
            }
            else
                throw new TransactionNotRunningException("GetLCMsByParent called without a running transaction, transaction needs to be started before calling this method");
        }

        public DataTable GetLCMsByParent(Guid parentCLID, Guid parentCSID)
        {
            if (InTransaction)
            {
                using (LCMListPersist lCMListPersist = new LCMListPersist(this.Transaction.SQLConnection))
                {
                    if (this.Transaction.SQLTransaction != null)
                        lCMListPersist.Transaction = this.Transaction.SQLTransaction;
                    return CMCacheManager.CacheManager.ListLCMsByParent(lCMListPersist, parentCLID, parentCSID, this.Transaction);
                }
            }
            else
                throw new TransactionNotRunningException("GetLCMsByParent called without a running transaction, transaction needs to be started before calling this method");
        }

        public IEnumerable GetConsolidationParentsList(Guid memberCLID)
        {
            if (InTransaction)
            {
                using (CMListPersist cMListPersist = new CMListPersist(this.Transaction.SQLConnection))
                {
                    if (this.Transaction.SQLTransaction != null)
                        cMListPersist.Transaction = this.Transaction.SQLTransaction;
                    return CMCacheManager.CacheManager.ListConsolidationParents(cMListPersist, memberCLID, this.Transaction);
                }
            }
            else
                throw new TransactionNotRunningException("GetConsolidationParentsList called without a running transaction, transaction needs to be started before calling this method");
        }

        public IEnumerable GetConsolidationParentsList(Guid memberCLID, Guid memberCSID)
        {
            if (InTransaction)
            {
                using (CMListPersist cMListPersist = new CMListPersist(this.Transaction.SQLConnection))
                {
                    if (this.Transaction.SQLTransaction != null)
                        cMListPersist.Transaction = this.Transaction.SQLTransaction;
                    return CMCacheManager.CacheManager.ListConsolidationParents(cMListPersist, memberCLID, memberCSID, this.Transaction);
                }
            }
            else
                throw new TransactionNotRunningException("GetConsolidationParentsList called without a running transaction, transaction needs to be started before calling this method");
        }

        public ReadOnlyCollection<ICalculationModule> GetGroupsThatHaveOrganisationUnitAsMember(ICalculationModule organisationUnit)
        {
            this.CheckTransationIsRunning();

            IEnumerable consolidationParents = this.GetConsolidationParentsList(organisationUnit.CLID, organisationUnit.CSID);

            List<ICalculationModule> parentGroups = new List<ICalculationModule>();

            foreach (DataRow row in consolidationParents)
            {
                Guid cid = (Guid)row[DSBMCInfo.CMID_FIELD];
                Guid csid = (Guid)row[DSBMCInfo.CMPARENTCSID_FIELD];

                ICalculationModule groupOrganisationUnit = (ICalculationModule)this.GetBMCInstance(cid);
                groupOrganisationUnit.CSID = csid;

                parentGroups.Add(groupOrganisationUnit);
            }

            return new ReadOnlyCollection<ICalculationModule>(parentGroups);
        }

        public IEnumerable GetConsolidationMembersList(Guid parentCLID, Guid parentCSID)
        {
            if (InTransaction)
            {
                using (CMListPersist cMListPersist = new CMListPersist(this.Transaction.SQLConnection))
                {
                    if (this.Transaction.SQLTransaction != null)
                        cMListPersist.Transaction = this.Transaction.SQLTransaction;
                    return CMCacheManager.CacheManager.ListConsolidationMembers(cMListPersist, parentCLID, parentCSID, this.Transaction);
                }
            }
            else
                throw new TransactionNotRunningException("GetConsolidationMembersList called without a running transaction, transaction needs to be started before calling this method");
        }

        //===============================================================================================
        // CM Type Management Functions

        public IEnumerable GetCMTypeList()
        {
            return CMCacheManager.CacheManager.ListAllCMTypes(this.Transaction);
        }

        public IEnumerable GetCMTypeList(string categoryName)
        {
            return CMCacheManager.CacheManager.ListCMTypes(categoryName, this.Transaction);
        }

        public IEnumerable GetCMTypeList(string[] categoryNames)
        {
            return CMCacheManager.CacheManager.ListCMTypes(categoryNames, this.Transaction);
        }

        /// <summary>
        /// Gets a list of the CM types for a CM type name
        /// </summary>
        /// <param name="strTypeName">The type name to return the details for</param>
        /// <returns>The CM types that match the type name</returns>
        public IEnumerable GetCMTypes(string strTypeName)
        {
            return CMCacheManager.CacheManager.ListCMTypesByTypeName(strTypeName, this.Transaction);
        }

        /// <summary>
        /// Determines if a particular CM is installed in the system
        /// </summary>
        /// <param name="strTypeName">The type name to check for</param>
        /// <returns>True if the CM is found, false otherwise</returns>
        public bool IsCMInstalled(string strTypeName)
        {
            if (this.InTransaction)
            {
                IEnumerable objCMTypes = this.GetCMTypes(strTypeName);

                IEnumerator objEn = objCMTypes.GetEnumerator();

                while (objEn.MoveNext())
                {
                    if (objEn.Current != null)
                        return true;
                }

                // if we reach this point and haven't returned, then the CM wasn't found
                return false;
            }
            else
                throw new TransactionNotRunningException("IsCMInstalled called without a running transaction, transaction needs to be started before calling this method");
        }

        /// <summary>
        /// Gets a list of the CM types for a CM type id
        /// </summary>
        /// <param name="objTypeID">The type id to return details for</param>
        /// <returns>The CMs that match the given type id</returns>
        public IEnumerable GetCMTypes(Guid objTypeID)
        {
            return CMCacheManager.CacheManager.ListCMTypesByTypeID(objTypeID, this.Transaction);
        }

        public ICMType CreateCMType(CMTypeInfo cMTypeDetails)
        {
            ICMType iCT = null;

            if (InTransaction)
            {
                iCT = CMCacheManager.CacheManager.CreateCMType(cMTypeDetails, this.Transaction);
                if (null != iCT)
                    iCT.Owner = (ICMBroker)this;

                // Create the entry in the BMC Types table and the persistence structure
                IComponentManagement componentDictionary = this.GetComponentDictionary();
                bool newComponent = false;
                if (!componentDictionary.ContainsComponent(cMTypeDetails.ID))
                {
                    ComponentDictionary.Component component = new ComponentDictionary.Component(cMTypeDetails.ID, cMTypeDetails.TypeName, cMTypeDetails.DisplayName, cMTypeDetails.Category, (CMApplicability)cMTypeDetails.Applicability, cMTypeDetails.Obsolete);
                    componentDictionary.Add(component);
                    newComponent = true;
                }

                ComponentDictionary.AssemblyVersionNumber assemblyVersionNumber = new ComponentDictionary.AssemblyVersionNumber(1, 1, 0, 0);
                ComponentDictionary.ComponentVersion componentVersion = new ComponentDictionary.ComponentVersion(
                    cMTypeDetails.ID,							// Component Version ID
                    assemblyVersionNumber,						// Assembly Version Number
                    cMTypeDetails.TypeName,						// Component name,
                    cMTypeDetails.DisplayName,					// Component display name,
                    cMTypeDetails.AssemblyName,					// strongName,
                    cMTypeDetails.TypeName,						// versionName,
                    cMTypeDetails.ID,							// componentID,
                    DateTime.Now,								// startDate,
                    DateTime.Now,								// endDate,
                    cMTypeDetails.pAssembly,					// persisterAssemblyStrongName,
                    cMTypeDetails.pClass,						// persisterClass,
                    cMTypeDetails.ClassName,						// implementationClass
                    cMTypeDetails.Obsolete						// obsolete
                );

                componentDictionary.Add(componentVersion);
                if (newComponent)
                    this.LogEvent(EventType.ImportCalculationModule,Guid.Empty, "New calculation module '" + cMTypeDetails.DisplayName + "' with version '" + assemblyVersionNumber + "' Imported");
            }
            else
                throw new TransactionNotRunningException("CreateCMType called without a running transaction, transaction needs to be started before calling this method");


            return (ICMType)null;
        }

        public void DeleteAllTypes()
        {
            if (InTransaction)
                CMCacheManager.CacheManager.DeleteAllTypes(this.Transaction);
            else
                throw new TransactionNotRunningException("DeleteAllTypes called without a running transaction, transaction needs to be started before calling this method");
        }

        //===============================================================================================
        // CM Deletion Functions
        public void DeleteCMInstance(Guid cIID)
        {
            if (this.InTransaction)
            {
                CMCacheManager.CacheManager.DeleteCMInstance(cIID, this.Transaction);
            }
            else
                throw new TransactionNotRunningException("DeleteCMInstance called without a running transaction, transaction needs to be started before calling this method");
        }

        public void DeleteCMInstanceImmediate(Guid cid)
        {
            if (this.InTransaction)
            {
                CMCacheManager.CacheManager.DeleteCMInstanceImmediate(cid, this.Transaction);
            }
            else
                throw new TransactionNotRunningException("DeleteCMInstanceImmediate called without a running transaction, transaction needs to be started before calling this method");
        }

        //===============================================================================================
        // Cache Management Functions

        /// <summary>
        /// Returns the number of BMC's currently present in the cache
        /// </summary>
        public int CacheSize()
        {
            if (InTransaction)
                return CMCacheManager.CacheManager.Size();
            else
                throw new TransactionNotRunningException("CacheSize called without a running transaction, transaction needs to be started before calling this method");
        }

        /// <summary>
        /// Removes all of the entries from the cache.  Any un-committed changes are lost.
        /// Note - this is NOT the same as deleting the entries in the cache.
        /// </summary>
        public void ClearCache()
        {
            if (this.InTransaction)
            {
                CMCacheManager.CacheManager.Clear(this.Transaction);
            }
            else
                throw new TransactionNotRunningException("ClearCache called without a running transaction, transaction needs to be started before calling this method");
        }

        /// <summary>
        /// Removes a specific entry from the cache.  Any un-committed changes are lost.
        /// Note - this is NOT the same as deleting the entry in the cache.
        /// </summary>
        public void ClearCache(Guid cID)
        {
            if (this.InTransaction)
            {
                CMCacheManager.CacheManager.Clear(this.Transaction, cID);
            }
            else
                throw new TransactionNotRunningException("ClearCache called without a running transaction, transaction needs to be started before calling this method");
        }

        /// <summary>
        /// Returns true if the BMC identified is in the cache
        /// </summary>
        public bool IsInCache(Guid cID)
        {
            if (InTransaction)
                return CMCacheManager.CacheManager.IsInCache(this.Transaction, cID);
            else
                throw new TransactionNotRunningException("IsInCache called without a running transaction, transaction needs to be started before calling this method");
        }

        //===============================================================================================
        // Utility Functions

        public Guid CIIDToCLID(Guid cIID)
        {
            if (this.InTransaction)
                return CMCacheManager.CacheManager.CIIDToCLID(cIID, this.Transaction);
            else
                throw new TransactionNotRunningException("CIIDToCLID called without a running transaction, transaction needs to be started before calling this method");
        }

        public void ExportXMLToFile(Guid cIID, string fileName)
        {
            IBrokerManagedComponent iBMC = GetBMCInstance(cIID);
            iBMC.ExportXMLToFile(fileName);
        }

        public string ExtractXML(Guid cIID)
        {
            IBrokerManagedComponent iBMC = GetBMCInstance(cIID);
            return iBMC.ExtractXML();
        }

        public string ExtractXML(Guid cLID, Guid cSID)
        {
            IBrokerManagedComponent iBMC = (IBrokerManagedComponent)this.GetCMImplementation(cLID, cSID);
            return iBMC.ExtractXML();
        }

        public string DeliverXML(Guid cIID, string xml, bool verify)
        {
            if (xml == "NULL") //the cm has signified that it can't migrate
                return "NULL";

            IMigrationDataMemento memento = (IMigrationDataMemento)new MigrationDataMemento();
            XmlDocument doc = new XmlDocument();
            doc.Load(new StringReader(xml));
            XmlNode root = doc.DocumentElement;
            XmlNode instNode = root["CINSTANCES"];
            if (instNode == null)
                instNode = root["CINSTANCES"];
            string typeIDStr = instNode["CTID"].InnerText;
            string bmcName = instNode["NAME"].InnerText;

            IBrokerManagedComponent iBMC = CreateComponentInstance(cIID, new Guid(typeIDStr), bmcName);
            return iBMC.DeliverXML(xml, verify, memento);
        }

        public void DeliverXMLFromFile(Guid cIID, string fileName)
        {
            IMigrationDataMemento memento = (IMigrationDataMemento)new MigrationDataMemento();
            XmlTextReader reader = new XmlTextReader("file://" + fileName);
            string typeIDStr = "";
            string bmcName = "";

            while (reader.Read())
            {
                if (reader.Name == "CINSTANCES")
                {
                    bool foundCTID = false;
                    bool foundName = false;

                    while (reader.Read())
                    {
                        if (reader.Name == "CTID")
                        {
                            typeIDStr = reader.ReadInnerXml();
                            foundCTID = true;
                        }
                        if (reader.Name == "NAME")
                        {
                            bmcName = reader.ReadInnerXml();
                            foundName = true;
                        }
                        if (foundCTID && foundName)
                        {
                            break;
                        }
                    }
                    break;
                }
            }

            IBrokerManagedComponent iBMC = CreateComponentInstance(cIID, new Guid(typeIDStr), bmcName);
            iBMC.DeliverXMLFromFile(fileName, memento);
        }

        /// <summary>
        /// Creates a new BMC of specified type and instance ID, then populates it from the DataSet
        /// information contained in the XML string.
        /// </summary>
        /// <param name="cID">Required instance ID.</param>
        /// <param name="cTID">Component Version Type Guid.</param>
        /// <param name="xml">XML String.</param>
        /// <param name="cmName"></param>
        /// <returns></returns>
        public IBrokerManagedComponent CreateFromXML(Guid cID, Guid cTID, string xml, string cmName)
        {
            IMigrationDataMemento memento = (IMigrationDataMemento)new MigrationDataMemento();
            IBrokerManagedComponent iBMC = CreateComponentInstance(cID, cTID, cmName);

            iBMC.MigrationInProgress = true;

            if (iBMC is ICalculationModule)
            {
                ICalculationModule CM = (ICalculationModule)iBMC;

                // Set the CSID to empty GUID, this will be set later 
                // by the delivery in the base class of the CM
                CM.CSID = Guid.Empty;

                CM.InitialiseBusinessStructureContext();
            }

            iBMC.DeliverXML(xml, false, memento);

            iBMC.MigrationInProgress = false;
            if (iBMC is ICalculationModule)
                iBMC.Resubscribe();
            iBMC.MigrationCompleted(memento);
            return iBMC;
        }

        public bool IsBMCCustomSerialized(Guid cIID)
        {
            IBrokerManagedComponent iBMC = GetBMCInstance(cIID);
            return iBMC.IsCustomSerialized();
        }

        public void MigrationCompleted(Guid cIID)
        {
            IBrokerManagedComponent iBMC = GetBMCInstance(cIID);

            if (iBMC == null)
                throw new Exception("The Broker Managed Component instance for the given ID was not found");

            if (iBMC is ICalculationModule)
                this.CalculationModuleMigrationCompleted((ICalculationModule)iBMC);
            else
                iBMC.MigrationCompleted();
        }

        private void CalculationModuleMigrationCompleted(ICalculationModule calculationModule)
        {
            Guid calculationModuleCID = calculationModule.CMID;

            ILogicalModule cmLogicalModule = this.GetLogicalCM(calculationModule.CLID);

            foreach (DictionaryEntry cmScenarioDE in cmLogicalModule.Scenarios)
            {
                CMScenario cmScenario = (CMScenario)cmScenarioDE.Value;
                ICalculationModule scenarioedCalculationModule = this.GetCMImplementation(cmScenario.CLID, cmScenario.CSID);

                scenarioedCalculationModule.MigrationCompleted();
            }
        }

        public void Resubscribe(Guid cIID)
        {
            IBrokerManagedComponent iBMC = GetBMCInstance(cIID);
            if (iBMC != null)
            {
                iBMC.Resubscribe();
            }
            else
                throw new Exception("The Broker Managed Component instance for the given ID was not found");
        }

        public bool OrganisationNameAlreadyExists(string organisationName)
        {
            bool organisationAlreadyExists = false;
            foreach (object row in this.GetBMCList("Organization"))
            {
                if (organisationAlreadyExists)
                    continue;
                DataRow bmcDataRow = (DataRow)row;
                IOrganization organisation = (IOrganization)this.GetBMCInstance((Guid)bmcDataRow[DSCalculationModulesInfo.CMID_FIELD]);
                if (organisation.OrganisationName == organisationName)
                    organisationAlreadyExists = true;
                this.ReleaseBrokerManagedComponent(organisation);
            }
            return organisationAlreadyExists;
        }

        #region Workpaper Version Information

        public string GetVersionedWorkpaperFromAssemblyID(string strWorkpaper, Guid assemblyID)
        {
            AssemblyDictionary dict = (AssemblyDictionary)GetAssemblyFullDictionary();
            IAssembly assembly = dict[assemblyID];
            string version = Global.GetUnderscoreVersionString(assembly.StrongName);
            return VersionUtilities.GetVersionedWorkpaper(strWorkpaper, version);
        }

        public string GetVersionedWorkpaper(string strWorkpaper, string strBMCTypeName)
        {
            if (!this.InTransaction)
                throw new TransactionNotRunningException("GetVersionedWorkpaper called without a running transaction, transaction needs to be started before calling this method");

            AssemblyDictionary.VersionedTypeName versionedTypeName = new AssemblyDictionary.VersionedTypeName(strBMCTypeName);

            if (versionedTypeName.IsVersioned())
            {
                return GetVersionedWorkpaper(strWorkpaper, strBMCTypeName, VersionRequest.LatestVersion);
            }
            else
            {
                IComponentManagement componentDictionary = this.GetComponentDictionary();

                IAssemblyVersionSet componentVersions = componentDictionary.GetComponentVersions(strBMCTypeName);

                IAssembly latestVersion = componentVersions.Latest;

                AssemblyDictionary.AssemblyVersionNumber latestVersionNumber = (AssemblyDictionary.AssemblyVersionNumber)latestVersion.VersionNumber;

                string strVersionInformation = latestVersionNumber.MajorVersion.ToString() + "_" + latestVersionNumber.MinorVersion.ToString();
                return VersionUtilities.GetVersionedWorkpaper(strWorkpaper, strVersionInformation);
            }
        }

        /// <summary>
        /// Gets the workpaper for a given workpaper name, for the version requested
        /// </summary>
        /// <param name="strWorkpaper"></param>
        /// <param name="strBMCTypeName"></param>
        /// <param name="objVersionRequest"></param>
        /// <returns></returns>
        public string GetVersionedWorkpaper(string strWorkpaper, string strBMCTypeName, VersionRequest objVersionRequest)
        {
            if (!this.InTransaction)
                throw new TransactionNotRunningException("GetVersionedWorkpaper called without a running transaction, transaction needs to be started before calling this method");

            IBrokerManagedComponent objBMC = this.CreateTransientBMCInstance(strBMCTypeName, String.Empty);

            if (objBMC == null)
                return string.Empty;

            return this.GetVersionedWorkpaper(strWorkpaper, objBMC);
        }

        public string GetVersionedWorkpaper(string strWorkpaper, Guid objCIID)
        {
            if (!this.InTransaction)
                throw new TransactionNotRunningException("GetVersionedWorkpaper called without a running transaction, transaction needs to be started before calling this method");

            if (objCIID == Guid.Empty)
                throw new Exception("CMTransactionBroker -> GetVersionedWorkpaper, called without a valid CIID");

            IBrokerManagedComponent objBMC = this.GetBMCInstance(objCIID);

            if (objBMC == null)
                return string.Empty;

            try
            {
                return this.GetVersionedWorkpaper(strWorkpaper, objBMC);
            }
            finally
            {
                this.ReleaseBrokerManagedComponent(objBMC);
            }
        }

        public string GetVersionedWorkpaper(string strWorkpaper, Guid objCLID, Guid objCSID)
        {
            if (!this.InTransaction)
                throw new TransactionNotRunningException("GetVersionedWorkpaper called without a running transaction, transaction needs to be started before calling this method");

            IBrokerManagedComponent objBMC = (IBrokerManagedComponent)this.GetCMImplementation(objCLID, objCSID);
            if (objBMC == null)
                return string.Empty;

            try
            {
                return this.GetVersionedWorkpaper(strWorkpaper, objBMC);
            }
            finally
            {
                this.ReleaseBrokerManagedComponent(objBMC);
            }
        }

        public string GetVersionedWorkpaper(string strWorkpaper, IBrokerManagedComponent objBMC)
        {
            if (this.MigrationInProgress)
                return String.Empty;
            if (!this.InTransaction)
                throw new TransactionNotRunningException("GetVersionedWorkpaper called without a running transaction, transaction needs to be started before calling this method");

            if (objBMC == null)
                throw new Exception("CMTransactionBroker -> GetVersionedWorkpaper; the component you are requesting information for is null");

            string strVersionInformation = objBMC.VersionString;
            return VersionUtilities.GetVersionedWorkpaper(strWorkpaper, strVersionInformation);
        }
        #endregion


        #region IDisposable Implementation ---------------------------------------

        /// <summary>
        ///		Performs application-defined tasks associated with freeing, releasing, 
        ///		or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///		Performs application-defined tasks associated with freeing, releasing, 
        ///		or resetting unmanaged resources.
        /// </summary>
        /// <param name="disposing">
        ///		True if called by the Dispose() method, false if called by the finalizer.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (transactions[clientID].SQLConnection != null)
                        transactions[clientID].SQLConnection.Close();

                    transactions.Remove(clientID);
                }
                disposed = true;
            }
        }

        public void UpdateCacheEntry(IBrokerManagedComponent bmc)
        {
            CMCacheManager.CacheManager.UpdateCacheEntry(bmc);
        }
        #endregion // IDisposable Implementation
    }
}
