using System;
using System.Data;

namespace Oritax.TaxSimp.Services.CMBroker
{
	/// <summary>
	/// Represents the persistent state of the event logs
	/// </summary>
	[Serializable] 
	public class DSEventsList : DataSet
	{
		public const string EVENTLIST_TABLE				= "EVENTLIST_TABLE";
		public const string EVENT_TYPE_FIELD			= "TYPE";
		public const string EVENT_NAME_FIELD			= "NAME";
		public const string EVENT_USERNAME_FIELD		= "USERNAME";
		public const string EVENT_DATETIME_FIELD		= "DATETIME";
		public const string EVENT_DETAIL_FIELD			= "DETAIL";
		public const string EVENT_ID_FIELD				= "EVENTID";
        public const string EVENT_CID_FIELD              = "EVENTCID";

		public DSEventsList()
		{
			DataTable table;
			DataColumnCollection columns;

			// Add the table for the instance data
			table = new DataTable(EVENTLIST_TABLE);
			columns = table.Columns;
			columns.Add(EVENT_ID_FIELD, typeof(System.Guid));
            columns.Add(EVENT_CID_FIELD, typeof(System.Guid));
			columns.Add(EVENT_TYPE_FIELD, typeof(System.String));
			columns.Add(EVENT_NAME_FIELD, typeof(System.String));
			columns.Add(EVENT_USERNAME_FIELD, typeof(System.String));
			columns.Add(EVENT_DATETIME_FIELD, typeof(System.DateTime));
			columns.Add(EVENT_DETAIL_FIELD, typeof(System.String));
			DataColumn[] lcPrimaryKey={	table.Columns[EVENT_ID_FIELD]};
			table.PrimaryKey=lcPrimaryKey;

			this.Tables.Add(table);
		}
	}
}
