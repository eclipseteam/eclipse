using System;
using System.Collections;
using System.Collections.Generic;

namespace Oritax.TaxSimp.Services.CMBroker
{
	internal class CMCache : Hashtable
	{
		public const int MaxSize=1000;
		public const int MinSize=800;

		private void Add(Guid ciid, CMCacheEntry cacheItem)
		{
			base.Add(ciid, cacheItem);
		}

		public virtual CMCacheEntry this[Guid cIID]
		{
			get
			{
				return (CMCacheEntry)base[cIID];
			}
			set
			{
				base[cIID]=value;
			}
		}

        public virtual void Remove(Guid cIID)
		{
			CMCacheEntry cacheItem = this[cIID];

			if( cacheItem != null )
				base.Remove(cIID);
		}

        public virtual void RemoveAll()
		{
			base.Clear();
		}

        public virtual ArrayList GetCollectionList()
		{
			int initialSize = base.Count;
			
			if(base.Count < 0)
				initialSize = 0;

			ArrayList collectionList = new ArrayList(initialSize);
			
			foreach(CMCacheEntry cacheEntry in base.Values)
				collectionList.Add(cacheEntry);

			collectionList.Sort();
			return collectionList;
		}

    }
}
