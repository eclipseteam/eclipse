using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;

using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Services.CMBroker
{
    /// <summary>
    /// Represents an N-ABLE transaction
    /// </summary>
    public class TransactionWithCache : Transaction
    {
        #region Stateful variables
        [NonSerialized] internal Hashtable components;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructs a new transaction
        /// </summary>
        /// <param name="broker">The broker for this transaction</param>
        /// <param name="transactionID">The transaction ID for this transaction</param>
        /// <param name="connectionString">The database connection string</param>
        /// <exception cref="System.ArgumentNullException">broker cannot be null</exception>
        /// <exception cref="System.ArgumentException">transactionID cannot be an empty guid</exception>
        /// <exception cref="System.ArgumentNullException">connectionString cannot be null or an empty string</exception>
        public TransactionWithCache(ICMBroker broker, Guid transactionID, string connectionString)
            : base(broker, transactionID, connectionString)
        {
            this.components = new Hashtable();
        }
        #endregion


        #region Methods
        /// <summary>
        /// Enlists a BMC in the transaction
        /// </summary>
        /// <param name="cid">The CID of the BMC to enlist</param>
        /// <param name="initialRefCount">The reference count</param>
        /// <exception cref="System.ArgumentException">cid cannot be an empty guid</exception>
        public override void EnlistBMC(IBrokerManagedComponent bmc, int initialRefCount)
        {
            EnlistBMC(bmc.CID, initialRefCount);
            SaveComponent(bmc);
        }
        /// <summary>
        /// Add component to the list of existing components, or update list of existing components
        /// </summary>
        /// <param name="bmc"></param>
        public void SaveComponent(IBrokerManagedComponent bmc)
        {
            if (bmc == null)
                throw new ArgumentException("Parameter cannot be null", "bmc");

            if (!components.Contains(bmc.CID))
                components.Add(bmc.CID, bmc);
            else
                components[bmc.CID] = bmc;
        }
        /// <summary>
        /// Get BrokerManagedComponent from the list of components within the transaction
        /// </summary>
        /// <param name="cid">id of the requested component</param>
        /// <returns>null if component not found</returns>
        public override IBrokerManagedComponent GetComponent(Guid cid)
        {
            if (this.components.ContainsKey(cid))
            {
                EnsureEnlistedBMC(cid);
                return (IBrokerManagedComponent)components[cid];
            }
            else
                return null;
        }

        /// <summary>
        /// Used to clear all lists of transaction tracking
        /// </summary>
        protected override void ClearTrackingLists()
        {
            System.Diagnostics.Debug.WriteLine("Transaction.ClearTrackingLists:");
            base.ClearTrackingLists();
            this.components.Clear();
        }
        public Hashtable GetComponents()
        {
            return components;
        }
        #endregion Methods
    }
}