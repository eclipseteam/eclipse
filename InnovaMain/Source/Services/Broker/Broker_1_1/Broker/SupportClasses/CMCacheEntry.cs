using System;
using System.Data;
using System.Runtime.Serialization;
using Oritax.TaxSimp.Services.Caching;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Services.CMBroker
{
    [Serializable]
    internal class CMCacheEntry : IComparable
	{
        private IBrokerManagedComponent cm;
        public Guid CIID;
        public bool Deleted;
        private ICacheToken cacheToken;
        public DataSet PersistedPrimaryDataSet;

		public CMCacheEntry(Guid cIID,IBrokerManagedComponent cM)
		{
			this.cm=cM;
			this.CIID=cIID;
			this.Deleted=false;
			Type cmType;

			if(cM == null)
				cmType = typeof(IBrokerManagedComponent);
			else
				cmType = cM.GetType();

			cacheToken = CacheFactory.GetCacheToken(cmType);

			PersistedPrimaryDataSet = null;
		}

        public ICacheToken CacheToken
		{
			get
			{
				return this.cacheToken;
			}
            set
            {
                this.cacheToken = value;

                if (this.cm != null)
                    this.CacheToken.UpdateCacheabilityType(CacheFactory.GetCacheability(this.cm.GetType()));

            }

		}

        public IBrokerManagedComponent CM
		{
			get
			{
				return this.cm;
			}
			set
			{
				this.cm = value;
                
                if (this.CacheToken != null)
				    this.CacheToken.UpdateCacheabilityType(CacheFactory.GetCacheability(this.cm.GetType()));
			}
		}
		#region IComparable Members
		public int CompareTo(object obj)
		{
			if(!(obj is CMCacheEntry))
				throw new Exception( "CMCacheEntry.CompareTo, CMCacheList contains elements that are not of type: CMCacheEntry" );

			return cacheToken.CompareTo(((CMCacheEntry)obj).CacheToken);
		}
		#endregion
	}
}
