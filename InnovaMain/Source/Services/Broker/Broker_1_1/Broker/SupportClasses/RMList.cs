﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace Oritax.TaxSimp.Services.CMBroker
{
    public class RMList : IEnlistmentNotification
    {
        List<IEnlistmentNotification> AllRMs = new List<IEnlistmentNotification>();

        public RMList()
        {
        }

        public virtual void Add(IEnlistmentNotification rm)
        {
            this.AllRMs.Add(rm);
        }

        public virtual void Commit(Enlistment enlistment)
        {
            foreach (IEnlistmentNotification rm in AllRMs)
                rm.Commit(enlistment);

            enlistment.Done();
        }

        public virtual void InDoubt(Enlistment enlistment)
        {
        }

        public virtual void Prepare(PreparingEnlistment preparingEnlistment)
        {
            try
            {
                foreach (IEnlistmentNotification rm in AllRMs)
                    rm.Prepare(preparingEnlistment);

                preparingEnlistment.Prepared();
            }
            catch
            {
                preparingEnlistment.ForceRollback();
            }

        }

        public virtual void Rollback(Enlistment enlistment)
        {
            foreach (IEnlistmentNotification rm in AllRMs)
                rm.Rollback(enlistment);

            enlistment.Done();
        }

    }
}
