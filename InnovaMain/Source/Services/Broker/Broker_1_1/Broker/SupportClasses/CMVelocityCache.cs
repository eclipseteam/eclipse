using System;
using System.Collections;
using Oritax.TaxSimp.VelocityCache;

namespace Oritax.TaxSimp.Services.CMBroker
{
    internal class CMVelocityCache : CMCache
	{
		new public const int MaxSize=1000;
		new public const int MinSize=800;

        private VelocityDataCache _DataCache;

        public CMVelocityCache()
        {
            _DataCache = new VelocityDataCache(); 
        }

		private void Add(Guid ciid, CMCacheEntry cacheItem)
		{
            this._DataCache.PutObject(ciid, cacheItem);
		}

		public override CMCacheEntry this[Guid cIID]
		{
			get
			{
                return (CMCacheEntry)this._DataCache.GetObejct(cIID);
			}
			set
			{
                this._DataCache.PutObject(cIID, value);
			}
		}

        public override void Remove(Guid cIID)
        {
            this._DataCache.RemoveObject(cIID);
        }

        public override void RemoveAll()
        {
        }

        public override ArrayList GetCollectionList()
        {
            return new ArrayList();
        }
	}
}
