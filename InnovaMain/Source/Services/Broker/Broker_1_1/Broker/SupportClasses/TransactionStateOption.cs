using System;

namespace Oritax.TaxSimp.Services.CMBroker
{
	/// <summary>
	/// Indicates the state of the transaction
	/// </summary>
	public enum TransactionStateOption
	{
		/// <summary>
		/// There is no transaction
		/// </summary>
		None			=0,

		/// <summary>
		/// The transaction has started
		/// </summary>
		Started			=1,

		/// <summary>
		/// The transaction is not supported
		/// </summary>
		NotSupported	=2,

		/// <summary>
		/// The transaction is complete
		/// </summary>
		Complete		=3,

		/// <summary>
		/// The transaction is ready
		/// </summary>
		Ready			=4,

		/// <summary>
		/// The transaction has been aborted
		/// </summary>
		Abort			=5
	}
}
