using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;

using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Services.CMBroker
{
    /// <summary>
    /// Represents an N-ABLE transaction
    /// </summary>
    public class Transaction : ITransaction
    {
        #region Constants
        private const int RELEASEVALUECACHECHECK = 1000;
        #endregion Constants
        #region Stateful variables
        private Guid transactionID;
        private TransactionStateOption state;
        private ICMBroker broker;
        private SqlTransaction sqlTransaction;
        private SqlConnection sqlConnection;
        internal CMBroker cmBroker;
        private int currentReleaseCount;
        private string connectionString;

        // using hybrid dictionary to speed up, so that .Contains() operation is O(1) operation
        // using arraylist makes this a linear operation, we do lots of .Contains() operations
        // so this needs to be as efficient as possible. Also hybrid is faster for small collections
        // but will switch to hashtable when required (size).
        internal HybridDictionary cidsEnlisted;
        internal HybridDictionary cidsCanBeCollected;

        private bool isLongRunningTask;
        #endregion
        #region Properties
        /// <summary>
        /// The ID of the transaction
        /// </summary>
        public Guid TID
        {
            get
            {
                return transactionID;
            }
        }

        /// <summary>
        /// The SQL transaction used by this transaction
        /// </summary>
        public SqlTransaction SQLTransaction
        {
            get
            {
                return sqlTransaction;
            }
        }

        /// <summary>
        /// The SQL connection used by this transaction
        /// </summary>
        public SqlConnection SQLConnection
        {
            get
            {
                return sqlConnection;
            }
            set
            {
                sqlConnection = value;
            }
        }

        /// <summary>
        /// The broker associated with this transaction
        /// </summary>
        public ICMBroker Broker
        {
            get
            {
                return this.broker;
            }
        }

        /// <summary>
        /// The current state of the transaction
        /// </summary>
        public TransactionStateOption State
        {
            get
            {
                return state;
            }
        }

        /// <summary>
        /// Indicates if a transaction is a long running transaction
        /// </summary>
        public bool IsLongRunningTransaction
        {
            get
            {
                return this.isLongRunningTask;
            }
            set
            {
                this.isLongRunningTask = value;
            }
        }
        #endregion
        #region Constructors
        /// <summary>
        /// Constructs a new transaction
        /// </summary>
        /// <param name="broker">The broker for this transaction</param>
        /// <param name="transactionID">The transaction ID for this transaction</param>
        /// <param name="connectionString">The database connection string</param>
        /// <exception cref="System.ArgumentNullException">broker cannot be null</exception>
        /// <exception cref="System.ArgumentException">transactionID cannot be an empty guid</exception>
        /// <exception cref="System.ArgumentNullException">connectionString cannot be null or an empty string</exception>
        public Transaction(ICMBroker broker, Guid transactionID, string connectionString)
        {
            if (broker == null)
                throw new ArgumentNullException("broker");

            if (transactionID == Guid.Empty)
                throw new ArgumentException("Parameter cannot be an empty guid", "transactionID");

            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException("connectionString");

            this.sqlConnection = null;
            this.sqlTransaction = null;
            this.transactionID = transactionID;
            this.state = TransactionStateOption.None;
            this.broker = broker;
            this.cmBroker = (CMBroker)broker;
            this.cidsEnlisted = new HybridDictionary();
            this.cidsCanBeCollected = new HybridDictionary();
            this.currentReleaseCount = 0;
            this.connectionString = connectionString;
            this.isLongRunningTask = false;
        }
        #endregion
        #region Methods
        /// <summary>
        /// Enlists a BMC in the transaction
        /// </summary>
        /// <param name="cid">The CID of the BMC to enlist</param>
        /// <param name="initialRefCount">The reference count</param>
        /// <exception cref="System.ArgumentException">cid cannot be an empty guid</exception>
        public void EnlistBMC(Guid cid, int initialRefCount)
        {
            if (cid == Guid.Empty)
                throw new ArgumentException("Parameter cannot be an empty guid", "cid");

            if (!cidsEnlisted.Contains(cid))
                cidsEnlisted.Add(cid, initialRefCount);
            else
            {
                // increment the reference count
                int refCount = (int)cidsEnlisted[cid];
                cidsEnlisted[cid] = ++refCount;
            }

            if (cidsCanBeCollected.Contains(cid))
                cidsCanBeCollected.Remove(cid);
        }

        /// <summary>
        /// Enlists a BMC in the transaction
        /// </summary>
        /// <param name="cid">The CID of the BMC to enlist</param>
        /// <param name="initialRefCount">The reference count</param>
        /// <exception cref="System.ArgumentException">cid cannot be an empty guid</exception>
        public virtual void EnlistBMC(IBrokerManagedComponent bmc, int initialRefCount)
        {
            if (bmc == null )
                throw new ArgumentNullException("bmc cannot be null");

            EnlistBMC(bmc.CID, initialRefCount);
        }
        /// <summary>
        /// Update reference count for BMC or add it to the enlisted list
        /// </summary>
        /// <param name="cid">The CID of the BMC to enlist</param>
        protected void EnsureEnlistedBMC(Guid cid)
        {
            EnlistBMC(cid, 1);
        }
        /// <summary>
        /// Removes a BMC as being referenced by this transaction (important: see remarks)
        /// </summary>
        /// <remarks>
        /// This is used to remove something when for example it is deleted or when the CID changes (divergance)
        /// it should be used very carefully, most times you will want to use ReleaseBMC
        /// </remarks>
        /// <param name="cid">The CID of the BMC to de-enlist</param>
        /// <returns>The ref count of the item being removed</returns>
        /// <exception cref="System.ArgumentException">cid cannot be an empty guid</exception>
        public int DeEnlistBMC(Guid cid)
        {
            if (cid == Guid.Empty)
                throw new ArgumentException("Parameter cannot be an empty guid", "cid");

            int refCount = -1;

            if (cidsEnlisted.Contains(cid))
            {
                refCount = (int)cidsEnlisted[cid];
                cidsEnlisted.Remove(cid);
            }

            if (cidsCanBeCollected.Contains(cid))
                cidsCanBeCollected.Remove(cid);

            return refCount;
        }

        /// <summary>
        /// Releases a BMC from the transaction when it is no longer needed by a piece of code
        /// </summary>
        /// <param name="cid">The CID of the BMC to release</param>
        /// <exception cref="System.ArgumentException">cid cannot be an empty guid</exception>
        /// <exception cref="System.InvalidOperationException">A BMC has been released more times than it has been requested, or a BMC which has never been requested by this transaction</exception>
        public void ReleaseBMC(Guid cid)
        {
            if (cid == Guid.Empty)
                throw new ArgumentException("Parameter cannot be an empty guid", "cid");

            if (cidsEnlisted.Contains(cid))
            {
                int refCount = (int)cidsEnlisted[cid];
                cidsEnlisted[cid] = --refCount;

                if (refCount < 0)
                    throw new InvalidOperationException("Transaction.ReleaseBMC, releasing a bmc more times than requested is not permitted");

                // only collect if no-one is referencing any more
                if (!cidsCanBeCollected.Contains(cid) && refCount <= 0)
                    cidsCanBeCollected.Add(cid, null);
            }
            else
                throw new InvalidOperationException("Transaction.ReleaseBMC, releasing a bmc which has not been requested is not permitted");

            this.currentReleaseCount++;

            if (this.currentReleaseCount >= RELEASEVALUECACHECHECK)
            {
                this.currentReleaseCount = 0;
                this.CheckCacheAndResize();
            }
        }

        /// <summary>
        /// Starts the transaction running
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Can't start a transaction which is already started</exception>
        public void Start()
        {
            if (this.state == TransactionStateOption.Started)
                throw new InvalidOperationException("Cannot start an already started transaction");

            this.SQLConnection = new SqlConnection(this.connectionString);
            this.SQLConnection.Open();

            //Set the transaction to null in case the BeginTransaction fails and throws
            this.sqlTransaction = null;

            // we are using ReadUncommited reads as N-ABLE enforces a reader/writer
            // model, so no readers can read while there is a writer and there can only be one
            // writer, so SQL will not need to isolate changes from N-ABLE (unless 2 instances are pointed at the 
            // same database or someone goes in through another tool and modifies something).
            // This is also higher performing as we avoid SQL locks
            this.sqlTransaction = this.SQLConnection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);

            this.state = TransactionStateOption.Started;
            CMCacheManager.CacheManager.LogTransactionStart(this);
            this.cmBroker.LogEvent("Transaction: " + this.TID.ToString() + " started ");
            this.ClearTrackingLists();
        }

        /// <summary>
        /// Commits the current transaction
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Can't complete a transaction which is not started</exception>
        public void Complete()
        {
            if (this.state != TransactionStateOption.Started)
                throw new InvalidOperationException("Cannot complete a transaction that isn't marked as started");

            //commit any changes to the database changes
            if (this.SQLTransaction != null)
            {
                this.SQLTransaction.Commit();
                this.SQLTransaction.Dispose();
            }

            if (this.sqlConnection != null)
            {
                this.sqlConnection.Close();
                this.sqlConnection.Dispose();
            }

            this.state = TransactionStateOption.Complete;
            CMCacheManager.CacheManager.LogTransactionComplete(this);
            this.ClearTrackingLists();
        }

        /// <summary>
        /// Rolls back the transaction
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Can't complete a transaction which is not started</exception>
        public void Rollback()
        {
            if (this.state != TransactionStateOption.Started)
                throw new InvalidOperationException("Cannot complete a transaction that isn't marked as started");

            this.RemoveInUseCacheEntries();

            try
            {
                //if the rollback was due to the begin failing then don't do a rollback
                if (this.SQLTransaction != null)
                    this.SQLTransaction.Rollback();
            }
            catch (InvalidOperationException)
            {
                // we ignore this as per the documentation for RollBack indicates if
                // the database has already rolled back the transaction we will get an
                // invalid operation exception, we should ignore this so the real
                // source of the error can be seen which caused the application to
                // attempt the rollback and allow the rest of the rollback operation 
                // to complete
            }

            if (this.SQLTransaction != null)
                this.SQLTransaction.Dispose();

            this.sqlTransaction = null;

            if (this.sqlConnection != null)
            {
                this.sqlConnection.Close();
                this.sqlConnection.Dispose();
            }

            this.state = TransactionStateOption.Abort;
            this.ClearTrackingLists();
        }

        /// <summary>
        /// Removes all cache entries enlisted in this transaction
        /// </summary>
        /// <remarks>
        /// Used by the rollback operation to remove those items from the cache
        /// which may be invalid because they have been used by a rolled-back
        /// transaction
        /// </remarks>
        public void RemoveInUseCacheEntries()
        {
            foreach (Guid cid in this.GetCIDsEnlisted())
                CMCacheManager.CacheManager.Remove(cid);
        }

        /// <summary>
        /// Gets a list of the instance ids that are enlisted in the transaction
        /// </summary>
        /// <returns>
        /// A list of the instance IDs that are currently enlisted in the transaction
        /// </returns>
        public Guid[] GetCIDsEnlisted()
        {
            Guid[] cids = new Guid[this.cidsEnlisted.Keys.Count];
            this.cidsEnlisted.Keys.CopyTo(cids, 0);
            return cids;
        }

        /// <summary>
        /// Indicates if a particular instance id can be collected, e.g. is not in use currently in the transaction
        /// </summary>
        /// <param name="cid">The instance ID of the module to check</param>
        /// <returns>True if the instance ID either has never been involoved in the transaction or is not currently in use, false otherwise</returns>
        /// <exception cref="System.ArgumentException">cid cannot be an empty guid</exception>
        public bool CanBeCollected(Guid cid)
        {
            if (cid == Guid.Empty)
                throw new ArgumentException("Parameter cannot be an empty guid", "cid");

            if (!this.cidsEnlisted.Contains(cid))
                return true;

            return (this.cidsCanBeCollected.Contains(cid));
        }

        /// <summary>
        /// Indicates if a specified instance ID has been involved in the current transaction
        /// </summary>
        /// <param name="cid">The instance ID to check</param>
        /// <returns>True if the instance ID has been involved in the transaction</returns>
        /// <exception cref="System.ArgumentException">cid cannot be an empty guid</exception>
        public bool IsEnlisted(Guid cid)
        {
            if (cid == Guid.Empty)
                throw new ArgumentException("Parameter cannot be an empty guid", "cid");

            return this.cidsEnlisted.Contains(cid);
        }

        /// <summary>
        /// Get BrokerManagedComponent from the list of components within the transaction
        /// </summary>
        /// <param name="cid">id of the requested component</param>
        /// <returns>null, as Transaction does not maintain components list </returns>
        public virtual IBrokerManagedComponent GetComponent(Guid cid)
        {
             return null;
        }

        /// <summary>
        /// Used to clear all lists of transaction tracking
        /// </summary>
        protected virtual void ClearTrackingLists()
        {
            this.cidsEnlisted.Clear();
            this.cidsCanBeCollected.Clear();
        }

        /// <summary>
        /// Checks if the cache is too big and reduces size if it is too big
        /// </summary>
        private void CheckCacheAndResize()
        {
            try
            {
                CMCacheManager.CacheLoadLock.AcquireWriterLock(-1);

                ArrayList invalidDependencies = null;
                CMCacheManager.CacheManager.CheckCache(this, new CMCacheManager.CheckBMCCacheRemovalDelegate(this.CanBeCollected), true, ref invalidDependencies);
                CMCacheManager.CacheManager.DeleteInvalidCacheEntries(this, ref invalidDependencies, true);
            }
            finally
            {
                CMCacheManager.CacheLoadLock.ReleaseLock();
            }
        }
        #endregion Methods
    }
}