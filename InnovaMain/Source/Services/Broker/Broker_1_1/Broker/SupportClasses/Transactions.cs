using System;
using System.Collections;

using Oritax.TaxSimp.CalculationInterface;
using System.Collections.Generic;

namespace Oritax.TaxSimp.Services.CMBroker
{
	internal class Transactions : Dictionary<Guid, ITransaction>
	{
		/// <summary>
		/// Gets or sets a transaction based on the transaction ID
		/// </summary>
		/// <param name="tID">The transaction ID</param>
		/// <returns>The transaction associated with the transaction</returns>
        public new ITransaction this[Guid tID]
		{
			get
			{
				return base[tID];
			}
			set
			{
                lock (this)
                {
                    base[tID] = value;
                }
			}
		}

        /// <summary>
        /// Removes a transaction from the current transaction list
        /// </summary>
        /// <param name="tID">The transaction ID to remove</param>
        /// <exception cref="System.ArgumentException">tID cannot be an empty guid</exception>
        public new void Remove(Guid tID)
        {
            if (tID == Guid.Empty)
                throw new ArgumentException("Parameter cannot be an empty guid", "tID");

            lock (this)
            {
                if (base.ContainsKey(tID))
                    base.Remove(tID);
            }
		}

        /// <summary>
        /// Determines if there is a long running transaction currently running
        /// </summary>
        /// <returns>True if there is a long running transaction in progress, false otherwise</returns>
        public bool IsLongRunningTransactionInProgress()
        {
            List<ITransaction> transactions = null;
            lock (this)
            {
                 transactions = new List<ITransaction>(this.Values);
            }

            foreach (ITransaction transaction in transactions)
            {
                if (transaction.IsLongRunningTransaction)
                    return true;
            }

            return false;
        }
	}
}
