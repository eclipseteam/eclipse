using System;
using System.Data;

using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Services.CMBroker
{
	/// <summary>
	/// Class used to store details of an event log
	/// </summary>
    public class EventLogEntry : IEventLogEntry
    {
        private Guid id = Guid.NewGuid();
        private DateTime time = DateTime.Now;
        private EventType type;
        private Guid cid = Guid.Empty;

        public Guid Cid
        {
            get { return cid; }
            set { cid = value; }
        } 
        private string details;
        private string userDetails;

        public EventLogEntry(EventType type, string details, string userDetails)
        {
            this.type = type;
            this.details = details;
            this.userDetails = userDetails;
        }

        public EventLogEntry(EventType type, string details, string userDetails, Guid cid)
        {
            this.type = type;
            this.details = details;
            this.userDetails = userDetails;
            this.cid = cid; 
        }

        public EventLogEntry(DataRow dataRow)
        {
            this.id = new Guid(dataRow[DSEventsList.EVENT_ID_FIELD].ToString());
            this.cid = new Guid(dataRow[DSEventsList.EVENT_CID_FIELD].ToString());
            this.type = new EventType(dataRow[DSEventsList.EVENT_TYPE_FIELD].ToString());
            this.time = DateTime.Parse(dataRow[DSEventsList.EVENT_DATETIME_FIELD].ToString());
            this.userDetails = dataRow[DSEventsList.EVENT_USERNAME_FIELD].ToString();
            this.details = dataRow[DSEventsList.EVENT_DETAIL_FIELD].ToString();
        }

        public Guid ID
        {
            get
            {
                return this.id;
            }
        }

        public DateTime Time
        {
            get
            {
                return this.time;
            }
        }

        public EventType Type
        {
            get
            {
                return this.type;
            }
        }

        public string Details
        {
            get
            {
                return this.details;
            }
        }

        public string UserDetails
        {
            get
            {
                return this.userDetails;
            }
        }

        public void FillDataRowWithEventLogData(DataRow row)
        {
            row[DSEventsList.EVENT_ID_FIELD] = this.ID;
            row[DSEventsList.EVENT_NAME_FIELD] = this.Type.GetEventName();
            row[DSEventsList.EVENT_TYPE_FIELD] = this.Type.GetEventType();
            row[DSEventsList.EVENT_DATETIME_FIELD] = this.Time;
            row[DSEventsList.EVENT_USERNAME_FIELD] = this.UserDetails;
            row[DSEventsList.EVENT_DETAIL_FIELD] = this.Details;
            row[DSEventsList.EVENT_CID_FIELD] = this.cid;
        }
    }
}
