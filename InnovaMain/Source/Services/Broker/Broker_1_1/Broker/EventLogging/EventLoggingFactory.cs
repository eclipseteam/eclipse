using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Threading;

using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Services.CMBroker
{
	/// <summary>
	/// Static factory used to log events from N-ABLE into the database
	/// </summary>
	internal class EventLoggingFactory
	{
		private EventLoggingFactory()
		{
		}

		public static void LogEvent(int eventType, string eventDetails, string userDetails, Guid cid)
		{
			EventLogEntry eventLog = new EventLogEntry(new EventType(eventType), eventDetails, userDetails, cid);
			EventLogEntries.Add(eventLog);
		}

		public static void CommitLogEntries(ITransaction transaction)
		{
			DSEventsList eventsDS = new DSEventsList();
			DataTable eventsTable = eventsDS.Tables[ DSEventsList.EVENTLIST_TABLE ];

			foreach(EventLogEntry eventLog in EventLogEntries)
			{
				DataRow eventLogRow = eventsTable.NewRow();
				eventLog.FillDataRowWithEventLogData(eventLogRow);
				eventsTable.Rows.Add(eventLogRow);
			}
			
			WriteEventsToDatabase(eventsDS, transaction);

			EventLogEntries = null;
		}

		public static void AbortLogEntries()
		{
			EventLogEntries = null;
		}

		private static ArrayList EventLogEntries
		{
			get
			{
				LocalDataStoreSlot eventLogEntriesSlot = Thread.GetNamedDataSlot("EventLogEntries");
				ArrayList eventLogEntries = (ArrayList) Thread.GetData(eventLogEntriesSlot);

				if(eventLogEntries == null)
				{
					eventLogEntries = new ArrayList();
					Thread.SetData(eventLogEntriesSlot, eventLogEntries);
				}

				return eventLogEntries;
			}
			set
			{
				LocalDataStoreSlot eventLogEntriesSlot = Thread.GetNamedDataSlot("EventLogEntries");

				if(value == null)
					Thread.FreeNamedDataSlot("EventLogEntries");
				else
					Thread.SetData(eventLogEntriesSlot, value);
			}
		}

		private static void WriteEventsToDatabase(DataSet eventsDS, ITransaction transaction)
		{
			DataTable eventsTable = eventsDS.Tables[ DSEventsList.EVENTLIST_TABLE ];

			// if there are no rows in the table, bomb out
			// early to save doing any work
			if(eventsTable.Rows.Count <= 0)
				return;

			SqlDataAdapter dataAdapter = null;

			try
			{
				dataAdapter = CreateDataAdapter(transaction);
				dataAdapter.Update(eventsTable);
			}
			finally
			{
				if(dataAdapter != null)
					dataAdapter.Dispose();
			}
		}

		private static SqlDataAdapter CreateDataAdapter(ITransaction transaction)
		{
			SqlDataAdapter dataAdapter = new SqlDataAdapter( );

			dataAdapter.SelectCommand = DBCommandFactory.Instance.NewSqlCommand();
			dataAdapter.SelectCommand.CommandType = CommandType.Text;

			dataAdapter.SelectCommand.Connection = transaction.SQLConnection;
			dataAdapter.SelectCommand.Transaction = transaction.SQLTransaction;

			dataAdapter.InsertCommand = DBCommandFactory.Instance.NewSqlCommand();
			dataAdapter.InsertCommand.CommandText = "INSERT INTO EVENTLOG (EVENTID,TYPE,NAME,USERNAME,DATETIME,DETAIL) VALUES(@EVENTID,@TYPE,@NAME,@USERNAME,@DATETIME,@DETAIL)";
			dataAdapter.InsertCommand.CommandType = CommandType.Text;
			dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@EVENTID", SqlDbType.UniqueIdentifier,16,DSEventsList.EVENT_ID_FIELD));
			dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@TYPE", SqlDbType.VarChar,50,DSEventsList.EVENT_TYPE_FIELD));
			dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@NAME", SqlDbType.VarChar,50,DSEventsList.EVENT_NAME_FIELD));
			dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@USERNAME", SqlDbType.VarChar,50,DSEventsList.EVENT_USERNAME_FIELD));
			dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@DATETIME", SqlDbType.DateTime,8,DSEventsList.EVENT_DATETIME_FIELD));
			dataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@DETAIL", SqlDbType.VarChar,500,DSEventsList.EVENT_DETAIL_FIELD));
			dataAdapter.InsertCommand.Connection = transaction.SQLConnection;
			dataAdapter.InsertCommand.Transaction = transaction.SQLTransaction;

			return dataAdapter;
		}
	}
}
