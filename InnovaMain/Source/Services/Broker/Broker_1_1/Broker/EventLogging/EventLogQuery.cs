﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using System.Data.SqlClient;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Services.CMBroker.EventLogging
{
    public class EventLogQuery : IEventLogQuery
    {
        private IList<IEventLogEntry> _eventLogList;
        private EventLogQueryType _eventLogQueryType;

        public EventLogQuery(ITransaction transaction, EventLogQueryType eventLogQueryType)
        {
            _eventLogQueryType = eventLogQueryType;
            SqlDataAdapter dataAdapter = CreateDataAdapter(transaction);

            DataSet eventLogDataSet = new DataSet();
            dataAdapter.Fill(eventLogDataSet);

            PopulateEventLogList(eventLogDataSet);
        }

        public int Count
        {
            get { return _eventLogList.Count; }
        }

        public IEventLogEntry this[int index]
        {
            get { return _eventLogList[index]; }
        }

        public IEnumerator<IEventLogEntry> GetEnumerator()
        {
            foreach (EventLogEntry eventLogEntry in _eventLogList)
                yield return eventLogEntry;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (EventLogEntry eventLogEntry in _eventLogList)
                yield return eventLogEntry;
        }

        private void PopulateEventLogList(DataSet eventLogDataSet)
        {
            _eventLogList = new List<IEventLogEntry>();
            foreach (DataRow dataRow in eventLogDataSet.Tables[0].Rows)
            {
                EventLogEntry eventLogEntry = new EventLogEntry(dataRow);
                _eventLogList.Add(eventLogEntry);
            }
        }

        private SqlDataAdapter CreateDataAdapter(ITransaction transaction)
        {
            SqlDataAdapter dataAdapter = new SqlDataAdapter();

            dataAdapter.SelectCommand = DBCommandFactory.Instance.NewSqlCommand();
            dataAdapter.SelectCommand.CommandType = CommandType.Text;

            dataAdapter.SelectCommand.Connection = transaction.SQLConnection;
            dataAdapter.SelectCommand.Transaction = transaction.SQLTransaction;

            switch (_eventLogQueryType)
            {
                case EventLogQueryType.SuperAdministratorCreated:
                    dataAdapter.SelectCommand.CommandText = SuperAdministratorCreateQuery();
                    break;

                default:
                    break;
            }

            return dataAdapter;
        }

        private string SuperAdministratorCreateQuery()
        {
            StringBuilder superAdministratorCreateQueryStringBuilder = new StringBuilder();
            EventType superAdministratorCreated = new EventType(EventType.SuperAdministratorCreated);
            string eventName = superAdministratorCreated.GetEventName();
            superAdministratorCreateQueryStringBuilder.AppendFormat("SELECT * FROM EVENTLOG WHERE EVENTLOG.{0}='{1}'", "NAME", eventName);
            return superAdministratorCreateQueryStringBuilder.ToString();
        }
    }
}
