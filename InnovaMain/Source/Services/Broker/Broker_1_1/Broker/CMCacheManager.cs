using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using Oritax.TaxSimp.Services.Caching;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CommonPersistence;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.VelocityCache;
using Oritax.TaxSimp.Security;
using System.Web;

namespace Oritax.TaxSimp.Services.CMBroker
{
	/// <summary>
	/// Summary description for CMCacheManager.
	/// </summary>
	public class CMCacheManager
    {
        public const string VELOCITY_LOCK_KEY = "VelocityLock";

		internal delegate bool CheckBMCCacheRemovalDelegate(Guid cid);
		
		private const int CACHEFREECHECKTIME = 120000;
		static CMCacheManager()
		{
			cacheCollectionTrigger = CacheFactory.GetCacheCollectionTrigger();
			cacheLock = new ReaderWriterLock();
			cacheLoadLock = new ReaderWriterLock();
		}

		private CMCacheManager()
		{
            if (VelocityConfiguration.IsVelocityConfigured)
                cMCache = new CMVelocityCache();
            else
                cMCache = new CMCache();

            cacheResizingTimer = new Timer(new TimerCallback(this.CheckCacheFromTimer), null, CACHEFREECHECKTIME, CACHEFREECHECKTIME);

            // Initialise velocity shared lock
            if (VelocityConfiguration.IsITLPDSCached)
                VelocityCacheString.VelocityCache.PutObject(CMCacheManager.VELOCITY_LOCK_KEY, "");

		}

		private static CMCacheManager cMCacheManager=new CMCacheManager();
		private static ICacheCollectionTrigger cacheCollectionTrigger;
		private static ReaderWriterLock cacheLock;
		private static ReaderWriterLock cacheLoadLock;
        

		private CMCache cMCache;
		private Timer cacheResizingTimer;

		static private bool		modified;
		static private bool		cacheLoggingOn=GetLoggingEnabled();
		static private string	logPath=GetLogPath();

		public static CMCacheManager CacheManager
		{
			get{return cMCacheManager;}
		}

		public static ReaderWriterLock CacheLock
		{
			get
			{
				return cacheLock;
			}
		}

		public static ReaderWriterLock CacheLoadLock
		{
			get
			{
				return cacheLoadLock;
			}
		}

		public static bool AmReader()
		{
			return cacheLock.IsReaderLockHeld;
		}

		public static bool AmWriter()
		{
			return cacheLock.IsWriterLockHeld;
		}

		private static void CheckReaderLockHeld()
		{
			if(!cacheLock.IsReaderLockHeld && !cacheLock.IsWriterLockHeld)
				throw new Exception("Unable to perform this operation when you don't have a reader lock");
		}

		private static void CheckWriterLockHeld()
		{
            //if (!cacheLock.IsWriterLockHeld)
            //    throw new Exception("Unable to perform this operation when you don't have a writer lock");
		}
		/// <summary>
		/// Commits the set of CMs in the cache that belong to the specified transaction.
		/// Uses a .NET Transaction to enable the rolling back of changes if an error occurs
		/// Performs cache maintainence
		/// </summary>
		/// <param name="transaction">The transaction to be committed.</param>
		public void Commit(ITransaction transaction)
		{
            CheckWriterLockHeld();
			
            ArrayList invalidDependencies = null;

			try
			{
                cacheLoadLock.AcquireWriterLock(-1);

				//Go through the cache saving and deleting cms to DB
				CommitCacheEntries(transaction, ref invalidDependencies);
				
				//Free up all cms that belonged to the transaction
				FreeCacheEntries(transaction);

				DeleteInvalidCacheEntries(transaction, ref invalidDependencies, true);

				EventLoggingFactory.CommitLogEntries(transaction);

				modified=false;

				NablePerformanceCounters.GetCounter("BMC Cache Size").SetRawValue(cMCache.Count);
			}
			finally
			{
				cacheLoadLock.ReleaseLock();
			}
		}

		public void DebugCheckReadHasntModifiedCMS()
		{
			CheckReaderLockHeld();

			try
			{
				CMCacheManager.cacheLoadLock.AcquireReaderLock(-1);

				foreach(DictionaryEntry cacheEntryDE in cMCache)
				{
					CMCacheEntry cacheEntry = (CMCacheEntry) cacheEntryDE.Value;

					if(cacheEntry.Deleted || cacheEntry.CM.Modified)
						throw new ApplicationException("DebugCheckReadHasntModifiedCMS, Read transaction has modified items in the cache, CID modified: " + cacheEntry.CIID.ToString());
				}
			}
			finally
			{
				CMCacheManager.cacheLoadLock.ReleaseLock();
			}
		}

		private void CommitCacheEntries(ITransaction transaction, ref ArrayList invalidDependencies)
		{
			ArrayList deletedCacheEntries = null;
			ArrayList refreshedCacheEntries = null;

			try
			{
                CMCacheManager.cacheLoadLock.AcquireWriterLock(-1); 

				foreach(Guid cid in transaction.GetCIDsEnlisted())
				{
					CMCacheEntry cMCacheEntry = (CMCacheEntry) cMCache[cid];

					if(cMCacheEntry != null)
						this.CommitCacheEntry(transaction, cMCacheEntry, ref deletedCacheEntries, ref refreshedCacheEntries, ref invalidDependencies);
				}

				this.DeleteCacheEntries(transaction, ref deletedCacheEntries, ref invalidDependencies);
				this.RefreshCacheEntries(ref refreshedCacheEntries);
			}
			finally
			{
				CMCacheManager.cacheLoadLock.ReleaseLock();
			}
		}

		private void CommitCacheEntry(ITransaction transaction, CMCacheEntry cacheEntry, ref ArrayList deletedCacheEntries, ref ArrayList refreshedCacheEntries, ref ArrayList invalidDependencies)
		{
            IBrokerManagedComponent bmc = transaction.GetComponent(cacheEntry.CM.CID);
            if (bmc == null)
				bmc = cacheEntry.CM;

			if(cacheEntry.Deleted)
			{
				// lazy initialise if required
				if(deletedCacheEntries == null)
					deletedCacheEntries = new ArrayList();

				deletedCacheEntries.Add(cacheEntry);
			}
			else if(bmc.Modified)
			{
				// need to enlist in case of divergance
				transaction.EnlistBMC(bmc, 1);

				bmc.PrePersistActions();

				if(bmc.UpdateToken == Token.Empty)
					bmc.CalculateToken(true);

				CMPersistenceController.Save(bmc, transaction, ref invalidDependencies, ref cacheEntry.PersistedPrimaryDataSet);

				bmc.PostPersistActions();

				bmc.Modified = false; //the only place that modified is set

				if(bmc.CID != cacheEntry.CIID)
				{
					// lazy initialise if required
					if(refreshedCacheEntries == null)
						refreshedCacheEntries = new ArrayList();

					refreshedCacheEntries.Add(new CMCacheEntry(bmc.CID, bmc));
				}

                this.UpdateCacheEntry(bmc);
			}
		}

		private void DeleteCacheEntries(ITransaction transaction, ref ArrayList deletedCacheEntries, ref ArrayList invalidDependencies)
		{
			// only do this if something has been added
			if(deletedCacheEntries != null)
			{
				if(!CMCacheManager.cacheLoadLock.IsWriterLockHeld)
				{
					CMCacheManager.cacheLoadLock.ReleaseLock();
					CMCacheManager.cacheLoadLock.AcquireWriterLock(-1);
				}

				foreach(CMCacheEntry deletedCacheEntry in deletedCacheEntries)
				{
					CMPersistenceController.Delete(transaction.Broker, deletedCacheEntry.CM, transaction, ref invalidDependencies);
					cMCache.Remove(deletedCacheEntry.CIID);
				}
			}		
		}

		private void RefreshCacheEntries(ref ArrayList refreshedCacheEntries)
		{
			// only do this if something has been added
			if(refreshedCacheEntries != null)
			{
				if(!CMCacheManager.cacheLoadLock.IsWriterLockHeld)
				{
					CMCacheManager.cacheLoadLock.ReleaseLock();
					CMCacheManager.cacheLoadLock.AcquireWriterLock(-1);
				}

				foreach(CMCacheEntry refreshedCacheEntry in refreshedCacheEntries)
				{
					cMCache.Remove(refreshedCacheEntry.CIID);
					cMCache[refreshedCacheEntry.CIID] = refreshedCacheEntry;
				}
			}		
		}

		private void FreeCacheEntries(ITransaction transaction)
		{
            try
            {
                // Set all of the ownertransaction entries for this transaction to null, only now that
                // Database transaction is complete.
                var cacheToClear = cMCache.GetCollectionList();

                foreach (CMCacheEntry dictionaryEntry in cacheToClear)
                {
                    if (cMCache.ContainsKey(dictionaryEntry.CIID))
                    {
                        CMCacheEntry cMCacheEntry = (CMCacheEntry)cMCache[dictionaryEntry.CIID];
                        cMCacheEntry.CacheToken.CacheItemReleased();
                    }
                }
            }
            catch {// Swallow Exception as the collection can be modified 
                }
		}

		public void DeleteInvalidCacheEntries(ITransaction transaction, ref ArrayList invalidDependencies, bool persistChanges)
		{
			ArrayList deletedCacheEntries = null;
			ArrayList refreshedCacheEntries = null;
			ArrayList newInvalidDependencies = null;

			// only do this if something has been added
			if(invalidDependencies != null)
			{
				if(!CMCacheManager.cacheLoadLock.IsWriterLockHeld)
				{
					CMCacheManager.cacheLoadLock.ReleaseLock();
					CMCacheManager.cacheLoadLock.AcquireWriterLock(-1);
				}

				foreach(Guid dependencyID in invalidDependencies)
				{
					if(persistChanges)
					{
						CMCacheEntry cacheEntry = this.cMCache[dependencyID];

						if(cacheEntry != null)
							this.CommitCacheEntry(transaction, cacheEntry, ref deletedCacheEntries, ref refreshedCacheEntries, ref newInvalidDependencies);
					}
				}

				foreach(Guid dependencyID in invalidDependencies)
				{
					CMCacheEntry cacheEntry = this.cMCache[dependencyID];

					if(transaction.CanBeCollected(dependencyID))
						this.Clear(transaction,dependencyID);
				}

				this.DeleteCacheEntries(transaction, ref deletedCacheEntries, ref newInvalidDependencies);
				this.RefreshCacheEntries(ref refreshedCacheEntries);
				this.DeleteInvalidCacheEntries(transaction, ref newInvalidDependencies, true);
			}
		}


		public void Abort(ITransaction transaction)
		{
			transaction.Rollback();
			modified=false;
		}

		public ICMType CreateCMType(CMTypeInfo cMTypeDetails,ITransaction transaction)
		{
			CheckWriterLockHeld();
			CalculationTypePersist calculationTypePersist=new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction);
			ICMType iCT=CMPersistenceController.CreateCMType(calculationTypePersist,cMTypeDetails, transaction);

			AddInstanceToEnlistedItems(transaction.Broker, iCT, 1);

			return iCT;
		}

		public ILogicalModule CreateLogicalCM(Guid typeId, String cmName, Guid initialCSID, ITransaction transaction, BusinessStructureContext objContext )
		{
			CheckWriterLockHeld();

			CalculationTypePersist calculationTypePersist=new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction);
			DSCalculationModuleType dSCalculationModuleType=(DSCalculationModuleType)calculationTypePersist.FindByTypeId(typeId);

			string cmTypeName = (string)dSCalculationModuleType.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0][DSCalculationModuleType.CMTYPENAME_FIELD];

			return this.CreateLogicalCM(cmTypeName, cmName, initialCSID, transaction, objContext);
		}

		public ILogicalModule CreateLogicalCM(string typeName, String cmName, Guid initialCSID, ITransaction transaction, BusinessStructureContext objContext)
		{
			CheckWriterLockHeld();

			CalculationTypePersist calculationTypePersist=new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction);
			DSCalculationModuleType dSCalculationModuleType=(DSCalculationModuleType)calculationTypePersist.FindByTypeName(typeName);
			Guid typeID=(Guid)dSCalculationModuleType.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0][DSCalculationModuleType.CMTYPEID_FIELD];

			string cmTypeName = (string)dSCalculationModuleType.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0][DSCalculationModuleType.CMTYPENAME_FIELD];
			
			Guid clid = Guid.NewGuid();
			ILogicalModule logicalCM = new LogicalModuleBase(clid, typeID, cmTypeName, initialCSID);

			logicalCM.CID = clid;
			logicalCM.CMType=typeID;
			logicalCM.CMTypeName=cmTypeName;
			logicalCM.CMCategory=(string)dSCalculationModuleType.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0][DSCalculationModuleType.CMCATEGORYNAME_FIELD];
			logicalCM.Name=cmName;
			logicalCM.TypeName="LogicalModuleBase";
			logicalCM.CategoryName="Framework";
			logicalCM.CurrentBusinessStructureContext = objContext;

			AddInstanceToEnlistedItems(transaction.Broker, logicalCM, 1);

			logicalCM.CalculateToken(true);
			cMCache[logicalCM.CID]=new CMCacheEntry(logicalCM.CID, logicalCM);
			modified=true;

			// Create the physical CM representing the default scenario
			if(initialCSID!=Guid.Empty)
			{
				ICalculationModule iCM=(ICalculationModule)CMPersistenceController.Create(calculationTypePersist,typeName, false);
				iCM.Initialize(cmName);
				this.InitialiseBusinessStructureContext( iCM, objContext );
				iCM.CLID=logicalCM.CID;
				iCM.CSID=initialCSID;

				AddInstanceToEnlistedItems(transaction.Broker, iCM, 1);

				iCM.CalculateToken(true);
				cMCache[iCM.CMID]=new CMCacheEntry(iCM.CMID, iCM);

				CMScenario currentScenario=logicalCM.GetScenario(logicalCM.CurrentScenario);
				currentScenario.CIID=iCM.CMID;
				currentScenario.CLID=iCM.CLID;
			}

			return logicalCM;
		}

		private void InitialiseBusinessStructureContext( ICalculationModule objCM, BusinessStructureContext objContext )
		{
			objCM.CurrentBusinessStructureContext = objContext;
			objCM.InitialiseBusinessStructureContext( );
		}

		/// <summary>
		/// Creates a CM instance, specified by type GUID within the specified transaction.  The instance will
		/// be cached in the database until the transaction is either committed or aborted.
		/// </summary>
		/// <param name="typeId">The CM Type Guid for which an instance is to be created.</param>
		/// <param name="cmName">The name the CM Instance is to have.</param>
		/// <param name="transaction">The transaction under which the CM is to be created.</param>
		/// <returns>An interface to the created CM, or null if the CM cannot be created.</returns>
		/// <param name="objContext"></param>
		/// <param name="objBroker"></param>
		/// <param name="objCLID"></param>
		/// <param name="objCSID"></param>
		public ICalculationModule CreateCMInstance(Guid typeId, String cmName, ITransaction transaction, BusinessStructureContext objContext, ICMBroker objBroker, Guid objCLID, Guid objCSID )
		{
			CheckWriterLockHeld();

			CalculationTypePersist calculationTypePersist=new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction);
			ICalculationModule iCM=(ICalculationModule)CMPersistenceController.Create(calculationTypePersist,typeId, false);
			iCM.CLID = objCLID;
			iCM.CSID = objCSID;
			iCM.Initialize(cmName);
			this.InitialiseBusinessStructureContext( iCM, objContext );

			iCM.CalculateToken(true);
			this.cMCache[ iCM.CMID ] = new CMCacheEntry( iCM.CMID, iCM );
			modified = true;

			AddInstanceToEnlistedItems(transaction.Broker, iCM, 1);

			return iCM;
		}

		/// <summary>
		/// Creates a CM instance, specified by typename, within the specified transaction.  The instance will be
		/// cached in the database until the transaction is either committed or aborted.
		/// </summary>
		/// <param name="typeName">The CM Type Name for which an instance is to be created.</param>
		/// <param name="cmName">The name the CM Instance is to have.</param>
		/// <param name="transaction">The transaction under which the CM is to be created.</param>
		/// <param name="objContext"></param>
		/// <param name="objBroker"></param>
		/// <param name="objCLID"></param>
		/// <param name="objCSID"></param>
		/// <returns>An interface to the created CM, or null if the CM cannot be created.</returns>
		public ICalculationModule CreateCMInstance(string typeName, string cmName, ITransaction transaction, BusinessStructureContext objContext, ICMBroker objBroker, Guid objCLID, Guid objCSID )
		{
			CheckWriterLockHeld();

			CalculationTypePersist calculationTypePersist=new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction);
			ICalculationModule iCM=(ICalculationModule)CMPersistenceController.Create(calculationTypePersist,typeName, false);
			iCM.CLID = objCLID;
			iCM.CSID = objCSID;
			iCM.Initialize(cmName);
			this.InitialiseBusinessStructureContext( iCM, objContext );

			iCM.CalculateToken(true);
			CMCacheEntry cMCacheEntry=new CMCacheEntry( iCM.CMID, iCM);
			cMCacheEntry.CacheToken.CacheItemRequested();
			this.cMCache[ iCM.CMID ]=cMCacheEntry;
			modified = true;

			AddInstanceToEnlistedItems(transaction.Broker, iCM, 1);

			return iCM;
		}

		/// <summary>
		/// Creates a BMC instance, specified by typename, within the specified transaction.  The instance will be
		/// cached in the database until the transaction is either committed or aborted.
		/// </summary>
		/// <param name="instID"></param>
		/// <param name="typeName">The CM Type Name for which an instance is to be created.</param>
		/// <param name="bmcName">The name the CM Instance is to have.</param>
		/// <param name="transaction">The transaction under which the CM is to be created.</param>
		/// <returns>An interface to the created CM, or null if the CM cannot be created.</returns>
		public IBrokerManagedComponent CreateBMCInstance(Guid instID, string typeName, string bmcName, ITransaction transaction)
		{
			CheckWriterLockHeld();

			CalculationTypePersist calculationTypePersist=new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction);
			IBrokerManagedComponent iBMC=CMPersistenceController.Create(calculationTypePersist,typeName, false);
			iBMC.CID=instID;
			iBMC.Initialize(bmcName);

			iBMC.CalculateToken(true);
			CMCacheEntry cMCacheEntry=new CMCacheEntry(iBMC.CID, iBMC);
			cMCacheEntry.CacheToken.CacheItemRequested();
			cMCache[iBMC.CID]=cMCacheEntry;

			AddInstanceToEnlistedItems(transaction.Broker, iBMC, 1);
			modified=true;

			return iBMC;
		}

		#region COMPONENT INSTANCE CREATION FUNCTIONS

		public IBrokerManagedComponent CreateComponentInstance(Guid instanceID,IComponentVersion componentVersion, string instanceName, ITransaction transaction)
		{
			CheckWriterLockHeld();

            AddInstanceIDToEnlistedItems(transaction.Broker, instanceID, 1);
			IBrokerManagedComponent iBMC=CMPersistenceController.Create(instanceID,componentVersion,instanceName, false);
            SaveInTransaction(transaction, iBMC);

			iBMC.CalculateToken(true);
			
			CMCacheEntry cMCacheEntry=new CMCacheEntry(iBMC.CID, iBMC);
			cMCacheEntry.CacheToken.CacheItemRequested();
			cMCache[iBMC.CID]=cMCacheEntry;

			modified = true;

			return iBMC;
		}

        private static void SaveInTransaction(ITransaction transaction, IBrokerManagedComponent iBMC)
        {
            if (transaction is TransactionWithCache)
                ((TransactionWithCache)transaction).SaveComponent(iBMC);
            else if (transaction is TransactionVelocity)
                ((TransactionVelocity)transaction).SaveComponent(iBMC);
        }

		public IBrokerManagedComponent CreateTransientComponentInstance(Guid typeID, ITransaction transaction)
		{
			CheckReaderLockHeld();

			CalculationTypePersist calculationTypePersist=new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction);
			
			if(transaction.SQLTransaction != null )
				calculationTypePersist.Transaction = transaction.SQLTransaction;
				
			return CMPersistenceController.Create(calculationTypePersist,typeID, true);
		}

		public IBrokerManagedComponent CreateTransientComponentInstance( string typeName, ITransaction transaction)
		{
			CheckReaderLockHeld();

			CalculationTypePersist calculationTypePersist=new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction);
			
			if(transaction.SQLTransaction != null )
				calculationTypePersist.Transaction = transaction.SQLTransaction;
				
			return CMPersistenceController.Create(calculationTypePersist,typeName, true);
		}

		public IBrokerManagedComponent CreateTransientComponentInstance( Guid cID,IComponentVersion componentVersion, string bmcName, ITransaction transaction)
		{
			CheckReaderLockHeld();

			CalculationTypePersist calculationTypePersist=new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction);
			
			if(transaction.SQLTransaction != null )
				calculationTypePersist.Transaction = transaction.SQLTransaction;
				
			return CMPersistenceController.Create(cID,componentVersion,string.Empty, true);
		}
		#endregion COMPONENT INSTANCE CREATION FUNCTIONS

		public IBrokerManagedComponent GetQualifiedList(Guid typeID, object qualifier, ITransaction transaction)
		{
			CheckReaderLockHeld();

			return CMPersistenceController.GetQualifiedList(typeID,qualifier,transaction);
		}

		public DataTable ListBMCCollection(CollectionSpecifier bMCCollectionSpecifier, ITransaction transaction)
		{
			CheckReaderLockHeld();
			
			return CMPersistenceController.ListBMCCollection(bMCCollectionSpecifier, transaction);
		}

		public IEnumerable ListAllCMTypes(ITransaction transaction)
		{
			CheckReaderLockHeld();

			CalculationTypePersist calculationTypePersist=new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction);
			if( transaction.SQLTransaction != null )
				calculationTypePersist.Transaction = transaction.SQLTransaction;
			return CMPersistenceController.ListAllCMTypes(calculationTypePersist);
		}

		public IEnumerable ListCMTypes(string categoryName, ITransaction transaction)
		{
			CheckReaderLockHeld();

			CalculationTypePersist calculationTypePersist=new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction);
			if( transaction.SQLTransaction != null )
				calculationTypePersist.Transaction = transaction.SQLTransaction;
			return CMPersistenceController.ListCMTypes(calculationTypePersist,categoryName);
		}

		public IEnumerable ListCMTypes(string[] categoryNames, ITransaction transaction)
		{
			CheckReaderLockHeld();

			CalculationTypePersist calculationTypePersist=new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction);
			if( transaction.SQLTransaction != null )
				calculationTypePersist.Transaction = transaction.SQLTransaction;
			return CMPersistenceController.ListCMTypes(calculationTypePersist,categoryNames);
		}

		public IEnumerable ListCMTypesByTypeName(string typeNames, ITransaction transaction)
		{
			CheckReaderLockHeld();

			CalculationTypePersist calculationTypePersist=new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction);
			if( transaction.SQLTransaction != null )
				calculationTypePersist.Transaction = transaction.SQLTransaction;
			return CMPersistenceController.ListCMTypesByTypeName(calculationTypePersist,typeNames);
		}

		public IEnumerable ListCMTypesByTypeID(Guid typeID, ITransaction transaction)
		{
			CheckReaderLockHeld();

			CalculationTypePersist calculationTypePersist=new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction);
			if( transaction.SQLTransaction != null )
				calculationTypePersist.Transaction = transaction.SQLTransaction;
			return CMPersistenceController.ListCMTypesByTypeID(calculationTypePersist,typeID);
		}

		public void DeleteAllTypes(ITransaction transaction)
		{
			CheckWriterLockHeld();

			CalculationTypePersist calculationTypePersist=new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction);
			if( transaction.SQLTransaction != null )
				calculationTypePersist.Transaction = transaction.SQLTransaction;
			CMPersistenceController.DeleteAllTypes(calculationTypePersist);
		}

		public Guid CIIDToCLID(Guid cIID, ITransaction transaction)
		{
			CheckReaderLockHeld();

			LCMListPersist lCMListPersist=new LCMListPersist(transaction.SQLConnection);
			if(  transaction.SQLTransaction != null )
				lCMListPersist.Transaction = transaction.SQLTransaction;
			return CMPersistenceController.GetCLID(lCMListPersist,cIID);
		}

		public void RegisterIDChange(Guid oldCID,Guid newCID,ITransaction transaction)
		{
			CheckWriterLockHeld();

			CMCacheEntry cacheEntry=cMCache[oldCID];
			cacheEntry.CIID=newCID;
			//When registering and ID change, also invalidate the Persisted primary dataset stored in cache object
			cacheEntry.PersistedPrimaryDataSet = null;
			
			cMCache.Remove(oldCID);
			int refCount = this.RemoveInstanceIDFromEnlistedItems(transaction.Broker, oldCID);

			cMCache[newCID]=cacheEntry;

            AddInstanceToEnlistedItems(transaction.Broker, cacheEntry.CM, refCount);
		}

		/// <summary>
		/// Copies an instance of a CM, giving it a new CIID, but CLID, CSID and ALL other data
		/// equal to the original CM.
		/// </summary>
		/// <param name="cIID">The CIID of the CM instance to be copied.</param>
		/// <param name="cSID"></param>
		/// <param name="transaction">The transaction under which the operation is to be carried out.</param>
		/// <returns>An interface to the new CM</returns>
		public ICalculationModule CopyCMInstance(Guid cIID,Guid cSID,ITransaction transaction)
		{
			CheckWriterLockHeld();

			CalculationTypePersist calculationTypePersist=new CalculationTypePersist(transaction.SQLConnection, transaction.SQLTransaction);
			ICalculationModule iCM=this.GetCMInstance(cIID,cSID,transaction);
			ICalculationModule newCM=CMPersistenceController.CloneCMInstance(iCM);

			newCM.CalculateToken(true);
			cMCache[newCM.CMID]=new CMCacheEntry(newCM.CMID,newCM);
			modified=true;

			AddInstanceToEnlistedItems(transaction.Broker, newCM, 1);
			return newCM;
		}

		/// <summary>
		/// Updates a CM instance from an old type to a new type, by returning a CM of the required new
		/// type populated with the data from the old instance.
		/// </summary>
		/// <param name="cIID">The implementation ID of the CM instance.</param>
		/// <param name="cTID">The required typeID of the new CM.</param>
		/// <param name="transaction">The transaction to which the operation belongs.</param>
		/// <returns>An interface to the new CM instance.</returns>
		public ICalculationModule UpdateCMInstance(Guid cIID,Guid cTID,ITransaction transaction)
		{
			CheckWriterLockHeld();

			// Get the original CM
			ICalculationModule cM=this.GetCMInstance(cIID,Guid.Empty,transaction);

			// Create a new instance of the required type.
			ICalculationModule newCM = this.CreateCMInstance( cTID, cM.Name, transaction, cM.CurrentBusinessStructureContext, cM.Broker, cM.CLID, cM.CSID );
			// Populate the new CM from the old data
			// Attempt to populate the new CM from the data in the old one by using the primary dataset if it is defined
			DataSet primaryDS = cM.PrimaryDataSet;
			if(null!=primaryDS)
			{
				cM.ExtractData(primaryDS);
				newCM.DeliverData(primaryDS);
				newCM.CIID=cM.CIID;
			}

			newCM.CalculateToken(true);
			cMCache[newCM.CIID]=new CMCacheEntry(newCM.CMID,newCM);
			modified=true;

			AddInstanceToEnlistedItems(transaction.Broker, newCM, 1);
			return newCM;
		}

		/// <summary>
		/// Deletes a Broker Managed Component from the system 
		/// </summary>
		/// <param name="cmId">The instance ID of the BMC</param>
		/// <param name="transaction">The transaction associated with the request</param>
		/// <remarks>Note that this overload ignores scenarios, so should not be called
		/// for Calculation Modules, only BMCs, for historical reasons this is named incorrectly</remarks>
		public void DeleteCMInstance(Guid cmId, ITransaction transaction)
		{
			CheckWriterLockHeld();

			CMCacheEntry cMCacheEntry=null;

			cMCacheEntry=cMCache[cmId];							// Obtain any existing cached instance

			if(cMCacheEntry == null) 
			{													// Action if CM is not already in the cache
				IBrokerManagedComponent iBMC=Load(cmId,transaction);

				if(null!=iBMC)									// Load the CM if it is available in the database
				{
					cMCacheEntry=cMCache[cmId];					// Obtain the cached instance
					transaction.Broker.ReleaseBrokerManagedComponent(iBMC);
				}
			}
			
			if(cMCacheEntry !=null)
			{
				AddInstanceToEnlistedItems(transaction.Broker, cMCacheEntry.CM, 1);
				cMCacheEntry.CM.DeletionOccurring();
			
				cMCacheEntry.Deleted=true;
				modified=true;
			}
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="cid"></param>
		/// <param name="transaction"></param>
		public void DeleteCMInstanceImmediate(Guid cid, ITransaction transaction)
		{
			ArrayList invalidDependencies = null;
			CheckWriterLockHeld();
			CMCacheEntry cMCacheEntry = cMCache[cid];							// Obtain any existing cached instance
		
			if(null==cMCacheEntry)												// Action if the CM is in the cache
				Load(cid,transaction);

            if (cMCacheEntry != null)
            {
                cMCacheEntry.CM.InPlaceUpgradeInProgress = ((CMBroker)transaction.Broker).InPlaceUpgradeInProgress;
                cMCacheEntry.CM.DeletionOccurring();
                cMCacheEntry.CM.InPlaceUpgradeInProgress = false;
            }

			CMPersistenceController.Delete(transaction.Broker, cMCacheEntry.CM, transaction, ref invalidDependencies);
			cMCache.Remove(cMCacheEntry.CIID);
			this.DeleteInvalidCacheEntries(transaction,ref invalidDependencies,true);
		}

		/// <summary>
		/// Deletes a Calculation Module from the system
		/// </summary>
		/// <param name="clid">The logical ID of the Calculation Module</param>
		/// <param name="csid">The scenario ID of the Calculation Module</param>
		/// <param name="transaction">The transaction associated with the request</param>
		public void DeleteCMInstance(Guid clid, Guid csid, ITransaction transaction)
		{
			CheckWriterLockHeld();

			ILogicalModule logicalModule = this.GetLogicalCM(clid, transaction);

			if(logicalModule != null)
			{
				ICalculationModule calculationModule = logicalModule[csid];

				if(calculationModule != null)
					this.DeleteCMInstance(calculationModule.CID, transaction);

				transaction.Broker.ReleaseBrokerManagedComponent(calculationModule);
			}

			transaction.Broker.ReleaseBrokerManagedComponent(logicalModule);
		}

		public void DeleteLogicalCM(Guid cLID, ITransaction transaction)
		{
			CheckWriterLockHeld();

			CMCacheEntry cMCacheEntry=null;

			cMCacheEntry=cMCache[cLID];							// Obtain any existing cached instance

			if(null!=cMCacheEntry)								// Action if the CM is in the cache
			{
				cMCacheEntry.Deleted=true;
				modified=true;
			}
			else
			{													// Action if CM is not already in the cache
				if(null!=Load(cLID,transaction))				// Load the CM if it is available in the database
				{
					cMCacheEntry=cMCache[cLID];					// Obtain the cached instance
					cMCacheEntry.Deleted=true;					// Mark it as deleted
					modified=true;
				}
			}

			AddInstanceToEnlistedItems(transaction.Broker, cMCacheEntry.CM, 1);
		}

		public IBrokerManagedComponent GetWellKnownBMC(WellKnownCM wellKnownCM,ITransaction transaction)
		{
            if (wellKnownCM == WellKnownCM.Organization)
                return GetOrganisationBMCInstance(transaction);

			string cmName;

			switch(wellKnownCM)
			{
				case WellKnownCM.Organization:
					cmName="Organization";
					break;
				case WellKnownCM.Licencing:
					cmName="Licence";
					break;
				case WellKnownCM.SecurityProvider:
					cmName="SAS70Password";
					break;
                case WellKnownCM.OBP:
                    cmName = "OBP";
                    break;
				default:
					return null;
			}
			
			IBrokerManagedComponent iBMC = null;
			CheckReaderLockHeld();
								
			DataRowCollection rowSet=(DataRowCollection)this.GetCMList(cmName, transaction);

			if(rowSet.Count==1)
				iBMC = this.GetBMCInstance((Guid)rowSet[0][DSCalculationModulesInfo.CMID_FIELD],transaction);
				
			return iBMC;
		}

        private IBrokerManagedComponent GetOrganisationBMCInstance(ITransaction transaction)
        {
            IBrokerManagedComponent iBMC = null;
            CheckReaderLockHeld();

            string userName = GetUserName();
            IDBUser currentUser = (IDBUser)this.GetBMCInstance(userName, "DBUser_1_1", transaction);

            if (currentUser != null)
            {
                Guid cid = currentUser.OrganisationCID;
                iBMC = this.GetBMCInstance(cid, transaction);
            }
            else
            {
                DataRowCollection rowSet = (DataRowCollection)this.GetCMList("Organization", transaction);
                if (rowSet.Count > 0)
                    iBMC = this.GetBMCInstance((Guid)rowSet[0][DSCalculationModulesInfo.CMID_FIELD], transaction);
            }

            return iBMC;
        }

        private string GetUserName()
        {
            HttpContext context = HttpContext.Current;
            if (context == null)
                return string.Empty;
            string userName = HttpContext.Current.User.Identity.Name;
            return userName;
        }

		public ICalculationModule GetWellKnownCM(WellKnownCM wellKnownCM,ITransaction transaction)
		{
			return (ICalculationModule) this.GetWellKnownCM(wellKnownCM, transaction);
		}

		public IBrokerManagedComponent GetBMCInstance(Guid cID, ITransaction transaction)
		{
            CheckReaderLockHeld();
            IBrokerManagedComponent bmc = Load(cID,transaction);
            return bmc;
		}

		public IBrokerManagedComponent GetBMCInstance(string bmcName, string strType, ITransaction transaction)
		{
			CheckReaderLockHeld();

            //CMListPersist cMListPersist = new CMListPersist(transaction.SQLConnection);
            CMListPersist cMListPersist = this.CreateCMListPersist(transaction.SQLConnection);

			if( transaction.SQLTransaction != null )
				cMListPersist.Transaction = transaction.SQLTransaction;

			DSBMCInfo dSBMCInfo=(DSBMCInfo)CMPersistenceController.ListBMCsOfName(cMListPersist, bmcName, strType );

			if(dSBMCInfo.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Count==0)
				return null;
			else
			{
				if( dSBMCInfo.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Count > 1 )
					throw new Exception( "CMCacheManager->GetBMCInstance( string, string, transaction) - More than one row returned for BMC : " + bmcName );
			}

			Guid cID = (Guid)dSBMCInfo.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0][DSBMCInfo.CMID_FIELD];

			return Load(cID,transaction);
		}

		public ICalculationModule GetCMInstance(Guid cIID, Guid cSID, ITransaction transaction)
		{
			CheckReaderLockHeld();
			
            ICalculationModule cm = (ICalculationModule)Load(cIID,transaction);
			if(cm!=null)
			{
				// If no CSID is specified (i.e. cSID is empty, use any existing cached CSID.
				if(cSID!=Guid.Empty)
				{
					if(cm.CSID != cSID)
					{
						cm.CSID=cSID;
						cm.CalculateToken(true);
					}
				}
			}
			return cm;
		}

		public ILogicalModule GetLogicalCM(Guid cLID,ITransaction transaction)
		{
			CheckReaderLockHeld();

			return (ILogicalModule)Load(cLID,transaction);
		}

		private IEnumerable GetCMList(string typeName, ITransaction transaction)
		{
            //using (CMListPersist cmListPersist = new CMListPersist(transaction.SQLConnection))

            CMListPersist cmListPersist = this.CreateCMListPersist(transaction.SQLConnection);
            using (cmListPersist)
            {
				if( transaction.SQLTransaction != null )
					cmListPersist.Transaction = transaction.SQLTransaction;

				DSBMCInfo dSBMCInfo=(DSBMCInfo)CMPersistenceController.ListBMCsOfTypeName( cmListPersist,typeName);
				return dSBMCInfo.Tables[DSBMCInfo.CMLIST_TABLE].Rows;
			}
		}


		//===========================================================================================
		// Catalog Functions
        public DataTable ListLCMsByParent(LCMListPersist lCMListPersist, Guid parentCLID, Guid parentCSID, ITransaction transaction)
		{
			CheckReaderLockHeld();
			DSLogicalModulesInfo dSLogicalModulesInfo=CMPersistenceController.ListLCMsByParent(lCMListPersist,parentCLID,parentCSID);
			return dSLogicalModulesInfo.Tables[DSLogicalModulesInfo.LCMLIST_TABLE];
		}

		public DataTable ListLCMsByParent(LCMListPersist lCMListPersist,Guid parentCLID,ITransaction transaction)
		{
			CheckReaderLockHeld();
			DSLogicalModulesInfo dSLogicalModulesInfo=CMPersistenceController.ListLCMsByParent(lCMListPersist,parentCLID);

			try
			{
				CMCacheManager.cacheLoadLock.AcquireReaderLock(-1);

				// Cache reconcilliation.
				// Firstly make sure that the rows retrieved from the database match the cache
				foreach(DataRow dataRow in dSLogicalModulesInfo.Tables[DSLogicalModulesInfo.LCMLIST_TABLE].Rows)
				{
					Guid cID=(Guid)dataRow[DSLogicalModulesInfo.LCM_CLID_FIELD];
					// Determine if a cache entry corresponds to the row from the database
					CMCacheEntry cMCacheEntry=this.cMCache[cID];
					// For the cache entry, determine if it has been deleted
					if(null!=cMCacheEntry && cMCacheEntry.Deleted)
						dataRow.Delete();		// If so, mark the row in the dataset as deleted
				}

				// Secondly, make sure that there are no matching objects in the cache that 
				// have been modified (or are new) and have not yet been persisted to the database.
				foreach(DictionaryEntry dictionaryEntry in cMCache)
				{
					CMCacheEntry cMCacheEntry=(CMCacheEntry)dictionaryEntry.Value;
					if(cMCacheEntry.CM.Modified && cMCacheEntry.CM.TypeName=="LogicalModuleBase" && !cMCacheEntry.Deleted )
					{
						ILogicalModule lcm=(ILogicalModule)cMCacheEntry.CM;
						if(lcm.ParentCLID==parentCLID)
						{
							DSLogicalModulesInfo dSLogicalModulesInfo1=new DSLogicalModulesInfo();
							cMCacheEntry.CM.ExtractData(dSLogicalModulesInfo1);
							dSLogicalModulesInfo.Merge(dSLogicalModulesInfo1,false,MissingSchemaAction.Ignore);
						}
					}
				}

				dSLogicalModulesInfo.AcceptChanges();
			}
			finally
			{
				CMCacheManager.cacheLoadLock.ReleaseLock();
			}

			return dSLogicalModulesInfo.Tables[DSLogicalModulesInfo.LCMLIST_TABLE];
		}

		public IEnumerable ListAllBMCs(CMListPersist cMListPersist,ITransaction transaction)
		{
			CheckReaderLockHeld();
			DSBMCInfo list=(DSBMCInfo)CMPersistenceController.ListAllBMCs(cMListPersist);

			try
			{
				CMCacheManager.cacheLoadLock.AcquireReaderLock(-1);
			
				// Cache reconcilliation.
				if(modified)	// Only perform cache reconciliation if the cache has been modified
				{					// in the current transaction.
					// Firstly make sure that the rows retrieved from the database match the cache
					foreach(DataRow dataRow in list.Tables[DSBMCInfo.CMLIST_TABLE].Rows)
					{
						Guid cID = (Guid) dataRow[DSBMCInfo.CMID_FIELD];

						// Determine if a cache entry corresponds to the row from the database
						CMCacheEntry cMCacheEntry=this.cMCache[cID];

						// For the cache entry, determine if it has been deleted
						if(null!=cMCacheEntry && cMCacheEntry.Deleted)
							dataRow.Delete();		// If so, mark the row in the dataset as deleted
					}

					// Secondly, make sure that there are no matching objects in the cache that 
					// have been modified (or are new) and have not yet been persisted to the database.
					foreach(DictionaryEntry dictionaryEntry in cMCache)
					{
						CMCacheEntry cMCacheEntry = (CMCacheEntry)dictionaryEntry.Value;
						if(cMCacheEntry.CM.Modified)
						{
							DSBMCInfo dSBMCInfo=new DSBMCInfo();
							cMCacheEntry.CM.ExtractData(dSBMCInfo);
							list.Merge(dSBMCInfo,false,MissingSchemaAction.Ignore);
						}
					}

					list.AcceptChanges();
				}
			}
			finally
			{
				CMCacheManager.cacheLoadLock.ReleaseLock();
			}

			return list.Tables[DSBMCInfo.CMLIST_TABLE].Rows;
		}

		public IEnumerable ListBMCsOfTypeID(CMListPersist cMListPersist,Guid typeId,ITransaction transaction)
		{
			CheckReaderLockHeld();
			DSBMCInfo list=(DSBMCInfo)CMPersistenceController.ListBMCsOfTypeID(cMListPersist,typeId);

			try
			{
				CMCacheManager.cacheLoadLock.AcquireReaderLock(-1);

				// Cache reconcilliation.
				if(modified)	// Only perform cache reconciliation if the cache has been modified
				{					// in the current transaction.
					// Firstly make sure that the rows retrieved from the database match the cache
					foreach(DataRow dataRow in list.Tables[DSBMCInfo.CMLIST_TABLE].Rows)
					{
						Guid cID=(Guid)dataRow[DSBMCInfo.CMID_FIELD];

						// Determine if a cache entry corresponds to the row from the database
						CMCacheEntry cMCacheEntry=this.cMCache[cID];

						// For the cache entry, determine if it has been deleted
						if(null!=cMCacheEntry && cMCacheEntry.Deleted)
							dataRow.Delete();		// If so, mark the row in the dataset as deleted
					}

					// Secondly, make sure that there are no matching objects in the cache that 
					// have been modified (or are new) and have not yet been persisted to the database.
					foreach(DictionaryEntry dictionaryEntry in cMCache)
					{
						CMCacheEntry cMCacheEntry=(CMCacheEntry)dictionaryEntry.Value;
						if(cMCacheEntry.CM.Modified && cMCacheEntry.CM.TypeID==typeId)
						{
							DSBMCInfo dSBMCInfo=new DSBMCInfo();
							cMCacheEntry.CM.ExtractData(dSBMCInfo);
							list.Merge(dSBMCInfo,false,MissingSchemaAction.Ignore);
						}
					}

					list.AcceptChanges();
				}
			}
			finally
			{
				CMCacheManager.cacheLoadLock.ReleaseLock();
			}

			return list.Tables[DSBMCInfo.CMLIST_TABLE].Rows;
		}

		public IEnumerable ListBMCsOfTypeName(CMListPersist cMListPersist,string typeName,ITransaction transaction)
		{
			CheckReaderLockHeld();
			DSBMCInfo list=(DSBMCInfo)CMPersistenceController.ListBMCsOfTypeName(cMListPersist,typeName);
			
			try
			{
				CMCacheManager.cacheLoadLock.AcquireReaderLock(-1);
			
				// Cache reconcilliation.
				if(modified)	// Only perform cache reconciliation if the cache has been modified
				{					// in the current transaction.
					// Firstly make sure that the rows retrieved from the database match the cache
					foreach(DataRow dataRow in list.Tables[DSBMCInfo.CMLIST_TABLE].Rows)
					{
						Guid cID=(Guid)dataRow[DSBMCInfo.CMID_FIELD];
						// Determine if a cache entry corresponds to the row from the database
						CMCacheEntry cMCacheEntry=this.cMCache[cID];
						// For the cache entry, determine if it has been deleted
						if(null!=cMCacheEntry && cMCacheEntry.Deleted)
						{
							dataRow.Delete();		// If so, mark the row in the dataset as deleted
						}
					}

					// Secondly, make sure that there are no matching objects in the cache that 
					// have been modified (or are new) and have not yet been persisted to the database.
					foreach(DictionaryEntry dictionaryEntry in cMCache)
					{
						CMCacheEntry cMCacheEntry=(CMCacheEntry)dictionaryEntry.Value;
						if(cMCacheEntry.CM.Modified && cMCacheEntry.CM.TypeName==typeName)
						{
							DSBMCInfo dSBMCInfo=new DSBMCInfo();
							cMCacheEntry.CM.ExtractData(dSBMCInfo);
							list.Merge(dSBMCInfo,false,MissingSchemaAction.Ignore);
						}
					}

					list.AcceptChanges();
				}
			}
			finally
			{
				CMCacheManager.cacheLoadLock.ReleaseLock();
			}

			return list.Tables[DSBMCInfo.CMLIST_TABLE].Rows;
		}

		//NOTE: This method is not cache reconciled.
		public IEnumerable ListConsolidationParents(CMListPersist cMListPersist,Guid memberCLID,ITransaction transaction)
		{
			CheckReaderLockHeld();
			DSBMCInfo list=(DSBMCInfo)CMPersistenceController.ListConsolidationParents(cMListPersist,memberCLID);

			return list.Tables[DSBMCInfo.CMLIST_TABLE].Rows;
		}

		//NOTE: This method is not cache reconciled.
		public IEnumerable ListConsolidationParents(CMListPersist cMListPersist,Guid memberCLID,Guid memberCSID,ITransaction transaction)
		{
			CheckReaderLockHeld();
			DSBMCInfo list=(DSBMCInfo)CMPersistenceController.ListConsolidationParents(cMListPersist,memberCLID, memberCSID);

			return list.Tables[DSBMCInfo.CMLIST_TABLE].Rows;
		}

		//NOTE: This method is not cache reconciled.
		public IEnumerable ListConsolidationMembers(CMListPersist cMListPersist,Guid parentCLID,Guid parentCSID,ITransaction transaction)
		{
			CheckReaderLockHeld();
			DSBMCInfo list=(DSBMCInfo)CMPersistenceController.ListConsolidationMembers(cMListPersist,parentCLID, parentCSID);

			return list.Tables[DSBMCInfo.CMLIST_TABLE].Rows;
		}

		public DataSet ListBMCDetailsOfTypeName(string typeName, object[] parameters, ITransaction transaction)
		{
			CheckReaderLockHeld();
			return CMPersistenceController.ListBMCDetailsOfTypeName(typeName, parameters, transaction);
		}

        public void UpdateCacheEntry(IBrokerManagedComponent bmc)
        {
            cMCache[bmc.CID] = new CMCacheEntry(bmc.CID, bmc);
        }

        public void UpdateCacheEntries(ITransaction transaction)
        {
            if (transaction is TransactionWithCache)
            {
                Hashtable components = ((TransactionWithCache)transaction).GetComponents();
                foreach (IBrokerManagedComponent bmc in components.Values)
                {
                    if (bmc != null && bmc.Modified)
                        UpdateCacheEntry(bmc);
                }
            }
        }
		public IBrokerManagedComponent Load(Guid instanceId,ITransaction transaction)
		{
            DateTime start = DateTime.Now;
            IBrokerManagedComponent iCM = null;

			try
			{
				CMCacheManager.cacheLoadLock.AcquireReaderLock(-1);

                if (transaction != null)//TRY TO GET BMC FROM CURRENT transaction    
                    iCM = transaction.GetComponent(instanceId);

                if (iCM == null)
                {
				    CMCacheEntry cMCacheEntry=null;
				    cMCacheEntry=cMCache[instanceId];					// Obtain any existing cached instance

                    if (null != cMCacheEntry)
                    {
                        this.LogCacheHit();
                        if (cMCacheEntry.Deleted == false)
                        {
                            cMCacheEntry.CacheToken.CacheItemRequested();
                            AddInstanceToEnlistedItems(transaction.Broker, cMCacheEntry.CM, 1);

                            ICalculationModule calcModule = cMCacheEntry.CM as ICalculationModule;
                            if (calcModule != null && calcModule.CSID == Guid.Empty)
                                Debug.WriteLine(string.Format("Problem:  CID '{0}', CLID '{1}', type '{2}'", calcModule.CID, calcModule.CLID, cMCacheEntry.CM.TypeName));
                            return cMCacheEntry.CM;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        cMCacheEntry = new CMCacheEntry(Guid.Empty, iCM);

                        cMCacheEntry.CacheToken.CacheItemLoadStart();


                        if (instanceId != Guid.Empty)
                            AddInstanceIDToEnlistedItems(transaction.Broker, instanceId, 1);
                        iCM = CMPersistenceController.Load(instanceId, transaction, ref cMCacheEntry.PersistedPrimaryDataSet);
                        if (transaction is TransactionWithCache)
                            ((TransactionWithCache)transaction).SaveComponent(iCM);

                        ICalculationModule calcModule = iCM as ICalculationModule;
                        if (calcModule != null && calcModule.CSID == Guid.Empty)
                            Debug.WriteLine(string.Format("CID '{0}'", calcModule.CID));

                        cMCacheEntry.CacheToken.CacheItemLoadEnd();

                        if (null != iCM)
                        {
                            // after loading a BMC, mark it as not modified as
                            // the act of loading should not modify a CM
                            // otherwise everything will be saved right after loading
                            // also mark reset token to original persisted token
                            iCM.MarkModuleAsUnmodified();

                            cMCacheEntry.CM = iCM;
                            cMCacheEntry.CIID = iCM.CID;
                            cMCacheEntry.CacheToken.CacheItemRequested();

                            CMCacheManager.cacheLoadLock.ReleaseLock();
                            CMCacheManager.cacheLoadLock.AcquireWriterLock(-1);

                            if (cMCache[iCM.CID] == null)
                            {
                                cMCache[iCM.CID] = cMCacheEntry;
                                this.LogCacheMiss();
                                return iCM;
                            }
                            else
                                return cMCache[instanceId].CM;
                        }
                        else
                        {
                            if (instanceId != Guid.Empty)
                                RemoveInstanceIDFromEnlistedItems(transaction.Broker, instanceId);

                            this.LogCacheMiss();
                            return null;
                        }
                    }
				}
                return iCM;
			}
			finally
			{
				CMCacheManager.cacheLoadLock.ReleaseLock();
                DateTime end = DateTime.Now;
                System.Diagnostics.Debug.WriteLineIf((end - start).TotalSeconds > 0, "Load: cid: " + instanceId + ", Time: " + (end - start));

			}
		}
        private void AddInstanceToEnlistedItems(ICMBroker broker, IBrokerManagedComponent bmc, int initialRefCount)
        {
            if (bmc == null)
                throw new ArgumentNullException("CMCacheManager.AddInstanceToEnlistedItems, the bmc can not be null");

            broker.Transaction.EnlistBMC(bmc, initialRefCount);
        }
        private void AddInstanceIDToEnlistedItems(ICMBroker broker, Guid cid, int initialRefCount)
        {
            if (cid == Guid.Empty)
                throw new ArgumentException("CMCacheManager.AddInstanceIDToEnlistedItems, the cid can not be empty");

            broker.Transaction.EnlistBMC(cid, initialRefCount);
        }

		private int RemoveInstanceIDFromEnlistedItems(ICMBroker broker, Guid cid)
		{
			if(cid == Guid.Empty)
				throw new ApplicationException("CMCacheManager.RemoveInstanceIDFromEnlistedItems, the CID cannot be an empty GUID");

			return broker.Transaction.DeEnlistBMC(cid);
		}

		internal void Remove(Guid cid)
		{
			cMCache.Remove(cid);
		}

		//=====================================================================
		// Cache Management Functions

		/// <summary>
		/// Returns the number of BMC's currently in the cache
		/// </summary>
		/// <returns>Count of BMC's</returns>
		public int Size()
		{
			CheckReaderLockHeld();
			try
			{
				CMCacheManager.cacheLoadLock.AcquireReaderLock(-1);
				int cacheSize=cMCache.Count;
				return cacheSize;
			}
			finally
			{
				CMCacheManager.cacheLoadLock.ReleaseLock();
			}
		}

		/// <summary>
		/// Removes all the entries currently in the cache.  All uncommitted changes are lost.
		/// </summary>
		/// <param name="transaction">The transaction in which the function executes.</param>
		/// <returns>Count of BMC's</returns>
		public void Clear(ITransaction transaction)
		{
			CheckWriterLockHeld();
			AssemblyDictionary.Delete();
			ComponentDictionary.Delete();
			cMCache.RemoveAll();
		}
		
		/// <summary>
		/// Removes a specific entry currently in the cache.  All uncommitted changes are lost.
		/// </summary>
		/// <param name="transaction">The transaction in which the function executes.</param>
		/// <param name="cID">The CID of the BMC instance to be removed.</param>
		/// <returns>Count of BMC's</returns>
		public void Clear(ITransaction transaction,Guid cID)
		{
			CheckWriterLockHeld();

			if(cMCache.Contains(cID))
			{
				CMCacheEntry cacheEntry = cMCache[cID];

				if(cacheEntry.CM.Modified)
					throw new InvalidOperationException("CMCacheManager.Remove, items that have been modified should not be removed as they need to be persisted first");
			}

			cMCache.Remove(cID);
			transaction.DeEnlistBMC(cID);
		}

		/// <summary>
		/// Returns true if the BMC identified is in the cache
		/// </summary>
		/// <param name="transaction">The transaction in which the function executes.</param>
		/// <param name="cID"></param>
		/// <returns>Count of BMC's</returns>
		public bool IsInCache(ITransaction transaction, Guid cID)
		{
			CheckReaderLockHeld();
			try
			{
				CMCacheManager.cacheLoadLock.AcquireReaderLock(-1);
				bool inCache=(null!=cMCache[cID]);
				return inCache;
			}
			finally
			{
				CMCacheManager.cacheLoadLock.ReleaseLock();
			}
		}

		#region Logging
		private static bool GetLoggingEnabled()
		{
            return "ON" == ConfigurationManager.AppSettings["CacheLoggingMode"];
		}

		private static string GetLogPath()
		{
            return ConfigurationManager.AppSettings["CacheLoggingPath"];
		}

		public void LogTransactionStart(ITransaction transaction)
		{
			WriteLogMessageLogLine("\r\nTransaction start:"+transaction.TID.ToString()+" at "+System.DateTime.Now.ToString()+ "ticks: "+System.DateTime.Now.Ticks.ToString());
		}

		public void LogText(string logText)
		{
			WriteLogMessageLogLine(logText);
		}

		public void LogAllocation(ITransaction transaction,ICalculationModule cM)
		{
			WriteLogMessageLogLine("\r\nCM Allocated to transaction:" + transaction.TID.ToString() + " " + cM.Name + " at " + System.DateTime.Now.ToString() + "ticks: " + System.DateTime.Now.Ticks.ToString());
		}

		public void LogAllocationBlocked(ITransaction transaction,Guid cIID)
		{
			WriteLogMessageLogLine("\r\nCM Blocked from to transaction:" + transaction.TID.ToString() + " " + cIID.ToString() + " at " + System.DateTime.Now.ToString() + "ticks: " + System.DateTime.Now.Ticks.ToString());
		}

		public void LogTransactionComplete(ITransaction transaction)
		{
			WriteLogMessageLogLine("\r\nTransaction complete:" + transaction.TID.ToString() + " at " + System.DateTime.Now.ToString() + "ticks: " + System.DateTime.Now.Ticks.ToString());
		}

		public void WriteLogMessageLogLine(string lineText)
		{
			try
			{
				if(cacheLoggingOn)
				{
					lock(this)
					{
						FileStream logStream=new FileStream(logPath,FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
						StreamWriter logWriter=new StreamWriter(logStream);
						logWriter.WriteLine(lineText);
						logWriter.Close();
						logStream.Close();
					}
				}
			}
			catch(Exception ex)
			{
				string message=ex.Message;
			}
		}
		#endregion // Logging
		#region Cache resizing
		private void CheckCacheFromTimer(object info)
		{
            //ArrayList invalidDependencies = null;

            cacheResizingTimer.Change(-1, -1);

            try
            {
                // only check cache if there is no lock for Velocity.
                //cacheLock.AcquireWriterLock(-1);
                //this.CheckCache(null, null, false, ref invalidDependencies);
            }
            finally
            {
                cacheLock.ReleaseLock();
            }

			cacheResizingTimer.Change(CACHEFREECHECKTIME, CACHEFREECHECKTIME);
		}

		internal void CheckCache(ITransaction transaction, CheckBMCCacheRemovalDelegate removalCheck, bool persistChangedObjects, ref ArrayList invalidDependencies)
		{
			// Sanity check
			const int MAXCOLLECTIONS = 5;
			int collectionCounter = 0;

			if( this.ShouldCollectCache() )
			{
				do
				{
					this.CollectCache(transaction, removalCheck, persistChangedObjects, ref invalidDependencies);

					if(!persistChangedObjects)
						GC.Collect();

					collectionCounter++;
				}
				while((!cacheCollectionTrigger.IsResizedCacheWithinParameters()) &&
					(collectionCounter <= MAXCOLLECTIONS));

				NablePerformanceCounters.GetCounter("BMC cache object collection").Increment();
			}
		}

		private bool ShouldCollectCache()
		{
			return cacheCollectionTrigger.CheckCacheCollectionTriggered();
		}

		private void CollectCache(ITransaction transaction, CheckBMCCacheRemovalDelegate removalCheck, bool persistChangedObjects, ref ArrayList invalidDependencies)
		{
			try
			{
				cacheLoadLock.AcquireWriterLock(-1);

				ArrayList cache = this.cMCache.GetCollectionList();

				double cacheReductionFactor = Convert.ToDouble(cacheCollectionTrigger.ReductionPercentage())/100;

				if(cacheReductionFactor < 1d &&
					cacheReductionFactor > 100d)
					throw new Exception("CMCacheManager.CollectCache, cacheCollectionTrigger.ReductionPercentage is not in acceptable range of 1 to 100");

				double currentCacheSize = Convert.ToDouble(cache.Count);
				int cacheEntriesToRemove = (int)(currentCacheSize * cacheReductionFactor);
			
				if(cacheEntriesToRemove > currentCacheSize)
					cacheEntriesToRemove = (int)currentCacheSize;

				ArrayList deletedCacheEntries = null;
				ArrayList refreshedCacheEntries = null;
			
				for(int cacheReductionCounter = 0; cacheReductionCounter < cacheEntriesToRemove; cacheReductionCounter++)
				{
					CMCacheEntry cacheEntry = (CMCacheEntry) cache[cacheReductionCounter];

					if(cacheEntry.CacheToken.CanBeRemoved() && 
						(removalCheck == null || removalCheck(cacheEntry.CIID)))
					{
						// this will only be set if we are collecting in a transaction
						// otherwise it has fired from the timer and no transaction
						// is in progress so there is no need to save any changes
						if(persistChangedObjects)
							this.CommitCacheEntry(transaction, cacheEntry, ref deletedCacheEntries, ref refreshedCacheEntries, ref invalidDependencies);

						this.LogCacheCollection();

						// De-enlist if the transaction is valid as we are removing the item from the cache
						if(transaction != null)
							transaction.DeEnlistBMC(cacheEntry.CIID);

						this.cMCache.Remove(cacheEntry.CIID);
					}

					// if this item was never cache, boost the number of cache entries to remove
					// so we can continue removing
					if(((cacheReductionCounter+1) >= cacheEntriesToRemove) &&
						CheckNeverCacheItems(cacheEntry.CacheToken))
					{
						cacheEntriesToRemove++;
					}
				}

				if(persistChangedObjects)
				{
					this.DeleteCacheEntries(transaction, ref deletedCacheEntries, ref invalidDependencies);
					this.RefreshCacheEntries(ref refreshedCacheEntries);
				}
			}
			finally
			{
				cacheLoadLock.ReleaseLock();
			}
		}

		private bool CheckNeverCacheItems(ICacheToken cacheToken)
		{
			return cacheToken.NeedsRemoving();
		}

		private void LogCacheHit()
		{
			this.PerformanceCounterIncrement("BMC cache hit");
		}

		private void LogCacheMiss()
		{
			this.PerformanceCounterIncrement("BMC cache miss");
		}

		private void LogCacheCollection()
		{
			this.PerformanceCounterIncrement("BMC cache object collection");
		}

		private void PerformanceCounterIncrement(string counterName)
		{
			NablePerformanceCounters.GetCounter(counterName).Increment();
		}

        private CMListPersist CreateCMListPersist(SqlConnection connection)
        {
            if (VelocityConfiguration.IsLogicalCMPersisterTableCached)
                return new CMListPersistVelocity(connection);
            else
                return new CMListPersist(connection);

        }
		#endregion // cache resizing
	}
}