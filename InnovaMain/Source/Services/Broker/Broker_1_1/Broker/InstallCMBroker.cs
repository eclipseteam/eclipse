using System;

namespace Oritax.TaxSimp.Services.CMBroker
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
	public class InstallCMBroker
	{
		#region INSTALLATION PROPERTIES

		public const string ASSEMBLY_ID="CFEAAB69-F37F-418b-8D17-2CC79D29AB66";
		public const string ASSEMBLY_NAME="CMBroker_1_1";
		public const string ASSEMBLY_DISPLAYNAME="CMBroker V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		public const string ASSEMBLY_REVISION="0";	

		public const string ASSEMBLY_PERSISTERASSEMBLY="CMBroker_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string ASSEMBLY_PERSISTERCLASS="Oritax.TaxSimp.Services.CMBroker.BrokerPersister";
		public const string	ASSOCIATEDFILE_WP_BROKERWEBSERVICE_FILENAME="BrokerWebService_1_1.asmx";

		#endregion
	}
}
