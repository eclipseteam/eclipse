using System;
using System.Collections;
using System.Runtime.Serialization;

using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Services.CMBroker
{
	/// <summary>
	/// Summary description for AssemblyDictionary.
	/// </summary>
	[Serializable]
	public class AssemblyDictionary : IAssemblyManagement
	{
		#region INTERNAL CLASSES

		[Serializable]
			public class VersionedTypeName
		{
			public class VersionedTypeNameException : Exception
			{
				public VersionedTypeNameException(string message):base(message){}
			}

			public string BaseName;
			public int MajorVersion;
			public int MinorVersion;
			public string Name;
			public string VersionedName;

			public bool IsVersioned()
			{
				return MajorVersion!=-1 && MinorVersion!=-1;
			}

			public VersionedTypeName(string name)
			{
				// first remove any path components of the name
				int lastBackslashIndex=name.LastIndexOf('\\');
				if(-1!=lastBackslashIndex)
				{
					name=name.Substring(lastBackslashIndex+1);
				}

				Name=name;
				int firstUnderScoreIndex=name.IndexOf('_');
				if(-1==firstUnderScoreIndex)
				{
					BaseName=name;
					MajorVersion=-1;
					MinorVersion=-1;
					VersionedName=name;
				}
				else
				{
					int secondUnderScoreIndex=name.IndexOf('_',firstUnderScoreIndex+1);
					if(-1==secondUnderScoreIndex || 0==secondUnderScoreIndex || secondUnderScoreIndex-firstUnderScoreIndex<2)
						throw new VersionedTypeNameException("Invalid versioned type name in constructor");				

					BaseName=name.Substring(0,firstUnderScoreIndex);
					try
					{
						MajorVersion=int.Parse(name.Substring(firstUnderScoreIndex+1,secondUnderScoreIndex-firstUnderScoreIndex-1));
						MinorVersion=int.Parse(name.Substring(secondUnderScoreIndex+1,name.Length-secondUnderScoreIndex-1));
					}
					catch
					{
						throw new VersionedTypeNameException("Invalid versioned type name in constructor");
					}

					VersionedName=Name;
				}
			}
		}

		[Serializable]
		public class AssemblyVersionNumber : IAssemblyVersionNumber
		{
			public struct ComparisonResult
			{
				public enum DifferenceColumnOption
				{
					NONE			=	0,
					MAJORVERSION	=	1,
					MINORVERSION	=	2,
					DATAFORMAT		=	3,
					REVISION		=	4
				}


				public bool MajorVersion;
				public bool MinorVersion;
				public bool DataFormat;
				public bool Revision;

				public ComparisonResult(bool majorVersion,bool minorVersion,bool dataFormat,bool revision)
				{
					MajorVersion=majorVersion;
					MinorVersion=minorVersion;
					DataFormat=dataFormat;
					Revision=revision;
				}

				public DifferenceColumnOption DifferenceColumn
				{
					get
					{
						if(MajorVersion)
							return DifferenceColumnOption.MAJORVERSION;
						if(MinorVersion)
							return DifferenceColumnOption.MINORVERSION;
						if(DataFormat)
							return DifferenceColumnOption.DATAFORMAT;
						if(Revision)
							return DifferenceColumnOption.REVISION;

						return DifferenceColumnOption.NONE;
					}
				}
			}


			private int majorVersion;
			private int minorVersion;
			private int dataFormat;
			private int revision;

			public int MajorVersion{get{return majorVersion;}set{majorVersion=value;}}
			public int MinorVersion{get{return minorVersion;}set{minorVersion=value;}}
			public int DataFormat{get{return dataFormat;}set{dataFormat=value;}}
			public int Revision{get{return revision;}set{revision=value;}}

			public override string ToString()
			{
				return majorVersion.ToString()+"."
						+minorVersion.ToString()+"."
						+dataFormat.ToString()+"."
						+revision;
			}

			public string ToStringMajMin()
			{
				return majorVersion.ToString()+"."
					+minorVersion.ToString();
			}

			public AssemblyVersionNumber(int majorVersion,int minorVersion,int dataFormat,int revision)
			{
				this.majorVersion=majorVersion;
				this.minorVersion=minorVersion;
				this.dataFormat=dataFormat;
				this.revision=revision;
			}

			public override bool Equals(object object1)
			{
				if(object1 is AssemblyVersionNumber)
				{
					AssemblyVersionNumber assemblyVersionNumber=object1 as AssemblyVersionNumber;
					return majorVersion==assemblyVersionNumber.majorVersion
						&& minorVersion==assemblyVersionNumber.minorVersion
						&& dataFormat==assemblyVersionNumber.dataFormat
						&& revision==assemblyVersionNumber.revision;
				}
				else
					return false;
			}

			public static ComparisonResult operator > (AssemblyVersionNumber assemblyVersionNumber1,AssemblyVersionNumber assemblyVersionNumber2)
			{
				return new ComparisonResult(
					assemblyVersionNumber1.majorVersion>assemblyVersionNumber2.majorVersion,
					assemblyVersionNumber1.minorVersion>assemblyVersionNumber2.minorVersion,
					assemblyVersionNumber1.dataFormat>assemblyVersionNumber2.dataFormat,
					assemblyVersionNumber1.revision>assemblyVersionNumber2.revision
				);
			}

			public static ComparisonResult operator < (AssemblyVersionNumber assemblyVersionNumber1,AssemblyVersionNumber assemblyVersionNumber2)
			{
				return new ComparisonResult(
					assemblyVersionNumber1.majorVersion<assemblyVersionNumber2.majorVersion,
					assemblyVersionNumber1.minorVersion<assemblyVersionNumber2.minorVersion,
					assemblyVersionNumber1.dataFormat<assemblyVersionNumber2.dataFormat,
					assemblyVersionNumber1.revision<assemblyVersionNumber2.revision
				);
			}

			public static bool operator == (AssemblyVersionNumber assemblyVersionNumber1,AssemblyVersionNumber assemblyVersionNumber2)
			{
				return assemblyVersionNumber1.Equals(assemblyVersionNumber2);
			}

			public static bool operator != (AssemblyVersionNumber assemblyVersionNumber1,AssemblyVersionNumber assemblyVersionNumber2)
			{
				return !assemblyVersionNumber1.Equals(assemblyVersionNumber2);
			}

			public override int GetHashCode()
			{
				return base.GetHashCode();
			}

		}


		[Serializable]
		public class Assembly : IAssembly
		{
			private Guid iD;
			private AssemblyVersionNumber versionNumber;
			private string name;
			private string displayName;
			private string strongName;

			public Guid ID{get{return iD;}set{iD=value;}}
			public IAssemblyVersionNumber VersionNumber{get{return versionNumber;}set{versionNumber=(AssemblyVersionNumber)value;}}
			public string Name{get{return name;}set{name=value;}}
			public string DisplayName{get{return displayName;}set{displayName=value;}}
			public string StrongName{get{return strongName;}set{strongName=value;}}

			public Assembly()
			{
				this.iD=Guid.Empty;
				this.versionNumber=new AssemblyVersionNumber(0,0,0,0);
				this.name="";
				this.displayName="";
				this.strongName="";
			}

			public Assembly(Guid iD,AssemblyVersionNumber versionNumber,string name,string displayName,string strongName)
			{
				this.iD=iD;
				this.versionNumber=versionNumber;
				this.name=name;
				this.displayName=displayName;
				this.strongName=strongName;
			}

			public override bool Equals(object object1)
			{
				if(object1 is Assembly)
				{
					Assembly assembly=object1 as Assembly;
					return iD==assembly.iD
						&& versionNumber==assembly.versionNumber
						&& name==assembly.name
						&& strongName==assembly.strongName;
				}
				else
					return false;
			}

			public static bool operator == (Assembly assembly1,Assembly assembly2)
			{
				if(((object)assembly1)==null && ((object)assembly2)==null)
					return true;
				if(((object)assembly1)==null || ((object)assembly2)==null)
					return false;

				return  assembly1.Equals(assembly2);
			}

			public static bool operator != (Assembly assembly1,Assembly assembly2)
			{
				if(((object)assembly1)==null && ((object)assembly2)==null)
					return false;
				if(((object)assembly1)==null || ((object)assembly2)==null)
					return true;

				return !assembly1.Equals(assembly2);
			}

			public override int GetHashCode()
			{
				return base.GetHashCode();
			}
		}


		[Serializable]
		protected class AssemblyIDDictionary : DictionaryBase
		{
			[Serializable]
			public class AssemblyIDDictionaryException : Exception
			{
				public AssemblyIDDictionaryException(string message)
					: base(message){}
			}

			public Assembly this[Guid iD]
			{
				get
				{
					return (Assembly)Dictionary[iD];
				}
			}

			public bool Contains(Guid iD)
			{
				return Dictionary.Contains(iD);
			}

			public void Add(Guid iD,IAssembly assembly)
			{
				Dictionary.Add(iD,assembly);
			}

			public void Replace(Guid iD,IAssembly assembly)
			{
				Dictionary[iD]=assembly;
			}

			public void Delete(Guid iD)
			{
				Dictionary.Remove(iD);
			}
		}


		[Serializable]
		public class AssemblyVersionSet : DictionaryBase,IAssemblyVersionSet,IEnumerator
		{
			[Serializable]
			public class AssemblyVersionSetException : Exception
			{
				public AssemblyVersionSetException(string message)
					: base(message){}

				protected AssemblyVersionSetException(SerializationInfo info, StreamingContext context)
					:base (info, context){}
			}

			private IEnumerator enumerator=null;

			public new int Count{get{return Dictionary.Count;}}

			public IAssembly Latest
			{
				get
				{
					Assembly latest=null;

					foreach(Assembly assembly in this)
					{
						if(latest==null)
							latest=assembly;
						else
						{
							AssemblyVersionNumber latestVersionNumber=(AssemblyVersionNumber)latest.VersionNumber;
							AssemblyVersionNumber versionNumber=(AssemblyVersionNumber)assembly.VersionNumber;
							if(latest==null || (AssemblyVersionNumber.ComparisonResult.DifferenceColumnOption.NONE!=(latestVersionNumber<versionNumber).DifferenceColumn))
								latest=assembly;
						}
					}

					return latest;
				}
			}

			public IAssembly this[IAssemblyVersionNumber assemblyVersionNumber]
			{
				get
				{
					string versionString=assemblyVersionNumber.ToStringMajMin();
					return (Assembly)Dictionary[versionString];
				}
			}

			public bool Contains(IAssemblyVersionNumber assemblyVersionNumber)
			{
				string versionString=assemblyVersionNumber.ToStringMajMin();
				return Dictionary.Contains(versionString);
			}

			public void Add(IAssemblyVersionNumber assemblyVersionNumber,IAssembly assembly)
			{
				string versionString=assemblyVersionNumber.ToStringMajMin();
				Dictionary.Add(versionString,assembly);
			}

			public void Replace(IAssemblyVersionNumber assemblyVersionNumber,IAssembly assembly)
			{
				string versionString=assemblyVersionNumber.ToStringMajMin();
				Dictionary[versionString]=assembly;
			}

			#region IEnumerable Members

			public new IEnumerator GetEnumerator()
			{
				this.enumerator=base.GetEnumerator();
				return this;
			}

			#endregion

			#region IEnumerator Members

			public void Reset()
			{
				if(this.enumerator==null)
					throw new InvalidOperationException("Enumerator is invalid");

				this.enumerator.Reset();
			}

			public object Current
			{
				get
				{
					if(this.enumerator==null)
						throw new InvalidOperationException("Enumerator is invalid");

					DictionaryEntry dictionaryEntry=(DictionaryEntry)enumerator.Current;
					return dictionaryEntry.Value as IAssembly;
				}
			}

			public bool MoveNext()
			{
				if(this.enumerator==null)
					throw new InvalidOperationException("Enumerator is invalid");

				return this.enumerator.MoveNext();
			}

			#endregion
		}


		[Serializable]
		protected class AssemblyNameDictionary : DictionaryBase
		{
			[Serializable]
			public class AssemblyNameDictionaryException : Exception
			{
				public AssemblyNameDictionaryException(string message)
					: base(message){}

				protected AssemblyNameDictionaryException(SerializationInfo info, StreamingContext context)
					:base (info, context){}
			}

			public AssemblyVersionSet this[string name]
			{
				get
				{
					return (AssemblyVersionSet)Dictionary[name];
				}
			}

			public bool Contains(string name)
			{
				return Dictionary.Contains(name);
			}

			private void Add(string name,AssemblyVersionSet assemblyVersionSet)
			{
				Dictionary.Add(name,assemblyVersionSet);
			}

			public void Add(string name,IAssemblyVersionNumber assemblyVersionNumber,IAssembly assembly)
			{
				if(Dictionary.Contains(name))
				{
					AssemblyVersionSet assemblyVersionSet=(AssemblyVersionSet)Dictionary[name];
					assemblyVersionSet.Add(assemblyVersionNumber,assembly);
				}
				else
				{
					AssemblyVersionSet assemblyVersionSet=new AssemblyVersionSet();
					assemblyVersionSet.Add(assemblyVersionNumber,assembly);
					this.Dictionary.Add(name,assemblyVersionSet);
				}
			}

			public void Replace(string name,IAssemblyVersionNumber assemblyVersionNumber,IAssembly assembly)
			{
				if(Dictionary.Contains(name))
				{
					AssemblyVersionSet assemblyVersionSet=(AssemblyVersionSet)Dictionary[name];
					assemblyVersionSet.Replace(assemblyVersionNumber,assembly);
				}
				else
				{
					throw new AssemblyNameDictionaryException("Entry for "+name+" does not exist");
				}
			}

			public void Delete(string name)
			{
				Dictionary.Remove(name);
			}
		}


		[Serializable]
		public class AssemblyArray : ArrayList
		{
			public new Assembly this[int index]
			{
				get
				{
					return (Assembly)base[index];
				}
			}

			public void Replace(IAssembly assembly)
			{
				for(int i=0;i<this.Count;i++)
				{
					if(((Assembly)this[i]).ID==assembly.ID)
					{
						base[i]=assembly;
					}
				}
			}
		}

		#endregion INTERNAL CLASSES

		#region FIELD VARIABLES

		protected AssemblyIDDictionary assemblyIDDictionary;
		protected AssemblyNameDictionary assemblyNameDictionary;
		protected AssemblyArray assemblyArray;
		protected static AssemblyDictionary dictionary;

		#endregion FIELD VARIABLES

		#region PROPERTIES

		public AssemblyArray Assemblies
		{
			get
			{
				return this.assemblyArray;
			}
		}

		#endregion PROPERTIES

		#region CONSTRUCTORS

		public AssemblyDictionary()
		{
			assemblyIDDictionary=new AssemblyIDDictionary();
			assemblyNameDictionary=new AssemblyNameDictionary();
			assemblyArray=new AssemblyArray();
		}

		#endregion CONSTRUCTORS

        public static AssemblyDictionary Dictionary(ITransaction transaction)
        {
            AssemblyDictionary.Dictionary(transaction, false);
            return dictionary;

        }
		public static AssemblyDictionary Dictionary(ITransaction transaction, bool fullVersion)
		{
			if(null==dictionary)
			{
                dictionary = new AssemblyDictionary();
                BrokerPersister brokerPersister = new BrokerPersister(transaction.SQLConnection, transaction.SQLTransaction);
                brokerPersister.LoadAssemblyDictionary(dictionary, fullVersion);
			}
			return dictionary;
		}
        //public virtual void LoadDictionary(ITransaction transaction)
        //{
        //    dictionary = new AssemblyDictionary();
        //    BrokerPersister brokerPersister = new BrokerPersister(transaction.SQLConnection, transaction.SQLTransaction);

        //    brokerPersister.LoadAssemblyDictionary(dictionary);
        //}
		public IAssembly this[Guid iD]
		{
			get
			{
				return assemblyIDDictionary[iD];
			}
		}

		public bool Contains(Guid iD)
		{
			return this.assemblyIDDictionary.Contains(iD);
		}

		public bool Contains(string name,IAssemblyVersionNumber assemblyVersionNumber)
		{
			if(this.assemblyNameDictionary.Contains(name))
			{
				return assemblyNameDictionary[name].Contains(assemblyVersionNumber);
			}
			else
				return false;
		}

		public void Add(IAssembly assembly)
		{
			this.assemblyIDDictionary.Add(assembly.ID,assembly);
			this.assemblyNameDictionary.Add(assembly.Name,assembly.VersionNumber,assembly);
			this.assemblyArray.Add(assembly);
		}

		public void Add(IAssembly assembly,string knownName)
		{
			this.assemblyIDDictionary.Add(assembly.ID,assembly);
			this.assemblyNameDictionary.Add(knownName,assembly.VersionNumber,assembly);
			this.assemblyArray.Add(assembly);
		}

		public void Replace(IAssembly assembly)
		{
			this.assemblyIDDictionary.Replace(assembly.ID,assembly);
			this.assemblyNameDictionary.Replace(assembly.Name,assembly.VersionNumber,assembly);
			this.assemblyArray.Replace(assembly);
		}

		public void Replace(IAssembly assembly,string knownName)
		{
			this.assemblyIDDictionary.Replace(assembly.ID,assembly);
			this.assemblyNameDictionary.Replace(knownName,assembly.VersionNumber,assembly);
			this.assemblyArray.Replace(assembly);
		}

		public void Delete(Guid iD)
		{
			Assembly assembly=assemblyIDDictionary[iD];
			this.assemblyArray.Remove(assembly);
			this.assemblyIDDictionary.Delete(iD);
			this.assemblyNameDictionary.Delete(assembly.Name);
		}

		public IAssembly GetAssembly(string name, IAssemblyVersionNumber assemblyVersionNumber)
		{
			return this.assemblyNameDictionary[name][assemblyVersionNumber];
		}

		public IAssemblyVersionSet GetAssemblyVersions(string name)
		{
			return this.assemblyNameDictionary[name];
		}

		public void Commit(ITransaction transaction)
		{
			BrokerPersister brokerPersister=new BrokerPersister(transaction.SQLConnection, transaction.SQLTransaction);

			brokerPersister.SaveAssemblyDictionary(this);
		}

		public virtual void Clear()
		{
			this.assemblyArray.Clear();
			this.assemblyIDDictionary.Clear();
			this.assemblyNameDictionary.Clear();
		}

		public static void Delete()
		{
			dictionary=null;
		}
	}

    /// <summary>
    /// This class contains all records from assembly table
    /// </summary>
    [Serializable]
    public class AssemblyFullDictionary : AssemblyDictionary
    {

    }
}
