using System.Reflection;
using System.Runtime.CompilerServices;

using Oritax.TaxSimp.Services.Caching;
using Oritax.TaxSimp.InstallInfo;

[assembly: AssemblyTitle("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]		
[assembly: AssemblyVersion(InstallCaching.ASSEMBLY_MAJORVERSION+"."+InstallCaching.ASSEMBLY_MINORVERSION)]
[assembly: AssemblyInformationalVersion(InstallCaching.ASSEMBLY_MAJORVERSION+"."+InstallCaching.ASSEMBLY_MINORVERSION+"."+InstallCaching.ASSEMBLY_DATAFORMAT+"."+InstallCaching.ASSEMBLY_REVISION)]
[assembly: AssemblyInstallInfo(InstallCaching.ASSEMBLY_ID,InstallCaching.ASSEMBLY_NAME,InstallCaching.ASSEMBLY_DISPLAYNAME)]
[assembly: AssemblyDelaySign(false)]
