using System;

namespace Oritax.TaxSimp.Services.Caching
{
	/// <summary>
	/// 
	/// </summary>
	public class InstallCaching
	{
		#region INSTALLATION PROPERTIES
		// Assembly Installation Properties
		public const string ASSEMBLY_ID="1AD04C61-8527-4a4a-AA21-16E52E90AC8D";
		public const string ASSEMBLY_NAME="Caching_1_1";
		public const string ASSEMBLY_DISPLAYNAME="Caching";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
//		public const string ASSEMBLY_REVISION="0"; 
		public const string ASSEMBLY_REVISION="1"; //2005.1
 		#endregion
	}
}
