using System;

namespace Oritax.TaxSimp.Services.Caching
{
	/// <summary>
	/// Cachability type is used in conjunction with CachabilityAttribute to indicate to the caching algorithm how the BMC should be cached
	/// </summary>
	public enum CacheabilityType
	{
		/// <summary>
		/// Don't ever cache the BMC
		/// </summary>
		NeverCache,
		/// <summary>
		/// Follow the normal caching algorithm
		/// </summary>
		NormalCache,
		/// <summary>
		/// Never remove the item from the cache
		/// </summary>
		AlwaysCache,
	}
}
