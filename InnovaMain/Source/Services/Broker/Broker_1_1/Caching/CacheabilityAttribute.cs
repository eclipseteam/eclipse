using System;

namespace Oritax.TaxSimp.Services.Caching
{
	/// <summary>
	/// Added to BMC classes to indicate the type of caching that should take place
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
	public class CacheabilityAttribute : Attribute
	{
		#region Private members
		private CacheabilityType cacheType;
		#endregion //Private members
		#region Constructors
		/// <summary>
		/// Creates an attribute with a CacheabilityType
		/// </summary>
		/// <param name="cacheType"></param>
		public CacheabilityAttribute(CacheabilityType cacheType)
		{
			this.cacheType = cacheType;
		}
		#endregion //Constructors
		#region Properties
		/// <summary>
		/// The CacheabilityType for this attribute
		/// </summary>
		public CacheabilityType CacheType
		{
			get
			{
				return this.cacheType;
			}
		}
		#endregion //Properties
	}
}
