using System;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Services.Caching
{
	/// <summary>
	/// Base class for all CacheCollectionTriggers, provides basic implementation
	/// </summary>
	public abstract class CacheCollectionTrigger : ICacheCollectionTrigger
	{
		#region Constructors
		public CacheCollectionTrigger()
		{
		}
		#endregion //Constructors
		#region ICacheCollectionTrigger Members
		/// <summary>
		/// When implemented by a derived class, indicates to the cache manager that the conditions for this trigger have been met and that the cache should be collected
		/// </summary>
		/// <returns>True if the conditions for collections are met, false otherwise</returns>
		public abstract bool CheckCacheCollectionTriggered();

		/// <summary>
		/// When implemented by a derived class, tells the cache manager what percentage of the cache entries to remove when collecting
		/// </summary>
		/// <returns>An integer between 1 and 100</returns>
		public abstract int ReductionPercentage();
		
		/// <summary>
		/// When implemented by a derived class, indicates to the cache manager that after collection if the resized cache is within acceptable parameters for the trigger
		/// </summary>
		/// <returns></returns>
		public abstract bool IsResizedCacheWithinParameters();
		#endregion //ICacheCollectionTrigger Members
	}
}
