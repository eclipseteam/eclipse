using System;

namespace Oritax.TaxSimp.Services.Caching
{
	/// <summary>
	/// An Interface representing all CacheTokens in the system
	/// </summary>
	public interface ICacheToken : IComparable
	{
		/// <summary>
		/// Indicates to the cache token that the cached item has started loading
		/// </summary>
		void CacheItemLoadStart( );
		
		/// <summary>
		/// Indicates to the cache token that the cache item has finished loading
		/// </summary>
		void CacheItemLoadEnd( );

		/// <summary>
		/// Indicates to the cache token that the cache item has been requested from the cache
		/// </summary>
		void CacheItemRequested( );

		/// <summary>
		/// Indicates to the cache token that the cache item has been released from the cache
		/// </summary>
		void CacheItemReleased( );

		/// <summary>
		/// Asks the cache token if a cached item can be removed
		/// </summary>
		/// <returns>True if the item can be removed, false otherwise</returns>
		bool CanBeRemoved( );

		/// <summary>
		/// Asks the cache token if a cached item must be removed
		/// </summary>
		/// <returns>True if the cached item must be removed, false otherwise</returns>
		bool NeedsRemoving( );

		/// <summary>
		/// Updates the cacheability type for a cache token
		/// </summary>
		/// <param name="cacheabilityType">The new cacheability type</param>
		void UpdateCacheabilityType(CacheabilityType cacheabilityType);
	}
}
