using System;
using System.Runtime.Serialization;

using Oritax.TaxSimp.Services.Caching;

namespace Oritax.TaxSimp.Services.Caching
{
	/// <summary>
	/// A base class CacheToken for all ICacheToken objects
	/// </summary>
    [Serializable]
    public abstract class CacheToken : ICacheToken
	{
		#region Private Members
        private CacheabilityType cacheabilityType;
		#endregion // Private Members
		#region Constructors
		/// <summary>
		/// Creates a cache token with a given CacheabilityType
		/// </summary>
		/// <param name="cacheabilityType">The CacheabilityType for the CacheToken</param>
		public CacheToken(CacheabilityType cacheabilityType)
		{
			this.cacheabilityType = cacheabilityType;
		}
		#endregion // Constructors
		#region Methods
		/// <summary>
		/// When overriden by a derived class, notifies the class that the cached item has begun loading
		/// </summary>
		public virtual void CacheItemLoadStart()
		{
		}

		/// <summary>
		/// When overriden by a derived class, notifies the class that the cached item has finished loading
		/// </summary>
		public virtual void CacheItemLoadEnd()
		{
		}

		/// <summary>
		///  When overriden by a derived class, notifies the class that the cached item has been requested
		/// </summary>
		public virtual void CacheItemRequested()
		{
		}

		/// <summary>
		/// When overriden by a derived class, notifies the class that the cached item has been released
		/// </summary>
		public virtual void CacheItemReleased()
		{
		}

		/// <summary>
		/// Determines if a given cached item can be removed from the cache
		/// </summary>
		/// <returns>True if the item can be removed, false otherwise</returns>
		public virtual bool CanBeRemoved()
		{
			if(this.cacheabilityType == CacheabilityType.AlwaysCache)
				return false;
			else if(this.cacheabilityType == CacheabilityType.NeverCache)
				return true;
			else
				return true;
		}

		/// <summary>
		/// Determines if a given cached item must be removed from the cache
		/// </summary>
		/// <returns>True if the item must be removed, false otherwise</returns>
		public virtual bool NeedsRemoving( )
		{
			return (this.cacheabilityType == CacheabilityType.NeverCache );
		}

		/// <summary>
		/// Updates the cacheability type for a token
		/// </summary>
		/// <param name="cacheabilityType"></param>
		public void UpdateCacheabilityType(CacheabilityType cacheabilityType)
		{
			this.cacheabilityType = cacheabilityType;
		}
		#endregion // Methods
		#region Properties
		/// <summary>
		/// Rereives the CacheabilityType for the token
		/// </summary>
		public CacheabilityType CacheabilityType
		{
			get
			{
				return this.cacheabilityType;
			}
		}
		#endregion // Properties
		#region Overridden Methods
		/// <summary>
		/// Tests if 2 cache tokens are equal
		/// </summary>
		/// <param name="obj">The cache token to compare against</param>
		/// <returns>True if the items are equal, false otherwise</returns>
		public override bool Equals(object obj)
		{
			if(obj == null)
				return false;

			if(!(obj is CacheToken))
				return false;

			if(CacheToken.ReferenceEquals(this, obj))
				return true;

			CacheToken cacheToken = (CacheToken) obj;

			if(this.cacheabilityType != cacheToken.CacheabilityType)
				return false;

			// if it passes all the tests then it must be the same
			return true;
		}

		/// <summary>
		/// Gets a hashcode for a CacheToken
		/// </summary>
		/// <returns>The hashcode for the CacheToken</returns>
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
		#endregion // Overridden Methods
		#region Operator Overloads
		/// <summary>
		/// Tests if 2 CacheTokens are equal
		/// </summary>
		/// <param name="token1">The first CacheToken to compare</param>
		/// <param name="token2">The second CacheToken to compare</param>
		/// <returns>True if the objects are equal, false otherwise</returns>
		public static bool operator == (CacheToken token1, CacheToken token2)
		{
			if(token1 == null &&
			   token2 == null)
				return true;
			else
				return token1.Equals(token2);
		}

		/// <summary>
		/// Tests if 2 CacheTokens are not equal
		/// </summary>
		/// <param name="token1">The first CacheToken to compare</param>
		/// <param name="token2">The second CacheToken to compare</param>
		/// <returns>True if the objects are not equal, false otherwise</returns>
		public static bool operator != (CacheToken token1, CacheToken token2)
		{
			if(token1 == null &&
			   token2 == null)
				return false;
			else
				return !token1.Equals(token2);
		}

		/// <summary>
		/// Tests if 1 CacheToken is less than another
		/// </summary>
		/// <param name="token1">The token to test if it is less than token2</param>
		/// <param name="token2">The CacheToken to compare against</param>
		/// <returns>True if token1 is less than token2</returns>
		public static bool operator < (CacheToken token1, CacheToken token2)
		{
			return (((int)token1.cacheabilityType) < ((int)token2.cacheabilityType));
		}

		/// <summary>
		/// Tests if 1 CacheToken is greater than another
		/// </summary>
		/// <param name="token1">The token to test if it is greater than token2</param>
		/// <param name="token2">The CacheToken to compare against</param>
		/// <returns>True if token1 is greater than token2</returns>
		public static bool operator > (CacheToken token1, CacheToken token2)
		{
			return (((int)token1.cacheabilityType) > ((int)token2.cacheabilityType));
		}
		#endregion // Operator Overloads
		#region IComparable Members
		/// <summary>
		/// Compares this object with another
		/// </summary>
		/// <param name="obj">The CacheToken to compare against</param>
		/// <returns>0 if the CacheToken is equal to this, -1 if CacheToken is less than this, 1 otherwise</returns>
		public virtual int CompareTo(object obj)
		{
			CacheToken cacheToken = (CacheToken)obj;

			if(obj.Equals(this))
				return 0;
			if(cacheToken < this)
				return -1;
			else
				return 1;
		}
		#endregion //IComparable Members
	}
}
