using System;
using System.Configuration;
using System.Reflection;

namespace Oritax.TaxSimp.Services.Caching
{
	/// <summary>
	/// The cache factory provides a mechanism to create configured cache triggers and tokens
	/// </summary>
	public class CacheFactory
	{
		#region ProviderConfiguration private struct
		/// <summary>
		/// Provider configuration gets a provider's configuration string and breaks it down into its constituent parts
		/// </summary>
		private struct ProviderConfiguration
		{
			private string assemblyName;
			private string className;

			/// <summary>
			/// Creates a ProviderConfiguration from a string
			/// </summary>
			/// <param name="providerConfiguration">The provider configuration created from the string</param>
			public ProviderConfiguration(string providerConfiguration)
			{
				string[] providerComponents = providerConfiguration.Split(';');

				if(providerComponents.Length < 2)
					throw new Exception("Configuration setting not in correct format, it should be in the format: Assembly;Classname, configuration provided: " + providerConfiguration );

				this.assemblyName = providerComponents[0];
				this.className = providerComponents[1];
			}

			/// <summary>
			/// The assembly name in the provider configuration
			/// </summary>
			public string AssemblyName
			{
				get
				{
					return this.assemblyName;
				}
			}

			/// <summary>
			/// The class name in the provider configuration
			/// </summary>
			public string ClassName
			{
				get
				{
					return this.className;
				}
			}
		}
		#endregion //ProviderConfiguration private struct
		#region Constructors
		private CacheFactory()
		{
		}
		#endregion //Constructors
		#region Public methods
		/// <summary>
		/// Creates a cache token based on the configured type
		/// </summary>
		/// <param name="cmType">The type of the CM, used to obtain information about the CacheabilityType</param>
		/// <returns>A new cache token</returns>
		public static ICacheToken GetCacheToken(Type cmType)
		{
			if(cmType == null)
				throw new ArgumentNullException("cmType", "CacheFactory.GetCacheToken, cmType cannot be null");
			
			string configuredCacheToken = ConfigurationManager.AppSettings["CachingCacheTokenProvider"];

			if(configuredCacheToken == null)
				throw new Exception("CacheFactory.GetCacheToken, setting CachingCacheTokenProvider is not set to a valid entry");

			ProviderConfiguration configuredCacheTokenProvider = new ProviderConfiguration(configuredCacheToken);
			object[] constructionArgs = new object[]{GetCacheability(cmType)};
			object component = CreateComponent(configuredCacheTokenProvider,constructionArgs);

			if(!(component is ICacheToken))
				throw new Exception( "ConfiguredCacheTokenComponent is not of type; ICacheToken, please correct the configuration file" );

			return (ICacheToken) component;
		}

		/// <summary>
		/// Creates a cache trigger based on the configured type
		/// </summary>
		/// <returns>A new ICacheCollectionTrigger</returns>
		public static ICacheCollectionTrigger GetCacheCollectionTrigger()
		{
            string configuredCollectionTrigger = ConfigurationManager.AppSettings["CachingCacheCollectionProvider"];

			if(configuredCollectionTrigger == null)
				throw new Exception("CacheFactory.GetCacheCollectionTrigger, setting CachingCacheCollectionProvider is not set to a valid entry");

			ProviderConfiguration configuredCollectionTriggerProvider = new ProviderConfiguration(configuredCollectionTrigger);
			object component = CreateComponent(configuredCollectionTriggerProvider,null);

			if(!(component is ICacheCollectionTrigger))
				throw new Exception( "CacheCollectionTriggerProvider is not of type; ICacheCollectionTrigger, please correct the configuration file" );

			return (ICacheCollectionTrigger) component;
		}

		/// <summary>
		/// Gets the cacheability of a given type
		/// </summary>
		/// <param name="type">The type to check for cachability</param>
		/// <returns>The CacheabilityType for the given BMC type</returns>
		public static CacheabilityType GetCacheability(Type type)
		{
			CacheabilityAttribute[] cacheabilityAttributes = (CacheabilityAttribute[]) type.GetCustomAttributes(typeof(CacheabilityAttribute),false);

			if(cacheabilityAttributes.Length > 0)
			{
				CacheabilityAttribute cacheabilityAttribute = cacheabilityAttributes[0];
				return cacheabilityAttribute.CacheType;
			}
			else
				return CacheabilityType.NormalCache;
		}
		#endregion //Public methods
		#region Private methods
		private static object CreateComponent(ProviderConfiguration providerConfiguration, object[] args)
		{
			Assembly assembly = Assembly.Load(providerConfiguration.AssemblyName);
			Type type = assembly.GetType(providerConfiguration.ClassName,true);
			return Activator.CreateInstance(type,args);
		}
		#endregion
	}
}
