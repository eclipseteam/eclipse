using System;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Services.Caching
{
	/// <summary>
	/// Interface representing cache collection triggers
	/// </summary>
	public interface ICacheCollectionTrigger
	{
		/// <summary>
		/// Indicates that the cache manager should collect items from the cache
		/// </summary>
		/// <returns>True if the cache manager should collect items from the cache, false otherwise</returns>
		bool CheckCacheCollectionTriggered();

		/// <summary>
		/// Tells the cache manager the percentage of items that should be removed from the cache
		/// </summary>
		/// <returns>A number between 1 and 100</returns>
		int ReductionPercentage();

		/// <summary>
		/// Indicates to the cache manager if the resized cache is now acceptable, if not then the collection is performed again
		/// </summary>
		/// <returns>True if the cache is now within parameters, false otherwise</returns>
		bool IsResizedCacheWithinParameters();
	}
}
