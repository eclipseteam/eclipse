using System;
using System.Configuration;

using Oritax.TaxSimp.Services.Caching;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Services.Caching.CacheTriggers
{
	/// <summary>
	/// A legacy CacheTrigger using the method of fixed size cache
	/// </summary>
	public class CacheSizeCacheTrigger : CacheCollectionTrigger
	{
		#region Private Members
		private int maximumCacheSize;
		private int reducedCacheSize;
		#endregion //Private Members
		#region Constructors
		/// <summary>
		/// Constructs a CacheSizeCacheTrigger
		/// </summary>
		public CacheSizeCacheTrigger()
		{
            string maxSize = ConfigurationManager.AppSettings["CachingCacheTriggerMaxCacheEntries"];
            string minSize = ConfigurationManager.AppSettings["CachingCacheTriggerMinCacheEntries"];

			if(maxSize == String.Empty ||
			   minSize == String.Empty)
				throw new Exception("CacheSizeCacheTrigger.CacheSizeCacheTrigger, CachingCacheTriggerMaxCacheEntries or CachingCacheTriggerMinCacheEntries is not specified in the web.config file");

			this.maximumCacheSize = Convert.ToInt32(maxSize);
			this.reducedCacheSize = Convert.ToInt32(minSize);

			if(this.reducedCacheSize > this.maximumCacheSize)
				throw new Exception("CacheSizeCacheTrigger.CacheSizeCacheTrigger(), CachingCacheTriggerMaxCacheEntries is less than CachingCacheTriggerMinCacheEntries");
		}
		#endregion //Constructors
		#region Public Methods
		/// <summary>
		/// Checks if the cache size has exceeded the maximum cache size set in web.config
		/// </summary>
		/// <returns>True if the number of items in the cache exceeds the threshold in web.config, false otherwise</returns>
		public override bool CheckCacheCollectionTriggered()
		{
			int currentCacheSize = CMCacheManager.CacheManager.Size();
			return(currentCacheSize > maximumCacheSize);
		}

		/// <summary>
		/// Checks if after resizing that the number of elements in the cache is below the minimum size
		/// </summary>
		/// <returns>True if the number of elements in the cache is below the minimum size, false otherwise</returns>
		public override bool IsResizedCacheWithinParameters()
		{
			int currentCacheSize = CMCacheManager.CacheManager.Size();
			return(currentCacheSize <= reducedCacheSize);
		}

		/// <summary>
		/// The percentage of elements that have to be removed to reduce the cache size below the minimum.
		/// </summary>
		/// <returns>The percentage of elements that have to be removed to reduce the cache size below the minimum</returns>
		public override int ReductionPercentage()
		{
			int currentCacheSize = CMCacheManager.CacheManager.Size();
			int cacheReductionNumber = currentCacheSize - reducedCacheSize;
			return Convert.ToInt32((Convert.ToDouble(cacheReductionNumber)/Convert.ToDouble(currentCacheSize))*100d)+2;
		}
		#endregion //Public Methods
	}
}
