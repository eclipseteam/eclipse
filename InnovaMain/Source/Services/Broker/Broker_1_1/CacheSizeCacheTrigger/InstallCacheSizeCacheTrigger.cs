using System;

namespace Oritax.TaxSimp.Services.Caching.CacheTriggers
{
	/// <summary>
	/// 
	/// </summary>
	public class InstallCacheSizeCacheTrigger
	{
		#region INSTALLATION PROPERTIES
		// Assembly Installation Properties
		public const string ASSEMBLY_ID="4CC7C130-D2AD-4690-8FF2-864658C4A824";
		public const string ASSEMBLY_NAME="CacheSizeCacheTrigger_1_1";
		public const string ASSEMBLY_DISPLAYNAME="CacheSizeCacheTrigger";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
//		public const string ASSEMBLY_REVISION="0";	
		public const string ASSEMBLY_REVISION="1";	//2005.1

		#endregion
	}
}
