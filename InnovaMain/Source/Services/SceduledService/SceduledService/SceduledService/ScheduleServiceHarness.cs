﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CalculationInterface;
using System.Threading;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Security;
using System.Net.Mail;
using System.Net;
using Amazon.SimpleNotificationService.Model;
using Amazon.SimpleNotificationService;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using System.Data;
using System.IO;
using System.Reflection;
using System.Net.Mime;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Configuration;
using Oritax.TaxSimp.Utilities;
using System.Xml.Linq;
using eclipseonlineweb.DesktopBrokerDataFeed;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM;

namespace Eclipse.ScheduledService
{
    public class ScheduleServiceHarness
    {
        static int percentageShown = 0;

        private string ConnectionString
        {
            get
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConfigurationManager.AppSettings["DBConnectionString"]);

                builder.Password = Encryption.DecryptData(builder.Password);
                builder.UserID = Encryption.DecryptData(builder.UserID);
                return builder.ConnectionString;
            }
        }
        
        public void ProcessArgument(string[] args)
        {
            Broker.SetStart(); 

            foreach (string argument in args)
            {
                switch (argument)
                {
                    case "ImportSMAClientMembersTransactionsLastFiveDays":
                        {
                            Console.WriteLine("Paramter is accepted: " + argument);
                            ProcessImportSMAClientMembersTransactionsLastFiveDays(); 
                        }
                        break;
                    case "ImportSMAClientMembersWithFULLData":
                        {
                            Console.WriteLine("Paramter is accepted: " + argument);
                            ProcessImportSMAClientMembersWithFULLData();
                        }
                        break;
                    case "ImportDesktopBrokerDataFeedImportFile":
                        {
                            Console.WriteLine("Paramter is accepted: " + argument);
                            ProcesASXTransactions(); 
                        }
                        break;
                    case "MaquarieBankTransactionsImport":
                        {
                            Console.WriteLine("Paramter is accepted: " + argument);
                            ProcesMaquarieBankTransactionsImport();
                        }
                        break;
                    case "ImportASXPriceHistoryFromYahooFinance":
                        {
                            Console.WriteLine("Paramter is accepted: " + argument);
                            ImportHistorialPrices();
                        }
                        break;
                    case "ImportSMAAdviserMap":
                        {
                            Console.WriteLine("Paramter is accepted: " + argument);
                            this.ProcessImportSMAAdviserMap();
                        }
                        break;
                    case "ImportSMAAccountStatus":
                        {
                            Console.WriteLine("Paramter is accepted: " + argument);
                            this.ProcessImportSMAAccountStatus();
                        }
                        break; 
                }
            }
        }

        public void ProcesMaquarieBankTransactionsImport()
        {
            this.Broker.SetStart();
            ImportProcessDS ds = new ImportProcessDS();
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.MaquarieBankTransactionsImport;
            ds.EndDate = DateTime.Now.AddDays(1);
            SaveOrganizanition(ds);

            ProcessEmail(ds, "Import Macquarie CMA Transactions Result");
        }
        
        private void SaveOrganizanition(DataSet ds)
        {
            this.Broker.SaveOverride = true;
            IBrokerManagedComponent organization = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IBrokerManagedComponent;
            organization.SetData(ds);
            this.Broker.SetComplete();
            this.Broker.SetStart();
        }

        public void ProcesASXTransactions()
        {
            this.Broker.SetStart();
            var distributionDictory = @"C:\Projects\InnovaProject\trunk\InnovaMain\Source\Services\SceduledService\SceduledService\SceduledService\Temp\";
            if (!Directory.Exists(distributionDictory))
            {
                Directory.CreateDirectory(distributionDictory);
            }

            var objDownload = new XMLDownloader
            {
                Url = ConfigurationManager.AppSettings["DBDataFeedURL"] + "&ReferenceID=&StartDate=&EndDate=" + DateTime.Now.ToString("dd-MM-yyyy")
            };
            string fileLocation = objDownload.MakeRequest(distributionDictory);

            ProcessASXFile(distributionDictory, fileLocation);
        }

        private static string GetDatafeedXml(FileInfo fileinfo)
        {
            var mergeXml = "";
            try
            {
                StreamReader reader = fileinfo.OpenText();
                string xml = reader.ReadToEnd();
                reader.Close();

                xml = xml.Replace(" xmlns=\"http://www.epistandard.com/epi/datafeed/4.0\"", "");

                var bellDirectXML = XElement.Parse(xml);

                var asxBrokers = from c in bellDirectXML.Elements("PlatformProviders")
                                     .Elements("PlatformProvider").Elements("Extracts")
                                     .Elements("Extract").Elements("Advisers").Elements("Adviser")
                                     .Elements("Accounts")
                                 select c;

                mergeXml = asxBrokers.Select(account => (from a in account.Elements("Account") select a)).Aggregate("<Datafeed> <Accounts>", (current1, accountXml) => accountXml.Aggregate(current1, (current, s) => current + s.ToString()));

                mergeXml += "</Accounts></Datafeed>";
            }
            catch (IOException)
            {
            }
            return mergeXml;
        }

        private void ProcessASXFile(string importFolder, string fileLocation)
        {
            FileInfo fileinfo = new FileInfo(fileLocation);
            string xml = GetDatafeedXml(fileinfo);

            if (string.IsNullOrEmpty(xml))
            {
                return;
            }
            // replacing & in data because it xml parser throughs exception
            xml = xml.Replace("&", "&amp;");

            ImportProcessDS ds = new ImportProcessDS();
            ds.ExtendedProperties.Add("Xml", xml);
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ValidateDesktopBrokerDataFeedImportFile;
            ds.FileName = fileinfo.Name;

            this.Broker.SaveOverride = true;
            IBrokerManagedComponent organization = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IBrokerManagedComponent;
            organization.SetData(ds);
            this.Broker.SetComplete();
            this.Broker.SetStart();

            string filename = Guid.NewGuid().ToString() + ".xml";
            using (StreamWriter writer = new StreamWriter(importFolder + filename, true))
            {
                writer.WriteLine(xml);
            }

            ds = new ImportProcessDS();
            xml = string.Empty;
            using (StreamReader reader = new StreamReader(importFolder + filename))
            {
                xml = reader.ReadToEnd();
            }
            ds.ExtendedProperties.Add("Xml", xml);
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportDesktopBrokerDataFeedImportFile;
            ds.FileName = filename;

            this.Broker.SaveOverride = true;
            organization = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IBrokerManagedComponent;
            organization.SetData(ds);
            this.Broker.SetComplete();
            this.Broker.SetStart();
            ds.MessageDetail = "Import all ASX transactions from Desktop Broker since inception";
            ds.MessageSummary = "Import all ASX transactions";
            ds.IsSuccess = true;
            ds.MessageNumber = "ASX01"; 
            ProcessEmail(ds, "Import ASX Tranactions Results");
        }

        public void ProcessImportSMAAccountStatus()
        {
            Broker.SetStart();

            SMAImportProcessDS ds = new SMAImportProcessDS();
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportSMAAccountStatus;
            IOrganization organization = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            organization.GetData(ds);
           
            ProcessEmail(ds, "Import SMA Account Status");
        }

        public void ProcessImportSMAAdviserMap()
        {
            Broker.SetStart();

            DBUser objUser = (DBUser)Broker.GetBMCInstance("Administrator", "DBUser_1_1");
            DBUserDetailsDS dBUserDetailsDS = new Oritax.TaxSimp.Security.DBUserDetailsDS();
            dBUserDetailsDS.UserList = true;
            objUser.GetData(dBUserDetailsDS);

            SMAImportProcessDS ds = new SMAImportProcessDS();
            ds.UserDetail = dBUserDetailsDS.Tables["UserDetail"];

            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportSMAAdviserMAP;
            IOrganization organization = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            organization.GetData(ds);
           
            ProcessEmail(ds, "Import Adviser Map SMA Results");
        }

        public void ProcessImportSMAClientMembersTransactionsLastFiveDays()
        {
            Broker.SetStart();
            SMAImportProcessDS ds = new SMAImportProcessDS();
            ds.StartDate = new DateTime(2011,7,1);
            ds.EndDate = DateTime.Now;
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportSMATransactions;

            IBrokerManagedComponent organization = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            organization.GetData(ds);
            ProcessEmail(ds, "Import Last 5 Days MIS Transactions and Since Inception Bank Transactions from SMA Results");
        }

        public void ProcessImportSMAClientMembersWithFULLData()
        {
            Broker.SetStart();
            ImportProcessDS ds = new ImportProcessDS();
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportSMAClientMembersWithFULLData;

            IBrokerManagedComponent organization = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            organization.GetData(ds);
            ProcessEmail(ds, "Import New SMA Accounts Result");
        }

        public void ProcessEmail(ImportProcessDS dataset, string subject)
        {
            List<String> emailList = new List<string>();
            emailList.Add("babar.ali@e-clipse.com.au");
            emailList.Add("support@e-clipse.com.au");
            emailList.Add("eprokopenko@e-clipse.com.au");
            emailList.Add("bwestbrook@innovapm.com.au");
            emailList.Add("kparrish@e-clipse.com.au");
            emailList.Add("auremovic@innovapm.com.au");
            
            foreach(string emailAdddress in emailList)
                SendEmailToDesitination(dataset, subject, emailAdddress);
        }

        private static void SendEmailToDesitination(ImportProcessDS dataset, string subject, string toAddress)
        {
            string awsAccessKey = "AKIAJ7MJ5JBFYSV4RQ3Q";
            string awsSecretKey = "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";
            AmazonSimpleEmailServiceClient ses = new AmazonSimpleEmailServiceClient(awsAccessKey, awsSecretKey);

            Amazon.SimpleEmail.Model.Message msg = new Amazon.SimpleEmail.Model.Message();
            msg.Subject = new Content(subject);
            Body body = new Body();

            string isSuccess = dataset.IsSuccess.ToString();
            string messageDetail = dataset.MessageDetail;
            string messageSummary = dataset.MessageSummary;
            string messageNumber = dataset.MessageNumber;


            Content content = new Content(@"<p style=""font-family:Calibri; font-size:medium"">Message Number:" + messageNumber + "<br/> Message Summary:" + messageSummary + "<br/> Message Detail:" + messageDetail + "<br/><br/>" + ConvertDataTableToHtml(dataset) + "</p>");

            //Email Recipients
            body.Html = content;
            msg.Body = body;
            Destination dest = new Destination();
            dest.ToAddresses.Add(toAddress);
            SendEmailRequest sendEmailRequest = new SendEmailRequest();
            sendEmailRequest.ReplyToAddresses.Add("support@e-clipse.com.au");
            sendEmailRequest.Source = "support@e-clipse.com.au";
            sendEmailRequest.ReturnPath = "support@e-clipse.com.au"; //Undeliverable messages get sent do this address
            sendEmailRequest.Message = msg;
            sendEmailRequest.Destination = dest;
            ses.SendEmail(sendEmailRequest);
        }

        public static string ConvertDataTableToHtml(DataSet targetDS)
        {
            if (targetDS.Tables.Count > 0)
            {
                DataTable targetTable = targetDS.Tables[0];
                string htmlString = "";

                if (targetTable == null)
                {
                    throw new System.ArgumentNullException("targetTable");
                }

                StringBuilder htmlBuilder = new StringBuilder();

                //Create Top Portion of HTML Document
                htmlBuilder.Append("Message:");
                htmlBuilder.Append("<br>");
                htmlBuilder.Append("<table border='1px' cellpadding='5' cellspacing='0' ");
                htmlBuilder.Append("style='border: solid 1px Black; font-size: small;'>");

                //Create Header Row
                htmlBuilder.Append("<tr align='left' valign='top'>");

                foreach (DataColumn targetColumn in targetTable.Columns)
                {
                    htmlBuilder.Append("<td align='left' valign='top'>");
                    htmlBuilder.Append(targetColumn.ColumnName);
                    htmlBuilder.Append("</td>");
                }

                htmlBuilder.Append("</tr>");

                //Create Data Rows
                foreach (DataRow myRow in targetTable.Rows)
                {
                    htmlBuilder.Append("<tr align='left' valign='top'>");

                    foreach (DataColumn targetColumn in targetTable.Columns)
                    {
                        htmlBuilder.Append("<td align='left' valign='top'>");
                        htmlBuilder.Append(myRow[targetColumn.ColumnName].ToString());
                        htmlBuilder.Append("</td>");
                    }

                    htmlBuilder.Append("</tr>");
                }

                //Create Bottom Portion of HTML Document
                htmlBuilder.Append("</table>");

                //Create String to be Returned
                htmlString = htmlBuilder.ToString();

                return htmlString;
            }
            else
                return string.Empty;
        }

        public static Boolean SendRawEmail(String from, String to, String cc, String Subject, String text, String html, String replyTo, String returnPath, Attachment objAttach)
        {
            AlternateView plainView = AlternateView.CreateAlternateViewFromString(text, Encoding.UTF8, "text/plain");
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html, Encoding.UTF8, "text/html");

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(from);
            List<String> toAddresses = to.Replace(", ", ",").Split(',').ToList();

            foreach (String toAddress in toAddresses)
            {
                mailMessage.To.Add(new MailAddress(toAddress));
            }

            List<String> ccAddresses = cc.Replace(", ", ",").Split(',').ToList();
            //foreach (String ccAddress in ccAddresses)
            //{
            //    mailMessage.CC.Add(new MailAddress(ccAddress));
            //}

            //List<String> bccAddresses = to.Replace(", ", ",").Split(',').ToList();
            //foreach (String bccAddress in bccAddresses)
            //{
            // mailMessage.Bcc.Add(new MailAddress(bccAddress));
            //}

            mailMessage.Subject = Subject;
            mailMessage.SubjectEncoding = Encoding.UTF8;

            if (replyTo != null)
            {
                mailMessage.ReplyTo = new MailAddress(replyTo);
            }

            if (text != null)
            {
                mailMessage.AlternateViews.Add(plainView);
            }

            if (html != null)
            {
                mailMessage.AlternateViews.Add(htmlView);
            }

            ContentDisposition cd = new ContentDisposition();
            objAttach.ContentType = new ContentType("application/x-rar-compressed");
            objAttach.ContentType = new ContentType("image/png");
            objAttach.ContentType = new ContentType("application/vnd.ms-excel");
            mailMessage.Attachments.Add(objAttach);

            RawMessage rawMessage = new RawMessage();

            using (MemoryStream memoryStream = ConvertMailMessageToMemoryStream(mailMessage))
            {
                rawMessage.WithData(memoryStream);
            }

            SendRawEmailRequest request = new SendRawEmailRequest();
            request.WithRawMessage(rawMessage);
            string awsAccessKey = "AKIAJ7MJ5JBFYSV4RQ3Q";
            string awsSecretKey = "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";
          
            request.WithDestinations(toAddresses);
            request.WithSource(from);
            AmazonSimpleEmailServiceClient serMailSer = new AmazonSimpleEmailServiceClient(awsAccessKey, awsSecretKey);

            try
            {
                SendRawEmailResponse response = serMailSer.SendRawEmail(request);

                SendRawEmailResult result = response.SendRawEmailResult;

                Console.WriteLine("Email sent.");
                Console.WriteLine(String.Format("Message ID: {0}", result.MessageId));

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }
        }

        public static MemoryStream ConvertMailMessageToMemoryStream(MailMessage message)
        {
            Assembly assembly = typeof(SmtpClient).Assembly;
            Type mailWriterType = assembly.GetType("System.Net.Mail.MailWriter");
            MemoryStream fileStream = new MemoryStream();
            ConstructorInfo mailWriterContructor = mailWriterType.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { typeof(Stream) }, null);
            object mailWriter = mailWriterContructor.Invoke(new object[] { fileStream });
            MethodInfo sendMethod = typeof(MailMessage).GetMethod("Send", BindingFlags.Instance | BindingFlags.NonPublic);
            sendMethod.Invoke(message, BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { mailWriter, true }, null);
            MethodInfo closeMethod = mailWriter.GetType().GetMethod("Close", BindingFlags.Instance | BindingFlags.NonPublic);
            closeMethod.Invoke(mailWriter, BindingFlags.Instance | BindingFlags.NonPublic, null, new object[] { }, null);
            return fileStream;
        }

        public UserEntity GetCurrentUser()
        {
            DBUser objUser = (DBUser)Broker.GetBMCInstance("Administrator", "DBUser_1_1");
            UserEntity UserEntity = new UserEntity();
            UserEntity.CID = objUser.CID;
            UserEntity.CurrentUserName = objUser.Name;
            UserEntity.IsAdmin = objUser.Administrator;
            UserEntity.IsWebUser = true;
            UserEntity.UserType = (int)objUser.UserType;
            this.Broker.ReleaseBrokerManagedComponent(objUser);

            return UserEntity;
        }

        public ScheduleServiceHarness()
        {
            if (Broker == null)
                Broker = new CMBroker(ConnectionString);
        }

        public void TestOrganisationLoad()
        {
            Broker.SetStart();
            IBrokerManagedComponent ibmc = Broker.GetWellKnownBMC(WellKnownCM.Organization); 
        }

        public ICMBroker Broker
        {
            get
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (brokerSlot == null)
                    return null;

                return (ICMBroker)Thread.GetData(brokerSlot);
            }
            set
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (brokerSlot == null)
                    brokerSlot = Thread.AllocateNamedDataSlot("Broker");

                if (value == null)
                    Thread.FreeNamedDataSlot("Broker");

                Thread.SetData(brokerSlot, value);
            }
        }

        #region Import Historical Prices from SS

        //public void ImportSSPriceFile()
        //{
        //    Ftp ftp = new Ftp();
        //    // Hook up progress event
        //    ftp.Progress += new EventHandler<ProgressEventArgs>(ftp_Progress);
        //    // Initialize session
        //    ftp.Session.RemoteEndPoint.HostNameOrAddress = "remote.fortnumhq.com.au";
        //    ftp.Session.Username = "eclipseadmin";
        //    ftp.Session.Password = "ftppass%01";
        //    //ftp.Session.Security.Protocols = System.Security.Authentication.SslProtocols.Default;
        //    // Get Remote Filename
        //    //string remoteFile = GetUserInput("Enter Remote Filename: ");

        //    Console.CursorVisible = false;

        //    // Connect to server
        //    Console.WriteLine("Connecting to server ...");
        //    ftp.Connect();

        //    // Login
        //    Console.WriteLine("Authenticating user ...");
        //    ftp.Authenticate();
        //}

        //static void ftp_Progress(object sender, ProgressEventArgs e)
        //{
        //    // Show a 'block' character for every other percentage increase
        //    // (Uses 50 characters total)
        //    if (Console.CursorLeft == 0)
        //    {
        //        Console.Write("[");
        //        Console.CursorLeft = 51;
        //        Console.Write("]");
        //        Console.CursorLeft = 1;
        //    }
        //    if (e.Item.Status == CopyStatus.InProgress)
        //    {
        //        int percentDiff = e.Item.Percentage - percentageShown;
        //        if (percentDiff > 0)
        //        {

        //            while (percentDiff > 1)
        //            {
        //                Console.Write((char)166);
        //                percentDiff -= 2;
        //                percentageShown += 2;
        //            }
        //            int cursorLeft = Console.CursorLeft;
        //            Console.CursorLeft = 54;
        //            Console.Write(string.Format("{0}%", e.Item.Percentage));
        //            Console.CursorLeft = cursorLeft;
        //        }
        //    }

        //}


        #endregion 

        #region Import Historical / Daily Prices

        public void ImportHistorialPrices()
        {
            Broker.SetStart();
            Broker.SaveOverride = true;

            ImportProcessDS ds = new ImportProcessDS();
            ds.Tables.Clear(); 

            DataTable dt = new DataTable("ResultTable");
            dt.Columns.Add("ASX Code");
            dt.Columns.Add("Company Name");
            dt.Columns.Add("Status");
            ds.Tables.Add(dt);

            ds.MessageNumber = "ASX History 01";
            ds.MessageSummary = "ASX History Load"; 
            ds.MessageDetail = "Import 3 years history for securities from EOD Data Services"; 


            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            EODWebserviceUtilities eodWebserviceUtilities = new EODWebserviceUtilities();
            eodWebserviceUtilities.Login(); 
            foreach (var securityEntity in orgCM.Securities)
                eodWebserviceUtilities.GetPriceHistory(ds, "ASX", securityEntity.AsxCode, "", securityEntity);

            orgCM.CalculateToken(true);
            Broker.SetComplete();
            Broker.SetStart();

            ProcessEmail(ds, "ASX History Load using EOD Data Services Result");
        }

        public static void CreateAsxPriceListFromYahoo(string csvStringUnitPriceFromYahoo, SecuritiesEntity secEntity)
        {
            if (!string.IsNullOrEmpty(csvStringUnitPriceFromYahoo) || !string.IsNullOrEmpty(csvStringUnitPriceFromYahoo.Trim()))
            {
                var sep = new[] { "\r", "\n" };
                var source = csvStringUnitPriceFromYahoo.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                DataTable dt = new DataTable();

                int count = 0;
                foreach (var line in source)
                {
                    if (count <= 1)
                    {
                        count++;
                        continue;
                    }

                    string[] splitName = line.Split(',');
                    DataRow dr = dt.NewRow();

                    DateTime priceDate = DateTime.Parse(splitName[0]);
                    var secHisEntity = secEntity.ASXSecurity.Where(secHis => secHis.Date.Date == priceDate.Date).FirstOrDefault();

                    if (secHisEntity == null)
                    {
                        ASXSecurityEntity securityEntity = new ASXSecurityEntity();
                        securityEntity.Date = priceDate.Date;
                        securityEntity.UnitPrice = Math.Round(double.Parse(splitName[4].ToString()), 6);
                        secEntity.ASXSecurity.Add(securityEntity);
                    }
                }
            }
        }

        /// <summary>
        /// Download ASX History
        /// </summary>
        /// <param name="ticker">Stock Symbol format e.g. CBA.AX, ANZ.AX</param>
        /// <param name="startMonth">Month no.</param>
        /// <param name="startDay">Day integer</param>
        /// <param name="startYear">Year Integer</param>
        /// <param name="secEntity"></param>
        /// <param name="ds"></param>
        public static void DownloadData(string ticker, int startMonth, int startDay, int startYear, SecuritiesEntity secEntity, ImportProcessDS ds)
        {
            DataRow row = ds.Tables[0].NewRow(); 
            row["ASX Code"] = secEntity.AsxCode;
            row["Company Name"] = secEntity.CompanyName;

            using (WebClient web = new WebClient())
            {
                try
                {
                    string data = web.DownloadString(string.Format("http://ichart.finance.yahoo.com/table.csv?s={0}&a={1}&b={2}&c={3}", ticker + ".AX", startMonth - 1, startDay, startYear));
                    CreateAsxPriceListFromYahoo(data, secEntity);
                    row["Status"] = "Account found and updated successfully";
                }
                catch (Exception ex)
                {
                    row["Status"] = "Account not found at Yahoo Finance";
                }
                finally
                {
                    ds.Tables[0].Rows.Add(row);
                }
            }
        }

        #endregion
    }
}
