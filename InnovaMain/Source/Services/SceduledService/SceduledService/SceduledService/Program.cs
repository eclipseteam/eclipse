﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Oritax.TaxSimp.CalculationInterface;

namespace Eclipse.ScheduledService
{
    public class Program
    {
      
        static void Main(string[] args)
        {
            ScheduleServiceHarness serviceHarness = new ScheduleServiceHarness();
            //serviceHarness.ImportHistorialPrices();
            serviceHarness.ProcessArgument(args);
        }
    }
}
