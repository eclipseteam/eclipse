﻿
namespace Oritax.TaxSimp.FormGenerators
{
    public interface IFormGeneratorFactory
    {
        IFormGenerator GetInstance(GeneratorType type);
    }
}
