﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.FormGenerators
{
    public enum GeneratorType
    {
        None = -1,
        Pdf = 5001,
        Excel2003,
        Excel2007,
        Document2003,
        Document2007,
        Text
    }

    public static class GeneratorTypeExtensions
    {
        public static GeneratorType ToGeneratorType(this string value)
        {
            GeneratorType type = GeneratorType.None;
            switch (value.ToLower())
            {
                case ".pdf": type = GeneratorType.Pdf; break;
            }
            return type;
        }
    }
}
