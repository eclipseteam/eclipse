﻿using System.Collections.Generic;

namespace Oritax.TaxSimp.FormGenerators
{
    public interface IFormGenerator
    {
        byte[] Generate(byte[] content, IDictionary<string, string> map);
    }
}
