﻿
using System.Collections.Generic;
using System;
namespace Oritax.TaxSimp.Commands
{
    public interface IGenericCommand<T>
    {
        T Entity { get; set; }
        string DoAction(string value);
        S DoAction<S>(string value) where S : class;
    }

    public interface ICommandFactory
    {
        IGenericCommand<T> GetCommand<T>(T entity, int command) where T : class, new();
    }

    public class CommandFactory : ICommandFactory
    {
        private IDictionary<int, Type> Commands = null;

        public IGenericCommand<T> GetCommand<T>(T entity, int command) where T : class, new()
        {
            IGenericCommand<T> generic = null;
            Type type ;
            Commands.TryGetValue(command, out type);
            if (type == null) return generic;

            generic = Activator.CreateInstance(type) as IGenericCommand<T>;
            generic.Entity = entity;

            return generic;
        }
        
        public static ICommandFactory GetFactory(IDictionary<int, Type> commands)
        {
            CommandFactory factory = new CommandFactory();
            factory.Commands = commands;
            return factory;
        }


    }
}
