﻿namespace Oritax.TaxSimp.Commands
{
    public abstract class GenericCommandBase<T> : IGenericCommand<T>
    {
        public virtual string DoAction(string value)
        {
            return string.Empty;
        }

        public virtual S DoAction<S>(string value) where S : class
        {
            return null;
        }

        public T Entity { get; private set; }

        T IGenericCommand<T>.Entity
        {
            get { return Entity; }
            set { this.Entity = value; }
        }
    }

}
