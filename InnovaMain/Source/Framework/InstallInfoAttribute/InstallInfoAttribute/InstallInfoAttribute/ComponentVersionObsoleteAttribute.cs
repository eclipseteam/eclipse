using System;

namespace Oritax.TaxSimp.InstallInfo
{
	/// <summary>
	/// Marks a component version as obsolete so it is not included in any calculations
	/// </summary>
	[AttributeUsage(AttributeTargets.Assembly)]
	public class ComponentVersionObsoleteAttribute : Attribute
	{
		public ComponentVersionObsoleteAttribute()
		{
		}
	}
}
