using System;

namespace Oritax.TaxSimp.InstallInfo
{
	/// <summary>
	/// Marks a component as obsolete so it is not included in any calculations
	/// </summary>
	[AttributeUsage(AttributeTargets.Assembly)]
	public class ComponentObsoleteAttribute : Attribute
	{
		public ComponentObsoleteAttribute()
		{
		}
	}
}
