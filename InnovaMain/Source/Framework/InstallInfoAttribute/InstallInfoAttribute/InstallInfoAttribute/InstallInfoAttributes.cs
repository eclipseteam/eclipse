using System;

namespace Oritax.TaxSimp.InstallInfo
{
	[Serializable]
	public class AssemblyInstallInfoAttribute : Attribute
	{
		public string	AssemblyID;
		public string	AssemblyName;
		public string	AssemblyDisplayName;

		public AssemblyInstallInfoAttribute(string assemblyID,string assemblyName,string assemblyDisplayName)
		{
			AssemblyID=assemblyID;
			AssemblyName=assemblyName;
			AssemblyDisplayName=assemblyDisplayName;
		}
	}

	[Serializable]
	public class ComponentInstallInfoAttribute : Attribute
	{
		public string	ComponentID;
		public string	ComponentName;
		public string	DisplayName;
		public string	Category;
		public int		Applicability = 3;

		public ComponentInstallInfoAttribute(string componentID,string componentName,string displayName,string category)
		{
			ComponentID=componentID;
			ComponentName=componentName;
			DisplayName=displayName;
			Category=category;
		}
		public ComponentInstallInfoAttribute(string componentID,string componentName,string displayName,string category, int applicability)
		{
			ComponentID=componentID;
			ComponentName=componentName;
			DisplayName=displayName;
			Category=category;
			Applicability=applicability;
		}
	}

	[Serializable]
	public class ComponentVersionInstallInfoAttribute : Attribute
	{
		public string	StartDate;
		public string	EndDate;
		public string	PersisterAssembly;
		public string	PersisterClass;
		public string	ImplementationClass;

		public ComponentVersionInstallInfoAttribute(string startDate,string endDate,string persisterAssembly,string persisterClass,string implementationClass)
		{
			StartDate=startDate;
			EndDate=endDate;
			PersisterAssembly=persisterAssembly;
			PersisterClass=persisterClass;
			ImplementationClass=implementationClass;
		}
	}

	[Serializable]
	public class AssociatedFileInstallInfoAttribute : Attribute
	{
		public class AssociatedFileEntry
		{
			public string	FileName;
			public string	RelativeInstallLocation;

			public AssociatedFileEntry(string fileName,string relativeInstallLocation)
			{
				FileName=fileName;
				RelativeInstallLocation=relativeInstallLocation;
			}
		}

		public AssociatedFileEntry[] AssociatedFileEntries;

		public AssociatedFileInstallInfoAttribute(AssociatedFileEntry[] associatedFileEntries)
		{
			this.AssociatedFileEntries=associatedFileEntries;
		}

		public AssociatedFileInstallInfoAttribute(string[] files,string[] locations,int count)
		{
			AssociatedFileEntries=new AssociatedFileEntry[count];
			for(int i=0;i<count;i++)
			{
				AssociatedFileEntries[i]=new AssociatedFileEntry(files[i],locations[i]);
			}
		}

		public AssociatedFileInstallInfoAttribute(string[] files,string[] locations)
		{
			if(files.Length!=locations.Length)
				throw new Exception("Invalid Associated Files Attribute information, no of files not equal to number of locations");

			AssociatedFileEntries=new AssociatedFileEntry[files.Length];
			for(int i=0;i<files.Length;i++)
			{
				AssociatedFileEntries[i]=new AssociatedFileEntry(files[i],locations[i]);
			}
		}
	}

	[Serializable]
	public class ComFileRegisterInfoAttribute : Attribute
	{
		public class ComFileEntry
		{
			public string	FileName;
			public string	RelativeInstallLocation;

			public ComFileEntry(string fileName,string relativeInstallLocation)
			{
				FileName=fileName;
				RelativeInstallLocation=relativeInstallLocation;
			}
		}

		public ComFileEntry[] ComFileEntries;

		public ComFileRegisterInfoAttribute(ComFileEntry[] comFileEntries)
		{
			this.ComFileEntries = comFileEntries;
		}

		public ComFileRegisterInfoAttribute(string[] files,string[] locations)
		{
			if(files.Length!=locations.Length)
				throw new Exception("Invalid Associated Files Attribute information, no of files not equal to number of locations");

			ComFileEntries = new ComFileEntry[files.Length];
			for(int i=0;i<files.Length;i++)
			{
				ComFileEntries[i] = new ComFileEntry(files[i],locations[i]);
			}
		}
	}

	[Serializable]
	public class DataModelInstallInfoAttribute : Attribute
	{
		public string	ModelManagerAssembly;
		public string	ModelManagerClass;

		public DataModelInstallInfoAttribute(string modelManagerAssembly,string modelManagerClass)
		{
			ModelManagerAssembly=modelManagerAssembly;
			ModelManagerClass=modelManagerClass;
		}
	}
}
