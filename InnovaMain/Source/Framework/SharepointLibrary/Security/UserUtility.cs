﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Oritax.TaxSimp.Security
{
    public class UserUtility
    {
        private const string CryptoKey = "87482909";

        private static string EncryptData(string strData)
        {
            string strResult;

            DESCryptoServiceProvider descsp = new DESCryptoServiceProvider();

            descsp.Key = ASCIIEncoding.ASCII.GetBytes(CryptoKey);
            descsp.IV = ASCIIEncoding.ASCII.GetBytes(CryptoKey);
            ICryptoTransform desEncrypt = descsp.CreateEncryptor();

            MemoryStream mOut = new MemoryStream();
            CryptoStream encryptStream = new CryptoStream(mOut, desEncrypt, CryptoStreamMode.Write);

            byte[] rbData = UnicodeEncoding.Unicode.GetBytes(strData);
            try
            {
                encryptStream.Write(rbData, 0, rbData.Length);
            }
            catch (Exception e)
            {
                throw new Exception("TXEncryption.Encrypt error: " + e.Message);
            }
            encryptStream.FlushFinalBlock();

            if (mOut.Length == 0)
                strResult = "";
            else
            {
                byte[] buff = mOut.ToArray();
                strResult = Convert.ToBase64String(buff, 0, buff.Length);
            }
            encryptStream.Close();

            return strResult;
        }

        private static string DecryptData(string strData)
        {
            string strResult;

            DESCryptoServiceProvider descsp = new DESCryptoServiceProvider();

            descsp.Key = ASCIIEncoding.ASCII.GetBytes(CryptoKey);
            descsp.IV = ASCIIEncoding.ASCII.GetBytes(CryptoKey);

            ICryptoTransform desDecrypt = descsp.CreateDecryptor();

            MemoryStream mOut = new MemoryStream();
            CryptoStream decryptStream = new CryptoStream(mOut, desDecrypt, CryptoStreamMode.Write);
            char[] carray = strData.ToCharArray();
            byte[] rbData = Convert.FromBase64CharArray(carray, 0, carray.Length);

            try
            {
                decryptStream.Write(rbData, 0, rbData.Length);
            }
            catch (Exception e)
            {
                throw new Exception("TXEncryption.Decrypt error: " + e.Message);
            }

            decryptStream.FlushFinalBlock();

            UnicodeEncoding aEnc = new UnicodeEncoding();
            strResult = aEnc.GetString(mOut.ToArray());

            decryptStream.Close();

            return strResult;
        }

        public static string GetUserPassword(string username)
        {
            string password = string.Empty;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConfigurationManager.AppSettings["DBConnectionString"]);
            builder.Password = DecryptData(builder.Password);
            builder.UserID = DecryptData(builder.UserID);
            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                SqlCommand command = new SqlCommand("SELECT Password FROM UserDetail WHERE Username = @Username", connection);
                command.Parameters.Add(new SqlParameter("@Username", username));
                connection.Open();
                object value = command.ExecuteScalar();
                if (value != null && value != DBNull.Value)
                {
                    password = DecryptData(value.ToString());
                }
                connection.Close();
            }
            return password;
        }
    }
}
