﻿using System;
using System.IO;
using System.Linq;
using System.Configuration;
using System.Xml.Serialization;

namespace Oritax.TaxSimp.Settings
{
    //internal static class SharepointServer
    //{
    //    public static readonly string Url = "http://spointpc:22333";
    //    public static readonly string Domain = "spointpc";
    //    public static readonly string AuthenticationMode = "forms";

    //    private static readonly List<string> _FileTypes = new List<string>(new string[] { "txt", "doc", "docx", "pdf" });
    //    public static bool IsValidFileType(string type)
    //    {
    //        return _FileTypes.Contains(type);
    //    }
    //}


    internal static class SharepointServer
    {
        public static readonly string AppSettingsKey = "SharepointSettingsPath";

        #region SharepointServerXml
        private static object LockXml = new object();
        private static SharepointServerXml _Xml;
        public static SharepointServerXml Xml
        {
            get
            {
                if (_Xml == null)
                {
                    lock (LockXml)
                    {
                        if (_Xml == null)
                        {
                            Refresh();
                        }
                    }
                }
                return _Xml;
            }
        }
        #endregion

        public static bool IsAvailable { get { return Xml.IsAvailable; } }
        public static string Url { get { return Xml.Url; } }
        public static string Domain { get { return Xml.Domain; } }
        public static string UserName { get { return Xml.UserName; } }
        public static string Password { get { return Xml.Password; } }
        public static string AuthenticationMode { get { return Xml.AuthenticationMode; } }
        public static string FileTypes { get { return Xml.FileTypes; } }

        public static bool IsValidFileType(string type)
        {
            return FileTypes.Split(';').Contains(type);
        }

        public static void Refresh()
        {
            string path = string.Format("{0}\\{1}", new FileInfo(Environment.CommandLine.Trim('"')).DirectoryName, ConfigurationManager.AppSettings[SharepointServer.AppSettingsKey]);  
            _Xml = SharepointServerXml.Load(path);
        }
    }

    [Serializable]
    [XmlRoot("Sharepoint")]
    public class SharepointServerXml
    {


        public SharepointServerXml() { }
        public bool IsAvailable { get; set; }
        public string Url { get; set; }
        public string Domain { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AuthenticationMode { get; set; }
        public string FileTypes { get; set; }

        public void Save(string file)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SharepointServerXml));
            using (FileStream stream = new FileStream(file, FileMode.Create, FileAccess.Write))
            {
                serializer.Serialize(stream, this);
            }
        }

        public static SharepointServerXml Load(string file)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SharepointServerXml));
            SharepointServerXml xml = null;
            using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                xml = serializer.Deserialize(stream) as SharepointServerXml;
            }
            return xml;
        }

        public static SharepointServerXml GetTest()
        {
            SharepointServerXml xml = new SharepointServerXml();
            xml.IsAvailable = true;
            xml.Url = "http://spointpc:22333";
            xml.Domain = "spointpc";
            xml.UserName = "Admin";
            xml.Password = "letmein1!";
            xml.AuthenticationMode = "forms";
            xml.FileTypes = "txt;doc;docx;pdf";
            return xml;
        }

    }
}
