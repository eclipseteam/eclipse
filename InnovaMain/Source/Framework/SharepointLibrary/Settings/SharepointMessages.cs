﻿using System.Collections.Generic;
using Oritax.TaxSimp.Proxies;

namespace Oritax.TaxSimp.Settings
{
    internal static class WebRequestHeaders
    {
        public static readonly NameValue FormAuthenticationFalse = new NameValue { Name = "X-FORMS_BASED_AUTH_ACCEPTED", Value = "f" };
    }

    internal class NameValue
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    internal static class SharepointMessages
    {
        public static readonly string FileTypeNotAllowed = "{0}.{1}  is not a valid file type. Only allow file types(txt, doc, docx, pdf)";
        public static readonly string FileUploaded = "{0} is uploaded successfully : Bytes saved {1}";
        public static readonly string FileDeleted = "{0} File is deleted successfully.";  
        public static readonly string LibraryCreated = "{0} Document Library Created Successfully.";
        public static readonly string LibraryDeleted  = "{0} Document Library Deleted Successfully.";
        public static readonly string AuthenticationModeError = "Authentication mode is not valid. Only allow Network or Forms Authentication";
        public static readonly string AuthenticationFailed = "Authentication Fails for the give user";
        public static readonly string LibraryDescription = "Created from Innova Sharepoint Proxy.";
        public static readonly string FileDeleted2 = string.Empty;
        public static readonly string FileDeleted1 = string.Empty;
    }
}
