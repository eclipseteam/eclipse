﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using Microsoft.SharePoint.Client;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Settings;
using Microsoft.SharePoint.Client.Utilities;

namespace Oritax.TaxSimp.Proxies
{
    public class SharepointField
    {
        public string Name { get; set; }
        public string FieldId { get; set; }
        public string XmlField { get; set; }
    }

    public static class SharepointFields
    {
        public static SharepointField PrimaryDocumentType = new SharepointField { Name = "Primary Document Type", FieldId = "Primary_x0020_Document_x0020_Type", XmlField = "<Field DisplayName='Primary Document Type' Type='Text' />" };
        public static SharepointField Received = new SharepointField { Name = "Received", FieldId = "Received", XmlField = "<Field DisplayName='Received' Type='DateTime' />" };
        public static SharepointField SenderName = new SharepointField { Name = "Sender Name", FieldId = "Sender_x0020_Name", XmlField = "<Field DisplayName='Sender Name' Type='Text' />" };
        public static SharepointField SenderEmailAddress = new SharepointField { Name = "Sender Email Address", FieldId = "Sender_x0020_Email_x0020_Address", XmlField = "<Field DisplayName='Sender Email Address' Type='Text' />" };
        public static SharepointField Subject = new SharepointField { Name = "Subject", FieldId = "Subject", XmlField = "<Field DisplayName='Subject' Type='Text' />" };
        public static SharepointField EmailId = new SharepointField { Name = "EmailId", FieldId = "EmailId", XmlField = "<Field DisplayName='EmailId' Type='Text' />" };
    }

    public static class SharepointProxy
    {
        #region File Methods

        public static List<SharepointFileInfo> GetFiles(string user, string library)
        {   
            List<SharepointFileInfo> list = new List<SharepointFileInfo>();            
            try
            {
                SharepointWork(user, context =>
                    {
                        Folder folder = context.Web.GetFolderByServerRelativeUrl(DocumentLibraryUrl(user, library));
                        var files = folder.Files;
                        var result = context.LoadQuery(files.Include(
                                    file => file.Name,
                                    file => file.Author.Title,
                                    file => file.ModifiedBy.Title,
                                    file => file.TimeCreated,
                                    file => file.TimeLastModified,
                                    file => file.ListItemAllFields
                                ));
                        context.ExecuteQuery();

                        foreach (var each in result)
                        {
                            object document = null;
                            each.ListItemAllFields.FieldValues.TryGetValue(SharepointFields.PrimaryDocumentType.FieldId, out document);
                            document = document ?? string.Empty;
                            list.Add(new SharepointFileInfo { Name = each.Name, Extension = new System.IO.FileInfo(each.Name).Extension, CreatedBy = each.Author.Title, CreatedAt = each.TimeCreated, ModifiedBy = each.ModifiedBy.Title, ModifiedAt = each.TimeLastModified, PrimaryDocumentType = document.ToString() });
                        }
                    });
            }
            catch (Exception ex)
            {                
                throw ex;
            }
            return list;
        }

        //private static string FormatUrl(string library)
        //{
        //    string[] specialChars = new string[] { "/", "\\", "&" };
        //    var _libname = library;

        //    foreach (string c in specialChars)
        //    {
        //        _libname = _libname.Replace(c, "");
        //    }


        //    _libname = HttpUtility.UrlPathEncode(_libname, true);
        //    return _libname;
        //}

        public static byte[] GetFileContent(string user, string library, string name)
        {
            byte[] bytes = null;
            SharepointWork(user, context =>
            {
                string path = string.Format("/{0}/{1}", library, name);
                using (FileInformation info = File.OpenBinaryDirect(context, path))
                {
                    using (System.IO.MemoryStream memory = new System.IO.MemoryStream())
                    {
                        info.Stream.CopyTo(memory);
                        memory.Position = 0;
                        bytes = new byte[memory.Length];
                        memory.Read(bytes, 0, (int)memory.Length);
                    }
                }
            });
            return bytes;
        }

        public static SerializedFile GetSerializedFile(string user, string library, string name)
        {
            byte[] bytes = GetFileContent(user, library, name);
            return new SerializedFile { Name = name, Type = string.Empty, Bytes = Convert.ToBase64String(bytes), PrimaryDocumentType = string.Empty };   
        }

        public static SharepointFileInfo UploadFile(string user, string library, SerializedFile file)
        {
            if (!SharepointServer.IsValidFileType(file.Type)) return null;
            SharepointFileInfo value = null;
            SharepointWork(user, context =>
            {
                Folder folder = context.Web.GetFolderByServerRelativeUrl(DocumentLibraryUrl(user, library));
                byte[] bytes = Convert.FromBase64String(file.Bytes);
                FileCreationInformation info = new FileCreationInformation();
                info.Content = bytes;
                info.Url = string.Format("{0}/{1}/{2}.{3}", SharepointServer.Url, DocumentLibraryUrl(user, library), file.Name, file.Type);
                info.Overwrite = true;
                File file1 = folder.Files.Add(info);
                file1.ListItemAllFields[SharepointFields.PrimaryDocumentType.FieldId] = file.PrimaryDocumentType;
                file1.ListItemAllFields.Update();
                folder.Update();
                context.ExecuteQuery();
                value = GetFile(context, DocumentLibraryUrl(user, library), string.Format("{0}.{1}", file.Name, file.Type));
            });
            return value;
        }

        public static SharepointFileInfo DeleteFile(string user, string library, string name)
        {
            SharepointFileInfo value = null;
            SharepointWork(user, context =>
            {
                value = GetFile(context, DocumentLibraryUrl(user, library), name);
                Folder folder = context.Web.GetFolderByServerRelativeUrl(DocumentLibraryUrl(user,library));
                var query = from f in folder.Files where f.Name == name select f;
                var result = context.LoadQuery<File>(query);
                context.ExecuteQuery();
                File delete = result.SingleOrDefault();
                delete.DeleteObject();
                folder.Update();
                context.ExecuteQuery();
            });
            return value;
        }

        #endregion

        #region Document Library Methods

        public static void CreateDocumentLibrary(string user, string library)
        {
            if (DocumentLibraryExists(user, library)) return;
            SharepointWork(user, context =>
            {
                ListCreationInformation info = new ListCreationInformation();
                info.Title = library;
                info.Description = string.Format("{0}.\r\n{1}", library, SharepointMessages.LibraryDescription);
                info.QuickLaunchOption = QuickLaunchOptions.On;
                info.TemplateType = (int)ListTemplateType.DocumentLibrary;                
                List list = context.Web.Lists.Add(info);
                list.Fields.AddFieldAsXml(SharepointFields.PrimaryDocumentType.XmlField, true, AddFieldOptions.AddFieldToDefaultView);
                list.Fields.AddFieldAsXml(SharepointFields.Received.XmlField, true, AddFieldOptions.AddFieldToDefaultView);
                list.Fields.AddFieldAsXml(SharepointFields.SenderEmailAddress.XmlField, true, AddFieldOptions.AddFieldToDefaultView);
                list.Fields.AddFieldAsXml(SharepointFields.Subject.XmlField, true, AddFieldOptions.AddFieldToDefaultView);
                list.Fields.AddFieldAsXml(SharepointFields.SenderName.XmlField, true, AddFieldOptions.AddFieldToDefaultView);
                list.Fields.AddFieldAsXml(SharepointFields.EmailId.XmlField, true, AddFieldOptions.AddFieldToDefaultView);
                context.ExecuteQuery();
            });
        }

        public static void DeleteDocumentLibrary(string user, string library)
        {
            SharepointWork(user, context =>
            {
                var query = (from i in context.Web.Lists where i.Title == library select i);
                var result = context.LoadQuery<List>(query);
                context.ExecuteQuery();
                var single = result.SingleOrDefault();
                single.DeleteObject();
                context.ExecuteQuery();
            });
        }

        public static List<string> GetDocumentLibraries(string user)
        {
            List<string> libraries = new List<string>();
            SharepointWork(user, context =>
            {
                var query = (from i in context.Web.Lists where i.Title != null && i.Description != null select i);
                var result = context.LoadQuery<List>(query);
                context.ExecuteQuery();
                foreach (List each in result)
                {
                    if (!each.Description.Contains(SharepointMessages.LibraryDescription)) continue;
                    libraries.Add(each.Title);
                }
            });
            return libraries;
        }

        public static bool DocumentLibraryExists(string user, string library)
        {
            bool exists = false;
            SharepointWork(user, context =>
            {
                try
                {
                    var query = from list in context.Web.Lists where list.Title == library select list;
                    var result = context.LoadQuery(query);
                    context.ExecuteQuery();
                    exists = result.SingleOrDefault() != null;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    exists = false;
                }
            });
            return exists;
        }

        public static string DocumentLibraryUrl(string user, string library)
        {
            string url = string.Empty;
            switch (library.ToLower())
            {
                case "adviser information\\template":
                case "adviser information\\research":
                    url = library;
                    break;
                default:
                    SharepointWork(user, context =>
                    {
                        try
                        {
                            var query = from list in context.Web.Lists where list.Title == library select list;
                            var result = context.LoadQuery(query);
                            context.ExecuteQuery();
                            url = result.SingleOrDefault().DefaultViewUrl;
                            url = url.Substring(1, url.Length - 1).Replace("/Forms/AllItems.aspx", "");
                            url = HttpUtility.UrlPathEncode(url, true);
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine(ex.Message);
                        }
                    });
                    break;
            }            
            return url;
        }

        #endregion

        #region Helper Methods

        private static SharepointFileInfo GetFile(ClientContext context, string library, string filename)
        {
            SharepointFileInfo value;
            Folder folder = context.Web.GetFolderByServerRelativeUrl(library);
            var query = from f in folder.Files where f.Name == filename select f;
            var result = context.LoadQuery(query.Include(
                        f => f.Name,
                        f => f.Author.Title,
                        f => f.ModifiedBy.Title,
                        f => f.TimeCreated,
                        f => f.TimeLastModified,
                        f => f.ListItemAllFields
                    ));
            context.ExecuteQuery();
            File file = result.SingleOrDefault();
            object document = file.ListItemAllFields[SharepointFields.PrimaryDocumentType.FieldId];
            value = new SharepointFileInfo { Name = file.Name, Extension = new System.IO.FileInfo(file.Name).Extension, CreatedBy = file.Author.Title, CreatedAt = file.TimeCreated, ModifiedBy = file.ModifiedBy.Title, ModifiedAt = file.TimeLastModified, PrimaryDocumentType =  document == null ? string.Empty : document.ToString()  };
            return value;
        }

        private static void SharepointWork(string user, Action<ClientContext> action)
        {
            if (!SharepointServer.IsAvailable) return; 
            using (ClientContext context = new ClientContext(SharepointServer.Url))
            {
                Authenticate(context, user);
                action(context);
            }
        }

        private static void Authenticate2(ClientContext context, string username)
        {
            switch (SharepointServer.AuthenticationMode)
            {
                case "network":
                    context.AuthenticationMode = ClientAuthenticationMode.Default;
                    context.Credentials = new NetworkCredential(SharepointServer.UserName, SharepointServer.Password, SharepointServer.Domain);
                    context.ExecutingWebRequest += (s, e) => e.WebRequestExecutor.WebRequest.Headers.Add(WebRequestHeaders.FormAuthenticationFalse.Name, WebRequestHeaders.FormAuthenticationFalse.Value);
                    break;
                case "forms":
                    string password = UserUtility.GetUserPassword(username);
                    if (string.IsNullOrEmpty(password)) throw new Exception(SharepointMessages.AuthenticationFailed);
                    context.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
                    context.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo(username, password);
                    break;
                default :
                    throw new Exception(SharepointMessages.AuthenticationModeError);
            }
        }

        private static void Authenticate(ClientContext context, string user)
        {
            string username = SharepointServer.UserName;
            string password = SharepointServer.Password;
            switch (SharepointServer.AuthenticationMode)
            {
                case "network":
                    context.AuthenticationMode = ClientAuthenticationMode.Default;
                    context.Credentials = new NetworkCredential(username, password, SharepointServer.Domain);
                    context.ExecutingWebRequest += (s, e) => e.WebRequestExecutor.WebRequest.Headers.Add(WebRequestHeaders.FormAuthenticationFalse.Name, WebRequestHeaders.FormAuthenticationFalse.Value);
                    break;
                case "forms":
                    context.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
                    context.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo(username, password);
                    break;
                default:
                    throw new Exception(SharepointMessages.AuthenticationModeError);
            }
        }

        #endregion

    }
}
