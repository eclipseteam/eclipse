﻿using System;

namespace Oritax.TaxSimp.Proxies
{
    [Serializable]
    public class SerializedFile
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Bytes { get; set; }
        public string PrimaryDocumentType { get; set; }
    }

    [Serializable]
    public class SharepointFileInfo
    {
        public string Name { get; set; }
        public string Extension { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedAt { get; set; }
        public string PrimaryDocumentType { get; set; }
    }
}
