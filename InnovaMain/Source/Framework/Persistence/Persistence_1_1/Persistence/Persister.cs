using System;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Collections;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;


namespace Oritax.TaxSimp.Persistence
{
	/// <summary>
	/// Summary description for Persister.
	/// </summary>
	public abstract class Persister : ICPersister, IDisposable
	{
		protected ICMBroker broker;
		private SqlConnection sqlConnection;
		private SqlTransaction sqlTransaction;

		public ICMBroker Broker{get{return broker;}set{broker=value;}}

		public Persister(SqlConnection connection, SqlTransaction transaction)
		{
			this.sqlConnection = connection;
			this.sqlTransaction = transaction;
		}

		public virtual DataTable ListBMCCollection(CollectionSpecifier bMCCollectionSpecifier)
		{
			DataTable collectionTable=null;
			Type type = this.GetType();
			MethodInfo method = type.GetMethod("ListBMCCollection", BindingFlags.NonPublic | BindingFlags.Instance,
				null, new Type[] {bMCCollectionSpecifier.GetType()}, null); 
			if (method != null)
			{
				collectionTable=(DataTable)method.Invoke(this, new object[] {bMCCollectionSpecifier});
			}

			return collectionTable;
		}

		public virtual SqlConnection Connection
		{
			get{return sqlConnection;}
			set{sqlConnection=value;}
		}
		public virtual SqlTransaction Transaction
		{
			get{return sqlTransaction;}
			set{sqlTransaction=value;}
		}
		public abstract IBrokerManagedComponent Get(Guid cID, ref DataSet persistedPrimaryDS);
		public abstract void EstablishDatabase();
		public abstract void RemoveOldDatabase();
		public abstract void ClearDatabase();
		public abstract void RemoveDatabase();
		public abstract void Delete(IBrokerManagedComponent iBMC);
		public abstract DataSet FindDetailsByTypeId(Guid typeId, object[] parameters);
		public abstract void Hydrate(IBrokerManagedComponent iBMC,Guid cID, ref DataSet persistedPrimaryDS);
		public abstract void Save(IBrokerManagedComponent iBMC, ref DataSet persistedPrimaryDS);
		
		public virtual IBrokerManagedComponent GetQualifiedList(object qualifier){return null;}
		public virtual void DeleteByQualifier(object qualifier){}

		/// <summary>
		/// Compares each row in the existing table with the new table.  For each row that is different, the row in the new table
		/// is copied into the existing table.  That row will be given the Modified State.  Any row that is the same as in the
		/// existing table will be removed from the existing table.  Rows that are deleted in the new table will be marked as deleted
		/// in the existing table.  Rows that are not present in the existing table will be added.
		/// </summary>
		/// <param name="newTable"></param>
		/// <param name="existingTable"></param>
		/// <param name="keyName"></param>
		protected void MarkChanges(DataTable newTable,DataTable existingTable,string keyName)
		{
			//Firstly create dataviews sorting both tables by Primary Key, ascending order
			DataView dViewNewTable = new DataView(newTable,String.Empty,keyName+" ASC",DataViewRowState.CurrentRows);
			DataView dViewExistingTable = new DataView(existingTable,String.Empty,keyName+" ASC",DataViewRowState.CurrentRows);

			int j = 0;
			int intMaxRowCount;
			//Need to cycle through all rows in both views
			//therefore get the largest views row count.
			if(dViewNewTable.Count > dViewExistingTable.Count)
				intMaxRowCount = dViewNewTable.Count;
			else
				intMaxRowCount = dViewExistingTable.Count;

			for(int i = 0; i < intMaxRowCount; i++)
			{
				//Make sure each view contains the current index before
				//retrieving the row view
				DataRowView dvNewRow = null;
				if(dViewNewTable.Count >= (i+1))
					dvNewRow= dViewNewTable[i];
				
				DataRowView dvExistingRow = null;
				if(dViewExistingTable.Count >= (j+1))
					dvExistingRow = dViewExistingTable[j];
				
				//Scenario 1: If there is an existing row but no new row
				//then the existing row must no longer exist
				//as part of the BMC therefore delete it.
				if( dvExistingRow != null  && dvNewRow == null)
				{
					dvExistingRow.Row.Delete();
					i--;
				}
				//Scenario 2: A matching row is found in both the new and the existing views.
				else if( (dvExistingRow != null) && dvNewRow[keyName].Equals(dvExistingRow[keyName]) )
				{
					//If the row exists in both views, then increment counter for existing table view
					j++;
					
					//Check that the row is not marked as deleted. If it is
					//then delete from the existing view also.
					if(dvNewRow.Row.RowState==DataRowState.Deleted)
						dvExistingRow.Row.Delete();
					else //Otherwise enumerate through the columns of the row comparing each value
					{
						//foreach(DataColumn dataColumn in dvNewRow.Row.Table.Columns)
						for(int k = 0; k < dvNewRow.Row.Table.Columns.Count; k++)
						{
							//string columnName=dataColumn.ColumnName;
							Object existingDataField=dvExistingRow.Row[k];
							Object newDataField=dvNewRow.Row[k];
							bool blEquals;

							if(newDataField.GetType() != typeof(System.Byte[]) 
								&& existingDataField.GetType() != typeof(System.Byte[]) )
								blEquals = newDataField.Equals(existingDataField);
							else
							{
								if(newDataField != DBNull.Value && existingDataField != DBNull.Value)
									blEquals = TokenEquals((byte[])newDataField,(byte[])existingDataField);
								else
									blEquals = false;
							}

							if((newDataField is DBNull) && !(existingDataField is DBNull)
								|| !(newDataField is DBNull) && (existingDataField is DBNull)
								|| !blEquals )
							{
								//If any column is different then set all the
								//columns in the row to that of the new row.
								//Then break out of the enumeration as the entire row's
								//ItemArray has now been copied.
								dvExistingRow.Row.ItemArray = dvNewRow.Row.ItemArray;
								break;
							}	
						}
					}
				}
				else
				{
					//Scenario 3:The row in the existing table is no longer found in the new table and needs to be deleted.
					if( (dvExistingRow != null) && String.Compare(dvExistingRow[keyName].ToString(), dvNewRow[keyName].ToString()) < 0 )
					{
						dvExistingRow.Row.Delete();
						//Decrement counter as row was deleted and need to retrieve new row at the same index.
						i--;
					}
					else //Scenario 4: The row in the new view does not exist in the existing view and needs to be added
					{
						//Add the new row to the existing datatable
						//existingTable.LoadDataRow(dvNewRow.Row.ItemArray, false);
                        DataRow newRow = existingTable.NewRow();
                        newRow.ItemArray = dvNewRow.Row.ItemArray;
                        existingTable.Rows.Add( newRow );

						j++;
					}
					
				}
				//Again find the max row count after any deletes and inserts have been performed.
				if(dViewNewTable.Count > dViewExistingTable.Count)
					intMaxRowCount = dViewNewTable.Count;
				else
					intMaxRowCount = dViewExistingTable.Count;
			}
		}

		private static bool TokenEquals(byte [] a1, byte [] a2)
		{
			Token objToken1 = new Token( a1 );
			Token objToken2 = new Token( a2 );

			return objToken1 == objToken2;
		}

		public virtual void EstablishDatabase(CMTypeInfo typeInfo){}
		public virtual void ClearDatabase(CMTypeInfo typeInfo){}
		public virtual void RemoveDatabase(CMTypeInfo typeInfo){}
		public virtual void FindDependentBMCs(Guid cID,ref ArrayList dependentCIDs, PersisterActionType actionType){}

		#region IDisposable Members

		public void Dispose()
		{
			// TODO:  Add Persister.Dispose implementation
		}

		#endregion
	}
}
