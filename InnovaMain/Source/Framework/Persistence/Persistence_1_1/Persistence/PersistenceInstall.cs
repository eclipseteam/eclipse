using System;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Summary description for BrokerManagedComponentBaseFW.
	/// </summary>
	public class PersistenceInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="BA1B9778-C76C-452d-8D6C-E2A659CC056A";
		public const string ASSEMBLY_NAME="Persistence_1_1";
		public const string ASSEMBLY_DISPLAYNAME="Persistence V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
//		public const string ASSEMBLY_REVISION="0";
		public const string ASSEMBLY_REVISION="1"; //2005.1

		#endregion
	}
}