using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Reflection;
using System.Diagnostics;
using System.Collections.Generic;
using Oritax.TaxSimp.BusinessStructureUtilities;

namespace Oritax.TaxSimp.Utilities
{
	[Serializable]
	public class SubscriptionsMap  
	{
        const string EMPTY_KEY = "Empty";
        private Hashtable map = new Hashtable();
        //private Dictionary<object, SubscriptionsList> map = new Dictionary<object,SubscriptionsList>();
        //private Dictionary<object, List<Subscription>> map = new Dictionary<object,List<Subscription>>();

		public SubscriptionsMap()
		{
		}


        //public void RefreshFromListByDataType(SubscriptionsList list)
        //{
        //    map.Clear();
        //    for (int iCount = 0; iCount < list.Count; iCount++)
        //    {
        //        Subscription subscription = (Subscription)list[iCount];
        //        AddToMap(subscription.DataType, subscription);
        //    }
        //}
        //public void RefreshFromListByCLID(SubscriptionsList list)
        //{
        //    map.Clear();
        //    for (int iCount = 0; iCount < list.Count; iCount++)
        //    {
        //        Subscription subscription = (Subscription)list[iCount];
        //        AddToMap(subscription.SubscriberCLID, subscription);
        //    }
        //}
        //public void RefreshFromListByCSID(SubscriptionsList list)
        //{
        //    map.Clear();
        //    for (int iCount = 0; iCount < list.Count; iCount++)
        //    {
        //        Subscription subscription = (Subscription)list[iCount];
        //        AddToMap(subscription.SubscriberCSID, subscription);
        //    }
        //}
         public virtual void AddToMap(object key, Subscription value)
        {
            if (key == null)
                Add(EMPTY_KEY, value);
            else if (key is string)
            {
                if (string.IsNullOrEmpty((string)key))
                    Add(EMPTY_KEY, value);
                else
                    Add(((string)key).ToUpper(), value);
            }
            else
                Add(key, value);
        }
        //public void RefreshFromList(SubscriptionsList list)
        //{
        //    map.Clear();
        //    for (int iCount = 0; iCount < list.Count; iCount++)
        //    {
        //        Subscription subscription = (Subscription)list[iCount];
        //        string key = subscription.DataType;
        //        if (string.IsNullOrEmpty(subscription.DataType))
        //            Add("EMPTY", subscription);
        //        else
        //            Add(subscription.DataType, subscription);
        //    }
        //}


        public virtual void Add(object key, Subscription value)
        {
            //if (key == null)
            //    key = EMPTY_KEY;
            if (map.ContainsKey(key))
            {
                //List<Subscription> list = map[key];
                //if (list.Contains(value))
                //    list.Remove(value);
                ((SubscriptionsList)map[key]).Add(value);
            }
            else
            {
                SubscriptionsList list = new SubscriptionsList();
                list.Add(value);
                map.Add(key, list);
            }
        }
        public virtual SubscriptionsList this[object key] 
        { 
            get 
            {
                SubscriptionsList list = null;

                if (key == null)
                {
                    if (map.ContainsKey(EMPTY_KEY))
                        list = (SubscriptionsList)map[EMPTY_KEY];
                }
                else if (key is string)
                {
                    string newKey = ((string)key).ToUpper();
                    if (newKey == "")
                        newKey = EMPTY_KEY;
                    if (map.ContainsKey(newKey))
                        list = (SubscriptionsList)map[newKey];
                }
                else if (map.ContainsKey(key))
                    list = (SubscriptionsList)map[key];
                return list;
            }
        } 

		public virtual int Count {get{return map.Count;}}
		public virtual int CountAll 
        {
            get
            {
                int count = 0;
                foreach (SubscriptionsList list in map.Values)
                {
                    count = count + list.Count;
                }
                return count;
            }
        }

		public virtual void Clear(){map.Clear();}
	
	}
}
