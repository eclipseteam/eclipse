using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Reflection;
using System.Diagnostics;
using Oritax.TaxSimp.BusinessStructureUtilities;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// This class is used to stored subscriptions. It is serialized when calculate token is 
	/// called on LogicalModule. However as the subscriptions collection can get very 
	/// large, to improve performance a timestamp is used and updated whenever a change
	/// is made to the collection and only this timestamp is serialized.
	/// </summary>

    [Serializable]
	public class SubscriptionsList : SerializableList, ISerializable
	{
        [NonSerialized]        SubscriptionsMap dataTypeMap = null;
        [NonSerialized]        SubscriptionsMap clidMap = null;
        [NonSerialized]        SubscriptionsMap csidMap = null;
        [NonSerialized]        SubscriptionsMap subscriberClidMap = null;
        [NonSerialized]        SubscriptionsMap subscribedClidMap = null;

        [NonSerialized] bool changedDataTypeMap = false;
        [NonSerialized] bool changedClidMap = false;
        [NonSerialized] bool changedCsidMap = false;
        [NonSerialized] bool changedSubscriberClidMap = false;
        [NonSerialized] bool changedSubscribedClidMap = false;
		uint updateVersion = 0;

		public SubscriptionsList() : base()
		{
		}
		protected SubscriptionsList(SerializationInfo si, StreamingContext context) : base(si, context)
		{
			updateVersion=Serialize.GetSerializedUInt32(si,"sl_updateVersion");
		}
	
		//Do not get data from the list for serialization, simply use the time stamp
		//to signal change in the CM.
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
            base.GetObjectData(si, context);
			Serialize.AddSerializedValue(si,"sl_updateVersion",updateVersion);
		}

        /// <summary>
        /// Dictionary of Subscription objects grouped by datatype.
        /// </summary>
        public SubscriptionsMap DataTypeMap
        {
            get
            {
                if (changedDataTypeMap || dataTypeMap == null)
                {
                    RefreshDataTypeMap();
                }
                return dataTypeMap;
            }
        }
        /// <summary>
        /// Dictionary of Subscription objects grouped by Subscriber CLID.
        /// </summary>
        public SubscriptionsMap ClidMap
        {
            get
            {
                if (changedClidMap || clidMap == null)
                {
                    RefreshClidMap();
                }
                return clidMap;
            }
        }
        /// <summary>
        /// Dictionary of Subscription objects grouped by Subscriber CLID.
        /// </summary>
        public SubscriptionsMap SubscriberClidMap
        {
            get
            {
                if (changedSubscriberClidMap || subscriberClidMap == null)
                {
                    RefreshSubscriberClidMap();
                }
                return subscriberClidMap;
            }
        }
        /// <summary>
        /// Dictionary of Subscription objects grouped by Subscribed CLID.
        /// </summary>
        public SubscriptionsMap SubscribedClidMap
        {
            get
            {
                if (changedSubscribedClidMap || subscribedClidMap == null)
                {
                    RefreshSubscribedClidMap();
                }
                return subscribedClidMap;
            }
        }
        /// <summary>
        /// Dictionary of Subscription objects grouped by Subscriber CSID.
        /// </summary>
        public SubscriptionsMap CsidMap
        {
            get
            {
                if (changedCsidMap || csidMap == null)
                {
                    RefreshCsidMap();
                }
                return csidMap;
            }
        }

        private bool Changed
        {
            get
            {
                return changedClidMap || changedCsidMap || changedDataTypeMap 
                    || changedSubscriberClidMap || changedSubscribedClidMap;
            }
            set
            {
                changedClidMap = value;
                changedCsidMap = value;
                changedDataTypeMap = value;
                changedSubscriberClidMap = value;
                changedSubscribedClidMap = value;
            }
        }
		public override int Add(object addObject)
		{
            Changed = true;
			unchecked{updateVersion++;}
			return base.Add(addObject);
		}

		public override void Clear()
		{
            Changed = true;

			unchecked{updateVersion++;}
			base.Clear();
		}
		public override void Insert(int index,object value)
		{
            Changed = true;
			unchecked{updateVersion++;}
			base.Insert(index,value);
		}
		public override void Remove(object value)
		{
            Changed = true;
			unchecked{updateVersion++;}
			base.Remove(value);
		}

		public override void RemoveAt(int index)
		{
            Changed = true;
			unchecked{updateVersion++;}
			base.RemoveAt(index);
		}
		/// <summary>
		/// Returns count
		/// </summary>
		public override int Count
		{
			get
			{
				return base.Count;
			}
		}


		/// <summary>
		/// Returns filtered subscription list based on current CSID, this is matched with SubscriberCSID
		/// </summary>
		/// <param name="subscriberCSID"></param>
		/// <returns></returns>
        //public SubscriptionsList FilterSubscriptions(Guid subscriberCSID)
        //{
        //    SubscriptionsList filteredlist = new SubscriptionsList();
        //    foreach (Subscription subscription in this)
        //    {
        //        if (subscription.SubscriberCSID == subscriberCSID)
        //            filteredlist.Add(subscription); 
        //    }
            
        //    return filteredlist;
        //}
        public SubscriptionsList FilterSubscriptions(Guid subscriberCSID)
        {
            SubscriptionsList list = CsidMap[subscriberCSID];
            if (list == null)
                list = new SubscriptionsList();
            return list;
        }

        private void RefreshMaps()
        {
            ClearMap(ref dataTypeMap);
            ClearMap(ref clidMap);
            ClearMap(ref csidMap);
            ClearMap(ref subscriberClidMap);
            ClearMap(ref subscribedClidMap);

            for (int iCount = 0; iCount < this.Count; iCount++)
            {
                Subscription subscription = (Subscription)this[iCount];
                dataTypeMap.AddToMap(subscription.DataType, subscription);
                clidMap.AddToMap(subscription.SubscriberCLID, subscription);
                subscriberClidMap.AddToMap(subscription.SubscriberCLID, subscription);
                subscribedClidMap.AddToMap(subscription.SubscribedCLID, subscription);
                csidMap.AddToMap(subscription.SubscriberCSID, subscription);
            }
            Changed = false;
        }
        private void ClearMap(ref SubscriptionsMap map)
        {
            if (map == null)
                map = new SubscriptionsMap();
            else
                map.Clear();  
        }
        private void RefreshDataTypeMap()
        {
            ClearMap(ref dataTypeMap);
            for (int iCount = 0; iCount < this.Count; iCount++)
            {
                Subscription subscription = (Subscription)this[iCount];
                dataTypeMap.AddToMap(subscription.DataType, subscription);
            }
            changedDataTypeMap = false;
        }
        private void RefreshClidMap()
        {
            ClearMap(ref clidMap);

            for (int iCount = 0; iCount < this.Count; iCount++)
            {
                Subscription subscription = (Subscription)this[iCount];
                clidMap.AddToMap(subscription.SubscriberCLID, subscription);
            }
            changedClidMap = false;
        }
        private void RefreshSubscriberClidMap()
        {
            ClearMap(ref subscriberClidMap);

            for (int iCount = 0; iCount < this.Count; iCount++)
            {
                Subscription subscription = (Subscription)this[iCount];
                subscriberClidMap.AddToMap(subscription.SubscriberCLID, subscription);
            }
            changedSubscriberClidMap = false;
        }
        private void RefreshSubscribedClidMap()
        {
            ClearMap(ref subscribedClidMap);

            for (int iCount = 0; iCount < this.Count; iCount++)
            {
                Subscription subscription = (Subscription)this[iCount];
                subscribedClidMap.AddToMap(subscription.SubscribedCLID, subscription);
            }
            changedSubscribedClidMap = false;
        }
        private void RefreshCsidMap()
        {
            ClearMap(ref csidMap);
            for (int iCount = 0; iCount < this.Count; iCount++)
            {
                Subscription subscription = (Subscription)this[iCount];
                csidMap.AddToMap(subscription.SubscriberCSID, subscription);
            }
            changedCsidMap = false;
        }

	}

}
