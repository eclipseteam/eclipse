using System;

namespace Oritax.TaxSimp.BusinessStructureUtilities
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
	public class Install
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="35B01E3E-2C65-4d3f-8162-23AE2DC606F3";
		public const string ASSEMBLY_NAME="BusinessStructureUtilities_1_1";
		public const string ASSEMBLY_DISPLAYNAME="Business structure utilities V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
	//	public const string ASSEMBLY_REVISION="0"; 
		public const string ASSEMBLY_REVISION="1"; //2005.1 

		#endregion
	}
}
