using System;

namespace Oritax.TaxSimp.BusinessStructureUtilities
{	
	/// <summary>
	/// The following enum is used to determine Business structure level, e.g. during push migration 
	/// </summary>
	public enum BusinessStrucutureNavigatorLevel
	{
		Organisation,
		Group,
		Entity,
		Period,
		CalculationModule,
		None
	}
}
