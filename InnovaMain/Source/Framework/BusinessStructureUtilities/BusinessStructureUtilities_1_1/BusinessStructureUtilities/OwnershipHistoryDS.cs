#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/Construction/BusinessStructureUtilities/BusinessStructureUtilities-1-0/BusinessStructureUtilities/Ownershi $
 $History: OwnershipHistoryDS.cs $
 * 
 * *****************  Version 1  *****************
 * User: Pveitch      Date: 25/02/03   Time: 9:42a
 * Created in $/2002/3. Implementation/Construction/BusinessStructureUtilities/BusinessStructureUtilities-1-0/BusinessStructureUtilities
 * 
 * *****************  Version 1  *****************
 * User: Sshrimpton   Date: 18/12/02   Time: 4:55p
 * Created in $/2002/3. Implementation/Construction/BusinessStructureCM/OrganizationUnit/OrganizationUnit-1-1-1
 * 
 * *****************  Version 1  *****************
 * User: Pveitch      Date: 9/12/02    Time: 6:23p
 * Created in $/2002/3. Implementation/Elaboration4/CM/Group
 * 
 * *****************  Version 6  *****************
 * User: Pveitch      Date: 7/11/02    Time: 3:07p
 * Updated in $/2002/3. Implementation/Elaboration4/CM/Group
 * Header information added
*/
#endregion
using System;
using System.Data;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.OrganizationUnit
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable] 
	public class OwnershipHistoryDS : DataSet
	{
		public const string OWNERSHIPHISTORY_TABLE	= "OWNERSHIPHISTORY";
		public const string KEY_FIELD				= "KEY";
		public const string STARTDATE_FIELD			= "STARTDATE";
		public const string ENDDATE_FIELD			= "ENDDATE";
		public const string PERCENTOWNERSHIP_FIELD	= "PERCENTOWNERSHIP";

		private Guid m_objEntityID = Guid.Empty;

		public OwnershipHistoryDS()
		{
			DataTable objOwnershipHistory = new DataTable( OWNERSHIPHISTORY_TABLE );
			objOwnershipHistory.Columns.Add( KEY_FIELD, typeof( Guid ) );
			objOwnershipHistory.Columns.Add( STARTDATE_FIELD, typeof( DateTime ) );
			objOwnershipHistory.Columns.Add( ENDDATE_FIELD, typeof( DateTime ) );
			objOwnershipHistory.Columns.Add( PERCENTOWNERSHIP_FIELD, typeof( Percent ) );
			this.Tables.Add( objOwnershipHistory );
		}

		/// <summary>
		/// Returns the ownership history table
		/// </summary>
		public DataTable OwnershipHistoryTable
		{
			get
			{
				return this.Tables[ OWNERSHIPHISTORY_TABLE ];
			}
		}

		/// <summary>
		/// The ID of the entity
		/// </summary>
		public Guid EntityID
		{
			get
			{
				return this.m_objEntityID;
			}
			set
			{
				this.m_objEntityID = value;
			}
		}
	}
}

