using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.BusinessStructureUtilities
{
	[Serializable]
	public class ScenarioType : ISerializable
	{

		public const int Standard	=	0;
		public const int WhatIf		=	1;

		public struct NameValue{public string Name; public int Value;public NameValue(string name,int val){Name=name;Value=val;}}

		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor

		private int theValue;

		public ScenarioType()
		{
			this.theValue=Standard;
		}

		public ScenarioType(int theValue)
		{
			this.theValue=theValue;
		}

		protected ScenarioType(SerializationInfo si, StreamingContext context)
		{
			theValue=Serialize.GetSerializedInt32(si,"st_theValue");
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			Serialize.AddSerializedValue(si,"st_theValue",theValue);
		}


		public static NameValue[] NameValuePairs
		{
			get
			{
				return new NameValue[]
				{
					new NameValue("Main",Standard),
					new NameValue("What-if",WhatIf)
				};
			}
		}
		public ScenarioType(ScenarioType old)
		{
			this.theValue=old.theValue;
		}

		public override string ToString()
		{
			switch(theValue)
			{
				case Standard: return "Standard";
				case WhatIf: return "WhatIf";
				default: return "";
			}
		}

		public int ToInt(){return theValue;}

		static public implicit operator ScenarioType(int theValue) 
		{
			return new ScenarioType(theValue);
		}

		public static bool operator == (ScenarioType val1, ScenarioType val2)
		{
			return val1.theValue==val2.theValue;
		}
		public static bool operator != (ScenarioType val1, ScenarioType val2)
		{
			return val1.theValue!=val2.theValue;
		}
		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}

	[Serializable]
	public class ScenarioStatus: ISerializable
	{
		public const int Final		=	0;
		public const int Draft		=	1;

		public struct NameValue{public string Name; public int Value;public NameValue(string name,int val){Name=name;Value=val;}}

		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor

		private int theValue;

		public ScenarioStatus(int theValue)
		{
			this.theValue=theValue;
		}

		public ScenarioStatus()
		{
			this.theValue=Final;
		}

		protected ScenarioStatus(SerializationInfo si, StreamingContext context)
		{
			theValue=Serialize.GetSerializedInt32(si,"ss_theValue");
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			Serialize.AddSerializedValue(si,"ss_theValue",theValue);
		}

		public static NameValue[] NameValuePairs
		{
			get
			{
				return new NameValue[]
				{
					new NameValue("Final",Final),
					new NameValue("Draft",Draft)
				};
			}
		}
		public ScenarioStatus(ScenarioStatus old)
		{
			this.theValue=old.theValue;
		}

		public override string ToString()
		{
			switch(theValue)
			{
				case Final: return "Final";
				case Draft: return "Draft";
				default: return "";
			}
		}

		public int ToInt(){return theValue;}

		static public implicit operator ScenarioStatus(int theValue) 
		{
			return new ScenarioStatus(theValue);
		}

		public static bool operator == (ScenarioStatus val1, ScenarioStatus val2)
		{
			return val1.theValue==val2.theValue;
		}

		public static bool operator != (ScenarioStatus val1, ScenarioStatus val2)
		{
			return val1.theValue!=val2.theValue;
		}

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

	}
}
