using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.BusinessStructureUtilities
{
	/// <summary>
	/// Class that forms the mapping between the CM Logical Instance ID and its set of CM Scenarios.
	/// </summary>
	[Serializable]
	public class ChildCMScenarios : SerializableHashtable, ISerializable
	{

		public ChildCMScenarios():base(){}

		protected ChildCMScenarios(SerializationInfo si, StreamingContext context)
			:base(si,context)
		{
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si,context);
		}


		// This class forms a dictionary that allows mapping between:
		//   CM Scenario ID (CSID) and the implementing CM Instance ID (CIID)
		/// <summary>
		/// Returns the set of CM Scenarios (CMScenario) for a given CM Logical instance ID (CLID).
		/// </summary>
		/// <param name="cLID">The logical ID of a CM</param>
		/// <returns>ArrayList of CM Scenario ID Guids, (CSID) for that logical CM.</returns>
		public LogicalCM this[Guid cLID]
		{
			get
			{
				if(base.Contains(cLID))
					return (LogicalCM)base[cLID];
				else
					return null;
			}
		}

		public void Add(Guid cLID,CMScenario scenario,string cMName)
		{
			if(base.Contains(cLID))
			{
				((LogicalCM)base[cLID]).Add(scenario.CSID,scenario);
			}
			else
			{
				LogicalCM newEntry=new LogicalCM(cLID,scenario.CMTypeID,scenario.CMTypeName,scenario.CMCategoryName,cMName);
				newEntry.Add(scenario.CSID,scenario);
				newEntry.CurrentScenario=scenario.CSID;		// Set the default scenario to the first scenario entered
				base.Add(cLID,newEntry);
			}
		}

		public void Remove(Guid cLID,Guid cSID)
		{
			if(base.Contains(cLID))
			{
				LogicalCM entry=(LogicalCM)base[cLID];
				entry.Remove(cSID);
				if(entry.Count==0)
					base.Remove(cLID);
			}
		}
	}
}
