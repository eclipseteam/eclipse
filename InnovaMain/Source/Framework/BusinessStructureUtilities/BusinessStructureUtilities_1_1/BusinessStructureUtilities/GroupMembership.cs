using System;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.OrganizationUnit
{
	[Serializable]
	public class GroupMember: ISerializable
	{
		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor

		public Guid CLID;
		public Guid CSID;

		[NonSerialized]
		public bool Selected=false;

		public bool IsDeleted=false;
		public bool Adjustment=false;
		/// <summary>
		/// The ownership history for the group membership record
		/// </summary>
		public virtual OwnershipHistories OwnershipHistory
		{
			get
			{
				return null;
			}
		}

		public GroupMember( Guid CLID, Guid CSID, bool blAdjustment)
		{
			this.CLID=CLID;
			this.CSID=CSID;
			Adjustment = blAdjustment;
		}
		public GroupMember(GroupMember original)
		{
			this.CLID=original.CLID;
			this.CSID=original.CSID;
			this.Adjustment=original.Adjustment;
		}
		protected GroupMember(SerializationInfo si, StreamingContext context)
		{
			CLID=Serialize.GetSerializedGuid(si,"gm_CLID");
			CSID=Serialize.GetSerializedGuid(si,"gm_CSID");
			IsDeleted=Serialize.GetSerializedBool(si,"gm_IsDeleted");
			Adjustment=Serialize.GetSerializedBool(si,"gm_Adjustment");
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			Serialize.AddSerializedValue(si,"gm_CLID",CLID);
			Serialize.AddSerializedValue(si,"gm_CSID",CSID);
			Serialize.AddSerializedValue(si,"gm_IsDeleted",IsDeleted);
			Serialize.AddSerializedValue(si,"gm_Adjustment",Adjustment);
		}
	}

	[Serializable]
	public class GroupMembers : SerializableHashtable, ISerializable
	{
		public GroupMembers():base(){}

		protected GroupMembers(SerializationInfo si, StreamingContext context)
			:base(si,context)
		{
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si,context);
		}

		public GroupMember this[Guid cLID]
		{
			get{return (GroupMember)base[cLID];}
			set{base[cLID]=value;}
		}
		public override bool Contains(object key){return base.Contains(key);}
		public void Add(Object key,GroupMember newEntity){base.Add(key,newEntity);}
		public override void Remove(Object key)
		{
			if(base.Contains(key))
				base.Remove(key);
		}
		public void CommitChanges()
		{
			bool more;
			do
			{
				more=false;
				foreach(DictionaryEntry dictionaryEntry in this)
				{
					GroupMember groupMember=(GroupMember)dictionaryEntry.Value;
					if(groupMember.IsDeleted)
					{
						base.Remove(dictionaryEntry.Key);
						more=true;
						break;
					}
				}
			}while(more);
		}
	}

	/// <summary>
	/// Represents an ownership history of a group or entity to another group
	/// </summary>
	[Serializable]
	public abstract class OwnershipHistory: ISerializable
	{
		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor

		Guid m_objKey;
		DateTime m_objStartDate = DateTime.Now;
		DateTime m_objEndDate;
		Percent m_objOwnershipPercentage = new Percent( 100 );

		/// <summary>
		/// Creates an ownership history with the start date and percentage specified
		/// </summary>
		/// <param name="objStartDate">The date the change is effective</param>
		/// <param name="objOwnershipPercentage">The percentage ownership from this date forward</param>
		public OwnershipHistory( DateTime objStartDate, Percent objOwnershipPercentage )
		{
			this.m_objStartDate = objStartDate;
			this.m_objOwnershipPercentage = objOwnershipPercentage;
			m_objKey = Guid.NewGuid();
		}

		public OwnershipHistory( Guid gKey, DateTime objStartDate, DateTime objEndDate, Percent objOwnershipPercentage )
		{
			this.m_objKey		= gKey;
			this.m_objStartDate = objStartDate;
			this.m_objEndDate	= objEndDate;
			this.m_objOwnershipPercentage = objOwnershipPercentage;
		}

		protected OwnershipHistory(SerializationInfo si, StreamingContext context)
		{
			m_objKey=Serialize.GetSerializedGuid(si,"oh_m_objKey");
			m_objStartDate=Serialize.GetSerializedDateTime(si,"oh_m_objStartDate");
			m_objEndDate=Serialize.GetSerializedDateTime(si,"oh_m_objEndDate");
			m_objOwnershipPercentage=Serialize.GetSerializedPercent(si,"oh_m_objOwnershipPercentage");
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			Serialize.AddSerializedValue(si,"oh_m_objKey",m_objKey);
			Serialize.AddSerializedValue(si,"oh_m_objStartDate",m_objStartDate);
			Serialize.AddSerializedValue(si,"oh_m_objEndDate",m_objEndDate);
			Serialize.AddSerializedValue(si,"oh_m_objOwnershipPercentage",m_objOwnershipPercentage);
		}

		/// <summary>
		/// The date that the ownership history became current
		/// </summary>
		public DateTime StartDate
		{
			get
			{
				return this.m_objStartDate;
			}
			set
			{
				this.m_objStartDate = value;
			}
		}
			
		/// <summary>
		/// The date on which the ownership history is no longer current
		/// </summary>
		public DateTime EndDate
		{
			get
			{
				return this.m_objEndDate;
			}
			set
			{
				this.m_objEndDate = value;
			}
		}

		/// <summary>
		/// The percentage ownership of this entity or group
		/// </summary>
		public Percent OwnershipPercent
		{
			get
			{
				return this.m_objOwnershipPercentage;
			}
			set
			{
				this.m_objOwnershipPercentage = value;
			}
		}

		/// <summary>
		/// The key that represents the instance of this ownership history
		/// </summary>
		public Guid Key
		{
			get
			{
				return this.m_objKey;
			}
		}

		public virtual void ExtractPresentationData( DataRow objRow )
		{
			objRow[ OwnershipHistoryDS.KEY_FIELD ] = this.Key;
			objRow[ OwnershipHistoryDS.STARTDATE_FIELD ] = this.StartDate;
			objRow[ OwnershipHistoryDS.ENDDATE_FIELD ] = this.Key;
			objRow[ OwnershipHistoryDS.PERCENTOWNERSHIP_FIELD ] = this.OwnershipPercent;
		}

		public virtual void DeliverPresentationData( DataRow objRow )
		{
			this.m_objKey = (Guid) objRow[ OwnershipHistoryDS.KEY_FIELD ];
			this.StartDate = (DateTime) objRow[ OwnershipHistoryDS.STARTDATE_FIELD ];
			this.OwnershipPercent = (Percent) objRow[ OwnershipHistoryDS.PERCENTOWNERSHIP_FIELD ];

			if ( this.m_objKey == Guid.Empty )
				this.m_objKey = Guid.NewGuid();
		}
	}

	/// <summary>
	/// A collection of ownership histories
	/// </summary>
	[Serializable]
	public abstract class OwnershipHistories : SerializableHashtable, ISerializable
	{
		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor
		OwnershipHistory m_objCurrentOwnership;


		public OwnershipHistories( )
		{
			OwnershipHistory objDefaultHistory = this.NewOwnershipHistory(DateTime.Now, new Percent(100) );
			this.Add( objDefaultHistory );
			this.m_objCurrentOwnership = objDefaultHistory;
		}

		protected OwnershipHistories(SerializationInfo si, StreamingContext context)
			:base(si,context)
		{
			m_objCurrentOwnership=(OwnershipHistory)Serialize.GetSerializedValue(si,"ohs_m_objCurrentOwnership",typeof(OwnershipHistory),null);
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si,context);
			Serialize.AddSerializedValue(si,"ohs_m_objCurrentOwnership",m_objCurrentOwnership);
		}


		public void Add( OwnershipHistory objOH )
		{
			if ( objOH != null )
			{
				if ( !base.Contains( objOH.Key ) )
					base.Add( objOH.Key, objOH );
				else
					base[ objOH.Key ] = objOH;
				

				// check if this is a new current ownership history
				if ( this.m_objCurrentOwnership != null && objOH.StartDate > this.m_objCurrentOwnership.StartDate )
				{
					// set the end date of the current ownership history to a day before the start
					// of the new ownership history, then replace the current ownership history
					// with the new one.
					this.m_objCurrentOwnership.EndDate = objOH.StartDate.AddDays( -1 );
					this.m_objCurrentOwnership = objOH;
				}
				else
				{
					// need to place in the middle of the periods and adjust the dates around it
				}
			}
		}

		public OwnershipHistory CurrentOwnership
		{
			get
			{
				return this.m_objCurrentOwnership;
			}
		}

		public void ExtractPresentationData( DataTable objOwnershipHistory )
		{
			IDictionaryEnumerator objOHEn = this.GetEnumerator();

			while ( objOHEn.MoveNext() )
			{
				DataRow objRow = objOwnershipHistory.NewRow();
				OwnershipHistory objOH = (OwnershipHistory) objOHEn.Value;
				objOH.ExtractPresentationData( objRow );
				objOwnershipHistory.Rows.Add( objRow );
			}
		}

		public virtual void DeliverPresentationData( DataTable objOHPresentationTable )
		{
			IEnumerator objEn = objOHPresentationTable.Rows.GetEnumerator();

			while ( objEn.MoveNext() )
			{
				DataRow objOHRow = (DataRow) objEn.Current;

				if ( objOHRow.RowState != DataRowState.Deleted )
				{
					OwnershipHistory objOH = (OwnershipHistory) this[ (Guid) objOHRow[ OwnershipHistoryDS.KEY_FIELD ] ];

					if ( objOH != null )
					{
						objOH.DeliverPresentationData( objOHRow );
					}
					else
					{
						objOH = (OwnershipHistory) this.NewOwnershipHistory(DateTime.Now, new Percent( ) );
						objOH.DeliverPresentationData( objOHRow );
						this.Add( objOH );
					}
				}
			}

			DataTable objDeletedRows = objOHPresentationTable.GetChanges( DataRowState.Deleted );

			if ( objDeletedRows != null )
			{
				foreach ( DataRow objRow in objDeletedRows.Rows )
				{
					objRow.RejectChanges();
					Guid objKey = (Guid) objRow[ OwnershipHistoryDS.KEY_FIELD ];
					this.Remove( objKey );
				}
			}

			objOHPresentationTable.AcceptChanges();
		}

		public void Remove( Guid objKey )
		{
			// need to adjust those dates around this
			base.Remove( objKey );
		}

		public OwnershipHistory this[ Guid objKey ]
		{
			get
			{
				return (OwnershipHistory) base[ objKey ];
			}
		}

		public abstract OwnershipHistory NewOwnershipHistory( DateTime objStartDate, Percent objOwnershipPercent );
	}
}
