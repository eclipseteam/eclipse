using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.BusinessStructureUtilities
{
	/// <summary>
	/// Contains a subscription that links a particular message datatype to a specific
	/// CM instance, via its CMID.
	/// </summary>
	[Serializable]
	public class Subscription: ISerializable
	{

		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor

		public Guid		ID;
		public string	DataType;
		public bool		Consolidation;
		public Guid		SubscriberCLID;
		public Guid		SubscriberCSID;
		public Guid		SubscribedCLID;
		public Guid		SubscribedCSID;
		public Object[] Parameters;

		//Flag to signal when a subscription was loaded from the persisted state
		//This is required as if a subscription is added then removed in one transaction
		//then you do not want the subscription added to the deleted subscriptions collection
		//of logical module base, as then this will try and delete it from the DB when it
		//does not actually exist in the DB
		[NonSerialized]public bool		IsPersisted = false;


		protected Subscription(SerializationInfo si, StreamingContext context)
		{
			ID=Serialize.GetSerializedGuid(si,"subs_ID");
			DataType=Serialize.GetSerializedString(si,"subs_DataType");
			Consolidation=Serialize.GetSerializedBool(si,"subs_Consolidation");
			SubscriberCLID=Serialize.GetSerializedGuid(si,"subs_SubscriberCLID");
			SubscriberCSID=Serialize.GetSerializedGuid(si,"subs_SubscriberCSID");
			SubscribedCLID=Serialize.GetSerializedGuid(si,"subs_SubscribedCLID");
			SubscribedCSID=Serialize.GetSerializedGuid(si,"subs_SubscribedCSID");
			Parameters= (object[])Serialize.GetSerializedValue(si,"subs_Parameters",typeof(Object[]),null);
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			Serialize.AddSerializedValue(si,"subs_ID",ID);
			Serialize.AddSerializedValue(si,"subs_DataType",DataType);
			Serialize.AddSerializedValue(si,"subs_Consolidation",Consolidation);
			Serialize.AddSerializedValue(si,"subs_SubscriberCLID",SubscriberCLID);
			Serialize.AddSerializedValue(si,"subs_SubscriberCSID",SubscriberCSID);
			Serialize.AddSerializedValue(si,"subs_SubscribedCLID",SubscribedCLID);
			Serialize.AddSerializedValue(si,"subs_SubscribedCSID",SubscribedCSID);
			Serialize.AddSerializedValue(si,"subs_Parameters",Parameters);
		}



		public Subscription(string dataType, Guid subscriberCLID)
		{
			ID=Guid.NewGuid();
			DataType=dataType;
			Consolidation = false;
			SubscriberCLID=subscriberCLID;
			SubscriberCSID=Guid.Empty;
			SubscribedCLID=Guid.Empty;
			SubscribedCSID=Guid.Empty;
			Parameters=null;
		}

		public Subscription(bool blConsolidation, Guid subscriberCLID, Guid subscribedCLID)
		{
			ID=Guid.NewGuid();
			DataType=null;
			Consolidation = blConsolidation;
			SubscriberCLID=subscriberCLID;
			SubscriberCSID=Guid.Empty;
			SubscribedCLID=subscribedCLID;
			SubscribedCSID=Guid.Empty;
			Parameters=null;
		}

		public Subscription(string dataType, Guid subscriberCLID, Object[] parameters)
		{
			ID=Guid.NewGuid();
			DataType=dataType;
			Consolidation = false;
			SubscriberCLID=subscriberCLID;
			SubscriberCSID=Guid.Empty;
			SubscribedCLID=Guid.Empty;
			SubscribedCSID=Guid.Empty;
			Parameters=parameters;
		}

		public Subscription(string dataType, Guid subscriberCLID, Guid subscriberCSID, Guid subscribedCSID)
		{
			ID=Guid.NewGuid();
			DataType=dataType;
			Consolidation = false;
			SubscriberCLID=subscriberCLID;
			SubscriberCSID=subscriberCSID;
			SubscribedCLID=Guid.Empty;
			SubscribedCSID=subscribedCSID;
			Parameters=null;
		}

		public Subscription( bool bolConsolidation, Guid subscriberCLID, Guid objSubscribedCLID, Guid subscriberCSID, Guid subscribedCSID )
		{
			ID=Guid.NewGuid();
			this.DataType = null;
			this.Consolidation = bolConsolidation;
			this.SubscriberCLID = subscriberCLID;
			this.SubscriberCSID = subscriberCSID;
			this.SubscribedCLID = objSubscribedCLID;
			this.SubscribedCSID = subscribedCSID;
			this.Parameters = null;
		}

		public Subscription( bool bolConsolidation, Guid objSubscriberCLID, Object[] objParameters)
		{
			ID=Guid.NewGuid();
			DataType = null;
			Consolidation = bolConsolidation;
			SubscriberCLID=objSubscriberCLID;
			SubscriberCSID=Guid.Empty;
			SubscribedCLID=Guid.Empty;
			SubscribedCSID=Guid.Empty;
			Parameters=objParameters;
		}

		public Subscription(Guid iD,string dataType, Guid subscriberCLID, Guid subscriberCSID, Guid subscribedCSID, Object[] objParameters)
		{
			ID=iD;
			DataType=dataType;
			Consolidation = false;
			SubscriberCLID=subscriberCLID;
			SubscriberCSID=subscriberCSID;
			SubscribedCSID=subscribedCSID;
			Parameters=objParameters;
		}

		public Subscription(Guid iD,bool bolConsolidation, Guid objSubscriberCLID, Guid objSubscribedCLID, Guid objSubscriberCSID, Guid objSubscribedCSID, Object[] objParameters)
		{
			ID=iD;
			DataType=null;
			Consolidation = bolConsolidation;
			SubscriberCLID=objSubscriberCLID;
			SubscribedCLID=objSubscribedCLID;
			SubscriberCSID=objSubscriberCSID;
			SubscribedCSID=objSubscribedCSID;
			Parameters=objParameters;
		}

		public Subscription(Subscription objCopySubscription)
		{
			ID=Guid.NewGuid();
			DataType=objCopySubscription.DataType;
			Consolidation = objCopySubscription.Consolidation;
			SubscriberCLID=objCopySubscription.SubscriberCLID;
			SubscribedCLID=objCopySubscription.SubscribedCLID;
			SubscriberCSID=objCopySubscription.SubscriberCSID;
			SubscribedCSID=objCopySubscription.SubscribedCSID;
			Parameters=objCopySubscription.Parameters;
		}
	}
}
