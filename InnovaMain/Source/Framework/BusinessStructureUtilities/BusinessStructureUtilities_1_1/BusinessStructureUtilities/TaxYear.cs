using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.BusinessStructureUtilities
{
	/// <summary>
	/// TaxYear represents information about a Tax Year
	/// </summary>
	[Serializable]
	public class TaxYear: ISerializable
	{
		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor

		private DateTime m_objStartDate;
		private DateTime m_objEndDate;

		public TaxYear( DateTime objStartDate, DateTime objEndDate )
		{
			this.m_objStartDate = objStartDate;
			this.m_objEndDate = objEndDate;
		}

		protected TaxYear(SerializationInfo si, StreamingContext context)
		{
			m_objStartDate=Serialize.GetSerializedDateTime(si,"ty_m_objStartDate");
			m_objEndDate=Serialize.GetSerializedDateTime(si,"ty_objEndDate");
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			Serialize.AddSerializedValue(si,"ty_m_objStartDate",m_objStartDate);
			Serialize.AddSerializedValue(si,"ty_objEndDate",m_objEndDate);
		}


		public DateTime StartDate
		{
			get
			{
				return this.m_objStartDate;
			}
		}
		
		public DateTime EndDate
		{
			get
			{
				return this.m_objEndDate;
			}
		}
	}
}
