#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/Construction/BusinessStructureUtilities/BusinessStructureUtilities-1-0/BusinessStructureUtilities/MembersD $
 $History: MembersDS.cs $
 * 
 * *****************  Version 3  *****************
 * User: Pveitch      Date: 30/04/03   Time: 12:14p
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureUtilities/BusinessStructureUtilities-1-0/BusinessStructureUtilities
 * Fix for CQ issue 1478, solves problem with scenarios being changed and
 * the cache being restarted results in an error which means that the
 * entities cannot be listed.
 * 
 * *****************  Version 2  *****************
 * User: Paubailey    Date: 4/03/03    Time: 11:10a
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureUtilities/BusinessStructureUtilities-1-0/BusinessStructureUtilities
 * 
 * *****************  Version 1  *****************
 * User: Paubailey    Date: 28/02/03   Time: 4:22p
 * Created in $/2002/3. Implementation/Construction/BusinessStructureUtilities/BusinessStructureUtilities-1-0/BusinessStructureUtilities
 * Updates for perisitance of group members into a relational table.
 
*/
#endregion
using System;
using System.Data;

namespace Oritax.TaxSimp.BusinessStructureUtilities
{
	/// <summary>
	/// MembersDS is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	public class MembersDS : DataSet
	{
		public const String CMMEMBERS_TABLE					= "CMMEMBERS";
		public const String PARENTCLID_FIELD				= "PARENTCLID";
		public const String PARENTCSID_FIELD				= "PARENTCSID";
		public const String MEMBERCLID_FIELD				= "MEMBERCLID";
		public const String MEMBERCSID_FIELD				= "MEMBERCSID";
		public const String ADDITION_FIELD					= "ADDITION";
		public const String ADJUSTMENT_FIELD				= "ADJUSTMENT";

		public MembersDS()
		{
			DataTable table;

			table = new DataTable(CMMEMBERS_TABLE);
			table.Columns.Add(PARENTCLID_FIELD, typeof(System.Guid));
			table.Columns.Add(PARENTCSID_FIELD, typeof(System.Guid));
			table.Columns.Add(MEMBERCLID_FIELD, typeof(System.Guid));
			table.Columns.Add(MEMBERCSID_FIELD, typeof(System.Guid));
			table.Columns.Add(ADDITION_FIELD, typeof(System.Boolean));
			table.Columns.Add(ADJUSTMENT_FIELD, typeof(System.Boolean));
			DataColumn[] sbPrimaryKey={table.Columns[PARENTCLID_FIELD],table.Columns[PARENTCSID_FIELD],table.Columns[MEMBERCLID_FIELD]};
			table.PrimaryKey=sbPrimaryKey;
			this.Tables.Add(table);
		}
	}
}

