using System;
using System.Runtime.Serialization;
using System.Security.Permissions ;

using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.BusinessStructureUtilities
{
	[Serializable]
	public class TaxStatusType: ISerializable
	{
		public const int Undefined								=	-1;
		public const int NotAMemberOfAConsolidatedGroup			=	0;
		public const int GroupMemberHeadCompany					=	1;
		public const int GroupMemberProvisionalHeadCompanyMEC	=	2;
		public const int GroupMemberEligibleTierOneCompany		=	3;
		public const int GroupMemberTierOneCompany				=	4;
		public const int GroupMemberSubsidiary					=	5;
		public const int Default								=	GroupMemberSubsidiary;

		public struct NameValue
		{
			public string Name; 
			public int Value;
				
			public NameValue( string strName, int intValue )
			{
				this.Name = strName;
				this.Value = intValue;
			}
		}

		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor
		private int m_intValue;

		public TaxStatusType()
		{
			this.m_intValue = Default;
		}

		public TaxStatusType( int intValue )
		{
			this.m_intValue = intValue;
		}

		public TaxStatusType( string strType )
		{
			this.m_intValue = Default;

			foreach ( NameValue objType in NameValuePairs )
			{
				if ( strType == objType.Name )
					this.m_intValue = objType.Value;
			}
		}

		protected TaxStatusType(SerializationInfo si, StreamingContext context)
		{
			m_intValue = Serialize.GetSerializedInt32(si,"tst_m_intValue");
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			Serialize.AddSerializedValue(si,"tst_m_intValue",m_intValue);
		}

		public static NameValue[] NameValuePairs
		{
			get
			{
				return new NameValue[]
					{
						new NameValue( "Not a member of a consolidated group", NotAMemberOfAConsolidatedGroup ),
						new NameValue( "Group member - head company", GroupMemberHeadCompany ),
						new NameValue( "Group member - provisional head company (MEC)", GroupMemberProvisionalHeadCompanyMEC ),
						new NameValue( "Group member - eligible tier one company", GroupMemberEligibleTierOneCompany ),
						new NameValue( "Group member - tier one company", GroupMemberTierOneCompany ),
						new NameValue( "Group member - subsidiary", GroupMemberSubsidiary ),
				};
			}
		}

		public TaxStatusType( TaxStatusType objType )
		{
			this.m_intValue = objType.m_intValue;
		}

		public override string ToString()
		{
			foreach ( NameValue objType in NameValuePairs )
			{
				if ( this.m_intValue == objType.Value )
					return objType.Name;
			}
			return String.Empty;
		}

		public int ToInt()
		{
			return this.m_intValue;
		}

		static public implicit operator TaxStatusType( int intValue ) 
		{
			return new TaxStatusType( intValue );
		}

		public static bool operator == ( TaxStatusType val1, TaxStatusType val2 )
		{
			// Modified - 09SEP03 - SC : Cater for null status.
			if(((Object)val1) != null && ((Object)val2) != null)
				return( val1.m_intValue == val2.m_intValue );

			if(((Object)val1) == null && ((Object)val2) == null)
				return true;

			return false;	
		}

		public static bool operator != ( TaxStatusType val1, TaxStatusType val2 )
		{
			return val1.m_intValue != val2.m_intValue;
		}

		public override bool Equals( object obj )
		{
			return base.Equals( obj );
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
}
