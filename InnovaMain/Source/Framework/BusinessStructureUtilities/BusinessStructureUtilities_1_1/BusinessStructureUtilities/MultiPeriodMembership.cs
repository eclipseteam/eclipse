using System;
using System.Collections;

namespace Oritax.TaxSimp.BusinessStructureUtilities
{
	//[Serializable]
	public class MemberPeriod  
	{
		public Guid CLID;
		public bool AdditionPeriod = true;
		public bool Selected=false;
		public bool Adjustment=false;
		public bool IsDeleted=false;

		public MemberPeriod(Guid cLID, bool blAdditionPeriod, bool blAdjustment)
		{
			this.CLID=cLID;
			AdditionPeriod = blAdditionPeriod;
			Adjustment = blAdjustment;
		}
		public MemberPeriod(MemberPeriod original)
		{
			this.CLID=original.CLID;
			this.AdditionPeriod = original.AdditionPeriod;
		}
	}

	public class MemberPeriods : System.Collections.DictionaryBase
	{
		public MemberPeriod this[Guid cLID]
		{
			get{return (MemberPeriod)Dictionary[cLID];}
			set{Dictionary[cLID]=value;}
		}
		public bool Contains(object key){return Dictionary.Contains(key);}
		public void Add(Object key,MemberPeriod newEntity){Dictionary.Add(key,newEntity);}
		public void Remove(Object key)
		{
			if(Dictionary.Contains(key))
				Dictionary.Remove(key);
		}
		public void CommitChanges()
		{
			bool more;
			do
			{
				more=false;
				foreach(DictionaryEntry dictionaryEntry in this.Dictionary)
				{
					MemberPeriod mpMember=(MemberPeriod)dictionaryEntry.Value;
					if(mpMember.IsDeleted)
					{
						this.Dictionary.Remove(dictionaryEntry.Key);
						more=true;
						break;
					}
				}
			}while(more);
		}
	}
}
