using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.BusinessStructureUtilities
{
	/// <summary>
	/// Class that forms the mapping between the CM Logical Instance ID and its set of CM Scenarios.
	/// </summary>
	[Serializable]
	public class CMScenario: ISerializable
	{

		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor
		
		public Guid ID = Guid.NewGuid();	// Scenario ID
		
		public string Name;
		public Guid CSID;					// Scenario ID
		public Guid CLID;					// Logical CM ID
		public Guid CIID;					// Implementation Physical CM ID.
		public Guid CMTypeID;				// Type ID of this CM.
		public string CMTypeName;			// Type Name of this CM.
		public ScenarioType Type;			// Scenario type.
		public ScenarioStatus Status;		// Scenario status.
		private DateTime creationDateTime;		// Date and time of creation
		private DateTime modificationDateTime;	// Date and time of last modification
		public bool Locked;


		public DateTime CreationDateTime
		{
			get
			{
				return creationDateTime;
			}
			set
			{
				creationDateTime = value;
			}
		}

		public DateTime ModificationDateTime
		{
			get
			{
				return modificationDateTime;
			}
			set
			{
				modificationDateTime = value;
			}
		}

		public CMScenario(){}

		public CMScenario(	
			Guid iD,
			string name,
			Guid cLID,
			Guid cSID,
			ScenarioType type, 
			Guid cIID, 
			ScenarioStatus status,
			Guid cMTypeID,
			string cMTypeName,
			DateTime creationDateTime,
			DateTime modificationDateTime,
			bool locked)
		{
			ID=iD;
			CSID=cSID;
			CLID=cLID;
			CIID=cIID;
			Type=type;
			Name=name;
			Status=status;
			CMTypeID=cMTypeID;
			CMTypeName=cMTypeName;
			CreationDateTime= creationDateTime;
			ModificationDateTime = modificationDateTime;
			Locked=locked;
		}

		public CMScenario(	
			string name,
			Guid cLID,
			Guid cSID,
			ScenarioType type, 
			Guid cIID, 
			ScenarioStatus status,
			Guid cMTypeID,
			string cMTypeName,
			DateTime creationDateTime,
			DateTime modificationDateTime,
			bool locked)
		{
			ID = Guid.NewGuid();
			CSID=cSID;
			CLID=cLID;
			CIID=cIID;
			Type=type;
			Name=name;
			Status=status;
			CMTypeID=cMTypeID;
			CMTypeName=cMTypeName;
			CreationDateTime= creationDateTime;
			ModificationDateTime = modificationDateTime;
			Locked=locked;
		}


		protected CMScenario(SerializationInfo si, StreamingContext context)
		{
			//NOTE: Removed all readonly fields which are not actually owned by the CMScenario from serialization.
			Name=Serialize.GetSerializedString(si,"cms_Name");
			CSID=Serialize.GetSerializedGuid(si,"cms_CSID");
			CLID=Serialize.GetSerializedGuid(si,"cms_CLID");
			CIID=Serialize.GetSerializedGuid(si,"cms_CIID");
			Type=(ScenarioType)Serialize.GetSerializedValue(si,"cms_Type",typeof(ScenarioType),ScenarioType.Standard);
			Status=(ScenarioStatus)Serialize.GetSerializedValue(si,"cms_Status",typeof(ScenarioStatus),ScenarioStatus.Draft);
			CreationDateTime=Serialize.GetSerializedDateTime(si,"cms_CreationDateTime");
			ModificationDateTime=Serialize.GetSerializedDateTime(si,"cms_ModificationDateTime");
			Locked=Serialize.GetSerializedBool(si,"cms_Locked");

            CMTypeID = Serialize.GetSerializedGuid(si, "cms_CMTypeID");
            CMTypeName = Serialize.GetSerializedString(si, "cms_CMTypeName");
        }
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			//NOTE: Removed all readonly fields which are not actually owned by the CMScenario from serialization.
			Serialize.AddSerializedValue(si,"cms_Name",Name);
			Serialize.AddSerializedValue(si,"cms_CSID",CSID);
			Serialize.AddSerializedValue(si,"cms_CLID",CLID);
			Serialize.AddSerializedValue(si,"cms_CIID",CIID);
			Serialize.AddSerializedValue(si,"cms_Type",Type);
			Serialize.AddSerializedValue(si,"cms_Status",Status);
			Serialize.AddSerializedValue(si,"cms_CreationDateTime",CreationDateTime);
			Serialize.AddSerializedValue(si,"cms_ModificationDateTime",ModificationDateTime);
			Serialize.AddSerializedValue(si,"cms_Locked",Locked);

            Serialize.AddSerializedValue(si, "cms_CMTypeID", CMTypeID);
            Serialize.AddSerializedValue(si, "cms_CMTypeName", CMTypeName);

		}
	}
}
