using System;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.BusinessStructureUtilities
{
	/// <summary>
	/// Summary description for PeriodSpecifier.
	/// </summary>
	public class PeriodSpecifier
	{
		#region Private members--------------------------------------------------------------------

		private DateTime startDate; 
		private DateTime endDate; 

		#endregion 

		#region Public properties------------------------------------------------------------------
		/// <summary>
		/// Gets and sets start date 
		/// </summary>
		public DateTime StartDate
		{
			get
			{
				return this.startDate; 
			}
			set
			{
				this.startDate = value; 
			}
		}
		/// <summary>
		/// Gets and sets end date
		/// </summary>
		public DateTime EndDate
		{
			get
			{
				return this.endDate;
			}
			set
			{
				this.endDate = value; 
			}
		}

		#endregion 
		
		#region Constructor------------------------------------------------------------------------

		public PeriodSpecifier()
		{
		}
		#endregion 
	}
}
