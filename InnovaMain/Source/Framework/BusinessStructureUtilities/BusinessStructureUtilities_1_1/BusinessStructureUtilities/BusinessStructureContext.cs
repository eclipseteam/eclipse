using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.BusinessStructureUtilities
{
	[Serializable]
	public enum ConsolidationType
	{
		None,
		Consolidation,
		Aggregation,
		ConsolidationCorrection,
        Adjustments,
        ConsolidationAdjusted,
        AggregationAdjusted,
    }

	[Serializable]
	public enum PeriodType
	{
		SinglePeriod,
		MultiPeriod,
	}

	[Serializable]
	public enum ConsolidationMode
	{
		None,
		IncomeTax,
		TaxAccounting,
	}

	[Serializable]
	public enum OrganisationUnitType
	{
		Group,
		Entity,
	}

	/// <summary>
	/// Specifies the business structure context that a Calculation Module is in
	/// </summary>
	[Serializable]
	public class BusinessStructureContext: ISerializable
	{
		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor

		public ConsolidationType Consolidation;
		public PeriodType Period;
		public ConsolidationMode ConsolMode;
		public OrganisationUnitType OrganisationUnitType;
        public bool IsConsolidationOn = true;

		public BusinessStructureContext( ConsolidationType objConsolidation, PeriodType objPeriod, ConsolidationMode objConsolMode, OrganisationUnitType objOrganisationUnitType )
		{
			this.Consolidation = objConsolidation;
			this.Period = objPeriod;
			this.ConsolMode = objConsolMode;
			this.OrganisationUnitType = objOrganisationUnitType;
		}

		protected BusinessStructureContext(SerializationInfo si, StreamingContext context)
		{
			Consolidation=(ConsolidationType)Serialize.GetSerializedValue(si,"bsc_Consolidation",typeof(ConsolidationType),ConsolidationType.None);
            IsConsolidationOn = Serialize.GetSerializedBool(si, "bsc_IsConsolidationOn");
            Period=(PeriodType)Serialize.GetSerializedValue(si,"bsc_Period",typeof(PeriodType),PeriodType.SinglePeriod);
			ConsolMode=(ConsolidationMode)Serialize.GetSerializedValue(si,"bsc_ConsolMode",typeof(ConsolidationMode),ConsolidationMode.None);
			OrganisationUnitType=(OrganisationUnitType)Serialize.GetSerializedValue(si,"bsc_OrganisationUnitType",typeof(OrganisationUnitType),OrganisationUnitType.Entity);
        }
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
            Serialize.AddSerializedValue(si, "bsc_IsConsolidationOn", IsConsolidationOn);
			Serialize.AddSerializedValue(si,"bsc_Consolidation",Consolidation);
			Serialize.AddSerializedValue(si,"bsc_Period",Period);
			Serialize.AddSerializedValue(si,"bsc_ConsolMode",ConsolMode);
			Serialize.AddSerializedValue(si,"bsc_OrganisationUnitType",OrganisationUnitType);
		}
	}
}
