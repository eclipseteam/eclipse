using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.BusinessStructureUtilities
{
	[Serializable]
	public class LogicalCM : SerializableHashtable, ISerializable
	{

		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor

		public Guid CLID;
		public Guid TypeID;
		public Guid CurrentScenario;
		public string TypeName;
		public string CategoryName;
		public string Name;

		public LogicalCM(Guid cLID,Guid typeID,string typeName,string categoryName,string name)
		{
			CLID=cLID;
			TypeID=typeID;
			TypeName=typeName;
			CategoryName=categoryName;
			Name=name;
		}

		protected LogicalCM(SerializationInfo si, StreamingContext context)
			:base(si,context)
		{
			CLID=Serialize.GetSerializedGuid(si,"lcm_CLID");
			TypeID=Serialize.GetSerializedGuid(si,"lcm_TypeID");
			CurrentScenario=Serialize.GetSerializedGuid(si,"lcm_CurrentScenario");
			TypeName=Serialize.GetSerializedString(si,"lcm_TypeName");
			CategoryName=Serialize.GetSerializedString(si,"lcm_CategoryName");
			Name=Serialize.GetSerializedString(si,"lcm_Name");
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si,context);

			Serialize.AddSerializedValue(si,"lcm_CLID",CLID);
			Serialize.AddSerializedValue(si,"lcm_TypeID",TypeID);
			Serialize.AddSerializedValue(si,"lcm_CurrentScenario",CurrentScenario);
			Serialize.AddSerializedValue(si,"lcm_TypeName",TypeName);
			Serialize.AddSerializedValue(si,"lcm_CategoryName",CategoryName);
			Serialize.AddSerializedValue(si,"lcm_Name",Name);
		}


		public CMScenario this[Guid cSID]
		{
			get
			{
				if(base.Contains(cSID))
					return (CMScenario)base[cSID];
				else
					return null;
			}
		}
		public void Add(Guid cSID,CMScenario scenario)
		{
			if(base.Contains(cSID))
			{
				base[cSID]=scenario;
			}
			else
			{
				base.Add(cSID,scenario);
			}
		}
		public void Remove(Guid cSID)
		{
			if(base.Contains(cSID))
			{
				base.Remove(cSID);
			}
		}
	}
}
