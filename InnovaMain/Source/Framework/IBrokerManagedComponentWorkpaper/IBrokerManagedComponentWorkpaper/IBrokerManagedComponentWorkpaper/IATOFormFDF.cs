using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data;
using System.Web;

namespace Oritax.TaxSimp.CalculationInterface
{
    public interface IATOFormFDF
    {
        void GetFormPDF(DataSet dataset, HttpResponse response);
    }
}
