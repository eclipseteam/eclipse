using System;
using System.Collections;



namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// Summary description for IBaseMessageBoxControl.
	/// </summary>
	public interface IBaseMessageBoxControl
	{
		bool UserConfirmation{get;}
		string Text {get;set;}
		Guid ID {get;set;}
		string ControlTypeName {get;}
		string ControlType {get;}
		string ScriptBuilder();
	}
}
