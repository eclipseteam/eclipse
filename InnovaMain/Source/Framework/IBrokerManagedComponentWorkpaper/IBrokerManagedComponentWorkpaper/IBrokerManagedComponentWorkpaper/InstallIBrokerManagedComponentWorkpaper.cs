using System;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
	public class InstallIBrokerManagedComponentWorkpaper
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="86383CFE-47B6-4327-9991-9832AAB93ACB";
		public const string ASSEMBLY_NAME="IBrokerManagedComponentWorpaper_1_1";
		public const string ASSEMBLY_DISPLAYNAME="IBrokerManagedComponentWorkpaper V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		public const string ASSEMBLY_REVISION="1"; 

		#endregion
	}
}
