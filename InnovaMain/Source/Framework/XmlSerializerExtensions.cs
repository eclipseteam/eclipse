﻿using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Extensions
{
    public static class XmlSerializerExtensions
    {

        public static T ToNewOrData<T>(this string value) where T : class, new()
        {
            return string.IsNullOrWhiteSpace(value) ? new T() : value.ToData<T>();
        }

        public static T ToData<T>(this string value) where T : class, new()
        {
            if (string.IsNullOrEmpty(value)) return null;
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            T t = null;
            using (StringReader reader = new StringReader(value))
            {
                try
                {
                    t = serializer.Deserialize(reader) as T;
                }
                catch{}
                reader.Close();
            }
            return t;
        }

        public static string ToXmlString(this BMCServiceDataArray data)
        {
            return ToXmlString<BMCServiceDataArray>(data);
        }

        public static string ToXmlString(this BMCServiceDataArrayByName data)
        {
            return ToXmlString<BMCServiceDataArrayByName>(data);
        }

        public static string ToXmlString(this BMCServiceDataArrayByCID data)
        {
            return ToXmlString<BMCServiceDataArrayByCID>(data);
        }

        public static string ToXmlString<T>(this T data) where T : class, new()
        {
            return ToXmlStringOfType(data, typeof(T));
        }

        private static string ToXmlStringOfType(object data, Type type)
        {
            if (data == null) return string.Empty;

            XmlSerializer serializer = new XmlSerializer(type);
            StringBuilder builder = new StringBuilder();
            using (StringWriter writer = new StringWriter(builder))
            {
                serializer.Serialize(writer, data);
                writer.Close();
            }
            string xml = builder.ToString();
            return xml;
        }

    }
}
