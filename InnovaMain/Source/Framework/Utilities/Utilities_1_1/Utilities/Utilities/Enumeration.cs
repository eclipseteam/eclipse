﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Oritax.TaxSimp.Utilities;
using System.ComponentModel;
using System.Collections;

namespace Oritax.TaxSimp
{
    public static class Enumeration
    {
        public static IDictionary<int, string> GetAll<TEnum>() where TEnum : struct
        {
            var enumerationType = typeof(TEnum);

            if (!enumerationType.IsEnum)
                throw new ArgumentException("Enumeration type is expected.");

            var dictionary = new Dictionary<int, string>();

            foreach (int value in Enum.GetValues(enumerationType))
            {
                var name = Enum.GetName(enumerationType, value);
                dictionary.Add(value, name);
            }

            return dictionary;
        }

        /// <summary>
        /// Gets the <see cref="DescriptionAttribute" /> of an <see cref="Enum" />
        /// type value.
        /// </summary>
        /// <param name="value">The <see cref="Enum" /> type value.</param>
        /// <returns>A string containing the text of the
        /// <see cref="DescriptionAttribute"/>.</returns>
        public static string GetDescription(Enum value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            string description = value.ToString();
            FieldInfo fieldInfo = value.GetType().GetField(description);
            DescriptionAttribute[] attributes =
               (DescriptionAttribute[])
             fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                description = attributes[0].Description;
            }
            return description;
        }

        // <summary>
        /// Converts the <see cref="Enum" /> type to an <see cref="IList" /> 
        /// compatible object.
        /// </summary>
        /// <param name="type">The <see cref="Enum"/> type.</param>
        /// <returns>An <see cref="IList"/> containing the enumerated
        /// type value and description.</returns>
        public static IDictionary<Enum, string> ToDictionary(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            Dictionary<Enum, string> list = new Dictionary<Enum, string>();
            Array enumValues = Enum.GetValues(type);
          
            foreach (Enum value in enumValues)
                list.Add(value, GetDescription(value));

            return list.OrderBy(values => values.Value).ToDictionary(x => x.Key, x => x.Value);
        }

        // <summary>
        /// Converts the <see cref="Enum" /> type to an <see cref="IList" /> 
        /// compatible object.
        /// </summary>
        /// <param name="type">The <see cref="Enum"/> type.</param>
        /// <returns>An <see cref="IList"/> containing the enumerated
        /// type value and description.</returns>
        public static IDictionary<int, string> ToIntDictionary(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            Dictionary<int, string> list = new Dictionary<int, string>();
            Array enumValues = Enum.GetValues(type);

            foreach (Enum value in enumValues)
                list.Add((int)Convert.ChangeType(value, value.GetTypeCode()), GetDescription(value));

            return list.OrderBy(values => values.Value).ToDictionary(x => x.Key, x => x.Value);
        }

        // <summary>
        /// Converts the <see cref="Enum" /> type to an <see cref="IList" /> 
        /// compatible object.
        /// </summary>
        /// <param name="type">The <see cref="Enum"/> type.</param>
        /// <returns>An <see cref="IList"/> containing the enumerated
        /// type value and description.</returns>
        public static IDictionary<string, int> ToDictionaryForBinding(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            Dictionary<string, int> list = new Dictionary<string, int>();
            Array enumValues = Enum.GetValues(type);

            foreach (Enum value in enumValues)
                list.Add(GetDescription(value), (int)Convert.ChangeType(value, value.GetTypeCode()));

            return list.OrderBy(values => values.Value).ToDictionary(x => x.Key, x => x.Value);
        }
    }
}
