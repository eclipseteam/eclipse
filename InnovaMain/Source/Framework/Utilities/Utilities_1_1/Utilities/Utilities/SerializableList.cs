using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Reflection;
using System.Diagnostics;
namespace Oritax.TaxSimp.Utilities
{
	[Serializable]
	public class SerializableList: IList, ICollection, ICloneable, ISerializable
	{
		private ArrayList ar;

		public SerializableList()
		{
			ar = new ArrayList();
		}

		public SerializableList(int capacity)
		{
			ar = new ArrayList(capacity);
		}

		public SerializableList(ICollection c)
		{
			ar = new ArrayList(c);
		}

		//ISerializable Interface
		public SerializableList(SerializationInfo si, StreamingContext context)
		{
		
			ar=Serialize.GetSerializedArrayList(si,"serlist_ar");
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			//Use reflection to get access to the private member of hashtable 'version'
			FieldInfo version = ar.GetType().GetField("_version", BindingFlags.Instance | BindingFlags.NonPublic );
			//Assert version is not null just in case later framework versions change the implementation
			//of this private member.
			Debug.Assert( version != null );
			if(version != null)
			{
				object objVersion = version.GetValue(ar);
				//Assert that the version value is an integer type, then if it is always set it to 0.
				Debug.Assert( objVersion is int );
				if(objVersion is int)
					version.SetValue(ar, 0);
			}
			Serialize.AddSerializedValue(si,"serlist_ar",ar);
		}
		

		//IList Interface
		public virtual bool IsFixedSize{get{return ar.IsFixedSize;}}
		public virtual bool IsReadOnly{get{return ar.IsReadOnly;}}
		public virtual object this[int index]{get{return ar[index];}set{ar[index]=value;}}
		public virtual int Add(object value){return ar.Add(value);}
		public virtual void Clear(){ar.Clear();}
		public virtual bool Contains(object value){return ar.Contains(value);}
		public virtual int IndexOf(object value){return ar.IndexOf(value);}
		public virtual int IndexOf(object value, int startIndex){return ar.IndexOf(value,startIndex);}
		public virtual int IndexOf(object value, int startIndex, int count){return ar.IndexOf(value,startIndex,count);}
		public virtual void Insert(int index,object value){ar.Insert(index,value);}
		public virtual void Remove(object value){ar.Remove(value);}
		public virtual void RemoveAt(int index){ar.RemoveAt(index);}
		
		//ICollection Interface
		public virtual int Count {get{return ar.Count;}}
		public virtual bool IsSynchronized {get{return ar.IsSynchronized;}}
		public virtual object SyncRoot {get{return ar.SyncRoot;}}
		public virtual void CopyTo(Array array, int index){ar.CopyTo(array,index);}
		public virtual void CopyTo(Array array){ar.CopyTo(array);}
		public virtual void CopyTo(int index,Array array, int arrayIndex, int count){ar.CopyTo(index,array,arrayIndex,count);}

		//IEnumerable Interface
		public virtual IEnumerator GetEnumerator(){return ar.GetEnumerator();}
		public virtual IEnumerator GetEnumerator(int index, int count){return ar.GetEnumerator(index,count);}

		//ICloneable
		public virtual object Clone(){return ar.Clone();}


		//Object
		
//		public new bool Equals(object obj){return ar.Equals(obj);}
//		public new int GetHashCode(){return ar.GetHashCode();}
//		public new Type GetType(){return ar.GetType();}
//		public new string ToString(){return ar.ToString();}


		//Array list functions

		public virtual void AddRange(ICollection c){ar.AddRange(c);}
		public virtual int BinarySearch(object value, IComparer comparer){return ar.BinarySearch(value,comparer);}
		public virtual int BinarySearch(object value){return ar.BinarySearch(value);}
		public virtual int BinarySearch(int index, int count, object value, IComparer comparer){return ar.BinarySearch(index,count,value,comparer);}
		public virtual int Capacity {get{return ar.Capacity;}}
		public virtual ArrayList GetRange(int index,int count){return ar.GetRange(index,count);}
		public virtual void InsertRange(int index, ICollection c){ar.InsertRange(index,c);}
		public virtual int LastIndexOf(object value, int startIndex, int count){return ar.LastIndexOf(value,startIndex,count);}
		public virtual int LastIndexOf(object value, int startIndex){return ar.LastIndexOf(value,startIndex);}
		public virtual int LastIndexOf(object value){return ar.LastIndexOf(value);}
		public virtual void RemoveRange(int index,int count){ar.RemoveRange(index,count);}
		public virtual void Reverse(int index, int count){ ar.Reverse(index,count);}
		public virtual void Reverse(){ ar.Reverse();}
		public virtual void SetRange(int index, ICollection c){ar.SetRange(index,c);}
		public virtual void Sort(int index, int count, IComparer comparer){ ar.Sort(index,count,comparer);}
		public virtual void Sort(IComparer comparer){ ar.Sort(comparer);}
		public virtual void Sort(){ ar.Sort();}
		public virtual Array ToArray(Type type){return ar.ToArray(type);}
		public virtual object [] ToArray(){return ar.ToArray();}
		public virtual void TrimToSize(){ar.TrimToSize();}

		public static ArrayList Adapter(IList list){return ArrayList.Adapter(list);}
		public static IList FixedSize(IList list){return ArrayList.FixedSize(list);}
		public static ArrayList FixedSize(ArrayList list){return ArrayList.FixedSize(list);}
		public static IList ReadOnly(IList list){return ArrayList.ReadOnly(list);}
		public static ArrayList ReadOnly(ArrayList list){return ArrayList.ReadOnly(list);}
		public static ArrayList Repeat(Object value, int count){return ArrayList.Repeat(value,count);}
		public static IList Synchronized(IList list){return ArrayList.Synchronized(list);}
		public static ArrayList Synchronized(ArrayList list){return ArrayList.Synchronized(list);}

		public ArrayList ToArrayList(){return this.ar;}
	}
}
