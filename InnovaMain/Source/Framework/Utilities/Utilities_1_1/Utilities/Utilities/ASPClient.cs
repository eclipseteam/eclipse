using System;
using System.Net;
using System.Web;
using System.IO;
using System.Text;
using System.Collections;

namespace Oritax.TaxSimp.Utilities
{
	public class ASPParseUtilities
	{
		/// <summary>
		/// Verifies that an option exists in an identified Select control of the specified name and value.
		/// </summary>
		/// <param name="hTMLPage">The HTML page text to be analyzed.</param>
		/// <param name="controlID">The required value of the Select control tag id attribute.</param>
		/// <param name="optionText">The required name of the option.</param>
		/// <param name="requiredValue">The required value of the option.</param>
		/// <returns></returns>
		public static bool VerifySelectOptionValue(string hTMLPage,string controlID,string optionText,string requiredValue)
		{
			string controlText=ExtractTagHTMLTextByTagID(hTMLPage,"select",controlID);
			if(null!=controlText)
			{
				string optionHTMLText=ExtractTagHTMLTextByEnclosedText(controlText,"option",optionText);
				string valuePattern="value=\""+requiredValue+"\"";
				if(optionHTMLText.IndexOf(valuePattern)>0)
					return true;
			}

			return false;
		}

		public static string ExtractTagHTMLTextByTagID(string hTMLPage,string tag,string tagID)
		{
			string controlHTMLText;
			int startPos,endPos,endPos1,endPos2;

			string startPattern="<"+tag;
			string endPattern1="/>";
			string endPattern2="</"+tag+">";
			string endPattern;
			string idPattern="id=\""+tagID+"\"";

			for(startPos=0;-1!=(startPos=hTMLPage.IndexOf(startPattern,startPos));startPos=endPos+1)
			{
				endPos1=hTMLPage.IndexOf(endPattern1,startPos);
				endPos2=hTMLPage.IndexOf(endPattern2,startPos);

				if(endPos1>0 || endPos2>0)
				{
					if(endPos2<0 || endPos1<endPos2)
					{
						endPos=endPos1;
						endPattern=endPattern1;
					}
					else
					{
						endPos=endPos2;
						endPattern=endPattern2;
					}
					controlHTMLText=hTMLPage.Substring(startPos,endPos-startPos+endPattern.Length);
					int idPos=controlHTMLText.IndexOf(idPattern);
					if(idPos>(int)0)
					{
						return controlHTMLText;
					}
				}
				else
					break;
			}

			return null;
		}

		public static string ExtractTagHTMLTextByTagName(string hTMLPage,string tag,string tagName)
		{
			string controlHTMLText;
			int startPos,endPos,endPos1,endPos2;

			string startPattern="<"+tag;
			string endPattern1="/>";
			string endPattern2="</"+tag+">";
			string endPattern;
			string idPattern="name=\""+tagName+"\"";

			for(startPos=0;-1!=(startPos=hTMLPage.IndexOf(startPattern,startPos));startPos=endPos+1)
			{
				endPos1=hTMLPage.IndexOf(endPattern1,startPos);
				endPos2=hTMLPage.IndexOf(endPattern2,startPos);

				if(endPos1>0 || endPos2>0)
				{
					if(endPos2<0 || endPos1<endPos2)
					{
						endPos=endPos1;
						endPattern=endPattern1;
					}
					else
					{
						endPos=endPos2;
						endPattern=endPattern2;
					}
					controlHTMLText=hTMLPage.Substring(startPos,endPos-startPos+endPattern.Length);
					int idPos=controlHTMLText.IndexOf(idPattern);
					if(idPos>(int)0)
					{
						return controlHTMLText;
					}
				}
				else
					break;
			}

			return null;
		}

		public static ArrayList ExtractAllTagHTMLTextByTag(string hTMLPage,string tag)
		{
			string controlHTMLText;
			int startPos,endPos,endPos1,endPos2;

			string startPattern="<"+tag;
			string endPattern1="/>";
			string endPattern2="</"+tag+">";
			string endPattern;

			ArrayList result=new ArrayList();

			for(startPos=0;-1!=(startPos=hTMLPage.IndexOf(startPattern,startPos));startPos=endPos+1)
			{
				endPos1=hTMLPage.IndexOf(endPattern1,startPos);
				endPos2=hTMLPage.IndexOf(endPattern2,startPos);

				if(endPos1>0 || endPos2>0)
				{
					if(endPos2<0 || (endPos1>0 && endPos1<endPos2))
					{
						endPos=endPos1;
						endPattern=endPattern1;
					}
					else
					{
						endPos=endPos2;
						endPattern=endPattern2;
					}
					controlHTMLText=hTMLPage.Substring(startPos,endPos-startPos+endPattern.Length);

					result.Add(controlHTMLText);
				}
				else
					break;
			}

			return result;
		}

		public static string ExtractTagHTMLTextByEnclosedText(string hTMLPage,string tag,string enclosedText)
		{
			string controlHTMLText;
			int startPos,endPos;

			string startPattern="<"+tag;
			string endPattern="</"+tag+">";
			string idPattern=enclosedText;

			for(startPos=0;-1!=(startPos=hTMLPage.IndexOf(startPattern,startPos));startPos=endPos+1)
			{
				if(-1!=(endPos=hTMLPage.IndexOf(endPattern,startPos)))
				{
					controlHTMLText=hTMLPage.Substring(startPos,endPos-startPos+endPattern.Length);
					if((-1)!=controlHTMLText.IndexOf(idPattern))
						return controlHTMLText;
				}
				else
					break;
			}

			return null;
		}

		public static string GetTagAttributeText(string tagText,string attributeName)
		{
			int startPos,endPos;

			string attributeNamePattern=attributeName+"=\"";
			string endPattern="\"";
			for(startPos=0;(-1)!=(startPos=tagText.IndexOf(attributeNamePattern,startPos));startPos=endPos+1)
			{
				if((-1)!=(endPos=tagText.IndexOf(endPattern,startPos+attributeNamePattern.Length)))
				{
					return tagText.Substring(startPos+attributeNamePattern.Length,endPos-startPos-attributeNamePattern.Length-endPattern.Length+1);
				}
			}

			return null;
		}

		public static string GetTagEnclosedText(string tagText)
		{
			int startPos,endPos,tagNameEndPos;

			string tagStartPattern;
			string tagEndPattern;

			startPos=tagText.IndexOf("<");
			endPos=tagText.IndexOf(">");

			if(startPos>=0 && endPos>0)
			{
				tagStartPattern=tagText.Substring(startPos,endPos-startPos+1);
				tagNameEndPos=tagStartPattern.IndexOf(" ");
				tagEndPattern=tagStartPattern.Substring(0,tagNameEndPos);
				tagEndPattern=tagEndPattern.Insert(1,"/")+">";

				startPos=endPos+1;
				endPos=tagText.IndexOf(tagEndPattern);
				if(endPos>0)
					return tagText.Substring(startPos,endPos-startPos);
			}
			return null;
		}

		public static string GetHiddenFieldValue(string hTMLPage,string fieldName)
		{
			string inputTagString=ExtractTagHTMLTextByTagName(hTMLPage,"input",fieldName);

			if(null!=inputTagString)
			{
				return GetTagAttributeText(inputTagString,"value");
			}

			return null;
		}
	}

	public class ReturnedASPPage
	{
		string pageString;
		string viewStateString,eventTargetString,eventArgumentString;
		string method;
		string postData;

		public ReturnedASPPage(string pageString,string method,string postData)
		{
			this.pageString=pageString;
			this.method=method;
			this.postData=postData;
			viewStateString=ASPParseUtilities.GetHiddenFieldValue(pageString,"__VIEWSTATE");
			eventTargetString=ASPParseUtilities.GetHiddenFieldValue(pageString,"__EVENTTARGET");
			eventArgumentString=ASPParseUtilities.GetHiddenFieldValue(pageString,"__EVENTARGUMENT");
		}

		public ReturnedASPPage(string pageString)
			: this(pageString,"GET","")
		{
		}

		public static ReturnedASPPage GetASPPage(string url)
		{
			WebRequest testRequest = WebRequest.Create(url);
			testRequest.Timeout=120000;

			WebResponse testResponse;
			//string dataString;
			Encoding encode=System.Text.Encoding.GetEncoding("utf-8");

			string responseString="";
			try
			{
				// Retrieve the page
				testResponse = testRequest.GetResponse();
				Stream testResponseStream=testResponse.GetResponseStream();
				StreamReader readStream=new StreamReader(testResponseStream,encode);
				Char[] testResponseArray = new Char[1000];
				int count;

				do
				{
					count=readStream.Read(testResponseArray,0,1000);
					responseString+=new String(testResponseArray,0,count);
				} while(count>0);

				testResponse.Close();
			}
			catch(WebException ex)
			{
				throw ex;
			}
			finally
			{
			}

			return new ReturnedASPPage(responseString);
		}


		public static ReturnedASPPage PostASPPage(string url,string postData)
		{
			WebRequest testRequest = WebRequest.Create(url);
			testRequest.Timeout=120000;

			WebResponse testResponse;
			//string dataString;
			Encoding encode=System.Text.Encoding.GetEncoding("utf-8");

			string responseString="";
			try
			{
				testRequest = WebRequest.Create(url);
				testRequest.Timeout=120000;
				testRequest.Method="POST";
				testRequest.ContentLength=encode.GetByteCount(postData);
				testRequest.ContentType="application/x-www-form-urlencoded";
				byte[] dataBytes=encode.GetBytes(postData);

				Stream reqStream=testRequest.GetRequestStream();
				reqStream.Write(dataBytes,0,(int)testRequest.ContentLength);
				reqStream.Close();

				// Retrieve the page
				testResponse = testRequest.GetResponse();
				Stream testResponseStream=testResponse.GetResponseStream();
				StreamReader readStream=new StreamReader(testResponseStream,encode);
				Char[] testResponseArray = new Char[1000];
				int count;

				do
				{
					count=readStream.Read(testResponseArray,0,1000);
					responseString+=new String(testResponseArray,0,count);
				} while(count>0);

				testResponse.Close();
			}
			catch(WebException ex)
			{
				throw ex;
			}
			finally
			{
			}

			return new ReturnedASPPage(responseString,"POST",postData);
		}


		public string PageString{get{return pageString;}}
		public string ViewStateString{get{return viewStateString;}}
		public string EventTargetString{get{return eventTargetString;}}
		public string EventArgumentString{get{return eventArgumentString;}}
		public string ViewStateStringEnc{get{return HttpUtility.UrlEncode(viewStateString);}}
		public string EventTargetStringEnc{get{return HttpUtility.UrlEncode(eventTargetString);}}
		public string EventArgumentStringEnc{get{return HttpUtility.UrlEncode(eventArgumentString);}}

		public override string ToString(){return pageString;}
	}
}
