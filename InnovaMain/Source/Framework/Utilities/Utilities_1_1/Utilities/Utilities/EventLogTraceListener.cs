using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Configuration;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// 30MAY03 - SC - Added Trace class to allow diagnostic data to be pushed asynchronously
	///					to an IP Port.
	/// </summary>
	/// 
	public class EventLogTraceListener : TraceListener
	{
		const String DEFAULT_TARGET = "localhost";
		const int DEFAULT_PORT = 13000;

		String strTarget;
		int iTargetPort;

		public EventLogTraceListener()
		{
			try
			{
                strTarget = ConfigurationManager.AppSettings["DIAGNOSTICS_TRACE_TARGET"];
				if( strTarget == null )
					strTarget = DEFAULT_TARGET;

                String strTargetPort = ConfigurationManager.AppSettings["DIAGNOSTICS_TRACE_TARGET_PORT"];
				if( strTargetPort == null )
					iTargetPort = DEFAULT_PORT;
				else
					iTargetPort = Convert.ToInt32( strTargetPort );
				this.Name = "TX360EnterpriseListener";
			}
			catch
			{
				strTarget = DEFAULT_TARGET;
				iTargetPort = DEFAULT_PORT;
			}
		}

		public override void Write( string strEvent )
		{
			// do nothing.
		}
	
		public override void WriteLine( string strEvent )
		{ 
			try
			{
				UdpClient udpClient = new UdpClient();
				Byte[] sendBytes = Encoding.ASCII.GetBytes(strEvent);
				udpClient.Send(sendBytes, sendBytes.Length, strTarget, iTargetPort );

				udpClient.Close();
			}
			catch (Exception e ) 
			{
				Console.WriteLine(e.ToString());
			}		
		}

		public override void WriteLine( object objEvent )
		{

		}

	}
}

