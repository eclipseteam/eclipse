using System;
using System.Reflection;
using System.Collections;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// Global Static Functions
	/// </summary>
	public class Global
	{
		private Global(){}	//allow nobody to "new" this class

        public static string GetVersionString(string fullName)
		{
			int versionStartIndex=fullName.IndexOf("Version",0);
			int versionValueFirstIndex=fullName.IndexOf("=",versionStartIndex)+1;
			int versionValueDotIndex=fullName.IndexOf(".",versionValueFirstIndex+1);
			versionValueDotIndex=fullName.IndexOf(".",versionValueDotIndex+1);
			versionValueDotIndex=fullName.IndexOf(".",versionValueDotIndex+1);

			if(versionValueFirstIndex>=0 && versionValueDotIndex>=versionValueFirstIndex)
				return fullName.Substring(versionValueFirstIndex,versionValueDotIndex-versionValueFirstIndex);
			else
				return "";
		}
        public static string GetVersionString(Assembly callingAssembly)
        {
            return GetVersionString(callingAssembly.FullName);
        }
        public static string GetUnderscoreVersionString(string fullName)
		{
			int versionStartIndex=fullName.IndexOf("Version",0);
			int versionValueFirstIndex=fullName.IndexOf("=",versionStartIndex)+1;
			int versionValueFirstDotIndex=fullName.IndexOf(".",versionValueFirstIndex);
			int versionValueSecondDotIndex=fullName.IndexOf(".",versionValueFirstDotIndex+1);

			if(versionValueFirstIndex>=0 && versionValueFirstDotIndex>=versionValueFirstIndex && versionValueSecondDotIndex>=versionValueFirstDotIndex)
			{
				string versionInfo=fullName.Substring(versionValueFirstIndex,versionValueFirstDotIndex-versionValueFirstIndex)
					+"_"
					+fullName.Substring(versionValueFirstDotIndex+1,versionValueSecondDotIndex-versionValueFirstDotIndex-1);
					return versionInfo;
			}
			else
				return "";
		}
        public static string GetUnderscoreVersionString(Assembly callingAssembly)
        {
            return GetUnderscoreVersionString(callingAssembly.FullName);
        }
	}

}
