using System;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// Represents a token
	/// </summary>
	[Serializable]
	public class Token
	{
		#region Constants
		private const int TokenLength = 16;
		private const byte ZeroTokenByte = 0x0;
		private const byte IllegalTokenByte = 0xff;
		private const byte IllegalTokenCounterLength = 2;
		#endregion
		#region Member variables
		private byte[] m_objByte;
		#endregion
		#region Constructors
		public Token()
		{
			this.m_objByte = Token.Empty.m_objByte;
		}

		public Token( string strToken )
		{
			if ( strToken.Length != TokenLength * 2 ) 
				throw new ArgumentException ( "Invalid character token, string must be " + ( TokenLength * 2 ) + " characters long" ); 

			m_objByte = new byte[ TokenLength ]; 

			for ( int intStringPosition = 0 , intArrayPosition = 0; intStringPosition < strToken.Length; intStringPosition += 2, intArrayPosition++ ) 
			{
				string strHexDigit = strToken.Substring( intStringPosition, 2 );
				this.m_objByte[ intArrayPosition ] = byte.Parse( strHexDigit, NumberStyles.AllowHexSpecifier );
			} 

		}

		public Token( byte[] objToken )
		{
			this.m_objByte = objToken;
		}
		#endregion
		#region Standard tokens
		public static Token Empty
		{
			get
			{
				return CreateFixedToken( ZeroTokenByte );
			}
		}

		public static Token Illegal
		{
			get
			{
				Token objInvalidToken = CreateFixedToken( IllegalTokenByte );
				objInvalidToken.SetIllegalTokenCounter( 0 );
				return objInvalidToken;
			}
		}
		#endregion
		#region Properties
		public bool IsIllegalToken
		{
			get
			{
				for( int intByteCounter = 0; intByteCounter < (TokenLength - IllegalTokenCounterLength); intByteCounter++ )
				{
					if( this.m_objByte[ intByteCounter ] != IllegalTokenByte )
						return false;
				}

				return true;
			}
		}

		public byte[] Hash
		{
			get
			{
				return this.m_objByte;
			}
		}
		#endregion
		#region Methods
		private void SetIllegalTokenCounter( ushort ushtTokenCounter )
		{
			if ( !this.IsIllegalToken )
				throw new Exception( "Token.SetIllegalTokenCounter, token is not an illegal token" );

			this.m_objByte[ 14 ] = Convert.ToByte( ushtTokenCounter >> 8 );
			this.m_objByte[ 15 ] = Convert.ToByte( ushtTokenCounter & 0xFF );
		}

		private ushort GetIllegalTokenCounter( )
		{
			if ( !this.IsIllegalToken )
				throw new Exception( "Token.GetIllegalTokenCounter, token is not an illegal token" );

			return Convert.ToUInt16( ( this.m_objByte[ 14 ] * 256 ) + this.m_objByte[ 15 ] );
		}

		private static Token CreateFixedToken( byte bytElement )
		{
			byte[] objArray = new byte[ TokenLength ];

			for ( int intByteCounter = 0 ; intByteCounter < TokenLength; intByteCounter++ )
				objArray[ intByteCounter ] = bytElement;

			return new Token( objArray );
		}

		public void IncrementIllegalToken( )
		{
			ushort ushtTokenCounter;

			if ( this.IsIllegalToken )
				ushtTokenCounter = this.GetIllegalTokenCounter( );
			else
				ushtTokenCounter = 0;

			if ( ushtTokenCounter == ushort.MaxValue )
				ushtTokenCounter = 0;
			else
				ushtTokenCounter++;

			this.m_objByte = Token.Illegal.m_objByte;
			this.SetIllegalTokenCounter( ushtTokenCounter );
		}

		public override string ToString()
		{
			StringBuilder objTokenString = new StringBuilder();

			for(int intByteCounter = 0; intByteCounter < TokenLength; intByteCounter++)
			{
				objTokenString.Append( this.m_objByte[ intByteCounter ].ToString( "X2" ) );
			}

			return objTokenString.ToString();
		}

		public Token Copy( )
		{
			return new Token( this.m_objByte );
		}

		public void CalculateToken( byte[] objObjectToToken )
		{			
			if ( this.IsIllegalToken )
				this.IncrementIllegalToken( );
			else
			{
				MD5CryptoServiceProvider objHasher = new MD5CryptoServiceProvider( );
				this.m_objByte = objHasher.ComputeHash( objObjectToToken );
				objHasher.Clear( );
			}
		}
        public void CalculateToken()
        {
            if (this.IsIllegalToken)
                this.IncrementIllegalToken();
            else
                this.m_objByte = Guid.NewGuid().ToByteArray();
        }

		#endregion
		#region Operator overloads
		public override bool Equals( object objToken )
		{
			if ( (object) objToken == null )
				return false;

			if( Token.ReferenceEquals( objToken, this ) )
				return true;

			if ( objToken is Token )
			{
				Token objToken1 = (Token) objToken;
				byte[] objToken1Array = objToken1.m_objByte;
				byte[] objToken2Array = this.m_objByte;

				if(objToken1Array.Length!=objToken2Array.Length)
				{
					return false;
				}

				for( int intByteCounter = 0; intByteCounter < objToken1Array.Length; intByteCounter++ )
				{
					if( objToken1Array[ intByteCounter ] != objToken2Array[ intByteCounter ] )
						return false;
				}

				return true;
			}

			return false;
		}

		public static bool operator == ( Token objToken1, Token objToken2 )
		{
			if( (object) objToken1 == null && (object) objToken2 == null )
				return true;

			if ( ( (object) objToken1 == null && (object) objToken2 != null ) ||
				 ( (object) objToken1 != null && (object) objToken2 == null ) )
				return false;

			return objToken1.Equals( objToken2 );
		}

		public static bool operator != ( Token objToken1, Token objToken2 )
		{
			return !( objToken1 == objToken2 );
		}

		public override int GetHashCode()
		{
			return base.GetHashCode ();
		}
		#endregion
	}
}
