using System;
using System.Globalization;

namespace Oritax.TaxSimp.Utilities
{
    /// <summary>
    /// Summary description for StringUtilities.
    /// </summary>
    public class StringUtilities
    {
        private StringUtilities()
        {
        }

        /// <summary>
        /// Removes Blank spaces and outs white spaces at beginning and end of instances
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>

        public static string RemoveRecurringSpaces(string text)
        {

            String strTemp = text.Trim();
            //IndexOf function would return  -1 when no such pattern exists
            while (strTemp.IndexOf("  ") != -1)
                strTemp = strTemp.Replace("  ", " ");
            return strTemp;

        }


    }

    /// <summary>
    /// Summary description for StringUtility.
    /// </summary>
    public static class StringUtility
    {

        /// <summary>
        //    '(1,789.34)' converted to -1789.34.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>

        public static CurrencyValue ToCurrency(this String text)
        {
            string value = text.Trim();
            if (value == null || value == "") value = "0";

            NumberStyles style;
            decimal number;

            // Parse string with negative value in parentheses           
            style = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands |
                    NumberStyles.AllowParentheses;
            number = Decimal.Parse(value, style);

            return new CurrencyValue { dcValue = number };
        }


    }





}
