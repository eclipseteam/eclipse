using System;
using System.Collections;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// Creates an AssemblyName from a string
	/// The .Net AssemblyName Class does not construct from a string
	/// </summary>
	/// 
	[Serializable]
	public class AssemblyFullName
	{
		private Hashtable ht;

		public AssemblyFullName(string FullName)
		{
			ht = ParseFullName(FullName);
		}

		private Hashtable ParseFullName(string fn)
		{
			Hashtable htFullName = new Hashtable(4);
	
			char [] delim = {','};
			int equalsLocation;

			string [] split = fn.Split(delim);

			for(int i=0; i<split.Length; i++)
			{
				equalsLocation = split[i].IndexOf('=');
				if(equalsLocation != -1)
				{
					htFullName[split[i].Substring(1,equalsLocation-1)] = split[i].Substring(equalsLocation+1);
				}
				else
				{
					htFullName["StrongName"] = split[i];
				}
			}
			return htFullName;
		}

		public override string ToString()
		{
			return ht["StrongName"].ToString() + ", " 
				+ "Version=" + ht["Version"].ToString() + ", "
				+ "Culture=" + ht["Culture"].ToString() + ", "
				+ "PublicKeyToken=" + ht["PublicKeyToken"].ToString();
		}

		public string StrongName
		{
			get
			{
				return ht["StrongName"].ToString();
			}
			set
			{
				ht["StrongName"] = value;
			}
		}

		public Version Version
		{
			get
			{
				return new Version(ht["Version"].ToString());
			}
			set
			{
				ht["Version"] = value.ToString();
			}
		}

		public string Culture
		{
			get
			{
				return ht["Culture"].ToString();
			}
			set
			{
				ht["Culture"] = value;
			}
		}

		public string PublicKeyToken
		{
			get
			{
				return ht["PublicKeyToken"].ToString();
			}
			set
			{
				ht["PublicKeyToken"] = value;
			}
		}
	}
}
