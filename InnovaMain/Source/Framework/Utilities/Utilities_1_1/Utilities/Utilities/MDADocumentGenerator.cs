﻿using System;
using System.Collections.Generic;
using System.Linq;
using Oritax.TaxSimp.DataSets;
using System.Data;
using Aspose.Words;
using System.IO;

namespace Oritax.TaxSimp.Utilities
{
    public class MDADocumentGenerator
    {


        public string GetMDANewFilePath(string clientID, string ext)
        {
            string fileName = string.Format("{0}_MDA_Agreement_{1}.{2}", clientID, DateTime.Now.ToString("dd_MM_yy_hh_mm_ss"), ext);
            string appPath = Path.GetTempPath();
            string filePath = string.Format("{0}\\{1}", appPath, fileName);
            return filePath;
        }
        public string CreateMDARenewalDocument(DataRow row, List<Data.ModelEntity> allModels, bool difm, bool diwm)
        {
            //Collecting Adviser and Client information for email body and agreement template.
            string adviserName = row[ClientDetailTable.ADVISERNAME].ToString().TrimStart();
            string clientName = row[ClientDetailTable.CLIENTNAME].ToString();
            string clientCode = row[ClientDetailTable.CLIENTID].ToString();
            string ifaName = row[ClientDetailTable.IFANAME].ToString();
            string dealerGroupName = row[ClientDetailTable.DGNAME].ToString();
            string dealerGroupAcn = row[ClientDetailTable.DGACN].ToString().TrimEnd();

            string difmProgramCode = row[ClientDetailTable.DIFMPROGRAMCODE].ToString();
            string difmProgramName = row[ClientDetailTable.DIFMPROGRAMNAME].ToString();


            string diwmProgramCode = row[ClientDetailTable.DIWMPROGRAMCODE].ToString();
            string diwmProgramName = row[ClientDetailTable.DIWMPROGRAMNAME].ToString();
            //calculate due date and current date
            DateTime mdaExecutionDate = DateTime.Parse(row[ClientDetailTable.MDAEXECUTIONDATE].ToString());

            // double sumSharePercentage = 0;

            string serviceType = "Do It With Me";
            string progamCode = diwmProgramCode.TrimStart(new[] { '0' });
            string progamName = diwmProgramName;
            byte[] resource = Properties.Resources.UMA_DIWM_Only_MDA_Renewal_Template;
            if (difm)
            {
                resource = Properties.Resources.MDA_Agreeement_Template;
                serviceType = "Do It For Me";
                progamCode = difmProgramCode.TrimStart(new[] { '0' });
                progamName = difmProgramName;
            }

            if (difm && diwm)
            {
                resource = Properties.Resources.UMA_DIFM___DIWM_MDA_Renewal_Template;
                serviceType = "Do It For Me, Do It With Me";
                progamCode = difmProgramCode.TrimStart(new[] { '0' });
                progamName = difmProgramName;
            }

            var stream = new MemoryStream(resource);
            var document = new Document(stream);
            string outPath = GetMDANewFilePath(clientCode, "PDF");

            //This is portion for the Header Section of the Document
            document.Range.Replace("[ClientName]", string.Format(" {0}", clientName.TrimStart()), false, false);
            document.Range.Replace("[CurrentDate]", DateTime.Now.ToString("dd/MM/yyyy"), false, false);
            document.Range.Replace("[AdviserName]", adviserName, false, false);
            document.Range.Replace("[IFA]", ifaName, false, false);
            document.Range.Replace("[DealerGroup]", dealerGroupName, false, false);
            document.Range.Replace("[DealerGroupACN]", dealerGroupAcn, false, false);
            document.Range.Replace("[InvType]", serviceType, false, false);
            document.Range.Replace("[ExDate]", mdaExecutionDate.ToString("dd/MM/yyyy"), false, false);
            document.Range.Replace("[InvProgram]", progamName, false, false);
            document.Range.Replace("[InvProgCode]", progamCode, false, false);
            if (difm)
            {
                FillModelData(allModels, difmProgramCode, document);
            }
            document.Save(outPath, SaveFormat.Pdf);
            return outPath;
        }
        private void FillModelData(List<Data.ModelEntity> allModels, string difmProgramCode, Document document)
        {
            //Hard coded becuase these will remain same. 
            const string assetClassCodes = "fi,psa,alt,aeq,gs,pro,cash";
            var model = allModels.FirstOrDefault(ss => ss.ProgramCode == difmProgramCode);

            if (model != null && model.Assets != null)
            {
                double sumSharePercentage = 0;
                foreach (var asset in model.Assets)
                {
                    if (assetClassCodes.Contains(asset.Name.ToLower()))
                    {
                        if (document.Range.Text.Contains(string.Format("[{0}Min]", asset.Name.ToLower())))
                            document.Range.Replace(string.Format("[{0}Min]", asset.Name.ToLower()), Convert.ToSingle(asset.MinAllocation.ToString()).ToString("N"), false, false);
                        else
                            document.Range.Replace(string.Format("[{0}Min]", asset.Name.ToLower()), "0.00", false, false);

                        if (document.Range.Text.Contains(string.Format("[{0}Target]", asset.Name.ToLower())))
                            document.Range.Replace(string.Format("[{0}Target]", asset.Name.ToLower()), Convert.ToSingle(asset.SharePercentage.ToString()).ToString("N"), false, false);
                        else
                            document.Range.Replace(string.Format("[{0}Target]", asset.Name.ToLower()), "0.00", false, false);

                        if (document.Range.Text.Contains(string.Format("[{0}Max]", asset.Name.ToLower())))
                            document.Range.Replace(string.Format("[{0}Max]", asset.Name.ToLower()), Convert.ToSingle(asset.MaxAllocation.ToString()).ToString("N"), false, false);
                        else
                            document.Range.Replace(string.Format("[{0}Max]", asset.Name.ToLower()), "0.00", false, false);

                        sumSharePercentage += asset.SharePercentage;
                    }
                }
                
                //Checking remaing asset and apply default value, if any.
                string[] assetArray = assetClassCodes.ToLower().Split(',');
                foreach (var s in assetArray)
                {
                    if (document.Range.Text.Contains(string.Format("[{0}Min]", s)))
                        document.Range.Replace(string.Format("[{0}Min]", s), "0.00", false, false);
                    if (document.Range.Text.Contains(string.Format("[{0}Target]", s)))
                        document.Range.Replace(string.Format("[{0}Target]", s), "0.00", false, false);
                    if (document.Range.Text.Contains(string.Format("[{0}Max]", s)))
                        document.Range.Replace(string.Format("[{0}Max]", s), "0.00", false, false);
                }

                //Here we add the Total Colunm
                document.Range.Replace("[totalTarget]", Convert.ToSingle(sumSharePercentage).ToString("N"), false, false);
            }
            //Checking remaing asset and apply default value, if any and remaining value empty.
            string[] assetArr = assetClassCodes.ToLower().Split(',');
            foreach (var s in assetArr)
            {
                if (document.Range.Text.Contains(string.Format("[{0}Min]", s)))
                    document.Range.Replace(string.Format("[{0}Min]", s), "0.00", false, false);
                if (document.Range.Text.Contains(string.Format("[{0}Target]", s)))
                    document.Range.Replace(string.Format("[{0}Target]", s), "0.00", false, false);
                if (document.Range.Text.Contains(string.Format("[{0}Max]", s)))
                    document.Range.Replace(string.Format("[{0}Max]", s), "0.00", false, false);
            }

        }

    }
}
