using System;
using System.Collections;
using System.Data;
using System.Runtime.Serialization;

namespace Oritax.TaxSimp.Utilities
{
	public static class Serialize
	{
		//private Serialize(){}	//allow nobody to "new" this class

		[Serializable]
		private class NullObject
		{
		}

		static NullObject no = new NullObject();

		/// <summary>
		///	Returns the named value from the serialization stream for custom serialization
		///	If the value does not exist then the default is returned
		/// </summary>
		/// <param name="si">Serialization Stream</param>
		/// <param name="name">Name/Key of the value to retrieve</param>
		/// <param name="type">Type of the value to be retrieved</param>
		/// <param name="defaultValue">A default value if the named value is not in the stream</param>
		/// <returns>A fully constructed value of the correct type taken from the serialzation stream</returns>
		public static object GetSerializedValue(SerializationInfo si, string name, Type type, object defaultValue)
		{
			object val=null;
			try
			{
				val = si.GetValue(name,typeof(object));
			}
			catch (SerializationException)
			{
				val = defaultValue;
				return val;
			}

			if (val is NullObject)
			{
				return null;
			}
			return val;
		}

        public static T GetValue<T>(this SerializationInfo info, string name, T defaultValue) 
        {
            T value = defaultValue;
            try
            {
                value = (T)info.GetValue(name, typeof(T));
                if (value is NullObject)
                {
                    value = defaultValue;
                }

            }
            catch (SerializationException)
            {
                value = defaultValue;
            }
            return value;
        }

        public static T GetValue<T>(this SerializationInfo info, string name) where T : class, new()
        {
            T value = null;
            try
            {
                value = info.GetValue(name, typeof(T)) as T;
                if (value is NullObject)
                {
                    value = null;
                }

            }
            catch (SerializationException)
            {
                value = new T();
            }
            return value;
        }

		/// <summary>
		///	Returns the named value from the serialization stream for custom serialization
		///	If the value does not exist then a new Guid is returned
		/// </summary>
		/// <param name="si">Serialization Stream</param>
		/// <param name="name">Name/Key of the value to retrieve</param>
		/// <returns>A fully constructed Guid</returns>
		public static Guid GetSerializedGuid(SerializationInfo si, string name)
		{
			object val = GetSerializedValue(si,name,typeof(Guid),null);

			if(val == null)
				return Guid.Empty;
			return (Guid)val;
		}

        public static Guid GetGuidX(this SerializationInfo si, string name)
        {
            return GetSerializedGuid(si, name);
        }

		/// <summary>
		///	Returns the named value from the serialization stream for custom serialization
		///	If the value does not exist then an Empty string is returned
		/// </summary>
		/// <param name="si">Serialization Stream</param>
		/// <param name="name">Name/Key of the value to retrieve</param>
		/// <returns>A fully constructed string</returns>
		public static string GetSerializedString(SerializationInfo si, string name)
		{
			object val = GetSerializedValue(si,name,typeof(string),null);

			if(val == null)
				return null;
			return (string)val;
		}

        public static string GetStringX(this SerializationInfo si, string name)
        {
            return GetSerializedString(si, name);
        }

		/// <summary>
		///	Returns the named value from the serialization stream for custom serialization
		///	If the value does not exist then false is returned
		/// </summary>
		/// <param name="si">Serialization Stream</param>
		/// <param name="name">Name/Key of the value to retrieve</param>
		/// <returns>A Boolean</returns>
		public static bool GetSerializedBool(SerializationInfo si, string name)
		{
			object val = GetSerializedValue(si,name,typeof(bool),null);

			if(val == null)
				return false;
			return (bool)val;
		}

        public static bool GetBoolX(this SerializationInfo si, string name)
        {
            return GetSerializedBool(si, name);
        }

		/// <summary>
		///	Returns the named value from the serialization stream for custom serialization
		///	If the value does not exist then a DateTime is returned
		/// </summary>
		/// <param name="si">Serialization Stream</param>
		/// <param name="name">Name/Key of the value to retrieve</param>
		/// <returns>A DateTime</returns>
		public static System.DateTime GetSerializedDateTime(SerializationInfo si, string name)
		{
			object val = GetSerializedValue(si,name,typeof(object),null);

			if (val is Date)
			{
				return ((Date)val).DateTime;
			}

            if (val == null)
            {

                return new System.DateTime(DateTime.Now.Ticks);
            }
		    return (System.DateTime)val;
		}

        public static System.DateTime GetSerializedDateTimeWithMin(SerializationInfo si, string name)
        {
            object val = GetSerializedValue(si, name, typeof(object), null);

            if (val is Date)
            {
                return ((Date)val).DateTime;
            }

            if (val == null)
            {

                return new System.DateTime(DateTime.MinValue.Ticks);
            }
            return (System.DateTime)val;
        }
        public static DateTime GetDateTimeX(this SerializationInfo si, string name)
        {
            return GetSerializedDateTime(si, name);
        }

		/// <summary>
		/// This function should be used for ArrayLists that are field variables
		///	Returns the named value from the serialization stream for custom serialization
		///	If the value does not exist then a new ArrayList is returned
		/// </summary>
		/// <param name="si">Serialization Stream</param>
		/// <param name="name">Name/Key of the value to retrieve</param>
		/// <returns>An ArrayList</returns>
		public static ArrayList GetSerializedArrayList(SerializationInfo si, string name)
		{
			object val = GetSerializedValue(si,name,typeof(ArrayList),null);

			if(val == null)
				return null;
			return (ArrayList)val;
		}

        public static ArrayList GetArrayListX(this SerializationInfo si, string name)
        {
            return GetSerializedArrayList(si, name);
        }

		/// <summary>
		/// This function should be used for hashtables that are field variables
		///	Returns the named value from the serialization stream for custom serialization
		///	If the value does not exist then a new Hashtable is returned
		/// </summary>
		/// <param name="si">Serialization Stream</param>
		/// <param name="name">Name/Key of the value to retrieve</param>
		/// <returns>A Hashtable</returns>
		public static SerializableHashtable GetSerializedHashtable(SerializationInfo si, string name)
		{
			object val = GetSerializedValue(si,name,typeof(SerializableHashtable),null);

			if(val == null)
				return null;
			return (SerializableHashtable)val;
		}

        public static SerializableHashtable GetSerializableHashtableX(this SerializationInfo si, string name)
        {
            return GetSerializedHashtable(si, name);
        }

		/// <summary>
		///	Returns the named value from the serialization stream for custom serialization
		///	If the value does not exist then a new Percent is returned
		/// </summary>
		/// <param name="si">Serialization Stream</param>
		/// <param name="name">Name/Key of the value to retrieve</param>
		/// <returns>A Percent</returns>
		public static Percent GetSerializedPercent(SerializationInfo si, string name)
		{
			object val = GetSerializedValue(si,name,typeof(Percent),null);

			if(val == null)
				return null;

			return (Percent)val;
		}

        public static Percent GetPercentX(this SerializationInfo si, string name)
        {
            return GetSerializedPercent(si, name);
        }

		/// <summary>
		///	Returns the named value from the serialization stream for custom serialization
		///	If the value does not exist then a new Currency Value is returned
		/// </summary>
		/// <param name="si">Serialization Stream</param>
		/// <param name="name">Name/Key of the value to retrieve</param>
		/// <returns>A CurrencyValue</returns>
		public static CurrencyValue GetSerializedCurrencyValue(SerializationInfo si, string name)
		{
			object val = GetSerializedValue(si,name,typeof(CurrencyValue),null);

			if(val == null)
				return null;

			return (CurrencyValue)val;
		}

        public static CurrencyValue GetCurrencyValueX(this SerializationInfo si, string name)
        {
            return GetSerializedCurrencyValue(si, name);
        }

		/// <summary>
		///	Returns the named value from the serialization stream for custom serialization
		///	If the value does not exist then 0 is returned
		/// </summary>
		/// <param name="si">Serialization Stream</param>
		/// <param name="name">Name/Key of the value to retrieve</param>
		/// <returns>A System.UInt16</returns>
		public static System.UInt16 GetSerializedUInt16(SerializationInfo si, string name)
		{
			object val = GetSerializedValue(si,name,typeof(System.UInt16),null);

			if(val == null)
				return 0;
			return (System.UInt16)val;
		}

        public static UInt16 GetUInt16X(this SerializationInfo si, string name)
        {
            return GetSerializedUInt16(si, name);
        }

		/// <summary>
		///	Returns the named value from the serialization stream for custom serialization
		///	If the value does not exist then 0 is returned
		/// </summary>
		/// <param name="si">Serialization Stream</param>
		/// <param name="name">Name/Key of the value to retrieve</param>
		/// <returns>A System.UInt32</returns>
		public static System.UInt32 GetSerializedUInt32(SerializationInfo si, string name)
		{
			object val = GetSerializedValue(si,name,typeof(System.UInt32),null);

			if(val == null)
				return 0;
			return (System.UInt32)val;
		}

        public static UInt32 GetUInt32X(this SerializationInfo si, string name)
        {
            return GetSerializedUInt32(si, name);
        }

		/// <summary>
		///	Returns the named value from the serialization stream for custom serialization
		///	If the value does not exist then 0 is returned
		/// </summary>
		/// <param name="si">Serialization Stream</param>
		/// <param name="name">Name/Key of the value to retrieve</param>
		/// <returns>A System.Int16</returns>
		public static System.Int16 GetSerializedInt16(SerializationInfo si, string name)
		{
			object val = GetSerializedValue(si,name,typeof(System.Int16),null);

			if(val == null)
				return 0;
			return (System.Int16)val;
		}

        public static Int16 GetInt16X(this SerializationInfo si, string name)
        {
            return GetSerializedInt16(si, name);
        }

		/// <summary>
		///	Returns the named value from the serialization stream for custom serialization
		///	If the value does not exist then 0 is returned
		/// </summary>
		/// <param name="si">Serialization Stream</param>
		/// <param name="name">Name/Key of the value to retrieve</param>
		/// <returns>A System.Int32</returns>
		public static System.Int32 GetSerializedInt32(SerializationInfo si, string name)
		{
			object val = GetSerializedValue(si,name,typeof(System.Int32),null);

			if(val == null)
				return 0;
			return (System.Int32)val;
		}

        public static Int32 GetInt32X(this SerializationInfo si, string name)
        {
            return GetSerializedInt32(si, name);
        }

		/// <summary>
		///	Returns the named value from the serialization stream for custom serialization
		///	If the value does not exist then 0.0 is returned
		/// </summary>
		/// <param name="si">Serialization Stream</param>
		/// <param name="name">Name/Key of the value to retrieve</param>
		/// <returns>A System.Decimal</returns>
		public static System.Decimal GetSerializedDecimal(SerializationInfo si, string name)
		{
			object val = GetSerializedValue(si,name,typeof(System.Decimal),null);

			if(val == null)
				return 0;
			return (System.Decimal)val;
		}

        public static Decimal GetDecimalX(this SerializationInfo si, string name)
        {
            return GetSerializedDecimal(si, name);
        }

		/// <summary>
		///	Returns the named value from the serialization stream for custom serialization
		///	If the value does not exist then 0.0 is returned
		/// </summary>
		/// <param name="si">Serialization Stream</param>
		/// <param name="name">Name/Key of the value to retrieve</param>
		/// <returns>A System.Decimal</returns>
		public static System.Double GetSerializedDouble(SerializationInfo si, string name)
		{
			object val = GetSerializedValue(si,name,typeof(System.Double),null);

			if(val == null)
				return 0;
			return (System.Double)val;
		}

        public static Double GetDoubleX(this SerializationInfo si, string name)
        {
            return GetSerializedDouble(si, name);
        }

		/// <summary>
		/// Sets a named value into the serialization stream for custom serialization
		/// </summary>
		/// <param name="si">Serialization Stream</param>
		/// <param name="name">Name/Key of the value to set</param>
		/// <param name="objValue">The value to set</param>
		public static void AddSerializedValue(SerializationInfo si, string name, object objValue)
		{
			if(objValue != null)
			{
				si.AddValue(name, objValue, objValue.GetType());
			}
			else
			{
				si.AddValue(name,no,no.GetType());
			}
		}

        public static void AddValueX(this SerializationInfo si, string name, object value)
        {
            AddSerializedValue(si, name, value);
        }

		/// <summary>
		/// Sets a named value into the serialization stream for custom serialization and forces type
		/// </summary>
		/// <param name="si">Serialization Stream</param>
		/// <param name="name">Name/Key of the value to set</param>
		/// <param name="objValue">The value to set</param>
		/// <param name="type"></param>
		public static void AddSerializedValue(SerializationInfo si, string name, object objValue, Type type)
		{
			if(objValue != null)
			{
				si.AddValue(name, objValue, type);
			}
			else
			{
				si.AddValue(name,no,no.GetType());
			}
		}
	}
}
