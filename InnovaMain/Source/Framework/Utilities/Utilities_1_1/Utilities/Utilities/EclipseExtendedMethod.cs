﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Oritax.TaxSimp.Utilities
{
    public static class EclipseExtendedMethod
    {
        public static string FormatToCurrency(this double? amount)
        {
            double tempAmount = amount.HasValue ? amount.Value : 0;

            if (tempAmount == 0)
                return string.Empty;

            return tempAmount.ToString("C", CultureInfo.CurrentCulture);
        }

        public static string FormatToCurrency(this double amount)
        {
            if (amount == 0)
                return string.Empty;

            return amount.ToString("C", CultureInfo.CurrentCulture);
        }

        public static string FormatEditable(this double amount)
        {
            if (amount == 0)
                return string.Empty;

            return Math.Round(amount, 2).ToString("0.00", CultureInfo.InvariantCulture);
        }



    }
}
