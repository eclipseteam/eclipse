using System;
using System.Diagnostics;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// An N-Able peformance counter that wraps .NET performance counters to disable the counter on errors
	/// </summary>
	public class NablePerformanceCounter
	{
		private string COUNTERCATEGORYNAME = "N-ABLE Tax Management Solution";
		private PerformanceCounter performanceCounter;
		private string counterName;
		private bool enabled;

		/// <summary>
		/// Creates an N-Able performance counter
		/// </summary>
		/// <param name="counterName"></param>
		/// <param name="enabled"></param>
		public NablePerformanceCounter(string counterName, bool enabled, string instanceName)
		{
			this.counterName = counterName;
			this.enabled = enabled;
			try
			{
				if (enabled)
					this.performanceCounter = new PerformanceCounter(COUNTERCATEGORYNAME, this.counterName, instanceName, false);
			}
			catch
			{
				this.enabled = false;
				//TODO:Log this event somewhere
			}
		}

		/// <summary>
		/// Increments a performance counter if the counter is enabled
		/// </summary>
		public void Increment()
		{
			if(this.enabled)
			{
				if(!PerformanceCounterCategory.Exists(COUNTERCATEGORYNAME))
				{
					this.enabled = false;
					return;
				}

				try
				{
					this.performanceCounter.Increment();
				}
				catch
				{
					// catch any exceptions when incrementing the counter and disable
					// the counter if there is a problem
					this.enabled = false;
					//TODO:Log this event somewhere
				}
			}
		}

		/// <summary>
		/// Sets the raw value of a counter if the counter is enabled
		/// </summary>
		/// <param name="rawValue">The value to set</param>
		public void SetRawValue(long rawValue)
		{
			if(this.enabled)
			{
				if(!PerformanceCounterCategory.Exists(COUNTERCATEGORYNAME))
				{
					this.enabled = false;
					return;
				}

				try
				{
					this.performanceCounter.RawValue = rawValue;;
				}
				catch
				{
					// catch any exceptions when incrementing the counter and disable
					// the counter if there is a problem
					this.enabled = false;
					//TODO:Log this event somewhere
				}
			}
		}
	}
}
