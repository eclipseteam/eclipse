using System;
using System.Collections.Generic;
using System.Text;

namespace Oritax.TaxSimp.Utilities
{
    public enum CMStatus
    {
        Completed,
        Started,
        NotStarted,
        Reviewed,
    }
}
