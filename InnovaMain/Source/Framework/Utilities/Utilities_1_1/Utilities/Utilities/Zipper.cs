﻿using System.IO;
using Ionic.Zip;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Utilities
{
    public class Zipper
    {
        public static byte[] CreateZip(SerializableDictionary<string, byte[]> output)
        {
            byte[] bytes = null;

            using (MemoryStream memory = new MemoryStream())
            {
                using (ZipFile zip = new ZipFile())
                {
                    foreach (var file in output)
                    {
                        ZipEntry e = zip.AddEntry(file.Key, file.Value);
                    }
                    zip.Save(memory);
                    bytes = memory.ToArray();
                }
            }
            return bytes;
        }
    }
}
