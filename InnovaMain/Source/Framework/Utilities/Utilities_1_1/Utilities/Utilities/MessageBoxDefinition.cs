using System;
using System.Collections;

namespace Oritax.TaxSimp.Utilities
{
    public enum MessageDisplayMethod
    {
        MainWindowError,
        MainWindowPopup
    }
    /// <summary>
    /// The following enum class is used as ControlType
    /// </summary>
    public enum ControlType
    {
        None,
        CheckBox,
        Button
    }


    /// <summary>
    /// Messagebox definition messages
    /// </summary>
    public class MessageBoxDefinition
    {
        #region Fields -----------------------------------------------------------------------------
        private static Hashtable messages;
        private int message = 0;
        private string[] args;

        public const int UNKNOWN = 0;
        public const int MSG_SEARCH_NORESULT = 1;
        public const int MSG_INVALID_DATE_FORMAT = 2;
        public const int MSG_INVALID_DATE_RANGE = 3;
        public const int MSG_INVALID_FUTURE_DATE = 4;
        public const int MSG_ENTER_ENTITY_NAME = 5;
        public const int MSG_SELECT_LEGAL_STRUCTURE = 6;
        public const int MSG_SELECT_LEGAL_STATUS = 7;
        public const int MSG_INVALID_TFN = 8;
        public const int MSG_INVALID_ABN = 9;
        public const int MSG_INVALID_ACN = 10;
        public const int MSG_DELETE_ENTITY_CONFIRMATION = 11;
        public const int MSG_DELETE_ENTITY_FAILURE = 12;
        public const int MSG_INVALID_PERCENTAGE = 13;
        public const int MSG_SELECT_CONSOLIDATION_METHOD = 14;
        public const int MSG_SELECT_HEAD_ENTITY = 15;
        public const int MSG_PERIOD_EXISTS = 16;
        public const int MSG_INVALID_PERIOD = 17;
        public const int MSG_PERIOD_EXISTS_GROUP_MEMBER = 18;
        public const int MSG_SCENARIO_EXISTS_GROUP_MEMBER = 19;
        public const int MSG_CM_ALREADY_EXISTS = 20;
        public const int MSG_ENTITY_NAME_EXISTS = 21;
        public const int MSG_INVALID_DESCRIPTION = 22;
        public const int MSG_INVALID_REFERENCE = 23;
        public const int MSG_INVALID_AMOUNT = 24;
        public const int MSG_EXCEEDED_AMOUNT = 25;
        public const int MSG_MANDATORY_DESC = 26;
        public const int MSG_MANDATORY_FILE = 27;
        public const int MSG_FILE_NOT_FOUND = 28;
        public const int MSG_UNABLE_TO_ATTACH = 29;
        public const int MSG_MANDATORY_DEST = 30;
        public const int MSG_UNABLE_TO_ADD_NOTE = 31;
        public const int MSG_INVALID_CHARACTERS = 32;
        public const int MSG_INVALID_CALC_CONFIGURE = 33;
        public const int MSG_SCENARIO_EXISTS = 34;
        public const int MSG_DELETE_SCENARIO_CONFIRMATION = 35;
        public const int MSG_DELETE_SCENARIO_FAILURE = 36;
        public const int MSG_DELETE_SCENARIO_FAILURE_GROUPMEMBERS = 37;
        public const int MSG_CM_DELETE_CONFIRMATION = 38;
        public const int MSG_INVALID_NAME_CHARACTERS = 39;
        public const int MSG_PERIOD_OVERLAP = 40;
        public const int MSG_GROUP_MEMBER_CIRCULAR_REF = 41;
        public const int MSG_INSUFFICIENT_AUTHORISATION = 42;
        public const int MSG_USER_GROUP_UNIQUE = 43;
        public const int MSG_PASSWORD_VERIFY_FAIL = 44;
        public const int MSG_PASSWORD_EMPTY = 45;
        public const int MSG_PASSWORD_VALIDATION_FAILED = 46;
        public const int MSG_PASSWORD_NO_MATCH = 47;
        public const int MSG_HEAD_ENTITY_DELETE = 48;
        public const int MSG_CONFIRM_DISCARD = 49;
        public const int MSG_UNABLE_TO_PERSIST = 50;
        public const int MSG_ENTER_SCENARIO_LABEL = 51;
        public const int MSG_DELETE_LAST_SCENARIO = 52;
        public const int MSG_UPDATE_ENTITY_STATUS_FAILURE = 53;
        public const int MSG_DELETE_PERIOD_CONFIRMATION = 54;
        public const int MSG_USER_GROUP_DELETE_CONFIRMATION = 55;
        public const int MSG_ENTER_DISPLAY_NAME = 56;
        public const int MSG_UPDATE_GROUP_STATUS_FAILURE = 57;
        public const int MSG_CURRENT_PASSWORD_INVALID = 58;
        public const int MSG_INVALID_SAP_START_DATE = 59;
        public const int MSG_INVALID_TRANSITION_BALANCER = 60;
        public const int MSG_SAP_DELETE_CONFIRMATION = 61;
        public const int MSG_SAP_OVERLAP_GROUP = 62;
        public const int MSG_SAP_INVALID_BALANCER = 63;
        public const int MSG_DELETE_SAP_FAILURE_GROUP = 64;
        public const int MSG_SAP_EXISTS = 65;
        public const int MSG_INVALID_SAP_DURATION = 66;
        public const int MSG_INVALID_PERIOD_OVERLAP_GROUP = 67;
        public const int MSG_INVALID_SAP_END_DATE = 68;
        public const int MSG_INVALID_SAP_OVERLAP = 69;
        public const int MSG_INVALID_PERIOD_OVERLAP = 70;
        public const int MSG_FAILED_SEARCH = 71;
        public const int MSG_MEMBER_OWNERSHP_FAILURE = 72;
        public const int MSG_INVALID_PERIOD_GROUP_MEMBER = 73;
        public const int MSG_PERIOD_OVERLAP_GROUP_MEMBER = 74;
        public const int MSG_INVALID_ATO_END_DATE = 75;
        public const int MSG_SET_TAX_BALANCER = 76;
        public const int MSG_SAP_NOT_IN_MEMBER = 77;
        public const int MSG_DELETE_NOTE_CONFIRMATION = 78;
        public const int MSG_PRINT_NOTE = 79;
        public const int MSG_DELETE_ATTACH_CONFIRMATION = 80;
        public const int MSG_DELETE_ROW_CONFIRMATION = 81;
        public const int MSG_SAP_PERIOD_OVERLAP = 82;
        public const int MSG_SAP_PERIOD_OVERLAP_GROUP = 83;
        public const int MSG_SAP_OVERLAPS_MEMBER_SAP = 84;
        public const int MSG_IMPORT_SUCCESSFUL = 87;
        public const int MSG_UNIQUE_DESC = 88;
        public const int MSG_DATA_SOURCE_UNRECOGNISED = 90;
        public const int MSG_DATA_SOURCE_MISSING_COLUMNS = 91;
        public const int MSG_MANDATORY_FIELDS_NOT_MAPPED = 92;
        public const int MSG_DELETE_COA_CONFIRMATION = 93;
        public const int MSG_DELETE_ACCOUNT_CONFIRMATION = 94;
        public const int MSG_DELETE_TEMPLATE_CONFIRMATION = 95;
        public const int MSG_MANDATORY_ACCOUNT_NO = 96;
        public const int MSG_UNIQUE_ACCOUNT_NO = 97;
        public const int MSG_SELECT_TEMPLATE = 98;
        public const int MSG_RESET_MAP_TEMPLATE_CONFIRMATION = 99;
        public const int MSG_MANDATORY_TEMPLATE_NAME = 104;
        public const int MSG_UNIQUE_TEMPLATE_NAME = 105;
        public const int MSG_SAVE_JOURNAL_FAILURE = 106;
        public const int MSG_DELETE_JOURNAL_CONFIRMATION = 108;
        public const int MSG_CANCEL_IMPORT_CONFIRMATION = 109;
        public const int MSG_FILTER_EXCLUDES_ACCOUNT = 110;
        public const int MSG_FAILED_FILTER = 112;
        public const int MSG_INVALID_FILTER = 113;
        public const int MSG_DEBIT_AND_CREDIT_AMOUNTS = 116;
        public const int MSG_EDIT_JOURNAL_FAILURE = 117;
        public const int MSG_JOURNAL_NOT_BALANCED = 121;
        public const int MSG_INDIVIDUAL_COLUMN_MAP = 122;
        public const int MSG_AMOUNT_NOT_ENTERED = 123;
        public const int MSG_ACCOUNT_NOT_FOUND = 125;
        public const int MSG_INVALID_MEMBER_SCENARIO = 126;
        public const int MSG_INVALID_ARBN = 127;
        public const int MSG_ULT_HOLDING_FAILURE = 128;
        public const int MSG_MAIN_BUSINESS_DESC_FAILURE = 129;
        public const int MSG_RESIDENCY_CORP_FAILURE = 130;
        public const int MSG_NEGATIVE_VALUES = 131;
        public const int MSG_PDF_WARNING = 132;
        public const int MSG_ERR_PRIOR_YEAR_LOSS = 133;
        public const int MSG_WARN_PRIOR_YEAR_LOSS = 134;
        public const int MSG_WARN_CLOSING_BAL_NEGATIVE = 135;
        public const int MSG_ERR_MAX_CLAIM_TAX_LOSS = 137;
        public const int MSG_NO_CLAIM_PRIOR_YEAR_TAX_LOSS = 138;
        public const int MSG_MAX_CLAIM_PRIOR_YEAR_TAX_LOSS = 139;
        public const int MSG_WARN_REALLOCATION_NEGATIVE = 140;
        public const int MSG_EXCESS_NET_EXEMPT_INCOME_OFFSET = 141;
        public const int MSG_WARN_EXCESS_NET_INCOME = 142;
        public const int MSG_LOGIN_NAME_EMPTY = 143;
        public const int MSG_INVALID_LOGIN_NAME = 144;
        public const int MSG_LOGIN_NAME_EXISTS = 145;
        public const int MSG_FIRST_NAME_EMPTY = 146;
        public const int MSG_INVALID_FIRST_NAME = 147;
        public const int MSG_INVALID_LAST_NAME = 148;
        public const int MSG_LAST_NAME_EMPTY = 149;
        public const int MSG_INVALID_EMAIL = 150;
        public const int MSG_EMAIL_EMPTY = 151;
        public const int MSG_INVALID_PASSWORD_LENGTH = 152;
        public const int MSG_DELETE_USER_CONFIRMATION = 153;
        public const int MSG_USER_ACCOUNT_LOCKED = 154;
        public const int MSG_INSUFFICIENT_RIGHTS = 155;
        public const int MSG_STALE_DATA = 156;
        public const int MSG_BUS_STRUCT_ELEM_DELETED = 158;
        public const int MSG_BUS_STRUCT_ELEM_NO_PRIV = 159;
        public const int MSG_WORKPAPER_DELETED = 160;
        public const int MSG_WORKPAPER_NO_PRIV = 161;
        public const int MSG_INVALID_TOTAL_DIFFERENCE = 162;
        public const int MSG_INVALID_TOTAL_DIFFERENCE_DETAILS = 270;
        public const int MSG_POSITIVE_AMOUNT = 163;
        public const int MSG_MANDATORY_TAX_TARGET = 164;
        public const int MSG_IMPORT_TAX_TARGET_FAILURE = 165;
        public const int MSG_DELETE_TAX_TARGET_CONFIRMATION = 166;
        public const int MSG_DELETE_TAX_MAP_CONFIRMATION = 167;
        public const int MSG_MISSING_TAX_TARGETS = 168;
        public const int MSG_MAP_COLUMN_INVALID = 169;
        public const int MSG_MANDATORY_TAX_MAP = 170;
        public const int MSG_IMPORT_TAX_MAP_FAILURE = 171;
        public const int MSG_UNIQUE_COLUMN_MAP = 172;
        public const int MSG_INCOMPATIBLE_MAPS = 174;
        public const int MSG_COA_REFERENCED = 175;
        public const int MSG_PRIMARY_CALCALATION_MODULE = 176;
        public const int MSG_INVALID_LOGIN = 177;
        public const int MSG_UNCONFIGURED_ACCOUNTS = 178;
        public const int MSG_ERR_PARSING_FILE = 180;
        public const int MSG_DELETE_TAX_MAP_FAILURE = 181;
        public const int MSG_DELETE_TAX_SET_FAILURE = 182;
        public const int MSG_INVALID_POSTCODE = 183;
        public const int MSG_NEGATIVE_DISCLOSED_AMOUNT = 184;
        public const int MSG_NEGATIVE_DISCLOSED_AMOUNT_DETAILS = 271;
        public const int MSG_CODE_QUESTION_UNANSWERED = 185;
        public const int MSG_CODE_QUESTION_UNANSWERED_DETAILS = 272;
        public const int MSG_QUESTION_UNANSWERED = 186;
        public const int MSG_INVALID_HOURS_TAKEN = 188;
        public const int MSG_INCORRECT_ANSWER = 190;
        public const int MSG_INCORRECT_ANSWER_DETAILS = 273;
        public const int MSG_WARN_FOR_CLOSING_BAL_NEGATIVE = 191;
        public const int MSG_VAL_NEG_CLOSING_BALANCE = 192;
        public const int MSG_EXCESS_PRIOR_LOSS = 193;
        public const int MSG_VAL_EXCESS_PRIOR_LOSS = 194;
        public const int MSG_INVALID_CL_USED = 195;
        public const int MSG_ENTER_DISPLAY_NAME_GROUP = 196;
        public const int MSG_INVALID_FILE_EXTENSION = 197;
        public const int MSG_INVALID_IRDB = 200;
        public const int MSG_INVALID_AVAIL_FRACTION = 201;
        public const int MSG_COPY_PREVIOUS_NOTES = 202;
        public const int MSG_UNCONFIGURED_ACCOUNTS_ENTITY = 203;
        public const int MSG_CHARTS_NOT_DIFFERENT = 204;
        public const int MSG_REPORT_NO_DATA = 205;
        public const int MSG_APPLY_MAP_CONFIRMATION = 206;
        public const int MSG_TRANSFERRED_ERROR = 207;
        public const int MSG_DESC_AMOUNTS = 208;
        public const int MSG_IND_AMT_FOR = 210;
        public const int MSG_UNIQUE_SELECTION = 211;
        public const int MSG_ERR_ADJOINING_PERIOD = 212;
        public const int MSG_ERR_MATCHING_PERIODS = 213;
        public const int MSG_TRANSFER_ERROR = 214;
        public const int MSG_RANDD_TAX_ERROR = 215;
        public const int MSG_INVALID_MAP_COMBINATION = 216;
        public const int MSG_GROSS_TAX_INVALID = 217;
        public const int MSG_COMPLETE_THIN_CAP = 218;
        public const int MSG_NO_REPORT_SELECTED = 219;
        public const int MSG_INVALID_REPORT_PERIOD = 220;
        public const int MSG_ERR_MULTIPLE_CM_VERSIONS = 221;
        public const int MSG_ADDITIONS_TAX_VALUES_DISAGREES = 222;
        public const int MSG_DISPOSAL_TAX_VALUES_DISAGREES = 223;
        public const int MSG_AMOUNT_TRANSFERRED_DISAGREES = 224;
        public const int MSG_ADDITIONS_FINANCE_LEASE_DISAGREES = 226;
        public const int MSG_ADDITIONS_BOOK_DISAGREES = 227;
        public const int MSG_DISPOSAL_BOOK_DISAGREES = 228;
        public const int MSG_TAX_TARGET_CONTROL_TOTAL_DISAGREES = 229;
        public const int MSG_TRANSFER_AMOUNT_CONTROL_TOTAL_DISGAREES = 230;
        public const int MSG_COMPLETE_SCHED25A_PARTA = 231;
        public const int MSG_COMPLETE_SCHED25A_PARTB = 232;
        public const int MSG_INVALID_TRANS_CURRENCY = 233;
        public const int MSG_INVALID_RATIO = 234;
        public const int MSG_INVALID_OTHER_MOVEMENT = 235;
        public const int MSG_SAP_INVALID_GROUP_MEMBER = 236;
        public const int MSG_UNCONFIGURED_ACCOUNTS_NAMED_ENTITY = 237;
        public const int MSG_COA_RETAINED_PROFIT_MISSING_OR_DUPLICATE = 238;
        public const int MSG_JOURNAL_ANALYSIS_GENERAL_CONSOL_LEDGER_RPTDATA = 239;
        public const int MSG_ERROR_CONFIGURE_COMPONENTS = 241;
        public const int MSG_NO_CHART_SELECTED = 242;
        public const int MSG_SELECTED_LEDGER_MAPPING_NOT_COMPLETE = 243;
        public const int MSG_ATTEMPT_TO_WRITE_TO_LOCKED_SCENARIO = 300;
        public const int MSG_ATTEMPT_TO_DELETE_RU_WITH_LOCKED_SCENARIO = 301;
        public const int MSG_DELETE_LOCKED_SCENARIO_FAILURE_GROUPMEMBER = 302;
        public const int MSG_INCOMPLETE_TAX_MAP_SELECTION = 303;
        public const int MSG_NON_STANDARD_CL_NOT_SUPPORTED = 304;
        public const int MSG_LEDGER_MAP_CONTAINS_DUPLICATE_ACCOUNTS = 305;
        public const int MSG_GROUP_MAP_SETTINGS_UNCHANGED_WARNING = 306;
        public const int MSG_DUPLICATE_CATEGORY_NAME = 307;
        public const int MSG_DELETE_CATEGORY_IN_LOCKED_SCENARIO = 308;
        public const int MSG_DELETE_CATEGORY_IN_USE = 309;
        public const int MSG_DUPLICATE_ENTITYID = 315;
        public const int MSG_CM_UPGRADE_CONFIRMATION = 247;
        public const int MSG_CM_ALT_UPGRADE_CONFIRMATION = 248;
        public const int MSG_CM_DOWNGRADE = 249;
        public const int MSG_LEDGER_NO_LICENCE = 320;
        public const int MSG_WARN_LK_VALID_DAYS = 321;
        public const int MSG_WARN_LK_CLIENT_ID = 322;
        public const int MSG_ERR_LK_EXPIRE = 323;
        public const int MSG_ERR_LK_INVALID = 324;
        public const int MSG_ERR_LK_EXCESS = 325;
        public const int MSG_ERR_LK_EXCESS_USERS = 326;
        public const int MSG_ERR_LK_EXCESS_ENTITIES = 327;
        public const int MSG_ERR_LK_EXCESS_GROUPS = 328;
        public const int MSG_ERR_LK_EXCEED_PERIODS = 329;
        public const int MSG_ERR_LK_EXCESS_SCENARIOS = 330;
        public const int MSG_ERR_LK_IMPORTLC_BREACH = 331;
        public const int MSG_ERR_LK_INSTALLID_INVALID = 332;
        public const int MSG_INFO_LK_CONTACTOritax = 264;
        public const int MSG_ERR_LK_TEA_INVALID = 265;
        public const int MSG_WARN_LK_EXPIRED_LICENCE = 266;
        public const int MSG_WARN_LK_LICENCE_VALID_UNTIL = 267;
        public const int MSG_WARN_LK_VALID_DAYS_COUNT = 268;
        public const int MSG_WARN_LK_LICENCE_RENEW = 269;
        public const int MSG_BTB_MISSING_PERIOD = 334;
        public const int MSG_BTB_ENTITY_WITH_LOCK_SCENERIO = 335;
        public const int Msg_system_locked = 336;
        public const int MSG_APPLY_MAP_SETTINGS_CONFIRMATION = 337;
        public const int MSG_BTB_MAIN_NO_MAP_SETTINGS = 338;
        public const int MSG_BTB_NO_JOURNALS_DISPLAY = 339;
        public const int MSG_CM_ATO2003_UPGRADE_NOT_SUPPORTED = 283;
        public const int MSG_GROUP_MAP_SETTINGS_SCENARIO_LOCKED = 284;
        public const int MSG_TEMPLATE_ALREADY_REFERENCED = 287;
        public const int MSG_LEDGERCM_NOT_FOUND_IN_CAE = 340;
        public const int MSG_AT02003_FOUND_IN_GROUP = 341;
        public const int MSG_LEDGERMAP_SELECTION_MISSING_IL_ACCOUNT = 342;
        public const int MSG_LEDGERMAP_SELECTION_MISSING_CL_ACCOUNT = 343;
        public const int MSG_LEDGERMAP_SELECTION_ADDED_ACCOUNTS = 344;
        public const int Msg_WRN_CM_NOTAPPLICABLEGROUP = 345;
        public const int MSG_WRN_RPT_INCONSISTENT_ATO_FORM_VERSION = 346;
        public const int MSG_ERR_INVALID_CM_VER = 347;
        public const int MSG_CONFIRM_CONSOLIDATION_RECALCULATION = 348;
        public const int MSG_CONFIRM_TIMEINTENSIVETASK = 349;
        public const int Msg_WRN_Select_Workpaper = 350;
        public const int Msg_WRN_Select_Print_View = 351;
        public const int MSG_WRN_ANOTHER_TIMEINTENSIVETASK = 352;
        public const int MSG_DELETE_EXIST_GRP_MEMBER_TBS = 353;
        public const int MSG_ORGANISATION_NAME_ALREADY_EXISTS = 360;
        public const int MSG_ONLY_ONE_ORGANISATION_REMAINING = 361;
        public const int MSG_ORGANISATION_CONTAINS_ENTITIES = 362;
        public const int MSG_USERS_ASSOCIATED_WITH_ORGANISATION = 363;
        #endregion

        #region Constructors -----------------------------------------------------------------------
        static MessageBoxDefinition()
        {
            messages = new Hashtable();
            messages.Add(UNKNOWN, String.Intern("Undefined message box definition"));
        }

        private MessageBoxDefinition()
        {
        }

        public MessageBoxDefinition(int intMBD)
        {
            message = intMBD;
        }

        public MessageBoxDefinition(int intMBD, string[] strArgs)
        {
            message = intMBD;
            args = strArgs;
        }
        #endregion

        #region Public Methods ---------------------------------------------------------------------

        public override string ToString()
        {
            string message = (string)messages[this.message];

            if (message != null)
            {
                if (this.args != null)
                {
                    message = String.Format(message, this.args);
                }

                return message;
            }
            else
            {
                return (string)messages[UNKNOWN];
            }
        }
        #endregion
    }
}
