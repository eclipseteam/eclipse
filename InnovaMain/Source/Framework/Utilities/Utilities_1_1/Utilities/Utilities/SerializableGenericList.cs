using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;

namespace Oritax.TaxSimp.Utilities
{
    [Serializable]
    public class SerializableGenericList<T> : IList<T>, ICollection<T>, IEnumerable<T>, ISerializable
    {
        #region Fields

        private List<T> list;

        #endregion // Fields

        #region Constructors

		public SerializableGenericList()
		{
            this.list = new List<T>();
		}

		public SerializableGenericList(int capacity)
		{
            this.list = new List<T>(capacity);
		}

        public SerializableGenericList(ICollection<T> c)
		{
            this.list = new List<T>(c);
		}

        public SerializableGenericList(SerializationInfo si, StreamingContext context)
        {
            this.list = (List<T>)Serialize.GetSerializedValue(si, "sergenlist_list", typeof(List<T>), new List<T>());
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            FieldInfo version = this.list.GetType().GetField("_version", BindingFlags.Instance | BindingFlags.NonPublic);
            //Assert version is not null just in case later framework versions change the implementation
            //of this private member.
            Debug.Assert(version != null);
            if (version != null)
            {
                object objVersion = version.GetValue(this.list);
                //Assert that the version value is an integer type, then if it is always set it to 0.
                Debug.Assert(objVersion is int);
                if (objVersion is int)
                    version.SetValue(this.list, 0);
            }
            Serialize.AddSerializedValue(info, "sergenlist_list", this.list);
        }

        #endregion // Constructors

        #region IList<T> Members

        public int IndexOf(T item)
        {
            return this.list.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            this.list.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            this.list.RemoveAt(index);
        }

        public T this[int index]
        {
            get
            {
                return this.list[index];
            }
            set
            {
                this.list[index] = value;
            }
        }

        #endregion

        #region ICollection<T> Members

        public void Add(T item)
        {
            this.list.Add(item);
        }

        public void Clear()
        {
            this.list.Clear();
        }

        public bool Contains(T item)
        {
            return this.list.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            this.list.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return this.list.Count; }
        }

        public bool IsReadOnly
        {
            get { return ((ICollection<T>)this.list).IsReadOnly; }
        }

        public bool Remove(T item)
        {
            return this.list.Remove(item);
        }

        #endregion

        #region IEnumerable<T> Members

        public IEnumerator<T> GetEnumerator()
        {
            return this.list.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return ((System.Collections.IEnumerable)this.list).GetEnumerator();
        }

        #endregion

        #region ICollection<T> Members

        void ICollection<T>.Add(T item)
        {
            ((ICollection<T>)this.list).Add(item);
        }

        void ICollection<T>.Clear()
        {
            ((ICollection<T>)this.list).Clear();
        }

        bool ICollection<T>.Contains(T item)
        {
            return ((ICollection<T>)this.list).Contains(item);
        }

        void ICollection<T>.CopyTo(T[] array, int arrayIndex)
        {
            ((ICollection<T>)this.list).CopyTo(array, arrayIndex);
        }

        int ICollection<T>.Count
        {
            get { return ((ICollection<T>)this.list).Count; }
        }

        bool ICollection<T>.IsReadOnly
        {
            get { return ((ICollection<T>)this.list).IsReadOnly; }
        }

        bool ICollection<T>.Remove(T item)
        {
            return ((ICollection<T>)this.list).Remove(item);
        }

        #endregion

        #region IEnumerable<T> Members

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return ((IEnumerable<T>)this.list).GetEnumerator();
        }

        #endregion
    }
}
