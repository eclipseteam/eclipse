using System;
using System.Web;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// Summary description for UserContext.
	/// </summary>
	[Serializable]
	public class UserContext: ISerializable
	{
		//User context information
		string m_ReportingUnitName;
		Guid m_ReportingUnitCLID;
		string m_ScenarioName;
		Guid m_ScenarioCSID;
		string m_PeriodName;
		Guid m_PeriodCLID;

		public UserContext()
		{
			//Initialize context to empty values
			m_ReportingUnitName =  String.Empty;
			m_ReportingUnitCLID = Guid.Empty;
			m_ScenarioName = String.Empty;
			m_ScenarioCSID = Guid.Empty;
			m_PeriodName = String.Empty;
			m_PeriodCLID = Guid.Empty;
		}

		#region Custom Serialization
		protected UserContext(SerializationInfo si, StreamingContext context)
		{
			//Get user context information 
			m_ReportingUnitName = Serialize.GetSerializedString(si,"uc_m_ReportingUnitName");
			m_ReportingUnitCLID = Serialize.GetSerializedGuid(si,"uc_m_ReportingUnitCLID");
			m_ScenarioName		= Serialize.GetSerializedString(si,"uc_m_ScenarioName");
			m_ScenarioCSID		= Serialize.GetSerializedGuid(si,"uc_m_ScenarioCSID");
			m_PeriodName		= Serialize.GetSerializedString(si,"uc_m_PeriodName");
			m_PeriodCLID		= Serialize.GetSerializedGuid(si,"uc_m_PeriodCLID");
		}

		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			//Add user context information 
			Serialize.AddSerializedValue(si,"uc_m_ReportingUnitName",m_ReportingUnitName);
			Serialize.AddSerializedValue(si,"uc_m_ReportingUnitCLID",m_ReportingUnitCLID);
			Serialize.AddSerializedValue(si,"dbu_m_ScenarioName",m_ScenarioName);
			Serialize.AddSerializedValue(si,"dbu_m_ScenarioCSID",m_ScenarioCSID);
			Serialize.AddSerializedValue(si,"dbu_m_PeriodName",m_PeriodName);
			Serialize.AddSerializedValue(si,"dbu_m_PeriodCLID",m_PeriodCLID);
		}
		#endregion
		#region Properties to Access User context information
		public string ReportingUnitName
		{
			get{return m_ReportingUnitName;}
			set{m_ReportingUnitName=value;}
		}

		public Guid ReportingUnitCLID
		{
			get{return m_ReportingUnitCLID;}
			set{m_ReportingUnitCLID=value;}
		}

		public string ScenarioName
		{
			get{return m_ScenarioName;}
			set{m_ScenarioName=value;}
		}
		public Guid ScenarioCSID 
		{
			get{return m_ScenarioCSID ;}
			set{m_ScenarioCSID =value;}
		}
		public string PeriodName
		{
			get{return m_PeriodName;}
			set{m_PeriodName=value;}
		}
		public Guid PeriodCLID
		{
			get{return m_PeriodCLID;}
			set{m_PeriodCLID =value;}
		}

		#endregion

		#region Backwards compatibiltiy methods
		public static string Get( HttpContext Context, String strElement )
		{
			return String.Empty;
		}
		public static void Set( HttpContext Context, String strElement, String strValue )
		{
		}
		public static String GetContext( HttpContext Context )
		{
			return String.Empty;
		}
		#endregion

	}
}
