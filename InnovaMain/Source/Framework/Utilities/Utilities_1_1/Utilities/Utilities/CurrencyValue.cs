using System;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Security.Permissions;


namespace Oritax.TaxSimp.Utilities
{
    /// <summary>
    /// Updated CurrencyValue class.
    /// Designed using the decimal datatype.
    /// </summary>
    [Serializable]
    public class CurrencyValue : ISerializable
    {
        // This flag, when set, indicates that the CurrencyValue has never been 
        // assigned and should therefore have a decimal value of zero but its 
        // string equivalent should be String.Empty.
        private bool isDefined;

        public void Clear()
        {
            isDefined = false;
            dcValue = 0;
        }

        public Decimal dcValue;

        public CurrencyValue(CurrencyValue currencyValue)
        {
            isDefined = true;
            dcValue = currencyValue.dcValue;
            this.dcValue = Math.Round(dcValue, 2);
        }

        public CurrencyValue(double val)
        {
            isDefined = true;
            dcValue = Convert.ToDecimal(val);
            this.dcValue = Math.Round(dcValue, 2);
        }

        public CurrencyValue(decimal val)
        {
            isDefined = true;
            dcValue = Convert.ToDecimal(val);
            this.dcValue = Math.Round(dcValue, 2);
        }

        public CurrencyValue(int major, int minor)
        {
            isDefined = true;
            dcValue = Decimal.Parse(major.ToString() + "." + minor.ToString());
            this.dcValue = Math.Round(dcValue, 2);
        }

        public CurrencyValue(int major)
        {
            isDefined = true;
            dcValue = new Decimal(major);
            this.dcValue = Math.Round(dcValue, 2);
        }

        public CurrencyValue()
        {
            isDefined = false;
            dcValue = new Decimal(0);
        }

        protected CurrencyValue(SerializationInfo si, StreamingContext context)
        {
            dcValue = Serialize.GetSerializedDecimal(si, "cv_dcValue");
            isDefined = Serialize.GetSerializedBool(si, "cv_isDefined");
        }

        //Only allow the .NET Serialization core code to call this function
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
        {
            Serialize.AddSerializedValue(si, "cv_dcValue", dcValue);
            Serialize.AddSerializedValue(si, "cv_isDefined", isDefined);
        }


        public static CurrencyValue operator +(CurrencyValue val1, CurrencyValue val2)
        {
            if (val1 != null && val2 != null)
                return new CurrencyValue(val1.dcValue + val2.dcValue);
            else if (val1 != null )
                return new CurrencyValue(val1.dcValue);
            else if (val2 != null)
                return new CurrencyValue(val2.dcValue);
            else
                return null;
        }

        public static CurrencyValue operator -(CurrencyValue val1, CurrencyValue val2)
        {
            if (val1 != null && val2 != null)
                return new CurrencyValue(val1.dcValue - val2.dcValue);
            else if (val1 != null )
                return new CurrencyValue(val1.dcValue);
            else if (val2 != null)
                return new CurrencyValue(val2.dcValue);
            else
                return null;
        }

        public static CurrencyValue operator *(Percent val1, CurrencyValue val2)
        {
            if (val2 != null)
                return (new CurrencyValue(val1 * val2.dcValue));
            else
                return null;
        }

        //PMB 11/10/02- Added method so that implicit conversions will occur and use this method
        // when a currencyvalue is multiplied by an int/double/decimal. Also made conversions
        // in percent class to explicit to stop int*CV being performed incorrectly as percent*CV
        public static CurrencyValue operator *(CurrencyValue val1, CurrencyValue val2)
        {
            if (val1 != null && val2 != null)
                return (new CurrencyValue(val1.dcValue * val2.dcValue));
            else if (val1 != null)
                return new CurrencyValue(0);
            else if (val2 != null)
                return new CurrencyValue(0);
            else
                return null;
        }

        public static CurrencyValue operator /(CurrencyValue val1, Percent val2)
        {
            if (val1 != null && val2 != null)
                return new CurrencyValue(val1.dcValue / val2.DecimalPercent);
            else if (val2 != null)
                return new CurrencyValue(0);
            else
                return new CurrencyValue(val1.dcValue / val2.DecimalPercent);
        }

        public static double operator /(CurrencyValue val1, CurrencyValue val2)
        {
            return (double)(val1.dcValue / val2.dcValue);
        }

        public static bool operator >=(CurrencyValue val1, CurrencyValue val2)
        {
            return (val1.dcValue >= val2.dcValue);
        }

        public static bool operator <=(CurrencyValue val1, CurrencyValue val2)
        {
            return (val1.dcValue <= val2.dcValue);
        }

        public static bool operator >(CurrencyValue val1, int val2)
        {
            return (val1.dcValue > val2);
        }

        public static bool operator <(CurrencyValue val1, int val2)
        {
            return (val1.dcValue < val2);
        }

        public static bool operator >(CurrencyValue val1, CurrencyValue val2)
        {
            return (val1.dcValue > val2.dcValue);
        }

        public static bool operator <(CurrencyValue val1, CurrencyValue val2)
        {
            return (val1.dcValue < val2.dcValue);
        }

        public static CurrencyValue operator *(CurrencyValue val1, Percent val2)
        {
            return (new CurrencyValue(val2 * val1.dcValue));
        }

        public static CurrencyValue operator -(CurrencyValue val1)
        {
            return new CurrencyValue(-val1.dcValue);
        }

        public static bool operator ==(CurrencyValue val1, CurrencyValue val2)
        {
            // We have to handle the case when one of the references is null - must
            // cast to Object to avoid invoking this overload
            if (((Object)val1) != null && ((Object)val2) != null)
                return (val1.dcValue == val2.dcValue);
            // Return true if both are null
            if (((Object)val1) == null && ((Object)val2) == null)
                return true;

            return false;
        }

        public override bool Equals(Object val1)
        {
            return (val1 is CurrencyValue) && this == (CurrencyValue)val1;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool operator !=(CurrencyValue val1, CurrencyValue val2)
        {

            // We have to handle the case when one of the references is null - must
            // cast to Object to avoid invoking this overload
            if (((Object)val1) != null && ((Object)val2) != null)
                return (val1.dcValue != val2.dcValue);
            // Return true if both are null
            if (((Object)val1) == null && ((Object)val2) == null)
                return false;

            return true;
        }

        static public implicit operator CurrencyValue(int major)
        {
            return new CurrencyValue(major);
        }

        static public implicit operator CurrencyValue(double val)
        {
            return new CurrencyValue(val);
        }

        static public implicit operator CurrencyValue(decimal val)
        {
            return new CurrencyValue(val);
        }

        public override String ToString()
        {
            if (!isDefined)
                return String.Empty;
            else if (dcValue >= 0)
                return (Decimal.Round(dcValue, 2).ToString("#,###,###,##0.00"));
            else
                return ("(" + (Decimal.Round(-dcValue, 2)).ToString("#,###,###,##0.00") + ")");
        }

        static private char[] chBrackets = new char[] { '(', ')' };

        static public CurrencyValue FromString(String inStr)
        {
            decimal returnVal = new decimal(0);
            try
            {
                if (inStr.Trim().Length > 0)
                {
                    if (inStr.StartsWith("(") && inStr.EndsWith(")")) //Then it is a negative number
                    {
                        String newString = inStr.Trim(chBrackets);
                        returnVal = decimal.Parse(newString);
                        returnVal = returnVal * -1;
                    }
                    else
                        returnVal = decimal.Parse(inStr);
                }
                else
                    return new CurrencyValue();
            }
            catch
            {
            }

            return new CurrencyValue(returnVal);
        }

        private struct SignedPattern
        {
            public decimal sign;
            public Regex r;

            public SignedPattern(decimal sign, Regex r)
            {
                this.r = r;
                this.sign = sign;
            }
        }

        static private SignedPattern[] signedPatternsEx = new SignedPattern[] {
			new SignedPattern( 1, new Regex(@"^(-{0,1}\d*\.{0,1}\d{0,})$",  RegexOptions.Compiled|RegexOptions.Singleline)),
			new SignedPattern(-1, new Regex(@"^\((\d*\.{0,1}\d{0,})\)$",    RegexOptions.Compiled|RegexOptions.Singleline)),
			new SignedPattern(-1, new Regex(@"^(\d*\.{0,1}\d{0,})CR{0,1}$", RegexOptions.Compiled|RegexOptions.Singleline|RegexOptions.IgnoreCase)),
			new SignedPattern(-1, new Regex(@"^CR{0,1}(\d*\.{0,1}\d{0,})$", RegexOptions.Compiled|RegexOptions.Singleline|RegexOptions.IgnoreCase)),
			new SignedPattern( 1, new Regex(@"^(\d*\.{0,1}\d{0,})DR{0,1}$", RegexOptions.Compiled|RegexOptions.Singleline|RegexOptions.IgnoreCase)),
			new SignedPattern( 1, new Regex(@"^DR{0,1}(\d*\.{0,1}\d{0,})$", RegexOptions.Compiled|RegexOptions.Singleline|RegexOptions.IgnoreCase))
		};

        /// <summary>
        /// Use this method to convert a stri n ng containing either a credit/debit amount to a CurrencyValue.
        /// </summary>
        /// <param name="inStr"></param>
        /// <returns></returns>
        static public CurrencyValue FromImportString(String inStr)
        {
            Match m = null;

            inStr = inStr.Replace(",", "").Replace(" ", "");

            // blank strings in an import column are considered to be zero
            if (inStr.Length == 0) return new CurrencyValue();

            foreach (SignedPattern s in signedPatternsEx)
                if ((m = s.r.Match(inStr)).Groups.Count == 2)
                    return new CurrencyValue(decimal.Parse(m.Groups[1].Value) * s.sign);

            throw new FormatException("Format not recognised in string '" + inStr + "'");
        }

        static public CurrencyValue FromString(String inStr, bool bolRoundToWhole)
        {
            CurrencyValue objNumber = FromString(inStr);

            if (bolRoundToWhole)
            {
                decimal objRoundedValue = Decimal.Round(objNumber.dcValue, 0);
                objNumber.dcValue = objRoundedValue;
            }

            return objNumber;
        }

        public decimal ToDecimal()
        {
            return dcValue;
        }

        public String ToWholeString(bool bFormatSign)
        {
            if (isDefined)
            {
                decimal dcRoundedValue = Decimal.Round(dcValue, 0);
                if (dcRoundedValue >= 0)
                    return this.ToWholeString();
                else
                    return ("" + (-dcRoundedValue).ToString("#,###,###,##0"));
            }
            else
                return String.Empty;
        }

        //Returns the whole number for the currency value input
        public String ToWholeString()
        {
            if (isDefined)
            {
                decimal dcRoundedValue = Decimal.Round(dcValue, 0);
                if (dcRoundedValue >= 0)
                    return (dcRoundedValue.ToString("#,###,###,##0"));
                else
                    return ("(" + (-dcRoundedValue).ToString("#,###,###,##0") + ")");
            }
            else
                return String.Empty;
        }

        /// <summary>
        /// Rounds the currency value to the number of places specified
        /// </summary>
        /// <param name="intDecimals">The number of places to round to</param>
        /// <returns>A new currency value that has been rounded to the specified number of decimal places</returns>
        public CurrencyValue Round(int intDecimals)
        {
            return new CurrencyValue(Decimal.Round(dcValue, intDecimals));
        }

        public CurrencyValue GetABSValue()
        {
            decimal tmpDecimal = dcValue;

            if (tmpDecimal < 0)
                tmpDecimal = -tmpDecimal;

            return new CurrencyValue(tmpDecimal);
        }

        public CurrencyValue GetDifferenceAfterRounding()
        {
            CurrencyValue objRoundedValue = this.Round(0);
            return objRoundedValue - this;
        }

        public string ToCentsString()
        {
            if (isDefined)
            {
                decimal decRoundedValue = Decimal.Truncate(dcValue);
                decimal decDifference = this.dcValue - decRoundedValue;

                decimal decCents = Decimal.Round(decDifference, 2) * 100;
                if (decCents < 0)
                    decCents = -decCents;

                return decCents.ToString("00");
            }
            else
                return String.Empty;
        }

        public string ToFlooredWholeString(bool bolFormatSign)
        {
            if (this.isDefined)
            {
                CurrencyValue objFlooredValue = Decimal.Truncate(this.dcValue);
                return objFlooredValue.ToWholeString(bolFormatSign);
            }
            else
                return String.Empty;
        }

        static public CurrencyValue GetMin(CurrencyValue[] cvArray)
        {
            CurrencyValue cvResult = new CurrencyValue();

            if (cvArray.GetLength(0) > 0)
                cvResult = cvArray[0];

            foreach (CurrencyValue cv in cvArray)
            {
                if (cv < cvResult)
                    cvResult = cv;
            }

            return cvResult;
        }

    }
}
