using System;
using System.Web;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// Cookie based user context
	/// </summary>
	[Serializable]
	public class CookieUserContext: ISerializable
	{
		public CookieUserContext(){}

		protected CookieUserContext(SerializationInfo si, StreamingContext context)
		{
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
		}
		
		private static HttpCookie CreateNewCookie(HttpContext Context)
		{
			HttpCookie cookie =	new HttpCookie("UserContext");
			cookie.Expires = DateTime.MaxValue;
			Context.Response.AppendCookie(cookie);
			return cookie;
		}

		#region Properties to Access User context information
		#region Reporting Unit Name Get/Set Methods
		public static string GetReportingUnitName(HttpContext Context)
		{
			HttpCookie objCookie = Context.Request.Cookies[ "UserContext" ];
			if( objCookie != null)
			{
				objCookie.Expires = DateTime.MaxValue;
				return( (string)objCookie[ "ReportingUnitName" ] );
			}
			else
				return String.Empty;
		}

		public static void SetReportingUnitName(HttpContext Context, string rUValue)
		{
			HttpCookie cookie = Context.Request.Cookies[ "UserContext" ];

			if(cookie == null)
			{
				cookie = CreateNewCookie(Context);
			}
			else
				cookie.Expires = DateTime.MaxValue;

			cookie["ReportingUnitName"] = rUValue;
			
			Context.Response.Cookies.Add(cookie);
		}

		#endregion
		#region Reporting Unit CLID Get/Set Methods
		public static Guid GetReportingUnitCLID(HttpContext Context)
		{
			HttpCookie objCookie = Context.Request.Cookies[ "UserContext" ];
			if( objCookie != null)
			{
				objCookie.Expires = DateTime.MaxValue;
				return( new Guid(objCookie[ "ReportingUnitCLID" ]) );
			}
			else
				return Guid.Empty;
		}

		public static void SetReportingUnitCLID(HttpContext Context, Guid rUValue)
		{
			HttpCookie cookie = Context.Request.Cookies[ "UserContext" ];

			if(cookie == null)
				cookie = CreateNewCookie(Context);
			else
				cookie.Expires = DateTime.MaxValue;

			cookie["ReportingUnitCLID"] = rUValue.ToString();

			Context.Response.Cookies.Add(cookie);
		}
		#endregion
		#region Scenario Name Get/Set Methods
		public static string GetScenarioName(HttpContext Context)
		{
			HttpCookie objCookie = Context.Request.Cookies[ "UserContext" ];
			if( objCookie != null)
			{
				objCookie.Expires = DateTime.MaxValue;
				return( (string)objCookie[ "ScenarioName" ] );
			}
			else
				return string.Empty;
		}

		public static void SetScenarioName(HttpContext Context, string scenarioNameValue)
		{
			HttpCookie cookie = Context.Request.Cookies[ "UserContext" ];

			if(cookie == null)
				cookie = CreateNewCookie(Context);
			else
				cookie.Expires = DateTime.MaxValue;

			cookie["ScenarioName"] = scenarioNameValue;

			Context.Response.Cookies.Add(cookie);
		}
		#endregion
		#region Scenario CSID Get/Set Methods
		public static Guid GetScenarioCSID(HttpContext Context)
		{
			HttpCookie objCookie = Context.Request.Cookies[ "UserContext" ];
			if( objCookie != null)
			{
				return( new Guid(objCookie[ "ScenarioCSID" ]) );
			}
			else
				return Guid.Empty;
		}

		public static void SetScenarioCSID(HttpContext Context, Guid scenarioCSIDValue)
		{
			HttpCookie cookie = Context.Request.Cookies[ "UserContext" ];

			if(cookie == null)
				cookie = CreateNewCookie(Context);
			else
				cookie.Expires = DateTime.MaxValue;

			cookie["ScenarioCSID"] = scenarioCSIDValue.ToString();

			Context.Response.Cookies.Add(cookie);
		}
		#endregion
		#region Period Name Get/Set Methods
		public static string GetPeriodName(HttpContext Context)
		{
			HttpCookie objCookie = Context.Request.Cookies[ "UserContext" ];
			if( objCookie != null)
			{
				objCookie.Expires = DateTime.MaxValue;
				return( objCookie[ "PeriodName" ] );
			}
			else
				return string.Empty;
		}

		public static void SetPeriodName(HttpContext Context, string periodNameValue)
		{
			HttpCookie cookie = Context.Request.Cookies[ "UserContext" ];

			if(cookie == null)
				cookie = CreateNewCookie(Context);
			else
				cookie.Expires = DateTime.MaxValue;

			cookie["PeriodName"] = periodNameValue;

			Context.Response.Cookies.Add(cookie);
		}
		#endregion
		#region Period CLID Get/Set Methods
		public static Guid GetPeriodCLID(HttpContext Context)
		{
			HttpCookie objCookie = Context.Request.Cookies[ "UserContext" ];
			if( objCookie != null)
			{
				objCookie.Expires = DateTime.MaxValue;
				return( new Guid(objCookie[ "PeriodCLID" ]));
			}
			else
				return Guid.Empty;
		}

		public static void SetPeriodCLID(HttpContext Context, Guid periodCLIDValue)
		{
			HttpCookie cookie = Context.Request.Cookies[ "UserContext" ];

			if(cookie == null)
				cookie = CreateNewCookie(Context);
			else
				cookie.Expires = DateTime.MaxValue;

			cookie["PeriodCLID"] = periodCLIDValue.ToString();

			Context.Response.Cookies.Add(cookie);
		}
		#endregion
	
		#endregion

	}
}
