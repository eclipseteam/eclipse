using System;
using System.Collections.Generic;
using System.Text;

namespace Oritax.TaxSimp.Utilities
{
    public class RequestProcessStatusProvider
    {
        private bool cancelled = false;
        private bool cancelRequestAwknowledged = false;
        private string statusInfoMessage = String.Empty;

        public RequestProcessStatusProvider()
        {
        }

        /// <summary>
        /// Cancels a request
        /// </summary>
        public void Cancel()
        {
            this.cancelled = true;
        }

        /// <summary>
        /// System awknowledges the request to cancel
        /// </summary>
        public void AwknowledgeCancelRequest()
        {
            this.cancelRequestAwknowledged = true;
        }

        /// <summary>
        /// Indicates that the request has been cancelled
        /// </summary>
        public bool Cancelled
        {
            get
            {
                return this.cancelled;
            }
        }


        /// <summary>
        /// Cancel has been awknowledged by the system
        /// </summary>
        public bool CancelRequestAwknowledged
        {
            get 
            { 
                return cancelRequestAwknowledged; 
            }
        }

        /// <summary>
        /// A message associated with the current cancellation
        /// </summary>
        public string StatusInfoMessage
        {
            get
            {
                return this.statusInfoMessage;
            }
            set
            {
                this.statusInfoMessage = value;
            }
        }
    }
}
