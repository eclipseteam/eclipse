using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Collections;
using System.Collections.Generic;
using System.Linq; 

namespace Oritax.TaxSimp.Utilities
{
    [Serializable]
    public class EventType : ISerializable
    {
        public const int LoginSuccessful = 0;
        public const int LoginUnsuccessful = 1;
        public const int Logout = 2;
        public const int CreateUser = 3;
        public const int DeleteUser = 4;
        public const int EditUser = 5;
        public const int ChangePassword = 6;
        public const int ChangeUserGroup = 7;
        public const int UserGroupCreate = 8;
        public const int UserGroupDelete = 9;
        public const int EntityUserGroups = 10;
        public const int UserLocked = 11;
        public const int UserUnlocked = 12;
        public const int UserDisabled = 13;
        public const int EntityCreated = 14;
        public const int EntityEdited = 15;
        public const int EntityDeleted = 16;
        public const int SetCurrentEntity = 17;
        public const int GroupCreated = 18;
        public const int GroupEdited = 19;
        public const int GroupDeleted = 20;
        public const int GroupMemberAdd = 21;
        public const int GroupMemberRemove = 22;
        //public const int ImportTrialBalance = 25;
        public const int ScenarioCopy = 27;
        public const int ScenarioDeleted = 28;
        public const int ScenarioDetailsEdited = 29;
        public const int SetCurrentScenario = 30;
        // No use in version 2005.1 ---------------------------
        public const int GroupMemberOwnershipEdit = 23;
        public const int ImportCalculationModule = 24;
        //public const int ImportTrialBalanceEntry = 26;
        //public const int ConsolidationAdjustmentCreate = 31;
        //public const int ConsolidationAdjustmentEdit = 32;
        //public const int ConsolidationAdjustmentDelete = 33;
        //public const int CalculationConfiguration = 39;
        //public const int MultiPeriodCreate = 35;
        //------------------------------------------------------
        //public const int PeriodCreate = 34;
        //public const int PeriodEdit = 36;
        //public const int PeriodDelete = 37;
        public const int PeriodSetCurrent = 38;
        //public const int AddCalculationModule = 40;
        //public const int WorkpaperUpdate = 41;
        //New added events in version 2005.1
        //public const int ATOFormPrint = 42;
        //public const int ITLConfiguration = 43;
        //public const int DeleteCalculationModule = 44;
        //public const int BulkImportTrialBalance = 45;
        //public const int DeleteBulkTrialBalance = 46;
        public const int ScenarioLocked = 47;
        public const int ScenarioUnLocked = 48;
        //public const int DeleteTrialBalance = 49;
        //public const int ImportLedgerMap = 50;
        //public const int ImportConsolidationChart = 51;
        //public const int ImportTaxMap = 52;
        public const int UserEnabled = 53;
        //public const int AddMappingToLedgerAccount = 54;
        //public const int RemoveMappingToLedgerAccount = 55;
        //public const int AddMappingToTaxAccount = 56;
        //public const int RemoveMappingToTaxAccount = 57;
        public const int PerformMigration = 58;
        public const int RunExportReport = 59;
        public const int LicenceEntityBreach = 60;
        public const int LicenceUserAccountBreach = 61;
        public const int LicenceGroupBreach = 62;
        public const int LicencePeriodBreach = 63;
        public const int LicenceScenarioBreach = 64;
        public const int ErrorPrompt = 65;
        public const int ConfirmationPrompt = 66;
        public const int MultiUserEvent = 67;
        public const int ErrorLogGenerated = 68;
        public const int UpdateComponentModule = 69;
        public const int UpdatePasswordMinLength = 70;
        public const int UpdatePasswordMaxLength = 71;
        public const int UpdatePasswordHistory = 72;
        public const int UpdateMaxRepeatingChars = 73;
        public const int UpdateExpiryPeriod = 74;
        public const int UpdateExpiryReminder = 75;
        public const int UpdateAllowableAttempts = 76;
        public const int AdministratorCheck = 77;
        public const int RestoreDefaultSettings = 78;
        public const int CorrectionTableUpdateRowIsNull = 79;
        //public const int WorkpaperVisited = 80;
        public const int SuperAdministratorCreated = 81;
        public const int EntityViewed = 82;
        public const int FileImported = 83;
        //UMAEvents
        public const int UMAClientCreated = 201;
        public const int UMAClientUpdated = 202;
        public const int UMAClientDeleted = 203;
        public const int UMAContactAdded = 204;
        public const int UMAContactUpdated = 205;
        public const int UMAContactDeleted = 206;
        public const int UMAWorkflowAccountUpdate = 207;
        public const int UMAWorkflowAccountDelete = 208;
        public const int UMAWorkflowAccountAdd = 209;
        public const int UMAWorkflowAccountDIFMModelUpdate = 210;
        public const int UMAWorkflowAccountAddressUpdate = 211;
        public const int UMAWorkflowAccountUserModified = 212;
        public const int UMAWorkflowAccountUserAdd = 213;
        public const int UMAWorkflowAccountAuthorityToActUpdate = 215;
        public const int UMAWorkflowAccountNameUpdate = 216;
        public const int UMAWorkflowAccountAdviserMapUpdate = 217;
        public const int UMAWorkflowAccountDIYUpdate = 218;
        public const int UMAWorkflowAccountDIWMUpdate = 219;
        public const int UMAWorkflowAccountDIFMUpdate = 220;
        public const int UMAWorkflowAccountTFNAABNUpdate = 221;
        public const int UMAClientStatusChange = 222;
        public const int UMAClientDoocumentUpload = 223;
        public const int UMAClientDoocumentDelete = 224;
        public const int UMAClientDoocumentEdit = 225;
        public const int UMAClientNotesAdded = 226;
        public const int UMAClientNotesDelete = 227;
        public const int UMAClientNotesEdit = 228;
        public const int UMAAccountChange = 229;
        public const int EmailSent = 230;
        public const string USER_EVENT_TYPE = "User";
        public const string BUSINESS_STRUCTURE_EVENT_TYPE = "Business Structure";
        public const string CALCULATION_EVENT_TYPE = "Calculation";
        public const string SYSTEM_EVENT_TYPE = "System";
        
        public struct EventDetails
        {
            public string EventName;
            public int Value;
            public string EventType;
            public EventDetails(int val, string name, string type)
            {
                EventName = name;
                Value = val;
                EventType = type;
            }
        }

        private int theValue;
       

        public EventType(int theValue)
        {
            this.theValue = theValue;
        }

        public EventType(string eventName)
        {

            foreach (EventDetails eventDetails in EventDetailsArray)
            {
                if (eventDetails.EventName == eventName)
                {
                    this.theValue = eventDetails.Value;
                    break;
                }
            }

        }
        protected EventType(SerializationInfo si, StreamingContext context)
        {
            theValue = Serialize.GetSerializedInt32(si, "et_theValue");
        }

        //Only allow the .NET Serialization core code to call this function
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
        {
            Serialize.AddSerializedValue(si, "et_theValue", theValue);
        }

        public static List<EventDetails> EventDetailsArray
        {
            get
            {
                return new List<EventDetails>()
				{
                    new EventDetails(UMAClientCreated,"Client Account Created",USER_EVENT_TYPE),
                    new EventDetails(UMAClientUpdated,"Client Account Updated",USER_EVENT_TYPE),
                    new EventDetails(UMAClientDeleted,"Client Account Deleted",USER_EVENT_TYPE),
                    new EventDetails(UMAContactAdded,"Individual Object Addedd",USER_EVENT_TYPE),
                    new EventDetails(UMAContactUpdated, "Individual Object Updated",USER_EVENT_TYPE),
                    new EventDetails(UMAContactDeleted,"Individual Object Deleted",USER_EVENT_TYPE),
                    new EventDetails(UMAWorkflowAccountUpdate,"Workflow Account Updated",USER_EVENT_TYPE),
                    new EventDetails(UMAWorkflowAccountDelete,"Workflow Account Deleted",USER_EVENT_TYPE),
                    new EventDetails(UMAWorkflowAccountAdd,"Workflow Account Added",USER_EVENT_TYPE),
                    new EventDetails(UMAWorkflowAccountDIFMModelUpdate,"Workflow Account DIFM Model Updated",USER_EVENT_TYPE),
                    new EventDetails(UMAWorkflowAccountUserModified,"Workflow Account Modified By",USER_EVENT_TYPE),
                    new EventDetails(UMAWorkflowAccountUserAdd,"Workflow Account Created By",USER_EVENT_TYPE),
                    new EventDetails(UMAWorkflowAccountAuthorityToActUpdate,"Workflow Authority To Act Updated",USER_EVENT_TYPE),
                    new EventDetails(UMAWorkflowAccountNameUpdate,"Workflow Account Name Updated",USER_EVENT_TYPE),
                    new EventDetails(UMAWorkflowAccountAdviserMapUpdate, "Workflow Account Adviser Association Updated",USER_EVENT_TYPE),
                    new EventDetails(UMAWorkflowAccountDIYUpdate,"Workflow Account DIY Service Update",USER_EVENT_TYPE),
                    new EventDetails(UMAWorkflowAccountDIWMUpdate,"Workflow Account DIWM Service Update",USER_EVENT_TYPE),
                    new EventDetails(UMAWorkflowAccountDIFMUpdate,"Workflow Account DIFM Update",USER_EVENT_TYPE),
                    new EventDetails(UMAWorkflowAccountTFNAABNUpdate,"Workflow Account Update TFN / ABN",USER_EVENT_TYPE),
                    new EventDetails(UMAClientStatusChange,"Client Account Status Change",USER_EVENT_TYPE),
                    
                    new EventDetails(UMAClientDoocumentUpload,"Client Account Document Upload",USER_EVENT_TYPE),
                    new EventDetails(UMAClientDoocumentDelete,"Client Account Document Delete",USER_EVENT_TYPE),
                    new EventDetails(UMAClientDoocumentEdit,"Client Account Document Edit",USER_EVENT_TYPE), 
                    new EventDetails(UMAClientNotesAdded,"Client Account Notes / Comment Added",USER_EVENT_TYPE),
                    new EventDetails(UMAClientNotesDelete,"Client Account Notes / Comment Delete",USER_EVENT_TYPE),
                    new EventDetails(UMAClientNotesEdit,"Client Account Notes / Comment Edit",USER_EVENT_TYPE), 

                    new EventDetails(LoginSuccessful,"Login successful",USER_EVENT_TYPE),
					new EventDetails(LoginUnsuccessful,"Login unsuccessful",USER_EVENT_TYPE),
					new EventDetails(Logout,"Logout",USER_EVENT_TYPE),
					new EventDetails(CreateUser,"Create user",USER_EVENT_TYPE),
					new EventDetails(DeleteUser,"Delete user",USER_EVENT_TYPE),
					new EventDetails(EditUser,"Edit user",USER_EVENT_TYPE),
					new EventDetails(ChangePassword,"Change password",USER_EVENT_TYPE),
					new EventDetails(ChangeUserGroup,"Change user group",USER_EVENT_TYPE),
					new EventDetails(UserGroupCreate,"User group create",USER_EVENT_TYPE),
					new EventDetails(UserGroupDelete,"User group delete",USER_EVENT_TYPE),
					new EventDetails(EntityUserGroups,"Entity user groups",USER_EVENT_TYPE),
					new EventDetails(UserLocked,"User locked",USER_EVENT_TYPE),
					new EventDetails(UserUnlocked,"User unlocked",USER_EVENT_TYPE),
					new EventDetails(UserDisabled,"User disabled",USER_EVENT_TYPE),
					new EventDetails(EntityCreated,"Entity created",BUSINESS_STRUCTURE_EVENT_TYPE),
					new EventDetails(EntityEdited,"Entity edited",BUSINESS_STRUCTURE_EVENT_TYPE),
					new EventDetails(EntityDeleted,"Entity deleted",BUSINESS_STRUCTURE_EVENT_TYPE),
					new EventDetails(SetCurrentEntity,"Set current entity",BUSINESS_STRUCTURE_EVENT_TYPE),
					new EventDetails(GroupCreated,"Group created",BUSINESS_STRUCTURE_EVENT_TYPE),
					new EventDetails(GroupEdited,"Group edited",BUSINESS_STRUCTURE_EVENT_TYPE),
					new EventDetails(GroupDeleted,"Group deleted",BUSINESS_STRUCTURE_EVENT_TYPE),
					new EventDetails(GroupMemberAdd,"Group member add",BUSINESS_STRUCTURE_EVENT_TYPE),
					new EventDetails(GroupMemberRemove,"Group member remove",BUSINESS_STRUCTURE_EVENT_TYPE),
                    new EventDetails(GroupMemberOwnershipEdit,"Group member ownership edit",BUSINESS_STRUCTURE_EVENT_TYPE),
                    new EventDetails(ImportCalculationModule,"Import calculation module",SYSTEM_EVENT_TYPE),
                    //new EventDetails(ImportTrialBalance,"Import trial balance",CALCULATION_EVENT_TYPE),
                    //new EventDetails(ImportTrialBalanceEntry,"Import trial balance entry",CALCULATION_EVENT_TYPE),
                    new EventDetails(ScenarioCopy,"Scenario copy",BUSINESS_STRUCTURE_EVENT_TYPE),
                    new EventDetails(ScenarioDeleted,"Scenario deleted",BUSINESS_STRUCTURE_EVENT_TYPE),
                    new EventDetails(ScenarioDetailsEdited,"Scenario details edited",BUSINESS_STRUCTURE_EVENT_TYPE),
                    new EventDetails(SetCurrentScenario,"Set current scenario",BUSINESS_STRUCTURE_EVENT_TYPE),
                    new EventDetails(PeriodSetCurrent,"Period set current",BUSINESS_STRUCTURE_EVENT_TYPE),
                    //new EventDetails(ConsolidationAdjustmentCreate,"Consolidation adjustment create",CALCULATION_EVENT_TYPE),
                    //new EventDetails(ConsolidationAdjustmentEdit,"Consolidation adjustment edit",CALCULATION_EVENT_TYPE),
                    //new EventDetails(ConsolidationAdjustmentDelete,"Consolidation adjustment delete",CALCULATION_EVENT_TYPE),
                    //new EventDetails(PeriodCreate,"Period create",BUSINESS_STRUCTURE_EVENT_TYPE),
                    //new EventDetails(MultiPeriodCreate,"Multi period create",BUSINESS_STRUCTURE_EVENT_TYPE),
                    //new EventDetails(PeriodEdit,"Period edit",BUSINESS_STRUCTURE_EVENT_TYPE),
                    //new EventDetails(PeriodDelete,"Period delete",BUSINESS_STRUCTURE_EVENT_TYPE),
                    //new EventDetails(CalculationConfiguration,"Calculation configuration",CALCULATION_EVENT_TYPE),
                    //new EventDetails(AddCalculationModule,"Add calculation module",CALCULATION_EVENT_TYPE),
                    //new EventDetails(WorkpaperUpdate,"Workpaper update",CALCULATION_EVENT_TYPE),
                    //new EventDetails(ATOFormPrint, "ATOFormPrint", USER_EVENT_TYPE),
                    //new EventDetails(ITLConfiguration, "ITL Configuration", USER_EVENT_TYPE),
                    //new EventDetails(DeleteCalculationModule, "Delete calculation module", CALCULATION_EVENT_TYPE),
                    //new EventDetails(BulkImportTrialBalance, "A bulk trial balance Imported", CALCULATION_EVENT_TYPE),
                    //new EventDetails(DeleteBulkTrialBalance, "A bulk trial balance deleted", CALCULATION_EVENT_TYPE),
					new EventDetails(ScenarioLocked, "A scenario locked", BUSINESS_STRUCTURE_EVENT_TYPE),
					new EventDetails(ScenarioUnLocked, "A scenario unlocked", BUSINESS_STRUCTURE_EVENT_TYPE), 
                    //new EventDetails(DeleteTrialBalance, "A trial balance deleted", CALCULATION_EVENT_TYPE),
                    //new EventDetails(ImportLedgerMap, "A ledger map imported", CALCULATION_EVENT_TYPE),
                    //new EventDetails(ImportConsolidationChart, "A consolidation chart imported", CALCULATION_EVENT_TYPE),
                    //new EventDetails(ImportTaxMap, "A tax map imported", CALCULATION_EVENT_TYPE),
					new EventDetails(UserEnabled, "A user enabled", USER_EVENT_TYPE),
                    //new EventDetails(AddMappingToLedgerAccount, "A mapping to ledger account added",CALCULATION_EVENT_TYPE),
                    //new EventDetails(RemoveMappingToLedgerAccount, "A mapping to ledger account removed", CALCULATION_EVENT_TYPE),
                    //new EventDetails(AddMappingToTaxAccount, "A mapping to tax account added", CALCULATION_EVENT_TYPE),
                    //new EventDetails(RemoveMappingToTaxAccount, "A mapping to tax account removed", CALCULATION_EVENT_TYPE),
					new EventDetails(PerformMigration, "Migration performed", SYSTEM_EVENT_TYPE),
					new EventDetails(RunExportReport, "A report run/exported", SYSTEM_EVENT_TYPE),
					new EventDetails(LicenceEntityBreach, "Create more entities than allowed", SYSTEM_EVENT_TYPE),
					new EventDetails(LicenceUserAccountBreach, "Create more user accounts than allowed", SYSTEM_EVENT_TYPE),
					new EventDetails(LicenceGroupBreach, "Create more groups than allowed", SYSTEM_EVENT_TYPE),
					new EventDetails(LicencePeriodBreach, "Create or modify a period with end date greater than allowed", SYSTEM_EVENT_TYPE),
					new EventDetails(LicenceScenarioBreach, "Attempt to create more scenarios than allowed in Licence", SYSTEM_EVENT_TYPE),
					new EventDetails(ErrorPrompt, "An error message Prompt", SYSTEM_EVENT_TYPE),
					new EventDetails(ConfirmationPrompt, "A comfirmation Prompt", SYSTEM_EVENT_TYPE),
					new EventDetails(MultiUserEvent, "Multi-user conflict", SYSTEM_EVENT_TYPE),
					new EventDetails(ErrorLogGenerated, "Error log file generate", SYSTEM_EVENT_TYPE),
					new EventDetails(UpdateComponentModule, "Component Modules updated", CALCULATION_EVENT_TYPE),
					new EventDetails(UpdatePasswordMinLength, "Edit password minimum length", SYSTEM_EVENT_TYPE),
					new EventDetails(UpdatePasswordMaxLength, "Edit password maximum length", SYSTEM_EVENT_TYPE),
					new EventDetails(UpdatePasswordHistory, "Edit the number of compared historical passwords", SYSTEM_EVENT_TYPE),
					new EventDetails(UpdateMaxRepeatingChars, "Edit the maximum number of repeating characters allowed", SYSTEM_EVENT_TYPE),
					new EventDetails(UpdateExpiryPeriod, "Edit expiry period", SYSTEM_EVENT_TYPE),
					new EventDetails(UpdateExpiryReminder, "Edit expiry reminder", SYSTEM_EVENT_TYPE),
					new EventDetails(UpdateAllowableAttempts, "Edit allowable password lockout attempts", SYSTEM_EVENT_TYPE),
					new EventDetails(AdministratorCheck, "Administrator rights changed", USER_EVENT_TYPE),
					new EventDetails(RestoreDefaultSettings, "Restore Default Settings", SYSTEM_EVENT_TYPE),
                    new EventDetails(CorrectionTableUpdateRowIsNull, "Correction DB null check", SYSTEM_EVENT_TYPE),
                  //  new EventDetails(WorkpaperVisited, "Workpaper visited", SYSTEM_EVENT_TYPE),
                    new EventDetails(SuperAdministratorCreated, "Super Administrator Created", SYSTEM_EVENT_TYPE),
                    new EventDetails(EntityViewed,"Entity viewed",BUSINESS_STRUCTURE_EVENT_TYPE),
                   new EventDetails(FileImported,"File Imported",USER_EVENT_TYPE),
                   new EventDetails(UMAAccountChange,"Account updated",USER_EVENT_TYPE),
                   new EventDetails(EmailSent,"Email sent",SYSTEM_EVENT_TYPE)
                   
				};
            }
        }

        public string GetEventType()
        {
            var eventName = EventDetailsArray.Where(item => item.Value == this.theValue).FirstOrDefault();
            if (string.IsNullOrEmpty(eventName.EventType))
                return USER_EVENT_TYPE;
            else
                return eventName.EventType;
        }

        public string GetEventName()
        {
            var eventName = EventDetailsArray.Where(item => item.Value == this.theValue).FirstOrDefault();
            if (string.IsNullOrEmpty(eventName.EventName))
                return theValue.ToString();
            else
                return eventName.EventName;
        }

    }

}
