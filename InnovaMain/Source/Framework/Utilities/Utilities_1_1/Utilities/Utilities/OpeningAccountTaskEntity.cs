﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Oritax.TaxSimp.Data;
//using System.Collections.ObjectModel;
//using Oritax.TaxSimp.Utilities;

//namespace Oritax.TaxSimp.Utilities
//{
//    public class SecurityHolding
//        {
//        public SecurityHolding()
//        {
//            Split = 1;
//        }

//        public string SecuirtyCode { get; set; }
//        public string Desc { get; set; }
//        public decimal UnitHolding { get; set; }
//        public decimal TranUnitHolding { get; set; }
//        public decimal Split { get; set; }

//        public decimal Difference
//            {
//            get { return UnitHolding - TranUnitHolding; }
//            }

//        public decimal UnitPrice { get; set; }
//        public decimal Total
//        {
//            get;
//            set;
//        }
//      }
    
//    public class OpeningAccountTaskEntity : PresentationTaskEntity
//    {
//        public OpeningAccountTaskEntity()
//            : base()
//        {
//        }

//        private decimal _Holding;
//        private decimal _SystemTransactionTotal;

//        public decimal Difference
//        {
//            get { return Holding - SystemTransactionTotal; }
//        }
//        // public decimal SystemTransactionTotal { get; set; }

//        //public decimal Holding { get; set; }
//        public decimal Holding
//        {
//            get { return _Holding; }
//            set
//            {
//                _Holding = value;
//            }
//        }
//        public decimal SystemTransactionTotal
//        {
//            get { return _SystemTransactionTotal; }
//            set
//            {
//                _SystemTransactionTotal = value;
//            }
//        }

//        public decimal Total { get { return Holding + UnsettledOrder; } }
//        public decimal UnsettledOrder { get; set; }
//        public DateTime? UploadTime { get; set; }
//        public string AccountNo { get; set; }
        
//        public ObservableCollection<SecurityHolding> Holdings { get; set; }
//        public Guid ProductSecuritiesId { get; set; }
//        public bool ShowDetailGrid { get; set; }

//        public bool DefaultProduct { get; set; }

//        public decimal Split { get; set; }
//    }
    
//    public class ManagedInvestmentSchemesAccountEntityCsidClidDetail
//     {
//         public Guid Csid { get; set; }
//         public Guid Clid { get; set; }
//         public TitleEntity BusinessTitle { get; set; }
//         public ManagedInvestmentSchemesAccountEntity Entity { get; set; }
//         public Guid FundID { get; set; }

//         public ManagedInvestmentSchemesAccountEntityCsidClidDetail()
//         {
//             BusinessTitle = new TitleEntity();
//             Entity = new ManagedInvestmentSchemesAccountEntity();
//             FundID = Guid.NewGuid();
//         }

//     }

//    public class MISFundTransactionPresentation
//    {
//        public Guid ID { get; set; }

//        public FundAccountEntity FundCode { get; set; }


//        public DateTime TradeDate { get; set; }
//        public string ClientID { get; set; }
//        public string TransactionType { get; set; }
//        public decimal Shares { get; set; }
//        public decimal UnitPrice { get; set; }
//        public decimal Amount
//        {
//            get { return Shares * UnitPrice; }
//        }
//        public string InvestorCategory { get; set; }
//        public string Status { get; set; }
//        public MISFundTransactionPresentation()
//        {
//            ID = Guid.Empty;
//            FundCode = new FundAccountEntity();

//            ClientID = string.Empty;
//            TransactionType = string.Empty;
//            InvestorCategory = string.Empty;
//            TradeDate = DateTime.Now;
//            UnitPrice = 0;
//            Shares = 0;
//            Status = string.Empty;
//        }
//    }

//    public class FundAccountEntity
//    {
//        public Guid ID { get; set; }
//        public string Code { get; set; }
//        public string Description { get; set; }
//        public ObservableCollection<MISFundTransactionEntity> FundTransactions { get; set; }
//        public double TotalShares { get; set; }
//        public double RedemptionPrice { get; set; }
//        public ObservableCollection<MISUnitHolderFile> SecurityHoldings { get; set; }

//        public FundAccountEntity()
//        {
//            ID = Guid.NewGuid();
//            Code = string.Empty;
//            Description = string.Empty;
//            FundTransactions = new ObservableCollection<MISFundTransactionEntity>();
//            TotalShares = 0;
//            RedemptionPrice = 0;
//            SecurityHoldings = new ObservableCollection<MISUnitHolderFile>();
//        }
//    }

//    public class MISUnitHolderFile
//    {
//        public MISUnitHolderFile()
//        {
//            Account = string.Empty;
//            FundCode = string.Empty;
//            FundName = string.Empty;
//            AsofDate = DateTime.Now;
//            SubAccount = string.Empty;
//            TotalShares = 0;
//            RedemptionPrice = 0;
//            CurrentValue = 0;
//            InvestorCategory = string.Empty;
//        }
//        public string Account { get; set; }
//        public string FundCode { get; set; }
//        public string FundName { get; set; }
//        public DateTime AsofDate { get; set; }
//        public string SubAccount { get; set; }
//        public double TotalShares { get; set; }
//        public double RedemptionPrice { get; set; }
//        public double CurrentValue { get; set; }
//        public string InvestorCategory { get; set; }
//    }

//    public class MISFundTransactionEntity
//    {
//        public Guid ID { get; set; }
//        public DateTime TradeDate { get; set; }
//        public string ClientID { get; set; }
//        public string TransactionType { get; set; }
//        public decimal Shares { get; set; }
//        public decimal UnitPrice { get; set; }
//        public decimal Amount
//        {
//            get { return Shares * UnitPrice; }
//        }
//        public string InvestorCategory { get; set; }
//        public string Status { get; set; }

//        public MISFundTransactionEntity()
//        {
//            ID = Guid.Empty;
//            ClientID = string.Empty;
//            TransactionType = string.Empty;
//            InvestorCategory = string.Empty;
//            TradeDate = DateTime.Now;
//            UnitPrice = 0;
//            Shares = 0;
//            Status = string.Empty;

//        }
//    }

//    public class ManagedInvestmentSchemesAccountEntity
//    {
//        public string Name { get; set; }
//        public ObservableCollection<FundAccountEntity> FundAccounts { get; set; }
//        public DateTime UploadTime { get; set; }


//        public ManagedInvestmentSchemesAccountEntity()
//        {
//            Name = string.Empty;
//            FundAccounts = new ObservableCollection<FundAccountEntity>();
//        }
//    }
//}
