using System;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Configuration;

namespace Oritax.TaxSimp.Utilities
{
	public class DBConnection
	{
		private static string strConnection;
		private static DBConnection objConnection = new DBConnection();
        
        public static bool IsTestMode
        {
            get
            {
                return objConnection.ConnectionString.Contains("Test");
            }
        }

		private DBConnection()
		{
			string strUID = null;
			string strUnencryptedUID = null;
			string strPwd = null;
			string strUnencryptedPwd = null;

            string strEncryptedConnection = ConfigurationManager.AppSettings["DBConnectionString"];
			string[] strConnectionProperties = strEncryptedConnection.Split( ';' );

			// Locate the encrypted uid and pwd in the configuration file.
			foreach( string strProperty in strConnectionProperties )
			{
				if( strProperty.Trim().StartsWith( "uid" ) )
				{
					strUID = strProperty.Substring( strProperty.IndexOf( "=" ) + 1 );
					if( strUID != null )
						strUnencryptedUID = Utilities.Encryption.DecryptData( strUID );
				}
				else if ( strProperty.Trim().StartsWith( "pwd" ) )
				{
					strPwd = strProperty.Substring( strProperty.IndexOf( "=" ) + 1 );
					if( strPwd != null )
						strUnencryptedPwd = Utilities.Encryption.DecryptData( strPwd );
				}
			}

			// Build an unencrypted version of the connection string for system usage.
			strConnection = null;
			foreach( string strProperty in strConnectionProperties )
			{
				if( strProperty.Trim().StartsWith( "uid" ) )
				{
					strConnection += "uid=" + strUnencryptedUID + ";";
				}
				else if( strProperty.Trim().StartsWith( "pwd" ) )
				{
					strConnection += "pwd=" + strUnencryptedPwd + ";";
				}
				else
					strConnection += strProperty + ";";
			}
		}

		public static DBConnection Connection
		{
			get{ return objConnection; }
		}

		public string ConnectionString
		{
			get{ return strConnection; }
		}
	}

	public class Encryption
	{
		private const string CryptoKey = "87482909";

		public static string EncryptData( string strData)
		{
			string strResult;

			DESCryptoServiceProvider descsp = new DESCryptoServiceProvider();

			descsp.Key = ASCIIEncoding.ASCII.GetBytes(CryptoKey);
			descsp.IV = ASCIIEncoding.ASCII.GetBytes(CryptoKey);
			ICryptoTransform desEncrypt = descsp.CreateEncryptor();

			MemoryStream mOut = new MemoryStream();
			CryptoStream encryptStream = new CryptoStream(mOut,	desEncrypt, CryptoStreamMode.Write);

			byte[] rbData = UnicodeEncoding.Unicode.GetBytes(strData);
			try
			{
				encryptStream.Write(rbData, 0, rbData.Length);
			}
			catch(Exception e)
			{
				throw new Exception( "TXEncryption.Encrypt error: " + e.Message );
			}
			encryptStream.FlushFinalBlock();

			if (mOut.Length == 0)
				strResult = "";
			else
			{
				byte []buff = mOut.ToArray();
				strResult = Convert.ToBase64String(buff, 0,	buff.Length);
			}
			encryptStream.Close();

			return strResult;
		}

		public static string DecryptData( string strData )
		{
			string strResult;

			DESCryptoServiceProvider descsp = new DESCryptoServiceProvider();

			descsp.Key = ASCIIEncoding.ASCII.GetBytes(CryptoKey);
			descsp.IV = ASCIIEncoding.ASCII.GetBytes(CryptoKey);

			ICryptoTransform desDecrypt = descsp.CreateDecryptor();

			MemoryStream mOut = new MemoryStream();
			CryptoStream decryptStream = new CryptoStream(mOut,	desDecrypt, CryptoStreamMode.Write);
			char [] carray = strData.ToCharArray();
			byte[] rbData = Convert.FromBase64CharArray(carray,	0, carray.Length);

			try
			{
				decryptStream.Write(rbData, 0, rbData.Length);
			}
			catch (Exception e)
			{
				throw new Exception( "TXEncryption.Decrypt error: " + e.Message );
			}

			decryptStream.FlushFinalBlock();

			UnicodeEncoding aEnc = new UnicodeEncoding();
			strResult = aEnc.GetString(mOut.ToArray());

			decryptStream.Close();

			return strResult;
		}	
	}
}
