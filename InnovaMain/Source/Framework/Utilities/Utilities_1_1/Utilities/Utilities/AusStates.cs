﻿namespace Oritax.TaxSimp.Utilities
{
    public class AusStates
    {
        public static string ReturnShortStateCodeFromLong(string value)
        {
            switch (value)
            {
                case "Australian Capital Territory":
                    return "ACT";
                case "New South Wales":
                    return "NSW";
                case "Northern Territory":
                    return "NT";
                case "Queensland":
                    return "QLD";
                case "South Australia":
                    return "SA";
                case "Tasmania":
                    return "TAS";
                case "Victoria":
                    return "VIC";
                case "Western Australia":
                    return "WA";
                default:
                    return value;
            }
        }
    }
}
