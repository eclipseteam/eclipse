using System;
using System.Data;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// Define all necessary fields in migration report
	/// </summary>
	public class MigrationReportEntry
	{
		public const string ATOENTRYKEY = "54A13D71-61D1-41ea-9E3F-7A63B5927BBD";
		public const string TEAENTRYKEY = "1FF89ADF-79C1-479f-8221-810D46C47425";
		public const string EXP40ENTRYKEY = "15D84984-C95C-40a0-A419-792D60BBE5FA";

		public const string TEAASSET = "Asset";
		public const string TEALIABILITY = "Liability";
	}	
	/// <summary>
	/// Summary description for MigrationUtilities.
	/// </summary>
	public class MigrationUtilities
	{
		private MigrationUtilities()
		{
		}

		public static void SetColumnValue( DataRow objRow, string strColumnName, object objValue )
		{
			if ( objValue == null )
				objRow[ strColumnName ] = DBNull.Value;
			else
				objRow[ strColumnName ] = objValue;
		}

		public static object GetColumnValue( DataRow objRow, string strColumnName )
		{
			object objValue = objRow[ strColumnName ];

			if ( objValue == DBNull.Value )
				return null;
			else
				return objValue;
		}

		//Removes the versioning string from strings
		//For example would take as a parameter the input string "AustralianLegals.1.0.3.aspx"
		//then return the value "AustralianLegals.aspx"
		public static string RemoveWorkpaperVersioning(string workpaperName)
		{
			string retVal = String.Empty;
			if(workpaperName != String.Empty)
			{
				int firstDecimal = workpaperName.IndexOf(".");
				int fileExtension = workpaperName.IndexOf(".aspx");

				retVal = workpaperName.Remove(firstDecimal, fileExtension-firstDecimal);
			}

			return retVal;
		}

		public static string AddWorkpaperVersion(string workpaper, string version)
		{
			string retVal = String.Empty;
			if(workpaper != String.Empty)
			{
				int firstDecimal = workpaper.IndexOf(".");
				retVal = workpaper.Insert(firstDecimal, version);
			}

			return retVal;
		}
	}
}
