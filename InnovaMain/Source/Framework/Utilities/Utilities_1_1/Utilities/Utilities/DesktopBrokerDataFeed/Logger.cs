﻿using System;
using System.IO;
using System.Configuration;

namespace eclipseonlineweb.DesktopBrokerDataFeed
{
    public class Logger
    {
        public static void Log(string line, string logFilePath)
        {
            try
            {
                if (Directory.Exists(logFilePath))
                {
                    if (File.Exists(logFilePath + DateTime.Now.ToString("dd-MM-yyyy")))
                    {
                        using (StreamWriter writer = File.AppendText(logFilePath + DateTime.Now.ToString("dd-MM-yyyy")))
                        {
                            writer.WriteLine(line);
                            writer.Close();
                        }
                    }
                    else
                    {
                        File.WriteAllText(logFilePath + DateTime.Now.ToString("dd-MM-yyyy"), line);
                    }
                }
                else
                {
                    Directory.CreateDirectory(logFilePath);
                    if (File.Exists(logFilePath + DateTime.Now.ToString("dd-MM-yyyy")))
                    {
                        using (StreamWriter writer = File.AppendText(logFilePath + DateTime.Now.ToString("dd-MM-yyyy")))
                        {
                            writer.WriteLine(line);
                            writer.Close();
                        }
                    }
                    else
                    {
                        File.WriteAllText(logFilePath + DateTime.Now.ToString("dd-MM-yyyy"), line);
                    }
                }
            }
            catch (UnauthorizedAccessException)
            {
                //Console.Write("Access denied to the path " + logFilePath);
                //Console.ReadLine();
            }
            catch
            {
            }
        }
    }
}
