﻿using System;
using System.IO;
using System.Net;
using System.Collections.Specialized;
using System.Configuration;


namespace eclipseonlineweb.DesktopBrokerDataFeed
{
    public class XMLDownloader
    {
        private string _url = string.Empty;
        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }

        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
        public string MakeRequest(string importFolder)
        {
            string saveFilePath = importFolder + "DesktopBrokerDataFeed\\XMLFiles\\";
            string logfilePath = importFolder + "DesktopBrokerDataFeed\\Logs\\";

            Logger.Log("\n"+DateTime.Now.ToString("dd/MM/yyy hh:mm:ss")+"File downloading started, please wait...", logfilePath);

            if (!Directory.Exists(saveFilePath))
            {
                Directory.CreateDirectory(saveFilePath);
            }

            string fileName = string.Empty;
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(Url);
                request.Method = "GET";
                request.Headers.Add(GetAthentication());
                var response = (HttpWebResponse)request.GetResponse();
                Logger.Log("\r\n"+DateTime.Now.ToString("dd/MM/yyy hh:mm:ss")+"Response received successfully.", logfilePath);
                Logger.Log("\n" + DateTime.Now.ToString("dd/MM/yyy hh:mm:ss") + "Processing reponse.", logfilePath);
                string reponseAsString = ConvertResponseToString(response);
                Logger.Log("\n" + DateTime.Now.ToString("dd/MM/yyy hh:mm:ss") + "Response processed succesfully.", logfilePath);
                fileName = saveFilePath + DateTime.Now.ToString("dd-MM-yyyy_") + DateTime.Now.ToFileTime() + ".xml";
                File.WriteAllText(fileName, reponseAsString);
                Logger.Log("\n" + DateTime.Now.ToString("dd/MM/yyy hh:mm:ss") + "File saved successfully.", logfilePath);
                return fileName;
            }
            catch (UnauthorizedAccessException ex)
            {
                Logger.Log("\n" + DateTime.Now.ToString("dd/MM/yyy hh:mm:ss") + "Access denied to the path " + saveFilePath + "_" + ex.Message, logfilePath);
            }
            catch (Exception ex)
            {
                Logger.Log("\n" + DateTime.Now.ToString("dd/MM/yyy hh:mm:ss") + "XML Downloader failed." + "_" + ex.Message, logfilePath);
            }
            return fileName;
        }

        private NameValueCollection GetAthentication()
        {
            NameValueCollection userCredential = new NameValueCollection();

            userCredential.Add("Username", ConfigurationManager.AppSettings["DBDataFeedUsername"]);
            userCredential.Add("Password", ConfigurationManager.AppSettings["DBDataFeedPassword"]);

            return userCredential;
        }

        string ConvertResponseToString(HttpWebResponse response)
        {
            string result = new StreamReader(response.GetResponseStream()).ReadToEnd();
            return result;
        }
    }
}
