using System;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// Summary description for WorkpaperFilters.
	/// </summary>
	public class WorkpaperFilterType
	{
		private WorkpaperFilterType()
		{
		}
		
		//Common
		public const string INCOMETAXCALCULATION	= "Income tax calculation";
		public const string TAXEFFECTACCOUNTING	= "Tax-effect accounting";

		// Workpaper filters
		
		public const string ASSETS	= "Assets";
		public const string LIABILITIES ="Liabilities";
		public const string EXPENSES	= "Expenses";
		public const string INCOME ="Income";	
		public const string ACCOUNTINGRECORDS ="Accounting records";
		public const string ATOFORMS	= "ATO Forms";
		public const string FIXEDASSETS ="Fixed assets";
		public const string CAPITALGAINS ="Capital gains";
		public const string FOREIGNINCOME	= "Foreign income";
		public const string TAXLOSSES	= "Tax losses";
		public const string LOSSES	= "Losses";
		public const string TAXOFFSETS ="Tax offsets";
		public const string CREDITS	= "Credits";
		public const string INSTALMENTS ="Instalments";
        public const string FRANKINGACCOUNT = "Franking Account";

		// Report Filters
		public const string COMPARISON = "Comparison";
		public const string VALIDATION ="Validation";
		public const string TRIALBALANCE ="Trial balance";
	}
}
