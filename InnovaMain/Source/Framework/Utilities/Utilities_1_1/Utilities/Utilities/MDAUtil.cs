﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.Utilities
{
    public class MDAUtil
    {
        #region Alert Email Report
        /// <summary>
        /// Sending email to adviser and Client on Due Date expires.
        /// </summary>       
        public void GeneratMDADocumentAndSendToAdviser(DataRow row, List<Data.ModelEntity> allModels, bool isFirstWarning, Action<string> callBack = null)
        {
            //Collecting Adviser and Client information for email body
            string adviserName = row[ClientDetailTable.ADVISERNAME].ToString();
            string clientName = row[ClientDetailTable.CLIENTNAME].ToString();
            string clientCode = row[ClientDetailTable.CLIENTID].ToString();
            bool serviceTypeDifm = false;
            bool serviceTypeDiwm = false;
            string attachFile = string.Empty;
            if (row[ClientDetailTable.HASDIFM] != DBNull.Value)
            serviceTypeDifm = Convert.ToBoolean(row[ClientDetailTable.HASDIFM]);

            if (row[ClientDetailTable.HASDIWM] != DBNull.Value)
            serviceTypeDiwm =Convert.ToBoolean(row[ClientDetailTable.HASDIWM]);

            //calculate due date and current date
            DateTime mdaExecutionDate = DateTime.Parse(row[ClientDetailTable.MDAEXECUTIONDATE].ToString());
            //todo:shall be decided after client consultation.
            DateTime dueDate = mdaExecutionDate.AddYears(1);//MDA due Date should be after one year            
            string IFAEmail = row[ClientDetailTable.IFAEMAIL].ToString();
            if(IFAEmail==string.Empty)
            {
                IFAEmail = "inquiries@e-Clipse.com.au";//Admin email shall be used
            }

            if (DBConnection.IsTestMode)
                IFAEmail = "alerts.test@objectsynergy.com";  
            
            const string fromEmail = "support@e-clipse.com.au";
            const string subject = "MDA Agreement Renewal";

            //string AttachFile = ConfigurationManager.AppSettings.Get(""MDAAgreement");
            var mdaGenerator = new MDADocumentGenerator();
            attachFile = mdaGenerator.CreateMDARenewalDocument(row, allModels, serviceTypeDifm, serviceTypeDiwm);
            
            //Making email body as per document.
            var sbBody = new StringBuilder();


           // sbBody.Append(string.Format("Dear {0},<br/><br/>", adviserName));
            if (isFirstWarning)
            {
                sbBody.Append(string.Format("Please be advised that the MDA agreement for client ‘{0} ({1})’ will expire on {2}.<br/><br/>", clientName, clientCode, dueDate.ToString("dd/MM/yyyy")));
            }
            else
            {
                sbBody.Append(string.Format("This is a final reminder that the MDA agreement for client ‘{0} ({1})’ will expire on {2}.<br/><br/>", clientName, clientCode, dueDate.ToString("dd/MM/yyyy")));
            }
            sbBody.Append("Attached is a renewal MDA agreement for execution. Please return the signed original to e-Clipse.<br/>");
            sbBody.Append("If you have any questions please do not hesitate to contact e-Clipse online team.<br/><br/>");
            sbBody.Append("*** This is an automatically generated email, please do not reply to this address ***<br/><br/>");
            sbBody.Append("Regards,<br/>");
            sbBody.Append("Administration<br/>");
            sbBody.Append("e-Clipse Online<br/><br/>");
            sbBody.Append("Phone: (02) 8203 9150<br/>");
            sbBody.Append("email: inquiries@e-Clipse.com.au<br/>");
            sbBody.Append("www.e-Clipse.com.au<br/>");

            //Calling email utility and send email to Adviser
            var objEmail = new EmailUtilities();
            //objEmail.SendRawEmail(fromEmail, IFAEmail, subject, string.Empty, sbBody.ToString(), attachFile);

            objEmail.SendRawEmail(fromEmail, IFAEmail, string.Empty, subject, string.Empty, sbBody.ToString(), attachFile);

            //For Future Ref
            //objEmail.SendEmailToDesitination(sbBody.ToString(), Subject, AdviserEmail, FromEmail);

            if(callBack!=null)
            {
                string message = string.Format("Client:{0} ({1}) - Adviser {2} - Email sent to {3}" , clientName,clientCode, adviserName, IFAEmail);
                callBack(message);
            }

        }
        #endregion

    }
}
