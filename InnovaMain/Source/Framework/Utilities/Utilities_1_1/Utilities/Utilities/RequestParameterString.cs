using System;
using System.Collections;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// Contains a request parameter string of the form:
	/// <![CDATA[name1=value1&ampname2=value2&amp ...]]>
	/// </summary>
	public class RequestParameterString : Hashtable
	{
		private string uRL;
		/// <summary>
		/// Creates a RequestParamterStringObject from a string of the correct format
		/// </summary>
		/// <param name="requestParameterString"></param>
		public RequestParameterString(string requestParameterString)
		{
			// if the string is null or empty, bomb out straight away
			if ( requestParameterString == null ||
				 requestParameterString == String.Empty )
				return;

			int startIndex,nameIndex,nameLength,valueIndex,ampIndex,valueLength,equalsIndex;
			string name,val;
			for(startIndex=0;startIndex>=0;)
			{
				if(-1!=(equalsIndex=requestParameterString.IndexOf('=',startIndex)))
				{
					nameIndex=startIndex;
					valueIndex=equalsIndex+1;
					nameLength=valueIndex-startIndex-1;
					if(-1!=(ampIndex=requestParameterString.IndexOf('&',valueIndex)))
						valueLength=ampIndex-valueIndex;
					else
						valueLength=requestParameterString.Length-valueIndex;
					name=requestParameterString.Substring(nameIndex,nameLength);
					val=requestParameterString.Substring(valueIndex,valueLength);
					base.Add(name,val);
					startIndex=ampIndex>0?ampIndex+1:ampIndex;
				}
			}
		}

		public RequestParameterString(string uRL, string[] names,string[] values)
		{
			this.uRL=uRL;

			int index=0;
			foreach(string name in names)
			{
				if(index<values.Length)
					base.Add(name,values[index++]);
			}
		}

		public RequestParameterString(string[] names,string[] values)
		{
			int index=0;
			foreach(string name in names)
			{
				if(index<values.Length)
					base.Add(name,values[index++]);
			}
		}

		public override string ToString()
		{
			string result="";
			bool first=true;
			foreach(DictionaryEntry dictionaryEntry in this)
			{
				if(!first)
				{
					result+="&";
				}
				else
				{
					if(uRL!=null)
					{
						result+=uRL+"?";
					}
				}
				result+=dictionaryEntry.Key.ToString()+"="+dictionaryEntry.Value.ToString();
				first=false;
			}
			return result;
		}


		public string this[string name]
		{
			get
			{
				if(base.Contains(name))
					return ((string)base[name]);
				else
					return null;
			}
			set
			{
				base[name] = value;
			}
		}
	}
}
