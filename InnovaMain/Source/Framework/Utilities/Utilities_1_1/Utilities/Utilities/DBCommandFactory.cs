using System;
using System.Data.SqlClient;
using System.Configuration;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// A class to create and return database command objects
	/// with a common initialisation
	/// </summary>
	public sealed class DBCommandFactory
	{
        #region Privates
        private const string TIMEOUT_APPSETTING = "DBCommandTimeout";
        private const int TIMEOUT_DEFAULT = 180;
        private static volatile DBCommandFactory theInstance = null;
        private static object syncRoot = new object();
        private static int m_commandTimeout;      
        #endregion

        #region Creation
        /// <summary>
        /// Thread safe creation of THE instance
        /// </summary>
        public static DBCommandFactory Instance
        {
            get
            {
                // only create if not already created
                if ( theInstance == null )
                {
                    // make sure it is thread safe
                    lock ( syncRoot )
                    {
                        if ( theInstance == null )
                        {
                            theInstance = new DBCommandFactory();
                        }
                    }
                }

                // return the one and only
                return theInstance;
            }
        }

		private DBCommandFactory()
		{
            Initialise();
		}

        private void Initialise()
        {
            try
            {
                string sTimeout = ConfigurationManager.AppSettings.Get( TIMEOUT_APPSETTING );
                int timeout = System.Convert.ToInt32( sTimeout );
                m_commandTimeout = timeout > 0 ? timeout : TIMEOUT_DEFAULT;
            }
            catch
            {
                m_commandTimeout = TIMEOUT_DEFAULT;
            }
        }
        #endregion

        #region Public methods

        /// <summary>
        /// Return a new SqlCommand object initialised with the default timeout
        /// </summary>
        /// <returns></returns>
        public SqlCommand NewSqlCommand()
        {
            SqlCommand newCommand = new SqlCommand();
            newCommand.CommandTimeout = m_commandTimeout;
            return newCommand;
        }

        /// <summary>
        /// Return a new SqlCommand object initialised with the default timeout
        /// </summary>
        /// <returns></returns>
        public SqlCommand NewSqlCommand( string cmdText )
        {
            SqlCommand newCommand = new SqlCommand( cmdText );
            newCommand.CommandText = cmdText;
            newCommand.CommandTimeout = m_commandTimeout;
            return newCommand;
        }

        /// <summary>
        /// Return a new SqlCommand object initialised with the default timeout
        /// </summary>
        /// <returns></returns>
        public SqlCommand NewSqlCommand( string cmdText, SqlConnection connection )
        {
            SqlCommand newCommand = new SqlCommand();
            newCommand.Connection = connection;
            newCommand.CommandText = cmdText;
            newCommand.CommandTimeout = m_commandTimeout;
            return newCommand;
        }

        #endregion
    }
}
