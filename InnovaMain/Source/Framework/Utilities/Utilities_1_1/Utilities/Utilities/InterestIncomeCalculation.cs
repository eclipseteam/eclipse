﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Utilities
{
    public  class InterestIncomeCalculation
    {
        public static decimal InterestIncome(decimal amount, decimal interestRate, int daysFactor)
        {
            return (amount * interestRate / 100) * daysFactor;
        }

        public static int DaysFactor(DateTime maturityDate, DateTime transactionDate)
        {
            return ((maturityDate - transactionDate).Days) / 365;
        }
    }
}
