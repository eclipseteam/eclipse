﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Utilities
{
   public class FileUtil
    {
       public string GetFileName(string path)
       {
           string fileName = string.Empty;
           string[] pathArray = path.Split('\\');
           if (pathArray.Length > 0)
           {
               fileName = pathArray[pathArray.Length - 1];
           }
           return fileName;
       }
    }
}
