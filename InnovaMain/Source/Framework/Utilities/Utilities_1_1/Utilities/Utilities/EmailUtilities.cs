﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;
using System.Reflection;
using Amazon;

namespace Oritax.TaxSimp.Utilities
{
    public class EmailUtilities
    {
        public void SendEmailToDesitination(string bodyContent, string subject, string toAddress, string formAddress, string sourceAddress = "support@e-clipse.com.au", string returnPathAddress = "support@e-clipse.com.au", string replyToAddress = "support@e-clipse.com.au")
        {
            string awsAccessKey = "AKIAJ7MJ5JBFYSV4RQ3Q";
            string awsSecretKey = "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";

            AmazonSimpleEmailServiceClient ses = new AmazonSimpleEmailServiceClient(awsAccessKey, awsSecretKey);

            Amazon.SimpleEmail.Model.Message msg = new Amazon.SimpleEmail.Model.Message();

            msg.Subject = new Content(subject);
            Body body = new Body();



            //Content content = new Content(@"<p style=""font-family:Calibri; font-size:medium"">Message Number:" + messageNumber + "<br/> Message Summary:" + messageSummary + "<br/> Message Detail:" + messageDetail + "<br/><br/>" + ConvertDataTableToHtml(dataset) + "</p>");
            Content content = new Content(bodyContent);

            //Email Recipients
            body.Html = content;
            msg.Body = body;
            Destination dest = new Destination();
            dest.ToAddresses.Add(toAddress);
            SendEmailRequest sendEmailRequest = new SendEmailRequest();

            sendEmailRequest.ReplyToAddresses.Add(replyToAddress);
            sendEmailRequest.Source = sourceAddress;
            sendEmailRequest.ReturnPath = returnPathAddress; //Undeliverable messages get sent do this address
            sendEmailRequest.Message = msg;
            sendEmailRequest.Destination = dest;


            ses.SendEmail(sendEmailRequest);
        }
        /// <summary>
        /// Send a raw email using Amazon Simple Email SDK.
        /// </summary>
        /// <param name="formAddresses">Single From Address</param>
        /// <param name="toAddresses">To Addresses, multiple with comma(,)</param>
        /// <param name="cc">CC Addresses, multiple with comma(,)</param>
        /// <param name="Subject">Subject Line of Email</param>
        /// <param name="textBodyContent">Text Body Content</param>
        /// <param name="htmlBodyContent">Html Body Content</param>
        /// <param name="attachPath">Attachment path, leave blank for no attachment</param>
        /// <param name="replyTo">Single Reply To Path</param>
        /// <returns>True on sucess /False on fail.</returns>
        public Boolean SendRawEmail(String formAddresses, String toAddresses, String cc, String Subject, String textBodyContent, String htmlBodyContent, string attachPath = "", String replyTo = "support@e-clipse.com.au")
        {
            AlternateView plainView = AlternateView.CreateAlternateViewFromString(textBodyContent, Encoding.UTF8, "text/plain");
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(htmlBodyContent, Encoding.UTF8, "text/html");

            MailMessage mailMessage = new MailMessage();

            mailMessage.From = new MailAddress(formAddresses);

            //Creating list of toAddress for multiple addresses.
            List<String> toAddressesList = toAddresses.Replace(", ", ",").Split(',').ToList();

            foreach (String toAddress in toAddressesList)
            {
                mailMessage.To.Add(new MailAddress(toAddress));
            }

            //Creating list of ccAddress for multiple addresses.
            List<String> ccAddresses = cc.Replace(", ", ",").Split(',').Where(y => y != "").ToList();
            foreach (String ccAddress in ccAddresses)
            {
                mailMessage.CC.Add(new MailAddress(ccAddress));
            }

            //Adding Subject
            mailMessage.Subject = Subject;
            mailMessage.SubjectEncoding = Encoding.UTF8;

            //Adding reptyTo address, if exists
            if (replyTo != null)
            {
                mailMessage.ReplyToList.Add(new MailAddress(replyTo));
            }
            //Adding html Body Content for main content.
            if (htmlBodyContent != null)
            {
                mailMessage.AlternateViews.Add(htmlView);
            }
            //Adding text Body Content for ending/closing text.
            if (textBodyContent != null)
            {
                mailMessage.AlternateViews.Add(plainView);
            }

            //Attaching file from given path.
            if (attachPath.Trim() != "")
            {
                if (System.IO.File.Exists(attachPath))
                {
                    System.Net.Mail.Attachment objAttach = new System.Net.Mail.Attachment(attachPath);
                    objAttach.ContentType = new ContentType("application/octet-stream");
                    System.Net.Mime.ContentDisposition disposition = objAttach.ContentDisposition;
                    disposition.DispositionType = "attachment";
                    disposition.CreationDate = System.IO.File.GetCreationTime(attachPath);
                    disposition.ModificationDate = System.IO.File.GetLastWriteTime(attachPath);
                    disposition.ReadDate = System.IO.File.GetLastAccessTime(attachPath);
                    mailMessage.Attachments.Add(objAttach);
                }
            }

            RawMessage rawMessage = new RawMessage();

            using (MemoryStream memoryStream = ConvertMailMessageToMemoryStream(mailMessage))
            {
                rawMessage.WithData(memoryStream);
            }

            SendRawEmailRequest request = new SendRawEmailRequest();
            request.WithRawMessage(rawMessage);

            request.WithDestinations(toAddresses);
            request.WithSource(formAddresses);

            //Create SES object
            //Get AccessKey from config file.

            //string awsAccessKey = ConfigurationManager.AppSettings.Get("AccessKeyId");
            //string awsSecretKey = ConfigurationManager.AppSettings.Get("SecretKeyId");

            string awsAccessKey = "AKIAJ7MJ5JBFYSV4RQ3Q";
            string awsSecretKey = "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";

            AmazonSimpleEmailService ses = AWSClientFactory.CreateAmazonSimpleEmailServiceClient(awsAccessKey, awsSecretKey);

            try
            {
                SendRawEmailResponse response = ses.SendRawEmail(request);
                SendRawEmailResult result = response.SendRawEmailResult;
                return true;
            }
            catch (AmazonSimpleEmailServiceException ex)
            {
                return false;
            }
        }
        public static MemoryStream ConvertMailMessageToMemoryStream(MailMessage message)
        {
            Assembly assembly = typeof(SmtpClient).Assembly;
            Type mailWriterType = assembly.GetType("System.Net.Mail.MailWriter");
            MemoryStream fileStream = new MemoryStream();
            ConstructorInfo mailWriterContructor = mailWriterType.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { typeof(Stream) }, null);
            object mailWriter = mailWriterContructor.Invoke(new object[] { fileStream });
            MethodInfo sendMethod = typeof(MailMessage).GetMethod("Send", BindingFlags.Instance | BindingFlags.NonPublic);
            try
            {
                //4.0                
                sendMethod.Invoke(message, BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { mailWriter, true }, null);
            }
            catch
            {
                //4.5
                sendMethod.Invoke(message, BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { mailWriter, true, true }, null);
            }
            MethodInfo closeMethod = mailWriter.GetType().GetMethod("Close", BindingFlags.Instance | BindingFlags.NonPublic);
            closeMethod.Invoke(mailWriter, BindingFlags.Instance | BindingFlags.NonPublic, null, new object[] { }, null);
            return fileStream;
        }
    }
}
