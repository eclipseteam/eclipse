using System;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// Summary description for VersionUtilities.
	/// </summary>
	public class VersionUtilities
	{
		private VersionUtilities()
		{
		}

        /// <summary>
        /// adds version information to the workpaper name
        /// Expects "WpName.aspx" , returns "WpName_1_1.aspx"
        /// </summary>
        /// <param name="strWorkpaper">such as "WpName.aspx", or "WpName"</param>
        /// <param name="strVersionString">version, such as "1_1"</param>
        /// <returns>Workpaper url, including version"</returns>
		public static string GetVersionedWorkpaper( string strWorkpaper, string strVersionString )
		{
			if ( String.IsNullOrEmpty(strWorkpaper))
                throw new ArgumentNullException("GetVersionedWorkpaper: strWorkpaper can't be null or empty");

            string result = String.Empty;
			int intFilenameEnd = strWorkpaper.LastIndexOf( "." );

            if (intFilenameEnd > 0)
            {
                int intExtensionStart = intFilenameEnd + 1;
                int intExtensionEnd = strWorkpaper.Length;

                string strFilename = strWorkpaper.Substring(0, intFilenameEnd);
                string strExtension = strWorkpaper.Substring(intExtensionStart, intExtensionEnd - intExtensionStart);
                result = strFilename + "_" + strVersionString + "." + strExtension;
            }
            else
            {
                result = strWorkpaper + "_" + strVersionString + ".aspx";
            }
            return result;
		}
	}
}
