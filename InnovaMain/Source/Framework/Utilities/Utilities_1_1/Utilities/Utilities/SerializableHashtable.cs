using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Reflection;
using System.Diagnostics;

namespace Oritax.TaxSimp.Utilities
{
    /// <summary>
    /// Delegate to handle any changes in objects stored in a hashtable
    /// </summary>
    public delegate void DataUpdateDelegate(object key);

    /// <summary>
    /// Delegate called when data is being revived from a deleted state
    /// </summary>
    /// <example>e.g. Data has been deleted and then re-added or updated</example>
    public delegate void DataBeingRevivedDelegate(object key, object deletedData, object newData);


    /// <summary>
    /// Class to represent objects deleted from SerializableHashtables
    /// </summary>
    [Serializable]
    public class DeletedElement
    {
        private object objKey;
        private object objDeleted;

        public DeletedElement(object key, object deleted)
        {
            if (key == null)
                throw new ArgumentNullException("key");

            if (deleted == null)
                throw new ArgumentNullException("deleted");

            objKey = key;
            objDeleted = deleted;
        }

        #region Properties
        public object Key
        {
            get { return objKey; }
        }
        public object DeletedObject
        {
            get { return objDeleted; }
        }
        #endregion
    }

    /// <summary>
    /// SerializableHashtable class used instead of Hashtable for any stateful data in the system.
    /// This wrapper of the hashtable framework class is used to set the version member
    /// in hashtable prior to serialization. This was done as the tokens calculated when the 
    /// version number was incremented resulted in CM's being marked as modified when they 
    /// shouldn't be. Therefore reflection is used to set the version to 1 each time.
    /// </summary>
    [Serializable]
    public class SerializableHashtable : IDictionary, ICollection, IEnumerable, ISerializable, IDeserializationCallback, ICloneable
    {
        protected Hashtable ht;

        private bool isTrackingHT;
        private Hashtable htAdded;
        private Hashtable htUpdated;
        private Hashtable htDeleted;

        /// <summary>
        /// Creates a SerializableHashtable that does not track changes
        /// </summary>
        public SerializableHashtable()
        {
            ht = new Hashtable();
        }

        /// <summary>
        /// Creates a SerializableHashtable
        /// </summary>
        /// <param name="isTracking">True if you want to track changes to the hashtable, false otherwise</param>
        /// <remarks>A tracking hashtable has properties that allow you to retrive items that have been added, removed or updated</remarks>
        public SerializableHashtable(bool isTracking)
        {
            ht = new Hashtable();

            isTrackingHT = isTracking;

            if (isTracking)
            {
                // init for dynamic change tracking
                htAdded = new Hashtable();
                htUpdated = new Hashtable();
                htDeleted = new Hashtable();
            }
        }

        public SerializableHashtable(SerializationInfo info, StreamingContext context)
        {
            this.ht = (Hashtable)Serialize.GetSerializedValue(info, "serht_ht", typeof(Hashtable), new Hashtable());
            this.isTrackingHT = Serialize.GetSerializedBool(info, "serht_isTrackingHT");

            this.htAdded = (Hashtable)Serialize.GetSerializedValue(info, "serht_htAdded", typeof(Hashtable), new Hashtable());
            this.htUpdated = (Hashtable)Serialize.GetSerializedValue(info, "serht_htUpdated", typeof(Hashtable), new Hashtable());
            this.htDeleted = (Hashtable)Serialize.GetSerializedValue(info, "serht_htDeleted", typeof(Hashtable), new Hashtable());

            //if (this.isTrackingHT)
            //{
            //    this.htAdded = (Hashtable)Serialize.GetSerializedValue(info, "serht_htAdded", typeof(Hashtable), null);
            //    this.htUpdated = (Hashtable)Serialize.GetSerializedValue(info, "serht_htUpdated", typeof(Hashtable), null);
            //    this.htDeleted = (Hashtable)Serialize.GetSerializedValue(info, "serht_htDeleted", typeof(Hashtable), null);
            //}
        }

        //Only allow the .NET Serialization core code to call this function
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            //Use reflection to get access to the private member of hashtable 'version'
            FieldInfo version = ht.GetType().GetField("version", BindingFlags.Instance | BindingFlags.NonPublic);
            //Assert version is not null just in case later framework versions change the implementation
            //of this private member.
            Debug.Assert(version != null);
            if (version != null)
            {
                object objVersion = version.GetValue(ht);
                //Assert that the version value is an integer type, then if it is always set it to 1.
                Debug.Assert(objVersion is int);
                if (objVersion is int)
                    version.SetValue(ht, 1);
            }

            Serialize.AddSerializedValue(info, "serht_ht", ht);

            Serialize.AddSerializedValue(info, "serht_isTrackingHT", this.isTrackingHT);

            Serialize.AddSerializedValue(info, "serht_htAdded", this.htAdded);
            Serialize.AddSerializedValue(info, "serht_htUpdated", this.htUpdated);
            Serialize.AddSerializedValue(info, "serht_htDeleted", this.htDeleted);

            //if (this.isTrackingHT)
            //{
            //    Serialize.AddSerializedValue(info, "serht_htAdded", this.htAdded);
            //    Serialize.AddSerializedValue(info, "serht_htUpdated", this.htUpdated);
            //    Serialize.AddSerializedValue(info, "serht_htDeleted", this.htDeleted);
            //}
        }

        //IDictionary
        public virtual void Add(object key, object value)
        {
            if (ht.ContainsKey(key))
            {
                ht[key] = value;
                this.OnDataUpdate(key);
            }
            else
            {
                ht.Add(key, value);
                OnDataAdd(key);
            }
        }

        public virtual void Clear()
        {
            ht.Clear();
            ResetTrackingTables();
        }

        public virtual void Remove(object key)
        {
            // need to add to the dynamic tracking table
            // before removing from the main table
            OnDataRemove(key);

            ht.Remove(key);
        }

        public virtual object this[object key]
        {
            get
            {
                return ht[key];
            }
            set
            {
                if (ht.Contains(key))
                {
                    this.ht[key] = value;
                    this.OnDataUpdate(key);
                }
                else
                {
                    this.ht[key] = value;
                    this.OnDataAdd(key);
                }
            }
        }

        public virtual ICollection Values { get { return ht.Values; } }
        public virtual bool IsReadOnly { get { return ht.IsReadOnly; } }
        public virtual bool IsFixedSize { get { return ht.IsFixedSize; } }
        public virtual bool Contains(object key) { return ht.Contains(key); }
        public virtual ICollection Keys { get { return ht.Keys; } }
        public virtual IDictionaryEnumerator GetEnumerator() { return ht.GetEnumerator(); }
        public Hashtable Base
        {
            get
            {
                return ht;
            }
        }
        /// <summary>
        /// This is a Utility Method to clone underlining HashTable for existing SerializableHashtable.
        /// This Clone can be used to work with the elements of SerializableHashTable
        /// without invoking OndataAdd(), OnDataRemove(), etc methods.
        /// It never returns null.
        /// </summary>
        /// <param name="sht">SerializableHashtable containing HahTable to clone</param>
        /// <returns>ht.Clone(), or new HashTable where argument is null</returns>
        public static Hashtable CloneBaseHashtable(SerializableHashtable sht)
        {
            Hashtable clone = new Hashtable();

            if (sht != null)
                clone = (Hashtable)sht.Base.Clone();
            return clone;
        }
        //IDeserialization
        public virtual void OnDeserialization(object sender)
        {
            //ht.OnDeserialization(sender); 
        }

        //ICollection Interface
        public virtual int Count { get { return ht.Count; } }
        public virtual bool IsSynchronized { get { return ht.IsSynchronized; } }
        public virtual object SyncRoot { get { return ht.SyncRoot; } }
        public virtual void CopyTo(Array array, int index) { ht.CopyTo(array, index); }

        /// <summary>
        /// Creates a shallow copy of the hashtable
        /// </summary>
        /// <returns>A Shallow copy of the hashtable</returns>
        public virtual object Clone()
        {
            SerializableHashtable clonedObject = new SerializableHashtable(this.isTrackingHT);
            clonedObject.ht = (Hashtable)this.ht.Clone();

            if (this.isTrackingHT)
            {
                clonedObject.htAdded = (Hashtable)this.htAdded.Clone();
                clonedObject.htUpdated = (Hashtable)this.htUpdated.Clone();
                clonedObject.htDeleted = (Hashtable)this.htDeleted.Clone();
            }

            return clonedObject;
        }

        /// <summary>
        /// Creates a clone of the current state including tracked changes
        /// </summary>
        /// <param name="hashtableToCloneTo">The SerializableHashtable to put the clone into</param>
        public void CloneToSerializableHashtable(SerializableHashtable hashtableToCloneTo)
        {
            hashtableToCloneTo.ht = (Hashtable)this.ht.Clone();
            hashtableToCloneTo.htAdded = (Hashtable)this.htAdded.Clone();
            hashtableToCloneTo.htDeleted = (Hashtable)this.htDeleted.Clone();
            hashtableToCloneTo.htUpdated = (Hashtable)this.htUpdated.Clone();
        }

        //IEnumerable
        IEnumerator IEnumerable.GetEnumerator() { return ht.GetEnumerator(); }

        //Hashtable properties
        public virtual bool ContainsKey(object key) { return ht.ContainsKey(key); }
        public virtual bool ContainsValue(object val) { return ht.ContainsValue(val); }

        #region Dynamic update tracking
        /// <summary>
        /// Indicates that the hashtable tracks changes to the internal state
        /// </summary>
        public bool IsTracking
        {
            get { return isTrackingHT; }
        }

        /// <summary>
        /// Returns the data that has been updated in the hashtable since the last
        /// reset of the tracking tables
        /// </summary>
        public Hashtable UpdatedData
        {
            get { return htUpdated; }
        }

        /// <summary>
        /// Returns the data that has been added to the hashtable since the last
        /// reset of the tracking tables
        /// </summary>
        public Hashtable AddedData
        {
            get { return htAdded; }
        }

        /// <summary>
        /// Returns the data that has been deleted from the hashtable since the last
        /// reset of the tracking tables
        /// </summary>
        /// <remarks>This is a COPY of the deleted items</remarks>
        public ArrayList DeletedData
        {
            get
            {
                ArrayList deletedData = new ArrayList(htDeleted.Values);
                return deletedData;
            }
        }

        public int DeletedDataCount
        {
            get
            {
                if (!this.isTrackingHT)
                    return 0;
                else
                    return this.htDeleted.Count;
            }
        }

        /// <summary>
        /// Reset the update tracking hashtables
        /// </summary>
        public void ResetTrackingTables()
        {
            if (!IsTracking) return;
            if (htAdded != null) htAdded.Clear();
            if (htUpdated != null) htUpdated.Clear();
            if (htDeleted != null) htDeleted.Clear();
        }

        /// <summary>
        /// Add object to the Updated records table for dynamic tracking
        /// </summary>
        /// <param name="key"></param>
        /// Delegate that is called when a change in data occurs for an object
        /// in the hashtable. All updated objects are to be placed in the 
        /// updated hashtable so changes can be tracked dynamically.
        public virtual void OnDataUpdate(object key)
        {
            if (!IsTracking)
                return;

            if (ht[key] == null)
                throw new InvalidOperationException("Data being updated is null in the item table");

            // if the item has been added then leave it as added but update it
            if (this.htAdded.ContainsKey(key))
                htAdded[key] = ht[key];
            else
                htUpdated[key] = ht[key];

            // if the item has been previously deleted
            // remove it from the deleted list
            if (this.htDeleted.ContainsKey(key))
            {
                DeletedElement deletedElement = (DeletedElement)htDeleted[key];
                this.FireDataBeingRevived(key, deletedElement.DeletedObject, ht[key]);

                htDeleted.Remove(key);
            }
        }

        /// <summary>
        /// Add object to the Added records table for dynamic tracking
        /// </summary>
        /// <param name="key">The key being added</param>
        protected virtual void OnDataAdd(object key)
        {
            if (!IsTracking)
                return;

            if (htAdded.ContainsKey(key))
                throw new InvalidOperationException("You are trying to add data that has already been added");

            if (htUpdated.ContainsKey(key))
                throw new InvalidOperationException("You are trying to add data that has already been updated");

            this.CheckChildSerializableHashtableState(ht[key]);

            if (ht[key] == null)
                throw new InvalidOperationException("Data being added is null in the item table");

            if (htDeleted.ContainsKey(key))
            {
                DeletedElement deletedElement = (DeletedElement)htDeleted[key];
                this.FireDataBeingRevived(key, deletedElement.DeletedObject, ht[key]);

                htUpdated.Add(key, ht[key]);
                htDeleted.Remove(key);
            }
            else
                htAdded[key] = ht[key];
        }

        /// <summary>
        /// Add object to the deleted records table for dynamic tracking
        /// </summary>
        /// <param name="key"></param>
        /// As there can be more than one object with the same key (but different ID)
        /// in the DeletedList always add objects to this list. Overrides can handle
        /// differently.
        protected virtual void OnDataRemove(object key)
        {
            if (!IsTracking)
                return;

            // if the item is not in the added list, add it to the deleted list
            if (!htAdded.ContainsKey(key))
            {
                // only place in deleted bucket if it isn't already there
                if (!htDeleted.Contains(key))
                {
                    // only delete it if it is in the item hashtable
                    // for instance if it has been added then deleted
                    // then it won't be recorded anywhere
                    if (ht[key] != null)
                    {
                        DeletedElement element = new DeletedElement(key, ht[key]);
                        htDeleted[element.Key] = element;
                    }
                }
            }
            else
                htAdded.Remove(key);

            // always remove from updated list
            htUpdated.Remove(key);
        }

        private void CheckChildSerializableHashtableState(object child)
        {
            if (child is SerializableHashtable)
            {
                SerializableHashtable childHT = child as SerializableHashtable;

                if (childHT.htDeleted.Count > 0 ||
                    childHT.htUpdated.Count > 0)
                    throw new InvalidOperationException("Child serializable hashtable cannot have any updated or deleted elements");
            }
        }

        private void FireDataBeingRevived(object key, object deletedData, object newData)
        {
            if (this.OnDataBeingRevived != null)
                this.OnDataBeingRevived(key, deletedData, newData);
        }

        public event DataBeingRevivedDelegate OnDataBeingRevived;
        #endregion
    }
}
