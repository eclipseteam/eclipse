using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Oritax.TaxSimp.Utilities
{
    /// <summary>
    /// Summary description for Percent.
    /// </summary>
    [Serializable]
    public class Percent : ISerializable
    {
        //USING CUSTOM SERIALIZATION.
        //Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
        //You must add your own serialzation code for each instance
        //See GetObjectData & Special Constructor

        // The  percentage value stored as fixed point 2 dec places
        //int percentX100;
        double percentVal;
        public int DecimalsToDisplay = 2;

        #region CONSTRUCTORS
        public Percent() { percentVal = 0; }
        /// <summary>
        /// Takes integer input (eg '50' results in '50%')
        /// </summary>
        /// <param name="percent"></param>
        public Percent(int percent) { percentVal = (Convert.ToDouble(percent) / 100); }
        /// <summary>
        /// Takes double input (eg '0.505' results in '50.5%')
        /// </summary>
        /// <param name="percent"></param>
        public Percent(double percent) { percentVal = percent; }
        /// <summary>
        /// Takes decimal input (eg '0.505' results in '50.5%')
        /// </summary>
        /// <param name="percent"></param>
        public Percent(decimal percent) { percentVal = Convert.ToDouble(percent); }
        /// <summary>
        /// Takes integer input (eg '50' results in '50%') with the specified decimal places
        /// </summary>
        /// <param name="percent"></param>
        /// <param name="decimalPlaces"></param>
        public Percent(int percent, int decimalPlaces)
        {
            percentVal = (Convert.ToDouble(percent) / 100);
            DecimalsToDisplay = decimalPlaces;
        }

        protected Percent(SerializationInfo si, StreamingContext context)
        {
            percentVal = Serialize.GetSerializedDouble(si, "pc_percentVal");
        }

        //Only allow the .NET Serialization core code to call this function
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
        {
            Serialize.AddSerializedValue(si, "pc_percentVal", percentVal);
        }

        #endregion
        #region OPERATORS
        public static Percent operator +(Percent val1, Percent val2)
        {
            Percent result = new Percent();
            result.percentVal = Math.Round(val1.percentVal + val2.percentVal, val1.DecimalsToDisplay);
            result.DecimalsToDisplay = val1.DecimalsToDisplay;
            return result;
        }

        public static Percent operator -(Percent val1, Percent val2)
        {
            // note that this should return a negative percent if it is negative, not zero
            Percent result = new Percent();
            result.percentVal = Math.Round(val1.percentVal - val2.percentVal);
            result.DecimalsToDisplay = val1.DecimalsToDisplay;
            return result;
        }

        //PMB 10/10/02 - Modified to return a decimal instead of an int
        public static decimal operator *(Percent val1, int val2)
        {
            return Decimal.Round(Convert.ToDecimal(val1.percentVal * val2), val1.DecimalsToDisplay);
        }

        //PMB 10/10/02 - added
        public static decimal operator *(Percent val1, Decimal val2)
        {
            return Decimal.Round((Convert.ToDecimal(val1.percentVal) * val2), val1.DecimalsToDisplay);
        }

        public static bool operator ==(Percent val1, Percent val2)
        {
            // We have to handle the case when one of the references is null - must
            // cast to Object to avoid invoking this overload
            if (((Object)val1) != null && ((Object)val2) != null)
                return val1.percentVal == val2.percentVal;
            // Return true if both are null
            else if (((Object)val1) == null && ((Object)val2) == null)
                return true;
            else
                return false;
        }

        public static bool operator !=(Percent val1, Percent val2)
        {
            // We have to handle the case when one of the references is null - must
            // cast to Object to avoid invoking this overload
            if (((Object)val1) != null && ((Object)val2) != null)
                return val1.percentVal != val2.percentVal;
            // Return true if both are null
            else if (((Object)val1) == null && ((Object)val2) == null)
                return false;
            else
                return true;
        }

        // PSV created: 10/09/02
        public static bool operator >(Percent objLeft, Percent objRight)
        {
            return objLeft.percentVal > objRight.percentVal;
        }

        // PSV created: 10/09/02
        public static bool operator >(Percent objLeft, int intRight)
        {
            return objLeft.percentVal > intRight;
        }

        // PSV created: 10/09/02
        public static bool operator <(Percent objLeft, Percent objRight)
        {
            return objLeft.percentVal < objRight.percentVal;
        }

        // PSV created: 10/09/02
        public static bool operator <(Percent objLeft, int intRight)
        {
            return objLeft.percentVal < intRight;
        }

        // PSV created: 10/09/02
        public static bool operator >=(Percent objLeft, Percent objRight)
        {
            return objLeft.percentVal >= objRight.percentVal;
        }

        // PSV created: 10/09/02
        public static bool operator >=(Percent objLeft, int intRight)
        {
            return objLeft.percentVal >= intRight;
        }

        // PSV created: 10/09/02
        public static bool operator <=(Percent objLeft, Percent objRight)
        {
            return objLeft.percentVal <= objRight.percentVal;
        }

        // PSV created: 10/09/02
        public static bool operator <=(Percent objLeft, int intRight)
        {
            return objLeft.percentVal <= intRight;
        }

        public override bool Equals(Object val1)
        {
            return (val1 is Percent) && this == (Percent)val1;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static Percent operator *(int val1, Percent val2) { return new Percent(val2 * val1); }

        static public explicit operator Percent(int val) { return new Percent(val); }
        static public explicit operator Percent(double val) { return new Percent(val); }
        static public explicit operator Percent(decimal val) { return new Percent(val); } //Added PMB 10/10/02

        #endregion

        public override String ToString()
        {
            string stringValue = Math.Round((this.percentVal * 100), DecimalsToDisplay).ToString();
            if (stringValue.IndexOf('.') != -1)
            {
                stringValue = stringValue.TrimEnd(new char[] { '0' });
                stringValue = stringValue.TrimEnd(new char[] { '.' });
            }
            return stringValue;
        }

        public double DoublePercent
        {
            get
            {
                return this.percentVal;
            }
        }

        public decimal DecimalPercent
        {
            get
            {
                return Convert.ToDecimal(this.percentVal);
            }
        }


        /// <summary>
        /// Creates a percent object from a string.
        /// </summary>
        /// <param name="strPercent">Percent as string (eg "100.5" will be 100.5%)</param>
        /// <returns>Percent object</returns>
        public static Percent FromString(string strPercent)
        {
            try
            {
                Percent returnVal = new Percent();

                // trim off any trailing or leading spaces
                string strModifiedPercent = strPercent.Trim();

                // check if it ends with a % sign, if it does, remove it
                if (strModifiedPercent.EndsWith("%"))
                    strModifiedPercent = strModifiedPercent.Substring(0, strModifiedPercent.Length - 1);

                //If and empty string is passed in, then just set it to a zero value.
                if (strModifiedPercent == String.Empty)
                    strModifiedPercent = "0";

                // finally convert it to double
                returnVal.percentVal = Convert.ToDouble(strModifiedPercent) / 100;

                return returnVal;
            }
            catch
            {
                throw new ArgumentException("FromString cannot interpret string as a Percentage", strPercent);
            }
        }

        /// <summary>
        /// Creates a percent object from a string.
        /// </summary>
        /// <param name="strPercent">Percent as string (eg "100.5" will be 100.5%)</param>
        /// <param name="decimalPlaces">with specified decimal places</param>
        /// <returns></returns>
        public static Percent FromString(string strPercent, int decimalPlaces)
        {
            Percent returnVal = Percent.FromString(strPercent);
            returnVal.DecimalsToDisplay = decimalPlaces;
            return returnVal;
        }

    }
}
