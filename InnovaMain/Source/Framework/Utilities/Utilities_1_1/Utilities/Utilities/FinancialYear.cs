﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Utilities
{
    public class AccountingFinancialYear
    {
        public DateTime FinYearStartDate;
        public DateTime FinYearEndDate;
        int yearNumber;
        int firstMonthInYear = 7;

        public static DateTime FirstDayOfMonthFromDateTime(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }

        public static DateTime LastDayOfMonthFromDateTime(DateTime dateTime)
        {
            DateTime firstDayOfTheMonth = new DateTime(dateTime.Year, dateTime.Month, 1);
            return firstDayOfTheMonth.AddMonths(1).AddDays(-1);
        }

        public AccountingFinancialYear(DateTime forDate)
        {
            if (forDate.Month < firstMonthInYear)
            {
                yearNumber = forDate.Year - 2;
            }
            else
            {
                yearNumber = forDate.Year - 1;
            }

            FinYearStartDate = new DateTime(yearNumber, 7, 1);
            FinYearEndDate = new DateTime(yearNumber + 1, 6, 30);
        }

        public AccountingFinancialYear NextFinancialYear()
        {
            AccountingFinancialYear nextAccountingFinancialYear = new AccountingFinancialYear(this.FinYearEndDate.AddYears(2));
            return nextAccountingFinancialYear; 
        }

        public override string ToString()
        {
            return yearNumber.ToString();
        }
    }
}
