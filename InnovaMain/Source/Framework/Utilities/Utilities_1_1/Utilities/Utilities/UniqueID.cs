﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace Oritax.TaxSimp.Utilities
{
    /// <summary>
    /// Enumeration of UniqueID types
    /// </summary>
    public enum EUniqueIDFormat { Numeric = 0, Text, Combined }
    /// <summary>
    /// Return unique generated ID's for file handling
    /// </summary>
    public abstract class UniqueID
    {
        /// <summary>
        /// Generate a generated ID from the date and time
        /// </summary>
        private static String generatedID
        {
            // create a read only unique ID
            get { return String.Format("{0}{1}{2}", DateTime.Now.ToString("dd"), DateTime.Now.Second, DateTime.Now.Millisecond); }
        }

        /// <summary>
        /// Get a unique ID for use with automated file handling
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        public static String ReturnUniqueID(EUniqueIDFormat format)
        {
            // return value
            String modifiedID = "";
            // create a return value
            String capturedID = UniqueID.generatedID;

            // select which format the ID should take
            switch (format)
            {
                // text only
                case (EUniqueIDFormat.Text):
                    {
                        // work through each character
                        foreach (Char idElement in capturedID)
                        {
                            // expose its int value, + 16 for ASCII offset to captials
                            modifiedID += (Char)((Int32)idElement + 17);
                        }
                        break;
                    }
                // text and numeric
                case (EUniqueIDFormat.Combined):
                    {
                        // work through each character
                        foreach (Char idElement in capturedID)
                        {
                            // if the characters ascii value is even
                            if ((Int32)idElement % 2 == 0)
                            {
                                // expose its int value, + 16 for ASCII offset to captials
                                modifiedID += (Char)((Int32)idElement + 17);
                            }
                            // element must be odd
                            else
                            {
                                // just add the element as its numeric form
                                modifiedID += idElement;
                            }
                        }
                        break;
                    }
                case (EUniqueIDFormat.Numeric):
                    {
                        // just set ID
                        modifiedID = capturedID;
                        break;
                    }
                default:
                    {
                        // throw excetion
                        throw new Exception("Unique format type is invalid");
                    }
            }


            if (modifiedID.Length < 7)
            {
                System.Threading.Thread.Sleep(10);
                modifiedID = ReturnUniqueID(format);
            }
            // retun ID
            return modifiedID;
        }

        public static String Return9UniqueID()
        {
            System.Threading.Thread.Sleep(100);
            return String.Format("{0:d9}", (DateTime.Now.Ticks / 10) % 1000000000);
        }

        public static string Get9UniqueKey()
        {
            int maxSize = 9;
            char[] chars = new char[62];
            string a;
            a = "ABCDEFGHJKMNOPQRSTUVWXYZ1234567890";
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - 1)]);
            }
            return result.ToString();
        }
    }

}
