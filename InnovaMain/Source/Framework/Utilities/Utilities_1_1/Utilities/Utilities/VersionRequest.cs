using System;

namespace Oritax.TaxSimp.Utilities
{
	public enum VersionRequest
	{
		SameVersion,
		CompatibleVersion,
		LatestVersion,
	}
}
