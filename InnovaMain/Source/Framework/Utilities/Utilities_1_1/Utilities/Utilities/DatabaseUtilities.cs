using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Text;
using System.Data;

namespace Oritax.TaxSimp.Utilities
{
    public class DatabaseUtilities
    {
        #region Public Methods

        public static void ExecuteSqlResource(Type type, string resourceName, string connectionString)
        {
            Assembly asm = Assembly.GetAssembly(type);
            Stream str = asm.GetManifestResourceStream(resourceName);
            StreamReader reader = new StreamReader(str);
            ExecuteSqlStream(reader, connectionString);
        }

        public static void ExecuteSqlResource(Type type, string resourceName, SqlConnection connection, SqlTransaction transaction)
        {
            Assembly asm = Assembly.GetAssembly(type);
            Stream str = asm.GetManifestResourceStream(resourceName);
            StreamReader reader = new StreamReader(str);

            ExecuteSqlStream(reader, connection, transaction);
        }

        public static void ExecuteNonQueryStoredProcedure(string storedProcedureName, List<SqlParameter> storedProcedureParameters)
        {
            string connectionString = string.Format("{0}", DBConnection.Connection.ConnectionString);
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(storedProcedureName, sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter parameter in storedProcedureParameters)
                        sqlCommand.Parameters.Add(parameter);
                    sqlCommand.Connection.Open();
                    int count = sqlCommand.ExecuteNonQuery();
                }
            }
        }

        public static void ExecuteStoredProcedure(DataSet dataSet, string storedProcedureName, Dictionary<string, string> tableNamePairs, List<SqlParameter> storedProcedureParameters)
        {
            using (SqlConnection sqlConnection = new SqlConnection(DBConnection.Connection.ConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(storedProcedureName, sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter parameter in storedProcedureParameters)
                        sqlCommand.Parameters.Add(parameter);
                    using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand))
                    {
                        foreach (KeyValuePair<string, string> tableNamePair in tableNamePairs)
                            sqlDataAdapter.TableMappings.Add(tableNamePair.Key, tableNamePair.Value);
                        sqlDataAdapter.Fill(dataSet);
                    }
                }
            }
        }

        public static void ExecuteStoredProcedure(DataSet dataSet, string storedProcedureName, string tableName, List<SqlParameter> storedProcedureParameters)
        {

            using (SqlConnection sqlConnection = new SqlConnection(DBConnection.Connection.ConnectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(storedProcedureName, sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter parameter in storedProcedureParameters)
                        sqlCommand.Parameters.Add(parameter);
                    using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand))
                    {
                        sqlDataAdapter.Fill(dataSet, tableName);
                    }
                }
            }
        }

        public static void AddParameterToList(List<SqlParameter> parameters, string parameterName, SqlDbType dbType, object parameterValue)
        {
            SqlParameter parameter = new SqlParameter(parameterName, dbType);
            parameter.Value = parameterValue;
            parameters.Add(parameter);
        }

        public static void AddParameterToList(List<SqlParameter> parameters, String parameterName, SqlDbType dbType, int parameterSize, object parameterValue)
        {
            SqlParameter parameter = new SqlParameter(parameterName, dbType);
            parameter.Size = parameterSize;
            parameter.Value = parameterValue;
            parameters.Add(parameter);
        }

        #endregion // Public Methods

        #region Private Methods

        /// <summary>
        /// Parse the stream extracting 
        /// </summary>
        /// <param name="sqlStream"></param>
        /// <param name="connectionString"></param>
        private static void ExecuteSqlStream(System.IO.StreamReader sqlStream, string connectionString)
        {
            // Parses the stream looking for GO markers which 
            // delimit blocks of SQL.
            string line;
            StringBuilder currentBlock = new StringBuilder();
            int overallLine = 0;
            int blockLine = 0;
            while ((line = sqlStream.ReadLine()) != null)
            {
                overallLine++;
                blockLine++;
                if (line == "GO")
                {
                    // GO marker found, Action the script block
                    // up to the GO marker
                    ExecuteSqlScript(currentBlock.ToString(), connectionString);

                    // Reset the script block
                    currentBlock = new StringBuilder();
                    blockLine = 0;
                }
                else
                {
                    // To assist debugging output the line of the stream (overallLine)
                    // and the line within the block (blockLine). SQL Server will report
                    // errors relative to the start of a block.
                    currentBlock.Append(line);
                    currentBlock.Append(Environment.NewLine);
                }
            }

            // Send the last block to be processed.
            ExecuteSqlScript(currentBlock.ToString(), connectionString);
        }

        /// <summary>
        /// Parse the stream extracting 
        /// </summary>
        /// <param name="sqlStream"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        private static void ExecuteSqlStream(System.IO.StreamReader sqlStream, SqlConnection connection, SqlTransaction transaction)
        {
            // Parses the stream looking for GO markers which 
            // delimit blocks of SQL.
            string line;
            StringBuilder currentBlock = new StringBuilder();
            int overallLine = 0;
            int blockLine = 0;
            while ((line = sqlStream.ReadLine()) != null)
            {
                overallLine++;
                blockLine++;
                if (line == "GO")
                {
                    // GO marker found, Action the script block
                    // up to the GO marker
                    ExecuteSqlScript(currentBlock.ToString(), connection, transaction);

                    // Reset the script block
                    currentBlock = new StringBuilder();
                    blockLine = 0;
                }
                else
                {
                    // To assist debugging output the line of the stream (overallLine)
                    // and the line within the block (blockLine). SQL Server will report
                    // errors relative to the start of a block.
                    currentBlock.Append(line);
                    currentBlock.Append(Environment.NewLine);
                }
            }

            // Send the last block to be processed.
            ExecuteSqlScript(currentBlock.ToString(), connection, transaction);
        }

        private static void ExecuteSqlScript(string script, string connectionString)
        {
            if (script == String.Empty)
                return;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                string scriptsCommandText = script;
                using (SqlCommand command = DBCommandFactory.Instance.NewSqlCommand(scriptsCommandText, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        private static void ExecuteSqlScript(string sqlQuery, SqlConnection connection, SqlTransaction transaction)
        {
            if (sqlQuery == String.Empty)
                return;

            string scriptsCommandText = sqlQuery;
            SqlCommand command = DBCommandFactory.Instance.NewSqlCommand(scriptsCommandText, connection);
            command.Transaction = transaction;
            command.ExecuteNonQuery();
        }

        #endregion // Private Methods
    }
}
