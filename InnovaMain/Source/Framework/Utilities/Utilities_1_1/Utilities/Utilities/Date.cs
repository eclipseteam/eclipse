using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Threading;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// Summary description for Date.
	/// </summary>
	[Serializable]
	public class Date : IComparable, ISerializable
	{
		DateTime theValue;

		public Date()
		{
			theValue=new DateTime(2000,1,1);
		}

		public Date( Date objDate )
		{
			this.theValue = objDate.theValue;
		}

		public Date( DateTime objDate )
		{
			this.theValue = objDate;
		}

		public Date(string dateString)
		{
			try
			{
				theValue=DateTime.Parse(dateString);
			}
			catch
			{
				theValue=new DateTime(2000,1,1);
			}
		}

		protected Date(SerializationInfo si, StreamingContext context)
		{
//			long ticks = (long)Serialize.GetSerializedValue(si,"dt_theValue",typeof(System.Int64),null);
//			theValue = new DateTime(ticks);
			theValue = (DateTime)Serialize.GetSerializedValue(si,"dt_theValue",typeof(System.DateTime),null);
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			Serialize.AddSerializedValue(si,"dt_theValue",theValue);
		}

		public DateTime DateTime
		{
			get
			{
				return this.theValue;
			}
		}

		public override string ToString()
		{
			//return theValue.ToShortDateString();
			string strShortDatePattern = "dd/MM/yyyy";
			return theValue.ToString( strShortDatePattern );
		}

		public void FromString(string dateString)
		{
				theValue=DateTime.Parse(dateString);
			}

		public static bool operator == (Date date1, Date date2)
		{
			object objDate1 = (object)date1;
			object objDate2 = (object)date2;

			if ( objDate1 == null && objDate2 == null )
				return true;
			else if ( objDate1 != null && objDate2 == null )
				return false;
			else if ( objDate2 == null && objDate2 != null )
				return false;
			else
			{
				return date1.theValue==date2.theValue;
			}
		}

		public static bool operator != (Date date1, Date date2)
		{
			if( date1 == null && date2 == null )
				return( false );
			else if( date2 == null )
				return( true );
			else if( date1 == null )
				return( true );
			else
				return date1.theValue!=date2.theValue;
		}

		public static bool operator > ( Date objDate1, Date objDate2 )
		{
			return objDate1.theValue > objDate2.theValue;
		}

		public static bool operator < ( Date objDate1, Date objDate2 )
		{
			return objDate1.theValue < objDate2.theValue;
		}

		public static bool operator >= ( Date objDate1, Date objDate2 )
		{
			return objDate1.theValue >= objDate2.theValue;
		}

		public static bool operator <= ( Date objDate1, Date objDate2 )
		{
			return objDate1.theValue <= objDate2.theValue;
		}

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public int CompareTo( object objDate )
		{
			if ( objDate != null )
			{
				Date objRealDate = (Date) objDate;
				return this.theValue.CompareTo( objRealDate.DateTime );
			}
			else
				// sort nulls before everything else
				return -1;
		}
	}

   
}
