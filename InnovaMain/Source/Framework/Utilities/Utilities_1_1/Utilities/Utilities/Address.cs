﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Utilities
{
    public class Address
    {
        public static string GetStateCode(string stateFullName)
        {
            stateFullName = stateFullName.Trim();
            if (stateFullName == "Australian Capital Territory")
                return "ACT";
            else if (stateFullName == "Northern Territory")
                return "NT";
            else if (stateFullName == "Queensland")
                return "QLD";
            else if (stateFullName == "New South Wales")
                return "NSW";
            else if (stateFullName == "Tasmania")
                return "TAS";
            else if (stateFullName == "Victoria")
                return "VIC";
            else if (stateFullName == "Western Australia")
                return "WA";
            else if (stateFullName == "South Australia")
                return "SA";

            return stateFullName;
        }

    }
}
