using System;
using System.Collections;
using System.Configuration;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// An N-Able performance counter factory
	/// </summary>
	public static class NablePerformanceCounters
	{
		private static Hashtable performanceCounters;
		private static bool enabled;
        private static string instanceName;

		static NablePerformanceCounters()
		{
			performanceCounters = new Hashtable();

			try
			{
                instanceName = GetInstanceName();
                enabled = bool.Parse(ConfigurationManager.AppSettings["PerformanceCountersEnabled"]);
			}
			catch
			{
				enabled = false;
			}
		}

		/// <summary>
		/// Get a counter from the factory, if the counter is not loaded, loads a new counter
		/// </summary>
		/// <param name="counterName">The name of the counter to return</param>
		/// <returns>The counter requested</returns>
		public static NablePerformanceCounter GetCounter(string counterName)
		{
			if(!performanceCounters.Contains(counterName))
				CreateCounter(counterName);

			return (NablePerformanceCounter) performanceCounters[counterName];
		}

		private static void CreateCounter(string counterName)
		{
			// make multi-threaded safe so that it can be called outside of a
			// transaction if necessary
			lock(performanceCounters)
			{
				// check that a different thread hasn't added the counter already
				if(!performanceCounters.Contains(counterName))
				{
					NablePerformanceCounter counter = new NablePerformanceCounter(counterName, enabled, instanceName);
					performanceCounters.Add(counterName,counter);
				}
			}
		}

        private static string GetInstanceName()
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;          
            string[] directoryParts = baseDirectory.Split('\\');

            System.Text.StringBuilder instanceName = new System.Text.StringBuilder();

            if (directoryParts[directoryParts.Length - 1] != "\\")
                instanceName.Append(directoryParts[directoryParts.Length - 1]);
            else
                instanceName.Append(directoryParts[directoryParts.Length - 2]);

            // Remove any counter instance name invalid characters
            instanceName = instanceName.Replace("(", "");
            instanceName = instanceName.Replace(")", "");
            instanceName = instanceName.Replace("#", "");
            instanceName = instanceName.Replace("/", "");
            instanceName = instanceName.Replace("\\", "");

            return instanceName.ToString();
        }
	}
}
