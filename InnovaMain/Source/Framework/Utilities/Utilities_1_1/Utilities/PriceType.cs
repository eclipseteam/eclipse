﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Oritax.TaxSimp
{
    [Serializable]
    public enum PriceType
    {
        [Description("UNIT PRICE")]
        UnitPrice,
        [Description("PUR PRICE")]
        PurchasePrice,
        [Description("NAV PRICE")]
        NavPrice
    }
}
