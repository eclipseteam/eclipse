﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public enum AcountingMethodType
    {
        [Description("Accrual")]
        Accrual,
        [Description("Cash")]
        Cash,
        [Description("Mixed")]
        Mixed,
        [Description("None")]
        None
    }
}
