﻿using System;
using System.Collections.Generic;

namespace Oritax.TaxSimp.Data
{
    public interface IHaveAssociation
    {
        OrganizationChart GetAssociation(IIdentityCM identitycm, OrganizationType organizationtype);
        OrganizationChart GetEntityParent(OrganizationChart organizationchart);
        OrganizationChart GetEntityChild();
        OrganizationChart GetEntityChildWithEntity();
    }
}
