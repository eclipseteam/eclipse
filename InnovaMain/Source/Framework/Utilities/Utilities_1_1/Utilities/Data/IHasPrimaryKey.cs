﻿using System;

namespace Oritax.TaxSimp.Data
{
    public interface IHasPrimaryKey
    {
        Guid PrimaryKey { get; set; }
    }
}
