﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class TaskEntity
    {
        public Guid TaskID { get; set; }
        public Guid ModelID { get; set; }
        public Guid AssetID { get; set; }
        public string TaskDescription { get; set; }
        public string Status { get; set; }
        public bool IsCompleted { get; set; }
        public string ServiceType { get; set; }
        public OrganizationType LinkedEntityType { get; set; }
        public IdentityCM LinkedEntity { get; set; }
        public bool IsExempted { get; set; }
        public string OldStatus { get; set; }
        public TaskEntity()
        {
            LinkedEntity = new IdentityCM();
        }
    }

    public class PresentationTaskEntity  
    {

        #region Private Fields

        private string _Status;
        private bool _IsCompleted;
        private string _serviceType;
        private IdentityCM _linkedEntity;
        private OrganizationType _linkedEntityType;
        private bool _IsExempted;
        private string _OldStatus;

        #endregion Private Fields

        #region Public Properties
        public ServiceTypes Type { get; set; }

        public Guid TaskID { get; set; }
        public Guid ModelID { get; set; }
        public Guid AssetID { get; set; }
        public string TaskDescription { get; set; }

        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
            }
        }

        public string AssetName { set; get; }
        public string AssetDesc { get; set; }
        public string ProductName { set; get; }

        public bool IsCompleted
        {
            get { return _IsCompleted; }
            set
            {
                _IsCompleted = value;
            }
        }

        public string ServiceType
        {
            get { return _serviceType; }
            set
            {
                _serviceType = value;
            }
        }

        public OrganizationType LinkedEntityType
        {
            get { return _linkedEntityType; }
            set
            {
                _linkedEntityType = value;
            }
        }

        public IdentityCM LinkedEntity
        {
            get { return _linkedEntity; }
            set
            {
                _linkedEntity = value;
            }
        }

        public bool IsExempted
        {
            get { return _IsExempted; }
            set
            {
                _IsExempted = value;
            }
        }

        public string OldStatus
        {
            get { return _OldStatus; }
            set
            {
                _OldStatus = value;
            }
        }

        #endregion Public Properties

        #region Constructors

        public PresentationTaskEntity()
        {
            LinkedEntity = new IdentityCM();
        }

        #endregion Constructors

        #region INotifyPropertyChanged Members

      
        #endregion
    }

    [Serializable]
    public class AccountProcessTaskEntity
    {
        public Guid TaskID { get; set; }
        public Guid ModelID { get; set; }
        public Guid AssetID { get; set; }
        public bool IsCompleted { get; set; }
        public OrganizationType LinkedEntityType { get; set; }
        public IdentityCM LinkedEntity { get; set; }
        public bool IsExempted { get; set; }
        public string OldStatus { get; set; }

        public AccountProcessTaskEntity()
        {
            LinkedEntity = new IdentityCM();
            TaskID = new Guid();
            ModelID = new Guid();
            AssetID = new Guid();
        }
    }
}
