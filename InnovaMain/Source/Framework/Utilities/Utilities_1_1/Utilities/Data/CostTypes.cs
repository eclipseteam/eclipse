﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public enum CostTypes
    {
        [Description("Last In First Out")]
        LIFO,
        [Description("First In First Out")]
        FIFO,
        [Description("Avergae")]
        AVG,
        [Description("None")]
        None
    }
}
