﻿using System;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class AtCallInstitutePriceEntity
    {
        
        public Guid ID { get; set; }
        public Guid InstituteID { get; set; }
        public string AccountTier { get; set; }
        public decimal? Min { get; set; }
        public decimal? Max { get; set; }
        public decimal? Rate { get; set; }//transition property, it will become obsolete
        
        public string Rate2 = "";
        public decimal? MaximumBrokerage { get; set; }
        public DateTime ApplicableFrom { get; set; }
        public DateTime Date { get; set; }
        public bool IsEditable { get; set; }
        public string Type { get; set; }  // Manual/Import
        public string Applicability { get; set; }  //Individual, Company, Trust, Partnership, Superfund
        public int? ImportRateID { get; set; }
        public decimal? HoneymoonPeriod { get; set; }
        public AtCallInstitutePriceEntity()
        {
            ID = Guid.NewGuid();
            InstituteID = Guid.Empty;
            Type = string.Empty;
            Date = DateTime.Now;
            ApplicableFrom = DateTime.Now;
            IsEditable = false;
            if (Rate != null)
            {
                Rate2 = Rate.ToString();
                Rate = null;
            }
            else if (Rate2 == null)
                Rate2 = "";
        }

    }
}
