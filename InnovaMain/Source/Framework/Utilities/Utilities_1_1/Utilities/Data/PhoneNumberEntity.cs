﻿using System;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class PhoneNumberEntity
    {
        public string CountryCode { get; set; }
        public string CityCode { get; set; }
        public string PhoneNumber { get; set; }
        public PhoneNumberEntity()
        {
            CountryCode = string.Empty;
            CityCode = string.Empty;
            PhoneNumber = string.Empty;
        }

        public static PhoneNumberEntity PhoneNumberConvertor(string phNumber)
        {
            PhoneNumberEntity phoneNumberEntity = new PhoneNumberEntity();
            phoneNumberEntity.CountryCode = "61";
            phNumber = phNumber.RemoveWhiteSpacesStartEndInBetween();

            if (phNumber.Length >= 9 && !phNumber.StartsWith("04"))
            {
                string cityCode = phNumber.Substring(0, 2);
                string phoneNumber = phNumber.Substring(2);

                phoneNumberEntity.CityCode = cityCode;
                phoneNumberEntity.PhoneNumber = phoneNumber;
            }
            else
                phoneNumberEntity.PhoneNumber = phNumber;

            return phoneNumberEntity;
        }

        public static MobileNumberEntity MobileNumberConvertor(string phNumber)
        {
            MobileNumberEntity mobileEntity = new MobileNumberEntity();
            mobileEntity.CountryCode = "61";
            phNumber = phNumber.RemoveWhiteSpacesStartEndInBetween();

            mobileEntity.MobileNumber = phNumber;
            return mobileEntity;
        }

        public static void FixPhoneNumbersAndMobile(PhoneNumberEntity phoneNumberEntity, MobileNumberEntity mobile)
        {
            phoneNumberEntity.CountryCode = "61";
            mobile.CountryCode = "61";
            phoneNumberEntity.PhoneNumber = phoneNumberEntity.PhoneNumber.RemoveWhiteSpacesStartEndInBetween();

            if (phoneNumberEntity.PhoneNumber.Length >= 9 )
            {
                if (phoneNumberEntity.PhoneNumber.StartsWith("04"))
                {
                    mobile.MobileNumber = phoneNumberEntity.PhoneNumber;
                    phoneNumberEntity.PhoneNumber = string.Empty;
                    phoneNumberEntity.CountryCode = string.Empty;
                    phoneNumberEntity.CityCode = string.Empty;
                }
                else
                {
                    string cityCode = phoneNumberEntity.PhoneNumber.Substring(0, 2);
                    string phoneNumber = phoneNumberEntity.PhoneNumber.Substring(2);

                    phoneNumberEntity.CityCode = cityCode;
                    phoneNumberEntity.PhoneNumber = phoneNumber;
                }
                if(mobile.MobileNumber.StartsWith("04"))
                    mobile.MobileNumber = mobile.MobileNumber.RemoveWhiteSpacesStartEndInBetween();
               
            }

            if (mobile.MobileNumber.StartsWith("04"))
                mobile.MobileNumber = mobile.MobileNumber.RemoveWhiteSpacesStartEndInBetween();
        }

        public new string ToString()
        {
            if (string.IsNullOrEmpty(CountryCode) && string.IsNullOrEmpty(CityCode) && string.IsNullOrEmpty(PhoneNumber))
                return string.Empty;
            return string.Format("(+{0})-{1}-{2}", CountryCode, CityCode, PhoneNumber);
        }

        public bool IsEmpty()
        {

            return string.IsNullOrEmpty(CountryCode) && string.IsNullOrEmpty(CityCode) && string.IsNullOrEmpty(PhoneNumber);
        }
    }
}
