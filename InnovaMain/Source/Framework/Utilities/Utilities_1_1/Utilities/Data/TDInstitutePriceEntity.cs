﻿using System;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    // ReSharper disable InconsistentNaming
    public class TDInstitutePriceEntity
    // ReSharper restore InconsistentNaming
    {
       
        // ReSharper disable InconsistentNaming
        public Guid ID { get; set; }
        // ReSharper restore InconsistentNaming
        // ReSharper disable InconsistentNaming
        public Guid InstituteID { get; set; }
        // ReSharper restore InconsistentNaming

        public decimal? Min { get; set; }
        public decimal? Max { get; set; }
        public decimal? MaximumBrokeage { get; set; }
        public decimal? Days30 { get; set; }
        public decimal? Days60 { get; set; }
        public decimal? Days90 { get; set; }
        public decimal? Days120 { get; set; }
        public decimal? Days150 { get; set; }
        public decimal? Days180 { get; set; }
        public decimal? Days270 { get; set; }
        public decimal? Years1 { get; set; }
        public decimal? Years2 { get; set; }
        public decimal? Years3 { get; set; }
        public decimal? Years4 { get; set; }
        public decimal? Years5 { get; set; }
        public string Status { get; set; }
        public DateTime Date { get; set; }
        public decimal? Ongoing { get; set; }

        public int ImportRateID { get; set; }
        public int ProviderID { get; set; }

        public ClientManagementType RateEffectiveFor { get; set; }

        public TDInstitutePriceEntity()
        {
            ID = Guid.NewGuid();
            InstituteID = Guid.Empty;

            Status = string.Empty;
            Date = DateTime.Now;
        }

    }
   
}
