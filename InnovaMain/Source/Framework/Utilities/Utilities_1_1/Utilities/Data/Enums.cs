﻿using System;
using System.ComponentModel;

namespace Oritax.TaxSimp.Data
{
    public enum DatasetCommandTypes { Get, Details, Update, Add, Delete, Process, Check, Validate, Approve, Cancel, Settle, Unsettle, GetChildren, GetStatus, ProcessAll, UpdateAdviser, RemoveAdviser, GetAll, RemoveInvalid, UpdatedSMAProperties, GetMax, SendOrderToSMA, GetClients, Complete, Incomplete, SubmitToDesktopBroker }
    public enum OperationResults { Successfull, Failed }
    public enum ClientDataSetOperationCommandTypes
    {
        None,
        PhoneFix
    }

    [Serializable]
    public enum ReinvestmentOption
    {
        Reinvest = 0,
        Distribute = 10,
    }

    [Serializable]
    public enum ClientManagementType
    {
        UMA = 0,
        SMA = 10,
    }
    [Serializable]
    public enum SMACallStatus
    {
        NotSent = 0,
        Failed = 10,
        Successful = 15,
        NA = 20,
    }
     [Serializable]
    public enum AddressPart
    {
        Addressline1,
        Addressline2,
        Suburb,
        State,
        PostCode,
        Country
    }

    public enum NonIndividualsType
    {
        Company=0,
        Trust=5,
        Partnership=10,
        Fund=15
    }

    public enum SuperAccountType
    {
        Super = 5,
        Pension = 10,
    }

    public enum AlertExecutionMode
    {
        [Description("Before")]
        Before = 0,
        [Description("On Due Date")]
        OnDueDate = 1,
        [Description("After")]
        After = 2
    }

    public enum AlertDurationType
    {
        [Description("Day")]
        Day = 0,
        [Description("Week")]
        Week = 1,
        [Description("Month")]
        Month = 2
    }

    public enum SortOrder
    {
        [Description("Ascending")]
        Ascending = 0,
        [Description("Descending")]
        Descending = 1
    }
}

