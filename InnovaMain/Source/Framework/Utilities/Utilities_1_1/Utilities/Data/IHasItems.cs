﻿using System.Collections.Generic;

namespace Oritax.TaxSimp.Data
{
    public interface IHasItems<T> 
    {
        List<T> Items { get; set; }
    }
}
