﻿using System;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class FormMap : IXElementable
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string MapType { get; set; }
        public string ClientType { get; set; }
        public string DefaultFileName { get; set; }
        public SerializableDictionary<string, string> Mapping { get; set; }

        public void ToXElement(XElement root)
        {
            root.Add(new XAttribute("ID", ID));
            root.Add(new XAttribute("Name", Name));
            root.Add(new XAttribute("MapType", MapType));
            root.Add(new XAttribute("ClientType", ClientType));
            root.Add(new XAttribute("DefaultFileName", DefaultFileName));

            XElement map = new XElement("Mapping");
            map.Add(Mapping.Keys.Select(k =>
            {
                XElement item = new XElement("Item");
                item.Add(new XAttribute("Key", k));
                item.Add(new XAttribute("Value", Mapping[k]));
                return item;
            }));

            root.Add(map);
        }

        public void FromXElement(XElement root)
        {
            ID = new Guid(root.Attribute("ID").Value);
            Name = root.Attribute("Name").Value;
            MapType = root.Attribute("MapType").Value;
            ClientType = root.Attribute("ClientType").Value;
            DefaultFileName = root.Attribute("DefaultFileName").Value;
            Mapping = new SerializableDictionary<string, string>();
            foreach (XElement each in root.XPathSelectElements("Mapping/Item"))
            {
                Mapping.Add(each.Attribute("Key").Value, each.Attribute("Value").Value);
            }
        }
    }
}
