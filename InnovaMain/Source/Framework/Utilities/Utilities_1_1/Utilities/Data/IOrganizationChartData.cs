﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Oritax.TaxSimp.Data
{
    public interface IOrganizationChartData
    {
        OrganizationChart GetOrganizationChart();
    }
}
