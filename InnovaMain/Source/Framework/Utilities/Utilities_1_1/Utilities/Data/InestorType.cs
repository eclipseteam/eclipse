﻿using System;
using System.ComponentModel;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public enum InvestorType
    {
        [Description("None")]
        None = 0,
        [Description("Wholesale Investor")]
        WholesaleInvestor = 0,
        [Description("Sophisticated Investor")]
        SophisticatedInvestor = 1,
        [Description("Professional Investor")]
        ProfessionalInvestor = 2,
        [Description("Retail Investor")]
        RetailInvestor = 3,
    }
}
