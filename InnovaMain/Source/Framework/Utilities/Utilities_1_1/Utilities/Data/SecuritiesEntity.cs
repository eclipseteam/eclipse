﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class SecuritiesEntity
    {
        #region Properties
        public Guid ID { get; set; }
        public string AsxCode { get; set; }
        public string CompanyName { get; set; }
        public ObservableCollection<ASXSecurityEntity> ASXSecurity { get; set; }

        public string InvestmentType
        {
            get;
            set;
        }

        public string Market
        {
            get;
            set;
        }


        public string Rating
        {
            get;
            set;
        }

        public bool IsPrefferedInvestment
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        public string ISINCode
        {
            get;
            set;
        }

        public double UnitPriceBaseCurrency
        {
            get;
            set;
        }

        public double UnitSize
        {
            get;
            set;
        }

        public double MinSecurityBal
        {
            get;
            set;
        }


        public double MinTradedLotSize
        {
            get;
            set;
        }

        public bool IgnoreMinSecurityBal
        {
            get;
            set;
        }

        public bool IgnoreMinTradedLotSize
        {
            get;
            set;
        }

        public DateTime? IssueDate
        {
            get;
            set;
        }

        public double PriceAsAtDate
        {
            get;
            set;
        }

        public Guid AssetID
        {
            get;
            set;
        }

        public Guid FundID
        {
            get;
            set;
        }
        public ObservableCollection<DividendEntity> DividendCollection { get; set; }
        public ObservableCollection<DistributionEntity> DistributionCollection { get; set; }

        [OptionalField]
        public int PricingSource = 0;
        [OptionalField]
        public int DistributionType = 0;
        [OptionalField]
        public int DistributionFrequency = 0;

        public bool IsSMAApproved
        {
            get;
            set;
        }

        public double SMAHoldingLimit
        {
            get;
            set;
        }
        #endregion

        #region Constructor
        public SecuritiesEntity()
        {
            AsxCode = string.Empty;
            CompanyName = string.Empty;
            ASXSecurity = new ObservableCollection<ASXSecurityEntity>();

            InvestmentType = string.Empty;
            Market = string.Empty;
            Rating = string.Empty;
            IsPrefferedInvestment = false;
            Status = string.Empty;
            ISINCode = string.Empty;
            UnitPriceBaseCurrency = 0;
            UnitSize = 0;
            MinSecurityBal = 0;
            IgnoreMinSecurityBal = false;
            MinTradedLotSize = 0;
            IgnoreMinTradedLotSize = false;
            IssueDate = null;
            PriceAsAtDate = 0;
            AssetID = Guid.Empty;
            FundID = Guid.Empty;
            DividendCollection = new ObservableCollection<DividendEntity>();
            DistributionCollection = new ObservableCollection<DistributionEntity>();

        }
        #endregion
    }

    public enum SecurityPricingSource
    {
        ASX = 0,
        BT,
        Manual,
        NZX,
        SSAL
    }
    public enum SecurityDistributionsType
    {
        Unknown = 0,
        Paid,
        Reinvested,
        DRP,
    }
    public enum SecurityDistributionsFrequency
    {
        Unknown = 0,
        Monthly,
        Quaterly,
        HalfYearly,
        Annualy
    }
}
