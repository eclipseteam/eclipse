﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class ManualAssetEntity
    {
        #region Properties
        public Guid ID { get; set; }
        public string AsxCode { get; set; }
        public string CompanyName { get; set; }
        public ObservableCollection<ManualAssetASXSecurityEntity> ASXSecurity { get; set; }

        public string InvestmentType
        {
            get;
            set;
        }

        public string Market
        {
            get;
            set;
        }


        public string Rating
        {
            get;
            set;
        }

        public bool IsPrefferedInvestment
        {
            get;
            set;
        }

        public bool isUnitised
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        public double UnitPriceBaseCurrency
        {
            get;
            set;
        }

        public double UnitSize
        {
            get;
            set;
        }

        public double MinSecurityBal
        {
            get;
            set;
        }


        public double MinTradedLotSize
        {
            get;
            set;
        }

        public bool IgnoreMinSecurityBal
        {
            get;
            set;
        }

        public bool IgnoreMinTradedLotSize
        {
            get;
            set;
        }

        public DateTime? IssueDate
        {
            get;
            set;
        }

        public double PriceAsAtDate
        {
            get;
            set;
        }

        public string AssetID
        {
            get;
            set;
        }
        #endregion

        #region Constructor
        public ManualAssetEntity()
        {
            AsxCode = string.Empty;
            CompanyName = string.Empty;
            ASXSecurity = new ObservableCollection<ManualAssetASXSecurityEntity>();
            InvestmentType = string.Empty;
            Market = string.Empty;
            Rating = string.Empty;
            IsPrefferedInvestment = false;
            Status = string.Empty;
            UnitPriceBaseCurrency = 0;
            UnitSize = 0;
            MinSecurityBal = 0;
            IgnoreMinSecurityBal = false;
            MinTradedLotSize = 0;
            IgnoreMinTradedLotSize = false;
            IssueDate = null;
            PriceAsAtDate = 0;
            AssetID = string.Empty;
            isUnitised = false;

        }
        #endregion
    }


}
