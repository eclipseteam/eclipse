﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class AccountRelation
    {
        public Guid Csid { get; set; }
        public Guid Clid { get; set; }
        public string ServiceType { get; set; }
        public string AccountType { get; set; }
        public string AccountName { get; set; }

        public AccountRelation() { }

        public AccountRelation(string clid, string csid)
        {
            Clid = new Guid(clid);
            Csid = new Guid(csid);
        }

        public void Replace(IIdentityCM identity)
        {
            Clid = new Guid(identity.Clid.ToString());
            Csid = new Guid(identity.Csid.ToString());
        }

        public override string ToString()
        {
            return string.Format("Clid:{0}, Csid:{1}", Clid, Csid);
        }
        public static bool operator ==(AccountRelation cm1, AccountRelation cm2)
        {
            object o1 = (object)cm1;
            object o2 = (object)cm2;
            if (o1 == null && o2 == null) return true;
            if (o1 == null || o2 == null) return false;
            return ((cm1.Clid == cm2.Clid) && (cm1.Csid == cm2.Csid));
        }
        public static bool operator !=(AccountRelation cm1, AccountRelation cm2)
        {
            return !(cm1 == cm2);
        }

        public override bool Equals(Object obj)
        {
            return Equals(obj as AccountRelation);
        }

        public bool Equals(AccountRelation s)
        {
            if ((object)s == null) { return false; }
            return this == s;
        }

        public override int GetHashCode()
        {
            int result = Clid.GetHashCode();
            result = 29 * result + Csid.GetHashCode();
            return result;
        }

        public static AccountRelation Null = new AccountRelation { Clid = Guid.Empty, Csid = Guid.Empty };
    }
}
