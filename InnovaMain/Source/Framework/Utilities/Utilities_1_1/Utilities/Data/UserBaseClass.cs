﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class UserBaseClass
    {
        [XmlElement("CurrentUserName")]
        public String CurrentUserName { get; set; }
    }

    [Serializable]
    public class CIDBase : UserBaseClass
    {
        [XmlElement("CID")]
        public String CID { get; set; }
    }

    [Serializable]
    public class UserEntity : UserBaseClass
    {
        [XmlElement("CID")]
        public Guid CID { get; set; }
        [XmlElement("IsAdmin")]
        public bool IsAdmin { get; set; }
        //To handle both SL and New Website when adding user
        [XmlElement("IsWebUser")]
        public bool IsWebUser { get; set; }
        [XmlElement("UserType")]
        public  int UserType { get; set; }
        [XmlElement("Email")]
        public string Email { get; set; }
    }

}
