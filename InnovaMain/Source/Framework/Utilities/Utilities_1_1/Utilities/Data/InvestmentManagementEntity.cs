﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Oritax.TaxSimp.DataSets;
using System.Data;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class InvestmentManagementEntity : ICloneable
    {
        #region Public Properties

        public Guid ID =Guid.Empty;
        public string Name = "";
        public string Description ="";
        public string InvestmentManager = "Innova Asset Management P/L";
        public string PortfoloManager = "";
        public string AssetType = "";
        public string Region = "";
        public string Objective = "";
        public string FundSize = "";   
        public string MinWithrawal = "";
        public string MinAddition = "";
        public string EntryFee = "";
        public string ExitFee = "";
        public string BuySellSpread = "";
        public string ManagementFee = "";
        public string ICR = "";
        public string Info = "";


        [OptionalField]
        public string MinInvestment = "";
        [OptionalField]
        public string InceptionDate = "";
        [OptionalField]
        public string ManagementFeeAsOf = "";
        [OptionalField]
        public string InvestmentTimeRecommended = "";

        [OptionalField]
        public Guid Benchmark = Guid.Empty;

        [OptionalField]
        public List<Allocation> ManagersAllocation = new List<Allocation>();
        #endregion


        public InvestmentManagementEntity()
        {
            ID = Guid.NewGuid();
        }

        object ICloneable.Clone()
        {
            return this.Clone();
        }

        public ProductEntity Clone()
        {
            return (ProductEntity)this.MemberwiseClone();
        }

        #region Public Metods
            public static void PopulateInvestmentManagementFromDataRow(InvestmentManagementEntity entity, DataRow row)
            {
                entity.InvestmentManager = row[InvestmentDS.INVESTMENTMANAGER].ToString();
                entity.PortfoloManager = row[InvestmentDS.PORTFOLOMANAGER].ToString();
                entity.AssetType = row[InvestmentDS.ASSETTYPE].ToString();
                entity.Region = row[InvestmentDS.REGION].ToString();

                entity.Objective = row[InvestmentDS.OBJECTIVE].ToString();
                entity.FundSize = row[InvestmentDS.FINDSIZE].ToString();
                entity.MinWithrawal = row[InvestmentDS.MINWITHRAWAL].ToString();
                entity.Info = row[InvestmentDS.INFO].ToString();

                entity.MinInvestment = row[InvestmentDS.MININVESTMENT].ToString();
                entity.InceptionDate = row[InvestmentDS.INCEPTIONDATE].ToString();
                entity.ManagementFeeAsOf = row[InvestmentDS.MANAGEMENTFEEOF].ToString();
                entity.InvestmentTimeRecommended = row[InvestmentDS.INVESTMENTTIME].ToString();

                entity.MinAddition = row[InvestmentDS.MINADDITION].ToString();
                entity.EntryFee = row[InvestmentDS.ENTRYFEE].ToString();
                entity.ExitFee = row[InvestmentDS.EXITFEE].ToString();
                entity.BuySellSpread = row[InvestmentDS.BUYSELLSPREAD].ToString();
                entity.ManagementFee = row[InvestmentDS.MANAGEMENTFEE].ToString();
                entity.ICR = row[InvestmentDS.ICR].ToString();
                if (!(row[InvestmentDS.BENCHMARK] is DBNull))
                    entity.Benchmark = new Guid(row[InvestmentDS.BENCHMARK].ToString());                   
            }
            public static void ExtractInvestmentManagementData(InvestmentManagementEntity entity, DataRow row)
            {
                row[InvestmentDS.ID] = entity.ID;
                row[InvestmentDS.INVESTMENTMANAGER] = entity.InvestmentManager;
                row[InvestmentDS.PORTFOLOMANAGER] = entity.PortfoloManager;
                row[InvestmentDS.ASSETTYPE] = entity.AssetType;
                row[InvestmentDS.REGION] = entity.Region;

                row[InvestmentDS.INFO] = entity.Info;
                row[InvestmentDS.OBJECTIVE] = entity.Objective;
                row[InvestmentDS.FINDSIZE] = entity.FundSize;
                row[InvestmentDS.MINWITHRAWAL] = entity.MinWithrawal;

                row[InvestmentDS.MININVESTMENT] = entity.MinInvestment;
                row[InvestmentDS.INCEPTIONDATE] = entity.InceptionDate;
                row[InvestmentDS.MANAGEMENTFEEOF] = entity.ManagementFeeAsOf;
                row[InvestmentDS.INVESTMENTTIME] = entity.InvestmentTimeRecommended ;

                row[InvestmentDS.MINADDITION] = entity.MinAddition;
                row[InvestmentDS.ENTRYFEE] = entity.EntryFee;
                row[InvestmentDS.EXITFEE] = entity.ExitFee;
                row[InvestmentDS.BUYSELLSPREAD] = entity.BuySellSpread;
                row[InvestmentDS.MANAGEMENTFEE] = entity.ManagementFee;
                row[InvestmentDS.ICR] = entity.ICR;
                row[InvestmentDS.BENCHMARK] = entity.Benchmark;
            }
            public static void PopulateManagersAllocationFromDataTable(InvestmentManagementEntity entity, DataTable table)
            {
                entity.ManagersAllocation = new List<Allocation>();
                foreach (DataRow row in table.Rows)
                {
                    PopulateManagersAllocationFromDataRow(entity, row);
                }
            }
            public static void PopulateManagersAllocationFromDataRow(InvestmentManagementEntity entity, DataRow row)
            {
                Allocation a = new Allocation();
                a.ID = (Guid)row[InvestmentDS.ID];
                a.PercentageAllocation = (double)row[InvestmentDS.PERCENTAGE];
                entity.ManagersAllocation.Add(a);
            }
            public static void ExtractManagersAllocationToDataTable(InvestmentManagementEntity entity, DataTable table)
            {
                table.Rows.Clear();
                if (entity.ManagersAllocation != null)
                {
                    foreach (Allocation alloc in entity.ManagersAllocation)
                    {
                        DataRow r = table.NewRow();
                        ExtractManagersAllocationData(alloc, r);
                        table.Rows.Add(r);
                    }
                }
            }
            public static void ExtractManagersAllocationData(Allocation alloc, DataRow row)
            {
                row[InvestmentDS.ID] = alloc.ID;
                row[InvestmentDS.PERCENTAGE] = alloc.PercentageAllocation;
            }
        #endregion
    }

    [Serializable]
    public class Allocation
    {
        public Guid ID = Guid.Empty;
        public double PercentageAllocation = 0;

        public Allocation()
        {
        }
    }
    public class AllocationExtention: Allocation
    {
        public string Name = "";

        public AllocationExtention():base()
        {
        }
    }
}
