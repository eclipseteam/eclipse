﻿using System.ComponentModel;

namespace Oritax.TaxSimp.Data
{
    public enum OrganizationType : int
    {
        Corporate = 1,
        Trust = 2,
        ClientIndividual = 3,
        DealerGroup = 4,
        MemberFirm = 5,
        Adviser = 6,
        Individual = 7,
        ClientCorporationPrivate = 8,
        ClientCorporationPublic = 9,
        ClientOtherTrustsCorporate = 10,
        ClientOtherTrustsIndividual = 11,
        ClientSMSFCorporateTrustee = 12,
        ClientSMSFIndividualTrustee = 13,
        [DescriptionAttribute("Bank Account")]
        BankAccount = 14,
        PrincipalPractice = 15,
        IFA = 16,
        ClientSoleTrader = 17,
        [DescriptionAttribute("ASX Broker Account")]
        DesktopBrokerAccount = 18,
        [DescriptionAttribute("Term Deposit Account")]
        TermDepositAccount = 19,
        [DescriptionAttribute("Managed Investment Scheme")]
        ManagedInvestmentSchemesAccount = 20,
        Accountant = 21,
        Period_1_1 = 22,
        Model = 23,
        ConsoleClient = 24,
        ClientEClipseSuper = 25,
        FeeRuns = 26,
        Order = 27,
        SettledUnsettled = 28,
        NonIndividual = 29,
    }
}
