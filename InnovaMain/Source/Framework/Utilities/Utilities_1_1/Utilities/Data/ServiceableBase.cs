﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public abstract class ServiceableHasTatalBase<TList, TItem> : IServiceable<TList, TItem>, IHasItems<TItem>, IHasTotal<TItem>
        where TList : IList<TItem>, IHasTotal<TItem>
        where TItem : IHasPrimaryKey
    {
        [XmlElement("Total")]
        public TItem Total { get; set; }

        [XmlArray("Items"), XmlArrayItem("Item")]
        public List<TItem> Items { get; set; }

        [XmlIgnore]
        public Action<TList> Process { get; set; }

        [XmlIgnore]
        public TList List { get; set; }

        public void GetList()
        {
            Items.Clear();
            foreach (TItem each in List)
            {
                Items.Add(each);
            }
            Total = List.Total;
        }

        public void GetItems()
        {
            List<TItem> selections = new List<TItem>();
            foreach (TItem each in Items)
            {
                TItem selected = List.Find<TItem>(each);
                if (selected != null)
                {
                    selections.Add(selected);
                }
            }
            Items = selections;
            Total = default(TItem);
        }

        public void Add()
        {
            List<TItem> selections = new List<TItem>();
            foreach (TItem each in Items)
            {
                List.Add(each);
            }
            SetTotal();
        }

        public void Update()
        {
            List<TItem> selections = new List<TItem>();
            foreach (TItem each in Items)
            {
                TItem selected = List.Find<TItem>(each);
                if (selected != null)
                {
                    List.Remove(selected);
                    List.Add(each);
                    selections.Add(each);
                }
            }
            Items = selections;
            SetTotal();
        }

        public void Delete()
        {
            List<TItem> selections = new List<TItem>();
            foreach (TItem each in Items)
            {
                TItem selected = List.Find<TItem>(each);
                if (selected != null)
                {
                    List.Remove(selected);
                    selections.Add(selected);
                }
            }
            Items = selections;
            SetTotal();
        }

        private void SetTotal()
        {
            if (Process != null)
            {
                Process(List);
            }
            Total = List.Total;
        }

    }
}
