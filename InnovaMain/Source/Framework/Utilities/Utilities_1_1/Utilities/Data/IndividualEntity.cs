﻿using System;
using System.Data;
using System.Net;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Text;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Data
{
   
    [Serializable]
    public class IndividualEntityCsidClidDetail
    {
        public Guid Csid { get; set; }
        public Guid Clid { get; set; }
        public TitleEntity BusinessTitle { get; set; }
      
        public bool IsPrimary { get; set; }
        public IndividualEntityCsidClidDetail()
        {
            BusinessTitle = new TitleEntity();
            
            IsPrimary = false;
        }

    }

    [Serializable]
    public static class IndividualEntityExtensions
    {

       
        public static ObservableCollection<IdentityCM> ToIdentityCM(this ObservableCollection<OrganizationUnit> list)
        {
            ObservableCollection<IdentityCM> result = new ObservableCollection<IdentityCM>();
            foreach (OrganizationUnit each in list)
            {
                result.Add(new IdentityCM() { Clid = each.Clid, Csid = each.Csid });
            }
            return result;
        }


        public static ObservableCollection<IdentityCMDetail> ToIdentityCMDetail(this ObservableCollection<IndividualEntityCsidClidDetail> list)
        {
            ObservableCollection<IdentityCMDetail> result = new ObservableCollection<IdentityCMDetail>();
            foreach (IndividualEntityCsidClidDetail each in list)
            {
                result.Add(new IdentityCMDetail() { Clid = each.Clid, Csid = each.Csid, BusinessTitle = new TitleEntity() { Title = each.BusinessTitle.Title },IsPrimary =each.IsPrimary });
            }
            return result;
        }
        public static ObservableCollection<IndividualEntityCsidClidDetail> ToIdentityCMDetail(this ObservableCollection<IdentityCMDetail> list, ObservableCollection<IndividualEntityCsidClidDetail> IndividualEntityCsidClidDetailList)
        {
            ObservableCollection<IndividualEntityCsidClidDetail> result = new ObservableCollection<IndividualEntityCsidClidDetail>();
            foreach (IndividualEntityCsidClidDetail entity in IndividualEntityCsidClidDetailList)
            {
                var searchitem = list.Where(e => e.Clid == entity.Clid && e.Csid == entity.Csid);
                if (searchitem.Count() > 0)
                {
                    entity.BusinessTitle.Title = searchitem.First().BusinessTitle.Title;
                    entity.IsPrimary = searchitem.First().IsPrimary;
                    result.Add(entity);
                }
            }
            return result;
        }
        public static ObservableCollection<IdentityCMDetail> ToIdentityCMDetail(this ObservableCollection<OrganizationUnit> list)
        {
            ObservableCollection<IdentityCMDetail> result = new ObservableCollection<IdentityCMDetail>();
            foreach (OrganizationUnit each in list)
            {
                result.Add(new IdentityCMDetail() { Clid = each.Clid, Csid = each.Csid });
            }
            return result;
        }

     


    }

    
    

   
}
