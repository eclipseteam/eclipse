﻿using System;
using System.Collections.Generic;

namespace Oritax.TaxSimp.Data
{

    public interface IServiceable
    {
        void GetList();
        void GetItems();
        void Add();
        void Update();
        void Delete();
    }

    public interface IServiceable<TList, TItem> : IServiceable
        where TList : IList<TItem>, IHasTotal<TItem>
        where TItem : IHasPrimaryKey
    {
        Action<TList> Process { get; set; }
        TList List { get; set; }
    }
}
