﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    public enum CSVType
    {
        StateStreet = 1,
        BankWest = 2,
        BankWestMarkCSVExport = 3,
        BankWest2 = 4,
        DesktopBroker = 5,
        AdviserDetails = 6
    }

    public enum AccountType
    {
        CMA = 1,
        MMA = 2,
        CMADIFM = 3,
        CMADIWM = 4,
        CMADIY = 5
    }

    public class AOIDCSVColumns
    {
        public readonly string Col_UnitholderCode = "Unitholder Code";
        public readonly string Col_UnitholderSubCode = "Unitholder Sub Code";
        public readonly string Col_UnitholderName1 = "Unitholder Name 1";
        public readonly string Col_UnitholderName2 = "Unitholder Name 2";
        public readonly string Col_PostalAddress1 = "Postal Address 1";
        public readonly string Col_PostalAddress2 = "Postal Address 2";
        public readonly string Col_PostalAddress3 = "Postal Address 3";
        public readonly string Col_PostalAddress4 = "Postal Address 4";
        public readonly string Col_PostCode = "Post Code";
        public readonly string Col_PhoneNumber1 = "Phone Number 1";
        public readonly string Col_PhoneNumber2 = "Phone Number 2";
        public readonly string Col_FaxNumber = "Fax Number";
        public readonly string Col_EmailAddress = "Email Address";
        public readonly string Col_ShortName = "Short Name";
        public readonly string Col_DateAccountOpened = "Date Account Opened";
        public readonly string Col_UnitholderInvestorType = "Unitholder Investor Type";
        public readonly string Col_DistributionOption = "Distribution Option (per fund)";
        public readonly string Col_CategoryCode3 = "Category Code 3";
       // public readonly string Col_StatementFrequency = "Statement Frequency";
      //  public readonly string Col_AnnualReport = "Annual Report";
        public readonly string Col_Active = "Active";
        public readonly string Col_Units = "Units";

        public readonly string Col_InvestorType = "Investor Type";

        public readonly string Col_FirstName = "First Name";
        public readonly string Col_MiddleName = "Middle Name";
        public readonly string Col_LastName = "Last Name";
        public readonly string Col_DateofBirth = "Date of Birth";
        public readonly string Col_Gender = "Gender";

        public readonly string Col_Name1 = "Name 1";
        public readonly string Col_Name2 = "Name 2";
        public readonly string Col_Name3 = "Name 3";
        public readonly string Col_Name4 = "Name 4";

        public readonly string Col_Address1 = "Address 1";
        public readonly string Col_Address2 = "Address 2";

        public readonly string Col_City = "City";
        public readonly string Col_State = "State";
        public readonly string Col_PostCode2 = "Post Code2";
        public readonly string Col_Country = "Country";
        public readonly string Col_TaxCountry = "Tax Country";
        public readonly string Col_TFN_ABN = "TFN/ABN";

        public readonly string Col_BankAccountType = "Bank Account Type";
        public readonly string Col_BSB = "BSB";
        public readonly string Col_BankAccountNumber = "Bank Account Number";
        public readonly string Col_BankAccountName = "Bank Account Name";
        public readonly string Col_BankAccountName2 = "Bank Account Name 2";
        private DataTable dtAOID;

        public DataTable Table { get { return this.dtAOID; } }

        public AOIDCSVColumns()
        {
            dtAOID = new DataTable("tblAOID");
            AddColumn(Col_UnitholderCode);
            AddColumn(Col_UnitholderSubCode);
            AddColumn(Col_UnitholderName1);
            AddColumn(Col_UnitholderName2);
            AddColumn(Col_PostalAddress1);
            AddColumn(Col_PostalAddress2);
            AddColumn(Col_PostalAddress3);
            AddColumn(Col_PostalAddress4);
            AddColumn(Col_PostCode);
            AddColumn(Col_PhoneNumber1);
            AddColumn(Col_PhoneNumber2);
            AddColumn(Col_FaxNumber);
            AddColumn(Col_EmailAddress);

            AddColumn(Col_ShortName);
            AddColumn(Col_DateAccountOpened);
            AddColumn(Col_UnitholderInvestorType);
            AddColumn(Col_DistributionOption);
            AddColumn(Col_CategoryCode3);
            //AddColumn(Col_StatementFrequency);
            //AddColumn(Col_AnnualReport);
            AddColumn(Col_Active);
            AddColumn(Col_Units);

            AddColumn(Col_InvestorType);

            AddColumn(Col_FirstName);
            AddColumn(Col_MiddleName);
            AddColumn(Col_LastName);
            AddColumn(Col_DateofBirth);
            AddColumn(Col_Gender);

            AddColumn(Col_Name1);
            AddColumn(Col_Name2);
            AddColumn(Col_Name3);
            AddColumn(Col_Name4);

            AddColumn(Col_Address1);
            AddColumn(Col_Address2);

            AddColumn(Col_City);
            AddColumn(Col_State);
            AddColumn(Col_PostCode2);
            AddColumn(Col_Country);
            AddColumn(Col_TaxCountry);
            AddColumn(Col_TFN_ABN);

            AddColumn(Col_BankAccountType);
            AddColumn(Col_BSB);
            AddColumn(Col_BankAccountNumber);
            AddColumn(Col_BankAccountName);
            AddColumn(Col_BankAccountName2);
            dtAOID.Rows.Add(dtAOID.NewRow());
        }

        private void AddColumn(string columnName)
        {
            if (!dtAOID.Columns.Contains(columnName))
            {
                DataColumn dc = dtAOID.Columns.Add(columnName);
                dc.DefaultValue = string.Empty;
            }
        }

        public void SetColumnValue(string columnName, string colunmValue)
        {
            if (dtAOID.Columns.Contains(columnName))
                dtAOID.Rows[0][columnName] = colunmValue;
        }

        public string ToCSV()
        {
            StringBuilder str = new StringBuilder();
            foreach (DataRow dr in dtAOID.Rows)
            {
                foreach (object field in dr.ItemArray)
                {
                    string value = field.ToString();
                    if (value.IndexOfAny(new char[] { '"', ',' }) != -1)
                    {
                        // Special handling for values that contain comma or quote
                        // Enclose in quotes and double up any double quotes
                        str.AppendFormat("\"{0}\"" + ",", value.Replace("\"", "\"\""));
                    }
                    else str.Append(value + ",");
                }
                str.Replace(",", Environment.NewLine, str.Length - 1, 1);
            }
            return str.ToString();
        }

        public string GetColumnCSV()
        {
            StringBuilder str = new StringBuilder();
            foreach (DataColumn dc in dtAOID.Columns)
            {
                str.Append(dc.ColumnName + ",");
            }
            str.Replace(",", Environment.NewLine, str.Length - 1, 1);
            return str.ToString();
        }
    }

    public class AOIDCSVColumnsRecon
    {
        public readonly string Col_UnitholderCode = "Unitholder Code";
        public readonly string Col_UnitholderSubCode = "Unitholder Sub Code";
        public readonly string Col_UnitholderName1 = "Unitholder Name 1";
        public readonly string Col_UnitholderName2 = "Unitholder Name 2";
        public readonly string Col_PostalAddress1 = "Postal Address 1";
        public readonly string Col_PostalAddress2 = "Postal Address 2";
        public readonly string Col_PostalAddress3 = "Postal Address 3";
        public readonly string Col_PostalAddress4 = "Postal Address 4";
        public readonly string Col_PostCode = "Post Code";
        public readonly string Col_PhoneNumber1 = "Phone Number 1";
        public readonly string Col_PhoneNumber2 = "Phone Number 2";
        public readonly string Col_FaxNumber = "Fax Number";
        public readonly string Col_EmailAddress = "Email Address";
        public readonly string Col_ShortName = "Short Name";
        public readonly string Col_DateAccountOpened = "Date Account Opened";
        public readonly string Col_UnitholderInvestorType = "Unitholder Investor Type";
        public readonly string Col_DistributionOption = "Distribution Option (per fund)";
        public readonly string Col_CategoryCode3 = "Category Code 3";
        // public readonly string Col_StatementFrequency = "Statement Frequency";
        //  public readonly string Col_AnnualReport = "Annual Report";
        public readonly string Col_Active = "Active";
        public readonly string Col_Units = "Units";

        public readonly string Col_InvestorType = "Investor Type";

        public readonly string Col_FirstName = "First Name";
        public readonly string Col_MiddleName = "Middle Name";
        public readonly string Col_LastName = "Last Name";
        public readonly string Col_DateofBirth = "Date of Birth";
        public readonly string Col_Gender = "Gender";

        public readonly string Col_Name1 = "Name 1";
        public readonly string Col_Name2 = "Name 2";
        public readonly string Col_Name3 = "Name 3";
        public readonly string Col_Name4 = "Name 4";

        public readonly string Col_Address1 = "Address 1";
        public readonly string Col_Address2 = "Address 2";

        public readonly string Col_City = "City";
        public readonly string Col_State = "State";
        public readonly string Col_PostCode2 = "Post Code2";
        public readonly string Col_Country = "Country";
        public readonly string Col_TaxCountry = "Tax Country";
        public readonly string Col_TFN_ABN = "TFN/ABN";

        public readonly string Col_BankAccountType = "Bank Account Type";
        public readonly string Col_BSB = "BSB";
        public readonly string Col_BankAccountNumber = "Bank Account Number";
        public readonly string Col_BankAccountName = "Bank Account Name";
        public readonly string Col_BankAccountName2 = "Bank Account Name 2";

        public readonly string Col_CLIENTTFN = "Client TFN";
        public readonly string Col_CLIENTABN = "Client ABN";
        public readonly string Col_CLIENTDIFMBANKACCOUNTBSB = "Client DIFM BANKACCOUNT BSB";
        public readonly string Col_CLIENTDIFMBANKACCOUNTNO = "Client DIFM BANKACCOUNT NO";
        public readonly string Col_CLIENTDIWMBANKACCOUNTBSB = "Client DIWM BANKACCOUNT BSB";
        public readonly string Col_CLIENTDIWMBANKACCOUNTNO = "Client DIWM BANKACCOUNT NO";
        public readonly string Col_CLIENTDIFMBANKACCOUNTMATCH = "IS DIFM MATCH";
        public readonly string Col_CLIENTDIWMBANKACCOUNTMATCH = "IS DIWM MATCH";

        private DataTable dtAOID;

        public DataTable Table { get { return this.dtAOID; } }

        public AOIDCSVColumnsRecon()
        {
            dtAOID = new DataTable("tblAOID");
            AddColumn(Col_UnitholderCode);
            AddColumn(Col_UnitholderSubCode);
            AddColumn(Col_UnitholderName1);
            AddColumn(Col_UnitholderName2);
            AddColumn(Col_PostalAddress1);
            AddColumn(Col_PostalAddress2);
            AddColumn(Col_PostalAddress3);
            AddColumn(Col_PostalAddress4);
            AddColumn(Col_PostCode);
            AddColumn(Col_PhoneNumber1);
            AddColumn(Col_PhoneNumber2);
            AddColumn(Col_FaxNumber);
            AddColumn(Col_EmailAddress);

            AddColumn(Col_ShortName);
            AddColumn(Col_DateAccountOpened);
            AddColumn(Col_UnitholderInvestorType);
            AddColumn(Col_DistributionOption);
            AddColumn(Col_CategoryCode3);
            AddColumn(Col_Active);
            AddColumn(Col_Units);

            AddColumn(Col_InvestorType);

            AddColumn(Col_FirstName);
            AddColumn(Col_MiddleName);
            AddColumn(Col_LastName);
            AddColumn(Col_DateofBirth);
            AddColumn(Col_Gender);

            AddColumn(Col_Name1);
            AddColumn(Col_Name2);
            AddColumn(Col_Name3);
            AddColumn(Col_Name4);

            AddColumn(Col_Address1);
            AddColumn(Col_Address2);

            AddColumn(Col_City);
            AddColumn(Col_State);
            AddColumn(Col_PostCode2);
            AddColumn(Col_Country);
            AddColumn(Col_TaxCountry);
            AddColumn(Col_TFN_ABN);

            AddColumn(Col_BankAccountType);
            AddColumn(Col_BSB);
            AddColumn(Col_BankAccountNumber);
            AddColumn(Col_BankAccountName);
            AddColumn(Col_BankAccountName2);

            AddColumn(Col_CLIENTTFN);
            AddColumn(Col_CLIENTABN);
            AddColumn(Col_CLIENTDIFMBANKACCOUNTBSB);
            AddColumn(Col_CLIENTDIFMBANKACCOUNTNO);
            AddColumn(Col_CLIENTDIWMBANKACCOUNTBSB);
            AddColumn(Col_CLIENTDIWMBANKACCOUNTNO);
            AddColumn(Col_CLIENTDIFMBANKACCOUNTMATCH);
            AddColumn(Col_CLIENTDIWMBANKACCOUNTMATCH);
            dtAOID.Rows.Add(dtAOID.NewRow());
        }

        private void AddColumn(string columnName)
        {
            if (!dtAOID.Columns.Contains(columnName))
            {
                DataColumn dc = dtAOID.Columns.Add(columnName);
                dc.DefaultValue = string.Empty;
            }
        }

        public void SetColumnValue(string columnName, string colunmValue)
        {
            if (dtAOID.Columns.Contains(columnName))
                dtAOID.Rows[0][columnName] = colunmValue;
        }

        public string ToCSV()
        {
            StringBuilder str = new StringBuilder();
            foreach (DataRow dr in dtAOID.Rows)
            {
                foreach (object field in dr.ItemArray)
                {
                    string value = field.ToString();
                    if (value.IndexOfAny(new char[] { '"', ',' }) != -1)
                    {
                        // Special handling for values that contain comma or quote
                        // Enclose in quotes and double up any double quotes
                        str.AppendFormat("\"{0}\"" + ",", value.Replace("\"", "\"\""));
                    }
                    else str.Append(value + ",");
                }
                str.Replace(",", Environment.NewLine, str.Length - 1, 1);
            }
            return str.ToString();
        }

        public string GetColumnCSV()
        {
            StringBuilder str = new StringBuilder();
            foreach (DataColumn dc in dtAOID.Columns)
            {
                str.Append(dc.ColumnName + ",");
            }
            str.Replace(",", Environment.NewLine, str.Length - 1, 1);
            return str.ToString();
        }
    }

    public class AOTrustCSVColumns
    {
        public readonly string Col_ApplicationID = "ApplicationID";
        public readonly string Col_ApplicationDate = "ApplicationDate";
        public readonly string Col_ApplicationStatus = "ApplicationStatus";
        public readonly string Col_CMTOpenDate = "CMTOpenDate";
        public readonly string Col_OpenAccountApplicationCreatorUserID = "OpenAccountApplicationCreatorUserID";
        public readonly string Col_OpenAccountApplicationApplicantType = "OpenAccountApplicationApplicantType";
        public readonly string Col_OpenAccountApplicationAccountOption = "OpenAccountApplicationAccountOption";
        public readonly string Col_OpenAccountApplicationProductType = "OpenAccountApplicationProductType";
        public readonly string Col_OpenAccountApplicationTrusteesCompany = "OpenAccountApplicationTrusteesCompany";
        public readonly string Col_TrusteesCompanyDetailEntityName1 = "TrusteesCompanyDetailEntityName1";
        public readonly string Col_TrusteesCompanyDetailEntityName2 = "TrusteesCompanyDetailEntityName2";
        public readonly string Col_TrusteesCompanyDetailABN = "TrusteesCompanyDetailABN";
        public readonly string Col_TrusteesCompanyDetailACNARBN = "TrusteesCompanyDetailACNARBN";
        public readonly string Col_TrusteesCompanyDetailContactName = "TrusteesCompanyDetailContactName";
        public readonly string Col_TrusteesCompanyDetailContactEmail = "TrusteesCompanyDetailContactEmail";
        public readonly string Col_TrusteesCompanyDetailContactPhPrefix = "TrusteesCompanyDetailContactPhPrefix";
        public readonly string Col_TrusteesCompanyDetailContactPh = "TrusteesCompanyDetailContactPh";
        public readonly string Col_TrusteesCompanyDetailCountryOfEstablishment = "TrusteesCompanyDetailCountryOfEstablishment";
        public readonly string Col_TrusteesCompanyDetailTrustType = "TrusteesCompanyDetailTrustType";
        public readonly string Col_TFNDetailTFN = "TFNDetailTFN";
        public readonly string Col_TFNDetailNRC = "TFNDetailNRC";
        public readonly string Col_TFNDetailExemptionCategory = "TFNDetailExemptionCategory";
        public readonly string Col_TFNDetailTIN = "TFNDetailTIN";
        public readonly string Col_CorpTrusteeDetailEntityName1 = "CorpTrusteeDetailEntityName1";
        public readonly string Col_CorpTrusteeDetailEntityName2 = "CorpTrusteeDetailEntityName2";
        public readonly string Col_CorpTrusteeDetailABN = "CorpTrusteeDetailABN";
        public readonly string Col_CorpTrusteeDetailACNARBN = "CorpTrusteeDetailACNARBN";
        public readonly string Col_CorpTrusteeDetailCountryOfEstablishment = "CorpTrusteeDetailCountryOfEstablishment";
        public readonly string Col_OpenAccountApplicationMannerOfOperation = "OpenAccountApplicationMannerOfOperation";
        public readonly string Col_OpenAccountApplicationReceiptNo = "OpenAccountApplicationReceiptNo";
        public readonly string Col_SignatoryPersonalDetailOccupation = "SignatoryPersonalDetailOccupation";
        public readonly string Col_SignatoryPersonalDetailEmployer = "SignatoryPersonalDetailEmployer";
        public readonly string Col_SignatoryPersonalDetailCountryOfResidence = "SignatoryPersonalDetailCountryOfResidence";
        public readonly string Col_SignatoryPersonalDetailTitle = "SignatoryPersonalDetailTitle";
        public readonly string Col_SignatoryPersonalDetailTitleOther = "SignatoryPersonalDetailTitleOther";
        public readonly string Col_SignatoryPersonalDetailGivenName = "SignatoryPersonalDetailGivenName";
        public readonly string Col_SignatoryPersonalDetailSurname = "SignatoryPersonalDetailSurname";
        public readonly string Col_SignatoryPersonalDetailPhoneWk = "SignatoryPersonalDetailPhoneWk";
        public readonly string Col_SignatoryPersonalDetailPhoneHm = "SignatoryPersonalDetailPhoneHm";
        public readonly string Col_SignatoryPersonalDetailPhoneMb = "SignatoryPersonalDetailPhoneMb";
        public readonly string Col_SignatoryPersonalDetailDOBDay = "SignatoryPersonalDetailDOBDay";
        public readonly string Col_SignatoryPersonalDetailDOBMonth = "SignatoryPersonalDetailDOBMonth";
        public readonly string Col_SignatoryPersonalDetailDOBYear = "SignatoryPersonalDetailDOBYear";
        public readonly string Col_SignatoryPersonalDetailEmail = "SignatoryPersonalDetailEmail";
        public readonly string Col_AddressAddress1 = "AddressAddress1";
        public readonly string Col_AddressAddress2 = "AddressAddress2";
        public readonly string Col_AddressAddress3 = "AddressAddress3";
        public readonly string Col_AddressSuburb = "AddressSuburb";
        public readonly string Col_AddressState = "AddressState";
        public readonly string Col_AddressPostcode = "AddressPostcode";
        public readonly string Col_AddressCountry = "AddressCountry";
        public readonly string Col_SignatoryPersonalDetailPhoneHmPrefix = "SignatoryPersonalDetailPhoneHmPrefix";
        public readonly string Col_SignatoryPersonalDetailPhonewkPrefix = "SignatoryPersonalDetailPhonewkPrefix";
        public readonly string Col_TFNDetailTFN_2 = "TFNDetailTFN_2";
        public readonly string Col_TFNDetailNRC_2 = "TFNDetailNRC_2";
        public readonly string Col_TFNDetailExemptionCategory_2 = "TFNDetailExemptionCategory_2";
        public readonly string Col_TFNDetailTIN_2 = "TFNDetailTIN_2";
        public readonly string Col_SignatoryPersonalDetailOccupation_2 = "SignatoryPersonalDetailOccupation_2";
        public readonly string Col_SignatoryPersonalDetailEmployer_2 = "SignatoryPersonalDetailEmployer_2";
        public readonly string Col_SignatoryPersonalDetailCountryOfResidence_2 = "SignatoryPersonalDetailCountryOfResidence_2";
        public readonly string Col_SignatoryPersonalDetailTitle_2 = "SignatoryPersonalDetailTitle_2";
        public readonly string Col_SignatoryPersonalDetailTitleOther_2 = "SignatoryPersonalDetailTitleOther_2";
        public readonly string Col_SignatoryPersonalDetailGivenName_2 = "SignatoryPersonalDetailGivenName_2";
        public readonly string Col_SignatoryPersonalDetailSurname_2 = "SignatoryPersonalDetailSurname_2";
        public readonly string Col_SignatoryPersonalDetailPhoneWk_2 = "SignatoryPersonalDetailPhoneWk_2";
        public readonly string Col_SignatoryPersonalDetailPhoneHm_2 = "SignatoryPersonalDetailPhoneHm_2";
        public readonly string Col_SignatoryPersonalDetailPhoneMb_2 = "SignatoryPersonalDetailPhoneMb_2";
        public readonly string Col_SignatoryPersonalDetailDOBDay_2 = "SignatoryPersonalDetailDOBDay_2";
        public readonly string Col_SignatoryPersonalDetailDOBMonth_2 = "SignatoryPersonalDetailDOBMonth_2";
        public readonly string Col_SignatoryPersonalDetailDOBYear_2 = "SignatoryPersonalDetailDOBYear_2";
        public readonly string Col_SignatoryPersonalDetailEmail_2 = "SignatoryPersonalDetailEmail_2";
        public readonly string Col_AddressAddress1_2 = "AddressAddress1_2";
        public readonly string Col_AddressAddress2_2 = "AddressAddress2_2";
        public readonly string Col_AddressAddress3_2 = "AddressAddress3_2";
        public readonly string Col_AddressSuburb_2 = "AddressSuburb_2";
        public readonly string Col_AddressState_2 = "AddressState_2";
        public readonly string Col_AddressPostcode_2 = "AddressPostcode_2";
        public readonly string Col_AddressCountry_2 = "AddressCountry_2";
        public readonly string Col_SignatoryPersonalDetailPhoneHmPrefix_2 = "SignatoryPersonalDetailPhoneHmPrefix_2";
        public readonly string Col_SignatoryPersonalDetailPhonewkPrefix_2 = "SignatoryPersonalDetailPhonewkPrefix_2";
        public readonly string Col_TFNDetailTFN_3 = "TFNDetailTFN_3";
        public readonly string Col_TFNDetailNRC_3 = "TFNDetailNRC_3";
        public readonly string Col_TFNDetailExemptionCategory_3 = "TFNDetailExemptionCategory_3";
        public readonly string Col_TFNDetailTIN_3 = "TFNDetailTIN_3";
        public readonly string Col_SignatoryPersonalDetailOccupation_3 = "SignatoryPersonalDetailOccupation_3";
        public readonly string Col_SignatoryPersonalDetailEmployer_3 = "SignatoryPersonalDetailEmployer_3";
        public readonly string Col_SignatoryPersonalDetailCountryOfResidence_3 = "SignatoryPersonalDetailCountryOfResidence_3";
        public readonly string Col_SignatoryPersonalDetailTitle_3 = "SignatoryPersonalDetailTitle_3";
        public readonly string Col_SignatoryPersonalDetailTitleOther_3 = "SignatoryPersonalDetailTitleOther_3";
        public readonly string Col_SignatoryPersonalDetailGivenName_3 = "SignatoryPersonalDetailGivenName_3";
        public readonly string Col_SignatoryPersonalDetailSurname_3 = "SignatoryPersonalDetailSurname_3";
        public readonly string Col_SignatoryPersonalDetailPhoneWk_3 = "SignatoryPersonalDetailPhoneWk_3";
        public readonly string Col_SignatoryPersonalDetailPhoneHm_3 = "SignatoryPersonalDetailPhoneHm_3";
        public readonly string Col_SignatoryPersonalDetailPhoneMb_3 = "SignatoryPersonalDetailPhoneMb_3";
        public readonly string Col_SignatoryPersonalDetailDOBDay_3 = "SignatoryPersonalDetailDOBDay_3";
        public readonly string Col_SignatoryPersonalDetailDOBMonth_3 = "SignatoryPersonalDetailDOBMonth_3";
        public readonly string Col_SignatoryPersonalDetailDOBYear_3 = "SignatoryPersonalDetailDOBYear_3";
        public readonly string Col_SignatoryPersonalDetailEmail_3 = "SignatoryPersonalDetailEmail_3";
        public readonly string Col_AddressAddress1_3 = "AddressAddress1_3";
        public readonly string Col_AddressAddress2_3 = "AddressAddress2_3";
        public readonly string Col_AddressAddress3_3 = "AddressAddress3_3";
        public readonly string Col_AddressSuburb_3 = "AddressSuburb_3";
        public readonly string Col_AddressState_3 = "AddressState_3";
        public readonly string Col_AddressPostcode_3 = "AddressPostcode_3";
        public readonly string Col_AddressCountry_3 = "AddressCountry_3";
        public readonly string Col_AddressCareOf_3 = "AddressCareOf_3";
        public readonly string Col_SignatoryPersonalDetailPhoneHmPrefix_3 = "SignatoryPersonalDetailPhoneHmPrefix_3";
        public readonly string Col_SignatoryPersonalDetailPhonewkPrefix_3 = "SignatoryPersonalDetailPhonewkPrefix_3";
        public readonly string Col_TFNDetailTFN_4 = "TFNDetailTFN_4";
        public readonly string Col_TFNDetailNRC_4 = "TFNDetailNRC_4";
        public readonly string Col_TFNDetailExemptionCategory_4 = "TFNDetailExemptionCategory_4";
        public readonly string Col_TFNDetailTIN_4 = "TFNDetailTIN_4";
        public readonly string Col_SignatoryPersonalDetailOccupation_4 = "SignatoryPersonalDetailOccupation_4";
        public readonly string Col_SignatoryPersonalDetailEmployer_4 = "SignatoryPersonalDetailEmployer_4";
        public readonly string Col_SignatoryPersonalDetailCountryOfResidence_4 = "SignatoryPersonalDetailCountryOfResidence_4";
        public readonly string Col_SignatoryPersonalDetailTitle_4 = "SignatoryPersonalDetailTitle_4";
        public readonly string Col_SignatoryPersonalDetailTitleOther_4 = "SignatoryPersonalDetailTitleOther_4";
        public readonly string Col_SignatoryPersonalDetailGivenName_4 = "SignatoryPersonalDetailGivenName_4";
        public readonly string Col_SignatoryPersonalDetailSurname_4 = "SignatoryPersonalDetailSurname_4";
        public readonly string Col_SignatoryPersonalDetailPhoneWk_4 = "SignatoryPersonalDetailPhoneWk_4";
        public readonly string Col_SignatoryPersonalDetailPhoneHm_4 = "SignatoryPersonalDetailPhoneHm_4";
        public readonly string Col_SignatoryPersonalDetailPhoneMb_4 = "SignatoryPersonalDetailPhoneMb_4";
        public readonly string Col_SignatoryPersonalDetailDOBDay_4 = "SignatoryPersonalDetailDOBDay_4";
        public readonly string Col_SignatoryPersonalDetailDOBMonth_4 = "SignatoryPersonalDetailDOBMonth_4";
        public readonly string Col_SignatoryPersonalDetailDOBYear_4 = "SignatoryPersonalDetailDOBYear_4";
        public readonly string Col_SignatoryPersonalDetailEmail_4 = "SignatoryPersonalDetailEmail_4";
        public readonly string Col_AddressAddress1_4 = "AddressAddress1_4";
        public readonly string Col_AddressAddress2_4 = "AddressAddress2_4";
        public readonly string Col_AddressAddress3_4 = "AddressAddress3_4";
        public readonly string Col_AddressSuburb_4 = "AddressSuburb_4";
        public readonly string Col_AddressState_4 = "AddressState_4";
        public readonly string Col_AddressPostcode_4 = "AddressPostcode_4";
        public readonly string Col_AddressCountry_4 = "AddressCountry_4";
        public readonly string Col_SignatoryPersonalDetailPhoneHmPrefix_4 = "SignatoryPersonalDetailPhoneHmPrefix_4";
        public readonly string Col_SignatoryPersonalDetailPhonewkPrefix_4 = "SignatoryPersonalDetailPhonewkPrefix_4";
        public readonly string Col_TFNDetailTFN_5 = "TFNDetailTFN_5";
        public readonly string Col_TFNDetailNRC_5 = "TFNDetailNRC_5";
        public readonly string Col_TFNDetailExemptionCategory_5 = "TFNDetailExemptionCategory_5";
        public readonly string Col_TFNDetailTIN_5 = "TFNDetailTIN_5";
        public readonly string Col_CorrespondenceAddressAddress1 = "CorrespondenceAddressAddress1";
        public readonly string Col_CorrespondenceAddressAddress2 = "CorrespondenceAddressAddress2";
        public readonly string Col_CorrespondenceAddressSuburb = "CorrespondenceAddressSuburb";
        public readonly string Col_CorrespondenceAddressState = "CorrespondenceAddressState";
        public readonly string Col_CorrespondenceAddressPostcode = "CorrespondenceAddressPostcode";
        public readonly string Col_CorrespondenceAddressCountry = "CorrespondenceAddressCountry";
        public readonly string Col_CorrespondenceAddressAddress1_2 = "CorrespondenceAddressAddress1_2";
        public readonly string Col_CorrespondenceAddressAddress2_2 = "CorrespondenceAddressAddress2_2";
        public readonly string Col_CorrespondenceAddressSuburb_2 = "CorrespondenceAddressSuburb_2";
        public readonly string Col_CorrespondenceAddressState_2 = "CorrespondenceAddressState_2";
        public readonly string Col_CorrespondenceAddressPostcode_2 = "CorrespondenceAddressPostcode_2";
        public readonly string Col_CorrespondenceAddressCountry_2 = "CorrespondenceAddressCountry_2";
        public readonly string Col_CorrespondenceAddressAddress1_3 = "CorrespondenceAddressAddress1_3";
        public readonly string Col_CorrespondenceAddressAddress2_3 = "CorrespondenceAddressAddress2_3";
        public readonly string Col_CorrespondenceAddressAddress3_3 = "CorrespondenceAddressAddress3_3";
        public readonly string Col_CorrespondenceAddressSuburb_3 = "CorrespondenceAddressSuburb_3";
        public readonly string Col_CorrespondenceAddressState_3 = "CorrespondenceAddressState_3";
        public readonly string Col_CorrespondenceAddressPostcode_3 = "CorrespondenceAddressPostcode_3";
        public readonly string Col_CorrespondenceAddressCountry_3 = "CorrespondenceAddressCountry_3";
        public readonly string Col_PlatformName = "PlatformName";
        public readonly string Col_PlatformCode = "PlatformCode";

        private DataTable dtAOTrust;

        public DataTable Table { get { return this.dtAOTrust; } }

        public AOTrustCSVColumns()
        {
            dtAOTrust = new DataTable("tblAOTrust");
            AddColumn(Col_ApplicationID);
            AddColumn(Col_ApplicationDate);
            AddColumn(Col_ApplicationStatus);
            AddColumn(Col_CMTOpenDate);
            AddColumn(Col_OpenAccountApplicationCreatorUserID);
            AddColumn(Col_OpenAccountApplicationApplicantType);
            AddColumn(Col_OpenAccountApplicationAccountOption);
            AddColumn(Col_OpenAccountApplicationProductType);
            AddColumn(Col_OpenAccountApplicationTrusteesCompany);
            AddColumn(Col_TrusteesCompanyDetailEntityName1);
            AddColumn(Col_TrusteesCompanyDetailEntityName2);
            AddColumn(Col_TrusteesCompanyDetailABN);
            AddColumn(Col_TrusteesCompanyDetailACNARBN);
            AddColumn(Col_TrusteesCompanyDetailContactName);
            AddColumn(Col_TrusteesCompanyDetailContactEmail);
            AddColumn(Col_TrusteesCompanyDetailContactPhPrefix);
            AddColumn(Col_TrusteesCompanyDetailContactPh);
            AddColumn(Col_TrusteesCompanyDetailCountryOfEstablishment);
            AddColumn(Col_TrusteesCompanyDetailTrustType);
            AddColumn(Col_TFNDetailTFN);
            AddColumn(Col_TFNDetailNRC);
            AddColumn(Col_TFNDetailExemptionCategory);
            AddColumn(Col_TFNDetailTIN);
            AddColumn(Col_CorpTrusteeDetailEntityName1);
            AddColumn(Col_CorpTrusteeDetailEntityName2);
            AddColumn(Col_CorpTrusteeDetailABN);
            AddColumn(Col_CorpTrusteeDetailACNARBN);
            AddColumn(Col_CorpTrusteeDetailCountryOfEstablishment);
            AddColumn(Col_OpenAccountApplicationMannerOfOperation);
            AddColumn(Col_OpenAccountApplicationReceiptNo);
            AddColumn(Col_SignatoryPersonalDetailOccupation);
            AddColumn(Col_SignatoryPersonalDetailEmployer);
            AddColumn(Col_SignatoryPersonalDetailCountryOfResidence);
            AddColumn(Col_SignatoryPersonalDetailTitle);
            AddColumn(Col_SignatoryPersonalDetailTitleOther);
            AddColumn(Col_SignatoryPersonalDetailGivenName);
            AddColumn(Col_SignatoryPersonalDetailSurname);
            AddColumn(Col_SignatoryPersonalDetailPhoneWk);
            AddColumn(Col_SignatoryPersonalDetailPhoneHm);
            AddColumn(Col_SignatoryPersonalDetailPhoneMb);
            AddColumn(Col_SignatoryPersonalDetailDOBDay);
            AddColumn(Col_SignatoryPersonalDetailDOBMonth);
            AddColumn(Col_SignatoryPersonalDetailDOBYear);
            AddColumn(Col_SignatoryPersonalDetailEmail);
            AddColumn(Col_AddressAddress1);
            AddColumn(Col_AddressAddress2);
            AddColumn(Col_AddressAddress3);
            AddColumn(Col_AddressSuburb);
            AddColumn(Col_AddressState);
            AddColumn(Col_AddressPostcode);
            AddColumn(Col_AddressCountry);
            AddColumn(Col_SignatoryPersonalDetailPhoneHmPrefix);
            AddColumn(Col_SignatoryPersonalDetailPhonewkPrefix);
            AddColumn(Col_TFNDetailTFN_2);
            AddColumn(Col_TFNDetailNRC_2);
            AddColumn(Col_TFNDetailExemptionCategory_2);
            AddColumn(Col_TFNDetailTIN_2);
            AddColumn(Col_SignatoryPersonalDetailOccupation_2);
            AddColumn(Col_SignatoryPersonalDetailEmployer_2);
            AddColumn(Col_SignatoryPersonalDetailCountryOfResidence_2);
            AddColumn(Col_SignatoryPersonalDetailTitle_2);
            AddColumn(Col_SignatoryPersonalDetailTitleOther_2);
            AddColumn(Col_SignatoryPersonalDetailGivenName_2);
            AddColumn(Col_SignatoryPersonalDetailSurname_2);
            AddColumn(Col_SignatoryPersonalDetailPhoneWk_2);
            AddColumn(Col_SignatoryPersonalDetailPhoneHm_2);
            AddColumn(Col_SignatoryPersonalDetailPhoneMb_2);
            AddColumn(Col_SignatoryPersonalDetailDOBDay_2);
            AddColumn(Col_SignatoryPersonalDetailDOBMonth_2);
            AddColumn(Col_SignatoryPersonalDetailDOBYear_2);
            AddColumn(Col_SignatoryPersonalDetailEmail_2);
            AddColumn(Col_AddressAddress1_2);
            AddColumn(Col_AddressAddress2_2);
            AddColumn(Col_AddressAddress3_2);
            AddColumn(Col_AddressSuburb_2);
            AddColumn(Col_AddressState_2);
            AddColumn(Col_AddressPostcode_2);
            AddColumn(Col_AddressCountry_2);
            AddColumn(Col_SignatoryPersonalDetailPhoneHmPrefix_2);
            AddColumn(Col_SignatoryPersonalDetailPhonewkPrefix_2);
            AddColumn(Col_TFNDetailTFN_3);
            AddColumn(Col_TFNDetailNRC_3);
            AddColumn(Col_TFNDetailExemptionCategory_3);
            AddColumn(Col_TFNDetailTIN_3);
            AddColumn(Col_SignatoryPersonalDetailOccupation_3);
            AddColumn(Col_SignatoryPersonalDetailEmployer_3);
            AddColumn(Col_SignatoryPersonalDetailCountryOfResidence_3);
            AddColumn(Col_SignatoryPersonalDetailTitle_3);
            AddColumn(Col_SignatoryPersonalDetailTitleOther_3);
            AddColumn(Col_SignatoryPersonalDetailGivenName_3);
            AddColumn(Col_SignatoryPersonalDetailSurname_3);
            AddColumn(Col_SignatoryPersonalDetailPhoneWk_3);
            AddColumn(Col_SignatoryPersonalDetailPhoneHm_3);
            AddColumn(Col_SignatoryPersonalDetailPhoneMb_3);
            AddColumn(Col_SignatoryPersonalDetailDOBDay_3);
            AddColumn(Col_SignatoryPersonalDetailDOBMonth_3);
            AddColumn(Col_SignatoryPersonalDetailDOBYear_3);
            AddColumn(Col_SignatoryPersonalDetailEmail_3);
            AddColumn(Col_AddressAddress1_3);
            AddColumn(Col_AddressAddress2_3);
            AddColumn(Col_AddressAddress3_3);
            AddColumn(Col_AddressSuburb_3);
            AddColumn(Col_AddressState_3);
            AddColumn(Col_AddressPostcode_3);
            AddColumn(Col_AddressCountry_3);
            AddColumn(Col_AddressCareOf_3);
            AddColumn(Col_SignatoryPersonalDetailPhoneHmPrefix_3);
            AddColumn(Col_SignatoryPersonalDetailPhonewkPrefix_3);
            AddColumn(Col_TFNDetailTFN_4);
            AddColumn(Col_TFNDetailNRC_4);
            AddColumn(Col_TFNDetailExemptionCategory_4);
            AddColumn(Col_TFNDetailTIN_4);
            AddColumn(Col_SignatoryPersonalDetailOccupation_4);
            AddColumn(Col_SignatoryPersonalDetailEmployer_4);
            AddColumn(Col_SignatoryPersonalDetailCountryOfResidence_4);
            AddColumn(Col_SignatoryPersonalDetailTitle_4);
            AddColumn(Col_SignatoryPersonalDetailTitleOther_4);
            AddColumn(Col_SignatoryPersonalDetailGivenName_4);
            AddColumn(Col_SignatoryPersonalDetailSurname_4);
            AddColumn(Col_SignatoryPersonalDetailPhoneWk_4);
            AddColumn(Col_SignatoryPersonalDetailPhoneHm_4);
            AddColumn(Col_SignatoryPersonalDetailPhoneMb_4);
            AddColumn(Col_SignatoryPersonalDetailDOBDay_4);
            AddColumn(Col_SignatoryPersonalDetailDOBMonth_4);
            AddColumn(Col_SignatoryPersonalDetailDOBYear_4);
            AddColumn(Col_SignatoryPersonalDetailEmail_4);
            AddColumn(Col_AddressAddress1_4);
            AddColumn(Col_AddressAddress2_4);
            AddColumn(Col_AddressAddress3_4);
            AddColumn(Col_AddressSuburb_4);
            AddColumn(Col_AddressState_4);
            AddColumn(Col_AddressPostcode_4);
            AddColumn(Col_AddressCountry_4);
            AddColumn(Col_SignatoryPersonalDetailPhoneHmPrefix_4);
            AddColumn(Col_SignatoryPersonalDetailPhonewkPrefix_4);
            AddColumn(Col_TFNDetailTFN_5);
            AddColumn(Col_TFNDetailNRC_5);
            AddColumn(Col_TFNDetailExemptionCategory_5);
            AddColumn(Col_TFNDetailTIN_5);
            AddColumn(Col_CorrespondenceAddressAddress1);
            AddColumn(Col_CorrespondenceAddressAddress2);
            AddColumn(Col_CorrespondenceAddressSuburb);
            AddColumn(Col_CorrespondenceAddressState);
            AddColumn(Col_CorrespondenceAddressPostcode);
            AddColumn(Col_CorrespondenceAddressCountry);
            AddColumn(Col_CorrespondenceAddressAddress1_2);
            AddColumn(Col_CorrespondenceAddressAddress2_2);
            AddColumn(Col_CorrespondenceAddressSuburb_2);
            AddColumn(Col_CorrespondenceAddressState_2);
            AddColumn(Col_CorrespondenceAddressPostcode_2);
            AddColumn(Col_CorrespondenceAddressCountry_2);
            AddColumn(Col_CorrespondenceAddressAddress1_3);
            AddColumn(Col_CorrespondenceAddressAddress2_3);
            AddColumn(Col_CorrespondenceAddressAddress3_3);
            AddColumn(Col_CorrespondenceAddressSuburb_3);
            AddColumn(Col_CorrespondenceAddressState_3);
            AddColumn(Col_CorrespondenceAddressPostcode_3);
            AddColumn(Col_CorrespondenceAddressCountry_3);
            AddColumn(Col_PlatformName);
            AddColumn(Col_PlatformCode);

            dtAOTrust.Rows.Add(dtAOTrust.NewRow());
        }

        private void AddColumn(string columnName)
        {
            if (!dtAOTrust.Columns.Contains(columnName))
            {
                DataColumn dc = dtAOTrust.Columns.Add(columnName);
                dc.DefaultValue = string.Empty;
            }
        }

        public void SetColumnValue(string columnName, string colunmValue)
        {
            if (dtAOTrust.Columns.Contains(columnName))
                dtAOTrust.Rows[0][columnName] = colunmValue;
        }

        public string ToCSV()
        {
            StringBuilder str = new StringBuilder();
            foreach (DataRow dr in dtAOTrust.Rows)
            {
                foreach (object field in dr.ItemArray)
                {
                    string value = field.ToString();
                    if (value.IndexOfAny(new char[] { '"', ',' }) != -1)
                    {
                        // Special handling for values that contain comma or quote
                        // Enclose in quotes and double up any double quotes
                        str.AppendFormat("\"{0}\"" + ",", value.Replace("\"", "\"\""));
                    }
                    else str.Append(value + ",");
                }
                str.Replace(",", Environment.NewLine, str.Length - 1, 1);
            }
            return str.ToString();
        }

        public string GetColumnCSV()
        {
            StringBuilder str = new StringBuilder();
            foreach (DataColumn dc in dtAOTrust.Columns)
            {
                str.Append(dc.ColumnName + ",");
            }
            str.Replace(",", Environment.NewLine, str.Length - 1, 1);
            return str.ToString();
        }
    }

    public class AOBusinessCSVColumns
    {
        public readonly string Col_ApplicationID = "ApplicationID";
        public readonly string Col_ApplicationDate = "ApplicationDate";
        public readonly string Col_OpenAccountApplicationTrusteesCompany = "OpenAccountApplicationTrusteesCompany";
        public readonly string Col_TrusteesCompanyDetailEntityName1 = "TrusteesCompanyDetailEntityName1";
        public readonly string Col_TrusteesCompanyDetailEntityName2 = "TrusteesCompanyDetailEntityName2";
        public readonly string Col_TrusteesCompanyDetailABN = "TrusteesCompanyDetailABN";
        public readonly string Col_TrusteesCompanyDetailACNARBN = "TrusteesCompanyDetailACNARBN";
        public readonly string Col_TrusteesCompanyDetailContactName = "TrusteesCompanyDetailContactName";
        public readonly string Col_TrusteesCompanyDetailContactEmail = "TrusteesCompanyDetailContactEmail";
        public readonly string Col_TrusteesCompanyDetailContactPhPrefix = "TrusteesCompanyDetailContactPhPrefix";
        public readonly string Col_TrusteesCompanyDetailContactPh = "TrusteesCompanyDetailContactPh";
        public readonly string Col_TrusteesCompanyDetailCountryOfEstablishment = "TrusteesCompanyDetailCountryOfEstablishment";
        public readonly string Col_TFNDetailTFN = "TFNDetailTFN";
        public readonly string Col_TFNDetailNRC = "TFNDetailNRC";
        public readonly string Col_TFNDetailExemptionCategory = "TFNDetailExemptionCategory";
        public readonly string Col_OpenAccountApplicationMannerOfOperation = "OpenAccountApplicationMannerOfOperation";
        public readonly string Col_OpenAccountApplicationDealerName = "OpenAccountApplicationDealerName";
        public readonly string Col_OpenAccountApplicationAdviserName = "OpenAccountApplicationAdviserName";
        public readonly string Col_OpenAccountApplicationAdviserCode = "OpenAccountApplicationAdviserCode";
        public readonly string Col_OpenAccountApplicationClientAccountDesignation = "OpenAccountApplicationClientAccountDesignation";
        public readonly string Col_SignatoryPersonalDetailOccupation = "SignatoryPersonalDetailOccupation";
        public readonly string Col_SignatoryPersonalDetailEmployer = "SignatoryPersonalDetailEmployer";
        public readonly string Col_SignatoryPersonalDetailCountryOfResidence = "SignatoryPersonalDetailCountryOfResidence";
        public readonly string Col_SignatoryPersonalDetailTitle = "SignatoryPersonalDetailTitle";
        public readonly string Col_SignatoryPersonalDetailTitleOther = "SignatoryPersonalDetailTitleOther";
        public readonly string Col_SignatoryPersonalDetailGivenName = "SignatoryPersonalDetailGivenName";
        public readonly string Col_SignatoryPersonalDetailSurname = "SignatoryPersonalDetailSurname";
        public readonly string Col_SignatoryPersonalDetailPhoneWk = "SignatoryPersonalDetailPhoneWk";
        public readonly string Col_SignatoryPersonalDetailPhoneHm = "SignatoryPersonalDetailPhoneHm";
        public readonly string Col_SignatoryPersonalDetailPhoneMb = "SignatoryPersonalDetailPhoneMb";
        public readonly string Col_SignatoryPersonalDetailDOBDay = "SignatoryPersonalDetailDOBDay";
        public readonly string Col_SignatoryPersonalDetailDOBMonth = "SignatoryPersonalDetailDOBMonth";
        public readonly string Col_SignatoryPersonalDetailDOBYear = "SignatoryPersonalDetailDOBYear";
        public readonly string Col_SignatoryPersonalDetailEmail = "SignatoryPersonalDetailEmail";
        public readonly string Col_AddressAddress1 = "AddressAddress1";
        public readonly string Col_AddressAddress2 = "AddressAddress2";
        public readonly string Col_AddressAddress3 = "AddressAddress3";
        public readonly string Col_AddressSuburb = "AddressSuburb";
        public readonly string Col_AddressState = "AddressState";
        public readonly string Col_AddressPostcode = "AddressPostcode";
        public readonly string Col_AddressCountry = "AddressCountry";
        public readonly string Col_SignatoryPersonalDetailPhoneHmPrefix = "SignatoryPersonalDetailPhoneHmPrefix";
        public readonly string Col_SignatoryPersonalDetailPhonewkPrefix = "SignatoryPersonalDetailPhonewkPrefix";
        public readonly string Col_TFNDetailTFN_2 = "TFNDetailTFN_2";
        public readonly string Col_TFNDetailNRC_2 = "TFNDetailNRC_2";
        public readonly string Col_TFNDetailExemptionCategory_2 = "TFNDetailExemptionCategory_2";
        public readonly string Col_TFNDetailTIN_2 = "TFNDetailTIN_2";
        public readonly string Col_SignatoryPersonalDetailOccupation_2 = "SignatoryPersonalDetailOccupation_2";
        public readonly string Col_SignatoryPersonalDetailEmployer_2 = "SignatoryPersonalDetailEmployer_2";
        public readonly string Col_SignatoryPersonalDetailCountryOfResidence_2 = "SignatoryPersonalDetailCountryOfResidence_2";
        public readonly string Col_SignatoryPersonalDetailTitle_2 = "SignatoryPersonalDetailTitle_2";
        public readonly string Col_SignatoryPersonalDetailTitleOther_2 = "SignatoryPersonalDetailTitleOther_2";
        public readonly string Col_SignatoryPersonalDetailGivenName_2 = "SignatoryPersonalDetailGivenName_2";
        public readonly string Col_SignatoryPersonalDetailSurname_2 = "SignatoryPersonalDetailSurname_2";
        public readonly string Col_SignatoryPersonalDetailPhoneWk_2 = "SignatoryPersonalDetailPhoneWk_2";
        public readonly string Col_SignatoryPersonalDetailPhoneHm_2 = "SignatoryPersonalDetailPhoneHm_2";
        public readonly string Col_SignatoryPersonalDetailPhoneMb_2 = "SignatoryPersonalDetailPhoneMb_2";
        public readonly string Col_SignatoryPersonalDetailDOBDay_2 = "SignatoryPersonalDetailDOBDay_2";
        public readonly string Col_SignatoryPersonalDetailDOBMonth_2 = "SignatoryPersonalDetailDOBMonth_2";
        public readonly string Col_SignatoryPersonalDetailDOBYear_2 = "SignatoryPersonalDetailDOBYear_2";
        public readonly string Col_SignatoryPersonalDetailEmail_2 = "SignatoryPersonalDetailEmail_2";
        public readonly string Col_AddressAddress1_2 = "AddressAddress1_2";
        public readonly string Col_AddressAddress2_2 = "AddressAddress2_2";
        public readonly string Col_AddressAddress3_2 = "AddressAddress3_2";
        public readonly string Col_AddressSuburb_2 = "AddressSuburb_2";
        public readonly string Col_AddressState_2 = "AddressState_2";
        public readonly string Col_AddressPostcode_2 = "AddressPostcode_2";
        public readonly string Col_AddressCountry_2 = "AddressCountry_2";
        public readonly string Col_SignatoryPersonalDetailPhoneHmPrefix_2 = "SignatoryPersonalDetailPhoneHmPrefix_2";
        public readonly string Col_SignatoryPersonalDetailPhonewkPrefix_2 = "SignatoryPersonalDetailPhonewkPrefix_2";
        public readonly string Col_TFNDetailTFN_3 = "TFNDetailTFN_3";
        public readonly string Col_TFNDetailNRC_3 = "TFNDetailNRC_3";
        public readonly string Col_TFNDetailExemptionCategory_3 = "TFNDetailExemptionCategory_3";
        public readonly string Col_TFNDetailTIN_3 = "TFNDetailTIN_3";
        public readonly string Col_SignatoryPersonalDetailOccupation_3 = "SignatoryPersonalDetailOccupation_3";
        public readonly string Col_SignatoryPersonalDetailEmployer_3 = "SignatoryPersonalDetailEmployer_3";
        public readonly string Col_SignatoryPersonalDetailCountryOfResidence_3 = "SignatoryPersonalDetailCountryOfResidence_3";
        public readonly string Col_SignatoryPersonalDetailTitle_3 = "SignatoryPersonalDetailTitle_3";
        public readonly string Col_SignatoryPersonalDetailTitleOther_3 = "SignatoryPersonalDetailTitleOther_3";
        public readonly string Col_SignatoryPersonalDetailGivenName_3 = "SignatoryPersonalDetailGivenName_3";
        public readonly string Col_SignatoryPersonalDetailSurname_3 = "SignatoryPersonalDetailSurname_3";
        public readonly string Col_SignatoryPersonalDetailPhoneWk_3 = "SignatoryPersonalDetailPhoneWk_3";
        public readonly string Col_SignatoryPersonalDetailPhoneHm_3 = "SignatoryPersonalDetailPhoneHm_3";
        public readonly string Col_SignatoryPersonalDetailPhoneMb_3 = "SignatoryPersonalDetailPhoneMb_3";
        public readonly string Col_SignatoryPersonalDetailDOBDay_3 = "SignatoryPersonalDetailDOBDay_3";
        public readonly string Col_SignatoryPersonalDetailDOBMonth_3 = "SignatoryPersonalDetailDOBMonth_3";
        public readonly string Col_SignatoryPersonalDetailDOBYear_3 = "SignatoryPersonalDetailDOBYear_3";
        public readonly string Col_SignatoryPersonalDetailEmail_3 = "SignatoryPersonalDetailEmail_3";
        public readonly string Col_AddressAddress1_3 = "AddressAddress1_3";
        public readonly string Col_AddressAddress2_3 = "AddressAddress2_3";
        public readonly string Col_AddressAddress3_3 = "AddressAddress3_3";
        public readonly string Col_AddressSuburb_3 = "AddressSuburb_3";
        public readonly string Col_AddressState_3 = "AddressState_3";
        public readonly string Col_AddressPostcode_3 = "AddressPostcode_3";
        public readonly string Col_AddressCountry_3 = "AddressCountry_3";
        public readonly string Col_AddressCareOf_3 = "AddressCareOf_3";
        public readonly string Col_SignatoryPersonalDetailPhoneHmPrefix_3 = "SignatoryPersonalDetailPhoneHmPrefix_3";
        public readonly string Col_SignatoryPersonalDetailPhonewkPrefix_3 = "SignatoryPersonalDetailPhonewkPrefix_3";
        public readonly string Col_TFNDetailTFN_4 = "TFNDetailTFN_4";
        public readonly string Col_TFNDetailNRC_4 = "TFNDetailNRC_4";
        public readonly string Col_TFNDetailExemptionCategory_4 = "TFNDetailExemptionCategory_4";
        public readonly string Col_TFNDetailTIN_4 = "TFNDetailTIN_4";
        public readonly string Col_SignatoryPersonalDetailOccupation_4 = "SignatoryPersonalDetailOccupation_4";
        public readonly string Col_SignatoryPersonalDetailEmployer_4 = "SignatoryPersonalDetailEmployer_4";
        public readonly string Col_SignatoryPersonalDetailCountryOfResidence_4 = "SignatoryPersonalDetailCountryOfResidence_4";
        public readonly string Col_SignatoryPersonalDetailTitle_4 = "SignatoryPersonalDetailTitle_4";
        public readonly string Col_SignatoryPersonalDetailTitleOther_4 = "SignatoryPersonalDetailTitleOther_4";
        public readonly string Col_SignatoryPersonalDetailGivenName_4 = "SignatoryPersonalDetailGivenName_4";
        public readonly string Col_SignatoryPersonalDetailSurname_4 = "SignatoryPersonalDetailSurname_4";
        public readonly string Col_SignatoryPersonalDetailPhoneWk_4 = "SignatoryPersonalDetailPhoneWk_4";
        public readonly string Col_SignatoryPersonalDetailPhoneHm_4 = "SignatoryPersonalDetailPhoneHm_4";
        public readonly string Col_SignatoryPersonalDetailPhoneMb_4 = "SignatoryPersonalDetailPhoneMb_4";
        public readonly string Col_SignatoryPersonalDetailDOBDay_4 = "SignatoryPersonalDetailDOBDay_4";
        public readonly string Col_SignatoryPersonalDetailDOBMonth_4 = "SignatoryPersonalDetailDOBMonth_4";
        public readonly string Col_SignatoryPersonalDetailDOBYear_4 = "SignatoryPersonalDetailDOBYear_4";
        public readonly string Col_SignatoryPersonalDetailEmail_4 = "SignatoryPersonalDetailEmail_4";
        public readonly string Col_AddressAddress1_4 = "AddressAddress1_4";
        public readonly string Col_AddressAddress2_4 = "AddressAddress2_4";
        public readonly string Col_AddressAddress3_4 = "AddressAddress3_4";
        public readonly string Col_AddressSuburb_4 = "AddressSuburb_4";
        public readonly string Col_AddressState_4 = "AddressState_4";
        public readonly string Col_AddressPostcode_4 = "AddressPostcode_4";
        public readonly string Col_AddressCountry_4 = "AddressCountry_4";
        public readonly string Col_SignatoryPersonalDetailPhoneHmPrefix_4 = "SignatoryPersonalDetailPhoneHmPrefix_4";
        public readonly string Col_SignatoryPersonalDetailPhonewkPrefix_4 = "SignatoryPersonalDetailPhonewkPrefix_4";
        public readonly string Col_TFNDetailTFN_5 = "TFNDetailTFN_5";
        public readonly string Col_TFNDetailNRC_5 = "TFNDetailNRC_5";
        public readonly string Col_TFNDetailExemptionCategory_5 = "TFNDetailExemptionCategory_5";
        public readonly string Col_TFNDetailTIN_5 = "TFNDetailTIN_5";
        public readonly string Col_CorrespondenceAddressAddress1 = "CorrespondenceAddressAddress1";
        public readonly string Col_CorrespondenceAddressAddress2 = "CorrespondenceAddressAddress2";
        public readonly string Col_CorrespondenceAddressSuburb = "CorrespondenceAddressSuburb";
        public readonly string Col_CorrespondenceAddressState = "CorrespondenceAddressState";
        public readonly string Col_CorrespondenceAddressPostcode = "CorrespondenceAddressPostcode";
        public readonly string Col_CorrespondenceAddressCountry = "CorrespondenceAddressCountry";
        public readonly string Col_CorrespondenceAddressAddress1_2 = "CorrespondenceAddressAddress1_2";
        public readonly string Col_CorrespondenceAddressAddress2_2 = "CorrespondenceAddressAddress2_2";
        public readonly string Col_CorrespondenceAddressSuburb_2 = "CorrespondenceAddressSuburb_2";
        public readonly string Col_CorrespondenceAddressState_2 = "CorrespondenceAddressState_2";
        public readonly string Col_CorrespondenceAddressPostcode_2 = "CorrespondenceAddressPostcode_2";
        public readonly string Col_CorrespondenceAddressCountry_2 = "CorrespondenceAddressCountry_2";
        public readonly string Col_CorrespondenceAddressAddress1_3 = "CorrespondenceAddressAddress1_3";
        public readonly string Col_CorrespondenceAddressAddress2_3 = "CorrespondenceAddressAddress2_3";
        public readonly string Col_CorrespondenceAddressAddress3_3 = "CorrespondenceAddressAddress3_3";
        public readonly string Col_CorrespondenceAddressSuburb_3 = "CorrespondenceAddressSuburb_3";
        public readonly string Col_CorrespondenceAddressState_3 = "CorrespondenceAddressState_3";
        public readonly string Col_CorrespondenceAddressPostcode_3 = "CorrespondenceAddressPostcode_3";
        public readonly string Col_CorrespondenceAddressCountry_3 = "CorrespondenceAddressCountry_3";
        public readonly string Col_PlatformName = "PlatformName";
        public readonly string Col_PlatformCode = "PlatformCode";

        private DataTable dtAOBusiness;

        public DataTable Table { get { return this.dtAOBusiness; } }

        public AOBusinessCSVColumns()
        {
            dtAOBusiness = new DataTable("tblAOBusiness");
            AddColumn(Col_ApplicationID);
            AddColumn(Col_ApplicationDate);
            AddColumn(Col_OpenAccountApplicationTrusteesCompany);
            AddColumn(Col_TrusteesCompanyDetailEntityName1);
            AddColumn(Col_TrusteesCompanyDetailEntityName2);
            AddColumn(Col_TrusteesCompanyDetailABN);
            AddColumn(Col_TrusteesCompanyDetailACNARBN);
            AddColumn(Col_TrusteesCompanyDetailContactName);
            AddColumn(Col_TrusteesCompanyDetailContactEmail);
            AddColumn(Col_TrusteesCompanyDetailContactPhPrefix);
            AddColumn(Col_TrusteesCompanyDetailContactPh);
            AddColumn(Col_TrusteesCompanyDetailCountryOfEstablishment);
            AddColumn(Col_TFNDetailTFN);
            AddColumn(Col_TFNDetailNRC);
            AddColumn(Col_TFNDetailExemptionCategory);
            AddColumn(Col_OpenAccountApplicationMannerOfOperation);
            AddColumn(Col_OpenAccountApplicationDealerName);
            AddColumn(Col_OpenAccountApplicationAdviserName);
            AddColumn(Col_OpenAccountApplicationAdviserCode);
            AddColumn(Col_OpenAccountApplicationClientAccountDesignation);
            AddColumn(Col_SignatoryPersonalDetailOccupation);
            AddColumn(Col_SignatoryPersonalDetailEmployer);
            AddColumn(Col_SignatoryPersonalDetailCountryOfResidence);
            AddColumn(Col_SignatoryPersonalDetailTitle);
            AddColumn(Col_SignatoryPersonalDetailTitleOther);
            AddColumn(Col_SignatoryPersonalDetailGivenName);
            AddColumn(Col_SignatoryPersonalDetailSurname);
            AddColumn(Col_SignatoryPersonalDetailPhoneWk);
            AddColumn(Col_SignatoryPersonalDetailPhoneHm);
            AddColumn(Col_SignatoryPersonalDetailPhoneMb);
            AddColumn(Col_SignatoryPersonalDetailDOBDay);
            AddColumn(Col_SignatoryPersonalDetailDOBMonth);
            AddColumn(Col_SignatoryPersonalDetailDOBYear);
            AddColumn(Col_SignatoryPersonalDetailEmail);
            AddColumn(Col_AddressAddress1);
            AddColumn(Col_AddressAddress2);
            AddColumn(Col_AddressAddress3);
            AddColumn(Col_AddressSuburb);
            AddColumn(Col_AddressState);
            AddColumn(Col_AddressPostcode);
            AddColumn(Col_AddressCountry);
            AddColumn(Col_SignatoryPersonalDetailPhoneHmPrefix);
            AddColumn(Col_SignatoryPersonalDetailPhonewkPrefix);
            AddColumn(Col_TFNDetailTFN_2);
            AddColumn(Col_TFNDetailNRC_2);
            AddColumn(Col_TFNDetailExemptionCategory_2);
            AddColumn(Col_TFNDetailTIN_2);
            AddColumn(Col_SignatoryPersonalDetailOccupation_2);
            AddColumn(Col_SignatoryPersonalDetailEmployer_2);
            AddColumn(Col_SignatoryPersonalDetailCountryOfResidence_2);
            AddColumn(Col_SignatoryPersonalDetailTitle_2);
            AddColumn(Col_SignatoryPersonalDetailTitleOther_2);
            AddColumn(Col_SignatoryPersonalDetailGivenName_2);
            AddColumn(Col_SignatoryPersonalDetailSurname_2);
            AddColumn(Col_SignatoryPersonalDetailPhoneWk_2);
            AddColumn(Col_SignatoryPersonalDetailPhoneHm_2);
            AddColumn(Col_SignatoryPersonalDetailPhoneMb_2);
            AddColumn(Col_SignatoryPersonalDetailDOBDay_2);
            AddColumn(Col_SignatoryPersonalDetailDOBMonth_2);
            AddColumn(Col_SignatoryPersonalDetailDOBYear_2);
            AddColumn(Col_SignatoryPersonalDetailEmail_2);
            AddColumn(Col_AddressAddress1_2);
            AddColumn(Col_AddressAddress2_2);
            AddColumn(Col_AddressAddress3_2);
            AddColumn(Col_AddressSuburb_2);
            AddColumn(Col_AddressState_2);
            AddColumn(Col_AddressPostcode_2);
            AddColumn(Col_AddressCountry_2);
            AddColumn(Col_SignatoryPersonalDetailPhoneHmPrefix_2);
            AddColumn(Col_SignatoryPersonalDetailPhonewkPrefix_2);
            AddColumn(Col_TFNDetailTFN_3);
            AddColumn(Col_TFNDetailNRC_3);
            AddColumn(Col_TFNDetailExemptionCategory_3);
            AddColumn(Col_TFNDetailTIN_3);
            AddColumn(Col_SignatoryPersonalDetailOccupation_3);
            AddColumn(Col_SignatoryPersonalDetailEmployer_3);
            AddColumn(Col_SignatoryPersonalDetailCountryOfResidence_3);
            AddColumn(Col_SignatoryPersonalDetailTitle_3);
            AddColumn(Col_SignatoryPersonalDetailTitleOther_3);
            AddColumn(Col_SignatoryPersonalDetailGivenName_3);
            AddColumn(Col_SignatoryPersonalDetailSurname_3);
            AddColumn(Col_SignatoryPersonalDetailPhoneWk_3);
            AddColumn(Col_SignatoryPersonalDetailPhoneHm_3);
            AddColumn(Col_SignatoryPersonalDetailPhoneMb_3);
            AddColumn(Col_SignatoryPersonalDetailDOBDay_3);
            AddColumn(Col_SignatoryPersonalDetailDOBMonth_3);
            AddColumn(Col_SignatoryPersonalDetailDOBYear_3);
            AddColumn(Col_SignatoryPersonalDetailEmail_3);
            AddColumn(Col_AddressAddress1_3);
            AddColumn(Col_AddressAddress2_3);
            AddColumn(Col_AddressAddress3_3);
            AddColumn(Col_AddressSuburb_3);
            AddColumn(Col_AddressState_3);
            AddColumn(Col_AddressPostcode_3);
            AddColumn(Col_AddressCountry_3);
            AddColumn(Col_SignatoryPersonalDetailPhoneHmPrefix_3);
            AddColumn(Col_SignatoryPersonalDetailPhonewkPrefix_3);
            AddColumn(Col_TFNDetailTFN_4);
            AddColumn(Col_TFNDetailNRC_4);
            AddColumn(Col_TFNDetailExemptionCategory_4);
            AddColumn(Col_TFNDetailTIN_4);
            AddColumn(Col_SignatoryPersonalDetailOccupation_4);
            AddColumn(Col_SignatoryPersonalDetailEmployer_4);
            AddColumn(Col_SignatoryPersonalDetailCountryOfResidence_4);
            AddColumn(Col_SignatoryPersonalDetailTitle_4);
            AddColumn(Col_SignatoryPersonalDetailTitleOther_4);
            AddColumn(Col_SignatoryPersonalDetailGivenName_4);
            AddColumn(Col_SignatoryPersonalDetailSurname_4);
            AddColumn(Col_SignatoryPersonalDetailPhoneWk_4);
            AddColumn(Col_SignatoryPersonalDetailPhoneHm_4);
            AddColumn(Col_SignatoryPersonalDetailPhoneMb_4);
            AddColumn(Col_SignatoryPersonalDetailDOBDay_4);
            AddColumn(Col_SignatoryPersonalDetailDOBMonth_4);
            AddColumn(Col_SignatoryPersonalDetailDOBYear_4);
            AddColumn(Col_SignatoryPersonalDetailEmail_4);
            AddColumn(Col_AddressAddress1_4);
            AddColumn(Col_AddressAddress2_4);
            AddColumn(Col_AddressAddress3_4);
            AddColumn(Col_AddressSuburb_4);
            AddColumn(Col_AddressState_4);
            AddColumn(Col_AddressPostcode_4);
            AddColumn(Col_AddressCountry_4);
            AddColumn(Col_SignatoryPersonalDetailPhoneHmPrefix_4);
            AddColumn(Col_SignatoryPersonalDetailPhonewkPrefix_4);
            AddColumn(Col_TFNDetailTFN_5);
            AddColumn(Col_TFNDetailNRC_5);
            AddColumn(Col_TFNDetailExemptionCategory_5);
            AddColumn(Col_TFNDetailTIN_5);
            AddColumn(Col_CorrespondenceAddressAddress1);
            AddColumn(Col_CorrespondenceAddressAddress2);
            AddColumn(Col_CorrespondenceAddressSuburb);
            AddColumn(Col_CorrespondenceAddressState);
            AddColumn(Col_CorrespondenceAddressPostcode);
            AddColumn(Col_CorrespondenceAddressCountry);
            AddColumn(Col_CorrespondenceAddressAddress1_2);
            AddColumn(Col_CorrespondenceAddressAddress2_2);
            AddColumn(Col_CorrespondenceAddressSuburb_2);
            AddColumn(Col_CorrespondenceAddressState_2);
            AddColumn(Col_CorrespondenceAddressPostcode_2);
            AddColumn(Col_CorrespondenceAddressCountry_2);
            AddColumn(Col_CorrespondenceAddressAddress1_3);
            AddColumn(Col_CorrespondenceAddressAddress2_3);
            AddColumn(Col_CorrespondenceAddressAddress3_3);
            AddColumn(Col_CorrespondenceAddressSuburb_3);
            AddColumn(Col_CorrespondenceAddressState_3);
            AddColumn(Col_CorrespondenceAddressPostcode_3);
            AddColumn(Col_CorrespondenceAddressCountry_3);
            AddColumn(Col_PlatformName);
            AddColumn(Col_PlatformCode);

            dtAOBusiness.Rows.Add(dtAOBusiness.NewRow());
        }

        private void AddColumn(string columnName)
        {
            if (!dtAOBusiness.Columns.Contains(columnName))
            {
                DataColumn dc = dtAOBusiness.Columns.Add(columnName);
                dc.DefaultValue = string.Empty;
            }
        }

        public void SetColumnValue(string columnName, string colunmValue)
        {
            if (dtAOBusiness.Columns.Contains(columnName))
                dtAOBusiness.Rows[0][columnName] = colunmValue;
        }

        public string ToCSV()
        {
            StringBuilder str = new StringBuilder();
            foreach (DataRow dr in dtAOBusiness.Rows)
            {
                foreach (object field in dr.ItemArray)
                {
                    string value = field.ToString();
                    if (value.IndexOfAny(new char[] { '"', ',' }) != -1)
                    {
                        // Special handling for values that contain comma or quote
                        // Enclose in quotes and double up any double quotes
                        str.AppendFormat("\"{0}\"" + ",", value.Replace("\"", "\"\""));
                    }
                    else str.Append(value + ",");
                }
                str.Replace(",", Environment.NewLine, str.Length - 1, 1);
            }
            return str.ToString();
        }

        public string GetColumnCSV()
        {
            StringBuilder str = new StringBuilder();
            foreach (DataColumn dc in dtAOBusiness.Columns)
            {
                str.Append(dc.ColumnName + ",");
            }
            str.Replace(",", Environment.NewLine, str.Length - 1, 1);
            return str.ToString();
        }
    }

    public class AdviserDetailsCSVColumns
    {
        public readonly string Col_ID = "ID";
        public readonly string Col_Name = "Name";
        public readonly string Col_Surname = "Surname";
        public readonly string Col_AuthorisedRepNo = "AuthorisedRepNo";
        public readonly string Col_Addressline1 = "Addressline1";
        public readonly string Col_Addressline2 = "Addressline2";
        public readonly string Col_Country = "Country";
        public readonly string Col_PostCode = "PostCode";
        public readonly string Col_Suburb = "Suburb";
        public readonly string Col_State = "State";
        public readonly string Col_HomePhoneNumber = "HomePhoneNumber";
        public readonly string Col_WorkPhoneNumber = "WorkPhoneNumber";
        public readonly string Col_MobilePhoneNumber = "MobilePhoneNumber";
        public readonly string Col_Facsimile = "Facsimile";
        public readonly string Col_Email = "Email";
        public readonly string Col_Title = "Title";
        public readonly string Col_Occupation = "Occupation";
        public readonly string Col_BTWRAPAdviserCode = "BTWRAPAdviserCode";
        public readonly string Col_DesktopBrokerAdviserCode = "DesktopBrokerAdviserCode";
        public readonly string Col_AFSLicenseeName = "AFSLicenseeName";
        public readonly string Col_AFSLicenceNumber = "AFSLicenceNumber";
        public readonly string Col_BankWestAdvisorCode = "BankWestAdvisorCode";
        public readonly string Col_IFA = "IFA";
        public readonly string Col_DealerGroup = "Dealer Group";

        private DataTable tblAdviser;

        public DataTable Table { get { return this.tblAdviser; } }

        public AdviserDetailsCSVColumns()
        {
            tblAdviser = new DataTable("tblAdviser");
            AddColumn(Col_ID);
            AddColumn(Col_Name);
            AddColumn(Col_Surname);
            AddColumn(Col_AuthorisedRepNo);
            AddColumn(Col_Addressline1);
            AddColumn(Col_Addressline2);
            AddColumn(Col_Country);
            AddColumn(Col_PostCode);
            AddColumn(Col_Suburb);
            AddColumn(Col_State);
            AddColumn(Col_HomePhoneNumber);
            AddColumn(Col_WorkPhoneNumber);
            AddColumn(Col_MobilePhoneNumber);
            AddColumn(Col_Facsimile);
            AddColumn(Col_Email);
            AddColumn(Col_Title);
            AddColumn(Col_Occupation);
            AddColumn(Col_BTWRAPAdviserCode);
            AddColumn(Col_DesktopBrokerAdviserCode);
            AddColumn(Col_AFSLicenseeName);
            AddColumn(Col_AFSLicenceNumber);
            AddColumn(Col_BankWestAdvisorCode);
            AddColumn(Col_IFA);
            AddColumn(Col_DealerGroup);

            tblAdviser.Rows.Add(tblAdviser.NewRow());
        }

        private void AddColumn(string columnName)
        {
            if (!tblAdviser.Columns.Contains(columnName))
            {
                DataColumn dc = tblAdviser.Columns.Add(columnName);
                dc.DefaultValue = string.Empty;
            }
        }

        public void SetColumnValue(string columnName, string colunmValue)
        {
            if (tblAdviser.Columns.Contains(columnName))
                tblAdviser.Rows[0][columnName] = colunmValue;
        }

        public string ToCSV()
        {
            StringBuilder str = new StringBuilder();
            foreach (DataRow dr in tblAdviser.Rows)
            {
                foreach (object field in dr.ItemArray)
                {
                    string value = field.ToString();
                    if (value.IndexOfAny(new char[] { '"', ',' }) != -1)
                    {
                        // Special handling for values that contain comma or quote
                        // Enclose in quotes and double up any double quotes
                        str.AppendFormat("\"{0}\"" + ",", value.Replace("\"", "\"\""));
                    }
                    else str.Append(value + ",");
                }
                str.Replace(",", Environment.NewLine, str.Length - 1, 1);
            }
            return str.ToString();
        }

        public string GetColumnCSV()
        {
            StringBuilder str = new StringBuilder();
            foreach (DataColumn dc in tblAdviser.Columns)
            {
                str.Append(dc.ColumnName + ",");
            }
            str.Replace(",", Environment.NewLine, str.Length - 1, 1);
            return str.ToString();
        }
    }

    public class AOPersonalCSVColumns
    {
        public readonly string Col_ApplicationID = "ApplicationID";
        public readonly string Col_ApplicationDate = "ApplicationDate";
        public readonly string Col_OpenAccountApplicationDealerName = "OpenAccountApplicationDealerName";
        public readonly string Col_OpenAccountApplicationAdviserName = "OpenAccountApplicationAdviserName";
        public readonly string Col_OpenAccountApplicationAdviserCode = "OpenAccountApplicationAdviserCode";
        public readonly string Col_OpenAccountApplicationClientAccountDesignation = "OpenAccountApplicationClientAccountDesignation";
        public readonly string Col_SignatoryPersonalDetailOccupation = "SignatoryPersonalDetailOccupation";
        public readonly string Col_SignatoryPersonalDetailEmployer = "SignatoryPersonalDetailEmployer";
        public readonly string Col_SignatoryPersonalDetailCountryOfResidence = "SignatoryPersonalDetailCountryOfResidence";
        public readonly string Col_SignatoryPersonalDetailTitle = "SignatoryPersonalDetailTitle";
        public readonly string Col_SignatoryPersonalDetailTitleOther = "SignatoryPersonalDetailTitleOther";
        public readonly string Col_SignatoryPersonalDetailGivenName = "SignatoryPersonalDetailGivenName";
        public readonly string Col_SignatoryPersonalDetailSurname = "SignatoryPersonalDetailSurname";
        public readonly string Col_SignatoryPersonalDetailPhoneWk = "SignatoryPersonalDetailPhoneWk";
        public readonly string Col_SignatoryPersonalDetailPhoneHm = "SignatoryPersonalDetailPhoneHm";
        public readonly string Col_SignatoryPersonalDetailPhoneMb = "SignatoryPersonalDetailPhoneMb";
        public readonly string Col_SignatoryPersonalDetailDOBDay = "SignatoryPersonalDetailDOBDay";
        public readonly string Col_SignatoryPersonalDetailDOBMonth = "SignatoryPersonalDetailDOBMonth";
        public readonly string Col_SignatoryPersonalDetailDOBYear = "SignatoryPersonalDetailDOBYear";
        public readonly string Col_SignatoryPersonalDetailEmail = "SignatoryPersonalDetailEmail";
        public readonly string Col_AddressAddress1 = "AddressAddress1";
        public readonly string Col_AddressAddress2 = "AddressAddress2";
        public readonly string Col_AddressAddress3 = "AddressAddress3";
        public readonly string Col_AddressSuburb = "AddressSuburb";
        public readonly string Col_AddressState = "AddressState";
        public readonly string Col_AddressPostcode = "AddressPostcode";
        public readonly string Col_AddressCountry = "AddressCountry";
        public readonly string Col_SignatoryPersonalDetailPhoneHmPrefix = "SignatoryPersonalDetailPhoneHmPrefix";
        public readonly string Col_SignatoryPersonalDetailPhonewkPrefix = "SignatoryPersonalDetailPhonewkPrefix";
        public readonly string Col_TFNDetailTFN_2 = "TFNDetailTFN_2";
        public readonly string Col_TFNDetailNRC_2 = "TFNDetailNRC_2";
        public readonly string Col_TFNDetailExemptionCategory_2 = "TFNDetailExemptionCategory_2";
        public readonly string Col_TFNDetailTIN_2 = "TFNDetailTIN_2";
        public readonly string Col_SignatoryPersonalDetailOccupation_2 = "SignatoryPersonalDetailOccupation_2";
        public readonly string Col_SignatoryPersonalDetailEmployer_2 = "SignatoryPersonalDetailEmployer_2";
        public readonly string Col_SignatoryPersonalDetailCountryOfResidence_2 = "SignatoryPersonalDetailCountryOfResidence_2";
        public readonly string Col_SignatoryPersonalDetailTitle_2 = "SignatoryPersonalDetailTitle_2";
        public readonly string Col_SignatoryPersonalDetailTitleOther_2 = "SignatoryPersonalDetailTitleOther_2";
        public readonly string Col_SignatoryPersonalDetailGivenName_2 = "SignatoryPersonalDetailGivenName_2";
        public readonly string Col_SignatoryPersonalDetailSurname_2 = "SignatoryPersonalDetailSurname_2";
        public readonly string Col_SignatoryPersonalDetailPhoneWk_2 = "SignatoryPersonalDetailPhoneWk_2";
        public readonly string Col_SignatoryPersonalDetailPhoneHm_2 = "SignatoryPersonalDetailPhoneHm_2";
        public readonly string Col_SignatoryPersonalDetailPhoneMb_2 = "SignatoryPersonalDetailPhoneMb_2";
        public readonly string Col_SignatoryPersonalDetailDOBDay_2 = "SignatoryPersonalDetailDOBDay_2";
        public readonly string Col_SignatoryPersonalDetailDOBMonth_2 = "SignatoryPersonalDetailDOBMonth_2";
        public readonly string Col_SignatoryPersonalDetailDOBYear_2 = "SignatoryPersonalDetailDOBYear_2";
        public readonly string Col_SignatoryPersonalDetailEmail_2 = "SignatoryPersonalDetailEmail_2";
        public readonly string Col_AddressAddress1_2 = "AddressAddress1_2";
        public readonly string Col_AddressAddress2_2 = "AddressAddress2_2";
        public readonly string Col_AddressAddress3_2 = "AddressAddress3_2";
        public readonly string Col_AddressSuburb_2 = "AddressSuburb_2";
        public readonly string Col_AddressState_2 = "AddressState_2";
        public readonly string Col_AddressPostcode_2 = "AddressPostcode_2";
        public readonly string Col_AddressCountry_2 = "AddressCountry_2";
        public readonly string Col_SignatoryPersonalDetailPhoneHmPrefix_2 = "SignatoryPersonalDetailPhoneHmPrefix_2";
        public readonly string Col_SignatoryPersonalDetailPhonewkPrefix_2 = "SignatoryPersonalDetailPhonewkPrefix_2";
        public readonly string Col_TFNDetailTFN_3 = "TFNDetailTFN_3";
        public readonly string Col_TFNDetailNRC_3 = "TFNDetailNRC_3";
        public readonly string Col_TFNDetailExemptionCategory_3 = "TFNDetailExemptionCategory_3";
        public readonly string Col_TFNDetailTIN_3 = "TFNDetailTIN_3";
        public readonly string Col_SignatoryPersonalDetailOccupation_3 = "SignatoryPersonalDetailOccupation_3";
        public readonly string Col_SignatoryPersonalDetailEmployer_3 = "SignatoryPersonalDetailEmployer_3";
        public readonly string Col_SignatoryPersonalDetailCountryOfResidence_3 = "SignatoryPersonalDetailCountryOfResidence_3";
        public readonly string Col_SignatoryPersonalDetailTitle_3 = "SignatoryPersonalDetailTitle_3";
        public readonly string Col_SignatoryPersonalDetailTitleOther_3 = "SignatoryPersonalDetailTitleOther_3";
        public readonly string Col_SignatoryPersonalDetailGivenName_3 = "SignatoryPersonalDetailGivenName_3";
        public readonly string Col_SignatoryPersonalDetailSurname_3 = "SignatoryPersonalDetailSurname_3";
        public readonly string Col_SignatoryPersonalDetailPhoneWk_3 = "SignatoryPersonalDetailPhoneWk_3";
        public readonly string Col_SignatoryPersonalDetailPhoneHm_3 = "SignatoryPersonalDetailPhoneHm_3";
        public readonly string Col_SignatoryPersonalDetailPhoneMb_3 = "SignatoryPersonalDetailPhoneMb_3";
        public readonly string Col_SignatoryPersonalDetailDOBDay_3 = "SignatoryPersonalDetailDOBDay_3";
        public readonly string Col_SignatoryPersonalDetailDOBMonth_3 = "SignatoryPersonalDetailDOBMonth_3";
        public readonly string Col_SignatoryPersonalDetailDOBYear_3 = "SignatoryPersonalDetailDOBYear_3";
        public readonly string Col_SignatoryPersonalDetailEmail_3 = "SignatoryPersonalDetailEmail_3";
        public readonly string Col_AddressAddress1_3 = "AddressAddress1_3";
        public readonly string Col_AddressAddress2_3 = "AddressAddress2_3";
        public readonly string Col_AddressAddress3_3 = "AddressAddress3_3";
        public readonly string Col_AddressSuburb_3 = "AddressSuburb_3";
        public readonly string Col_AddressState_3 = "AddressState_3";
        public readonly string Col_AddressPostcode_3 = "AddressPostcode_3";
        public readonly string Col_AddressCountry_3 = "AddressCountry_3";
        public readonly string Col_AddressCareOf_3 = "AddressCareOf_3";
        public readonly string Col_SignatoryPersonalDetailPhoneHmPrefix_3 = "SignatoryPersonalDetailPhoneHmPrefix_3";
        public readonly string Col_SignatoryPersonalDetailPhonewkPrefix_3 = "SignatoryPersonalDetailPhonewkPrefix_3";
        public readonly string Col_TFNDetailTFN_4 = "TFNDetailTFN_4";
        public readonly string Col_TFNDetailNRC_4 = "TFNDetailNRC_4";
        public readonly string Col_TFNDetailExemptionCategory_4 = "TFNDetailExemptionCategory_4";
        public readonly string Col_TFNDetailTIN_4 = "TFNDetailTIN_4";
        public readonly string Col_SignatoryPersonalDetailOccupation_4 = "SignatoryPersonalDetailOccupation_4";
        public readonly string Col_SignatoryPersonalDetailEmployer_4 = "SignatoryPersonalDetailEmployer_4";
        public readonly string Col_SignatoryPersonalDetailCountryOfResidence_4 = "SignatoryPersonalDetailCountryOfResidence_4";
        public readonly string Col_SignatoryPersonalDetailTitle_4 = "SignatoryPersonalDetailTitle_4";
        public readonly string Col_SignatoryPersonalDetailTitleOther_4 = "SignatoryPersonalDetailTitleOther_4";
        public readonly string Col_SignatoryPersonalDetailGivenName_4 = "SignatoryPersonalDetailGivenName_4";
        public readonly string Col_SignatoryPersonalDetailSurname_4 = "SignatoryPersonalDetailSurname_4";
        public readonly string Col_SignatoryPersonalDetailPhoneWk_4 = "SignatoryPersonalDetailPhoneWk_4";
        public readonly string Col_SignatoryPersonalDetailPhoneHm_4 = "SignatoryPersonalDetailPhoneHm_4";
        public readonly string Col_SignatoryPersonalDetailPhoneMb_4 = "SignatoryPersonalDetailPhoneMb_4";
        public readonly string Col_SignatoryPersonalDetailDOBDay_4 = "SignatoryPersonalDetailDOBDay_4";
        public readonly string Col_SignatoryPersonalDetailDOBMonth_4 = "SignatoryPersonalDetailDOBMonth_4";
        public readonly string Col_SignatoryPersonalDetailDOBYear_4 = "SignatoryPersonalDetailDOBYear_4";
        public readonly string Col_SignatoryPersonalDetailEmail_4 = "SignatoryPersonalDetailEmail_4";
        public readonly string Col_AddressAddress1_4 = "AddressAddress1_4";
        public readonly string Col_AddressAddress2_4 = "AddressAddress2_4";
        public readonly string Col_AddressAddress3_4 = "AddressAddress3_4";
        public readonly string Col_AddressSuburb_4 = "AddressSuburb_4";
        public readonly string Col_AddressState_4 = "AddressState_4";
        public readonly string Col_AddressPostcode_4 = "AddressPostcode_4";
        public readonly string Col_AddressCountry_4 = "AddressCountry_4";
        public readonly string Col_SignatoryPersonalDetailPhoneHmPrefix_4 = "SignatoryPersonalDetailPhoneHmPrefix_4";
        public readonly string Col_SignatoryPersonalDetailPhonewkPrefix_4 = "SignatoryPersonalDetailPhonewkPrefix_4";
        public readonly string Col_TFNDetailTFN_5 = "TFNDetailTFN_5";
        public readonly string Col_TFNDetailNRC_5 = "TFNDetailNRC_5";
        public readonly string Col_TFNDetailExemptionCategory_5 = "TFNDetailExemptionCategory_5";
        public readonly string Col_TFNDetailTIN_5 = "TFNDetailTIN_5";
        public readonly string Col_CorrespondenceAddressAddress1 = "CorrespondenceAddressAddress1";
        public readonly string Col_CorrespondenceAddressAddress2 = "CorrespondenceAddressAddress2";
        public readonly string Col_CorrespondenceAddressSuburb = "CorrespondenceAddressSuburb";
        public readonly string Col_CorrespondenceAddressState = "CorrespondenceAddressState";
        public readonly string Col_CorrespondenceAddressPostcode = "CorrespondenceAddressPostcode";
        public readonly string Col_CorrespondenceAddressCountry = "CorrespondenceAddressCountry";
        public readonly string Col_CorrespondenceAddressAddress1_2 = "CorrespondenceAddressAddress1_2";
        public readonly string Col_CorrespondenceAddressAddress2_2 = "CorrespondenceAddressAddress2_2";
        public readonly string Col_CorrespondenceAddressSuburb_2 = "CorrespondenceAddressSuburb_2";
        public readonly string Col_CorrespondenceAddressState_2 = "CorrespondenceAddressState_2";
        public readonly string Col_CorrespondenceAddressPostcode_2 = "CorrespondenceAddressPostcode_2";
        public readonly string Col_CorrespondenceAddressCountry_2 = "CorrespondenceAddressCountry_2";
        public readonly string Col_CorrespondenceAddressAddress1_3 = "CorrespondenceAddressAddress1_3";
        public readonly string Col_CorrespondenceAddressAddress2_3 = "CorrespondenceAddressAddress2_3";
        public readonly string Col_CorrespondenceAddressAddress3_3 = "CorrespondenceAddressAddress3_3";
        public readonly string Col_CorrespondenceAddressSuburb_3 = "CorrespondenceAddressSuburb_3";
        public readonly string Col_CorrespondenceAddressState_3 = "CorrespondenceAddressState_3";
        public readonly string Col_CorrespondenceAddressPostcode_3 = "CorrespondenceAddressPostcode_3";
        public readonly string Col_CorrespondenceAddressCountry_3 = "CorrespondenceAddressCountry_3";
        public readonly string Col_PlatformName = "PlatformName";
        public readonly string Col_PlatformCode = "PlatformCode";

        private DataTable dtAOPersonal;

        public DataTable Table { get { return this.dtAOPersonal; } }

        public AOPersonalCSVColumns()
        {
            dtAOPersonal = new DataTable("tblAOPersonal");
            AddColumn(Col_ApplicationID);
            AddColumn(Col_ApplicationDate);
            AddColumn(Col_OpenAccountApplicationDealerName);
            AddColumn(Col_OpenAccountApplicationAdviserName);
            AddColumn(Col_OpenAccountApplicationAdviserCode);
            AddColumn(Col_OpenAccountApplicationClientAccountDesignation);
            AddColumn(Col_SignatoryPersonalDetailOccupation);
            AddColumn(Col_SignatoryPersonalDetailEmployer);
            AddColumn(Col_SignatoryPersonalDetailCountryOfResidence);
            AddColumn(Col_SignatoryPersonalDetailTitle);
            AddColumn(Col_SignatoryPersonalDetailTitleOther);
            AddColumn(Col_SignatoryPersonalDetailGivenName);
            AddColumn(Col_SignatoryPersonalDetailSurname);
            AddColumn(Col_SignatoryPersonalDetailPhoneWk);
            AddColumn(Col_SignatoryPersonalDetailPhoneHm);
            AddColumn(Col_SignatoryPersonalDetailPhoneMb);
            AddColumn(Col_SignatoryPersonalDetailDOBDay);
            AddColumn(Col_SignatoryPersonalDetailDOBMonth);
            AddColumn(Col_SignatoryPersonalDetailDOBYear);
            AddColumn(Col_SignatoryPersonalDetailEmail);
            AddColumn(Col_AddressAddress1);
            AddColumn(Col_AddressAddress2);
            AddColumn(Col_AddressAddress3);
            AddColumn(Col_AddressSuburb);
            AddColumn(Col_AddressState);
            AddColumn(Col_AddressPostcode);
            AddColumn(Col_AddressCountry);
            AddColumn(Col_SignatoryPersonalDetailPhoneHmPrefix);
            AddColumn(Col_SignatoryPersonalDetailPhonewkPrefix);
            AddColumn(Col_TFNDetailTFN_2);
            AddColumn(Col_TFNDetailNRC_2);
            AddColumn(Col_TFNDetailExemptionCategory_2);
            AddColumn(Col_TFNDetailTIN_2);
            AddColumn(Col_SignatoryPersonalDetailOccupation_2);
            AddColumn(Col_SignatoryPersonalDetailEmployer_2);
            AddColumn(Col_SignatoryPersonalDetailCountryOfResidence_2);
            AddColumn(Col_SignatoryPersonalDetailTitle_2);
            AddColumn(Col_SignatoryPersonalDetailTitleOther_2);
            AddColumn(Col_SignatoryPersonalDetailGivenName_2);
            AddColumn(Col_SignatoryPersonalDetailSurname_2);
            AddColumn(Col_SignatoryPersonalDetailPhoneWk_2);
            AddColumn(Col_SignatoryPersonalDetailPhoneHm_2);
            AddColumn(Col_SignatoryPersonalDetailPhoneMb_2);
            AddColumn(Col_SignatoryPersonalDetailDOBDay_2);
            AddColumn(Col_SignatoryPersonalDetailDOBMonth_2);
            AddColumn(Col_SignatoryPersonalDetailDOBYear_2);
            AddColumn(Col_SignatoryPersonalDetailEmail_2);
            AddColumn(Col_AddressAddress1_2);
            AddColumn(Col_AddressAddress2_2);
            AddColumn(Col_AddressAddress3_2);
            AddColumn(Col_AddressSuburb_2);
            AddColumn(Col_AddressState_2);
            AddColumn(Col_AddressPostcode_2);
            AddColumn(Col_AddressCountry_2);
            AddColumn(Col_SignatoryPersonalDetailPhoneHmPrefix_2);
            AddColumn(Col_SignatoryPersonalDetailPhonewkPrefix_2);
            AddColumn(Col_TFNDetailTFN_3);
            AddColumn(Col_TFNDetailNRC_3);
            AddColumn(Col_TFNDetailExemptionCategory_3);
            AddColumn(Col_TFNDetailTIN_3);
            AddColumn(Col_SignatoryPersonalDetailOccupation_3);
            AddColumn(Col_SignatoryPersonalDetailEmployer_3);
            AddColumn(Col_SignatoryPersonalDetailCountryOfResidence_3);
            AddColumn(Col_SignatoryPersonalDetailTitle_3);
            AddColumn(Col_SignatoryPersonalDetailTitleOther_3);
            AddColumn(Col_SignatoryPersonalDetailGivenName_3);
            AddColumn(Col_SignatoryPersonalDetailSurname_3);
            AddColumn(Col_SignatoryPersonalDetailPhoneWk_3);
            AddColumn(Col_SignatoryPersonalDetailPhoneHm_3);
            AddColumn(Col_SignatoryPersonalDetailPhoneMb_3);
            AddColumn(Col_SignatoryPersonalDetailDOBDay_3);
            AddColumn(Col_SignatoryPersonalDetailDOBMonth_3);
            AddColumn(Col_SignatoryPersonalDetailDOBYear_3);
            AddColumn(Col_SignatoryPersonalDetailEmail_3);
            AddColumn(Col_AddressAddress1_3);
            AddColumn(Col_AddressAddress2_3);
            AddColumn(Col_AddressAddress3_3);
            AddColumn(Col_AddressSuburb_3);
            AddColumn(Col_AddressState_3);
            AddColumn(Col_AddressPostcode_3);
            AddColumn(Col_AddressCountry_3);
            AddColumn(Col_SignatoryPersonalDetailPhoneHmPrefix_3);
            AddColumn(Col_SignatoryPersonalDetailPhonewkPrefix_3);
            AddColumn(Col_TFNDetailTFN_4);
            AddColumn(Col_TFNDetailNRC_4);
            AddColumn(Col_TFNDetailExemptionCategory_4);
            AddColumn(Col_TFNDetailTIN_4);
            AddColumn(Col_SignatoryPersonalDetailOccupation_4);
            AddColumn(Col_SignatoryPersonalDetailEmployer_4);
            AddColumn(Col_SignatoryPersonalDetailCountryOfResidence_4);
            AddColumn(Col_SignatoryPersonalDetailTitle_4);
            AddColumn(Col_SignatoryPersonalDetailTitleOther_4);
            AddColumn(Col_SignatoryPersonalDetailGivenName_4);
            AddColumn(Col_SignatoryPersonalDetailSurname_4);
            AddColumn(Col_SignatoryPersonalDetailPhoneWk_4);
            AddColumn(Col_SignatoryPersonalDetailPhoneHm_4);
            AddColumn(Col_SignatoryPersonalDetailPhoneMb_4);
            AddColumn(Col_SignatoryPersonalDetailDOBDay_4);
            AddColumn(Col_SignatoryPersonalDetailDOBMonth_4);
            AddColumn(Col_SignatoryPersonalDetailDOBYear_4);
            AddColumn(Col_SignatoryPersonalDetailEmail_4);
            AddColumn(Col_AddressAddress1_4);
            AddColumn(Col_AddressAddress2_4);
            AddColumn(Col_AddressAddress3_4);
            AddColumn(Col_AddressSuburb_4);
            AddColumn(Col_AddressState_4);
            AddColumn(Col_AddressPostcode_4);
            AddColumn(Col_AddressCountry_4);
            AddColumn(Col_SignatoryPersonalDetailPhoneHmPrefix_4);
            AddColumn(Col_SignatoryPersonalDetailPhonewkPrefix_4);
            AddColumn(Col_TFNDetailTFN_5);
            AddColumn(Col_TFNDetailNRC_5);
            AddColumn(Col_TFNDetailExemptionCategory_5);
            AddColumn(Col_TFNDetailTIN_5);
            AddColumn(Col_CorrespondenceAddressAddress1);
            AddColumn(Col_CorrespondenceAddressAddress2);
            AddColumn(Col_CorrespondenceAddressSuburb);
            AddColumn(Col_CorrespondenceAddressState);
            AddColumn(Col_CorrespondenceAddressPostcode);
            AddColumn(Col_CorrespondenceAddressCountry);
            AddColumn(Col_CorrespondenceAddressAddress1_2);
            AddColumn(Col_CorrespondenceAddressAddress2_2);
            AddColumn(Col_CorrespondenceAddressSuburb_2);
            AddColumn(Col_CorrespondenceAddressState_2);
            AddColumn(Col_CorrespondenceAddressPostcode_2);
            AddColumn(Col_CorrespondenceAddressCountry_2);
            AddColumn(Col_CorrespondenceAddressAddress1_3);
            AddColumn(Col_CorrespondenceAddressAddress2_3);
            AddColumn(Col_CorrespondenceAddressAddress3_3);
            AddColumn(Col_CorrespondenceAddressSuburb_3);
            AddColumn(Col_CorrespondenceAddressState_3);
            AddColumn(Col_CorrespondenceAddressPostcode_3);
            AddColumn(Col_CorrespondenceAddressCountry_3);
            AddColumn(Col_PlatformName);
            AddColumn(Col_PlatformCode);

            dtAOPersonal.Rows.Add(dtAOPersonal.NewRow());
        }

        private void AddColumn(string columnName)
        {
            if (!dtAOPersonal.Columns.Contains(columnName))
            {
                DataColumn dc = dtAOPersonal.Columns.Add(columnName);
                dc.DefaultValue = string.Empty;
            }
        }

        public void SetColumnValue(string columnName, string colunmValue)
        {
            if (dtAOPersonal.Columns.Contains(columnName))
                dtAOPersonal.Rows[0][columnName] = colunmValue;
        }

        public string ToCSV()
        {
            StringBuilder str = new StringBuilder();
            foreach (DataRow dr in dtAOPersonal.Rows)
            {
                foreach (object field in dr.ItemArray)
                {
                    string value = field.ToString();
                    if (value.IndexOfAny(new char[] { '"', ',' }) != -1)
                    {
                        // Special handling for values that contain comma or quote
                        // Enclose in quotes and double up any double quotes
                        str.AppendFormat("\"{0}\"" + ",", value.Replace("\"", "\"\""));
                    }
                    else str.Append(value + ",");
                }
                str.Replace(",", Environment.NewLine, str.Length - 1, 1);
            }
            return str.ToString();
        }

        public string GetColumnCSV()
        {
            StringBuilder str = new StringBuilder();
            foreach (DataColumn dc in dtAOPersonal.Columns)
            {
                str.Append(dc.ColumnName + ",");
            }
            str.Replace(",", Environment.NewLine, str.Length - 1, 1);
            return str.ToString();
        }
    }

    public class AccountOpeningBankWestCSVColumns
    {
        #region CSV Columns Name
        public readonly string Col_ApplicationID = "ApplicationID";
        public readonly string Col_ApplicationDate = "ApplicationDate";
        public readonly string Col_ApplicationStatus = "ApplicationStatus";

        public readonly string Col_CMTOpenDate = "CMTOpenDate";

        public readonly string Col_OpenAccountApplicationCreatorUserID = "OpenAccountApplicationCreatorUserID";
        public readonly string Col_OpenAccountApplicationApplicantType = "OpenAccountApplicationApplicantType";
        public readonly string Col_OpenAccountApplicationAccountOption = "OpenAccountApplicationAccountOption";
        public readonly string Col_OpenAccountApplicationProductType = "OpenAccountApplicationProductType";
        public readonly string Col_OpenAccountApplicationTrusteesCompany = "OpenAccountApplicationTrusteesCompany";

        public readonly string Col_TrusteesCompanyDetailEntityName1 = "TrusteesCompanyDetailEntityName1";
        public readonly string Col_TrusteesCompanyDetailEntityName2 = "TrusteesCompanyDetailEntityName2";
        public readonly string Col_TrusteesCompanyDetailACNABN = "TrusteesCompanyDetailACNABN";
        public readonly string Col_TrusteesCompanyDetailABN = "TrusteesCompanyDetailABN";
        public readonly string Col_TrusteesCompanyDetailACNARBN = "TrusteesCompanyDetailACNARBN";
        public readonly string Col_TrusteesCompanyDetailContactName = "TrusteesCompanyDetailContactName";
        public readonly string Col_TrusteesCompanyDetailContactEmail = "TrusteesCompanyDetailContactEmail";
        public readonly string Col_TrusteesCompanyDetailContactPhPrefix = "TrusteesCompanyDetailContactPhPrefix";
        public readonly string Col_TrusteesCompanyDetailContactPh = "TrusteesCompanyDetailContactPh";
        public readonly string Col_TrusteesCompanyDetailCountryOfEstablishment = "TrusteesCompanyDetailCountryOfEstablishment";
        public readonly string Col_TrusteesCompanyDetailTrustType = "TrusteesCompanyDetailTrustType";

        public readonly string Col_TFNDetailTFN = "TFNDetailTFN";
        public readonly string Col_TFNDetailNRC = "TFNDetailNRC";
        public readonly string Col_TFNDetailExemptionCategory = "TFNDetailExemptionCategory";
        public readonly string Col_TFNDetailTIN = "TFNDetailTIN";

        public readonly string Col_CorpTrusteeDetailEntityName1 = "CorpTrusteeDetailEntityName1";
        public readonly string Col_CorpTrusteeDetailEntityName2 = "CorpTrusteeDetailEntityName2";
        public readonly string Col_CorpTrusteeDetailACNABN = "CorpTrusteeDetailACNABN";
        public readonly string Col_CorpTrusteeDetailABN = "CorpTrusteeDetailABN";
        public readonly string Col_CorpTrusteeDetailACNARBN = "CorpTrusteeDetailACNARBN";
        public readonly string Col_CorpTrusteeDetailCountryOfEstablishment = "CorpTrusteeDetailCountryOfEstablishment";

        public readonly string Col_OpenAccountApplicationInitialInvestmentAmount = "OpenAccountApplicationInitialInvestmentAmount";
        public readonly string Col_OpenAccountApplicationMannerOfOperation = "OpenAccountApplicationMannerOfOperation";
        public readonly string Col_OpenAccountApplicationFinancialAdviserAccess = "OpenAccountApplicationFinancialAdviserAccess";
        public readonly string Col_OpenAccountApplicationAdvisorFirmAccess = "OpenAccountApplicationAdvisorFirmAccess";
        public readonly string Col_OpenAccountApplicationLimitedAccessApproved = "OpenAccountApplicationLimitedAccessApproved";

        public readonly string Col_AccessFacilityType = "AccessFacilityType";
        public readonly string Col_AccessFacilitySelected = "AccessFacilitySelected";
        public readonly string Col_AccessFacilityType_2 = "AccessFacilityType_2";
        public readonly string Col_AccessFacilitySelected_2 = "AccessFacilitySelected_2";
        public readonly string Col_AccessFacilityType_3 = "AccessFacilityType_3";
        public readonly string Col_AccessFacilitySelected_3 = "AccessFacilitySelected_3";
        public readonly string Col_AccessFacilityType_4 = "AccessFacilityType_4";
        public readonly string Col_AccessFacilitySelected_4 = "AccessFacilitySelected_4";
        public readonly string Col_AccessFacilityType_5 = "AccessFacilityType_5";
        public readonly string Col_AccessFacilitySelected_5 = "AccessFacilitySelected_5";

        public readonly string Col_OpenAccountApplicationDealerName = "OpenAccountApplicationDealerName";
        public readonly string Col_OpenAccountApplicationAdviserName = "OpenAccountApplicationAdviserName";
        public readonly string Col_OpenAccountApplicationAdviserCode = "OpenAccountApplicationAdviserCode";
        public readonly string Col_OpenAccountApplicationCMTaccountNumber = "OpenAccountApplicationCMTaccountNumber";
        public readonly string Col_OpenAccountApplicationCustomerAccountNumber = "OpenAccountApplicationCustomerAccountNumber";
        public readonly string Col_OpenAccountApplicationCMTaccountBSB1 = "OpenAccountApplicationCMTaccountBSB1";
        public readonly string Col_OpenAccountApplicationCMTaccountBSB2 = "OpenAccountApplicationCMTaccountBSB2";
        public readonly string Col_OpenAccountApplicationClientAccountDesignation = "OpenAccountApplicationClientAccountDesignation";
        public readonly string Col_OpenAccountApplicationPartnerCode = "OpenAccountApplicationPartnerCode";
        public readonly string Col_OpenAccountApplicationReceiptNo = "OpenAccountApplicationReceiptNo";

        public readonly string Col_SignatoryPersonalDetailOccupation = "SignatoryPersonalDetailOccupation";
        public readonly string Col_SignatoryPersonalDetailEmployer = "SignatoryPersonalDetailEmployer";
        public readonly string Col_SignatoryPersonalDetailCountryOfResidence = "SignatoryPersonalDetailCountryOfResidence";
        public readonly string Col_SignatoryPersonalDetailTitle = "SignatoryPersonalDetailTitle";
        public readonly string Col_SignatoryPersonalDetailTitleOther = "SignatoryPersonalDetailTitleOther";
        public readonly string Col_SignatoryPersonalDetailGivenName = "SignatoryPersonalDetailGivenName";
        public readonly string Col_SignatoryPersonalDetailSurname = "SignatoryPersonalDetailSurname";
        public readonly string Col_SignatoryPersonalDetailPhoneWk = "SignatoryPersonalDetailPhoneWk";
        public readonly string Col_SignatoryPersonalDetailPhoneHm = "SignatoryPersonalDetailPhoneHm";
        public readonly string Col_SignatoryPersonalDetailPhoneMb = "SignatoryPersonalDetailPhoneMb";
        public readonly string Col_SignatoryPersonalDetailDOBDay = "SignatoryPersonalDetailDOBDay";
        public readonly string Col_SignatoryPersonalDetailDOBMonth = "SignatoryPersonalDetailDOBMonth";
        public readonly string Col_SignatoryPersonalDetailDOBYear = "SignatoryPersonalDetailDOBYear";
        public readonly string Col_SignatoryPersonalDetailEmail = "SignatoryPersonalDetailEmail";
        public readonly string Col_SignatoryPersonalDetailTelephonePassword = "SignatoryPersonalDetailTelephonePassword";
        public readonly string Col_AddressAddress1 = "AddressAddress1";
        public readonly string Col_AddressAddress2 = "AddressAddress2";
        public readonly string Col_AddressAddress3 = "AddressAddress3";
        public readonly string Col_AddressSuburb = "AddressSuburb";
        public readonly string Col_AddressState = "AddressState";
        public readonly string Col_AddressPostcode = "AddressPostcode";
        public readonly string Col_AddressCountry = "AddressCountry";
        public readonly string Col_AddressCareOf = "AddressCareOf";
        public readonly string Col_SignatoryPersonalDetailPhoneHmPrefix = "SignatoryPersonalDetailPhoneHmPrefix";
        public readonly string Col_SignatoryPersonalDetailPhonewkPrefix = "SignatoryPersonalDetailPhonewkPrefix";

        public readonly string Col_TFNDetailTFN_2 = "TFNDetailTFN_2";
        public readonly string Col_TFNDetailNRC_2 = "TFNDetailNRC_2";
        public readonly string Col_TFNDetailExemptionCategory_2 = "TFNDetailExemptionCategory_2";
        public readonly string Col_TFNDetailTIN_2 = "TFNDetailTIN_2";
        public readonly string Col_SignatoryPersonalDetailOccupation_2 = "SignatoryPersonalDetailOccupation_2";
        public readonly string Col_SignatoryPersonalDetailEmployer_2 = "SignatoryPersonalDetailEmployer_2";
        public readonly string Col_SignatoryPersonalDetailCountryOfResidence_2 = "SignatoryPersonalDetailCountryOfResidence_2";
        public readonly string Col_SignatoryPersonalDetailTitle_2 = "SignatoryPersonalDetailTitle_2";
        public readonly string Col_SignatoryPersonalDetailTitleOther_2 = "SignatoryPersonalDetailTitleOther_2";
        public readonly string Col_SignatoryPersonalDetailGivenName_2 = "SignatoryPersonalDetailGivenName_2";
        public readonly string Col_SignatoryPersonalDetailSurname_2 = "SignatoryPersonalDetailSurname_2";
        public readonly string Col_SignatoryPersonalDetailPhoneWk_2 = "SignatoryPersonalDetailPhoneWk_2";
        public readonly string Col_SignatoryPersonalDetailPhoneHm_2 = "SignatoryPersonalDetailPhoneHm_2";
        public readonly string Col_SignatoryPersonalDetailPhoneMb_2 = "SignatoryPersonalDetailPhoneMb_2";
        public readonly string Col_SignatoryPersonalDetailDOBDay_2 = "SignatoryPersonalDetailDOBDay_2";
        public readonly string Col_SignatoryPersonalDetailDOBMonth_2 = "SignatoryPersonalDetailDOBMonth_2";
        public readonly string Col_SignatoryPersonalDetailDOBYear_2 = "SignatoryPersonalDetailDOBYear_2";
        public readonly string Col_SignatoryPersonalDetailEmail_2 = "SignatoryPersonalDetailEmail_2";
        public readonly string Col_SignatoryPersonalDetailTelephonePassword_2 = "SignatoryPersonalDetailTelephonePassword_2";
        public readonly string Col_AddressAddress1_2 = "AddressAddress1_2";
        public readonly string Col_AddressAddress2_2 = "AddressAddress2_2";
        public readonly string Col_AddressAddress3_2 = "AddressAddress3_2";
        public readonly string Col_AddressSuburb_2 = "AddressSuburb_2";
        public readonly string Col_AddressState_2 = "AddressState_2";
        public readonly string Col_AddressPostcode_2 = "AddressPostcode_2";
        public readonly string Col_AddressCountry_2 = "AddressCountry_2";
        public readonly string Col_AddressCareOf_2 = "AddressCareOf_2";
        public readonly string Col_SignatoryPersonalDetailPhoneHmPrefix_2 = "SignatoryPersonalDetailPhoneHmPrefix_2";
        public readonly string Col_SignatoryPersonalDetailPhonewkPrefix_2 = "SignatoryPersonalDetailPhonewkPrefix_2";

        public readonly string Col_TFNDetailTFN_3 = "TFNDetailTFN_3";
        public readonly string Col_TFNDetailNRC_3 = "TFNDetailNRC_3";
        public readonly string Col_TFNDetailExemptionCategory_3 = "TFNDetailExemptionCategory_3";
        public readonly string Col_TFNDetailTIN_3 = "TFNDetailTIN_3";
        public readonly string Col_SignatoryPersonalDetailOccupation_3 = "SignatoryPersonalDetailOccupation_3";
        public readonly string Col_SignatoryPersonalDetailEmployer_3 = "SignatoryPersonalDetailEmployer_3";
        public readonly string Col_SignatoryPersonalDetailCountryOfResidence_3 = "SignatoryPersonalDetailCountryOfResidence_3";
        public readonly string Col_SignatoryPersonalDetailTitle_3 = "SignatoryPersonalDetailTitle_3";
        public readonly string Col_SignatoryPersonalDetailTitleOther_3 = "SignatoryPersonalDetailTitleOther_3";
        public readonly string Col_SignatoryPersonalDetailGivenName_3 = "SignatoryPersonalDetailGivenName_3";
        public readonly string Col_SignatoryPersonalDetailSurname_3 = "SignatoryPersonalDetailSurname_3";
        public readonly string Col_SignatoryPersonalDetailPhoneWk_3 = "SignatoryPersonalDetailPhoneWk_3";
        public readonly string Col_SignatoryPersonalDetailPhoneHm_3 = "SignatoryPersonalDetailPhoneHm_3";
        public readonly string Col_SignatoryPersonalDetailPhoneMb_3 = "SignatoryPersonalDetailPhoneMb_3";
        public readonly string Col_SignatoryPersonalDetailDOBDay_3 = "SignatoryPersonalDetailDOBDay_3";
        public readonly string Col_SignatoryPersonalDetailDOBMonth_3 = "SignatoryPersonalDetailDOBMonth_3";
        public readonly string Col_SignatoryPersonalDetailDOBYear_3 = "SignatoryPersonalDetailDOBYear_3";
        public readonly string Col_SignatoryPersonalDetailEmail_3 = "SignatoryPersonalDetailEmail_3";
        public readonly string Col_SignatoryPersonalDetailTelephonePassword_3 = "SignatoryPersonalDetailTelephonePassword_3";
        public readonly string Col_AddressAddress1_3 = "AddressAddress1_3";
        public readonly string Col_AddressAddress2_3 = "AddressAddress2_3";
        public readonly string Col_AddressAddress3_3 = "AddressAddress3_3";
        public readonly string Col_AddressSuburb_3 = "AddressSuburb_3";
        public readonly string Col_AddressState_3 = "AddressState_3";
        public readonly string Col_AddressPostcode_3 = "AddressPostcode_3";
        public readonly string Col_AddressCountry_3 = "AddressCountry_3";
        public readonly string Col_AddressCareOf_3 = "AddressCareOf_3";
        public readonly string Col_SignatoryPersonalDetailPhoneHmPrefix_3 = "SignatoryPersonalDetailPhoneHmPrefix_3";
        public readonly string Col_SignatoryPersonalDetailPhonewkPrefix_3 = "SignatoryPersonalDetailPhonewkPrefix_3";

        public readonly string Col_TFNDetailTFN_4 = "TFNDetailTFN_4";
        public readonly string Col_TFNDetailNRC_4 = "TFNDetailNRC_4";
        public readonly string Col_TFNDetailExemptionCategory_4 = "TFNDetailExemptionCategory_4";
        public readonly string Col_TFNDetailTIN_4 = "TFNDetailTIN_4";
        public readonly string Col_SignatoryPersonalDetailOccupation_4 = "SignatoryPersonalDetailOccupation_4";
        public readonly string Col_SignatoryPersonalDetailEmployer_4 = "SignatoryPersonalDetailEmployer_4";
        public readonly string Col_SignatoryPersonalDetailCountryOfResidence_4 = "SignatoryPersonalDetailCountryOfResidence_4";
        public readonly string Col_SignatoryPersonalDetailTitle_4 = "SignatoryPersonalDetailTitle_4";
        public readonly string Col_SignatoryPersonalDetailTitleOther_4 = "SignatoryPersonalDetailTitleOther_4";
        public readonly string Col_SignatoryPersonalDetailGivenName_4 = "SignatoryPersonalDetailGivenName_4";
        public readonly string Col_SignatoryPersonalDetailSurname_4 = "SignatoryPersonalDetailSurname_4";
        public readonly string Col_SignatoryPersonalDetailPhoneWk_4 = "SignatoryPersonalDetailPhoneWk_4";
        public readonly string Col_SignatoryPersonalDetailPhoneHm_4 = "SignatoryPersonalDetailPhoneHm_4";
        public readonly string Col_SignatoryPersonalDetailPhoneMb_4 = "SignatoryPersonalDetailPhoneMb_4";
        public readonly string Col_SignatoryPersonalDetailDOBDay_4 = "SignatoryPersonalDetailDOBDay_4";
        public readonly string Col_SignatoryPersonalDetailDOBMonth_4 = "SignatoryPersonalDetailDOBMonth_4";
        public readonly string Col_SignatoryPersonalDetailDOBYear_4 = "SignatoryPersonalDetailDOBYear_4";
        public readonly string Col_SignatoryPersonalDetailEmail_4 = "SignatoryPersonalDetailEmail_4";
        public readonly string Col_SignatoryPersonalDetailTelephonePassword_4 = "SignatoryPersonalDetailTelephonePassword_4";
        public readonly string Col_AddressAddress1_4 = "AddressAddress1_4";
        public readonly string Col_AddressAddress2_4 = "AddressAddress2_4";
        public readonly string Col_AddressAddress3_4 = "AddressAddress3_4";
        public readonly string Col_AddressSuburb_4 = "AddressSuburb_4";
        public readonly string Col_AddressState_4 = "AddressState_4";
        public readonly string Col_AddressPostcode_4 = "AddressPostcode_4";
        public readonly string Col_AddressCountry_4 = "AddressCountry_4";
        public readonly string Col_AddressCareOf_4 = "AddressCareOf_4";
        public readonly string Col_SignatoryPersonalDetailPhoneHmPrefix_4 = "SignatoryPersonalDetailPhoneHmPrefix_4";
        public readonly string Col_SignatoryPersonalDetailPhonewkPrefix_4 = "SignatoryPersonalDetailPhonewkPrefix_4";

        public readonly string Col_TFNDetailTFN_5 = "TFNDetailTFN_5";
        public readonly string Col_TFNDetailNRC_5 = "TFNDetailNRC_5";
        public readonly string Col_TFNDetailExemptionCategory_5 = "TFNDetailExemptionCategory_5";
        public readonly string Col_TFNDetailTIN_5 = "TFNDetailTIN_5";

        public readonly string Col_CorrespondenceAddressAddress1 = "CorrespondenceAddressAddress1";
        public readonly string Col_CorrespondenceAddressAddress2 = "CorrespondenceAddressAddress2";
        public readonly string Col_CorrespondenceAddressAddress3 = "CorrespondenceAddressAddress3";
        public readonly string Col_CorrespondenceAddressSuburb = "CorrespondenceAddressSuburb";
        public readonly string Col_CorrespondenceAddressState = "CorrespondenceAddressState";
        public readonly string Col_CorrespondenceAddressPostcode = "CorrespondenceAddressPostcode";
        public readonly string Col_CorrespondenceAddressCountry = "CorrespondenceAddressCountry";
        public readonly string Col_CorrespondenceAddressCareOf = "CorrespondenceAddressCareOf";

        public readonly string Col_CorrespondenceAddressAddress1_2 = "CorrespondenceAddressAddress1_2";
        public readonly string Col_CorrespondenceAddressAddress2_2 = "CorrespondenceAddressAddress2_2";
        public readonly string Col_CorrespondenceAddressAddress3_2 = "CorrespondenceAddressAddress3_2";
        public readonly string Col_CorrespondenceAddressSuburb_2 = "CorrespondenceAddressSuburb_2";
        public readonly string Col_CorrespondenceAddressState_2 = "CorrespondenceAddressState_2";
        public readonly string Col_CorrespondenceAddressPostcode_2 = "CorrespondenceAddressPostcode_2";
        public readonly string Col_CorrespondenceAddressCountry_2 = "CorrespondenceAddressCountry_2";
        public readonly string Col_CorrespondenceAddressCareOf_2 = "CorrespondenceAddressCareOf_2";

        public readonly string Col_CorrespondenceAddressAddress1_3 = "CorrespondenceAddressAddress1_3";
        public readonly string Col_CorrespondenceAddressAddress2_3 = "CorrespondenceAddressAddress2_3";
        public readonly string Col_CorrespondenceAddressAddress3_3 = "CorrespondenceAddressAddress3_3";
        public readonly string Col_CorrespondenceAddressSuburb_3 = "CorrespondenceAddressSuburb_3";
        public readonly string Col_CorrespondenceAddressState_3 = "CorrespondenceAddressState_3";
        public readonly string Col_CorrespondenceAddressPostcode_3 = "CorrespondenceAddressPostcode_3";
        public readonly string Col_CorrespondenceAddressCountry_3 = "CorrespondenceAddressCountry_3";
        public readonly string Col_CorrespondenceAddressCareOf_3 = "CorrespondenceAddressCareOf_3";

        public readonly string Col_TrusteesCompanyCIF = "TrusteesCompanyCIF";
        public readonly string Col_CorpTrusteeCIF = "CorpTrusteeCIF";

        public readonly string Col_SignatoryCIF = "SignatoryCIF";
        public readonly string Col_SignatoryCIF_2 = "SignatoryCIF_2";
        public readonly string Col_SignatoryCIF_3 = "SignatoryCIF_3";
        public readonly string Col_SignatoryCIF_4 = "SignatoryCIF_4";

        public readonly string Col_PAN = "PAN";
        public readonly string Col_PAN_2 = "PAN_2";
        public readonly string Col_PAN_3 = "PAN_3";
        public readonly string Col_PAN_4 = "PAN_4";

        public readonly string Col_CBSStatus = "CBSStatus";
        public readonly string Col_Allocation = "Allocation";
        public readonly string Col_FollowupDate = "FollowupDate";
        public readonly string Col_OperationsStatus = "OperationsStatus";
        public readonly string Col_OpenAccountApplicationPartnerCIF = "OpenAccountApplicationPartnerCIF";
        public readonly string Col_OpenAccountApplicationAdviserCIF = "OpenAccountApplicationAdviserCIF";
        public readonly string Col_PlatformName = "PlatformName";
        public readonly string Col_PlatformCode = "PlatformCode";
        public readonly string Col_PlatformReference1 = "PlatformReference1";
        public readonly string Col_PlatformReference2 = "PlatformReference2";
        public readonly string Col_PlatformName_2 = "PlatformName_2";
        public readonly string Col_PlatformCode_2 = "PlatformCode_2";
        public readonly string Col_PlatformReference1_2 = "PlatformReference1_2";
        public readonly string Col_PlatformReference2_2 = "PlatformReference2_2";
        public readonly string Col_PlatformName_3 = "PlatformName_3";
        public readonly string Col_PlatformCode_3 = "PlatformCode_3";
        public readonly string Col_PlatformReference1_3 = "PlatformReference1_3";
        public readonly string Col_PlatformReference2_3 = "PlatformReference2_3";
        public readonly string Col_ThirdPartyCIFKey = "ThirdPartyCIFKey";
        public readonly string Col_ThirdPartyPartnerCode = "ThirdPartyPartnerCode";
        public readonly string Col_OpenAccountApplicationFirmName = "OpenAccountApplicationFirmName";
        public readonly string Col_OpenAccountApplicationAdvisorFirmName = "OpenAccountApplicationAdvisorFirmName";
        public readonly string Col_ReportedOn = "ReportedOn";
        public readonly string Col_ProcessingResult = "ProcessingResult";
        public readonly string Col_ProcessingIssues = "ProcessingIssues";
        public readonly string Col_ApplicationReceivedDate = "ApplicationReceivedDate";
        public readonly string Col_DocumentationOutstandingDate = "DocumentationOutstandingDate";
        public readonly string Col_FinalisedDate = "FinalisedDate";
        public readonly string Col_OpenAccountApplicationProductCode = "OpenAccountApplicationProductCode";
        public readonly string Col_OpenAccountApplicationProductCostCenter = "OpenAccountApplicationProductCostCenter";
        public readonly string Col_TS_AccountDetails = "TS_AccountDetails";
        public readonly string Col_PurposeOfInvestment = "PurposeOfInvestment";
        public readonly string Col_TrusteesCompanyDetailPassword = "TrusteesCompanyDetailPassword";
        #endregion

        private DataTable dtAOBankWest;

        public DataTable Table { get { return this.dtAOBankWest; } }

        public AccountOpeningBankWestCSVColumns()
        {
            dtAOBankWest = new DataTable("tblAccountOpeningBankWest");
            AddColumn(Col_ApplicationID);
            AddColumn(Col_ApplicationDate);
            AddColumn(Col_ApplicationStatus);
            AddColumn(Col_CMTOpenDate);
            AddColumn(Col_OpenAccountApplicationCreatorUserID);
            AddColumn(Col_OpenAccountApplicationApplicantType);
            AddColumn(Col_OpenAccountApplicationAccountOption);
            AddColumn(Col_OpenAccountApplicationProductType);
            AddColumn(Col_OpenAccountApplicationTrusteesCompany);
            AddColumn(Col_TrusteesCompanyDetailEntityName1);
            AddColumn(Col_TrusteesCompanyDetailEntityName2);
            AddColumn(Col_TrusteesCompanyDetailACNABN);
            AddColumn(Col_TrusteesCompanyDetailABN);
            AddColumn(Col_TrusteesCompanyDetailACNARBN);
            AddColumn(Col_TrusteesCompanyDetailContactName);
            AddColumn(Col_TrusteesCompanyDetailContactEmail);
            AddColumn(Col_TrusteesCompanyDetailContactPhPrefix);
            AddColumn(Col_TrusteesCompanyDetailContactPh);
            AddColumn(Col_TrusteesCompanyDetailCountryOfEstablishment);
            AddColumn(Col_TrusteesCompanyDetailTrustType);
            AddColumn(Col_TFNDetailTFN);
            AddColumn(Col_TFNDetailNRC);
            AddColumn(Col_TFNDetailExemptionCategory);
            AddColumn(Col_TFNDetailTIN);
            AddColumn(Col_CorpTrusteeDetailEntityName1);
            AddColumn(Col_CorpTrusteeDetailEntityName2);
            AddColumn(Col_CorpTrusteeDetailACNABN);
            AddColumn(Col_CorpTrusteeDetailABN);
            AddColumn(Col_CorpTrusteeDetailACNARBN);
            AddColumn(Col_CorpTrusteeDetailCountryOfEstablishment);
            AddColumn(Col_OpenAccountApplicationInitialInvestmentAmount);
            AddColumn(Col_OpenAccountApplicationMannerOfOperation);
            AddColumn(Col_OpenAccountApplicationFinancialAdviserAccess);
            AddColumn(Col_OpenAccountApplicationAdvisorFirmAccess);
            AddColumn(Col_OpenAccountApplicationLimitedAccessApproved);
            AddColumn(Col_AccessFacilityType);
            AddColumn(Col_AccessFacilitySelected);
            AddColumn(Col_AccessFacilityType_2);
            AddColumn(Col_AccessFacilitySelected_2);
            AddColumn(Col_AccessFacilityType_3);
            AddColumn(Col_AccessFacilitySelected_3);
            AddColumn(Col_AccessFacilityType_4);
            AddColumn(Col_AccessFacilitySelected_4);
            AddColumn(Col_AccessFacilityType_5);
            AddColumn(Col_AccessFacilitySelected_5);
            AddColumn(Col_OpenAccountApplicationDealerName);
            AddColumn(Col_OpenAccountApplicationAdviserName);
            AddColumn(Col_OpenAccountApplicationAdviserCode);
            AddColumn(Col_OpenAccountApplicationCMTaccountNumber);
            AddColumn(Col_OpenAccountApplicationCustomerAccountNumber);
            AddColumn(Col_OpenAccountApplicationCMTaccountBSB1);
            AddColumn(Col_OpenAccountApplicationCMTaccountBSB2);
            AddColumn(Col_OpenAccountApplicationClientAccountDesignation);
            AddColumn(Col_OpenAccountApplicationPartnerCode);
            AddColumn(Col_OpenAccountApplicationReceiptNo);
            AddColumn(Col_SignatoryPersonalDetailOccupation);
            AddColumn(Col_SignatoryPersonalDetailEmployer);
            AddColumn(Col_SignatoryPersonalDetailCountryOfResidence);
            AddColumn(Col_SignatoryPersonalDetailTitle);
            AddColumn(Col_SignatoryPersonalDetailTitleOther);
            AddColumn(Col_SignatoryPersonalDetailGivenName);
            AddColumn(Col_SignatoryPersonalDetailSurname);
            AddColumn(Col_SignatoryPersonalDetailPhoneWk);
            AddColumn(Col_SignatoryPersonalDetailPhoneHm);
            AddColumn(Col_SignatoryPersonalDetailPhoneMb);
            AddColumn(Col_SignatoryPersonalDetailDOBDay);
            AddColumn(Col_SignatoryPersonalDetailDOBMonth);
            AddColumn(Col_SignatoryPersonalDetailDOBYear);
            AddColumn(Col_SignatoryPersonalDetailEmail);
            AddColumn(Col_SignatoryPersonalDetailTelephonePassword);
            AddColumn(Col_AddressAddress1);
            AddColumn(Col_AddressAddress2);
            AddColumn(Col_AddressAddress3);
            AddColumn(Col_AddressSuburb);
            AddColumn(Col_AddressState);
            AddColumn(Col_AddressPostcode);
            AddColumn(Col_AddressCountry);
            AddColumn(Col_AddressCareOf);
            AddColumn(Col_SignatoryPersonalDetailPhoneHmPrefix);
            AddColumn(Col_SignatoryPersonalDetailPhonewkPrefix);
            AddColumn(Col_TFNDetailTFN_2);
            AddColumn(Col_TFNDetailNRC_2);
            AddColumn(Col_TFNDetailExemptionCategory_2);
            AddColumn(Col_TFNDetailTIN_2);
            AddColumn(Col_SignatoryPersonalDetailOccupation_2);
            AddColumn(Col_SignatoryPersonalDetailEmployer_2);
            AddColumn(Col_SignatoryPersonalDetailCountryOfResidence_2);
            AddColumn(Col_SignatoryPersonalDetailTitle_2);
            AddColumn(Col_SignatoryPersonalDetailTitleOther_2);
            AddColumn(Col_SignatoryPersonalDetailGivenName_2);
            AddColumn(Col_SignatoryPersonalDetailSurname_2);
            AddColumn(Col_SignatoryPersonalDetailPhoneWk_2);
            AddColumn(Col_SignatoryPersonalDetailPhoneHm_2);
            AddColumn(Col_SignatoryPersonalDetailPhoneMb_2);
            AddColumn(Col_SignatoryPersonalDetailDOBDay_2);
            AddColumn(Col_SignatoryPersonalDetailDOBMonth_2);
            AddColumn(Col_SignatoryPersonalDetailDOBYear_2);
            AddColumn(Col_SignatoryPersonalDetailEmail_2);
            AddColumn(Col_SignatoryPersonalDetailTelephonePassword_2);
            AddColumn(Col_AddressAddress1_2);
            AddColumn(Col_AddressAddress2_2);
            AddColumn(Col_AddressAddress3_2);
            AddColumn(Col_AddressSuburb_2);
            AddColumn(Col_AddressState_2);
            AddColumn(Col_AddressPostcode_2);
            AddColumn(Col_AddressCountry_2);
            AddColumn(Col_AddressCareOf_2);
            AddColumn(Col_SignatoryPersonalDetailPhoneHmPrefix_2);
            AddColumn(Col_SignatoryPersonalDetailPhonewkPrefix_2);
            AddColumn(Col_TFNDetailTFN_3);
            AddColumn(Col_TFNDetailNRC_3);
            AddColumn(Col_TFNDetailExemptionCategory_3);
            AddColumn(Col_TFNDetailTIN_3);
            AddColumn(Col_SignatoryPersonalDetailOccupation_3);
            AddColumn(Col_SignatoryPersonalDetailEmployer_3);
            AddColumn(Col_SignatoryPersonalDetailCountryOfResidence_3);
            AddColumn(Col_SignatoryPersonalDetailTitle_3);
            AddColumn(Col_SignatoryPersonalDetailTitleOther_3);
            AddColumn(Col_SignatoryPersonalDetailGivenName_3);
            AddColumn(Col_SignatoryPersonalDetailSurname_3);
            AddColumn(Col_SignatoryPersonalDetailPhoneWk_3);
            AddColumn(Col_SignatoryPersonalDetailPhoneHm_3);
            AddColumn(Col_SignatoryPersonalDetailPhoneMb_3);
            AddColumn(Col_SignatoryPersonalDetailDOBDay_3);
            AddColumn(Col_SignatoryPersonalDetailDOBMonth_3);
            AddColumn(Col_SignatoryPersonalDetailDOBYear_3);
            AddColumn(Col_SignatoryPersonalDetailEmail_3);
            AddColumn(Col_SignatoryPersonalDetailTelephonePassword_3);
            AddColumn(Col_AddressAddress1_3);
            AddColumn(Col_AddressAddress2_3);
            AddColumn(Col_AddressAddress3_3);
            AddColumn(Col_AddressSuburb_3);
            AddColumn(Col_AddressState_3);
            AddColumn(Col_AddressPostcode_3);
            AddColumn(Col_AddressCountry_3);
            AddColumn(Col_AddressCareOf_3);
            AddColumn(Col_SignatoryPersonalDetailPhoneHmPrefix_3);
            AddColumn(Col_SignatoryPersonalDetailPhonewkPrefix_3);
            AddColumn(Col_TFNDetailTFN_4);
            AddColumn(Col_TFNDetailNRC_4);
            AddColumn(Col_TFNDetailExemptionCategory_4);
            AddColumn(Col_TFNDetailTIN_4);
            AddColumn(Col_SignatoryPersonalDetailOccupation_4);
            AddColumn(Col_SignatoryPersonalDetailEmployer_4);
            AddColumn(Col_SignatoryPersonalDetailCountryOfResidence_4);
            AddColumn(Col_SignatoryPersonalDetailTitle_4);
            AddColumn(Col_SignatoryPersonalDetailTitleOther_4);
            AddColumn(Col_SignatoryPersonalDetailGivenName_4);
            AddColumn(Col_SignatoryPersonalDetailSurname_4);
            AddColumn(Col_SignatoryPersonalDetailPhoneWk_4);
            AddColumn(Col_SignatoryPersonalDetailPhoneHm_4);
            AddColumn(Col_SignatoryPersonalDetailPhoneMb_4);
            AddColumn(Col_SignatoryPersonalDetailDOBDay_4);
            AddColumn(Col_SignatoryPersonalDetailDOBMonth_4);
            AddColumn(Col_SignatoryPersonalDetailDOBYear_4);
            AddColumn(Col_SignatoryPersonalDetailEmail_4);
            AddColumn(Col_SignatoryPersonalDetailTelephonePassword_4);
            AddColumn(Col_AddressAddress1_4);
            AddColumn(Col_AddressAddress2_4);
            AddColumn(Col_AddressAddress3_4);
            AddColumn(Col_AddressSuburb_4);
            AddColumn(Col_AddressState_4);
            AddColumn(Col_AddressPostcode_4);
            AddColumn(Col_AddressCountry_4);
            AddColumn(Col_AddressCareOf_4);
            AddColumn(Col_SignatoryPersonalDetailPhoneHmPrefix_4);
            AddColumn(Col_SignatoryPersonalDetailPhonewkPrefix_4);
            AddColumn(Col_TFNDetailTFN_5);
            AddColumn(Col_TFNDetailNRC_5);
            AddColumn(Col_TFNDetailExemptionCategory_5);
            AddColumn(Col_TFNDetailTIN_5);
            AddColumn(Col_CorrespondenceAddressAddress1);
            AddColumn(Col_CorrespondenceAddressAddress2);
            AddColumn(Col_CorrespondenceAddressAddress3);
            AddColumn(Col_CorrespondenceAddressSuburb);
            AddColumn(Col_CorrespondenceAddressState);
            AddColumn(Col_CorrespondenceAddressPostcode);
            AddColumn(Col_CorrespondenceAddressCountry);
            AddColumn(Col_CorrespondenceAddressCareOf);
            AddColumn(Col_CorrespondenceAddressAddress1_2);
            AddColumn(Col_CorrespondenceAddressAddress2_2);
            AddColumn(Col_CorrespondenceAddressAddress3_2);
            AddColumn(Col_CorrespondenceAddressSuburb_2);
            AddColumn(Col_CorrespondenceAddressState_2);
            AddColumn(Col_CorrespondenceAddressPostcode_2);
            AddColumn(Col_CorrespondenceAddressCountry_2);
            AddColumn(Col_CorrespondenceAddressCareOf_2);
            AddColumn(Col_CorrespondenceAddressAddress1_3);
            AddColumn(Col_CorrespondenceAddressAddress2_3);
            AddColumn(Col_CorrespondenceAddressAddress3_3);
            AddColumn(Col_CorrespondenceAddressSuburb_3);
            AddColumn(Col_CorrespondenceAddressState_3);
            AddColumn(Col_CorrespondenceAddressPostcode_3);
            AddColumn(Col_CorrespondenceAddressCountry_3);
            AddColumn(Col_CorrespondenceAddressCareOf_3);
            AddColumn(Col_TrusteesCompanyCIF);
            AddColumn(Col_CorpTrusteeCIF);
            AddColumn(Col_SignatoryCIF);
            AddColumn(Col_SignatoryCIF_2);
            AddColumn(Col_SignatoryCIF_3);
            AddColumn(Col_SignatoryCIF_4);
            AddColumn(Col_PAN);
            AddColumn(Col_PAN_2);
            AddColumn(Col_PAN_3);
            AddColumn(Col_PAN_4);
            AddColumn(Col_CBSStatus);
            AddColumn(Col_Allocation);
            AddColumn(Col_FollowupDate);
            AddColumn(Col_OperationsStatus);
            AddColumn(Col_OpenAccountApplicationPartnerCIF);
            AddColumn(Col_OpenAccountApplicationAdviserCIF);
            AddColumn(Col_PlatformName);
            AddColumn(Col_PlatformCode);
            AddColumn(Col_PlatformReference1);
            AddColumn(Col_PlatformReference2);
            AddColumn(Col_PlatformName_2);
            AddColumn(Col_PlatformCode_2);
            AddColumn(Col_PlatformReference1_2);
            AddColumn(Col_PlatformReference2_2);
            AddColumn(Col_PlatformName_3);
            AddColumn(Col_PlatformCode_3);
            AddColumn(Col_PlatformReference1_3);
            AddColumn(Col_PlatformReference2_3);
            AddColumn(Col_ThirdPartyCIFKey);
            AddColumn(Col_ThirdPartyPartnerCode);
            AddColumn(Col_OpenAccountApplicationFirmName);
            AddColumn(Col_OpenAccountApplicationAdvisorFirmName);
            AddColumn(Col_ReportedOn);
            AddColumn(Col_ProcessingResult);
            AddColumn(Col_ProcessingIssues);
            AddColumn(Col_ApplicationReceivedDate);
            AddColumn(Col_DocumentationOutstandingDate);
            AddColumn(Col_FinalisedDate);
            AddColumn(Col_OpenAccountApplicationProductCode);
            AddColumn(Col_OpenAccountApplicationProductCostCenter);
            AddColumn(Col_TS_AccountDetails);
            AddColumn(Col_PurposeOfInvestment);
            AddColumn(Col_TrusteesCompanyDetailPassword);
            
            
            dtAOBankWest.Rows.Add(dtAOBankWest.NewRow());
        }

        private void AddColumn(string columnName)
        {
            if (!dtAOBankWest.Columns.Contains(columnName))
            {
                DataColumn dc = dtAOBankWest.Columns.Add(columnName);
                dc.DefaultValue = string.Empty;
            }
        }

        public void SetColumnValue(string columnName, string colunmValue)
        {
            if (dtAOBankWest.Columns.Contains(columnName))
                dtAOBankWest.Rows[0][columnName] = colunmValue;
        }

        public string ToCSV()
        {
            StringBuilder str = new StringBuilder();
            foreach (DataRow dr in dtAOBankWest.Rows)
            {
                foreach (object field in dr.ItemArray)
                {
                    string value = field.ToString();
                    if (value.IndexOfAny(new char[] { '"', ',' }) != -1)
                    {
                        // Special handling for values that contain comma or quote
                        // Enclose in quotes and double up any double quotes
                        str.AppendFormat("\"{0}\"" + ",", value.Replace("\"", "\"\""));
                    }
                    else str.Append(value + ",");
                }
                str.Replace(",", Environment.NewLine, str.Length - 1, 1);
            }
            return str.ToString();
        }

        public string GetColumnCSV()
        {
            StringBuilder str = new StringBuilder();
            foreach (DataColumn dc in dtAOBankWest.Columns)
            {
                str.Append(dc.ColumnName + ",");
            }
            str.Replace(",", Environment.NewLine, str.Length - 1, 1);
            return str.ToString();
        }
    }

    public class AccountOpeningBankWestIDCSVColumns
    {
        #region CSV Columns Name
        public readonly string Col_ApplicationID = "ApplicationID";
        public readonly string Col_ApplicationDate = "ApplicationDate";
        public readonly string Col_ApplicationStatus = "ApplicationStatus";
        public readonly string Col_HundredPointApplicationCreatorUserID = "HundredPointApplicationCreatorUserID";
        public readonly string Col_HundredPointApplicationReceiptNo = "HundredPointApplicationReceiptNo";
        public readonly string Col_HundredPointApplicationTitle = "HundredPointApplicationTitle";
        public readonly string Col_HundredPointApplicationTitleOther = "HundredPointApplicationTitleOther";
        public readonly string Col_HundredPointApplicationSurname = "HundredPointApplicationSurname";
        public readonly string Col_HundredPointApplicationGivenName = "HundredPointApplicationGivenName";
        public readonly string Col_HundredPointApplicationDOBDay = "HundredPointApplicationDOBDay";
        public readonly string Col_HundredPointApplicationDOBMonth = "HundredPointApplicationDOBMonth";
        public readonly string Col_HundredPointApplicationDOBYear = "HundredPointApplicationDOBYear";
        public readonly string Col_HundredPointApplicationResidentialAddress = "HundredPointApplicationResidentialAddress";
        public readonly string Col_HundredPointApplicationPartnerCode = "HundredPointApplicationPartnerCode";
        public readonly string Col_HundredPointApplicationBSB1 = "HundredPointApplicationBSB1";
        public readonly string Col_HundredPointApplicationBSB2 = "HundredPointApplicationBSB2";
        public readonly string Col_HundredPointApplicationAccountNumber = "HundredPointApplicationAccountNumber";
        public readonly string Col_HundredPointApplicationOccupation = "HundredPointApplicationOccupation";
        public readonly string Col_HundredPointApplicationCountryOfResidence = "HundredPointApplicationCountryOfResidence";
        public readonly string Col_ResidentialAddressStreet = "ResidentialAddressStreet";
        public readonly string Col_ResidentialAddressSuburb = "ResidentialAddressSuburb";
        public readonly string Col_ResidentialAddressState = "ResidentialAddressState";
        public readonly string Col_ResidentialAddressPostcode = "ResidentialAddressPostcode";
        public readonly string Col_HundredPointApplicationMailingAddress = "HundredPointApplicationMailingAddress";
        public readonly string Col_MailingAddressStreet = "MailingAddressStreet";
        public readonly string Col_MailingAddressSuburb = "MailingAddressSuburb";
        public readonly string Col_MailingAddressState = "MailingAddressState";
        public readonly string Col_MailingAddressPostcode = "MailingAddressPostcode";
        public readonly string Col_HundredPointApplicationHomePh = "HundredPointApplicationHomePh";
        public readonly string Col_HundredPointApplicationBusPh = "HundredPointApplicationBusPh";
        public readonly string Col_HundredPointApplicationMobilePh = "HundredPointApplicationMobilePh";
        public readonly string Col_HundredPointApplicationPeriodKnown = "HundredPointApplicationPeriodKnown";
        public readonly string Col_HundredPointApplicationPartiallyComplete = "HundredPointApplicationPartiallyComplete";
        public readonly string Col_HundredPointApplicationIDDocument = "HundredPointApplicationIDDocument";
        public readonly string Col_IDDocumentDocumentLayout = "IDDocumentDocumentLayout";
        public readonly string Col_IDDocumentTypeOfDoc = "IDDocumentTypeOfDoc";
        public readonly string Col_IDDocumentFullName = "IDDocumentFullName";
        public readonly string Col_IDDocumentDOBDay = "IDDocumentDOBDay";
        public readonly string Col_IDDocumentDOBMonth = "IDDocumentDOBMonth";
        public readonly string Col_IDDocumentDOBYear = "IDDocumentDOBYear";
        public readonly string Col_IDDocumentDocNumber = "IDDocumentDocNumber";
        public readonly string Col_IDDocumentPhotoShown = "IDDocumentPhotoShown";
        public readonly string Col_IDDocumentAddressMatch = "IDDocumentAddressMatch";
        public readonly string Col_IDDocumentDateOfIssueDay = "IDDocumentDateOfIssueDay";
        public readonly string Col_IDDocumentDateOfIssueMonth = "IDDocumentDateOfIssueMonth";
        public readonly string Col_IDDocumentDateOfIssueYear = "IDDocumentDateOfIssueYear";
        public readonly string Col_IDDocumentExpDateDay = "IDDocumentExpDateDay";
        public readonly string Col_IDDocumentExpDateMonth = "IDDocumentExpDateMonth";
        public readonly string Col_IDDocumentExpDateYear = "IDDocumentExpDateYear";
        public readonly string Col_IDDocumentIssuePlace = "IDDocumentIssuePlace";
        public readonly string Col_IDDocumentPlaceResidence = "IDDocumentPlaceResidence";
        public readonly string Col_IDDocumentPoints = "IDDocumentPoints";
        public readonly string Col_IDDocumentReferenceNumber = "IDDocumentReferenceNumber";
        public readonly string Col_IDDocumentInformationProvidedByOne = "IDDocumentInformationProvidedByOne";
        public readonly string Col_IDDocumentTitleRankOrDesignation = "IDDocumentTitleRankOrDesignation";
        public readonly string Col_IDDocumentInformationProvidedByTwo = "IDDocumentInformationProvidedByTwo";
        public readonly string Col_IDDocumentNameOfOrganisation = "IDDocumentNameOfOrganisation";
        public readonly string Col_IDDocumentAddressOfOrganisation = "IDDocumentAddressOfOrganisation";
        public readonly string Col_IDDocumentPhoneNumberOfOrganisation = "IDDocumentPhoneNumberOfOrganisation";
        public readonly string Col_IDDocumentBillLetterSighted = "IDDocumentBillLetterSighted";
        public readonly string Col_IDDocumentDateTelephoneContactMadeDay = "IDDocumentDateTelephoneContactMadeDay";
        public readonly string Col_IDDocumentDateTelephoneContactMadeMonth = "IDDocumentDateTelephoneContactMadeMonth";
        public readonly string Col_IDDocumentDateTelephoneContactMadeYear = "IDDocumentDateTelephoneContactMadeYear";
        public readonly string Col_HundredPointApplicationIDDocument_2 = "HundredPointApplicationIDDocument_2";
        public readonly string Col_IDDocumentDocumentLayout_2 = "IDDocumentDocumentLayout_2";
        public readonly string Col_IDDocumentTypeOfDoc_2 = "IDDocumentTypeOfDoc_2";
        public readonly string Col_IDDocumentFullName_2 = "IDDocumentFullName_2";
        public readonly string Col_IDDocumentDOBDay_2 = "IDDocumentDOBDay_2";
        public readonly string Col_IDDocumentDOBMonth_2 = "IDDocumentDOBMonth_2";
        public readonly string Col_IDDocumentDOBYear_2 = "IDDocumentDOBYear_2";
        public readonly string Col_IDDocumentDocNumber_2 = "IDDocumentDocNumber_2";
        public readonly string Col_IDDocumentPhotoShown_2 = "IDDocumentPhotoShown_2";
        public readonly string Col_IDDocumentAddressMatch_2 = "IDDocumentAddressMatch_2";
        public readonly string Col_IDDocumentDateOfIssueDay_2 = "IDDocumentDateOfIssueDay_2";
        public readonly string Col_IDDocumentDateOfIssueMonth_2 = "IDDocumentDateOfIssueMonth_2";
        public readonly string Col_IDDocumentDateOfIssueYear_2 = "IDDocumentDateOfIssueYear_2";
        public readonly string Col_IDDocumentExpDateDay_2 = "IDDocumentExpDateDay_2";
        public readonly string Col_IDDocumentExpDateMonth_2 = "IDDocumentExpDateMonth_2";
        public readonly string Col_IDDocumentExpDateYear_2 = "IDDocumentExpDateYear_2";
        public readonly string Col_IDDocumentIssuePlace_2 = "IDDocumentIssuePlace_2";
        public readonly string Col_IDDocumentPlaceResidence_2 = "IDDocumentPlaceResidence_2";
        public readonly string Col_IDDocumentPoints_2 = "IDDocumentPoints_2";
        public readonly string Col_IDDocumentReferenceNumber_2 = "IDDocumentReferenceNumber_2";
        public readonly string Col_IDDocumentInformationProvidedByOne_2 = "IDDocumentInformationProvidedByOne_2";
        public readonly string Col_IDDocumentTitleRankOrDesignation_2 = "IDDocumentTitleRankOrDesignation_2";
        public readonly string Col_IDDocumentInformationProvidedByTwo_2 = "IDDocumentInformationProvidedByTwo_2";
        public readonly string Col_IDDocumentNameOfOrganisation_2 = "IDDocumentNameOfOrganisation_2";
        public readonly string Col_IDDocumentAddressOfOrganisation_2 = "IDDocumentAddressOfOrganisation_2";
        public readonly string Col_IDDocumentPhoneNumberOfOrganisation_2 = "IDDocumentPhoneNumberOfOrganisation_2";
        public readonly string Col_IDDocumentBillLetterSighted_2 = "IDDocumentBillLetterSighted_2";
        public readonly string Col_IDDocumentDateTelephoneContactMadeDay_2 = "IDDocumentDateTelephoneContactMadeDay_2";
        public readonly string Col_IDDocumentDateTelephoneContactMadeMonth_2 = "IDDocumentDateTelephoneContactMadeMonth_2";
        public readonly string Col_IDDocumentDateTelephoneContactMadeYear_2 = "IDDocumentDateTelephoneContactMadeYear_2";
        public readonly string Col_HundredPointApplicationIDDocument_3 = "HundredPointApplicationIDDocument_3";
        public readonly string Col_IDDocumentDocumentLayout_3 = "IDDocumentDocumentLayout_3";
        public readonly string Col_IDDocumentTypeOfDoc_3 = "IDDocumentTypeOfDoc_3";
        public readonly string Col_IDDocumentFullName_3 = "IDDocumentFullName_3";
        public readonly string Col_IDDocumentDOBDay_3 = "IDDocumentDOBDay_3";
        public readonly string Col_IDDocumentDOBMonth_3 = "IDDocumentDOBMonth_3";
        public readonly string Col_IDDocumentDOBYear_3 = "IDDocumentDOBYear_3";
        public readonly string Col_IDDocumentDocNumber_3 = "IDDocumentDocNumber_3";
        public readonly string Col_IDDocumentPhotoShown_3 = "IDDocumentPhotoShown_3";
        public readonly string Col_IDDocumentAddressMatch_3 = "IDDocumentAddressMatch_3";
        public readonly string Col_IDDocumentDateOfIssueDay_3 = "IDDocumentDateOfIssueDay_3";
        public readonly string Col_IDDocumentDateOfIssueMonth_3 = "IDDocumentDateOfIssueMonth_3";
        public readonly string Col_IDDocumentDateOfIssueYear_3 = "IDDocumentDateOfIssueYear_3";
        public readonly string Col_IDDocumentExpDateDay_3 = "IDDocumentExpDateDay_3";
        public readonly string Col_IDDocumentExpDateMonth_3 = "IDDocumentExpDateMonth_3";
        public readonly string Col_IDDocumentExpDateYear_3 = "IDDocumentExpDateYear_3";
        public readonly string Col_IDDocumentIssuePlace_3 = "IDDocumentIssuePlace_3";
        public readonly string Col_IDDocumentPlaceResidence_3 = "IDDocumentPlaceResidence_3";
        public readonly string Col_IDDocumentPoints_3 = "IDDocumentPoints_3";
        public readonly string Col_IDDocumentReferenceNumber_3 = "IDDocumentReferenceNumber_3";
        public readonly string Col_IDDocumentInformationProvidedByOne_3 = "IDDocumentInformationProvidedByOne_3";
        public readonly string Col_IDDocumentTitleRankOrDesignation_3 = "IDDocumentTitleRankOrDesignation_3";
        public readonly string Col_IDDocumentInformationProvidedByTwo_3 = "IDDocumentInformationProvidedByTwo_3";
        public readonly string Col_IDDocumentNameOfOrganisation_3 = "IDDocumentNameOfOrganisation_3";
        public readonly string Col_IDDocumentAddressOfOrganisation_3 = "IDDocumentAddressOfOrganisation_3";
        public readonly string Col_IDDocumentPhoneNumberOfOrganisation_3 = "IDDocumentPhoneNumberOfOrganisation_3";
        public readonly string Col_IDDocumentBillLetterSighted_3 = "IDDocumentBillLetterSighted_3";
        public readonly string Col_IDDocumentDateTelephoneContactMadeDay_3 = "IDDocumentDateTelephoneContactMadeDay_3";
        public readonly string Col_IDDocumentDateTelephoneContactMadeMonth_3 = "IDDocumentDateTelephoneContactMadeMonth_3";
        public readonly string Col_IDDocumentDateTelephoneContactMadeYear_3 = "IDDocumentDateTelephoneContactMadeYear_3";
        public readonly string Col_HundredPointApplicationIDDocType = "HundredPointApplicationIDDocType";
        public readonly string Col_IDDocTypeType = "IDDocTypeType";
        public readonly string Col_IDDocTypeSelected = "IDDocTypeSelected";
        public readonly string Col_HundredPointApplicationIDDocType_2 = "HundredPointApplicationIDDocType_2";
        public readonly string Col_IDDocTypeType_2 = "IDDocTypeType_2";
        public readonly string Col_IDDocTypeSelected_2 = "IDDocTypeSelected_2";
        public readonly string Col_HundredPointApplicationIDDocType_3 = "HundredPointApplicationIDDocType_3";
        public readonly string Col_IDDocTypeType_3 = "IDDocTypeType_3";
        public readonly string Col_IDDocTypeSelected_3 = "IDDocTypeSelected_3";
        public readonly string Col_HundredPointApplicationIDDocType_4 = "HundredPointApplicationIDDocType_4";
        public readonly string Col_IDDocTypeType_4 = "IDDocTypeType_4";
        public readonly string Col_IDDocTypeSelected_4 = "IDDocTypeSelected_4";
        public readonly string Col_HundredPointApplicationIDDocType_5 = "HundredPointApplicationIDDocType_5";
        public readonly string Col_IDDocTypeType_5 = "IDDocTypeType_5";
        public readonly string Col_IDDocTypeSelected_5 = "IDDocTypeSelected_5";
        public readonly string Col_HundredPointApplicationIDDocType_6 = "HundredPointApplicationIDDocType_6";
        public readonly string Col_IDDocTypeType_6 = "IDDocTypeType_6";
        public readonly string Col_IDDocTypeSelected_6 = "IDDocTypeSelected_6";
        public readonly string Col_HundredPointApplicationIDDocType_7 = "HundredPointApplicationIDDocType_7";
        public readonly string Col_IDDocTypeType_7 = "IDDocTypeType_7";
        public readonly string Col_IDDocTypeSelected_7 = "IDDocTypeSelected_7";
        public readonly string Col_HundredPointApplicationIDDocType_8 = "HundredPointApplicationIDDocType_8";
        public readonly string Col_IDDocTypeType_8 = "IDDocTypeType_8";
        public readonly string Col_IDDocTypeSelected_8 = "IDDocTypeSelected_8";
        public readonly string Col_HundredPointApplicationAdvisorName = "HundredPointApplicationAdvisorName";
        public readonly string Col_HundredPointApplicationFirmName = "HundredPointApplicationFirmName";
        public readonly string Col_HundredPointApplicationAcceptableRefDoc = "HundredPointApplicationAcceptableRefDoc";
        public readonly string Col_AcceptableRefDocDocumentLayout = "AcceptableRefDocDocumentLayout";
        public readonly string Col_AcceptableRefDocTypeOfDoc = "AcceptableRefDocTypeOfDoc";
        public readonly string Col_AcceptableRefDocFullName = "AcceptableRefDocFullName";
        public readonly string Col_AcceptableRefDocDOBDay = "AcceptableRefDocDOBDay";
        public readonly string Col_AcceptableRefDocDOBMonth = "AcceptableRefDocDOBMonth";
        public readonly string Col_AcceptableRefDocDOBYear = "AcceptableRefDocDOBYear";
        public readonly string Col_AcceptableRefDocDocNumber = "AcceptableRefDocDocNumber";
        public readonly string Col_AcceptableRefDocPhotoShown = "AcceptableRefDocPhotoShown";
        public readonly string Col_AcceptableRefDocDateOfIssueDay = "AcceptableRefDocDateOfIssueDay";
        public readonly string Col_AcceptableRefDocDateOfIssueMonth = "AcceptableRefDocDateOfIssueMonth";
        public readonly string Col_AcceptableRefDocDateOfIssueYear = "AcceptableRefDocDateOfIssueYear";
        public readonly string Col_AcceptableRefDocExpDateDay = "AcceptableRefDocExpDateDay";
        public readonly string Col_AcceptableRefDocExpDateMonth = "AcceptableRefDocExpDateMonth";
        public readonly string Col_AcceptableRefDocExpDateYear = "AcceptableRefDocExpDateYear";
        public readonly string Col_AcceptableRefDocIssuePlace = "AcceptableRefDocIssuePlace";
        public readonly string Col_AcceptableRefDocPlaceResidence = "AcceptableRefDocPlaceResidence";
        public readonly string Col_AcceptableRefDocPoints = "AcceptableRefDocPoints";
        public readonly string Col_AcceptableRefDocReferenceNumber = "AcceptableRefDocReferenceNumber";
        public readonly string Col_AcceptableRefDocInformationProvidedByOne = "AcceptableRefDocInformationProvidedByOne";
        public readonly string Col_AcceptableRefDocTitleRankOrDesignation = "AcceptableRefDocTitleRankOrDesignation";
        public readonly string Col_AcceptableRefDocInformationProvidedByTwo = "AcceptableRefDocInformationProvidedByTwo";
        public readonly string Col_AcceptableRefDocNameOfOrganisation = "AcceptableRefDocNameOfOrganisation";
        public readonly string Col_AcceptableRefDocAddressOfOrganisation = "AcceptableRefDocAddressOfOrganisation";
        public readonly string Col_AcceptableRefDocPhoneNumberOfOrganisation = "AcceptableRefDocPhoneNumberOfOrganisation";
        public readonly string Col_AcceptableRefDocBillLetterSighted = "AcceptableRefDocBillLetterSighted";
        public readonly string Col_AcceptableRefDocDateTelephoneContactMadeDay = "AcceptableRefDocDateTelephoneContactMadeDay";
        public readonly string Col_AcceptableRefDocDateTelephoneContactMadeMonth = "AcceptableRefDocDateTelephoneContactMadeMonth";
        public readonly string Col_AcceptableRefDocDateTelephoneContactMadeYear = "AcceptableRefDocDateTelephoneContactMadeYear";
        public readonly string Col_CMTOpenDate = "CMTOpenDate";
        public readonly string Col_HundredPointApplicationIDDocType_9 = "HundredPointApplicationIDDocType_9";
        public readonly string Col_IDDocTypeType_9 = "IDDocTypeType_9";
        public readonly string Col_IDDocTypeSelected_9 = "IDDocTypeSelected_9";
        public readonly string Col_HundredPointApplicationIDDocType0 = "HundredPointApplicationIDDocType0";
        public readonly string Col_IDDocTypeType0 = "IDDocTypeType0";
        public readonly string Col_IDDocTypeSelected0 = "IDDocTypeSelected0";
        public readonly string Col_HundredPointApplicationIDDocType1 = "HundredPointApplicationIDDocType1";
        public readonly string Col_IDDocTypeType1 = "IDDocTypeType1";
        public readonly string Col_IDDocTypeSelected1 = "IDDocTypeSelected1";
        public readonly string Col_HundredPointApplicationIDDocType2 = "HundredPointApplicationIDDocType2";
        public readonly string Col_IDDocTypeType2 = "IDDocTypeType2";
        public readonly string Col_IDDocTypeSelected2 = "IDDocTypeSelected2";
        public readonly string Col_HundredPointApplicationIDDocument_4 = "HundredPointApplicationIDDocument_4";
        public readonly string Col_IDDocumentDocumentLayout_4 = "IDDocumentDocumentLayout_4";
        public readonly string Col_IDDocumentTypeOfDoc_4 = "IDDocumentTypeOfDoc_4";
        public readonly string Col_IDDocumentFullName_4 = "IDDocumentFullName_4";
        public readonly string Col_IDDocumentDOBDay_4 = "IDDocumentDOBDay_4";
        public readonly string Col_IDDocumentDOBMonth_4 = "IDDocumentDOBMonth_4";
        public readonly string Col_IDDocumentDOBYear_4 = "IDDocumentDOBYear_4";
        public readonly string Col_IDDocumentDocNumber_4 = "IDDocumentDocNumber_4";
        public readonly string Col_IDDocumentPhotoShown_4 = "IDDocumentPhotoShown_4";
        public readonly string Col_IDDocumentAddressMatch_4 = "IDDocumentAddressMatch_4";
        public readonly string Col_IDDocumentDateOfIssueDay_4 = "IDDocumentDateOfIssueDay_4";
        public readonly string Col_IDDocumentDateOfIssueMonth_4 = "IDDocumentDateOfIssueMonth_4";
        public readonly string Col_IDDocumentDateOfIssueYear_4 = "IDDocumentDateOfIssueYear_4";
        public readonly string Col_IDDocumentExpDateDay_4 = "IDDocumentExpDateDay_4";
        public readonly string Col_IDDocumentExpDateMonth_4 = "IDDocumentExpDateMonth_4";
        public readonly string Col_IDDocumentExpDateYear_4 = "IDDocumentExpDateYear_4";
        public readonly string Col_IDDocumentIssuePlace_4 = "IDDocumentIssuePlace_4";
        public readonly string Col_IDDocumentPlaceResidence_4 = "IDDocumentPlaceResidence_4";
        public readonly string Col_IDDocumentPoints_4 = "IDDocumentPoints_4";
        public readonly string Col_IDDocumentReferenceNumber_4 = "IDDocumentReferenceNumber_4";
        public readonly string Col_IDDocumentInformationProvidedByOne_4 = "IDDocumentInformationProvidedByOne_4";
        public readonly string Col_IDDocumentTitleRankOrDesignation_4 = "IDDocumentTitleRankOrDesignation_4";
        public readonly string Col_IDDocumentInformationProvidedByTwo_4 = "IDDocumentInformationProvidedByTwo_4";
        public readonly string Col_IDDocumentNameOfOrganisation_4 = "IDDocumentNameOfOrganisation_4";
        public readonly string Col_IDDocumentAddressOfOrganisation_4 = "IDDocumentAddressOfOrganisation_4";
        public readonly string Col_IDDocumentPhoneNumberOfOrganisation_4 = "IDDocumentPhoneNumberOfOrganisation_4";
        public readonly string Col_IDDocumentBillLetterSighted_4 = "IDDocumentBillLetterSighted_4";
        public readonly string Col_IDDocumentDateTelephoneContactMadeDay_4 = "IDDocumentDateTelephoneContactMadeDay_4";
        public readonly string Col_IDDocumentDateTelephoneContactMadeMonth_4 = "IDDocumentDateTelephoneContactMadeMonth_4";
        public readonly string Col_IDDocumentDateTelephoneContactMadeYear_4 = "IDDocumentDateTelephoneContactMadeYear_4";
        public readonly string Col_IDDocumentNameOfSignatoryConfirmed = "IDDocumentNameOfSignatoryConfirmed";
        public readonly string Col_IDDocumentAddressOfSignatoryConfirmed = "IDDocumentAddressOfSignatoryConfirmed";
        public readonly string Col_IDDocumentNameOfSignatoryConfirmed_2 = "IDDocumentNameOfSignatoryConfirmed_2";
        public readonly string Col_IDDocumentAddressOfSignatoryConfirmed_2 = "IDDocumentAddressOfSignatoryConfirmed_2";
        public readonly string Col_IDDocumentNameOfSignatoryConfirmed_3 = "IDDocumentNameOfSignatoryConfirmed_3";
        public readonly string Col_IDDocumentAddressOfSignatoryConfirmed_3 = "IDDocumentAddressOfSignatoryConfirmed_3";
        public readonly string Col_IDDocumentNameOfSignatoryConfirmed_4 = "IDDocumentNameOfSignatoryConfirmed_4";
        public readonly string Col_IDDocumentAddressOfSignatoryConfirmed_4 = "IDDocumentAddressOfSignatoryConfirmed_4";
        public readonly string Col_AcceptableRefDocNameOfSignatoryConfirmed = "AcceptableRefDocNameOfSignatoryConfirmed";
        public readonly string Col_AcceptableRefDocAddressOfSignatoryConfirmed = "AcceptableRefDocAddressOfSignatoryConfirmed";
        public readonly string Col_HundredPointApplicationIDDocType_10 = "HundredPointApplicationIDDocType_10";
        public readonly string Col_IDDocTypeType_10 = "IDDocTypeType_10";
        public readonly string Col_IDDocTypeSelected_10 = "IDDocTypeSelected_10";
        public readonly string Col_HundredPointApplicationIDDocType_11 = "HundredPointApplicationIDDocType_11";
        public readonly string Col_IDDocTypeType_11 = "IDDocTypeType_11";
        public readonly string Col_IDDocTypeSelected_11 = "IDDocTypeSelected_11";
        public readonly string Col_HundredPointApplicationIDDocType_12 = "HundredPointApplicationIDDocType_12";
        public readonly string Col_IDDocTypeType_12 = "IDDocTypeType_12";
        public readonly string Col_IDDocTypeSelected_12 = "IDDocTypeSelected_12";

        #endregion

        private DataTable dtAOBankWestID;

        public DataTable Table { get { return this.dtAOBankWestID; } }

        public AccountOpeningBankWestIDCSVColumns()
        {
            dtAOBankWestID = new DataTable("tblAccountOpeningBankWestID");
            AddColumn(Col_ApplicationID);
            AddColumn(Col_ApplicationDate);
            AddColumn(Col_ApplicationStatus);
            AddColumn(Col_HundredPointApplicationCreatorUserID);
            AddColumn(Col_HundredPointApplicationReceiptNo);
            AddColumn(Col_HundredPointApplicationTitle);
            AddColumn(Col_HundredPointApplicationTitleOther);
            AddColumn(Col_HundredPointApplicationSurname);
            AddColumn(Col_HundredPointApplicationGivenName);
            AddColumn(Col_HundredPointApplicationDOBDay);
            AddColumn(Col_HundredPointApplicationDOBMonth);
            AddColumn(Col_HundredPointApplicationDOBYear);
            AddColumn(Col_HundredPointApplicationResidentialAddress);
            AddColumn(Col_HundredPointApplicationPartnerCode);
            AddColumn(Col_HundredPointApplicationBSB1);
            AddColumn(Col_HundredPointApplicationBSB2);
            AddColumn(Col_HundredPointApplicationAccountNumber);
            AddColumn(Col_HundredPointApplicationOccupation);
            AddColumn(Col_HundredPointApplicationCountryOfResidence);
            AddColumn(Col_ResidentialAddressStreet);
            AddColumn(Col_ResidentialAddressSuburb);
            AddColumn(Col_ResidentialAddressState);
            AddColumn(Col_ResidentialAddressPostcode);
            AddColumn(Col_HundredPointApplicationMailingAddress);
            AddColumn(Col_MailingAddressStreet);
            AddColumn(Col_MailingAddressSuburb);
            AddColumn(Col_MailingAddressState);
            AddColumn(Col_MailingAddressPostcode);
            AddColumn(Col_HundredPointApplicationHomePh);
            AddColumn(Col_HundredPointApplicationBusPh);
            AddColumn(Col_HundredPointApplicationMobilePh);
            AddColumn(Col_HundredPointApplicationPeriodKnown);
            AddColumn(Col_HundredPointApplicationPartiallyComplete);
            AddColumn(Col_HundredPointApplicationIDDocument);
            AddColumn(Col_IDDocumentDocumentLayout);
            AddColumn(Col_IDDocumentTypeOfDoc);
            AddColumn(Col_IDDocumentFullName);
            AddColumn(Col_IDDocumentDOBDay);
            AddColumn(Col_IDDocumentDOBMonth);
            AddColumn(Col_IDDocumentDOBYear);
            AddColumn(Col_IDDocumentDocNumber);
            AddColumn(Col_IDDocumentPhotoShown);
            AddColumn(Col_IDDocumentAddressMatch);
            AddColumn(Col_IDDocumentDateOfIssueDay);
            AddColumn(Col_IDDocumentDateOfIssueMonth);
            AddColumn(Col_IDDocumentDateOfIssueYear);
            AddColumn(Col_IDDocumentExpDateDay);
            AddColumn(Col_IDDocumentExpDateMonth);
            AddColumn(Col_IDDocumentExpDateYear);
            AddColumn(Col_IDDocumentIssuePlace);
            AddColumn(Col_IDDocumentPlaceResidence);
            AddColumn(Col_IDDocumentPoints);
            AddColumn(Col_IDDocumentReferenceNumber);
            AddColumn(Col_IDDocumentInformationProvidedByOne);
            AddColumn(Col_IDDocumentTitleRankOrDesignation);
            AddColumn(Col_IDDocumentInformationProvidedByTwo);
            AddColumn(Col_IDDocumentNameOfOrganisation);
            AddColumn(Col_IDDocumentAddressOfOrganisation);
            AddColumn(Col_IDDocumentPhoneNumberOfOrganisation);
            AddColumn(Col_IDDocumentBillLetterSighted);
            AddColumn(Col_IDDocumentDateTelephoneContactMadeDay);
            AddColumn(Col_IDDocumentDateTelephoneContactMadeMonth);
            AddColumn(Col_IDDocumentDateTelephoneContactMadeYear);
            AddColumn(Col_HundredPointApplicationIDDocument_2);
            AddColumn(Col_IDDocumentDocumentLayout_2);
            AddColumn(Col_IDDocumentTypeOfDoc_2);
            AddColumn(Col_IDDocumentFullName_2);
            AddColumn(Col_IDDocumentDOBDay_2);
            AddColumn(Col_IDDocumentDOBMonth_2);
            AddColumn(Col_IDDocumentDOBYear_2);
            AddColumn(Col_IDDocumentDocNumber_2);
            AddColumn(Col_IDDocumentPhotoShown_2);
            AddColumn(Col_IDDocumentAddressMatch_2);
            AddColumn(Col_IDDocumentDateOfIssueDay_2);
            AddColumn(Col_IDDocumentDateOfIssueMonth_2);
            AddColumn(Col_IDDocumentDateOfIssueYear_2);
            AddColumn(Col_IDDocumentExpDateDay_2);
            AddColumn(Col_IDDocumentExpDateMonth_2);
            AddColumn(Col_IDDocumentExpDateYear_2);
            AddColumn(Col_IDDocumentIssuePlace_2);
            AddColumn(Col_IDDocumentPlaceResidence_2);
            AddColumn(Col_IDDocumentPoints_2);
            AddColumn(Col_IDDocumentReferenceNumber_2);
            AddColumn(Col_IDDocumentInformationProvidedByOne_2);
            AddColumn(Col_IDDocumentTitleRankOrDesignation_2);
            AddColumn(Col_IDDocumentInformationProvidedByTwo_2);
            AddColumn(Col_IDDocumentNameOfOrganisation_2);
            AddColumn(Col_IDDocumentAddressOfOrganisation_2);
            AddColumn(Col_IDDocumentPhoneNumberOfOrganisation_2);
            AddColumn(Col_IDDocumentBillLetterSighted_2);
            AddColumn(Col_IDDocumentDateTelephoneContactMadeDay_2);
            AddColumn(Col_IDDocumentDateTelephoneContactMadeMonth_2);
            AddColumn(Col_IDDocumentDateTelephoneContactMadeYear_2);
            AddColumn(Col_HundredPointApplicationIDDocument_3);
            AddColumn(Col_IDDocumentDocumentLayout_3);
            AddColumn(Col_IDDocumentTypeOfDoc_3);
            AddColumn(Col_IDDocumentFullName_3);
            AddColumn(Col_IDDocumentDOBDay_3);
            AddColumn(Col_IDDocumentDOBMonth_3);
            AddColumn(Col_IDDocumentDOBYear_3);
            AddColumn(Col_IDDocumentDocNumber_3);
            AddColumn(Col_IDDocumentPhotoShown_3);
            AddColumn(Col_IDDocumentAddressMatch_3);
            AddColumn(Col_IDDocumentDateOfIssueDay_3);
            AddColumn(Col_IDDocumentDateOfIssueMonth_3);
            AddColumn(Col_IDDocumentDateOfIssueYear_3);
            AddColumn(Col_IDDocumentExpDateDay_3);
            AddColumn(Col_IDDocumentExpDateMonth_3);
            AddColumn(Col_IDDocumentExpDateYear_3);
            AddColumn(Col_IDDocumentIssuePlace_3);
            AddColumn(Col_IDDocumentPlaceResidence_3);
            AddColumn(Col_IDDocumentPoints_3);
            AddColumn(Col_IDDocumentReferenceNumber_3);
            AddColumn(Col_IDDocumentInformationProvidedByOne_3);
            AddColumn(Col_IDDocumentTitleRankOrDesignation_3);
            AddColumn(Col_IDDocumentInformationProvidedByTwo_3);
            AddColumn(Col_IDDocumentNameOfOrganisation_3);
            AddColumn(Col_IDDocumentAddressOfOrganisation_3);
            AddColumn(Col_IDDocumentPhoneNumberOfOrganisation_3);
            AddColumn(Col_IDDocumentBillLetterSighted_3);
            AddColumn(Col_IDDocumentDateTelephoneContactMadeDay_3);
            AddColumn(Col_IDDocumentDateTelephoneContactMadeMonth_3);
            AddColumn(Col_IDDocumentDateTelephoneContactMadeYear_3);
            AddColumn(Col_HundredPointApplicationIDDocType);
            AddColumn(Col_IDDocTypeType);
            AddColumn(Col_IDDocTypeSelected);
            AddColumn(Col_HundredPointApplicationIDDocType_2);
            AddColumn(Col_IDDocTypeType_2);
            AddColumn(Col_IDDocTypeSelected_2);
            AddColumn(Col_HundredPointApplicationIDDocType_3);
            AddColumn(Col_IDDocTypeType_3);
            AddColumn(Col_IDDocTypeSelected_3);
            AddColumn(Col_HundredPointApplicationIDDocType_4);
            AddColumn(Col_IDDocTypeType_4);
            AddColumn(Col_IDDocTypeSelected_4);
            AddColumn(Col_HundredPointApplicationIDDocType_5);
            AddColumn(Col_IDDocTypeType_5);
            AddColumn(Col_IDDocTypeSelected_5);
            AddColumn(Col_HundredPointApplicationIDDocType_6);
            AddColumn(Col_IDDocTypeType_6);
            AddColumn(Col_IDDocTypeSelected_6);
            AddColumn(Col_HundredPointApplicationIDDocType_7);
            AddColumn(Col_IDDocTypeType_7);
            AddColumn(Col_IDDocTypeSelected_7);
            AddColumn(Col_HundredPointApplicationIDDocType_8);
            AddColumn(Col_IDDocTypeType_8);
            AddColumn(Col_IDDocTypeSelected_8);
            AddColumn(Col_HundredPointApplicationAdvisorName);
            AddColumn(Col_HundredPointApplicationFirmName);
            AddColumn(Col_HundredPointApplicationAcceptableRefDoc);
            AddColumn(Col_AcceptableRefDocDocumentLayout);
            AddColumn(Col_AcceptableRefDocTypeOfDoc);
            AddColumn(Col_AcceptableRefDocFullName);
            AddColumn(Col_AcceptableRefDocDOBDay);
            AddColumn(Col_AcceptableRefDocDOBMonth);
            AddColumn(Col_AcceptableRefDocDOBYear);
            AddColumn(Col_AcceptableRefDocDocNumber);
            AddColumn(Col_AcceptableRefDocPhotoShown);
            AddColumn(Col_AcceptableRefDocDateOfIssueDay);
            AddColumn(Col_AcceptableRefDocDateOfIssueMonth);
            AddColumn(Col_AcceptableRefDocDateOfIssueYear);
            AddColumn(Col_AcceptableRefDocExpDateDay);
            AddColumn(Col_AcceptableRefDocExpDateMonth);
            AddColumn(Col_AcceptableRefDocExpDateYear);
            AddColumn(Col_AcceptableRefDocIssuePlace);
            AddColumn(Col_AcceptableRefDocPlaceResidence);
            AddColumn(Col_AcceptableRefDocPoints);
            AddColumn(Col_AcceptableRefDocReferenceNumber);
            AddColumn(Col_AcceptableRefDocInformationProvidedByOne);
            AddColumn(Col_AcceptableRefDocTitleRankOrDesignation);
            AddColumn(Col_AcceptableRefDocInformationProvidedByTwo);
            AddColumn(Col_AcceptableRefDocNameOfOrganisation);
            AddColumn(Col_AcceptableRefDocAddressOfOrganisation);
            AddColumn(Col_AcceptableRefDocPhoneNumberOfOrganisation);
            AddColumn(Col_AcceptableRefDocBillLetterSighted);
            AddColumn(Col_AcceptableRefDocDateTelephoneContactMadeDay);
            AddColumn(Col_AcceptableRefDocDateTelephoneContactMadeMonth);
            AddColumn(Col_AcceptableRefDocDateTelephoneContactMadeYear);
            AddColumn(Col_CMTOpenDate);
            AddColumn(Col_HundredPointApplicationIDDocType_9);
            AddColumn(Col_IDDocTypeType_9);
            AddColumn(Col_IDDocTypeSelected_9);
            AddColumn(Col_HundredPointApplicationIDDocType0);
            AddColumn(Col_IDDocTypeType0);
            AddColumn(Col_IDDocTypeSelected0);
            AddColumn(Col_HundredPointApplicationIDDocType1);
            AddColumn(Col_IDDocTypeType1);
            AddColumn(Col_IDDocTypeSelected1);
            AddColumn(Col_HundredPointApplicationIDDocType2);
            AddColumn(Col_IDDocTypeType2);
            AddColumn(Col_IDDocTypeSelected2);
            AddColumn(Col_HundredPointApplicationIDDocument_4);
            AddColumn(Col_IDDocumentDocumentLayout_4);
            AddColumn(Col_IDDocumentTypeOfDoc_4);
            AddColumn(Col_IDDocumentFullName_4);
            AddColumn(Col_IDDocumentDOBDay_4);
            AddColumn(Col_IDDocumentDOBMonth_4);
            AddColumn(Col_IDDocumentDOBYear_4);
            AddColumn(Col_IDDocumentDocNumber_4);
            AddColumn(Col_IDDocumentPhotoShown_4);
            AddColumn(Col_IDDocumentAddressMatch_4);
            AddColumn(Col_IDDocumentDateOfIssueDay_4);
            AddColumn(Col_IDDocumentDateOfIssueMonth_4);
            AddColumn(Col_IDDocumentDateOfIssueYear_4);
            AddColumn(Col_IDDocumentExpDateDay_4);
            AddColumn(Col_IDDocumentExpDateMonth_4);
            AddColumn(Col_IDDocumentExpDateYear_4);
            AddColumn(Col_IDDocumentIssuePlace_4);
            AddColumn(Col_IDDocumentPlaceResidence_4);
            AddColumn(Col_IDDocumentPoints_4);
            AddColumn(Col_IDDocumentReferenceNumber_4);
            AddColumn(Col_IDDocumentInformationProvidedByOne_4);
            AddColumn(Col_IDDocumentTitleRankOrDesignation_4);
            AddColumn(Col_IDDocumentInformationProvidedByTwo_4);
            AddColumn(Col_IDDocumentNameOfOrganisation_4);
            AddColumn(Col_IDDocumentAddressOfOrganisation_4);
            AddColumn(Col_IDDocumentPhoneNumberOfOrganisation_4);
            AddColumn(Col_IDDocumentBillLetterSighted_4);
            AddColumn(Col_IDDocumentDateTelephoneContactMadeDay_4);
            AddColumn(Col_IDDocumentDateTelephoneContactMadeMonth_4);
            AddColumn(Col_IDDocumentDateTelephoneContactMadeYear_4);
            AddColumn(Col_IDDocumentNameOfSignatoryConfirmed);
            AddColumn(Col_IDDocumentAddressOfSignatoryConfirmed);
            AddColumn(Col_IDDocumentNameOfSignatoryConfirmed_2);
            AddColumn(Col_IDDocumentAddressOfSignatoryConfirmed_2);
            AddColumn(Col_IDDocumentNameOfSignatoryConfirmed_3);
            AddColumn(Col_IDDocumentAddressOfSignatoryConfirmed_3);
            AddColumn(Col_IDDocumentNameOfSignatoryConfirmed_4);
            AddColumn(Col_IDDocumentAddressOfSignatoryConfirmed_4);
            AddColumn(Col_AcceptableRefDocNameOfSignatoryConfirmed);
            AddColumn(Col_AcceptableRefDocAddressOfSignatoryConfirmed);
            AddColumn(Col_HundredPointApplicationIDDocType_10);
            AddColumn(Col_IDDocTypeType_10);
            AddColumn(Col_IDDocTypeSelected_10);
            AddColumn(Col_HundredPointApplicationIDDocType_11);
            AddColumn(Col_IDDocTypeType_11);
            AddColumn(Col_IDDocTypeSelected_11);
            AddColumn(Col_HundredPointApplicationIDDocType_12);
            AddColumn(Col_IDDocTypeType_12);
            AddColumn(Col_IDDocTypeSelected_12);

            dtAOBankWestID.Rows.Add(dtAOBankWestID.NewRow());
        }

        private void AddColumn(string columnName)
        {
            if (!dtAOBankWestID.Columns.Contains(columnName))
            {
                DataColumn dc = dtAOBankWestID.Columns.Add(columnName);
                dc.DefaultValue = string.Empty;
            }
        }

        public void SetColumnValue(string columnName, string colunmValue)
        {
            if (dtAOBankWestID.Columns.Contains(columnName))
                dtAOBankWestID.Rows[0][columnName] = colunmValue;
        }

        public string ToCSV()
        {
            StringBuilder str = new StringBuilder();
            foreach (DataRow dr in dtAOBankWestID.Rows)
            {
                foreach (object field in dr.ItemArray)
                {
                    string value = field.ToString();
                    if (value.IndexOfAny(new char[] { '"', ',' }) != -1)
                    {
                        // Special handling for values that contain comma or quote
                        // Enclose in quotes and double up any double quotes
                        str.AppendFormat("\"{0}\"" + ",", value.Replace("\"", "\"\""));
                    }
                    else str.Append(value + ",");
                }
                str.Replace(",", Environment.NewLine, str.Length - 1, 1);
            }
            return str.ToString();
        }

        public string GetColumnCSV()
        {
            StringBuilder str = new StringBuilder();
            foreach (DataColumn dc in dtAOBankWestID.Columns)
            {
                str.Append(dc.ColumnName + ",");
            }
            str.Replace(",", Environment.NewLine, str.Length - 1, 1);
            return str.ToString();
        }
    }

    public class ClientDesktopBrokerColumns
    {
        public DataTable Table = null;

        public static string LinceseeNumber = "Licensee Number";
        public static string AdviserNumber = "Adviser Number";
        public static string ClientReferenceNumber = "Client Reference Number";
        public static string RegistrationAddressLine1 = "Registration Address Line1";
        public static string RegistrationAddressLine2 = "Registration Address Line2";
        public static string Suburb = "Suburb";
        public static string Postcode = "Postcode";
        public static string State = "State";



        public static string TableName = "ClientDesktopBroker";

        public ClientDesktopBrokerColumns()
        {
            Table = new DataTable(TableName);

            Table.Columns.Add(LinceseeNumber, typeof(string));
            Table.Columns.Add(AdviserNumber, typeof(string));
            Table.Columns.Add(ClientReferenceNumber, typeof(string));
            Table.Columns.Add(RegistrationAddressLine1, typeof(string));
            Table.Columns.Add(RegistrationAddressLine2, typeof(string));
            Table.Columns.Add(Suburb, typeof(string));
            Table.Columns.Add(Postcode, typeof(string));
            Table.Columns.Add(State, typeof(string));





        }


    }

    public class AccountHolderDesktopBrokerColumns
    {
        public DataTable Table = null;

        public static string IdentificationNumber = "Identification Number";
        public static string IdentificationType = "Identification Type";
        public static string Employer = "Employer";
        public static string Occupation = "Occupation";
        public static string DateOfBirth = "Date Of Birth";

        public static string FamilyName = "FamilyName";
        public static string GivenNames = "GivenNames";
        public static string Title = "Title";
        public static string ResidentForTaxPurpose = "Resident For Tax Purpose";
        public static string TaxFileNumber = "Tax File Number";
        public static string TFNExemption = "TFN Exemption";
        public static string EmailAddress = "Email Address";

        public static string MobilePhoneNumber = "Mobile Phone Number";
        public static string HomePhoneNumber = "Home Phone Number";
        public static string WorkPhoneNumber = "Work Phone Number";

        public static string PreferredContact = "PreferredContact";
        public static string UserName = "UserName";

        public static string ResidentialAddressLine1 = "Residential address line1";
        public static string ResidentialAddressLine2 = "Residential address line2";
        public static string Residential_Suburb = "Residential Suburb";
        public static string Residential_PostCode = "Residential Postcode";
        public static string Residential_State = "Residential State";

        public static string PostalAddressLine1 = "Postal address line1";
        public static string PostalAddressLine2 = "Postal address line2";
        public static string PostalSuburb = "Postal Suburb";
        public static string Postal_PostCode = "Postal Postcode";
        public static string Postal_State = "Postal State";

        public static string TableName = "AccountHolderDesktopBroker";


        public AccountHolderDesktopBrokerColumns()
        {

            Table = new DataTable(TableName);
            Table.Columns.Add(IdentificationNumber, typeof(string));
            Table.Columns.Add(IdentificationType, typeof(string));
            Table.Columns.Add(Employer, typeof(string));
            Table.Columns.Add(Occupation, typeof(string));
            Table.Columns.Add(DateOfBirth, typeof(string));
            Table.Columns.Add(FamilyName, typeof(string));
            Table.Columns.Add(GivenNames, typeof(string));
            Table.Columns.Add(Title, typeof(string));
            Table.Columns.Add(ResidentForTaxPurpose, typeof(string));
            Table.Columns.Add(TaxFileNumber, typeof(string));
            Table.Columns.Add(TFNExemption, typeof(string));
            Table.Columns.Add(EmailAddress, typeof(string));
            Table.Columns.Add(MobilePhoneNumber, typeof(string));
            Table.Columns.Add(WorkPhoneNumber, typeof(string));
            Table.Columns.Add(HomePhoneNumber, typeof(string));
            Table.Columns.Add(PreferredContact, typeof(string));
            Table.Columns.Add(UserName, typeof(string));
            Table.Columns.Add(ResidentialAddressLine1, typeof(string));
            Table.Columns.Add(ResidentialAddressLine2, typeof(string));
            Table.Columns.Add(Residential_Suburb, typeof(string));
            Table.Columns.Add(Residential_PostCode, typeof(string));
            Table.Columns.Add(Residential_State, typeof(string));
            Table.Columns.Add(PostalAddressLine1, typeof(string));
            Table.Columns.Add(PostalAddressLine2, typeof(string));
            Table.Columns.Add(PostalSuburb, typeof(string));
            Table.Columns.Add(Postal_PostCode, typeof(string));
            Table.Columns.Add(Postal_State, typeof(string));
        }

    }

    public class CompanyDetailsDesktopBrokerColumns
    {
        public DataTable Table = null;

        public static string CompanyName = "Company Number";
        public static string ABN = "ABN";
        public static string ACN = "ACN";

        public static string TableName = "CompanyDetailsDesktopBroker";

        public CompanyDetailsDesktopBrokerColumns()
        {
            Table = new DataTable(TableName);

            Table.Columns.Add(CompanyName, typeof(string));
            Table.Columns.Add(ABN, typeof(string));
            Table.Columns.Add(ACN, typeof(string));
        }

    }

    public class TrustAccountDetailsDesktopBrokerColumns
    {

        public  DataTable Table = null;

        public static string TrustName = "Trust Name";
        public static string DesignationForAccount = "Designation For Account";
        public static string TrustABN = "Trust_ABN";
        public static string TaxFileNumber = "Tax File Number";
        public static string TFNExemption = "TFN Exemption";

        public static string TableName = "TrustAccountDetailsDesktopBroker";

        public TrustAccountDetailsDesktopBrokerColumns()
        {
            Table = new DataTable(TableName);

            Table.Columns.Add(TrustName, typeof(string));
            Table.Columns.Add(DesignationForAccount, typeof(string));
            Table.Columns.Add(TrustABN, typeof(string));
            Table.Columns.Add(TaxFileNumber, typeof(string));
            Table.Columns.Add(TFNExemption, typeof(string));

        }


        //public DataTable GetHeaderColumns()
        //{
        //    DataRow row = Table.NewRow();
        //    row[TrustName] = "Trust Name";
        //    row[DesignationForAccount] = "";
        //    row[TrustABN] = "";
        //    row[TaxFileNumber] = "";
        //    row[TFNExemption] = "";
        //}

    }

    public class TransferingStockDesktopBrokerColumns
    {
        public DataTable Table = null;

        public static string CompanyName = "Company Number";
        public static string HIN = "HIN";
        public static string PID = "PID";

        public static string TableName = "TransferingStockDesktopBroker";

        public TransferingStockDesktopBrokerColumns()
        {
            Table = new DataTable(TableName);

            Table.Columns.Add(CompanyName, typeof(string));
            Table.Columns.Add(HIN, typeof(string));
            Table.Columns.Add(PID, typeof(string));
        }


    }

    public class BankAccountDesktopBrokerColumns
    {
        public DataTable Table = null;

        public static string AccountName = "Account Name";
        public static string BSB = "BSB";
        public static string AccountNumber = "A/C Number";

        public static string TableName = "BankAccountDesktopBroker";

        public BankAccountDesktopBrokerColumns()
        {
            Table = new DataTable(TableName);

            Table.Columns.Add(AccountName, typeof(string));
            Table.Columns.Add(BSB, typeof(string));
            Table.Columns.Add(AccountNumber, typeof(string));

        }

    }

    public class SuperFundDesktopBrokerColumns
    {
        public DataTable Table = null;

        public static string SFName = "S/F Name";
        public static string DesignationForAccount = "Designation For Account";
        public static string ABN = "S/F ABN";
        public static string TaxFileNumber = "Tax File Number";
        public static string TFNExemption = "TFN Exemption";

        public static string TableName = "SuperFundDesktopBroker";

        public SuperFundDesktopBrokerColumns()
        {
            Table = new DataTable(TableName);

            Table.Columns.Add(SFName, typeof(string));
            Table.Columns.Add(DesignationForAccount, typeof(string));
            Table.Columns.Add(ABN, typeof(string));
            Table.Columns.Add(TaxFileNumber, typeof(string));
            Table.Columns.Add(TFNExemption, typeof(string));

        }

    }


    //public static class DesktopBrokerExportKeys
    //{
    //    public static readonly string Applicant_1 = "Applicant_1";
    //    public static readonly string Applicant_2 = "Applicant_2";
    //    public static readonly string BankAccount = "BankAccount";
    //    public static readonly string TransferingStock = "TransferingStock";
    //    public static readonly string TrustAccountDetails = "TrustAccountDetails";
    //    public static readonly string AccountHolder = "AccountHolder";
    //    public static readonly string Client = "Client";

    //}

    public enum DesktopBrokerExportKeys
    {
        Applicant_1,
        Applicant_2,
        BankAccount,
        TransferingStock,
        TrustAccountDetails,
        AccountHolder,
        Client,
        SuperFund,
        CompanyDetails
    }



    public class DesktopBrokerDataResponse
    {
        public Dictionary<string, DataTable> Data { get; set; }
        public OrganizationType Type { get; set; }

        public DesktopBrokerDataResponse()
        {
            Data=new Dictionary<string, DataTable>();
        }
    }




}
