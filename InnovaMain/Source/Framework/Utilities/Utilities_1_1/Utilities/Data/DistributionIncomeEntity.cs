﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    public enum DistributionIncomeStatus
    {
        _ = 0,
        Matched = 5,
        Accrued = 10
    };

    [Serializable]
    public class DistributionIncomeEntity
    {
        public Guid ID { get; set; }
        public string BGLCODE { get; set; }
        public DateTime? RecordDate { get; set; }
        public double RecodDate_Shares { get; set; }
        public DateTime? PaymentDate { get; set; }
        public Guid FundID { get; set; }
        public string FundCode { get; set; }
        public string FundName { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public double NetDistributionDPU
        {
            get
            {
                return Math.Round(NetCashDistribution.HasValue ? NetCashDistribution.Value / RecodDate_Shares : 0, 8);
            }
        }

        public double GrossDistributionDPU
        {
            get
            {
                return Math.Round(NetGrossDistribution.HasValue ? NetGrossDistribution.Value / RecodDate_Shares : 0, 8);
            }
        }
        public List<DistributionComponent> Components { get; set; }
        public double? NetCashDistribution
        {
            get
            {
                double? total = null;
                if (Components != null)
                {
                    total = Components.Sum(ss => ss.Unit_Amount);
                }
                return total;
            }
        }
        public double? NetGrossDistribution
        {
            get
            {
                double? total = null;
                if (Components != null)
                {
                    total = Components.Sum(ss => ss.GrossDistribution);
                }
                return total;
            }
        }
        public DistributionIncomeStatus Status { get; set; }
        public string CashManagementTransactionID { get; set; }
        public bool IsManualUpdated { get; set; }
        public string MisTransactionID { get; set; }
       

        public DistributionIncomeEntity()
        {
            ID = Guid.Empty;
            RecordDate = DateTime.Now;
            PaymentDate = DateTime.Now;
            Components = new List<DistributionComponent>();
            Status = DistributionIncomeStatus.Accrued;
            CashManagementTransactionID = string.Empty;
        }
        /// <summary>
        /// this method is called only when user is updating distributions from GUI. Basic Fields are left as it as they may affect 
        /// </summary>
        /// <param name="entity"></param>
        public void ManuallyUpdateValues(DistributionIncomeEntity entity)
        {
            BGLCODE = entity.BGLCODE;
            RecordDate = entity.RecordDate;
            RecodDate_Shares = entity.RecodDate_Shares;
            PaymentDate = entity.PaymentDate;
            IsManualUpdated = true;

            foreach (DistributionComponent distributionComponent in entity.Components)
            {
                var component = Components.FirstOrDefault(ss => ss.ComponentType == distributionComponent.ComponentType);
                if(component!=null)
                {
                    component.Autax_Credit = distributionComponent.Autax_Credit;
                    component.Tax_WithHeld = distributionComponent.Tax_WithHeld;
                    component.Unit_Amount = distributionComponent.Unit_Amount;
                  
                }
            }


        }
    }
}
