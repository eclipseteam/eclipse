﻿
namespace Oritax.TaxSimp.Data
{
    public interface IHasTotal<T> 
    {
        T Total { get; set; }
    }
}
