﻿using System.Collections.Generic;

namespace Oritax.TaxSimp.Data
{
    public interface IHasItem<T>
    {
        T Item { get; set; }
    }
}
