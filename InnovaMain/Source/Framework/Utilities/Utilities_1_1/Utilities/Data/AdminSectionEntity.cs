﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class AdminSectionEntity
    {
        public AccountRelation LeftAccount { get; set; }
        public AccountRelation RightAccount { get; set; }

        public string ServiceType { get; set; }

        public AdminSectionEntity()
        {
            LeftAccount = new AccountRelation();
            RightAccount = new AccountRelation();
        }
    }
}
