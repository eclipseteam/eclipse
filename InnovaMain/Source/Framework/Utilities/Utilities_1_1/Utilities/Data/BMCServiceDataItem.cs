
﻿using System;
using System.Diagnostics;
using System.Data;
using System.Xml.Serialization;

namespace Oritax.TaxSimp.Data
{
    public interface IBMCData
    {
        int Type { get; set; }
        Guid Clid { get; set; }
        Guid Csid { get; set; }
        Guid Cid { get; set; }
    }

    [Serializable]
    [DebuggerStepThroughAttribute]
    public partial class BMCServiceDS : IBMCData
    {
        [XmlElement] 
        public int Type { get; set; }
        [XmlElement]
        public Guid Clid { get; set; }
        [XmlElement]
        public Guid Csid { get; set; }
        [XmlElement]
        public Guid Cid { get; set; }
        [XmlElement]
        public DataSet Data { get; set; }
    }
}
