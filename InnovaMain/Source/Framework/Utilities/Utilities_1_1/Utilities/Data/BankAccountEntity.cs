﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Oritax.TaxSimp.Data;



namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class BankAccountEntityCsidClidDetail
    {
        public Guid Csid { get; set; }
        public Guid Clid { get; set; }
        public TitleEntity BusinessTitle { get; set; }
     

        public BankAccountEntityCsidClidDetail()
        {
            BusinessTitle = new TitleEntity();
            
        }
    }

    [Serializable]
    public static class BankAccountEntityExtensions
    {
        public static ObservableCollection<IdentityCMDetail> ToIdentityCMDetail(this ObservableCollection<BankAccountEntityCsidClidDetail> list)
        {
            ObservableCollection<IdentityCMDetail> result = new ObservableCollection<IdentityCMDetail>();
            foreach (BankAccountEntityCsidClidDetail each in list)
            {
                result.Add(new IdentityCMDetail() { Clid = each.Clid, Csid = each.Csid, BusinessTitle = each.BusinessTitle });
            }
            return result;
        }
        public static ObservableCollection<BankAccountEntityCsidClidDetail> ToIdentityCMDetail(this ObservableCollection<IdentityCMDetail> list, ObservableCollection<BankAccountEntityCsidClidDetail> BankAccountEntityCsidClidList)
        {
            ObservableCollection<BankAccountEntityCsidClidDetail> result = new ObservableCollection<BankAccountEntityCsidClidDetail>();
            foreach (BankAccountEntityCsidClidDetail entity in BankAccountEntityCsidClidList)
            {
                if (list.Where(e => e.Clid == entity.Clid && e.Csid == entity.Csid).Count() > 0)
                    result.Add(entity);
            }
            return result;
        }
    }

   
}
