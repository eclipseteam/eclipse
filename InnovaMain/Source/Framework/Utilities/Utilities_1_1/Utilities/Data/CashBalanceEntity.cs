﻿using System;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class CashBalanceEntity
    {
        public Guid ID { get; set; }
        public SuperAccountType SuperAccountType { get; set; }
        public decimal Percentage { get; set; }
        public decimal MinValue { get; set; }
        public decimal MaxValue { get; set; }

        public CashBalanceEntity()
        {
            ID = Guid.Empty;
            SuperAccountType = SuperAccountType.Super;
            Percentage = 0;
            MinValue = 0;
            MaxValue = 0;
        }
    }
}
