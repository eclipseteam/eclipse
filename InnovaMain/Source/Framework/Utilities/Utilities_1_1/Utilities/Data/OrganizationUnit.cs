﻿using System;
using System.ComponentModel;


namespace Oritax.TaxSimp.Data
{

    [Serializable]
    public class OrganizationUnit : INotifyPropertyChanged
    {
        public Guid Clid { get; set; }
        public Guid Csid { get; set; }
        public UserEntity CurrentUser { get; set; }
        private string _Name;
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if (_Name == value) return;
                _Name = value;
                NotifyPropertyChanged("Name");
            }
        }

        public string Type { get; set; }
        public string Data { get; set; }
        public bool IsInvestableClient { get; set; }
        public string OrganizationStatus { get; set; }
        public string ClientId { get; set; }
        public string AccountProcessStatus { get; set; }
        public string ApplicationID { get; set; }
        public Guid Cid { get; set; }

        public OrganizationUnit()
        {
            IsInvestableClient = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }

    [Serializable]
    public class OrganizationUnitExport : OrganizationUnit
    {
        public string IsExport { get; set; }
        public string ApplicationDate { get; set; }
    }

    [Serializable]
    public static class GetOrganizationType
    {
        public static string GetDisplayNameByEnum(OrganizationType oType)
        {
            string xml = string.Empty;
            switch (oType)
            {
                case OrganizationType.Corporate:
                    xml = "Corporate";
                    break;
                case OrganizationType.Trust:
                    xml = "Trust";
                    break;
                case OrganizationType.ClientIndividual:
                    xml = "Individuals Client";
                    break;
                case OrganizationType.DealerGroup:
                    xml = "Dealer Group";
                    break;
                case OrganizationType.MemberFirm:
                    xml = "Member Firm";
                    break;
                case OrganizationType.Adviser:
                    xml = "Adviser";
                    break;
                case OrganizationType.Individual:
                    xml = "Individual";
                    break;
                case OrganizationType.ClientCorporationPrivate:
                    xml = "Corporation - Private";
                    break;
                case OrganizationType.ClientCorporationPublic:
                    xml = "Corporation - Public";
                    break;
                case OrganizationType.ClientOtherTrustsCorporate:
                    xml = "Other Trusts - Corporate";
                    break;
                case OrganizationType.ClientOtherTrustsIndividual:
                    xml = "Other Trusts - Individual";
                    break;
                case OrganizationType.ClientSMSFCorporateTrustee:
                    xml = "SMSF – Corporate Trustee";
                    break;
                case OrganizationType.ClientSMSFIndividualTrustee:
                    xml = "SMSF – Individual Trustee/s";
                    break;
                case OrganizationType.BankAccount:
                    xml = "Bank Account";
                    break;
                case OrganizationType.PrincipalPractice:
                    xml = "Principal Practice";
                    break;
                case OrganizationType.IFA:
                    xml = "IFA";
                    break;
                case OrganizationType.ClientSoleTrader:
                    xml = "Sole Trader";
                    break;
                case OrganizationType.DesktopBrokerAccount:
                    //xml = "Desktop Broker";
                    xml = "ASX Broker";
                    break;
                case OrganizationType.ManagedInvestmentSchemesAccount:
                    xml = "Managed Investment Schemes";
                    break;
                case OrganizationType.TermDepositAccount:
                    xml = "Term Deposit";
                    break;
                case OrganizationType.Period_1_1:
                    xml = "Period";
                    break;


                default:
                    break;
            }
            return xml;
        }

        public static String GetNameByEnum(OrganizationType oType)
        {
            String result = String.Empty;
            switch (oType)
            {
                case OrganizationType.Corporate:
                    result = "Corporate";
                    break;
                case OrganizationType.Trust:
                    result = "Trust";
                    break;
                case OrganizationType.ClientIndividual:
                    result = "ClientIndividual";
                    break;
                case OrganizationType.DealerGroup:
                    result = "DealerGroup";
                    break;
                case OrganizationType.MemberFirm:
                    result = "MemberFirm";
                    break;
                case OrganizationType.PrincipalPractice:
                    result = "PrincipalPractice";
                    break;
                case OrganizationType.Adviser:
                    //result = "Advisor";
                    result = "Adviser";
                    break;
                case OrganizationType.Individual:
                    result = "Individual";
                    break;
                case OrganizationType.ClientCorporationPrivate:
                    result = "ClientCorporationPrivate";
                    break;
                case OrganizationType.ClientCorporationPublic:
                    result = "ClientCorporationPublic";
                    break;
                case OrganizationType.ClientOtherTrustsCorporate:
                    result = "ClientOtherTrustsCorporate";
                    break;
                case OrganizationType.ClientOtherTrustsIndividual:
                    result = "ClientOtherTrustsIndividual";
                    break;
                case OrganizationType.ClientSMSFCorporateTrustee:
                    result = "ClientSMSFCorporateTrustee";
                    break;
                case OrganizationType.ClientSMSFIndividualTrustee:
                    result = "ClientSMSFIndividualTrustee";
                    break;
                case OrganizationType.BankAccount:
                    result = "BankAccount";
                    break;
                case OrganizationType.IFA:
                    result = "IFA";
                    break;
                case OrganizationType.ClientSoleTrader:
                    result = "Sole Trader";
                    break;
                case OrganizationType.DesktopBrokerAccount:
                    result = "DesktopBrokerAccount";
                    break;
                case OrganizationType.ManagedInvestmentSchemesAccount:
                    result = "ManagedInvestmentSchemesAccount";
                    break;
                case OrganizationType.TermDepositAccount:
                    result = "TermDepositAccount";
                    break;
                default:
                    break;
            }
            return result;
        }

        public static OrganizationType GetEnumByName(string value)
        {
            OrganizationType result = OrganizationType.Trust;
            switch (value.ToLower())
            {
                case "corporate":
                    result = OrganizationType.Corporate;
                    break;
                case "trust":
                    result = OrganizationType.Trust;
                    break;
                case "clientindividual":
                case "individuals client":
                    result = OrganizationType.ClientIndividual;
                    break;
                case "dealergroup":
                    result = OrganizationType.DealerGroup;
                    break;
                case "memberfirm":
                    result = OrganizationType.MemberFirm;
                    break;
                case "principalpractice":
                    result = OrganizationType.PrincipalPractice;
                    break;
                case "advisor":
                case "adviser":
                    result = OrganizationType.Adviser;
                    break;
                case "individual":
                    result = OrganizationType.Individual;
                    break;
                case "clientcorporationprivate":
                case "corporation - private":
                    result = OrganizationType.ClientCorporationPrivate;
                    break;
                case "clientcorporationpublic":
                case "corporation - public":
                    result = OrganizationType.ClientCorporationPublic;
                    break;
                case "clientothertrustscorporate":
                    result = OrganizationType.ClientOtherTrustsCorporate;
                    break;
                case "clientothertrustsindividual":
                case "other trusts - individual":
                    //case "smsf – corporate trustee":
                    result = OrganizationType.ClientOtherTrustsIndividual;
                    break;
                case "clientsmsfcorporatetrustee":
                case "smsf – corporate trustee":
                case "smsf–corporatetrustee":
                    result = OrganizationType.ClientSMSFCorporateTrustee;
                    break;
                case "clientsmsfindividualtrustee":
                case "smsf – individual trustee/s":
                    result = OrganizationType.ClientSMSFIndividualTrustee;
                    break;
                case "bankaccount":
                    result = OrganizationType.BankAccount;
                    break;
                case "ifa":
                    result = OrganizationType.IFA;
                    break;
                case "clientsoletrader":
                case "sole trader":
                    result = OrganizationType.ClientSoleTrader;
                    break;
                case "desktopbrokeraccount":
                    result = OrganizationType.DesktopBrokerAccount;
                    break;
                case "managedinvestmentschemesaccount":
                    result = OrganizationType.ManagedInvestmentSchemesAccount;
                    break;
                case "termdepositaccount":
                    result = OrganizationType.TermDepositAccount;
                    break;
                default:
                    break;
            }
            return result;
        }
    }

    [Serializable]
    public class SPLibraryDocument
    {
        public UserEntity CurrentUser { get; set; }
        public string Name { get; set; }

        public SPLibraryDocument()
        {
            CurrentUser = new UserEntity();
            Name = string.Empty;
        }
    }

}
