﻿using System;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    [DebuggerStepThroughAttribute]
    public partial class BMCServiceDataItem
    {
        [XmlElement] 
        public int Type { get; set; }
        [XmlElement]
        public Guid Clid { get; set; }
        [XmlElement]
        public Guid Csid { get; set; }
        [XmlElement]
        public Guid Cid { get; set; }
[XmlElement]
        public string Data { get; set; }

    }

    [Serializable]
    [DebuggerStepThroughAttribute]
    public partial class BMCServiceDataItemByName
    {
        [XmlElement]
        public int Type { get; set; }

        [XmlElement]
        public string Data { get; set; }

        [XmlElement]
        public string ComponentName { get; set; }
        
    }

    [Serializable]
    [DebuggerStepThroughAttribute]
    public partial class BMCServiceDataItemByTrans
    {
        [XmlElement]
        public int Type { get; set; }

        [XmlElement]
        public string Data { get; set; }

        [XmlElement]
        public Guid TypeID { get; set; }

    }

    [Serializable]
    [DebuggerStepThroughAttribute]
    public partial class BMCServiceDataItemByCID
    {
        [XmlElement]
        public int Type { get; set; }

        [XmlElement]
        public string Data { get; set; }

        [XmlElement]
        public Guid Cid { get; set; }

    }
}
