﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class OrganizationChart : IIdentityCM
    {
        public Guid Clid { get; set; }
        public Guid Csid { get; set; }
        public string NodeName { get; set; }
        public string TypeName { get; set; }
        public List<OrganizationChart> Subordinates { get; set; }

        public OrganizationChart()
        {
            Subordinates = new List<OrganizationChart>();
            NodeName = string.Empty;
            TypeName = string.Empty;
        }
    }
}
