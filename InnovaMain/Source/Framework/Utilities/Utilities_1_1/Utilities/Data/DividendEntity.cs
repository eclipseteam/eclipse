﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    public enum DividendStatus
    {
        _ = 0,
        Matched = 5,
        Accrued = 10
    };
    public enum DividendType
    {
        _ = 0,
        Interim = 5,
        Final = 10,
        CapitalReturn = 15
    };

    [Serializable]
    public class DividendComponentType
    {
        public int Code { get; set; }
        public string Description { get; set; }
        public ComponentCategory Category { get; set; }

    }

    [Serializable]
    public class DividendComponent
    {


        public static Dictionary<int, DividendComponentType> ComponentsTypes = new Dictionary<int, DividendComponentType>()
                                                                    {
                                                                         { 871,new DividendComponentType(){Code = 871,Description = "Interest (not subject to NR WHT)",Category = ComponentCategory.Australian_Income}  },
                                                                        { 872,new DividendComponentType(){Code = 872,Description = "Foreign Sourced Income",Category = ComponentCategory.Foreign_Income}  }, 
                                                                        { 873,new DividendComponentType(){Code = 873,Description = "Dividends Unfranked - CFI" ,Category = ComponentCategory.Australian_Income}} ,
                                                                        { 874,new DividendComponentType(){Code = 874,Description = "Australian Foreign Capital Gain - Indexed" ,Category = ComponentCategory.Capital_Gains}} ,
                                                                        { 875,new DividendComponentType(){Code = 875,Description = "Australian Foreign Capital Gain – Other" ,Category = ComponentCategory.Capital_Gains}} ,
                                                                        { 876,new DividendComponentType(){Code = 876,Description = "Australian Foreign Discount Gain – Direct Investment"  ,Category = ComponentCategory.Capital_Gains}} ,
                                                                        { 877,new DividendComponentType(){Code = 877,Description = "Australian Foreign Discount Gain – Unit Trust" ,Category = ComponentCategory.Capital_Gains} },
                                                                        { 878,new DividendComponentType(){Code = 878,Description = "Australian Foreign CGT Concession Amount - Direct Investment" ,Category = ComponentCategory.Capital_Gains}} ,
                                                                        { 879,new DividendComponentType(){Code = 879,Description = "Australian Foreign CGT Concession Amount - Unit Trust"  ,Category = ComponentCategory.Capital_Gains}} ,
                                                                        { 880,new DividendComponentType(){Code = 880,Description = "Australian Franked Dividend" ,Category = ComponentCategory.Australian_Income}} ,
                                                                        { 881,new DividendComponentType(){Code = 881,Description = "Australian Unfranked Dividend Transactions" ,Category = ComponentCategory.Australian_Income}} ,
                                                                        { 882,new DividendComponentType(){Code = 882,Description = "Interest (subject to NR WHT)"  ,Category = ComponentCategory.Australian_Income}} ,
                                                                        { 883,new DividendComponentType(){Code = 883,Description = "Australian Other Income" ,Category = ComponentCategory.Australian_Income} },
                                                                        { 884,new DividendComponentType(){Code = 884,Description = "Australian Foreign Dividend " ,Category = ComponentCategory.Foreign_Income}} ,
                                                                        { 885,new DividendComponentType(){Code = 885,Description = "Australian Foreign Interest"  ,Category = ComponentCategory.Foreign_Income}} ,
                                                                        { 886,new DividendComponentType(){Code = 886,Description = "Australian Foreign Other Income"  ,Category = ComponentCategory.Foreign_Income} },
                                                                        { 887,new DividendComponentType(){Code = 887,Description = "Australian Capital Gain",Category = ComponentCategory.Capital_Gains}} ,
                                                                        { 888,new DividendComponentType(){Code = 888,Description = "Australian Tax Deferred Income"  ,Category = ComponentCategory.Other_Non_Assessable_Amount}} ,
                                                                        { 889,new DividendComponentType(){Code = 889,Description = "Australian Tax Free Income" ,Category = ComponentCategory.Other_Non_Assessable_Amount}} ,
                                                                        { 890,new DividendComponentType(){Code = 890,Description = "Other Income"  ,Category = ComponentCategory.Other_Non_Assessable_Amount}} ,
                                                                        { 894,new DividendComponentType(){Code = 894,Description = "CGT Concession Amount"  ,Category = ComponentCategory.Capital_Gains} },
                                                                        { 897,new DividendComponentType(){Code = 897,Description = "Dividends Franked" ,Category = ComponentCategory.Australian_Income}} ,
                                                                        { 899,new DividendComponentType(){Code = 899,Description = "Trans-Tasman dividends" ,Category = ComponentCategory.Other_Non_Assessable_Amount}},
                                                                        { 900,new DividendComponentType(){Code = 900,Description = "Return of Capital" ,Category = ComponentCategory.Other_Non_Assessable_Amount}},
                                                                    };

        public int ComponentType { get; set; }
        public string Decription
        {
            get
            {
                if (ComponentsTypes.ContainsKey(ComponentType))
                {
                    return ComponentsTypes[ComponentType].Description;
                }
                return "";
            }

        }
        public double? Unit_Amount
        {
            get;
            set;
        }
        public double? Autax_Credit { get; set; }
        public double? Tax_WithHeld { get; set; }
        public double GrossDistribution
        {
            get
            {
                double total = 0;

                if (Unit_Amount.HasValue)
                {
                    total += Unit_Amount.Value;
                }
                if (Autax_Credit.HasValue)
                {
                    total += Autax_Credit.Value;
                }
                if (Tax_WithHeld.HasValue)
                {
                    total += Tax_WithHeld.Value;
                }


                return total;
            }
        }
        public double? DividendRate { get; set; }
        public ComponentCategory Category
        {
            get
            {
                if (ComponentsTypes.ContainsKey(ComponentType))
                {
                    return ComponentsTypes[ComponentType].Category;
                }
                return ComponentCategory.Australian_Income;
            }

        }
    }


    [Serializable]
    public class DividendEntity
    {
        public Guid ID { get; set; }
        public string Investor { get; set; }
        public string BGLCode { get; set; }
        public string AdministrationSystem { get; set; }
        public string TransactionType { get; set; }
        public string InvestmentCode { get; set; }
        public string ExternalReferenceID { get; set; }
        public DateTime? RecordDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public int UnitsOnHand { get; set; }
        public double FrankedAmount { get; set; }
        public double UnfrankedAmount { get; set; }
        public double TaxFree { get; set; }
        public double TaxDeferred { get; set; }
        public double ReturnOfCapital { get; set; }
        public double DomesticInterestAmount { get; set; }
        public double ForeignInterestAmount { get; set; }
        public double DomesticOtherIncome { get; set; }
        public double ForeignOtherIncome { get; set; }
        public double DomesticWithHoldingTax { get; set; }
        public double ForeignWithHoldingTax { get; set; }
        public double DomesticImputationTaxCredits { get; set; }
        public double ForeignImputationTaxCredits { get; set; }
        public double ForeignInvestmentFunds { get; set; }
        public double DomesticIndexationCGT { get; set; }
        public double ForeignIndexationCGT { get; set; }
        public double DomesticDiscountedCGT { get; set; }
        public double ForeignDiscountedCGT { get; set; }
        public double DomesticOtherCGT { get; set; }
        public double ForeignOtherCGT { get; set; }
        public double DomesticGrossAmount { get; set; }

        public double PercentFrankedAmount { get; set; }
        public double PercentUnfrankedAmount { get; set; }
        public double PercentTaxFree { get; set; }
        public double PercentTaxDeferred { get; set; }
        public double PercentReturnOfCapital { get; set; }
        public double PercentDomesticInterestAmount { get; set; }
        public double PercentForeignInterestAmount { get; set; }
        public double PercentDomesticOtherIncome { get; set; }
        public double PercentForeignOtherIncome { get; set; }
        public double PercentDomesticWithHoldingTax { get; set; }
        public double PercentForeignWithHoldingTax { get; set; }
        public double PercentDomesticImputationTaxCredits { get; set; }
        public double PercentForeignImputationTaxCredits { get; set; }
        public double PercentForeignInvestmentFunds { get; set; }
        public double PercentDomesticIndexationCGT { get; set; }
        public double PercentForeignIndexationCGT { get; set; }
        public double PercentDomesticDiscountedCGT { get; set; }
        public double PercentForeignDiscountedCGT { get; set; }
        public double PercentDomesticOtherCGT { get; set; }
        public double PercentForeignOtherCGT { get; set; }
        public double PercentDomesticGrossAmount { get; set; }
        public double CentsPerShare { get; set; }
        public string Comment { get; set; }
        public bool ProcessFlag { get; set; }
        public ObservableCollection<DividendReportEntity> ReportCollection { get; set; }
        public Oritax.TaxSimp.Data.IdentityCM Account { get; set; }
        //public double PaidDividend { get { return UnitsOnHand * CentsPerShare; } }
        public double PaidDividend { get; set; }
        public string CashManagementTransactionID { get; set; }

        public decimal InterestRate { get; set; }
        public decimal InvestmentAmount { get; set; }
        public decimal InterestPaid { get; set; }
        public double FrankingCredit
        {
            get { return CentsPerShare * (3.0 / 7) * (PercentFrankedAmount / 100); }

        }
        public double TotalFrankingCredit
        {

            get { return (FrankingCredit * UnitsOnHand); }
        }
        public bool MannualEdit { get; set; }

        public DateTime? BalanceDate { get; set; }
        public DateTime? BooksCloseDate { get; set; }
        public DividendType Dividendtype { get; set; }
        public string Currency { get; set; }
        public DividendStatus Status { get; set; }
        public string DesktopbrokerTransID { get; set; }
        public List<DividendComponent> Components { get; set; }
        public bool IsManualUpdated { get; set; }

        public void CopyTo(DividendEntity target)
        {
            target.ID  = 	this.ID ;
            target.Investor  = 	this.Investor ;
            target.AdministrationSystem  = 	this.AdministrationSystem ;
            target.TransactionType  = 	this.TransactionType ;
            target.InvestmentCode  = 	this.InvestmentCode ;
            target.ExternalReferenceID  = 	this.ExternalReferenceID ;
            target.RecordDate  = 	this.RecordDate ;
            target.PaymentDate  = 	this.PaymentDate ;
            target.UnitsOnHand  = 	this.UnitsOnHand ;
            target.FrankedAmount  = 	this.FrankedAmount ;
            target.UnfrankedAmount  = 	this.UnfrankedAmount ;
            target.TaxFree  = 	this.TaxFree ;
            target.TaxDeferred  = 	this.TaxDeferred ;
            target.ReturnOfCapital  = 	this.ReturnOfCapital ;
            target.DomesticInterestAmount  = 	this.DomesticInterestAmount ;
            target.ForeignInterestAmount  = 	this.ForeignInterestAmount ;
            target.DomesticOtherIncome  = 	this.DomesticOtherIncome ;
            target.ForeignOtherIncome  = 	this.ForeignOtherIncome ;
            target.DomesticWithHoldingTax  = 	this.DomesticWithHoldingTax ;
            target.ForeignWithHoldingTax  = 	this.ForeignWithHoldingTax ;
            target.DomesticImputationTaxCredits  = 	this.DomesticImputationTaxCredits ;
            target.ForeignImputationTaxCredits  = 	this.ForeignImputationTaxCredits ;
            target.ForeignInvestmentFunds  = 	this.ForeignInvestmentFunds ;
            target.DomesticIndexationCGT  = 	this.DomesticIndexationCGT ;
            target.ForeignIndexationCGT  = 	this.ForeignIndexationCGT ;
            target.DomesticDiscountedCGT  = 	this.DomesticDiscountedCGT ;
            target.ForeignDiscountedCGT  = 	this.ForeignDiscountedCGT ;
            target.DomesticOtherCGT  = 	this.DomesticOtherCGT ;
            target.ForeignOtherCGT  = 	this.ForeignOtherCGT ;
            target.DomesticGrossAmount  = 	this.DomesticGrossAmount ;

            target.PercentFrankedAmount  = 	this.PercentFrankedAmount ;
            target.PercentUnfrankedAmount  = 	this.PercentUnfrankedAmount ;
            target.PercentTaxFree  = 	this.PercentTaxFree ;
            target.PercentTaxDeferred  = 	this.PercentTaxDeferred ;
            target.PercentReturnOfCapital  = 	this.PercentReturnOfCapital ;
            target.PercentDomesticInterestAmount  = 	this.PercentDomesticInterestAmount ;
            target.PercentForeignInterestAmount  = 	this.PercentForeignInterestAmount ;
            target.PercentDomesticOtherIncome  = 	this.PercentDomesticOtherIncome ;
            target.PercentForeignOtherIncome  = 	this.PercentForeignOtherIncome ;
            target.PercentDomesticWithHoldingTax  = 	this.PercentDomesticWithHoldingTax ;
            target.PercentForeignWithHoldingTax  = 	this.PercentForeignWithHoldingTax ;
            target.PercentDomesticImputationTaxCredits  = 	this.PercentDomesticImputationTaxCredits ;
            target.PercentForeignImputationTaxCredits  = 	this.PercentForeignImputationTaxCredits ;
            target.PercentForeignInvestmentFunds  = 	this.PercentForeignInvestmentFunds ;
            target.PercentDomesticIndexationCGT  = 	this.PercentDomesticIndexationCGT ;
            target.PercentForeignIndexationCGT  = 	this.PercentForeignIndexationCGT ;
            target.PercentDomesticDiscountedCGT  = 	this.PercentDomesticDiscountedCGT ;
            target.PercentForeignDiscountedCGT  = 	this.PercentForeignDiscountedCGT ;
            target.PercentDomesticOtherCGT  = 	this.PercentDomesticOtherCGT ;
            target.PercentForeignOtherCGT  = 	this.PercentForeignOtherCGT ;
            target.PercentDomesticGrossAmount  = 	this.PercentDomesticGrossAmount ;

            target.CentsPerShare  = 	this.CentsPerShare ;
            target.Comment  = 	this.Comment ;
            target.ProcessFlag  = 	this.ProcessFlag ;
            target.ReportCollection  = 	this.ReportCollection ;
            target.Account  = 	this.Account ;
            target.CashManagementTransactionID  = 	this.CashManagementTransactionID ;
            target.InterestRate  = 	this.InterestRate ;
            target.InvestmentAmount  = 	this.InvestmentAmount ;
            target.InterestPaid  = 	this.InterestPaid ;
            target.MannualEdit  = 	this.MannualEdit ;

            target.BalanceDate  = 	this.BalanceDate ;
            target.BooksCloseDate  = 	this.BooksCloseDate ;
            target.Dividendtype  = 	this.Dividendtype ;
            target.Currency  = 	this.Currency ;
            target.Status  = 	this.Status ;

            target.IsManualUpdated  = 	this.IsManualUpdated ;

            target.Components  = 	this.Components ;
        }

        public DividendEntity() 
        {
            ID = Guid.Empty;
            Investor = string.Empty;
            AdministrationSystem = string.Empty;
            TransactionType = string.Empty;
            InvestmentCode = string.Empty;
            ExternalReferenceID = string.Empty;
            RecordDate = DateTime.Now;
            PaymentDate = DateTime.Now;
            UnitsOnHand = 0;
            FrankedAmount = 0;
            UnfrankedAmount = 0;
            TaxFree = 0;
            TaxDeferred = 0;
            ReturnOfCapital = 0;
            DomesticInterestAmount = 0;
            ForeignInterestAmount = 0;
            DomesticOtherIncome = 0;
            ForeignOtherIncome = 0;
            DomesticWithHoldingTax = 0;
            ForeignWithHoldingTax = 0;
            DomesticImputationTaxCredits = 0;
            ForeignImputationTaxCredits = 0;
            ForeignInvestmentFunds = 0;
            DomesticIndexationCGT = 0;
            ForeignIndexationCGT = 0;
            DomesticDiscountedCGT = 0;
            ForeignDiscountedCGT = 0;
            DomesticOtherCGT = 0;
            ForeignOtherCGT = 0;
            DomesticGrossAmount = 0;

            PercentFrankedAmount = 0;
            PercentUnfrankedAmount = 0;
            PercentTaxFree = 0;
            PercentTaxDeferred = 0;
            PercentReturnOfCapital = 0;
            PercentDomesticInterestAmount = 0;
            PercentForeignInterestAmount = 0;
            PercentDomesticOtherIncome = 0;
            PercentForeignOtherIncome = 0;
            PercentDomesticWithHoldingTax = 0;
            PercentForeignWithHoldingTax = 0;
            PercentDomesticImputationTaxCredits = 0;
            PercentForeignImputationTaxCredits = 0;
            PercentForeignInvestmentFunds = 0;
            PercentDomesticIndexationCGT = 0;
            PercentForeignIndexationCGT = 0;
            PercentDomesticDiscountedCGT = 0;
            PercentForeignDiscountedCGT = 0;
            PercentDomesticOtherCGT = 0;
            PercentForeignOtherCGT = 0;
            PercentDomesticGrossAmount = 0;

            CentsPerShare = 0;
            Comment = "";
            ProcessFlag = new bool();
            ReportCollection = new ObservableCollection<DividendReportEntity>();
            Account = new Oritax.TaxSimp.Data.IdentityCM();
            CashManagementTransactionID = string.Empty;
            InterestRate = 0;
            InvestmentAmount = 0;
            InterestPaid = 0;
            MannualEdit = false;

            BalanceDate = DateTime.Now;
            BooksCloseDate = DateTime.Now;
            Dividendtype = DividendType.Interim;
            Currency = string.Empty;
            Status = DividendStatus.Accrued;

            IsManualUpdated = false;

            Components = new List<DividendComponent>();
        }

    }
}
