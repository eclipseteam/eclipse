﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Oritax.Serilization;
using Oritax.TaxSimp.Extensions;
namespace EPI.Data
{
  public static class XmlHelper
    {

      public static XElement[] GetElements(object[] Items)
      {
          var elements = new XElement[Items.Length];
        
          for (int i = 0; i < Items.Length; i++)
          {
              var xml = XElement.Parse(Items[i].ToXmlString());
              xml.RemoveAttributes();
              elements[i] = xml;
          }
          return elements;
      }
      public static XElement ToXElement(this MonetaryAmount amount,string NodeName)
      {
          if (amount == null) return null;
          XElement element = new XElement(NodeName);
          element.RemoveAttributes();
          element.Add(new XAttribute("Currency", amount.Currency));
          if (amount.ForeignExchangeRate!=0)
          element.Add(new XAttribute("ForeignExchangeRate", amount.ForeignExchangeRate));
          element.Add(new XText(amount.Value.ToString()));
          return element;
      }

      public static XElement ToXElement(this MonetaryAmountPositive amount, string NodeName)
      {
          if (amount == null) return null;
          XElement element = new XElement(NodeName);
          element.RemoveAttributes();
          element.Add(new XText(amount.Value.ToString()));
          return element;
      }

    }
}
