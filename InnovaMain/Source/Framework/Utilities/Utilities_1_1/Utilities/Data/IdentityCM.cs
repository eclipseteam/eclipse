﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    public interface IHaveClidCsid
    {
        Guid Clid { get; set; }
        Guid Csid { get; set; }
    }
    public interface IIdentityCMReplaceable
    {
        void Replace(IIdentityCM identity);
    }

    public interface IIdentityCM
    {
        Guid Csid { get; set; }
        Guid Clid { get; set; }
    }

    public interface IIdentityCMDetail
    {
        TitleEntity BusinessTitle { get; set; }
    }

    public interface ICmHasParent
    {
        IdentityCM Parent { get; set; }
    }

    public interface ICmHasChildren
    {
        List<IdentityCM> Children { get; set; }
    }

    public interface IIdentitySMSFCorporateTrustee
    {
        IdentityCM IdentitySMSFCorporateTrustee { get; set; }
    }

    public interface ICmHasClient
    {
        bool IsClient { get; set; }
    }

    [Serializable]
    public class IdentityCM : IIdentityCM, IIdentityCMReplaceable 
    {
        public IdentityCM() { }
        
        public IdentityCM(string clid, string csid) 
        {
            Clid = new Guid(clid);
            Csid = new Guid(csid);
        }

        public IdentityCM(Guid cid)
        {
            Cid = cid;
        }

        public Guid Csid { get; set; }
        public Guid Clid { get; set; }

        public Guid Cid { get; set; }

        public void Replace(IIdentityCM identity)
        {
            Clid = new Guid(identity.Clid.ToString());
            Csid = new Guid(identity.Csid.ToString());
        }

        public override string ToString()
        {
            return string.Format("Clid:{0}, Csid:{1}", Clid, Csid);
        }
        public static bool operator ==(IdentityCM cm1, IdentityCM cm2)
        {
            object o1 = (object)cm1;
            object o2 = (object)cm2;
            if (o1 == null && o2 == null) return true;
            if (o1 == null || o2 == null) return false;
            return ((cm1.Clid == cm2.Clid) && (cm1.Csid == cm2.Csid));
        }
        public static bool operator !=(IdentityCM cm1, IdentityCM cm2)
        {
            return !(cm1 == cm2);
        }

        public override bool Equals(Object obj)
        {
            return Equals(obj as IdentityCM);
        }

        public bool Equals(IdentityCM s)
        {
            if ((object)s == null) { return false; }
            return this == s;
        }

        public override int GetHashCode()
        {
            int result = Clid.GetHashCode();
            result = 29 * result + Csid.GetHashCode();
            return result;
        }

        public static IdentityCM Null = new IdentityCM { Clid = Guid.Empty, Csid = Guid.Empty };


    }

    public class IdentityCMType
    {
        public OrganizationType Otype { get; set; }
        public string Username { get; set; }
    }

    // code changes from here


    // code changes till here
}
