﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class InstitutionEntity
    {
        #region Properties
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public String TextInstitutionCode { get; set; }
        public String TradingName { get; set; }
        public String LegalName { get; set; }
        public bool ChkLegalName { get; set; }
        public int Type { get; set; }
        public int SPCreditRatingLong { get; set; }
        public int SPCreditRatingShort { get; set; }
        public String ACN { get; set; }
        public String ABN { get; set; }
        public String TFN { get; set; }
        public PhoneNumberEntity PhoneNumber { get; set; }
        public PhoneNumberEntity Facsimile { get; set; }
        public DualAddressEntity Addresses { get; set; }
        public ObservableCollection<IndividualEntityCsidClidDetail> Signatories { get; set; }
        public ObservableCollection<BankAccountEntityCsidClidDetail> BankAccounts { get; set; }
        public ObservableCollection<TDInstitutePriceEntity> TDPrices { get; set; }
        public ObservableCollection<AtCallInstitutePriceEntity> AtCallPrices { get; set; }
        #endregion

        #region Constructor
        public InstitutionEntity()
        {
            ID = Guid.NewGuid();
            Name = String.Empty;
            Description = string.Empty;
            TextInstitutionCode = String.Empty;
            TradingName = String.Empty;
            LegalName = String.Empty;
            ChkLegalName = false;
            Type = new int();
            SPCreditRatingLong = new int();
            SPCreditRatingShort = new int();
            ACN = String.Empty;
            ABN = String.Empty;
            TFN = String.Empty;
            PhoneNumber = new PhoneNumberEntity();
            Facsimile = new PhoneNumberEntity();
            Addresses = new DualAddressEntity();
            Signatories = new ObservableCollection<IndividualEntityCsidClidDetail>();
            BankAccounts = new ObservableCollection<BankAccountEntityCsidClidDetail>();
            TDPrices = new ObservableCollection<TDInstitutePriceEntity>();
            AtCallPrices = new ObservableCollection<AtCallInstitutePriceEntity>();
        }
        #endregion
    }
    public class InstituteCombos
    {

        public static Dictionary<string, int> Types = new Dictionary<string, int>
                                                          {
                                                              {"Administrator", 0},
                                                              {"ADI", 1},
                                                              {"Bank", 2},
                                                              {"Broker", 3},
                                                              {"Custodian", 4},
                                                              {"Fund Manager", 5},
                                                              {"Insurer", 6},
                                                              {"Investment Adviser", 7},
                                                              {"Other", 8},
                                                              {"Software provider", 9},
                                                              {"RE", 10},
                                                              {"RSE", 11}
                                                          };

        public static Dictionary<string, int> SPLongTermRatings = new Dictionary<string, int>
                                                                      {
                                                              {"AAA", 0},
                                                              {"AA+", 1},
                                                              {"AA", 2},
                                                              {"AA-", 3},
                                                              {"A+", 4},
                                                              {"A", 5},
                                                              {"A-", 6},
                                                              {"BBB+", 7},
                                                              {"BBB", 8},
                                                              {"BBB-", 9},
                                                              {"BB+", 10},
                                                              {"BB", 11},
                                                              {"BB-", 12},
                                                              {"B+", 13},
                                                              {"B", 14},
                                                              {"B-", 15},
                                                              {"CCC+", 16},
                                                              {"CCC", 17},
                                                              {"CCC-",18},
                                                              {"CC", 19},
                                                              {"C", 20},
                                                              {"D", 21},
                                                              {"Unrated",22}
                                                          };
        public static Dictionary<string, int> SPShortTermRatings = new Dictionary<string, int>
                                                                       {
                                                              {"A-1+", 0},
                                                              {"A-2", 1},
                                                              {"A-3", 2},
                                                              {"B", 3},
                                                              {"C", 4},
                                                              {"/", 5},
                                                              {"A-", 6}
                                                             
                                                          };







    }
}
