﻿using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    [DebuggerStepThroughAttribute]
    [XmlRoot]
    public partial class BMCServiceDataArray
    {
        public BMCServiceDataArray() { Items = new List<BMCServiceDataItem>(); }
 
        [XmlArray("Items")]
        [XmlArrayItem("Item")] 
        public List<BMCServiceDataItem> Items { get; set; }
    }


    [Serializable]
    [DebuggerStepThroughAttribute]
    [XmlRoot]
    public partial class BMCServiceDataArrayByName
    {
        public BMCServiceDataArrayByName() { Items = new List<BMCServiceDataItemByName>(); }

        [XmlArray("Items")]
        [XmlArrayItem("Item")]
        public List<BMCServiceDataItemByName> Items { get; set; }
    }

    [Serializable]
    [DebuggerStepThroughAttribute]
    [XmlRoot]
    public partial class BMCServiceDataArrayByTrans
    {
        public BMCServiceDataArrayByTrans() { Items = new List<BMCServiceDataItemByTrans>(); }

        [XmlArray("Items")]
        [XmlArrayItem("Item")]
        public List<BMCServiceDataItemByTrans> Items { get; set; }
    }

    [Serializable]
    [DebuggerStepThroughAttribute]
    [XmlRoot]
    public partial class BMCServiceDataArrayByCID
    {
        public BMCServiceDataArrayByCID() { Items = new List<BMCServiceDataItemByCID>(); }

        [XmlArray("Items")]
        [XmlArrayItem("Item")]
        public List<BMCServiceDataItemByCID> Items { get; set; }
    }
}
