﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using System.Xml.Serialization;

namespace Oritax.TaxSimp.Data
{
    [Serializable]    
    public class ErrorMessage
    {
        [XmlElement("Message")]
        public string Message { get; set; }
    }

    [Serializable]
    [XmlRoot("ErrorList")]
    public class ErrorList : List<ErrorMessage>
    {

    }
}
