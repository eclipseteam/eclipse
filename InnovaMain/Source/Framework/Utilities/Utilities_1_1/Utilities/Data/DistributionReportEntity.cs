﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class DistributionReportEntity
    {
        public Guid Clid { get; set; }
        public Guid Csid { get; set; }
        public Guid ID { get; set; }
        //public string ClientId { get; set; }
        public string Name { get; set; }
        public int UnitsOnHand { get; set; }
        public double CentsPerShare { get; set; }
        public DateTime? RecordDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string ClientIdentification { get; set; }
        public Oritax.TaxSimp.Data.IdentityCM Account { get; set; }
        //public double PaidDividend { get { return UnitsOnHand * CentsPerShare; } }
        public double PaidDividend { get; set; }
        public string acname { get; set; }
        public string acnumber{ get; set; }

        public DistributionReportEntity()
        {
            ID = Guid.Empty;
            Clid = Guid.Empty;
            Csid = Guid.Empty;
            //ClientId = string.Empty;
            Name = string.Empty;
            RecordDate = DateTime.Now;
            PaymentDate = DateTime.Now;
            UnitsOnHand = 0;
            CentsPerShare = 0;
            ClientIdentification = string.Empty;
            Account = new IdentityCM();
            acname = string.Empty;
            acnumber = string.Empty;

        }
    }
}
