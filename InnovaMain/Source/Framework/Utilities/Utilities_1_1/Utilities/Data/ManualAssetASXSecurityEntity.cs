﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class ManualAssetASXSecurityEntity
    {
        public Guid ID { get; set; }
        public DateTime Date { get; set; }
        public double UnitPrice { get; set; }
        public double TotalUnits { get; set; }
        public double Value { get; set; }
        public double PurValue { get; set; }
        public double NavValue { get; set; }
        public string Currency { get; set; }


        public ManualAssetASXSecurityEntity()
        {
            ID = Guid.NewGuid();
            Date = DateTime.Now;
            UnitPrice = 0;
            TotalUnits = 0;
            Value = 0;
            PurValue = 0;
            NavValue = 0;
            Currency = string.Empty;
        }
    }
}
