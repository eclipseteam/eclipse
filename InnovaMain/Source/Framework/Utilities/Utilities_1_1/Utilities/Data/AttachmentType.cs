﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public enum AttachmentType
    {
        [Description("Other")]
        Other = 0,
        [Description("Application")]
        Application = 1,
        [Description("Bank Statement")]
        BankStatement = 2,
        [Description("Business Name Registration")]
        BusinessNameRegistration = 3,
        [Description("BWA Identification and Verfication")]
        BWAIdentificationAndVerfication = 4,
        [Description("Client Periodic Inv Program Confirmation")]
        ClientPeriodicInvProgramConfirmation = 5,
        [Description("ContractNote")]
        ContractNote = 6,
        [Description("Correspondence")]
        Correspondence = 7,
        [Description("Death Certificate")]
        DeathCertificate = 8,
        [Description("Distribution Statement")]
        DistributionStatement = 9,
        [Description("Dividend Statement")]
        DividendStatement = 10,
        [Description("Financial Services Guide (FSG)")]
        FinancialServicesGuide = 11,
        [Description("Grant of Probate")]
        GrantOfProbate = 12,
        [Description("Holding Statement")]
        HoldingStatement = 13,
        [Description("Identification & Verification")]
        IdentificationVerification = 14,
        [Description("Information Memorandum (IM)")]
        InformationMemorandum = 15,
        [Description("Investment Program")]
        InvestmentProgram = 16,
        [Description("Issuer Sponsored Transfer")]
        IssuerSponsoredTransfer = 17,
        [Description("Letters of Administration")]
        LettersOfAdministration = 18,
        [Description("MDA Operators Agreement")]
        MDAOperatorsAgreement = 19,
        [Description("Member Election")]
        MemberElection = 20,
        [Description("Member Statement")]
        MemberStatement = 21,
        [Description("New Adviser")]
        NewAdviser = 22,
        [Description("Notice of Assessment")]
        NoticeOfAssessment = 23,
        [Description("Offmarket Transfers")]
        OffmarketTransfers = 24,
        [Description("Partnership Agreement")]
        PartnershipAgreement = 25,
        [Description("Product Disclosure Statement (PDS)")]
        ProductDisclosureStatement = 26,
        [Description("Product Information Statement (PIS)")]
        ProductInformationStatement = 27,
        [Description("Proof of Identity")]
        ProofOfIdentity = 28,
        [Description("Record of Advice (RoA)")]
        RecordOfAdvice = 29,
        [Description("Redemption")]
        Redemption = 30,
        [Description("Share - Corporate Action")]
        ShareCorporateAction = 31,
        [Description("Solicitors Letter")]
        SolicitorsLetter = 32,
        [Description("Statement of Advice (SoA)")]
        StatementOfAdvice = 33,
        [Description("Trust Deed")]
        TrustDeed = 34,
        [Description("Welcome Pack")]
        WelcomePack = 35,
        [Description("Will")]
        Will = 36,
        [Description("P2 Reports")]
        P2Reports = 37,
        [Description("Annual Report")]
        AnnualReport = 38
    }
}
