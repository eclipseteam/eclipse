﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    public interface IHasOrganizationUnit
    {
        OrganizationUnit Unit { get; set; }
        int Command { get; set; }
    }
    public interface ISupportRecordCount
    {
        int Count { get; set; }
    }
    public interface ISupportClientIDFilter
    {
        string ClientIDs { get; set; }
    }
    public interface IHasFileName
    {
        
        string FileName { get; set; }
    }
}
