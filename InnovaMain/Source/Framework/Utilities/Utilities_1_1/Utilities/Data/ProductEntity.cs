﻿using System;
using System.Collections.Generic;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class ProductEntity : ICloneable
    {
        #region Public Properties
        
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string TaskDescription { get; set; }
        public OrganizationType EntityType { get; set; }
        public IdentityCM EntityId { get; set; }
        public string BankAccountType { get; set; }
        public List<IdentityCM> IncludedAccounts { get; set; }
        public List<Guid> FundAccounts { get; set; }
        public double SharePercentage { get; set; }
        public double DynamicPercentage { get; set; }
        public double MinAllocation { get; set; }
        public double MaxAllocation { get; set; }
        public Guid ParentId { get; set; }
        public string ProductType { get; set; }
        public string ProductAPIR { get; set; }
        public string ProductISIN { get; set; }
        public Guid ProductSecuritiesId { get; set; }
        public bool IsDefaultProductSecurity { get; set; }
        #endregion

        public ProductEntity()
        {
            ID = Guid.NewGuid();
            Name = String.Empty;
            Description = String.Empty;
            IncludedAccounts = new List<IdentityCM>();
            FundAccounts = new List<Guid>();
            ProductType = String.Empty;
            ProductAPIR = String.Empty;
            ProductISIN = String.Empty;
            IsDefaultProductSecurity = false;
        }

        object ICloneable.Clone()
        {
            return this.Clone();
        }

        public ProductEntity Clone()
        {
            return (ProductEntity)this.MemberwiseClone();
        }
    }
}
