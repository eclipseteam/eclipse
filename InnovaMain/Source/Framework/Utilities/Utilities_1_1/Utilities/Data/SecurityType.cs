﻿using System;
using System.ComponentModel;
namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public enum SecurityType
    {
        [Description("None")]
        None,
        [Description("Funds")]
        Funds,
        [Description("Sec")]
        Sec,
        [Description("Cash")]
        Cash,
        [Description("TD")]
        TD,
        [Description("Manual Asset")]
        ManualAsset
    }
}
