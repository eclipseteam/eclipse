﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class ModelEntity
    {        
        public string Name { get; set; }
        public string Description { get; set; }
        public string ProgramCode { get; set; }
        public Guid ID { get; set; }
        public ObservableCollection<AssetEntity> Assets { get; set; }
        public double SharePercentage { get; set; }
        public double DynamicPercentage { get; set; }
        public double MinAllocation { get; set; }
        public double MaxAllocation { get; set; }
        public List<IdentityCM> LinkedClients { get; set; }
        public ServiceTypes ServiceType { get; set; }
        public bool IsPublic { get { return !IsPrivate; } }
        public bool IsPrivate { get; set; }      
            
        public ModelEntity()
        {   
            Name = string.Empty;
            Description = string.Empty;
            ID = Guid.NewGuid();
            ProgramCode = string.Empty;
            Assets = new ObservableCollection<AssetEntity>();
            LinkedClients = new List<IdentityCM>();
            IsPrivate = false;           
        }

        public void CalculateAllocation()
        {
            this.DynamicPercentage = 0;
            this.MinAllocation = 0;
            this.MaxAllocation = 0;
            this.SharePercentage = 0;

            foreach (AssetEntity assetEntity in Assets)
            {
                assetEntity.CalculateAllocation();
                this.DynamicPercentage += assetEntity.DynamicPercentage;
                this.MinAllocation += assetEntity.MinAllocation;
                this.MaxAllocation += assetEntity.MaxAllocation;
                this.SharePercentage += assetEntity.SharePercentage;
            }
        }
    }
}
