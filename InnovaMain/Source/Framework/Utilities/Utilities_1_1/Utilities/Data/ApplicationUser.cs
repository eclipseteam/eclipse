﻿using System;

namespace Oritax.TaxSimp.Data
{
    public class ApplicationUser
    {
        public string LoginId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsAdministrator { get; set; }
        public bool IsActive { get; set; }
        public bool IsLocked { get; set; }
        public Guid OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public int UserType{ get; set; }
    }
}
