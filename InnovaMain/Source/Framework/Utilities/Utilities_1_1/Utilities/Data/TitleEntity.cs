﻿using System;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class TitleEntity
    {
        public string Title { get; set; }
    }

}
