﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Data
{
    public abstract class SerializableListBase<T> : SerializableList, IList<T> where T : class
    {
        public SerializableListBase() : base() { }
        public SerializableListBase(ICollection c) : base(c) { }
        public SerializableListBase(int capacity) : base(capacity) { }
        public SerializableListBase(SerializationInfo si, StreamingContext context) : base(si, context) { }

        public int IndexOf(T item)
        {
            return base.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            base.Insert(index, item);
        }

        public new T this[int index]
        {
            get
            {
                return base[index] as T;
            }
            set
            {
                base[index] = value;
            }
        }

        public void Add(T item)
        {
            base.Add(item);
        }

        public bool Contains(T item)
        {
            return base.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            base.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            try { base.Remove(item); return true; }
            catch { return false; }
        }

        public new IEnumerator<T> GetEnumerator()
        {

            for (int i = 0; i < base.Count; i++)
            {
                yield return base[i] as T;
            }
            //yield return base.GetEnumerator().MoveNext() as T;
        }
    }
}
