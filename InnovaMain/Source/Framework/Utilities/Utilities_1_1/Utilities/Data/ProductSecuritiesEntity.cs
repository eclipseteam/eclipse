﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class ProductSecuritiesEntity
    {
        #region Public Properties

        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ObservableCollection<ProductSecurityDetail> Details { get; set; }


        #endregion

        #region Constructor

        public ProductSecuritiesEntity()
        {
            Name = string.Empty;
            Description = string.Empty;
            Details = new ObservableCollection<ProductSecurityDetail>();
        }

        #endregion
    }

    [Serializable]
    public class ProductSecurityDetail 
    {
        public Guid DetailId { get; set; }
        public Guid SecurityCodeId { get; set; }
        //public SecuritiesEntity SecurityCode { get; set; }
        public string MarketCode { get; set; }
        public string Currency { get; set; }
        public double Allocation { get; set; }
        public double PercentageAllocation { get; set; }
        public string Rating { get; set; }
        public double BuyPrice { get; set; }
        public double SellPrice { get; set; }
        public string RatingOptions { get; set; }
        public string Comments { get; set; }

        public bool IsDeactive { get; set; }


        public ProductSecurityDetail()
        {
            MarketCode = "ASX";
            Currency    = "AUD";
            Rating = string.Empty;
            RatingOptions=  string.Empty;
            Comments = string.Empty;
            IsDeactive = false;
        }
    }

   
}
