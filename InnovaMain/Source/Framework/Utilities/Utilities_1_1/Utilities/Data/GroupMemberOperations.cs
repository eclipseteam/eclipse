﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public enum GroupMemberOperations
    {
        [Description("All Clients")]
        AllClients,
        [Description("None")]
        None
    }
}
