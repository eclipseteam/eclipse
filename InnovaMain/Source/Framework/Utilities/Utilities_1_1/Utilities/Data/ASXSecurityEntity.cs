﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class ASXSecurityEntity
    {
        public Guid ID { get; set; }
        public DateTime Date { get; set; }
        public double UnitPrice { get; set; }
        public double AdjustedPrice { get; set; }
        public double InterestRate { get; set; }

        public double? PricePUR { get; set; }
        public double? PriceNAV { get; set; }
        public string Currency { get; set; }

        public ASXSecurityEntity()
        {
            ID = Guid.NewGuid();
            Date = DateTime.Now;
            UnitPrice = 0;
            AdjustedPrice = 0;
            InterestRate = 0;
            PricePUR = null;
            PriceNAV = null;
            Currency = string.Empty;
        }
    }
}
