﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class CashManagementEntity : ISerializable
    {
        public Guid ID { get; set; }
        public string InvestmentCode { get; set; }
        public string Status { get; set; }
        public string Currency { get; set; }
        public string InvestmentName { get; set; }
        public string ImportTransactionType { get; set; }
        public string SystemTransactionType { get; set; }
        public string Category { get; set; }
        public string AdministrationSystem { get; set; }
        public string ExternalReferenceID { get; set; }
        public DateTime TransactionDate { get; set; }
        public string AccountName { get; set; }
        public decimal Amount { get; set; }
        public decimal Adjustment { get; set; }
        public decimal TotalAmount { get; set; }
        public string Comment { get; set; }
        public string ContractNote { get; set; }
        public DateTime MaturityDate { get; set; }
        public decimal InterestRate { get; set; }
        public bool IsContractNote { get; set; }
        public Guid InstitutionID { get; set; }
        public string TransactionType { get; set; }
        public ServiceTypes ServiceType { get; set; }
        public Guid? Product { get; set; }

        public CashManagementEntity()
        {
            ID = Guid.Empty;
            InvestmentCode = string.Empty;
            Status = string.Empty;
            Currency = string.Empty;
            InvestmentName = string.Empty;
            ImportTransactionType = string.Empty;
            SystemTransactionType = string.Empty;
            Category = string.Empty;
            AdministrationSystem = string.Empty;
            ExternalReferenceID = string.Empty;
            TransactionDate = DateTime.Now;
            AccountName = string.Empty;
            Amount = 0;
            Adjustment = 0;
            TotalAmount = 0;
            Comment = string.Empty;
            ContractNote = string.Empty;
            MaturityDate = DateTime.Now;
            InterestRate = 0;
            InstitutionID = Guid.Empty;
        }


        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("<ID>k__BackingField", ID);
            info.AddValue("<InvestmentCode>k__BackingField", InvestmentCode);
            info.AddValue("<Status>k__BackingField", Status);
            info.AddValue("<Currency>k__BackingField", Currency);
            info.AddValue("<InvestmentName>k__BackingField", InvestmentName);
            info.AddValue("<ImportTransactionType>k__BackingField", ImportTransactionType);
            info.AddValue("<SystemTransactionType>k__BackingField", SystemTransactionType);
            info.AddValue("<Category>k__BackingField", Category);
            info.AddValue("<AdministrationSystem>k__BackingField", Adjustment);
            info.AddValue("<ExternalReferenceID>k__BackingField", ExternalReferenceID);
            info.AddValue("<TransactionDate>k__BackingField", TransactionDate);
            info.AddValue("<AccountName>k__BackingField", AccountName);
            info.AddValue("<Amount>k__BackingField", Amount);
            info.AddValue("<Adjustment>k__BackingField", Adjustment);
            info.AddValue("<TotalAmount>k__BackingField", TotalAmount);
            info.AddValue("<Comment>k__BackingField", Comment);

            info.AddValue("<IsContractNote>k__BackingField", IsContractNote);
            info.AddValue("<InstitutionID>k__BackingField", InstitutionID);
            info.AddValue("<ServiceType>k__BackingField", ServiceType);
            info.AddValue("<Product>k__BackingField", Product);
            info.AddValue("<TransactionType>k__BackingField", TransactionType);
        }

        protected CashManagementEntity(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new System.ArgumentNullException("info");

            ID = (Guid)info.GetValue("<ID>k__BackingField", typeof(Guid));
            InvestmentCode = (string)info.GetValue("<InvestmentCode>k__BackingField", typeof(string));
            Status = (string)info.GetValue("<Status>k__BackingField", typeof(string));
            Currency = (string)info.GetValue("<Currency>k__BackingField", typeof(string));
            InvestmentName = (string)info.GetValue("<InvestmentName>k__BackingField", typeof(string));
            ImportTransactionType = (string)info.GetValue("<ImportTransactionType>k__BackingField", typeof(string));
            SystemTransactionType = (string)info.GetValue("<SystemTransactionType>k__BackingField", typeof(string));
            Category = (string)info.GetValue("<Category>k__BackingField", typeof(string));
            AdministrationSystem = (string)info.GetValue("<AdministrationSystem>k__BackingField", typeof(string));
            ExternalReferenceID = (string)info.GetValue("<ExternalReferenceID>k__BackingField", typeof(string));
            TransactionDate = (DateTime)info.GetValue("<TransactionDate>k__BackingField", typeof(DateTime));
            AccountName = (string)info.GetValue("<AccountName>k__BackingField", typeof(string));
            Amount = (decimal)info.GetValue("<Amount>k__BackingField", typeof(decimal));
            Adjustment = (decimal)info.GetValue("<Adjustment>k__BackingField", typeof(decimal));
            TotalAmount = (decimal)info.GetValue("<TotalAmount>k__BackingField", typeof(decimal));
            Comment = (string)info.GetValue("<Comment>k__BackingField", typeof(string));


            IsContractNote = (bool)info.GetValue("<IsContractNote>k__BackingField", typeof(bool));

            InstitutionID = (Guid)info.GetValue("<InstitutionID>k__BackingField", typeof(Guid));
            TransactionType = (string)info.GetValue("<TransactionType>k__BackingField", typeof(string));
            var serviceType = (int)info.GetValue("<ServiceType>k__BackingField", typeof(int));
            if (Enum.IsDefined(typeof(ServiceTypes), serviceType))
                ServiceType = (ServiceTypes)serviceType;
            if (info.GetValue("<Product>k__BackingField", typeof(Guid)) != null)
                Product = (Guid)info.GetValue("<Product>k__BackingField", typeof(Guid));
            else
                Product = Guid.Empty; 
        }

        public static Dictionary<string, string> GetImportTransactionDataSource()
        {
            Dictionary<string, string> _ImportTransaction = new Dictionary<string, string>();
            _ImportTransaction.Add("------", "none");
            _ImportTransaction.Add("Deposit", "Deposit");
            _ImportTransaction.Add("Withdrawal", "Withdrawal");
            return _ImportTransaction;
        }

        public static Dictionary<string, string> GetCategoryDataSource(string filter)
        {
            Dictionary<string, string> _Category = new Dictionary<string, string>();

            switch (filter)
            {
                case "Withdrawal":
                    _Category.Add("------", "none");
                    _Category.Add("Benefit Payment", "Benefit Payment");
                    _Category.Add("Expense", "Expense");
                    _Category.Add("Investment", "Investment");
                    _Category.Add("TAX", "TAX");
                    break;
                case "Deposit":
                    _Category.Add("------", "none");
                    _Category.Add("Contribution", "Contribution");
                    _Category.Add("Income", "Income");
                    _Category.Add("Investment", "Investment");
                    _Category.Add("TAX", "TAX");
                    break;
            }

            return _Category;
        }


        public static Dictionary<string, string> GetSystemTransactionDataSource(string filter)
        {
            Dictionary<string, string> _SystemTransaction = new Dictionary<string, string>();

            switch (filter)
            {
                case "Benefit Payment":
                    _SystemTransaction.Add("------", "none");
                    _SystemTransaction.Add("Withdrawal", "Withdrawal");

                    break;

                case "Expense":
                    _SystemTransaction.Add("------", "none");
                    _SystemTransaction.Add("Accounting Expense", "Accounting Expense");
                    _SystemTransaction.Add("Administration Fee", "Administration Fee");
                    _SystemTransaction.Add("Advisory Fee", "Advisory Fee");
                    _SystemTransaction.Add("General", "General");
                    _SystemTransaction.Add("Insurance Premium", "Insurance Premium");
                    _SystemTransaction.Add("Internal Cash Movement", "Internal Cash Movement");
                    _SystemTransaction.Add("Investment Fee", "Investment Fee");
                    _SystemTransaction.Add("Legal Expense", "Legal Expense");
                    _SystemTransaction.Add("Pension Payment", "Pension Payment");
                    _SystemTransaction.Add("Property", "Property");
                    _SystemTransaction.Add("Regulatory Fee", "Regulatory Fee");
                    _SystemTransaction.Add("Transfer Out", "Transfer Out");
                    break;

                case "Investment":
                    _SystemTransaction.Add("------", "none");
                    _SystemTransaction.Add("Application", "Application");
                    _SystemTransaction.Add("Redemption", "Redemption");
                    break;

                case "TAX":
                    _SystemTransaction.Add("------", "none");
                    _SystemTransaction.Add("Business Activity Statement", "Business Activity Statement");
                    _SystemTransaction.Add("Instalment Activity Statement", "Instalment Activity Statement");
                    _SystemTransaction.Add("Income Tax", "Income Tax");
                    _SystemTransaction.Add("PAYG", "PAYG");
                    _SystemTransaction.Add("Tax Refund", "Tax Refund");
                    break;
                case "Contribution":
                    _SystemTransaction.Add("------", "none");
                    _SystemTransaction.Add("Employer Additional", "Employer Additional");
                    _SystemTransaction.Add("Employer SG", "Employer SG");
                    _SystemTransaction.Add("Personal", "Personal");
                    _SystemTransaction.Add("Salary Sacrifice", "Salary Sacrifice");
                    _SystemTransaction.Add("Spouse", "Spouse");
                    break;
                case "Income":
                    _SystemTransaction.Add("------", "none");
                    _SystemTransaction.Add("Commission Rebate", "Commission Rebate");
                    _SystemTransaction.Add("Distribution", "Distribution");
                    _SystemTransaction.Add("Dividend", "Dividend");
                    _SystemTransaction.Add("Interest", "Interest");
                    _SystemTransaction.Add("Transfer In", "Transfer In");
                    _SystemTransaction.Add("Internal Cash Movement", "Internal Cash Movement");
                    _SystemTransaction.Add("Rental Income", "Rental Income");
                    break;


            }

            return _SystemTransaction;
        }

    }

}
