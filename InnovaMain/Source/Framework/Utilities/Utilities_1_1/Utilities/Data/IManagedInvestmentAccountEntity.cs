﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.Data
{
    public interface IManagedInvestmentAccountEntity
   {
       void FillDS(ManagedInvestmentAccountDS DS,Guid ID);
   }
}
