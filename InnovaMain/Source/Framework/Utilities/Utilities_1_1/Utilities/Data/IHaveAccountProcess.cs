﻿using System;
using System.Collections.Generic;

namespace Oritax.TaxSimp.Data
{
    public interface IHaveAccountProcess
    {
        string GetAccountProcessStatus();
    }
}
