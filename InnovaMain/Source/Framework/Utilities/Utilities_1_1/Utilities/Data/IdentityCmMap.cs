﻿using System.Collections.Generic;

namespace Oritax.TaxSimp.Data
{
    public class IdentityCmMap : Dictionary<IIdentityCM, IIdentityCM>
    {
        public void ReplaceOrNull<T>(T id)
            where T : IIdentityCM, IIdentityCMReplaceable
        {
            IIdentityCM value;
            this.TryGetValue(id, out value);
            if (value == null) value = IdentityCM.Null;
            id.Replace(value);
        }

        public void ReplaceOrRemove<T>(List<T> ids)
            where T : IIdentityCM, IIdentityCMReplaceable
        {
            IIdentityCM value;
            List<T> removed = new List<T>();
            foreach (T each in ids)
            {
                value = null;
                this.TryGetValue(each, out value);
                if (value == null) removed.Add(each);
                each.Replace(value);
            }
            removed.ForEach(i => ids.Remove(i));
        }

    }
}
