﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class ClientManualAssetEntity
    {
        public Guid ID { get; set; }
        public string AdministrationSystem { get; set; }
        public Guid InvestmentCode { get; set; }
        public decimal UnitsOnHand { get; set; }
        public DateTime TransactionDate { get; set; }
        public DateTime SettlementDate { get; set; }
        public string TransactionType { get; set; }
        public PriceType PriceType = PriceType.UnitPrice;

        public ClientManualAssetEntity()
        {
            ID = Guid.Empty;
            AdministrationSystem = string.Empty; TransactionType = string.Empty;
            UnitsOnHand = 0;
            InvestmentCode = Guid.Empty;
            TransactionDate = DateTime.Now;
            SettlementDate = DateTime.Now;
        }
    }
}
