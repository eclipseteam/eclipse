﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class AssetEntity : ICloneable
    {
        #region Properties
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double SharePercentage { get; set; }
        public double DynamicPercentage { get; set; }
        public double MinAllocation { get; set; }
        public double MaxAllocation { get; set; }
        public ObservableCollection<ProductEntity> Products { get; set; }
        public Guid ParentId { get; set; }
        #endregion

        #region Constructor
        public AssetEntity()
        {
            ID = Guid.NewGuid();
            Name = String.Empty;
            Description = string.Empty;
            Products = new ObservableCollection<ProductEntity>();
        }

        object ICloneable.Clone()
        {
            return this.Clone();
        }

        public AssetEntity Clone()
        {
            return (AssetEntity)this.MemberwiseClone();
        }

        public void CalculateAllocation()
        {
            this.DynamicPercentage = 0;
            this.MinAllocation = 0;
            this.MaxAllocation = 0;
            this.SharePercentage = 0;

            foreach (ProductEntity productEntity in Products)
            {
                this.DynamicPercentage += productEntity.DynamicPercentage;
                this.MinAllocation += productEntity.MinAllocation;
                this.MaxAllocation += productEntity.MaxAllocation;
                this.SharePercentage += productEntity.SharePercentage;
            }
        }

        #endregion
    }

    [Serializable]
    public class BusinessGroupEntity
    {
        #region Properties
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double SharePercentage { get; set; }
        public double DynamicPercentage { get; set; }
        public ObservableCollection<ProductEntity> Products { get; set; }
        public Guid ParentId { get; set; }
        #endregion

        #region Constructor
        public BusinessGroupEntity()
        {
            ID = Guid.NewGuid();
            Name = String.Empty;
            Description = string.Empty;
            Products = new ObservableCollection<ProductEntity>();
        }
        #endregion
    }
}
