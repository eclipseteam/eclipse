﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class IdentityCMDetail : IdentityCM
    {
        public TitleEntity BusinessTitle { get; set; }
        public bool IsPrimary { get; set; }
        public Guid FundID { get; set; }
        public ReinvestmentOption ReinvestmentOption { get; set; }
        public IdentityCMDetail()
        {
            BusinessTitle = new TitleEntity();
            IsPrimary = false;
            FundID = new Guid();
        }
    }
}
