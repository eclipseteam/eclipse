﻿using System;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class MobileNumberEntity
    {
        public string CountryCode { get; set; }
        public string MobileNumber { get; set; }
        public MobileNumberEntity()
        {
            CountryCode = string.Empty;
            MobileNumber = string.Empty;
        }

        public new string ToString()
        {
            if (string.IsNullOrEmpty(CountryCode) && string.IsNullOrEmpty(MobileNumber))
                return string.Empty;
            return string.Format("(+{0})-{1}", CountryCode, MobileNumber);
        }

        public bool IsEmpty()
        {

            return string.IsNullOrEmpty(CountryCode) && string.IsNullOrEmpty(MobileNumber);
        }
    }
}
