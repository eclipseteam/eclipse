﻿using System.Collections.Generic;

namespace Oritax.TaxSimp.Data
{
    public class EntityTag
    {
        public IDictionary<string, string> Tagged { get; private set; }
        public FormMap Map { get; private set; }
        public string Appender { get; private set; }
        public EntityTag SetAppender(string appender)
        {
            Appender = appender;
            return this;
        }
        public int Counter { get; private set; }
        public EntityTag SetCounter(int counter)
        {
            Counter = counter;
            return this;
        }


        public EntityTag(IDictionary<string, string> tagged, FormMap map)
        {
            Tagged = tagged;
            Map = map;
            Appender = string.Empty;
        }
    }
}
