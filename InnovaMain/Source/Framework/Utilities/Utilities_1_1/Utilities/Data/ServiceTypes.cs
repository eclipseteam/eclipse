﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public enum ServiceTypes
    {
        [Description("Do It Yourself")]
        DoItYourSelf,
        [Description("Do It For Me")]
        DoItForMe,
        [Description("Do It With Me")]
        DoItWithMe,
        [Description("None")]
        None,
        [Description("Manual Asset")]
        ManualAsset,
        [Description("Custom")]
        Custom,
        [Description("All")]
        All,
        [Description("Accrual")]
        Accrual
    }
}
