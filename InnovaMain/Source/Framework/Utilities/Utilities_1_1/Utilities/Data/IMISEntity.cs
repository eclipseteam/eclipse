﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Data
{
   public interface IMISEntity
   {
       bool HasFund(string FundCode, Guid FundID);
   }
}
