﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.Data
{
    public interface IBankAccountEntity
    {
        void FillDataset(BankAccountDS dataSet);
    }
}
