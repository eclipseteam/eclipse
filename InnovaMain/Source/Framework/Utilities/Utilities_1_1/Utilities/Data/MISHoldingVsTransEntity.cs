﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.Data
{
    public class MISHoldingVsTransEntity
    {
        #region Properties
        public Guid ID { get; set; }
        public string FundCode { get; set; }
        public string FundName { get; set; }
        public decimal Holding { get; set; }
        public decimal Transaction { get; set; }
        public string ClientID { get; set; }
       
        #endregion

        #region Constructor
        public MISHoldingVsTransEntity()
        {
            ID = Guid.NewGuid(); 
        }
        #endregion
    }


}
