﻿using System;


namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class DualAddressEntity
    {
        public AddressEntity BusinessAddress { get; set; }
        public AddressEntity ResidentialAddress { get; set; }
        public AddressEntity RegisteredAddress { get; set; }

        public AddressEntity MailingAddress { get; set; }
        public AddressEntity DuplicateMailingAddress { get; set; }

        public bool RegisteredAddressSame { get; set; }
        public bool MailingAddressSame { get; set; }
        public bool DuplicateMailingAddressSame { get; set; }

        public DualAddressEntity()
        {
            BusinessAddress = new AddressEntity();
            ResidentialAddress = new AddressEntity();
            RegisteredAddress = new AddressEntity();

            MailingAddress = new AddressEntity();
            DuplicateMailingAddress = new AddressEntity();

            RegisteredAddressSame = true;
            MailingAddressSame = true;
            DuplicateMailingAddressSame = true;
        }
    }
}
