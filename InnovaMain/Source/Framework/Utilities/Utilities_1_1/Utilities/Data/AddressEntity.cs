﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace  Oritax.TaxSimp.Data
{
    [Serializable]
    public class AddressEntity
    {
        public string Addressline1 { get; set; }
        public string Addressline2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }


        public AddressEntity()
        {
            Addressline1 = string.Empty;
            Addressline2 = string.Empty;
            Suburb = string.Empty;
            State = string.Empty;
            PostCode = string.Empty;
            Country = string.Empty;

        }
    }
}
