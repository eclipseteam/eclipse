﻿using System.Xml.Linq;

namespace Oritax.TaxSimp.Data
{
    public interface IXElementable
    {
        void ToXElement(XElement element);
        void FromXElement(XElement element);
    }
}
