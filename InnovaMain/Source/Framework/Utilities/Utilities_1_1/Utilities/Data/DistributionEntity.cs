﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public enum ComponentCategory
    {
        [Description("Australian Income")]
        Australian_Income,
        [Description("Capital Gains")]
        Capital_Gains,
        [Description("Foreign Income")]
        Foreign_Income,
        [Description("Other Non Assessable")]
        Other_Non_Assessable_Amount
    }

    [Serializable]
    public class DistributionComponentType
    {
        public int Code { get; set; }
        public string Description { get; set; }
        public ComponentCategory Category { get; set; }

    }

    [Serializable]
    public class DistributionComponent
    {


        public static Dictionary<int, DistributionComponentType> ComponentsTypes = new Dictionary<int, DistributionComponentType>()
                                                                    {
                                                                         { 871,new DistributionComponentType(){Code = 871,Description = "Interest (not subject to NR WHT)",Category = ComponentCategory.Australian_Income}  },
                                                                        { 872,new DistributionComponentType(){Code = 872,Description = "Foreign Sourced Income",Category = ComponentCategory.Foreign_Income}  }, 
                                                                        { 873,new DistributionComponentType(){Code = 873,Description = "Dividends Unfranked - CFI" ,Category = ComponentCategory.Australian_Income}} ,
                                                                        { 874,new DistributionComponentType(){Code = 874,Description = "Australian Foreign Capital Gain - Indexed" ,Category = ComponentCategory.Capital_Gains}} ,
                                                                        { 875,new DistributionComponentType(){Code = 875,Description = "Australian Foreign Capital Gain – Other" ,Category = ComponentCategory.Capital_Gains}} ,
                                                                        { 876,new DistributionComponentType(){Code = 876,Description = "Australian Foreign Discount Gain – Direct Investment"  ,Category = ComponentCategory.Capital_Gains}} ,
                                                                        { 877,new DistributionComponentType(){Code = 877,Description = "Australian Foreign Discount Gain – Unit Trust" ,Category = ComponentCategory.Capital_Gains} },
                                                                        { 878,new DistributionComponentType(){Code = 878,Description = "Australian Foreign CGT Concession Amount - Direct Investment" ,Category = ComponentCategory.Capital_Gains}} ,
                                                                        { 879,new DistributionComponentType(){Code = 879,Description = "Australian Foreign CGT Concession Amount - Unit Trust"  ,Category = ComponentCategory.Capital_Gains}} ,
                                                                        { 880,new DistributionComponentType(){Code = 880,Description = "Australian Franked Dividend" ,Category = ComponentCategory.Australian_Income}} ,
                                                                        { 881,new DistributionComponentType(){Code = 881,Description = "Australian Unfranked Dividend Transactions" ,Category = ComponentCategory.Australian_Income}} ,
                                                                        { 882,new DistributionComponentType(){Code = 882,Description = "Interest (subject to NR WHT)"  ,Category = ComponentCategory.Australian_Income}} ,
                                                                        { 883,new DistributionComponentType(){Code = 883,Description = "Australian Other Income" ,Category = ComponentCategory.Australian_Income} },
                                                                        { 884,new DistributionComponentType(){Code = 884,Description = "Australian Foreign Dividend " ,Category = ComponentCategory.Foreign_Income}} ,
                                                                        { 885,new DistributionComponentType(){Code = 885,Description = "Australian Foreign Interest"  ,Category = ComponentCategory.Foreign_Income}} ,
                                                                        { 886,new DistributionComponentType(){Code = 886,Description = "Australian Foreign Other Income"  ,Category = ComponentCategory.Foreign_Income} },
                                                                        { 887,new DistributionComponentType(){Code = 887,Description = "Australian Capital Gain",Category = ComponentCategory.Capital_Gains}} ,
                                                                        { 888,new DistributionComponentType(){Code = 888,Description = "Australian Tax Deferred Income"  ,Category = ComponentCategory.Other_Non_Assessable_Amount}} ,
                                                                        { 889,new DistributionComponentType(){Code = 889,Description = "Australian Tax Free Income" ,Category = ComponentCategory.Other_Non_Assessable_Amount}} ,
                                                                        { 890,new DistributionComponentType(){Code = 890,Description = "Other Income"  ,Category = ComponentCategory.Other_Non_Assessable_Amount}} ,
                                                                        { 894,new DistributionComponentType(){Code = 894,Description = "CGT Concession Amount"  ,Category = ComponentCategory.Capital_Gains} },
                                                                        { 897,new DistributionComponentType(){Code = 897,Description = "Dividends Franked" ,Category = ComponentCategory.Australian_Income}} ,
                                                                        { 899,new DistributionComponentType(){Code = 899,Description = "Trans-Tasman dividends" ,Category = ComponentCategory.Other_Non_Assessable_Amount}}
                                                                    };

        public int ComponentType { get; set; }
        public string Decription
        {
            get
            {
                if (ComponentsTypes.ContainsKey(ComponentType))
                {
                    return ComponentsTypes[ComponentType].Description;
                }
                return "";
            }

        }
        public double? Unit_Amount
        {
            get;
            set;
        }
        public double? Autax_Credit { get; set; }
        public double? Tax_WithHeld { get; set; }
        public double GrossDistribution
        {
            get
            {
                double total = 0;

                if (Unit_Amount.HasValue)
                {
                    total += Unit_Amount.Value;
                }
                if (Autax_Credit.HasValue)
                {
                    total += Autax_Credit.Value;
                }
                if (Tax_WithHeld.HasValue)
                {
                    total += Tax_WithHeld.Value;
                }


                return total;
            }
        }
        public double? DividendRate { get; set; }
        public ComponentCategory Category
        {
            get
            {
                if (ComponentsTypes.ContainsKey(ComponentType))
                {
                    return ComponentsTypes[ComponentType].Category;
                }
                return ComponentCategory.Australian_Income;
            }

        }
    }

    [Serializable]
    public class DistributionEntity
    {
        public Guid ID { get; set; }
        public DateTime? RecordDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public bool ProcessFlag { get; set; }
        public List<DistributionComponent> Components { get; set; }
        public string Code { get; set; }
        public DistributionEntity()
        {
            ID = Guid.Empty;
            RecordDate = DateTime.Now;
            PaymentDate = DateTime.Now;
            ProcessFlag = new bool();
            Components = new List<DistributionComponent>();
            Code = string.Empty;
        }
    }
}
