﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public enum StatusType
    {
        [Description("None")]
        None = 0,
        [Description("Pending")]
        Pending = 1,
        [Description("In Progress")]
        InProgress = 2,
        [Description("Under Review")]
        UnderReview = 3,
        [Description("Approved")]
        Approved = 4,
        [Description("Deactivated")]
        Deactivated = 5,
        [Description("Active")]
        Active = 6,
        [Description("Closed")]
        Closed = 7,
    }
}
