﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class GoalsDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public GoalsTable GoalsTable = new GoalsTable();

        public GoalsDS()
        {
            Tables.Add(GoalsTable);
        }

        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
    }

    public class GoalsTable : DataTable
    {
        private readonly string TABLENAME = "GoalsList";
        public readonly string ADVICESID = "AdvicesId";
        public readonly string GOALID = "GoalId";
        public readonly string GOALTYPE = "GoalType";
        public readonly string AMOUNT = "Amount";
        public readonly string AGE = "Age";
    
        internal GoalsTable()
        {
            TableName = TABLENAME;
            Columns.Add(ADVICESID, typeof(Guid));
            Columns.Add(GOALID, typeof(Guid));
            Columns.Add(GOALTYPE, typeof(int));
            Columns.Add(AMOUNT, typeof(int));
            Columns.Add(AGE, typeof(int));
        }
    }
}
