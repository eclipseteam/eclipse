﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;
using System.Collections.Generic;

namespace Oritax.TaxSimp.DataSets
{
    public class AdvicesDS : DataSet, IHasOrganizationUnit
    {
        public Guid IndividualCID { get; set; }
        public Guid AdviceID { get; set; }
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public AdvicesTable AdvicesTable = new AdvicesTable();
        public PersonalDetailsTable PersonalDetailsTable = new PersonalDetailsTable();
        public AdviceActionTable AdviceActionTable = new AdviceActionTable();
        public GoalsTable GoalsTable = new GoalsTable();
        public CurrentAssetsTable CurrentAssetsTable = new CurrentAssetsTable();

        public AdvicesDS()
        {
            Tables.Add(AdvicesTable);
            Tables.Add(PersonalDetailsTable);
            Tables.Add(AdviceActionTable);
            Tables.Add(GoalsTable);
            Tables.Add(CurrentAssetsTable);
        }

        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
    }

    public class AdvicesTable : DataTable
    {
        private readonly string TABLENAME = "AdvicesList";
        public readonly string ID = "Id";
        public readonly string TITLE = "Title";
        public readonly string CLIENTCID = "ClientCid";
        public readonly string CLIENTID = "ClientID";
        public readonly string INDIVIDUALCID = "IndividualCid";
        public readonly string INDIVIDUALCLIENTID = "IndividualClientID";
        public readonly string CID = "CID";
        public readonly string USER = "User";
        public readonly string CREATEDON = "CreatedOn";
        public readonly string MODIFIEDON = "ModifiedOn";
        public readonly string INVESTMENTSTRATEGY = "InvestmentStrategy";

        internal AdvicesTable()
        {
            TableName = TABLENAME;
            Columns.Add(TITLE, typeof(string));
            Columns.Add(CID, typeof(Guid));
            Columns.Add(ID, typeof(Guid));
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(CLIENTCID, typeof(Guid));
            Columns.Add(INDIVIDUALCLIENTID, typeof(string));
            Columns.Add(INDIVIDUALCID, typeof(Guid));
            Columns.Add(USER, typeof(Guid));
            Columns.Add(CREATEDON, typeof(DateTime));
            Columns.Add(MODIFIEDON, typeof(DateTime));
            Columns.Add(INVESTMENTSTRATEGY, typeof(string));
        }

        public AdvicesRow GetNewRow()
        {
            AdvicesRow row = (AdvicesRow)NewRow();
            return row;
        }

        public void Add(AdvicesRow row)
        {
            Rows.Add(row);
        }

        protected override Type GetRowType()
        {
            return typeof(AdvicesRow);
        }

        protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
        {
            return new AdvicesRow(builder);
        }
    }

    public class AdvicesRow : DataRow
    {
        public List<Guid> UserList { get; set; }
        public SortedList<string, DataSet> MillimanResultSet { get; set; }

        protected internal AdvicesRow(DataRowBuilder builder)
            : base(builder)
        {
        }
    }
}
