﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public partial class AlertDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public ClientAdviserTable ClientAdviserTable = new ClientAdviserTable();

        public AlertDS()
        {
            Tables.Add(ClientAdviserTable);
        }

        public AlertDS(IContainer container)
        {
            container.Add(this);

            //InitializeComponent();
        }

        public OrganizationUnit Unit
        {
            get; 
            set; 
        }

        public int Command
        {
            get;
            set; 
        }
    }

    public class ClientAdviserTable : DataTable
    {
        public const string TABLENAME = "ClientAdviser";
        public const string ClientCID = "ClientCID";
        public const string ClientID = "ClientID";

        public const string ClientName = "ClientName";

        public const string AdviserCID = "AdviserCID";
        public const string AdviserID = "AdviserID";
        public const string AdviserName = "AdviserName";
        public const string ServiceType = "ServiceType";
        public const string MDAExecutionDate = "MDAExecutionDate";

        public ClientAdviserTable()
        {
            TableName = TABLENAME;
            Columns.Add(ClientCID, typeof(Guid));
            Columns.Add(ClientID, typeof(string));
            Columns.Add(ClientName, typeof(string));
            Columns.Add(AdviserCID, typeof(Guid));
            Columns.Add(AdviserID, typeof(string));
            Columns.Add(AdviserName, typeof(string));
            Columns.Add(ServiceType, typeof(string));
            Columns.Add(MDAExecutionDate, typeof(DateTime));

        }

    }

}
