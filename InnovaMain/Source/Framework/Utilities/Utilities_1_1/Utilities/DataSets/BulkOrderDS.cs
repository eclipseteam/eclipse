﻿using System;
using System.Collections.Generic;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class BulkOrderDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public BulkOrderList BulkOrdersTable = new BulkOrderList();
        public BulkOrderItemList BulkOrderItemsTable = new BulkOrderItemList();
        public BulkOrderItemDetailList BulkOrderItemDetailsList = new BulkOrderItemDetailList();
        public List<Guid> BulkOrderIDs { get; set; }

        public BulkOrderDS()
        {
            Tables.Add(BulkOrdersTable);
            Tables.Add(BulkOrderItemsTable);
            Tables.Add(BulkOrderItemDetailsList);
        }

        public OrganizationUnit Unit { get; set; }
        public int Command { get; set; }

    }
    public class BulkOrderList : DataTable
    {
        private readonly string TABLENAME = "BulkOrdersList";

        public readonly string ID = "ID";
        public readonly string BULKORDERID = "BulkOrderID";
        public readonly string STATUS = "Status";
        public readonly string ORDERACCOUNTTYPE = "OrderAccountType";
        public readonly string ORDERTYPE = "OrderType";
        public readonly string ORDERITEMTYPE = "OrderItemType";
        public readonly string CREATEDDATE = "CreatedDate";
        public readonly string UPDATEDDATE = "UpdatedDate";
        public readonly string UPDATEDBY = "UpdatedBy";
        public readonly string UPDATEDBYCID = "UpdatedByCID";
        public readonly string ISEXPORTED = "IsExported";
        public readonly string TRADEDATE = "TradeDate";
        public readonly string CLIENTMANAGEMENTTYPE = "ClientManagementType";
        public readonly string ORDERPREFEREDBY = "OrderPreferedBy";
        public readonly string SMAREQUESTID = "SMARequestID";
        public readonly string SMACALLSTATUS = "SMACallStatus";

        internal BulkOrderList()
        {
            TableName = TABLENAME;
            Columns.Add(ID, typeof(Guid));
            Columns.Add(BULKORDERID, typeof(long));
            Columns.Add(STATUS, typeof(string));
            Columns.Add(ORDERACCOUNTTYPE, typeof(string));
            Columns.Add(ORDERTYPE, typeof(string));
            Columns.Add(ORDERITEMTYPE, typeof(string));
            Columns.Add(CREATEDDATE, typeof(DateTime));
            Columns.Add(UPDATEDDATE, typeof(DateTime));
            Columns.Add(UPDATEDBY, typeof(string));
            Columns.Add(UPDATEDBYCID, typeof(Guid));
            var isExported = new DataColumn(ISEXPORTED, typeof(bool)) { DefaultValue = false };
            Columns.Add(isExported);
            Columns.Add(TRADEDATE, typeof(DateTime));
            Columns.Add(CLIENTMANAGEMENTTYPE, typeof(string));
            Columns.Add(ORDERPREFEREDBY, typeof(string));
            Columns.Add(SMAREQUESTID, typeof(string));
            Columns.Add(SMACALLSTATUS, typeof(string));
        }
    }

    public class BulkOrderItemList : DataTable
    {
        private readonly string TABLENAME = "BulkOrderItemsList";

        public readonly string ID = "ID";
        public readonly string BULKORDERID = "BulkOrderID";
        public readonly string ORDERITEMIDS = "OrderItemIDs";
        public readonly string CREATEDDATE = "CreatedDate";
        public readonly string UPDATEDDATE = "UpdatedDate";
        public readonly string CONTRACTNOTE = "ContractNote";
        public readonly string ISAPPORTIONED = "IsApportioned";

        //ASX Bulk Items
        public readonly string CODE = "Code";
        public readonly string EXPECTEDUNITPRICE = "ExpectedUnitPrice";
        public readonly string EXPECTEDUNITS = "ExpectedUnits";
        public readonly string EXPECTEDAMOUNT = "ExpectedAmount";
        public readonly string ACTUALUNITPRICE = "ActualUnitPrice";
        public readonly string ACTUALUNITS = "ActualUnits";
        public readonly string ACTUALAMOUNT = "ActualAmount";
        public readonly string BROKERAGE = "Brokerage";
        public readonly string CHARGES = "Charges";
        public readonly string TAX = "Tax";
        public readonly string GROSSVALUE = "GrossValue";

        //TD Bulk Items
        public readonly string BROKERID = "BrokerId";
        public readonly string BROKERNAME = "BrokerName";
        public readonly string INSTITUTEID = "InstituteID";
        public readonly string INSTITUTENAME = "InstituteName";
        public readonly string TERM = "Term";
        public readonly string RATE = "Rate";
        public readonly string AMOUNT = "Amount";

        internal BulkOrderItemList()
        {
            TableName = TABLENAME;
            Columns.Add(ID, typeof(Guid));
            Columns.Add(BULKORDERID, typeof(Guid));
            Columns.Add(ORDERITEMIDS, typeof(string));
            Columns.Add(CODE, typeof(string));
            Columns.Add(EXPECTEDUNITPRICE, typeof(decimal));
            Columns.Add(EXPECTEDUNITS, typeof(decimal));
            Columns.Add(EXPECTEDAMOUNT, typeof(decimal));
            Columns.Add(ACTUALUNITPRICE, typeof(decimal));
            Columns.Add(ACTUALUNITS, typeof(decimal));
            Columns.Add(ACTUALAMOUNT, typeof(decimal));
            Columns.Add(CREATEDDATE, typeof(DateTime));
            Columns.Add(UPDATEDDATE, typeof(DateTime));
            Columns.Add(CONTRACTNOTE, typeof(string));
            var isApportioned = new DataColumn(ISAPPORTIONED, typeof(bool)) { DefaultValue = false };
            Columns.Add(isApportioned);
            Columns.Add(BROKERAGE, typeof(decimal));
            Columns.Add(CHARGES, typeof(decimal));
            Columns.Add(TAX, typeof(decimal));
            Columns.Add(GROSSVALUE, typeof(decimal));
            Columns.Add(BROKERID, typeof(Guid));
            Columns.Add(BROKERNAME, typeof(string));
            Columns.Add(INSTITUTEID, typeof(Guid));
            Columns.Add(INSTITUTENAME, typeof(string));
            Columns.Add(TERM, typeof(string));
            Columns.Add(RATE, typeof(decimal));
            Columns.Add(AMOUNT, typeof(decimal));
        }
    }

    public class BulkOrderItemDetailList : DataTable
    {
        private readonly string TABLENAME = "BulkOrderItemDetailsList";

        public readonly string BULKORDERID = "BulkOrderID";
        public readonly string BULKORDERITEMID = "BulkOrderItemID";
        public readonly string ORDERID = "OrderID";
        public readonly string ORDERITEMID = "OrderItemID";
        public readonly string ORDERNO = "OrderNo";
        public readonly string ORDERSTATUS = "OrderStatus";
        public readonly string CLIENTID = "ClientID";
        public readonly string CLIENTCID = "ClientCID";
        public readonly string ORDERITEMTYPE = "OrderItemType";
        public readonly string AMOUNT = "Amount";
        
        //ASX Bulk Items
        public readonly string CODE = "Code";
        public readonly string CURRENCY = "Currency";
        public readonly string UNITPRICE = "UnitPrice";
        public readonly string UNITS = "Units";
        public readonly string ACTUALUNITPRICE = "ActualUnitPrice";
        public readonly string ACTUALUNITS = "ActualUnits";
        public readonly string ACTUALAMOUNT = "ActualAmount";
        
        //TD Bulk Items
        public readonly string BROKERID = "BrokerId";
        public readonly string BROKERNAME = "BrokerName";
        public readonly string INSTITUTEID = "InstituteID";
        public readonly string INSTITUTENAME = "InstituteName";
        public readonly string TERM = "Term";
        public readonly string RATE = "Rate";
        

        internal BulkOrderItemDetailList()
        {
            TableName = TABLENAME;
            Columns.Add(BULKORDERID, typeof(Guid));
            Columns.Add(BULKORDERITEMID, typeof(Guid));
            Columns.Add(ORDERID, typeof(Guid));
            Columns.Add(ORDERITEMID, typeof(Guid));
            Columns.Add(ORDERNO, typeof(long));
            Columns.Add(ORDERSTATUS, typeof(string));
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(CLIENTCID, typeof(Guid));
            Columns.Add(ORDERITEMTYPE, typeof(string));
            Columns.Add(CODE, typeof(string));
            Columns.Add(CURRENCY, typeof(string));
            Columns.Add(UNITPRICE, typeof(decimal));
            Columns.Add(UNITS, typeof(decimal));
            Columns.Add(AMOUNT, typeof(decimal));
            Columns.Add(BROKERID, typeof(Guid));
            Columns.Add(BROKERNAME, typeof(string));
            Columns.Add(INSTITUTEID, typeof(Guid));
            Columns.Add(INSTITUTENAME, typeof(string));
            Columns.Add(TERM, typeof(string));
            Columns.Add(RATE, typeof(decimal));
            Columns.Add(ACTUALUNITPRICE, typeof(decimal));
            Columns.Add(ACTUALUNITS, typeof(decimal));
            Columns.Add(ACTUALAMOUNT, typeof(decimal));
        }
    }
}
