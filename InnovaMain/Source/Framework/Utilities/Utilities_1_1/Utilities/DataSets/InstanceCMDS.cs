﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public enum UnitPropertyType
    {
        ShareHolders,
        Partnerships,
        Corporates
    }

    public class InstanceCMDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public InstanceCMDetails InstanceCMDetailsTable = new InstanceCMDetails();

        public InstanceCMDS()
        {
            Tables.Add(InstanceCMDetailsTable);
        }


        public OrganizationUnit Unit { get; set; }

        public int Command { get; set; }
        public UnitPropertyType UnitPropertyType { get; set; }

    }

    public class InstanceCMDetails : DataTable
    {
        public readonly string TABLENAME = "InstanceCMDetails";
        public readonly string CID = "Cid";
        public readonly string CLID = "CLid";
        public readonly string CSID = "CSid";
        public readonly string NAME = "Name";
        public readonly string TYPE = "Type";
        public readonly string STATUS = "Status";
       
        internal InstanceCMDetails()
        {
            TableName = TABLENAME;
            Columns.Add(CID, typeof(Guid));
            Columns.Add(CLID, typeof(Guid));
            Columns.Add(CSID, typeof(Guid));
            Columns.Add(NAME, typeof(string));
            Columns.Add(TYPE, typeof(string));
            Columns.Add(STATUS, typeof(string));
        }
    }
}
