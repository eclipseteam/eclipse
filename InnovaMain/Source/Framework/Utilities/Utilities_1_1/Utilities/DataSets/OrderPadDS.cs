﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    [Serializable]
    public class OrderPadDS : DataSet, IHasOrganizationUnit
    {
        public Guid OrderId { get; set; }
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public ClientManagementType ClientManagementType = ClientManagementType.UMA;

        public OrderList OrdersTable = new OrderList();
        public StateStreetList StateStreetTable = new StateStreetList();
        public TermDepositList TermDepositTable = new TermDepositList();
        public ASXList ASXTable = new ASXList();
        public AtCallList AtCallTable = new AtCallList();

        public OrderPadDS()
        {
            Tables.Add(OrdersTable);
            Tables.Add(StateStreetTable);
            Tables.Add(TermDepositTable);
            Tables.Add(ASXTable);
            Tables.Add(AtCallTable);
        }

        public OrganizationUnit Unit { get; set; }
        public int Command { get; set; }

    }
    [Serializable]
    public class OrderList : DataTable
    {
        public readonly string TABLENAME = "OrdersList";

        public readonly string CID = "Cid";
        public readonly string CLID = "CLid";
        public readonly string CSID = "CSid";
        public readonly string ID = "ID";
        public readonly string ORDERID = "OrderID";
        public readonly string STATUS = "Status";
        public readonly string ORDERTYPE = "OrderType";
        public readonly string ORDERACCOUNTTYPE = "OrderAccountType";
        public readonly string FILENAME = "FileName";
        public readonly string ATTACHMENT = "Attachment";
        public readonly string CLIENTCID = "ClientCID";
        public readonly string CLIENTID = "ClientID";
        public readonly string CREATEDDATE = "CreatedDate";
        public readonly string UPDATEDDATE = "UpdatedDate";
        public readonly string APPROVEDDATE = "ApprovedDate";
        public readonly string BATCHID = "BatchID";
        public readonly string ACCOUNTCID = "AccountCID";
        public readonly string ACCOUNTNUMBER = "AccountNumber";
        public readonly string ACCOUNTBSB = "AccountBSB";
        public readonly string ORDERITEMTYPE = "OrderItemType";
        public readonly string VALUE = "Value";
        public readonly string VALIDATIONMSG = "ValidationMsg";
        public readonly string COMMENTS = "Comments";
        public readonly string TRADEDATE = "TradeDate";
        public readonly string CREATEDBY = "CreatedBy";
        public readonly string CREATEDBYCID = "CreatedByCID";
        public readonly string UPDATEDBY = "UpdatedBy";
        public readonly string UPDATEDBYCID = "UpdatedByCID";
        public readonly string ORDERBULKSTATUS = "OrderBulkStatus";
        public readonly string CLIENTMANAGEMENTTYPE = "ClientManagementType";
        public readonly string ORDERPREFEREDBY = "OrderPreferedBy";
        public readonly string ORDERBANKACCOUNTTYPE = "OrderBankAccountType";
        public readonly string SMAREQUESTID = "SMARequestID";
        public readonly string SMACALLSTATUS = "SMACallStatus";

        internal OrderList()
        {
            TableName = TABLENAME;
            Columns.Add(CID, typeof(Guid));
            Columns.Add(CLID, typeof(Guid));
            Columns.Add(CSID, typeof(Guid));
            Columns.Add(ID, typeof(Guid));
            Columns.Add(ORDERID, typeof(long));
            Columns.Add(STATUS, typeof(string));
            Columns.Add(ORDERTYPE, typeof(string));
            Columns.Add(ORDERACCOUNTTYPE, typeof(string));
            Columns.Add(FILENAME, typeof(string));
            Columns.Add(ATTACHMENT, typeof(string));
            Columns.Add(CLIENTCID, typeof(Guid));
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(CREATEDDATE, typeof(DateTime));
            Columns.Add(TRADEDATE, typeof(DateTime));
            Columns.Add(UPDATEDDATE, typeof(DateTime));
            Columns.Add(APPROVEDDATE, typeof(DateTime));
            Columns.Add(BATCHID, typeof(long));
            Columns.Add(ORDERITEMTYPE, typeof(string));
            Columns.Add(ACCOUNTCID, typeof(Guid));
            Columns.Add(ACCOUNTNUMBER, typeof(string));
            Columns.Add(ACCOUNTBSB, typeof(string));
            Columns.Add(VALUE, typeof(decimal));
            Columns.Add(VALIDATIONMSG, typeof(string));
            Columns.Add(COMMENTS, typeof(string));
            Columns.Add(CREATEDBY, typeof(string));
            Columns.Add(CREATEDBYCID, typeof(Guid));
            Columns.Add(UPDATEDBY, typeof(string));
            Columns.Add(UPDATEDBYCID, typeof(Guid));
            Columns.Add(ORDERBULKSTATUS, typeof(string));
            Columns.Add(CLIENTMANAGEMENTTYPE, typeof(string));
            Columns.Add(ORDERPREFEREDBY, typeof(string));
            Columns.Add(ORDERBANKACCOUNTTYPE, typeof(string));
            Columns.Add(SMAREQUESTID, typeof(string));
            Columns.Add(SMACALLSTATUS, typeof(string));
        }
    }
    [Serializable]
    public abstract class OrderItemList : DataTable
    {
        public readonly string ID = "ID";
        public readonly string ORDERID = "OrderID";
        public readonly string UNITS = "Units";
        public readonly string AMOUNT = "Amount";
        public readonly string CREATEDDATE = "CreatedDate";
        public readonly string UPDATEDDATE = "UpdatedDate";
        public readonly string VALIDATIONMSG = "ValidationMsg";
        public readonly string SMATRANSACTIONID = "SMATransactionID";
        internal OrderItemList()
        {
            Columns.Add(ID, typeof(Guid));
            Columns.Add(ORDERID, typeof(Guid));
            Columns.Add(UNITS, typeof(decimal));
            Columns.Add(AMOUNT, typeof(decimal));
            Columns.Add(CREATEDDATE, typeof(DateTime));
            Columns.Add(UPDATEDDATE, typeof(DateTime));
            Columns.Add(VALIDATIONMSG, typeof(string));
            Columns.Add(SMATRANSACTIONID, typeof(string));
        }
    }

    [Serializable]
    public class StateStreetList : OrderItemList
    {
        public readonly string TABLENAME = "StateStreetList";
        public readonly string PRODUCTID = "ProductID";
        public readonly string FUNDCODE = "FundCode";
        public readonly string FUNDNAME = "FundName";

        internal StateStreetList()
        {
            TableName = TABLENAME;
            Columns.Add(PRODUCTID, typeof(Guid));
            Columns.Add(FUNDCODE, typeof(string));
            Columns.Add(FUNDNAME, typeof(string));
        }
    }
    [Serializable]
    public class TermDepositList : OrderItemList
    {
        public readonly string TABLENAME = "TermDepositList";
        public readonly string BROKERID = "BrokerID";
        public readonly string BROKERNAME = "BrokerName";
        public readonly string BANKID = "BankID";
        public readonly string BANKNAME = "BankName";
        public readonly string MIN = "Min";
        public readonly string MAX = "Max";
        public readonly string RATING = "Rating";
        public readonly string DURATION = "Duration";
        public readonly string PERCENTAGE = "Percentage";
        public readonly string PRODUCTID = "ProductID";
        public readonly string TDACCOUNTCID = "TDAccountCID";
        public readonly string CONTRACTNOTE = "ContractNote";

        internal TermDepositList()
        {
            TableName = TABLENAME;
            Columns.Add(BROKERID, typeof(Guid));
            Columns.Add(BROKERNAME, typeof(string));
            Columns.Add(BANKID, typeof(Guid));
            Columns.Add(BANKNAME, typeof(string));
            Columns.Add(MIN, typeof(decimal));
            Columns.Add(MAX, typeof(decimal));
            Columns.Add(RATING, typeof(string));
            Columns.Add(DURATION, typeof(string));
            Columns.Add(PERCENTAGE, typeof(decimal));
            Columns.Add(PRODUCTID, typeof(Guid));
            Columns.Add(TDACCOUNTCID, typeof(Guid));
            Columns.Add(CONTRACTNOTE, typeof(string));
        }
    }
    [Serializable]
    public class ASXList : OrderItemList
    {
        public readonly string TABLENAME = "ASXList";
        public readonly string INVESTMENTCODE = "InvestmentCode";
        public readonly string INVESTMENTNAME = "InvestmentName";
        public readonly string PRODUCTID = "ProductID";
        public readonly string ASXACCOUNTNO = "ASXAccountNo";
        public readonly string PARENTACC = "ParentAcc";
        public readonly string ASXACCOUNTCID = "ASXAccountCID";
        public readonly string ASXUNITPRICE = "ASXUnitPrice";
        public readonly string SUGGESTEDUNITPRICE = "SuggestedUnitPrice";
        public readonly string SUGGESTEDUNITS = "SuggestedUnits";
        public readonly string SUGGESTEDAMOUNT = "SuggestedAmount";

        internal ASXList()
        {
            TableName = TABLENAME;
            Columns.Add(INVESTMENTCODE, typeof(string));
            Columns.Add(INVESTMENTNAME, typeof(string));
            Columns.Add(PRODUCTID, typeof(Guid));
            Columns.Add(ASXACCOUNTNO, typeof(string));
            Columns.Add(ASXACCOUNTCID, typeof(Guid));
            Columns.Add(ASXUNITPRICE, typeof(decimal));
            Columns.Add(SUGGESTEDUNITPRICE, typeof(decimal));
            Columns.Add(SUGGESTEDUNITS, typeof(decimal));
            Columns.Add(SUGGESTEDAMOUNT, typeof(decimal));
        }
    }
    [Serializable]
    public class OrderInstructionList : DataTable
    {

        public readonly string TABLENAME = "OrderInstructionList";
        public readonly string ID = "ID";
        public readonly string ORDERDETAILID = "OrderDetailID";
        public readonly string ACCOUNTCID = "AccountCID";
        public readonly string INSTRUCTIONPRODUCTID = "InstructionProductID";
        public readonly string ISSETTLED = "IsSettled";
        public readonly string INSTRUCTIONSTATUS = "InstructionStatus";
        public readonly string CREATEDDATE = "CreatedDate";
        public readonly string UPDATEDDATE = "UpdatedDate";

        internal OrderInstructionList()
        {
            TableName = TABLENAME;
            Columns.Add(ID, typeof(Guid));
            Columns.Add(ORDERDETAILID, typeof(Guid));
            Columns.Add(ACCOUNTCID, typeof(Guid));
            Columns.Add(INSTRUCTIONPRODUCTID, typeof(Guid));
            Columns.Add(ISSETTLED, typeof(bool));
            Columns.Add(INSTRUCTIONSTATUS, typeof(string));
            Columns.Add(CREATEDDATE, typeof(DateTime));
            Columns.Add(UPDATEDDATE, typeof(DateTime));
        }
    }
    [Serializable]
    public class AtCallList : OrderItemList
    {
        public readonly string TABLENAME = "AtCallList";
        public readonly string TRANSFERTO = "TransferTo";
        public readonly string TRANSFERACCCID = "TransferAccCID";
        public readonly string TRANSFERACCNUMBER = "TransferAccNumber";
        public readonly string TRANSFERACCBSB = "TransferAccBSB";
        public readonly string TRANSFERNARRATION = "TransferNarration";
        public readonly string FROMNARRATION = "FromNarration";

        public readonly string BROKERID = "BrokerID";
        public readonly string BROKERNAME = "BrokerName";
        public readonly string BANKID = "BankID";
        public readonly string BANKNAME = "BankName";
        public readonly string ACCOUNTTIER = "AccountTier";
        public readonly string MIN = "Min";
        public readonly string MAX = "Max";
        public readonly string RATE = "Rate";
        public readonly string PRODUCTID = "ProductID";
        public readonly string ATCALLACCOUNTCID = "AtCallAccountCID";

        public readonly string ATCALLTYPE = "AtCallType";
        public string ISEXTERNALACCOUNT = "IsExternalAccount";

        internal AtCallList()
        {
            TableName = TABLENAME;
            Columns.Add(TRANSFERTO, typeof(string));
            Columns.Add(TRANSFERACCCID, typeof(Guid));
            Columns.Add(TRANSFERACCNUMBER, typeof(string));
            Columns.Add(TRANSFERACCBSB, typeof(string));
            Columns.Add(TRANSFERNARRATION, typeof(string));
            Columns.Add(FROMNARRATION, typeof(string));

            Columns.Add(BROKERID, typeof(Guid));
            Columns.Add(BROKERNAME, typeof(string));
            Columns.Add(BANKID, typeof(Guid));
            Columns.Add(BANKNAME, typeof(string));
            Columns.Add(ACCOUNTTIER, typeof(string));
            Columns.Add(MIN, typeof(decimal));
            Columns.Add(MAX, typeof(decimal));
            Columns.Add(RATE, typeof(string));
            Columns.Add(PRODUCTID, typeof(Guid));
            Columns.Add(ATCALLACCOUNTCID, typeof(Guid));
            Columns.Add(ISEXTERNALACCOUNT, typeof(string));
            Columns.Add(ATCALLTYPE, typeof(string));
        }
    }
}
