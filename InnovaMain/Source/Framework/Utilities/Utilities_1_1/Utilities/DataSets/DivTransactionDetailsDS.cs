﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;

namespace Oritax.TaxSimp.DataSets
{
    public class DivTransactionDetailsDS : UMABaseDS
    {
        public const string DIVTRANSACTIONDETAILSTABLE = "DivTransactionDetailsTable";
        public const string ID = "ID"; 

        public DivTransactionDetailsDS()
        {
            DataTable dt = new DataTable(DIVTRANSACTIONDETAILSTABLE);
            dt.Columns.Add(ID, typeof(Guid));
            this.Tables.Add(dt); 
        }
    }
}
