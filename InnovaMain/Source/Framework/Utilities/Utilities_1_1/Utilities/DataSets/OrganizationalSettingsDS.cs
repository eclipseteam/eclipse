﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class OrganizationalSettingsDS:DataSet 
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public OrganizationalSettingTable OrganizationalSettingTable = new OrganizationalSettingTable();

        public OrganizationalSettingsDS()
        {
            Tables.Add(OrganizationalSettingTable);
        }
    }

    public class OrganizationalSettingTable : DataTable
    {
        public readonly string TABLENAME = "OrganizationalSettings";

        public readonly string SENDORDERSTOSMA = "SendOrdersToSMA";
        public readonly string READFROMSMA = "ReadFromSMA";


        internal OrganizationalSettingTable()
        {
            TableName = TABLENAME;

            Columns.Add(SENDORDERSTOSMA, typeof(bool));
            Columns.Add(READFROMSMA, typeof(bool));
        }
    }
}
