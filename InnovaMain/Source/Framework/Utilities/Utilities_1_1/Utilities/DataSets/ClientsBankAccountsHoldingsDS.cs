﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class ClientsBankAccountsHoldingsDS : DataSet, IHasOrganizationUnit
    {

        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public BankAccountHoldingTable BankAccountHoldingTable = new BankAccountHoldingTable();
        public ClientsBankAccountsHoldingsDS()
        {
            Tables.Add(BankAccountHoldingTable);
        }
        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
    }
    public class BankAccountHoldingTable : DataTable
    {
        private readonly string TABLENAME = "BankAccountHoldingList";
        public readonly string ClientCID = "CID";
        public readonly string CLIENTID = "ClientID";
        public readonly string ENTITYCLIENTID = "EntityClientID";
        public readonly string TRANDINGNAME = "TrandingName";
        public readonly string SERVICETYPES = "ServiceTypes";
        public readonly string ACCOUNTNAME = "AccountName";
        public readonly string ACCOUNTTYPE = "AccountType";
        public readonly string ACCOUNTSERVICETYPE = "AccountServicetype";
        public readonly string ACCOUNTNUMBER = "AccountNumber";
        public readonly string BSB = "BSB";
        public readonly string HOLDING = "Holding";
        public readonly string SYSTEMTRANSACTIONS = "SystemTransactions";
        public readonly string DIFFERENCE = "Difference";
        public readonly string UNSETTLEDORDER = "UnsettledOrder";
        public readonly string TOTAL = "Total";


        internal BankAccountHoldingTable()
        {
            TableName = TABLENAME;
            Columns.Add(ClientCID, typeof(Guid));
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(ENTITYCLIENTID, typeof(string));
            Columns.Add(TRANDINGNAME, typeof(string));
            Columns.Add(SERVICETYPES, typeof(string));
            Columns.Add(ACCOUNTNAME, typeof(string));
            Columns.Add(ACCOUNTTYPE, typeof(string));
            Columns.Add(ACCOUNTSERVICETYPE, typeof(string));
            Columns.Add(ACCOUNTNUMBER, typeof(string));
            Columns.Add(BSB, typeof(string));
            Columns.Add(HOLDING, typeof(decimal));
            Columns.Add(SYSTEMTRANSACTIONS, typeof(decimal));
            Columns.Add(DIFFERENCE, typeof(decimal));
            Columns.Add(UNSETTLEDORDER, typeof(decimal));
            Columns.Add(TOTAL, typeof(decimal));

        }



        public void AddNewRow(Guid clientCID, string clinetID, string entityClientID, string tradingName, string serviceTypes,string bankAccountName,string bankaccountype,string bankAccountServiceType,string accountNumber,string bsb, decimal holding, decimal systemTransHolding, decimal unsetteledOrder)
        {
            DataRow dr = NewRow();
            dr[ClientCID] = clientCID;
            dr[CLIENTID] = clinetID;
            dr[ENTITYCLIENTID] = entityClientID;
            dr[TRANDINGNAME] = tradingName;
            dr[SERVICETYPES] = serviceTypes;
            dr[ACCOUNTNAME] = bankAccountName;
            dr[ACCOUNTTYPE] = bankaccountype;
            dr[ACCOUNTSERVICETYPE] = bankAccountServiceType;
            dr[ACCOUNTNUMBER] = accountNumber;
            dr[BSB] = bsb;
            dr[HOLDING] = holding;
            dr[SYSTEMTRANSACTIONS] = systemTransHolding;
            dr[DIFFERENCE] = (holding - systemTransHolding);
            dr[UNSETTLEDORDER] = unsetteledOrder;
            dr[TOTAL] = (holding + unsetteledOrder);

            Rows.Add(dr);

        }
    }
}
