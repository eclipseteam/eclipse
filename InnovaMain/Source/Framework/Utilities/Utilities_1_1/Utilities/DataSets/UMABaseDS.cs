﻿using System.Data;

namespace Oritax.TaxSimp.DataSets
{
    public enum DataSetOperationType
    {
        None = 0,   
        NewSingle = 1,
        NewBulk = 2,
        UpdateSingle = 3,
        UpdateBulk = 4,
        NewSingleTD = 5,
        UpdateSingleTD = 6,
        DeletSingleTD = 7,
        DeletSingle = 8,
        DeletBulk = 9,
        PushDeletBulk = 10,
        PushUpdate = 11,
        DeletBulkTD = 12,
    }


    public class UMABaseDS : DataSet 
    {
        public const string BGLCODE = "BGLCODE";
        public const string BGLDEFAULTCODE = "BGLDEFAULTCODE";
        public const string INVESTMENTCODE = "InvestmentCode";
        public const string INVESTMENTCODEDESC = "InvestmentCodeDesc";
        public const string INVESTMENTCID = "InvestmentCid";
        public const string INVESTMENTNAME = "InvestmentName";
        public const string INVESTMENTTYPE = "InvestmentType";
        public const string INVESTMENTPROVIDER = "InvestmentProvider";
        public const string INVESTMENTCOATYPE = "InvestmentCOAType";
        public const string INVESTMENTACCNO = "InvestmentAccNo";
        public const string INVESTMENTACCOUNTFILTERTYPE = "InvestmentAccFilterType";
        

        public const string INCOMETRANMAPTABLE = "Income Transactions Map";

        public DataTable GetIncomeTransactionMapTable()
        {
            DataTable dt = new DataTable(INCOMETRANMAPTABLE);
            dt.Columns.Add(INVESTMENTCID);
            dt.Columns.Add(INVESTMENTCODE);
            dt.Columns.Add(INVESTMENTNAME);
            dt.Columns.Add(INVESTMENTTYPE);
            dt.Columns.Add(INVESTMENTPROVIDER);
            dt.Columns.Add(INVESTMENTCOATYPE);
            dt.Columns.Add(INVESTMENTCODEDESC);
            dt.Columns.Add(BGLDEFAULTCODE);
            dt.Columns.Add(INVESTMENTACCNO);
            dt.Columns.Add(INVESTMENTACCOUNTFILTERTYPE);
            dt.Columns.Add(BGLCODE, typeof(string));
            return dt;
        }

        public DataSetOperationType DataSetOperationType; 
    }
}
