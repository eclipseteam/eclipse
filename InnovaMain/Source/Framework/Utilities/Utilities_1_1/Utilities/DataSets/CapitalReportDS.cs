﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.DataSets
{
    public partial class CapitalReportDS : BaseClientDetails
    {
        public bool PrintAddress = true;
        public static string CAPITALFLOWSUMMARY = "CapitalFlowSummary";
        public static string ASSETNAME = "AssetName";
        public static string PRODUCTNAME = "ProductName";
        public static string DESCRIPTION = "Description";
        public static string ACCOUNTNO = "AccountNo";
        public static string HOLDINGENDDATE = "HoldingEndDate";
        public static string HOLDINGSTARTDATE = "HoldingStartDate";
        public static string HOLDINGLATESTPRICE = "HoldingLatestPrice";
        public static string DIFFERENCE = "Difference";
        public static string SUMMDIFFERENCE = "SummaryDifference";
        public static string SERVICETYPE = "ServiceType";
        public static string TDID = "TDID";
        public static string ID = "ID";

        public static string PRODUCTBREAKDOWNTABLE = "ProductBreakDown";
        public static string PRODUCTID = "ProductID";
        public static string INVESMENTCODE = "InvestmentCode";
        public static string UNITPRICE = "UnitPrice";
        public static string CURRENTVALUE = "CurrentValue";
        public static string STARTVALUE = "StartValue";
        public static string ENDVALUE = "EndValue";
        public static string HOLDING = "Holding";

        public static string TDBREAKDOWNTABLE = "TDBreakDown";
        public static string INSNAME = "InsName";
        public static string TRANSTYPE = "TransType";
        public static string BROKER = "BROKER";

        public static string CAPITALFLOWSUMMARYCATBYSEC = "CapitalFlowSummaryCatBySec";
        public static string CAPITALFLOWSUMMARYCAT = "CapitalFlowSummaryCat";
        public static string MONTH = "Month";
        public static string OPENINGBAL = "OpeningBalance";
        public static string TRANSFEROUT = "TransferInOut";
        public static string INCOME = "Income";
        public static string APPLICATIONREDEMPTION = "AppicationRedemption";
        public static string TAXINOUT = "TaxInOut";
        public static string EXPENSE = "Expense";
        public static string INTERNALCASHMOVEMENT = "InternalCashMovement";
        public static string CLOSINGBALANCE = "ClosingBalance";
        public static string CHANGEININVESTMENTVALUE = "ChangeInInvestmentValue";
        public static string MODDIETZ = "ModDietz";
        public static string GROWTHRETURN = "GrowthReturn";
        public static string INCOMERETURN = "IncomeReturn";
        public static string OVERALLRETURN = "OverallReturn";
        public static string SUMCF = "SumCF";
        public static string SUMWEIGHTCF = "SumWeightCF";

        public static string BANKTRANSACTIONSTABLE = "BankTransactionsTable";
        public static string BSB = "BSB";
        public static string ACCOUNTTYPE = "AccountType";
        public static string ACCOUNTNAME = "AccountName";
        public static string IMPTRANTYPE = "ImportTransactionType";
        public static string SYSTRANTYPE = "SystemTransactionType";
        public static string CATEGORY = "Category";
        public static string COMMENT = "Comment";
        public static string AMOUNTTOTAL = "AmountTotal";
        public static string BANKAMOUNT = "BankAmount";
        public static string BANKADJUSTMENT = "BankAdjustment";
        public static string BANKTRANSDATE = "BankTransactionDate";
        public static string CAPITALMOVEMENTCAT = "CapitalMovementCat";

        public static string ASXTRANSTABLE = "ASXTRANSTable";
        public static string NAME = "Name";
        public static string TRANSACTIONTYPE = "TransactionType";
        public static string FPSID = "FPSInstructionID";
        public static string TRADEDATE = "TradeDate";
        public static string SETTLEDATE = "SettlementDate";
        public static string UNITS = "Units";
        public static string GROSSVALUE = "GrossValue";
        public static string BROKERAGEAMOUNT = "BrokerageAmount";
        public static string CHARGES = "Charges";
        public static string BROKERGST = "BrokerageGST";
        public static string NETVALUE = "NetValue";
        public static string NARRATIVE = "Narrative";

        public static string TRANBREAKDOWNTABLE = "TranBreakDown";
        public static string MAJORCATEGORY = "MajorCategory";
        public DateTime StartDate;
        public DateTime EndDate;

        public CapitalReportDS()
        {
            IntialiseDates();
            this.Tables.Add(TransactionBreakDown());
            this.Tables.Add(CAPITALFLOWSUMMARYSERVICETable());
            this.Tables.Add(ProductBreakDown());
            this.Tables.Add(TDBreakDownTable());
            this.Tables.Add(CapitalFlowSummaryCategoryTable());
            this.Tables.Add(BankTransTable());
            this.Tables.Add(ASXTRANTable());
            this.Tables.Add(SummaryCategoryTableBySecurities());
        }

        public virtual void IntialiseDates()
        {
            this.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(DateTime.Now.AddMonths(-1));
            this.EndDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(DateTime.Now.AddMonths(-1));
        }

        public void SetOneMonthDate()
        {
            this.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(DateTime.Now.AddMonths(-1));
            this.EndDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(DateTime.Now.AddMonths(-1));
        }

        public void SetThreeMonthDate()
        {
            this.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(DateTime.Now.AddMonths(-3));
            this.EndDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(DateTime.Now.AddMonths(-1));
        }

        public void SetFiveFinancialYear()
        {
            this.EndDate = DateTime.Now;
            this.StartDate = DateTime.Now.AddYears(-5);
        }

        public void SetSixMonthDate()
        {
            this.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(DateTime.Now.AddMonths(-6));
            this.EndDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(DateTime.Now.AddMonths(-1));
        }

        public void SetTwelveMonthDate()
        {
            this.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(DateTime.Now.AddMonths(-12));
            this.EndDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(DateTime.Now.AddMonths(-1));
        }

        public void SetLastFinancialYear()
        {
            AccountingFinancialYear accountingFinancialYear = new AccountingFinancialYear(DateTime.Now);

            this.StartDate = accountingFinancialYear.FinYearStartDate;
            this.EndDate = accountingFinancialYear.FinYearEndDate;
        }

        public void SetCurrentFinancialYear()
        {
            AccountingFinancialYear accountingFinancialYear = new AccountingFinancialYear(DateTime.Now.AddYears(1));

            this.StartDate = accountingFinancialYear.FinYearStartDate;
            this.EndDate = DateTime.Now;
        }

        public string DateInfo()
        {
            return this.StartDate.ToString("dd/MM/yyyy") + " - " + this.EndDate.ToString("dd/MM/yyyy");
        }

        public void TDBreakDownRow(string insName, string serviceType, string tdType, string broker, decimal holding, string TDID, DataRow tDBreakDownTableRow)
        {
            tDBreakDownTableRow[CapitalReportDS.TRANSTYPE] = tdType;
            tDBreakDownTableRow[CapitalReportDS.INSNAME] = insName;
            tDBreakDownTableRow[CapitalReportDS.BROKER] = broker;
            tDBreakDownTableRow[CapitalReportDS.HOLDING] = holding;
            tDBreakDownTableRow[CapitalReportDS.SERVICETYPE] = serviceType;
            tDBreakDownTableRow[CapitalReportDS.TDID] = TDID;
        }

        public void AddTranBreakdownRow(Guid id, string transType, string accountNo, string comment, string category, string majorCategory, DateTime tranDate, decimal amount)
        {
            DataRow row = this.Tables[TRANBREAKDOWNTABLE].NewRow();
            row[ID] = id;
            row[TRANSACTIONTYPE] = transType;
            row[ACCOUNTNO] = accountNo;
            row[COMMENT] = comment;
            row[CATEGORY] = category;
            row[MAJORCATEGORY] = majorCategory;
            row[TRADEDATE] = tranDate;
            row[AMOUNTTOTAL] = amount;
            this.Tables[TRANBREAKDOWNTABLE].Rows.Add(row);
        }

        public DataTable TransactionBreakDown()
        {
            DataTable dt = new DataTable(TRANBREAKDOWNTABLE);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(TRANSACTIONTYPE, typeof(string));
            dt.Columns.Add(ACCOUNTNO, typeof(string));
            dt.Columns.Add(COMMENT, typeof(string));
            dt.Columns.Add(CATEGORY, typeof(string));
            dt.Columns.Add(MAJORCATEGORY, typeof(string));
            dt.Columns.Add(TRADEDATE, typeof(DateTime));
            dt.Columns.Add(AMOUNTTOTAL, typeof(decimal));
            return dt;
        }

        public DataTable CAPITALFLOWSUMMARYSERVICETable()
        {
            DataTable dt = new DataTable(CAPITALFLOWSUMMARY);
            dt.Columns.Add(TDID, typeof(string));
            dt.Columns.Add(PRODUCTID, typeof(string));
            dt.Columns.Add(ASSETNAME, typeof(string));
            dt.Columns.Add(PRODUCTNAME, typeof(string));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(ACCOUNTNO, typeof(string));
            dt.Columns.Add(HOLDINGENDDATE, typeof(decimal));
            dt.Columns.Add(OPENINGBAL, typeof(decimal));
            dt.Columns.Add(TRANSFEROUT, typeof(decimal));
            dt.Columns.Add(INCOME, typeof(decimal));
            dt.Columns.Add(APPLICATIONREDEMPTION, typeof(decimal));
            dt.Columns.Add(TAXINOUT, typeof(decimal));
            dt.Columns.Add(EXPENSE, typeof(decimal));
            dt.Columns.Add(INTERNALCASHMOVEMENT, typeof(decimal));
            dt.Columns.Add(CHANGEININVESTMENTVALUE, typeof(decimal));
            dt.Columns.Add(CLOSINGBALANCE, typeof(decimal));
            dt.Columns.Add(SERVICETYPE, typeof(string));

            return dt;
        }

        public DataTable CapitalFlowSummaryCategoryTable()
        {
            DataTable CapitalFlowSummaryReport = new DataTable(CAPITALFLOWSUMMARYCAT);

            CapitalFlowSummaryReport.Columns.Add(MONTH, typeof(DateTime));
            CapitalFlowSummaryReport.Columns.Add(OPENINGBAL, typeof(decimal));
            CapitalFlowSummaryReport.Columns.Add(TRANSFEROUT, typeof(decimal));
            CapitalFlowSummaryReport.Columns.Add(INCOME, typeof(decimal));
            CapitalFlowSummaryReport.Columns.Add(APPLICATIONREDEMPTION, typeof(decimal));
            CapitalFlowSummaryReport.Columns.Add(TAXINOUT, typeof(decimal));
            CapitalFlowSummaryReport.Columns.Add(EXPENSE, typeof(decimal));
            CapitalFlowSummaryReport.Columns.Add(INTERNALCASHMOVEMENT, typeof(decimal));
            CapitalFlowSummaryReport.Columns.Add(CLOSINGBALANCE, typeof(decimal));
            CapitalFlowSummaryReport.Columns.Add(CHANGEININVESTMENTVALUE, typeof(decimal));
            CapitalFlowSummaryReport.Columns.Add(MODDIETZ, typeof(decimal));
            CapitalFlowSummaryReport.Columns.Add(SUMCF, typeof(decimal));
            CapitalFlowSummaryReport.Columns.Add(SUMWEIGHTCF, typeof(decimal));
            CapitalFlowSummaryReport.Columns.Add(GROWTHRETURN, typeof(decimal));
            CapitalFlowSummaryReport.Columns.Add(INCOMERETURN, typeof(decimal));
            CapitalFlowSummaryReport.Columns.Add(OVERALLRETURN, typeof(decimal));

            return CapitalFlowSummaryReport;
        }

        public DataTable SummaryCategoryTableBySecurities()
        {
            DataTable performaneSummaryBySecurities = new DataTable(CAPITALFLOWSUMMARYCATBYSEC);
            performaneSummaryBySecurities.Columns.Add(MONTH, typeof(DateTime));
            performaneSummaryBySecurities.Columns.Add(DESCRIPTION, typeof(string));
            performaneSummaryBySecurities.Columns.Add(OPENINGBAL, typeof(decimal));
            performaneSummaryBySecurities.Columns.Add(TRANSFEROUT, typeof(decimal));
            performaneSummaryBySecurities.Columns.Add(INCOME, typeof(decimal));
            performaneSummaryBySecurities.Columns.Add(APPLICATIONREDEMPTION, typeof(decimal));
            performaneSummaryBySecurities.Columns.Add(TAXINOUT, typeof(decimal));
            performaneSummaryBySecurities.Columns.Add(EXPENSE, typeof(decimal));
            performaneSummaryBySecurities.Columns.Add(INTERNALCASHMOVEMENT, typeof(decimal));
            performaneSummaryBySecurities.Columns.Add(CLOSINGBALANCE, typeof(decimal));
            performaneSummaryBySecurities.Columns.Add(CHANGEININVESTMENTVALUE, typeof(decimal));
            performaneSummaryBySecurities.Columns.Add(MODDIETZ, typeof(decimal));
            performaneSummaryBySecurities.Columns.Add(SUMCF, typeof(decimal));
            performaneSummaryBySecurities.Columns.Add(SUMWEIGHTCF, typeof(decimal));
            performaneSummaryBySecurities.Columns.Add(GROWTHRETURN, typeof(decimal));
            performaneSummaryBySecurities.Columns.Add(INCOMERETURN, typeof(decimal));
            performaneSummaryBySecurities.Columns.Add(OVERALLRETURN, typeof(decimal));

            return performaneSummaryBySecurities;
        }
        public DataTable ProductBreakDown()
        {
            DataTable dt = new DataTable(PRODUCTBREAKDOWNTABLE);
            dt.Columns.Add(PRODUCTID, typeof(string));
            dt.Columns.Add(INVESMENTCODE, typeof(string));
            dt.Columns.Add(UNITPRICE, typeof(string));
            dt.Columns.Add(CURRENTVALUE, typeof(string));
            dt.Columns.Add(STARTVALUE, typeof(string));
            dt.Columns.Add(ENDVALUE, typeof(string));
            dt.Columns.Add(HOLDINGENDDATE, typeof(decimal));
            dt.Columns.Add(HOLDINGSTARTDATE, typeof(decimal));
            dt.Columns.Add(HOLDINGLATESTPRICE, typeof(decimal));
            dt.Columns.Add(DIFFERENCE, typeof(decimal));
            dt.Columns.Add(SUMMDIFFERENCE, typeof(decimal));
            dt.Columns.Add(SERVICETYPE, typeof(string));

            return dt;
        }


        public DataTable BankTransTable()
        {
            DataTable dt = new DataTable(BANKTRANSACTIONSTABLE);

            dt.Columns.Add(CLIENTID, typeof(string));
            dt.Columns.Add(SERVICETYPE, typeof(string));
            dt.Columns.Add(BSB);
            dt.Columns.Add(MONTH, typeof(string));
            dt.Columns.Add(CAPITALMOVEMENTCAT, typeof(string));
            dt.Columns.Add(ACCOUNTNO);
            dt.Columns.Add(ACCOUNTNAME);
            dt.Columns.Add(ACCOUNTTYPE);
            dt.Columns.Add(IMPTRANTYPE);
            dt.Columns.Add(SYSTRANTYPE);
            dt.Columns.Add(CATEGORY);
            dt.Columns.Add(COMMENT);
            dt.Columns.Add(AMOUNTTOTAL, typeof(decimal));
            dt.Columns.Add(BANKAMOUNT, typeof(decimal));
            dt.Columns.Add(BANKADJUSTMENT, typeof(decimal));
            dt.Columns.Add(BANKTRANSDATE, typeof(DateTime));

            return dt;
        }


        public DataTable ASXTRANTable()
        {
            DataTable dt = new DataTable(ASXTRANSTABLE);
            dt.Columns.Add(CLIENTID, typeof(string));
            dt.Columns.Add(SERVICETYPE, typeof(string));
            dt.Columns.Add(INVESMENTCODE, typeof(string));
            dt.Columns.Add(MONTH, typeof(string));
            dt.Columns.Add(CAPITALMOVEMENTCAT, typeof(string));
            dt.Columns.Add(NAME, typeof(string));
            dt.Columns.Add(TRANSACTIONTYPE, typeof(string));
            dt.Columns.Add(ACCOUNTNO, typeof(string));
            dt.Columns.Add(FPSID, typeof(string));
            dt.Columns.Add(TRADEDATE, typeof(DateTime));
            dt.Columns.Add(SETTLEDATE, typeof(DateTime));
            dt.Columns.Add(UNITS, typeof(decimal));
            dt.Columns.Add(GROSSVALUE, typeof(decimal));
            dt.Columns.Add(BROKERAGEAMOUNT, typeof(decimal));
            dt.Columns.Add(CHARGES, typeof(decimal));
            dt.Columns.Add(BROKERGST, typeof(decimal));
            dt.Columns.Add(NETVALUE, typeof(decimal));
            dt.Columns.Add(NARRATIVE, typeof(string));

            return dt;
        }



        public DataTable TDBreakDownTable()
        {
            DataTable dt = new DataTable(TDBREAKDOWNTABLE);
            dt.Columns.Add(INSNAME, typeof(string));
            dt.Columns.Add(TRANSTYPE, typeof(string));
            dt.Columns.Add(BROKER, typeof(string));
            dt.Columns.Add(HOLDING, typeof(decimal));
            dt.Columns.Add(SERVICETYPE, typeof(string));
            dt.Columns.Add(TDID, typeof(string));
            return dt;
        }

        public static double CalculateTotalPercentageForSplit(IEnumerable<AssetEntity> linkedAssets)
        {
            double totalPercentage = 0;

            foreach (var linkedasst in linkedAssets)
                totalPercentage += linkedasst.SharePercentage;

            return totalPercentage;
        }

        public void AddProductDetailRow(ProductEntity product, decimal unitPrice, string investmentCode, decimal holdingPresent, decimal holdingEndDate, decimal holdingStartDate, decimal currentValue, decimal startValue, decimal endValue, string modelServiceType)
        {
            DataRow productBreakDownRow = this.Tables[PRODUCTBREAKDOWNTABLE].NewRow();

            productBreakDownRow[CapitalReportDS.PRODUCTID] = product.ID;
            productBreakDownRow[CapitalReportDS.INVESMENTCODE] = investmentCode;
            productBreakDownRow[CapitalReportDS.UNITPRICE] = unitPrice;
            productBreakDownRow[CapitalReportDS.CURRENTVALUE] = currentValue;
            productBreakDownRow[CapitalReportDS.STARTVALUE] = startValue;
            productBreakDownRow[CapitalReportDS.ENDVALUE] = endValue;
            productBreakDownRow[CapitalReportDS.HOLDINGLATESTPRICE] = holdingStartDate;
            productBreakDownRow[CapitalReportDS.HOLDINGENDDATE] = holdingEndDate;
            productBreakDownRow[CapitalReportDS.HOLDINGLATESTPRICE] = holdingPresent;
            productBreakDownRow[CapitalReportDS.DIFFERENCE] = holdingPresent - holdingEndDate;
            productBreakDownRow[CapitalReportDS.SUMMDIFFERENCE] = holdingPresent - holdingStartDate;
            productBreakDownRow[CapitalReportDS.SERVICETYPE] = this.GetServiceTypeDescription(modelServiceType);

            this.Tables[PRODUCTBREAKDOWNTABLE].Rows.Add(productBreakDownRow);
        }

        public void AddCapitalRow(string assetName, string modelServiceType, string productName, string productTaskDesc, string accountNo, decimal openingBalTotal, decimal closingBalanceTotal, decimal transferInOutTotal, decimal incomeTotal, decimal applicationRedTotal, decimal taxInOutTotal, decimal expenseTotal, decimal internalCashMovementTotal, decimal changeInInvestTotal, string productID, string TDID)
        {
            DataRow holdingSumaryRow = this.Tables[CAPITALFLOWSUMMARY].NewRow();

            holdingSumaryRow[CapitalReportDS.ASSETNAME] = assetName;
            holdingSumaryRow[CapitalReportDS.SERVICETYPE] = this.GetServiceTypeDescription(modelServiceType);
            holdingSumaryRow[CapitalReportDS.PRODUCTNAME] = productName;
            holdingSumaryRow[CapitalReportDS.DESCRIPTION] = productTaskDesc;
            holdingSumaryRow[CapitalReportDS.ACCOUNTNO] = accountNo;
            holdingSumaryRow[CapitalReportDS.OPENINGBAL] = openingBalTotal;
            holdingSumaryRow[CapitalReportDS.TRANSFEROUT] = transferInOutTotal;
            holdingSumaryRow[CapitalReportDS.INCOME] = incomeTotal;
            holdingSumaryRow[CapitalReportDS.APPLICATIONREDEMPTION] = applicationRedTotal;
            holdingSumaryRow[CapitalReportDS.TAXINOUT] = taxInOutTotal;
            holdingSumaryRow[CapitalReportDS.EXPENSE] = expenseTotal;
            holdingSumaryRow[CapitalReportDS.INTERNALCASHMOVEMENT] = internalCashMovementTotal;
            holdingSumaryRow[CapitalReportDS.CLOSINGBALANCE] = closingBalanceTotal;
            if (assetName == "Cash" || assetName == "TDs")
                holdingSumaryRow[CapitalReportDS.CHANGEININVESTMENTVALUE] = 0;
            else
                holdingSumaryRow[CapitalReportDS.CHANGEININVESTMENTVALUE] = closingBalanceTotal - internalCashMovementTotal - expenseTotal - taxInOutTotal - applicationRedTotal - transferInOutTotal - openingBalTotal;
            holdingSumaryRow[CapitalReportDS.PRODUCTID] = productID;
            holdingSumaryRow[CapitalReportDS.TDID] = TDID;

            this.Tables[CAPITALFLOWSUMMARY].Rows.Add(holdingSumaryRow);
        }

    }

}
