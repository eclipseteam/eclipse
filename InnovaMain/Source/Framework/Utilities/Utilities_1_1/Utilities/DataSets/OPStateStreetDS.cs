﻿using System;
using System.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class OPStateStreetDS : DataSet
    {
        public FundsTable FundsTable = new FundsTable();

        public OPStateStreetDS()
        {
            Tables.Add(FundsTable);
        }
    }

    public class FundsTable : DataTable
    {
        public readonly string TABLENAME = "FundsList";
        public readonly string FUNDID = "FundID";
        public readonly string FUNDCODE = "FundCode";
        public readonly string FUNDDESCRIPTION = "FundDescription";
        public readonly string CLID = "CLid";
        public readonly string CSID = "CSid";
        public readonly string CID = "CID";
        public readonly string AMOUNTTOINVEST = "AmountToInvest";

        internal FundsTable()
        {
            TableName = TABLENAME;
            Columns.Add(CLID, typeof(Guid));
            Columns.Add(CSID, typeof(Guid));
            Columns.Add(CID, typeof(Guid));
            Columns.Add(FUNDID, typeof(Guid));
            Columns.Add(FUNDCODE, typeof(string));
            Columns.Add(FUNDDESCRIPTION, typeof(string));
            Columns.Add(AMOUNTTOINVEST, typeof(decimal));
        }
    }
}
