﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class ManagedInvestmentAccountDS : DataSet, IHasOrganizationUnit
    {
      
        public Guid FundID = Guid.Empty;
        public readonly string TABLENAME = "ManagedInvestmentAccount";

        public FundAccountTable FundAccountsTable = new FundAccountTable();
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        
        public ManagedInvestmentAccountDS()
        {
            this.Tables.Add(FundAccountsTable);
        }

        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
    }
    public class FundAccountTable: DataTable
    {   
        public readonly string TABLENAME = "FundAccountList";
        public readonly string FUNDID = "FundID";
        public readonly string FUNDCODE = "FundCode";
        public readonly string FUNDDESCRIPTION = "FundDescription";
        public readonly string CLID = "CLid";
        public readonly string CSID = "CSid";
        public readonly string CID = "CID";
        public readonly string REINVESTMENTOPTION = "ReinvestmentOption";
        internal FundAccountTable()
        {
            this.TableName = TABLENAME;
            this.Columns.Add(CLID, typeof(Guid));
            this.Columns.Add(CSID, typeof(Guid));
            this.Columns.Add(CID, typeof(Guid));
            this.Columns.Add(FUNDID, typeof(Guid));
            this.Columns.Add(FUNDCODE, typeof(string));
            this.Columns.Add(FUNDDESCRIPTION, typeof(string));
            this.Columns.Add(REINVESTMENTOPTION, typeof(string));
        }
    }
}
