﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public partial class SecuritySummaryReportDS : BaseClientDetails
    {
        private CostTypes costTypes = CostTypes.FIFO;
        public ServiceTypes ServiceTypes = ServiceTypes.All;
        public CostTypes CostTypes
        {
            get { return costTypes; }
            set { costTypes = value; }
        } 

        public static string SECURITYSUMMARYTABLE = "SecuritySummary";
        public static string SECURITYSUMMARYTABLEREALISED = "SecuritySummaryRealised";
        public static string SECURITYSUMMARYTABLEUNREALISED = "SecuritySummaryUNRealised";
        public static string SECNAME = "SecurityName";
        public static string DESCRIPTION = "Description";
        public static string TOTALUNITS = "TotalUnits";
        public static string UNITPRICE = "UnitPrice";
        public static string PRICEATDATE = "PriceAtDate";
        public static string TOTAL = "Total";
        public static string CLIENTCID = "ClientCid";
        public static string CURRENTUNITS = "CurrentUnits";
        public static string SOLDUNITS = "SoldUnits";
        public static string SALENETVALUE = "SaleNetVaue";
        public static string TOTALFINSIMPCASH = "FinSimpTotalCash";
        public static string TOTALCASHDIFF = "DifferenceCash";
        public static string TOTALUNITSDIFF = "DifferenceUnits";
        public static string TOTALFINSIMPUNITS = "FinSimpTotalUnits";
        public static string FINSIMPREBALANCEDVALUE = "FinSimpRebalancedValue";
        public static string FINSIMPUNITPRICE = "FinsimpUnitPrice";

        public static string SECTYPE = "SecType";

      
        public DateTime ValuationDate;

        public SecuritySummaryReportDS()
        {
            this.ValuationDate = DateTime.Today;
            this.Tables.Add(SecuritySummaryTable());
         }

        public DataTable SecuritySummaryTable()
        {
            DataTable dt = new DataTable(SECURITYSUMMARYTABLE);
            dt.Columns.Add(CLIENTCID, typeof(Guid));
            dt.Columns.Add(CLIENTNAME, typeof(string));
            dt.Columns.Add(BGLCODE, typeof(string));
            dt.Columns.Add(CLIENTID, typeof(string));
            dt.Columns.Add(SECTYPE, typeof(string));
            dt.Columns.Add(SECNAME, typeof(string));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(TOTALUNITS, typeof(decimal));
            dt.Columns.Add(UNITPRICE, typeof(decimal));
            dt.Columns.Add(TOTAL, typeof(decimal));
            dt.Columns.Add(TOTALFINSIMPUNITS, typeof(decimal));
            dt.Columns.Add(FINSIMPUNITPRICE, typeof(decimal));
            dt.Columns.Add(TOTALFINSIMPCASH, typeof(decimal));
            dt.Columns.Add(FINSIMPREBALANCEDVALUE, typeof(decimal));
            dt.Columns.Add(TOTALUNITSDIFF, typeof(decimal));
            dt.Columns.Add(TOTALCASHDIFF, typeof(decimal));
            dt.Columns.Add(PRICEATDATE, typeof(DateTime));
            return dt; 
        }

        public string DateInfo()
        {
            return "AS OF " + this.ValuationDate.ToString("dd/MM/yyyy");
        }

        public void AddSecuritySummaryRow(string secType, string securityName, string description, decimal totalUnits, decimal unitPrice, DateTime priceAtDate, decimal totalTotal, Guid clientCID, string clientID,string clientName)
        {
            DataRow secSumaryRow = this.Tables[SECURITYSUMMARYTABLE].NewRow();
            secSumaryRow[SecuritySummaryReportDS.CLIENTCID] = clientCID;
            secSumaryRow[SecuritySummaryReportDS.CLIENTID] = clientID;
            secSumaryRow[SecuritySummaryReportDS.CLIENTNAME] = clientName;
            secSumaryRow[SecuritySummaryReportDS.SECNAME] = securityName;
            secSumaryRow[SecuritySummaryReportDS.DESCRIPTION] = description;
            secSumaryRow[SecuritySummaryReportDS.TOTALUNITS] = totalUnits;
            secSumaryRow[SecuritySummaryReportDS.UNITPRICE] = unitPrice;
            secSumaryRow[SecuritySummaryReportDS.TOTAL] = totalTotal;
            secSumaryRow[SecuritySummaryReportDS.PRICEATDATE] = priceAtDate;
            secSumaryRow[SecuritySummaryReportDS.SECTYPE] = secType;
            this.Tables[SECURITYSUMMARYTABLE].Rows.Add(secSumaryRow);
        }

        public void AddSecuritySummaryRowBankTransaction(string secType, string securityName, string description, decimal totalUnits, decimal unitPrice, DateTime priceAtDate, decimal totalTotal, Guid clientCID, string clientID, string clientName)
        {
            DataRow secSumaryRow = this.Tables[SECURITYSUMMARYTABLE].NewRow();
            secSumaryRow[SecuritySummaryReportDS.CLIENTCID] = clientCID;
            secSumaryRow[SecuritySummaryReportDS.CLIENTID] = clientID;
            secSumaryRow[SecuritySummaryReportDS.CLIENTNAME] = clientName;
            secSumaryRow[SecuritySummaryReportDS.SECNAME] = securityName;
            secSumaryRow[SecuritySummaryReportDS.DESCRIPTION] = description;
            secSumaryRow[SecuritySummaryReportDS.TOTAL] = totalTotal;
            secSumaryRow[SecuritySummaryReportDS.PRICEATDATE] = priceAtDate;
            secSumaryRow[SecuritySummaryReportDS.SECTYPE] = secType;
            this.Tables[SECURITYSUMMARYTABLE].Rows.Add(secSumaryRow);
        }

        public void AddSecuritySummaryRowBankTransactionWithBGLCode(string BGLCode, string secType, string securityName, string description, decimal totalUnits, decimal unitPrice, DateTime priceAtDate, decimal totalTotal, Guid clientCID, string clientID, string clientName)
        {
            DataRow secSumaryRow = this.Tables[SECURITYSUMMARYTABLE].NewRow();
            secSumaryRow[SecuritySummaryReportDS.CLIENTCID] = clientCID;
            secSumaryRow[SecuritySummaryReportDS.CLIENTID] = clientID;
            secSumaryRow[SecuritySummaryReportDS.CLIENTNAME] = clientName;
            secSumaryRow[SecuritySummaryReportDS.SECNAME] = securityName;
            secSumaryRow[SecuritySummaryReportDS.DESCRIPTION] = description;
            secSumaryRow[SecuritySummaryReportDS.TOTAL] = totalTotal;
            secSumaryRow[SecuritySummaryReportDS.PRICEATDATE] = priceAtDate;
            secSumaryRow[SecuritySummaryReportDS.SECTYPE] = secType;
            secSumaryRow[SecuritySummaryReportDS.BGLCODE] = BGLCode;
            this.Tables[SECURITYSUMMARYTABLE].Rows.Add(secSumaryRow);
        }

    }
}
