﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class AdviceActionDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public AdviceActionTable AdviceActionTable = new AdviceActionTable();

        public AdviceActionDS()
        {
            Tables.Add(AdviceActionTable);
        }

        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
    }

    public class AdviceActionTable : DataTable
    {
        private readonly string TABLENAME = "AdviceActionList";
        public readonly string ADVICESID = "AdvicesId";
        public readonly string ACTIONTYPE = "ActionType";
        public readonly string PERCENTOFSALARY = "PercentOfSalary";
        public readonly string AMOUNT = "Amount";
        public readonly string CONTRIBUTIONSTARTAGE = "ContributionStartAge";
        public readonly string CONTRIBUTIONENDAGE = "ContributionEndAge";
        public readonly string SAVINGSTARTAGE = "SavingStartAge";
        public readonly string SAVINGENDAGE = "SavingEndAge";

        internal AdviceActionTable()
        {
            TableName = TABLENAME;
            Columns.Add(ADVICESID, typeof(Guid));
            Columns.Add(ACTIONTYPE, typeof(int));
            Columns.Add(PERCENTOFSALARY, typeof(int));
            Columns.Add(AMOUNT, typeof(decimal));
            Columns.Add(CONTRIBUTIONSTARTAGE, typeof(int));
            Columns.Add(CONTRIBUTIONENDAGE, typeof(int));
            Columns.Add(SAVINGSTARTAGE, typeof(int));
            Columns.Add(SAVINGENDAGE, typeof(int));
        }
    }
}
