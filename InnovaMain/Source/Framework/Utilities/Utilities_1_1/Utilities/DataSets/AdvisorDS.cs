﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class AdvisorDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public AdvisorTable AdvisorTable = new AdvisorTable();
        
        public AdvisorDS()
        {
            Tables.Add(AdvisorTable);
            
        }


        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
    }

    public class AdvisorTable : DataTable
    {
        private readonly string TABLENAME = "AdvisorList";
        public readonly string CID = "Cid";
        public readonly string CLID = "Clid";
        public readonly string CSID = "Csid";
        public readonly string CLIENTID = "ClientID";
        public readonly string OTHERID = "OtherID";
        public readonly string NAME = "Name";
        public readonly string TYPE = "Type";
        public readonly string STATUS = "Status";
        public readonly string FIRSTNAME = "FirstName";
        public readonly string SURNAME = "Surname";
        public readonly string AUTHORISEDREPNO = "AuthorisedRepNo";
        public readonly string HOMEPHONECOUNTRYCODE = "HomePhoneCountryCode";
        public readonly string HOMEPHONECITYCODE = "HomePhoneCityCode";
        public readonly string HOMEPHONENUMBER = "HomePhoneNumber";
        public readonly string WORKPHONECOUNTRYCODE = "WorkPhoneCountryCode";
        public readonly string WORKPHONECITYCODE = "WorkPhoneCityCode";
        public readonly string WORKPHONENUMBER = "WorkPhoneNumber";
        public readonly string MOBILEPHONECOUNTRYCODE = "MobilePhoneCountryCode";
        public readonly string MOBILEPHONENUMBER = "MobilePhoneNumber";
        public readonly string FACSIMILECOUNTRYCODE = "FacsimileCountryCode";
        public readonly string FACSIMILECITYCODE = "FacsimileCityCode";
        public readonly string FACSIMILENUMBER = "FacsimileNumber";
        public readonly string TFN = "TFN";
        public readonly string EMAIL = "Email";
        public readonly string APPOINTEDDATE = "AppointedDate";
        public readonly string TERMINATEDDATE = "TerminatedDate";
        public readonly string REASONTERMINATED = "ReasonTerminated";
        public readonly string GENDER = "Gender";
        public readonly string PREFERREDNAME = "PreferredName";
        public readonly string TITLE = "Title";
        public readonly string OCCUPATION = "Occupation";
        public readonly string BTWRAPADVISERCODE = "BTWRAPAdviserCode";
        public readonly string DESKTOPBROKERADVISERCODE = "DesktopBrokerAdviserCode";
        public readonly string AFSLICENSEENAME = "AFSLicenseeName";
        public readonly string AFSLICENCENUMBER = "AFSLicenceNumber";
        public readonly string BANKWESTADVISORCODE = "BankWestAdvisorCode";

        internal AdvisorTable()
        {
            TableName = TABLENAME;
            Columns.Add(CLID, typeof(Guid));
            Columns.Add(CSID, typeof(Guid));
            Columns.Add(CID, typeof(Guid));
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(OTHERID, typeof(string));
            Columns.Add(NAME, typeof(string));
            Columns.Add(TYPE, typeof(string));
            Columns.Add(STATUS, typeof(string));
            Columns.Add(FIRSTNAME, typeof(string));
            Columns.Add(SURNAME, typeof(string));
            Columns.Add(AUTHORISEDREPNO, typeof(string));
            Columns.Add(HOMEPHONECOUNTRYCODE, typeof(string));
            Columns.Add(HOMEPHONECITYCODE, typeof(string));
            Columns.Add(HOMEPHONENUMBER, typeof(string));
            Columns.Add(WORKPHONECOUNTRYCODE, typeof(string));
            Columns.Add(WORKPHONECITYCODE, typeof(string));
            Columns.Add(WORKPHONENUMBER, typeof(string));
            Columns.Add(MOBILEPHONECOUNTRYCODE, typeof(string));
            Columns.Add(MOBILEPHONENUMBER, typeof(string));
            Columns.Add(FACSIMILECOUNTRYCODE, typeof(string));
            Columns.Add(FACSIMILECITYCODE, typeof(string));
            Columns.Add(FACSIMILENUMBER, typeof(string));
            Columns.Add(TFN, typeof(string));
            Columns.Add(EMAIL, typeof(string));
            Columns.Add(APPOINTEDDATE, typeof(DateTime));
            Columns.Add(TERMINATEDDATE, typeof(DateTime));
            Columns.Add(REASONTERMINATED, typeof(string));
            Columns.Add(GENDER, typeof(string));
            Columns.Add(PREFERREDNAME, typeof(string));
            Columns.Add(TITLE, typeof(string));
            Columns.Add(OCCUPATION, typeof(string));
            Columns.Add(BTWRAPADVISERCODE, typeof(string));
            Columns.Add(DESKTOPBROKERADVISERCODE, typeof(string));
            Columns.Add(AFSLICENSEENAME, typeof(string));
            Columns.Add(AFSLICENCENUMBER, typeof(string));
            Columns.Add(BANKWESTADVISORCODE, typeof(string));
        }
    }

}
