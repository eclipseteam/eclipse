﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class NonIndividualDS : DataSet, IHasOrganizationUnit
    {
        public Guid NonIndividualCID { get; set; }

        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public NonIndividuals NonIndividualTable = new NonIndividuals();

        public NonIndividualDS()
        {
            Tables.Add(NonIndividualTable);
        }

        public OrganizationUnit Unit { get; set; }

        public int Command { get; set; }

    }

    public class NonIndividuals : DataTable
    {
        public readonly string TABLENAME = "NonIndividualList";
        public readonly string CLIENTID = "ClientId";
        public readonly string CID = "Cid";
        public readonly string CLID = "CLid";
        public readonly string CSID = "CSid";

        public readonly string TYPE = "Type";
        public readonly string NAME = "Name";
        public readonly string EMAILADDRESS = "EmailAddress";
        public readonly string DATEOFCREATION = "DateOfCreation";
        public readonly string OFFICECOUNTRYCODE = "OfficeCountryCode";
        public readonly string OFFICECITYCODE = "OfficeCityCode";
        public readonly string OFFICEPHONENUMBER = "OfficePhoneNumber";
        public readonly string OFFICECOUNTRYCODE2 = "OfficeCountryCode2";
        public readonly string OFFICECITYCODE2 = "OfficeCityCode2";
        public readonly string OFFICEPHONENUMBER2 = "OfficePhoneNumber2";
        public readonly string MOBILECOUNTRYCODE = "MobileCountryCode";
        public readonly string MOBILEPHONENUMBER = "MobilePhoneNumber";
        public readonly string FACSIMILECOUNTRYCODE = "FacsimileCountryCode";
        public readonly string FACSIMILECITYCODE = "FacsimileCityCode";
        public readonly string FACSIMILEPHONENUMBER = "FacsimilePhoneNumber";
        public readonly string TFN = "TFN";
        public readonly string ABN = "ABN";
        public readonly string DESCRIPTION = "Description";
        
        public readonly string RESIDENTIALADDRESSLINE1 = "ResidentialAddressLine1";
        public readonly string RESIDENTIALADDRESSLINE2 = "ResidentialAddressLine2";
        public readonly string RESIDENTIALADDRESSSUBRUB = "ResidentialAddressSubrub";
        public readonly string RESIDENTIALADDRESSSTATE = "ResidentialAddressState";
        public readonly string RESIDENTIALADDRESSPOSTALCODE = "ResidentialAddressPostalCode";
        public readonly string RESIDENTIALADDRESSCOUNTRY = "ResidentialAddressCountry";

        public readonly string MAILINGADDRESSLINE1 = "MailingAddressLine1";
        public readonly string MAILINGADDRESSLINE2 = "MailingAddressLine2";
        public readonly string MAILINGADDRESSSUBRUB = "MailingAddressSubrub";
        public readonly string MAILINGADDRESSSTATE = "MailingAddressState";
        public readonly string MAILINGADDRESSPOSTALCODE = "MailingAddressPostalCode";
        public readonly string MAILINGADDRESSCOUNTRY = "MailingAddressCountry";
        
        public readonly string MAILINGADDRESSSAME = "MailingAddressSame";
        public readonly string HASADDRESS = "HasAddress";
        public readonly string ADVISERCID = "AdviserCid";
        public readonly string ADVISERCLID = "AdviserClid";
        public readonly string ADVISERCSID = "AdviserCsid";

        internal NonIndividuals()
        {
            TableName = TABLENAME;
            Columns.Add(CLIENTID, typeof (string));
            Columns.Add(CID, typeof (Guid));
            Columns.Add(CLID, typeof (Guid));
            Columns.Add(CSID, typeof (Guid));
            Columns.Add(TYPE, typeof(string));
            Columns.Add(NAME, typeof (string));
            Columns.Add(EMAILADDRESS, typeof (string));
            Columns.Add(DATEOFCREATION, typeof (DateTime));
            Columns.Add(OFFICECOUNTRYCODE, typeof(string));
            Columns.Add(OFFICECITYCODE, typeof(string));
            Columns.Add(OFFICEPHONENUMBER, typeof(string));
            Columns.Add(OFFICECOUNTRYCODE2, typeof(string));
            Columns.Add(OFFICECITYCODE2, typeof(string));
            Columns.Add(OFFICEPHONENUMBER2, typeof(string));
            Columns.Add(MOBILECOUNTRYCODE, typeof(string));
            Columns.Add(MOBILEPHONENUMBER, typeof(string));
            Columns.Add(FACSIMILECOUNTRYCODE, typeof (string));
            Columns.Add(FACSIMILECITYCODE, typeof(string));
            Columns.Add(FACSIMILEPHONENUMBER, typeof(string));
            Columns.Add(TFN, typeof (string));
            Columns.Add(ABN, typeof (string));
            Columns.Add(DESCRIPTION, typeof (string));
            Columns.Add(RESIDENTIALADDRESSLINE1, typeof(string));
            Columns.Add(RESIDENTIALADDRESSLINE2, typeof(string));
            Columns.Add(RESIDENTIALADDRESSSUBRUB, typeof(string));
            Columns.Add(RESIDENTIALADDRESSSTATE, typeof(string));
            Columns.Add(RESIDENTIALADDRESSPOSTALCODE, typeof(string));
            Columns.Add(RESIDENTIALADDRESSCOUNTRY, typeof(string));
            Columns.Add(MAILINGADDRESSLINE1, typeof(string));
            Columns.Add(MAILINGADDRESSLINE2, typeof(string));
            Columns.Add(MAILINGADDRESSSUBRUB, typeof(string));
            Columns.Add(MAILINGADDRESSSTATE, typeof(string));
            Columns.Add(MAILINGADDRESSPOSTALCODE, typeof(string));
            Columns.Add(MAILINGADDRESSCOUNTRY, typeof(string));
            Columns.Add(MAILINGADDRESSSAME, typeof (bool));
            Columns.Add(HASADDRESS, typeof (bool));
            Columns.Add(ADVISERCID, typeof (Guid)).DefaultValue = Guid.Empty;
            Columns.Add(ADVISERCLID, typeof (Guid)).DefaultValue = Guid.Empty;
            Columns.Add(ADVISERCSID, typeof (Guid)).DefaultValue = Guid.Empty;
        }
    }
}