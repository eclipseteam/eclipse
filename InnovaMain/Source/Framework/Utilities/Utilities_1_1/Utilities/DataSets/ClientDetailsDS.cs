﻿using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class ClientDetailsDS : DataSet
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public ClientDetails ClientDetailsTable = new ClientDetails();
        
        public ClientDetailsDS()
        {
            Tables.Add(ClientDetailsTable);
        }
    }

    public class ClientDetails : DataTable
    {
        public readonly string TABLENAME = "ClientDetails";
        public readonly string CLIENTID = "ClientId";
        public readonly string CLIENTNAME = "ClientName";
        public readonly string ISCLIENT = "IsClient";
        public readonly string INVESTORTYPE = "InvesterType";
        public readonly string STATUSFLAG = "StatusFlag";
        public readonly string DOITYOURSELF = "DoItYourSelf";
        public readonly string DOITFORME = "DoItForMe";
        public readonly string DOITWITHME = "DoItWithMe";

        internal ClientDetails()
        {
            TableName = TABLENAME;
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(CLIENTNAME, typeof(string));
            Columns.Add(ISCLIENT, typeof(string));
            Columns.Add(INVESTORTYPE, typeof(string));
            Columns.Add(STATUSFLAG, typeof(string));
            Columns.Add(DOITYOURSELF, typeof(string));
            Columns.Add(DOITFORME, typeof(string));
            Columns.Add(DOITWITHME, typeof(string));
        }
    }
}
