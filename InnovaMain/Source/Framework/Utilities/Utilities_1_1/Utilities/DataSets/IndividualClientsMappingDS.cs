﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class IndividualClientsMappingDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public IndvidualClientMappingTable ClientMappingTable = new IndvidualClientMappingTable();

        public IndividualClientsMappingDS()
        {
            Tables.Add(ClientMappingTable);
        }
        public OrganizationUnit Unit
        {
            get; set;
        }
        public int Command
        {
            get;
            set;
        }
    }
     public class IndvidualClientMappingTable : DataTable
    {
        private readonly string TABLENAME = "ClientMappingList";
        public readonly string CLIENTID = "ClientID";
        public readonly string CID = "Cid";
        public readonly string CLID = "Clid";
        public readonly string CSID = "Csid";
        public readonly string NAME = "Name";
        public readonly string TYPE = "Type";
        public readonly string INDISHARE = "IndiShare";
        public readonly string INDICID = "IndiCid";
        public readonly string INDICLID = "IndiClid";
        public readonly string INDICSID = "IndiCsid";
        public string INDIBUSINESSTITLE = "IndiBusinessTitle";

         internal IndvidualClientMappingTable()
        {
            TableName = TABLENAME;
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(CID, typeof(Guid));
            Columns.Add(CLID, typeof(Guid));
            Columns.Add(CSID, typeof(Guid));
            Columns.Add(NAME, typeof(string));
            Columns.Add(TYPE, typeof(string));
            Columns.Add(INDISHARE, typeof(decimal));
            Columns.Add(INDICID, typeof(Guid));
            Columns.Add(INDICLID, typeof(Guid));
            Columns.Add(INDICSID, typeof(Guid));
            Columns.Add(INDIBUSINESSTITLE, typeof(string));
            
        }

         public void AddRow(Guid individualCId,Guid indiClid,Guid indiCsid,string clientid,Guid cid,Guid clid,Guid csid,string name,string type ,decimal? indishare )
         {
             var row = NewRow();
             row[CLIENTID] = clientid;
             row[CID] = cid;
             row[CLID] = clid;
             row[CSID] = csid;
             row[NAME] = name;
             row[TYPE] = type;
             if (indishare.HasValue)
                 row[INDISHARE] = indishare;
             row[INDICID] = individualCId;
             row[INDICLID] = indiClid;
             row[INDICSID] = indiCsid;
             Rows.Add(row);

         }
    }
}


