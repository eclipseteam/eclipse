﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class ExportAppFormsDS : DataSet
    {
       public string AppNo=string.Empty;
       public string Contents = string.Empty;
       public ServiceTypes serviceType = ServiceTypes.None;
       public bool SearchByCID = false;
       public Guid ClientCID = Guid.Empty;
       public ExportAppFormsDS()
        {
        }
    }
}
