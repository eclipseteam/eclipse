﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class MDAExecutionDS : DataSet, IHasOrganizationUnit
    {
        public MDAExecutionDS()
        {
            this.Tables.Add(MdaExecutionTable);
        }

        public DataSetOperationType DataSetOperationType;
        public OrganizationUnit Unit
        {
            get; 
            set;
        }

        public int Command
        {
            get;
            set;
        }

        public MDAExecutionTable MdaExecutionTable = new MDAExecutionTable();
      
    }

    public class MDAExecutionTable : DataTable
    {
        public const string TABLENAME = "MDAExecutionTable";
        public const string MDAEXECUTIONDATE = "MdaExecutionDate";
        
        public MDAExecutionTable()
        {
            TableName = TABLENAME;
            this.Columns.Add(MDAEXECUTIONDATE, typeof (DateTime));
        }

        
    }

}
