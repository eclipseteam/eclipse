﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class MissingAccountDS : DataSet,IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public MissingAccountTable MissingAccountTable = new MissingAccountTable();

        public MissingAccountDS()
        {
            Tables.Add(MissingAccountTable);
        }


        public OrganizationUnit Unit
        {
            get; set;
        }

        public int Command
        {
            get;
            set;
        }
    }

    public class MissingAccountTable : DataTable
    {
        public readonly string TABLENAME = "MISBasicList";
        public readonly string ID = "ID";
        public readonly string CID = "Cid";
        public readonly string CLID = "CLid";
        public readonly string CSID = "CSid";
        public readonly string NAME = "Name";
        public readonly string STATUS = "Status";

        internal MissingAccountTable()
        {
            TableName = TABLENAME;
            Columns.Add(ID, typeof(Guid));
            Columns.Add(CID, typeof(Guid));
            Columns.Add(CLID, typeof(Guid));
            Columns.Add(CSID, typeof(Guid));
            Columns.Add(NAME, typeof(string));            
            Columns.Add(STATUS, typeof(string));
        }
    }
}
