﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class OtherServicesDS : DataSet
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public readonly string TABLENAME = "OtherServicesList";

        //BTWrap
        public readonly string ACCOUNTNUMBER = "AccountNumber";
        public readonly string ACCOUNTDESCRIPTION = "AccountDescription";

        //ExemptionCategory
        public readonly string CATEGORY1 = "Category1";
        public readonly string CATEGORY2 = "Category2";
        public readonly string CATEGORY3 = "Category3";

        //OperationManner
        public readonly string ANYONEOFUSTOSIGN = "AnyOneOfUsToSign";
        public readonly string ANYTWOOFUSTOSIGN = "AnyTwoOfUsToSign";
        public readonly string ALLOFUSTOSIGN = "AllOfUsToSign";

        //AccessFacilities
        public readonly string PHONEACCESS = "PhoneAccess";
        public readonly string ONLINEACCESS = "OnlineAccess";
        public readonly string DEBITCARD = "DebitCard";
        public readonly string CHEQUEBOOK = "ChequeBook";
        public readonly string DEPOSITBOOK = "DepositBook";

        //BusinessGroupID
        public readonly string BUSINESSGROUPID = "BusinessGroupID";
        public bool IsClassSuper = false;
        public bool IsDesktopSuper = false;
        public bool IsBGL = false;

        public OtherServicesDS()
        {
            DataTable dt = new DataTable(TABLENAME);
            dt.Columns.Add(ACCOUNTNUMBER, typeof(string));
            dt.Columns.Add(ACCOUNTDESCRIPTION, typeof(string));
            dt.Columns.Add(CATEGORY1, typeof(bool));
            dt.Columns.Add(CATEGORY2, typeof(bool));
            dt.Columns.Add(CATEGORY3, typeof(bool));
            dt.Columns.Add(ANYONEOFUSTOSIGN, typeof(bool));
            dt.Columns.Add(ANYTWOOFUSTOSIGN, typeof(bool));
            dt.Columns.Add(ALLOFUSTOSIGN, typeof(bool));
            dt.Columns.Add(PHONEACCESS, typeof(bool));
            dt.Columns.Add(ONLINEACCESS, typeof(bool));
            dt.Columns.Add(DEBITCARD, typeof(bool));
            dt.Columns.Add(CHEQUEBOOK, typeof(bool));
            dt.Columns.Add(DEPOSITBOOK, typeof(bool));
            dt.Columns.Add(BUSINESSGROUPID, typeof(Guid));
            Tables.Add(dt);
        }
    }
}
