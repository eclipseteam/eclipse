﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;

namespace Oritax.TaxSimp.DataSets
{
    public class BankDetailsDS : UMABaseDS
    {
        public const string BANKDETAILSTABLE = "BankDetailsTable";
        public const string BANKDETAILSTABLELIST = "BankDetailsTableList";
        public const string ID = "ID";
        public const string ACCOUNTNUMBER = "AccountNumber";
        public const string ACCOUTTYPE = "AccoutType";
        public const string BSB = "BSB";
        public const string CLIENTID = "ClientID";
        public const string CLIENTCID = "ClientCID";
        public const string CLIENTNAME = "ClientName";
        public const string TRANSACTION = "Transaction";
        public const string DIFF = "Difference";
        public const string HOLDING = "Holding";
        public const string INSID = "InstitutionID";
        public const string INSNAME = "InstitutionName";
        public const string ACCNAME = "Name";
        public const string CUSTNO = "CustomerNO";
        public const string ISEXTERNALACCOUNT = "IsExternalAccount";


        public BankDetailsDS()
        {
            DataTable dt = new DataTable(BANKDETAILSTABLE);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(ACCOUNTNUMBER, typeof(string));
            dt.Columns.Add(ACCOUTTYPE, typeof(string));
            dt.Columns.Add(BSB, typeof(string));
            dt.Columns.Add(CLIENTCID, typeof(Guid));
            dt.Columns.Add(CLIENTID, typeof(string));
            dt.Columns.Add(CLIENTNAME, typeof(string));
            dt.Columns.Add(TRANSACTION, typeof(decimal));
            dt.Columns.Add(HOLDING, typeof(decimal));
            dt.Columns.Add(DIFF, typeof(decimal));
            dt.Columns.Add(INSID, typeof(Guid));
            dt.Columns.Add(ACCNAME, typeof(string));
            dt.Columns.Add(CUSTNO, typeof(string));
            var externalAcc = dt.Columns.Add(ISEXTERNALACCOUNT, typeof(bool));
            externalAcc.DefaultValue = false;
            this.Tables.Add(dt);
        }
    }
}
