﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class IndividualCurrentAssetsDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public AssetsDetails assetsDetailsTable = new AssetsDetails();

        public IndividualCurrentAssetsDS()
        {
            Tables.Add(assetsDetailsTable);
        }

        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
    }

    public class AssetsDetails : DataTable
    {
        public readonly string TABLENAME = "ClientAssetsDetails";
        public readonly string CLIENTID = "ClientId";
        public readonly string CLIENTCID = "CLientCid";
        public readonly string AUSTRALIANEQUITIES = "AustralianEquities";
        public readonly string AUSTRALIANFIXEDINTEREST = "AustralianFixedInterest";
        public readonly string INTERNATIONALEQUITIES = "InternationalEquities";
        public readonly string CASH = "Cash";
        public readonly string PROPERTY = "Property";
        public readonly string GOLD = "Gold";

        private double ComputeSum(string Col)
        {
            double sum = 0;
            if (Rows.Count > 0)
            {
                var obj = Compute(string.Format("Sum({0})", Col), "");

                double.TryParse(obj.ToString(), out sum);
            }
            return sum;
        }
        public double AustralianEquities
        {
            get { return ComputeSum(AUSTRALIANEQUITIES); }
        }

        public double AustralianFixedInterest
        {
            get { return ComputeSum(AUSTRALIANFIXEDINTEREST); }

        }
        public double InternationalEquities
        {
            get { return ComputeSum(INTERNATIONALEQUITIES); }

        }
        public double Cash
        {
            get { return ComputeSum(CASH); }
        }

        public double Property
        {
            get { return ComputeSum(PROPERTY); }
        }
        public double Gold
        {
            get { return ComputeSum(GOLD); }
        }
        internal AssetsDetails()
        {
            TableName = TABLENAME;
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(CLIENTCID, typeof(Guid));
            Columns.Add(AUSTRALIANEQUITIES, typeof(double));
            Columns.Add(AUSTRALIANFIXEDINTEREST, typeof(double));
            Columns.Add(INTERNATIONALEQUITIES, typeof(double));
            Columns.Add(CASH, typeof(double));
            Columns.Add(PROPERTY, typeof(double));
            Columns.Add(GOLD, typeof(double));
        }
    }
}
