﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.DataSets
{
    public class SecTransactionDetailsDS : UMABaseDS
    {
        public string CLIENTID = string.Empty;
        public string CLIENTNAME = string.Empty; 

        public const string SECTRANSDETAILSTABLE = "SECTransactionDetailsTables";
        public const string ID = "Id";

        public const string TRANTYPE = "TranType";
        public const string UNITSPRICE = "UnitPrice";
        public const string HOLDINGS = "Holdings";
        public const string UNITSONHAND = "UnitsOnHand";
        public const string BROKERAGE = "BrokerageAmount";
        public const string CONFIRMED = "Comfirmed";
        public const string CHARGES = "Charges";
        public const string ACCOUNTNO = "AccountNo";
        public const string INVESTCODE = "InvestmentCode";
        public const string INVESTID = "InvestmentID";
        public const string INVESTDESC = "InvestmentDesc";
        public const string NARRATIVE = "Narrative";

        public const string SETTLEDATE = "SettlementDate";
        public const string TRANDATE = "TransactionDate";

        public Guid ParentID = Guid.Empty;

        public SecTransactionDetailsDS()
        {
            DataTable dt = new DataTable(SECTRANSDETAILSTABLE);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(TRANTYPE, typeof(string));
            dt.Columns.Add(ACCOUNTNO, typeof(string));
            dt.Columns.Add(UNITSPRICE, typeof(decimal));
            dt.Columns.Add(HOLDINGS, typeof(decimal));
            dt.Columns.Add(UNITSONHAND, typeof(decimal));
            dt.Columns.Add(BROKERAGE, typeof(decimal));
            dt.Columns.Add(CONFIRMED, typeof(bool));
            dt.Columns.Add(CHARGES, typeof(decimal));
            dt.Columns.Add(INVESTCODE, typeof(string));
            dt.Columns.Add(INVESTDESC, typeof(string));
            dt.Columns.Add(NARRATIVE, typeof(string));
            dt.Columns.Add(INVESTID, typeof(Guid));
            dt.Columns.Add(SETTLEDATE, typeof(DateTime));
            dt.Columns.Add(TRANDATE, typeof(DateTime));
            this.Tables.Add(dt); 
        }
    }
}
