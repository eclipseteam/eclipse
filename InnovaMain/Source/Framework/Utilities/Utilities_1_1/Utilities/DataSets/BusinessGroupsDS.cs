﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class BusinessGroupsDS : DataSet
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;        
        public const string TABLENAME = "BusinessGroupsTable";
        public const string ID= "ID";
        public const string NAME= "Name";
        public const string DESCRIPTION = "Description";


        public BusinessGroupsDS()
        {
            DataTable tab = new DataTable(TABLENAME);
            tab.Columns.Add(ID, typeof(Guid));            
            tab.Columns.Add(NAME, typeof(string));
            tab.Columns.Add(DESCRIPTION, typeof(string));            
            this.Tables.Add(tab);            
        }


    }
}
