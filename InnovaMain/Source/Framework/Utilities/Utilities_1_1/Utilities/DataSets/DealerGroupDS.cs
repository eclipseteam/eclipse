﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class DealerGroupDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public DealerGroupTable DealerGroupTable = new DealerGroupTable();

        public DealerGroupDS()
        {
            Tables.Add(DealerGroupTable);
        }


        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
    }

    public class DealerGroupTable : DataTable
    {
        private readonly string TABLENAME = "DealerGroupList";
        public readonly string CLIENTID = "ClientID";
        public readonly string TRADINGNAME = "TradingName";
        public readonly string TYPE = "Type";
        public readonly string STATUS = "Status";

        public readonly string DEALERGROUPTYPE = "DealerGroupType";
        public readonly string TITLE = "Title";
        public readonly string NAME = "Name";
        public readonly string LEGALNAME = "LegalName";
        public readonly string PREFERREDNAME = "PreferredName";
        public readonly string SURNAME = "SurName";
        public readonly string GENDER = "Gender";
        public readonly string HOMEPHONENUMBER = "HomePhoneNumber";
        public string HOMEPHONECITYCODE = "HomePhoneCountryCode";
        public string HOMEPHONECOUTNRYCODE = "HomePhoneCityCode";
        public readonly string WORKPHONENUMBER = "WorkPhoneNumber";
        public readonly string WORKPHONECOUTNRYCODE = "WorkPhoneCountryCode";
        public readonly string WORKPHONECITYCODE = "WorkPhoneCityCode";
        public readonly string FACSIMLENUMBER = "FacsimileNumber";
        public readonly string FACSIMLECOUTNRYCODE = "FacsimileCountryCode";
        public readonly string FACSIMLECITYCODE = "FacsimileCityCode";
        public readonly string MOBILEPHONENUMBER = "MobilePhoneNumber";
        public readonly string MOBILEPHONECOUTNRYCODE = "MobilePhoneCountryCode";
        public readonly string ADDRESS = "Address";
        public readonly string EMAIL = "Email";

        public readonly string OCCUPATION = "Occupation";
        public readonly string ACN = "ACN";
        public readonly string ABN = "ABN";
        public readonly string TFN = "TFN";
        public readonly string AFSLNO = "AFSLNO";

        public readonly string ADVISORYFIRMTERMINOLOGY = "AdvisoryFirmTerminology";
        public readonly string WEBSITEADDRESS = "WebSiteAddress";
        public readonly string FUM = "FUM";
        public readonly string TURNOVER = "TurnOver";





        public readonly string CID = "Cid";
        public readonly string CLID = "Clid";
        public readonly string CSID = "Csid";



        internal DealerGroupTable()
        {
            TableName = TABLENAME;
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(TRADINGNAME, typeof(string));
            Columns.Add(TYPE, typeof(string));
            Columns.Add(STATUS, typeof(string));

            Columns.Add(DEALERGROUPTYPE, typeof(string));
            Columns.Add(TITLE, typeof(string));
            Columns.Add(NAME, typeof(string));
            Columns.Add(LEGALNAME, typeof(string));
            Columns.Add(PREFERREDNAME, typeof(string));
            Columns.Add(SURNAME, typeof(string));
            Columns.Add(GENDER, typeof(string));

            Columns.Add(HOMEPHONECOUTNRYCODE, typeof(string));
            Columns.Add(HOMEPHONECITYCODE, typeof(string));
            Columns.Add(HOMEPHONENUMBER, typeof(string));

            Columns.Add(WORKPHONECOUTNRYCODE, typeof(string));
            Columns.Add(WORKPHONECITYCODE, typeof(string));
            Columns.Add(WORKPHONENUMBER, typeof(string));
            Columns.Add(FACSIMLECOUTNRYCODE, typeof(string));
            Columns.Add(FACSIMLECITYCODE, typeof(string));
            Columns.Add(FACSIMLENUMBER, typeof(string));
            Columns.Add(MOBILEPHONECOUTNRYCODE, typeof(string));
            Columns.Add(MOBILEPHONENUMBER, typeof(string));

            Columns.Add(ADDRESS, typeof(string));
            Columns.Add(EMAIL, typeof(string));
            Columns.Add(OCCUPATION, typeof(string));
            Columns.Add(ACN, typeof(string));
            Columns.Add(ABN, typeof(string));
            Columns.Add(TFN, typeof(string));
            Columns.Add(AFSLNO, typeof(string));
            Columns.Add(ADVISORYFIRMTERMINOLOGY, typeof(string));
            Columns.Add(WEBSITEADDRESS, typeof(string));

            Columns.Add(FUM, typeof(string));
            Columns.Add(TURNOVER, typeof(string));

            Columns.Add(CLID, typeof(Guid));
            Columns.Add(CSID, typeof(Guid));
            Columns.Add(CID, typeof(Guid));

        }
    }
}
