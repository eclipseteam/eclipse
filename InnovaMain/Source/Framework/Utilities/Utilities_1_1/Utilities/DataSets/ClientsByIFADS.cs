﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;

namespace Oritax.TaxSimp.DataSets
{
    public class ClientsByIFADS : DataSet
    {
        public const string IFADETAILTABLE = "IFADetailTable";
        public const string IFANAME = "IFAName";
        public const string IFAID = "IFAID";
        public const string ISCLASSSUPER = "IsClassSuper";
        public const string ISBGL = "IsBGL";
        public const string CLIENTDETAILSTABLE = "Client";
        public const string ID = "ID";
        public const string CLIENTID = "ClientID";
        public const string CLIENTNAME = "Name";
        public const string CLIENTTYPE = "Type";
        public bool IncludeCheckForCID = true;

        public ClientsByIFADS()
        {
            DataTable dt = new DataTable(IFADETAILTABLE);
            dt.Columns.Add(IFANAME, typeof(string));
            dt.Columns.Add(IFAID, typeof(string));
            this.Tables.Add(dt);

            dt = new DataTable(CLIENTDETAILSTABLE);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(CLIENTID, typeof(string));
            dt.Columns.Add(CLIENTNAME, typeof(string));
            dt.Columns.Add(CLIENTTYPE, typeof(string));
            this.Tables.Add(dt);
        }
    }
}
