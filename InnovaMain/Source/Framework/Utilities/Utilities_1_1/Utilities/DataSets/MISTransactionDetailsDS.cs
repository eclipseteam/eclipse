﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;

namespace Oritax.TaxSimp.DataSets
{
    public class MISTransactionDetailsDS : UMABaseDS
    {
        public const string MISTRANSDETAILSTABLE = "MISTransactionDetailsTables";
        public const string ID = "Id";
        public const string SYSTRANTYPE = "SysTranType";
        public const string AMOUNT = "Amount";
        public const string SHARES = "Shares";
        public const string UNITPRICE = "UnitPrice";
        public const string FUNDCODE = "FunCode";
        public const string FUNDCODEOLD = "FunCodeOld";
        public const string CLIENTID = "ClientID";
        public const string TRADEDATE = "TradeDate";
        public string ClientID = "";

        public MISTransactionDetailsDS()
        {
            DataTable dt = new DataTable(MISTRANSDETAILSTABLE);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(SYSTRANTYPE, typeof(string));
            dt.Columns.Add(AMOUNT, typeof(decimal));
            dt.Columns.Add(SHARES, typeof(decimal));
            dt.Columns.Add(UNITPRICE, typeof(decimal));
            dt.Columns.Add(TRADEDATE, typeof(DateTime));
            dt.Columns.Add(FUNDCODE, typeof(string));
            dt.Columns.Add(FUNDCODEOLD, typeof(string));
            dt.Columns.Add(CLIENTID, typeof(string));
            this.Tables.Add(dt);

        }
    }
}
