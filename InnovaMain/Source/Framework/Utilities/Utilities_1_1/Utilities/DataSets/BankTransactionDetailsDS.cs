﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class BankTransactionDetailsDS : UMABaseDS, IHasOrganizationUnit
    {

        public const string BANKTRANSDETAILSTABLE = "BankTransactionDetailsTables";
        public const string ID = "Id";
        public const string IMPORTTRANTYPE = "ImportTranType";
        public const string SYSTRANTYPE = "SysTranType";
        public const string CATTRANTYPE = "CatTranType";
        public const string TRANTYPE = "TranType";
        public const string AMOUNT = "Amount";
        public const string ADJUSTMENT = "Adjustment";
        public const string TOTALAMOUNT = "TotalAmount";
        public const string COMMENT = "Comment";

        public string BankTransactionID = string.Empty;
        public const string INSTITUTEID = "InstituteID";
        public const string CONTRACTNOTE = "ContractNote";
        public const string INTERESTRATE = "InterestRate";
        public const string MATURITYDATE = "MaturityDate";
        public const string TRANDATE = "TranDate";
        public const string PROID = "ProductID";
        public const string ADMINSYS = "AdminSys";
        public const string ORDERID = "OrderID";
        public const string ORDERITEMID = "OrderItemID";
        public const string ISCONFIRMED = "IsConfirmed";

        public BankTransactionDetailsDS()
        {
            DataTable dt = new DataTable(BANKTRANSDETAILSTABLE);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(CONTRACTNOTE, typeof(string));
            dt.Columns.Add(INSTITUTEID, typeof(Guid));
            dt.Columns.Add(TRANTYPE, typeof(string));
            dt.Columns.Add(IMPORTTRANTYPE, typeof(string));
            dt.Columns.Add(SYSTRANTYPE, typeof(string));
            dt.Columns.Add(CATTRANTYPE, typeof(string));
            dt.Columns.Add(AMOUNT, typeof(decimal));
            dt.Columns.Add(INTERESTRATE, typeof(decimal));
            dt.Columns.Add(ADJUSTMENT, typeof(decimal));
            dt.Columns.Add(TOTALAMOUNT, typeof(decimal));
            dt.Columns.Add(COMMENT, typeof(string));
            dt.Columns.Add(TRANDATE, typeof(DateTime));
            dt.Columns.Add(ADMINSYS, typeof(string));
            dt.Columns.Add(PROID, typeof(Guid));
            dt.Columns.Add(MATURITYDATE, typeof(DateTime));
            dt.Columns.Add(ORDERID, typeof(Guid));
            dt.Columns.Add(ORDERITEMID, typeof(Guid));
            dt.Columns.Add(ISCONFIRMED, typeof(bool));
            this.Tables.Add(dt);

        }

        public OrganizationUnit Unit
        {
            get;set;
        }

        public int Command
        {
            get;set;

        }
    }
}
