﻿using System;
using System.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class TDTransactionDS : BaseClientDetails
    {
        public string CLIENTIDPara = string.Empty;
        public string CLIENTNAMEPara = string.Empty;
        public const string BANKACCOUNTLIST = "BankAccountList";
        public const string TDACCOUNTLIST = "TDAccountList";
        public const string TDTRANTABLE = "TDs Transactions";
        public const string BANKCID = "BankCID";
        public const string BSBNO = "BSBNo";
        public const string BANKACCOUNTNO = "AccountNo";
        public const string BANKCODE = "BANKCODE";
        public const string BANKACCOUNTNAME = "AccountName";
        public DateTime StartDate = DateTime.Now.AddYears(-10);
        public DateTime EndDate = DateTime.Now;

        public const string PRODUCTLIST = "PRODUCTLIST";
        public const string PROID = "ProID";
        public const string PRONAME = "ProName";

        public string DateInfo()
        {
            return this.StartDate.ToString("dd/MM/yyyy") + " - " + this.EndDate.ToString("dd/MM/yyyy");
        }

        public TDTransactionDS()
        {
            DataTable tdDetails = new DataTable(TDACCOUNTLIST);
            tdDetails.Columns.Add(BANKCID, typeof(Guid));
            tdDetails.Columns.Add(BSBNO, typeof(string));
            tdDetails.Columns.Add(BANKACCOUNTNO, typeof(string));
            tdDetails.Columns.Add(BANKCODE, typeof(string));
            tdDetails.Columns.Add(BANKACCOUNTNAME, typeof(string));
            this.Tables.Add(tdDetails);

            DataTable productListing = new DataTable(PRODUCTLIST);
            productListing.Columns.Add(PROID, typeof(Guid));
            productListing.Columns.Add(PRONAME, typeof(string));
            this.Tables.Add(productListing);
        }
    }
}
