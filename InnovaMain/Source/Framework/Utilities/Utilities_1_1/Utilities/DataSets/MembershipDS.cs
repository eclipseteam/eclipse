﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class MembershipDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public MembershipTable membershipTable = new MembershipTable();
        public ParentTable parentTable = new ParentTable();
        public MembershipDS()
        {
            Tables.Add(membershipTable);
            Tables.Add(parentTable);
        }


        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
    }

    public class MembershipTable : DataTable
    {
        private readonly string TABLENAME = "MembershipList";
        public readonly string CLIENTID = "ClientID";
        public readonly string CID = "Cid";
        public readonly string CLID = "Clid";
        public readonly string CSID = "Csid";
        public readonly string NAME = "Name";
        public readonly string TYPE = "Type";
        public readonly string STATUS = "Status";


        internal MembershipTable()
        {
            TableName = TABLENAME;
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(CID, typeof(Guid));
            Columns.Add(CLID, typeof(Guid));
            Columns.Add(CSID, typeof(Guid));
            Columns.Add(NAME, typeof(string));
            Columns.Add(TYPE, typeof(string));
            Columns.Add(STATUS, typeof(string));

        }
    }

    public class ParentTable : DataTable
    {
        private readonly string TABLENAME = "MembershipParent";
        public readonly string CLIENTID = "ClientID";
        public readonly string NAME = "Name";
        public readonly string TYPE = "Type";

         internal ParentTable()
        {
            TableName = TABLENAME;
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(NAME, typeof(string));
            Columns.Add(TYPE, typeof(string));
            

        }
         
    }
}
