﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class SettledUnsetteledDS : DataSet, IHasOrganizationUnit, IUMABaseDaa
    {
        public Guid OrderId { get; set; }
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public DatasetCommandTypes CommandTypeData
        {
            get { return CommandType; }
        }

        public OrganizationUnit Unit { get; set; }
        public int Command { get; set; }

        public SettledUnsetteledList SettledUnsetteledTable=new SettledUnsetteledList();

        public SettledUnsetteledDS()
        {
            Tables.Add(SettledUnsetteledTable);
        }
    }


    public class SettledUnsetteledList : DataTable
    {
        public const string TABLENAME = "SettledUnsetteledList";
        public readonly string ID = "ID";
        public readonly string CLIENTCID = "ClientCID";
        public readonly string CLIENTID = "ClientID";
        public readonly string ACCOUNTCID = "AccountCID";
        public readonly string ACCOUNTNUMBER = "AccountNumber";
        public readonly string PRODUCTID = "ProductID";
        public readonly string INVESTMENTCODE = "InvestmentCode";
        public readonly string ORDERID = "OrderID";
        public readonly string ORDERNO = "OrderNo";
        public readonly string ORDERITEMID = "OrderItemID";
        public readonly string AMOUNT = "Amount";
        public readonly string TYPE = "Type";
        public readonly string CREATEDDATE = "CreatedDate";
        public readonly string UPDATEDDATE = "UpdatedDate";
        public readonly string SETTLEDASSOCIATION = "SettledAssociation";
        public readonly string TRANSACTIONID = "TransactionID";
        public readonly string TRADETYPE= "TradeType";
        public readonly string TRADEDATE= "TradeDate";
        public readonly string ENTRYTYPE = "EntryType";
        public readonly string ACCOUNTBSB = "AccountBSB";
        public readonly string TRANSACTIONTYPE = "TransactionType";
        public readonly string ACCOUNTTYPE = "AccountType";
        public readonly string UNITS = "Units";
        public readonly string UNITPRICE = "UnitPrice";
        public readonly string PERCENTAGE = "Percentage";
        public readonly string BROKERID = "BrokerId";
        public readonly string BANKID = "BankId";
        public readonly string BROKERNAME = "BrokerName";
        public readonly string BANKNAME = "BankName";
        public readonly string DURATION = "Duration";
        public readonly string ORDERSTATUS = "OrderStatus";
        public readonly string ORDERTYPE = "OrderType";
        public readonly string CLIENTMANAGEMENTTYPE = "ClientManagementType";
        public readonly string ORDERCREATEDBY = "OrderCreatedBy";
        public readonly string ORDERPREFEREDBY = "OrderPreferedBy";
        public readonly string ORDERBANKACCOUNTTYPE = "OrderBankAccountType";
       
        internal SettledUnsetteledList()
        {
            TableName = TABLENAME;
            Columns.Add(ID, typeof(Guid));
            Columns.Add(CLIENTCID, typeof(Guid));
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(ACCOUNTCID, typeof(Guid));
            Columns.Add(ACCOUNTNUMBER, typeof(string));
            Columns.Add(ACCOUNTBSB, typeof(string));
            Columns.Add(PRODUCTID, typeof(Guid));
            Columns.Add(INVESTMENTCODE, typeof(string));
            Columns.Add(ORDERID, typeof(Guid));
            Columns.Add(ORDERNO, typeof(long));
            Columns.Add(ORDERITEMID, typeof(Guid));
            Columns.Add(AMOUNT, typeof(decimal));
            Columns.Add(UNITS, typeof(decimal));
            Columns.Add(UNITPRICE, typeof(decimal));
            Columns.Add(TYPE, typeof(string));
            Columns.Add(CREATEDDATE, typeof(DateTime));
            Columns.Add(UPDATEDDATE, typeof(DateTime));
            Columns.Add(SETTLEDASSOCIATION, typeof(Guid));
            Columns.Add(TRANSACTIONID, typeof(Guid));
            Columns.Add(TRADETYPE, typeof(string));
            Columns.Add(TRADEDATE, typeof(DateTime));
            Columns.Add(ENTRYTYPE, typeof(string));
            Columns.Add(TRANSACTIONTYPE, typeof(string));
            Columns.Add(ACCOUNTTYPE, typeof(string));
            Columns.Add(PERCENTAGE, typeof(decimal));
            Columns.Add(BROKERID, typeof(Guid));
            Columns.Add(BANKID, typeof(Guid));
            Columns.Add(DURATION, typeof(string));
            Columns.Add(ORDERSTATUS, typeof(string));
            Columns.Add(ORDERTYPE, typeof(string));
            Columns.Add(BROKERNAME, typeof(string));
            Columns.Add(BANKNAME, typeof(string));
            Columns.Add(CLIENTMANAGEMENTTYPE, typeof(string));
            Columns.Add(ORDERCREATEDBY, typeof(string));
            Columns.Add(ORDERPREFEREDBY, typeof(string));
            Columns.Add(ORDERBANKACCOUNTTYPE, typeof(string));
        }
    }
}
