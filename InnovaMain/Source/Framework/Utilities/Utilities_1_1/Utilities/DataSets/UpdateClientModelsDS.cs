﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{


    public class UpdateClientModelsDS : DataSet,IHasOrganizationUnit
    {
        public ModelDataSetOperationType ModelDataSetOperationType = ModelDataSetOperationType.None;
        public Guid ModelEntityID = Guid.Empty;
        public ModelEntity ModelEntity = null;

        public OrganizationUnit Unit
        {get; set; }

        public int Command{ get; set; }
    }
}
