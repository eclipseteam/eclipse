﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;

namespace Oritax.TaxSimp.DataSets
{
    public class PROSecuritiesDS : DataSet
    {
        public bool GetDetails = false;
        public Guid DetailsGUID = Guid.Empty; 

        public const string PROSECLISTTABLE = "PROSecurities";
        public const string ID = "ID";
        public const string NAME = "Name";
        public const string DESCRIPTION = "Description";

        public const string PROSECDETAILTABLE = "ProSecDetail";
        public const string DETAILID = "DetailId";
        public const string ALLOCATION = "Allocation";
        public const string BUYPRICE = "BuyPrice";
        public const string CURRENCY = "Currency";
        public const string COMMENTS = "Comments";
        public const string ISDEACTIVE = "IsDeactive";
        public const string MARKETCODE = "MarketCode";
        public const string PERCENTALLOCATION = "PercentageAllocation";
        public const string RATING = "Rating";
        public const string RATINGOPTIONS = "RatingOptions";
        public const string SELLPRICE = "SellPrice";

        public PROSecuritiesDS()
        {
            this.Tables.Add(CreateProSecuritiesListTable());
            this.Tables.Add(ProSecuritiesDetailsListTable());
        }

        public static DataTable CreateProSecuritiesListTable()
        {
            DataTable ProSecListTable = new DataTable(PROSECLISTTABLE);
            ProSecListTable.Columns.Add(ID, typeof(Guid));
            ProSecListTable.Columns.Add(NAME, typeof(string));
            ProSecListTable.Columns.Add(DESCRIPTION, typeof(string));

            return ProSecListTable;
        }

        public DataTable ProSecuritiesDetailsListTable()
        {
            DataTable ProSecListPriceHisTable = new DataTable(PROSECDETAILTABLE);
            ProSecListPriceHisTable.Columns.Add(NAME, typeof(string));
            ProSecListPriceHisTable.Columns.Add(DESCRIPTION, typeof(string));
            ProSecListPriceHisTable.Columns.Add(DETAILID, typeof(Guid));
            ProSecListPriceHisTable.Columns.Add(ALLOCATION, typeof(double));
            ProSecListPriceHisTable.Columns.Add(CURRENCY, typeof(string));
            ProSecListPriceHisTable.Columns.Add(BUYPRICE, typeof(double));
            ProSecListPriceHisTable.Columns.Add(COMMENTS, typeof(string));
            ProSecListPriceHisTable.Columns.Add(MARKETCODE, typeof(string));
            ProSecListPriceHisTable.Columns.Add(PERCENTALLOCATION, typeof(double));
            ProSecListPriceHisTable.Columns.Add(RATING, typeof(string));
            ProSecListPriceHisTable.Columns.Add(RATINGOPTIONS, typeof(string));
            ProSecListPriceHisTable.Columns.Add(SELLPRICE, typeof(double));
            ProSecListPriceHisTable.Columns.Add(ISDEACTIVE, typeof(bool));

            return ProSecListPriceHisTable;
        }
    }
}
