﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;
using System.Diagnostics;

namespace Oritax.TaxSimp.DataSets
{
    public class EventLogDS : DataSet
    {
        public int EventLogFilter;
        public Guid CIDFilter; 
    }
}
