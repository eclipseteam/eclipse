﻿using System;
using System.Collections.Generic;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class InstitutesDS : DataSet
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public InstitutionsTable InstitutionsTable = new InstitutionsTable();
      

        public InstitutesDS()
        {
            Tables.Add(InstitutionsTable);
        }
    }

    public class InstitutionsTable : DataTable
    {
        public readonly string TABLENAME = "InstitutionList";
        public readonly string ID = "Id";
        public readonly string NAME = "Name";
        public readonly string DESCRIPTION = "Description";
        public readonly string TRADINGNAME = "TradingName";
        public readonly string LEGALNAME = "LegalName";
        public readonly string CHKLEGALNAME = "ChkLegalName";
        public readonly string SPCREDITRATINGLONG = "SPCreditRatingLong";
        public readonly string SPCREDITRATINGSHORT = "SPCreditRatingShort";
        public readonly string TYPE = "Type";
        public readonly string ABN = "ABN";
        public readonly string ACN = "ACN";
        public readonly string TFN = "TFN";
        public readonly string INSTITUTIONCODE = "InstitutionCode";
        public readonly string FACSIMILECOUNTYCODE = "FacsimileCountryCode";
        public readonly string FACSIMILECITYCODE = "FacsimileCityCode";
        public readonly string FACSIMILE = "Facsimile";
        public readonly string PHONENUMBERCOUNTYCODE = "PhoneNumberCountryCode";
        public readonly string PHONENUMBERCITYCODE = "PhoneNumberCityCode";
        public readonly string PHONENUMBER = "PhoneNumber";
        public readonly string ADDRESSLINE1 = "Addressline1";
        public readonly string ADDRESSLINE2 = "Addressline2";
        public readonly string SUBURB = "Suburb";
        public readonly string POSTCODE = "PostCode";
        public readonly string STATE = "State";
        public readonly string COUNTRY = "Country";
        

        internal InstitutionsTable()
        {
            TableName = TABLENAME;
            Columns.Add(ID, typeof(Guid));
            Columns.Add(NAME, typeof(string));
            Columns.Add(DESCRIPTION, typeof(string));
            Columns.Add(TRADINGNAME, typeof(string));
            Columns.Add(LEGALNAME, typeof(string));
            Columns.Add(CHKLEGALNAME, typeof(bool));
            Columns.Add(SPCREDITRATINGLONG, typeof(int));
            Columns.Add(SPCREDITRATINGSHORT, typeof(int));
            Columns.Add(TYPE, typeof(int));
            Columns.Add(ABN, typeof(string));
            Columns.Add(ACN, typeof(string));
            Columns.Add(TFN, typeof(string));
            Columns.Add(INSTITUTIONCODE, typeof(string));
            Columns.Add(FACSIMILECOUNTYCODE, typeof(string));
            Columns.Add(FACSIMILECITYCODE, typeof(string));
            Columns.Add(FACSIMILE, typeof(string));
            Columns.Add(PHONENUMBERCOUNTYCODE, typeof(string));
            Columns.Add(PHONENUMBERCITYCODE, typeof(string));
            Columns.Add(PHONENUMBER, typeof(string));
            Columns.Add(ADDRESSLINE1, typeof(string));
            Columns.Add(ADDRESSLINE2, typeof(string));
            Columns.Add(SUBURB, typeof(string));
            Columns.Add(POSTCODE, typeof(string));
            Columns.Add(STATE, typeof(string));
            Columns.Add(COUNTRY, typeof(string));
            
        }
    }
}
