﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    

    public class ProductsDS : DataSet
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public const string PRODUCTSTABLE = "ProductsTable";
        public const string ID= "ID";
        public const string NAME = "Name";
        public const string DESCRIPTION = "Description";


        public const string BankAccountType = "BankAccountType";// As Acc/Fund code Combo
        public const string DynamicPercentage = "DynamicPercentage";
        
        public const string EntityId = "EntityId"; // As Institute/Broker Combo=CLID
        public const string EntityCsId = "EntityCsId"; // As Institute/Broker Combo=CSID

        public const string EntityType = "EntityType";// As (Org) Type code Combo
        public const string FundAccounts = "FundAccounts";
        //public const string IncludedAccounts = "IncludedAccounts";
        public const string IsDefaultProductSecurity = "IsDefaultProductSecurity";
        public const string MaxAllocation = "MaxAllocation";
        public const string MinAllocation = "MinAllocation";

        //public const string ParentId = "ParentId";
        public const string ProductAPIR = "ProductAPIR";
        public const string ProductISIN = "ProductISIN";
        public const string ProductSecuritiesId = "ProductSecuritiesId";
        public const string ProductType = "ProductType";// As Product Type Combo
        public const string SharePercentage = "SharePercentage";
        public const string TaskDescription = "TaskDescription";

        public ProductsDS()
        {
            DataTable tab = CreateProductsTable();

            this.Tables.Add(tab);

            //this.Tables.Add(InvestmentDS.InvestmentManagementTable());
        }

        public static DataTable CreateProductsTable()
        {
            DataTable tab = new DataTable(PRODUCTSTABLE);
            tab.Columns.Add(ID, typeof(Guid));
            tab.Columns.Add(NAME, typeof(string));
            tab.Columns.Add(DESCRIPTION, typeof(string));
            tab.Columns.Add(BankAccountType, typeof(string));
            tab.Columns.Add(DynamicPercentage, typeof(double));
            tab.Columns.Add(EntityId, typeof(Guid));
            tab.Columns.Add(EntityCsId, typeof(Guid));
            tab.Columns.Add(EntityType, typeof(string));
            tab.Columns.Add(FundAccounts, typeof(Guid));
            //tab.Columns.Add(IncludedAccounts, typeof(string));
            tab.Columns.Add(IsDefaultProductSecurity, typeof(bool));
            tab.Columns.Add(MaxAllocation, typeof(double));
            tab.Columns.Add(MinAllocation, typeof(double));
            // tab.Columns.Add(ParentId, typeof(Guid));
            tab.Columns.Add(ProductAPIR, typeof(string));
            tab.Columns.Add(ProductISIN, typeof(string));
            tab.Columns.Add(ProductSecuritiesId, typeof(Guid));
            tab.Columns.Add(ProductType, typeof(string));
            tab.Columns.Add(SharePercentage, typeof(double));
            tab.Columns.Add(TaskDescription, typeof(string));
            return tab;
        }


    }
}
