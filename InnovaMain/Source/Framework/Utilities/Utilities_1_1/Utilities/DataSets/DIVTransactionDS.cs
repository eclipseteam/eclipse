﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class DIVTransactionDS : BaseClientDetails, IHasOrganizationUnit
    {
        public const string ID = "ID";
        public const string CLIENTDISID = "ClientID";
        public const string PRODUCTTYPE = "ProductType";
        public const string TRANSACTIONTYPE  = "TransactionType";
        public const string RECORDDATE  = "RecordDate";
        public const string PAYMENTDATE  = "PaymentDate";
        public const string UNITSONHAND  = "UnitsOnHand";
        public const string CENTSPERSHARE  = "CentsPerShare";
        public const string INVESTMENTAMOUNT  = "InvestmentAmount";
        public const string PAIDDIVIDEND  = "PaidDividend";
        public const string INTERESTPAID  = "InterestPaid";
        public const string INTERESTRATE  = "InterestRate";
        public const string FRANKINGCREDITS  = "FrankingCredits";
        public const string TOTALFRANKINGCREDITS  = "TotalFrankingCredits";
        public const string FRANKEDAMOUNT  = "FrankedAmount";
        public const string UNFRANKEDAMOUNT  = "UnfrankedAmount";
        public const string BALANCEDATE  = "BalanceDate";
        public const string BOOKSCLOSEDATE  = "BooksCloseDate";
        public const string DIVIDENDTYPE  = "DividendType";
        public const string CURRENCY  = "Currency";
        public const string STATUS = "Status";
        public const string MANUALLY_ADJUSTED = "ManuallyAdjusted";
        public const string INCOMESTRANSACTIONTABLE = "Income Transactions";
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
        public string CLIENTIDForm = string.Empty;
        public string CLIENTNAMEForm = string.Empty;

        private DataTable GetIncomeTransactionTable()
        {
            DataTable dt = new DataTable(INCOMESTRANSACTIONTABLE);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(CLIENTDISID, typeof(string));
            dt.Columns.Add(INVESTMENTCODE);
            dt.Columns.Add(PRODUCTTYPE);
            dt.Columns.Add(TRANSACTIONTYPE);
            dt.Columns.Add(RECORDDATE, typeof(DateTime));
            dt.Columns.Add(PAYMENTDATE, typeof(DateTime));
            dt.Columns.Add(UNITSONHAND, typeof(decimal));
            dt.Columns.Add(CENTSPERSHARE, typeof(decimal));
            dt.Columns.Add(INVESTMENTAMOUNT, typeof(decimal));
            dt.Columns.Add(PAIDDIVIDEND, typeof(decimal));
            dt.Columns.Add(INTERESTPAID, typeof(decimal));
            dt.Columns.Add(INTERESTRATE, typeof(decimal));
            dt.Columns.Add(FRANKINGCREDITS, typeof(double));
            dt.Columns.Add(TOTALFRANKINGCREDITS, typeof(double));
            dt.Columns.Add(FRANKEDAMOUNT, typeof(double));
            dt.Columns.Add(UNFRANKEDAMOUNT, typeof(double));
            dt.Columns.Add(BALANCEDATE, typeof(DateTime));
            dt.Columns.Add(BOOKSCLOSEDATE, typeof(DateTime));
            dt.Columns.Add(DIVIDENDTYPE, typeof(string));
            dt.Columns.Add(CURRENCY, typeof(string));
            dt.Columns.Add(STATUS, typeof(string));
            dt.Columns.Add(BGLCODE, typeof(string));
            dt.Columns.Add(MANUALLY_ADJUSTED, typeof(string));
            return dt;
        }

        public DIVTransactionDS()
        {
            this.Tables.Add(GetIncomeTransactionTable());
            this.Tables.Add(this.GetIncomeTransactionMapTable());
        }
    }
}
