﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    

    public class AssetDS : DataSet
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;        
        public const string TABLENAME = "AssetTable";
        public const string ID= "ID";
        public const string NAME= "Name";
        public const string DESCRIPTION = "Description";


        public AssetDS()
        {
            DataTable tab = new DataTable(TABLENAME);
            tab.Columns.Add(ID, typeof(Guid));            
            tab.Columns.Add(NAME, typeof(string));
            tab.Columns.Add(DESCRIPTION, typeof(string));            
            this.Tables.Add(tab);            
        }


    }
}
