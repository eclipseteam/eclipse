﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.DataSets
{
    public partial class LossAndGainsReportDS : BaseClientDetails
    {
        private CostTypes costTypes = CostTypes.FIFO;

        public CostTypes CostTypes
        {
            get { return costTypes; }
            set { costTypes = value; }
        }

        private AcountingMethodType acountingMethod = AcountingMethodType.Accrual;

        public AcountingMethodType AcountingMethod
        {
            get { return acountingMethod; }
            set { acountingMethod = value; }
        }


        public static string TAXREPORTINGMATRIXTABLE = "TaxReportMatrixTable";
        public static string OPENINGBALANCE = "OpeningBalance";
        public static string MOVEMENT = "Movement";
        public static string INTEREST = "Interest";
        public static string RENTAL = "Rental";
        public static string DIVIDEND = "Dividend";
        public static string DISTRIBUTION = "Distribution";
        public static string DISTRIBUTIONADJ = "DistributionAdj";
        public static string REALISED = "Realised";
        public static string UNREALISED = "UnRealised";
        public static string INCOME = "Income";
        public static string EXPENSE = "EXPENSE";
        public static string TRANSFERIN = "TransferIn";
        public static string TRANSFEROUT = "TransferOut";
        public static string EXPENSES = "Expenses";
        public static string CLOSINGBALANCE = "ClosingBalance";
        public static string CLOSINGADJBALANCE = "ClosingAdjBalance";

        public static string ID = "ID";

        public static string BANKSTRANSACTIONDETAILS = "BankTransactionDetailsTable";
        public static string SYSTRANSACIONTYPE = "SysTransactionType";
        public static string TRANSACTIONCATEGORY = "TransactionCategory";

        public static string GAINSLOSSESSUMMARYTABLE = "GainsLossesSuammryTable";
        public static string ACCOUNTNAME = "AccountName";
        public static string ACCOUNTNO = "AccountNO";
        public static string ACCOUNTTYPE = "AccountType";
        public static string TRANSACTIONDATE = "TranDate";

        public static string SECURITYSUMMARYTABLE = "SecuritySummary";
        public static string SECURITYSUMMARYTABLEREALISED = "SecuritySummaryRealised";
        public static string SECURITYSUMMARYTABLEUNREALISED = "SecuritySummaryUNRealised";
        public static string SECNAME = "SecurityName";
        public static string ACCNO = "AccNo";
        public static string DESCRIPTION = "Description";
        public static string OPENINGBALUNITS = "OpeningBalanceUnits";
        public static string MOVEMENTUNITS = "MovementUnits";
        public static string CLOSINGBALUNITS = "ClosingBalanceUnits";
        public static string UNITPRICE = "UnitPrice";
        public static string PRICEATDATE = "PriceAtDate";
        public static string TOTAL = "Total";
        public static string FEE = "Fee";
        public static string TRADEVALUE = "TradeValue";
        public static string INVESTMENT = "INVESTMENT";
        public static string CHANGEINMKT = "ChangeInMktVal";
        public static string SECTYPE = "SECTYPE";
        public static string RECORDDATE = "RECORDDATE";
        public static string PAYMENTDATE = "PAYMENTDATE";
        public static string DIVTYPE = "DIVTYPE";
        public static string DIVAMOUNTPERSHARE = "DIVAMOUNTPERSHARE";
        public static string INCPAID = "INCPAID";
        public static string FRANKEDAMOUNT = "FRANKEDAMOUNT";
        public static string UNFRANKEDAMOUNT = "UNFRANKEDAMOUNT";
        public static string FRANKINGCREDIT = "FRANKINGCREDIT";
        public static string UNITSHELD = "UNITSHELD";
        public static string INCOMETYPE = "INCOMETYPE";
        public static string EXPENSETYPE = "EXPENSETYPE";
        public static string EXPENSESUBTYPE = "EXPENSESUBTYPE";
        public static string SOURCE = "SOURCE";

        public static string TRANSACTIONTYPE = "TRANSACTIONTYPE";
        public static string SYSTEMTRANSACTIONTYPE = "SYSTEMTRANSACTIONTYPE";
        public static string TRANCATEGORY = "TRANCATEGORY";
        public static string GST = "GST";
        public static string BSB = "BSB";
        public static string COMMENT = "COMMENT";

        public static string CASH_BANKACC = "CASH_BANKACC";
        public static string CASH_AMOUNT = "CASH_AMOUNT";
        public static string CASH_PAYMENTDATE = "CASH_PAYMENTDATE";
        public static string CASH_INCOMETYPE = "CASH_INCOMETYPE";

        public static string CURRENTUNITS = "CurrentUnits";
        public static string SOLDUNITS = "SoldUnits";
        public static string SALENETVALUE = "SaleNetVaue";
        public static string REALISEDCOSTPRICE = "RealisedCostPrice";
        public static string REALISEDCOSTVALUE = "RealisedCostValue";
        public static string REALISEDPROFITLOSS = "RealisedProfitLoss";
        public static string SALESPROCEED = "SalesProceed";
        public static string ISMATCHED = "ISMATCHED";
        public static string UNREALISEDCOSTPRICE = "UnrealisedCostPrice";
        public static string UNREALISEDCOSTVALUE = "UnrealisedCostValue";
        public static string UNREALISEDPROFITLOSS = "UnrealisedProfitLoss";
        public static string NETDPU = "NETDPU";
        public static string INCOMEBREAKDOWN = "INCOMEBREAKDOWN";
        public static string EXPENSEBREAKDOWN = "EXPENSEBREAKDOWN";

        public DateTime StartDate;
        public DateTime EndDate;

        public DataTable IncomeBreakDown()
        {
            DataTable dt = new DataTable(INCOMEBREAKDOWN);
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(SECNAME, typeof(string));
            dt.Columns.Add(SECTYPE, typeof(string));
            dt.Columns.Add(RECORDDATE, typeof(DateTime));
            dt.Columns.Add(PAYMENTDATE, typeof(DateTime));
            dt.Columns.Add(DIVTYPE, typeof(string));
            dt.Columns.Add(INCOMETYPE, typeof(string));
            dt.Columns.Add(SOURCE, typeof(string));
            dt.Columns.Add(CASH_BANKACC, typeof(string));
            dt.Columns.Add(NETDPU, typeof(decimal));
            dt.Columns.Add(CASH_INCOMETYPE, typeof(string));
            dt.Columns.Add(CASH_AMOUNT, typeof(decimal));
            dt.Columns.Add(CASH_PAYMENTDATE, typeof(DateTime));
            dt.Columns.Add(DIVAMOUNTPERSHARE, typeof(decimal));
            dt.Columns.Add(FRANKEDAMOUNT, typeof(decimal));
            dt.Columns.Add(UNFRANKEDAMOUNT, typeof(decimal));
            dt.Columns.Add(FRANKINGCREDIT, typeof(decimal));
            dt.Columns.Add(INCPAID, typeof(decimal));
            dt.Columns.Add(UNITSHELD, typeof(decimal));

            return dt;
        }

        public DataTable ExpenseBreakDown()
        {
            DataTable dt = new DataTable(EXPENSEBREAKDOWN);
            dt.Columns.Add(BSB, typeof(string));
            dt.Columns.Add(CASH_BANKACC, typeof(string));
            dt.Columns.Add(SECTYPE, typeof(string));
            dt.Columns.Add(SOURCE, typeof(string));
            dt.Columns.Add(PAYMENTDATE, typeof(DateTime));
            dt.Columns.Add(TRANCATEGORY, typeof(string));
            dt.Columns.Add(SYSTEMTRANSACTIONTYPE, typeof(string));
            dt.Columns.Add(TRANSACTIONTYPE, typeof(string));
            dt.Columns.Add(EXPENSE, typeof(decimal));
            dt.Columns.Add(GST, typeof(decimal));
            dt.Columns.Add(TOTAL, typeof(decimal));
            dt.Columns.Add(COMMENT, typeof(string));
            return dt;
        }

        public LossAndGainsReportDS()
        {
            AccountingFinancialYear financialYear = new AccountingFinancialYear(DateTime.Now);

            this.StartDate = financialYear.FinYearStartDate;
            this.EndDate = financialYear.FinYearEndDate;
            this.Tables.Add(SecuritySummaryTable());
            this.Tables.Add(SecuritySummaryTableRealised());
            this.Tables.Add(SecuritySummaryTableUnRealised());
            this.Tables.Add(IncomeBreakDown());
            this.Tables.Add(this.GainsLossesSummaryTableOverall());
            this.Tables.Add(this.TaxSummaryTable());
            this.Tables.Add(ExpenseBreakDown());

            DataTable dt = new DataTable(BANKSTRANSACTIONDETAILS);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(SYSTRANSACIONTYPE, typeof(string));
            dt.Columns.Add(TRANSACTIONCATEGORY, typeof(string));
            dt.Columns.Add(TRANSACTIONDATE, typeof(DateTime));
            dt.Columns.Add(ACCOUNTNAME, typeof(string));
            dt.Columns.Add(ACCOUNTNO, typeof(string));
            dt.Columns.Add(ACCOUNTTYPE, typeof(string));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(ISMATCHED, typeof(string));
            dt.Columns.Add(TOTAL, typeof(decimal));
            this.Tables.Add(dt);

        }

        public DataTable SecuritySummaryTable()
        {
            DataTable dt = new DataTable(SECURITYSUMMARYTABLE);
            dt.Columns.Add(SECNAME, typeof(string));
            dt.Columns.Add(ACCNO, typeof(string));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(OPENINGBALUNITS, typeof(decimal));
            dt.Columns.Add(MOVEMENTUNITS, typeof(decimal));
            dt.Columns.Add(CLOSINGBALUNITS, typeof(decimal));
            dt.Columns.Add(UNITPRICE, typeof(decimal));
            dt.Columns.Add(TOTAL, typeof(decimal));
            dt.Columns.Add(UNREALISEDCOSTPRICE, typeof(decimal));
            dt.Columns.Add(UNREALISEDCOSTVALUE, typeof(decimal));
            dt.Columns.Add(UNREALISEDPROFITLOSS, typeof(decimal));
            dt.Columns.Add(REALISEDCOSTPRICE, typeof(decimal));
            dt.Columns.Add(REALISEDCOSTVALUE, typeof(decimal));
            dt.Columns.Add(REALISEDPROFITLOSS, typeof(decimal));
            dt.Columns.Add(FEE, typeof(decimal));
            dt.Columns.Add(TRADEVALUE, typeof(decimal));
            dt.Columns.Add(PRICEATDATE, typeof(DateTime));

            return dt;
        }

        public DataTable SecuritySummaryTableRealised()
        {
            DataTable dt = new DataTable(SECURITYSUMMARYTABLEREALISED);
            dt.Columns.Add(SECNAME, typeof(string));
            dt.Columns.Add(ACCNO, typeof(string));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(SOLDUNITS, typeof(decimal));
            dt.Columns.Add(UNITPRICE, typeof(decimal));
            dt.Columns.Add(SALESPROCEED, typeof(decimal));
            dt.Columns.Add(REALISEDCOSTPRICE, typeof(decimal));
            dt.Columns.Add(REALISEDCOSTVALUE, typeof(decimal));
            dt.Columns.Add(REALISEDPROFITLOSS, typeof(decimal));
            dt.Columns.Add(FEE, typeof(decimal));
            dt.Columns.Add(TRADEVALUE, typeof(decimal));
            dt.Columns.Add(PRICEATDATE, typeof(DateTime));
            return dt;
        }

        public DataTable SecuritySummaryTableUnRealised()
        {
            DataTable dt = new DataTable(SECURITYSUMMARYTABLEUNREALISED);
            dt.Columns.Add(SECNAME, typeof(string));
            dt.Columns.Add(ACCNO, typeof(string));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(OPENINGBALUNITS, typeof(decimal));
            dt.Columns.Add(MOVEMENTUNITS, typeof(decimal));
            dt.Columns.Add(CLOSINGBALUNITS, typeof(decimal));
            dt.Columns.Add(UNITPRICE, typeof(decimal));
            dt.Columns.Add(TOTAL, typeof(decimal));
            dt.Columns.Add(UNREALISEDCOSTPRICE, typeof(decimal));
            dt.Columns.Add(UNREALISEDCOSTVALUE, typeof(decimal));
            dt.Columns.Add(UNREALISEDPROFITLOSS, typeof(decimal));
            dt.Columns.Add(FEE, typeof(decimal));
            dt.Columns.Add(TRADEVALUE, typeof(decimal));
            dt.Columns.Add(PRICEATDATE, typeof(DateTime));
            return dt;
        }

        public DataTable TaxSummaryTable()
        {
            DataTable dt = new DataTable(TAXREPORTINGMATRIXTABLE);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(ACCOUNTNAME, typeof(string));
            dt.Columns.Add(ACCOUNTNO, typeof(string));
            dt.Columns.Add(ACCOUNTTYPE, typeof(string));
            dt.Columns.Add(OPENINGBALANCE, typeof(decimal));
            dt.Columns.Add(INVESTMENT, typeof(decimal));
            dt.Columns.Add(TRANSFERIN, typeof(decimal));
            dt.Columns.Add(TRANSFEROUT, typeof(decimal));
            dt.Columns.Add(MOVEMENT, typeof(decimal));
            dt.Columns.Add(INTEREST, typeof(decimal));
            dt.Columns.Add(RENTAL, typeof(decimal));
            dt.Columns.Add(DIVIDEND, typeof(decimal));
            dt.Columns.Add(DISTRIBUTION, typeof(decimal));
            dt.Columns.Add(DISTRIBUTIONADJ, typeof(decimal));
            dt.Columns.Add(INCOME, typeof(decimal));
            dt.Columns.Add(EXPENSES, typeof(decimal));
            dt.Columns.Add(CHANGEINMKT, typeof(decimal));
            dt.Columns.Add(REALISED, typeof(decimal));
            dt.Columns.Add(UNREALISED, typeof(decimal));
            dt.Columns.Add(CLOSINGBALANCE, typeof(decimal));
            dt.Columns.Add(CLOSINGADJBALANCE, typeof(decimal));
            return dt;
        }

        public DataTable GainsLossesSummaryTableOverall()
        {
            DataTable dt = new DataTable(GAINSLOSSESSUMMARYTABLE);
            dt.Columns.Add(ACCOUNTNAME, typeof(string));
            dt.Columns.Add(ACCOUNTNO, typeof(string));
            dt.Columns.Add(ACCOUNTTYPE, typeof(string));
            dt.Columns.Add(REALISED, typeof(decimal));
            dt.Columns.Add(UNREALISED, typeof(decimal));
            return dt;
        }

        public string DateInfo()
        {
            return this.StartDate.ToString("dd/MM/yyyy") + " - " + this.EndDate.ToString("dd/MM/yyyy");
        }

        public void AddTaxSummaryRow(Guid ID, string accType, string accoutName, string accountNO, decimal openingBal, decimal movement, decimal interest, decimal rental, decimal distribution, decimal dividend, decimal income, decimal transferIn,
                decimal transferOut, decimal expenses, decimal realised, decimal unrealised, decimal closingBal, decimal investment, decimal closingBalanceWithAjd, decimal distributionAdj)
        {
            DataRow secSumaryRow = this.Tables[TAXREPORTINGMATRIXTABLE].NewRow();
            secSumaryRow[LossAndGainsReportDS.ID] = ID;
            secSumaryRow[LossAndGainsReportDS.ACCOUNTNAME] = accoutName;
            secSumaryRow[LossAndGainsReportDS.ACCOUNTNO] = accountNO;
            secSumaryRow[LossAndGainsReportDS.ACCOUNTTYPE] = accType;
            secSumaryRow[LossAndGainsReportDS.OPENINGBALANCE] = openingBal;
            secSumaryRow[LossAndGainsReportDS.INVESTMENT] = investment + movement;
            secSumaryRow[LossAndGainsReportDS.INTEREST] = interest;
            secSumaryRow[LossAndGainsReportDS.RENTAL] = rental;
            secSumaryRow[LossAndGainsReportDS.DISTRIBUTION] = distribution;
            secSumaryRow[LossAndGainsReportDS.DISTRIBUTIONADJ] = distributionAdj;
            secSumaryRow[LossAndGainsReportDS.DIVIDEND] = dividend;
            secSumaryRow[LossAndGainsReportDS.INCOME] = income;
            secSumaryRow[LossAndGainsReportDS.TRANSFERIN] = transferIn;
            secSumaryRow[LossAndGainsReportDS.TRANSFEROUT] = transferOut;
            secSumaryRow[LossAndGainsReportDS.EXPENSES] = expenses;
            secSumaryRow[LossAndGainsReportDS.REALISED] = realised;
            secSumaryRow[LossAndGainsReportDS.UNREALISED] = unrealised;
            secSumaryRow[LossAndGainsReportDS.CLOSINGBALANCE] = closingBal;
            secSumaryRow[LossAndGainsReportDS.CLOSINGADJBALANCE] = closingBalanceWithAjd;


            secSumaryRow[LossAndGainsReportDS.CHANGEINMKT] = closingBal - (openingBal + investment + movement + interest + rental + distribution + dividend
                                                                           + income + transferIn + transferOut + expenses);
            this.Tables[TAXREPORTINGMATRIXTABLE].Rows.Add(secSumaryRow);
        }

        public void AddTaxSummaryRowMissingDistributions(Guid ID, string accType, string accoutName, string accountNO, decimal distribution)
        {
            DataRow secSumaryRow = this.Tables[TAXREPORTINGMATRIXTABLE].NewRow();
            secSumaryRow[LossAndGainsReportDS.ID] = ID;
            secSumaryRow[LossAndGainsReportDS.ACCOUNTNAME] = accoutName;
            secSumaryRow[LossAndGainsReportDS.ACCOUNTNO] = accountNO;
            secSumaryRow[LossAndGainsReportDS.ACCOUNTTYPE] = accType;
            secSumaryRow[LossAndGainsReportDS.DISTRIBUTION] = distribution;
            secSumaryRow[LossAndGainsReportDS.DISTRIBUTIONADJ] = distribution;
            secSumaryRow[LossAndGainsReportDS.CLOSINGBALANCE] = distribution;
            secSumaryRow[LossAndGainsReportDS.CLOSINGADJBALANCE] = distribution;
            this.Tables[TAXREPORTINGMATRIXTABLE].Rows.Add(secSumaryRow);
        }

        public void AddSecurityMainSummaryRow(string accoutName, string accountNO, decimal realised, decimal unrealised, string accType)
        {
            DataRow secSumaryRow = this.Tables[GAINSLOSSESSUMMARYTABLE].NewRow();
            secSumaryRow[LossAndGainsReportDS.ACCOUNTNAME] = accoutName;
            secSumaryRow[LossAndGainsReportDS.ACCOUNTNO] = accountNO;
            secSumaryRow[LossAndGainsReportDS.ACCOUNTTYPE] = accType;
            secSumaryRow[LossAndGainsReportDS.REALISED] = realised;
            secSumaryRow[LossAndGainsReportDS.UNREALISED] = unrealised;
            this.Tables[GAINSLOSSESSUMMARYTABLE].Rows.Add(secSumaryRow);
        }

        public void AddSecuritySummaryRow(string securityName, decimal fee, decimal tradeValue, string description, decimal openingBalanceUnits, decimal movementUnits, decimal closingBalanceUnits, decimal unitPrice, decimal totalTotal, decimal unrealisedcostPrice, decimal unrealisedcostValue, decimal unrealisedProfitLoss, string accNo)
        {
            DataRow secSumaryRow = this.Tables[SECURITYSUMMARYTABLEUNREALISED].NewRow();

            secSumaryRow[LossAndGainsReportDS.SECNAME] = securityName;
            secSumaryRow[LossAndGainsReportDS.ACCNO] = accNo;
            secSumaryRow[LossAndGainsReportDS.DESCRIPTION] = description;
            secSumaryRow[LossAndGainsReportDS.OPENINGBALUNITS] = openingBalanceUnits;
            secSumaryRow[LossAndGainsReportDS.MOVEMENTUNITS] = movementUnits;
            secSumaryRow[LossAndGainsReportDS.CLOSINGBALUNITS] = closingBalanceUnits;
            secSumaryRow[LossAndGainsReportDS.UNITPRICE] = unitPrice;
            secSumaryRow[LossAndGainsReportDS.TOTAL] = totalTotal;
            secSumaryRow[LossAndGainsReportDS.PRICEATDATE] = this.EndDate;
            secSumaryRow[LossAndGainsReportDS.UNREALISEDCOSTPRICE] = unrealisedcostPrice;
            secSumaryRow[LossAndGainsReportDS.UNREALISEDCOSTVALUE] = unrealisedcostValue;
            secSumaryRow[LossAndGainsReportDS.UNREALISEDPROFITLOSS] = unrealisedProfitLoss;
            secSumaryRow[LossAndGainsReportDS.FEE] = fee;
            secSumaryRow[LossAndGainsReportDS.TRADEVALUE] = tradeValue;
            this.Tables[SECURITYSUMMARYTABLEUNREALISED].Rows.Add(secSumaryRow);
        }

        public void AddSecuritySummaryRowRealised(string securityName, decimal fee, decimal tradeValue, string description, decimal soldUnits, decimal unitPrice, decimal totalTotal, decimal realisedcostPrice, decimal realisedcostValue, decimal realisedProfitLoss, string accNo)
        {
            DataRow secSumaryRow = this.Tables[SECURITYSUMMARYTABLEREALISED].NewRow();

            secSumaryRow[LossAndGainsReportDS.SECNAME] = securityName;
            secSumaryRow[LossAndGainsReportDS.ACCNO] = accNo;
            secSumaryRow[LossAndGainsReportDS.DESCRIPTION] = description;
            secSumaryRow[LossAndGainsReportDS.SOLDUNITS] = soldUnits;
            secSumaryRow[LossAndGainsReportDS.UNITPRICE] = unitPrice;
            secSumaryRow[LossAndGainsReportDS.SALESPROCEED] = totalTotal;
            secSumaryRow[LossAndGainsReportDS.PRICEATDATE] = this.EndDate;
            secSumaryRow[LossAndGainsReportDS.REALISEDCOSTPRICE] = realisedcostPrice;
            secSumaryRow[LossAndGainsReportDS.REALISEDCOSTVALUE] = realisedcostValue;
            secSumaryRow[LossAndGainsReportDS.REALISEDPROFITLOSS] = realisedProfitLoss;
            secSumaryRow[LossAndGainsReportDS.FEE] = fee;
            secSumaryRow[LossAndGainsReportDS.TRADEVALUE] = tradeValue;
            this.Tables[SECURITYSUMMARYTABLEREALISED].Rows.Add(secSumaryRow);
        }

        public void AddSecuritySummaryRowBankTransaction(string securityName, string description, decimal totalUnits, decimal unitPrice, DateTime priceAtDate, decimal totalTotal)
        {
            DataRow secSumaryRow = this.Tables[SECURITYSUMMARYTABLE].NewRow();

            secSumaryRow[LossAndGainsReportDS.SECNAME] = securityName;
            secSumaryRow[LossAndGainsReportDS.DESCRIPTION] = description;
            secSumaryRow[LossAndGainsReportDS.TOTAL] = totalTotal;
            secSumaryRow[LossAndGainsReportDS.PRICEATDATE] = priceAtDate;
            this.Tables[SECURITYSUMMARYTABLE].Rows.Add(secSumaryRow);
        }

    }
}
