﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public enum ExportMode
    {
        StateStreet = 0,
        BankWest = 1,
        DesktopBroker = 2,
    }

    public class ExportStatusDS : DataSet, IHasOrganizationUnit
    {
        public OrganizationUnit Unit { get; set; }
        public int Command { get; set; }
        public ExportMode ExportMode { get; set; }
        public ExportStatusTable ExportStatusTable = new ExportStatusTable();

        public ExportStatusDS()
        {
            Tables.Add(ExportStatusTable);
        }
    }

    public class ExportStatusTable : DataTable
    {
        public readonly string TABLENAME = "ExportStatusList";
        public readonly string CID = "Cid";
        public readonly string CLID = "Clid";
        public readonly string CSID = "Csid";
        public readonly string CLIENTID = "ClientID";
        public readonly string TRADINGNAME = "TradingName";
        public readonly string TYPE = "Type";
        public readonly string STATUS = "Status";
        public readonly string APPLICATIONDATE = "ApplicationDate";
        public readonly string APPLICATIONID = "ApplicationID";
        public readonly string SSEXPORT = "SSExport";
        public readonly string BWEXPORT = "BWExport";
        public readonly string DBEXPORT = "DBExport";

        internal ExportStatusTable()
        {
            TableName = TABLENAME;
            Columns.Add(CID, typeof(Guid));
            Columns.Add(CLID, typeof(Guid));
            Columns.Add(CSID, typeof(Guid));
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(TRADINGNAME, typeof(string));
            Columns.Add(TYPE, typeof(string));
            Columns.Add(STATUS, typeof(string));
            Columns.Add(APPLICATIONDATE, typeof(string));
            Columns.Add(APPLICATIONID, typeof(string));
            Columns.Add(SSEXPORT, typeof(bool));
            Columns.Add(BWEXPORT, typeof(bool));
            Columns.Add(DBEXPORT, typeof(bool));
        }
    }
}
