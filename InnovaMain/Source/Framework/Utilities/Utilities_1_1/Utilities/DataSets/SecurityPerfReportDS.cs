﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public partial class SecurityPerfReportDS : DataSet
    {
        public static string OVERALLPERFTABLE = "PerformanceTableOverall";
        public static string DIFMPERFTABLE = "PerformanceTableDIFM";
        public static string DIWMPERFTABLE = "PerformanceTableDIWM";
        public static string DIYPERFTABLE = "PerformanceTableDIY";
        public static string MANPERFTABLE = "PerformanceTableMAN";

        public static string ASSESTDESC = "AssetDesc";
        public static string CHAINLINKDATA = "ChainLinkData";
       
        public static string PERFSUMMARYTABLE = "PerfSummaryTable";
        public static string ONEMONTH = "1Month";
        public static string THREEMONTH = "3Month";
        public static string SIXMONTH = "6Month";
        public static string TWELVEMONTH = "12Month";
        public static string ONEYEAR = "1YEAR";
        public static string TWOYEAR = "2YEAR";
        public static string THREEYEAR = "3YEAR";
        public static string FIVEYEAR = "5YEAR";

        public SecurityPerfReportDS()
        {
            //PerformanceDetailTable(OVERALLPERFTABLE);
            //PerformanceDetailTable(DIYPERFTABLE);
            //PerformanceDetailTable(DIFMPERFTABLE);
            //PerformanceDetailTable(DIWMPERFTABLE);
            //PerformanceDetailTable(MANPERFTABLE);
            PerformanceSummaryTable();
        }

        private void PerformanceDetailTable(string tablename)
        {
        //    DataTable PerformanceTable = new DataTable(tablename);
        //    PerformanceTable.Columns.Add(SecurityPerfReportDS.MONTH, typeof(DateTime));
        //    PerformanceTable.Columns.Add(SecurityPerfReportDS.ASSESTDESC, typeof(string));
        //    PerformanceTable.Columns.Add(SecurityPerfReportDS.OPENINGBAL, typeof(decimal));
        //    PerformanceTable.Columns.Add(SecurityPerfReportDS.CLOSINGBALANCE, typeof(decimal));
        //    PerformanceTable.Columns.Add(SecurityPerfReportDS.SUMWEIGHTCF, typeof(decimal));
        //    PerformanceTable.Columns.Add(SecurityPerfReportDS.SUMCF, typeof(decimal));
        //    PerformanceTable.Columns.Add(SecurityPerfReportDS.MODDIETZ, typeof(decimal));
        //    PerformanceTable.Columns.Add(SecurityPerfReportDS.CHAINLINKDATA, typeof(decimal));
        //    this.Tables.Add(PerformanceTable);
        }

        private void PerformanceSummaryTable()
        {
            DataTable PerformanceTable = new DataTable(PERFSUMMARYTABLE);
            PerformanceTable.Columns.Add(PerfReportDS.DESCRIPTION, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.ONEMONTH, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.THREEMONTH, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.SIXMONTH, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.TWELVEMONTH, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.ONEYEAR, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.TWOYEAR, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.THREEYEAR, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.FIVEYEAR, typeof(string));

            this.Tables.Add(PerformanceTable);
        }
    }
}
