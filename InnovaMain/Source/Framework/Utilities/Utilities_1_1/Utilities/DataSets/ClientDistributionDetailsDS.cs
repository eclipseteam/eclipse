﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class ClientDistributionDetailsDS : DataSet
    {
        public Guid ID = Guid.Empty;
        public Guid Cid = Guid.Empty;
        public string ClientID=string.Empty;
        public double UnitsHeld = 0; 

        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public DistributionIncomeEntity Entity { get; set; }
        public ClientDistributionDetailsDS()
        {
        }
    }
}
