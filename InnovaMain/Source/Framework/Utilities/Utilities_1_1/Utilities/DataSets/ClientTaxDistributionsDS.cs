﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.DataSets
{
    public class ClientTaxDistributionsDS : BaseClientDetails, IHasOrganizationUnit
    {
        public const string CLIENTDISTRIBUTIONTABLE = "ClientDistributionsTable";
        public const string DISTAXTABLE = "DisTaxTable";

        public const string LABELORDERNO = "LABELORDERNO";
        public const string TAXLABEL = "TAXLABEL";
        public const string TAXLABELDESC = "TAXLABELDESC";
        public const string AMOUNT = "AMOUNT";

        public const string FUNDCODE = "FUNDCODE";
        public const string FUNDNAME = "FUNDNAME";
        public const string STARTDATE = "STARTDATE";
        public const string ENDDATE = "ENDDATE";

        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        
        public string CLIENTIDForm = string.Empty;
        public string CLIENTNAMEForm = string.Empty;


        public ObservableCollection<DistributionIncomeEntity> DistributionIncomeEntityList { get; set; }

        public ClientTaxDistributionsDS()
        {
            DataTable dt = new DataTable(DISTAXTABLE);
            dt.Columns.Add(LABELORDERNO, typeof(int));
            dt.Columns.Add(TAXLABEL, typeof(string));
            dt.Columns.Add(TAXLABELDESC, typeof(string));
            dt.Columns.Add(AMOUNT, typeof(double));
            dt.Columns.Add(FUNDCODE, typeof(string));
            dt.Columns.Add(FUNDNAME, typeof(string));
            dt.Columns.Add(STARTDATE, typeof(DateTime));
            dt.Columns.Add(ENDDATE, typeof(DateTime));

            this.Tables.Add(dt); 
        }


        public OrganizationUnit Unit { get; set; }
        public int Command { get; set; }
    }
  
}
