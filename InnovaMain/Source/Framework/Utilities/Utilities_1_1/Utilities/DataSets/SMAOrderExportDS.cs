﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class SMAOrderExportDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public object Data { get; set; }
        public OrganizationUnit Unit { get; set; }
        public int Command { get; set; }
        public string OutputData { get; set; }
        public SMAOperationType Operation { get; set; }
    }
}
