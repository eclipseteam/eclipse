﻿using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class P2ReportsDS : DataSet
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public P2ReportDetails P2ReportDetailsTable = new P2ReportDetails();

        public P2ReportsDS()
        {
            Tables.Add(P2ReportDetailsTable);
        }
    }

    public class P2ReportDetails : DataTable
    {
        public readonly string TABLENAME = "P2ReportDetails";
        public readonly string ATTACHEDBMCID = "AttachedBMCID";
        public readonly string YEAR = "Year";
        public readonly string QUARTER = "Quarter";
        public readonly string P2TYPE = "P2Type";

        internal P2ReportDetails()
        {
            TableName = TABLENAME;
            Columns.Add(ATTACHEDBMCID, typeof(string));
            Columns.Add(YEAR, typeof(int));
            Columns.Add(QUARTER, typeof(string));
            Columns.Add(P2TYPE, typeof(string));
        }
    }
}