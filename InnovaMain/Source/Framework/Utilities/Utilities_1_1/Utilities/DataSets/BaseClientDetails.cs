﻿using System;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Oritax.TaxSimp.Data;
using System.Collections.Generic;

namespace Oritax.TaxSimp.DataSets
{
    public class BaseClientDetails : UMABaseDS
    {
        public static string CLIENTSUMMARYTABLE = "ClientSummaryTable";
        public static string CLIENTCID = "ClientCID";
        public static string CLIENTID = "ClientID";
        public static string ECLIPSECLIENTID = "EclipseClientID";
        public static string CLIENTNAME = "ClientName";
        public static string CIENTABN = "ClientABN";
        public static string CLIENTACN = "ClientACN";
        public static string CLIENTTYPE = "CLIENTTYPE";
        public static string ADVISERFULLNAME = "ADVISERFULLNAME";
        public static string ADVISERID = "ADVISERID";
        public static string ADVISERCID = "AdviserCID";
        public static string ADDRESSLINE1 = "ADDRESSLINE1";
        public static string ADDRESSLINE2 = "ADDRESSLINE2";
        public static string SUBURB = "Suburb";
        public static string STATE = "State";
        public static string POSTCODE = "PostCode";
        public static string COUNTRY = "Country";
        public static string CLIENTAPPLICABILITYTYPE = "ClientApplicabilityType";

        public static string CONTACTTABLE = "ContactTable";
        public static string CONTACT_TITLE = "PC_Title";
        public static string CONTACT_NAME = "PC_Name";
        public static string CONTACT_BIZNO = "PC_BizNo";
        public static string CONTACT_MOBNO = "PC_MobNo";
        public static string CONTACT_HNO = "PC_HNo";
        public static string CONTACT_FAXNO = "PC_FaxNo";
        public static string CONTACT_EMAIL = "PC_Email";

        public string SETADDRESSLINE1 = "";
        public string SETADDRESSLINE2 = "";
        public string SETSUBURB = "";
        public string SETSTATE = "";
        public string SETPOSTCODE = "";
        public string SETCOUNTRY = "";

        public string AddressKey = String.Empty; 
        public Dictionary<string, string> AddressList = new Dictionary<string,string>();
        public bool UseAddressEntity = false; 

        public BaseClientDetails()  
        {
            this.Tables.Add(AddClientDetails());
            this.Tables.Add(AddContactTable());
        }

        public DataTable AddClientDetails()
        {
            DataTable dt = new DataTable(CLIENTSUMMARYTABLE);
            dt.Columns.Add(CLIENTCID, typeof(string));
            dt.Columns.Add(CLIENTID, typeof(string));
            dt.Columns.Add(ECLIPSECLIENTID, typeof(string));
            dt.Columns.Add(CLIENTNAME, typeof(string));
            dt.Columns.Add(CIENTABN, typeof(string));
            dt.Columns.Add(CLIENTTYPE, typeof(string));
            dt.Columns.Add(ADVISERFULLNAME, typeof(string));
            dt.Columns.Add(ADVISERID, typeof(string));
            dt.Columns.Add(ADVISERCID, typeof(string));
            dt.Columns.Add(CLIENTACN, typeof(string));
            dt.Columns.Add(ADDRESSLINE1, typeof(string));
            dt.Columns.Add(ADDRESSLINE2, typeof(string));
            dt.Columns.Add(STATE, typeof(string));
            dt.Columns.Add(SUBURB, typeof(string));
            dt.Columns.Add(POSTCODE, typeof(string));
            dt.Columns.Add(COUNTRY, typeof(string));
            dt.Columns.Add(CLIENTAPPLICABILITYTYPE, typeof(string));

            return dt;
        }

        public DataTable AddContactTable()
        {
            DataTable dt = new DataTable(CONTACTTABLE);

            dt.Columns.Add(CONTACT_TITLE, typeof(string));
            dt.Columns.Add(CONTACT_NAME, typeof(string));
            dt.Columns.Add(CONTACT_BIZNO, typeof(string));
            dt.Columns.Add(CONTACT_MOBNO, typeof(string));
            dt.Columns.Add(CONTACT_HNO, typeof(string));
            dt.Columns.Add(CONTACT_FAXNO, typeof(string));
            dt.Columns.Add(CONTACT_EMAIL, typeof(string));

            return dt;
        }

        public string GetServiceTypeDescription(string serviceTypeEnum)
        {
            switch (serviceTypeEnum)
            {
                case "DoItForMe":
                    return "Do It For Me";
                case "DoItWithMe":
                    return "Do It With Me";
                case "DoItYourSelf":
                    return "Do It Yourself";
                case "ManualAsset":
                    return "Manual Asset";
                default:
                    return "";
            }
        }

        /// <summary>
        /// Serialize given object into XmlElement.
        /// </summary>
        /// <param name="transformObject">Input object for serialization.</param>
        /// <returns>Returns serialized XmlElement.</returns>
        #region Serialize given object into stream.
        public static XmlElement Serialize(object transformObject)
        {
            XmlElement serializedElement = null;
            try
            {
                MemoryStream memStream = new MemoryStream();
                XmlSerializer serializer = new XmlSerializer(transformObject.GetType());
                serializer.Serialize(memStream, transformObject);
                memStream.Position = 0;
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(memStream);
                serializedElement = xmlDoc.DocumentElement;
            }
            catch{}
            return serializedElement;
        }
        #endregion // End - Serialize given object into stream.






        /// <summary>
        /// Deserialize given XmlElement into object.
        /// </summary>
        /// <param name="xmlElement">xmlElement to deserialize.</param>
        /// <param name="tp">Type of resultant deserialized object.</param>
        /// <returns>Returns deserialized object.</returns>
        #region Deserialize given string into object.
        public static object Deserialize(XmlElement xmlElement, System.Type tp)
        {
            Object transformedObject = null;
            try
            {
                Stream memStream = StringToStream(xmlElement.OuterXml);
                XmlSerializer serializer = new XmlSerializer(tp);
                transformedObject = serializer.Deserialize(memStream);
            }
            catch
            {

            }
            return transformedObject;
        }

        #endregion // End - Deserialize given string into object.
        /// <summary>
        /// Conversion from string to stream.
        /// </summary>
        /// <param name="str">Input string.</param>
        /// <returns>Returns stream.</returns>
        #region Conversion from string to stream.
        public static Stream StringToStream(String str)
        {
            MemoryStream memStream = null;
            try
            {
                byte[] buffer = Encoding.UTF8.GetBytes(str);//new byte[str.Length];
                memStream = new MemoryStream(buffer);
            }
            catch { }
            finally
            {
                memStream.Position = 0;
            }

            return memStream;
        }
        #endregion // End - Conversion from string to stream.

        public static DataSet ConvertXMLToDataSet(string xmlData)
        {
            StringReader stream = null;
            XmlTextReader reader = null;
            try
            {
                DataSet xmlDS = new DataSet();
                stream = new StringReader(xmlData);
                // Load the XmlTextReader from the stream
                reader = new XmlTextReader(stream);
                xmlDS.ReadXml(reader);
                return xmlDS;
            }
            catch
            {
                return null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }
    }
}
