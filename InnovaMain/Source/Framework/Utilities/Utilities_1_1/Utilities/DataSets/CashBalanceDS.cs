﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    [Serializable]
    public class CashBalanceDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public CashBalanceTable CashBalanceTable = new CashBalanceTable();
        public OrganizationUnit Unit { get; set; }
        public int Command { get; set; }

        public CashBalanceDS()
        {
            Tables.Add(CashBalanceTable);
        }
    }

    [Serializable]
    public class CashBalanceTable : DataTable
    {
        private readonly string TABLENAME = "CashBalanceList";

        public readonly string ID = "ID";
        public readonly string SUPERACCOUNTTYPE = "SuperAccountType";
        public readonly string PERCENTAGE = "Percentage";
        public readonly string MINVALUE = "MinValue";
        public readonly string MAXVALUE = "MaxValue";

        internal CashBalanceTable()
        {
            TableName = TABLENAME;
            Columns.Add(ID, typeof(Guid));
            Columns.Add(SUPERACCOUNTTYPE, typeof(string));
            Columns.Add(PERCENTAGE, typeof(decimal));
            Columns.Add(MINVALUE, typeof(decimal));
            Columns.Add(MAXVALUE, typeof(decimal));
        }
    }
}
