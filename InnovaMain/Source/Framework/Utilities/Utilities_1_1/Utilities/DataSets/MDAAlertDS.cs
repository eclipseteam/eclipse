﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class MDAAlertDS : DataSet, IHasOrganizationUnit, ISupportRecordCount, ISupportClientIDFilter
    {
        #region Properties

        public const string CLIENTDETAILTABLE = "ClientDetailTable";
        public const string MDAALERTTABLE = "MDAAlertTable";
        public const string AlertConfigurationTable = "AlertConfigurationTable";

        public  DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public ClientDetailTable ClientDetailTable = new ClientDetailTable();
        //public MDAAlertTable MdaAlertTable = new MDAAlertTable();


        #endregion

        public MDAAlertDS()
        {
            Tables.Add(ClientDetailTable);
        }


        public OrganizationUnit Unit
        {
            get; 
            set;
        }

        public int Command
        {
            get;
            set;
        }

        public int Count { get; set; }

        public string ClientIDs { get; set; }
    }

    public class ClientDetailTable : DataTable
    {
       
        public const string CLIENTID = "ClientID";
        public const string CLIENTCID = "ClientCID";
        public const string PARENTID = "ParentID";
        public const string CLIENTNAME = "ClientName";
        public const string CLIENTEMAIL = "ClientEmail";
        public const string MDAEXECUTIONDATE = "MDAExecutionDate";
        
        public const string SERVICE = "Service";
        public const string ADVISERID = "AdviserID";
        public const string ADVISERNAME = "AdviserName";
        public const string ADVISERCODE = "AdviserCode";
        public const string ADVISEREMAIL = "AdviserEmail";
        public const string IFACID = "IFACID";
        public const string IFACODE = "IFACODE";
        public const string IFANAME = "IFANAME";
        public const string IFAEMAIL = "IFAEmail";
        public const string DGCID = "DGCID";
        public const string DGCODE = "DGCODE";
        public const string DGNAME = "DGNAME";
        public const string DGACN = "DGACN";

        public const string HASDIFM = "HASDIFM";
        public const string HASDIWM = "HASDIWM";
        public const string HASDIYS = "HASDIYS";

        public const string DIFMPROGRAMCODE = "DIFMProgramCode";
        public const string DIWMPROGRAMCODE = "DIWMProgramCode";
        public const string DIYSPROGRAMCODE= "DIYSProgramCode";

        public const string DIFMPROGRAMNAME = "DIFMProgramName";
        public const string DIWMPROGRAMNAME = "DIWMProgramName";
        public const string DIYSPROGRAMNAME = "DIYSProgramName";

        
        public ClientDetailTable()
        {
            this.TableName = MDAAlertDS.CLIENTDETAILTABLE;
            Columns.Add(CLIENTID, typeof (string));
            Columns.Add(CLIENTCID, typeof(Guid));
            Columns.Add(PARENTID, typeof(Guid));
            Columns.Add(CLIENTNAME, typeof(string));
            Columns.Add(CLIENTEMAIL, typeof(string));
            Columns.Add(MDAEXECUTIONDATE, typeof(string));
            Columns.Add(SERVICE, typeof(string));
            Columns.Add(ADVISERID, typeof(Guid));
            Columns.Add(ADVISERNAME, typeof(string));
            Columns.Add(ADVISERCODE, typeof(string));
            Columns.Add(ADVISEREMAIL, typeof(string));
            Columns.Add(IFACID, typeof(Guid));
            Columns.Add(IFACODE, typeof(string));
            Columns.Add(IFANAME, typeof(string));
            Columns.Add(IFAEMAIL, typeof(string));
            Columns.Add(DGCID, typeof(Guid));
            Columns.Add(DGCODE, typeof(string));
            Columns.Add(DGNAME, typeof(string));
            Columns.Add(DGACN, typeof(string));
            Columns.Add(HASDIFM, typeof(bool));
            Columns.Add(HASDIWM, typeof(bool));
            Columns.Add(HASDIYS, typeof(bool));
            Columns.Add(DIFMPROGRAMCODE, typeof(string));
            Columns.Add(DIWMPROGRAMCODE, typeof(string));
            Columns.Add(DIYSPROGRAMCODE, typeof(string));
            Columns.Add(DIFMPROGRAMNAME, typeof(string));
            Columns.Add(DIWMPROGRAMNAME, typeof(string));
            Columns.Add(DIYSPROGRAMNAME, typeof(string));
        }
    }

    public class MDAAlertTable : DataTable
    {
        #region AlertTable Fields
        public const string ACTIONID = "ActionID";
        public const string CLIENTID = "ClientID";
        public const string CLIENTCID = "ClientCID";

        public const string CLIENTNAME = "ClientName";
      
        public const string DUEDATE = "DueDate";
        public const string EVENT = "Event";
        public const string STATUS = "Status";

        public const string ADVISERID = "AdviserID";
        public const string ADVISERNAME = "AdviserName";
      
        public const string SERVICE = "Service";
        public const string RAISEDDATE = "RaisedDate";

        public const string ISEXPIRED = "IsExpired";
        public const string ATTACHMENTID = "AttachmentID";
        public const string NOTIFICATIONID = "NotificationID";

        public const string FILEDOWNLOADURL = "FileDownloadURL";
        public const string ACTIONTYPEDETAIL = "ActionTypeDetail";
        public const string ATTACHED = "Attached";
        public const string COMPLETED = "Completed";
        //Added:Mansoor Ali for avoiding using of active values.
        public enum AlretStatus
        {
            Complete,
            Active
        };
        #endregion

        public MDAAlertTable()
        {
            this.TableName = MDAAlertDS.MDAALERTTABLE;
            Columns.Add(ACTIONID, typeof(string));
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(CLIENTCID, typeof(Guid));
            Columns.Add(CLIENTNAME, typeof(string));
            Columns.Add(DUEDATE, typeof(DateTime));
            Columns.Add(EVENT, typeof(string));
            Columns.Add(STATUS, typeof(string));
            Columns.Add(ADVISERID, typeof(Guid));
            Columns.Add(ADVISERNAME, typeof(string));
            Columns.Add(SERVICE, typeof(string));
            Columns.Add(RAISEDDATE, typeof(DateTime));
            Columns.Add(ISEXPIRED, typeof(bool));
            Columns.Add(ATTACHMENTID, typeof(Guid));
            Columns.Add(NOTIFICATIONID, typeof(Guid));
            Columns.Add(FILEDOWNLOADURL, typeof(string));
            Columns.Add(ACTIONTYPEDETAIL, typeof(string));
            Columns.Add(ATTACHED, typeof(bool));
            Columns.Add(COMPLETED, typeof(bool));

        }

    }
}
