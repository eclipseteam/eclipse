﻿using System;
using System.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class OPSaleDS : DataSet
    {
        public SaleInvestmentTable SaleInvestmentTable = new SaleInvestmentTable();

        public OPSaleDS()
        {
            Tables.Add(SaleInvestmentTable);
        }
    }

    public class SaleInvestmentTable : DataTable
    {
        public readonly string TABLENAME = "SaleInvestmentList";

        public readonly string INVESTMENTCODE = "InvestmentCode";
        public readonly string INVESTMENTNAME = "InvestmentName";
        public readonly string INVESTMENTTYPE = "InvestmentType";
        public readonly string MARKETCODE = "MarketCode";
        public readonly string CURRENCYCODE = "CurrencyCode";
        public readonly string UNITS = "Units";
        public readonly string UNITPRICE = "UnitPrice";
        public readonly string AMOUNTVALUE = "AmountValue";
        public readonly string CASHACCOUNTNAME = "CashAccountName";


        internal SaleInvestmentTable()
        {
            TableName = TABLENAME;

            Columns.Add(INVESTMENTCODE, typeof(string));
            Columns.Add(INVESTMENTNAME, typeof(string));
            Columns.Add(INVESTMENTTYPE, typeof(string));
            Columns.Add(MARKETCODE, typeof(string));
            Columns.Add(CURRENCYCODE, typeof(string));
            Columns.Add(UNITS, typeof(decimal));
            Columns.Add(UNITPRICE, typeof(decimal));
            Columns.Add(AMOUNTVALUE, typeof (decimal));
            Columns.Add(CASHACCOUNTNAME, typeof(string));
        }
    }
}
