﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class MISTransactionDS : DataSet,IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType;

        public const string MISTRANTABLE = "MIS Transactions";
        public const string MISTRANHOLDINGTABLE = "MISTranVsHolding";
        public string CLIENTID = string.Empty;
        public string CLIENTNAME = string.Empty;

        public const string CODE = "Code";
        public const string MISCID = "MISCID";
        public const string CLIENTIDCOL = "ClientID";
        public const string ACCOUNTNAME = "ClientName";
        public const string ACCOUNTTYPE = "AccountType";
        public const string CLIENTCID = "ClientCID";
        public const string DESCRIPTION = "Description";
        public const string ID = "ID";
        public const string TRADEDATE = "TradeDate";
        public const string TRANSTYPE = "TransactionType";
        public const string SHARES = "Shares";
        public const string UNITPRICE = "UnitPrice";
        public const string AMOUNT = "Amount";
        public const string STATUS = "Status";
        public const string HOLDING = "Holding";
        public const string DIFF = "Diff";
        public const string ERROR = "Error";
        public bool GetOnlyReconcileReport = false;

        public MISTransactionDS()
        {
            DataTable misTranTable = new DataTable(MISTransactionDS.MISTRANTABLE);
            this.Tables.Add(misTranTable);
            misTranTable.Columns.Add(CODE);
            misTranTable.Columns.Add(MISCID);
            misTranTable.Columns.Add(CLIENTIDCOL);
            misTranTable.Columns.Add(CLIENTCID);
            misTranTable.Columns.Add(DESCRIPTION);
            misTranTable.Columns.Add(ID);
            misTranTable.Columns.Add(TRADEDATE, typeof(DateTime));
            misTranTable.Columns.Add(TRANSTYPE);
            misTranTable.Columns.Add(SHARES);
            misTranTable.Columns.Add(UNITPRICE, typeof(decimal));
            misTranTable.Columns.Add(AMOUNT, typeof(decimal));
            misTranTable.Columns.Add(STATUS);

            DataTable misTranHoldingTable = new DataTable(MISTransactionDS.MISTRANHOLDINGTABLE);
            this.Tables.Add(misTranHoldingTable);
            misTranHoldingTable.Columns.Add(MISCID);
            misTranHoldingTable.Columns.Add(CLIENTIDCOL);
            misTranHoldingTable.Columns.Add(CLIENTCID);
            misTranHoldingTable.Columns.Add(ACCOUNTNAME);
            misTranHoldingTable.Columns.Add(ACCOUNTTYPE);
            misTranHoldingTable.Columns.Add(CODE);
            misTranHoldingTable.Columns.Add(DESCRIPTION);
            misTranHoldingTable.Columns.Add(AMOUNT, typeof(decimal));
            misTranHoldingTable.Columns.Add(SHARES, typeof(decimal));
            misTranHoldingTable.Columns.Add(HOLDING, typeof(decimal));
            misTranHoldingTable.Columns.Add(DIFF, typeof(decimal));
            misTranHoldingTable.Columns.Add(ERROR, typeof(bool));
           
        }

        public OrganizationUnit Unit{get; set; }

        public int Command{get; set; }
    }
}
