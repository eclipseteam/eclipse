﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class ManualAssetsDS : DataSet
    {
        public bool IsDetails;
        public Guid DetailsGUID = Guid.Empty;
        
        public Guid ItemID = Guid.Empty;
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public const string MANIST = "ManualAssetsList";
        public const string ID = "ID";
        public const string CODE = "Code";
        public const string DESCRIPTION = "Description";
        public const string INVESTMENTTYPE = "InvestmentType";
        public const string MARKET = "Market";
        public const string RATING = "Rating";
        public const string ISPREFFEREDINVESTMENT = "IsPrefferedInvestment";
        public const string UNITPRICEBASECURRENCY = "UnitPriceBaseCurrency";
        public const string UNITSIZE = "UnitSize";
        public const string MINSECURITYBAL = "MinSecurityBal";
        public const string MINTRADEDLOTSIZE = "MinTradedLotSize";
        public const string PRICEASATDATE = "PriceAsAtDate";
        public const string IGNOREMINSECURITYBAL = "IgnoreMinSecurityBal";
        public const string ISUNITISED = "IsUnitised";
        public const string IGNOREMINTRADEDLOTSIZE = "IgnoreMinTradedLotSize";
        public const string ISSUEDATE = "IssueDate";
        public const string STATUS = "Status";
       
        public const string MANPRICEHISTORY = "ManPriceHistory";
        public const string MANHISID = "ManualHistoryID";
        public const string TOTALUNITS = "TotalUnits";
        public const string DATE = "Date";
        public const string CURRENCY = "Currency";
        public const string VALUE = "Value";
        public const string UNITPRICE = "UnitPrice";
        public const string NAVPRICE = "NavPrice";
        public const string PURPRICE = "PurPrice";

        public ManualAssetsDS()
        {
            Tables.Add(ManListTable());
            Tables.Add(ManDetailListTable());
        }

        public DataTable ManListTable()
        {
            var manListTable = new DataTable(MANIST);
            manListTable.Columns.Add(ID, typeof(Guid));
            manListTable.Columns.Add(CODE, typeof(string));
            manListTable.Columns.Add(DESCRIPTION, typeof(string));
            manListTable.Columns.Add(INVESTMENTTYPE, typeof(string));
            manListTable.Columns.Add(MARKET, typeof(string));
            manListTable.Columns.Add(RATING, typeof(string));
            manListTable.Columns.Add(ISPREFFEREDINVESTMENT, typeof(bool));
            manListTable.Columns.Add(UNITPRICEBASECURRENCY, typeof(double));
            manListTable.Columns.Add(UNITSIZE, typeof(double));
            manListTable.Columns.Add(MINSECURITYBAL, typeof(double));
            manListTable.Columns.Add(MINTRADEDLOTSIZE, typeof(double));
            manListTable.Columns.Add(PRICEASATDATE, typeof(double));
            manListTable.Columns.Add(IGNOREMINSECURITYBAL, typeof(bool));
            manListTable.Columns.Add(ISUNITISED, typeof(bool));
            manListTable.Columns.Add(IGNOREMINTRADEDLOTSIZE, typeof(bool));
            manListTable.Columns.Add(ISSUEDATE, typeof(DateTime));
            manListTable.Columns.Add(STATUS, typeof(string));

            return manListTable;
        }

        public DataTable ManDetailListTable()
        {
            var manListPriceHisTable = new DataTable(MANPRICEHISTORY);
            manListPriceHisTable.Columns.Add(MANHISID, typeof(Guid));
            manListPriceHisTable.Columns.Add(CURRENCY, typeof(string));
            manListPriceHisTable.Columns.Add(DATE, typeof(DateTime));
            manListPriceHisTable.Columns.Add(VALUE, typeof(double));
            manListPriceHisTable.Columns.Add(UNITPRICE, typeof(double));
            manListPriceHisTable.Columns.Add(NAVPRICE, typeof(double));
            manListPriceHisTable.Columns.Add(PURPRICE, typeof(double));
            manListPriceHisTable.Columns.Add(TOTALUNITS, typeof(double));

            return manListPriceHisTable;
        }
    }
}
