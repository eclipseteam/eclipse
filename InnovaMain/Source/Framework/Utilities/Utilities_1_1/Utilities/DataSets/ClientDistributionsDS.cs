﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.DataSets
{
    public class ClientDistributionsDS : BaseClientDetails, IHasOrganizationUnit
    {
        public const string CLIENTDISTRIBUTIONTABLE = "ClientDistributionsTable";
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public string CLIENTIDForm = string.Empty;
        public string CLIENTNAMEForm = string.Empty;


        public ObservableCollection<DistributionIncomeEntity> DistributionIncomeEntityList { get; set; }

        public ClientDistributionsDS()
        {
            this.Tables.Add(this.GetIncomeTransactionMapTable());
        }

        public OrganizationUnit Unit { get; set; }
        public int Command { get; set; }
    }
  
}
