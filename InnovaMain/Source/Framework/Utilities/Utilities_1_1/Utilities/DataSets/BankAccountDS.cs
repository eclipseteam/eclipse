﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class BankAccountDS : UMABaseDS, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public BankAccounts BankAccountsTable = new BankAccounts();

        public BankAccountDS()
        {
            Tables.Add(BankAccountsTable);
            this.Tables.Add(GetIncomeTransactionMapTable()); 
        }


        public OrganizationUnit Unit { get; set; }

        public int Command { get; set; }

    }

    public class BankAccounts : DataTable
    {
        public readonly string TABLENAME = "BankAccountList";
        public readonly string CID = "Cid";
        public readonly string CLID = "CLid";
        public readonly string CSID = "CSid";
        public readonly string ACCOUNTNAME = "AccountName";
        public readonly string ACCOUNTTYPE = "AccountType";
        public readonly string BSBNO = "BSBNo";
        public readonly string ACCOUNTNO = "AccountNo";
        public readonly string INSTITUTION = "Institution";
        public readonly string INSTITUTIONID = "InstitutionId";
        public readonly string ACCOUNTTYPEOTHER = "AccountTypeOther";
        public readonly string BROKER = "Broker";
        public readonly string BROKERID = "BrokerId";
        public readonly string BROKERCSID = "BrokerCsid";
        public readonly string STATUS = "Status";
        public readonly string ISSELECTED = "IsSelected";
        public readonly string CUSTOMERNO = "CustomerNO";
        public readonly string TOTALAMOUNT = "TotalAmount";
        public readonly string UNSETTLEDBUY = "UnsettledBuy";
        public readonly string UNSETTLEDSELL = "UnsettledSell";
        public readonly string TRANSACTIONHOLDING = "TransactionHolding";
        public readonly string ISEXTERNALACCOUNT = "IsExternalAccount";
        public readonly string SERVICETYPES = "ServiceTypes";

        internal BankAccounts()
        {
            TableName = TABLENAME;
            Columns.Add(CID, typeof(Guid));
            Columns.Add(CLID, typeof(Guid));
            Columns.Add(CSID, typeof(Guid));
            Columns.Add(ACCOUNTNAME, typeof(string));
            Columns.Add(ACCOUNTTYPE, typeof(string));
            Columns.Add(BSBNO, typeof(string));
            Columns.Add(ACCOUNTNO, typeof(string));
            Columns.Add(INSTITUTION, typeof(string));
            Columns.Add(INSTITUTIONID, typeof(Guid));
            Columns.Add(ACCOUNTTYPEOTHER, typeof(string));
            Columns.Add(BROKER, typeof(string));
            Columns.Add(BROKERID, typeof(Guid));
            Columns.Add(BROKERCSID, typeof(Guid));
            Columns.Add(STATUS, typeof(string));
            var isSelected = Columns.Add(ISSELECTED, typeof(bool));
            isSelected.DefaultValue = false;
            Columns.Add(CUSTOMERNO, typeof(string));
            Columns.Add(TOTALAMOUNT, typeof(decimal));
            Columns.Add(UNSETTLEDBUY, typeof(decimal));
            Columns.Add(UNSETTLEDSELL, typeof(decimal));
            Columns.Add(TRANSACTIONHOLDING, typeof(decimal));
            var externalAcc = Columns.Add(ISEXTERNALACCOUNT, typeof(bool));
            externalAcc.DefaultValue = false;
            Columns.Add(SERVICETYPES, typeof(string));

        }
    }
}
