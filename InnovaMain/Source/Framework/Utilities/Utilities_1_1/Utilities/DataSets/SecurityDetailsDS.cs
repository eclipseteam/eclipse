﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public enum SecurityDetailTypes
    {
        HistoricalPrices,
        Distiribution,
        DistiributionPushDownReport,
        DividendPushDown,
        DividendPushDownReport

    };

    public class SecPriceHistory
    {
        public readonly string TB_Name = "SecurityPriceHistory";
        public readonly string ID = "ID";
        public readonly string SECID = "SecID";
        public readonly string ADJUSTEDPRICE = "AdjustedPrice";
        public readonly string DATE = "Date";
        public readonly string CURRENCY = "Currency";
        public readonly string INTERESTRATE = "InterestRate";
        public readonly string PRICENAV = "PriceNAV";
        public readonly string PRICEPUR = "PricePUR";
        public readonly string UNITPRICE = "UnitPrice";

        internal DataTable Table()
        {
            DataTable SecListPriceHisTable = new DataTable(TB_Name);
            SecListPriceHisTable.Columns.Add(ID, typeof(Guid));
            SecListPriceHisTable.Columns.Add(SECID, typeof(Guid));
            SecListPriceHisTable.Columns.Add(ADJUSTEDPRICE, typeof(double));
            SecListPriceHisTable.Columns.Add(CURRENCY, typeof(string));
            SecListPriceHisTable.Columns.Add(DATE, typeof(DateTime));
            SecListPriceHisTable.Columns.Add(INTERESTRATE, typeof(double));
            SecListPriceHisTable.Columns.Add(PRICENAV, typeof(double));
            SecListPriceHisTable.Columns.Add(PRICEPUR, typeof(double));
            SecListPriceHisTable.Columns.Add(UNITPRICE, typeof(double));
            return SecListPriceHisTable;
        }

        internal SecPriceHistory() { }
    }
    public class SecDistributions
    {
        public readonly string TB_Name = "SecuritysDistiributions";
        public readonly string ID = "ID";
        public readonly string SECID = "SecID";
        public readonly string INVESTMENTCODE = "InvestmentCode";
        public readonly string RECORDDATE = "RecordDate";
        public readonly string PAYMENTDATE = "PaymentDate";
        public readonly string CENTSPERSHARE = "CentsPerShare";

        internal DataTable Table()
        {
            DataTable SecListDistributionTable = new DataTable(TB_Name);
            SecListDistributionTable.Columns.Add(ID, typeof(Guid));
            SecListDistributionTable.Columns.Add(SECID, typeof(Guid));
            SecListDistributionTable.Columns.Add(INVESTMENTCODE, typeof(string));
            SecListDistributionTable.Columns.Add(RECORDDATE, typeof(DateTime));
            SecListDistributionTable.Columns.Add(PAYMENTDATE, typeof(DateTime));
            SecListDistributionTable.Columns.Add(CENTSPERSHARE, typeof(double));
            return SecListDistributionTable;
        }

        internal SecDistributions() { }
    }

    public class SecDividends
    {
        public readonly string TB_Name = "SecuritysDividends";
        public readonly string ID = "ID";
        public readonly string SECID = "SecID";
        public readonly string TRANSACTIONTYPE = "TransactionType";
        public readonly string INVESTMENTCODE = "InvestmentCode";
        public readonly string RECORDDATE = "RecordDate";
        public readonly string PAYMENTDATE = "PaymentDate";
        public readonly string CENTSPERSHARE = "CentsPerShare";

        public readonly string BALANCEDATE = "BalanceDate";
        public readonly string DIVIDENDTYPE = "Dividendtype";
        public readonly string CURRENCY = "Currency";
        public readonly string BOOKSCLOSEDATE = "BooksCloseDate";

        internal DataTable Table()
        {
            DataTable SecListDividendTable = new DataTable(TB_Name);
            SecListDividendTable.Columns.Add(ID, typeof(Guid));
            SecListDividendTable.Columns.Add(SECID, typeof(Guid));
            SecListDividendTable.Columns.Add(TRANSACTIONTYPE, typeof(string));
            SecListDividendTable.Columns.Add(INVESTMENTCODE, typeof(string));
            SecListDividendTable.Columns.Add(RECORDDATE, typeof(DateTime));
            SecListDividendTable.Columns.Add(PAYMENTDATE, typeof(DateTime));
            SecListDividendTable.Columns.Add(CENTSPERSHARE, typeof(double));

            SecListDividendTable.Columns.Add(BALANCEDATE, typeof(DateTime));
            SecListDividendTable.Columns.Add(DIVIDENDTYPE, typeof(string));
            SecListDividendTable.Columns.Add(CURRENCY, typeof(string));
            SecListDividendTable.Columns.Add(BOOKSCLOSEDATE, typeof(DateTime));

            return SecListDividendTable;
        }

        internal SecDividends() { }
    }
    public class SecDividendsReport
    {
        public readonly string TB_Name = "SecuritysDividendsReport";
        public readonly string ID = "ID";
        public readonly string SECID = "SecID";
        public readonly string CLIENTIDENTIFICATION = "ClientIdentification";
        public readonly string CLIENTNAME = "ClientName";
        public readonly string ACCOUNTNAME = "AccountName";
        public readonly string ACCOUNTNUMBER = "AccountNumber";
        public readonly string RECORDDATE = "RecordDate";
        public readonly string PAYMENTDATE = "PaymentDate";
        public readonly string UNITSONHAND = "UnitsOnHand";
        public readonly string CENTSPERSHARE = "CentsPerShare";
        public readonly string PAIDDIVIDEND = "PaidDividend";

        public readonly string BALANCEDATE = "BalanceDate";
        public readonly string DIVIDENDTYPE = "Dividendtype";
        public readonly string CURRENCY = "Currency";
        public readonly string BOOKSCLOSEDATE = "BooksCloseDate";

        internal DataTable Table()
        {
            DataTable SecListDividendTable = new DataTable(TB_Name);
            SecListDividendTable.Columns.Add(ID, typeof(Guid));
            SecListDividendTable.Columns.Add(SECID, typeof(Guid));
            SecListDividendTable.Columns.Add(CLIENTIDENTIFICATION, typeof(string));
            SecListDividendTable.Columns.Add(CLIENTNAME, typeof(string));
            SecListDividendTable.Columns.Add(ACCOUNTNAME, typeof(string));
            SecListDividendTable.Columns.Add(ACCOUNTNUMBER, typeof(string));
            SecListDividendTable.Columns.Add(RECORDDATE, typeof(DateTime));
            SecListDividendTable.Columns.Add(PAYMENTDATE, typeof(DateTime));
            SecListDividendTable.Columns.Add(UNITSONHAND, typeof(int));
            SecListDividendTable.Columns.Add(CENTSPERSHARE, typeof(double));
            SecListDividendTable.Columns.Add(PAIDDIVIDEND, typeof(double));

            SecListDividendTable.Columns.Add(BALANCEDATE, typeof(DateTime));
            SecListDividendTable.Columns.Add(DIVIDENDTYPE, typeof(string));
            SecListDividendTable.Columns.Add(CURRENCY, typeof(string));
            SecListDividendTable.Columns.Add(BOOKSCLOSEDATE, typeof(DateTime));
            return SecListDividendTable;
        }

        internal SecDividendsReport() { }
    }

    public class SecurityDetailsDS : DataSet
    {

        public Guid DetailsGUID = Guid.Empty;
        public SecurityDetailTypes RequiredDataType = SecurityDetailTypes.HistoricalPrices;
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public string ASXCode = string.Empty;
        public string Description = string.Empty;
        public SecPriceHistory PriceTable = new SecPriceHistory();
        public SecDistributions DistributionTable = new SecDistributions();
        public SecDividends DividendTable = new SecDividends();
        public SecDividendsReport SecDividendsReportTable = new SecDividendsReport();

        public SecurityDetailsDS()
        {
            this.Tables.Add(PriceTable.Table());
            this.Tables.Add(DistributionTable.Table());
            this.Tables.Add(DividendTable.Table());
            this.Tables.Add(SecDividendsReportTable.Table());
        }

    }
}
