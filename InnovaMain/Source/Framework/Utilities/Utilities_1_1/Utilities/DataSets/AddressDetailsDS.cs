﻿using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public enum AddressType
    {
        BusinessAddress = 5,
        ResidentialAddress = 10,
        RegisteredAddress = 15,
        MailingAddress = 20,
        DuplicateMailingAddress = 25,
    }
    public class AddressDetailsDS : DataSet
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public AddressDetails ClientDetailsTable = new AddressDetails();
       
        public AddressDetailsDS()
        {
            Tables.Add(ClientDetailsTable);
        }
    }

    public class AddressDetails : DataTable
    {
        private readonly string TABLENAME = "AddressDetails";
        public readonly string CLIENTID = "ClientId";
        public readonly string ADDRESSLINE1 = "AddressLine1";
        public readonly string ADDRESSLINE2 = "ADDRESSLINE2";
        public readonly string SUBURB = "SubUrb";
        public readonly string STATE = "State";
        public readonly string COUNTRY = "Country";
        public readonly string POSTCODE = "PostCode";
        public readonly string ADDRESSTYPE = "AddressType";

        internal AddressDetails()
        {
            TableName = TABLENAME;
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(ADDRESSLINE1, typeof(string));
            Columns.Add(ADDRESSLINE2, typeof(string));
            Columns.Add(SUBURB, typeof(string));
            Columns.Add(STATE, typeof(string));
            Columns.Add(COUNTRY, typeof(string));
            Columns.Add(POSTCODE, typeof(string));
            Columns.Add(ADDRESSTYPE, typeof(string));
        }
    }
}
