﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class SettledUnsetteledTransactionDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public OrganizationUnit Unit { get; set; }
        public int Command { get; set; }
        
        public SettledUnsetteledTransactionDS()
        {
        
        }
    }


    
}
