﻿using System;
using System.Collections.Generic;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public partial class HoldingRptDataSet : BaseClientDetails
    {
        private ServiceTypes serviceTypes = ServiceTypes.None;
        public bool IsMainView = false;
        public UserEntity CurrentUser { get; set; }
        public ServiceTypes ServiceTypes
        {
            get { return serviceTypes; }
            set { serviceTypes = value; }
        }
        public bool CheckForUnsettledCM = true; 

        public HoldingSummaryTable HoldingSummaryTable = new HoldingSummaryTable();
        public ProductBreakDownTable ProductBreakDownTable = new ProductBreakDownTable();
        public TDBreakDownTable TDBreakDownTable = new TDBreakDownTable();

        public DateTime ValuationDate;

        public HoldingRptDataSet()
        {
            ValuationDate = DateTime.Today;

            Tables.Add(HoldingSummaryTable);
            Tables.Add(ProductBreakDownTable);
            Tables.Add(TDBreakDownTable);
        }

        public decimal GetHoldingTotal()
        {
            decimal total = 0;

            foreach (DataRow row in HoldingSummaryTable.Rows)
                total += (decimal)row[HoldingSummaryTable.TOTAL];

            return total;
        }

        public string DateInfo()
        {
            return "AS OF " + this.ValuationDate.ToString("dd/MM/yyyy");
        }

        public void TDBreakDownRow(string insName, string serviceType, string tdType, string broker, decimal holding, string tdID, DataRow tDBreakDownTableRow, DateTime transDate, Guid brokerId, Guid insId, Guid productId, Guid accountCID, string contractNote)
        {
            tDBreakDownTableRow[TDBreakDownTable.TRANSTYPE] = tdType;
            tDBreakDownTableRow[TDBreakDownTable.INSNAME] = insName;
            tDBreakDownTableRow[TDBreakDownTable.BROKER] = broker;
            tDBreakDownTableRow[TDBreakDownTable.HOLDING] = holding;
            tDBreakDownTableRow[TDBreakDownTable.SERVICETYPE] = serviceType;
            tDBreakDownTableRow[TDBreakDownTable.TDID] = tdID;
            tDBreakDownTableRow[TDBreakDownTable.TRANSDATE] = transDate;
            tDBreakDownTableRow[TDBreakDownTable.BROKERID] = brokerId;
            tDBreakDownTableRow[TDBreakDownTable.INSID] = insId;
            tDBreakDownTableRow[TDBreakDownTable.TDACCOUNTCID] = accountCID;
            tDBreakDownTableRow[TDBreakDownTable.PRODUCTID] = productId;
            tDBreakDownTableRow[TDBreakDownTable.CONTRACTNOTE] = contractNote;
        }

        public static double CalculateTotalPercentageForSplit(IEnumerable<AssetEntity> linkedAssets)
        {
            double totalPercentage = 0;

            foreach (var linkedasst in linkedAssets)
                totalPercentage += linkedasst.SharePercentage;

            return totalPercentage;
        }

        public void AddProductDetailRow(ProductEntity product, decimal unitPrice, string investmentCode, string investmentName, decimal holding, decimal currentValue, string modelServiceType, decimal unsettledBuyUnits, decimal unsettledSellUnits, bool isSMAApproved, double smaHoldingLimit)
        {
            DataRow productBreakDownRow = ProductBreakDownTable.NewRow();

            productBreakDownRow[ProductBreakDownTable.PRODUCTID] = product.ID;
            productBreakDownRow[ProductBreakDownTable.LINKEDENTITYTYPE] = product.EntityType.ToString();
            productBreakDownRow[ProductBreakDownTable.INVESMENTCODE] = investmentCode;
            productBreakDownRow[ProductBreakDownTable.INVESMENTNAME] = investmentName;
            productBreakDownRow[ProductBreakDownTable.UNITPRICE] = unitPrice;
            productBreakDownRow[ProductBreakDownTable.CURRENTVALUE] = currentValue;
            productBreakDownRow[ProductBreakDownTable.UNSETTLED] = (unsettledBuyUnits + unsettledSellUnits) * unitPrice;
            productBreakDownRow[ProductBreakDownTable.TOTAL] = ((unsettledBuyUnits + unsettledSellUnits) * unitPrice) + currentValue;
            productBreakDownRow[ProductBreakDownTable.UNSETTLEDBUY] = unsettledBuyUnits;
            productBreakDownRow[ProductBreakDownTable.UNSETTLEDSELL] = unsettledSellUnits;
            productBreakDownRow[ProductBreakDownTable.HOLDING] = holding;
            productBreakDownRow[ProductBreakDownTable.SERVICETYPE] = GetServiceTypeDescription(modelServiceType);
            productBreakDownRow[ProductBreakDownTable.ISSMAAPPROVED] = isSMAApproved;
            productBreakDownRow[ProductBreakDownTable.SMAHOLDINGLIMIT] = smaHoldingLimit;
            ProductBreakDownTable.Rows.Add(productBreakDownRow);
        }

        public void AddProductDetailRow(Guid productID, string entityType, decimal unitPrice, string investmentCode, string investmentName, decimal holding, decimal currentValue, string modelServiceType, decimal unsettledBuyUnits, decimal unsetteledSellUnits, bool isSMAApproved, double smaHoldingLimit)
        {
            DataRow productBreakDownRow = ProductBreakDownTable.NewRow();

            productBreakDownRow[ProductBreakDownTable.PRODUCTID] = productID;
            productBreakDownRow[ProductBreakDownTable.LINKEDENTITYTYPE] = entityType;
            productBreakDownRow[ProductBreakDownTable.INVESMENTCODE] = investmentCode;
            productBreakDownRow[ProductBreakDownTable.INVESMENTNAME] = investmentName;
            productBreakDownRow[ProductBreakDownTable.UNITPRICE] = unitPrice;
            productBreakDownRow[ProductBreakDownTable.CURRENTVALUE] = currentValue;
            productBreakDownRow[ProductBreakDownTable.UNSETTLED] = (unsettledBuyUnits + unsetteledSellUnits) * unitPrice;
            productBreakDownRow[ProductBreakDownTable.TOTAL] = ((unsettledBuyUnits + unsetteledSellUnits) * unitPrice) + currentValue;
            productBreakDownRow[ProductBreakDownTable.UNSETTLEDBUY] = unsettledBuyUnits;
            productBreakDownRow[ProductBreakDownTable.UNSETTLEDSELL] = unsetteledSellUnits;
            productBreakDownRow[ProductBreakDownTable.HOLDING] = holding;
            productBreakDownRow[ProductBreakDownTable.SERVICETYPE] = GetServiceTypeDescription(modelServiceType);
            productBreakDownRow[ProductBreakDownTable.ISSMAAPPROVED] = isSMAApproved;
            productBreakDownRow[ProductBreakDownTable.SMAHOLDINGLIMIT] = smaHoldingLimit;

            ProductBreakDownTable.Rows.Add(productBreakDownRow);
        }

        public void AddHoldingRow(string accountName, string accountID, bool isPriceAtDate, DateTime priceAtDate, string modelName, string assetName, ServiceTypes modelServiceType, string productName, string productTaskDesc, Guid accountCID, string accountNo, string productID, string tdID, string entityType, string bsb, decimal holdingTotal, decimal unsettledBuy, decimal unsettledSell, decimal unsettledBuyUnits, decimal unsettledSellUnits, Guid brokerId, Guid insId, decimal unsettledSellByUnits, decimal unsettledSellByAmount)
        {
            decimal unsettled = unsettledBuy + unsettledSell;
            decimal unsettledUnits = unsettledBuyUnits + unsettledSellUnits;

            decimal total = holdingTotal + unsettled;

            DataRow holdingSumaryRow = HoldingSummaryTable.NewRow();
            holdingSumaryRow[HoldingSummaryTable.ACCOUNTNAME] = accountName;
            holdingSumaryRow[HoldingSummaryTable.ACCOUNTID] = accountID;
            holdingSumaryRow[HoldingSummaryTable.ASSETNAME] = assetName;
            holdingSumaryRow[HoldingSummaryTable.SERVICETYPE] = Enumeration.GetDescription(modelServiceType);
            holdingSumaryRow[HoldingSummaryTable.PRODUCTNAME] = productName;
            holdingSumaryRow[HoldingSummaryTable.DESCRIPTION] = productTaskDesc;
            holdingSumaryRow[HoldingSummaryTable.ACCOUNTCID] = accountCID;
            holdingSumaryRow[HoldingSummaryTable.ACCOUNTNO] = accountNo;
            holdingSumaryRow[HoldingSummaryTable.BSB] = bsb;
            holdingSumaryRow[HoldingSummaryTable.HOLDING] = holdingTotal;
            holdingSumaryRow[HoldingSummaryTable.UNSETTLED] = unsettled;
            holdingSumaryRow[HoldingSummaryTable.UNSETTLEDBUY] = unsettledBuy;
            holdingSumaryRow[HoldingSummaryTable.UNSETTLEDSELL] = unsettledSell;
            holdingSumaryRow[HoldingSummaryTable.UNSETTLEDUNITS] = unsettledUnits;
            holdingSumaryRow[HoldingSummaryTable.UNSETTLEDBUYUNITS] = unsettledBuyUnits;
            holdingSumaryRow[HoldingSummaryTable.UNSETTLEDSELLUNITS] = unsettledSellUnits;
            holdingSumaryRow[HoldingSummaryTable.TOTAL] = total;
            holdingSumaryRow[HoldingSummaryTable.PRODUCTID] = productID;
            holdingSumaryRow[HoldingSummaryTable.TDID] = tdID;
            holdingSumaryRow[HoldingSummaryTable.LINKEDENTITYTYPE] = entityType;
            holdingSumaryRow[HoldingSummaryTable.BROKERID] = brokerId;
            holdingSumaryRow[HoldingSummaryTable.INSTITUTEID] = insId;
            if (isPriceAtDate)
                holdingSumaryRow[HoldingSummaryTable.PRICEATDATE] = priceAtDate;

            if (modelName == "ManualAsset")
            {
                holdingSumaryRow[HoldingSummaryTable.MODELNAME] = Enumeration.GetDescription(modelServiceType);
                holdingSumaryRow[HoldingSummaryTable.MODELNAMECONSOL] = accountName + "- " + Enumeration.GetDescription(modelServiceType);
            }
            else
            {
                holdingSumaryRow[HoldingSummaryTable.MODELNAME] = Enumeration.GetDescription(modelServiceType) + ": " + modelName;
                holdingSumaryRow[HoldingSummaryTable.MODELNAMECONSOL] = accountName + "- " + Enumeration.GetDescription(modelServiceType) + ": " + modelName;
            }

            holdingSumaryRow[HoldingSummaryTable.UNSETTLEDSELLBUYUNITS] = unsettledSellByUnits;
            holdingSumaryRow[HoldingSummaryTable.UNSETTLEDSELLBYAMOUNT] = unsettledSellByAmount;

            HoldingSummaryTable.Rows.Add(holdingSumaryRow);
        }
    }


    public class HoldingSummaryTable : DataTable
    {
        private readonly string HOLDINGSUMMARYTABLE = "HoldingSummary";
        public readonly string ACCOUNTNAME = "AccuontName";
        public readonly string ACCOUNTID = "AccountID";
        public readonly string MODELNAMECONSOL = "MODELNAMECONSOL";
        public readonly string ASSETNAME = "AssetName";
        public readonly string PRODUCTNAME = "ProductName";
        public readonly string DESCRIPTION = "Description";
        public readonly string ACCOUNTCID = "AccountCID";
        public readonly string ACCOUNTNO = "AccountNo";
        public readonly string BSB = "BSB";
        public readonly string HOLDING = "Holding";
        public readonly string UNSETTLED = "Unsettled";
        public readonly string TOTAL = "Total";
        public readonly string UNSETTLEDBUY = "UnsettledBuy";
        public readonly string UNSETTLEDSELL = "UnsettledSell";
        public readonly string UNSETTLEDUNITS = "UnsettledUnits";
        public readonly string UNSETTLEDBUYUNITS = "UnsettledBuyUnits";
        public readonly string UNSETTLEDSELLUNITS = "UnsettledSellUnits";
        public readonly string SERVICETYPE = "ServiceType";
        public readonly string TDID = "TDID";
        public readonly string PRODUCTID = "ProductID";
        public readonly string LINKEDENTITYTYPE = "LinkedEntityType";
        public readonly string MODELNAME = "MODELNAME";
        public readonly string PRICEATDATE = "PRICEATDATE";
        public readonly string BROKERID = "BrokerId";
        public readonly string INSTITUTEID = "InstituteId";
        public readonly string UNSETTLEDSELLBUYUNITS = "UnsettledSellBuyUnits";
        public readonly string UNSETTLEDSELLBYAMOUNT = "UnsettledSellByAmount";

        internal HoldingSummaryTable()
        {
            TableName = HOLDINGSUMMARYTABLE;
            Columns.Add(ACCOUNTNAME, typeof(string));
            Columns.Add(ACCOUNTID, typeof(string));
            Columns.Add(TDID, typeof(string));
            Columns.Add(PRODUCTID, typeof(string));
            Columns.Add(LINKEDENTITYTYPE, typeof(string));
            Columns.Add(ASSETNAME, typeof(string));
            Columns.Add(PRODUCTNAME, typeof(string));
            Columns.Add(DESCRIPTION, typeof(string));
            Columns.Add(ACCOUNTCID, typeof(Guid));
            Columns.Add(ACCOUNTNO, typeof(string));
            Columns.Add(BSB, typeof(string));
            Columns.Add(HOLDING, typeof(decimal));
            Columns.Add(UNSETTLED, typeof(decimal));
            Columns.Add(UNSETTLEDBUY, typeof(decimal));
            Columns.Add(UNSETTLEDBUYUNITS, typeof(decimal));
            Columns.Add(UNSETTLEDUNITS, typeof(decimal));
            Columns.Add(UNSETTLEDSELLUNITS, typeof(decimal));
            Columns.Add(UNSETTLEDSELL, typeof(decimal));
            Columns.Add(TOTAL, typeof(decimal));
            Columns.Add(SERVICETYPE, typeof(string));
            Columns.Add(MODELNAME, typeof(string));
            Columns.Add(MODELNAMECONSOL, typeof(string));
            Columns.Add(PRICEATDATE, typeof(DateTime));
            Columns.Add(BROKERID, typeof(Guid));
            Columns.Add(INSTITUTEID, typeof(Guid));
            Columns.Add(UNSETTLEDSELLBUYUNITS, typeof(decimal));
            Columns.Add(UNSETTLEDSELLBYAMOUNT, typeof(decimal));
        }
    }

    public class ProductBreakDownTable : DataTable
    {
        private readonly string PRODUCTBREAKDOWNTABLE = "ProductBreakDown";

        public readonly string PRODUCTID = "ProductID";
        public readonly string LINKEDENTITYTYPE = "LinkedEntityType";
        public readonly string INVESMENTCODE = "InvestmentCode";
        public readonly string INVESMENTNAME = "InvestmentName";
        public readonly string UNITPRICE = "UnitPrice";
        public readonly string CURRENTVALUE = "CurrentValue";
        public readonly string UNSETTLEDBUY = "UnsettledBuy";
        public readonly string UNSETTLEDSELL = "UnsettledSell";
        public readonly string HOLDING = "Holding";
        public readonly string UNSETTLED = "Unsettled";
        public readonly string TOTAL = "Total";
        public readonly string SERVICETYPE = "ServiceType";
        public readonly string ISSMAAPPROVED = "IsSMAApproved";
        public readonly string SMAHOLDINGLIMIT = "SMAHoldingLimit";

        internal ProductBreakDownTable()
        {
            TableName = PRODUCTBREAKDOWNTABLE;

            Columns.Add(PRODUCTID, typeof(string));
            Columns.Add(LINKEDENTITYTYPE, typeof(string));
            Columns.Add(INVESMENTCODE, typeof(string));
            Columns.Add(INVESMENTNAME, typeof(string));
            Columns.Add(UNITPRICE, typeof(decimal));
            Columns.Add(UNSETTLEDBUY, typeof(decimal));
            Columns.Add(UNSETTLEDSELL, typeof(decimal));
            Columns.Add(CURRENTVALUE, typeof(decimal));
            Columns.Add(HOLDING, typeof(decimal));
            Columns.Add(UNSETTLED, typeof(decimal));
            Columns.Add(TOTAL, typeof(decimal));
            Columns.Add(SERVICETYPE, typeof(string));
            Columns.Add(ISSMAAPPROVED, typeof(bool));
            Columns.Add(SMAHOLDINGLIMIT, typeof(double));
        }
    }

    public class TDBreakDownTable : DataTable
    {
        private readonly string TDBREAKDOWNTABLE = "TDBreakDown";

        public readonly string INSNAME = "InsName";
        public readonly string TRANSTYPE = "TransType";
        public readonly string BROKER = "BROKER";
        public readonly string BROKERID = "BrokerId";
        public readonly string INSID = "InsId";
        public readonly string HOLDING = "Holding";
        public readonly string SERVICETYPE = "ServiceType";
        public readonly string TDID = "TDID";
        public readonly string TRANSDATE = "TRANSDATE";
        public readonly string TDACCOUNTCID = "TDAccountCID";
        public readonly string PRODUCTID = "ProductID";
        public readonly string CONTRACTNOTE = "ContractNote";

        internal TDBreakDownTable()
        {
            TableName = TDBREAKDOWNTABLE;

            Columns.Add(INSNAME, typeof(string));
            Columns.Add(TRANSTYPE, typeof(string));
            Columns.Add(BROKER, typeof(string));
            Columns.Add(HOLDING, typeof(decimal));
            Columns.Add(SERVICETYPE, typeof(string));
            Columns.Add(TDID, typeof(string));
            Columns.Add(TRANSDATE, typeof(DateTime));
            Columns.Add(BROKERID, typeof(Guid));
            Columns.Add(INSID, typeof(Guid));
            Columns.Add(TDACCOUNTCID, typeof(Guid));
            Columns.Add(PRODUCTID, typeof(Guid));
            Columns.Add(CONTRACTNOTE, typeof(string));
        }

    }
}
