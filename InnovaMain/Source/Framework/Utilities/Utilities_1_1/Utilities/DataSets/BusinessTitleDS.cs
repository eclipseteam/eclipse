﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class BusinessTitleDS : DataSet
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public string TypeFilter { get; set; }
        public BussinessTitleTable BTitleTable = new BussinessTitleTable();

        public BusinessTitleDS()
        {
            this.Tables.Add(BTitleTable);
        }
    }
    public class BussinessTitleTable : DataTable
    {
        public readonly string NAME = "BusinessTitleList";
        public readonly string TITLE = "Title";
        public readonly string Value = "Value";
        internal BussinessTitleTable()
        {
            TableName = NAME;

            Columns.Add(TITLE, typeof(string));
            Columns.Add(Value, typeof(string));

        }
    }
}
