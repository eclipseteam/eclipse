﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class ClientTotalUnitsDS : DataSet, IHasOrganizationUnit
    {
       
        public string clientID { get; set; }
        public string code { get; set; }
        public ClientTotalUnitsTable clientTotalUnitsTable = new ClientTotalUnitsTable();

        public ClientTotalUnitsDS()
        {
            Tables.Add(clientTotalUnitsTable);
        }
        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
    }

    

    public class ClientTotalUnitsTable : DataTable
    {
        public readonly string TABLENAME = "ClientTotalUnitsList";
        public readonly string TOTALUNITS = "TotalUnits";
        public readonly string UNSETTLED = "UnSettled";
        public readonly string UNSETTLEDAMOUNT = "UnSettledAmount";
        

        internal ClientTotalUnitsTable()
        {
            TableName = TABLENAME;
            Columns.Add(TOTALUNITS, typeof(string));
            Columns.Add(UNSETTLED, typeof(string));
            Columns.Add(UNSETTLEDAMOUNT, typeof(string));
          
        }
    }
}
