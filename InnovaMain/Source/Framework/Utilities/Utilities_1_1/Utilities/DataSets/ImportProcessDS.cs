﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class ImportProcessDS : DataSet, IHasOrganizationUnit, IHasFileName
    {
        public string FileName
        {
            get;
            set;
        }

        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
        public Dictionary<string, string> Variables = new Dictionary<string, string>(); 
        public DateTime StartDate;
        public DateTime EndDate;
        public bool IsSuccess = false;
        public string MessageNumber = String.Empty;
        public string MessageSummary = String.Empty;
        public string MessageDetail = String.Empty;
        public bool UseFile = false;
        public string ByIDPara = string.Empty;
        public bool ImportByID = false;
    }
}
