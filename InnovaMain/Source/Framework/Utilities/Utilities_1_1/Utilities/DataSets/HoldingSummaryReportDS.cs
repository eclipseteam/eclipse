﻿using System;
using System.Data;
using System.Linq;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.DataSets
{
    public partial class HoldingSummaryReportDS : BaseClientDetails,IHasOrganizationUnit,ISupportRecordCount
    {
        private CostTypes costTypes = CostTypes.FIFO;

        public CostTypes CostTypes
        {
            get { return costTypes; }
            set { costTypes = value; }
        }

        private AcountingMethodType acountingMethodType = AcountingMethodType.Mixed;

        public AcountingMethodType AcountingMethodType
        {
            get { return acountingMethodType; }
            set { acountingMethodType = value; }
        }

        public bool AddBanktransInInvestment { get; set; }
        

        public SecuritySummaryTable securitySummaryTable = new SecuritySummaryTable();
        public MISTransactionDetailsTable misTransactionDetailsTable = new MISTransactionDetailsTable();
        public TransactionDetailsTable transactiondetailstable = new TransactionDetailsTable();
        public static string SECURITYSUMMARYTABLEREALISED = "SecuritySummaryRealised";
        public static string SECURITYSUMMARYTABLEUNREALISED = "SecuritySummaryUNRealised";
        public static string CURRENTUNITS = "CurrentUnits";
        public static string SOLDUNITS = "SoldUnits";
        public static string SALENETVALUE = "SaleNetVaue";

        public const string CAPITALTRANTABLE = "CAPITALTRANTABLE";
       
        public const string CLIENTID = "ClientID";
        public const string CLIENTNAME = "ClientName";
        public const string CLIENTCID = "ClientCID";
        public const string SERVICETYPE = "ServiceType";

        public const string TRANID = "ID";
        public const string ACCCID = "ACCID";
        public const string SECODE = "SECCODE";
        public const string PROTYPE = "PROTYPE";
        public const string SECNAME = "SECNAME";
        public const string TRANTYPE = "TRANTYPE";
        public const string ACCNO = "ACCNO";
        public const string TRADEDATE = "TRADEDATE";
        public const string SETTLEDATE = "SETTLEDATE";
        public const string UNITS = "UNITS";
        public const string TRADEPRICE = "TRADEPRICE";
        public const string GROSSVAL = "GROSSVAL";
        public const string FEES = "FEES";
        public const string NETVALUE = "NETVALUE";


        public DateTime StartDate;
        public DateTime EndDate;

        public HoldingSummaryReportDS()
        {

            AccountingFinancialYear financialYear = new AccountingFinancialYear(DateTime.Now);

            this.StartDate = financialYear.FinYearStartDate;
            this.EndDate = financialYear.FinYearEndDate;

            Tables.Add(securitySummaryTable);
            Tables.Add(misTransactionDetailsTable);
            Tables.Add(transactiondetailstable);

            DataTable dt = new DataTable(CAPITALTRANTABLE);
            dt.Columns.Add(CLIENTNAME, typeof(string));
            dt.Columns.Add(CLIENTID, typeof(string));
            dt.Columns.Add(CLIENTCID, typeof(Guid));
            dt.Columns.Add(SERVICETYPE, typeof(string));
            dt.Columns.Add(TRANID, typeof(Guid));
            dt.Columns.Add(ACCCID, typeof(Guid));
            dt.Columns.Add(SECODE, typeof(string));
            dt.Columns.Add(PROTYPE, typeof(string));
            dt.Columns.Add(SECNAME, typeof(string));
            dt.Columns.Add(TRANTYPE, typeof(string));
            dt.Columns.Add(ACCNO, typeof(string));
            dt.Columns.Add(TRADEDATE, typeof(DateTime));
            dt.Columns.Add(SETTLEDATE, typeof(DateTime));
            dt.Columns.Add(UNITS, typeof(decimal));
            dt.Columns.Add(TRADEPRICE, typeof(decimal));
            dt.Columns.Add(GROSSVAL, typeof(decimal));
            dt.Columns.Add(FEES, typeof(decimal));
            dt.Columns.Add(NETVALUE, typeof(decimal));
            this.Tables.Add(dt);
        }

        public void AddCapitalTranRow(string clientName,string clientId, Guid clientCid, string serviceType, Guid tranID, Guid accID, string seccode, string protype, string secname, string trantype, string accno, DateTime tradeDate, DateTime settleDate
                                   , decimal units, decimal tradeprice, decimal grossval, decimal fees, decimal netval)
        {
            DataRow capRow = this.Tables[CAPITALTRANTABLE].NewRow();
            capRow[HoldingSummaryReportDS.CLIENTNAME] = clientName;
            capRow[HoldingSummaryReportDS.CLIENTID] = clientId;
            capRow[HoldingSummaryReportDS.CLIENTCID] = clientCid;
            capRow[HoldingSummaryReportDS.SERVICETYPE] = serviceType;
            capRow[HoldingSummaryReportDS.TRANID] = tranID;
            capRow[HoldingSummaryReportDS.ACCCID] = accID;
            capRow[HoldingSummaryReportDS.SECODE] = seccode;
            capRow[HoldingSummaryReportDS.PROTYPE] = protype;
            capRow[HoldingSummaryReportDS.SECNAME] = secname;
            capRow[HoldingSummaryReportDS.TRANTYPE] = trantype;
            capRow[HoldingSummaryReportDS.ACCNO] = accno;
            capRow[HoldingSummaryReportDS.TRADEDATE] = tradeDate;
            capRow[HoldingSummaryReportDS.SETTLEDATE] = settleDate;
            capRow[HoldingSummaryReportDS.UNITS] = units;
            capRow[HoldingSummaryReportDS.TRADEPRICE] = tradeprice;
            capRow[HoldingSummaryReportDS.GROSSVAL] = grossval;
            capRow[HoldingSummaryReportDS.FEES] = fees;
            capRow[HoldingSummaryReportDS.NETVALUE] = netval;
            this.Tables[CAPITALTRANTABLE].Rows.Add(capRow);

        }

        public string DateInfo()
        {
            return this.StartDate.ToString("dd/MM/yyyy") + "-" + this.EndDate.ToString("dd/MM/yyyy");
        }

        public void AddSecuritySummaryRow(string type, string accountNo, string securityName, string description, DateTime priceAtDate, decimal openingUnitsBal
                                     , decimal openingUnitPrice, decimal openingBal, decimal application, decimal redemption, decimal adjustmentUp, decimal adjustmentDown, decimal movement, decimal closingUnits, decimal closingUnitPrice, decimal closingBal, decimal closingBalAdj, decimal movementAdj)
        {
            DataRow secSumaryRow = securitySummaryTable.NewRow();
            secSumaryRow[securitySummaryTable.TYPE] = type;
            secSumaryRow[securitySummaryTable.SECNAME] = securityName;
            secSumaryRow[securitySummaryTable.DESCRIPTION] = description;
            secSumaryRow[securitySummaryTable.ACCOUTNO] = accountNo;
            secSumaryRow[securitySummaryTable.OPENINGUNITSBAL] = openingUnitsBal;
            secSumaryRow[securitySummaryTable.OPENINGUNITPRICE] = openingUnitPrice;
            secSumaryRow[securitySummaryTable.OPENINGBALANCE] = openingBal;
            secSumaryRow[securitySummaryTable.APPLICATION] = application;
            secSumaryRow[securitySummaryTable.REDEMPTION] = redemption;
            secSumaryRow[securitySummaryTable.ADJUSTMENTUP] = adjustmentUp;
            secSumaryRow[securitySummaryTable.ADJUSTMENTDOWN] = adjustmentDown;
            secSumaryRow[securitySummaryTable.MOVEMENT] = movement;
            secSumaryRow[securitySummaryTable.CLOSINGUNITS] = closingUnits;
            secSumaryRow[securitySummaryTable.CLOSINGUNITPRICE] = closingUnitPrice;
            secSumaryRow[securitySummaryTable.CLOSINGBAL] = closingBal;
            secSumaryRow[securitySummaryTable.CLOSINGBALADJ] = closingBalAdj;
            secSumaryRow[securitySummaryTable.MOVEMENTADJ] = movementAdj;
            secSumaryRow[securitySummaryTable.PRICEATDATE] = priceAtDate;
            securitySummaryTable.Rows.Add(secSumaryRow);
        }

        public void AddTransDetailsRow(string securityCode, DateTime transactionDate, decimal units, decimal unitsBalance, string transType, string transFPSID)
        {
            DataRow secSumaryRow = transactiondetailstable.NewRow();

            secSumaryRow[transactiondetailstable.SECCODE] = securityCode;
            secSumaryRow[transactiondetailstable.TRANSTYPE] = transType;

            secSumaryRow[transactiondetailstable.TRANSDISID] = transFPSID;
            secSumaryRow[transactiondetailstable.UNITS] = units;
            secSumaryRow[transactiondetailstable.UNITSBALANCE] = unitsBalance;
            secSumaryRow[transactiondetailstable.TRANSACTIONDATE] = transactionDate;

            transactiondetailstable.Rows.Add(secSumaryRow);
        }

        public void AddMISTransDetailsRow(string secDesc, string securityCode, DateTime transactionDate, decimal unitsAmt, decimal unitsBalanceAmt, decimal unitPrice, decimal units, decimal unitsBalance, string transType, string transFPSID)
        {
            DataRow secSumaryRow = misTransactionDetailsTable.NewRow();

            secSumaryRow[misTransactionDetailsTable.SECCODE] = securityCode;
            secSumaryRow[misTransactionDetailsTable.SECCODEDESC] = secDesc;
            secSumaryRow[misTransactionDetailsTable.TRANSTYPE] = transType;
            secSumaryRow[misTransactionDetailsTable.TRANSTYPE] = transType;
            secSumaryRow[misTransactionDetailsTable.UNITSAMT] = unitsAmt;
            secSumaryRow[misTransactionDetailsTable.UNITSAMTBALANCE] = unitsBalanceAmt;
            secSumaryRow[misTransactionDetailsTable.UNITSPRICE] = unitPrice;

            secSumaryRow[misTransactionDetailsTable.UNITS] = units;
            secSumaryRow[misTransactionDetailsTable.UNITSBALANCE] = unitsBalance;
            secSumaryRow[misTransactionDetailsTable.TRANSACTIONDATE] = transactionDate;

            misTransactionDetailsTable.Rows.Add(secSumaryRow);
        }


        public void AddSecuritySummaryRowBank(string type, string securityName, string description, DateTime priceAtDate
                       , decimal openingBal, decimal application, decimal redemption, decimal adjustmentUp, decimal adjustmentDown, decimal movement, decimal closingBal, string accountNO, string bsb, string custNo, decimal closingBalAdj, decimal movementAdj)
        {
            DataRow secSumaryRow = securitySummaryTable.NewRow();
            secSumaryRow[securitySummaryTable.TYPE] = type;
            secSumaryRow[securitySummaryTable.SECNAME] = securityName;
            secSumaryRow[securitySummaryTable.DESCRIPTION] = description;
            secSumaryRow[securitySummaryTable.OPENINGBALANCE] = openingBal;
            secSumaryRow[securitySummaryTable.APPLICATION] = application;
            secSumaryRow[securitySummaryTable.REDEMPTION] = redemption;
            secSumaryRow[securitySummaryTable.ADJUSTMENTUP] = adjustmentUp;
            secSumaryRow[securitySummaryTable.ADJUSTMENTDOWN] = adjustmentDown;
            secSumaryRow[securitySummaryTable.MOVEMENT] = movement;
            secSumaryRow[securitySummaryTable.MOVEMENTADJ] = movementAdj;
            secSumaryRow[securitySummaryTable.CLOSINGBAL] = closingBal;
            secSumaryRow[securitySummaryTable.CLOSINGBALADJ] = closingBalAdj;
            secSumaryRow[securitySummaryTable.PRICEATDATE] = priceAtDate;
            secSumaryRow[securitySummaryTable.ACCOUTNO] = accountNO;
            secSumaryRow[securitySummaryTable.BSB] = bsb;
            secSumaryRow[securitySummaryTable.CUSTNO] = custNo;
            securitySummaryTable.Rows.Add(secSumaryRow);
        }

        public OrganizationUnit Unit{get; set; }
        public int Command{get; set; }
        public int Count{get; set; }

        public ClientInvementIntegrityTable GetCapitalTransTableToInvestmentIntegrityTable()
        {
            var dt=new ClientInvementIntegrityTable();
            var clientRows = Tables[CAPITALTRANTABLE].AsEnumerable().GroupBy(r => new
             { 
                 ClientCid = r.Field<Guid>(CLIENTCID),
                 ClientCode = r.Field<string>(CLIENTID),
                 ClientName = r.Field<string>(CLIENTNAME),
                 ClientServiceType   = r.Field<string>(SERVICETYPE),
                 Month   =new DateTime(r.Field<DateTime>(TRADEDATE).Year,r.Field<DateTime>(TRADEDATE).Month,1),
                 MonthYear   =r.Field<DateTime>(TRADEDATE).ToString("MMM-yyyy")
             });

            foreach (var clientRow in clientRows)
            {
                var dr=dt.NewRow();
                dr[dt.CLIENTCID] = clientRow.Key.ClientCid;
                dr[dt.CLIENTNAME] = clientRow.Key.ClientName;
                dr[dt.CLIENTCODE] = clientRow.Key.ClientCode;
                dr[dt.SERVICETYPE] = clientRow.Key.ClientServiceType.ToUpper();
                dr[dt.TRANSACTIONDATE] = clientRow.Key.Month;
                dr[dt.MONTHYEAR] = clientRow.Key.MonthYear;
                dr[dt.INVETMENTAMOUNT] = decimal.Round(clientRow.Sum(s => s.Field<decimal>(GROSSVAL)),2);
                dt.Rows.Add(dr);
            }
            return dt;
        }

    }

    public class TransactionDetailsTable : DataTable
    {
        private readonly string TRANSACTIONDETAILS = "TransactionDetails";
        public readonly string TRANSDISID = "TransFPSID";
        public readonly string SECCODE = "Code";
        public readonly string TRANSTYPE = "TransactionType";
        public readonly string UNITS = "Units";
        public readonly string UNITSBALANCE = "UnitsBalance";
        public readonly string TRANSACTIONDATE = "TradeDate";
        internal TransactionDetailsTable()
        {

            TableName = TRANSACTIONDETAILS;
            Columns.Add(SECCODE, typeof(string));
            Columns.Add(TRANSTYPE, typeof(string));
            Columns.Add(TRANSDISID, typeof(string));
            Columns.Add(UNITS, typeof(decimal));
            Columns.Add(UNITSBALANCE, typeof(decimal));
            Columns.Add(TRANSACTIONDATE, typeof(DateTime));


        }
    }
    public class MISTransactionDetailsTable : DataTable
    {
        private readonly string MISTRANSACTIONDETAILS = "MISTransactionDetails";

        public readonly string SECCODE = "Code";
        public readonly string SECCODEDESC = "CodeDesc";
        public readonly string TRANSTYPE = "TransactionType";
        public readonly string UNITSAMT = "UnitsAmt";
        public readonly string UNITSAMTBALANCE = "UnitsAmtBalance";
        public readonly string UNITSPRICE = "UnitsPrice";
        public readonly string UNITS = "Units";
        public readonly string UNITSBALANCE = "UnitsBalance";
        public readonly string TRANSACTIONDATE = "TradeDate";

        internal MISTransactionDetailsTable()
        {
            TableName = MISTRANSACTIONDETAILS;
            Columns.Add(SECCODE, typeof(string));
            Columns.Add(SECCODEDESC, typeof(string));
            Columns.Add(TRANSTYPE, typeof(string));
            Columns.Add(UNITSAMT, typeof(decimal));
            Columns.Add(UNITSAMTBALANCE, typeof(decimal));
            Columns.Add(UNITSPRICE, typeof(decimal));
            Columns.Add(UNITS, typeof(decimal));
            Columns.Add(UNITSBALANCE, typeof(decimal));
            Columns.Add(TRANSACTIONDATE, typeof(DateTime));
        }
    }
    public class SecuritySummaryTable : DataTable
    {
        private readonly string HOLDINGSUMMARYTABLE = "HoldingSummary";

        public readonly string SECNAME = "SecurityName";
        public readonly string TYPE = "Type";
        public readonly string OPENINGUNITSBAL = "OpeningBalanceUnits";
        public readonly string OPENINGUNITPRICE = "OpeningUnitPrice";
        public readonly string OPENINGBALANCE = "OpeningBalance";
        public readonly string APPLICATION = "Application";
        public readonly string REDEMPTION = "Redemption";
        public readonly string ADJUSTMENTUP = "AdjustmentUp";
        public readonly string ADJUSTMENTDOWN = "AdjustmentDown";
        public readonly string MOVEMENT = "Movement";
        public readonly string MOVEMENTADJ = "MovementAdj";
        public readonly string DESCRIPTION = "Description";
        public readonly string CLOSINGUNITS = "ClosingUnits";
        public readonly string CLOSINGUNITPRICE = "ClosingUnitPrice";
        public readonly string PRICEATDATE = "PriceAtDate";
        public readonly string CLOSINGBAL = "ClosingBalance";
        public readonly string CLOSINGBALADJ = "ClosingBalanceAdj";
        public readonly string ACCOUTNO = "AccountNo";
        public readonly string BSB = "BSB";
        public readonly string CUSTNO = "CustNo";

        internal SecuritySummaryTable()
        {
            TableName = HOLDINGSUMMARYTABLE;
            Columns.Add(SECNAME, typeof(string));
            Columns.Add(TYPE, typeof(string));
            Columns.Add(DESCRIPTION, typeof(string));
            Columns.Add(ACCOUTNO, typeof(string));
            Columns.Add(CUSTNO, typeof(string));
            Columns.Add(BSB, typeof(string));
            Columns.Add(OPENINGUNITSBAL, typeof(decimal));
            Columns.Add(OPENINGUNITPRICE, typeof(decimal));
            Columns.Add(OPENINGBALANCE, typeof(decimal));
            Columns.Add(APPLICATION, typeof(decimal));
            Columns.Add(REDEMPTION, typeof(decimal));
            Columns.Add(ADJUSTMENTUP, typeof(decimal));
            Columns.Add(ADJUSTMENTDOWN, typeof(decimal));
            Columns.Add(MOVEMENT, typeof(decimal));
            Columns.Add(CLOSINGUNITS, typeof(decimal));
            Columns.Add(CLOSINGUNITPRICE, typeof(decimal));
            Columns.Add(CLOSINGBAL, typeof(decimal));
            Columns.Add(CLOSINGBALADJ, typeof(decimal));
            Columns.Add(MOVEMENTADJ, typeof(decimal));
            Columns.Add(PRICEATDATE, typeof(DateTime));
        }
    }

    public class ClientInvementIntegrityTable : DataTable
    {
        private readonly string Name = "ClientInvementIntegrity";
        public readonly string CLIENTCID = "CLientCID";
        public readonly string CLIENTNAME = "CLientName";
        public readonly string CLIENTCODE = "ClientCode";
        public readonly string SERVICETYPE = "ServiceType";
        public readonly string INVETMENTAMOUNT = "InvetmentAmount";
        public readonly string TRANSACTIONDATE = "TradeDate";
        public readonly string MONTHYEAR = "MonthYear";
        internal ClientInvementIntegrityTable()
        {
            
            TableName = Name;
            Columns.Add(CLIENTCID, typeof(Guid));
            Columns.Add(CLIENTNAME, typeof(string));
            Columns.Add(CLIENTCODE, typeof(string));
            Columns.Add(SERVICETYPE, typeof(string));
            Columns.Add(INVETMENTAMOUNT, typeof(decimal));
            Columns.Add(TRANSACTIONDATE, typeof(DateTime));
            Columns.Add(MONTHYEAR, typeof(string));
        }
    }
    

}
