﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
   
    public class ClientIndividualShareDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public ClientShare ClientShareTable = new ClientShare();

        public ClientIndividualShareDS()
        {
            Tables.Add(ClientShareTable);
        }


        public OrganizationUnit Unit { get; set; }

        public int Command { get; set; }
        public IndividualsType IndividualsType { get; set; }
    }

    public class ClientShare : DataTable
    {
        private readonly string TABLENAME = "ClientShareList";
        public readonly string CLIENTID = "ClientId";
        public readonly string CLIENTCID = "ClientCid";
        public readonly string CLIENTCLID = "ClientCLid";
        public readonly string CLIENTCSID = "ClientCSid";


        public readonly string SHAREPERCENT = "SharePercent";

        internal ClientShare()
        {
            TableName = TABLENAME;
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(CLIENTCID, typeof(Guid));
            Columns.Add(CLIENTCLID, typeof(Guid));
            Columns.Add(CLIENTCSID, typeof(Guid));
            Columns.Add(SHAREPERCENT, typeof(decimal));
           
        }
    }
}
