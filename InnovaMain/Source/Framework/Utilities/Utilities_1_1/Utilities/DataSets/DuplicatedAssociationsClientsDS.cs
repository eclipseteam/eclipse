﻿using System;
using System.Collections.Generic;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public partial class DuplicatedAssociationsClientsDS : BaseClientDetails
    {
        public const string DUPASSOCIATIONTABLE = "DUPASSOCIATIONTABLE";
        public const string PRODUCTID = "PRODUCTID";
        public const string PRODUCTNAME = "PRODUCTNAME";
        public const string PRODUCTDESC = "PRODUCTDESC";
        public const string CLIENTCID = "CLIENTCID";
        public const string HOLDINGTOTAL = "HOLDINGTOTAL";

        public DuplicatedAssociationsClientsDS()
        {

            DataTable dt = new DataTable(DUPASSOCIATIONTABLE);
            dt.Columns.Add(PRODUCTID, typeof(Guid));
            dt.Columns.Add(PRODUCTNAME, typeof(string));
            dt.Columns.Add(PRODUCTDESC, typeof(string));
            dt.Columns.Add(CLIENTID, typeof(string));
            dt.Columns.Add(CLIENTNAME, typeof(string));
            dt.Columns.Add(CLIENTCID, typeof(Guid));
            dt.Columns.Add(HOLDINGTOTAL, typeof(decimal));
            Tables.Add(dt);
       }
    }
}
