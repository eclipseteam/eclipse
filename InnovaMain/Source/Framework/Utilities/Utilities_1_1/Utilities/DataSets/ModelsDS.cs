﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public enum ModelDataSetOperationType
    {
        None = 0,
        NewAsset = 1,
        NewProduct = 2,
        DeleteAsset = 3,
        DeleteProduct = 4,
        NewModel = 5,
        DeleteModel = 6,
        UpdateModel = 7,
        UpdateProduct = 8,
        ReCalculationAllocation = 9,
        BulkUpdate = 10,
        ModelExport = 11
    }

    public class ModelsDS : DataSet
    {
        public Double ProMinAllocation = 0;
        public Double ProNeutralPercentage = 0;
        public Double ProDynamicPercentage = 0;
        public Double ProMaxAllocation = 0;

        public ModelDataSetOperationType ModelDataSetOperationType = ModelDataSetOperationType.None;

        public bool GetDetails = false;
        public Guid DetailsGUID = Guid.Empty;
        public Guid AssetEntityID = Guid.Empty;
        public Guid ProductEntityID = Guid.Empty;
        public Guid ModelEntityID = Guid.Empty;

        public const string MODELTABLE = "ModelTable";
        public const string MODELDETAILSLIST = "ModelsDetailsList";
        public const string MODELLIST = "ModelsList";
        public const string ID = "ID";
        public const string DESCRIPTION = "Description";
        public const string DYNAMICPERCENTAGE = "DynamicPercentage";
        public const string MAXALLOCATION = "MaxAllocation";
        public const string MINALLOCATION = "MinAllocation";
        public const string MODELNAME = "ModelName";
        public const string PROGRAMCODE = "ProgramCode";
        public const string SHAREPERCENTAGE = "NeutralPercentage";
        public const string SERVICETYPE = "ServiceType";
        public const string ISPRIVATE = "IsPrivate";
        public const string HASERRORS = "HasErrors";
        

        public const string MODELDETAILSLISTASSETS = "ModelsDetailsListAssets";
        public const string MODELDETAILSLISTPRO = "ModelsDetailsListPro";
        public const string ASSETDESCRIPTION = "AssetDescription";
        public const string ASSETID = "AssetID";
        public const string ASSETDYNAMICPERCENTAGE = "AssetDynamicPercentage";
        public const string ASSETSHAREPERCENTAGE = "AssetNeutralPercentage";
        public const string ASSETMAXALLOCATION = "AssetMaxAllocation";
        public const string ASSETMINALLOCATION = "AssetMinAllocation";
        public const string ASSETMODELNAME = "AssetName";
        public const string PRODESCRIPTION = "ProDescription";
        public const string PROID = "ProID"; 
        public const string PRODYNAMICPERCENTAGE = "ProDynamicPercentage";
        public const string PROSHAREPERCENTAGE = "ProNeutralPercentage";
        public const string PROMAXALLOCATION = "ProMaxAllocation";
        public const string PROMINALLOCATION = "ProMinAllocation";
        public const string PROMODELNAME = "ProName";

        public const string MODELLISTTABLEASSETPRODUCTS = "MODELLISTTABLEASSETPRODUCTS"; 
        

        public ModelsDS()
        {
            this.Tables.Add(ModelsListTable());
            this.Tables.Add(ModelsListTableWithAssPro());
            this.Tables.Add(ModelTable());
            this.Tables.Add(ModelsDetailsListTableAssets());
            this.Tables.Add(ModelsDetailsListTableAssetsProducts());
            this.Tables.Add(ModelsDetailsListTable());
        }

        public DataTable ModelTable()
        {
            DataTable ModelListTable = new DataTable(MODELTABLE);
            ModelListTable.Columns.Add(ID, typeof(Guid));
            ModelListTable.Columns.Add(PROGRAMCODE, typeof(string));
            ModelListTable.Columns.Add(MODELNAME, typeof(string));
            ModelListTable.Columns.Add(SERVICETYPE, typeof(ServiceTypes));
            ModelListTable.Columns.Add(ISPRIVATE, typeof(bool));
           
            
            return ModelListTable;
        }

        public DataTable ModelsListTableWithAssPro()
        {
            DataTable ModelListTable = new DataTable(MODELLIST);
            ModelListTable.Columns.Add(ID, typeof(Guid));
            ModelListTable.Columns.Add(PROGRAMCODE, typeof(string));
            ModelListTable.Columns.Add(MODELNAME, typeof(string));
            ModelListTable.Columns.Add(DESCRIPTION, typeof(string));
            ModelListTable.Columns.Add(DYNAMICPERCENTAGE, typeof(decimal));
            ModelListTable.Columns.Add(MAXALLOCATION, typeof(decimal));
            ModelListTable.Columns.Add(MINALLOCATION, typeof(decimal));
            ModelListTable.Columns.Add(SHAREPERCENTAGE, typeof(decimal));
            ModelListTable.Columns.Add(SERVICETYPE, typeof(string));
            ModelListTable.Columns.Add(ISPRIVATE, typeof(bool));
            ModelListTable.Columns.Add(HASERRORS, typeof(string));
            return ModelListTable;
        }

        public DataTable ModelsListTable()
        {
            DataTable ModelListTable = new DataTable(MODELLISTTABLEASSETPRODUCTS);
            ModelListTable.Columns.Add(ID, typeof(Guid));
            ModelListTable.Columns.Add(PROGRAMCODE, typeof(string));
            ModelListTable.Columns.Add(MODELNAME, typeof(string));
            ModelListTable.Columns.Add(DESCRIPTION, typeof(string));
            ModelListTable.Columns.Add(DYNAMICPERCENTAGE, typeof(decimal));
            ModelListTable.Columns.Add(MAXALLOCATION, typeof(decimal));
            ModelListTable.Columns.Add(MINALLOCATION, typeof(decimal));
            ModelListTable.Columns.Add(SHAREPERCENTAGE, typeof(decimal));
            ModelListTable.Columns.Add(SERVICETYPE, typeof(string));
            ModelListTable.Columns.Add(ISPRIVATE, typeof(bool));
            ModelListTable.Columns.Add(ASSETMODELNAME, typeof(string));
            ModelListTable.Columns.Add(ASSETDESCRIPTION, typeof(string));
            ModelListTable.Columns.Add(ASSETSHAREPERCENTAGE, typeof(decimal));
            ModelListTable.Columns.Add(ASSETDYNAMICPERCENTAGE, typeof(decimal));
            ModelListTable.Columns.Add(ASSETMINALLOCATION, typeof(decimal));
            ModelListTable.Columns.Add(ASSETMAXALLOCATION, typeof(decimal));
            ModelListTable.Columns.Add(ASSETID, typeof(Guid));
            ModelListTable.Columns.Add(PROID, typeof(Guid));
            ModelListTable.Columns.Add(PROMODELNAME, typeof(string));
            ModelListTable.Columns.Add(PRODESCRIPTION, typeof(string));
            ModelListTable.Columns.Add(PROSHAREPERCENTAGE, typeof(decimal));
            ModelListTable.Columns.Add(PRODYNAMICPERCENTAGE, typeof(decimal));
            ModelListTable.Columns.Add(PROMINALLOCATION, typeof(decimal));
            ModelListTable.Columns.Add(PROMAXALLOCATION, typeof(decimal));

            return ModelListTable;
        }

        public DataTable ModelsDetailsListTableAssets()
        {
            DataTable ModelListTable = new DataTable(MODELDETAILSLISTASSETS);

            ModelListTable.Columns.Add(ASSETMODELNAME, typeof(string));
            ModelListTable.Columns.Add(ASSETDESCRIPTION, typeof(string));
            ModelListTable.Columns.Add(ASSETSHAREPERCENTAGE, typeof(decimal));
            ModelListTable.Columns.Add(ASSETDYNAMICPERCENTAGE, typeof(decimal));
            ModelListTable.Columns.Add(ASSETMINALLOCATION, typeof(decimal));
            ModelListTable.Columns.Add(ASSETMAXALLOCATION, typeof(decimal));
            ModelListTable.Columns.Add(ASSETID, typeof(Guid));
           
            return ModelListTable;

        }

        public DataTable ModelsDetailsListTableAssetsProducts()
        {
            DataTable ModelListTable = new DataTable(MODELDETAILSLISTPRO);

            ModelListTable.Columns.Add(PROID, typeof(Guid));
            ModelListTable.Columns.Add(ASSETID, typeof(Guid));
            ModelListTable.Columns.Add(PROMODELNAME, typeof(string));
            ModelListTable.Columns.Add(PRODESCRIPTION, typeof(string));
            ModelListTable.Columns.Add(PROSHAREPERCENTAGE, typeof(decimal));
            ModelListTable.Columns.Add(PRODYNAMICPERCENTAGE, typeof(decimal));
            ModelListTable.Columns.Add(PROMINALLOCATION, typeof(decimal));
            ModelListTable.Columns.Add(PROMAXALLOCATION, typeof(decimal));

            return ModelListTable;

        }

        public DataTable ModelsDetailsListTable()
        {

            DataTable ModelListTable = new DataTable(MODELDETAILSLIST);

            ModelListTable.Columns.Add(ASSETMODELNAME, typeof(string));
            ModelListTable.Columns.Add(ASSETDESCRIPTION, typeof(string));
            ModelListTable.Columns.Add(ASSETSHAREPERCENTAGE, typeof(decimal));
            ModelListTable.Columns.Add(ASSETDYNAMICPERCENTAGE, typeof(decimal));
            ModelListTable.Columns.Add(ASSETMINALLOCATION, typeof(decimal));
            ModelListTable.Columns.Add(ASSETMAXALLOCATION, typeof(decimal));

            ModelListTable.Columns.Add(PROMODELNAME, typeof(string));
            ModelListTable.Columns.Add(PRODESCRIPTION, typeof(string));
            ModelListTable.Columns.Add(PROSHAREPERCENTAGE, typeof(decimal));
            ModelListTable.Columns.Add(PRODYNAMICPERCENTAGE, typeof(decimal));
            ModelListTable.Columns.Add(PROMINALLOCATION, typeof(decimal));
            ModelListTable.Columns.Add(PROMAXALLOCATION, typeof(decimal));

            return ModelListTable;
        }
    }
}
