﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class PersonalDetailsDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public PersonalDetailsTable PersonalDetailsTable = new PersonalDetailsTable();

        public PersonalDetailsDS()
        {
            Tables.Add(PersonalDetailsTable);
        }

        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
    }

    public class PersonalDetailsTable : DataTable
    {
        private readonly string TABLENAME = "PersonalDetailsList";
        public readonly string ID = "Id";
        public readonly string NAME = "Name";
        public readonly string AGE = "Age";
        public readonly string DOB = "DOB";
        public readonly string ADVICESID = "AdvicesId";
        public readonly string RETIREMENTAGE = "RetirementAge";
        public readonly string SALARY = "Salary";
        public readonly string PLANAGE = "PlanAge";
        public readonly string OUTPUTFILENAME = "OutputFileName";
        public readonly string DIRECTORYTOSAVERESULTS = "DirectoryToSaveResults";
        public readonly string SYSTEMRUNNUMBER = "SystemRunNumber";


        internal PersonalDetailsTable()
        {
            TableName = TABLENAME;
            Columns.Add(ID, typeof(Guid));
            Columns.Add(NAME, typeof(string));
            Columns.Add(AGE, typeof(int));
            Columns.Add(DOB, typeof(DateTime));
            Columns.Add(ADVICESID, typeof(Guid));
            Columns.Add(RETIREMENTAGE, typeof(int));
            Columns.Add(SALARY, typeof(decimal));
            Columns.Add(PLANAGE, typeof(int));
            Columns.Add(OUTPUTFILENAME, typeof(string));
            Columns.Add(DIRECTORYTOSAVERESULTS, typeof(string));
            Columns.Add(SYSTEMRUNNUMBER, typeof(string));
        }
    }
}
