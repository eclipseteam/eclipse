﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class OrderPadAtCallRatesDS : DataSet
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public OrderPadAtCallRatesTable OrderPadAtCallRatesTable = new OrderPadAtCallRatesTable();

        public Guid MainInstituteId = Guid.Empty;

        public OrderPadAtCallRatesDS()
        {
            Tables.Add(OrderPadAtCallRatesTable);
        }
    }

    public class OrderPadAtCallRatesTable : DataTable
    {
        private readonly string TABLENAME = "OrderPadAtCallRates";
        public readonly string ID = "ID";
        public readonly string INSTITUTEID = "InstituteID";
        public readonly string INSTITUTENAME = "InstituteName";
        public readonly string PRICEINSTITUTEID = "PriceInstituteID";
        public readonly string PRICEINSTITUTENAME = "PriceInstituteName";
        public readonly string ACCOUNTTIER = "AccountTier";
        public readonly string MIN = "Min";
        public readonly string MAX = "Max";
        public readonly string RATE = "Rate";
        public readonly string MAXIMUMBROKERAGE = "MaximumBrokerage";
        public readonly string TYPE = "Type";
        public readonly string APPLICABLEFROM = "ApplicableFrom";
        public readonly string DATE = "Date";
        public readonly string ISEDITABLE = "IsEditable";
        public readonly string APPLICABILITY = "Applicability";
        public readonly string HONEYMOONPERIOD = "HoneymoonPeriod";

        internal OrderPadAtCallRatesTable()
        {
            TableName = TABLENAME;
            Columns.Add(ID, typeof(Guid));
            Columns.Add(INSTITUTEID, typeof(Guid));
            Columns.Add(INSTITUTENAME, typeof(string));
            Columns.Add(PRICEINSTITUTEID, typeof(Guid));
            Columns.Add(PRICEINSTITUTENAME, typeof(string));
            Columns.Add(ACCOUNTTIER, typeof(string));
            Columns.Add(MIN, typeof(decimal));
            Columns.Add(MAX, typeof(decimal));
            Columns.Add(RATE, typeof(string));
            Columns.Add(MAXIMUMBROKERAGE, typeof(decimal));
            Columns.Add(TYPE, typeof(string));
            Columns.Add(APPLICABLEFROM, typeof(DateTime));
            Columns.Add(APPLICABILITY, typeof(string));
            Columns.Add(DATE, typeof(DateTime));
            Columns.Add(HONEYMOONPERIOD, typeof(decimal));
            var dc = Columns.Add(ISEDITABLE, typeof(bool));
            dc.DefaultValue = false;

        }
    }
}
