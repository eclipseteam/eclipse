﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;

namespace Oritax.TaxSimp.DataSets
{
    public class ManualTransactionDetailsDS : UMABaseDS
    {
        public string CLIENTID = string.Empty;
        public string CLIENTNAME = string.Empty; 

        public const string MANUALTRANSDETAILSTABLE = "ManualTransactionDetailsTables";
        public const string ID = "Id";

        public const string TRANTYPE = "TranType";
        public const string UNITSPRICE = "UnitPrice";
        public const string HOLDINGS = "Holdings";
        public const string UNITSONHAND = "UnitsOnHand";

        public const string INVESTCODE = "InvestmentCode";
        public const string INVESTID = "InvestmentID";
        public const string INVESTDESC = "InvestmentDesc";

        public const string SETTLEDATE = "SettlementDate";
        public const string TRANDATE = "TransactionDate";
        public const string PRICETYPE = "PRICETYPE";

        public ManualTransactionDetailsDS()
        {
            DataTable dt = new DataTable(MANUALTRANSDETAILSTABLE);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(TRANTYPE, typeof(string));
            dt.Columns.Add(UNITSPRICE, typeof(decimal));
            dt.Columns.Add(HOLDINGS, typeof(decimal));
            dt.Columns.Add(UNITSONHAND, typeof(decimal));
            dt.Columns.Add(INVESTCODE, typeof(string));
            dt.Columns.Add(INVESTDESC, typeof(string));
            dt.Columns.Add(INVESTID, typeof(Guid));
            dt.Columns.Add(PRICETYPE, typeof(PriceType));
            dt.Columns.Add(SETTLEDATE, typeof(DateTime));
            dt.Columns.Add(TRANDATE, typeof(DateTime));
            this.Tables.Add(dt); 
        }
    }
}
