﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class TermDepositDS : DataSet,IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public TermDeposit TermDepositTable = new TermDeposit();

        public TermDepositDS()
        {
            Tables.Add(TermDepositTable);
        }


        public OrganizationUnit Unit { get; set; }

        public int Command{ get; set; }
       
    }

    public class TermDeposit : DataTable
    {
        public readonly string TABLENAME = "TermDepositList";
        public readonly string CID = "Cid";
        public readonly string CLID = "CLid";
        public readonly string CSID = "CSid";
        public readonly string ACCOUNTNAME = "AccountName";
        public readonly string ACCOUNTTYPE = "AccountType";
        public readonly string BSBNO = "BSBNo";
        public readonly string ACCOUNTNO = "AccountNo";
        public readonly string INSTITUTION = "Institution";
        public readonly string INSTITUTIONID = "InstitutionId";
        public readonly string ACCOUNTTYPEOTHER = "AccountTypeOther";
        public readonly string BROKER = "Broker";
        public readonly string BROKERID = "BrokerId";
        public readonly string STATUS = "Status";
        public readonly string ISSELECTED = "IsSelected";
        internal TermDeposit()
        {
            TableName = TABLENAME;
            Columns.Add(CID, typeof(Guid));
            Columns.Add(CLID, typeof(Guid));
            Columns.Add(CSID, typeof(Guid));
            Columns.Add(ACCOUNTNAME, typeof(string));
            Columns.Add(ACCOUNTTYPE, typeof(string));
            Columns.Add(BSBNO, typeof(string));
            Columns.Add(ACCOUNTNO, typeof(string));
            Columns.Add(INSTITUTION, typeof(string));
            Columns.Add(INSTITUTIONID, typeof(Guid));
            Columns.Add(ACCOUNTTYPEOTHER, typeof(string));
            Columns.Add(BROKER, typeof(string));
            Columns.Add(BROKERID, typeof(Guid));
            Columns.Add(STATUS, typeof(string));
          var dc=  Columns.Add(ISSELECTED, typeof(bool));
            dc.DefaultValue = false;
        }
    }
}
