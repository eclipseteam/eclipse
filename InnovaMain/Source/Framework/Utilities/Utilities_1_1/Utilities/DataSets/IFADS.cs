﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class IFADS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public IFATable ifaTable = new IFATable();

        public IFADS()
        {
            Tables.Add(ifaTable);
        }


        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
    }

    public class IFATable : DataTable
    {
        private readonly string TABLENAME = "DealerGroupList";
        public readonly string CLIENTID = "ClientID";
        public readonly string TRADINGNAME = "TradingName";
        public readonly string TYPE = "Type";
        public readonly string STATUS = "Status";

        public readonly string IFATYPE = "IFAType";
        public readonly string NAME = "Name";
        public readonly string LEGALNAME = "LegalName";
        public readonly string FACSIMILE = "Facsimile";
        public readonly string FACSIMILECITYCODE = "FacsimileCityCode";
        public readonly string FACSIMILECOUNTRYCODE = "FacsimileCountryCode";

        public readonly string ADDRESS = "Address";
        public string PHONENUMBERCOUNTRYCODE = "PhoneNumberCountryCode";
        public string PHONENUMBERCITYCODE = "PhoneNumberCityCode";

        public readonly string PHONENUBER = "PhoneNumber";
        public readonly string ACN = "ACN";
        public readonly string WEBSITEADDRESS = "WebSiteAddress";
        public readonly string ABN = "ABN";
        public readonly string FUM = "FUM";
        public readonly string TURNOVER = "TurnOver";
        public readonly string SKCODE = "SKCode";
        public readonly string LASTAUDITEDDATE = "LastAuditedDate";

        public readonly string ACCOUNTDESIGNATION = "AccountDesignation";
        public readonly string TFN = "TFN";
        public readonly string PREVIOUSPRACTICE = "PreviousPractice";

        public readonly string CID = "Cid";
        public readonly string CLID = "Clid";
        public readonly string CSID = "Csid";
        public readonly string EMAIL="Email";


        internal IFATable()
        {
            TableName = TABLENAME;
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(TRADINGNAME, typeof(string));
            Columns.Add(TYPE, typeof(string));
            Columns.Add(STATUS, typeof(string));

            Columns.Add(IFATYPE, typeof(string));
            Columns.Add(NAME, typeof(string));
            Columns.Add(LEGALNAME, typeof(string));

            Columns.Add(FACSIMILECOUNTRYCODE, typeof(string));
            Columns.Add(FACSIMILECITYCODE, typeof(string));
            Columns.Add(FACSIMILE, typeof(string));


            Columns.Add(ADDRESS, typeof(string));
            Columns.Add(PHONENUMBERCOUNTRYCODE, typeof(string));
            Columns.Add(PHONENUMBERCITYCODE, typeof(string));
            Columns.Add(PHONENUBER, typeof(string));
            Columns.Add(ACN, typeof(string));
            Columns.Add(WEBSITEADDRESS, typeof(string));
            Columns.Add(EMAIL, typeof(string));
            Columns.Add(ABN, typeof(string));
            Columns.Add(FUM, typeof(string));
            Columns.Add(TURNOVER, typeof(string));
            Columns.Add(SKCODE, typeof(string));
            Columns.Add(LASTAUDITEDDATE, typeof(string));

            Columns.Add(ACCOUNTDESIGNATION, typeof(String));
            Columns.Add(TFN, typeof(String));
            Columns.Add(PREVIOUSPRACTICE, typeof(String));

            Columns.Add(CID, typeof(Guid));
            Columns.Add(CLID, typeof(Guid));
            Columns.Add(CSID, typeof(Guid));

        }
    }
}
