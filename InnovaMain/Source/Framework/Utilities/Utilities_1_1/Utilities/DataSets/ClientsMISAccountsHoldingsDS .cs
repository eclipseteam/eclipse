﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class ClientsMISAccountsHoldingsDS : DataSet, IHasOrganizationUnit
    {
    
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public MISHoldingTable MisHoldingTable = new MISHoldingTable();
        public ClientsMISAccountsHoldingsDS()
        {
            Tables.Add(MisHoldingTable);
        }
        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
    }
    public class MISHoldingTable: DataTable
    {
        private readonly string TABLENAME = "MISHoldingList";
        public readonly string ClientCID = "CID";
        public readonly string CLIENTID = "ClientID";
        public readonly string ENTITYCLIENTID = "EntityClientID";
        public readonly string TRANDINGNAME = "TrandingName";
        public readonly string SERVICETYPES = "ServiceTypes";
        public readonly string FUNDCODE = "FundCode";
        public readonly string UNITHOLDING = "UnitHolding";
        public readonly string TRANSACTIONUNITHOLDING = "TransactionUnitHolding";
        public readonly string DIFFERENCE = "Difference";
        public readonly string UNSETTLEDORDER = "UnsettledOrder";
        public readonly string TOTAL = "Total";



        internal MISHoldingTable()
        {
            TableName = TABLENAME;
            Columns.Add(ClientCID, typeof(Guid));
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(ENTITYCLIENTID, typeof(string));
            Columns.Add(TRANDINGNAME, typeof(string));
            Columns.Add(SERVICETYPES, typeof(string));
            Columns.Add(FUNDCODE, typeof(string));
            Columns.Add(UNITHOLDING, typeof(decimal));
            Columns.Add(TRANSACTIONUNITHOLDING, typeof(decimal));
            Columns.Add(DIFFERENCE, typeof(decimal));
            Columns.Add(UNSETTLEDORDER, typeof(decimal));
            Columns.Add(TOTAL, typeof(decimal));
            
        }



        public void AddNewRow(Guid clientCID,string clinetID,string entityClientID,string tradingName,string serviceTypes,string fundCode,decimal UnitHolding,decimal TransUnitHolding,decimal unsetteledOrder)
        {
            DataRow dr = NewRow();
            dr[ClientCID] = clientCID;
            dr[CLIENTID] = clinetID;
            dr[ENTITYCLIENTID] = entityClientID;
            dr[TRANDINGNAME] = tradingName;
            dr[SERVICETYPES] = serviceTypes;
            dr[FUNDCODE] = fundCode;
            dr[UNITHOLDING] = UnitHolding;
            dr[TRANSACTIONUNITHOLDING] = TransUnitHolding;
            dr[DIFFERENCE] =(UnitHolding-TransUnitHolding);
            dr[UNSETTLEDORDER] = unsetteledOrder;
            dr[TOTAL] = (UnitHolding+unsetteledOrder);

            Rows.Add(dr);

        }
    }
}
