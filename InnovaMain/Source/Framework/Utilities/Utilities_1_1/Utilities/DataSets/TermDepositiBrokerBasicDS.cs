﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class TermDepositBrokerBasicDS : DataSet,IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public TermDepositBasicTable TermDepositBasicTable = new TermDepositBasicTable();

        public TermDepositBrokerBasicDS()
        {
            Tables.Add(TermDepositBasicTable);
        }


        public OrganizationUnit Unit
        {
            get; set;
        }

        public int Command
        {
            get;
            set;
        }
    }

    public class TermDepositBasicTable : DataTable
    {
        public readonly string TABLENAME = "TermDepositList";
        public readonly string ID = "ID";
        public readonly string CID = "Cid";
        public readonly string CLID = "CLid";
        public readonly string CSID = "CSid";
        public readonly string BROKERNAME = "BrokerName";
        public readonly string ACCOUNTNUMBER = "accountNumber";
        public readonly string STATUS = "Status";
        
        internal TermDepositBasicTable()
        {
            TableName = TABLENAME;
            Columns.Add(ID, typeof(Guid));
            Columns.Add(CID, typeof(Guid));
            Columns.Add(CLID, typeof(Guid));
            Columns.Add(CSID, typeof(Guid));
            Columns.Add(BROKERNAME, typeof(string));
            Columns.Add(ACCOUNTNUMBER, typeof(string));
            Columns.Add(STATUS, typeof(string));
        }
    }
}
