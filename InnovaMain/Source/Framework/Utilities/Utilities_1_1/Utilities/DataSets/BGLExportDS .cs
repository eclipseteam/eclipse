﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.DataSets
{
    public class BGLExportDS : UMABaseDS
    {
        public string BGLAccountCode = "";
        public string BGLExportXMLString = "";
        public DateTime StartDate;
        public DateTime EndDate;
        public bool IsExport = false;
        public bool IsHoldingExport = false; 
        public BGLExportDS()
        {
            AccountingFinancialYear financialYear = new AccountingFinancialYear(DateTime.Now);

            this.StartDate = financialYear.FinYearStartDate;
            this.EndDate = financialYear.FinYearEndDate;
        }
    }
}
