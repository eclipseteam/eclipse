﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class BTWrapDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public BTWrapTable btwrapTable = new BTWrapTable();

        public BTWrapDS()
        {
            Tables.Add(btwrapTable);
        }


        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
    }

    public class BTWrapTable : DataTable
    {
        private readonly string TABLENAME = "BTWarpList";
        public readonly string CID = "CID";       
        public readonly string CLIENTID = "ClientID";
        public readonly string CLIENTNAME = "ClientName";
        public readonly string ACCOUNTNUMBER = "AccountNumber";
        public readonly string ACCOUNTDESCRIPTION = "AccountDescription";
        public readonly string BUSINESSGROUPID = "BusinessGroupID";
        public readonly string BUSINESSGROUPNAME = "BusinessGroupName";
        
        internal BTWrapTable()
        {
            TableName = TABLENAME;
            Columns.Add(CID, typeof(Guid));            
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(CLIENTNAME, typeof(string));
            Columns.Add(ACCOUNTNUMBER, typeof(string));
            Columns.Add(ACCOUNTDESCRIPTION, typeof(string));
            Columns.Add(BUSINESSGROUPID, typeof(Guid));
            Columns.Add(BUSINESSGROUPNAME, typeof(string));
        }
    }
}
