﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public partial class PerfReportDS : CapitalReportDS
    {
        public bool IsRange = false;
        public DateTime EndDateIfClosingZero = DateTime.Now;
        public DateTime StartDateIfOpeningZero = DateTime.Now;

        public bool HasDIFM = false;
        public bool HasDIWM = false;
        public bool HasDIY = false;

        public static string OVERALLPERFTABLE = "PerformanceTableOverall";
        public static string OVERALLPERFTABLEBYSEC = "PerformanceTableOverallBySec";
        public static string DIFMPERFTABLE = "PerformanceTableDIFM";
        public static string DIWMPERFTABLE = "PerformanceTableDIWM";
        public static string DIYPERFTABLE = "PerformanceTableDIY";
        public static string MANPERFTABLE = "PerformanceTableMAN";

        public static string CHAINLINKDATA = "ChainLinkData";
        public static string OTHER = "OTHER";

        public static string PERFSUMMARYTABLEBYSEC = "PerfSummaryTableBySec";
        public static string PERFSUMMARYTABLE = "PerfSummaryTable";
        public static string ONEMONTH = "1Month";
        public static string THREEMONTH = "3Month";
        public static string SIXMONTH = "6Month";
        public static string TWELVEMONTH = "12Month";
        public static string ONEYEAR = "1YEAR";
        public static string TWOYEAR = "2YEAR";
        public static string THREEYEAR = "3YEAR";
        public static string FIVEYEAR = "5YEAR";
        public static string PERPERIOD = "PERPERIOD";
        public static string TYPE = "TYPE";
        public static string MOVEMENTEXCINVEST = "MOVEMENTEXCINVEST";

        public static string PERPERIODDIFF = "PERPERIODDIFF";
        public static string ONEMONTHDIFF = "1MonthDIFF";
        public static string THREEMONTHDIFF = "3MonthDIFF";
        public static string SIXMONTHDIFF = "6MonthDIFF";
        public static string TWELVEMONTHDIFF = "12MonthDIFF";
        public static string ONEYEARDIFF = "1YEARDIFF";
        public static string TWOYEARDIFF = "2YEARDIFF";
        public static string THREEYEARDIFF = "3YEARDIFF";
        public static string FIVEYEARDIFF = "5YEARDIFF";

        public static string PERPERIODINCOME = "PERPERIODINCOME";
        public static string ONEMONTHINCOME = "1MonthINCOME";
        public static string THREEMONTHINCOME = "3MonthINCOME";
        public static string SIXMONTHINCOME = "6MonthINCOME";
        public static string TWELVEMONTHINCOME = "12MonthINCOME";
        public static string ONEYEARINCOME = "1YEARINCOME";
        public static string TWOYEARINCOME = "2YEARINCOME";
        public static string THREEYEARINCOME = "3YEARINCOME";
        public static string FIVEYEARINCOME = "5YEARINCOME";

        public static string PERFSUMMARYINCOMEGROWTHTABLE = "PerfSummaryTableIncomeGrowth";
        public static string PERFSUMMARYINCOMEGROWTHTABLEBYSEC = "PerfSummaryTableIncomeGrowthBySec";
        public static string STARTDATE = "StartDate";
        public static string ENDDATE = "EndDate";
        public static string MOVEMENTEXCLINCOME = "MovementExclIncome";

        public static string APPLICATION = "APPLICATION";
        public static string REDEMPTION = "REDEMPTION";
        public static string CHAINLINKDATAINCOME = "CHAINLINKDATAINCOME";
        public static string OVERALLGROWTH = "OVERALLGROWTH";
       
        public PerfReportDS()
        {
            PerformanceDetailIncomeGrowthTable();
            PerformanceDetailIncomeGrowthTableBySec();
            PerformanceDetailTable(OVERALLPERFTABLE);
            PerformanceDetailTable(OVERALLPERFTABLEBYSEC);
            PerformanceDetailTable(DIYPERFTABLE);
            PerformanceDetailTable(DIFMPERFTABLE);
            PerformanceDetailTable(DIWMPERFTABLE);
            PerformanceDetailTable(MANPERFTABLE);
            PerformanceSummaryTable();
            PerformanceSummaryTableBySec();
        }

        private void PerformanceDetailTable(string tablename)
        {
            DataTable PerformanceTable = new DataTable(tablename);
            PerformanceTable.Columns.Add(PerfReportDS.MONTH, typeof(DateTime));
            PerformanceTable.Columns.Add(PerfReportDS.INVESMENTCODE, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.TYPE, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.OPENINGBAL, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.APPLICATION, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.REDEMPTION, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.INCOME, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.MOVEMENTEXCLINCOME, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.TRANSFEROUT, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.EXPENSE, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.INTERNALCASHMOVEMENT, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.OTHER, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.CHANGEININVESTMENTVALUE, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.CLOSINGBALANCE, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.SUMWEIGHTCF, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.SUMCF, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.MODDIETZ, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.CHAINLINKDATA, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.OVERALLGROWTH, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.OVERALLRETURN, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.CHAINLINKDATAINCOME, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.INCOMERETURN, typeof(decimal));
            this.Tables.Add(PerformanceTable);
        }

        private void PerformanceDetailIncomeGrowthTable()
        {
            DataTable PerformanceTable = new DataTable(PERFSUMMARYINCOMEGROWTHTABLE);
            PerformanceTable.Columns.Add(PerfReportDS.DESCRIPTION, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.INVESMENTCODE, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.STARTDATE, typeof(DateTime));
            PerformanceTable.Columns.Add(PerfReportDS.ENDDATE, typeof(DateTime));
            PerformanceTable.Columns.Add(PerfReportDS.OPENINGBAL, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.INCOME, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.MOVEMENTEXCLINCOME, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.CHANGEININVESTMENTVALUE, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.CLOSINGBALANCE, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.SUMCF, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.INCOMERETURN, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.GROWTHRETURN, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.OVERALLRETURN, typeof(decimal));
           
            this.Tables.Add(PerformanceTable);
        }

        private void PerformanceDetailIncomeGrowthTableBySec()
        {
            DataTable PerformanceTable = new DataTable(PERFSUMMARYINCOMEGROWTHTABLEBYSEC);
            PerformanceTable.Columns.Add(PerfReportDS.DESCRIPTION, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.INVESMENTCODE, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.STARTDATE, typeof(DateTime));
            PerformanceTable.Columns.Add(PerfReportDS.ENDDATE, typeof(DateTime));
            PerformanceTable.Columns.Add(PerfReportDS.OPENINGBAL, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.APPLICATION, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.REDEMPTION, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.INCOME, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.MOVEMENTEXCLINCOME, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.CHANGEININVESTMENTVALUE, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.CLOSINGBALANCE, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.SUMCF, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.INCOMERETURN, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.GROWTHRETURN, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.OVERALLRETURN, typeof(decimal));

            this.Tables.Add(PerformanceTable);
        }

        private void PerformanceSummaryTable()
        {
            DataTable PerformanceTable = new DataTable(PERFSUMMARYTABLE);
            PerformanceTable.Columns.Add(PerfReportDS.DESCRIPTION, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.ONEMONTH, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.THREEMONTH, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.SIXMONTH, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.TWELVEMONTH, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.ONEYEAR, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.TWOYEAR, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.THREEYEAR, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.FIVEYEAR, typeof(string));

            this.Tables.Add(PerformanceTable);
        }

        private void PerformanceSummaryTableBySec()
        {
            DataTable PerformanceTable = new DataTable(PERFSUMMARYTABLEBYSEC);
            PerformanceTable.Columns.Add(PerfReportDS.DESCRIPTION, typeof(string));

            PerformanceTable.Columns.Add(PerfReportDS.OPENINGBAL, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.CLOSINGBALANCE, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.APPLICATION, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.REDEMPTION, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.INCOME, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.TRANSFEROUT, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.INTERNALCASHMOVEMENT, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.CHANGEININVESTMENTVALUE, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.MOVEMENTEXCINVEST, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.OTHER, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.EXPENSE, typeof(decimal));
            PerformanceTable.Columns.Add(PerfReportDS.TYPE, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.PERPERIOD, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.ONEMONTH, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.THREEMONTH, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.SIXMONTH, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.TWELVEMONTH, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.ONEYEAR, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.TWOYEAR, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.THREEYEAR, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.FIVEYEAR, typeof(string));

            PerformanceTable.Columns.Add(PerfReportDS.PERPERIODDIFF, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.ONEMONTHDIFF, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.THREEMONTHDIFF, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.SIXMONTHDIFF, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.TWELVEMONTHDIFF, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.ONEYEARDIFF, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.TWOYEARDIFF, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.THREEYEARDIFF, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.FIVEYEARDIFF, typeof(string));

            PerformanceTable.Columns.Add(PerfReportDS.PERPERIODINCOME, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.ONEMONTHINCOME, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.THREEMONTHINCOME, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.SIXMONTHINCOME, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.TWELVEMONTHINCOME, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.ONEYEARINCOME, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.TWOYEARINCOME, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.THREEYEARINCOME, typeof(string));
            PerformanceTable.Columns.Add(PerfReportDS.FIVEYEARINCOME, typeof(string));

            this.Tables.Add(PerformanceTable);
        }
    }
}
