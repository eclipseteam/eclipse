﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class AlertConfigurationDS : DataSet, IHasOrganizationUnit, ISupportRecordCount, ISupportClientIDFilter
    {
        #region Properties

        public  DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public ConfigurationTable configurationTable = new ConfigurationTable();

        public ConfigurationPresentationTable configurationPresentationTable = new ConfigurationPresentationTable();

        #endregion

        public AlertConfigurationDS()
        {
            Tables.Add(configurationTable);
            Tables.Add(configurationPresentationTable);
        }


        public OrganizationUnit Unit
        {
            get; 
            set;
        }

        public int Command
        {
            get;
            set;
        }

        public int Count { get; set; }

        public string ClientIDs { get; set; }

        public Guid CID { get; set; }
        public Guid ParentID { get; set; }

        public SortOrder SortOrderBy { get; set; }

    }

    public class ConfigurationTable : DataTable
    {
    
        #region Table Fields
        //Alert Configuration Table Fields
        public readonly string CONFIGURATIONID = "ConfigurationID";
        public readonly string WORKFLOWID = "WorkflowID";
        public readonly string CID = "CID";
        public readonly string REMINDERDURATION = "ReminderDuration";
        public readonly string DURATIONTYPE = "DurationType";
        public readonly string EXECUTIONMODE = "ExecutionMode";
        public readonly string VIEWONLINE = "ViewOnline";
        public readonly string SENDEMAIL = "SendEmail";
        public readonly string CREATEDON = "CreatedOn";
        public readonly string MODIFIEDON = "ModifiedOn";
        public readonly string UPDATEBY = "UpdateBy";
        public readonly string USERTYPE = "UserType";
        public readonly string EXECUTIONDAYS = "ExecutionDays";
        
        #endregion
        private readonly string AlertConfigurationTableName = "AlertConfigurationTable";
        internal ConfigurationTable()
        {
            TableName = AlertConfigurationTableName;
            Columns.Add(CONFIGURATIONID, typeof(Guid)); //Row ID
            Columns.Add(WORKFLOWID, typeof(Guid)); //WorkflowID
            Columns.Add(CID, typeof(Guid)); //Entity ID
            Columns.Add(REMINDERDURATION, typeof(int)); //Any Number: 2,4,8
            Columns.Add(DURATIONTYPE, typeof(int)); //Day/Week/Month
            Columns.Add(EXECUTIONMODE, typeof(int)); //Before/On Date/After
            Columns.Add(VIEWONLINE, typeof(bool)); //True/False
            Columns.Add(SENDEMAIL, typeof(bool)); //True/False
            Columns.Add(CREATEDON, typeof(DateTime));
            Columns.Add(MODIFIEDON, typeof(DateTime));
            Columns.Add(UPDATEBY, typeof(string));
            Columns.Add(USERTYPE, typeof(int)); //Admin/Adviser/Client
            Columns.Add(EXECUTIONDAYS, typeof(int)); //Count of Days as per alert settings
        }

        public void AddDefaultConfigurationRows(Guid workflowID,Guid cid)
        {

            for (int i = 1; i <= 4; i++)
            {
                int executionDays = 0;
                DataRow row = NewRow();
                row[CONFIGURATIONID] = Guid.NewGuid();
                row[WORKFLOWID] = workflowID;
                row[CID] = cid;
                if (i == 1)
                {
                    row[REMINDERDURATION] = 8;
                    row[EXECUTIONMODE] = (int)AlertExecutionMode.Before;
                    executionDays = 8*7*-1;
                }
                else if (i == 2)
                {
                    row[REMINDERDURATION] = 2;
                    row[EXECUTIONMODE] = (int)AlertExecutionMode.Before;
                    executionDays = 2 * 7 * -1;
                }
                else if (i == 3)
                {
                    row[REMINDERDURATION] = 0;
                    row[EXECUTIONMODE] = (int)AlertExecutionMode.OnDueDate;
                    executionDays = 0;
                }
                else if (i == 4)
                {
                    row[REMINDERDURATION] = 4;
                    row[EXECUTIONMODE] = (int) AlertExecutionMode.After;
                    executionDays = 4*7;
                }

                row[DURATIONTYPE] = (int)AlertDurationType.Week;

                row[VIEWONLINE] = true;

                if (i == 1 || i == 4)
                    row[SENDEMAIL] = true;
                else
                    row[SENDEMAIL] = false;

                row[EXECUTIONDAYS] = executionDays;
                Rows.Add(row);
            }



        }
        

        
    }

    public class ConfigurationPresentationTable : DataTable
    {

        #region Table Fields
        //Configuration Presentation Table Fields
        //Columns1
        public readonly string CONFIGURATIONID1 = "ConfigurationID1";
        public readonly string WORKFLOWID1 = "WorkflowID1";
        public readonly string CID1 = "CID1";
        public readonly string REMINDERDURATION1 = "ReminderDuration1";
        public readonly string DURATIONTYPE1 = "DurationType1";
        public readonly string EXECUTIONMODE1 = "ExecutionMode1";
        public readonly string VIEWONLINE1 = "ViewOnline1";
        public readonly string SENDEMAIL1 = "SendEmail1";
        public readonly string USERTYPE1 = "UserType1";
        public readonly string CREATEDON1 = "CreatedOn1";
        public readonly string MODIFIEDON1 = "ModifiedOn1";
        public readonly string UPDATEBY1 = "UpdateBy1";

        //Columns2
        public readonly string CONFIGURATIONID2 = "ConfigurationID2";
        public readonly string WORKFLOWID2 = "WorkflowID2";
        public readonly string CID2 = "CID2";
        public readonly string REMINDERDURATION2 = "ReminderDuration2";
        public readonly string DURATIONTYPE2 = "DurationType2";
        public readonly string EXECUTIONMODE2 = "ExecutionMode2";
        public readonly string VIEWONLINE2 = "ViewOnline2";
        public readonly string SENDEMAIL2 = "SendEmail2";
        public readonly string USERTYPE2 = "UserType2";
        public readonly string CREATEDON2 = "CreatedOn2";
        public readonly string MODIFIEDON2 = "ModifiedOn2";
        public readonly string UPDATEBY2 = "UpdateBy2";

        //Columns3
        public readonly string CONFIGURATIONID3 = "ConfigurationID3";
        public readonly string WORKFLOWID3 = "WorkflowID3";
        public readonly string CID3 = "CID3";
        public readonly string REMINDERDURATION3 = "ReminderDuration3";
        public readonly string DURATIONTYPE3 = "DurationType3";
        public readonly string EXECUTIONMODE3 = "ExecutionMode3";
        public readonly string VIEWONLINE3 = "ViewOnline3";
        public readonly string SENDEMAIL3 = "SendEmail3";
        public readonly string USERTYPE3 = "UserType3";
        public readonly string CREATEDON3 = "CreatedOn3";
        public readonly string MODIFIEDON3 = "ModifiedOn3";
        public readonly string UPDATEBY3 = "UpdateBy3";

        //Columns4
        public readonly string CONFIGURATIONID4 = "ConfigurationID4";
        public readonly string WORKFLOWID4 = "WorkflowID4";
        public readonly string CID4 = "CID4";
        public readonly string REMINDERDURATION4 = "ReminderDuration4";
        public readonly string DURATIONTYPE4 = "DurationType4";
        public readonly string EXECUTIONMODE4 = "ExecutionMode4";
        public readonly string VIEWONLINE4 = "ViewOnline4";
        public readonly string SENDEMAIL4 = "SendEmail4";
        public readonly string USERTYPE4 = "UserType4";
        public readonly string CREATEDON4 = "CreatedOn4";
        public readonly string MODIFIEDON4 = "ModifiedOn4";
        public readonly string UPDATEBY4 = "UpdateBy4";

        #endregion
        private readonly string PresentationTableName = "PresentationTable";
        internal ConfigurationPresentationTable()
        {
            TableName = PresentationTableName;
            //Column1
            Columns.Add(CONFIGURATIONID1, typeof(Guid)); //Row ID
            Columns.Add(WORKFLOWID1, typeof(Guid)); //WorkflowID
            Columns.Add(CID1, typeof(Guid)); //Entity ID
            Columns.Add(REMINDERDURATION1, typeof(int)); //Any Number: 2,4,8
            Columns.Add(DURATIONTYPE1, typeof(int)); //Day/Week/Month
            Columns.Add(EXECUTIONMODE1, typeof(int)); //Before/On Date/After
            Columns.Add(VIEWONLINE1, typeof(bool)); //True/False
            Columns.Add(SENDEMAIL1, typeof(bool)); //True/False
            Columns.Add(USERTYPE1, typeof(int)); //Admin/Adviser/Client
            Columns.Add(CREATEDON1, typeof(DateTime));
            Columns.Add(MODIFIEDON1, typeof(DateTime));
            Columns.Add(UPDATEBY1, typeof(string));
            //Column2
            Columns.Add(CONFIGURATIONID2, typeof(Guid)); //Row ID
            Columns.Add(WORKFLOWID2, typeof(Guid)); //WorkflowID
            Columns.Add(CID2, typeof(Guid)); //Entity ID
            Columns.Add(REMINDERDURATION2, typeof(int)); //Any Number: 2,4,8
            Columns.Add(DURATIONTYPE2, typeof(int)); //Day/Week/Month
            Columns.Add(EXECUTIONMODE2, typeof(int)); //Before/On Date/After
            Columns.Add(VIEWONLINE2, typeof(bool)); //True/False
            Columns.Add(SENDEMAIL2, typeof(bool)); //True/False
            Columns.Add(USERTYPE2, typeof(int)); //Admin/Adviser/Client
            Columns.Add(CREATEDON2, typeof(DateTime));
            Columns.Add(MODIFIEDON2, typeof(DateTime));
            Columns.Add(UPDATEBY2, typeof(string));
            //Column3
            Columns.Add(CONFIGURATIONID3, typeof(Guid)); //Row ID
            Columns.Add(WORKFLOWID3, typeof(Guid)); //WorkflowID
            Columns.Add(CID3, typeof(Guid)); //Entity ID
            Columns.Add(REMINDERDURATION3, typeof(int)); //Any Number: 3,4,8
            Columns.Add(DURATIONTYPE3, typeof(int)); //Day/Week/Month
            Columns.Add(EXECUTIONMODE3, typeof(int)); //Before/On Date/After
            Columns.Add(VIEWONLINE3, typeof(bool)); //True/False
            Columns.Add(SENDEMAIL3, typeof(bool)); //True/False
            Columns.Add(USERTYPE3, typeof(int)); //Admin/Adviser/Client
            Columns.Add(CREATEDON3, typeof(DateTime));
            Columns.Add(MODIFIEDON3, typeof(DateTime));
            Columns.Add(UPDATEBY3, typeof(string));
            //Column4
            Columns.Add(CONFIGURATIONID4, typeof(Guid)); //Row ID
            Columns.Add(WORKFLOWID4, typeof(Guid)); //WorkflowID
            Columns.Add(CID4, typeof(Guid)); //Entity ID
            Columns.Add(REMINDERDURATION4, typeof(int)); //Any Number: 4,4,8
            Columns.Add(DURATIONTYPE4, typeof(int)); //Day/Week/Month
            Columns.Add(EXECUTIONMODE4, typeof(int)); //Before/On Date/After
            Columns.Add(VIEWONLINE4, typeof(bool)); //True/False
            Columns.Add(SENDEMAIL4, typeof(bool)); //True/False
            Columns.Add(USERTYPE4, typeof(int)); //Admin/Adviser/Client
            Columns.Add(CREATEDON4, typeof(DateTime));
            Columns.Add(MODIFIEDON4, typeof(DateTime));
            Columns.Add(UPDATEBY4, typeof(string));
        }


        public void FillFromConfigurationTable(ConfigurationTable configurationTable)
        {
           

            var rows = from contact in configurationTable.AsEnumerable()
                       select contact;

            var i = 1;

            DataRow dr = NewRow();
            foreach (var row in rows)
            {
                foreach (DataColumn dataColumn in configurationTable.Columns)
                {
                    string mapColumn = string.Format("{0}{1}", dataColumn.ColumnName, i);
                    if (Columns.Contains(mapColumn))
                        dr[mapColumn] = row[dataColumn.ColumnName];
                }
                i++;
            }
            Rows.Add(dr);

          
        }

    }

    
}
