﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class SecuritiesDS : DataSet
    {

        public Guid SecID = Guid.Empty;
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public const string SECLIST = "Securities";
       
        
        public const string ID = "ID";
        public const string CODE = "Code";
        public const string DESCRIPTION = "Description";
        public const string PRICEDATE = "PriceDate";
        public const string UNITPRICE = "UnitPrice";

        public const string INVESTMENTTYPE = "InvestmentType";
        public const string MARKET = "Market";
        public const string ASSET = "Asset";
        public const string STATUS = "Status";
        public const string ISINCODE = "IsinCode";
        public const string RATING = "Rating";
        public const string UNITSHELD = "UnitsHeld";
        public const string CURRENCY = "Currency";
        public const string PRICINGSOURCE = "PricingSource";
        public const string DISTRIBUTIONTYPE = "DistributionType";
        public const string DISTRIBUTIONFREQUENCY = "DistributionFrequency";
        public const string ISSMAAPPROVED = "IsSMAApproved";
        public const string SMAHOLDINGLIMIT = "SMAHoldingLimit";

        public SecuritiesDS()
        {
            Tables.Add(CreateSecuritiesListTable());
        }
        
        public static DataTable CreateSecuritiesListTable()
        {
            DataTable SecListTable = new DataTable(SECLIST);
            SecListTable.Columns.Add(ID, typeof(Guid));
            SecListTable.Columns.Add(CODE, typeof(string));
            SecListTable.Columns.Add(DESCRIPTION, typeof(string));
            SecListTable.Columns.Add(PRICEDATE, typeof(DateTime));
            SecListTable.Columns.Add(UNITPRICE, typeof(double));

            SecListTable.Columns.Add(INVESTMENTTYPE, typeof(string));
            SecListTable.Columns.Add(MARKET, typeof(string));
            SecListTable.Columns.Add(ASSET, typeof(string));
            SecListTable.Columns.Add(STATUS, typeof(string));
            SecListTable.Columns.Add(ISINCODE, typeof(string));
            SecListTable.Columns.Add(RATING, typeof(string));
            SecListTable.Columns.Add(UNITSHELD, typeof(double));
            SecListTable.Columns.Add(CURRENCY, typeof(string));
            SecListTable.Columns.Add(PRICINGSOURCE, typeof(int));
            SecListTable.Columns.Add(DISTRIBUTIONTYPE, typeof(int));
            SecListTable.Columns.Add(DISTRIBUTIONFREQUENCY, typeof(int));
            SecListTable.Columns.Add(ISSMAAPPROVED, typeof(bool));
            SecListTable.Columns.Add(SMAHOLDINGLIMIT, typeof(double));

            return SecListTable;
        }
    }
}
