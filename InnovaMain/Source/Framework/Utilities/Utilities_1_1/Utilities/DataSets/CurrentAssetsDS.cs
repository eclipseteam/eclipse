﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class CurrentAssetsDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public CurrentAssetsTable CurrentAssetsTable = new CurrentAssetsTable();

        public CurrentAssetsDS()
        {
            Tables.Add(CurrentAssetsTable);
        }

        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
    }

    public class CurrentAssetsTable : DataTable
    {
        private readonly string TABLENAME = "CurrentAssetsList";
        public readonly string ADVICESID = "AdvicesId";
        public readonly string ASSETTYPE = "AssetType";
        public readonly string EQUITY = "Equity";
        public readonly string FIXEDINTEREST = "FixedInterest";
        public readonly string INTERNATIONALEQUITY = "InternationalEquity";
        public readonly string CASH = "Cash";
        public readonly string GOLD = "Gold";
        public readonly string PROPERTY = "Property";

        internal CurrentAssetsTable()
        {
            TableName = TABLENAME;
            Columns.Add(ADVICESID, typeof(Guid));
            Columns.Add(ASSETTYPE, typeof(int));
            Columns.Add(EQUITY, typeof(decimal));
            Columns.Add(FIXEDINTEREST, typeof(decimal));
            Columns.Add(INTERNATIONALEQUITY, typeof(decimal));
            Columns.Add(CASH, typeof(decimal));
            Columns.Add(GOLD, typeof(decimal));
            Columns.Add(PROPERTY, typeof(decimal));
        }
    }
}
