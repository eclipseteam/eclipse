﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public enum IndividualsType
    {
        Applicants,
        Signatories,
        Directors,
        Contacts,
        Individual,
    }

    public class IndividualDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public Individuals IndividualTable = new Individuals();

        public IndividualDS()
        {
            Tables.Add(IndividualTable);
        }


        public OrganizationUnit Unit { get; set; }

        public int Command { get; set; }
        public IndividualsType IndividualsType { get; set; }
    }

    public class Individuals : DataTable
    {
        public readonly string TABLENAME = "IndividualList";
        public readonly string CLIENTID = "ClientId";
        public readonly string CID = "Cid";
        public readonly string CLID = "CLid";
        public readonly string CSID = "CSid";

        public readonly string PERSONALTITLE = "PersonalTitle";
        public readonly string NAME = "Name";
        public readonly string SURNAME = "Surname";
        public readonly string FULLNAME = "Fullname";
        public readonly string MIDDLENAME = "MiddleName";
        public readonly string EMAILADDRESS = "EmailAddress";

        public readonly string WORKCOUNTRYCODE = "WorkCountryCode";
        public readonly string WORKCITYCODE = "WorkCityCode";
        public readonly string WORKPHONENUMBER = "WorkPhoneNumber";

        public readonly string HOMECOUNTRYCODE = "HomeCountryCode";
        public readonly string HOMECITYCODE = "HomeCityCode";
        public readonly string HOMEPHONENUMBER = "HomePhoneNumber";

        public readonly string MOBILECOUNTRYCODE = "MobileCountryCode";
        public readonly string MOBILEPHONENUMBER = "MobilePhoneNumber";

        public readonly string TFN = "TFN";
        public readonly string TIN = "TIN";

        public readonly string FACSIMILECOUNTYCODE = "FacsimileCountryCode";
        public readonly string FACSIMILECITYCODE = "FacsimileCityCode";
        public readonly string FACSIMILE = "Facsimile";

        public readonly string DOB = "DOB";
        public readonly string PASSWORD = "Password";
        public readonly string OCCUPATION = "Occupation";
        public readonly string EMPLOYER = "Employer";
        public readonly string LICENSENUMBER = "LicenseNumber";
        public readonly string PASSPORTNUMBER = "PassportNumber";

        public readonly string RESIDENTIALADDRESSLINE1 = "ResidentialAddressLine1";
        public readonly string RESIDENTIALADDRESSLINE2 = "ResidentialAddressLine2";
        public readonly string RESIDENTIALADDRESSSUBRUB = "ResidentialAddressSubrub";
        public readonly string RESIDENTIALADDRESSSTATE = "ResidentialAddressState";
        public readonly string RESIDENTIALADDRESSPOSTALCODE = "ResidentialAddressPostalCode";
        public readonly string RESIDENTIALADDRESSCOUNTRY = "ResidentialAddressCountry";

        public readonly string MAILINGADDRESSLINE1 = "MailingAddressLine1";
        public readonly string MAILINGADDRESSLINE2 = "MailingAddressLine2";
        public readonly string MAILINGADDRESSSUBRUB = "MailingAddressSubrub";
        public readonly string MAILINGADDRESSSTATE = "MailingAddressState";
        public readonly string MAILINGADDRESSPOSTALCODE = "MailingAddressPostalCode";
        public readonly string MAILINGADDRESSCOUNTRY = "MailingAddressCountry";


        public readonly string MAILINGADDRESSSAME = "MailingAddressSame";
        public readonly string ORGANIZATIONSTATUS = "OrganizationStatus";
        public readonly string HASADDRESS = "HasAddress";
        public readonly string ORGANIZATIONTYPE = "OrganizationType";
        public readonly string BUSINESSTITLE = "BusinessTitle";

        public readonly string ADVISERCID = "AdviserCid";
        public readonly string ADVISERCLID = "AdviserClid";
        public readonly string ADVISERCSID = "AdviserCsid";
        public readonly string GENDER = "Gender";

        internal Individuals()
        {
            TableName = TABLENAME;
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(CID, typeof(Guid));
            Columns.Add(CLID, typeof(Guid));
            Columns.Add(CSID, typeof(Guid));
            Columns.Add(PERSONALTITLE, typeof(string));
            Columns.Add(NAME, typeof(string));
            Columns.Add(SURNAME, typeof(string));
            Columns.Add(FULLNAME, typeof(string));
            Columns.Add(MIDDLENAME, typeof(string));
            Columns.Add(EMAILADDRESS, typeof(string));
            Columns.Add(GENDER, typeof(string));
            Columns.Add(WORKCOUNTRYCODE, typeof(string));
            Columns.Add(WORKCITYCODE, typeof(string));
            Columns.Add(WORKPHONENUMBER, typeof(string));

            Columns.Add(HOMECOUNTRYCODE, typeof(string));
            Columns.Add(HOMECITYCODE, typeof(string));
            Columns.Add(HOMEPHONENUMBER, typeof(string));

            Columns.Add(MOBILECOUNTRYCODE, typeof(string));
            Columns.Add(MOBILEPHONENUMBER, typeof(string));

            Columns.Add(TFN, typeof(string));
            Columns.Add(TIN, typeof(string));

            Columns.Add(FACSIMILECOUNTYCODE, typeof(string));
            Columns.Add(FACSIMILECITYCODE, typeof(string));
            Columns.Add(FACSIMILE, typeof(string));

            Columns.Add(DOB, typeof(DateTime));
            Columns.Add(PASSWORD, typeof(string));
            Columns.Add(OCCUPATION, typeof(string));
            Columns.Add(EMPLOYER, typeof(string));
            Columns.Add(LICENSENUMBER, typeof(string));
            Columns.Add(PASSPORTNUMBER, typeof(string));

            Columns.Add(RESIDENTIALADDRESSLINE1, typeof(string));
            Columns.Add(RESIDENTIALADDRESSLINE2, typeof(string));
            Columns.Add(RESIDENTIALADDRESSSUBRUB, typeof(string));
            Columns.Add(RESIDENTIALADDRESSSTATE, typeof(string));
            Columns.Add(RESIDENTIALADDRESSPOSTALCODE, typeof(string));
            Columns.Add(RESIDENTIALADDRESSCOUNTRY, typeof(string));

            Columns.Add(MAILINGADDRESSLINE1, typeof(string));
            Columns.Add(MAILINGADDRESSLINE2, typeof(string));
            Columns.Add(MAILINGADDRESSSUBRUB, typeof(string));
            Columns.Add(MAILINGADDRESSSTATE, typeof(string));
            Columns.Add(MAILINGADDRESSPOSTALCODE, typeof(string));
            Columns.Add(MAILINGADDRESSCOUNTRY, typeof(string));

            Columns.Add(MAILINGADDRESSSAME, typeof(bool));
            Columns.Add(ORGANIZATIONSTATUS, typeof(string));
            Columns.Add(HASADDRESS, typeof(bool));
            Columns.Add(ORGANIZATIONTYPE, typeof(string));
            Columns.Add(BUSINESSTITLE, typeof(string));

            Columns.Add(ADVISERCID, typeof(Guid)).DefaultValue = Guid.Empty;
            Columns.Add(ADVISERCLID, typeof(Guid)).DefaultValue = Guid.Empty;
            Columns.Add(ADVISERCSID, typeof(Guid)).DefaultValue = Guid.Empty;
        }
    }
}
