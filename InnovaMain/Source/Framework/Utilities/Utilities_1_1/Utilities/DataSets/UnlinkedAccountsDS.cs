﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class UnlinkedAccountsDS: DataSet,IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public FundAccountTable fundAccountTable = new FundAccountTable();

        public BankAccounts bankAccounts = new BankAccounts();

        public DesktopBrokerAccounts desktopBrokerAccounts = new DesktopBrokerAccounts();

        public UnlinkedAccountsDS()
        {
            Tables.Add(fundAccountTable);
            Tables.Add(bankAccounts);
            Tables.Add(desktopBrokerAccounts);
        }


        public OrganizationUnit Unit
        {
            get; set;
        }

        public int Command
        {
            get;
            set;
        }
    }

    
}
