﻿using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class ExportDS : DataSet,IHasOrganizationUnit
    {
        public ExportDS()
        {
        }
        
        public OrganizationUnit Unit
        {
            get; set;
        }

        public int Command
        {
            get;
            set;
        }

        public ClientManagementType ClientManagementType { get; set; }
    }
}
