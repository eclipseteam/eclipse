﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class OrderPadRatesDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public Guid InstituteId = Guid.Empty;
        public ClientManagementType ClientManagementType = ClientManagementType.UMA;

        public static string ORDERPADRATESTABLE = "OrderPadRates";
        public static string ORDERPADMAXRATESTABLE = "OrderPadMaxRates";
        public static string ID = "ID";
        public static string INSTITUTEID = "InstituteID";
        public static string INSTITUTENAME = "InstituteName";
        public static string PRICEINSTITUTEID = "PriceInstituteID";
        public static string PRICEINSTITUTENAME = "PriceInstituteName";
        public static string INSTITUTESHORTRATING = "InstituteShortRating";
        public static string INSTITUTESHORTRATINGVALUE = "InstituteShortRatingValue";
        public static string INSTITUTELONGRATING = "InstituteLongRating";
        public static string INSTITUTELONGRATINGVALUE = "InstituteLongRatingValue";
        public static string PRICEINSTITUTESHORTRATING = "PriceInstituteShortRating";
        public static string PRICEINSTITUTESHORTRATINGVALUE = "PriceInstituteShortRatingValue";
        public static string PRICEINSTITUTELONGRATING = "PriceInstituteLongRating";
        public static string PRICEINSTITUTELONGRATINGVALUE = "PriceInstituteLongRatingValue";
        public static string MIN = "Min";
        public static string MAX = "Max";
        public static string MAXIMUMBROKEAGE = "MaximumBrokeage";
        public static string DAYS30 = "Days30";
        public static string DAYS60 = "Days60";
        public static string DAYS90 = "Days90";
        public static string DAYS120 = "Days120";
        public static string DAYS150 = "Days150";
        public static string DAYS180 = "Days180";
        public static string DAYS270 = "Days270";
        public static string YEARS1 = "Years1";
        public static string YEARS2 = "Years2";
        public static string YEARS3 = "Years3";
        public static string YEARS4 = "Years4";
        public static string YEARS5 = "Years5";
        public static string STATUS = "Status";
        public static string DATE = "Date";
        public static string ONGOING = "Ongoing";
        public static string CLIENTMANAGEMENTTYPE = "ClientManagementType";
        public static string SMAHOLDINGLIMIT = "SMAHoldingLimit";
        public static string RATEID = "RateID";
        public static string PROVIDERID = "ProviderID";

        public DateTime ValuationDate;

        public OrderPadRatesDS()
        {
            this.ValuationDate = DateTime.Today;
            this.Tables.Add(OrderPadRatesTable());
            this.Tables.Add(OrderPadMaxRatesTable());
        }

        public DataTable OrderPadRatesTable()
        {
            DataTable dt = new DataTable(ORDERPADRATESTABLE);
            dt.Columns.Add(ID, typeof(string));
            dt.Columns.Add(INSTITUTEID, typeof(string));
            dt.Columns.Add(INSTITUTENAME, typeof(string));
            dt.Columns.Add(PRICEINSTITUTEID, typeof(string));
            dt.Columns.Add(PRICEINSTITUTENAME, typeof(string));
            dt.Columns.Add(INSTITUTESHORTRATING, typeof(string));
            dt.Columns.Add(INSTITUTESHORTRATINGVALUE, typeof(int));
            dt.Columns.Add(INSTITUTELONGRATING, typeof(string));
            dt.Columns.Add(INSTITUTELONGRATINGVALUE, typeof(int));
            dt.Columns.Add(PRICEINSTITUTESHORTRATING, typeof(string));
            dt.Columns.Add(PRICEINSTITUTESHORTRATINGVALUE, typeof(int));
            dt.Columns.Add(PRICEINSTITUTELONGRATING, typeof(string));
            dt.Columns.Add(PRICEINSTITUTELONGRATINGVALUE, typeof(int));
            dt.Columns.Add(MIN, typeof(decimal));
            dt.Columns.Add(MAX, typeof(decimal));
            dt.Columns.Add(MAXIMUMBROKEAGE, typeof(decimal));
            dt.Columns.Add(DAYS30, typeof(decimal));
            dt.Columns.Add(DAYS60, typeof(decimal));
            dt.Columns.Add(DAYS90, typeof(decimal));
            dt.Columns.Add(DAYS120, typeof(decimal));
            dt.Columns.Add(DAYS150, typeof(decimal));
            dt.Columns.Add(DAYS180, typeof(decimal));
            dt.Columns.Add(DAYS270, typeof(decimal));
            dt.Columns.Add(YEARS1, typeof(decimal));
            dt.Columns.Add(YEARS2, typeof(decimal));
            dt.Columns.Add(YEARS3, typeof(decimal));
            dt.Columns.Add(YEARS4, typeof(decimal));
            dt.Columns.Add(YEARS5, typeof(decimal));
            dt.Columns.Add(STATUS, typeof(string));
            dt.Columns.Add(DATE, typeof(DateTime));
            dt.Columns.Add(ONGOING, typeof(decimal));
            dt.Columns.Add(CLIENTMANAGEMENTTYPE, typeof(string));
            dt.Columns.Add(SMAHOLDINGLIMIT, typeof(decimal));
            dt.Columns.Add(RATEID, typeof(int));
            dt.Columns.Add(PROVIDERID, typeof(int));

            return dt;
        }

        public DataTable OrderPadMaxRatesTable()
        {
            DataTable dt = new DataTable(ORDERPADMAXRATESTABLE);
            dt.Columns.Add(MIN, typeof(decimal));
            dt.Columns.Add(MAX, typeof(decimal));
            dt.Columns.Add(MAXIMUMBROKEAGE, typeof(decimal));
            dt.Columns.Add(DAYS30, typeof(decimal));
            dt.Columns.Add(DAYS60, typeof(decimal));
            dt.Columns.Add(DAYS90, typeof(decimal));
            dt.Columns.Add(DAYS120, typeof(decimal));
            dt.Columns.Add(DAYS150, typeof(decimal));
            dt.Columns.Add(DAYS180, typeof(decimal));
            dt.Columns.Add(DAYS270, typeof(decimal));
            dt.Columns.Add(YEARS1, typeof(decimal));
            dt.Columns.Add(YEARS2, typeof(decimal));
            dt.Columns.Add(YEARS3, typeof(decimal));
            dt.Columns.Add(YEARS4, typeof(decimal));
            dt.Columns.Add(YEARS5, typeof(decimal));
            dt.Columns.Add(DATE, typeof(DateTime));
            dt.Columns.Add(ONGOING, typeof(decimal));
            return dt;
        }

        public string DateInfo()
        {
            return "AS OF " + this.ValuationDate.ToString("dd/MM/yyyy");
        }

        public object GetMaxValue(string tableName, string ColumnName)
        {
            var max = this.Tables[tableName].Compute("Max(" + ColumnName + ")", string.Empty);
            return max;
        }

        public OrganizationUnit Unit { get; set; }

        public int Command { get; set; }
    }
}
