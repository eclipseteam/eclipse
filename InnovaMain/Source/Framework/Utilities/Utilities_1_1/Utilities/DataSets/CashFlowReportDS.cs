﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.DataSets
{
    public partial class CashFlowReportDS:DataSet ,IHasOrganizationUnit,ISupportRecordCount
    {
        public DateTime StartDate;
        public DateTime EndDate;
        public CashFlowReportTable CashFlowReportTable=new CashFlowReportTable();
        public CashFlowReportDS()
        {
            
            Tables.Add(CashFlowReportTable);
        }

        public OrganizationUnit Unit
        {
            get; set; }

        public int Command
        {
            get; set; }

        public int Count
        {
            get; set; }
    }



    public class CashFlowReportTable : DataTable
    {
        private readonly string Name = "ClientsCashFlowReport";
        public readonly string TradeDate = "TradeDate";
        public readonly string MonthYear = "MonthYear";
        public readonly string ClientName = "ClientName";
        public readonly string ClientCode = "ClientCode";
        public readonly string ClientCid = "ClientCid";
        public readonly string ClientType = "ClientType";
        public readonly string AdviserID = "AdviserID";
        public readonly string AdviserName = "AdviserName";
        public readonly string AdviserCID = "AdviserCID";
        public readonly string ServiceType = "ServiceType";

        public readonly string ReinvestOrDistributeOption = "ReinvestOrDistributeOption";
        public readonly string AccountType = "AccountType";
        public readonly string AccountName = "AccountName";
        public readonly string OpeningBalance = "OpeningBalance";
        public readonly string TransferIn = "TransferIn";
        public readonly string TransferOut = "TransferOut";
        public readonly string Income = "Income";
        public readonly string Investments = "Investments";
        public readonly string FeesTaxes = "FeesTaxes";
        public readonly string InternalCashMovement = "InternalCashMovement";
        public readonly string InvestmentReturn = "InvestmentReturn";
        public readonly string ClosingBalance = "ClosingBalance";
       
        internal CashFlowReportTable()
        {
            TableName = Name;
            Columns.Add(TradeDate, typeof(DateTime));
            Columns.Add(MonthYear,typeof(string));
            Columns.Add(ClientName, typeof(string));
            Columns.Add(ClientCode, typeof(string));
            Columns.Add(ClientCid, typeof(Guid));
            Columns.Add(ClientType, typeof(string));
            Columns.Add(AdviserID, typeof(string));
            Columns.Add(AdviserName, typeof(string));
            Columns.Add(AdviserCID, typeof(Guid));
            Columns.Add(ServiceType, typeof(string));
            Columns.Add(ReinvestOrDistributeOption, typeof(string));
            Columns.Add(AccountType, typeof(string));
            Columns.Add(AccountName, typeof(string));
            Columns.Add(OpeningBalance, typeof(decimal));
            Columns.Add(TransferIn, typeof(decimal));
            Columns.Add(TransferOut, typeof(decimal));
            Columns.Add(Income, typeof(decimal));
            Columns.Add(Investments, typeof(decimal));
            Columns.Add(FeesTaxes, typeof(decimal));
            Columns.Add(InternalCashMovement, typeof(decimal));
            Columns.Add(InvestmentReturn, typeof(decimal));
            Columns.Add(ClosingBalance, typeof(decimal));
            
        }



        public void AddRow(DateTime tradeDate, string monthYear, string clientName, string clientCode, Guid clientCid, string clientType, string adviserID, string adviserName, Guid adviserCID, string serviceType, string reinvestOrDistributeOption, string accountType, string accountName, decimal openingBalance, decimal transferIn, decimal transferOut, decimal income, decimal investments, decimal feesTaxes, decimal internalCashMovement,  decimal closingBalance)
        {
            var row=NewRow();
            //Change in Mkt Val = "Closing Bal" - "Transfer In"+ "Transfer Out" - "Income" - "Investments" + "Tax & Expenses" - "Internal Cash Movement" - "Opening Bal"			
            decimal investmentReturn = closingBalance-transferIn+transferOut-income-investments+feesTaxes-internalCashMovement-openingBalance;

            row[TradeDate] = tradeDate;
            row[MonthYear] = monthYear;
            row[ClientName] = clientName;
            row[ClientCode] = clientCode;
            row[ClientCid] = clientCid;
            row[ClientType] = clientType;
            row[AdviserID] = adviserID;
            row[AdviserName] = adviserName;
            row[AdviserCID] = adviserCID;
            row[ServiceType] = serviceType;
            row[ReinvestOrDistributeOption] = reinvestOrDistributeOption;
            row[AccountType] = accountType;
            row[AccountName] = accountName;
            row[OpeningBalance] = openingBalance;
            row[TransferIn] = transferIn;
            row[TransferOut] = transferOut;
            row[Income] = income;
            row[Investments] = investments;
            row[FeesTaxes] = feesTaxes;
            row[InternalCashMovement] = internalCashMovement;
            row[InvestmentReturn] = investmentReturn;
            row[ClosingBalance] = closingBalance;
             
            Rows.Add(row);
        }
    }

}
