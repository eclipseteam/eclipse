﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class ClientMappingControlDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public ClientMappingTable ClientMappingTable = new ClientMappingTable();

        public ClientMappingControlDS()
        {
            Tables.Add(ClientMappingTable);
        }


        public OrganizationUnit Unit
        {
            get; set;
        }
        public int Command
        {
            get;
            set;
        }
    }
     public class ClientMappingTable : DataTable
    {
        private readonly string TABLENAME = "ClientMappingList";
        public readonly string CLIENTID = "ClientID";
        public readonly string CID = "Cid";
        public readonly string CLID = "Clid";
        public readonly string CSID = "Csid";
        public readonly string NAME = "Name";
        public readonly string TYPE = "Type";
        public readonly string STATUS = "Status";


        internal ClientMappingTable()
        {
            TableName = TABLENAME;
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(CID, typeof(Guid));
            Columns.Add(CLID, typeof(Guid));
            Columns.Add(CSID, typeof(Guid));
            Columns.Add(NAME, typeof(string));
            Columns.Add(TYPE, typeof(string));
            Columns.Add(STATUS, typeof(string));            
            
        }
    }
}


