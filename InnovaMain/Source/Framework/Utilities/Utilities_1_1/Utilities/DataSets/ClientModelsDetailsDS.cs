﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class ClientModelsDetailsDS : DataSet, IHasOrganizationUnit
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public bool DoItForMe { get; set; }
        public bool DoItWithMe { get; set; }
        public bool DoItYourself { get; set; }
        public DoItForME DoItForMeTable = new DoItForME();
        public DoItWithMe DoItWithMeTable = new DoItWithMe();
        public DoItYourself DoItYourselfTable = new DoItYourself();
        public ClientModelsTable ClientModelsTable = new ClientModelsTable();
        public ClientModelsDetailsDS()
        {
            Tables.Add(DoItForMeTable);
            Tables.Add(DoItWithMeTable);
            Tables.Add(DoItYourselfTable);
            Tables.Add(ClientModelsTable);
        }

        public OrganizationUnit Unit { get; set; }

        public int Command{get;set;}
    }
    
    public class ClientModelsTable : DataTable
    {
        private readonly string TABLENAME = "ClientModels";
        public readonly string CLIENTCID = "CLientCID";
        public readonly string CLIENTID = "ClientID";
        public readonly string CLIENTNAME = "ClientName";
        public readonly string PROGRAMCODE = "ProgramCode";
        public readonly string PROGRAMNAME = "ProgramName";
        public readonly string SERVICETYPE = "ServiceType";


        internal ClientModelsTable()
        {
            TableName = TABLENAME;
            Columns.Add(CLIENTCID, typeof(Guid));
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(CLIENTNAME, typeof(string));
            Columns.Add(PROGRAMCODE, typeof(string));
            Columns.Add(PROGRAMNAME, typeof(string));
            Columns.Add(SERVICETYPE, typeof(string));
        }

        public void AddRow(Guid clientCID, string clientID, string clientName, string programCode, string programName, string serviceType)
        {
            var row = NewRow();
            row[CLIENTCID] = clientCID;
            row[CLIENTID] = clientID;
            row[CLIENTNAME] = clientName;
            row[PROGRAMCODE] = programCode;
            row[PROGRAMNAME] = programName;
            row[SERVICETYPE] = serviceType.ToUpper();
            Rows.Add(row);
        }
    }

    public class DoItForME : DataTable
    {
        public readonly string TABLENAME = "DoItForMEDetails";

        public readonly string PROGRAMCODE = "ProgramCode";
        public readonly string PROGRAMNAME = "ProgramName";
        public readonly string AUTHORIASTIONTYPE = "AuthoriastionType";
        public readonly string SPONSORINGBROKER = "SponsoringBroker";
        public readonly string HIN = "HIN";
        public readonly string WHOLESALEINVESTOR = "WholesaleInvestor";
        public readonly string RETAILINVESTOR = "RetailInvestor";



        internal DoItForME()
        {
            TableName = TABLENAME;

            Columns.Add(PROGRAMCODE, typeof(string));
            Columns.Add(PROGRAMNAME, typeof(string));
            Columns.Add(AUTHORIASTIONTYPE, typeof(string));
            Columns.Add(SPONSORINGBROKER, typeof(string));
            Columns.Add(WHOLESALEINVESTOR, typeof(string));
            Columns.Add(RETAILINVESTOR, typeof(string));
            Columns.Add(HIN, typeof(string));


        }
    }
    public class DoItWithMe : DataTable
    {
        public readonly string TABLENAME = "DoItWithMeDetails";



        public readonly string PROGRAMCODE = "ProgramCode";
        public readonly string PROGRAMNAME = "ProgramName";
        public readonly string AUTHORIASTIONTYPE = "AuthoriastionType";

        public readonly string DOITYOURSELF_AUTHORIASTIONTYPE = "DoItYourSelf_AuthoriastionType";
        public readonly string DOITYOURSELF_ASX = "DoItYourSelf_ASX";
        public readonly string DOITYOURSELF_TDS = "DoItYourSelf_TDS";
        public readonly string DOITYOURSELF_DP = "DoItYourSelf_DP";
        public readonly string DOITYOURSELF_OTHER = "DoItYourSelf_Other";


        internal DoItWithMe()
        {
            TableName = TABLENAME;


            Columns.Add(PROGRAMCODE, typeof(string));
            Columns.Add(PROGRAMNAME, typeof(string));
            Columns.Add(AUTHORIASTIONTYPE, typeof(string));


            Columns.Add(DOITYOURSELF_AUTHORIASTIONTYPE, typeof(string));
            Columns.Add(DOITYOURSELF_ASX, typeof(bool));
            Columns.Add(DOITYOURSELF_TDS, typeof(bool));
            Columns.Add(DOITYOURSELF_DP, typeof(bool));
            Columns.Add(DOITYOURSELF_OTHER, typeof(string));

        }
    }
    public class DoItYourself : DataTable
    {
        public readonly string TABLENAME = "DoItYourselfDetails";

        public readonly string AUTHORIASTIONTYPE = "AuthoriastionType";
        public readonly string ASX = "ASX";
        public readonly string TDS = "TDS";
        public readonly string DP = "DP";
        public readonly string OTHER = "Other";
        public readonly string PROGRAMCODE = "ProgramCode";
        public readonly string PROGRAMNAME = "ProgramName";




        internal DoItYourself()
        {
            TableName = TABLENAME;
            Columns.Add(AUTHORIASTIONTYPE, typeof(string));
            Columns.Add(ASX, typeof(bool));
            Columns.Add(TDS, typeof(bool));
            Columns.Add(DP, typeof(bool));
            Columns.Add(OTHER, typeof(string));
            Columns.Add(PROGRAMCODE, typeof(string));
            Columns.Add(PROGRAMNAME, typeof(string));


        }
    }
}
