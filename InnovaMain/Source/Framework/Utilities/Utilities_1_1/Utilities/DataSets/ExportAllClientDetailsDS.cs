﻿using System.Data;
using Oritax.TaxSimp.Data;
using System.Collections.Generic;

namespace Oritax.TaxSimp.DataSets
{
    public class ExportAllClientDetailsDS : DataSet, IHasOrganizationUnit, ISupportRecordCount
    {
        public AccountOpeningBankWestCSVColumns objCSVColumns = new AccountOpeningBankWestCSVColumns();
        public AccountOpeningBankWestIDCSVColumns obje = new AccountOpeningBankWestIDCSVColumns();
        public ClientDetailsTable ClientDetailsTable = new ClientDetailsTable();
        public ClientSignatoryDetailsTable ClientSignatoryDetailsTable = new ClientSignatoryDetailsTable();
        public ClientModelTable ClientModelTable = new ClientModelTable();

        public ExportAllClientDetailsDS()
        {
            Tables.Add(objCSVColumns.Table);
            Tables.Add(obje.Table);
            Tables.Add(ClientDetailsTable);
            Tables.Add(ClientSignatoryDetailsTable);
            Tables.Add(ClientModelTable);
        }

        public int Count { get; set; }

        public OrganizationUnit Unit { get; set; }
        public int Command { get; set; }
    }

    public class ClientDetailsTable : DataTable
    {
        private const string Name = "CLientsData";
        public readonly string ClientCode = "ClientCode";
        public readonly string ClientName = "ClientName";
        public readonly string TFN = "TFN";
        public readonly string ABN = "ABN";
        public readonly string Adviser = "Adviser";
        //public readonly string Email = "Email";
        //public readonly string Phone = "Phone";
        public readonly string InvestorStatus = "InvestorStatus";
        public readonly string LimitedPowerofAttorney = "LimitedPowerofAttorney";
        public readonly string BAddressLine1 = "BAddressLine1";
        public readonly string BAddressLine2 = "BAddressLine2";
        public readonly string BPostCode = "BPostCode";
        public readonly string BCountry = "BCountry";
        public readonly string BState = "BState";
        public readonly string BSubRub = "BSubRub";
        public readonly string RAddressLine1 = "RAddressLine1";
        public readonly string RAddressLine2 = "RAddressLine2";
        public readonly string RPostCode = "RPostCode";
        public readonly string RCountry = "RCountry";
        public readonly string RState = "RState";
        public readonly string RSubRub = "RSubRub";
        public readonly string RIAddressLine1 = "RIAddressLine1";
        public readonly string RIAddressLine2 = "RIAddressLine2";
        public readonly string RIPostCode = "RIPostCode";
        public readonly string RICountry = "RICountry";
        public readonly string RIState = "RIState";
        public readonly string RISubRub = "RISubRub";
        public readonly string MAddressLine1 = "MAddressLine1";
        public readonly string MAddressLine2 = "MAddressLine2";
        public readonly string MPostCode = "MPostCode";
        public readonly string MCountry = "MCountry";
        public readonly string MState = "MState";
        public readonly string MSubRub = "MSubRub";
        public readonly string DMAddressLine1 = "DMAddressLine1";
        public readonly string DMAddressLine2 = "DMAddressLine2";
        public readonly string DMPostCode = "DMPostCode";
        public readonly string DMCountry = "DMCountry";
        public readonly string DMState = "DMState";
        public readonly string DMSubRub = "DMSubRub";
        public readonly string CRAddressLine1 = "CRAddressLine1";
        public readonly string CRAddressLine2 = "CRAddressLine2";
        public readonly string CRPostCode = "CRPostCode";
        public readonly string CRCountry = "CRCountry";
        public readonly string CRState = "CRState";
        public readonly string CRSubRub = "CRSubRub";

        internal ClientDetailsTable()
        {
            TableName = Name;
            Columns.Add(ClientCode);
            Columns.Add(ClientName);
            Columns.Add(TFN);
            Columns.Add(ABN);
            Columns.Add(Adviser);
            //Columns.Add(Email);
            //Columns.Add(Phone);
            Columns.Add(InvestorStatus);
           
            Columns.Add(LimitedPowerofAttorney);
            Columns.Add(BAddressLine1);
            Columns.Add(BAddressLine2);
            Columns.Add(BPostCode);
            Columns.Add(BCountry);
            Columns.Add(BState);
            Columns.Add(BSubRub);
            Columns.Add(RAddressLine1);
            Columns.Add(RAddressLine2);
            Columns.Add(RPostCode);
            Columns.Add(RCountry);
            Columns.Add(RState);
            Columns.Add(RSubRub);
            Columns.Add(RIAddressLine1);
            Columns.Add(RIAddressLine2);
            Columns.Add(RIPostCode);
            Columns.Add(RICountry);
            Columns.Add(RIState);
            Columns.Add(RISubRub);
            Columns.Add(MAddressLine1);
            Columns.Add(MAddressLine2);
            Columns.Add(MPostCode);
            Columns.Add(MCountry);
            Columns.Add(MState);
            Columns.Add(MSubRub);
            Columns.Add(DMAddressLine1);
            Columns.Add(DMAddressLine2);
            Columns.Add(DMPostCode);
            Columns.Add(DMCountry);
            Columns.Add(DMState);
            Columns.Add(DMSubRub);
            Columns.Add(CRAddressLine1);
            Columns.Add(CRAddressLine2);
            Columns.Add(CRPostCode);
            Columns.Add(CRCountry);
            Columns.Add(CRState);
            Columns.Add(CRSubRub);
        }

        public void AddRow(string ClientCode, string ClientName, string TFN, string ABN, string Adviser,  string InvestorStatus, string LimitedPowerofAttorney, Dictionary<AddressPart, string> BAddress, Dictionary<AddressPart, string> RAddress, Dictionary<AddressPart, string> RIAddress, Dictionary<AddressPart, string> MAddress, Dictionary<AddressPart, string> DMAddress, Dictionary<AddressPart, string> CRAddress)
        {

            DataRow dr = NewRow();
            dr[this.ClientCode] = ClientCode;
            dr[this.ClientName] = ClientName;
            dr[this.TFN] = TFN;
            dr[this.ABN] = ABN;
            dr[this.Adviser] = Adviser;
            //dr[this.Email] = Email;
            //dr[this.Phone] = Phone;
            dr[this.InvestorStatus] = InvestorStatus;
            dr[this.LimitedPowerofAttorney] = LimitedPowerofAttorney;
            if (BAddress != null)
            {
                dr[this.BAddressLine1] = BAddress[AddressPart.Addressline1];
                dr[this.BAddressLine2] = BAddress[AddressPart.Addressline2];
                dr[this.BPostCode] = BAddress[AddressPart.PostCode];
                dr[this.BCountry] = BAddress[AddressPart.Country];
                dr[this.BState] = BAddress[AddressPart.State];
                dr[this.BSubRub] = BAddress[AddressPart.Suburb];
            }
            if (RAddress != null)
            {
                dr[this.RAddressLine1] = RAddress[AddressPart.Addressline1];
                dr[this.RAddressLine2] = RAddress[AddressPart.Addressline2];
                dr[this.RPostCode] = RAddress[AddressPart.PostCode];
                dr[this.RCountry] = RAddress[AddressPart.Country];
                dr[this.RState] = RAddress[AddressPart.State];
                dr[this.RSubRub] = RAddress[AddressPart.Suburb];
            }
            if (RIAddress != null)
            {
                dr[this.RIAddressLine1] = RIAddress[AddressPart.Addressline1];
                dr[this.RIAddressLine2] = RIAddress[AddressPart.Addressline2];
                dr[this.RIPostCode] = RIAddress[AddressPart.PostCode];
                dr[this.RICountry] = RIAddress[AddressPart.Country];
                dr[this.RIState] = RIAddress[AddressPart.State];
                dr[this.RISubRub] = RIAddress[AddressPart.Suburb];
            }
            if (MAddress != null)
            {
                dr[this.MAddressLine1] = MAddress[AddressPart.Addressline1];
                dr[this.MAddressLine2] = MAddress[AddressPart.Addressline2];
                dr[this.MPostCode] = MAddress[AddressPart.PostCode];
                dr[this.MCountry] = MAddress[AddressPart.Country];
                dr[this.MState] = MAddress[AddressPart.State];
                dr[this.MSubRub] = MAddress[AddressPart.Suburb];
            }
            if (DMAddress != null)
            {
                dr[this.DMAddressLine1] = DMAddress[AddressPart.Addressline1];
                dr[this.DMAddressLine2] = DMAddress[AddressPart.Addressline2];
                dr[this.DMPostCode] = DMAddress[AddressPart.PostCode];
                dr[this.DMCountry] = DMAddress[AddressPart.Country];
                dr[this.DMState] = DMAddress[AddressPart.State];
                dr[this.DMSubRub] = DMAddress[AddressPart.Suburb];
            }
            if (CRAddress != null)
            {
                dr[this.CRAddressLine1] = CRAddress[AddressPart.Addressline1];
                dr[this.CRAddressLine2] = CRAddress[AddressPart.Addressline2];
                dr[this.CRPostCode] = CRAddress[AddressPart.PostCode];
                dr[this.CRCountry] = CRAddress[AddressPart.Country];
                dr[this.CRState] = CRAddress[AddressPart.State];
                dr[this.CRSubRub] = CRAddress[AddressPart.Suburb];
            }
            
            Rows.Add(dr);
        }

    }
    public class ClientSignatoryDetailsTable : DataTable
    {
        private const string Name = "ClientSignatories";
        public readonly string ClientCode = "ClientCode";
        public readonly string SignatoryName = "SignatoryName";
        public readonly string TFN = "TFN";
        //public readonly string ABN = "ABN";
        public readonly string Email = "Email";
        public readonly string Phone = "Phone";
        public readonly string WorkPhone = "WorkPhone";
        public readonly string MobilePhone = "MbilePhone";
        public readonly string RAddressLine1 = "RAddressLine1";
        public readonly string RAddressLine2 = "RAddressLine2";
        public readonly string RPostCode = "RPostCode";
        public readonly string RCountry = "RCountry";
        public readonly string RState = "RState";
        public readonly string RSubRub = "RSubRub";
     
        public readonly string MAddressLine1 = "MAddressLine1";
        public readonly string MAddressLine2 = "MAddressLine2";
        public readonly string MPostCode = "MPostCode";
        public readonly string MCountry = "MCountry";
        public readonly string MState = "MState";
        public readonly string MSubRub = "MSubRub";
     

        internal ClientSignatoryDetailsTable()
        {
            TableName =Name;
            Columns.Add(ClientCode);
            Columns.Add(SignatoryName);
            Columns.Add(TFN);
            //Columns.Add(ABN);
            Columns.Add(Email);
            Columns.Add(Phone);
            Columns.Add(WorkPhone);
            Columns.Add(MobilePhone);
           
            Columns.Add(RAddressLine1);
            Columns.Add(RAddressLine2);
            Columns.Add(RPostCode);
            Columns.Add(RCountry);
            Columns.Add(RState);
            Columns.Add(RSubRub);
            
            Columns.Add(MAddressLine1);
            Columns.Add(MAddressLine2);
            Columns.Add(MPostCode);
            Columns.Add(MCountry);
            Columns.Add(MState);
            Columns.Add(MSubRub);
            
        }

        public void AddRow(string ClientCode,string SignatoryName,string TFN,/*string ABN,*/string Email,string Phone,string workPhone,string mobilePhone, Dictionary<AddressPart, string> RAddress, Dictionary<AddressPart, string> MAddress)
        {


            DataRow dr = NewRow();


            dr[this.ClientCode] = ClientCode;
            dr[this.SignatoryName] = SignatoryName;
            dr[this.TFN] = TFN;
            //dr[this.ABN] = ABN;
            dr[this.Email] = Email;
            dr[this.Phone] = Phone;
            dr[this.WorkPhone] = workPhone;
            dr[this.MobilePhone] = mobilePhone;
            if (RAddress != null)
            {
                dr[this.RAddressLine1] = RAddress[AddressPart.Addressline1];
                dr[this.RAddressLine2] = RAddress[AddressPart.Addressline2];
                dr[this.RPostCode] = RAddress[AddressPart.PostCode];
                dr[this.RCountry] = RAddress[AddressPart.Country];
                dr[this.RState] = RAddress[AddressPart.State];
                dr[this.RSubRub] = RAddress[AddressPart.Suburb];
            }
            
            if (MAddress != null)
            {
                dr[this.MAddressLine1] = MAddress[AddressPart.Addressline1];
                dr[this.MAddressLine2] = MAddress[AddressPart.Addressline2];
                dr[this.MPostCode] = MAddress[AddressPart.PostCode];
                dr[this.MCountry] = MAddress[AddressPart.Country];
                dr[this.MState] = MAddress[AddressPart.State];
                dr[this.MSubRub] = MAddress[AddressPart.Suburb];
            }
           
            Rows.Add(dr);
        }

       
    }
    public class ClientModelTable : DataTable
    {
        private const string name = "ClientModels";
        public readonly string ClientCode = "ClientCode";
        public readonly string ModelCode = "ModelCode";
        public readonly string ModelName = "ModelName";

        internal ClientModelTable()
        {
            TableName = name;
            Columns.Add(ClientCode);
            Columns.Add(ModelCode);
            Columns.Add(ModelName);
        }
        public void AddRow(string ClientCode,string ModelCode,string ModelName)
        {
            DataRow dr = NewRow();
            dr[this.ClientCode] = ClientCode;
            dr[this.ModelCode] = ModelCode;
            dr[this.ModelName] = ModelName;
            Rows.Add(dr);
        }
    }
}
