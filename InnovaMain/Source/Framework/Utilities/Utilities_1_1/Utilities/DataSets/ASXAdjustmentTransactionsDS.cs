﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Data;

namespace Oritax.TaxSimp.DataSets
{
    public partial class ASXAdjustmentTransactionsDS : UMABaseDS
    {
        public const string ASXADJTABLE = "ASXAdjustmentTable"; 
        public const string INVESTCODE = "InvestmentCode";
        public const string PARENTTRANID = "ParentID";
        public const string TRANID = "TransactionID";
        public const string TYPE = "Type";
        public const string TRADEDATE = "TradeDate";
        public const string UNITS = "Units";
        public const string NETVALUE = "NetValue";
        public const string BROKERAGEAMT = "BrokerageAmount";
        public const string TRANSACTIONTYPE = "TransactionType";
        public const string UNITPRICE = "UnitPrice";
        public Guid ParentTransactionID = Guid.Empty; 
        public ASXAdjustmentTransactionsDS()
        {
            DataTable dt = new DataTable(ASXAdjustmentTransactionsDS.ASXADJTABLE);
            dt.Columns.Add(INVESTCODE, typeof(string));
            dt.Columns.Add(PARENTTRANID, typeof(Guid));
            dt.Columns.Add(TRANID, typeof(Guid));
            dt.Columns.Add(TYPE, typeof(string));
            dt.Columns.Add(TRADEDATE, typeof(DateTime));
            dt.Columns.Add(UNITS, typeof(decimal));
            dt.Columns.Add(NETVALUE, typeof(decimal));
            dt.Columns.Add(BROKERAGEAMT, typeof(decimal));
            dt.Columns.Add(TRANSACTIONTYPE, typeof(string));
            dt.Columns.Add(UNITPRICE, typeof(decimal));
            this.Tables.Add(dt); 
        }
    }
}
