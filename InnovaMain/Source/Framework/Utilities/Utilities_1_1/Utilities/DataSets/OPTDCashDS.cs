﻿using System;
using System.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class OPTDCashDS : DataSet
    {
        public TDCashTable TDCashTable = new TDCashTable();

        public OPTDCashDS()
        {
            Tables.Add(TDCashTable);
        }
    }

    public class TDCashTable : DataTable
    {
        public readonly string TABLENAME = "TDCashList";

        public readonly string BROKERID = "BrokerID";
        public readonly string BANKID = "BankID";
        public readonly string BROKERNAME = "BrokerName";
        public readonly string BANKNAME = "BankName";
        public readonly string PERCENTAGE = "Percentage";
        public readonly string DURATION = "Duration";
        public readonly string MIN = "Min";
        public readonly string MAX = "Max";
        public readonly string AMOUNT = "Amount";
        public readonly string RATING = "Rating";
        public readonly string PRODUCTID = "ProductID";
        public readonly string TDACCOUNTCID = "TDAccountCID";
        public readonly string HOLDINGLIMIT = "HoldingLimit";
        public readonly string TOTALHOLDING = "TotalHolding";
        public readonly string AVAILABLEFUNDS = "AvailableFunds";

        internal TDCashTable()
        {
            TableName = TABLENAME;
            Columns.Add(BROKERID, typeof(Guid));
            Columns.Add(BANKID, typeof(Guid));
            Columns.Add(BROKERNAME, typeof(string));
            Columns.Add(BANKNAME, typeof(string));
            Columns.Add(PERCENTAGE, typeof(decimal));
            Columns.Add(DURATION, typeof(string));
            Columns.Add(MIN, typeof(decimal));
            Columns.Add(MAX, typeof(decimal));
            Columns.Add(AMOUNT, typeof(decimal));
            Columns.Add(RATING, typeof(string));
            Columns.Add(PRODUCTID, typeof(Guid));
            Columns.Add(TDACCOUNTCID, typeof(Guid));
            Columns.Add(HOLDINGLIMIT, typeof(decimal));
            Columns.Add(TOTALHOLDING, typeof(decimal));
            Columns.Add(AVAILABLEFUNDS, typeof(decimal));
        }
    }
}
