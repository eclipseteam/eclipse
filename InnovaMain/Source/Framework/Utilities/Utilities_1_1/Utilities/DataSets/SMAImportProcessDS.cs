﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class SMAImportProcessDS : ImportProcessDS
    {
        public DataTable UserDetail = new DataTable();
        public string MemberID = String.Empty;
        public string SuperID = String.Empty;
        public string ClientID = string.Empty;
        public string ClientUMAID = string.Empty;
        public Guid ClientCID = Guid.Empty;
        public List<SecuritiesEntity> Securities = new List<SecuritiesEntity>();
        public const string CASHTRANSACTIONTABLE = "CashTransactionViewModel";
        public const string MISTRANSACTIONTABLE = "InvestmentProfileAssetViewModel";
        public const string HOLDINGVIEWTABLE = "InvestmentViewModel";
        public const string RESULTTABLE = "ResultTable";
        public const string MESSAGE = "Message";
        public const string ISMISSINGITEM = "IsMissingItem";
        public const string HASERRORS = "HasErrors";
        public decimal AccruedHolding = 0;
        public string AccruedDescription = string.Empty;
        public string AccruedDescriptionProductCode = string.Empty;

        public SMAImportProcessDS()
        {
            StartDate = DateTime.Now.AddDays(-20);
            EndDate = DateTime.Now;
        }

        public void AddBasicResultTableToDataSet()
        {
            DataTable resultTable = new DataTable(RESULTTABLE);
            resultTable.Columns.Add(MESSAGE);
            resultTable.Columns.Add(ISMISSINGITEM);
            resultTable.Columns.Add(HASERRORS);
            this.Tables.Add(resultTable);
        }
    }
}
