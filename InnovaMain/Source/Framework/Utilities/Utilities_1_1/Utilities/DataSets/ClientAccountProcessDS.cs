﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class ClientAccountProcessDS : DataSet
    {
        public DatasetCommandTypes Command = DatasetCommandTypes.Get;
        public const string ACCOUNTPROCESS_TABLE = "AccountProcess";
        public const string SERVICETYPE = "ServiceType";
        public const string ASSET = "Asset";
        public const string PRODUCT = "Product";
        public const string TASK = "Task";
        public const string STATUS = "Status";
        public const string COMPLETED = "Completed";
        public const string LINKENTIYCLID = "LinkEntiyCLID";
        public const string LINKENTIYCSID = "LinkEntiyCSID";
        public const string LINKENTIYFUNDID = "LinkEntiyFundID";
        public const string CID = "CID";
        public const string TASKID = "TaskID";
        public const string MODELID = "ModelID";
        public const string ASSETID = "AssetID";
        public const string MODEL = "Model";
        public const string LINKEDENTITYTYPE = "LinkedEntityType";
        public const string INSTITUTEID = "InstituteID";
        public const string INSTITUTENAME = "InstituteName";
        public const string BROKERID = "BrokerID";
        public const string BROKERNAME = "BrokerName";

        public const string LINKENTITYFUNDCODE = "LinkEntityFundCode";


        public const string PRODUCTSECURITIES_TABLE = "ProductSecuritiesTable";
        public const string ACCOUNTNUMBER = "AccountNumber";
        public const string INVESTMENTCODE = "InvestmentCode";

        
        public ClientAccountProcessDS()
        {
            Tables.Add(SecuritiesListTable());
            Tables.Add(ProductSecurtiesTable());
        }

        public DataTable SecuritiesListTable()
        {
            DataTable SecListTable = new DataTable(ACCOUNTPROCESS_TABLE);
            SecListTable.Columns.Add(TASK, typeof(string));
            SecListTable.Columns.Add(STATUS, typeof(string));
            SecListTable.Columns.Add(COMPLETED, typeof(bool));
            SecListTable.Columns.Add(LINKENTIYCLID, typeof(Guid));
            SecListTable.Columns.Add(LINKENTIYCSID, typeof(Guid));
            SecListTable.Columns.Add(LINKENTIYFUNDID, typeof(Guid));
            SecListTable.Columns.Add(CID, typeof(Guid));
            SecListTable.Columns.Add(LINKEDENTITYTYPE, typeof(string));
            SecListTable.Columns.Add(MODELID, typeof(Guid));
            SecListTable.Columns.Add(MODEL, typeof(string));
            SecListTable.Columns.Add(SERVICETYPE, typeof(string));
            SecListTable.Columns.Add(ASSETID, typeof(Guid));
            SecListTable.Columns.Add(ASSET, typeof(string));
            SecListTable.Columns.Add(TASKID, typeof(Guid));
            SecListTable.Columns.Add(PRODUCT, typeof(string));
            SecListTable.Columns.Add(INSTITUTEID, typeof(Guid));
            SecListTable.Columns.Add(INSTITUTENAME, typeof(string));
            SecListTable.Columns.Add(BROKERID, typeof(Guid));
            SecListTable.Columns.Add(BROKERNAME, typeof(string));
            SecListTable.Columns.Add(LINKENTITYFUNDCODE, typeof(string));
            SecListTable.Columns.Add(ACCOUNTNUMBER, typeof(string));
            return SecListTable;
        }
         public DataTable ProductSecurtiesTable()
        {
            DataTable SecListTable = new DataTable(PRODUCTSECURITIES_TABLE);
            SecListTable.Columns.Add(MODELID, typeof(Guid));
            SecListTable.Columns.Add(ASSETID, typeof(Guid));
            SecListTable.Columns.Add(TASKID, typeof(Guid));
            SecListTable.Columns.Add(INVESTMENTCODE, typeof(string));
           return SecListTable;
        }
    }
    
}
