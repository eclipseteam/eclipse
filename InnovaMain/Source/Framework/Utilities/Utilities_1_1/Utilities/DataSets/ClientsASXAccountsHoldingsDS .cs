﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class ClientsASXAccountsHoldingsDS : DataSet, IHasOrganizationUnit
    {
    
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public ASXHoldingTable ASXHoldingTable = new ASXHoldingTable();
        public ClientsASXAccountsHoldingsDS()
        {
            Tables.Add(ASXHoldingTable);
        }
        public OrganizationUnit Unit
        {
            get;
            set;
        }

        public int Command
        {
            get;
            set;
        }
    }
    public class ASXHoldingTable: DataTable
    {
        private readonly string TABLENAME = "ASXHoldingList";
        public readonly string ClientCID = "CID";
        public readonly string CLIENTID = "ClientID";
        public readonly string ENTITYCLIENTID = "EntityClientID";
        public readonly string TRANDINGNAME = "TrandingName";
        public readonly string SERVICETYPES = "ServiceTypes";
        public readonly string ACCOUNTNUMBER = "AccountNumber";
        public readonly string UNITHOLDING = "UnitHolding";
        public readonly string TRANSACTIONUNITHOLDING = "TransactionUnitHolding";
        public readonly string DIFFERENCE = "Difference";
        public readonly string UNSETTLEDORDER = "UnsettledOrder";
        public readonly string TOTAL = "Total";
       


        internal ASXHoldingTable()
        {
            TableName = TABLENAME;
            Columns.Add(ClientCID, typeof(Guid));
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(ENTITYCLIENTID, typeof(string));
            Columns.Add(TRANDINGNAME, typeof(string));
            Columns.Add(SERVICETYPES, typeof(string));
            Columns.Add(ACCOUNTNUMBER, typeof(string));
            Columns.Add(UNITHOLDING, typeof(decimal));
            Columns.Add(TRANSACTIONUNITHOLDING, typeof(decimal));
            Columns.Add(DIFFERENCE, typeof(decimal));
            Columns.Add(UNSETTLEDORDER, typeof(decimal));
            Columns.Add(TOTAL, typeof(decimal));
            
        }



        public void AddNewRow(Guid clientCID,string clinetID,string entityClientID,string tradingName,string serviceTypes,string accountNumber,decimal UnitHolding,decimal TransUnitHolding,decimal unsetteledOrder)
        {
            DataRow dr = NewRow();
            dr[ClientCID] = clientCID;
            dr[CLIENTID] = clinetID;
            dr[ENTITYCLIENTID] = entityClientID;
            dr[TRANDINGNAME] = tradingName;
            dr[SERVICETYPES] = serviceTypes;
            dr[ACCOUNTNUMBER] = accountNumber;
            dr[UNITHOLDING] = UnitHolding;
            dr[TRANSACTIONUNITHOLDING] = TransUnitHolding;
            dr[DIFFERENCE] =(UnitHolding-TransUnitHolding);
            dr[UNSETTLEDORDER] = unsetteledOrder;
            dr[TOTAL] = (UnitHolding+unsetteledOrder);

            Rows.Add(dr);

        }
    }
}
