﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class StatusDS : DataSet
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public string TypeFilter { get; set; }
        public const string TABLENAME = "StatusList";
        public const string NAME = "Name";
        public const string TYPE = "Type";

        public StatusDS()
        {
            DataTable tab = new DataTable(TABLENAME);
            tab.Columns.Add(NAME, typeof(string));
            tab.Columns.Add(TYPE, typeof(string));            
            this.Tables.Add(tab);            
        }
    }
}
