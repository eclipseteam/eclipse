﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class MaquarieCMATransactionsDS : UMABaseDS
    {
        public bool IsSuccess = false;
        public string MessageNumber = String.Empty;
        public string MessageSummary = String.Empty;
        public string MessageDetail = String.Empty;
      

        public const string MACQUARIEBANKTRANSACTIONSTABLE = "MACQUARIEBANKTRANSACTIONSTABLE";
        public const string ACCOUNTNUMBER = "AccountNumber";
        public const string TRANSACTIONDATE = "TransactionDate";
        public const string REVERSEFLAG = "ReversalFlag";
        public const string DEBITCREDIT = "DebitCredit";
        public const string TRANSACTIONTYPE = "TransactionType";
        public const string AMOUNT = "Amount";
        public const string NARRATIVE = "Narrative";
        public const string DATEMODIFIED = "DateModified";
        public const string TRANSACTIONCATEGORY = "TransactionCategory";

        public const string SYSTEMTRANSACTIONTYPE = "SystemTransactionType";
        public const string IMPORTTRANSACTIONTYPEDETAIL = "ImportTransactionTypeDetail";
        
        public const string MESSAGE = "Message";
        public const string HASERRORS = "HasErrors";
        public const string ISMISSINGITEM = "IsMissingItem";
        public const string CLIENTID = "ClientID";
    }
}
