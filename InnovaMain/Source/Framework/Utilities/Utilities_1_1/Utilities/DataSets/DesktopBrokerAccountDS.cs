﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class DesktopBrokerAccountDS : DataSet,IHasOrganizationUnit
    {
        public const string TABLENAME = "DesktopBrokerAccountList";
      
        public const string CID = "Cid";
        public const string CLID = "CLid";
        public const string CSID = "CSid";
        public const string ACCOUNTNAME = "AccountName";
        public const string ACCOUNTNO = "AccountNo";
        public const string ACCOUNTDESIGNATION = "AccountDesignation";
        public const string HINNUMBER = "HINNumber";
        public const string STATUS = "Status";


        public string RepUnitID = "";
        public string RepUnitSuperID = "";
        public Guid RepUnitCID = Guid.Empty;

        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public DesktopBrokerAccounts DesktopBrokerAccountsTable = new DesktopBrokerAccounts();

        public DesktopBrokerAccountDS()
        {
            Tables.Add(DesktopBrokerAccountsTable);
        }

        public OrganizationUnit Unit { get; set; }
        public int Command { get; set; }
    }

    public class DesktopBrokerAccounts : DataTable
    {
        public readonly string TABLENAME = "DesktopBrokerAccountList";
        public readonly string CID = "Cid";
        public readonly string CLID = "CLid";
        public readonly string CSID = "CSid";
        public readonly string ACCOUNTNAME = "AccountName";
        public readonly string ACCOUNTNO = "AccountNo";
        public readonly string ACCOUNTDESIGNATION = "AccountDesignation";
        public readonly string HINNUMBER = "HINNumber";
        public readonly string STATUS = "Status";
        public readonly string CLIENTCID = "ClientCID";
        public readonly string CLIENTNAME = "ClientName";
        public readonly string CLIENTCODE= "ClientCode";
        public readonly string SERVICETYPE = "ServiceType";
        public readonly string UNITSHELD = "UnitsHeld";
        internal DesktopBrokerAccounts()
        {
            TableName = TABLENAME;
            Columns.Add(CLIENTCID, typeof(Guid));
            Columns.Add(CLIENTNAME, typeof(string));
            Columns.Add(CLIENTCODE, typeof(string));
            Columns.Add(CID, typeof(Guid));
            Columns.Add(CLID, typeof(Guid));
            Columns.Add(CSID, typeof(Guid));
            Columns.Add(ACCOUNTNAME, typeof(string));
            Columns.Add(ACCOUNTNO, typeof(string));
            Columns.Add(ACCOUNTDESIGNATION, typeof(string));
            Columns.Add(HINNUMBER, typeof(string));
            Columns.Add(STATUS, typeof(string));
            Columns.Add(SERVICETYPE, typeof(string));
            Columns.Add(UNITSHELD, typeof(decimal));
        }
    }
}
