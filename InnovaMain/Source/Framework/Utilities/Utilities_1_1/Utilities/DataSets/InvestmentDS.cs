﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    

    public class InvestmentDS : DataSet
    {
        public Guid InvestmentID = Guid.Empty;


        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public const string ID= "ID";

        public const string INVESTMENTMANAGEMENT = "InvestmentManagement";
        public const string INVESTMENTMANAGER = "InvestmentManager";
        public const string PORTFOLOMANAGER = "PortfoloManager";
        public const string ASSETTYPE = "AssetType";
        public const string REGION = "Region";
        public const string OBJECTIVE = "Objective";
        public const string FINDSIZE = "FindSize";
        public const string MINWITHRAWAL = "MinWithrawal";
        public const string MINADDITION = "MinAddition";
        public const string MININVESTMENT = "MinInvestment";
        public const string ENTRYFEE = "EntryFee";
        public const string EXITFEE = "ExitFee";
        public const string BUYSELLSPREAD = "BuySellSpread";
        public const string MANAGEMENTFEE = "ManagementFee";
        public const string MANAGEMENTFEEOF = "ManagementFeeAsOf";
        public const string ICR = "ICR";
        public const string INFO = "INFO";
        public const string BENCHMARK = "BENCHMARK";
        public const string INCEPTIONDATE = "InceptionDate";
        public const string INVESTMENTTIME = "InvestmentTimeRecommended";


        public const string ALLOCATIONS = "Allocations";
        public const string NAME = "Name";
        public const string PERCENTAGE = "PercentageAllocation";

        public InvestmentDS()
        {
            this.Tables.Add(CreateInvestmentManagementTable());
            this.Tables.Add(ProductsDS.CreateProductsTable());
            this.Tables.Add(CreateAllocationsTable());
        }

        public static DataTable CreateInvestmentManagementTable()
        {
            DataTable table = new DataTable(INVESTMENTMANAGEMENT);
            table.Columns.Add(ID, typeof(Guid));


            table.Columns.Add(INVESTMENTMANAGER, typeof(string));
            table.Columns.Add(PORTFOLOMANAGER, typeof(string));
            table.Columns.Add(ASSETTYPE, typeof(string));
            table.Columns.Add(REGION, typeof(string));
            table.Columns.Add(OBJECTIVE, typeof(string));
            table.Columns.Add(FINDSIZE, typeof(string));
            table.Columns.Add(INFO, typeof(string));
            table.Columns.Add(INVESTMENTTIME, typeof(string));
            table.Columns.Add(INCEPTIONDATE, typeof(string));

            table.Columns.Add(MINWITHRAWAL, typeof(string));
            table.Columns.Add(MINADDITION, typeof(string));
            table.Columns.Add(MININVESTMENT, typeof(string));
            table.Columns.Add(ENTRYFEE, typeof(string));
            table.Columns.Add(EXITFEE, typeof(string));
            table.Columns.Add(BUYSELLSPREAD, typeof(string));
            table.Columns.Add(MANAGEMENTFEE, typeof(string));
            table.Columns.Add(MANAGEMENTFEEOF, typeof(string));
            table.Columns.Add(ICR, typeof(string));
            table.Columns.Add(BENCHMARK, typeof(Guid));

            return table;
        }

        public static DataTable CreateAllocationsTable()
        {
            DataTable table = new DataTable(ALLOCATIONS);
            table.Columns.Add(ID, typeof(Guid));
            table.Columns.Add(NAME, typeof(string));
            table.Columns.Add(PERCENTAGE, typeof(double));

            return table;
        }    
    }
}
