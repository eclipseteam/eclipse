﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class BankTransactionDS : BaseClientDetails,IHasOrganizationUnit
    {
        public string CLIENTIDPara = string.Empty;
        public string CLIENTNAMEPara = string.Empty;
        public const string BANKACCOUNTLIST = "BankAccountList";
        public const string TDACCOUNTLIST = "TDAccountList";
        public const string BANKCID = "BankCID";
        public const string BSBNO = "BSBNo";
        public const string BANKACCOUNTNO = "AccountNo";
        public const string BANKCODE = "BANKCODE";
        public const string BANKACCOUNTNAME = "AccountName";
        public const string DIVIDENDSTATUS = "DividendStatus";
        public DateTime StartDate = DateTime.Now.AddYears(-10);
        public DateTime EndDate = DateTime.Now;
        public bool AddEmptyRow = true;
        public bool IsSinceInception = false; 
        public string DateInfo()
        {
            return this.StartDate.ToString("dd/MM/yyyy") + " - " + this.EndDate.ToString("dd/MM/yyyy");
        }

        public BankTransactionDS()
        {
            DataTable bankDetails = new DataTable(BANKACCOUNTLIST);
            bankDetails.Columns.Add(BANKCID, typeof(Guid));
            bankDetails.Columns.Add(BSBNO, typeof(string));
            bankDetails.Columns.Add(BANKACCOUNTNO, typeof(string));
            bankDetails.Columns.Add(BANKCODE, typeof(string));
            bankDetails.Columns.Add(BANKACCOUNTNAME, typeof(string));
            bankDetails.Columns.Add(DIVIDENDSTATUS, typeof(string));
            this.Tables.Add(bankDetails);
     }

        public OrganizationUnit Unit
        {
            get;set;
        }

        public int Command
        {
            get;
            set;
        }
    }

}
