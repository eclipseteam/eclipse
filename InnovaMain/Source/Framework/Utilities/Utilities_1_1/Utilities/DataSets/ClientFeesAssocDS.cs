﻿using System.Data;
using Oritax.TaxSimp.Data;
using System;

namespace Oritax.TaxSimp.DataSets
{
    public class ClientFeesAssocDS : DataSet
    {
        public const string CLIENTID = "ClientID";
        public const string CLIENTCID = "ClientCID";
        public const string CLIENTNAME= "ClientName";
        public const string SERVICETYPE = "ServiceType";
        public const string FEETYPE = "FeeType";
        public const string DESCRIPTION = "Description";
        public const string COMMENTS = "Comments";
        public const string FEETEMPLATECID = "FeeTemplateCID";
        public const string ADVISERNAME = "ADVISERNAME";
        public const string CLIENTFEEASSOCIATIONSTABLE = "ClientFeeAssociationsTable";

        public ClientFeesAssocDS()
        {
             this.Tables.Add(ClientFeeAssociationsTable());
        }

        public DataTable ClientFeeAssociationsTable()
        {
            DataTable dt = new DataTable(CLIENTFEEASSOCIATIONSTABLE);
            dt.Columns.Add(CLIENTCID, typeof(Guid));
            dt.Columns.Add(CLIENTID, typeof(string));
            dt.Columns.Add(CLIENTNAME, typeof(string));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(COMMENTS, typeof(string));
            dt.Columns.Add(SERVICETYPE, typeof(string));
            dt.Columns.Add(FEETYPE, typeof(string));
            dt.Columns.Add(FEETEMPLATECID, typeof(Guid));
            dt.Columns.Add(ADVISERNAME, typeof(string));
            return dt; 
        }
    }
}
