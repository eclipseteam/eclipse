﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class SecurityDividendDetailsDS : DataSet
    {
        public Guid ID = Guid.Empty;
        public Guid SecID = Guid.Empty;
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;
        public DividendEntity Entity { get; set; }
        public SecurityDividendDetailsDS()
        {
        
        }
    }
}
