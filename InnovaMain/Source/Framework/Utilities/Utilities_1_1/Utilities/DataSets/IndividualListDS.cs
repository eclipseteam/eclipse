﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
    public class IndividualListDS : UMABaseDS
    {
        public IndividualListDS()
        {
            DataTable table = new DataTable(INDIVIDUALISTTABLENAME);
            table.Columns.Add(CLIENTID, typeof(string));
            table.Columns.Add(CID, typeof(Guid));
            table.Columns.Add(CLID, typeof(Guid));
            table.Columns.Add(CSID, typeof(string));
            table.Columns.Add(PERSONALTITLE, typeof(string));
            table.Columns.Add(FIRSTNAME, typeof(string));
            table.Columns.Add(MIDDLENAME, typeof(string));
            table.Columns.Add(SURNAME, typeof(string));
            table.Columns.Add(EMAILADDRESS, typeof(string));
            table.Columns.Add(DOB, typeof(DateTime));
            table.Columns.Add(HMPHCOUNTRYCODE, typeof(string));
            table.Columns.Add(HMPHCITYCODE, typeof(string));
            table.Columns.Add(HMPHNO, typeof(string));
            table.Columns.Add(WKPHCOUNTRYCODE, typeof(string));
            table.Columns.Add(WKPHCITYCODE, typeof(string));
            table.Columns.Add(WKPHNO, typeof(string));
            table.Columns.Add(FAXPHCOUNTRYCODE, typeof(string));
            table.Columns.Add(FAXPHCITYCODE, typeof(string));
            table.Columns.Add(FAXPHNO, typeof(string));
            table.Columns.Add(MBPHCOUNTRYCODE, typeof(string));
            table.Columns.Add(MBPHNO, typeof(string));
            table.Columns.Add(OCCUAPATION, typeof(string));
            table.Columns.Add(TIN, typeof(string));
            table.Columns.Add(TFN, typeof(string));
            table.Columns.Add(EMPLOYER, typeof(string));
            table.Columns.Add(DOCNUMBERLIC, typeof(string));
            table.Columns.Add(ISPHOTOSHOWNLIC, typeof(string));
            table.Columns.Add(ISSUEDATELIC, typeof(string));
            table.Columns.Add(EXPDAELIC, typeof(string));
            table.Columns.Add(ISSUEPLACELIC, typeof(string));
            table.Columns.Add(ORGNAMELIC, typeof(string));
            table.Columns.Add(ISORIGINALLIC, typeof(string));
            table.Columns.Add(DOCNUMBERPASS, typeof(string));
            table.Columns.Add(ISPHOTOSHOWNPASS, typeof(string));
            table.Columns.Add(ISSUEDATEPASS, typeof(string));
            table.Columns.Add(EXPDAEPASS, typeof(string));
            table.Columns.Add(ISSUEPLACEPASS, typeof(string));
            table.Columns.Add(ORGNAMEPASS, typeof(string));
            table.Columns.Add(ISORIGINALPASS, typeof(string));
            table.Columns.Add(ADDRESSLN1RESI, typeof(string));
            table.Columns.Add(ADDRESSLN2RESI, typeof(string));
            table.Columns.Add(SUBURBRESI, typeof(string));
            table.Columns.Add(STATERESI, typeof(string));
            table.Columns.Add(POSTCODERESI, typeof(string));
            table.Columns.Add(COUNTRYRESI, typeof(string));
            table.Columns.Add(ADDRESSLN1MAILING, typeof(string));
            table.Columns.Add(ADDRESSLN2MAILING, typeof(string));
            table.Columns.Add(SUBURBMAILING, typeof(string));
            table.Columns.Add(STATEMAILING, typeof(string));
            table.Columns.Add(POSTCODEMAILING, typeof(string));
            table.Columns.Add(COUNTRYMAILING, typeof(string));
            this.Tables.Add(table);
        }

        public const string INDIVIDUALISTTABLENAME = "IndividualList";
        public const string CLIENTID = "CLIENTID";
        public const string CID = "CID";
        public const string CLID = "CLID";
        public const string CSID = "CSID";
        public const string PERSONALTITLE = "PERSONALTITLE";
        public const string FIRSTNAME = "FIRSTNAME";
        public const string MIDDLENAME = "MIDDLENAME";
        public const string SURNAME = "SURNAME";
        public const string EMAILADDRESS = "EMAILADDRESS";
        public const string DOB = "DOB";
        public const string HMPHCOUNTRYCODE = "HMPHCOUNTRYCODE";
        public const string HMPHCITYCODE = "HMPHCITYCODE";
        public const string HMPHNO = "HMPHNO";
        public const string WKPHCOUNTRYCODE = "WKPHCOUNTRYCODE";
        public const string WKPHCITYCODE = "WKPHCITYCODE";
        public const string WKPHNO = "WKPHNO";
        public const string FAXPHCOUNTRYCODE = "FAXPHCOUNTRYCODE";
        public const string FAXPHCITYCODE = "FAXPHCITYCODE";
        public const string FAXPHNO = "FAXPHNO";
        public const string MBPHCOUNTRYCODE = "MBPHCOUNTRYCODE";
        public const string MBPHNO = "MBPHNO";
        public const string OCCUAPATION = "OCCUAPATION";
        public const string TIN = "TIN";
        public const string TFN = "TFN";
        public const string EMPLOYER = "EMPLOYER";
        public const string DOCNUMBERLIC = "DOCNUMBERLIC";
        public const string ISPHOTOSHOWNLIC = "ISPHOTOSHOWNLIC";
        public const string ISSUEDATELIC = "ISSUEDATELIC";
        public const string EXPDAELIC = "EXPDAELIC";

        public const string ISSUEPLACELIC = "ISSUEPLACELIC";
        public const string ORGNAMELIC = "ORGNAMELIC";
        public const string ISORIGINALLIC = "ISORIGINALLIC";
        public const string DOCNUMBERPASS = "DOCNUMBERPASS";
        public const string ISPHOTOSHOWNPASS = "ISPHOTOSHOWNPASS";
        public const string ISSUEDATEPASS = "ISSUEDATEPASS";
        public const string EXPDAEPASS = "EXPDAEPASS";
        public const string ISSUEPLACEPASS = "ISSUEPLACEPASS";
        public const string ORGNAMEPASS = "ORGNAMEPASS";
        public const string ISORIGINALPASS = "ISORIGINALPASS";
        public const string ADDRESSLN1RESI = "ADDRESSLN1RESI";
        public const string ADDRESSLN2RESI = "ADDRESSLN2RESI";
        public const string SUBURBRESI = "SUBURBRESI";
        public const string STATERESI = "STATERESI";
        public const string POSTCODERESI = "POSTCODERESI";
        public const string COUNTRYRESI = "COUNTRYRESI";
        public const string ADDRESSLN1MAILING = "ADDRESSLN1MAILING";
        public const string ADDRESSLN2MAILING = "ADDRESSLN2MAILING";
        public const string SUBURBMAILING = "SUBURBMAILING";
        public const string STATEMAILING = "STATEMAILING";
        public const string POSTCODEMAILING = "POSTCODEMAILING";
        public const string COUNTRYMAILING = "COUNTRYMAILING";

    
    
    }
}
