﻿using System;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.DataSets
{
   
    public class ClientReportDataValidationDS : DataSet, IHasOrganizationUnit,ISupportRecordCount
    {
        public DatasetCommandTypes CommandType = DatasetCommandTypes.Get;

        public ClientReportDataValidationTable ClientReportShareTable = new ClientReportDataValidationTable();

        public ClientReportDataValidationDS()
        {
            Tables.Add(ClientReportShareTable);
        }


        public OrganizationUnit Unit { get; set; }

        public int Command { get; set; }
        public int ClientCount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Count{ get; set; }
        
    }

    public class ClientReportDataValidationTable : DataTable
    {
        private readonly string TABLENAME = "ClientValidationData";
        public readonly string CLIENTID = "ClientID";
        public readonly string CLIENTNAME = "ClientName";
        public readonly string CLIENTCID = "ClientCID";
        public readonly string CLIENTCLID = "ClientCLID";
        public readonly string CLIENTCSID = "ClientCSID";
        public readonly string REPORTNAME = "ReportName";
        public readonly string MESSAGE = "Message";
        internal ClientReportDataValidationTable()
        {
            TableName = TABLENAME;
            Columns.Add(CLIENTID, typeof(string));
            Columns.Add(CLIENTNAME, typeof(string));
            Columns.Add(CLIENTCID, typeof(Guid));
            Columns.Add(CLIENTCLID, typeof(Guid));
            Columns.Add(CLIENTCSID, typeof(Guid));
            Columns.Add(REPORTNAME, typeof(string));
            Columns.Add(MESSAGE, typeof(string));
           
        }

        public void AddRow(string clientID,string clientName,Guid cliendCID,Guid clientClid,Guid clientCsid,string reportName,string message )
        {
            var row=NewRow();
            row[CLIENTID] = clientID;
            row[CLIENTNAME] = clientName;
            row[CLIENTCID] = cliendCID;
            row[CLIENTCLID] = clientClid;
            row[CLIENTCSID] = clientCsid;
            row[REPORTNAME] = reportName;
            row[MESSAGE] = message;

            Rows.Add(row);

        }

    }
}
