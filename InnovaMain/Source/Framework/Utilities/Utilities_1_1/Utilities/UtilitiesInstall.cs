using System;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// Summary description for BrokerManagedComponentBaseFW.
	/// </summary>
	public class UtilitiesInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="D14CBE28-A8C8-46d1-9B82-FE5B4EBE8694";
		public const string ASSEMBLY_NAME="Utilities_1_1";
		public const string ASSEMBLY_DISPLAYNAME="Utilities V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		public const string ASSEMBLY_REVISION="1";	

		#endregion
	}
}
