﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Extensions
{
    static public class DecimalExtensions
    {
        private static decimal RoundOffAmount(this decimal inputValue)
        {
            return Math.Round(inputValue, 2);
        }
        private static decimal RoundOffUnits(this decimal inputValue)
        {
            return Math.Round(inputValue, 4);
        }

        public static bool IsAbsLessThanEquals(this decimal inputvalue1, decimal inputvalue2, decimal precision)
        {

            return Math.Abs(inputvalue1 - inputvalue2) <= precision;
        }

        public static bool IsAbsMatched(this decimal inputvalue1, decimal inputvalue2, decimal precision )
        {

            return  Math.Abs(inputvalue1 - inputvalue2) < precision;
        }

        public static bool IsMatchedInRange(this decimal inputvalue1, decimal inputvalue2,  decimal minflunction, decimal maxflunction)
        {
            return Math.Abs(inputvalue2) >= Math.Abs(inputvalue1 * minflunction) && Math.Abs(inputvalue2) <= Math.Abs(inputvalue1 * maxflunction);
        }

        public static bool IsMatched(this decimal inputvalue1, decimal inputvalue2)
        {
            return (inputvalue1 == inputvalue2) ;
        }

        public static decimal CalculateBrokrage(this decimal amount)
        {
            return amount * (decimal)0.0011;
        }

        public static decimal GetMax(this decimal value1, decimal value2)
        {
            return (value1 >= value2) ? value1 : value2;
        }

        public static decimal CalculatePercentage(this decimal value, decimal total)
        {
            return (value / total) * 100;
        }
        public static decimal? CalculatePercentage(this decimal? value, decimal? total)
        {
            return (value / total) * 100;
        }

        public static decimal CalculateValueFromPercentage(this decimal value, decimal percent)
        {
            return (value * percent) / 100;
        }
        
        public static decimal? CalculateValueFromPercentage(this decimal? value, decimal? percent)
        {
            return (value * percent) / 100;
        }

        public static string ToPositiveString(this decimal? value)
        {
           return value.HasValue?Math.Abs(value.Value).ToString(): string.Empty;
        }
        public static decimal? Normalize(this decimal? value)
        {
            return value / 1.000000000000000000000000000000000m;
        }
    }
}
