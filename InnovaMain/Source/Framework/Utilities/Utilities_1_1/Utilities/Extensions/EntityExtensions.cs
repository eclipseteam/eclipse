﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oritax.TaxSimp.Data;
using System.Reflection;
using System;

namespace Oritax.TaxSimp.Extensions
{
    public static class EntityExtensions
    {

        public static void AddSingleKeyValueExtension(this EntityTag tag, string name, object value)
        {
            if (tag.Tagged.ContainsKey(name)) return;

            string key2 = null;


            tag.Map.Mapping.TryGetValue(name, out key2);
            if (string.IsNullOrEmpty(key2)) return;
            if (value == null) return;

            tag.Tagged.Add(key2, value.ToString());

        }

        public static void DoTaggedExtension<T>(this T t, EntityTag tag)
            where T : class
        {
            if (t == null) return;
            string key1 = null;
            string key2 = null;
            object value = null;

            string[] appendersPrefix = { "Signatory1", "Contact1", "Applicant1", "Signatory2", "Contact2", "Applicant2" };

          
                string[] matchingKeys = { "MobilePhoneNumber", "WorkPhoneNumber", "HomePhoneNumber" };

                List<string> phoneKeys = new List<string>();

                foreach (var appender in appendersPrefix)
                {
                    foreach (var matchingKey in matchingKeys)
                    {
                        phoneKeys.Add(string.Format("{0}.{1}", appender, matchingKey));
                    }
                }

                PropertyInfo[] propertyInfos = typeof(T).GetProperties();
                PropertyInfo[] nonNumberProperties = propertyInfos.Where(p => matchingKeys.Contains(p.Name) == false).ToArray();

                PropertyInfo[] numberProperties = propertyInfos.Where(p => matchingKeys.Contains(p.Name) == true).OrderByDescending(p => p.Name.Length).ThenByDescending(p => p.Name).ToArray();
                MapPropertiesWithTag<T>(t, tag, ref key1, ref key2, ref value, nonNumberProperties);
                MapPropertiesWithTag<T>(t, tag, ref key1, ref key2, ref value, numberProperties);

                return;

          


         //   MapPropertiesWithTag<T>(t, tag, ref key1, ref key2, ref value,typeof(T).GetProperties());
        }

        private static void MapPropertiesWithTag<T>(T t, EntityTag tag, ref string key1, ref string key2, ref object value,PropertyInfo[] propertyInfos) where T : class
        {
            foreach (var each in propertyInfos)
            {
                key1 = string.IsNullOrEmpty(tag.Appender) ? each.Name : string.Format("{0}.{1}", tag.Appender, each.Name);
                tag.Map.Mapping.TryGetValue(key1, out key2);
                if (string.IsNullOrEmpty(key2)) continue;
                value = each.GetValue(t, null);
                if (value == null) continue;
                AddPDFValue(value, each, tag.Tagged, key2);
                value = key2 = key1 = null;
            }
        }

        public static IDictionary<string, string> DoPartialTagExtension<T>(this T t, EntityTag tag)
           where T : class
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();

            if (t == null) return dic;
            string key1 = null;
            string key2 = null;
            object value = null;
            foreach (var each in typeof(T).GetProperties())
            {


                key1 = string.IsNullOrEmpty(tag.Appender) ? each.Name : string.Format("{0}.{1}", tag.Appender, each.Name);
                tag.Map.Mapping.TryGetValue(key1, out key2);

                if (string.IsNullOrEmpty(key2))
                {
                    continue;
                }
                value = each.GetValue(t, null);
                if (value == null) continue;
                AddPDFValue(value, each, dic, key2);
                value = key2 = key1 = null;
            }


            return dic;
        }
        private static void AddPDFValue(object value, PropertyInfo property, IDictionary<string, string> tag, string key2)
        {
            if (tag.ContainsKey(key2)) return;

            if (property.PropertyType == typeof(bool))
            {
                if ((bool)value == true)
                    tag.Add(key2, "Yes");
                else
                    tag.Add(key2, "0");
            }
            else if (property.PropertyType == typeof(DateTime?) || property.PropertyType == typeof(DateTime))
            {
                if (tag.ContainsKey(key2 + ".day")) return;
                tag.Add(key2 + ".day", ((DateTime)value).Day.ToString());
                tag.Add(key2 + ".month", ((DateTime)value).Month.ToString());
                tag.Add(key2 + ".year", ((DateTime)value).Year.ToString());


            }
            else if (property.PropertyType == typeof(PhoneNumberEntity))
            {
                PhoneNumberEntity entity = ((PhoneNumberEntity)value);
                if (entity.IsEmpty()) return;
                if (tag.ContainsKey(key2 + ".number")) return;
                tag.Add(key2 + ".prefix", "+" + entity.CountryCode);

                string prefix = entity.CityCode;
                int number;
                if (Int32.TryParse(prefix, out number))
                    prefix = number.ToString();

                prefix += " ";
                prefix += entity.PhoneNumber;
                tag.Add(key2 + ".number", prefix);



            }
            else if (property.PropertyType == typeof(MobileNumberEntity))
            {
                MobileNumberEntity entity = ((MobileNumberEntity)value);
                if (entity.IsEmpty()) return;
                if (tag.ContainsKey(key2 + ".number")) return;
                tag.Add(key2 + ".prefix", "+" + entity.CountryCode);

                string prefix = entity.MobileNumber;
                int number;
                if (Int32.TryParse(prefix, out number))
                    prefix = number.ToString();

                tag.Add(key2 + ".number", prefix);
            }
            else if (property.PropertyType == typeof(TitleEntity))
            {
                TitleEntity entity = ((TitleEntity)value);
                if (!tag.ContainsKey(key2 + ".mr"))
                    tag.Add(key2 + ".mr", entity.Title.ToLower() == "mr" ? "Yes" : "0");
                if (!tag.ContainsKey(key2 + ".mrs"))
                    tag.Add(key2 + ".mrs", entity.Title.ToLower() == "mrs" ? "Yes" : "0");
                if (!tag.ContainsKey(key2 + ".miss"))
                    tag.Add(key2 + ".miss", entity.Title.ToLower() == "miss" ? "Yes" : "0");
                if (!tag.ContainsKey(key2 + ".ms"))
                    tag.Add(key2 + ".ms", entity.Title.ToLower() == "ms" ? "Yes" : "0");
                if (!tag.ContainsKey(key2 + ".dr"))
                    tag.Add(key2 + ".dr", entity.Title.ToLower() == "dr" ? "Yes" : "0");
                if (!tag.ContainsKey(key2 + ".rev"))
                    tag.Add(key2 + ".rev", entity.Title.ToLower() == "rev" ? "Yes" : "0");

                if (entity.Title.ToLower() != "mr" &&
                    entity.Title.ToLower() != "mrs" &&
                    entity.Title.ToLower() != "miss" &&
                    entity.Title.ToLower() != "ms" &&
                    entity.Title.ToLower() != "dr" &&
                    entity.Title.ToLower() != "rev")
                {
                    if (!tag.ContainsKey(key2 + ".other"))
                        tag.Add(key2 + ".other", entity.Title.ToLower());
                }
            }
            else
            {
                if (!tag.ContainsKey(key2))
                    tag.Add(key2, value.ToString());
            }


        }
    }
}
