﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Extensions
{
  public static class MobilePhoneNumberExtensions
    {

      public static string PhoneNumberToString(this PhoneNumberEntity number)
      {
          string result = string.Empty;
          if (number != null)
              result = number.ToString();

          return result;
      }

      public static string MobileNumberToString(this MobileNumberEntity number)
      {
          string result = string.Empty;
          if (number != null)
              result = number.ToString();
          return result;
      }

    }
}
