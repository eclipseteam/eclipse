﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Oritax.Serilization;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Extensions
{
    public static class XmlSerializerExtensions
    {
        public static T ToNewOrData<T>(this string value) where T : class, new()
        {
            return string.IsNullOrWhiteSpace(value) ? new T() : ToData<T>(value);
        }

        public static T[] ToDataArray<T>(this string value)
        {
            return _ToData<T[]>(value);
        }

        public static T ToData<T>(this string xml) where T : class, new()
        {
            return _ToData<T>(xml);
        }
       

        private static T _ToData<T>(string xml) where T : class
        {
            return SerializerFactory.Get(typeof(T)).Read<T>(xml);
        }

        #region ToXmlString

        public static string ToXmlString(this IServiceable service)
            {
            return _ToXmlString(service);
            }

        public static string ToXmlString(this BMCServiceDataArray data)
            {
            return ToXmlString<BMCServiceDataArray>(data);
            }

        public static string ToXmlString<T>(this T data) where T : class
            {
            return _ToXmlString(data);
            }

        private static string _ToXmlString(object data)
            {
            return SerializerFactory.Get(data.GetType()).Write(data);
            } 
        #endregion
    }

}

namespace Oritax.Serilization
{
    public interface IStringSerializer
    {
        string Write(object value);
        T Read<T>(string xml) where T : class;
    }

    public class XmlSerializerProxy : IStringSerializer
    {
        public string Write(object value)
        {
            if (value == null) return string.Empty;

            var serializer = new XmlSerializer(value.GetType());
            var builder = new StringBuilder();
            using (var writer = new StringWriter(builder))
            {
                serializer.Serialize(writer, value);
                writer.Close();
            }
            var xml = builder.ToString();
            return xml;
        }


        public T Read<T>(string xml) where T : class
            {
            if (string.IsNullOrEmpty(xml)) return null;
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            T t = null;
            using (StringReader reader = new StringReader(xml))
                {
                try
                    {
                    t = serializer.Deserialize(reader) as T;
                    }
                catch { }
                reader.Close();
                }
            return t;
            }
    }

    public class DataContractSerializerProxy : IStringSerializer
    {

        public string Write(object value)
        {
            if (value == null) return string.Empty;
          
            var serializer = new DataContractSerializer(value.GetType());
            var builder = new StringBuilder();
            using (var writer = XmlWriter.Create(builder))
            {
                serializer.WriteObject(writer, value);
                writer.Close();
            }
            var xml = builder.ToString();
            return xml;
        }

        public T Read<T>(string xml) where T : class
            {


            DataContractSerializer serializer = new DataContractSerializer(typeof(T));
            
            T t = null;
            using (XmlReader reader = XmlReader.Create(new StringReader(xml)))
                {
                try
                    {
                    t = serializer.ReadObject(reader) as T;
                    }
                catch { }
                reader.Close();
                }
            return t;
            
            
            
            }


    }

    public static class SerializerFactory
    {
        private static readonly IStringSerializer Xml = new XmlSerializerProxy();
        private static readonly IStringSerializer Dc = new DataContractSerializerProxy();

        public static IStringSerializer Get(Type type)
        {
            return type.IsDefined(typeof(DataContractAttribute), true) ? Dc : Xml;
        }
    }

    

}
