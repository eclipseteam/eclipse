﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using Oritax.TaxSimp.Data;
using System;

namespace Oritax.TaxSimp.Extensions
{
    public static class ListExtensions
    {
        public static T Find<T>(this IList<T> list, T item) where T : IHasPrimaryKey
        {
            return (from i in list where i.PrimaryKey.Equals(item.PrimaryKey) select i).FirstOrDefault();
        }

        public static int FindIndex<T>(this IList<T> list, T item) where T : IHasPrimaryKey
        {
            T selected = (from i in list
                          where i.PrimaryKey.Equals(item.PrimaryKey)
                          select i).FirstOrDefault();

            return list.IndexOf(selected);
        }

        public static DataTable ToDataTable<T>(this IEnumerable<T> data)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            object[] values = new object[props.Count];
            try
            {
                foreach (T item in data)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = props[i].GetValue(item);
                    }
                    table.Rows.Add(values);
                }
            }
            catch
            { }
            return table;
        }
       
    }
    public static class DictionaryExtensionMethods
    {
        public static void Merge<TKey, TValue>(this Dictionary<TKey, TValue> me, Dictionary<TKey, TValue> merge)
        {
            foreach (var item in merge)
            {
                me[item.Key] = item.Value;
            }
        }
    }
}
