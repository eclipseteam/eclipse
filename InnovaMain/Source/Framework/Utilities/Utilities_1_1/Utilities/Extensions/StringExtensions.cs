﻿
using System.Text;
using System.Text.RegularExpressions;

namespace Oritax.TaxSimp.Extensions
{
    public static class StringExtensions
    {
        public static int ToInt(this string value)
        {
            return System.Convert.ToInt32(value);
        }

        public static string ToDoubeDigit(this string value)
        {
            if (value.Length == 1)
                return "0" + value;
            else
                return value;
        }

        public static string  LimitToLength(this string text,int length)
        {
            if(text==null) return null;
            string result = text;

            result = text.Length <= length ? text : text.Substring(0, length);

            return result;

        }

        public static void SplitInTwo(this string text, int length, ref string text1, ref string text2)
        {

            if (text.Length <= length)
            {
                text1 = text;
                text2 = string.Empty;

            }
            else
            {



                int pos = text.LastIndexOf(" ", length);
                if (pos >= 0)
                {
                    text1 = text.Substring(0, pos);
                    text2 = text.Substring(pos);

                }
            }

        }

        public static void SplitInTwo(this string text, int length, string filter,ref string text1, ref string text2)
        {

            if (text.Length <= length)
            {
                text1 = text;
                text2 = string.Empty;

            }
            else
            {

                if (text.ToLower().Contains(filter))
                {
                    int pos = text.ToLower().IndexOf(filter);
                    text1 = text.Substring(0, pos);
                    text2 = text.Substring(pos);

                }
                else
                {

                    int pos = text.LastIndexOf(" ", length);
                    if (pos >= 0)
                    {
                        text1 = text.Substring(0, pos);
                        text2 = text.Substring(pos);

                    }
                }
            }

        }

       

        public static string ReplaceWildCard(this string content, string pattern, string replace, bool isCaseSensitive = false)
        {



            if (pattern.Contains("*") || pattern.Contains("?"))
            {
                RegexOptions options = RegexOptions.Compiled;
                if (!isCaseSensitive) options |= RegexOptions.IgnoreCase;
                var pat = pattern.Replace(@".", @"\.").Replace(@"*", @".*").Replace("?", "(.{1,1})");
                Regex regex = new Regex(pat, options);

                //return regex.IsMatch(stringToSearch);

                if (regex.IsMatch(content))
                {
                    content = regex.Replace(content, replace);
                }

            }
            else
            {
                content = content.Replace(pattern, replace);
            }

            return content;
        }


        public static string ReplaceEmptyWithNull(this string text)
        {
            string result = null;
            if(!string.IsNullOrEmpty(text))
            {
                if(text.Trim()!=string.Empty)
                result = text;
            }

            return result;
        }

        public static string RemoveWhiteSpacesStartEndInBetween(this string text)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(text))
            {
                text.Trim();
                result = text.Replace(" ", string.Empty);
            }

            return result;
        }

        public static  string RemoveLast(this string source,string removeText)
        {
            string result = source;
            if (source.Contains(removeText))
            {
              result=  source.Remove(source.LastIndexOf(removeText),removeText.Length);
            }

            return result;
        }

        public static bool IsEmptyOrNull(this string text)
        {
           return string.IsNullOrEmpty(text);
        }
    }
}
