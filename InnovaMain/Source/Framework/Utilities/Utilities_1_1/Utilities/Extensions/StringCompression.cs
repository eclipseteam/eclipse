﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using C1.C1Zip;

namespace Oritax.TaxSimp.Extensions
    {
  public static  class StringCompression
      {
      public static string Compress(this string text)
          {
          using (MemoryStream ms = new MemoryStream())
              {
              using (C1ZStreamWriter zip = new C1ZStreamWriter(ms))
                  {
                  using (StreamWriter writer = new StreamWriter(zip))
                      {
                      //write the data into the compressed strewam
                      writer.Write(text);
                      writer.Flush();

                      //converted to the array
                      byte[] compressed = ms.ToArray();

                      //build based 64 layout
                      byte[] gzBuffer = new byte[compressed.Length + 4];

                      System.Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
                      System.Buffer.BlockCopy(BitConverter.GetBytes(compressed.Length), 0, gzBuffer, 0, 4);

                      //convert to base 64.
                      return Convert.ToBase64String(gzBuffer);
                      }
                  }
              }
          }
      public static string Decompress(this string compressedText)
      {

          StringBuilder readerBuilder = new StringBuilder();

          //convert binary array from base64 string
          byte[] gzBuffer = Convert.FromBase64String(compressedText);

          //get the stream length
          int streamLength = BitConverter.ToInt32(gzBuffer, 0);

          //extract compressed binary array 
          byte[] buffer = new byte[streamLength];
          System.Buffer.BlockCopy(gzBuffer, 4, buffer, 0, streamLength);

          using (MemoryStream stream = new MemoryStream(buffer))
          {
              using (C1ZStreamReader unzip = new C1ZStreamReader(stream))
              {

                  using (StreamReader reader = new StreamReader(unzip))
                  {
                      char[] readerBuffer = new char[1024 * 1024];
                      int position = 0;
                      int charsRead = 0;

                      while ((charsRead = reader.Read(readerBuffer, 0, readerBuffer.Length)) > 0)
                      {
                          string contentToProcess = new string(readerBuffer, 0, charsRead);
                          readerBuilder.Append(contentToProcess);
                          position += charsRead;
                      }
                  }
              }
          }
          return readerBuilder.ToString();
      }

      public static string Compress( this Stream stream)
          {
          using (MemoryStream ms = new MemoryStream())
              {
              using (C1ZStreamWriter zip = new C1ZStreamWriter(ms))
                  {
                  //compressed the byte array
                  byte[] buffer = new byte[stream.Length];

                  stream.Read(buffer, 0, (int)stream.Length);

                  zip.Write(buffer, 0, buffer.Length);

                  zip.Flush();

                  //converted to the array
                  byte[] compressed = ms.ToArray();

                  //build based 64 layout
                  byte[] gzBuffer = new byte[compressed.Length + 4];

                  System.Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
                  System.Buffer.BlockCopy(BitConverter.GetBytes(compressed.Length), 0, gzBuffer, 0, 4);

                  //convert to base 64.
                  return Convert.ToBase64String(gzBuffer);
                  }
              }
          }

      public static string Decompress(this string compressedText, byte[] result)
          {
          //convert binary array from base64 string
          byte[] gzBuffer = Convert.FromBase64String(compressedText);

          //get the stream length
          int streamLength = BitConverter.ToInt32(gzBuffer, 0);

          //extract compressed binary array 
          byte[] buffer = new byte[streamLength];
          System.Buffer.BlockCopy(gzBuffer, 4, buffer, 0, streamLength);

          using (MemoryStream stream = new MemoryStream(buffer))
              {
              using (C1ZStreamReader unzip = new C1ZStreamReader(stream))
                  {
                  unzip.Read(result, 0, unzip.SizeUncompressed);

                  return string.Empty;
                  }
              }
          }
        }
    }
