﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Extensions
{
    public static class GuidExtensions
    {
        public static Guid ToGuid(this string value)
        {
            if(!string.IsNullOrEmpty(value)) return new Guid(value);
            throw new Exception("String is empty");
        }
    }
}
