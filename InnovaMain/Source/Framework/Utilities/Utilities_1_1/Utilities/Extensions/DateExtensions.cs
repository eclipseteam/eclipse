﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace Oritax.TaxSimp.Extensions
{
    public static class DateExtensions
    {
        /// <summary>
        /// Tolerance Calendar Days
        /// </summary>
        /// <param name="sourceValue"></param>
        /// <param name="targetValue"></param>
        /// <param name="tolerence"></param>
        /// <returns></returns>
        public static bool IsMatched(this DateTime sourceValue, DateTime targetValue, int tolerence = 0)
        {
            return (targetValue - sourceValue).Days <= tolerence;
        }

        /// <summary>
        /// Tolerance Business Days
        /// </summary>
        /// <param name="sourceValue"></param>
        /// <param name="targetValue"></param>
        /// <param name="tolerence"></param>
        /// <param name="excludeWeekends"></param>
        /// <returns></returns>
        public static bool IsMatched(this DateTime sourceValue, DateTime targetValue, int tolerence = 0, Boolean excludeWeekends = false)
        {
            return (DaysLeft(sourceValue, targetValue, excludeWeekends) <= tolerence);
        }

        public static int DaysLeft(DateTime startDate, DateTime endDate, Boolean excludeWeekends)
        {
            int count = 0;
            var excludeDates = new List<DateTime>();
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["Holidays"]))
            {
                var holidays = ConfigurationManager.AppSettings["Holidays"].Split(',');
                foreach (string date in holidays)
                {
                    excludeDates.Add(Convert.ToDateTime(date));
                }
            }

            for (DateTime index = startDate; index < endDate; index = index.AddDays(1))
            {
                if (excludeWeekends && index.DayOfWeek != DayOfWeek.Sunday && index.DayOfWeek != DayOfWeek.Saturday)
                {
                    bool excluded = false; ;
                    for (int i = 0; i < excludeDates.Count; i++)
                    {
                        DateTime excludeDate = new DateTime(index.Date.Year, excludeDates[i].Date.Month, excludeDates[i].Date.Day);
                        if (index.Date.CompareTo(excludeDate) == 0)
                        {
                            excluded = true;
                            break;
                        }
                    }

                    if (!excluded)
                    {
                        count++;
                    }
                }
            }

            return count;
        }
    }
}