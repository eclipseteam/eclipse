﻿using System;
using System.Linq;
using System.Xml.Linq;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Extensions
{
    public static class IdentityCMExtensiosn
    {
        public static string ToIdentityXml(this IIdentityCM value)
        {
            XElement xml = new XElement(
                typeof(IdentityCM).Name,
                new XElement("Clid", value.Clid),
                new XElement("Csid", value.Csid)

            );
            return xml.ToString();
        }
        public static string ToIdentityXml(this IdentityCM value)
        {
            XElement xml = new XElement(
                typeof(IdentityCM).Name,
                new XElement("Clid", value.Clid),
                new XElement("Csid", value.Csid),
                new XElement("Cid", value.Cid)

            );
            return xml.ToString();
        }

 
        public static IdentityCM ToIdentity(this string xml)
        {
            XDocument document = XDocument.Parse(xml);
            Guid clid = new Guid(document.Descendants("Clid").SingleOrDefault().Value);
            Guid csid = new Guid(document.Descendants("Csid").SingleOrDefault().Value);
            Guid cid = Guid.Empty;

            var cidnode = document.Descendants("Cid").SingleOrDefault();
            if (cidnode != null)
            {
                cid = new Guid(document.Descendants("Cid").SingleOrDefault().Value);

            }

            return new IdentityCM { Clid = clid, Csid = csid, Cid = cid };
        }
    }
}
