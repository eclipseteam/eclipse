﻿using System;
using System.Collections;
using System.Reflection;
using System.Xml.Linq;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Extensions
{
    public static class XElementExtensions
    {

        #region ToXElement

        public static XElement ToXElement(this object t, string name)
        {
            return ToXElement(t, name, true);
        }

        public static XElement ToXElement(this object t, string name, bool attribute)
        {
            XElement root = new XElement(name);
            if (t == null) return root;

            if (t is IXElementable)
            {
                (t as IXElementable).ToXElement(root);
            }
            else if (t is IEnumerable && t.GetType() != typeof(string))
            {
                FillEnumerable(root, t as IEnumerable, attribute);
            }
            else
            {
                FillSingle(root, t, attribute);
            }
            return root;
        }

        private static void FillEnumerable(XElement root, IEnumerable items, bool attribute)
        {
            if (items == null || items.GetType() == typeof(string)) return;

            foreach (var item in items)
            {
                root.Add(item.ToXElement(item.GetType().Name, attribute));
            }
        }

        private static void FillSingle(XElement root, object item, bool attribute)
        {
            foreach (var each in item.GetType().GetProperties())
            {
                object value = each.GetValue(item, null);

                if (value is IEnumerable && each.PropertyType != typeof(string))
                {
                    XElement list = new XElement(each.Name);
                    FillEnumerable(list, value as IEnumerable, attribute);
                    root.Add(list);
                }
                else if (each.PropertyType.IsClass && each.PropertyType != typeof(string))
                {

                    root.Add(value == null ? new XElement(each.Name) : value.ToXElement(each.Name, attribute));
                }
                else
                {
                    root.Add((attribute ? new XAttribute(each.Name, value == null ? string.Empty : value) : (object)new XElement(each.Name, value)));
                }
            }
        }

        #endregion

        #region FromXElement

        public static T FromXElement<T>(this XElement element, string name)
            where T : class, new()
        {
            T t = new T();
            FromXElement(t, element.Element(name));
            return t;
        }

        public static void FromXElement(this object item, XElement element, string name)
        {
            FromXElement(item, element.Element(name)); 
        }

        public static void FromXElement(this object item, XElement element)
        {
            foreach (XAttribute each in element.Attributes())
            {
                SetValue(item, each);
            }
            foreach (XElement each in element.Elements())
            {
                object value = GetObject(item, each);
                if (value != null && value is IXElementable)
                {
                    (value as IXElementable).FromXElement(each);
                }
                else
                {
                    FromXElement(value, each);
                }
            }
        }

        private static void SetValue(object item, XAttribute attribute)
        {
            PropertyInfo property = item.GetType().GetProperty(attribute.Name.LocalName);
            if (property == null) return;
            Type t = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;
            object value = SafeConvert(attribute.Value, t);
            property.SetValue(item, value, null);
        }

        private static object GetObject(object item, XElement element)
        {
            if (item == null) return null;
            object value = null;
            if (item is IList && item.GetType().IsGenericType && item.GetType().GetGenericArguments()[0].Name == element.Name.LocalName)
            {
                value = item.GetType().GetGenericArguments()[0].GetConstructor(new Type[] { }).Invoke(new object[] { });
                (item as IList).Add(value);
            }
            else
            {
                PropertyInfo property = item.GetType().GetProperty(element.Name.LocalName);
                if (property == null) return null;
                value = property.PropertyType.GetConstructor(new Type[] { }).Invoke(new object[] { });
                property.SetValue(item, value, null);
            }
            return value;
        }

        private static object SafeConvert(string value, Type type)
        {
            object converted = null;

            if (type == typeof(String))
            {
                converted = value;
            }
            else if (string.IsNullOrEmpty(value.Trim()))
            {
                converted = null;
            }
            else if (type == typeof(Guid))
            {
                converted = new Guid(value);
            }
            else
            {
                converted = System.Convert.ChangeType(value, type);
            }

            return converted;
        }
        #endregion
    }

}
