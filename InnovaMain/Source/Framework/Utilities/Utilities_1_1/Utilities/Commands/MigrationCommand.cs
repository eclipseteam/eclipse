﻿
namespace Oritax.TaxSimp.Commands
{
    public enum MigrationCommand
    {
        Export = 6001,
        Import = 6002,
        ImportBankWestAccounts = 6003,
        ImportBankAccountTransactions = 6004,
        ImportBellDirectDataFeed = 6005,
        ValidateBellDirectDataFeed = 6006,
        ValidateAsxPriceList = 6007,
        ImportASXPriceList = 6008,
        ValidateMISFundTransactions = 6009,
        ImportMISFundTransactions = 6010,
        ValidateStatestreetPriceList = 6011,
        ImportStatestreetPriceList = 6012,
        ValidateBankWestAccounts = 6013,
        ValidateBankAccountTransactions = 6014,
        ValidateProductSecuirtiesPriceList = 6015,
        ImportProductSecuirtiesPriceList = 6016,
        ValidateUnitHolderBalanceTransactions = 6017,
        ImportUnitHolderTransactions=6018,
        ResetUnitHolderBalanceFunds = 6019,
        ValidateBankAccountPaidOrders=6020,
        ImportBankAccountPaidOrders = 6021,
        ValidateMISPaidOrders = 6022,
        ImportMISPaidOrders = 6023,
        ValidateASXPaidOrders = 6024,
        ImportASXPaidOrders = 6025,
        ValidateTDPriceList = 6026,
        ImportTDPriceList = 6027,
        ExportFinSimplicityHoldings=6028,
        ExportFinSimplicityInvestments=6029
    }
}
