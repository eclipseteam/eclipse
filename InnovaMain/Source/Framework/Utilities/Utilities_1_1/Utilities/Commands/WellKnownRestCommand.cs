﻿
namespace Oritax.TaxSimp.Commands
{
    public enum WellKnownRestCommand
    {
        AllClient = 5001,
        AllClientByType = 5002,
        ClientByName = 5003,
        AllClientsMainData = 5004,
        ClientFinancialDetails = 5005, 
        EPIDataReposneAllClient = 5006,
        EPIDataReposneAllClientByType = 5007,
        EPIDataReposneClientByName = 5008,
        AllClientsByAdviserID = 5009,
        AllClientsByIFAID = 5010,
        AllModels=5011,
        ModelDetailsByID = 5012,
        EPIASXDataReposneAllClient=5013,
        EPIDataReposneAllSecurities=5014,
        SuperManagersEclipseSuperBasic=5015
    }
}
