using System;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
	public class InstallIConsolidationUtilities
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="3C86C8B3-3914-409f-BB36-64F33EF49153";
		public const string ASSEMBLY_NAME="IConsolidationUtilities_1_1";
		public const string ASSEMBLY_DISPLAYNAME="IConsolidationUtilities V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
//		public const string ASSEMBLY_REVISION="0"; 
		public const string ASSEMBLY_REVISION="0"; //2005.1

		#endregion
	}
}
