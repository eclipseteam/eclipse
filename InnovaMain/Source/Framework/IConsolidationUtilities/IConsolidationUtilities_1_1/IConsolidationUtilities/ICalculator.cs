using System;
using System.Data;
using System.Collections;
using Oritax.TaxSimp.CalculationInterface;


namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// 
	/// </summary>
	public interface ICalculator
	{
		ICalculationModule OwnerCalculationModule
		{
			get;
			set;
		}
		bool ProcessMessage( IMessage objMessage );
		void Publish(IMessage objMessage);
		void PublishCrossPeriod( );
		void ProcessMessagePull( IMessage objMessage );
		IEnumerable Subscriptions
		{
			get;
		}
		IEnumerable WorkPapers
		{
			get;
		}

		DataSet PrimaryDataSet{get;}

		void ExtractData(DataSet data);
		void DeliverData(DataSet data);

		Guid ID
		{
			get;
			set;
		}

		/// <summary>
		/// A calculator overrides this method to be notified when data update changes
		/// should be accepted, for example after having been loaded from the database
		/// </summary>
		void MarkCalculatorAsUnmodified();
	}
}
