using System.Reflection;
using System.Runtime.CompilerServices;

using Oritax.TaxSimp.InstallInfo;
using Oritax.TaxSimp.CalculationInterface;

//
 


//
[assembly: AssemblyTitle("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]		

[assembly: AssemblyVersion(InstallIConsolidationUtilities.ASSEMBLY_MAJORVERSION+"."+InstallIConsolidationUtilities.ASSEMBLY_MINORVERSION)]
[assembly: AssemblyInformationalVersion(InstallIConsolidationUtilities.ASSEMBLY_MAJORVERSION+"."+InstallIConsolidationUtilities.ASSEMBLY_MINORVERSION+"."+InstallIConsolidationUtilities.ASSEMBLY_DATAFORMAT+"."+InstallIConsolidationUtilities.ASSEMBLY_REVISION)]
[assembly: AssemblyInstallInfo(InstallIConsolidationUtilities.ASSEMBLY_ID,InstallIConsolidationUtilities.ASSEMBLY_NAME,InstallIConsolidationUtilities.ASSEMBLY_DISPLAYNAME)]

//


//
// Use the attributes below to control which key is used for signing. 
//
// Notes: 
//   (*) If no key is specified, the assembly is not signed.
//   (*) KeyName refers to a key that has been installed in the Crypto Service
//       Provider (CSP) on your machine. KeyFile refers to a file which contains
//       a key.
//   (*) If the KeyFile and the KeyName values are both specified, the 
//       following processing occurs:
//       (1) If the KeyName can be found in the CSP, that key is used.
//       (2) If the KeyName does not exist and the KeyFile does exist, the key 
//           in the KeyFile is installed into the CSP and used.
//   (*) In order to create a KeyFile, you can use the sn.exe (Strong Name) utility.
//       When specifying the KeyFile, the location of the KeyFile should be
//       relative to the project output directory which is
//       %Project Directory%\obj\<configuration>. For example, if your KeyFile is
//       located in the project directory, you would specify the AssemblyKeyFile 
//       attribute as [assembly: AssemblyKeyFile("..\\..\\mykey.snk")]
//   (*) Delay Signing is an advanced option - see the Microsoft .NET Framework
//       documentation for more information on this.
//
[assembly: AssemblyDelaySign(false)]
