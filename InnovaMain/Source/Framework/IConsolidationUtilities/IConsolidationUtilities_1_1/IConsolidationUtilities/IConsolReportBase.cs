using System;
using System.Collections.Generic;
using System.Text;

namespace Oritax.TaxSimp.CalculationInterface
{
    public interface IConsolReportBase
    {
        void AddURLInformationToReportingTables();
    }
}
