using System;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// Base interface for a Message routing class, used within
	/// Calculation modules to determine where a message should be sent
	/// for processing.
	/// </summary>
	public interface IMessageRouter
	{
		/// <summary>
		/// Process the message being sent to the Calculation Module
		/// </summary>
		/// <param name="objMessage">The message to be processed</param>
		bool ProcessMessage( IMessage objMessage );
		void PublishCrossPeriod( );
		string GetPublisherCIID( IMessage objMessage );

		void ProcessTargettedMessagePull(Guid targetCLID, Guid targetCSID);
		void OnMigrationCompleted();
	}
}
