﻿using System.Collections.Generic;

namespace Oritax.TaxSimp.FormGenerators
{
    public class FormGeneratorFactory : IFormGeneratorFactory
    {
        private static Dictionary<GeneratorType, IFormGenerator> _Generators = new Dictionary<GeneratorType, IFormGenerator>()
        {
            { GeneratorType.Pdf, new PdfGenerator() }
        };

        IFormGenerator IFormGeneratorFactory.GetInstance(GeneratorType type)
        {
            return FormGeneratorFactory.GetInstance(type);
        }

        public static IFormGenerator GetInstance(GeneratorType type)
        {
            IFormGenerator instance = _Generators[type];
            return instance;
        }
    }
}
