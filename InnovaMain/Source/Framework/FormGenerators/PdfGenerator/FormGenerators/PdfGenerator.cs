﻿using System.IO;
using System.Collections.Generic;
using iTextSharp.text.pdf;

namespace Oritax.TaxSimp.FormGenerators
{
    public class PdfGenerator : IFormGenerator
    {
        public byte[] Generate(byte[] content, IDictionary<string, string> map)
        {
            byte[] generated = null;
            PdfReader reader = new PdfReader(content);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfStamper stamper = new PdfStamper(reader, stream);
                AcroFields fields = stamper.AcroFields;

                foreach (var each in map)
                {
                    fields.SetField(each.Key, each.Value);
                }

                stamper.FormFlattening = false;
                stamper.Close();
                generated = stream.GetBuffer();
            }
            return generated;
        }
    }
}
