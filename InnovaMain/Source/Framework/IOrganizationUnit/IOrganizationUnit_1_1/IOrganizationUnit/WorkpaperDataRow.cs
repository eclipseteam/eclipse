﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Oritax.TaxSimp.IOrganizationUnits
{
    [DataContract]
    public class WorkpaperDataRow
    {
        [DataMember]
        public List<WorkpaperDataItem> Items { get; set; }
    }
}
