﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CM.Organization;

namespace Oritax.TaxSimp.IOrganizationUnits
{
    public interface IModelStructure : IOrganization
    {
        new List<ModelEntity> Model { get; set; }
        new List<AssetEntity> Assets { get; set; }
        new List<ProductEntity> Products { get; set; }

    }
}
