using System;
using System.Collections;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.SAPBase;
using Oritax.TaxSimp.CM.Entity;
using System.Collections.Generic;

namespace Oritax.TaxSimp.CM.Group
{
	public interface IGroup : IOrganizationUnit
	{
		GroupMembers IncludedGroupMembers{get;}
		Guid ConsolidationAdjustmentEntityCLID{get;set;}
		Guid ConsolidationAdjustmentEntityCSID{get;set;}

        /// <summary>
        /// Enumerate a collection of BMC's of a specified type from all members of the group in the specified period
        /// </summary>
        /// <param name="periodStart">The start date of the period to return</param>
        /// <param name="periodEnd">The end date of the period to return</param>
        /// <param name="strTypeName">The type name to get a list of</param>
        /// <returns>An IEnumerable list of ICalculationModules</returns>
        IEnumerable<ICalculationModule> GetBMCTypeFromMembers(DateTime periodStart, DateTime periodEnd, string strTypeName);

        /// <summary>
        /// Enumerate a collection of BMC's of a specified type from all members of the group in the specified period
        /// </summary>
        /// <param name="periodStart">The start date of the period to return</param>
        /// <param name="periodEnd">The end date of the period to return</param>
        /// <param name="strTypeName">The type name to get a list of</param>
        /// <param name="exceptionIfCMNotExist">Throws an exception if the CM does not exist anywhere in the structure</param>
        /// <returns>An IEnumerable list of ICalculationModules</returns>
        IEnumerable<ICalculationModule> GetBMCTypeFromMembers(DateTime periodStart, DateTime periodEnd, string strTypeName, bool exceptionIfCMNotExist);

		void PublishNewPeriodStreamControlToMembers(DateTime dtPeriodStart, DateTime dtPeriodEnd );

		/// <summary>
		/// Checks any members of the group for the period. Adds member names to arraylist which already contain period.
		/// </summary>
		/// <param name="dtPeriodStart"></param>
		/// <param name="dtPeriodEnd"></param>
		/// <param name="alMemberNames"></param>
		/// <returns></returns>
		bool VerifyPeriodDoesNotExistInMembers(DateTime dtPeriodStart, DateTime dtPeriodEnd, ref ArrayList alMemberNames);
		bool VerifyPeriodDoesNotOverlapInMembers(DateTime dtStartDate, DateTime dtEndDate, Guid gdOwnerCLID, ref ArrayList alMemberNames);
		bool VerifyPeriodIsSAPCompliantInMembers( DateTime dtStartDate, DateTime dtEndDate, ref ArrayList alMemberNames );

		void DeleteScenario( Guid objScenarioID, bool bolDeleteFromSubGroups, ArrayList objLastScenarioConflicts, ArrayList objMemberOfGroupScenario , ArrayList objLockedScenarios);
		
		void SetCurrentPeriodInMembers( String startDate, String endDate);

		/// <summary>
		/// Gets the consolidation adjustment entity for this group
		/// </summary>
		/// <returns>The consolidation adjustment entity for the group</returns>
		IEntity GetConsolidationAdjustmentEntity();

		/// <summary>
		/// Determines if the entity passed in is the consolidation adjustment
		/// entity for this group
		/// </summary>
		/// <param name="entity">The entity to check</param>
		/// <returns>True if it is the entity, false otherwise</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if entity is null</exception>
		bool IsConsolidationAdjustmentEntity(IEntity entity);

		/// <summary>
		/// Gets a member organisation unit from the logical module
		/// </summary>
		/// <param name="organizationUnitLM">The logical organisation unit that is a member of the group</param>
		/// <returns>The organisation unit that is the member of the group</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if organizationUnitLM is null</exception>
		/// <exception cref="System.ApplicationException">Thrown if the organizationUnitLM is not a member of the group</exception>
		IOrganizationUnit GetMemberOrganisationUnit(ILogicalModule organizationUnitLM);

        /// <summary>
        /// Checks if the size of structure qualifies as a time intensive structure
        /// </summary>
        /// <param name="userConfirmationKey">The key thrown in the user confirmation message</param>
        /// <param name="direction">The direction to check e.g. Up or Down</param>
        /// <exception cref="Oritax.TaxSimp.Exceptions.GetUserConfirmationException">Thrown if the task is time intensive</exception>
        void CheckIfStructureWillBeTimeIntensive(string userConfirmationKey, BusinessStructureNavigationDirection direction);
	}
}
