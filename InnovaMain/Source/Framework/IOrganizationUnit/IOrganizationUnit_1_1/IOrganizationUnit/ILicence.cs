using System;

using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.TaxSimpLicence
{
	public enum LicenceValidationType
	{
		Entity = 0,
		StandardFiles,
		Groups,
		Period,
		Scenarios,
		LedgerImport,
		User
	}

	public interface ILicence : IBrokerManagedComponent
	{
		bool IsLicenceValid();
        bool IsTeaLicenced();
		int DaysRemainingOnLicence();
		bool IsLicenced(DateTime periodEndDate, string reportingUnitName);
		bool IsLicenced(Guid componentGUID, bool throwOnFailure);
		bool IsLicenced(Guid componentGUID);
		bool IsLicenced(string typeName);
		bool IsLicenced(Guid componentGUID, LicenceValidationType licenceValidationType);
		bool IsLicenced(string fileText, LicenceValidationType licenceValidationType);
	}
}
