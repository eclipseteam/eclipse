using System;

namespace Oritax.TaxSimp.IOrganizationUnits
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
	public class Install
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="17A3DE7B-5A7B-4612-9A76-6D07E78473F3";
		public const string ASSEMBLY_NAME="IOrganizationUnit_1_1";
		public const string ASSEMBLY_DISPLAYNAME="IOrganizationUnit V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
	//	public const string ASSEMBLY_REVISION="0"; 
		public const string ASSEMBLY_REVISION="1"; //2005.1 

		#endregion
	}
}
