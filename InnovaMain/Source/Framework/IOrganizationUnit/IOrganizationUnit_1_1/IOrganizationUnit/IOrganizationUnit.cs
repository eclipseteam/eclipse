using System;
using System.Collections;

using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.SAPBase;
using Oritax.TaxSimp.CM.Period;
using Oritax.TaxSimp.Common;
using System.Collections.Generic;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Common.UMAFee;

namespace Oritax.TaxSimp.CM.OrganizationUnit
{
	/// <summary>
	/// IOrganizationUnit is the interface to an organization unit CM.
	/// </summary>
    public interface IOrganizationUnit : ICalculationModule
	{
        string FullName();
        bool IsCorporateType();
		Guid CurrentPeriod{	get; set;}
        bool IsExported { get; set; }
		SerializableHashtable MemberPeriods{ get; }
		string Identifier{get;set;}
		string LegalName{get;set;}
        TaxStatusType TaxStatus { get;set;}
		void AddNewMemberPeriodDetails( Guid gdCLID, DateTime dtStartDate, DateTime dtEndDate,  string strTypeName);
		void EditMemberPeriodDetails( Guid gdCLID, DateTime dtStartDate, DateTime dtEndDate);
		void RemoveMemberPeriodDetails( Guid gdCLID);
		Guid GetCLIDOfMemberPeriod(DateTime dtStartDate, DateTime dtEndDate);
		bool IsUniqueOrganisationName( string strOrganisationName );
	    void SetShortEntityName(string name, bool isNew);
        Oritax.TaxSimp.Common.BankAccountEntity GetBankAccountDetails(string bsb, string accountNo, ServiceTypes serviceType);
		/// <summary>
		/// Deletes a scenario from the organisation unit
		/// </summary>
		/// <param name="objScenarioCSID">The CSID of the scenario to delete</param>
		/// <param name="objParentCSID">The CSID of the parent scenario</param>
		/// <param name="objLastScenarioConflicts">A list of scenarios that can't be deleted because they are the last scenario</param>
		/// <param name="objMemberOfGroupScenario">A list of scenarios that can't be deleted because they are a member of another group</param>
		/// <param name="objLockedScenarios">A list of scenarios that can't be deleted because they are locked</param>
		/// <returns>True if the scenario was deleted, false otherwise</returns>
		bool DeleteScenario( Guid objScenarioCSID, Guid objParentCSID, ArrayList objLastScenarioConflicts, ArrayList objMemberOfGroupScenario, ArrayList objLockedScenarios);

		TaxYear GetTaxYear( DateTime objPeriodStartDate, DateTime objPeriodEndDate );
		FinancialYear GetFinancialYear( DateTime objPeriodStartDate, DateTime objPeriodEndDate );
		void PreDeleteActions();
		IEnumerable PeriodScenarios{get;}
        IClientEntity ClientEntity { get; set; }
        ISubstitutedAccountingPeriods SAPs{get;}
        void CreateUniqueID();
        bool IsMemberOfGroup( );
		IPeriod[] GetChildPeriods();
        bool IsInvestableClient { get; set; }
        bool IsConsolCM { get;}
        string OnUpdateClient(int type, string data);
        string ClientId { get; set; }
        string OldClientId { get; }
        string SuperMemberID { get; set; }
        string SuperClientID { get; set; }
        string ApplicationID { get; set; }
        bool HasAdviser { get; }
        Guid AdviserID { get; }
        bool HasDIFM { get;}
        List<Guid> ConfiguredFeesList { get; set; }
        List<FeeTransaction> FeeTransactions { get; set; }
    }
}
