using System;
using System.Collections;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.SAPBase;

namespace Oritax.TaxSimp.CM.Entity
{
	public interface IEntity : IOrganizationUnit
	{
		string TaxStatusName{get;}
	}
}
