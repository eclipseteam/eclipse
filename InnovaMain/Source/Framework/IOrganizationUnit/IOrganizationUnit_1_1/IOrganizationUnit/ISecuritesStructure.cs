﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.IOrganizationUnits
{
    public interface ISecuritesStructure : IOrganization
    {
         new List<SecuritiesEntity> Securities { get; set; }
    }
}