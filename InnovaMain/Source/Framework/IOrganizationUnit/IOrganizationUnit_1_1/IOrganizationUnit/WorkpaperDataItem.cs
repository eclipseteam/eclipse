﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Oritax.TaxSimp.IOrganizationUnits
{
    [DataContract]
    public class WorkpaperDataItem
    {
        [DataMember]
        public string Value { get; set; }

        [DataMember]
        public WorkpaperDataItemType WorkpaperDataItemType { get; set; }
    }
}
