using System;
using System.Data;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CM.OrganizationUnit;

namespace Oritax.TaxSimp.CM.Period
{
	/// <summary>
	/// Represents a period in the system
	/// </summary>
	public interface IPeriod : ICalculationModule
	{
		DateTime StartDate{get;set;}
		DateTime EndDate{get;set;}
		void AddPrevPeriodChildren(Guid prevPeriodID);
		void PublishPeriodDeleteStreamControl();
		/// <summary>
		/// Calls functions in Period to start inplace migration for organisation unit
		/// </summary>
		void MigrateATOFormsPushDown(DataRow[] selectedCMs, bool userConfirmedTimeIntensiveTask);
        /// <summary>
        /// Calls functions in Period to start inplace migration for organisation unit
        /// </summary>
        void MigrateTEAPushDown(DataRow[] selectedCMs);
		/// <summary>
		/// Check in period whether existing CMs can be migrated with new CMs selected 
		/// </summary>
		/// <param name="selectedAvailableCMs"></param>
		/// <returns></returns>
		string CheckForExistingComponentException(DataRow[] selectedAvailableCMs);

		///<summary>
		/// Returns the Tax year that the period belongs to
		/// </summary>
		TaxYear GetTaxYear();
		
		/// <summary>
		/// Returns the financial year that the period belongs to
		/// </summary>
		FinancialYear GetFinancialYear();

		OrganisationUnitType OrganisationUnitType{get;set;}

		/// <summary>
		/// Returns the prior period if it exists, otherwise returns null
		/// </summary>
		/// <returns>The prior period object, null if it does not exist</returns>
		IPeriod GetPriorPeriod();
   			
		/// <summary>
		/// Returns true if a prior period exists for the parent period
		/// </summary>
		/// <returns>True if the period has a prior period, false otherwise</returns>
		bool HasPriorPeriod();

		/// <summary>
		/// Dynamically gets component type name(ASSEMBLY_NAME) based on the version ID(ASSEMBLY_ID).
		/// </summary>
		/// <param name="componentVersionID">version id(ASSEMBLY_ID)ASSEMBLY_ID</param>
		/// <returns>type name for the given component version(ASSEMBLY_NAME)</returns>
		 string GetComponentTypeName(Guid componentVersionID);

		/// <summary>
		 /// Dynamically gets component type ID(COMPONENT_ID) based on the version ID(ASSEMBLY_ID).
		/// </summary>
		 /// <param name="componentVersionID">version id(ASSEMBLY_ID)</param>
		 /// <returns>type name for the given component version(COMPONENT_ID)</returns>
		Guid GetComponentTypeID(Guid componentVersionID);

        /// <summary>
        /// Gets the organisation unit that owns this period
        /// </summary>
        /// <returns>The organisation unit that owns this period</returns>
        IOrganizationUnit GetParentOrganisationUnit();
	}
}
