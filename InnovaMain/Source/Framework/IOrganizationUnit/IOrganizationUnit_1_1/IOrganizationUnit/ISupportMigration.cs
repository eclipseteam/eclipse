﻿using System.Xml.Linq;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM
{
    public interface ISupportMigration
    {
        void Export(XElement root);
        void Import(XElement root, IdentityCmMap map);
    }
}
