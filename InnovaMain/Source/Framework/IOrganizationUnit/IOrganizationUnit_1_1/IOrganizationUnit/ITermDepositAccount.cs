﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Common;

namespace Oritax.TaxSimp.CM.Organization
{
    public interface ITermDepositAccount
    {
        TermDepositAccountEntity TermDepositAccountEntity { get; }
    }
}
