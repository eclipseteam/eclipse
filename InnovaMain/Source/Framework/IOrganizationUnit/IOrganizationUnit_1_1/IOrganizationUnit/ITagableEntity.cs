﻿using System;
using System.Collections;
using System.Collections.Generic;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM
{
    public interface ITagableEntity
    {
        void DoTagged(EntityTag tag,Guid modelID);
        List<IDictionary<string,string>> DoPartialTag(EntityTag tag);

    }
}
