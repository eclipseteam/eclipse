﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Oritax.TaxSimp.IOrganizationUnits
{
    [DataContract]
    public enum WorkpaperDataItemType
    {
        String = 0,
        Decimal = 1
    }
}
