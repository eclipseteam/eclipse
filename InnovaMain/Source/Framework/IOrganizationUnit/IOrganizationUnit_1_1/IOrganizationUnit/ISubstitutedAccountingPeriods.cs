using System;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.BusinessStructureUtilities;

namespace Oritax.TaxSimp.SAPBase
{
	public interface ISubstitutedAccountingPeriod
	{
		Guid Key{get;}
		DateTime StartDate{	get; set;}
		DateTime EndDate{get; set;}
		bool IsShared{get; set;}
		ISubstitutedAccountingPeriod Copy( );
	
	}
	/// <summary>
	/// Summary description for ISubstitutedAccountingPeriods.
	/// </summary>
	public interface ISubstitutedAccountingPeriods
	{
		void Add(ISubstitutedAccountingPeriod period);
		void Remove(ISubstitutedAccountingPeriod period);
		void Remove(Guid key);
		int Count{get;}
		bool VerifyPeriodIsSAPCompliant(DateTime startDate, DateTime endDate);
		TaxYear GetTaxYear(DateTime periodStartDate, DateTime periodEndDate);
		FinancialYear GetFinancialYear(DateTime periodStartDate, DateTime periodEndDate);
		bool DoPeriodsOverlap(DateTime objStartDate, DateTime objEndDate, Guid objKey);
		ISubstitutedAccountingPeriod this[ Guid key ]{get;}
		
	}
}
