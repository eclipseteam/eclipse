﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Common.UMAFee;

namespace Oritax.TaxSimp.CalculationInterface
{
    public interface IFeeRun : IBrokerManagedComponent
    {
        bool RunLocalFee { get; set; }
        DateTime LastModifiedDate { get; set; }
        DateTime CreatedDate { get; set; }
        FeeRunType FeeRunType { get; set; }
        string ShortName { get; set; }
        string Description { get; set; }
        int Month { get; set; }
        int Year { get; set; }
        List<FeeTransaction> FeeTransactionLists { get; set; }
    }
}
