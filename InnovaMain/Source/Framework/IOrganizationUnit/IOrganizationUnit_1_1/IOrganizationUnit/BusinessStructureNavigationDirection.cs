using System;
using System.Collections.Generic;
using System.Text;

namespace Oritax.TaxSimp.CalculationInterface
{
    public enum BusinessStructureNavigationDirection
    {
        DownAndAcross,
        UpAndAcross,
    }
}
