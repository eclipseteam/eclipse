using System;
using System.Collections;
using System.Data;

using Oritax.TaxSimp.CalculationInterface;
using System.Collections.Generic;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Common.UMAFee;

namespace Oritax.TaxSimp.CM.Organization    
{
    public interface IOrganization : ICalculationModule
    {
        void DeleteOrganizationUnit(Guid gdOrgUnitCLID);
        int SecuritySetting { get; set; }
        string SystemWideDisplayMessage { get; set; }
        DataSet GetReportingUnits();
        string OrganisationName { get; set; }
        List<ProductEntity> Products { get; set; }
        List<InvestmentManagementEntity> InvestmentManagementDetails { get; set; }
        List<AssetEntity> Assets { get; set; }
        List<BusinessGroupEntity> BusinessGroups { get; set; }
        List<ModelEntity> Model { get; set; }
        List<ProductSecuritiesEntity> ProductSecurities { get; set; }
        List<ManualAssetEntity> ManualAsset { get; set; }
        List<SecuritiesEntity> Securities { get; set; }
        List<InstitutionEntity> Institution { get; set; }
        List<FeeEntity> FeeEnttyList { get; set; }
        Dictionary<string, string> BussinessTitles { get; set; }
        OrganizationalSettings OrganizationalSettings { get; set; }
    }
}
