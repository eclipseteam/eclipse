using System;
using Oritax.TaxSimp.BusinessStructureUtilities;

using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CM.Period;

namespace Oritax.TaxSimp.CM.MultiPeriod
{
	/// <summary>
	/// Represents a multi period in the system
	/// </summary>
	public interface IMultiPeriod : IPeriod
	{
		MemberPeriods IncludedMultiPeriodMembers{get;}
		ConsolidationMode ConsolMode{get;}

		void RemoveAdjMember(Guid gdCLID);
		MemberPeriod NewPeriodMember(  Guid cLID, bool blAddition,  bool blAdjustment);

		 void SubscribeNewScenario();
	}
}
