using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using System;
using System.Data;

namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// Interface for DBUser.
	/// </summary>
	public interface IDBUser : IBrokerManagedComponent
	{
		UserContext Context{get; set;}
		bool Administrator{get; set;}
        bool DisplayZeroPreference { get; set;}
        Guid OrganisationCID { get; set; }
        void AddFavouriteWorkpaper(string folderName, string workpaperName, string workpaperAddress);
        void ExtractFavouriteWorkpapers(DataSet dataSet);
	}
}
