using System;

namespace Oritax.TaxSimp.Security
{
	public interface IParty
	{
		Guid PrivilegeTemplateID{get;}
	}
}
