using System;
using System.Collections.Generic;
using System.Text;

namespace Oritax.TaxSimp.Security
{
    public class DBUserConstants
    {
        #region Fields

        public const string FavouritesFolderId              = "FAF9A5A7-842C-49ff-94D9-62F6DF151887";

        public const string FAVOURITEWORKPAPERSTABLE        = "FAVOURITEWORKPAPERS";
        public const string WorkpaperId                     = "ID";
        public const string FolderField                     = "Folder";
        public const string WorkpaperName                   = "WorkpaperName";
        public const string WorkpaperAddress                = "WorkpaperAddress";

        public const string AddFavorite_SP                  = "DbUser_AddFavourite";
        public const string AddFavorite_Id                  = "@id";
        public const string AddFavorite_UserId              = "@userid";
        public const string AddFavorite_Folder              = "@folder";
        public const string AddFavorite_WorkpaperName       = "@workpapername";
        public const int AddFavorite_WorkpaperName_Size     = 355;
        public const string AddFavorite_WorkpaperAddr       = "@workpaperaddress";
        public const int AddFavorite_WorkpaperAddr_Size     = 355;

        public const string SP_Favourites                   = "DbUser_Favourites";
        public const string Favourites_Username             = "@username";
        public const int Favourites_Username_Size           = 50;

        public const string FAVOURITEFOLDERSTABLE           = "FAVOURITEFOLDERS";
        public const string FolderId                        = "ID";
        public const string ParentFolderField               = "ParentFolder";
        public const string FolderName                      = "Name";
        public const string IsRootFolder                    = "IsRoot";

        public const string AddFavoriteFolder_SP            = "DBUser_AddFavouriteFolder";
        public const string AddFavoriteFolder_Id            = "@id";
        public const string AddFavoriteFolder_UserId        = "@userid";
        public const string AddFavoriteFolder_ParentFolder  = "@parentFolder";
        public const string AddFavoriteFolder_FolderName    = "@name";
        public const int AddFavoriteFolder_FolderName_Size  = 50;
        public const string AddFavoriteFolder_IsRoot        = "@isRoot";

        public const string SP_UserVisitedWorkpapers        = "DbUser_UserVisitedWorkpapers";
        public const string UserVisitedWorkpapers_Username  = "@username";
        public const int UserVisitedWorkpapers_Username_Size = 50;

        #endregion // Fields
    }
}
