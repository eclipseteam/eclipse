using System;

namespace Oritax.TaxSimp.Security
{
	public interface IPrivilegeTemplate
	{
		PrivEntrySet PrivilegeEntries{get;}
	}
}
