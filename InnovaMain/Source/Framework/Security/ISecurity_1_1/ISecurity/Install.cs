using System;

namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
	public class Install
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="8A9B9D6C-58E1-4727-B2DE-DE8C1D3483C0";
		public const string ASSEMBLY_NAME="ISecurity_1_1";
		public const string ASSEMBLY_DISPLAYNAME="ISecurity V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
//		public const string ASSEMBLY_REVISION="0"; 
		public const string ASSEMBLY_REVISION="1"; //2005.1

		#endregion
	}
}
