﻿using System;
using System.Web;

namespace WebClient
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LabelUsername.Text =  HttpContext.Current.User.Identity.Name;
            }
        }
    }
}