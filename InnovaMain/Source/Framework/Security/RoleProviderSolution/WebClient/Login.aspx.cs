﻿using System;
using System.Web.Security;
using System.Diagnostics;

namespace WebClient
{
    public partial class Login : System.Web.UI.Page
    {

        protected void ButtonLogin_Click(object sender, EventArgs e)
        {
            Authenticate();
        }

        private void Authenticate()
        {
            Debug.WriteLine(Membership.Provider.Name);
            if (Membership.ValidateUser(TextUsername.Text, TextPassword.Text))
            {
                FormsAuthentication.RedirectFromLoginPage(TextUsername.Text, true);
            }
        }
    }
}