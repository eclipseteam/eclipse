﻿using System;

namespace Oritax.TaxSimp.Security
{
    public class InnovaUser 
    {
        public Guid Cid { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public DateTime? LastLoggedIn { get; set; }
        public string EmailAddress { get; set;}
        public int FailedAttempts { get; set; }
        public bool IsLocked { get; set; }
        public bool IsAdministrator { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
