﻿using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.Web.Security;
using Oritax.TaxSimp.Managers;

namespace Oritax.TaxSimp.Security
{
    public class InnovaRoleProvider : RoleProvider
    {
        public string ConnectionString { get; set; }
        private RoleManager _Manager;
        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(name, config);
            ConnectionStringSettings ConnectionStringSettings = ConfigurationManager.ConnectionStrings[config["connectionStringName"]];
            if (ConnectionStringSettings == null || ConnectionStringSettings.ConnectionString.Trim() == "") throw new ProviderException("Connection string cannot be blank.");
            ConnectionString = ConnectionStringSettings.ConnectionString;
            _Manager = new RoleManager(ConnectionString);
        }

        #region Abstract Methods - Not Implemented

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new System.NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new System.NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        #region Abstract Properties - Implemented
        private string _ApplicatioName;
        public override string ApplicationName { get { return _ApplicatioName; } set { _ApplicatioName = value; } }
        #endregion

        #region Abstract Methods - Implemented
        
        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            return _Manager.FindUsersInRole(roleName, usernameToMatch);
        }

        public override string[] GetAllRoles()
        {
            return _Manager.GetAllRoles();
        }

        public override string[] GetRolesForUser(string username)
        {
            return _Manager.GetUserRoles(username);
        }

        public override string[] GetUsersInRole(string roleName)
        {
            return _Manager.GetUsersInRole(roleName);
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            return _Manager.IsUserInRole(username, roleName);
        }

        public override bool RoleExists(string roleName)
        {
            return _Manager.RoleExists(roleName);
        }

        #endregion

    }
}
