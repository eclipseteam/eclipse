﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.Web.Security;
using Oritax.TaxSimp.Converters;
using Oritax.TaxSimp.Managers;

namespace Oritax.TaxSimp.Security
{
    public class InnovaMembershipProvider : MembershipProvider
    {
        public string ConnectionString { get; set; }
        private UserManager _Manager;
        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(name, config);
            ConnectionStringSettings ConnectionStringSettings = ConfigurationManager.ConnectionStrings[config["connectionStringName"]];
            if (ConnectionStringSettings == null || ConnectionStringSettings.ConnectionString.Trim() == "") throw new ProviderException("Connection string cannot be blank.");
            ConnectionString = ConnectionStringSettings.ConnectionString;
            _Manager = new UserManager(ConnectionString);
        }

        #region Abstract Properties - Not Implemented

        public override bool EnablePasswordReset
        {
            get { throw new System.NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new System.NotImplementedException(); }
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new System.NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new System.NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new System.NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new System.NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new System.NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new System.NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new System.NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new System.NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new System.NotImplementedException(); }
        }

        #endregion

        #region Abstract Methods - Not Implemented

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new System.NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new System.NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new System.NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new System.NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new System.NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new System.NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new System.NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        #region Abstract Properties - Implemented
        private string _ApplicatioName;
        public override string ApplicationName { get { return _ApplicatioName; } set { _ApplicatioName = value; } }
        #endregion

        #region Abstract Methods - Implemented

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            IList<InnovaUser> users = _Manager.FindUsersByEmail(emailToMatch);
            totalRecords = users.Count;
            return UserConvert.ToMembershipUserCollection(users, this.Name);
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            IList<InnovaUser> users = _Manager.FindUsersByName(usernameToMatch);
            totalRecords = users.Count;
            return UserConvert.ToMembershipUserCollection(users, this.Name);
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            IList<InnovaUser> users = _Manager.GetAllUsers();
            totalRecords = users.Count;
            return UserConvert.ToMembershipUserCollection(users, this.Name);
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            InnovaUser user = _Manager.GetUserByName(username);
            return UserConvert.ToMembershipUser(user, this.Name);
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            InnovaUser user = _Manager.GetUserById(SafeConvert.ToGuid(providerUserKey));
            return UserConvert.ToMembershipUser(user, this.Name);
        }

        public override string GetUserNameByEmail(string email)
        {
            return _Manager.GetUserNameByEmail(email);
        }

        public override bool ValidateUser(string username, string password)
        {
            return _Manager.ValidateUser(username, password);
        }

        #endregion

    }
}
