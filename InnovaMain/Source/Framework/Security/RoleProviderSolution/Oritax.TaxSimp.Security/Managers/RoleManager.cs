﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Oritax.TaxSimp.Converters;

namespace Oritax.TaxSimp.Managers
{
    public class RoleManager
    {
        private const string AdministratorRole = "Administrator";
        private const string UserRole = "User";
        private static readonly List<string> Roles = new List<string>() { AdministratorRole, UserRole };
        private SqlManager _Sql;

        public RoleManager(string connectionString)
        {
            _Sql = new SqlManager(connectionString);
        }

        public string[] GetAllRoles()
        {
            return Roles.ToArray();
        }

        public bool RoleExists(string role)
        {
            return Roles.Exists(r => EqualIgnoreCase(r,role));
        }

        public string[] GetUserRoles(string name)
        {
            if(EqualIgnoreCase(name, AdministratorRole)) return Roles.ToArray(); 
            var user = new UserManager(_Sql.ConnectionString).GetUserByName(name);
            if (user == null) return new string[] { };
            return user.IsAdministrator ? Roles.ToArray() : new string[] { UserRole };
        }

        public string[] GetUsersInRole(string role)
        {
            if(!RoleExists(role)) return new string[]{};
            return new UserManager(_Sql.ConnectionString).GetUsersInRole(EqualIgnoreCase(role, AdministratorRole));
        }

        public string[] FindUsersInRole(string role, string user)
        {
            if(!RoleExists(role)) return new string[]{};
            List<string> users = new List<string>();
            bool administrator = EqualIgnoreCase(role, AdministratorRole);
            SqlCommand command = new SqlCommand();
            string query = "SELECT Username FROM UserDetail WHERE Username LIKE @Username";
            if( administrator)
            {
                query += " AND Administrator = @Administrator";
                command.Parameters.Add(new SqlParameter("@Administrator", administrator));
                users.Add("Administrator");
            }
            command.Parameters.Add(new SqlParameter("@Username", string.Format("%{0}%", user)));
            command.CommandText = query;
            _Sql.ForEach(command, r => users.Add(SafeConvert.ToString(r["Username"])));
            return users.ToArray();
        }

        public bool IsUserInRole(string name, string role)
        {
            if (!RoleExists(role)) return false;
            List<string> roles = new List<string>(GetUserRoles(name));
            return roles.Exists(r => EqualIgnoreCase(r, role));
        }

        private static bool EqualIgnoreCase(string v1, string v2)
        {
            return string.Equals(v1, v2, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
