﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Oritax.TaxSimp.Converters;
using Oritax.TaxSimp.Security;

namespace Oritax.TaxSimp.Managers
{
    public class UserManager
    {
        private const string QueryGetAll = "SELECT Cid, Username, Password, Active, LastLoggedIn, EmailAddress, FailedAttempts, Locked, Administrator, FirstName, LastName FROM UserDetail";
        private const string AdministratorUser = "Administrator";

        private SqlManager _Sql;

        public UserManager(string connectionString)
        {
            _Sql = new SqlManager(connectionString);
        }

        public IList<InnovaUser> GetAllUsers()
        {
            List<InnovaUser> users = new List<InnovaUser>();
            _Sql.ForEach(QueryGetAll, r => users.Add(GetUserFromDataReader(r)));
            return users;
        }

        public IList<InnovaUser> FindUsersByEmail(string value)
        {
            List<InnovaUser> users = new List<InnovaUser>();
            _Sql.ForEachLike(QueryGetAll, "EmailAddress", value, 
                r => users.Add(GetUserFromDataReader(r)));
            return users;
        }

        public IList<InnovaUser> FindUsersByName(string value)
        {
            List<InnovaUser> users = new List<InnovaUser>();
            _Sql.ForEachLike(QueryGetAll, "Username", value, 
                r => users.Add(GetUserFromDataReader(r)));
            return users;
        }

        public InnovaUser GetUserByName(string value)
        {
            InnovaUser user = null;
            _Sql.ForEach(QueryGetAll, "Username", value, 
                r => user = GetUserFromDataReader(r));
            return user;
        }

        public InnovaUser GetUserById(Guid value)
        {
            InnovaUser user = null;
            _Sql.ForEach(QueryGetAll, "CID", value, 
                r => user = GetUserFromDataReader(r));
            return user;
        }

        public string GetUserNameByEmail(string value)
        {
            string name = null;
            string query = "SELECT Username FROM UserDetail";
            _Sql.ForEach(query, "EmailAddress", value, 
                r => name = SafeConvert.ToString(r["Username"]));
            return name;
        }

        public string[] GetUsersInRole(bool administrator)
        {
            List<string> names = new List<string>();
            string query = "SELECT Username FROM UserDetail";
            if (administrator)
            {
                _Sql.ForEach(query, "Administrator", 1,
                           r => names.Add(SafeConvert.ToString(r["Username"])));
                names.Add(AdministratorUser);
            }
            else
            {
                _Sql.ForEach(query,
                    r => names.Add(SafeConvert.ToString(r["Username"])));
            }
            return names.ToArray();
        }

        public bool ValidateUser(string username, string password)
        {
            string name = null;
            SqlCommand command = new SqlCommand("SELECT Username FROM UserDetail WHERE Username = @Username AND Password = @Password");
            command.Parameters.Add(new SqlParameter("@Username", username));
            command.Parameters.Add(new SqlParameter("@Password", EncryptionManager.EncryptData(password)));
            //command.Parameters.Add(new SqlParameter("@Password", password));
            _Sql.ForEach(command, r => name = SafeConvert.ToString(r["Username"]));
            return !string.IsNullOrEmpty(name);
        }

        private static InnovaUser GetUserFromDataReader(SqlDataReader r)
        {
            InnovaUser user = new InnovaUser();
            user.Cid = SafeConvert.ToGuid(r["CID"]);
            user.Username = SafeConvert.ToString(r["Username"]);
            user.Password = SafeConvert.ToString(r["Password"]);
            user.IsActive = SafeConvert.ToBool(r["Active"]);
            user.LastLoggedIn = SafeConvert.ToDateTime(r["LastLoggedIn"]);
            user.EmailAddress = SafeConvert.ToString(r["EmailAddress"]);
            user.FailedAttempts = SafeConvert.ToInt32( r["FailedAttempts"]).Value;
            user.IsLocked = SafeConvert.ToBool(r["Locked"]);
            user.IsAdministrator = SafeConvert.ToBool(r["Administrator"]);
            user.FirstName = SafeConvert.ToString(r["FirstName"]);
            user.LastName = SafeConvert.ToString(r["LastName"]);
            return user;
        }
    }
}
