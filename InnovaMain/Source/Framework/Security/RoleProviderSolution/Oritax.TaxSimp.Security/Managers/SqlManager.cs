﻿using System;
using System.Data.SqlClient;

namespace Oritax.TaxSimp.Managers
{
    public class SqlManager
    {
        //private string ConnectionString = "server=shamil-pc;database=RPDB;UID=beta;pwd=beta;";
        public SqlManager(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public string ConnectionString { get; private set; }

        public void ForEach(string query, Action<SqlDataReader> action)
        {
            SqlCommand command = new SqlCommand(query);
            ForEach(command, action);
        }

        public void ForEach(string query, string column, object value,  Action<SqlDataReader> action)
        {
            string text = string.Format("{0} WHERE {1} = @{1}", query, column);
            SqlCommand command = new SqlCommand(text);
            command.Parameters.Add(new SqlParameter(string.Format("@{0}", column), value));
            ForEach(command, action);
        }
        
        public void ForEachLike(string query, string column, object value, Action<SqlDataReader> action)
        {
            string text = string.Format("{0} WHERE {1} LIKE @{1}", query, column);
            SqlCommand command = new SqlCommand(text);
            command.Parameters.Add(new SqlParameter(string.Format("@{0}", column), string.Format("%{0}%", value)));
            ForEach(command, action);
        }

        public void ForEach(SqlCommand command, Action<SqlDataReader> action)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                command.Connection = connection;
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while(reader.Read())
                {
                    action(reader);
                }
                connection.Close();
            }
        }
    }
}
