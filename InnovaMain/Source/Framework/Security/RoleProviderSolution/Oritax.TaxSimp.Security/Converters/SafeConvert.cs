﻿using System;

namespace Oritax.TaxSimp.Converters
{
    public static class SafeConvert
    {
        public static Guid ToGuid(object value)
        {
            return value == null || string.IsNullOrEmpty(value.ToString()) ? new Guid() :  new Guid(value.ToString());
        }

        public static bool ToBool(object value)
        {
            try
            {
                int? converted = ToInt32(value);
                return converted.HasValue  && converted.Value != 0 ? true : false;
            }
            catch { }
            return Convert.ToBoolean(value);
        }

        public static int? ToInt32(object value)
        {
            return Convert.ToInt32(value);
        }

        public static DateTime? ToDateTime(object value)
        {
            try
            {
                return Convert.ToDateTime(value);
            }
            catch
            {
                return null;
            }
        }
        
        public static string ToString(object value)
        {
            return value == null ? string.Empty : Convert.ToString(value);
        }
    }
}
