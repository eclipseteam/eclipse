﻿using System;
using System.Collections.Generic;
using System.Web.Security;
using Oritax.TaxSimp.Security;

namespace Oritax.TaxSimp.Converters
{
    public static class UserConvert
    {
        public static MembershipUser ToMembershipUser(InnovaUser user, string provider)
        {
            return new MembershipUser(
                provider, user.Username, user.Cid, user.EmailAddress, 
                string.Empty, string.Empty, user.IsActive, user.IsLocked,
                DateTime.MinValue, user.LastLoggedIn ?? DateTime.MinValue, 
                DateTime.MinValue, DateTime.MinValue, DateTime.MinValue);
        }

        public static MembershipUserCollection ToMembershipUserCollection(IList<InnovaUser> users, string provider)
        {
            MembershipUserCollection collection = new MembershipUserCollection();
            foreach(var each in users)
            {
                collection.Add(ToMembershipUser(each, provider));
            }
            return collection;
        }
    }
}
