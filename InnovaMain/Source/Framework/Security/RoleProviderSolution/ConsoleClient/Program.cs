﻿using System;
using Oritax.TaxSimp.Managers;

namespace ConsoleClient
{
    class Program
    {
        private const string ConnectionString = "server=shamil-pc;database=RPDB;UID=beta;pwd=beta;";
        private const string AdministratorRole = "Administrator";
        private const string UserRole = "User";
        private const string FakeRole = "Fake";

        static void Main(string[] args)
        {
            RoleProviderTests();
            MembershipProviderTests();
        }

        private static void MembershipProviderTests()
        {
            UserManager manager = new UserManager(ConnectionString);

            var v11 = manager.FindUsersByEmail("A");
            var v12 = manager.FindUsersByEmail("A1");
            var v13 = manager.FindUsersByEmail("B");

            var v21 = manager.FindUsersByName("A");
            var v22 = manager.FindUsersByName("A1");
            var v23 = manager.FindUsersByName("B");

            var v31 = manager.GetAllUsers();


            var v41 = manager.GetUserByName("A1");
            var v42 = manager.GetUserByName("A101");
            var v43 = manager.GetUserByName("A");
            var v44 = manager.GetUserByName("B");

            var v51 = manager.GetUserById(new Guid("84DBBEA8-D19B-43A8-9F2F-53888F8795D9"));
            var v52 = manager.GetUserById(new Guid("AA427159-AB9A-4F4E-8EDD-4B898BC162D5"));
            var v53 = manager.GetUserById(new Guid("AA427159-AB9A-4F4E-8EDD-4B898BC162D4"));

            var v61 = manager.GetUserNameByEmail("A1@gmail.com");
            var v62 = manager.GetUserNameByEmail("A@gmail.com");
            var v63 = manager.GetUserNameByEmail("B@gmail.com");

            var v71 = manager.ValidateUser("A1", "P1");
            var v72 = manager.ValidateUser("A1", "P2");
            var v73 = manager.ValidateUser("B1", "B1");
        }

        private static void RoleProviderTests()
        {
            RoleManager manager = new RoleManager(ConnectionString);

            var v11 = manager.FindUsersInRole(AdministratorRole, "A");
            var v12 = manager.FindUsersInRole(UserRole, "A");
            var v13 = manager.FindUsersInRole(AdministratorRole, "A1");
            var v14 = manager.FindUsersInRole(UserRole, "A1");
            var v15 = manager.FindUsersInRole(FakeRole, "A");
            var v16 = manager.FindUsersInRole(AdministratorRole, "B");
            var v17 = manager.FindUsersInRole(UserRole, "B");

            var v21 = manager.GetAllRoles();

            var v31 = manager.GetUserRoles("A1");
            var v32 = manager.GetUserRoles("A101");
            var v33 = manager.GetUserRoles("B");

            var v41 = manager.GetUsersInRole(AdministratorRole);
            var v42 = manager.GetUsersInRole(UserRole);
            var v43 = manager.GetUsersInRole(FakeRole);

            var v51 = manager.IsUserInRole("A1", AdministratorRole);
            var v52 = manager.IsUserInRole("A1", UserRole);
            var v53 = manager.IsUserInRole("A101", AdministratorRole);
            var v54 = manager.IsUserInRole("A101", UserRole);
            var v55 = manager.IsUserInRole("A1", FakeRole);
            var v56 = manager.IsUserInRole("B", AdministratorRole);
            var v57 = manager.IsUserInRole("B", UserRole);

            var v61 = manager.RoleExists(AdministratorRole);
            var v62 = manager.RoleExists(UserRole);
            var v63 = manager.RoleExists(FakeRole);

        }

    }
}
