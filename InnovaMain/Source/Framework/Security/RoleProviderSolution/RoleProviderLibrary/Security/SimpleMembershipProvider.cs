﻿using System;
using System.Web.Security;

namespace MyWork.Security
{
    public class SimpleMembershipProvider : MembershipProvider
    {
        private string _ApplicationName = string.Empty;
        public override string ApplicationName
        {
            get { return _ApplicationName; }
            set { _ApplicationName = value; }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            totalRecords = 3;
            MembershipUserCollection users = new MembershipUserCollection();
            users.Add(Helper.ToUser("admin"));
            users.Add(Helper.ToUser("tom"));
            users.Add(Helper.ToUser("user"));
            return users;
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            totalRecords = 3;
            MembershipUserCollection users = new MembershipUserCollection();
            users.Add(Helper.ToUser("admin"));
            users.Add(Helper.ToUser("tom"));
            users.Add(Helper.ToUser("user"));
            return users;
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            totalRecords = 3;
            MembershipUserCollection users = new MembershipUserCollection();
            users.Add(Helper.ToUser("admin"));
            users.Add(Helper.ToUser("tom"));
            users.Add(Helper.ToUser("user"));
            return users;
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            if (username == "admin") return Helper.ToUser("admin");
            if (username == "tom") return Helper.ToUser("tom");
            if (username == "user") return Helper.ToUser("user");
            return null;
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            if (providerUserKey.ToString() == "admin") return Helper.ToUser("admin");
            if (providerUserKey.ToString() == "tom") return Helper.ToUser("tom");
            if (providerUserKey.ToString() == "user") return Helper.ToUser("user");
            return null;
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string username, string password)
        {
            if (username == "admin" && password == "admin") return true;
            else if (username == "user" && password == "user") return true;
            else if (username == "tom" && password == "tom") return true;
            return false;
        }
    }
}
