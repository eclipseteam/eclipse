﻿using System;
using System.Web.Security;

namespace MyWork.Security
{
    [Serializable]
    public class SimpleUser : MembershipUser
    {
        public Guid Cid { get; set; }
        public string TName { get; set; }
        public bool IsActive { get; set; }
        public int FailAttempts { get; set; }

        public SimpleUser(string name)
            : base("SimpleMembership", name, name, name + "@gmail.com", string.Empty, string.Empty, true, false, DateTime.Now.AddYears(-5), DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1).AddMinutes(-20), DateTime.Now.AddDays(-2), DateTime.Now.AddDays(-2))
        {
            Cid = Guid.NewGuid();
            TName = name;
            IsActive = true;
            FailAttempts = 2;
        }
    }
}
