﻿using System;
using System.Web.Security;
using System.IO;

namespace MyWork.Security
{
    
    public static class Helper
    {
        //public static MembershipUser ToUser(string name)
        //{
        //    return new MembershipUser("SimpleMembership", name, name, name + "@gmail.com", string.Empty, string.Empty, true, false, DateTime.Now.AddYears(-5), DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1).AddMinutes(-20), DateTime.Now.AddDays(-2), DateTime.Now.AddDays(-2));
        //}

        public static SimpleUser ToUser(string name)
        {
            return new SimpleUser(name);
        }

        private static readonly string LogFileName = @"c:\MyLogs\RPLog.txt";
        public static void Log(string value)
        {
            using (StreamWriter writer = new StreamWriter(new FileStream(LogFileName, FileMode.Append, FileAccess.Write)))
            {
                writer.WriteLine(string.Format("{0:hh.mm.ssss} - {1}", DateTime.Now, value));
                writer.Close();
            }

        }
    }
}
