﻿using System;
using System.Web.Security;

namespace MyWork.Security
{
    public class SimpleRoleProvider : RoleProvider
    {
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        private string _ApplicationName = string.Empty;
        public override string ApplicationName
        {
            get { return _ApplicationName; }
            set { _ApplicationName = value;  }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            if( roleName == "admin") return new string[] { "admin" };
            else if(roleName == "user") return new string[] { "user", "tom" };
            return new string[] {};
        }

        public override string[] GetAllRoles()
        {
            return new string[] { "admin", "user" };
        }

        public override string[] GetRolesForUser(string username)
        {
            if (username == "admin") return new string[] { "admin", "user" };
            else if(username == "user" || username == "tom") return new string[] { "user" };
            return new string[] { };
        }

        public override string[] GetUsersInRole(string roleName)
        {
            if (roleName == "admin") return new string[] { "admin" };
            else if (roleName == "user") return new string[] {"admin", "tom", "user" };
            return new string[] { };
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            if (roleName == "admin") return username == "admin";
            if (roleName == "user") return username == "user" || username == "tom" || username == "admin";
            return false;
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            return roleName == "admin" || roleName == "user";
        }
    }
}
