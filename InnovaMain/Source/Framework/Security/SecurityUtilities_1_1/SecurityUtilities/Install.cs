using System;

namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
	public class SecurityUtilitiesInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="32D664F3-5FE9-4520-A521-1C3A4F4D0589";
		public const string ASSEMBLY_NAME="SecurityUtilities_1_1";
		public const string ASSEMBLY_DISPLAYNAME="SecurityUtilities V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
//		public const string ASSEMBLY_REVISION="0";
		public const string ASSEMBLY_REVISION="1"; //2005.1
		#endregion
	}
}
