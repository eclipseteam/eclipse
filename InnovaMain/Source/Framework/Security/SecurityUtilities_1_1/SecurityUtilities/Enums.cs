using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.Utilities;


namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>

	[Serializable]
	public class PrivilegeType: ISerializable
	{
		public const int SysAddUsers						=	0;
		public const int SysDeleteUsers						=	1;
		public const int SysModifyUserDetails				=	2;
		public const int SysChangePrivileges				=	3;
		public const int SysAddOrganization					=	4;
		public const int OrgDeleteOrganizations				=	5;
		public const int OrgModifyOrganizationDetails		=	6;
		public const int OrgChangePrivileges				=	7;
		public const int EntDeleteEntity					=	8;
		public const int EntModifyEntityDetails				=	9;
		public const int EntChangePrivileges				=	10;
		public const int EntCreateScenario					=	11;
		public const int SceDeleteScenario					=	12;
		public const int SceModScenarioDetails				=	13;
		public const int SceChangePrivileges				=	14;
		public const int SceAddPeriod						=	15;
		public const int PerDeletePeriod					=	16;
		public const int PerModifyPeriodDetails				=	17;
		public const int PerChangePrivileges				=	18;
		public const int PerAddCalculationModule			=	19;
		public const int CalRemoveCalculationModule			=	20;
		public const int CalChangePrivileges				=	21;
		public const int CalModCalculationModuleDetails		=	22;
		public const int BMCFullAccess						=   23;


		public struct NameValue{public string Name; public int Value;public NameValue(string name,int val){Name=name;Value=val;}}


		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor

		private int theValue;


		private static PrivilegeOption[] allowDeny=new PrivilegeOption[]{new PrivilegeOption(PrivilegeOption.Allow),new PrivilegeOption(PrivilegeOption.Deny)};
		private static PrivilegeOption[] readModifyDeny=new PrivilegeOption[]{new PrivilegeOption(PrivilegeOption.Read),new PrivilegeOption(PrivilegeOption.Modify),new PrivilegeOption(PrivilegeOption.Deny)};
		private static PrivilegeOption[] none=new PrivilegeOption[]{};

		public PrivilegeOption[] AllowedValues
		{
			get
			{
				switch(theValue)
				{
					case SysAddUsers: return allowDeny;
					case SysDeleteUsers: return allowDeny;
					case SysModifyUserDetails: return readModifyDeny;
					case SysChangePrivileges: return allowDeny;
					case SysAddOrganization: return allowDeny;
					case OrgDeleteOrganizations: return allowDeny;
					case OrgModifyOrganizationDetails: return readModifyDeny;
					case OrgChangePrivileges: return allowDeny;
					case EntDeleteEntity: return allowDeny;
					case EntModifyEntityDetails: return readModifyDeny;
					case EntChangePrivileges: return allowDeny;
					case EntCreateScenario: return allowDeny;
					case SceDeleteScenario: return allowDeny;
					case SceModScenarioDetails: return readModifyDeny;
					case SceChangePrivileges: return allowDeny;
					case SceAddPeriod: return allowDeny;
					case PerDeletePeriod: return allowDeny;
					case PerModifyPeriodDetails: return readModifyDeny;
					case PerChangePrivileges: return allowDeny;
					case PerAddCalculationModule: return allowDeny;
					case CalRemoveCalculationModule: return allowDeny;
					case CalChangePrivileges: return allowDeny;
					case CalModCalculationModuleDetails: return readModifyDeny;
					case BMCFullAccess: return allowDeny;
					default: return none;
				}
			}
		}

		public PrivilegeType(int theValue)
		{
			this.theValue=theValue;
		}

		protected PrivilegeType(SerializationInfo si, StreamingContext context)
		{
			theValue=Serialize.GetSerializedInt32(si,"pt_theValue");
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			Serialize.AddSerializedValue(si,"pt_theValue",theValue);
		}

		public static NameValue[] NameValuePairs
		{
			get
			{
				return new NameValue[]
				{
					new NameValue("Add Users", SysAddUsers),
					new NameValue("Delete Users", SysDeleteUsers),
					new NameValue("Modify User Details", SysModifyUserDetails),
					new NameValue("Change Privileges", SysChangePrivileges),
					new NameValue("Add Organization", SysAddOrganization),
					new NameValue("Delete Organization", OrgDeleteOrganizations),
					new NameValue("Modify Organization Details", OrgModifyOrganizationDetails),
					new NameValue("Change Privileges", OrgChangePrivileges),
					new NameValue("Delete Entity", EntDeleteEntity),
					new NameValue("Modify Entity Details", EntModifyEntityDetails),
					new NameValue("Change Privileges", EntChangePrivileges),
					new NameValue("Create Scenario", EntCreateScenario),
					new NameValue("Delete Scenario", SceDeleteScenario),
					new NameValue("Modify Scenario Details", SceModScenarioDetails),
					new NameValue("Change Privileges", SceChangePrivileges),
					new NameValue("Add Period", SceAddPeriod),
					new NameValue("Delete Period", PerDeletePeriod),
					new NameValue("Modify Period Details", PerModifyPeriodDetails),
					new NameValue("Change Privileges", PerChangePrivileges),
					new NameValue("Add Calculation Module", PerAddCalculationModule),
					new NameValue("Remove Calculation Module", CalRemoveCalculationModule),
					new NameValue("Change Privileges", CalChangePrivileges),
					new NameValue("Modify Calc. Module Details", CalModCalculationModuleDetails),
					new NameValue("Modify Calc. Module Details", CalModCalculationModuleDetails),
					new NameValue("Full access", BMCFullAccess)
				};
			}
		}

		public PrivilegeType(PrivilegeType old)
		{
			this.theValue=old.theValue;
		}

		public override string ToString()
		{
			switch(theValue)
			{
				case SysAddUsers: return "Add Users";
				case SysDeleteUsers: return "Delete Users";
				case SysModifyUserDetails: return "Modify User Details";
				case SysChangePrivileges: return "Change Privileges";
				case SysAddOrganization: return "Add Organization";
				case OrgDeleteOrganizations: return "Delete Organization";
				case OrgModifyOrganizationDetails: return "Modify Organization Details";
				case OrgChangePrivileges: return "Change Privileges";
				case EntDeleteEntity: return "Delete Entity";
				case EntModifyEntityDetails: return "Modify Entity Details";
				case EntChangePrivileges: return "Change Privileges";
				case EntCreateScenario: return "Create Scenario";
				case SceDeleteScenario: return "Delete Scenario";
				case SceModScenarioDetails: return "Modify Scenario Details";
				case SceChangePrivileges: return "Change Privileges";
				case SceAddPeriod: return "Add Period";
				case PerDeletePeriod: return "Delete Period";
				case PerModifyPeriodDetails: return "Modify Period Details";
				case PerChangePrivileges: return "Change Privileges";
				case PerAddCalculationModule: return "Add Calculation Module";
				case CalRemoveCalculationModule: return "Remove Calculation Module";
				case CalChangePrivileges: return "Change Privileges";
				case CalModCalculationModuleDetails: return "Modify Calc. Module Details";
				case BMCFullAccess: return "Full Access";
				default: return String.Empty;
			}
		}

		public int ToInt(){return theValue;}

		static public implicit operator PrivilegeType(int theValue) 
		{
			return new PrivilegeType(theValue);
		}

		public static bool operator == (PrivilegeType val1, PrivilegeType val2)
		{
			return val1.theValue==val2.theValue;
		}

		public static bool operator != (PrivilegeType val1, PrivilegeType val2)
		{
				return val1.theValue!=val2.theValue;
		}
		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

	}

	[Serializable]
	public class PrivilegeOption: ISerializable
	{
		public const int Unspecified		=  -1;
		public const int Deny				=	0;
		public const int Read				=	1;
		public const int Modify				=	2;
		public const int Allow				=	3;

		public struct NameValue{public string Name; public int Value;public NameValue(string name,int val){Name=name;Value=val;}}

		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor
		private int theValue;

		public PrivilegeOption(int theValue)
		{
			this.theValue=theValue;
		}

		protected PrivilegeOption(SerializationInfo si, StreamingContext context)
		{
			theValue=Serialize.GetSerializedInt32(si,"po_theValue");
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			Serialize.AddSerializedValue(si,"po_theValue",theValue);
		}

		public static NameValue[] NameValuePairs
		{
			get
			{
				return new NameValue[]
				{
					new NameValue( "Unspecified", Unspecified ),
					new NameValue("Allow", Allow),
					new NameValue("Modify", Modify),
					new NameValue("Read", Read),
					new NameValue("Deny", Deny)
				};
			}
		}

		public PrivilegeOption(PrivilegeOption old)
		{
			this.theValue=old.theValue;
		}

		public override string ToString()
		{
			switch(theValue)
			{
				case Unspecified: return "Unspecified";
				case Allow: return "Allow";
				case Read: return "Read";
				case Modify: return "Modify";
				case Deny: return "Deny";
				default: return String.Empty;
			}
		}

		public int ToInt(){return theValue;}

		static public implicit operator PrivilegeOption(int theValue)
		{
			return new PrivilegeOption(theValue);
		}

		public override int GetHashCode()
		{
			return this.theValue;
		}

		public static bool operator == (PrivilegeOption val1, PrivilegeOption val2)
		{
			return val1.theValue==val2.theValue;
		}

		public static bool operator != (PrivilegeOption val1, PrivilegeOption val2)
		{
			return val1.theValue!=val2.theValue;
		}

		public static bool operator >= (PrivilegeOption val1, PrivilegeOption val2)
		{
			return val1.theValue>=val2.theValue;
		}

		public static bool operator <= (PrivilegeOption val1, PrivilegeOption val2)
		{
			return val1.theValue<=val2.theValue;
		}

		public static bool operator > (PrivilegeOption val1, PrivilegeOption val2)
		{
			return val1.theValue>val2.theValue;
		}

		public static bool operator < (PrivilegeOption val1, PrivilegeOption val2)
		{
			return val1.theValue<val2.theValue;
		}

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}
	}

	[Serializable]
	public class PrivTemplateEntry: ISerializable
	{
		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor
		public Guid					ID;
		public PrivilegeType		PrivType;
		public PrivilegeOption		PrivValue;

		public PrivTemplateEntry(Guid iD,PrivilegeType	privType,PrivilegeOption privValue)
		{
			PrivType=privType;
			PrivValue=privValue;
			ID=iD;
		}

		protected PrivTemplateEntry(SerializationInfo si, StreamingContext context)
		{
			ID=Serialize.GetSerializedGuid(si,"pte_ID");
			PrivType=(PrivilegeType)Serialize.GetSerializedValue(si,"pte_PrivType",typeof(PrivilegeType),PrivilegeType.SysAddUsers);
			PrivValue=(PrivilegeOption)Serialize.GetSerializedValue(si,"pte_PrivValue",typeof(PrivilegeOption),PrivilegeOption.Deny);
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			Serialize.AddSerializedValue(si,"pte_ID",ID);
			Serialize.AddSerializedValue(si,"pte_PrivType",PrivType);
			Serialize.AddSerializedValue(si,"pte_PrivValue",PrivValue);
		}
	}

	[Serializable]
	public class PrivEntrySet : SerializableList, ISerializable
	{

		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor

		public PrivEntrySet(): base()
		{
		}

		protected PrivEntrySet(SerializationInfo si, StreamingContext context)
			:base(si,context)
		{
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si,context);
		}

		
		public new PrivTemplateEntry this[int index]
		{
			get
			{
				return (PrivTemplateEntry)base[index];
			}
		}

		public void Add(PrivTemplateEntry privTemplateEntry)
		{
			base.Add(privTemplateEntry);
		}
	}

}
