﻿using System;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class Goal
    {
        public Guid ID { get; set; }
        public Goal()
        {
            ID = Guid.NewGuid();
        } 

        public GoalType GoalType
        { get; set; }

        public decimal? Amount
        { get; set; }

        public int? Age
        { get; set; }
    }
}
