﻿
namespace Oritax.TaxSimp.Common
{
    public enum ActionType
    {
        SuperGuaranteeContributions,
        SuperSalarySacrificeContributions,
        SuperAfterTaxContributions,
        NonSuperInvestments,
    }
}
