﻿using System;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class AdviceAction
    {
        
        public ActionType ActionType
        { get; set; }

        public int? PercentOfSalary
        { get; set; }

        public decimal? Amount
        { get; set; }

        public int? ContributionStartAge
        { get; set; }

        public int? ContributionEndAge
        { get; set; }

        public int? SavingStartAge
        { get; set; }

        public int? SavingEndAge
        { get; set; }
    }
}
