﻿
namespace Oritax.TaxSimp.Common
{
    public enum AssetType
    {
        Superannuation,
        Pension,
        NonSuperInvestments,
    }
}
