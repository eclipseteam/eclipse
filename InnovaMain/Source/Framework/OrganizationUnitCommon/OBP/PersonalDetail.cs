﻿using System;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class PersonalDetail
    {
        public string Name
        { get; set; }

        public int? Age
        { get; set; }

        public DateTime? DOB
        { get; set; }

        public int? RetirementAge
        { get; set; }

        public decimal? Salary
        { get; set; }

        public int? PlanAge
        { get; set; }
    }
}
