﻿
namespace Oritax.TaxSimp.Common
{
    public enum GoalType
    {
        Wealth,
        Income,
        LumpSum,
        LiquidAssets,
        Bequest,
    }
}
