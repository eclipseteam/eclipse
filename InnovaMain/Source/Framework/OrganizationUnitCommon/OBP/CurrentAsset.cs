﻿using System;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class CurrentAsset
    {
        
        public AssetType AssetType
        { get; set; }

        public decimal? Equity
        { get; set; }

        public decimal? FixedInterest
        { get; set; }

        public decimal? InternationalEquity
        { get; set; }

        public decimal? Cash
        { get; set; }

        public decimal? Gold
        { get; set; }

        public decimal? Property
        { get; set; }

    }
}
