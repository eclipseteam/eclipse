﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class Advice
    {
        public Advice() {
            ID = Guid.NewGuid();
        } 

        public Guid ID { get; set; }

        public string Title { get; set; }

        public Guid ClientCid { get; set; }

        public string ClientId { get; set; }

        public Guid IndividualCid { get; set; }

        public string IndividualClientId { get; set; }

        public List<Guid> Users { get; set; }

        public PersonalDetail PersonalDetails { get; set; }

        public List<CurrentAsset> CurrentAssets { get; set; }

        public List<AdviceAction> Actions { get; set; }

        public List<Goal> Goals { get; set; }

        public string InvestmentStrategy { get; set; }

        public SortedList<string, DataSet> MillimanResultSet { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }
    }
}
