﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class DoItYourSelf
    {
        public string AuthoriastionType { get; set; }
        public bool ASX { get; set; }
        public bool TDS { get; set; }
        public bool DP { get; set; }
        public string Other { get; set; }
        public string ProgramCode { get; set; }
        public string ProgramName { get; set; }
        public ModelEntity Model { get; set; }
        public AccessFacilities AccessFacilities { get; set; }
        public OperationManner OperationManner { get; set; }

        public bool UseDIYMAQ = false;
        public string DIYMaqBSBNo = string.Empty;
        public string DIYMaqACCNo = string.Empty;
        public string DIYMaqACCName = string.Empty;
        public bool UseDIYBWA = false;
        public bool UseDIYDesktopBroker = false;
        public bool UseDIYAMM = false;
        public bool UseDIYFIIG = false;
        public decimal DIYInitialInvestment = 0;

        public DoItYourSelf()
        {
            AuthoriastionType = "D";
        }
    }
}

