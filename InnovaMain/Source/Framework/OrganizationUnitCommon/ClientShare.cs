﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CM;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class ClientShare
    {
        public string ClientID { get; set; }
        public Guid ClientClid { get; set; }
        public Guid ClientCsid { get; set; }
        public Guid ClientCid { get; set; }
        public decimal? Share { get; set; }
    }
}
