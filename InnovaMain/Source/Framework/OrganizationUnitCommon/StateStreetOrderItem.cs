﻿using System;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class StateStreetOrderItem : OrderItemEntity
    {
        public Guid ProductID { get; set; }
        public string FundCode { get; set; }
    }
}
