﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class OrderPadASXInstructionEntity: OrderInstructionEntity
    {
        public string InvestmentCode { get; set; }
        public string ASXAccountNo { get; set; }
        public string ParentAcc { get; set; }
        public decimal? Units { get; set; }
        public decimal? Amount { get; set; }
        public decimal? UnitPrice { get; set; }
        public string ASXAccountName { get; set; }
    }
}
