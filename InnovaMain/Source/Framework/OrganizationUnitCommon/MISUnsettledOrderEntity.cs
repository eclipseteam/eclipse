﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
    {
    [Serializable]
    public class MISUnsettledOrderEntity
        {
        public Guid ID { get; set; }
        public string ClientID { get; set; }
        public decimal UnsettledOrders { get; set; }
        public decimal UnsettledUnits { get; set; }

        public MISUnsettledOrderEntity()
            {
            ID = Guid.Empty;
            ClientID = string.Empty;
            UnsettledOrders = 0;
            UnsettledUnits = 0;



            }
        }
    }
