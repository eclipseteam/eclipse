﻿using System;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Common
{

    [Serializable]
    public abstract class OrderItemEntity
    {
        public Guid ID { get; set; }
        public Guid OrderID { get; set; }
        public decimal? Units { get; set; }
        public decimal? Amount { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string ValidationMessage { get; set; }
        public string SMATransactionID { get; set; }
    }
}
