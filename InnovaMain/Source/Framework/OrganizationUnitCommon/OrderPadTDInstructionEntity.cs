﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class OrderPadTDInstructionEntity : OrderInstructionEntity
    {
        public string CustomerName { get; set; }
        public string AccountNumber { get; set; }
        public string BSB { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Units { get; set; }
        public Guid BrokerID { get; set; }
        public Guid BankID { get; set; }
        public string ClientID { get; set; }
        public decimal? Min { get; set; }
        public decimal? Max { get; set; }
        public string Rating { get; set; }
        public string Duration { get; set; }
        public decimal? Percentage { get; set; }
        public string Reference_Narration {get;set;}
        public Guid TDAccountCID { get; set; }
        
      
    }
}
