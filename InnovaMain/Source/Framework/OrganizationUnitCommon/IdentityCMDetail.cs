﻿using System;
using System.Collections.Generic;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class IdentityCMDetail : IdentityCM, IIdentityCMDetail
    {
        public TitleEntity BusinessTitle { get; set; }
        public bool IsPrimary { get; set; }
        public Guid FundID { get; set; }
        public ReinvestmentOption ReinvestmentOption { get; set; }

        public IdentityCMDetail()
        {
            BusinessTitle = new TitleEntity();
            IsPrimary = false;
            FundID = new Guid();
        }
        public static bool operator ==(IdentityCMDetail cm1, IdentityCMDetail cm2)
        {
            object o1 = (object)cm1;
            object o2 = (object)cm2;
            if (o1 == null && o2 == null) return true;
            if (o1 == null || o2 == null) return false;
            return ((cm1.Clid == cm2.Clid) && (cm1.Csid == cm2.Csid)  && (cm1.FundID == cm2.FundID));
        }
        public static bool operator !=(IdentityCMDetail cm1, IdentityCMDetail cm2)
        {
            return !(cm1 == cm2);
        }

        public override bool Equals(Object obj)
        {
            return Equals(obj as IdentityCMDetail);
        }

        public bool Equals(IdentityCMDetail s)
        {
            if ((object)s == null) { return false; }
            return this == s;
        }

        public override int GetHashCode()
        {
            int result = Clid.GetHashCode();
            result = 29 * result + Csid.GetHashCode();
            return result;
        }

        public new static IdentityCMDetail Null = new IdentityCMDetail { Clid = Guid.Empty, Csid = Guid.Empty, FundID = Guid.Empty };

        public static IdentityCMDetail FromIdetityCM(IdentityCM identityCm)
        {
            return new IdentityCMDetail { Cid = identityCm.Cid, Csid = identityCm.Csid, Clid = identityCm.Clid };
        }
    }

}
