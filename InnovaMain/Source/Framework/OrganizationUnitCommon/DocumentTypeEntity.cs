﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class DocumentTypeEntityList : Dictionary<string, DocumentTypeEntity>
    {
    }

   [Serializable]
   public class DocumentTypeEntity
    {
        public string Type { get; set; }
        public Guid AttachementID { get; set; }
        public string FullName { get; set; }
        public DateTime? DOB { get; set; }
        public string DocNumber { get; set; }
        public bool IsPhotoShown { get; set; }
        public bool IsAddressMatch { get; set; }
        public DateTime? IssueDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string IssuePlace { get; set; }
        public string PlaceResidence { get; set; }
        public double Points { get; set; }
        public string ReferenceNumber { get; set; }
        public string InfoProvidedByOne { get; set; }
        public string TitleRankDesignation { get; set; }
        public string InfoProvidedByTwo { get; set; }
        public string OrganizationName { get; set; }
        public AddressEntity OrganizationAddress { get; set; }
        public PhoneNumberEntity OrganizationPhone { get; set; }
        public bool BillLetterSighted { get; set; }
        public DateTime? TelephoneContactDate { get; set; }
        private bool isEnabled;
        [NonSerialized]
        public string FileDownLoadURL = string.Empty;
        public bool Original { get; set; }
        public bool CertifiedCopy { get; set; }
        public bool AccreditedEnglishTranslationNA { get; set; }
        public bool AccreditedEnglishTranslationSighted { get; set; }

        public bool IsEnabled
        {
            get { return isEnabled; }
            set { isEnabled = value; }
        }

       public string GetAddressString()
       {
           StringBuilder addressBuilder = new StringBuilder();

           addressBuilder.Append(OrganizationAddress.Addressline1);
           if (OrganizationAddress.Addressline2 != String.Empty)
           {
               addressBuilder.Append(",");
               addressBuilder.Append(OrganizationAddress.Addressline2);
           }
           
           addressBuilder.Append(",");
           addressBuilder.Append(OrganizationAddress.Suburb);
           addressBuilder.Append(",");
           addressBuilder.Append(OrganizationAddress.State);
           addressBuilder.Append(",");
           addressBuilder.Append(OrganizationAddress.Country);
           addressBuilder.Append(",");
           addressBuilder.Append(OrganizationAddress.PostCode);

           return addressBuilder.ToString();
       }
    }
}
