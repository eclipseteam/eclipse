﻿using System;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public abstract class BulkOrderInstructionEntity
    {
        public Guid ID { get; set; }
        public Guid BulkOrderID { get; set; }
        public long BulkOrderNo { get; set; }
        public InstructionStatus Status { get; set; }
        public InstructionType Type { get; set; }
        public DateTime TradeDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
