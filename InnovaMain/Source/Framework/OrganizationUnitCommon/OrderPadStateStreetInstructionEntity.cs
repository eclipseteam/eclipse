﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class OrderPadStateStreetInstructionEntity : OrderInstructionEntity
    {
        public string UnitholderCode { get; set; }
        public string UnitHolderSubCode { get; set; }
        public string FundCode { get; set; }
        public string MCHCode { get; set; }        
        public DateTime ClearDate { get; set; }
        public decimal? Amount { get; set; }
        public string RedumptionOrExchangeOption {get;set;}
        public string Transactiontype { get; set; }
        public decimal? NumberOfShares { get; set; }
        public string ExchangeToFund { get; set; }
        public string DealerCommissionRate { get; set; }
        public string CurrencyOfTrade { get; set; }
        public string ExternalID { get; set; }
        public string PurchaseSource { get; set; }
        public string RedumptionSource { get; set; }
        public string OperatorEnteredBy { get; set; }
        public string ProcessAtNAV { get; set; }
        public string Generator { get; set; }
        public DateTime? EffectuveDate { get; set; }
        public string AUDEquivalentAmount { get; set; }
        public int FileBatchNo { get; set; }
        public long FileNameSequanceNumber { get; set; }
    }
}
