﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
    {
    [Serializable]
  public class ASXUnSettledOrderEntity
      {
      public Guid ID { get; set; }
      public string InvestmentCode { get; set; }
      public decimal UnsettledOrders { get; set; }
      public ASXUnSettledOrderEntity()
          {
          ID = Guid.NewGuid();
          InvestmentCode = string.Empty;
          UnsettledOrders = 0;




          }
        }
    }
