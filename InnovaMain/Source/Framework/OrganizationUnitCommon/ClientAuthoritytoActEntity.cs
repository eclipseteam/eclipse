﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class ClientAuthoritytoActEntity
    {
        #region Authority to Act

        public bool EclipseOnline  { get; set; }
        public bool AustralianMoneyMarket  { get; set; }
        public bool MacquarieCMA  { get; set; }

        public bool InvestorStatusWholesaleInvestor  { get; set; }
        public bool InvestorStatusSophisticatedInvestor  { get; set; }
        public bool InvestorStatusProfessionalInvestor  { get; set; }
        public bool InvestorStatusRetailInvestor  { get; set; }

        public bool CertificateFromAccountantIsAttached  { get; set; }

        #endregion


       public ClientAuthoritytoActEntity()
        {
            EclipseOnline = false;
            AustralianMoneyMarket = false;
            MacquarieCMA = false;

            InvestorStatusWholesaleInvestor = false;
            InvestorStatusSophisticatedInvestor = false;
            InvestorStatusProfessionalInvestor = false;
            InvestorStatusRetailInvestor = false;

            CertificateFromAccountantIsAttached = false;
           
        }
    }
}
