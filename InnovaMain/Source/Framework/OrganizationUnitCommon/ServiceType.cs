﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class ServiceType
    {
        public bool DO_IT_YOURSELF { get; set; }
        public bool DO_IT_WITH_ME { get; set; }
        public bool DO_IT_FOR_ME { get; set; }

        public DoItForMe DoItForMe { get; set; }
        public DoItWithMe DoItWithMe { get; set; }
        public DoItYourSelf DoItYourSelf { get; set; }

        public ServiceType()
        {
            DoItForMe = new DoItForMe();
            DoItWithMe = new DoItWithMe();
            DoItYourSelf = new DoItYourSelf();
        }
    }
}
