﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common
{
    public enum TransectionStatus
    {
        _ = 0,
        Matched = 5,
        Loaded = 10
    };
    [Serializable]
    public class CashManagementEntity
    {
        [NonSerialized]
        public string BGLAccountName = string.Empty;
        [NonSerialized]
        public string BGLAccountNo = string.Empty;
        [NonSerialized]
        public string BGLCode = string.Empty;
        [NonSerialized]
        public string BGLCodeCash = string.Empty;
        [NonSerialized]
        public string BGLCodeSuspense = string.Empty;

        public Guid ID { get; set; }
        public string InvestmentCode { get; set; }
        public string Status { get; set; }
        public string Currency { get; set; }
        public string InvestmentName { get; set; }
        public string ImportTransactionType { get; set; }
        public string ImportTransactionTypeDetail { get; set; }
        public string SystemTransactionType { get; set; }
        public string Category { get; set; }
        public string AdministrationSystem { get; set; }
        public string ExternalReferenceID { get; set; }
        public DateTime TransactionDate { get; set; }
        public string AccountName { get; set; }
        public decimal Amount { get; set; }
        public decimal Balance { get; set; }
        public decimal Adjustment { get; set; }
        public TransectionStatus DividendStatus { get; set; }

        [NonSerialized]
        public int Rank;

        public decimal TotalAmount
        {
            get
            {
                return
                    Amount + Adjustment;
            }
        }
        public string Comment { get; set; }
        public Guid DividendID { get; set; }
        public string ContractNote { get; set; }
        public DateTime MaturityDate { get; set; }
        public decimal InterestRate { get; set; }
        public Guid InstitutionID { get; set; }
        public bool IsContractNote { get; set; }
        public ServiceTypes ServiceType { get; set; }
        public Guid? Product { get; set; }
        public string TransactionType { get; set; }
        public Guid? OrderID { get; set; }
        public Guid? OrderItemID { get; set; }
        public bool IsConfirmed { get; set; }

        public CashManagementEntity()
        {
            ID = Guid.Empty;
            InvestmentCode = string.Empty;
            Status = string.Empty;
            Currency = string.Empty;
            InvestmentName = string.Empty;
            ImportTransactionType = string.Empty;
            ImportTransactionTypeDetail = string.Empty;
            SystemTransactionType = string.Empty;
            Category = string.Empty;
            AdministrationSystem = string.Empty;
            ExternalReferenceID = string.Empty;
            TransactionDate = DateTime.Now;
            AccountName = string.Empty;
            Amount = 0;
            Adjustment = 0;
          
            Comment = string.Empty;
            DividendID = Guid.Empty;
            ContractNote = string.Empty;
            IsConfirmed = false;
            MaturityDate = DateTime.Now;
            InterestRate = 0;
            DividendStatus = TransectionStatus.Loaded;
        }

        public  bool IsDividendMatchingTrans(DividendEntity dv, decimal dvunits)
        {
            // Normally paidDividend == dv.CentsPerShare * dvunits, but it can be adjusted.
            decimal paidDividend = 0;
            if (dv.PaidDividend == 0)
                paidDividend = (decimal)dv.CentsPerShare * dvunits;
            else
                paidDividend = (decimal)dv.PaidDividend;

            return Math.Abs(TotalAmount - paidDividend) < 1 && TransactionDate == dv.PaymentDate && SystemTransactionType == "Dividend";
        }

        public void CopyImportantPropertiesFromOldTransaction(CashManagementEntity oldTransaction)
        {
            ID = oldTransaction.ID;
            DividendID = oldTransaction.DividendID;
            DividendStatus = oldTransaction.DividendStatus;
        }

    }
}
