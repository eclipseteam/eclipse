﻿using System;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class OrderPadMacquarieInstructionEntity : OrderInstructionEntity
    {
        public string ClientAccountName { get; set; }
        public string ClientAccountNumber { get; set; }
        public decimal? DebitAmount { get; set; }
        public string Narration { get; set; }
        public string ToAccountName { get; set; }
        public string ToAccountBSB { get; set; }
        public string ToAccountNumber { get; set; }
        public string ReceiverDescription { get; set; }
        public MacquarieInstructionType MACInstructionType { get; set; }
    }
}
