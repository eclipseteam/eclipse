﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class OrganizationUnit
    {
        public Guid Csid { get; set; }
        public Guid Clid { get; set; }
        public String Name { get; set; }
        public string Type { get; set; }
        public string Data { get; set; }
    }

    [Serializable]
    public class SPLibraryDocument
    {
        public UserEntity CurrentUser { get; set; }
        public string Name { get; set; }

        public SPLibraryDocument()
        {
            CurrentUser = new UserEntity();
            Name = string.Empty;
        }
    }
}
