﻿using System;
using System.Collections.Generic;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class AccountanttoAdvisor : IIdentityCM
    {
        public Guid Clid { get; set; }
        public Guid Csid { get; set; }
        public PhoneNumberEntity WorkPhoneNumber { get; set; }
        public PhoneNumberEntity FaxNumber { get; set; }
        public DualAddressEntity Address { get; set; }
    }
}
