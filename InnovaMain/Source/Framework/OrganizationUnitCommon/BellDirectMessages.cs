﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
 public   struct ImportMessageStatus
    {
        public const string Sucess = "Successfully Parsed";
        public const string Failed = "Error in Parsing";
        public const string SuccessFullyAdded = "Successfully Uploaded";
        public const string AlreayExists="Already Exists";
        public const string AccountNotFound = "Account Not Found";
        public const string SuccessFullyDeleted = "Successfully Deleted";
    }
    [Serializable]
    public class BellDirectMessages : ImportMessages
    {
        public string InvestmentCode { get; set; }
        public string TransactionID { get; set; }
        public string TransactionDate { get; set; }
        public string AccountNumber { get; set; }

        public BellDirectMessages()
        {
            InvestmentCode = string.Empty;
            TransactionID = string.Empty;
            TransactionDate = string.Empty;
            AccountNumber = string.Empty;
            Message = string.Empty;
            Status = string.Empty;
        }
    }


    [Serializable]
    public class ASXPriceMessage : ImportMessages
    {
        public string InvestmentCode { get; set; }
        public string CompanyName { get; set; }
        public string Price { get; set; }
        public string PriceDate { get; set; }
        public string Currency { get; set; }
        public ASXPriceMessage()
        {
            InvestmentCode = string.Empty;
            CompanyName = string.Empty;
            Price = string.Empty;
            PriceDate = string.Empty;
           
            Message = string.Empty;
            Status = string.Empty;
            Currency = string.Empty;
        }
    }

        [Serializable]
    public class BankAccountOrdersMessage : ImportMessages
    {
      
        public string Account { get; set; }
        public string Amount { get; set; }
        public string ReferenceNarration { get; set; }
        public string DateRequired { get; set; }

        public BankAccountOrdersMessage()
        {
            
            Account = string.Empty;
            Amount = string.Empty;
            ReferenceNarration = string.Empty;
            DateRequired = string.Empty;
           
            Message = string.Empty;
            Status = string.Empty;
          
        }
    }
     [Serializable]
      public class ASXPaidOrderMessage : ImportMessages
        {

       public string  ParentAcc { get; set; }
        public string BookedACC { get; set; }
        public string BuySale { get; set; }
        public string QTY { get; set; }
        public string Stock { get; set; }
        public string PriceAVG { get; set; }
        public string TradeDate { get; set; }
        public string ClientAccountName { get; set; }




        public ASXPaidOrderMessage()
            {
            ParentAcc = string.Empty;
            BookedACC = string.Empty;
            BuySale = string.Empty;
            QTY = string.Empty;



            Stock = string.Empty;
            PriceAVG = string.Empty;
            TradeDate = string.Empty;
            ClientAccountName = string.Empty;

          


            Message = string.Empty;
            Status = string.Empty;
            }
        }

    [Serializable]
    public class BankAccountTransactionMessage : ImportMessages
    {
        public string BSBNAccountNumber { get; set; }
        public string Type { get; set; }
        public string InputMode { get; set; }
        public string BankwestReferenceNumber { get; set; }
        public string Date { get; set; }
        public string Amount { get; set; }
        public string TransactionDescription { get; set; }
        public string TransactionType { get; set; }
        public string UnsettledOrder { get; set; }


        public BankAccountTransactionMessage()
        {
            Type = string.Empty;
            InputMode = string.Empty;
            BankwestReferenceNumber = string.Empty;
            Date = string.Empty;
            Amount = string.Empty;
            TransactionDescription = string.Empty;
            TransactionType = string.Empty;
            UnsettledOrder = string.Empty;


            Message = string.Empty;
            Status = string.Empty;
          
        }
    }


     [Serializable]
    public class BankAccountImportMessage : ImportMessages
    {

         public string ClientID { get; set; }
         public string AccountNumber { get; set; }
         public string AccountName { get; set; }
         public string Type { get; set; }
        public string OrganizationStatus { get; set; }
        public string BSB { get; set; }
        public string BCH { get; set; }
       

        public BankAccountImportMessage()
        {
            ClientID = string.Empty;
            Type = string.Empty;
            AccountName = string.Empty;
            OrganizationStatus = string.Empty;
            BSB = string.Empty;
            BCH = string.Empty;
            AccountNumber = string.Empty;
           


            Message = string.Empty;
            Status = string.Empty;
          
        }
    }





     


    [Serializable]
    public class StatestreetPriceMessage : ImportMessages
    {
        public string InvestmentCode { get; set; }
        public string CompanyName { get; set; }
        public string PriceDate { get; set; }
        public string Price_NAV { get; set; }
            public string Price_PUR { get; set; }
            public string Price_RDM { get; set; }
            public string Currency { get; set; }
            public StatestreetPriceMessage()
        {
            InvestmentCode = string.Empty;
            CompanyName = string.Empty;
          
            PriceDate = string.Empty;

            Message = string.Empty;
            Status = string.Empty;

            Price_NAV = string.Empty;
            Price_PUR = string.Empty;

            Price_RDM = string.Empty;
            Currency = string.Empty;


        }
    }
      [Serializable]
    public class TDPriceListMessage : ImportMessages
        {

        public string Provider { get; set; }
        public string ProviderType { get; set; }
        public string SPLongTermRating { get; set; }
        public string Min { get; set; }
        public string Max { get; set; }
        public string MaximumBrokeage { get; set; }
        public string Days30 { get; set; }
        public string Days60 { get; set; }
        public string Days90 { get; set; }
        public string Days120 { get; set; }
        public string Days150 { get; set; }
        public string Days180 { get; set; }
        public string Days270 { get; set; }
        public string Years1 { get; set; }
        public string Years2 { get; set; }
        public string Years3 { get; set; }
        public string Years4 { get; set; }
        public string Years5 { get; set; }
        public string Date { get; set; }




        public TDPriceListMessage()
            {
            Provider = string.Empty;
            ProviderType = string.Empty;
            SPLongTermRating = string.Empty;
            Min = string.Empty;
            Max = string.Empty;
            MaximumBrokeage = string.Empty;
            Days30 = string.Empty;
            Days60 = string.Empty;
            Days90 = string.Empty;
            Days120 = string.Empty;
            Days150 = string.Empty;
            Days180 = string.Empty;
            Days270 = string.Empty;
            Years1 = string.Empty;
            Years2 = string.Empty;
            Years3 = string.Empty;
            Years4 = string.Empty;
            Years5 = string.Empty;


            Message = string.Empty;
            Status = string.Empty;
            }

       
        }


    [Serializable]
    public class ProductSecuirtyPriceMessage : ImportMessages
    {
        public string StockCode { get; set; }
        public string Market { get; set; }
        public string Currency { get; set; }
        public string Recommendation { get; set; }
        public string Weighting { get; set; }
        public string Comment { get; set; }
        public string BuyPrice { get; set; }
        public string SellPrice { get; set; }
        public string DynamicRatingOption { get; set; }
        public ProductSecuirtyPriceMessage()
        {
            StockCode = string.Empty;
            Market = string.Empty;

            Currency = string.Empty;

            Message = string.Empty;
            Status = string.Empty;

            Recommendation = string.Empty;
            Weighting = string.Empty;

            Comment = string.Empty;
            BuyPrice = string.Empty;
            SellPrice = string.Empty;
            DynamicRatingOption = string.Empty;

        }
    }

    

    [Serializable]
    public class MISFundTransactionMessage : ImportMessages
    {
        public string FundCode { get; set; }
        public string FundName { get; set; }
       
        public string TradeDate { get; set; }
         public string Account { get; set; }
          
        
        
        public string SubAccount { get; set; }
        public string TransactionType { get; set; }
       
        public string Shares { get; set; }
         public string UnitPrice { get; set; }

                public string Amount { get; set; }
         public string InvestorCategory { get; set; }





         public MISFundTransactionMessage()
        {
            FundCode = string.Empty;
            FundName = string.Empty;
            Account = string.Empty;
            TradeDate = string.Empty;


            
            SubAccount = string.Empty;
            TransactionType = string.Empty;
            Shares = string.Empty;
            UnitPrice = string.Empty;
            
            Amount = string.Empty;
            InvestorCategory = string.Empty;
          




            Message = string.Empty;
            Status = string.Empty;
        }
    }











    [Serializable]
    public class MISPaidOrderMessage : ImportMessages
        {
      
        public string UnitholderCode { get; set; }
         public string UnitholderSubCode { get; set; }
        
          public string FundCode { get; set; }
        public string MCHCode { get; set; }
        public string TradeDate { get; set; }



        public string ClearDate { get; set; }
        public string Amount { get; set; }

        public string TransactionType { get; set; }
        public string CurrencyOfTrade { get; set; }

        public string PurchaseSource { get; set; }
        public string ProcessAtNAV { get; set; }
        public string RedemptionOrExchangeOptions { get; set; }
        public string NumberofShares { get; set; }
        public string ExchangeToFund { get; set; }
        public string DealerCommissionRate { get; set; }
        public string ExternalID { get; set; }
        public string RedemptionSource { get; set; }
        public string OperatorEnteredBy { get; set; }
        public string Generator { get; set; }
        public string EffectiveDate { get; set; }
        public string AUDEquivalentAmount { get; set; }
        

        public MISPaidOrderMessage()
            {
            UnitholderCode = string.Empty;
            UnitholderSubCode = string.Empty;
            FundCode = string.Empty;
            MCHCode = string.Empty;



            TradeDate = string.Empty;
            ClearDate = string.Empty;
            Amount = string.Empty;
            TransactionType = string.Empty;

            CurrencyOfTrade = string.Empty;
            PurchaseSource = string.Empty;

            ProcessAtNAV = string.Empty;



            RedemptionOrExchangeOptions = string.Empty;
            NumberofShares = string.Empty;
            ExchangeToFund = string.Empty;
            DealerCommissionRate = string.Empty;
            ExternalID = string.Empty;
            RedemptionSource = string.Empty;
            OperatorEnteredBy = string.Empty;
            Generator = string.Empty;
            EffectiveDate = string.Empty;
            AUDEquivalentAmount = string.Empty;

            Message = string.Empty;
            Status = string.Empty;
            }
        }
    [Serializable]
    public class UnitHolderBalanceTransactionMessage : ImportMessages
    {
        public string Account { get; set; }
        public string SubAccount { get; set; }
        public string FundCode { get; set; }
        public string FundName { get; set; }
        public string TotalShares { get; set; }
        public string RedemptionPrice { get; set; }
        public string CurrentValue { get; set; }
        public DateTime AsofDate { get; set; }
        public string InvestorCategory { get; set; }

        public UnitHolderBalanceTransactionMessage()
        {
        Account=string.Empty;
        SubAccount =string.Empty;
        FundCode =string.Empty;
        FundName =string.Empty;
        TotalShares =string.Empty;
        RedemptionPrice =string.Empty;
        CurrentValue =string.Empty;
        AsofDate = DateTime.Now;
        InvestorCategory = string.Empty;
        }
    }

}
