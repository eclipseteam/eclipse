﻿using System;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class OrderPadTDBulkInstructionEntity : BulkOrderInstructionEntity
    {
        public Guid BrokerID { get; set; }
        public Guid BankID { get; set; }
        public decimal? Amount { get; set; }
        public string Term { get; set; }
        public decimal? Rate { get; set; }
    }
}
