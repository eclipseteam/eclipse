﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class StateStreetAccountEntity
    {
        public string Name { get; set; }
        public string AccountNumber { get; set; }
        public string OrganizationStatus { get; set; }
    }
}
