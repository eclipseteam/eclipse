﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Common;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.CM
{
    public interface IClientUMADataCorpTrust
    {
        string InvestorType { get; set; }
        string EntityName { get; set; }
        string TrusteeType { get; set; }
        string CorporateTrusteeName { get; set; }
        string TrustPassword { get; set; }
        string Country { get; set; }
        string ACN { get; set; }
        string ABN { get; set; }
        string TrustABN { get; set; }
        string AdvisorCode { get; set; }
        ServiceType Servicetype { get; set; }
        string InvestmentDetails { get; set; }
        ExemptionCategory ExemptionCategory { get; set; }
        AccessFacilities AccessFacilities { get; set; }
        OperationManner OperationManner { get; set; }
        IdentityCM Parent { get; set; }
        BTWrapEntity BTWrap { get; set; }
        bool IsCharity { get; set; }
        string PrincipalTrustActivity { get; set; }
        bool IsSMSFTrust { get; set; }
        bool IsOtherTrust { get; set; }
    }
}
