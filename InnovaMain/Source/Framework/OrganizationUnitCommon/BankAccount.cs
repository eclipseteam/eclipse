﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using Oritax.TaxSimp.CM;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class BankAccountEntity: IClientEntity
    {
        private Guid parentCID = Guid.Empty;
        private String bglCodeCashAccount = String.Empty;
        private String bglCodeSuspense = String.Empty;

        public Guid ParentCID
        {
            get { return parentCID; }
            set { parentCID = value; }
        }
        public string BGLCode { get; set; }
        public string BGLCodeCommissionRebate { get; set; }
        public string BGLCodeCashAccount { get { return bglCodeCashAccount; } set { bglCodeCashAccount = value;  } }
        public string BGLCodeSuspense { get { return bglCodeSuspense; } set { bglCodeSuspense = value; } }
        public string Name { get; set; }
        public string AccoutType { get; set; }
        public string AccountNumber { get; set; }
        public string CustomerNo { get; set; }
        public string BSB { get; set; }
        public string OrganizationStatus { get; set; }
        public Guid InstitutionID { get; set; }
        public TermDepositAccountEntity BrokerID { get; set; }
        public List<AccountRelation> LinkedAccounts { get; set; }
        public ObservableCollection<CashManagementEntity> CashManagementTransactions { get; set; }
        public List<BankAccountPaidOrderEntity> PaidOrders { get; set; }
        public decimal Holding { get; set; }
        public bool IsExternalAccount { get; set; }
        public decimal UnsettledOrder { get; set; }
        public DateTime UploadTime { get; set; }
        public decimal Total { get { return Holding + UnsettledOrder; } }
        public ObservableCollection<CashManagementEntity> DeletedCashManagementTransactions { get; set; }
        public decimal TransactionHolding
        {
            get
            {
                return CashManagementTransactions != null ? CashManagementTransactions.Sum(tt => tt.TotalAmount) : 0;
            }
        }

        public BankAccountEntity()
        {
            LinkedAccounts = new List<AccountRelation>();
            InstitutionID = Guid.Empty;
            BrokerID = new TermDepositAccountEntity();
            CashManagementTransactions = new ObservableCollection<CashManagementEntity>();
            PaidOrders = new List<BankAccountPaidOrderEntity>();
            DeletedCashManagementTransactions = new ObservableCollection<CashManagementEntity>();
        }
    }
}
