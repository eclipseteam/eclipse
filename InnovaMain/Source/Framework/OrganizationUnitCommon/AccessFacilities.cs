﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class AccessFacilities
    {
        public bool PHONEACCESS { get; set; }
        public bool ONLINEACCESS { get; set; }
        public bool DEBITCARD { get; set; }
        public bool CHEQUEBOOK { get; set; }
        public bool DEPOSITBOOK { get; set; }
    }
}
