﻿using System;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class OrderPadASXBulkInstructionEntity : BulkOrderInstructionEntity
    {
        public string InvestmentCode { get; set; }
        public string InvestmentName { get; set; }
        public string MarketCode { get; set; }
        public string CurrencyCode { get; set; }
        public string OrderType { get; set; }
        public decimal? RebalanceUnitPrice { get; set; }
        public decimal? SuggestedOrderUnits { get; set; }
        public decimal? SuggestedOrderAmount { get; set; }
        public OrderPreferedBy OrderPreferedBy { get; set; }
    }
}
