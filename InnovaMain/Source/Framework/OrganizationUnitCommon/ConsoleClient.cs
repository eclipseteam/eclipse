﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CM;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class ConsoleClient : IClientEntity
    {
        public Guid Clid { get; set; }
        public Guid Csid { get; set; }
        public string Name { get; set; }
        public String LegalName { get; set; }
        public string AccountDesignation { get; set; }
        public List<IdentityCM> LinkedClients { get; set; }
        public bool IsActive { get; set; }
        public string OrganizationStatus { get; set; }
        public String ClientId { get; set; }
        public DualAddressEntity Address { get; set; }
        public IdentityCM Parent { get; set; }
        public bool IsClient { get; set; }
        
        public int LinkedClientsCount
        {
            set { }
            get
            {
                int count = 0;
                if (LinkedClients != null)
                {
                    count = LinkedClients.Count;
                }
                return count;
            }

        }
        
        public ConsoleClient()
        {
            LinkedClients = new List<IdentityCM>();
            Address = new DualAddressEntity();
            Parent = IdentityCM.Null;
            IsClient = false;
            LegalName = string.Empty;
            AccountDesignation = string.Empty;
        }
    }

}
