﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.Common.Data
{
    public class FeeRunDetailsDS : UMABaseDS
    {
        public const string FEERUNSDETAILSTABLE = "FEERUNSDETAILSTABLE";
        public const string ID = "ID";
        public const string FEERUNDATE = "FEERUNDATE";
        public const string SHORTNAME = "SHORTNAME";
        public const string DESCRIPTION = "DESCRIPTION";
        public const string YEAR = "YEAR";
        public const string MONTH = "MONTH";
        public const string MONTHINT = "MONTHINT";
        public const string RUNTYPE = "RUNTYPE";
        public const string RUNTYPEENUM = "RUNTYPEENUM";
        public const string TOTALFEES = "TOTALFEES";

        public FeeRunType FeeRunType = FeeRunType.Global; 

        public FeeRunDetailsDS()
        {
            DataTable dt = new DataTable(FEERUNSDETAILSTABLE);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(YEAR, typeof(int));
            dt.Columns.Add(MONTH, typeof(String));
            dt.Columns.Add(MONTHINT, typeof(int));
            dt.Columns.Add(SHORTNAME, typeof(String));
            dt.Columns.Add(RUNTYPE, typeof(String));
            dt.Columns.Add(RUNTYPEENUM, typeof(FeeRunType));
            dt.Columns.Add(FEERUNDATE, typeof(DateTime));
            dt.Columns.Add(TOTALFEES, typeof(decimal));
            this.Tables.Add(dt);
        }
    }
}
