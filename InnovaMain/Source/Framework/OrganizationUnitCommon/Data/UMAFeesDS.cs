﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common.Data
{
    public class UMAFeesDS : UMABaseDS
    {
        public List<FeeEntity> FeeEnttyList = new List<FeeEntity>();
        public FeeEntity FeeEntityToAddUpdate = new FeeEntity();

        public const string FEELISTABLE = "FEELISTABLE";
        public const string ID = "ID";
        public const string FEETYPE = "FEETYPE";
        public const string FEETYPEENUM = "FEETYPEENUM";
        public const string COMMENTS = "COMMENTS";
        public const string DESCRIPTION = "DESCRIPTION";
        public const string YEAR = "YEAR";
        public const string STARTDATE = "STARTDATE";
        public const string ENDDATE = "ENDDATE";
        public const string SERVICESTYPEENUM = "SERVICESTYPEENUM";
        public const string SERVICESTYPE = "SERVICESTYPE";

        public UMAFeesDS()
        {
            DataTable dt = new DataTable(FEELISTABLE);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(FEETYPE, typeof(string));
            dt.Columns.Add(FEETYPEENUM, typeof(FeeType));
            dt.Columns.Add(COMMENTS, typeof(string));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(YEAR, typeof(int));
            dt.Columns.Add(STARTDATE, typeof(DateTime));
            dt.Columns.Add(ENDDATE, typeof(DateTime));
            dt.Columns.Add(SERVICESTYPEENUM, typeof(ServiceTypes));
            dt.Columns.Add(SERVICESTYPE, typeof(string));
            this.Tables.Add(dt); 
        }
    }
}
