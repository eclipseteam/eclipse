﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.Common.Data
{
    public class UMAFeesDetailsDS : UMABaseDS
    {
        public FeeEntity FeeEntity = new FeeEntity();
        public Guid FeeEntityID = Guid.Empty;
        public Guid ProductID = Guid.Empty;
      
        public FeeOperationType FeeOperationType = FeeOperationType.None; 

        public const string PRODUCTSTABLENAME = "PRODUCTSTABLENAME";
        public const string PROID = "PROID";
        public const string PRONAME = "PRONAME";

        public UMAFeesDetailsDS()
        {
            DataTable dt = new DataTable(PRODUCTSTABLENAME);
            dt.Columns.Add(PROID, typeof(Guid));
            dt.Columns.Add(PRONAME, typeof(string));
            this.Tables.Add(dt); 
        }
    }
}
