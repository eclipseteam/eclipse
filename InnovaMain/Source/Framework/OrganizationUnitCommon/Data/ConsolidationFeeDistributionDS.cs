﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.Common.Data
{
    public class ConsolidationFeeDistributionDS : UMABaseDS
    {
        public const string CONSOLIDATIONFEEDISTRIBUTIONTABLE = "CONSOLIDATIONFEEDISTRIBUTIONTABLE";
        public const string CLIENTCID = "CLIENTCID";
        public const string CLIENTID = "CLIENTID";
        public const string CLIENTNAME = "CLIENTNAME";
        public const string DISTRIBUTEDVALUE = "DISTRIBUTEDVALUE";

        public Guid OrgCID = Guid.Empty;
        public decimal DistributionPercentage = 0;

        public ConsolidationFeeDistributionDS()
        {
            DataTable dt = new DataTable(CONSOLIDATIONFEEDISTRIBUTIONTABLE);
            dt.Columns.Add(CLIENTCID, typeof(Guid));
            dt.Columns.Add(CLIENTID, typeof(string));
            dt.Columns.Add(CLIENTNAME, typeof(string));
            dt.Columns.Add(DISTRIBUTEDVALUE, typeof(decimal));
            this.Tables.Add(dt);
        }
    }
}
