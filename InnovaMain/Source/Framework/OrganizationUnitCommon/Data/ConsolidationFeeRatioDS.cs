﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.Common.Data
{
    public class FeeRatio
    {
        public DateTime HoldingDate = new DateTime();
        public Guid ClientCID = new Guid();
        public decimal Ratio = 0;
        public decimal Holding = 0;
        public decimal TotalHolding = 0; 
    }

    public class ConsolidationFeeRatioDS : UMABaseDS
    {
        public DateTime FeeRunMonth = new DateTime(); 
        public List<FeeTransaction> FeeTransactions = new List<FeeTransaction>(); 
        public List<FeeRatio> FeeRatios = new List<FeeRatio>();
        public Guid FeeRunID = Guid.Empty;

        public ConsolidationFeeRatioDS()
        {
         
        }

        public void CalculateFeeRatio()
        { 
            int totalDays = DateTime.DaysInMonth(FeeRunMonth.Year, FeeRunMonth.Month); 

            for(int i = 1;i <= totalDays; i++)
            {
                foreach(var feeTranItem in FeeTransactions)
                {
                    FeeRatio feeRatio = new FeeRatio(); 
                    decimal clientHolding = feeTranItem.FeeHoldingEntityList.Where(t => t.TransactionDate.Day == i && t.ServiceTypes == TaxSimp.Data.ServiceTypes.All).Sum(t => t.HoldingValue);

                    feeRatio.Holding = clientHolding;
                    feeRatio.HoldingDate = new DateTime(FeeRunMonth.Year, FeeRunMonth.Month, i);
                    feeRatio.ClientCID = feeTranItem.ClientCID;
                    FeeRatios.Add(feeRatio);
                }

                var ratios = FeeRatios.Where(t => t.HoldingDate.Day == i);
                
                decimal totalHolding = ratios.Sum(t => t.Holding);

                foreach (var ratio in ratios)
                {
                    ratio.TotalHolding = totalHolding;
                    ratio.Ratio = ratio.Holding / totalHolding;
                }
            }
        }
    }
}
