﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.Common.Data
{
    public class FeeTransactionsDS : UMABaseDS
    {
        public int Month = 1;
        public int Year = 2000;
        public FeeTransaction FeeTransaction = new FeeTransaction();

        public const string FEETRANSACTIONS = "FEETRANSACTIONS";
        public const string ID = "ID";
        public const string INS = "INS";
        public const string FEERUNID = "FEERUNID";
        public const string DESCRIPTION = "DESCRIPTION";
        public const string YEAR = "YEAR";
        public const string MONTH = "MONTH";
        public const string CALCULATEDFEES = "CALCULATEDFEES";
        public const string TRANSACTIONTYPE = "TRANSACTIONTYPE";
        public const string TRANSACTIONDATE = "TRANSACTIONDATE";

        public FeeTransactionsDS()
        {
            DataTable dt = new DataTable(FEETRANSACTIONS);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(INS, typeof(Guid));
            dt.Columns.Add(FEERUNID, typeof(Guid));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(YEAR, typeof(int));
            dt.Columns.Add(MONTH, typeof(String));
            dt.Columns.Add(TRANSACTIONDATE, typeof(DateTime));
            dt.Columns.Add(TRANSACTIONTYPE, typeof(string));
            dt.Columns.Add(CALCULATEDFEES, typeof(decimal));
            this.Tables.Add(dt);
        }
    }
}
