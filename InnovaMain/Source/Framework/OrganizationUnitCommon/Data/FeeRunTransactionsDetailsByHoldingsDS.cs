﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.Common.Data
{
    public class FeeRunTransactionsDetailsByHoldingsDS : UMABaseDS
    {
        public const string FEEHOLDINGTABLE = "FEEHOLDINGTABLE";
        public const string ID = "ID";
        public const string FEERUNID = "FEERUNID";
        public const string FEERUNDATE = "FEERUNDATE";
        public const string SHORTNAME = "SHORTNAME";
        public const string DESCRIPTION = "DESCRIPTION";
        public const string YEAR = "YEAR";
        public const string MONTH = "MONTH";
        public const string MONTHINT = "MONTHINT";
        public const string CLIENTID = "CLIENTID";
        public const string CLIENTCID = "CLIENTCID";
        public const string CLIENTNAME = "CLIENTNAME";
        public const string CLENTTYPE = "CLENTTYPE";
        public const string ADVISERID = "ADVISERID";
        public const string ADVISERCID = "ADVISERCID";
        public const string DEEALERGROUPNAME = "DEALERGROUPNAME";
        public const string IFANAME = "IFANAME";
        public const string ADVISERNAME = "ADVISERNAME";
        public const string TRANSACTIONDATE = "TRANSACTIONDATE";
        public const string TOTALFEES = "TOTALFEES";
        public const string PRODUCTID = "PRODUCTID";
        public const string PRODUCTNAME = "PRODUCTNAME";
        public const string SERVICESTYPE = "SERVICESTYPE";
        public const string TOTALFUM = "TOTALFUM";
        public FeeRunTransactionsDetailsByHoldingsDS()
        {
            DataTable dt = new DataTable(FEEHOLDINGTABLE);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(CLENTTYPE, typeof(String));
            dt.Columns.Add(CLIENTNAME, typeof(String));
            dt.Columns.Add(CLIENTID, typeof(String));
            dt.Columns.Add(CLIENTCID, typeof(Guid));
            dt.Columns.Add(DEEALERGROUPNAME, typeof(String));
            dt.Columns.Add(IFANAME, typeof(String));
            dt.Columns.Add(ADVISERID, typeof(String));
            dt.Columns.Add(ADVISERNAME, typeof(String));
            dt.Columns.Add(ADVISERCID, typeof(Guid));
            dt.Columns.Add(FEERUNID, typeof(Guid));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(YEAR, typeof(int));
            dt.Columns.Add(MONTH, typeof(String));
            dt.Columns.Add(MONTHINT, typeof(int));
            dt.Columns.Add(TRANSACTIONDATE, typeof(DateTime));
            dt.Columns.Add(SHORTNAME, typeof(String));
            dt.Columns.Add(FEERUNDATE, typeof(DateTime));
            dt.Columns.Add(TOTALFUM, typeof(decimal));
            dt.Columns.Add(TOTALFEES, typeof(decimal));
            dt.Columns.Add(PRODUCTID, typeof(Guid));
            dt.Columns.Add(PRODUCTNAME, typeof(String));
            dt.Columns.Add(SERVICESTYPE, typeof(String));
            this.Tables.Add(dt);
        }
    }
}
