﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.Common.Data
{
    public class FeeTransactionDetailsDS : UMABaseDS
    {
        public Guid AdviserCID = Guid.Empty;
        public string AdviserID = String.Empty;
        public string AdviserName = String.Empty;
        public bool UseConsolidationRatios = false;
        public List<FeeRatio> FeeRatios = new List<FeeRatio>();
        public string FeeRunName = string.Empty;
        public decimal ManAdjustment = 0;

        public int Month = 1;
        public int Year = 2000;
        public Guid FeeTransactionID = Guid.Empty;
        public Guid ClientID = Guid.Empty;
        public FeeTransaction FeeTransaction = new FeeTransaction();
        public const string ID = "ID";
        public const string INS = "INS";
        public const string FEETYPE = "FEETYPE";
        public const string FEETYPEENUM = "FEETYPEENUM";
        public const string COMMENTS = "COMMENTS";
        public const string DESCRIPTION = "DESCRIPTION";
        public const string YEAR = "YEAR";
        public const string MONTH = "MONTH";
        public const string CALCULATEDFEES = "CALCULATEDFEES";
        public const string AUTOADJUST = "AUTOADJUST";
        public const string ADJUST = "ADJUST";
        public const string FEETOTALADJUST = "FEETOTALADJUST";
        public const string FEETOTALMANADJUST = "FEETOTALMANADJUST";

        public const string CALCULATEDFEESSUMMARYBYFEETYPE = "CALCULATEDFEESSUMMARYBYFEETYPE";
        public const string FEESUBTYPE = "FEESUBTYPE";
        public const string FEETYPEDESC = "FEETYPEDESC";
        public const string FROMVALUE = "FROMVALUE";
        public const string TOVALUE = "TOVALUE";
        public const string FEEVALUE = "FEEVALUE";
        public const string FEEPERCENT = "FEEPERCENT";
        public const string FEETYPEID = "FEETYPEID";
        public const string ONGOINGVALUEBREAKDOWN = "ONGOINGVALUEBREAKDOWN";
        public const string ONGOINGPERCENTBREAKDOWN = "ONGOINGPERCENTBREAKDOWN";
        public const string TIERDFEEBREAKDOWN = "TIERDFEEBREAKDOWN";

        public FeeTransactionDetailsDS()
        {
            DataTable dt = new DataTable(CALCULATEDFEESSUMMARYBYFEETYPE);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(INS, typeof(Guid));
            dt.Columns.Add(FEETYPEID, typeof(Guid));
            dt.Columns.Add(FEETYPE, typeof(string));
            dt.Columns.Add(FEESUBTYPE, typeof(string));
            dt.Columns.Add(FEETYPEDESC, typeof(string));
            dt.Columns.Add(FEETYPEENUM, typeof(FeeType));
            dt.Columns.Add(COMMENTS, typeof(string));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(YEAR, typeof(int));
            dt.Columns.Add(MONTH, typeof(string));
            dt.Columns.Add(CALCULATEDFEES, typeof(decimal));
            dt.Columns.Add(AUTOADJUST, typeof(decimal));
            dt.Columns.Add(ADJUST, typeof(decimal));
            dt.Columns.Add(FEETOTALADJUST, typeof(decimal));
            dt.Columns.Add(FEETOTALMANADJUST, typeof(decimal));
            this.Tables.Add(dt);
        }
    }
}
