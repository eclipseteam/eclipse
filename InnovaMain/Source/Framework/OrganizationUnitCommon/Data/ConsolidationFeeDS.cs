﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.Common.Data
{
    public class ConsolidationFeeDS : UMABaseDS
    {
        public const string CONSOLIDATIONFEETABLE = "CONSOLIDATIONFEETABLE";
        public const string CLIENTCID = "CLIENTCID";
        public const string CLIENTID = "CLIENTID";
        public const string CLIENTNAME = "CLIENTNAME";
        public const string FEETRANID = "FEETRANID";
        public const string FEERUNID = "FEERUNID";
        public const string FEERUNNAME = "FEERUNNAME";
        public const string FEETYPE = "FEETYPE";
        public const string FEETYPEID = "FEETYPEID";
        public const string FEETOTAL = "FEETOTAL";
        public const string AUTOADJUST = "AUTOADJUST";
        public const string ADJUST = "ADJUST";
        public const string FEETOTALADJUST = "FEETOTALADJUST";
        public const string FEETOTALMANADJUST = "FEETOTALMANADJUST";
        public const string FEETOTALTOTAL = "FEETOTALTOTAL";

        public Guid FeeRunID = Guid.Empty;

        public List<FeeRatio> FeeRatios = new List<FeeRatio>();

        public Dictionary<Guid, decimal> Ratios = new Dictionary<Guid, decimal>(); 

        public ConsolidationFeeDS()
        {
            DataTable dt = new DataTable(CONSOLIDATIONFEETABLE);
            dt.Columns.Add(CLIENTCID, typeof(Guid));
            dt.Columns.Add(CLIENTID, typeof(string));
            dt.Columns.Add(CLIENTNAME, typeof(string));
            dt.Columns.Add(FEETRANID, typeof(Guid));
            dt.Columns.Add(FEERUNID, typeof(Guid));
            dt.Columns.Add(FEETYPEID, typeof(Guid));
            dt.Columns.Add(FEERUNNAME, typeof(String));
            dt.Columns.Add(FEETYPE, typeof(String));
            dt.Columns.Add(FEETOTAL, typeof(decimal));
            dt.Columns.Add(AUTOADJUST, typeof(decimal));
            dt.Columns.Add(ADJUST, typeof(decimal));
            dt.Columns.Add(FEETOTALADJUST, typeof(decimal));
            dt.Columns.Add(FEETOTALMANADJUST, typeof(decimal));
            dt.Columns.Add(FEETOTALTOTAL, typeof(decimal));
            this.Tables.Add(dt);
        }
    }
}
