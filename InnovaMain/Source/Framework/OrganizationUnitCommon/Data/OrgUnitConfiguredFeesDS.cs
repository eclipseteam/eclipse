﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.Common.Data
{
    public class OrgUnitConfiguredFeesDS : UMABaseDS
    {
        public const string FEESLIST = "FEESLIST";
        public const string CONFIGUREDFEES = "ConfiguredFees";
        public const string ID = "ID";
        public const string INS = "INS";
        public const string FEETYPE = "FEETYPE";
        public const string FEETYPEENUM = "FEETYPEENUM";
        public const string DESCRIPTION = "DESCRIPTION";
        public const string YEAR = "YEAR";
        public const string STARTDATE = "STARTDATE";
        public const string ENDDATE = "ENDDATE";
        public const string COMMENT = "COMMENT";

        public Guid FeeEntityToConfigure = Guid.Empty;

        public OrgUnitConfiguredFeesDS()
        {
            DataTable dt = new DataTable(CONFIGUREDFEES);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(INS, typeof(Guid));
            dt.Columns.Add(FEETYPE, typeof(string));
            dt.Columns.Add(FEETYPEENUM, typeof(FeeType));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(COMMENT, typeof(string));
            dt.Columns.Add(YEAR, typeof(int));
            dt.Columns.Add(STARTDATE, typeof(DateTime));
            dt.Columns.Add(ENDDATE, typeof(DateTime));
            this.Tables.Add(dt);

            dt = new DataTable(FEESLIST);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(FEETYPE, typeof(string));
            dt.Columns.Add(FEETYPEENUM, typeof(FeeType));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(COMMENT, typeof(string));
            dt.Columns.Add(YEAR, typeof(int));
            dt.Columns.Add(STARTDATE, typeof(DateTime));
            dt.Columns.Add(ENDDATE, typeof(DateTime));
            this.Tables.Add(dt); 
        }
    }
}
