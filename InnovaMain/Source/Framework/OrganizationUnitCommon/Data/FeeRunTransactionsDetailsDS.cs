﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.Common.Data
{
    public enum FeeRunTransactionsOperation
    { 
        None,
        AddFeeTemplate,
        DeleteFeeTemplate,
        GetOnlyConfiguredTemplate,
    }

    public class FeeRunTransactionsDetailsDS : UMABaseDS
    {
        public bool UseLocalFee = false;
        public const string TEMPLATELISTTABLE = "TEMPLATELISTTABLE";
        public const string FEERUNSDETAILSTRANSACTIONTABLE = "FEERUNSDETAILSTRANSACTIONTABLE";
        public const string ID = "ID";
        public const string FEERUNID = "FEERUNID";
        public const string FEERUNDATE = "FEERUNDATE";
        public const string SHORTNAME = "SHORTNAME";
        public const string DESCRIPTION = "DESCRIPTION";
        public const string YEAR = "YEAR";
        public const string MONTH = "MONTH";
        public const string MONTHINT = "MONTHINT";
        public const string CLIENTID = "CLIENTID";
        public const string CLIENTCID = "CLIENTCID";
        public const string CLIENTNAME = "CLIENTNAME";
        public const string CLENTTYPE = "CLENTTYPE";
        public const string ADVISERID = "ADVISERID";
        public const string ADVISERCID = "ADVISERCID";
        public const string ADVISERNAME = "ADVISERNAME";
        public const string TRANSACTIONDATE = "TRANSACTIONDATE";
        public const string TOTALFEES = "TOTALFEES";
        public string FeeRunDetails = string.Empty;
        public FeeRunTransactionsOperation FeeRunTransactionsOperation = FeeRunTransactionsOperation.None;
        public FeeRunType FeeRunType = FeeRunType.Global;
        public Guid FeeTemplateID = Guid.Empty;
        public List<Guid> ConfiguredFeesTemplateAtRun = new List<Guid>();
        public List<FeeType> FeeType = new List<FeeType>();
        public decimal ManAdjustment = 0;
        public FeeRunTransactionsDetailsDS()
        {
            DataTable dt = new DataTable(FEERUNSDETAILSTRANSACTIONTABLE);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(FEERUNID, typeof(Guid));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(YEAR, typeof(int));
            dt.Columns.Add(MONTH, typeof(String));
            dt.Columns.Add(MONTHINT, typeof(int));
            dt.Columns.Add(TRANSACTIONDATE, typeof(DateTime));
            dt.Columns.Add(CLENTTYPE, typeof(String));
            dt.Columns.Add(ADVISERNAME, typeof(String));
            dt.Columns.Add(CLIENTNAME, typeof(String));
            dt.Columns.Add(CLIENTID, typeof(String));
            dt.Columns.Add(CLIENTCID, typeof(Guid));
            dt.Columns.Add(ADVISERID, typeof(String));
            dt.Columns.Add(ADVISERCID, typeof(Guid));
            dt.Columns.Add(SHORTNAME, typeof(String));
            dt.Columns.Add(FEERUNDATE, typeof(DateTime));
            dt.Columns.Add(TOTALFEES, typeof(decimal));
            this.Tables.Add(dt);

            dt = new DataTable(TEMPLATELISTTABLE);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            this.Tables.Add(dt);
        }
    }
}
