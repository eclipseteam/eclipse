﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.Common.Data
{
    public class UMAEntityFeesDS : UMABaseDS
    {
        public int Month = 1;
        public int Year = 2000;
        public FeeTransaction FeeTransaction = new FeeTransaction();
        public Guid FeeRunID = Guid.Empty;
        public FeeRunType FeeRunType = FeeRunType.Global;
        public List<Guid> ConfiguredFeesTemplateAtRun = new List<Guid>();
        public List<FeeType> FeeType = new List<FeeType>();
        public bool UseLocalFee = false;
        public const string CALCULATEDFEESSUMMARY = "CALCULATEDFEESSUMMARY";
        public const string ID = "ID";
        public const string INS = "INS";
        public const string FEETYPE = "FEETYPE";
        public const string FEETYPEENUM = "FEETYPEENUM";
        public const string COMMENTS = "COMMENTS";
        public const string DESCRIPTION = "DESCRIPTION";
        public const string YEAR = "YEAR";
        public const string MONTH = "MONTH";
        public const string CALCULATEDFEES = "CALCULATEDFEES";

        public const string CALCULATEDFEESSUMMARYBYFEETYPE = "CALCULATEDFEESSUMMARYBYFEETYPE";
        public const string FEESUBTYPE = "FEESUBTYPE";
        public const string FROMVALUE = "FROMVALUE";
        public const string TOVALUE = "TOVALUE";
        public const string FEEVALUE = "FEEVALUE";
        public const string FEEPERCENT = "FEEPERCENT";
        public const string SERVICETYPE = "SERVICETYPE";

        public const string ONGOINGVALUEBREAKDOWN = "ONGOINGVALUEBREAKDOWN";
        public const string ONGOINGPERCENTBREAKDOWN = "ONGOINGPERCENTBREAKDOWN";
        public const string TIERDFEEBREAKDOWN = "TIERDFEEBREAKDOWN";

        public UMAEntityFeesDS()
        {
            DataTable dt = new DataTable(CALCULATEDFEESSUMMARY);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(INS, typeof(Guid));
            dt.Columns.Add(FEETYPE, typeof(string));
            dt.Columns.Add(FEETYPEENUM, typeof(FeeType));
            dt.Columns.Add(COMMENTS, typeof(string));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(SERVICETYPE, typeof(string));
            dt.Columns.Add(YEAR, typeof(int));
            dt.Columns.Add(MONTH, typeof(int));
            dt.Columns.Add(CALCULATEDFEES, typeof(decimal));
            this.Tables.Add(dt);

            dt = new DataTable(CALCULATEDFEESSUMMARYBYFEETYPE);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(INS, typeof(Guid));
            dt.Columns.Add(FEETYPE, typeof(string));
            dt.Columns.Add(FEESUBTYPE, typeof(string));
            dt.Columns.Add(FEETYPEENUM, typeof(FeeType));
            dt.Columns.Add(COMMENTS, typeof(string));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(SERVICETYPE, typeof(string));
            dt.Columns.Add(YEAR, typeof(int));
            dt.Columns.Add(MONTH, typeof(int));
            dt.Columns.Add(CALCULATEDFEES, typeof(decimal));
            this.Tables.Add(dt);

            dt = new DataTable(ONGOINGPERCENTBREAKDOWN);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(INS, typeof(Guid));
            dt.Columns.Add(FEEPERCENT, typeof(decimal));
            dt.Columns.Add(FEETYPE, typeof(string));
            dt.Columns.Add(FEESUBTYPE, typeof(string));
            dt.Columns.Add(FEETYPEENUM, typeof(FeeType));
            dt.Columns.Add(COMMENTS, typeof(string));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(SERVICETYPE, typeof(string));
            dt.Columns.Add(YEAR, typeof(int));
            dt.Columns.Add(MONTH, typeof(int));
            dt.Columns.Add(CALCULATEDFEES, typeof(decimal));
            this.Tables.Add(dt);

            dt = new DataTable(ONGOINGVALUEBREAKDOWN);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(INS, typeof(Guid));
            dt.Columns.Add(FEEVALUE, typeof(decimal));
            dt.Columns.Add(FEETYPE, typeof(string));
            dt.Columns.Add(FEESUBTYPE, typeof(string));
            dt.Columns.Add(FEETYPEENUM, typeof(FeeType));
            dt.Columns.Add(COMMENTS, typeof(string));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(SERVICETYPE, typeof(string));
            dt.Columns.Add(YEAR, typeof(int));
            dt.Columns.Add(MONTH, typeof(int));
            dt.Columns.Add(CALCULATEDFEES, typeof(decimal));
            this.Tables.Add(dt);

            dt = new DataTable(TIERDFEEBREAKDOWN);
            dt.Columns.Add(ID, typeof(Guid));
            dt.Columns.Add(INS, typeof(Guid));
            dt.Columns.Add(FROMVALUE, typeof(decimal));
            dt.Columns.Add(TOVALUE, typeof(decimal));
            dt.Columns.Add(FEEPERCENT, typeof(decimal));
            dt.Columns.Add(FEETYPE, typeof(string));
            dt.Columns.Add(FEESUBTYPE, typeof(string));
            dt.Columns.Add(FEETYPEENUM, typeof(FeeType));
            dt.Columns.Add(COMMENTS, typeof(string));
            dt.Columns.Add(DESCRIPTION, typeof(string));
            dt.Columns.Add(SERVICETYPE, typeof(string));
            dt.Columns.Add(YEAR, typeof(int));
            dt.Columns.Add(MONTH, typeof(int));
            dt.Columns.Add(CALCULATEDFEES, typeof(decimal));
            this.Tables.Add(dt);
        }

        
    }
}
