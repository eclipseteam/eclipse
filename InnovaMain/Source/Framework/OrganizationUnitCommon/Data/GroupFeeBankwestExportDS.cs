﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common.Data
{
    public class GroupFeeBankwestExportDS : UMABaseDS
    {
        public Guid FeeRunID = Guid.Empty;
        public List<FeeEntity> FeeEnttyList = new List<FeeEntity>();
        public List<AssetEntity> Assets = new List<AssetEntity>();
        public List<ModelEntity> Models = new List<ModelEntity>();
        public string Narration = String.Empty; 

        public const string GROUPFEEEXPORTBANKWESTABLE = "GROUPFEEEXPORTBANKWESTABLE";
        public const string CID = "CID";
        public const string ACCOUNTNAME = "ACCOUNTNAME";
        public const string BSB = "BSB";
        public const string ACCOUNTNO = "ACCOUNTNO";
        public const string AMOUNT = "AMOUNT";
        public const string NARRATION = "NARRATION";

        public GroupFeeBankwestExportDS()
        {
            DataTable dt = new DataTable(GROUPFEEEXPORTBANKWESTABLE);
            dt.Columns.Add(CID, typeof(Guid));
            dt.Columns.Add(ACCOUNTNAME, typeof(String));
            dt.Columns.Add(ACCOUNTNO, typeof(String));
            dt.Columns.Add(BSB, typeof(String));
            dt.Columns.Add(AMOUNT, typeof(decimal));
            dt.Columns.Add(NARRATION, typeof(String));
            this.Tables.Add(dt);
        }
    }
}
