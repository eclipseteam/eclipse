﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Common.Data;

namespace Oritax.TaxSimp.Common.UMAFee
{
    [Serializable]
    public class FeeEntity
    {
        Guid id = Guid.NewGuid();

        public Guid ID
        {
            get { return id; }
            set { id = value; }
        } 

        [NonSerialized]
        int month = 1;
        
        public int Month
        {
            get { return month; }
            set { month = value; }
        }

        List<Guid> productList = new List<Guid>();

        public List<Guid> ProductList
        {
            get { return productList; }
            set { productList = value; }
        } 

        ProductConfgType productConfgType = ProductConfgType.All;

        public ProductConfgType ProductConfgType
        {
            get { return productConfgType; }
            set { productConfgType = value; }
        }

        ServiceTypes serviceTypes = ServiceTypes.All;

        public ServiceTypes ServiceTypes
        {
            get { return serviceTypes; }
            set { serviceTypes = value; }
        }

        int year = 1900;

        public int Year
        {
            get { return year; }
            set { year = value; }
        }

        DateTime startDate = new DateTime(2000,1,1);

        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }

        DateTime endDate = new DateTime(2050, 12, 31);

        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }

        FeeType feeType = FeeType.None;

        public FeeType FeeType
        {
            get { return feeType; }
            set { feeType = value; }
        }

        decimal feeTotal = 0;

        public decimal FeeTotal
        {
            get { return feeTotal; }
            set { feeTotal = value; }
        }

        decimal feeTotalConsol = 0;

        public decimal FeeTotalConsol
        {
            get { return feeTotalConsol; }
            set { feeTotalConsol = value; }
        }

        decimal autoAdjustment = 0;

        public decimal AutoAdjustment
        {
            get { return autoAdjustment; }
            set { autoAdjustment = value; }
        }

        decimal adjustment = 0;

        public decimal Adjustment
        {
            get { return adjustment; }
            set { adjustment = value; }
        }

        public decimal FeeTotalAdj
        {
            get { return adjustment + autoAdjustment + feeTotal; }
        }

        string comments = string.Empty;

        public string Comments
        {
            get { return comments; }
            set { comments = value; }
        }

        string description = string.Empty;

        public string Description
        {
            get { return description; }
            set { description = value; }
        } 

        decimal upFrontFee = 0;

        public decimal UpFrontFee
        {
            get { return upFrontFee; }
            set { upFrontFee = value; }
        }
       
        List<OngoingFeeValue> ongoingFeeValueList = new List<OngoingFeeValue>();

        public List<OngoingFeeValue> OngoingFeeValueList
        {
            get { return ongoingFeeValueList; }
            set { ongoingFeeValueList = value; }
        }

        List<OngoingFeePercent> ongoingFeePercentList = new List<OngoingFeePercent>();

        public List<OngoingFeePercent> OngoingFeePercentList
        {
            get { return ongoingFeePercentList; }
            set { ongoingFeePercentList = value; }
        }

        List<FeeTieredRange> feeTieredRangeList = new List<FeeTieredRange>();

        public List<FeeTieredRange> FeeTieredRangeList
        {
            get { return feeTieredRangeList; }
            set { feeTieredRangeList = value; }
        }

        public void CalculateFee(decimal proFum, decimal fum, decimal consoleFum, int month, int year, decimal ratio)
        {
            this.month = month;
            this.year = year;

            foreach (FeeTieredRange feeTieredRange in feeTieredRangeList)
            {
                feeTieredRange.Month = month;
                feeTieredRange.Year = year;
                feeTieredRange.CalculateFee(fum, proFum);
                feeTieredRange.CalculateFeeWithConsolidation(consoleFum, proFum);
                feeTieredRange.CalculateAdjustmentAndTotal();
            }

            decimal totalTierdFee = feeTieredRangeList.Sum(fee => fee.FeeForPeriod);
            decimal totalTierdFeeConsol = feeTieredRangeList.Sum(fee => fee.ConsolFeeForPeriod);

            foreach (OngoingFeePercent ongoingFeePercent in ongoingFeePercentList)
            {
                ongoingFeePercent.Month = month;
                ongoingFeePercent.Year = year;
                ongoingFeePercent.CalculateFee(proFum);
                ongoingFeePercent.CalculateConsolFee(proFum);
                ongoingFeePercent.CalculateAdjustmentAndTotal();
            }

            decimal totalFeePercent = ongoingFeePercentList.Sum(fee => fee.FeeForPeriod);
            decimal totalFeePercentConsol = ongoingFeePercentList.Sum(fee => fee.ConsolFeeForPeriod);

            this.FeeTotal = totalFeePercent + totalTierdFee;
            this.FeeTotalConsol = totalFeePercentConsol + totalTierdFeeConsol;

            if (FeeTotalConsol != 0)
            {
                autoAdjustment = FeeTotalConsol - FeeTotal;

            }
            else
                autoAdjustment = 0; 
        }

        public void CalculateFeeOnGoingValue( int month, int year)
        {
            this.month = month;
            this.year = year;

            foreach (OngoingFeeValue ongoingFeeValue in ongoingFeeValueList)
            {
                ongoingFeeValue.Month = month;
                ongoingFeeValue.Year = year;
                ongoingFeeValue.CalculateFee();
                ongoingFeeValue.CalculateConsolFee();
                ongoingFeeValue.CalculateAdjustmentAndTotal();
            }

            decimal totalFeeValue = ongoingFeeValueList.Sum(fee => fee.FeeForPeriod);

            this.FeeTotal = totalFeeValue;
        }

        public static FeeEntity GetFeeEntity(ClientFeeEntity clientFeeEntity, string clientID, int month, int year)
        {
            FeeEntity feeEntity = new FeeEntity();
            feeEntity.Year = year;
            feeEntity.Month = month;
            feeEntity.Comments = clientID + " - Local Fee";
            feeEntity.Description = clientID + " - Local Fee";
            
            if(clientFeeEntity.OngoingFeePercent)
            {
                OngoingFeePercent feePercent = new OngoingFeePercent();
                feePercent.Year = year;
                feePercent.Month = month;
                feePercent.Comments = clientID + " - Local Fee";
                feePercent.Description = clientID + " - Local Fee";
                feePercent.PercentageFee = Convert.ToDecimal(clientFeeEntity.OngoingFeePercentage);
                feeEntity.OngoingFeePercentList.Add(feePercent);
            }

            if(clientFeeEntity.OnGoingFeeValue)
            {
                OngoingFeeValue feeValue = new OngoingFeeValue();
                feeValue.Year = year;
                feeValue.Month = month;
                feeValue.Comments = clientID + " - Local Fee";
                feeValue.Description = clientID + " - Local Fee";
                feeValue.FeeValue = Convert.ToDecimal(clientFeeEntity.OngoingFeeDollar);
                feeEntity.OngoingFeeValueList.Add(feeValue);
            }

            if(clientFeeEntity.OngoingTierFee)
            {
                FeeTieredRange feeTieredRange1 = new FeeTieredRange();
                feeTieredRange1.Year = year;
                feeTieredRange1.Month = month;
                feeTieredRange1.Description = "Tier 1"; 
                feeTieredRange1.Comments = "Tier 1"; 
                feeTieredRange1.FromValue = clientFeeEntity.Tier1From;
                feeTieredRange1.ToValue = clientFeeEntity.Tier1To;
                feeTieredRange1.PercentageFee = clientFeeEntity.Tier1Percentage;

                feeEntity.FeeTieredRangeList.Add(feeTieredRange1);

                FeeTieredRange feeTieredRange2 = new FeeTieredRange();
                feeTieredRange2.Year = year;
                feeTieredRange2.Month = month;
                feeTieredRange2.Description = "Tier 2";
                feeTieredRange2.Comments = "Tier 2";
                feeTieredRange2.FromValue = clientFeeEntity.Tier2From;
                feeTieredRange2.ToValue = clientFeeEntity.Tier2To;
                feeTieredRange2.PercentageFee = clientFeeEntity.Tier2Percentage;

                feeEntity.FeeTieredRangeList.Add(feeTieredRange2);

                FeeTieredRange feeTieredRange3 = new FeeTieredRange();
                feeTieredRange3.Year = year;
                feeTieredRange3.Month = month;
                feeTieredRange3.Description = "Tier 3";
                feeTieredRange3.Comments = "Tier 3";
                feeTieredRange3.FromValue = clientFeeEntity.Tier3From;
                feeTieredRange3.ToValue = clientFeeEntity.Tier3To;
                feeTieredRange3.PercentageFee = clientFeeEntity.Tier3Percentage;

                feeEntity.FeeTieredRangeList.Add(feeTieredRange3);

                FeeTieredRange feeTieredRange4 = new FeeTieredRange();
                feeTieredRange4.Year = year;
                feeTieredRange4.Month = month;
                feeTieredRange4.Description = "Tier 4";
                feeTieredRange4.Comments = "Tier 4";
                feeTieredRange4.FromValue = clientFeeEntity.Tier4From;
                feeTieredRange4.ToValue = clientFeeEntity.Tier4To;
                feeTieredRange4.PercentageFee = clientFeeEntity.Tier4Percentage;

                feeEntity.FeeTieredRangeList.Add(feeTieredRange4);

                FeeTieredRange feeTieredRange5 = new FeeTieredRange();
                feeTieredRange5.Year = year;
                feeTieredRange5.Month = month;
                feeTieredRange5.Description = "Tier 5";
                feeTieredRange5.Comments = "Tier 5";
                feeTieredRange5.FromValue = clientFeeEntity.Tier5From;
                feeTieredRange5.ToValue = clientFeeEntity.Tier5To;
                feeTieredRange5.PercentageFee = clientFeeEntity.Tier5Percentage;

                feeEntity.FeeTieredRangeList.Add(feeTieredRange5);
            }


            return feeEntity;
        }

    }
}
