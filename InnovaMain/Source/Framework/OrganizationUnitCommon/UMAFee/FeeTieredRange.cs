﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common.UMAFee
{
    [Serializable]
    public class FeeTieredRange
    {
        Guid id = Guid.NewGuid();

        public Guid ID
        {
            get { return id; }
        } 

        [NonSerialized]
        int month = 1;

        public int Month
        {
            get { return month; }
            set { month = value; }
        }

        [NonSerialized]
        int year = 1900;

        public int Year
        {
            get { return year; }
            set { year = value; }
        }

        [NonSerialized]
        decimal calculatedFUM = 0;

        public decimal CalculatedFUM
        {
            get { return calculatedFUM; }
            set { calculatedFUM = value; }
        }

        [NonSerialized]
        decimal baseFUM = 0;

        public decimal BaseFUM
        {
            get { return baseFUM; }
            set { baseFUM = value; }
        }

        [NonSerialized]
        decimal consolBaseFUM = 0;

        public decimal ConsolBaseFUM
        {
            get { return consolBaseFUM; }
            set { consolBaseFUM = value; }
        }

        [NonSerialized]
        decimal productFUM = 0;

        public decimal ProductFUM
        {
            get { return productFUM; }
            set { productFUM = value; }
        }

        [NonSerialized]
        decimal productRatio = 0;

        public decimal ProductRatio
        {
            get { return productRatio; }
            set { productRatio = value; }
        } 

        decimal fromValue = 0;

        public decimal FromValue
        {
            get { return fromValue; }
            set { fromValue = value; }
        }
        decimal toValue = 0;

        public decimal ToValue
        {
            get { return toValue; }
            set { toValue = value; }
        }
        decimal percentageFee = 0;

        public decimal PercentageFee
        {
            get { return percentageFee; }
            set { percentageFee = value; }
        }
        
        public decimal PercentageFeeCalc
        {
            get { return percentageFee/100; }
        }

        decimal feeForPeriod = 0;

        public decimal FeeForPeriod
        {
            get { return feeForPeriod; }
            set { feeForPeriod = value; }
        }

        decimal feeAdjForPeriod = 0;

        public decimal FeeAdjForPeriod
        {
            get { return feeAdjForPeriod; }
            set { feeAdjForPeriod = value; }
        }

        decimal consolFeeForPeriod = 0;

        public decimal ConsolFeeForPeriod
        {
            get { return consolFeeForPeriod; }
            set { consolFeeForPeriod = value; }
        }

        decimal autoAdjustmentForPeriod = 0;

        public decimal AutoAdjustmentForPeriod
        {
            get { return autoAdjustmentForPeriod; }
            set { autoAdjustmentForPeriod = value; }
        }

        string comments = string.Empty;

        public string Comments
        {
            get { return comments; }
            set { comments = value; }
        }

        string description = string.Empty;

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        DateTime startDate = new DateTime(2000, 1, 1);

        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }

        DateTime endDate = new DateTime(2050, 12, 31);

        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }

        public void CalculateFee(decimal baseFUM, decimal productFUM)
        {
            this.productRatio = productFUM / baseFUM;
            this.productFUM = productFUM;
            this.baseFUM = baseFUM;
            CalculateFUMByProduct();
            this.feeForPeriod = ((PercentageFeeCalc * CalculatedFUM) / 365);
        }

        public void CalculateFeeWithConsolidation(decimal consolBaseFUM, decimal productFUM)
        {
            if (consolBaseFUM != 0)
            {
                this.productRatio = productFUM / consolBaseFUM;
                this.productFUM = productFUM;
                this.consolBaseFUM = consolBaseFUM;
                CalculateConsolFUMByProduct();
                this.consolFeeForPeriod = ((PercentageFeeCalc * CalculatedFUM) / 365);
            }
        }

        public void CalculateAdjustmentAndTotal()
        {
            if (consolFeeForPeriod != 0)
                autoAdjustmentForPeriod = consolFeeForPeriod - feeForPeriod;

            feeAdjForPeriod = feeForPeriod + autoAdjustmentForPeriod; 
        }

        public void CalculateFUMByProduct()
        {
            if (this.baseFUM > toValue)
            {
                calculatedFUM = (toValue - fromValue)*productRatio;
            }
            else
            {
                if (this.productFUM > fromValue)
                    calculatedFUM = (productFUM - fromValue);
                else

                    calculatedFUM = 0;
            }
        }

        public void CalculateConsolFUMByProduct()
        {
            if (this.consolBaseFUM > toValue)
            {
                calculatedFUM = (toValue - fromValue) * productRatio;
            }
            else
            {
                if (this.productFUM > fromValue)
                    calculatedFUM = (productFUM - fromValue);
                else

                    calculatedFUM = 0;
            }
        }

        public void CalculateFUM()
        {
            if(this.baseFUM > toValue)
                calculatedFUM = toValue-fromValue;
            else
            {
                if(this.baseFUM > fromValue)
                    calculatedFUM = baseFUM - fromValue;
                else

                    calculatedFUM = 0; 
            }
        }

        public void CalculateConsolFUM()
        {
            if (this.consolBaseFUM > toValue)
                calculatedFUM = toValue - fromValue;
            else
            {
                if (this.baseFUM > fromValue)
                    calculatedFUM = consolBaseFUM - fromValue;
                else

                    calculatedFUM = 0;
            }
        }
    }
}
