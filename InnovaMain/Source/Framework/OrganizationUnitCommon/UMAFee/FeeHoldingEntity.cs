﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common.UMAFee
{
    [Serializable]
    public class FeeHoldingEntity
    {
        Guid productID = Guid.Empty;

        public Guid ProductID
        {
            get { return productID; }
            set { productID = value; }
        } 

        DateTime transactionDate = DateTime.Now;

        public DateTime TransactionDate
        {
            get { return transactionDate; }
            set { transactionDate = value; }
        }

        ServiceTypes serviceTypes = ServiceTypes.None;

        public ServiceTypes ServiceTypes
        {
            get { return serviceTypes; }
            set { serviceTypes = value; }
        }

        decimal holdingValue = 0;

        public decimal HoldingValue
        {
            get { return holdingValue; }
            set { holdingValue = value; }
        }

        decimal totalHoldingValue = 0;

        public decimal TotalHoldingValue
        {
            get { return totalHoldingValue; }
            set { totalHoldingValue = value; }
        }

        decimal calculatedRatio = 0;

        public decimal CalculatedRatio
        {
            get { return calculatedRatio; }
            set { calculatedRatio = value; }
        }

        decimal consolHolding = 0;

        public decimal ConsolHolding
        {
            get { return consolHolding; }
            set { consolHolding = value; }
        }
    }
}
