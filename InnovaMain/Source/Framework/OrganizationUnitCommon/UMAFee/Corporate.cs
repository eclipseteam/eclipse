﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CM;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class CorporateEntity : IClientEntity
    {
        public Guid Clid { get; set; }
        public Guid Csid { get; set; }
        public string Name { get; set; }
        // dummy property used in pdf form
        public string EntityName
        {
            get { return Name; }
            set { Name = value; }
        }

        public string AccountDesignation { get; set; }
        public String LegalName { get; set; }
        public string BTWRAPCode { get; set; }
        public string InvestorType { get; set; }
        public string ClientName { get; set; }
        public string TrusteeType { get; set; }
        public string CorporateTrusteeName { get; set; }
        public string TrustPassword { get; set; }
        public string Country { get; set; }
        public string ACN { get; set; }
        public string ABN { get; set; }
        // dummy property used in pdf form
        public string TrustABN
        {
            get { return ABN; }
            set { ABN = value; }
        }
        public string TFN { get; set; }
        public string TIN { get; set; }
        public string AdvisorCode { get; set; }
        public List<IdentityCMDetail> Contacts { get; set; }
        public List<IdentityCMDetail> Signatories { get; set; }
        public List<IdentityCMDetail> BankAccounts { get; set; }
        public List<IdentityCMDetail> DesktopBrokerAccounts { get; set; }
        public List<IdentityCMDetail> TermDepositAccounts { get; set; }
        public List<IdentityCMDetail> ManagedInvestmentSchemesAccounts { get; set; }
        public List<IdentityCM> CorporatePrivate { get; set; }
        public List<IdentityCM> CorporatePublic { get; set; }
        public bool IsActive { get; set; }
        public string OrganizationStatus { get; set; }
        public String ClientId { get; set; }
        public DualAddressEntity Address { get; set; }
        public ServiceType Servicetype { get; set; }
        public string InvestmentDetails { get; set; }
        public ExemptionCategory ExemptionCategory { get; set; }
        public AccessFacilities AccessFacilities { get; set; }
        public OperationManner OperationManner { get; set; }
        public IdentityCM Parent { get; set; }
        public bool IsClient { get; set; }
        public List<AdminSectionEntity> LinkedAccounts { get; set; }
        public List<AccountProcessTaskEntity> ListAccountProcess { get; set; }

        public BTWrapEntity BTWrap { get; set; }
        public Guid BusinessGroup { get; set; }

        public bool IsCharity { get; set; }
        public string PrincipalTrustActivity { get; set; }
        public bool IsSMSFTrust { get; set; }
        public bool IsOtherTrust { get; set; }
        public List<IdentityCMDetail> Periods { get; set; }
        public ObservableCollection<DividendEntity> DividendCollection { get; set; }
        public ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection { get; set; }

        public int ApplicantsCount
        {
            set
            {
                



            }
            get
            {
                int count = 0;
                if (Contacts != null)
                {
                    count = Contacts.Count;
                }
                return count;
            }

        }
        public int SignatoryCount
        {
            set
            {
                
            }
            get
            {
                int count = 0;
                if (Signatories != null)
                {
                    count = Signatories.Count;
                }
                return count;
            }

        }

        public bool NotCharity
        {
            
            set { IsCharity = !value; }
            get { return !IsCharity; }

        }
        public ObservableCollection<DistributionIncomeEntity> DistributionIncomes { get; set; }
        public CorporateEntity()
        {
            Contacts = new List<IdentityCMDetail>();
            Signatories = new List<IdentityCMDetail>();
            BankAccounts = new List<IdentityCMDetail>();
            DesktopBrokerAccounts = new List<IdentityCMDetail>();
            TermDepositAccounts = new List<IdentityCMDetail>();
            ManagedInvestmentSchemesAccounts = new List<IdentityCMDetail>();
            CorporatePrivate = new List<IdentityCM>();
            CorporatePublic = new List<IdentityCM>();
            Address = new DualAddressEntity();
            Servicetype = new ServiceType();
            ExemptionCategory = new ExemptionCategory();
            AccessFacilities = new AccessFacilities();
            OperationManner = new OperationManner();
            Parent = IdentityCM.Null;
            IsClient = false;
            LegalName = string.Empty;
            AccountDesignation = string.Empty;
            ClientName = string.Empty;
            LinkedAccounts = new List<AdminSectionEntity>();
            ListAccountProcess = new List<AccountProcessTaskEntity>();
            PrincipalTrustActivity = string.Empty;
            BTWrap = new BTWrapEntity();
            IsCharity = false;
            IsSMSFTrust = false;
            IsOtherTrust = false;
            DividendCollection = new ObservableCollection<DividendEntity>();
            ClientManualAssetCollection = new ObservableCollection<ClientManualAssetEntity>();
            DistributionIncomes = new ObservableCollection<DistributionIncomeEntity>();
        }
    }
}
