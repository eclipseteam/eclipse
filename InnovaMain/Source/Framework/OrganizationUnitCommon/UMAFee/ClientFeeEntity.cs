﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common.UMAFee
{
    [Serializable]
    public class ClientFeeEntity
    {
        #region Fee

        public bool OngoingTierFee = false;
        public bool OnGoingFeeValue = false;
        public bool OngoingFeePercent = false; 
        public bool UpFrontFeePaid = false; 

        public decimal UpFrontFee = 0;
        public double OngoingFeeDollar = 0;
        public double OngoingFeePercentage = 0;

        public decimal Tier1To = 0;
        public decimal Tier1From = 0;
        public decimal Tier1Percentage = 0;

        public decimal Tier2To = 0;
        public decimal Tier2From = 0;
        public decimal Tier2Percentage = 0;

        public decimal Tier3To = 0;
        public decimal Tier3From = 0;
        public decimal Tier3Percentage = 0;

        public decimal Tier4To = 0;
        public decimal Tier4From = 0;
        public decimal Tier4Percentage = 0;

        public decimal Tier5To = 0;
        public decimal Tier5From = 0;
        public decimal Tier5Percentage = 0;

        #endregion
    }
}
