﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Oritax.TaxSimp.Common.UMAFee
{
    [Serializable]
    public enum FeeType
    {
        [Description("Account Keeping")]
        AccountKeeping,
        [Description("Adviser")]
        Adviser,
        [Description("Asset Mgt")]
        AssetMgt,
        [Description("Dealer")]
        Dealer,
        [Description("Investment Mgr")]
        InvestmentMgr,
        [Description("MDA Operator")]
        MDAOperator,
        [Description("Platform")]
        Platform,
        [Description("None")]
        None
    }
}
