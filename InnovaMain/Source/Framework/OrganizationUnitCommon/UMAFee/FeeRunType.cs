﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Oritax.TaxSimp.Common.UMAFee
{
    [Serializable]
    public enum FeeRunType
    {
        [Description("Global")]
        Global,
        [Description("Local")]
        Local
    }
}
