﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Oritax.TaxSimp.Common.UMAFee
{
    [Serializable]
    public enum FeeOperationType
    {
        [Description("New Product")]
        NewProduct,
        [Description("Delete Product")]
        DeleteProduct,
        [Description("New Ongoing Fee Value")]
        NewOngoingFeeValue,
        [Description("Delete Ongoing Fee Value")]
        DeleteOngoingFeeValue,
        [Description("Update Ongoing Fee Value")]
        UpdateOngoingFeeValue,
        [Description("New Ongoing Fee Percent")]
        NewOngoingFeePercent,
        [Description("Delete Ongoing Fee Percent")]
        DeleteOngoingFeePercent,
        [Description("Update Ongoing Fee Percent")]
        UpdateOngoingFeePercent,
        [Description("New Tiered Fee")]
        NewTieredFee,
        [Description("Delete Tiered Fee")]
        DeleteTieredFee,
        [Description("Update Tiered Fee")]
        UpdateTieredFee,
        [Description("None")]
        None,
         [Description("Use Local Fee")]
        UseLocalFee
    }
}
