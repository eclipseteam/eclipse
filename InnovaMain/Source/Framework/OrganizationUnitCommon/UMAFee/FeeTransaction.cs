﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
namespace Oritax.TaxSimp.Common.UMAFee
{
    [Serializable]
    public class FeeTransaction
    {
        Guid id = Guid.NewGuid();

        public Guid ID
        {
            get { return id; }
            set { id = value; }
        }

        bool useLocalFee = false;

        public bool UseLocalFee
        {
            get { return useLocalFee; }
            set { useLocalFee = value; }
        }

        string ifaID = string.Empty;

        public string IfaID
        {
            get { return ifaID; }
            set { ifaID = value; }
        }

        Guid ifaCID = Guid.Empty;

        public Guid IfaCID
        {
            get { return ifaCID; }
            set { ifaCID = value; }
        }

        string ifaName = string.Empty;

        public string IfaName
        {
            get { return ifaName; }
            set { ifaName = value; }
        } 

        string clientID = string.Empty;

        public string ClientID
        {
            get { return clientID; }
            set { clientID = value; }
        }

        string clientName = string.Empty;

        public string ClientName
        {
            get { return clientName; }
            set { clientName = value; }
        }

        Guid clientCID = Guid.Empty;

        public Guid ClientCID
        {
            get { return clientCID; }
            set { clientCID = value; }
        }

        string clientType = string.Empty;

        public string ClientType
        {
            get { return clientType; }
            set { clientType = value; }
        }

        string adviserID = string.Empty;

        public string AdviserID
        {
            get { return adviserID; }
            set { adviserID = value; }
        }

        Guid adviserCID = Guid.Empty;

        public Guid AdviserCID
        {
            get { return adviserCID; }
            set { adviserCID = value; }
        }

        string adviserName = string.Empty;

        public string AdviserName
        {
            get { return adviserName; }
            set { adviserName = value; }
        } 

        Guid feeRunID = Guid.NewGuid();

        public Guid FeeRunID
        {
            get { return feeRunID; }
            set { feeRunID = value; }
        }

        DateTime transactionDate = DateTime.Now;

        public DateTime TransactionDate
        {
            get { return transactionDate; }
            set { transactionDate = value; }
        }

        int month = 1;

        public int Month
        {
            get { return month; }
            set { month = value; }
        } 

        int year = 1900;

        public int Year
        {
            get { return year; }
            set { year = value; }
        }

        List<FeeHoldingEntity> feeHoldingEntityList = new List<FeeHoldingEntity>();

        public List<FeeHoldingEntity> FeeHoldingEntityList
        {
            get { return feeHoldingEntityList; }
            set { feeHoldingEntityList = value; }
        }

        List<Guid> configuredFeesList = new List<Guid>();

        public List<Guid> ConfiguredFeesList
        {
            get { return configuredFeesList; }
            set { configuredFeesList = value; }
        }

        Hashtable configuredFeesListWithManualAdjustment = new Hashtable();

        public Hashtable ConfiguredFeesListWithManualAdjustment
        {
            get { return configuredFeesListWithManualAdjustment; }
            set { configuredFeesListWithManualAdjustment = value; }
        } 

        string description = string.Empty;

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        decimal total = 0;

        public decimal Total
        {
            get { return total; }
            set { total = value; }
        }

        decimal adjustment = 0;

        public decimal Adjustment
        {
            get { return adjustment; }
            set { adjustment = value; }
        }

        decimal manAdjustment = 0;

        public decimal ManAdjustment
        {
            get { return manAdjustment; }
            set { manAdjustment = value; }
        }

        public decimal FeeTotalAdj
        {
            get { return adjustment + total; }
        }

        public decimal FeeTotalManAdj
        {
            get { return FeeTotalAdj + manAdjustment; }
        }

        public TransactionType TransactionType = new TransactionType("EX", "Expense", AllowablePolarityType.Negative);

        public static readonly List<TransactionType> TransactionTypes = new List<TransactionType>()
{
new TransactionType( "VA", "Reversal - Redemption / Withdrawal / Fee / Tax" ,AllowablePolarityType.Positive), 
new TransactionType( "AE", "Rights Issue" ,AllowablePolarityType.Positive), 
new TransactionType( "RA", "Sale" ,AllowablePolarityType.Negative), 
new TransactionType( "ST", "Surcharge Tax" ,AllowablePolarityType.Negative), 
new TransactionType( "AS", "Switch In" ,AllowablePolarityType.Positive), 
new TransactionType( "RS", "Switch Out" ,AllowablePolarityType.Negative), 
new TransactionType( "RX", "Tax" ,AllowablePolarityType.Negative), 
new TransactionType( "AR", "Tax Rebate" ,AllowablePolarityType.Positive), 
new TransactionType( "AT", "Transfer In" ,AllowablePolarityType.Positive), 
new TransactionType( "RT", "Transfer Out" ,AllowablePolarityType.Negative), 
new TransactionType( "RW", "Withdrawal" ,AllowablePolarityType.Negative), 
new TransactionType( "RJ", "Adjustment Up" ,AllowablePolarityType.Positive), 
new TransactionType( "AJ", "Adjustment Down" ,AllowablePolarityType.Negative), 
new TransactionType( "AP", "Application" ,AllowablePolarityType.Positive), 
new TransactionType( "AM", "Bonus Issue" ,AllowablePolarityType.Positive), 
new TransactionType( "AN", "Buy / Purchase" ,AllowablePolarityType.Positive), 
new TransactionType( "RU", "Commutation" ,AllowablePolarityType.Negative), 
new TransactionType( "AC", "Contribution" ,AllowablePolarityType.Positive), 
new TransactionType( "RL", "Contribution Tax" ,AllowablePolarityType.Negative), 
new TransactionType( "AD", "Deposit" ,AllowablePolarityType.Positive), 
new TransactionType( "EX", "Expense" ,AllowablePolarityType.Negative), 
new TransactionType( "IN", "Opening (Initial) Balance" ,AllowablePolarityType.NegativeOrPositive), 
new TransactionType( "LM", "Instalment" ,AllowablePolarityType.Positive), 
new TransactionType( "RH", "Insurance Premium" ,AllowablePolarityType.Negative), 
new TransactionType( "AL", "Lodgement" ,AllowablePolarityType.Positive), 
new TransactionType( "RK", "Lump Sum Tax" ,AllowablePolarityType.Negative), 
new TransactionType( "MA", "Manager Fee" ,AllowablePolarityType.Negative), 
new TransactionType( "RY", "PAYG" ,AllowablePolarityType.Negative), 
new TransactionType( "RP", "Pension Payment" ,AllowablePolarityType.Negative), 
new TransactionType( "RI", "Pension Payment – Income Stream" ,AllowablePolarityType.Negative), 
new TransactionType( "PR", "Purchase Reinvestment" ,AllowablePolarityType.Positive), 
new TransactionType( "RR", "Redemption" ,AllowablePolarityType.Negative), 
new TransactionType( "VB", "Reversal - Buy / Purchase / Deposit" ,AllowablePolarityType.Negative)

};
    }
}
