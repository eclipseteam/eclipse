﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Oritax.TaxSimp.Common.UMAFee
{
    [Serializable]
    public enum FeeSubType
    {
        [Description("Ongoing Fee ($)")]
        OngoingFeeValue,
        [Description("Ongoing Fee (%)")]
        OngoingFeePercent,
        [Description("Tiered Fee")]
        TieredFee,
        [Description("None")]
        None
    }
}
