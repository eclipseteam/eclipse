﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Oritax.TaxSimp.Common.UMAFee
{
    [Serializable]
    public enum ProductConfgType
    {
        [Description("All")]
        All,
        [Description("By Exclusions")]
        ByExclusions,
        [Description("By Inclusions")]
        ByInclusions,
        [Description("None")]
        None
    }
}
