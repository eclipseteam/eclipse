﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Oritax.TaxSimp.Common.UMAFee
{
    [Serializable]
    public class OngoingFeeValue
    {
        Guid id = Guid.NewGuid();

        public Guid ID
        {
            get { return id; }
        }

        [NonSerialized]
        int month = 1;

        public int Month
        {
            get { return month; }
            set { month = value; }
        }

        [NonSerialized]
        int year = 1900;

        public int Year
        {
            get { return year; }
            set { year = value; }
        }

        [NonSerialized]
        decimal baseFUM = 0;

        public decimal BaseFUM
        {
            get { return baseFUM; }
            set { baseFUM = value; }
        }

        decimal feeValue = 0;

        public decimal FeeValue
        {
            get { return feeValue; }
            set { feeValue = value; }
        }

        decimal feeForPeriod = 0;

        public decimal FeeForPeriod
        {
            get { return feeForPeriod; }
            set { feeForPeriod = value; }
        }

        decimal consolFeeForPeriod = 0;

        public decimal ConsolFeeForPeriod
        {
            get { return consolFeeForPeriod; }
            set { consolFeeForPeriod = value; }
        }

        string comments = string.Empty;

        public string Comments
        {
            get { return comments; }
            set { comments = value; }
        }

        string description = string.Empty;

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        decimal autoAdjustmentForPeriod = 0;

        public decimal AutoAdjustmentForPeriod
        {
            get { return autoAdjustmentForPeriod; }
            set { autoAdjustmentForPeriod = value; }
        }

        decimal feeAdjForPeriod = 0;

        public decimal FeeAdjForPeriod
        {
            get { return feeAdjForPeriod; }
            set { feeAdjForPeriod = value; }
        }

        DateTime startDate = new DateTime(2000, 1, 1);

        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }

        DateTime endDate = new DateTime(2050, 12, 31);

        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }

        public void CalculateFee()
        {
            this.feeForPeriod = this.feeValue / 365;
        }

        public void CalculateConsolFee()
        {
            this.consolFeeForPeriod = this.feeValue / 365;
        }

        public void CalculateAdjustmentAndTotal()
        {
            if (consolFeeForPeriod != 0)
                autoAdjustmentForPeriod = consolFeeForPeriod - feeForPeriod;

            feeAdjForPeriod = feeForPeriod + autoAdjustmentForPeriod;
        }
    }

}
