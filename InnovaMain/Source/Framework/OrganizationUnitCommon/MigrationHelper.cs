﻿using System;
using System.Xml.Linq;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.Common
{
    public interface IMigrationHelper<T>
    {
        string XName { get; }
        Guid CLID { get; set; }
        Guid CSID { get; set; }
        string Name { get; set; }
        T Entity { get; set; }
        bool IsInvestableClient { get; set; }
        string OrganizationStatus { get; set; }
        string ClientId { get; set; }
        IdentityCM Parent { get; set; }
        bool IsClient { get; set; }
        void SetServiceTypeFromCM(T entity);
        void SetExemptionCategoryFromCM(T entity);
        void SetOperationMannerFromCM(T entity);
        void SetAccessFacilitiesFromCM(T entity);
        void SetServiceTypeToCM(T entity);
        void SetExemptionCategoryToCM(T entity);
        void SetOperationMannerToCM(T entity);
        void SetAccessFacilitiesToCM(T entity);
    }

    public static class MigrationHelper
    {
        public static void ExportHelper(this IMigrationHelper<ClientIndividualEntity> unit, XElement root)
        {
            unit.Entity.IsActive = unit.IsInvestableClient;
            unit.Entity.OrganizationStatus = unit.OrganizationStatus;
            unit.Entity.ClientId = unit.ClientId;
            unit.Entity.Parent = unit.Parent;
            unit.Entity.IsClient = unit.IsClient;
            unit.SetServiceTypeFromCM(unit.Entity);
            unit.SetExemptionCategoryFromCM(unit.Entity);
            unit.SetOperationMannerFromCM(unit.Entity);
            unit.SetAccessFacilitiesFromCM(unit.Entity);
            root.Add(unit.Entity.ToXElement(unit.XName));
        }

        public static void ImportHelper(this IMigrationHelper<ClientIndividualEntity> unit, XElement root, IdentityCmMap map)
        {
            unit.Entity = root.FromXElement<ClientIndividualEntity>(unit.XName);
            unit.Entity.Clid = unit.CLID;
            unit.Entity.Csid = unit.CSID;
            map.ReplaceOrRemove(unit.Entity.Applicants);
            map.ReplaceOrRemove(unit.Entity.BankAccounts);
            map.ReplaceOrNull(unit.Entity.Parent);
            unit.Parent = unit.Entity.Parent;
            unit.IsClient = unit.Entity.IsClient;
            unit.IsInvestableClient = unit.Entity.IsActive;
            unit.OrganizationStatus = unit.Entity.OrganizationStatus;
            unit.Name = unit.Entity.Name;
            unit.ClientId = unit.Entity.ClientId;
            unit.SetServiceTypeToCM(unit.Entity);
            unit.SetExemptionCategoryToCM(unit.Entity);
            unit.SetOperationMannerToCM(unit.Entity);
            unit.SetAccessFacilitiesToCM(unit.Entity);
        }

        public static void ExportHelper(this IMigrationHelper<CorporateEntity> unit, XElement root)
        {
            unit.Entity.IsActive = unit.IsInvestableClient;
            unit.Entity.OrganizationStatus = unit.OrganizationStatus;
            unit.Entity.ClientId = unit.ClientId;
            unit.Entity.Parent = unit.Parent;
            unit.Entity.IsClient = unit.IsClient;
            unit.SetServiceTypeFromCM(unit.Entity);
            unit.SetExemptionCategoryFromCM(unit.Entity);
            unit.SetOperationMannerFromCM(unit.Entity);
            unit.SetAccessFacilitiesFromCM(unit.Entity);
            root.Add(unit.Entity.ToXElement(unit.XName));
        }

        public static void ImportHelper(this IMigrationHelper<CorporateEntity> unit, XElement root, IdentityCmMap map)
        {
            unit.Entity = root.FromXElement<CorporateEntity>(unit.XName);
            unit.Entity.Clid = unit.CLID;
            unit.Entity.Csid = unit.CSID;
            map.ReplaceOrRemove(unit.Entity.BankAccounts);
            map.ReplaceOrRemove(unit.Entity.Contacts);
            map.ReplaceOrRemove(unit.Entity.CorporatePrivate);
            map.ReplaceOrRemove(unit.Entity.CorporatePublic);
            map.ReplaceOrRemove(unit.Entity.Signatories);
            map.ReplaceOrNull(unit.Entity.Parent);
            unit.Parent = unit.Entity.Parent;
            unit.IsClient = unit.Entity.IsClient;
            unit.IsInvestableClient = unit.Entity.IsActive;
            unit.OrganizationStatus = unit.Entity.OrganizationStatus;
            unit.Name = unit.Entity.Name;
            unit.ClientId = unit.Entity.ClientId;
            unit.SetServiceTypeToCM(unit.Entity);
            unit.SetExemptionCategoryToCM(unit.Entity);
            unit.SetOperationMannerToCM(unit.Entity);
            unit.SetAccessFacilitiesToCM(unit.Entity);
        }

    }
}
