﻿using System;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CM;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class NonIndividualEntity : IClientEntity
    {
        public Guid ID { get; set; }
        public Guid ClientCid { get; set; }
        public string ClientId { get; set; }
        public NonIndividualsType Type { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public DateTime? DateOfCreation { get; set; }
        public PhoneNumberEntity OfficePhoneNumber { get; set; }
        public PhoneNumberEntity OfficePhoneNumber2 { get; set; }
        public MobileNumberEntity MobilePhoneNumber { get; set; }
        public PhoneNumberEntity Facsimile { get; set; }
        public string TFN { get; set; }
        public string ABN { get; set; }
        public string Description { get; set; }
        public Oritax.TaxSimp.Common.AddressEntity ResidentialAddress { get; set; }
        public Oritax.TaxSimp.Common.AddressEntity MailingAddress { get; set; }
        public bool MailingAddressSame { get; set; }
        public bool HasAddress { get; set; }

        public NonIndividualEntity()
        {
            ID = Guid.NewGuid();
            Type = new NonIndividualsType();
            DateOfCreation = new DateTime();
            OfficePhoneNumber = new PhoneNumberEntity();
            OfficePhoneNumber2 = new PhoneNumberEntity();
            MobilePhoneNumber = new MobileNumberEntity();
            ResidentialAddress = new AddressEntity();
            MailingAddress = new AddressEntity();
            Facsimile = new PhoneNumberEntity();
        }

        public IdentityCMDetail Advisor { get; set; }

    }

    public interface IHasNonIndividualEntity
    {
        Oritax.TaxSimp.Common.NonIndividualEntity NonIndividual { get; set; }
    }
}
