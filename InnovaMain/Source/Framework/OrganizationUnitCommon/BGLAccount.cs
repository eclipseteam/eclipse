﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    public class BGLAccount
    {
        public string ImportTransactionType = string.Empty;
        public string Category = string.Empty;
        public string SubCategory  = string.Empty;
        public int DefaultAccountingCode = 0;
        public int DefaultBankAccountingCode = 0;

        public BGLAccount(string importTransactionType, string category, string subCategory, int defaultAccountingCode, int defaultBankAccountingCode)
        {
            ImportTransactionType = importTransactionType;
            Category = category;
            SubCategory = subCategory;
            DefaultAccountingCode = defaultAccountingCode;
            DefaultBankAccountingCode = defaultBankAccountingCode;
        }
    }
}
