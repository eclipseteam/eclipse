﻿using System;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public abstract class OrderInstructionEntity
    {
        public Guid ID { get; set; }
        public Guid OrderID { get; set; }
        public Guid AccountCID { get; set; }
        public Guid ProductID { get; set; }
        public bool IsSettled { get; set; }
        public InstructionStatus Status { get; set; }
        public InstructionType Type { get; set; }
        public DateTime TradeDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public Guid OrderItemID { get; set; }
    }
}
