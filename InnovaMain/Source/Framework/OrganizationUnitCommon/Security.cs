﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class EntitySecurity
    {
        public List<UserEntity> IncludedUsers { get; set; }
        public List<UserEntity> AvailableUsers { get; set; }
        public EntitySecurity()
        {
            IncludedUsers = new List<UserEntity>();
            AvailableUsers = new List<UserEntity>();
        }
    }
}
