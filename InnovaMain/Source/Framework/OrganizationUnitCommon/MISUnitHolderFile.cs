﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class MISUnitHolderFile
    {
        public MISUnitHolderFile()
        {
            Account = string.Empty;
            FundCode = string.Empty;
            FundName = string.Empty;
            AsofDate = DateTime.Now;
            SubAccount = string.Empty;
            TotalShares = 0;
            RedemptionPrice = 0;
            CurrentValue = 0;
            InvestorCategory = string.Empty;
        }
        public string Account { get; set; }
        public string FundCode { get; set; }
        public string FundName { get; set; }
        public DateTime AsofDate { get; set; }
        public string SubAccount { get; set; }
        public double TotalShares { get; set; }
        public double RedemptionPrice { get; set; }
        public double CurrentValue { get; set; }
        public string InvestorCategory { get; set; }

    }
}
