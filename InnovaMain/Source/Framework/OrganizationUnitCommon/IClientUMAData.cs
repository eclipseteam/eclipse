﻿using System;
using System.Collections.Generic;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Common;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.CM
{
    public interface IClientUMAData
    {
        string Name { get; set; }
        string AccountDesignation { get; set; }
        String LegalName { get; set; }
        string BTWRAPCode { get; set; }
        string Country { get; set; }
        string ABN { get; set; }
        string TFN { get; set; }    
        string TIN { get; set; }
        ObservableCollection<DividendEntity> DividendCollection { get; set; }
        List<Common.IdentityCMDetail> DesktopBrokerAccounts { get; set; }
        List<Common.IdentityCMDetail> TermDepositAccounts { get; set; }
        List<Common.IdentityCMDetail> ManagedInvestmentSchemesAccounts { get; set; }
        ObservableCollection<DistributionIncomeEntity> DistributionIncomes { get; set; }
        ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection { get; set; }
        List<AccountProcessTaskEntity> ListAccountProcess { get; set; }
        Common.DualAddressEntity Address { get; set; }
        ServiceType Servicetype { get; set; }
        string InvestmentDetails { get; set; }
        ExemptionCategory ExemptionCategory { get; set; }
        AccessFacilities AccessFacilities { get; set; }
        OperationManner OperationManner { get; set; }
        IdentityCM Parent { get; set; }
        BTWrapEntity BTWrap { get; set; }
        BTWrapEntity EclipseSuper { get; set; }
        bool IsCharity { get; set; }
        string PrincipalTrustActivity { get; set; }
        bool IsSMSFTrust { get; set; }
        bool IsOtherTrust { get; set; }
        List<Common.IdentityCMDetail> GetSignatoriesList();
        ClientWorkflowEntity ClientWorkflowEntity { get; set; }
        List<Common.IdentityCMDetail> BankAccounts { get; set; }
        DateTime? MDAExecutionDate { get; set; }
        ClientAuthoritytoActEntity AuthoritytoAct { get; set; }
        ClientChessTransferofSecurityEntity ChessTransferofSecurity { get; set; }
    }
}
