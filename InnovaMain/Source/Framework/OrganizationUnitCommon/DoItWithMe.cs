﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class DoItWithMe
    {
        public string AuthoriastionType { get; set; }
        public DoItYourSelf DoItYourSelf { get; set; }
        public string ProgramCode { get; set; }
        public string ProgramName { get; set; }
        public ModelEntity Model { get; set; }
        public AccessFacilities AccessFacilities { get; set; }
        public OperationManner OperationManner { get; set; }

        public bool UseDIWMBWA = false;
        public bool UseDIWMMAQ = false;
        public string DIWMMaqBSBNo = string.Empty;
        public string DIWMMaqACCNo = string.Empty;
        public string DIWMMaqACCName = string.Empty;
        public bool UseDIWMDesktopBroker = false;
        public bool UseDIWMAMM = false;
        public bool UseDIWMFIIG = false;
        public bool UseDIWMSS = false;
        public decimal DIWMInitialInvestment = 0;
        public List<Guid> PreferredFunds = new List<Guid>();

        public DoItWithMe()
        {
            DoItYourSelf = new DoItYourSelf();
            AuthoriastionType = "A";
        }
    }
}
