﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Oritax.TaxSimp.Common
{
    public class BGLAccountMap : List<BGLAccount>
    {
        public BGLAccountMap()
        {
            this.IntialiseMap();
        }

        private void IntialiseMap()
        {
            this.Add(new BGLAccount("Deposit", "Contibution", "Employer Additional", 242, 604));
            this.Add(new BGLAccount("Deposit", "Contibution", "Employer SG", 242, 604));
            this.Add(new BGLAccount("Deposit", "Contibution", "Personal", 260, 604));
            this.Add(new BGLAccount("Deposit", "Contibution", "Salary Sacrifice", 242, 604));
            this.Add(new BGLAccount("Deposit", "Contibution", "Spouse", 260, 604));

            this.Add(new BGLAccount("Deposit", "Income", "Rental Income", 280, 604));
            this.Add(new BGLAccount("Deposit", "Income", "Commission Rebate", 265, 604));
            this.Add(new BGLAccount("Deposit", "Income", "Distribution", 238, 604));
            this.Add(new BGLAccount("Deposit", "Income", "Dividend", 239, 604));
            this.Add(new BGLAccount("Deposit", "Income", "Interest", 250, 604));
            this.Add(new BGLAccount("Deposit", "Income", "Transfer In", 999, 604));
            this.Add(new BGLAccount("Deposit", "Income", "Internal Cash Movement", 999, 604));

            this.Add(new BGLAccount("Deposit", "Investment", "Redemption", 491, 604));

            this.Add(new BGLAccount("Deposit", "Tax", "Business Activity Statement", 840, 604));
            this.Add(new BGLAccount("Deposit", "Tax", "Instalment Activity Statement", 850, 604));
            this.Add(new BGLAccount("Deposit", "Tax", "Income Tax", 850, 604));
            this.Add(new BGLAccount("Deposit", "Tax", "PAYG", 860, 604));
            this.Add(new BGLAccount("Deposit", "Tax", "Tax Refund", 850, 604));

            this.Add(new BGLAccount("Withdrawal", "Benefit Payment", "Withdrawal", 323, 604));

            this.Add(new BGLAccount("Withdrawal", "Expense", "Accounting Expense", 301, 604));
            this.Add(new BGLAccount("Withdrawal", "Expense", "Administration Fee", 302, 604));
            this.Add(new BGLAccount("Withdrawal", "Expense", "Advisory Fee", 302, 604));
            this.Add(new BGLAccount("Withdrawal", "Expense", "General", 350, 604));
            this.Add(new BGLAccount("Withdrawal", "Expense", "Insurance Premuim (Life)", 390, 604));
            this.Add(new BGLAccount("Withdrawal", "Expense", "Other Insurance", 480, 604));
            this.Add(new BGLAccount("Withdrawal", "Expense", "Internal Cash Movement", 999, 604));
            this.Add(new BGLAccount("Withdrawal", "Expense", "Investment Fee", 302, 604));
            this.Add(new BGLAccount("Withdrawal", "Expense", "Legal Expense", 302, 604));
            this.Add(new BGLAccount("Withdrawal", "Expense", "Pension Payment", 999, 604));
            this.Add(new BGLAccount("Withdrawal", "Expense", "Property", 397, 604));
            this.Add(new BGLAccount("Withdrawal", "Expense", "Regulatory Fee", 304, 604));
            this.Add(new BGLAccount("Withdrawal", "Expense", "Transfer Out", 999, 604));

            this.Add(new BGLAccount("Withdrawal", "Investment", "Application",999, 604));

            this.Add(new BGLAccount("Withdrawal", "Investment", "Shares in Listed Companies (Australian)", 776, 604));
            this.Add(new BGLAccount("Withdrawal", "Investment", "Shares in Listed Companies (International)", 777, 604));
            this.Add(new BGLAccount("Withdrawal", "Investment", "Shares in Unlisted Companies (Australian)", 778, 604));
            this.Add(new BGLAccount("Withdrawal", "Investment", "Shares in Unlisted Companies (International)", 779, 604));
            this.Add(new BGLAccount("Withdrawal", "Investment", "Units in Listed Trusts(Australian)", 782, 604));
            this.Add(new BGLAccount("Withdrawal", "Investment", "Units in Listed Trusts (International)", 783, 604));
            this.Add(new BGLAccount("Withdrawal", "Investment", "Units in Unlisted Trusts (Australian)", 784, 604));
            this.Add(new BGLAccount("Withdrawal", "Investment", "Units in Unlisted Trusts (International)", 785, 604));

            this.Add(new BGLAccount("Withdrawal", "Tax", "Business Activity Statement", 840, 604));
            this.Add(new BGLAccount("Withdrawal", "Tax", "Instalment Activity Statement", 850, 604));
            this.Add(new BGLAccount("Withdrawal", "Tax", "Income Tax", 850, 604));
            this.Add(new BGLAccount("Withdrawal", "Tax", "PAYG", 860, 604));
            this.Add(new BGLAccount("Withdrawal", "Tax", "Tax Refund", 850, 604));
        }
    }
}
