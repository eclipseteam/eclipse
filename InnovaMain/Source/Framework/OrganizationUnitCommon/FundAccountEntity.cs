﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class FundAccountEntity
    {
        public Guid ID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public ObservableCollection<MISFundTransactionEntity> FundTransactions { get; set; }
        public double TotalShares { get; set; }
        public double RedemptionPrice { get; set; }
        public ObservableCollection<MISUnitHolderFile> SecurityHoldings { get; set; }

        public ObservableCollection<MISPaidOrderEntity> PaidOrders { get; set; }
        public ObservableCollection<MISUnsettledOrderEntity> UnsettledOrders { get; set; }
        public ObservableCollection<MISFundTransactionEntity> DeletedFundTransactions { get; set; }
    }
}
