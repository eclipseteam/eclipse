﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class ImportMessages
    {
        public string Message { set; get; }
        public string Status { get; set; }
        public ImportMessageType MessageType { get; set; }
        public ImportMessages()
        {
            Message = string.Empty;
            Status = string.Empty;
            MessageType = ImportMessageType.Sucess;
        }

    }

    public enum ImportMessageType
    {
        InvestmentCodeAdded,
        MissingInvestmentCode,
        Error,
        Sucess,
        Warning,
        InvestmentCodeDeleted,
    }
}
