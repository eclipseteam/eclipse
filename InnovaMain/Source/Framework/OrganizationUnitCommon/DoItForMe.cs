﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class DoItForMe
    {
        public string AuthoriastionType { get; set; }
        public string ProgramCode { get; set; }
        public string ProgramName { get; set; }

        public string SponsoringBroker { get; set; }
        public string HIN { get; set; }

        public bool WholesaleInvestor { get; set; }
        public bool RetailInvestor { get; set; }

        public AccessFacilities AccessFacilities { get; set; }
        public OperationManner OperationManner { get; set; }


        public decimal DIFMInitialInvestment = 0;
        //ToDo: check with above program code and name field as in Model Summary screen with in client.
        //public Guid ModelID = Guid.Empty;
        public bool UseMinBalance = true;
        public bool UseMinTrade = true;
        public bool SellTheHolding = true;
        public decimal MinBalance = 2500;
        public decimal MinBalancePercentage = 0;
        public bool UseMinBalanceCurr = true;
        public bool UseMinBalancePercentage = false;
        public decimal MinTrade = 2500;
        public decimal MinTradePercentage = 0;
        public bool UseMinTradeCurr = true;
        public bool UseMinTradePercentage = false;
        public bool AssignedAllocatedValueHoldSell = true;
        public bool DistributedAllocatedValueHoldSell = false;
        public bool AssignedAllocatedValueExclusion = true;
        public bool DistributedAllocatedValueExclusion = false;
        public bool AssignedAllocatedValueMinTradeHolding = true;
        public bool DistributedAllocatedValueMinTradeHolding = false;

        public DoItForMe()
        {
            AuthoriastionType = "D";
        }
    }
}
