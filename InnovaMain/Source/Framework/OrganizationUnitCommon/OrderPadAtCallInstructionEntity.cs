﻿using System;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class OrderPadAtCallInstructionEntity : OrderInstructionEntity
    {
        public string CustomerName { get; set; }
        public string ClientID { get; set; }
        public string AccountNumber { get; set; }
        public string BSB { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Units { get; set; }
        public AtCallAccountType TransferTo { get; set; }
        public Guid TransferAccCID { get; set; }
        public string TransferAccNumber { get; set; }
        public string TransferAccBSB { get; set; }
        public string TransferNarration { get; set; }
        public string FromNarration { get; set; }
        
        public Guid BrokerID { get; set; }
        public Guid BankID { get; set; }
        public decimal? Min { get; set; }
        public decimal? Max { get; set; }
        public decimal? Rate { get; set; }
        public Guid AtCallAccountCID { get; set; }
        public string AccountTier { get; set; }

        public AtCallType AtCallType { get; set; }
        public OrderItemType OrderItemType { get; set; }
        public OrderBankAccountType OrderBankAccountType { get; set; }
    }
}
