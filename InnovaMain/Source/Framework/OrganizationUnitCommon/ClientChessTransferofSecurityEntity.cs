﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class ClientChessTransferofSecurityEntity
    {

        public bool TransferListedSecuritiesToDesktopBroker { get; set; }
    
        public string ChessDIYDIWM { get; set; }

        public string ChessRegisteredAccountName { get; set; }
        public string ChessAccountDesignation { get; set; }

        public string ChessAddressLine1 { get; set; }
        public string ChessAddressLine2 { get; set; }
        public string ChessSuburb { get; set; }
        public string ChessState { get; set; }
        public string ChessPostCode { get; set; }
        public string ChessCountry { get; set; }


        public bool IssuedSponsor { get; set; }
        public bool ExactNameMatch { get; set; }
        public string ExactNameMatchAmount { get; set; }
        public bool ExactNameNoMatch { get; set; }
        public string ExactNameNoMatchAmount { get; set; }
        public bool BrokerSponsored { get; set; }

        public string ExistingBroker1 { get; set; }
        public string ClientNo1 { get; set; }
        public string ClientHin1 { get; set; }

        public string ExistingBroker2 { get; set; }
        public string ClientNo2 { get; set; }
        public string ClientHin2 { get; set; }

        public ClientChessTransferofSecurityEntity()
        {
            TransferListedSecuritiesToDesktopBroker = false;
            ChessDIYDIWM = string.Empty;
            ChessRegisteredAccountName = string.Empty;
            ChessAccountDesignation = string.Empty;
            ChessAddressLine1 = string.Empty;
            ChessAddressLine2 = string.Empty;
            ChessSuburb = string.Empty;
            ChessState = string.Empty;
            ChessPostCode = string.Empty;
            ChessCountry = string.Empty;
            IssuedSponsor = false;
            ExactNameMatch = false;
            ExactNameMatchAmount = string.Empty;
            ExactNameNoMatch = false;
            ExactNameNoMatchAmount = string.Empty;
            BrokerSponsored = false;
            ExistingBroker1 = string.Empty;
            ClientNo1 = string.Empty;
            ClientHin1 = string.Empty;
            ExistingBroker2 = string.Empty;
            ClientNo2 = string.Empty;
            ClientHin2 = string.Empty;

        }

        public Dictionary<AddressPart, string> GetAddressDictionary()
        {
            return new Dictionary<AddressPart, string>()
               {
                   { AddressPart.Addressline1, ChessAddressLine1??string.Empty },
                   { AddressPart.Addressline2, ChessAddressLine2??string.Empty },
                   { AddressPart.Suburb, ChessSuburb??string.Empty },
                   { AddressPart.State, ChessState??string.Empty },
                   { AddressPart.PostCode, ChessPostCode??string.Empty },
                   { AddressPart.Country, ChessCountry??string.Empty },
               };
        }
    }
}
