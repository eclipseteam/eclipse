﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Web;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;



namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class BTWRAPCode
    {
        public static string GetBTWRAPCode(IdentityCM identitycm, ICMBroker Broker)
        {
            string xml = string.Empty;
            if (identitycm != IdentityCM.Null)
            {


                IBrokerManagedComponent component = null;

                if (identitycm == null) return "";
                component = Broker.GetCMImplementation(identitycm.Clid, identitycm.Csid) as IBrokerManagedComponent;
                if (component == null) return "";
                xml = component.GetDataStream((int)CmCommand.GetBTWRAPCode, "");
                return xml;
            }
            return xml;
        }
    }


    [Serializable]
    public class UpDateEventLog
    {
        public static void UpdateLog(string entity,string record, int eventnum, ICMBroker Broker)
        {
            if (eventnum == -1)
            {
                Broker.LogEvent(EventType.EntityCreated, String.Format("Record added '{0}' in '{1}'", record, entity), "", Guid.Empty);
            }

            if (eventnum == 500)
            {
                Broker.LogEvent(EventType.EntityEdited, String.Format("Record updated '{0}' in '{1}'", record, entity), "", Guid.Empty);
            }
            
            if (eventnum == 4)
            {
                Broker.LogEvent(EventType.EntityDeleted, String.Format("Record deleted '{0}' from '{1}'", record, entity), "", Guid.Empty);
            }

            if (eventnum == 1)
            {
                Broker.LogEvent(EventType.EntityViewed, String.Format("Record Viewed '{0}' from '{1}'", record, entity), "", Guid.Empty);
            }
        }
        

        public static void UpdateLog(string entity, int eventnum, ICMBroker Broker,string CurrentUser)
        {
            
            if (eventnum == -1)
            {
                Broker.LogEvent(EventType.EntityCreated, String.Format(entity), CurrentUser, Guid.Empty);
            }

            if (eventnum == 500)
            {
                Broker.LogEvent(EventType.EntityEdited, String.Format(entity), CurrentUser, Guid.Empty);
            }
            
            if (eventnum == 4)
            {
                Broker.LogEvent(EventType.EntityDeleted, String.Format(entity), CurrentUser, Guid.Empty);
            }
        }

    }
}

