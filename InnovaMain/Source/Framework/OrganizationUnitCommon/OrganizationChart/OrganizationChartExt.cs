﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;


namespace Oritax.TaxSimp.Common
{
    public static class OrganizationChartExt
    {
        //public static OrganizationChart ToOrganizationChart(this List<IdentityCMDetail> entities, ICMBroker Broker, string NodeName)
        //{
        //    if (entities == null || entities.Count <= 0) return null;

        //    OrganizationChart chart = new OrganizationChart();
        //    chart.NodeName = NodeName;

        //    IOrganizationChartData component = null;
        //    foreach (var each in entities)
        //    {
        //        component = Broker.GetCMImplementation(each.Clid, each.Csid) as IOrganizationChartData;
        //        chart.Subordinates.Add(component.GetOrganizationChart());
        //    }

        //    return chart;
        //}

        //public static OrganizationChart ToOrganizationChart(this List<IdentityCM> entities, ICMBroker Broker, string NodeName)
        //{
        //    if (entities == null || entities.Count <= 0) return null;

        //    OrganizationChart chart = new OrganizationChart();
        //    chart.NodeName = NodeName;

        //    IOrganizationChartData component = null;
        //    foreach (var each in entities)
        //    {
        //        component = Broker.GetCMImplementation(each.Clid, each.Csid) as IOrganizationChartData;
        //        chart.Subordinates.Add(component.GetOrganizationChart());
        //    }

        //    return chart;
        //}

        public static OrganizationChart ToOrganizationChart(this List<IdentityCMDetail> list, ICMBroker Broker, string nodetype)
        {
            if (list.Count == 0) return null;

            OrganizationChart orgchart = new OrganizationChart();
            orgchart.NodeName = nodetype;

            foreach (IdentityCMDetail _each in list)
            {
                
                orgchart.Subordinates.Add(new OrganizationChart() { NodeName = GetAssosiateName(_each, Broker), Clid = _each.Clid, Csid = _each.Csid, TypeName = OrganizationType.Individual.ToString() });
                
                //orgchart.Subordinates.Add();
                //((Oritax.TaxSimp.Data.IdentityCM)((new System.Collections.Generic.Mscorlib_CollectionDebugView<Oritax.TaxSimp.Common.IdentityCMDetail>(list)).Items[0])).Clid;
                //orgchart.Clid = _each.Clid;
                //orgchart.Csid = _each.Csid;
            }
            return orgchart;
        }

        public static OrganizationChart ToOrganizationChart(this List<IdentityCM> list, ICMBroker Broker, string nodetype)
        {
            if (list.Count == 0) return null;

            OrganizationChart orgchart = new OrganizationChart();
            foreach (IdentityCM _each in list)
            {
                orgchart.NodeName = nodetype;
                orgchart.Subordinates.Add(new OrganizationChart() { NodeName = GetAssosiateName(_each, Broker) });
            }

            return orgchart;
        }

        private static string GetAssosiateName(IIdentityCM identity, ICMBroker Broker)
        {
            IBrokerManagedComponent bmc = Broker.GetCMImplementation(identity.Clid, identity.Csid);
            string name = string.Empty;
            if (bmc != null)
            {
                switch (bmc.TypeName.ToLower())
                {
                    case "advisor":
                    case "adviser":
                    case "individual":
                        name = bmc.GetDataStream(999, string.Empty);
                        break;
                    default:
                        name = bmc.Name;
                        break;
                }
            }
            return name;
        }

        public static OrganizationChart ToOrganizationChart(this IdentityCM identitycm, ICMBroker Broker, string nodetype)
        {
            if (identitycm == null || identitycm == IdentityCM.Null) return null;

            OrganizationChart orgchart = new OrganizationChart() { NodeName = GetAssosiateName(identitycm, Broker), TypeName = nodetype };
            return orgchart;
        }
    }
}
