﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class ASXOrderItem : OrderItemEntity
    {
        public string InvestmentCode { get; set; }
        
        public Guid ProductID { get; set; }
        public string ASXAccountNo { get; set; }
        public string ParentAcc { get; set; }
        public Guid ASXAccountCID { get; set; }
        public string ASXAccountName { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? SuggestedUnitPrice { get; set; }
        public decimal? SuggestedUnits { get; set; }
        public decimal? SuggestedAmount { get; set; }
    }
}
