﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class ManagedInvestmentSchemesAccountEntity:IClientEntity
    {
        public string Name { get; set; }
        public List<FundAccountEntity> FundAccounts { get; set; }
        public List<AccountRelation> LinkedAccounts { get; set; }
        public DateTime UploadTime { get; set; }

        public ManagedInvestmentSchemesAccountEntity()
        {
            FundAccounts = new List<FundAccountEntity>(); 
            LinkedAccounts = new List<AccountRelation>();          
        }
    }
}
