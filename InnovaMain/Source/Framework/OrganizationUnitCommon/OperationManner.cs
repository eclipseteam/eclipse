﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class OperationManner
    {
        public bool ANY_ONE_OF_US_TO_SIGN { get; set; }
        public bool ANY_TWO_OF_US_TO_SIGN { get; set; }
        public bool ALL_OF_US_TO_SIGN { get; set; }
    }
}
