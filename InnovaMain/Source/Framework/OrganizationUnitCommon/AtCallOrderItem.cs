﻿using System;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class AtCallOrderItem : OrderItemEntity
    {
        public AtCallAccountType TransferTo { get; set; }
        public Guid TransferAccCID { get; set; }
        public string TransferAccNumber { get; set; }
        public string TransferAccBSB { get; set; }
        public string TransferNarration { get; set; }
        public string FromNarration { get; set; }
      
        public Guid BrokerID { get; set; }
        public Guid BankID { get; set; }
        public decimal? Min { get; set; }
        public decimal? Max { get; set; }

        public decimal? Rate { get; set; }

        public Guid ProductID { get; set; }
        public Guid AtCallAccountCID { get; set; }
        public string AccountTier { get; set; }

        public AtCallType AtCallType { get; set; }
        public bool IsExternalAccount { get; set; }


        /// <summary>
        /// Attemt to extract simple rate from string
        /// </summary>
        /// <returns></returns>
        public decimal? GetDecimalRate()
        {
            decimal d = 0;
            if (Rate > 0) // for backward compatibility
                return Rate;
            if (TransferNarration == null)
                return Rate;
            string s = TransferNarration.Trim();
            int i = s.IndexOf('%');
            if (i> 0 && i < s.Length)
                s= s.Remove(i);
            if (Decimal.TryParse(s, out d))//this will convert a simple rate
                return d;
            else
                return 0;
         }   
    }
}
