﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
    {
    [Serializable]
    public class MISPaidOrderEntity
        {
        public Guid ID { get; set; }
        public string UnitholderCode { get; set; }
        public string UnitholderSubCode { get; set; }
        public string FundCode { get; set; }
        public string MCHCode { get; set; }
        public DateTime TradeDate { get; set; }
        public DateTime ClearDate { get; set; }
        public decimal Amount { get; set; }
        public string TransactionType { get; set; }
        public string CurrencyOfTrade { get; set; }
        public string PurchaseSource { get; set; }
        public string ProcessAtNAV { get; set; }
        public bool IsSettled { get; set; }
        public string SettelerTransactionID { get; set; }
        public string Status { get; set; }

        public string RedemptionOrExchangeOptions { get; set; }
        public decimal NumberofShares { get; set; }
        public string ExchangeToFund { get; set; }
        public string DealerCommissionRate { get; set; }
        public string ExternalID { get; set; }
        public string RedemptionSource { get; set; }
        public string OperatorEnteredBy { get; set; }
        public string Generator { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string AUDEquivalentAmount { get; set; }

        public MISPaidOrderEntity()
            {
            ID = Guid.Empty;

            UnitholderCode = string.Empty;

            UnitholderSubCode = string.Empty;
            FundCode = string.Empty;

            MCHCode = string.Empty;
            TradeDate = DateTime.Now;

            ClearDate = DateTime.Now;


            Amount = 0;
            TransactionType = string.Empty;
            CurrencyOfTrade = string.Empty;

            PurchaseSource = string.Empty;
            ProcessAtNAV = string.Empty;

            RedemptionOrExchangeOptions = string.Empty;
            
            ExchangeToFund = string.Empty;
            DealerCommissionRate = string.Empty;
            ExternalID = string.Empty;
            RedemptionSource = string.Empty;
            OperatorEnteredBy = string.Empty;
            Generator = string.Empty;
            
            AUDEquivalentAmount = string.Empty;


            IsSettled = false;
            SettelerTransactionID = string.Empty;
            Status = string.Empty;



            }
        }
    }
