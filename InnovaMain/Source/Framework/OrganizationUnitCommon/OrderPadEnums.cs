﻿namespace Oritax.TaxSimp.Common
{

    public enum OrderAccountType
    {
        TermDeposit = 1,
        StateStreet = 2,
        ASX = 3,
        BankAccount = 4,
        AtCall = 5,
    }
    public enum OrderType
    {
        FinSimplicity = 1, Manual = 2
    }
    public enum OrderStatus
    {
        Active = 1, ValidationFailed = 2, Validated = 3, Approved = 4, Submitted = 5, Complete = 6, Cancelled = 7
    }

    public enum OrderItemType
    {
        Buy = 1,
        Sell = 2
    }
    public enum TradeType
    {
        Buy = 1,
        Sell = 2
    }
    public enum OrderSetlledUnsettledType
    {
        Initialized = 0,// when order is validated but not submitted
        Unsettled = 1,
        OnMarket = 2,
        Settled = 3,
        Complete = 4,
        Cancelled = 5,

    }

    public enum InstructionStatus
    {
        Created = 1,
        Issued = 2,
        OnMarket = 3,
        Settled = 4,
        Complete = 5,
        Cancelled = 6,
    }

    public enum InstructionType
    {
        StateStreet = 1,
        ASX = 2,
        BankWest = 3,
        TermDeposit = 4,
        AtCall = 5,
        ASXBulk = 6,
        Macquarie = 7,
        TDBulk = 8,
    }

    public enum BankWestInstructionType
    {
        StateStreet = 1,
        P2 = 2,
        BankWest = 3,
    }

    public enum MacquarieInstructionType
    {
        StateStreet = 1,
        P2 = 2,
        Macquarie = 3,
    }

    public enum SetlledunSettledEntryType { TradeTransactions, SettlementTransactions }
    public enum SetlledunSettledTransactionType { Orignal, Confirmation }
    public enum MatchedBy
    {
        ByDate,
        ByAmount,
    }

    public enum AtCallAccountType
    {
        Client,
        Adviser,
    }

    public enum AtCallType
    {
        BestRates,
        MoneyMovement,
        MoneyMovementBroker,
    }

    public enum BulkOrderStatus
    {
        None = 0, Active = 1, OnMarket = 6, Complete = 11, Canceled = 16
    }

    public enum OrderBulkStatus
    {
        None = 0, Batched = 1, BulkTrade = 6, Complete = 11,
    }

    public enum OrderPreferedBy
    {
        Amount = 0, Units = 5
    }

    public enum OrderBankAccountType
    {
        Internal = 0, External = 5, AtCall = 10
    }
}