﻿using System.Collections.Generic;
using System.Data;

namespace Oritax.TaxSimp.Common
{
   public interface IHasImportTransaction
    {
        List<ImportMessages> ImportTransactions(System.Xml.Linq.XElement xml,DataSet ds);
    }
}
