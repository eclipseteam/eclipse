﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class BTWrapEntity
    {
        public string AccountNumber { get; set; }
        public string AccountDescription { get; set; }
    }
}
