﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class ClientIndividualEntityCSV
    {
        public List<IndividualEntity> Applicants { get; set; }
        public List<BankAccountEntity> BankAccounts { get; set; }
        public Guid Clid { get; set; }
        public Guid Csid { get; set; }
        public string Name { get; set; }
        public string LegalName { get; set; }        
        public bool IsActive { get; set; }
        public string OrganizationStatus { get; set; }
        public String ClientId { get; set; }
        public ServiceType Servicetype { get; set; }
        public DualAddressEntity Address { get; set; }
        public string InvestmentDetails { get; set; }
        public ExemptionCategory ExemptionCategory { get; set; }
        public AccessFacilities AccessFacilities { get; set; }
        public OperationManner OperationManner { get; set; }
        public string TFN_ABN_ARBN { get; set; }
        public string TIN { get; set; }
        public string Country { get; set; }
        public List<AdminSectionEntity> LinkedAccounts { get; set; }
        public List<AccountProcessTaskEntity> ListAccountProcess { get; set; }
        public ClientIndividualEntityCSV()
        {
            Applicants = new List<IndividualEntity>();
            BankAccounts = new List<BankAccountEntity>();
            Servicetype = new ServiceType();
            Address = new DualAddressEntity();
            ExemptionCategory = new ExemptionCategory();
            AccessFacilities = new AccessFacilities();
            OperationManner = new OperationManner();
            LinkedAccounts = new List<AdminSectionEntity>();
            ListAccountProcess = new List<AccountProcessTaskEntity>();
        }

       
    }
}
