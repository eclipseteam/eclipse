﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class CorporateCSV
    {
        public List<IndividualEntity> Contacts { get; set; }
        public List<IndividualEntity> Signatories { get; set; }
        public List<BankAccountEntity> BankAccounts { get; set; }
        public Guid Clid { get; set; }
        public Guid Csid { get; set; }
        public string Name { get; set; }
        public string LegalName { get; set; }   
        public string InvestorType { get; set; }
        public string ClientName { get; set; }
        public string TrusteeType { get; set; }
        public string CorporateTrusteeName { get; set; }
        public string TrustPassword { get; set; }
        public string Country { get; set; }
        public string ACN { get; set; }
        public string ABN { get; set; }
        public string TFN { get; set; }
        public string TIN { get; set; }
        public string AdvisorCode { get; set; }        
        public bool IsActive { get; set; }
        public string OrganizationStatus { get; set; }
        public String ClientId { get; set; }
        public DualAddressEntity Address { get; set; }
        public ServiceType Servicetype { get; set; }
        public string InvestmentDetails { get; set; }
        public ExemptionCategory ExemptionCategory { get; set; }
        public AccessFacilities AccessFacilities { get; set; }
        public OperationManner OperationManner { get; set; }
        public List<AdminSectionEntity> LinkedAccounts { get; set; }
        public List<AccountProcessTaskEntity> ListAccountProcess { get; set; }

        public CorporateCSV()
        {
            Contacts = new List<IndividualEntity>();
            Signatories = new List<IndividualEntity>();
            BankAccounts = new List<BankAccountEntity>();            
            Address = new DualAddressEntity();
            Servicetype = new ServiceType();
            ExemptionCategory = new ExemptionCategory();
            AccessFacilities = new AccessFacilities();
            OperationManner = new OperationManner();
            LinkedAccounts = new List<AdminSectionEntity>();
            ListAccountProcess = new List<AccountProcessTaskEntity>();
        }
    }
}
