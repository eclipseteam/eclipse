﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;

namespace Oritax.TaxSimp.Common
{
    public static class CSVUtilities
    {
        public static String GetBankWestType(string value)
        {
            string result = string.Empty;
            switch (value.ToLower())
            {
                case "clientcorporationprivate":
                case "clientcorporationpublic":
                case "clientsoletrader":
                    result = "Business";
                    break;
                case "clientothertrustscorporate":
                case "clientothertrustsindividual":
                case "clientsmsfcorporatetrustee":
                case "clientsmsfindividualtrustee":
                    result = "Trust";
                    break;
                case "clientindividual":
                case "clienteclipsesuper":
                    result = "Personal";
                    break;
                default:
                    break;
            }
            return result;
        }

        public static String GetTrusteesCompanyType(string value)
        {
            string result = string.Empty;
            switch (value.ToLower())
            {
                case "clientothertrustscorporate":
                case "clientsmsfcorporatetrustee":
                    result = bool.TrueString;
                    break;

                case "clientcorporationprivate":
                case "clientcorporationpublic":
                case "clientsoletrader":
                case "clientothertrustsindividual":                
                case "clientsmsfindividualtrustee":                    
                case "clientindividual":
                case "clienteclipsesuper":
                    result = bool.FalseString;
                    break;
                default:
                    break;
            }
            return result;
        }

        public static String GetTrustType(string value)
        {
            string result = string.Empty;
            switch (value.ToLower())
            {
                case "clientsmsfindividualtrustee":
                case "clientsmsfcorporatetrustee":
                    result = "Super Fund";
                    break;
                case "clientothertrustscorporate":
                case "clientcorporationprivate":
                case "clientcorporationpublic":
                case "clientsoletrader":
                case "clientothertrustsindividual":                
                case "clientindividual":
                case "clienteclipsesuper":
                    result = "Trust";
                    break;
                default:
                    break;
            }
            return result;
        }

        public static String GetAdvisorName(IdentityCM identitycm, ICMBroker broker)
        {

            return GetEntityDataByCommand(identitycm, CmCommand.FullName, broker); 
        }

        public static String GetEntityDataByCommand(IdentityCM identitycm, CmCommand command, ICMBroker broker)
        {
            if (identitycm != IdentityCM.Null) return "";

            IBrokerManagedComponent component = null;
            component = broker.GetCMImplementation(identitycm.Clid, identitycm.Csid) as IBrokerManagedComponent;
            if (component == null) return "";
            return component.GetDataStream((int)command, "");
        }

        private static string RunCommand(List<IdentityCMDetail> items, ICMBroker broker, CmCommand command)
        {
            string result = "";


            if (items != null && items.Count > 0)
            {
                var contact = items.FirstOrDefault(ss => ss.IsPrimary == true) ?? items.First();
                if (contact != null)
                {
                    result = GetEntityDataByCommand(contact, command, broker);
                }
            }

            return result;
        }
        public static string GetContactName(List<IdentityCMDetail> contacts, ICMBroker broker)
        {
            return RunCommand(contacts, broker, CmCommand.FullName);
        }
        public static string GetContactPhoneCityCode(List<IdentityCMDetail> contacts, ICMBroker broker)
        {

            return RunCommand(contacts, broker, CmCommand.PhoneCityCode);
           
        }

        public static string GetContactPhoneNumber(List<IdentityCMDetail> contacts, ICMBroker broker)
        {
            return RunCommand(contacts, broker, CmCommand.PhoneNumber);
        }
    }
}
