﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml;
using System.Xml.Linq;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CalculationInterface;


namespace Oritax.TaxSimp.Common
{
    public class CSVInstance
    {
        public CSVInstance(ICMBroker _broker)
        {
            Broker = _broker;
        }

        private ICMBroker Broker { get; set; }

        public CorporateCSV GetEntityConcreteInstance(CorporateEntity _entity)
        {
            CorporateCSV corporatecsv = new CorporateCSV();
            corporatecsv.Signatories = GetConcreteIndividualEntityObject(_entity.Signatories);
            corporatecsv.Contacts = GetConcreteIndividualEntityObject(_entity.Contacts);
            corporatecsv.BankAccounts = GetConcreteBankAccountEntityObjectList(_entity.BankAccounts);
            corporatecsv.ABN = _entity.ABN;
            corporatecsv.AccessFacilities = _entity.AccessFacilities;
            corporatecsv.ACN = _entity.ACN;
            corporatecsv.Address = _entity.Address;
            corporatecsv.AdvisorCode = _entity.AdvisorCode;
            corporatecsv.Clid = _entity.Clid;
            corporatecsv.ClientId = _entity.ClientId;
            corporatecsv.ClientName = _entity.ClientName;
            corporatecsv.CorporateTrusteeName = _entity.CorporateTrusteeName;
            corporatecsv.Country = _entity.Country;
            corporatecsv.Csid = _entity.Csid;
            corporatecsv.ExemptionCategory = _entity.ExemptionCategory;
            corporatecsv.InvestmentDetails = _entity.InvestmentDetails;
            corporatecsv.InvestorType = _entity.InvestorType;
            corporatecsv.IsActive = _entity.IsActive;
            corporatecsv.Name = _entity.Name;
            corporatecsv.LegalName = _entity.LegalName;
            corporatecsv.OperationManner = _entity.OperationManner;
            corporatecsv.OrganizationStatus = _entity.OrganizationStatus;
            corporatecsv.Servicetype = _entity.Servicetype;
            corporatecsv.TFN = _entity.TFN;
            corporatecsv.TIN = _entity.TIN;
            corporatecsv.TrusteeType = _entity.TrusteeType;
            corporatecsv.TrustPassword = _entity.TrustPassword;
            corporatecsv.LinkedAccounts = _entity.LinkedAccounts;
            corporatecsv.ListAccountProcess = _entity.ListAccountProcess;
            return corporatecsv;
        }

        public ClientIndividualEntityCSV GetEntityConcreteInstance(ClientIndividualEntity _entity)
        {
            ClientIndividualEntityCSV clientindividualentitycsv = new ClientIndividualEntityCSV();
            clientindividualentitycsv.Applicants = GetConcreteIndividualEntityObject(_entity.Applicants);
            clientindividualentitycsv.BankAccounts = GetConcreteBankAccountEntityObjectList(_entity.BankAccounts);            
            clientindividualentitycsv.AccessFacilities = _entity.AccessFacilities;            
            clientindividualentitycsv.Address = _entity.Address;            
            clientindividualentitycsv.Clid = _entity.Clid;
            clientindividualentitycsv.ClientId = _entity.ClientId;
            clientindividualentitycsv.Country = _entity.Country;
            clientindividualentitycsv.Csid = _entity.Csid;
            clientindividualentitycsv.ExemptionCategory = _entity.ExemptionCategory;
            clientindividualentitycsv.InvestmentDetails = _entity.InvestmentDetails;            
            clientindividualentitycsv.IsActive = _entity.IsActive;
            clientindividualentitycsv.Name = _entity.Name;
            clientindividualentitycsv.LegalName = _entity.LegalName;
            clientindividualentitycsv.OperationManner = _entity.OperationManner;
            clientindividualentitycsv.OrganizationStatus = _entity.OrganizationStatus;
            clientindividualentitycsv.Servicetype = _entity.Servicetype;
            clientindividualentitycsv.TIN = _entity.TIN;
            clientindividualentitycsv.TFN_ABN_ARBN = _entity.TFN;
            clientindividualentitycsv.LinkedAccounts = _entity.LinkedAccounts;
            clientindividualentitycsv.ListAccountProcess = _entity.ListAccountProcess;
            return clientindividualentitycsv;
        }

        public List<IndividualEntity> GetConcreteIndividualEntityObject(List<IdentityCMDetail> list)
        {
            string xml = string.Empty;
            IBrokerManagedComponent component = null;
            List<IndividualEntity> returnlist = new List<IndividualEntity>();
            returnlist.Clear();
            foreach (var each in list)
            {
                component = Broker.GetCMImplementation(each.Clid, each.Csid) as IBrokerManagedComponent;
                if (component != null)
                {
                    xml = component.GetDataStream((int)CmCommand.CSVData, null);
                    returnlist.Add(xml.ToNewOrData<IndividualEntity>());
                }
            }
            return returnlist;
        }

        private List<BankAccountEntity> GetConcreteBankAccountEntityObjectList(List<IdentityCMDetail> list)
        {
            string xml = string.Empty;
            List<BankAccountEntity> returnlist = new List<BankAccountEntity>();
            returnlist.Clear();
            foreach (var each in list)
            {
              var acc=  GetConcreteBankAccountEntityObject(each);
                if(acc!=null)
                {returnlist.Add(acc);
                }
            }
            return returnlist;
        }



        public BankAccountEntity GetConcreteBankAccountEntityObject(IdentityCMDetail cmdetail)
            {
            string xml = string.Empty;
            IBrokerManagedComponent component = null;
            BankAccountEntity returnlist =null;
         
            
                component = Broker.GetCMImplementation(cmdetail.Clid, cmdetail.Csid) as IBrokerManagedComponent;
                if (component != null)
                    {
                    xml = component.GetDataStream((int)CmCommand.CSVData, null);
                    returnlist=xml.ToNewOrData<BankAccountEntity>();
                    }
              
            return returnlist;
            }

        public BankAccountEntity GetConcreteBankAccountEntityObject(IdentityCM cmdetail)
            {
            string xml = string.Empty;
            IBrokerManagedComponent component = null;
            BankAccountEntity returnlist = null;


            component = Broker.GetCMImplementation(cmdetail.Clid, cmdetail.Csid) as IBrokerManagedComponent;
            if (component != null)
                {
                xml = component.GetDataStream((int)CmCommand.CSVData, null);
                returnlist = xml.ToNewOrData<BankAccountEntity>();
                }

            return returnlist;
            }
    }
}
