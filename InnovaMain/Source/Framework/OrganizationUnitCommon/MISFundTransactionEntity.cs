﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using System.Data;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class MISFundTransactionEntity
    {
        [NonSerialized]
        public bool ProcessedTransactionForCostPrice = false;

       

        public Guid ID { get; set; }
        public DateTime TradeDate { get; set; }
        public string ClientID { get; set; }
        public string TransactionType { get; set; }
        public decimal Shares { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Amount
        {
            get { return Shares * UnitPrice; }
        }
        public string InvestorCategory { get; set; }
        public string Status { get; set; }

        public Guid DistributionID{get; set; }
        public TransectionStatus DistributionStatus;
        public MISFundTransactionEntity()
        {
            ID = Guid.Empty;
            ClientID = string.Empty;
            TransactionType = string.Empty;
            InvestorCategory = string.Empty;
            TradeDate = DateTime.Now;
            UnitPrice = 0;
            Shares = 0;
            Status = string.Empty;

        }

        public static MISFundTransactionEntity GetFundAccountEntity(DataRow rowEntity)
        {
            MISFundTransactionEntity misFundTransactionEntity = new Common.MISFundTransactionEntity();
            string cliendID = rowEntity[MISTransactionDetailsDS.CLIENTID].ToString();
            string transactionType = rowEntity[MISTransactionDetailsDS.SYSTRANTYPE].ToString();
            Guid transactionID = (Guid)rowEntity[MISTransactionDetailsDS.ID];
            decimal unitPrice = (decimal)rowEntity[MISTransactionDetailsDS.UNITPRICE];
            DateTime tradeDate = (DateTime)rowEntity[MISTransactionDetailsDS.TRADEDATE];
            decimal shares = (decimal)rowEntity[MISTransactionDetailsDS.SHARES];
            
            misFundTransactionEntity.ID = transactionID;
            misFundTransactionEntity.ClientID = cliendID;
            misFundTransactionEntity.TransactionType = transactionType;
            misFundTransactionEntity.TradeDate = tradeDate;
            misFundTransactionEntity.UnitPrice = unitPrice;
            misFundTransactionEntity.Shares = shares;

            return misFundTransactionEntity;
        }

        public bool IsDistributionMatchWithTrans(string clientID, DistributionIncomeEntity db, string transactionFundCode)
        {
            /*   
             * 1. Fund code is the same for distribution and MIS transactions 
             * 2. MIS Transaction type is “Adjustment Up”
             * 3. Trade date for MIS transaction = payment date for distribution transaction 
             * 4. “Net Cash Distribution” in distribution transaction = Amount in MIS transaction
             * 
             */
            bool isclientIDMatched = ClientID == clientID;
            bool isAdjustupTans = this.TransactionType == "Adjustment Up";
            bool issameinvestmentcode = transactionFundCode == db.FundCode;
            bool hasDateInRange = db.PaymentDate.HasValue && db.PaymentDate.Value.Date==TradeDate.Date;
            bool isAmount = Math.Abs(Amount - Convert.ToDecimal(db.NetCashDistribution)) < (decimal) 0.5;
            bool isNotMatched = DistributionStatus != TransectionStatus.Matched;
            bool isMatchedWithSameDiv = DistributionID != Guid.Empty && this.DistributionID == db.ID; // if dividend is already matched with same transactions
            return isclientIDMatched && isAdjustupTans && issameinvestmentcode && hasDateInRange && isAmount && (isNotMatched || isMatchedWithSameDiv);
        }

    
    }
}
