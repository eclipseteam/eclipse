﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class AddressEntity
    {
        public string Addressline1 { get; set; }
        public string Addressline2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }

        public string GetFullAddress()
        {
            StringBuilder fullAddress = new StringBuilder();

            fullAddress.Append(Addressline1);
            fullAddress.Append(", ");
            if (Addressline2 != String.Empty)
            {
                fullAddress.Append(Addressline2);
                fullAddress.Append(", ");
            }
            fullAddress.Append(Suburb);
            fullAddress.Append(", ");
            fullAddress.Append(State);
            fullAddress.Append(", ");
            fullAddress.Append(Country);
            fullAddress.Append(", ");
            fullAddress.Append(PostCode);

            return fullAddress.ToString();
        }

        public Dictionary<AddressPart, string> GetAddressDictionary()
        {
            return new Dictionary<AddressPart, string>()
               {
                   { AddressPart.Addressline1, Addressline1??string.Empty },
                   { AddressPart.Addressline2, Addressline2??string.Empty },
                   { AddressPart.Suburb, Suburb??string.Empty },
                   { AddressPart.State, State??string.Empty },
                   { AddressPart.PostCode, PostCode??string.Empty },
                   { AddressPart.Country, Country??string.Empty },
               };
        }
    }
}
