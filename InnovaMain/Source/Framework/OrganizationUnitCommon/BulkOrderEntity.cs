﻿using System;
using System.Collections.Generic;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class BulkOrderEntity
    {
        public Guid ID { get; set; }
        public long BulkOrderID { get; set; }
        public BulkOrderStatus Status { get; set; }
        public OrderAccountType OrderAccountType { get; set; }
        public OrderType OrderType { get; set; }
        public OrderItemType OrderItemType { get; set; }
        //For ASX
        public List<BulkOrderItemEntity> BulkOrderItems { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public Guid UpdatedByCID { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsExported { get; set; }
        public List<BulkOrderInstructionEntity> Instructions { get; set; }
        public DateTime? TradeDate { get; set; }
        public ClientManagementType ClientManagementType { get; set; }
        public OrderPreferedBy OrderPreferedBy { get; set; }
        public string SMARequestID { get; set; }
        
        //For TD
        public List<TDBulkOrderItemEntity> TDBulkOrderItems { get; set; }
        public SMACallStatus SMACallStatus { get; set; }
        public byte SMARequestCount { get; set; }

        public bool IsSubmitted { get; set; }
    }
}
