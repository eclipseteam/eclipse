﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CM;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class ClientIndividualEntity : IClientEntity, IClientEntityOrgUnits, IClientUMAData
    {
        private ClientWorkflowEntity clientWorkflowEntity = new ClientWorkflowEntity();

        public ClientWorkflowEntity ClientWorkflowEntity
        {
            get { return clientWorkflowEntity; }
            set { clientWorkflowEntity = value; }
        }

        public Guid Clid { get; set; }
        public Guid Csid { get; set; }
        public string Name { get; set; }
        public string AccountDesignation { get; set; }
        public String LegalName { get; set; }
        public string BTWRAPCode { get; set; }
        public List<IdentityCMDetail> Applicants { get; set; }

        public List<IdentityCMDetail> BankAccounts { get; set; }
        public List<IdentityCMDetail> DesktopBrokerAccounts { get; set; }
        public List<IdentityCMDetail> TermDepositAccounts { get; set; }
        public List<IdentityCMDetail> ManagedInvestmentSchemesAccounts { get; set; }
        public bool IsActive { get; set; }
        public string OrganizationStatus { get; set; }
        public String ClientId { get; set; }
        public ServiceType Servicetype { get; set; }
        public DualAddressEntity Address { get; set; }
        public string InvestmentDetails { get; set; }
        public ExemptionCategory ExemptionCategory { get; set; }
        public AccessFacilities AccessFacilities { get; set; }
        public OperationManner OperationManner { get; set; }
        public string TFN { get; set; }
        public string ABN { get; set; }
        public string TIN { get; set; }
        public string Country { get; set; }
        public IdentityCM Parent { get; set; }
        public bool IsClient { get; set; }
        public List<AdminSectionEntity> LinkedAccounts { get; set; }
        public List<AccountProcessTaskEntity> ListAccountProcess { get; set; }

        public BTWrapEntity BTWrap { get; set; }
        public BTWrapEntity EclipseSuper { get; set; }
        public Guid BusinessGroup { get; set; }

        public bool IsCharity { get; set; }
        public string PrincipalTrustActivity { get; set; }
        public bool IsSMSFTrust { get; set; }
        public bool IsOtherTrust { get; set; }
        public List<IdentityCMDetail> Periods { get; set; }
        public ObservableCollection<DividendEntity> DividendCollection { get; set; }
        public ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection { get; set; }

        public List<Oritax.TaxSimp.Common.IdentityCMDetail> GetSignatoriesList()
        {
            return this.Applicants;
        }

        public int ApplicantsCount
        {
            set { }
            get
            {
                int count = 0;
                if (Applicants != null)
                {
                    count = Applicants.Count;
                }
                return count;
            }

        }

        public int SignatoryCount
        {
            set { }
            get
            {
                int count = 0;
                if (Applicants != null)
                {
                    count = Applicants.Count;
                }
                return count;
            }

        }

        public bool NotCharity
        {

            set { IsCharity = !value; }
            get { return !IsCharity; }

        }

        public ObservableCollection<DistributionIncomeEntity> DistributionIncomes { get; set; }
        public ClientChessTransferofSecurityEntity ChessTransferofSecurity { get; set; }
        public ClientAuthoritytoActEntity AuthoritytoAct { get; set; }
        public List<P2ReportData> P2Reports { get; set; }
        public DateTime? MDAExecutionDate { get; set; }

        public ClientIndividualEntity()
        {
            Applicants = new List<IdentityCMDetail>();
            BankAccounts = new List<IdentityCMDetail>();
            BankAccounts = new List<IdentityCMDetail>();
            DesktopBrokerAccounts = new List<IdentityCMDetail>();
            TermDepositAccounts = new List<IdentityCMDetail>();
            ManagedInvestmentSchemesAccounts = new List<IdentityCMDetail>();
            Servicetype = new ServiceType();
            Address = new DualAddressEntity();
            ExemptionCategory = new ExemptionCategory();
            AccessFacilities = new AccessFacilities();
            OperationManner = new OperationManner();
            TFN = string.Empty;
            ABN = string.Empty;
            TIN = string.Empty;
            Country = string.Empty;
            Parent = IdentityCM.Null;
            IsClient = false;
            LegalName = string.Empty;
            PrincipalTrustActivity = string.Empty;
            BTWRAPCode = string.Empty;
            AccountDesignation = string.Empty;
            LinkedAccounts = new List<AdminSectionEntity>();
            ListAccountProcess = new List<AccountProcessTaskEntity>();
            BTWrap = new BTWrapEntity();
            IsCharity = false;
            IsSMSFTrust = false;
            IsOtherTrust = false;
            DividendCollection = new ObservableCollection<DividendEntity>();
            ClientManualAssetCollection = new ObservableCollection<ClientManualAssetEntity>();
            DistributionIncomes = new ObservableCollection<DistributionIncomeEntity>();
            ChessTransferofSecurity = new ClientChessTransferofSecurityEntity();
            AuthoritytoAct = new ClientAuthoritytoActEntity();
        }
    }

    public enum Quarters
    {
        None = 0,
        Q1 = 5,
        Q2 = 10,
        Q3 = 15,
        Q4 = 20,
    }
    [Serializable]
    public class P2ReportData
    {
        public string AttachedBMCID { get; set; }
        public int Year { get; set; }
        public string Quarter { get; set; }
        public string P2Type { get; set; }
    }
}
