﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
   public interface IHasDistributionsAndDividends
    {
        void MatchBankTransactionWithDividendOrDistribution(CashManagementEntity cashTrans,IdentityCMDetail account);
        void MatchMISTransactionWithDistribution(MISFundTransactionEntity fundTransaction, IdentityCMDetail misAccountWithFundID,string fundCode);
    }
}
