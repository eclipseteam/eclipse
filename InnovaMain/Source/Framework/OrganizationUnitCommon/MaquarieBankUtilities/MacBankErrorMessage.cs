﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    public class MacBankErrorMessage
    {
        public string ErrorCode;
        public string ErrorMessage;
        public string ErrorMessageDetail;

        public MacBankErrorMessage(string errorCode, string errorMessage, string errorMessageDetail)
        {
            ErrorCode = errorCode;
            ErrorMessage = errorMessage;
            ErrorMessageDetail = errorMessageDetail;
        }
    }
}
