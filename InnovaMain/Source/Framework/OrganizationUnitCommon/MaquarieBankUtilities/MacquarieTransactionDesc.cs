﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    public class MacquarieTransactionDesc
    {
        public string Code = string.Empty;
        public string TransactionDescriptionDetail = String.Empty;
        public string SystemTransaction = string.Empty; 
        string debitCreditCode = string.Empty;

        public string DebitCreditCode
        {
            get
            {
                if (debitCreditCode == "C")
                    return "Deposit";
                else
                    return "Withdrawal";
            }
            set { debitCreditCode = value; }
        }

        public MacquarieTransactionDesc(string code, string transactionDescriptionDetail, string systemTransaction, string debitCredit)
        {
            Code = code;
            TransactionDescriptionDetail = transactionDescriptionDetail;
            SystemTransaction = systemTransaction;
            DebitCreditCode = debitCredit;
        }
    }
}
