﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    public class MacBankErrorMsgList
    {
        public static readonly List<MacBankErrorMessage> ErrorList = new List<MacBankErrorMessage>()
        {
            new MacBankErrorMessage("V0001","Invalid Account Number","Account number supplied in request is not accessible or does not exist."),
            new MacBankErrorMessage("V0002","Invalid From Date - must be in format YYYY-MM- DD HH:MM","‘From date’ not in correct format."),
            new MacBankErrorMessage("V0003","Invalid To Date - must be in format YYYY-MM- DD HH:MM","‘To date’ not in correct format.)"),
            new MacBankErrorMessage("V0004","You cannot supply a From Date without supplying a To Date or vice versa","Both dates must be supplied in a request using dates, not one or the other.)"),
            new MacBankErrorMessage("V0005","From Date is after To Date","The ‘from date’ must be before ‘to date’.)"),
            new MacBankErrorMessage("V0006","You must supply a From Date and a To Date when no account parameter is supplied","One must supply a date range or account number in this request.)"),
            new MacBankErrorMessage("V0008","Invalid As At Date - must be in format YYYY-MM-DD","‘As at’ date not in correct format.)"),
            new MacBankErrorMessage("V0009","You must supply an As At Date when no account parameter is supplied","One must supply an ‘as at’ date or account number in this request.)"),
            new MacBankErrorMessage("V0023","Invalid request data","The parameters supplied contain invalid characters.)"),
            new MacBankErrorMessage("V0024","You cannot select more than 2 days transactions for all your accounts","If a date range is specified and no account number is specified, the maximum date range that one can request is 2 days.)"),
            new MacBankErrorMessage("V0025","You cannot select more than 20,000 transactions at a time. Please break your request up.",		"The account which you are requesting for has over 20,000 transactions on it. (only shows when account is specified))"),
            new MacBankErrorMessage("V0026","Include Closed Accounts flag must be either Y or N.","The ‘Include closed account’s parameter for the account details extract must be ‘Y’ or ‘N’ (case sensitive).)"),
            new MacBankErrorMessage("E0001","Unidentified Error","This error may be returned if there is a technical issue on our end.)"),
            new MacBankErrorMessage("E0002","Invalid number of parameters have been supplied","You have not supplied required parameters.)"),
            new MacBankErrorMessage("E0003","You are not authorised to access this extract","Either the details of the Extract you supplied (Extract Name & Version) are invalid or you are not authorised to use this extract.)"),
            new MacBankErrorMessage("E0004","Invalid Vendor Code","The Vendor Code supplied is invalid.)"),
            new MacBankErrorMessage("E0005","Invalid Authentication Code or Authentication Password","The Authentication Code and/or the password supplied are invalid.)"),
            new MacBankErrorMessage("E0006","Authentication Code is blocked","This Authentication Code has been blocked from accessing ESI Web Services. This may have been due to an invalid authentication password being supplied over 4 times in a row or manually blocked for another reason.)"),
            new MacBankErrorMessage("E0007","Vendor Code is blocked","This Vendor Code has been blocked from accessing ESI Extract Web Services.)"),
            new MacBankErrorMessage("E0008","Authentication details have expired","The Authentication Code and the Authentication Password supplied have expired. The Adviser/Dealer will need to visit the MAS Website in order to generate new Authentication Details.)"),
            new MacBankErrorMessage("E0009","Unable to process this request at this time.","This error indicates that we are unable to process your request at this point in time. This may be the result of the server being inundated with requests or over a certain number of requests have been received from the requesting authentication details in a time period.)")
        };
    }
}
