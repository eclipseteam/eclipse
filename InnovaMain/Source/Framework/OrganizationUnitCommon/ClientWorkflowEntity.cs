﻿using System;
using System.Collections.Generic;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class ClientWorkflowEntity
    {
        #region DIY

        public bool UseDIYService = false;
        public bool UseDIYMAQ = false;
        public string DIYMaqBSBNo = string.Empty;
        public string DIYMaqACCNo = string.Empty;
        public string DIYMaqACCName = string.Empty;
        public bool UseDIYBWA = false;
        public bool UseDIYDesktopBroker = false;
        public bool UseDIYAMM = false;
        public bool UseDIYFIIG = false;
        public decimal DIYInitialInvestment = 0;

        #endregion

        #region DIWM

        public bool UseDIWMService = false;
        public bool UseDIWMBWA = false;
        public bool UseDIWMMAQ = false;
        public string DIWMMaqBSBNo = string.Empty;
        public string DIWMMaqACCNo = string.Empty;
        public string DIWMMaqACCName = string.Empty;
        public bool UseDIWMDesktopBroker = false;
        public bool UseDIWMAMM = false;
        public bool UseDIWMFIIG = false;
        public bool UseDIWMSS = false;
        public decimal DIWMInitialInvestment = 0;
        public List<Guid> PreferredFunds = new List<Guid>();

        #endregion 

        #region DIFM

        public bool UseDIFMService = false;
        public decimal DIFMInitialInvestment = 0;
        public Guid ModelID = Guid.Empty;
        public bool UseMinBalance = true;
        public bool UseMinTrade = true;
        public bool SellTheHolding = true;
        public decimal MinBalance = 2500;
        public decimal MinBalancePercentage = 0;
        public bool UseMinBalanceCurr = true;
        public bool UseMinBalancePercentage = false;
        public decimal MinTrade = 2500;
        public decimal MinTradePercentage = 0;
        public bool UseMinTradeCurr = true;
        public bool UseMinTradePercentage = false;
        public bool AssignedAllocatedValueHoldSell = true;
        public bool DistributedAllocatedValueHoldSell = false;
        public bool AssignedAllocatedValueExclusion = true;
        public bool DistributedAllocatedValueExclusion = false;
        public bool AssignedAllocatedValueMinTradeHolding = true;
        public bool DistributedAllocatedValueMinTradeHolding = false;

        #endregion

        public string Sig1TFN = string.Empty;
        public string Sig2TFN = string.Empty;
        public string Sig3TFN = string.Empty;
        public string Sig4TFN = string.Empty;
        public string TrustTFN = string.Empty;
        public string ACN = string.Empty;
        public string ABN = string.Empty;
        public List<ExlusionEntity> ExlusionEntityList = new List<ExlusionEntity>();

        #region Checklist

        public bool ProofID = false;
        public bool TrustDeed = false;
        public bool LetterOfAccountant = false;
        public bool BusinessNameReg = false;
        public bool IssueSponsorChessSponsor = false;
        public bool IssueSponsorChessSponsorHoldsingStatements = false;
        public bool OffMarketTransfer = false;
        public bool ChangeOfClient = false;
        public bool BrokerToBroker = false;
        public bool ProofOfAddress = false;

        #endregion
    }
}
