﻿using System;
using System.Collections.Generic;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common
{

    [Serializable]
    public class OrderEntity
    {
        public Guid ID { get; set; }
        public OrderStatus Status { get; set; }
        public OrderType OrderType { get; set; }
        public OrderAccountType OrderAccountType { get; set; }
        public List<OrderItemEntity> Items { get; set; }
        public List<OrderInstructionEntity> Instructions { get; set; }
        public string FileName { get; set; }
        public long FileNameSequanceNumber { get; set; }
        public byte[] Attachment { get; set; }
        /// <summary>
        /// CID of CientCM
        /// </summary>
        public Guid ClientCID { get; set; }
        /// <summary>
        /// ClientID of the Client (example Iv102010)
        /// </summary>
        public string ClientID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public long OrderID { get; set; }
        public long? BatchID { get; set; }
        public string ValidationMessage { get; set; }
        public Guid AccountCID { get; set; }
        public string AccountNumber { get; set; }
        public string AccountBSB { get; set; }
        public OrderItemType OrderItemType { get; set; }
        public string Comments { get; set; }
        public DateTime TradeDate { get; set; }
        public Guid UpdatedByCID { get; set; }
        public string UpdatedBy { get; set; }
        public OrderBulkStatus OrderBulkStatus { get; set; }
        public ClientManagementType ClientManagementType { get; set; }
        public Guid CreatedByCID { get; set; }
        public string CreatedBy { get; set; }
        public OrderPreferedBy OrderPreferedBy { get; set; }
        public string SMARequestID { get; set; }
        public OrderBankAccountType OrderBankAccountType { get; set; }
        public SMACallStatus SMACallStatus { get; set; }
        public byte SMARequestCount { get; set; }
    }
   
}
