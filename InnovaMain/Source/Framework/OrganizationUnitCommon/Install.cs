﻿using System;

namespace Oritax.TaxSimp.Common
{
    public class Install
    {
        #region INSTALLATION PROPERTIES
        // Assembly Installation Properties
        public const string ASSEMBLY_ID = "2B2BE6A4-E554-41AD-B7B3-504B997BBB08";
        public const string ASSEMBLY_NAME = "OrganizationUnitCommon_1_1";
        public const string ASSEMBLY_DISPLAYNAME = "OrganizationUnitCommon V1.1";
        public const string ASSEMBLY_MAJORVERSION = "1";
        public const string ASSEMBLY_MINORVERSION = "1";
        public const string ASSEMBLY_DATAFORMAT = "0";        
        public const string ASSEMBLY_REVISION = "1";

        #endregion
    }
}
