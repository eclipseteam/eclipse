﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
    {
    [Serializable]
    public class ASXPaidOrderEntity
        {
        public Guid ID { get; set; }


        public string ParentAcc { get; set; }
        public string BuySale { get; set; }
        public int QTY { get; set; }
        public string InvestmentCode { get; set; }
        public decimal PriceAVG { get; set; }
        public DateTime TradeDate { get; set; }


        public bool IsSettled { get; set; }
        public string SettelerTransactionID { get; set; }
        public string Status { get; set; }
        public ASXPaidOrderEntity()
            {
            ID = Guid.NewGuid();

            ParentAcc = string.Empty;
            BuySale = string.Empty;
            QTY = 0;
            InvestmentCode = string.Empty;
            PriceAVG = 0;
            TradeDate = DateTime.Now;


            IsSettled = false;
            SettelerTransactionID = string.Empty;
            Status = string.Empty;



            }
        }
    }
