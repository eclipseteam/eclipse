﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.Common
{
    public static class XMLExtensions
    {
        public static void AddNode(string NodeName, string NodeValue, XmlDocument doc)
        {
            RemoveNode(NodeName, doc);
            XmlElement Node = doc.CreateElement(NodeName);
            XmlDocument doc2 = new XmlDocument();
            doc2.LoadXml(NodeValue.ToXmlString());
            XmlElement el = doc2.DocumentElement;
            Node.InnerXml = el.InnerXml;
            doc.DocumentElement.AppendChild(Node);
        }

        public static void RemoveNode(string NodeName, XmlDocument doc)
        {
            XmlNode XNode = null;
            XNode = doc.DocumentElement.SelectSingleNode(NodeName);
            if (XNode != null)
            {
                doc.DocumentElement.RemoveChild(XNode);
            }
        }

        public static void RemoveNodeByTagName(string TagName, XmlDocument doc)
        {
            //Get list of offending nodes
            XmlNodeList Nodes = doc.GetElementsByTagName(TagName);
            //Loop through the list
            while (Nodes.Count != 0)
            {
                foreach (XmlNode Node in Nodes)
                {
                    //Remove the offending node
                    Node.ParentNode.RemoveChild(Node); //<--This line messes with our iteration and forces us to get a new list after each remove
                    //Stop the loop
                    break;
                }
                //Get a refreshed list of offending nodes
                Nodes = doc.GetElementsByTagName(TagName);
            }
        }

        public static void SetNode(List<IdentityCMDetail> entitylist, string ElementNames, string ElementName, XmlDocument doc, ICMBroker Broker)
        {
            string xml = string.Empty;
            IBrokerManagedComponent component = null;
            XmlDocument xmldoc = new XmlDocument();
            XmlElement Elements = xmldoc.CreateElement(ElementName);
            if (entitylist != null)
            {
                foreach (var each in entitylist)
                {
                    component = Broker.GetCMImplementation(each.Clid, each.Csid) as IBrokerManagedComponent;
                    if (component != null)
                    {
                        xml = component.GetDataStream((int)CmCommand.XMLData, ElementName);
                        Elements.InnerXml += SetBusinessTitle("BusinessTitle", each.BusinessTitle.Title, xml);
                    }
                }

                doc.DocumentElement.RemoveChild(doc.DocumentElement.SelectSingleNode(ElementNames));
                XmlElement Node = doc.CreateElement(ElementNames);
                Node.InnerXml = Elements.InnerXml;
                doc.DocumentElement.AppendChild(Node);
            }
        }

        public static void SetNode(List<IdentityCM> entitylist, string ElementNames, string ElementName, XmlDocument doc, ICMBroker Broker)
        {
            string xml = string.Empty;
            IBrokerManagedComponent component = null;
            XmlDocument xmldoc = new XmlDocument();
            XmlElement Elements = xmldoc.CreateElement(ElementName);
            foreach (var each in entitylist)
            {
                component = Broker.GetCMImplementation(each.Clid, each.Csid) as IBrokerManagedComponent;
                xml = component.GetDataStream((int)CmCommand.XMLData, ElementName);
                Elements.InnerXml += xml;
            }

            doc.DocumentElement.RemoveChild(doc.DocumentElement.SelectSingleNode(ElementNames));
            XmlElement Node = doc.CreateElement(ElementNames);
            Node.InnerXml = Elements.InnerXml;
            doc.DocumentElement.AppendChild(Node);
        }

        public static void SetNode(List<AccountRelation> entitylist, string ElementNames, string ElementName, XmlDocument doc, ICMBroker Broker)
        {
            string xml = string.Empty;
            IBrokerManagedComponent component = null;
            XmlDocument xmldoc = new XmlDocument();
            XmlElement Elements = xmldoc.CreateElement(ElementName);
            foreach (var each in entitylist)
            {
                component = Broker.GetCMImplementation(each.Clid, each.Csid) as IBrokerManagedComponent;
                xml = component.GetDataStream((int)CmCommand.XMLData, ElementName);
                Elements.InnerXml += xml;
            }

            doc.DocumentElement.RemoveChild(doc.DocumentElement.SelectSingleNode(ElementNames));
            XmlElement Node = doc.CreateElement(ElementNames);
            Node.InnerXml = Elements.InnerXml;
            doc.DocumentElement.AppendChild(Node);
        }

        public static void SetNode(List<IdentityCMDetail> entitylist, string ElementNames, string ElementName, XmlDocument doc, ICMBroker Broker, string ClientID)
        {
            string xml = string.Empty;
            IBrokerManagedComponent component = null;
            XmlDocument xmldoc = new XmlDocument();
            XmlElement Elements = xmldoc.CreateElement(ElementName);
            foreach (var each in entitylist)
            {
                component = Broker.GetCMImplementation(each.Clid, each.Csid) as IBrokerManagedComponent;
                if (component != null)
                {
                    xml = component.GetDataStream((int)CmCommand.XMLData, ElementName);
                    xml = AddNewElement("InvestorID", ClientID, xml, ElementName);
                    xml = AddNewElement("BusinessTitle", each.BusinessTitle.Title, xml, ElementName);
                    Elements.InnerXml += xml;
                }
            }
            doc.DocumentElement.RemoveChild(doc.DocumentElement.SelectSingleNode(ElementNames));
            XmlElement Node = doc.CreateElement(ElementNames);
            Node.InnerXml = Elements.InnerXml;
            doc.DocumentElement.AppendChild(Node);
        }

        public static void SetParentNode(IdentityCM identitycm, string ElementNames, string ElementName, XmlDocument doc, ICMBroker Broker)
        {
            if (identitycm != IdentityCM.Null)
            {
                string xml = string.Empty;
                IBrokerManagedComponent component = null;
                XmlDocument xmldoc = new XmlDocument();
                XmlElement Elements = xmldoc.CreateElement(ElementName);
                if (identitycm == null) return;
                component = Broker.GetCMImplementation(identitycm.Clid, identitycm.Csid) as IBrokerManagedComponent;
                if (component == null) return;
                xml = component.GetDataStream((int)CmCommand.XMLData, ElementName);
                Elements.InnerXml += xml;

                XmlElement Node;
                if (!string.IsNullOrEmpty(ElementNames))
                {
                    doc.DocumentElement.RemoveChild(doc.DocumentElement.SelectSingleNode(ElementNames));
                    Node = doc.CreateElement(ElementNames);
                    Node.InnerXml = Elements.InnerXml;
                    doc.DocumentElement.AppendChild(Node);
                }
                else
                {
                    Node = doc.CreateElement(ElementName);
                    Node.InnerXml = Elements.InnerXml;
                    doc.DocumentElement.AppendChild(Node);
                }
            }
        }

        public static void SetParentNode(IdentityCM identitycm, string ElementName, XmlDocument doc, ICMBroker Broker)
        {
            if (identitycm != IdentityCM.Null)
            {
                string xml = string.Empty;
                IBrokerManagedComponent component = null;
                XmlDocument xmldoc = new XmlDocument();
                XmlElement Elements = xmldoc.CreateElement(ElementName);
                if (identitycm == null) return;
                component = Broker.GetCMImplementation(identitycm.Clid, identitycm.Csid) as IBrokerManagedComponent;
                if (component == null) return;
                //xml = component.GetDataStream((int)CmCommand.XMLData, ElementName);

                XmlDocument doc2 = new XmlDocument();
                xml = component.Name;
                doc2.LoadXml(xml.ToXmlString());
                XmlElement el = doc2.DocumentElement;

                Elements.InnerXml += el.InnerXml;

                XmlElement Node;
                Node = doc.CreateElement(ElementName);
                Node.InnerXml = Elements.InnerXml;
                doc.DocumentElement.AppendChild(Node);

                Broker.ReleaseBrokerManagedComponent(component);
            }

        }

        public static IdentityCM GetParentIDs(IdentityCM identitycm, ICMBroker Broker)
        {
            IdentityCM ParentIDs = IdentityCM.Null;

            if (identitycm != IdentityCM.Null)
            {
                IBrokerManagedComponent component = null;
                if (identitycm == null) return null;
                component = Broker.GetCMImplementation(identitycm.Clid, identitycm.Csid) as IBrokerManagedComponent;
                if (component == null) return null;
                ParentIDs = (component as ICmHasParent).Parent;
            }

            return ParentIDs;
        }

        public static string SetBusinessTitle(string TagName, string businessTitle, string _xml)
        {
            string xml = _xml;
            XmlDocument doc = new XmlDocument();
            doc.InnerXml = _xml;
            XmlElement ele = doc.CreateElement(TagName);
            ele.InnerText = businessTitle;
            doc.DocumentElement.AppendChild(ele);
            return doc.OuterXml;
        }

        public static string AddNewElement(string TagName, string value, string _xml, string parentNode)
        {
            //string xml = _xml;
            //XmlDocument doc = new XmlDocument();
            //doc.InnerXml = _xml;
            //XmlElement ele = doc.CreateElement(TagName);
            //ele.InnerText = value;
            //doc.DocumentElement.AppendChild(ele);
            //return doc.OuterXml;

            string xml = _xml;
            XmlDocument doc = new XmlDocument();
            doc.InnerXml = _xml;
            XmlNode xNode = doc.CreateNode(XmlNodeType.Element, TagName, "");
            xNode.InnerXml = value;
            doc.GetElementsByTagName(parentNode)[0].InsertAfter(xNode,
            doc.GetElementsByTagName(parentNode)[0].LastChild);
            return doc.OuterXml;
        }
    }
}