﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Utilities;
using System.Data;
using Oritax.TaxSimp.CM;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class IndividualEntity : IClientEntity
    {
        public TitleEntity PersonalTitle { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Fullname { get { return Name + " " + MiddleName + " " + Surname; } set { value = Name + " " + MiddleName + " " + Surname; } }
        public string MiddleName { get; set; }
        public string EmailAddress { get; set; }
        public PhoneNumberEntity WorkPhoneNumber { get; set; }
        public PhoneNumberEntity HomePhoneNumber { get; set; }
        public MobileNumberEntity MobilePhoneNumber { get; set; }
        public string TFN { get; set; }
        public string TIN { get; set; }
        public PhoneNumberEntity Facsimile { get; set; }
        public DateTime? DOB { get; set; }
        public string Password { get; set; }
        public string Occupation { get; set; }
        public string Employer { get; set; }
        public string LicenseNumber { get; set; }
        public string PassportNumber { get; set; }
        public AddressEntity ResidentialAddress { get; set; }
        public AddressEntity MailingAddress { get; set; }
        public bool MailingAddressSame { get; set; }
        public string OrganizationStatus { get; set; }
        public bool HasAddress { get; set; }

        [NonSerialized]
        public bool HasDocAustralianPassport;
        [NonSerialized]
        public bool HasDocForeignPassport;
        [NonSerialized]
        public bool HasDocVEDA;
        [NonSerialized]
        public bool HasDocDrivingLicence;
        [NonSerialized]
        public bool HasDocProofOfAge;

        private DocumentTypeEntity docVEDA;
        private DocumentTypeEntity docAustralianPassport;
        private DocumentTypeEntity docDrivingLicence;
        private DocumentTypeEntity docForeignPassport;
        private DocumentTypeEntity docProofOfAge;
        private DocumentTypeEntity australianCitizenshipCertificate;
        private DocumentTypeEntity australianTaxOfficeNotice;
        private DocumentTypeEntity birthCertificate;
        private DocumentTypeEntity centrelinkHealthCard;
        private DocumentTypeEntity centrelinkPensionCard;
        private DocumentTypeEntity currentForeignDriversLicence;
        private DocumentTypeEntity otherBankStatements;
        private DocumentTypeEntity publicUtilityRecords;

        public DocumentTypeEntity DocProofOfAge
        {
            get { return docProofOfAge; }
            set { docProofOfAge = value; }
        }

        public DocumentTypeEntity DocAustralianPassport
        {
            get { return docAustralianPassport; }
            set { docAustralianPassport = value; }
        }

        public DocumentTypeEntity DocVEDA
        {
            get { return docVEDA; }
            set { docVEDA = value; }
        }

        public DocumentTypeEntity DocDrivingLicence
        {
            get { return docDrivingLicence; }
            set { docDrivingLicence = value; }
        }

        public DocumentTypeEntity DocForeignPassport
        {
            get { return docForeignPassport; }
            set { docForeignPassport = value; }
        }

        public DocumentTypeEntity AustralianTaxOfficeNotice
        {
            get { return australianTaxOfficeNotice; }
            set { australianTaxOfficeNotice = value; }
        }

        public DocumentTypeEntity AustralianCitizenshipCertificate
        {
            get { return australianCitizenshipCertificate; }
            set { australianCitizenshipCertificate = value; }
        }

        public DocumentTypeEntity BirthCertificate
        {
            get { return birthCertificate; }
            set { birthCertificate = value; }
        }

        public DocumentTypeEntity CentrelinkHealthCard
        {
            get { return centrelinkHealthCard; }
            set { centrelinkHealthCard = value; }
        }

        public DocumentTypeEntity CentrelinkPensionCard
        {
            get { return centrelinkPensionCard; }
            set { centrelinkPensionCard = value; }
        }

        public DocumentTypeEntity CurrentForeignDriversLicence
        {
            get { return currentForeignDriversLicence; }
            set { currentForeignDriversLicence = value; }
        }

        public DocumentTypeEntity OtherBankStatements
        {
            get { return otherBankStatements; }
            set { otherBankStatements = value; }
        }

        public DocumentTypeEntity PublicUtilityRecords
        {
            get { return publicUtilityRecords; }
            set { publicUtilityRecords = value; }
        }


        public IdentityCMDetail Advisor { get; set; }
        public string NameWithMiddle
        {
            get { return Name + " " + MiddleName; }
        }

        public DocumentTypeEntityList DocumentTypeEntityList()
        {
            DocumentTypeEntityList documentTypeEntityList = new DocumentTypeEntityList();
            AddDocumentTypeToDocumentList(documentTypeEntityList, ref docAustralianPassport, "Australian Passport (Current or < 2 Years Expired)");
            AddDocumentTypeToDocumentList(documentTypeEntityList, ref australianCitizenshipCertificate, "Australian Citizenship Certificate");
            AddDocumentTypeToDocumentList(documentTypeEntityList, ref australianTaxOfficeNotice, "Australian Tax Office Notice (< 12 Months)");
            AddDocumentTypeToDocumentList(documentTypeEntityList, ref birthCertificate, "Birth Certificate (Australian or Foreign)");
            AddDocumentTypeToDocumentList(documentTypeEntityList, ref centrelinkHealthCard, "Centrelink Health Card");
            AddDocumentTypeToDocumentList(documentTypeEntityList, ref centrelinkPensionCard, "Centrelink Pension Card");
            AddDocumentTypeToDocumentList(documentTypeEntityList, ref docDrivingLicence, "Current Australian Drivers Licence");
            AddDocumentTypeToDocumentList(documentTypeEntityList, ref currentForeignDriversLicence, "Current Foreign Drivers Licence");
            AddDocumentTypeToDocumentList(documentTypeEntityList, ref docForeignPassport, "Current Foreign Passport");
            AddDocumentTypeToDocumentList(documentTypeEntityList, ref otherBankStatements, "Other Bank Statements (< 3 months)");
            AddDocumentTypeToDocumentList(documentTypeEntityList, ref docProofOfAge, "Proof of Age Card issued under law of an Australian State or Territory");
            AddDocumentTypeToDocumentList(documentTypeEntityList, ref publicUtilityRecords, "Public Utility Records - Bill Sighted (< 3 Months)");

            return documentTypeEntityList;
        }

        private void AddDocumentTypeToDocumentList(DocumentTypeEntityList documentTypeEntityList, ref DocumentTypeEntity documentTypeEntity, string typeName)
        {
            if (documentTypeEntity != null && documentTypeEntity.Type != null && documentTypeEntity.Type != String.Empty)
            {
                documentTypeEntityList.Add(documentTypeEntity.Type, documentTypeEntity);
            }
            else
            {
                documentTypeEntity = new DocumentTypeEntity();
                documentTypeEntity.Type = typeName;
                documentTypeEntityList.Add(documentTypeEntity.Type, documentTypeEntity);
            }
        }

        public string Gender { get; set; }

        public List<ClientShare> ClientShares { get; set; }
        
        public void ToAccountHolderDataTable(ref DataRow row, KeyValuePair<string, string> identificationData)
        {
            string strPhone = "";
            bool flag = false;

            if (DOB != null)
            {
                row[AccountHolderDesktopBrokerColumns.DateOfBirth] = (new Utilities.Date(DOB.Value)).ToString();
            }

            row[AccountHolderDesktopBrokerColumns.EmailAddress] = EmailAddress;
            //row[AccountHolderDesktopBrokerColumns.UserName] = EmailAddress;
            string strusername = Name.Replace(" ", "") + DOB.Value.Year.ToString();
            if (strusername.Length > 12)
            {
                row[AccountHolderDesktopBrokerColumns.UserName] = strusername.Substring(0, 12);
            }
            else
            {
                row[AccountHolderDesktopBrokerColumns.UserName] = strusername;
            }
            row[AccountHolderDesktopBrokerColumns.Employer] = Employer;
            row[AccountHolderDesktopBrokerColumns.FamilyName] = Surname;
            row[AccountHolderDesktopBrokerColumns.GivenNames] = Name;

            if (HomePhoneNumber != null)
            {
                strPhone = HomePhoneNumber.ToString().Replace("(", "");
                strPhone = strPhone.ToString().Replace(")", " ");
                strPhone = strPhone.ToString().Replace("+", "");
                strPhone = strPhone.ToString().Replace("-", "");
                strPhone = strPhone.Insert(strPhone.Length, " ");
                //row[AccountHolderDesktopBrokerColumns.HomePhoneNumber] = HomePhoneNumber.ToString();
                row[AccountHolderDesktopBrokerColumns.HomePhoneNumber] = strPhone.ToString();
            }

            row[AccountHolderDesktopBrokerColumns.IdentificationNumber] = identificationData.Value;
            row[AccountHolderDesktopBrokerColumns.IdentificationType] = identificationData.Key;

            strPhone = "";
            if (MobilePhoneNumber != null)
            {
                strPhone = MobilePhoneNumber.ToString().Replace("(", "");
                strPhone = strPhone.ToString().Replace(")", " ");
                strPhone = strPhone.ToString().Replace("+", "");
                strPhone = strPhone.ToString().Replace("-", "");
                strPhone = strPhone.Insert(strPhone.Length, " ");
                //row[AccountHolderDesktopBrokerColumns.MobilePhoneNumber] = MobilePhoneNumber.ToString();
                row[AccountHolderDesktopBrokerColumns.MobilePhoneNumber] = strPhone.ToString();
            }

            row[AccountHolderDesktopBrokerColumns.Occupation] = Occupation;


            if (MailingAddressSame == true)
            {
                MailingAddress = ResidentialAddress;
            }

            if (MailingAddress != null)
            {
                row[AccountHolderDesktopBrokerColumns.Postal_PostCode] = MailingAddress.PostCode;
                row[AccountHolderDesktopBrokerColumns.Postal_State] = Address.GetStateCode(MailingAddress.State);
                row[AccountHolderDesktopBrokerColumns.PostalAddressLine1] = MailingAddress.Addressline1;
                row[AccountHolderDesktopBrokerColumns.PostalAddressLine2] = MailingAddress.Addressline2;
                row[AccountHolderDesktopBrokerColumns.PostalSuburb] = MailingAddress.Suburb;
            }

            row[AccountHolderDesktopBrokerColumns.ResidentForTaxPurpose] = "Y";

            if (ResidentialAddress != null)
            {
                row[AccountHolderDesktopBrokerColumns.Residential_PostCode] = Address.GetStateCode(ResidentialAddress.PostCode);
                row[AccountHolderDesktopBrokerColumns.Residential_State] = Address.GetStateCode(ResidentialAddress.State);
                row[AccountHolderDesktopBrokerColumns.Residential_Suburb] = ResidentialAddress.Suburb;
                row[AccountHolderDesktopBrokerColumns.ResidentialAddressLine1] = ResidentialAddress.Addressline1;
                row[AccountHolderDesktopBrokerColumns.ResidentialAddressLine2] = ResidentialAddress.Addressline2;
            }

            row[AccountHolderDesktopBrokerColumns.TaxFileNumber] = TFN;
            //row[AccountHolderDesktopBrokerColumns.TFNExemption] = "1";
            if (TFN != "")
            {
                row[AccountHolderDesktopBrokerColumns.TFNExemption] = "";
            }
            else
            {
                row[AccountHolderDesktopBrokerColumns.TFNExemption] = "1";
            }

            if (PersonalTitle != null)
                row[AccountHolderDesktopBrokerColumns.Title] = PersonalTitle.Title;

            strPhone = "";
            if (WorkPhoneNumber != null)
            {
                strPhone = WorkPhoneNumber.ToString().Replace("(", "");
                strPhone = strPhone.ToString().Replace(")", " ");
                strPhone = strPhone.ToString().Replace("+", "");
                strPhone = strPhone.ToString().Replace("-", "");
                strPhone = strPhone.Insert(strPhone.Length, " ");
                //row[AccountHolderDesktopBrokerColumns.WorkPhoneNumber] = WorkPhoneNumber.ToString();
                row[AccountHolderDesktopBrokerColumns.WorkPhoneNumber] = strPhone.ToString();
            }

            if (HomePhoneNumber != null)
            {
                if (HomePhoneNumber.ToString() != "")
                {
                    row[AccountHolderDesktopBrokerColumns.PreferredContact] = "1";
                    flag = true;
                }
            }

            if (WorkPhoneNumber != null && flag == false)
            {
                if (WorkPhoneNumber.ToString() != "")
                {
                    row[AccountHolderDesktopBrokerColumns.PreferredContact] = "2";
                    flag = true;
                }
            }

            if (MobilePhoneNumber != null && flag == false)
            {
                if (MobilePhoneNumber.ToString() != "")
                {
                    row[AccountHolderDesktopBrokerColumns.PreferredContact] = "3";
                    flag = true;
                }
            }
            if (EmailAddress != null && flag == false)
            {
                if (EmailAddress.ToString() != "")
                {
                    row[AccountHolderDesktopBrokerColumns.PreferredContact] = "4";
                    flag = true;
                }
            }


        }

        public IndividualEntity()
        {
            PersonalTitle = new TitleEntity();
            WorkPhoneNumber = new PhoneNumberEntity();
            HomePhoneNumber = new PhoneNumberEntity();
            MobilePhoneNumber = new MobileNumberEntity();
            Facsimile = new PhoneNumberEntity();
            ResidentialAddress = new AddressEntity();
            MailingAddress = new AddressEntity();
            DocVEDA = new DocumentTypeEntity();
            DocAustralianPassport = new DocumentTypeEntity();
            DocDrivingLicence = new DocumentTypeEntity();
            DocForeignPassport = new DocumentTypeEntity();
            DocProofOfAge = new DocumentTypeEntity();
            Advisor = new IdentityCMDetail();
            ClientShares=new List<ClientShare>();
        }

     
    }


    public interface IHasIndividualEntity
    {
        Oritax.TaxSimp.Common.IndividualEntity Individual { get; set; }
    }
}
