﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Enum
{
    public enum CmCommand
    {
        Add_Single,
        Add_List,
        Remove_Single,
        Remove_List,
        GetLightEntity,
        AttachToParent = 100,
        DeAttachFromParent = 200,
        GetIncludedIFAList = 400,
        GetIncludedAdvisorList = 401,
        GetIncludedClientList = 402,
        EditOrganizationUnit = 500,
        GetEntitySecurity = 501,
        AddEntitySecurity = 502,
        DeleteEntitySecurity = 503,
        XMLData = 911,
        FullName = 999,
        Email = 1000,
        CSVData = 1024,
        UpdateClientID = 2001,
        AccountRelationGetList = 1025,
        AccountRelationGetEntity = 1026,
        AccountRelationAdd = 1027,
        AccountRelationAddReference = 1028,
        AccountRelationUpdate = 1029,
        AccountRelationUpdateRefrence = 1030,
        AccountRelationDelete = 1031,
        AccountRelationDeleteRefrence = 1032,
        GetAdminAccontRelation = 1033,
        GetAdminAccontListByType = 1034,
        IncludedBankAccountGetList = 1035,
        ResetMISFunds = 1036,
        MarkExported = 11000,
        GetBTWRAPCode = 12000,
        UpdateApplicationID = 2002,
        SPAddLibrary = 19001,
        SPUpdateLibrary = 19002,
        SPDeleteLibrary = 19003,
        MarkExportedSS = 11001,
        MarkExportedDB = 11002,
        BSB = 11003,
        AccountNumber = 11004,
        AccountProcessFundCodeProductID = 11005,
        AccountProcessInvestmentCodeProductID = 11006,
        MatchFileName = 11007,
        PhoneCityCode = 11008,
        PhoneNumber = 11009,
        FullPhoneNumber = 11010,
        CheckASXInvestmentCode = 11011,
        GetType = 11012,
        GetAccountType=11013,
    }


    public enum AssociatedOrganizationStatus
    {
        AttachedToClient,
        AttachedToProduct,
        AttachedToCientAndProduct
    }

}
