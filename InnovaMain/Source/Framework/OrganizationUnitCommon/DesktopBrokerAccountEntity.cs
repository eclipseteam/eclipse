﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Oritax.TaxSimp.CM;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using System.Collections.ObjectModel;
using Oritax.TaxSimp.Extensions;
namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class DesktopBrokerAccountEntity:IClientEntity
    {
        public string Name { get; set; }
        public string AccountNumber { get; set; }
        public string OrganizationStatus { get; set; }
        public string AccountDesignation { get; set; }
        public string HINNumber { get; set; }
        public List<AccountRelation> LinkedAccounts { get; set; }
        public ObservableCollection<HoldingTransactions> holdingTransactions
        {
            get;
            set;
        }

        public ObservableCollection<HoldingBalance> HoldingBalances
        {
            get;
            set;
        }
        public DateTime UploadTime { get; set; }
        public ObservableCollection<HoldingTransactions> PaidOrdersTranactions { get; set; }
        public ObservableCollection<ASXUnSettledOrderEntity> UnSettledOrders { get; set; }
        public ObservableCollection<HoldingTransactions> DeletedHoldingTransactions
        {
            get;
            set;
        }
        public DesktopBrokerAccountEntity()
        {
            LinkedAccounts = new List<AccountRelation>();
            holdingTransactions = new ObservableCollection<HoldingTransactions>();
            HoldingBalances=new ObservableCollection<HoldingBalance>();
            PaidOrdersTranactions = new ObservableCollection<HoldingTransactions>();
            UnSettledOrders = new ObservableCollection<ASXUnSettledOrderEntity>();
            DeletedHoldingTransactions = new ObservableCollection<HoldingTransactions>();
        }

    }
    [Serializable]
    public class HoldingTransactions
    {

        public ObservableCollection<AdjustmentTransaction> AdjustmentTransactions
        {
            get;
            set;
        }
        public string InvestmentCode { get; set; }
        public string Id { get; set; }
        public string Delete { get; set; }

        public string Type { get; set; }

        public DateTime TradeDate { get; set; }

        public decimal Units { get; set; }

        public decimal GrossValue { get; set; }
        public decimal NetValue { get; set; }
        public decimal CostBase { get; set; }

        public string TransactionStatus { get; set; }
        public DateTime? SettlementDate { get; set; }

        public decimal FeesTotal { get; set; }

        public decimal BrokerageAmount { get; set; }
        public decimal BrokerageGST { get; set; }

        public string Narrative { get; set; }
        public string FPSInstructionID { get; set; }
        public string ReversedTransactionID { get; set; }

        public string Currency { get; set; }
        public string TransactionType { get; set; }
        public bool Confirmed { get; set; }

        public decimal Charges { get; set; }
        public string Status { get; set; }
        public decimal UnitPrice
        {
            get 
            {
                if (Units != 0)
                    return GrossValue / Units;
                else
                    return 0;
            }
        }

        [NonSerialized]
        public decimal AdjustedUnits = 0; 

        public decimal Brokerage
        {
            get { return BrokerageAmount - BrokerageGST; }
        }
      
        public Guid TranstactionUniqueID { get; set; }


        public static readonly List<TransactionType> TransactionTypes = new List<TransactionType>()
{
new TransactionType( "VA", "Reversal - Redemption / Withdrawal / Fee / Tax" ,AllowablePolarityType.Positive), 
new TransactionType( "AE", "Rights Issue" ,AllowablePolarityType.Positive), 
new TransactionType( "RA", "Sale" ,AllowablePolarityType.Negative), 
new TransactionType( "ST", "Surcharge Tax" ,AllowablePolarityType.Negative), 
new TransactionType( "AS", "Switch In" ,AllowablePolarityType.Positive), 
new TransactionType( "RS", "Switch Out" ,AllowablePolarityType.Negative), 
new TransactionType( "RX", "Tax" ,AllowablePolarityType.Negative), 
new TransactionType( "AR", "Tax Rebate" ,AllowablePolarityType.Positive), 
new TransactionType( "AT", "Transfer In" ,AllowablePolarityType.Positive), 
new TransactionType( "RT", "Transfer Out" ,AllowablePolarityType.Negative), 
new TransactionType( "RW", "Withdrawal" ,AllowablePolarityType.Negative), 
new TransactionType( "RJ", "Adjustment Up" ,AllowablePolarityType.Positive), 
new TransactionType( "AJ", "Adjustment Down" ,AllowablePolarityType.Negative), 
new TransactionType( "AP", "Application" ,AllowablePolarityType.Positive), 
new TransactionType( "AM", "Bonus Issue" ,AllowablePolarityType.Positive), 
new TransactionType( "AN", "Buy / Purchase" ,AllowablePolarityType.Positive), 
new TransactionType( "RU", "Commutation" ,AllowablePolarityType.Negative), 
new TransactionType( "AC", "Contribution" ,AllowablePolarityType.Positive), 
new TransactionType( "RL", "Contribution Tax" ,AllowablePolarityType.Negative), 
new TransactionType( "AD", "Deposit" ,AllowablePolarityType.Positive), 
new TransactionType( "EX", "Expense" ,AllowablePolarityType.Negative), 
new TransactionType( "IN", "Opening (Initial) Balance" ,AllowablePolarityType.NegativeOrPositive), 
new TransactionType( "LM", "Instalment" ,AllowablePolarityType.Positive), 
new TransactionType( "RH", "Insurance Premium" ,AllowablePolarityType.Negative), 
new TransactionType( "AL", "Lodgement" ,AllowablePolarityType.Positive), 
new TransactionType( "RK", "Lump Sum Tax" ,AllowablePolarityType.Negative), 
new TransactionType( "MA", "Manager Fee" ,AllowablePolarityType.Negative), 
new TransactionType( "RY", "PAYG" ,AllowablePolarityType.Negative), 
new TransactionType( "RP", "Pension Payment" ,AllowablePolarityType.Negative), 
new TransactionType( "RI", "Pension Payment – Income Stream" ,AllowablePolarityType.Negative), 
new TransactionType( "PR", "Purchase Reinvestment" ,AllowablePolarityType.Positive), 
new TransactionType( "RR", "Redemption" ,AllowablePolarityType.Negative), 
new TransactionType( "VB", "Reversal - Buy / Purchase / Deposit" ,AllowablePolarityType.Negative)

};
        public bool IsSettled { get; set; }
        public string SettelerTransactionID { get; set; }
        public string SettledStatus { get; set; }
        public TransectionStatus DividendStatus { get; set; }
        public Guid DividendID { get; set; }

        public HoldingTransactions()
        {
            TranstactionUniqueID = Guid.NewGuid();
            IsSettled = false;
            SettelerTransactionID = string.Empty;
            Status = string.Empty;
            DividendID = Guid.Empty;
            DividendStatus = TransectionStatus.Loaded;

        }


        /*
         * INNOVA-1339
         * Sam Quan 2013-11-28
         */
        public bool IsDividentMatchWithTrans(DividendEntity dv, decimal dvunits, decimal asOfDateUnitPrice)
        {
            /* 1.	ASX code is the same as dividend security code
             * 2.	ASX transaction type is “Adjustment Up”
             * 3.	Trade date for  ASX transaction is within 1-5 days from dividend payment date
             * 4.	ABS (“Paid dividend amount”/ “share price” – “ASX Units” ) <  1
             *      “Paid dividend amount” is value in Paid dividend column in dividend transaction
             *      “Share price” is last known share price for this security on the date less or equal to Trade date
             *      “ASX Units” is value in Units column in ASX transaction.   
             */

            bool isAdjustupTans = this.TransactionType == "Adjustment Up";
            bool issameinvestmentcode = this.InvestmentCode == dv.InvestmentCode;
            bool hasDateInRange = dv.PaymentDate.HasValue && dv.PaymentDate.Value.IsMatched(this.TradeDate, 3);
            bool isunitMatching = asOfDateUnitPrice > 0 && this.Units.IsAbsLessThanEquals((decimal)((dvunits * (decimal)dv.CentsPerShare) / asOfDateUnitPrice), 1);
            bool isNotMatched = this.DividendStatus != TransectionStatus.Matched;
            bool isMatchedWithSameDiv = this.DividendID != Guid.Empty && this.DividendID == dv.ID; // if dividend is already matched with same transactions
            
            bool criteria1 = isAdjustupTans && issameinvestmentcode && hasDateInRange && isunitMatching && (isNotMatched || isMatchedWithSameDiv);

            /*
                Criteria 2 (new)
                1.	ASX code is the same as dividend security code
                2.	ASX transaction type is “Adjustment Up”
                3.	Trade date for ASX transaction = dividend payment date
             */
            bool areDatesSame = dv.PaymentDate.HasValue && (dv.PaymentDate.Value - this.TradeDate).Days == 0;
            bool criteria2 = isAdjustupTans && issameinvestmentcode && areDatesSame && (isNotMatched || isMatchedWithSameDiv);

            return criteria1 || criteria2;

        }

       

    }
    [Serializable]
    public class AdjustmentTransaction
    {
        public string InvestmentCode { get; set; }
        public Guid TransactionID { get; set; }
        public Guid ParentID { get; set; }
        public string Type { get; set; }
        public DateTime TradeDate { get; set; }
        public decimal Units { get; set; }
        public decimal NetValue { get; set; }
        public decimal BrokerageAmount { get; set; }
        public string TransactionType { get; set; }
        public decimal UnitPrice  { get; set; }
   
        public static readonly List<TransactionType> TransactionTypes = new List<TransactionType>()
{
new TransactionType( "VA", "Reversal - Redemption / Withdrawal / Fee / Tax" ,AllowablePolarityType.Positive), 
new TransactionType( "AE", "Rights Issue" ,AllowablePolarityType.Positive), 
new TransactionType( "RA", "Sale" ,AllowablePolarityType.Negative), 
new TransactionType( "ST", "Surcharge Tax" ,AllowablePolarityType.Negative), 
new TransactionType( "AS", "Switch In" ,AllowablePolarityType.Positive), 
new TransactionType( "RS", "Switch Out" ,AllowablePolarityType.Negative), 
new TransactionType( "RX", "Tax" ,AllowablePolarityType.Negative), 
new TransactionType( "AR", "Tax Rebate" ,AllowablePolarityType.Positive), 
new TransactionType( "AT", "Transfer In" ,AllowablePolarityType.Positive), 
new TransactionType( "RT", "Transfer Out" ,AllowablePolarityType.Negative), 
new TransactionType( "RW", "Withdrawal" ,AllowablePolarityType.Negative), 
new TransactionType( "RJ", "Adjustment Up" ,AllowablePolarityType.Positive), 
new TransactionType( "AJ", "Adjustment Down" ,AllowablePolarityType.Negative), 
new TransactionType( "AP", "Application" ,AllowablePolarityType.Positive), 
new TransactionType( "AM", "Bonus Issue" ,AllowablePolarityType.Positive), 
new TransactionType( "AN", "Buy / Purchase" ,AllowablePolarityType.Positive), 
new TransactionType( "RU", "Commutation" ,AllowablePolarityType.Negative), 
new TransactionType( "AC", "Contribution" ,AllowablePolarityType.Positive), 
new TransactionType( "RL", "Contribution Tax" ,AllowablePolarityType.Negative), 
new TransactionType( "AD", "Deposit" ,AllowablePolarityType.Positive), 
new TransactionType( "EX", "Expense" ,AllowablePolarityType.Negative), 
new TransactionType( "IN", "Opening (Initial) Balance" ,AllowablePolarityType.NegativeOrPositive), 
new TransactionType( "LM", "Instalment" ,AllowablePolarityType.Positive), 
new TransactionType( "RH", "Insurance Premium" ,AllowablePolarityType.Negative), 
new TransactionType( "AL", "Lodgement" ,AllowablePolarityType.Positive), 
new TransactionType( "RK", "Lump Sum Tax" ,AllowablePolarityType.Negative), 
new TransactionType( "MA", "Manager Fee" ,AllowablePolarityType.Negative), 
new TransactionType( "RY", "PAYG" ,AllowablePolarityType.Negative), 
new TransactionType( "RP", "Pension Payment" ,AllowablePolarityType.Negative), 
new TransactionType( "RI", "Pension Payment – Income Stream" ,AllowablePolarityType.Negative), 
new TransactionType( "PR", "Purchase Reinvestment" ,AllowablePolarityType.Positive), 
new TransactionType( "RR", "Redemption" ,AllowablePolarityType.Negative), 
new TransactionType( "VB", "Reversal - Buy / Purchase / Deposit" ,AllowablePolarityType.Negative)

};

        public AdjustmentTransaction()
        {
            TransactionID = Guid.NewGuid();
        }

        public HoldingTransactions GetHoldingTransactionObjFromAdjustmentTransaction()
        {
            HoldingTransactions item = new HoldingTransactions();
            item.TranstactionUniqueID = this.TransactionID;
            item.TransactionType = this.TransactionType;
            item.Type = this.Type;
            item.TradeDate = this.TradeDate;
            item.Units = this.Units;
            item.GrossValue = this.UnitPrice * this.Units;
            item.NetValue = this.NetValue;
            item.BrokerageAmount = this.BrokerageAmount;

            return item;  
        }
    }
    [Serializable]
    public class HoldingBalance
    {
        public string InvestmentCode { get; set; }
        public decimal Actual { get; set; }
        public decimal Unconfirmed { get; set; }
        public DateTime? AsAtDate { get; set; }
    }
    [Serializable]
    public class TransactionType
    {
        public string Type { get; set; }
        public string TransactionDescription { get; set; }
        public AllowablePolarityType AllowablePolarity { get; set; }

        public TransactionType(string type, string transactionDescription, AllowablePolarityType allowablePolarity)
        {
            this.Type = type;
            this.TransactionDescription = transactionDescription;
            this.AllowablePolarity = allowablePolarity;
        }
    }

    public enum AllowablePolarityType
    {
        Negative,
        Positive,
        NegativeOrPositive,
    }
}
