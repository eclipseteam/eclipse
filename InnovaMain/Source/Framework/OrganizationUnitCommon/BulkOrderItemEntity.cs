﻿using System;
using System.Collections.Generic;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class BulkOrderItemEntity
    {
        public Guid ID { get; set; }
        public Guid BulkOrderID { get; set; }
        public List<OrderItemPair> OrderItems { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string ContractNote { get; set; }
        public bool IsApportioned { get; set; }

        public string Code { get; set; }
        public decimal? ExpectedUnitPrice { get; set; }
        public decimal? ExpectedUnits { get; set; }
        public decimal? ExpectedAmount { get; set; }
        public decimal? ActualUnitPrice { get; set; }
        public decimal? ActualUnits { get; set; }
        public decimal? ActualAmount { get; set; }
        public decimal? Brokerage { get; set; }
        public decimal? Charges { get; set; }
        public decimal? Tax { get; set; }
        public decimal? GrossValue { get; set; }
    }

    [Serializable]
    public class TDBulkOrderItemEntity
    {
        public Guid ID { get; set; }
        public Guid BulkOrderID { get; set; }
        public List<OrderItemPair> OrderItems { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string ContractNote { get; set; }
        public bool IsApportioned { get; set; }

        public Guid BrokerId { get; set; }
        public Guid InstituteID { get; set; }
        public string Term { get; set; }
        public decimal? Rate { get; set; }
        public decimal? Amount { get; set; }
    }

    [Serializable]
    public class OrderItemPair
    {
        public Guid OrderID { get; set; }
        public Guid OrderItemID { get; set; }
    }
}
