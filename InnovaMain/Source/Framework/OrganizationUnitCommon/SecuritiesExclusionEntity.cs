﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class ExlusionEntity
    {
        public ExlusionEntity(string code, string description, string investmentAction, Guid ID)
        {
            this.InvestmentAction = investmentAction;
            this.Code = code;
            this.Description = description;
            this.ID = ID;
        }
        public string Code { get; set; }
        public string InvestmentAction { get; set; }
        public string Description { get; set; }
        public Guid ID { get; set; }
    }
}
