﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class OrderPadBankWestInstructionEntity : OrderInstructionEntity
    {
        public string CustomerName { get; set; }
        public string AccountNumber { get; set; }
        public string BSB { get; set; }
        public decimal? Amount { get; set; }
        public string Reference_Narration {get;set;}
        public BankWestInstructionType BWInstructionType { get; set; }
    }
}
