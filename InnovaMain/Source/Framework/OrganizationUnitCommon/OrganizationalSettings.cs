﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class OrganizationalSettings
    {
        public bool SendToSMAService { get; set; }
        public bool ReadFromSMAService { get; set; }
        public OrganizationalSettings()
        {
            SendToSMAService = true;
            ReadFromSMAService = true;
        }
    }
}
