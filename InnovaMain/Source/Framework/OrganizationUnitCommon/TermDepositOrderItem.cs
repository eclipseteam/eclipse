﻿using System;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public class TermDepositOrderItem : OrderItemEntity
    {
        public Guid BrokerID { get; set; }
        public Guid BankID { get; set; }
        public decimal? Min { get; set; }
        public decimal? Max { get; set; }
        public string Rating { get; set; }
        public string Duration { get; set; }
        public decimal? Percentage { get; set; }
        public Guid ProductID { get; set; }
        public Guid TDAccountCID { get; set; }
        public string ContractNote { get; set; }

    }
}
