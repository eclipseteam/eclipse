﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Common
    {
    [Serializable]
        public class BankAccountPaidOrderEntity
            {
            public decimal Amount { get; set; }
            public string Status { get; set; }
            public Guid ID { get; set; }
            public DateTime RequiredDate { get; set; }
            public string ReferenceNarration { get; set; }
            public bool IsSettled { get; set; }
            public string SettelerTransactionID { get; set; }


            public BankAccountPaidOrderEntity()
        {
            ID = Guid.Empty;
            RequiredDate = DateTime.Now;
            Status = string.Empty;

            ReferenceNarration = string.Empty;
            IsSettled =false;
            SettelerTransactionID = string.Empty;
            
            Amount = 0;
            
        }
            }
    }
