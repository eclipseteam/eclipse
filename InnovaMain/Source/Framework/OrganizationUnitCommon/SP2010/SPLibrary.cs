﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Proxies;

namespace Oritax.TaxSimp.Common
{
    [Serializable]
    public static class SPLibrary
    {
        public static void AddLibraryToClient(string user, string libraryname)
        {
            SharepointProxy.CreateDocumentLibrary(user, libraryname);
        }
    }
}
