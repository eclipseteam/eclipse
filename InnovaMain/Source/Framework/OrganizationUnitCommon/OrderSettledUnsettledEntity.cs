﻿using System;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Common
{

    [Serializable]
    public class OrderSettledUnsettledEntity
    {
        public Guid ID { get; set; }
        public Guid ClientCID { get; set; }
        public string ClientID { get; set; }
        public Guid AccountCID { get; set; }
        public string AccountNo { get; set; }
        public string AccountBsb { get; set; }
        public Guid ProductID { get; set; }
        public string InvestmentCode { get; set; }
        public Guid OrderID { get; set; }
        public long OrderNo { get; set; }
        public Guid OrderItemID { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Units { get; set; }
        public decimal? UnitPrice { get; set; }
        public OrderSetlledUnsettledType Type { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public Guid SettledAssociation { get; set; }
        public Guid TransactionID { get; set; }
        public DateTime TradeDate { get; set; }
        public TradeType TradeType { get; set; }
        public SetlledunSettledEntryType EntryType { get; set; }
        public SetlledunSettledTransactionType TransactionType { get; set; }
        public OrderAccountType AccountType { get; set; }

        public decimal? Percentage { get; set; }
        public Guid BrokerId { get; set; }
        public Guid BankId { get; set; }
        public string Duration { get; set; }
        public OrderAccountType OrderType { get; set; }
        public decimal? BrokrageFee { get; set; }

        public ClientManagementType ClientManagementType { get; set; }

        public OrderSettledUnsettledEntity GetConfirmationTransaction(Guid transactionID, DateTime tradeDate, decimal amount, decimal units)
        {
            var entity = new OrderSettledUnsettledEntity
                             {
                                 ID = Guid.NewGuid(),
                                 ClientCID = ClientCID,
                                 ClientID = ClientID,
                                 AccountCID = AccountCID,
                                 AccountNo = AccountNo,
                                 AccountBsb = AccountBsb,
                                 ProductID = ProductID,
                                 InvestmentCode = InvestmentCode,
                                 OrderID = OrderID,
                                 OrderNo = OrderNo,
                                 OrderItemID = OrderItemID,
                                 Amount = amount, //Math.Abs(amount) * (Amount >= 0 ? -1 : 1),//invert amount
                                 Units = units,  //Math.Abs(units) * (Units >= 0 ? -1 : 1),//invert Units
                                 UnitPrice = UnitPrice,
                                 Type = Type,
                                 CreatedDate = DateTime.Now,
                                 UpdatedDate = null,
                                 SettledAssociation = ID,
                                 TransactionID = transactionID,
                                 TradeDate = tradeDate,
                                 TradeType = TradeType,
                                 EntryType = EntryType,
                                 TransactionType = SetlledunSettledTransactionType.Confirmation,
                                 AccountType = AccountType,
                                 Percentage = Percentage,
                                 BrokerId = BrokerId,
                                 BankId = BankId,
                                 Duration = Duration,
                                 OrderType = OrderType,
                                 BrokrageFee = BrokrageFee,
                                 ClientManagementType = ClientManagementType,
                             };
            return entity;
        }
    }
}
