using System;
using Oritax.TaxSimp.CM.Entity;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// EntityNodeEventArgs class 
	/// </summary>
	public class EntityNodeEventArgs : ReportingUnitEventArgs
	{
		#region Private members--------------------------------------------------------------------

		private IEntity entity; 
	
		#endregion 
	
		#region Public properties------------------------------------------------------------------
		/// <summary>
		/// Gets and sets entity object
		/// </summary>
		public IEntity Entity
		{
			get
			{
				return this.entity; 
			}
			set
			{
				this.entity = value; 
			}
		}

		#endregion 
	}
}
