using System;
using Oritax.TaxSimp.CM.OrganizationUnit;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// ReportingUnitEventArgs Class
	/// </summary>
	public class ReportingUnitEventArgs : NodeEventArgs
	{
		#region Private members--------------------------------------------------------------------
 
		private IOrganizationUnit organisationUnit;
		private Guid clid; 
		private Guid csid; 

		#endregion 
	
		#region Public properties------------------------------------------------------------------
		/// <summary>
		/// Gets or sets CLID for reporting unit
		/// </summary>
		public Guid CLID
		{
			get
			{
				return this.clid; 
			}
			set
			{
				this.clid = value; 
			}
		}
		/// <summary>
		/// Gets or sets CSID for reporting unit
		/// </summary>
		public Guid CSID
		{
			get
			{
				return this.csid; 
			}
			set
			{
				this.csid = value; 
			}
		}
		/// <summary>
		/// Gets or sets organistaion unit object
		/// </summary>
		public IOrganizationUnit OrganisationUnit
		{
			get
			{
				return this.organisationUnit; 
			}
			set
			{
				this.organisationUnit = value; 
			}
		}
		#endregion 

		#region Support functions------------------------------------------------------------------

		/// <summary>
		/// Check if reporting unit contains scenario. 
		/// </summary>
		/// <param name="ScenarioName"></param>
		/// <returns></returns>
		public bool ContainsScenario(string ScenarioName)
		{
			if(ScenarioName != null)
				return true;

			return false;
		}
		/// <summary>
		/// Check if reporting unit contains scenario. 
		/// </summary>
		/// <param name="ScenarioID"></param>
		/// <returns></returns>
		public bool ContainsScenario(Guid ScenarioID)
		{
			if(ScenarioID == Guid.Empty)
				return true;

			return false;
		}
		#endregion 
	}
}
