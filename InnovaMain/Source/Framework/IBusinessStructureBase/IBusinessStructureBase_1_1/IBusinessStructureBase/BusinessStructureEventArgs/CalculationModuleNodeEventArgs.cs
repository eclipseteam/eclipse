using System;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// CalculationModuleNodeEventArgs class
	/// </summary>
	public class CalculationModuleNodeEventArgs : NodeEventArgs
	{
		#region Private members--------------------------------------------------------------------
		
		private ICalculationModule cm; 
	
		#endregion 

		#region Public properties------------------------------------------------------------------
		/// <summary>
		/// Gets and sets Calculation Module object
		/// </summary>
		public ICalculationModule CalculationModule
		{
			get
			{
				return this.cm; 
			}
		}

		#endregion 

        #region Constructors
        public CalculationModuleNodeEventArgs(ICalculationModule calculationModule)
        {
            this.cm = calculationModule;
        }
        #endregion
    }
}
