using System;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Exceptions;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// Summary description for NodeEventArgs.
	/// </summary>
	public class NodeEventArgs : EventArgs
	{
		#region Private members--------------------------------------------------------------------
		
		private bool continueAcrossTree; 
		private bool continueDownTree;
		
		#endregion 

		#region Public properties------------------------------------------------------------------
		/// <summary>
		/// Gets or sets continueAcrossTree
		/// </summary>
		public bool ContinueAcrossTree
		{
			get
			{
				return this.continueAcrossTree; 
			}
			set
			{
				this.continueAcrossTree = value; 
			}
		}
	
		/// <summary>
		/// Gets or sets continueDownTree
		/// </summary>
		public bool ContinueDownTree
		{
			get
			{
				return this.continueDownTree; 
			}
			set
			{
				this.continueDownTree = value; 
			}
		}
		#endregion 
	
		#region Methods----------------------------------------------------------------------------
		/// <summary>
		/// Passes message object with operation warning
		/// </summary>
		/// <param name="message"></param>
		/// <param name="confirmType"></param>
		public void OperationWarning(MessageBoxDefinition message, string confirmType)
		{
			throw new GetUserConfirmationException(message,confirmType);
		}
		/// <summary>
		/// Passes message object with operation failure
		/// </summary>
		/// <param name="message"></param>
		public void OperationFailure(MessageBoxDefinition message)
		{
		
		}

		#endregion 
	}
	
}
