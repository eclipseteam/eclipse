using System;
using Oritax.TaxSimp.CM.Group;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// GroupNodeEventArgs class 
	/// </summary>
	public class GroupNodeEventArgs : ReportingUnitEventArgs
	{
		#region Private members--------------------------------------------------------------------

		private IGroup group;
		
		#endregion 

		#region Public members---------------------------------------------------------------------
		/// <summary>
		/// Gets and sets GroupCM object 
		/// </summary>
		public IGroup Group
		{
			get
			{
				return this.group; 
			}
			set
			{
				this.group = value; 
			}
		}
		#endregion

		#region Support functions------------------------------------------------------------------
		/// <summary>
		///Returns true if it has any scenarios.  
		/// </summary>
		/// <param name="scenarioName"></param>
		/// <param name="scenarioID"></param>
		/// <returns></returns>
		public bool ContainsMembers(string scenarioName, Guid scenarioID)
		{
			return true;				
		}
		/// <summary>
		///Returns true if it has any scenarios.  
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="scenarioID"></param>
		/// <returns></returns>
		public bool ContainsMembers(Guid memberID, Guid scenarioID)
		{
			return true; 		
		}
		#endregion
	}
}
