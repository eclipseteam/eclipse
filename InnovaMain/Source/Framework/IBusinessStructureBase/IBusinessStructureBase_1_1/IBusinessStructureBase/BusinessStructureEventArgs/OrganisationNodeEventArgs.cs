using System;
using Oritax.TaxSimp.CM.Organization;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// OrganisationNodeEventArgs event argumments
	/// </summary>
	public class OrganisationNodeEventArgs : NodeEventArgs
	{
		#region Private membes---------------------------------------------------------------------

		private IOrganization organization; 
		
		#endregion 
		
		#region Public properties------------------------------------------------------------------
		
		/// <summary>
		/// Gets or sets organisation object type. 
		/// </summary>
		public IOrganization Organization
		{
			get
			{
				return this.organization; 
			}
			set
			{
				this.organization= value; 
			}
		}

		#endregion 

		#region Support functions------------------------------------------------------------------
		/// <summary>
		/// Returns true if organisation exists using reportingUnitID. 
		/// </summary>
		/// <param name="reportingUnitID"></param>
		/// <returns></returns>
		private bool ContainsReportingUnit(Guid reportingUnitID)
		{
			return true; 
		}
		/// <summary>
		/// Returns true if organisation exists using reporting unit name check. 
		/// </summary>
		/// <param name="reportingUnitName"></param>
		/// <returns></returns>
		private bool ContainsReportingUnit(string reportingUnitName)
		{
			return true; 
		}

		#endregion 
	}
}
