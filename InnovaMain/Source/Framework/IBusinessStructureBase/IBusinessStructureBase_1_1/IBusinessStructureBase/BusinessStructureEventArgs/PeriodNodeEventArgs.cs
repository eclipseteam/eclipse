using System;
using Oritax.TaxSimp.CM.Period;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// PeriodNodeEventArgs event argument class
	/// </summary>
	public class PeriodNodeEventArgs : NodeEventArgs
	{
		#region Private members--------------------------------------------------------------------

		private IPeriod period; 
		private Guid clid; 
	
		#endregion 
	
		#region Public properties------------------------------------------------------------------
		/// <summary>
		/// Gets or sets CLID for reporting unit
		/// </summary>
		public Guid CLID
		{
			get
			{
				return this.clid; 
			}
			set
			{
				this.clid = value; 
			}
		}
		/// <summary>
		/// Returns and sets period object
		/// </summary>
		public IPeriod Period
		{
			get
			{
				return this.period; 
			}
			set
			{
				this.period = value; 
			}
		}

		#endregion 

		#region Othere functions-------------------------------------------------------------------
		/// <summary>
		/// Checks if CM exist by using Type name
		/// </summary>
		/// <param name="TypeName"></param>
		/// <returns></returns>
		public bool ContainsCM(string TypeName)
		{
			return true; 
		}
		/// <summary>
		/// Checks if CM exist by using component ID
		/// </summary>
		/// <param name="CTID"></param>
		/// <returns></returns>
		public bool ContainsCM(Guid CTID)
		{	
			return true; 
		}

		#endregion 
	}
}
