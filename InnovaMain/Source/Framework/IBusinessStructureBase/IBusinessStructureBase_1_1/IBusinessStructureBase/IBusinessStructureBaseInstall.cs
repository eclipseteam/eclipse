using System;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
	public class IBusinessStructureBaseInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="90400719-2C40-4be7-9206-D26CD6284C24";
		public const string ASSEMBLY_NAME="IBusinessStructureBase_1_1";
		public const string ASSEMBLY_DISPLAYNAME="IBusinessStructureBase V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
//		public const string ASSEMBLY_REVISION="0"; 
		public const string ASSEMBLY_REVISION="1"; //2005.1 

		#endregion
	}
}
