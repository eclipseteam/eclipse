using System;
using Oritax.TaxSimp.BusinessStructureUtilities;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// Summary description for IBusinessStructureNavigatorVisitor.
	/// </summary>
	public interface IBusinessStructureNavigatorVisitor
	{
        /// <summary>
        /// Returns assemnbly namme e.g. ATOFORMS_1_1, ATOFORMS_3_1 etc
        /// </summary>
		BusinessStructureNavigationDirection NavigationDirection
		{
			get;
		}
       	/// <summary>
		/// Visits organisation node. Implementaion is not required for ATO MIGRATION PUSH DOWN. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void VisitOrganisationNode(object sender, OrganisationNodeEventArgs e);
		/// <summary>
		/// The following method visit's group object. 
		/// 1. It validates business structure and add information to Exception list. 
		/// 2. It navigates period(s). 
		/// 3. It navigates to member(s). 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void VisitGroupNode(object sender, GroupNodeEventArgs e);
		/// <summary>
		/// Visits Entity node. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void VisitEntityNode(object sender, EntityNodeEventArgs e);
		/// <summary>
		/// Visits period object. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void VisitPeriodNode(object sender, PeriodNodeEventArgs e);
		/// <summary>
		/// The following method visits Calculation node. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void VisitCalculationNode(object sender, CalculationModuleNodeEventArgs e);
		/// <summary>
		/// Check to see which level is Navigator at. 
		/// </summary>
		/// <param name="currentLevel"></param>
		/// <returns></returns>
		bool ShouldNavigateToLowerLevels(BusinessStrucutureNavigatorLevel currentLevel);
		/// <summary>
		/// The following method returns true/false. It checks whether object of reporting unit type is not in exception list e.g. 
		/// Entity A is member of one or more groups or it has wrong ATO configured. If a reporting unit had wrong ATO configured 
		/// it will not visit that object. 
		/// </summary>
		/// <param name="reportingUnitCLID"></param>
		/// <param name="reportingUnitCSID"></param>
		/// <returns></returns>
		bool ShouldVisitReportingUnitNode(Guid reportingUnitCLID, Guid reportingUnitCSID);
		/// <summary>
		/// Checks if it needs to visit period object
		/// </summary>
		/// <param name="periodDate"></param>
		/// <returns></returns>
		bool ShouldVisitPeriodNode(PeriodSpecifier periodDate);
		/// <summary>
		/// Check is navigator should visit calculation module node.
		/// </summary>
		/// <param name="typeName"></param>
		/// <returns></returns>
		bool ShouldVisitCalculationModule(string typeName);

		/// <summary>
		/// Increments the number of nested group levels that have been visited
		/// at this point in the structure.
		/// </summary>
		void IncrementGroupLevelsVisited();

		/// <summary>
		/// Decrements the number of nested group levels that have been visited
		/// at this point in the structure.
		/// </summary>
		void DecrementGroupLevelsVisited();
	}
}
