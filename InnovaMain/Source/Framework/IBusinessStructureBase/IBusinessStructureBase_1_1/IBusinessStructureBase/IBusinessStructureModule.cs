using System;
using System.Collections;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CalculationInterface
{
	public enum IsSubscriberType { True, False, Unknown }

	public interface IBusinessStructureModule : ICalculationModule
	{
		/// <summary>
		/// The following function acts as access point for Navigator object type of IBusinessStructureNavigatorVisitor, through out business structure objects e.g. entity   
		/// </summary>
		/// <param name="navigator"></param>
		void PerformBusinessStructreNavigationOperation(IBusinessStructureNavigatorVisitor navigator);
		void PostMessage(IMessage message);
		void DeliverMessage(IMessage message);
		void OnInternalSubscriptionUpdate(IMessage message);
		bool FilterMessage(IMessage message,Object[] parameters);
		IsSubscriberType IsSubscriber(IMessage message, SubscriptionsList objSubscriptions, Guid subscriberCLID, Guid subscriberCSID);
	}
}
