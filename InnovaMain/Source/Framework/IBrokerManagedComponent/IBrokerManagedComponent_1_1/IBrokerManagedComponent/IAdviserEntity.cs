﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM
{
    public interface IAdviserEntity
    {
         String Name { get; set; }
         String LegalName { get; set; }
         String Surname { get; set; }
         String AuthorisedRepNo { get; set; }
         PhoneNumberEntity HomePhoneNumber { get; set; }
         PhoneNumberEntity WorkPhoneNumber { get; set; }
         MobileNumberEntity MobilePhoneNumber { get; set; }
         PhoneNumberEntity Facsimile { get; set; }
         String TFN { get; set; }
         String Email { get; set; }
         DateTime? AppointedDate { get; set; }
         String Status { get; set; }
         DateTime? TerminatedDate { get; set; }
         String ReasonTerminated { get; set; }
         String Gender { get; set; }
         String PreferredName { get; set; }
         String Title { get; set; }
         String Occupation { get; set; }
         String BTWRAPAdviserCode { get; set; }
         String DesktopBrokerAdviserCode { get; set; }
         String OrganizationStatus { get; set; }
         String ClientId { get; set; }
         string AFSLicenseeName { get; set; }
         string AFSLicenceNumber { get; set; }
         string BankWestAdvisorCode { get; set; }
         string FullName { get; }
    }
}
