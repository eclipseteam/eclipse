using System;
using System.Security.Principal;
using Oritax.TaxSimp.BusinessStructureUtilities;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// Interface for Event.
	/// </summary>
	public interface IEvent
	{
		void LogEvent(int eventType, string eventDetails, string userInfo);
				
	}
}
