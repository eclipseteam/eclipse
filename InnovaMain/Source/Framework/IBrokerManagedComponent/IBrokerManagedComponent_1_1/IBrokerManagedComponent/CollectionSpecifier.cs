using System;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// Summary description for CollectionSpecifier.
	/// </summary>
	public abstract class CollectionSpecifier
	{
		public Guid TypeID;

		public CollectionSpecifier(Guid typeID)
		{
			TypeID=typeID;
		}
	}
}
