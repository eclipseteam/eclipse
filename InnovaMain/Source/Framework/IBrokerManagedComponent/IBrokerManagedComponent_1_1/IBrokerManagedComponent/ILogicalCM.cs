using System;
using System.Data;
using System.Collections;

using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Utilities;
using System.Collections.Generic;

namespace Oritax.TaxSimp.CalculationInterface
{
	public interface ILogicalModule : IBrokerManagedComponent
	{
		ICalculationModule this[Guid cSID]{get;}

		Guid CLID{get;}
		Guid CMType{get;set;} 
		string CMTypeName{get;set;} 
		string CMCategory{get;set;} 
		Guid CurrentScenario{get;set;}

		Guid ParentCLID{get;set;}
		ArrayList ChildCLIDs{get;}

		ILogicalModule ParentLCM{get;}
		ILogicalModule[] ChildLCMs{get;}
		BusinessStructureContext CurrentBusinessStructureContext{get;set;}

		Guid GetCIID(Guid cSID);
		ICalculationModule GetParentCM(Guid cSID);
		ArrayList GetChildCMs(Guid cSID);
		ArrayList GetChildCMsByType(Guid gdTypeId, Guid cSID);

		IEnumerable Scenarios{get;}
		CMScenario GetScenario(Guid cSID);
		
		SubscriptionsList Subscriptions{get;}

		ICalculationModule AddChildLogicalCM(ILogicalModule childLCM);
		ICalculationModule AddChildLogicalCM(ILogicalModule childLCM,Guid cSID);
		void RemoveChildLogicalCM(ILogicalModule childLCM, Guid cSID);
		void RemoveChildLogicalCM(ILogicalModule childLCM);
		CMScenario AddScenario(CMScenario cMScenario);
		CMScenario CreateScenario(string scenarioName,Guid newCSID,ScenarioType type,Guid cIID,ScenarioStatus status,DateTime creationDateTime,DateTime modificationDateTime);
		CMScenario CreateScenario(string scenarioName,Guid newCSID,ScenarioType type,Guid cIID,ScenarioStatus status,DateTime creationDateTime,DateTime modificationDateTime, bool locked);
		CMScenario CreateCopyScenario(string scenarioName,Guid newCSID,ScenarioType type,Guid copiedCSID,ScenarioStatus status,DateTime creationDateTime,DateTime modificationDateTime);
		void DivergeScenario(ICalculationModule iCM);
		void DeleteScenario(Guid cSID);
		bool IsSubscriber(ICalculationModule businessStructureCM,IMessage message, Guid cLID);
		bool IsSubscriber(ICalculationModule businessStructureCM,IMessage message, Guid subscriberCLID, Guid subscriberCSID, Guid subscribedCSID);
		bool IsShared(Guid cSID);
		bool IsScenarioLocked(Guid cSID);
		bool ContainsLockedScenario();
		bool IsConsolidationOwnerLocked(Guid cSID);

		/// <summary>
		/// Gets a CMScenario for a given scenario ID
		/// </summary>
		/// <param name="scenarioID">The scenario ID to get</param>
		/// <returns>The CMScenario for the specified ID</returns>
		CMScenario GetCMScenario(Guid scenarioID);

		/// <summary>
		/// Lock the specified scenario
		/// </summary>
		/// <param name="scenarioID">The ID of the scenario to lock</param>
		void LockScenario(Guid scenarioID);

		/// <summary>
		/// Unlock the specified scenario
		/// </summary>
		/// <param name="scenarioID">The ID of the scenario to unlock</param>
		void UnLockScenario(Guid scenarioID);

		/// <summary>
		/// Adds an owner of this logical module
		/// </summary>
		/// <param name="ownerCLID">The CLID of the owner</param>
		/// <param name="ownerCSID">The CSID of the owner</param>
		/// <param name="memberCSID">The CSID of the member which is a member of the group</param>
		/// <example>A group is the owner of an entity if it is a member of that group</example>
		void AddOwner(Guid ownerCLID, Guid ownerCSID, Guid memberCSID);


		/// <summary>
		/// Removes an owner of this logical module
		/// </summary>
		/// <param name="ownerCLID">The CLID of the owner</param>
		/// <param name="ownerCSID">The CSID of the owner</param>
		/// <param name="memberCSID">The CSID of the member which is a member of the group</param>
		void RemoveOwner(Guid ownerCLID, Guid ownerCSID, Guid memberCSID);

		void Subscribe(string dataType,Guid subscriberCLID);
		void Subscribe(bool blConsol,Guid subscriberCLID, Guid subscribedCLID);
		void Subscribe( bool bolConsolidation, Guid objSubscriberCLID, Guid objSubscribedCLID, Guid objSubscriberCSID, Guid objSubscribedCSID );
		void Subscribe( bool bolSubscription, Guid objSubscriberCLID,Guid objSubscriberCSID, Object[] objParameters );
		void Subscribe( string dataType, Guid objSubscriberCLID,Guid objSubscriberCSID, Object[] objParameters );
		void Subscribe( string dataType, Guid objSubscriberCLID, Guid objSubscriberCSID );
		void UnSubscribe(bool blConsolidation, Guid subscriberCLID,Guid subscriberCSID);
		void UnSubscribe(string dataType, Guid subscriberCLID, Guid subscriberCSID);
		void UnSubscribe(Guid objSubscriberCLID, Guid objSubscribedCLID, Guid objSubscriberCSID, Guid objSubscribedCSID );
		void UnSubscribe(Guid subscriberCLID);
		void UnSubscribe(Guid subscriberCLID, Guid subscriberCSID);
		void UnSubscribeGL(Guid subscriberCLID,Guid subscriberCSID, IEnumerable defaultSubscriptions);
		PrivilegeOption CheckPrivilege(PrivilegeType privilege,Guid userCID);

		bool ScenarioLockOverridden{get;set;}

        /// <summary>
        /// Gets a list of CMScenarios that are children of this particular logical modules
        /// </summary>
        /// <param name="cSID">The scenario to return items for</param>
        /// <returns>A list of CMScenarios</returns>
        List<CMScenario> GetChildCMScenarios(Guid cSID);
	}
}
