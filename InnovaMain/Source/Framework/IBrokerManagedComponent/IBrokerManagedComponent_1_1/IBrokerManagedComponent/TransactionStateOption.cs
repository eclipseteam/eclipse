using System;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// Indicates the state of the transaction
	/// </summary>
	public enum TransactionStateOption
	{
		/// <summary>
		/// There is no transaction
		/// </summary>
		None			=0,

		/// <summary>
		/// The transaction has started
		/// </summary>
		Started			=1,

		/// <summary>
		/// The transaction is complete
		/// </summary>
		Complete		=3,

		/// <summary>
		/// The transaction has been aborted
		/// </summary>
		Abort			=5
	}
}
