using System;
using System.Data;

using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CalculationInterface
{
	public interface IBrokerManagedComponent
	{
		#region INSTALLATION PROPERITES
		string InstallTypeID
		{
			get;
		}

		string InstallTypeName
		{
			get;
		}

		string InstallDisplayName
		{
			get;
		}

		string InstallCategory
		{
			get;
		}

		string InstallAssemblyName
		{
			get;
		}

		string InstallClassName
		{
			get;
		}

		int InstallMajVersion
		{
			get;
		}

		int InstallMinVersion
		{
			get;
		}

		int InstallRelease
		{
			get;
		}

		string InstallPersisterAssembly
		{
			get;
		}

		string InstallPersisterClass
		{
			get;
		}
		#endregion

		bool Modified
		{
			get;
			set;
		}

        string EclipseClientID
        {
            get;
            set;
        }

        string OtherID
        {
            get;
            set;
        }

        StatusType StatusType
        {
            get;
            set;
        }

        CMStatus CMStatus
        {
            get;
            set;
        }

		string Name
		{
			get;
			set;
		}

		Guid CID
		{
			get;
			set;
		}

		Guid TypeID
		{
			get;
			set;
		}

		string TypeName
		{
			get;
			set;
		}

		IComponent ComponentInformation
		{
			get;
		}

		IComponentVersion ComponentVersionInformation
		{
			get;
		}

		string CategoryName
		{
			get;
			set;
		}

		DataSet PrimaryDataSet
		{
			get;
		}

		Token PersistedUpdateToken
		{
			get;
		}

		Token UpdateToken
		{
			get;
		}

		/// <summary>
		/// Indicates that the component will not be persisted
		/// </summary>
		bool TransientInstance
		{
			get;
		}

		/// <summary>
		/// Only to be called by the broker to reset update token to persisted
		/// update token so that when it loads a bmc from disk it is not marked
		/// as modified
		/// </summary>
		void ResetTokenToPersistedToken();

		string VersionString
		{
			get;
		}
		
		bool MigrationInProgress
		{
			get;
			set;
		}

		ICMBroker Broker
		{
			get;
		}

		void OnCopyInstance();
        string GetAllDataStream(int type);
        string GetAttachmentURL(string attachmentBMCID);
        string GetDataStream(int type, string data);
        string AddDataStream(int type, string data);
        string UpdateDataStream(int type, string data);
        string DeleteDataStream(int type, string data);

		void GetData(DataSet data);
		void SetData(DataSet data);
		void ExtractData(DataSet data);
		void DeliverData(DataSet data);
		void PrePersistActions();
		void PostPersistActions();
		void Initialize(String name);
		Token CalculateToken(bool Set);
		string ExtractXML();
		void ExportXMLToFile(string fileName);
		string DeliverXML(string xml, bool verify, IMigrationDataMemento memento);
		void DeliverXMLFromFile(string fileName, IMigrationDataMemento memento);
		void DeliverPrimaryData(DataSet data);
		bool IsCustomSerialized();
		void MigrationCompleted();
		void MigrationCompleted(IMigrationDataMemento memento);
		void Resubscribe();
		void DeletionOccurring();
        bool InPlaceUpgradeInProgress
        {
            get;
            set;
        }
		/// <summary>
		/// Apply changes for this component to the dataset
		/// </summary>
		/// <param name="data"></param>
		/// <param name="resetOnFinish"></param>
		void ApplyUpdateData(DataSet data, bool resetOnFinish);

		/// <summary>
		/// A module overrides this method to be notified when data update changes
		/// should be accepted, for example after having been loaded from the database
		/// </summary>
		void MarkModuleAsUnmodified();
    }
}
