using System;

namespace Oritax.TaxSimp.CalculationInterface
{
	public interface IMapTargetSM : IBrokerManagedComponent
	{
		bool CheckIfTargetCategoryInUse(string targetDescription, string targetCategory);
	}
}
