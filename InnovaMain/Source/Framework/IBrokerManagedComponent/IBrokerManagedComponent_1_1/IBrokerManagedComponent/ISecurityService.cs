﻿using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CalculationInterface
{
    public interface ISecurityService
    {
        void AddUser(UserEntity user);
    }
}
