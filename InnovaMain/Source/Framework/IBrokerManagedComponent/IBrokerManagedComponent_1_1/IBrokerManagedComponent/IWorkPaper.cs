using System;
using System.Collections.Specialized;

namespace Oritax.TaxSimp.CalculationInterface
{
	public enum WorkPaperType
	{
		Supporting,
		Primary,
		Aggregation,
		Consolidation,
		ConsolidationCorrection,
		Report,
        Adjustments,
        ConsolidationAdjusted,
        AggregationAdjusted,
	}

	public enum WorkPaperViewType
	{
		Specific, // view is specific for a workpaper
		Common	  // view is shared by several workpapers	
	}
	public class WorkpaperViews
	{
		/// <summary>
		/// This method populates provides the list of group views and their WorkPaperType indexes 
		/// in the order they are added to the list:
		/// </summary>
		/// <returns> ("2","Aggregation"),("3","Consolidation"),("4","Correction")</returns>
		public static OrderedDictionary GetGroupViews()
		{
			OrderedDictionary namesList = new OrderedDictionary();
			namesList.Add(((int)WorkPaperType.Consolidation).ToString(), "Consolidation");
			namesList.Add(((int)WorkPaperType.ConsolidationCorrection).ToString(), "Correction");
			namesList.Add(((int)WorkPaperType.Aggregation).ToString(), "Aggregation");
			return namesList;
		}
	}
	/// <summary>
	/// Summary description for IWorkPaper.
	/// </summary>
	public interface IWorkPaper
	{
		ICMBroker Broker
		{
			get;
		}
	}
}
