﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CalculationInterface
{
    public interface IEventLogEntry
    {
        Guid ID { get; }
        DateTime Time { get; }
        EventType Type { get; }
        string UserDetails { get; }
    }
}
