﻿using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CalculationInterface
{
    public interface IApplicationUserService
    {
        void AddUser(UserEntity current, ApplicationUser user);
    }
}
