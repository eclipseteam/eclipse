using System;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
	public class Install
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="084D5531-1670-4f6c-B86E-0A1B09615A85";
		public const string ASSEMBLY_NAME="IBrokerManagedComponent_1_1";
		public const string ASSEMBLY_DISPLAYNAME="IBrokerManagedComponent V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
//		public const string ASSEMBLY_REVISION="0";
		public const string ASSEMBLY_REVISION="1"; //2005.1

		#endregion
	}
}
