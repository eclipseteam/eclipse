using System;
using System.Collections;
using System.Data;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// Interface supported by a CMType object.
	/// </summary>
	public interface ICMType : IBrokerManagedComponent
	{
		//bool Modified{get;set;}
		ICMBroker Owner{get;set;}
	}
}
