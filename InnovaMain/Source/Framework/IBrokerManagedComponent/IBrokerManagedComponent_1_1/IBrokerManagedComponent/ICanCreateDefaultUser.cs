﻿using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CalculationInterface
{
    public interface ICanCreateDefaultUser
    {
        ApplicationUser GetDefaultUser();
    }
}
