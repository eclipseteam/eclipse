﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.CalculationInterface
{
    public interface IEventLogQuery : IEnumerable<IEventLogEntry>
    {
        int Count { get; }
        IEventLogEntry this[int index] { get; }
    }
}
