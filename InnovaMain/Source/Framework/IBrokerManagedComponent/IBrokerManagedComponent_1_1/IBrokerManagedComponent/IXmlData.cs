﻿
namespace Oritax.TaxSimp.CalculationInterface
{
    public interface IXmlData
    {
        string GetXmlData();
    }
}
