using System;
using System.Collections;
using System.Data;
using Oritax.TaxSimp.Security;

using Oritax.TaxSimp.BusinessStructureUtilities;

namespace Oritax.TaxSimp.CalculationInterface
{
	public interface ICalculationModule : IBrokerManagedComponent
	{
		Guid CMID
		{
			get;
			set;
		}

		Guid CIID
		{
			get;
			set;
		}

		Guid CLID
		{
			get;
			set;
		}

		Guid CSID
		{
			get;
			set;
		}

		/// <summary>
		/// Returns a collection of WorkPaperData objects each consisting of a name
		/// and relative URL.
		/// </summary>
		IEnumerable WorkPapers
		{
			get;
		}

		IEnumerable PrintableWorkPapers
		{
			get;
		}

		string EditWorkPaper
		{
			get;
		}

		string OpenWorkPaper
		{
			get;
		}

		string ConfigureWPURL
		{
			get;
		}

		ScenarioStatus Status
		{
			get;
			set;
		}

		IEnumerable Subscriptions
		{
			get;
		}

		BusinessStructureContext CurrentBusinessStructureContext
		{
			get;
			set;
		}

		bool ScenarioLockOverridden
		{
			get;
			set;
		}

		/// <summary>
		/// Indicates if the CM is joining a parent module
		/// </summary>
		bool IsJoining
		{
			get;
		}

		/// <summary>
		/// Indicates if the CM is leaving a parent module
		/// </summary>
		bool IsLeaving
		{
			get;
		}

		void PerformCalculation();
		void PublishMessage(IMessage message);
		void PublishValidationMessage(IMessage message, bool validCondition);
		void SetSubscriptionData(IMessage message);
		void ChangeName(string newName);
		string ConstructBreadcrumb();
		void OnJoinCalculationSet();
		void OnLeaveCalculationSet();
		void EndLeaveCalculationSet(); 
		DataSet CreateWPInterfaceClass(string className);
		void InitialiseBusinessStructureContext();
		PrivilegeOption CheckPrivilege(PrivilegeType privilege,Guid userCID);

		/// <summary>
		/// SJD Created 3/3/2003  This member function publishes GL messages
		/// GL Enabled CMs should publish GL messages
		/// </summary>
		void PublishGL();

		void PublishCrossPeriod();
		void PublishCrossPeriodStreamEnd();
        void PublishOnPeriodDatesChange(DateTime startDate, DateTime endDate);

		bool IsScenarioLocked();
		ILogicalModule GetLogicalModule();
	}

	
	public struct MapTargetCollection
	{
		private ArrayList collection;
		public ArrayList Collection { get { return collection; } }

		public Category AddCategory(string id, string description)
		{
			if(collection==null) collection=new ArrayList();
			Category c=new Category(id, description, this);
			collection.Add(c);

			return c;
		}

		public struct Category
		{
			private ArrayList targets;
			public ArrayList MapTargets { get { return targets; } }

			private MapTargetCollection collection;
			public MapTargetCollection GetCollection()
			{
				return collection;
			}

			private string id;
			public string Id { get { return id; } }

			private string description;
			public string Description { get { return description; } }

			public Category(string id, string description, MapTargetCollection collection)
			{
				this.targets=new ArrayList();
				this.collection=collection;
				this.id=id;
				this.description=description;
			}

			public Category AddCategory(string id, string description)
			{
				return collection.AddCategory(id, description);
			}

			public Category AddTarget(string id, string datatype, string description, string comment)
			{
				targets.Add(new MapTarget(id, datatype, description, comment, this));
				return this;
			}
		}

		public struct MapTarget
		{
			private Category category;
			public Category Category { get { return category; } }

			private string id;
			public string Id { get { return id; } }

			private string description;
			public string Description { get { return description; } }

			private string datatype;
			public string DataType { get { return datatype; } }

			private string comment;
			public string Comment { get { return comment; } }

			public MapTarget(string id, string datatype, string description, string comment, Category category)
			{
				this.id=id;
				this.datatype=datatype;
				this.description=description;
				this.comment=comment;
				this.category=category;
			}
		}
	}
}
