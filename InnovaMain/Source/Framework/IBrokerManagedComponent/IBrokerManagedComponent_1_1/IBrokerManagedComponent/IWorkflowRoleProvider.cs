﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.CalculationInterface
{
    public interface IWorkflowRoleProvider
    {
        IEnumerable<Guid> GetAllInRole(Guid roleId);
        bool IsInRole(Guid identityId, Guid roleId);
    }
}
