using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Oritax.TaxSimp.CalculationInterface
{
	public enum PersisterActionType
	{
		Save,
		Delete
	}
	/// <summary>
	/// Summary description for ICPersister.
	/// </summary>
	public interface ICPersister : IDataModelManagement
	{
		SqlConnection Connection{set;}
		SqlTransaction Transaction{set;}
		IBrokerManagedComponent Get(Guid instanceId, ref DataSet persistedPrimaryDS);
		void Hydrate(IBrokerManagedComponent iBMC,Guid instanceId,  ref DataSet persistedPrimaryDS);
		void Save(IBrokerManagedComponent iBMC, ref DataSet persistedPrimaryDS);
		void Delete(IBrokerManagedComponent iBMC);
		void FindDependentBMCs(Guid cID,ref ArrayList dependentCIDs, PersisterActionType actionType);
		ICMBroker Broker{set;get;}
		DataSet FindDetailsByTypeId(Guid typeId, object [] parameters);
		DataTable ListBMCCollection(CollectionSpecifier bMCCollectionSpecifier);

		IBrokerManagedComponent GetQualifiedList(object qualifier);
		void DeleteByQualifier(object qualifier);

		void ClearDatabase();

		void EstablishDatabase(CMTypeInfo typeInfo);
		void ClearDatabase(CMTypeInfo typeInfo);
		void RemoveDatabase(CMTypeInfo typeInfo);
	}

    public interface ICPersisterVelocity : ICPersister
    {
        void Save(IBrokerManagedComponent iBMC, ref DataSet persistedPrimaryDS, ITransactionVelocity trx);
    }

}
