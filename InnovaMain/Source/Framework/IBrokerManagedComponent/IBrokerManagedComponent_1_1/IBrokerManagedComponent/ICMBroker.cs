using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Security.Principal;
using System.Transactions;

using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.Utilities;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.CalculationInterface
{
    public enum WellKnownCM
    {
        Organization = 0,
        Licencing = 1,
        SecurityProvider = 2,
        OBP=3,
    }

    public struct CMTypeInfo
    {
        public string TypeName;
        public string DisplayName;
        public Guid ID;
        public string AssemblyName;
        public string ClassName;
        public string Category;
        public int Applicability;
        public int majorVersion;
        public int minorVersion;
        public int release;
        public string pAssembly;
        public string pClass;
        public bool Obsolete;

        // SJD 5/2/2002 Added pAssembly and pClass 
        public CMTypeInfo(
            Guid ID,
            string TypeName,
            string DisplayName,
            string Category,
            int Applicability,
            string AssemblyName,
            string ClassName,
            int majorVersion,
            int minorVersion,
            int release,
            string passembly,
            string pclass,
            bool obsolete)
        {
            this.ID = ID;
            this.TypeName = TypeName;
            this.DisplayName = DisplayName;
            this.Category = Category;
            this.Applicability = Applicability;
            this.AssemblyName = AssemblyName;
            this.ClassName = ClassName;
            this.majorVersion = majorVersion;
            this.minorVersion = minorVersion;
            this.release = release;
            this.pAssembly = passembly;
            this.pClass = pclass;
            this.Obsolete = obsolete;
        }
    }


    public interface ITransaction
    {
        /// <summary>
        /// The ID of the transaction
        /// </summary>
        Guid TID
        {
            get;
        }

        /// <summary>
        /// The SQL transaction used by this transaction
        /// </summary>
        SqlTransaction SQLTransaction
        {
            get;
        }

        /// <summary>
        /// The SQL connection used by this transaction
        /// </summary>
        SqlConnection SQLConnection
        {
            get;
            set;
        }

        /// <summary>
        /// The broker associated with this transaction
        /// </summary>
        ICMBroker Broker
        {
            get;
        }

        /// <summary>
        /// The current state of the transaction
        /// </summary>
        TransactionStateOption State
        {
            get;
        }

        /// <summary>
        /// Indicates if a transaction is a long running transaction
        /// </summary>
        bool IsLongRunningTransaction
        {
            get;
            set;
        }

        /// <summary>
        /// Enlists a BMC in the transaction
        /// </summary>
        /// <param name="cid">ID of BMC to enlist</param>
        /// <param name="initialRefCount">The reference count</param>
        /// <exception cref="System.ArgumentException">cid cannot be an empty guid</exception>
        void EnlistBMC(Guid cid, int initialRefCount);

        /// <summary>
        /// Enlists a BMC in the transaction
        /// </summary>
        /// <param name="bmc">BMC to enlist</param>
        /// <param name="initialRefCount">The reference count</param>
        /// <exception cref="System.ArgumentException">cid cannot be an empty guid</exception>
        void EnlistBMC(IBrokerManagedComponent bmc, int initialRefCount);

        /// <summary>
        /// Removes a BMC as being referenced by this transaction (important: see remarks)
        /// </summary>
        /// <remarks>
        /// This is used to remove something when for example it is deleted or when the CID changes (divergance)
        /// it should be used very carefully, most times you will want to use ReleaseBMC
        /// </remarks>
        /// <param name="cid">The CID of the BMC to de-enlist</param>
        /// <returns>The ref count of the item being removed</returns>
        /// <exception cref="System.ArgumentException">cid cannot be an empty guid</exception>
        int DeEnlistBMC(Guid cid);

        /// <summary>
        /// Releases a BMC from the transaction when it is no longer needed by a piece of code
        /// </summary>
        /// <param name="cid">The CID of the BMC to release</param>
        /// <exception cref="System.ArgumentException">cid cannot be an empty guid</exception>
        /// <exception cref="System.InvalidOperationException">A BMC has been released more times than it has been requested, or a BMC which has never been requested by this transaction</exception>
        void ReleaseBMC(Guid cid);

        /// <summary>
        /// Starts the transaction running
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Can't start a transaction which is already started</exception>
        void Start();

        /// <summary>
        /// Commits the current transaction
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Can't complete a transaction which is not started</exception>
        void Complete();

        /// <summary>
        /// Rolls back the transaction
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Can't complete a transaction which is not started</exception>
        void Rollback();

        /// <summary>
        /// Removes all cache entries enlisted in this transaction
        /// </summary>
        /// <remarks>
        /// Used by the rollback operation to remove those items from the cache
        /// which may be invalid because they have been used by a rolled-back
        /// transaction
        /// </remarks>
        void RemoveInUseCacheEntries();

        /// <summary>
        /// Gets a list of the instance ids that are enlisted in the transaction
        /// </summary>
        /// <returns>
        /// A list of the instance IDs that are currently enlisted in the transaction
        /// </returns>
        Guid[] GetCIDsEnlisted();

        /// <summary>
        /// Indicates if a particular instance id can be collected, e.g. is not in use currently in the transaction
        /// </summary>
        /// <param name="cid">The instance ID of the module to check</param>
        /// <returns>True if the instance ID either has never been involoved in the transaction or is not currently in use, false otherwise</returns>
        /// <exception cref="System.ArgumentException">cid cannot be an empty guid</exception>
        bool CanBeCollected(Guid cid);

        /// <summary>
        /// Indicates if a specified instance ID has been involved in the current transaction
        /// </summary>
        /// <param name="cid">The instance ID to check</param>
        /// <returns>True if the instance ID has been involved in the transaction</returns>
        /// <exception cref="System.ArgumentException">cid cannot be an empty guid</exception>
        bool IsEnlisted(Guid cid);

        /// <summary>
        /// Get BrokerManagedComponent from the list of components within the transaction
        /// </summary>
        /// <param name="cid">id of the requested component</param>
        /// <returns>null if component not found</returns>
        IBrokerManagedComponent GetComponent(Guid cid);
    }

    public interface ITransactionVelocity : ITransaction
    {
        void EnlistRM(IEnlistmentNotification rm, string sourceFlag);

        //bool IsSQLTransactionDisposed
        //{
        //    get;
        //    set;
        //}
    }

    /// <summary>
    /// The broker is the interface between the business objects and the framework
    /// </summary>
    public interface ICMBroker
    {
        void SetStart();
        void SetComplete();
        void SetComplete(bool installationsChanged);
        void SetAbort();
        void SetWriteStart();
        void SetRequestStatusProvider(RequestProcessStatusProvider statusProvider);

        void RegisterIDChange(Guid oldCID, Guid newCID);

        bool MigrationInProgress
        {
            get;
            set;
        }

        bool SaveOverride
        {
            get;
            set;
        }

        bool InPlaceUpgradeInProgress
        {
            get;
            set;
        }

        Guid GetCMInstanceID(Guid objCLID, Guid objCSID);
        ILogicalModule CreateLogicalCM(Guid typeId, string cmName, Guid cSID, BusinessStructureContext objContext);
        ILogicalModule CreateLogicalCM(string typeName, string cmName, Guid cSID, BusinessStructureContext objContext);
        ICMType CreateCMType(CMTypeInfo cMTypeDetails);
        void DeleteAllTypes();
        ICalculationModule CopyCMInstance(Guid cIID, Guid cSID);
        ICalculationModule UpdateCMInstance(Guid cIID, Guid cTID);
        IBrokerManagedComponent GetBMCInstance(string strName, string strType);
        IBrokerManagedComponent GetBMCInstance(Guid typeId, string cmName);
        IBrokerManagedComponent GetBMCInstance(Guid cID);
        IEnumerable GetBMCList(string typeName);
        IEnumerable GetBMCList(Guid typeId);
        IEnumerable GetBMCList();

        IBrokerManagedComponent CreateComponentInstance(Guid typeID, string instanceName);
        IBrokerManagedComponent CreateComponentInstance(Guid instanceID, Guid typeID, string instanceName);
        IBrokerManagedComponent CreateTransientComponentInstance(Guid typeID);

        ICalculationModule GetCMInstance(Guid typeId, string cmName);
        ICalculationModule GetCMInstance(string typeName, string cmName);
        ICalculationModule GetCMInstance(Guid cIID, Guid cSID);
        ICalculationModule GetCMImplementation(Guid cLID, Guid cSID);
        IBrokerManagedComponent GetCMImplementation(string typeName, string cmName);
        ILogicalModule GetLogicalCM(Guid cLID);
        ICalculationModule GetWellKnownCM(WellKnownCM wellKnownCM);
        IBrokerManagedComponent GetWellKnownBMC(WellKnownCM wellKnownCM);
        IAssemblyManagement GetAssemblyDictionary();
        IComponentManagement GetComponentDictionary();
        void SetComponentDictionary(IComponentManagement componentDictionary);
        DataSet GetBMCDetailList(string typeName);
        DataSet GetBMCDetailList(string typeName, object[] parameters);
        IEnumerable GetCMTypeList();
        IEnumerable GetCMTypeList(string categoryName);
        IEnumerable GetCMTypeList(string[] categoryNames);
        IEnumerable GetCMTypes(string strTypeName);
        bool IsCMInstalled(string strTypeName);
        IEnumerable GetCMTypes(Guid objTypeID);
        DataTable GetLCMsByParent(Guid parentCLID);
        DataTable GetLCMsByParent(Guid parentCLID, Guid parentCSID);
        void DeleteCMInstance(Guid cmId);
        void DeleteCMInstanceImmediate(Guid cid);
        void DeleteLogicalCM(Guid cLID);
        Guid CIIDToCLID(Guid cIID);
        IEnumerable GetConsolidationParentsList(Guid memberCLID);
        IEnumerable GetConsolidationParentsList(Guid memberCLID, Guid memberCSID);
        IPrincipal UserContext { get; }
        bool InTransaction { get; }
        ITransaction Transaction { get; }

        /// <summary>
        /// Indicates if the user has cancelled the operation
        /// </summary>
        bool UserRequestCancelled
        {
            get;
        }

        /// <summary>
        /// Indicates if a long running transaction is in progress
        /// somewhere in the system
        /// </summary>
        /// <returns>True if there is a long running transaction in progress, false otherwise</returns>
        bool IsLongRunningTransactionInProgress();

        /// <summary>
        /// Sets the status message to be displayed to the user
        /// about the current status of the operation
        /// </summary>
        void SetStatusDisplayMessage(string statusMessage);

        /// <summary>
        /// Gets the status message that is currently being
        /// displayed to the user about the status of the operation
        /// </summary>
        /// <returns>The status message being displayed to the user</returns>
        string GetStatusDisplayMessage();

        void Dispose();

        void LogEvent(int eventType,Guid cid, string eventDetails);
        void LogEvent(int eventType, string eventDetails, string userDetails, Guid cid);
        IEventLogQuery QueryEventLog(EventLogQueryType eventLogQueryType);

        IBrokerManagedComponent GetQualifiedList(Guid typeID, object qualifier);
        DataTable ListBMCCollection(CollectionSpecifier bMCCollectionSpecifier);

        void ClearCache();
        void ClearCache(Guid cID);
        int CacheSize();
        bool IsInCache(Guid cID);

        IEnumerable GetConsolidationMembersList(Guid parentCLID, Guid parentCSID);
        ReadOnlyCollection<ICalculationModule> GetGroupsThatHaveOrganisationUnitAsMember(ICalculationModule organisationUnit);

        void ExportXMLToFile(Guid cIID, string fileName);
        string ExtractXML(Guid cIID);
        string ExtractXML(Guid cLID, Guid cSID);
        string DeliverXML(Guid cIID, string xml, bool verify);
        void DeliverXMLFromFile(Guid cIID, string fileName);
        IBrokerManagedComponent CreateFromXML(Guid cID, Guid cTID, string xml, string name);
        bool IsBMCCustomSerialized(Guid cIID);
        void MigrationCompleted(Guid cIID);
        void Resubscribe(Guid cIID);
        bool OrganisationNameAlreadyExists(string organisationName);
        int SecuritySetting { get; set; }
        string SystemWideDisplayMessage { get; set; }

        string GetVersionedWorkpaper(string strWorkpaper, Guid objCIID);
        string GetVersionedWorkpaper(string strWorkpaper, Guid objCLID, Guid objCSID);
        string GetVersionedWorkpaper(string strWorkpaper, IBrokerManagedComponent objBMC);
        string GetVersionedWorkpaper(string strWorkpaper, string strBMCTypeName);
        string ProductVersion { get; }

        void ReleaseBrokerManagedComponent(IBrokerManagedComponent iBMC);
        string GetVersionedWorkpaperFromAssemblyID(string strWorkpaper, Guid assemblyID);


        void UpdateCacheEntry(IBrokerManagedComponent bmc);

    }
}
