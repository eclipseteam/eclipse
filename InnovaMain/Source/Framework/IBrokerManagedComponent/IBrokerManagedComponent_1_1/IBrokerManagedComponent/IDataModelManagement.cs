using System;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// The interface that must be implemented by code within a deployable unit when that unit requires
	/// a suuporting data model in the database which must be installed by the setup utilities.
	/// </summary>
	public interface IDataModelManagement
	{
		void EstablishDatabase();
		void RemoveDatabase();
		/// <summary>
		/// Removes any DB structures which belonged to previous datamodel versions
		/// for and assembly and are no longer required.
		/// </summary>
		void RemoveOldDatabase();
	}
}
