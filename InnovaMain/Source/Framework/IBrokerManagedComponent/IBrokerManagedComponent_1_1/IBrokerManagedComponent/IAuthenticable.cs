﻿
namespace Oritax.TaxSimp.CalculationInterface
{
    public interface IAuthenticable
    {
        bool Authenticate(string name, string password);
    }

}
