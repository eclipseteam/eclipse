using System;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CalculationInterface
{
	[Serializable]
	public class PublisherData: ISerializable
	{
		public Guid CLID;
		public Guid CSID;
		public Guid CIID;
		public string CMName;
		public string CMTypeName;
		public string CMCategoryName;
		public PublisherData(Guid cLID,Guid cSID,Guid cIID,string cMName,string cMTypeName,string cMCategoryName)
		{
			CLID=cLID;
			CSID=cSID;
			CIID=cIID;
			CMName=cMName;
			CMTypeName=cMTypeName;
			CMCategoryName=cMCategoryName;
		}

		protected PublisherData(SerializationInfo si, StreamingContext context)
		{
			CLID=Serialize.GetSerializedGuid(si,"pd_CLID");
			CSID=Serialize.GetSerializedGuid(si,"pd_CSID");
			CIID=Serialize.GetSerializedGuid(si,"pd_CIID");
			CMName=Serialize.GetSerializedString(si,"pd_CMName");
			CMTypeName=Serialize.GetSerializedString(si,"pd_CMTypeName");
			CMCategoryName=Serialize.GetSerializedString(si,"pd_CMCategoryName");
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			Serialize.AddSerializedValue(si,"pd_CLID",CLID);
			Serialize.AddSerializedValue(si,"pd_CSID",CSID);
			Serialize.AddSerializedValue(si,"pd_CIID",CIID);
			Serialize.AddSerializedValue(si,"pd_CMName",CMName);
			Serialize.AddSerializedValue(si,"pd_CMTypeName",CMTypeName);
			Serialize.AddSerializedValue(si,"pd_CMCategoryName",CMCategoryName);
		}

	}

	/// <summary>
	/// Summary description for IMessage.
	/// </summary>
	public interface IMessage : IDictionary//IDictionaryEnumerator//IEnumerable
	{
		// Add an item to collection
		void Add(String key, object newObject);
		// Return true if collection contains an item with key
		bool Contains(String key);
		void Remove(String key);
		// Get/Set selected item
		object this[String key]{get;set;}
		IEnumerable PublishersByType{get;}
		IEnumerable PublishersByCategory{get;}
		bool IsPublisher(Guid cLID);
		PublisherData GetPublisherByCMTypeName(string cMTypeName);
		PublisherData GetPublisherByCMCategoryName(string cMTypeName);
		int Scope{get;set;}
		bool InterScenario{get;}
		Guid MSID{get;set;}
		string Datatype{get;}
		PublisherData OriginatingPublisher{get;}
		void CopyMessageData(IMessage objMessge);
		void ExtractData(DataSet dataSet);
		void DeliverData(DataSet dataSet);
		void SetBusinessEntityPublisherData(Guid publisherCLID);
		Guid ID{get;set;}
		SortedList ToSortedList();
	}
}
