﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.CalculationInterface
{
    public enum WellKnownRestCommand
    {
        ClientByNameAndType      =   5001,
        ClientByName             =   5002,
        AllClient                =   5003,
        AllClientByType          =   5004
    }
}
