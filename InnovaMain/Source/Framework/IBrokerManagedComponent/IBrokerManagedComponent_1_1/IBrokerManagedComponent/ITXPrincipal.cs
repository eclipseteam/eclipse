using System;
using System.Security.Principal;

namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// Summary description for ITXPrincipal.
	/// </summary>
	public interface ITXPrincipal : IPrincipal
	{
		String[] Groups {get; set;}
	}
}
