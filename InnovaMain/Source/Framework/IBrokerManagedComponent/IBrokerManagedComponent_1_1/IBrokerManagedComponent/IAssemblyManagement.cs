using System;
using System.Collections;

namespace Oritax.TaxSimp.CalculationInterface
{
	public interface IAssemblyManagement
	{
		void Add(IAssembly assembly);
		bool Contains(Guid iD);
		void Replace(IAssembly componentVersion);
		void Delete(Guid iD);
		IAssembly this[Guid cTID]{get;}
		void Clear();
		IAssembly GetAssembly(string name, IAssemblyVersionNumber assemblyVersionNumber);
		IAssemblyVersionSet GetAssemblyVersions(string name);
	}

	public interface IAssemblyVersionNumber
	{
		int MajorVersion{get;set;}
		int MinorVersion{get;set;}
		int DataFormat{get;set;}
		int Revision{get;set;}
		string ToString();
		string ToStringMajMin();
	}

	public interface IAssembly
	{
		Guid ID{get;set;}
		IAssemblyVersionNumber VersionNumber{get;set;}
		string Name{get;set;}
		string DisplayName{get;set;}
		string StrongName{get;set;}
	}

	public interface IAssemblyVersionSet : IEnumerable
	{
		IAssembly Latest{get;}
		IAssembly this[IAssemblyVersionNumber assemblyVersionNumber]{get;}
		bool Contains(IAssemblyVersionNumber assemblyVersionNumber);
		void Add(IAssemblyVersionNumber assemblyVersionNumber,IAssembly assembly);
		int Count{get;}
	}
}