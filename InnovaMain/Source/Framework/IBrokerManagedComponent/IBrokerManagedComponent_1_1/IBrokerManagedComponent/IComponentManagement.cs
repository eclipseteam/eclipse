using System;
using System.Collections;


namespace Oritax.TaxSimp.CalculationInterface
{
	public interface IComponentManagement
	{
		bool ContainsComponent(Guid iD);
		bool ContainsComponent(string name);
		bool ContainsComponentVersion(Guid iD);
		void Add(string componentName,IComponentVersion componentVersion);
		void Replace(string componentName,IComponentVersion componentVersion);
		void Add(IComponentVersion componentVersion);
		void Replace(IComponentVersion componentVersion);
		void Replace(IComponent component);
		void Add(IComponent component);
		void Delete(Guid iD);
		void DeleteComponent(Guid iD);
		void DeleteComponentVersion(Guid iD);
		void Clear();
		IComponentVersion this[Guid iD]{get;}
		Guid this[string typeName]{get;}
		IComponent GetComponent(Guid componentID);
		IComponentVersion GetComponentVersion(string name,int majorVersion,int minorVersion);
//		IComponentManagement GetVersionsForCMScenarios(ArrayList childCMScenarios);
		IComponentManagement GetDictionaryForCMScenarios(ArrayList childCMScenarios);
		IComponentVersionArray GetDateQualifiedTaxTopicComponentVersions(DateTime startDate,DateTime endDate);
		IComponentVersionArray GetQualifiedTaxTopicComponentVersions(DateTime startDate,DateTime endDate,IComponentManagement includedVersions);
        IComponentVersionArray GetQualifiedPrimarySupportTaxTopicComponentVersions(DateTime startDate, DateTime endDate, IComponentManagement includedVersions);
        IComponentVersionArray GetTaxTopicComponentVersions(IComponentManagement includedVersions);
		IComponentVersionArray GetQualifiedPrimaryTaxTopicComponentVersions(DateTime startDate,DateTime endDate,IComponentManagement includedVersions);
		IComponentVersionArray TaxTopicComponentsVersions{get;}
		IAssemblyVersionSet GetComponentVersions(string name);
		IComponentVersionArray ComponentVersions{get;}
		IComponentVersion[] GetComponentVersions(Guid componentID);
	}

	public interface IComponentVersion : IAssembly
	{
		int MajorVersion{get;}
		int MinorVersion{get;}
		string VersionName{get;set;}
		Guid ComponentID{get;set;}
		DateTime StartDate{get;set;}
		DateTime EndDate{get;set;}
		string PersisterAssemblyStrongName{get;set;}
		string PersisterClass{get;set;}
		string ImplementationClass{get;set;}
		bool Obsolete{get;set;}
		IComponentManagement ContainingComponentDictionary{get;set;}
		bool IsLaterVersion(IComponentVersion componentVersion);
		bool IsEarlierVersion(IComponentVersion componentVersion);
		bool IsSameVersion(IComponentVersion componentVersion);

		/// <summary>
		/// Determines if a component version is the next version to this component version
		/// </summary>
		/// <param name="componentVersion">The component version to check</param>
		/// <returns>True if the component version is 1 major version greater than this component version, false otherwise</returns>
		bool IsNextVersion(IComponentVersion componentVersion);
	}

	public interface IComponent
	{
		Guid ID{get;set;}
		string Name{get;set;}
		string DisplayName{get;set;}
		string Category{get;set;}
		int Applicability{get;set;}
		bool Obsolete{get;set;}
	}

	public interface IComponentVersionArray : IEnumerable
	{
		int Count{get;}
		IComponentVersion this[int index]{get;}
		void Add(IComponentVersion component);
	}

	public interface IComponentArray : IEnumerable
	{
		IComponent this[int index]{get;}
		void Add(IComponent component);
	}

	public interface IComponentIDDictionary
	{
		IComponent this[Guid iD]{get;}
		bool Contains(Guid iD);
		void Add(Guid iD,IComponent component);
		void Delete(Guid iD);
	}

	public interface IComponentVersionIDDictionary
	{
		IComponentVersion this[Guid iD]{get;}
		bool Contains(Guid iD);
		void Add(Guid iD,IComponentVersion component);
		void Delete(Guid iD);
	}

	public interface IComponentVersionNameDictionary
	{
		IComponentVersion this[string name]{get;}
		bool Contains(Guid iD);
		void Add(Guid iD,IComponentVersion component);
		void Delete(Guid iD);
	}

	public interface IComponentVersionSet
	{
		IAssembly this[IAssemblyVersionNumber assemblyVersionNumber]{get;}
		bool Contains(IAssemblyVersionNumber assemblyVersionNumber);
		void Add(IAssemblyVersionNumber assemblyVersionNumber,IAssembly assembly);
	}
}