using System;
using System.Data;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// ICalculationModulePersist is the interface implemented by any Calculation Module Persistence engine.
	/// </summary>
	public interface ICalculationModulePersist
	{
		void StartTransaction();
		void CommitTransaction();
		void RollbackTransaction();
		DataSet Update(DataSet data);
		DataSet FindByInstanceId(Guid instanceId);
		DataSet FindAll();
		long CMBlobSize{set;}
	}
}
