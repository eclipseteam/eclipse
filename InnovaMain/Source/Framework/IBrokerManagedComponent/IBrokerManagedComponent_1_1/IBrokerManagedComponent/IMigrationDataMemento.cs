using System;

namespace Oritax.TaxSimp.CalculationInterface
{
	/// <summary>
	/// Allows a BMC to store information from the DeliverData phase to the MigrationComplete phase
	/// </summary>
	public interface IMigrationDataMemento
	{
		void Add(string key, object value);
		void Remove(string key);
		object this[string key]{get;}
	}
}
