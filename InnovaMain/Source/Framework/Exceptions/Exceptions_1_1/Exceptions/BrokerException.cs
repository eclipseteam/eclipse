using System;
using System.Runtime.Serialization;

namespace Oritax.TaxSimp.Exceptions
{
	[Serializable()]
	public class BrokerException : Exception
	{
		public BrokerException(): base()
		{
		}

		public BrokerException( string strException): base( strException )
		{
		}

		protected BrokerException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}