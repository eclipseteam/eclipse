using System;
using System.Runtime.Serialization;

namespace Oritax.TaxSimp.Exceptions
{

	public enum BusinessRuleViolationType
	{
		OrganisationUnitNameConflict,
		OrganisationUnitLegalNameConflict,
		ConsolidationMember,
		LastMainScenario,
		GroupMembershipCircularReference,
	}

	[Serializable()]
	public class BusinessRuleViolation : System.Exception
	{
		private BusinessRuleViolationType m_objType;

		public BusinessRuleViolation( string strException, BusinessRuleViolationType objType ): base( strException )
		{
			this.m_objType = objType;
		}

		protected BusinessRuleViolation(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}

		public BusinessRuleViolationType Type
		{
			get
			{
				return this.m_objType;
			}
			set
			{
				this.m_objType = value;
			}
		}
	}
}
