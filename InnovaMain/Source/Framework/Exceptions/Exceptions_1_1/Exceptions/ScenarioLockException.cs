using System;
using Oritax.TaxSimp.Utilities;
namespace Oritax.TaxSimp.Exceptions
{
	/// <summary>
	/// Summary description for ScenarioLockException.
	/// </summary>
	public class ScenarioLockException : DisplayUserMessageException
	{
		public ScenarioLockException() : base(new MessageBoxDefinition(MessageBoxDefinition.MSG_ATTEMPT_TO_WRITE_TO_LOCKED_SCENARIO), true, MessageDisplayMethod.MainWindowPopup){}
		
		public ScenarioLockException(string entityName) : 
			base(new MessageBoxDefinition(MessageBoxDefinition.MSG_BTB_ENTITY_WITH_LOCK_SCENERIO, new string[] {entityName}), true, MessageDisplayMethod.MainWindowPopup){}

		public ScenarioLockException(MessageBoxDefinition mdb) : 
			base(mdb, true, MessageDisplayMethod.MainWindowPopup){}

	}
}
