using System;
using System.Runtime.Serialization;

namespace Oritax.TaxSimp.Exceptions
{
	public enum MultiUserExceptionType
	{
		UpdateConflict,
		SystemStructureChanged,
	}

	/// <summary>
	/// 
	/// </summary>
	[Serializable()]
	public class MultiUserException : Exception
	{
		private MultiUserExceptionType m_objExceptionType;

		public MultiUserException() : base()
		{
		}
		
		/// <summary>
		/// Create a multi-user exception
		/// </summary>
		/// <param name="objType">The type of multi-user exception that occured</param>
		/// <param name="strMessage">The error message</param>
		public MultiUserException( MultiUserExceptionType objType, string strMessage ): base( strMessage )
		{
			this.m_objExceptionType = objType;
		}

		/// <summary>
		/// The type of multi-user exception that occured
		/// </summary>
		public MultiUserExceptionType Type
		{
			get
			{
				return this.m_objExceptionType;
			}
		}

		protected MultiUserException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
