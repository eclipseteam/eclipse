using System;
using System.Text;
using System.Runtime.Serialization;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Exceptions
{
	/// <summary>
	/// Exception that can be thrown, which will display the specified message to the user
	/// </summary>
	[Serializable()]
	public class DisplayUserMessageException : Exception
	{
		#region Member Variables
		private MessageBoxDefinition [] m_objMBS;
		private bool m_bolReloadWorkpaper;
		private MessageDisplayMethod m_objMessageDisplayMethod;
		private String m_strRedirectWorkpaper = String.Empty;
		private bool redirectToOriginalReferrer;
		#endregion //Member Variables
		#region Construtors
		/// <summary>
		/// Creates a DisplayUserMessageException with no parameters
		/// </summary>
		public DisplayUserMessageException() : base()
		{
		}

		/// <summary>
		/// Creates a DisplayUserMessageException
		/// </summary>
		/// <param name="mbd">The message box definition to use</param>
		/// <param name="redirectToOriginalReferrer">Indicates if the user should be sent back to the page they came from after awknowledging the message</param>
		public DisplayUserMessageException(MessageBoxDefinition mbd, bool redirectToOriginalReferrer) : this(new MessageBoxDefinition[]{mbd}, redirectToOriginalReferrer)
		{
		}

		/// <summary>
		/// Creates a DisplayUserMessageException
		/// </summary>
		/// <param name="objMBDItems">The message box definitions to use</param>
		/// <param name="redirectToOriginalReferrer">Indicates if the user should be sent back to the page they came from after awknowledging the message</param>
		public DisplayUserMessageException(MessageBoxDefinition[] objMBDItems, bool redirectToOriginalReferrer) : this(objMBDItems, false, MessageDisplayMethod.MainWindowPopup)
		{
			this.redirectToOriginalReferrer = redirectToOriginalReferrer;
		}

		/// <summary>
		/// Creates a DisplayUserMessageException
		/// </summary>
		/// <param name="objMBD">The message box definition to use</param>
		/// <param name="bolReloadWorkpaper">The workpaper data should be reloaded from the cm before display</param>
		/// <param name="objMessageDisplayMethod">The method used to display the message, e.g on the workpaper or as a popup</param>
		public DisplayUserMessageException( MessageBoxDefinition objMBD, bool bolReloadWorkpaper, MessageDisplayMethod objMessageDisplayMethod ) : this ( objMBD, bolReloadWorkpaper, objMessageDisplayMethod, String.Empty )
		{
		}

		/// <summary>
		/// Creates a DisplayUserMessageException
		/// </summary>
		/// <param name="objMBD">The message box definition to use</param>
		/// <param name="bolReloadWorkpaper">The workpaper data should be reloaded from the cm before display</param>
		/// <param name="objMessageDisplayMethod">The method used to display the message, e.g on the workpaper or as a popup</param>
		/// <param name="strRedirectWorkpaper">The workpaper to redirect to after displaying the message</param>
		public DisplayUserMessageException( MessageBoxDefinition objMBD, bool bolReloadWorkpaper, MessageDisplayMethod objMessageDisplayMethod, string strRedirectWorkpaper ) : this ( new MessageBoxDefinition [] {objMBD}, bolReloadWorkpaper, objMessageDisplayMethod, strRedirectWorkpaper )
		{
		}

		/// <summary>
		/// Creates a DisplayUserMessageException
		/// </summary>
		/// <param name="objMBDItems">The message box definitions to use</param>
		/// <param name="bolReloadWorkpaper">The workpaper data should be reloaded from the cm before display</param>
		/// <param name="objMessageDisplayMethod">The method used to display the message, e.g on the workpaper or as a popup</param>
		public DisplayUserMessageException( MessageBoxDefinition [] objMBDItems, bool bolReloadWorkpaper, MessageDisplayMethod objMessageDisplayMethod ) : this ( objMBDItems, bolReloadWorkpaper, objMessageDisplayMethod, String.Empty )
		{
		}

		/// <summary>
		/// Creates a DisplayUserMessageException
		/// </summary>
		/// <param name="objMBDItems">The message box definitions to use</param>
		/// <param name="bolReloadWorkpaper">The workpaper data should be reloaded from the cm before display</param>
		/// <param name="objMessageDisplayMethod">The method used to display the message, e.g on the workpaper or as a popup</param>
		/// <param name="strRedirectWorkpaper">The workpaper to redirect to after displaying the message</param>
		public DisplayUserMessageException( MessageBoxDefinition [] objMBDItems, bool bolReloadWorkpaper, MessageDisplayMethod objMessageDisplayMethod, string strRedirectWorkpaper ) : base ( objMBDItems[ 0 ].ToString( ) )
		{
			this.m_objMBS = objMBDItems;
			this.m_bolReloadWorkpaper = bolReloadWorkpaper;
			this.m_objMessageDisplayMethod = objMessageDisplayMethod;
			this.m_strRedirectWorkpaper = strRedirectWorkpaper;
		}

		protected DisplayUserMessageException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
		#endregion //Constructors
		#region Properties
		/// <summary>
		/// The message box items to display in the message
		/// </summary>
		public MessageBoxDefinition [] MBDItems
		{
			get
			{
				return this.m_objMBS;
			}
		}

		/// <summary>
		/// Should the workpaper data be reloaded from the cm before display
		/// </summary>
		public bool ReloadWorkpaper
		{
			get
			{
				return this.m_bolReloadWorkpaper;
			}
		}

		/// <summary>
		/// How the message should be displayed, e.g. on the workpaper, or in a popup window.
		/// </summary>
		public MessageDisplayMethod MessageDisplay
		{
			get
			{
				return this.m_objMessageDisplayMethod;
			}
		}

		/// <summary>
		/// After the message has been displayed, the workpaper the user should be sent to, if it is not set, the user remains on the workpaper
		/// </summary>
		public string RedirectWorkpaper
		{
			get
			{
				return this.m_strRedirectWorkpaper;
			}
		}

		/// <summary>
		/// After the message has been displayed, should the user be sent back to the workpaper they came from
		/// </summary>
		public bool RedirectToOriginalReferrer
		{
			get
			{
				return this.redirectToOriginalReferrer;
			}
		}
		#endregion //Properties
	}
}
