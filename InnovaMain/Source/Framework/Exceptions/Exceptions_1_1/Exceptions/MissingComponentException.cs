using System;

namespace Oritax.TaxSimp.Exceptions
{
	/// <summary>
	/// An exception to indicate that the broker managed component does not exist and has been deleted
	/// </summary>
	public class MissingComponentException : ApplicationException
	{
		public MissingComponentException(string message):base(message)
		{
		}
	}
}
