using System;
using System.Runtime.Serialization;

namespace Oritax.TaxSimp.Exceptions
{
	[Serializable()]
	public class MigrationException : Exception
	{
		public MigrationException() : base()
		{
		}

		public MigrationException( string strException): base( strException )
		{
		}

		protected MigrationException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}