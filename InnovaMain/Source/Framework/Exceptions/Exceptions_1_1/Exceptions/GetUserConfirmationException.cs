using System;
using System.Collections;
using Oritax.TaxSimp.CalculationInterface;

using Oritax.TaxSimp.Utilities;
using System.Collections.Generic;
using System.Text;

namespace Oritax.TaxSimp.Exceptions
{
	/// <summary>
	/// GetUserConfirmationExceptions are thrown from OnSetData calls in the CM to get a confirmation from the user to perform an action
	/// </summary>
	[Serializable]
	public class GetUserConfirmationException : ApplicationException
	{
		private List<MessageBoxDefinition> objMBDs = new List<MessageBoxDefinition>(1);

     	private IMessageBoxControlsList messageBoxControlCollection;

		// different types of user confirmation are required
		private string m_confirmationType = "Default";

        private bool allowUserToCancelConfirmedOperation = false;

		public GetUserConfirmationException( MessageBoxDefinition objMBD ) : base( objMBD.ToString( ) )
		{
			this.objMBDs.Add(objMBD);
		}

        public GetUserConfirmationException(MessageBoxDefinition[] mbds)
            : base()
        {
            this.objMBDs.AddRange(mbds);
        }

		/// <summary>
		/// The following constructor uses confirmType parameter. This is for getting multiple confirmation 
		/// from users.
		/// </summary>
		/// <param name="objMBD"></param>
		/// <param name="confirmType"></param>
		public GetUserConfirmationException( MessageBoxDefinition objMBD, string confirmType ) : this( objMBD )
		{
			m_confirmationType = confirmType;
		}

        /// <summary>
        /// The following constructor uses confirmType parameter. This is for getting multiple confirmation 
        /// from users.
        /// </summary>
        /// <param name="objMBD">The message box definition to display</param>
        /// <param name="confirmType">The confirmation key</param>
        /// <param name="allowUserToCancelConfirmedOperation">Indicates that the system will allow the user to cancel the request, even once confirmed</param>
        public GetUserConfirmationException(MessageBoxDefinition objMBD, string confirmType, bool allowUserToCancelConfirmedOperation)
            : this(objMBD, confirmType)
        {
            this.allowUserToCancelConfirmedOperation = allowUserToCancelConfirmedOperation;
        }

		/// <summary>
		/// The constructor will have control list which will setup message box as requested
		/// </summary>
		/// <param name="objMBD"></param>
		/// <param name="controlCollection"></param>
		public GetUserConfirmationException( MessageBoxDefinition objMBD, IMessageBoxControlsList controlCollection ) : this( objMBD )
		{
			messageBoxControlCollection = controlCollection;
		}

		public MessageBoxDefinition[] MBDs
		{
			get
			{
				return this.objMBDs.ToArray();
			}
		}
		/// <summary>
		/// Return ControlCollection
		/// </summary>
		public object ControlCollection
		{
			get
			{
				return this.messageBoxControlCollection;
			}
		}

		public override string ToString()
		{
            StringBuilder messages = new StringBuilder();

            foreach (MessageBoxDefinition mbd in this.objMBDs)
                messages.AppendLine(mbd.ToString());

            return messages.ToString();
		}

		public string ConfirmationType
		{
			get { return m_confirmationType; }
		}

        public bool AllowUserToCancelConfirmedOperation
        {
            get
            {
                return this.allowUserToCancelConfirmedOperation;
            }
        }
	}
}
