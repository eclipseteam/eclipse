using System;

namespace Oritax.TaxSimp.Exceptions
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
	public class Install
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="EBDB0DCF-5673-41d2-947E-F5347F36ADB8";
		public const string ASSEMBLY_NAME="Exceptions_1_1";
		public const string ASSEMBLY_DISPLAYNAME="Exceptions V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
	//	public const string ASSEMBLY_REVISION="0"; 
		public const string ASSEMBLY_REVISION="1"; //2005.1

		#endregion
	}
}
