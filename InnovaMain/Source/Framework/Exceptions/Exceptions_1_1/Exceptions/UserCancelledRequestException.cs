using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace Oritax.TaxSimp.Exceptions
{
    public class UserCancelledRequestException : ApplicationException, ISerializable
    {
        public UserCancelledRequestException()
            : base()
        {
        }

        public UserCancelledRequestException(string message)
            : base(message)
        {
        }

        public UserCancelledRequestException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public UserCancelledRequestException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        #region ISerializable Members
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            this.GetObjectData(info, context);
        }
        #endregion
    }
}
