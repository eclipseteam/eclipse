using System;
using System.Runtime.Serialization;

namespace Oritax.TaxSimp.Exceptions
{
	/// <summary>
	/// Exception thrown when a method is called that requires it to be in a transaction and a transaction is not running
	/// </summary>
	[Serializable()]
	public class TransactionNotRunningException : Exception
	{
		public TransactionNotRunningException() : base()
		{
		}

		/// <summary>
		/// Creates a TransactionNotRunningException with a particular message
		/// </summary>
		/// <param name="strMessage">The error message</param>
		public TransactionNotRunningException( string strMessage ) : base( strMessage )
		{
		}

		protected TransactionNotRunningException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
