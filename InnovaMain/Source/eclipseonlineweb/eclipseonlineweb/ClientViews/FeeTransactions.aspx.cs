﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.CM.OrganizationUnit;
using System.Globalization;

namespace eclipseonlineweb
{
    public partial class FeeTransactions : UMABasePage
    {
        protected override void GetBMCData()
        {
            this.cid = Request.QueryString["ins"].ToString();
            IOrganizationUnit clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            FeeTransactionsDS feeTransactionsDS = new FeeTransactionsDS();

            clientData.GetData(feeTransactionsDS);
            this.PresentationData = feeTransactionsDS;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
        }

        public override void LoadPage()
        {
            this.cid = Request.QueryString["ins"].ToString();
            base.LoadPage();
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
                ((SetupMaster)Master).IsAdmin = "1";
        }

        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e) { }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                UMABroker.SaveOverride = true;
                Guid rowID = (Guid)gDataItem.OwnerTableView.DataKeyValues[gDataItem.ItemIndex]["ID"];
                IOrganizationUnit clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
                FeeTransactionDetailsDS feeTransactionDetailsDS = new FeeTransactionDetailsDS();
                feeTransactionDetailsDS.DataSetOperationType = DataSetOperationType.DeletSingle;
                feeTransactionDetailsDS.FeeTransactionID = rowID;
                clientData.SetData(feeTransactionDetailsDS);
                UMABroker.ReleaseBrokerManagedComponent(clientData);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.PresentationGrid.Rebind();
            }
            if (e.CommandName == "Detail")
            {
                string[] IdIns = e.CommandArgument.ToString().Split(',');
                Response.Redirect("~/ClientViews/FeeTransactionDetails.aspx?ID=" + IdIns[0] + "&INS=" + IdIns[1] + "&PageName=" + "FeeTransaction");
            }
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
           
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) { }
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e) { }
        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
        }

        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e) { }

        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DataView View = new DataView(this.PresentationData.Tables[FeeTransactionsDS.FEETRANSACTIONS]);
            View.Sort = FeeTransactionsDS.TRANSACTIONDATE + " DESC";
            this.PresentationGrid.DataSource = View.ToTable();
        }
    }
}
