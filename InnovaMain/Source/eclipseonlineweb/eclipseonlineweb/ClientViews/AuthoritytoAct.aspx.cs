﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using C1.Web.Wijmo.Controls.C1ComboBox;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Security;
using eclipseonlineweb.WebUtilities;

namespace eclipseonlineweb
{
    /// <summary>
    /// Keep commented lines intact - just in case
    /// </summary>
    public partial class AuthoritytoAct : UMABasePage
    {

        #region Authority to Act

        public bool AuthorityToActEclispe
        {
            get
            { return this.chkSelectLimitedPowerAttorneyEclipse.Checked; }
            set
            {
                chkSelectLimitedPowerAttorneyEclipse.Checked = value;
            }
        }


        public bool AuthorityToActAMM
        {
            get
            { return this.chkLimitedPowerAttorneyAMM.Checked; }
            set
            { chkLimitedPowerAttorneyAMM.Checked = value; }
        }

        public bool AuthorityToActMacBank
        {
            get
            { return this.chkLimitedPowerAttorneyMacBankCMA.Checked; }
            set
            { chkLimitedPowerAttorneyMacBankCMA.Checked = value; }
        }

        public bool InvestorStatusWholesaleInvestor
        {
            get
            { return this.rbWholesaleInvestor.Checked; }
            set
            { rbWholesaleInvestor.Checked = value; }
        }

        public bool InvestorStatusSophisticatedInvestor
        {
            get
            { return this.rbSophisticatedInvestor.Checked; }
            set
            { rbSophisticatedInvestor.Checked = value; }
        }

        public bool InvestorStatusProfessionalInvestor
        {
            get
            { return this.rbProfessionalInvestor.Checked; }
            set
            { rbProfessionalInvestor.Checked = value; }
        }

        public bool InvestorStatusRetailInvestor
        {
            get
            { return this.rbRetailInvestor.Checked; }
            set
            { rbRetailInvestor.Checked = value; }
        }

        public bool CertificateFromAccountantIsAttached
        {
            get
            { return this.chkCertificateFromAccountantAttached.Checked; }
            set
            { chkCertificateFromAccountantAttached.Checked = value; }
        }

        #endregion 
        public override void LoadPage()
        {
            this.cid = Request.QueryString["ins"].ToString();

            if (!this.Page.IsPostBack)
            {
                SetData();
            }

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");

            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                this.ibtnSave.Visible = false;

            }

            UMABroker.ReleaseBrokerManagedComponent(objUser);
        }

     


        private void SetData()
        {

            OrganizationUnitCM clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as OrganizationUnitCM;
            if (clientData != null)
                if (clientData.IsCorporateType())
                {
                    var entity = clientData.ClientEntity as CorporateEntity;

                    if (entity.AuthoritytoAct == null)
                        entity.AuthoritytoAct = new ClientAuthoritytoActEntity();

                    SetControlData(entity.AuthoritytoAct);
                }
                else
                {
                    var entity = clientData.ClientEntity as ClientIndividualEntity;
                    if (entity.AuthoritytoAct == null)
                        entity.AuthoritytoAct = new ClientAuthoritytoActEntity();
                    SetControlData(entity.AuthoritytoAct);
                }
        }



        public void SetControlData(ClientAuthoritytoActEntity umaFormEntity)
        {
            AuthorityToActEclispe = umaFormEntity.EclipseOnline;
            AuthorityToActAMM = umaFormEntity.AustralianMoneyMarket;
            AuthorityToActMacBank = umaFormEntity.MacquarieCMA;
            InvestorStatusWholesaleInvestor = umaFormEntity.InvestorStatusWholesaleInvestor;
            InvestorStatusSophisticatedInvestor = umaFormEntity.InvestorStatusSophisticatedInvestor;
            InvestorStatusProfessionalInvestor = umaFormEntity.InvestorStatusProfessionalInvestor;
            InvestorStatusRetailInvestor = umaFormEntity.InvestorStatusRetailInvestor;
            CertificateFromAccountantIsAttached = umaFormEntity.CertificateFromAccountantIsAttached;

        }


        public void SetEntityValues(ClientAuthoritytoActEntity umaFormEntity)
        {
            #region Authority To Act

            umaFormEntity.EclipseOnline = AuthorityToActEclispe;
            umaFormEntity.AustralianMoneyMarket = AuthorityToActAMM;
            umaFormEntity.MacquarieCMA = AuthorityToActMacBank;
            umaFormEntity.InvestorStatusWholesaleInvestor = InvestorStatusWholesaleInvestor;
            umaFormEntity.InvestorStatusSophisticatedInvestor = InvestorStatusSophisticatedInvestor;
            umaFormEntity.InvestorStatusProfessionalInvestor = InvestorStatusProfessionalInvestor;
            umaFormEntity.InvestorStatusRetailInvestor = InvestorStatusRetailInvestor;
            umaFormEntity.CertificateFromAccountantIsAttached = CertificateFromAccountantIsAttached;

            #endregion

          
        }


        protected void ibtnSave_Click(object sender, EventArgs e)
        {
            OrganizationUnitCM clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as OrganizationUnitCM;
            clientData.IsInvestableClient = true;
            clientData.UpdationDate = DateTime.Now;

            if (clientData.IsCorporateType())
            {
                CorporateEntity entity = clientData.ClientEntity as CorporateEntity;
                SetEntityValues(entity.AuthoritytoAct);
            }
            else
            {
                Oritax.TaxSimp.Common.ClientIndividualEntity entity = clientData.ClientEntity as ClientIndividualEntity;

                SetEntityValues(entity.AuthoritytoAct);
            }

            this.UMABroker.SaveOverride = true;
            clientData.CalculateToken(true);
            this.UMABroker.SetComplete();
            this.UMABroker.SetStart();


            SetData();
        }
    }
}
