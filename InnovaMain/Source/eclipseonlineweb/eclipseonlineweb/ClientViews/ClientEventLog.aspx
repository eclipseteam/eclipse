﻿<%@ Page Title="e-Clipse Online Portal" EnableViewState="true" Language="C#" MasterPageFile="ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="ClientEventLog.aspx.cs" Inherits="eclipseonlineweb.ClientEventLog" %>
  <%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
          <fieldset>
                <table width="100%">
                    <tr>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblAdministrationUsersList" Text="PLATFORM EVENTS LOG (Last 50,000 Platform Events)"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid ID="LogGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                Width="100%" PageSize="20" AllowSorting="True" AllowMultiRowSelection="False"
                AllowPaging="True" GridLines="None" AllowFilteringByColumn="true" EnableViewState="true"
                ShowFooter="false">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowFilteringByColumn="True" TableLayout="Fixed" Width="100%">
                    <Columns>
                        <telerik:GridBoundColumn FilterControlWidth="90px" SortExpression="Name" HeaderStyle-Width="12%"
                            ItemStyle-Width="10%" HeaderText="Name" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Name" UniqueName="Name">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                            SortExpression="UserName" HeaderText="User Name" AutoPostBackOnFilter="false"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="UserName" UniqueName="UserName" ReadOnly="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="8%" ItemStyle-Width="8%"
                            SortExpression="Type" HeaderText="Type" AutoPostBackOnFilter="false"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Type" UniqueName="Type" ReadOnly="false">
                        </telerik:GridBoundColumn>

                         <telerik:GridBoundColumn FilterControlWidth="600px" HeaderStyle-Width="50%" ItemStyle-Width="50%"
                            SortExpression="Detail" HeaderText="Detail" AutoPostBackOnFilter="false"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Detail" UniqueName="Detail" ReadOnly="false">
                        </telerik:GridBoundColumn>

                        <telerik:GridDateTimeColumn DataField="DateTime" HeaderText="DateTime" FilterControlWidth="95px" HeaderStyle-Width="10%"
                            SortExpression="DateTime" UniqueName="DateTime" PickerType="DatePicker" EnableTimeIndependentFiltering="true">
                        </telerik:GridDateTimeColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="Html"></Excel>
                </ExportSettings>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
