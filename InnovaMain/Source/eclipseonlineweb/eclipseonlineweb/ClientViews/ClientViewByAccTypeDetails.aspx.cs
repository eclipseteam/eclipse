﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;

namespace eclipseonlineweb
{
    public partial class ClientViewByAccTypeDetails : UMABasePage
    {
        string ccid = string.Empty;
        string selectAssetID = string.Empty;

        protected override void Intialise()
        {
            base.Intialise();
        }

        protected override void GetBMCData()
        {
            this.cid = Request.QueryString["ins"].ToString();

            string acCType = Request.QueryString["AccType"].ToString();
            string accID = Request.QueryString["AccNo"].ToString();
            string sysTran = Request.QueryString["SysTran"].ToString();
            
            if (!this.IsPostBack)
            {
                IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
                LossAndGainsReportDS ds = new LossAndGainsReportDS();

                clientData.GetData(ds);
                this.PresentationData = ds;

                this.PopulatePage(this.PresentationData);

                UMABroker.ReleaseBrokerManagedComponent(clientData);

                ScriptManager sm = ScriptManager.GetCurrent(Page);


                DataView view = new DataView(ds.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS]);
                view.RowFilter = LossAndGainsReportDS.ID + "='" + accID + "' AND " + LossAndGainsReportDS.SYSTRANSACIONTYPE + "='" + sysTran + "'";
                view.Sort = LossAndGainsReportDS.TRANSACTIONDATE + " DESC";
                    
                this.PresentationGrid.DataSource = view.ToTable();
                PresentationGrid.DataBind();
            }
        }

        protected void PopulateForm()
        {
            //IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            //ModelsDS modelsDS = new Oritax.TaxSimp.DataSets.ModelsDS();
            //modelsDS.GetDetails = true;
            //modelsDS.DetailsGUID = new Guid(this.cid);

            //organization.GetData(modelsDS);
            //this.PresentationData = modelsDS;


            //DataView view = new DataView(modelsDS.Tables[ModelsDS.MODELDETAILSLISTASSETS]);
            //view.Sort = ModelsDS.ASSETDESCRIPTION + " ASC";

            //this.PresentationGrid.DataSource = view.ToTable();
            //PresentationGrid.DataBind();
        }

        public override void LoadPage()
        {

            base.LoadPage();
            this.ccid = Request.Params["ins"];
            PresentationGrid.ExportSettings.IgnorePaging = true;
            PresentationGrid.ExportSettings.FileName = "ModelDetailedStructure";
            PresentationGrid.ExportSettings.ExportOnlyData = true;

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
            {
                PresentationGrid.Columns[PresentationGrid.Columns.Count - 1].Visible = false;
                PresentationGrid.Enabled = false;
            }

            UMABroker.ReleaseBrokerManagedComponent(objUser);
        }

        public override void PopulatePage(DataSet ds)
        {
            //DataView ModelListView = new DataView(ds.Tables[ModelsDS.MODELLIST]);
            //ModelListView.Sort = ModelsDS.MODELNAME + " ASC";
        }

        protected void btnBackToListing(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Administration/AdministrationView.aspx");
        }

        #region Telerik Code

        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e)
        {

        }

        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;

            switch (e.DetailTableView.Name)
            {
                case "Products":
                    {
                        string assetID = dataItem.GetDataKeyValue("AssetID").ToString();

                        IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                        ModelsDS modelsDS = new Oritax.TaxSimp.DataSets.ModelsDS();

                        modelsDS.GetDetails = true;
                        modelsDS.DetailsGUID = new Guid(this.cid);
                        organization.GetData(modelsDS);

                        DataView view = new DataView(modelsDS.Tables[ModelsDS.MODELDETAILSLISTPRO]);
                        view.Sort = ModelsDS.PRODESCRIPTION + " ASC";
                        view.RowFilter = ModelsDS.ASSETID + "= '" + assetID + "'";

                        e.DetailTableView.DataSource = view.ToTable();

                        this.UMABroker.ReleaseBrokerManagedComponent(organization);

                        break;
                    }
            }
        }

        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem && !e.Item.IsInEditMode)
            {
               
            }
        }

        protected void SetURLForDrillDown(GridDataItem dataItem, string columnName, string baseURL)
        {
            TableCell cell = dataItem[columnName];
            if (!System.DBNull.Value.Equals(((System.Data.DataRowView)(((Telerik.Web.UI.GridItem)(dataItem)).DataItem)).Row[columnName]))
            {
                double value = Convert.ToDouble(((System.Data.DataRowView)(((Telerik.Web.UI.GridItem)(dataItem)).DataItem)).Row[columnName]);
                if (value != 0)
                    cell.Text = "<a style='color:Blue' href='" + baseURL + "'>" + value.ToString("C") + "</a>";
            }
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            LossAndGainsReportDS ds = new LossAndGainsReportDS();

            if (this.C1InputStartDate.Date.HasValue)
                ds.StartDate = this.C1InputStartDate.Date.Value;

            if (this.C1InputEndDate.Date.HasValue)
                ds.EndDate = this.C1InputEndDate.Date.Value;

            clientData.GetData(ds);
            this.PresentationData = ds;

            this.PopulatePage(this.PresentationData);

            UMABroker.ReleaseBrokerManagedComponent(clientData);

            ScriptManager sm = ScriptManager.GetCurrent(Page);

            DataView view = new DataView(ds.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS]);
            view.Sort = LossAndGainsReportDS.TRANSACTIONDATE + " DESC";

            this.PresentationGrid.DataSource = view.ToTable();
        }
        protected void GenerateReport(object sender, EventArgs e)
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"].ToString()));
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            LossAndGainsReportDS ds = new LossAndGainsReportDS();

            if (this.C1InputStartDate.Date.HasValue)
                ds.StartDate = this.C1InputStartDate.Date.Value;

            if (this.C1InputEndDate.Date.HasValue)
                ds.EndDate = this.C1InputEndDate.Date.Value;

            clientData.GetData(ds);
            this.PresentationData = ds;
            DataView view = new DataView(ds.Tables[LossAndGainsReportDS.TAXREPORTINGMATRIXTABLE]);
            view.Sort = LossAndGainsReportDS.ACCOUNTNAME + " ASC";
            this.PresentationGrid.DataSource = view.ToTable();
            PresentationGrid.DataBind();
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            BankTransactionDS banktransacationDS = new BankTransactionDS();
            MISTransactionDS mISTransactionDS = new Oritax.TaxSimp.DataSets.MISTransactionDS();
            ASXTransactionDS aSXTransactionDS = new Oritax.TaxSimp.DataSets.ASXTransactionDS();
            DIVTransactionDS dIVTransactionDS = new Oritax.TaxSimp.DataSets.DIVTransactionDS();
            TDTransactionDS tdTransactionDS = new Oritax.TaxSimp.DataSets.TDTransactionDS();
            ManualTransactionDS manualTransactionDS = new Oritax.TaxSimp.DataSets.ManualTransactionDS();

            clientData.GetData(banktransacationDS);
            clientData.GetData(mISTransactionDS);
            clientData.GetData(aSXTransactionDS);
            clientData.GetData(dIVTransactionDS);
            clientData.GetData(tdTransactionDS);
            clientData.GetData(manualTransactionDS);

            DataSet excelDataset = new DataSet();
            excelDataset.Merge(PresentationData, true, MissingSchemaAction.Add);
            excelDataset.Merge(banktransacationDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(mISTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(aSXTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(dIVTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(tdTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(manualTransactionDS, true, MissingSchemaAction.Add);

            if (excelDataset.Tables.Contains("HoldingSummary"))
            {
                excelDataset.Tables["HoldingSummary"].Columns.Remove("TDID");
                excelDataset.Tables["HoldingSummary"].Columns.Remove("ProductID");
            }

            if (excelDataset.Tables.Contains("Cash Transactions"))
            {
                excelDataset.Tables["Cash Transactions"].Columns.Remove("InstitutionID");
                excelDataset.Tables["Cash Transactions"].Columns.Remove("ID");
                excelDataset.Tables["Cash Transactions"].Columns.Remove("DividendID");
            }

            if (excelDataset.Tables.Contains("MIS Transactions"))
                excelDataset.Tables["MIS Transactions"].Columns.Remove("ID");

            if (excelDataset.Tables.Contains("TDs Transactions"))
            {
                excelDataset.Tables["TDs Transactions"].Columns.Remove("ID");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("AccountName");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("DividendID");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("InstitutionID");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("Product");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("BSB");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("AccountType");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("AccountNo");
            }

            if (excelDataset.Tables.Contains("Manual Transactions"))
                excelDataset.Tables["Manual Transactions"].Columns.Remove("ID");

            if (excelDataset.Tables.Contains("ProductBreakDown"))
                excelDataset.Tables.Remove("ProductBreakDown");
            if (excelDataset.Tables.Contains("TDBreakDown"))
                excelDataset.Tables.Remove("TDBreakDown");
            if (excelDataset.Tables.Contains("ContactTable"))
                excelDataset.Tables.Remove("ContactTable");

            ExcelHelper.ToExcel(excelDataset, ClientHeaderInfo1.ClientName + ".xls", this.Page.Response);
        }

        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.Contains("Export"))
            {
                PresentationGrid.MasterTableView.HierarchyDefaultExpanded = true; // for the first level
                PresentationGrid.MasterTableView.DetailTables[0].HierarchyDefaultExpanded = true; // for the second level 
            }
            else if (e.CommandName.ToLower() == "delete")
            {
                if ("Assets".Equals(e.Item.OwnerTableView.Name))
                {
                    selectAssetID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["AssetID"].ToString();

                    UMABroker.SaveOverride = true;
                    IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                    ModelsDS modelsDS = new ModelsDS();
                    modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.DeleteAsset;
                    modelsDS.ModelEntityID = new Guid(this.cid);
                    modelsDS.AssetEntityID = new Guid(selectAssetID);

                    iorg.SetData(modelsDS);
                    UMABroker.SetComplete();
                    UMABroker.SetStart();
                    e.Item.Edit = false;
                    this.PopulateForm();
                }
                else if ("Products".Equals(e.Item.OwnerTableView.Name))
                {
                    string parentID = string.Empty;

                    GridDataItem parentItem = (GridDataItem)(e.Item.OwnerTableView.ParentItem);

                    if (parentItem != null)
                        parentID = parentItem.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["AssetID"].ToString();

                    string productID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ProID"].ToString();
                    UMABroker.SaveOverride = true;
                    IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                    ModelsDS modelsDS = new ModelsDS();
                    modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.DeleteProduct;
                    modelsDS.ModelEntityID = new Guid(this.cid);
                    modelsDS.AssetEntityID = new Guid(parentID);
                    modelsDS.ProductEntityID = new Guid(productID);
                    iorg.SetData(modelsDS);

                    UMABroker.SetComplete();
                    UMABroker.SetStart();
                    e.Item.Edit = false;
                    this.PopulateForm();
                }
            }

            if (e.CommandName.ToLower() == "update")
            {
                if ("Products".Equals(e.Item.OwnerTableView.Name))
                {
                    ModelsDS modelsDS = new ModelsDS();

                    string parentID = string.Empty;

                    GridDataItem parentItem = (GridDataItem)(e.Item.OwnerTableView.ParentItem);

                    if (parentItem != null)
                        parentID = parentItem.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["AssetID"].ToString();

                    GridDataItem gdItem = (e.Item as GridDataItem);
                    modelsDS.ProMinAllocation = Double.Parse(((TextBox)(((GridEditFormItem)(e.Item))["ProMinAllocation"].Controls[0])).Text);
                    modelsDS.ProNeutralPercentage = Double.Parse(((TextBox)(((GridEditFormItem)(e.Item))["ProNeutralPercentage"].Controls[0])).Text);
                    modelsDS.ProDynamicPercentage = Double.Parse(((TextBox)(((GridEditFormItem)(e.Item))["ProDynamicPercentage"].Controls[0])).Text);
                    modelsDS.ProMaxAllocation = Double.Parse(((TextBox)(((GridEditFormItem)(e.Item))["ProMaxAllocation"].Controls[0])).Text);

                    string productID = e.Item.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["ProID"].ToString();
                    UMABroker.SaveOverride = true;
                    IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

                    modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.UpdateProduct;
                    modelsDS.ModelEntityID = new Guid(this.cid);
                    modelsDS.AssetEntityID = new Guid(parentID);
                    modelsDS.ProductEntityID = new Guid(productID);
                    iorg.SetData(modelsDS);


                    UMABroker.SetComplete();
                    UMABroker.SetStart();
                    e.Item.Edit = false;
                    UMABroker.ReleaseBrokerManagedComponent(iorg);
                }


            }


        }

        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e)
        {

        }

        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e)
        {

        }

        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("Assets".Equals(e.Item.OwnerTableView.Name))
            {
                GridEditFormInsertItem EditForm = (GridEditFormInsertItem)e.Item;
                RadComboBox combo = (RadComboBox)EditForm["AssetDescription"].Controls[0];
                string ProdWorkstation = combo.SelectedItem.Text;

                UMABroker.SaveOverride = true;
                selectAssetID = ((RadComboBox)(((GridEditableItem)(e.Item))["AssetDescription"].Controls[0])).SelectedItem.Value;

                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                ModelsDS modelsDS = new ModelsDS();
                modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.NewAsset;
                modelsDS.ModelEntityID = new Guid(this.cid);
                modelsDS.AssetEntityID = new Guid(selectAssetID);
                iorg.SetData(modelsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Item.Edit = false;
                this.PresentationGrid.Rebind();

            }
            else if ("Products".Equals(e.Item.OwnerTableView.Name))
            {
                string parentID = string.Empty;

                GridDataItem parentItem = (GridDataItem)(e.Item.OwnerTableView.ParentItem);

                if (parentItem != null)
                    parentID = parentItem.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["AssetID"].ToString();

                GridEditFormInsertItem EditForm = (GridEditFormInsertItem)e.Item;
                RadComboBox combo = (RadComboBox)EditForm["ProDescription"].Controls[0];
                string ProdWorkstation = combo.SelectedItem.Text;

                string productID = ((RadComboBox)(((GridEditableItem)(e.Item))["ProDescription"].Controls[0])).SelectedItem.Value;
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                ModelsDS modelsDS = new ModelsDS();

                GridDataItem gdItem = (e.Item as GridDataItem);
                modelsDS.ProMinAllocation = Double.Parse(((TextBox)(((GridEditFormItem)(e.Item))["ProMinAllocation"].Controls[0])).Text);
                modelsDS.ProNeutralPercentage = Double.Parse(((TextBox)(((GridEditFormItem)(e.Item))["ProNeutralPercentage"].Controls[0])).Text);
                modelsDS.ProDynamicPercentage = Double.Parse(((TextBox)(((GridEditFormItem)(e.Item))["ProDynamicPercentage"].Controls[0])).Text);
                modelsDS.ProMaxAllocation = Double.Parse(((TextBox)(((GridEditFormItem)(e.Item))["ProMaxAllocation"].Controls[0])).Text);

                modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.NewProduct;
                modelsDS.ModelEntityID = new Guid(this.cid);
                modelsDS.AssetEntityID = new Guid(parentID);
                modelsDS.ProductEntityID = new Guid(productID);

                iorg.SetData(modelsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Item.Edit = false;
                this.PresentationGrid.Rebind();
            }


        }

        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;
            if (item != null && item.IsInEditMode && item.ItemIndex != -1)
            {
                if (item.OwnerTableView.Name == ((RadGrid)sender).MasterTableView.Name)
                {
                    (item.EditManager.GetColumnEditor("CustomerID").ContainerControl.Controls[0] as TextBox).Enabled = false;
                }
                else if (item.OwnerTableView.Name == "Details")
                {
                    (item.EditManager.GetColumnEditor("ProductID").ContainerControl.Controls[0] as TextBox).Enabled = false;
                }
            }
        }

        private void DisplayMessage(string text)
        {
            PresentationGrid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
        }

        #endregion
    }
}
