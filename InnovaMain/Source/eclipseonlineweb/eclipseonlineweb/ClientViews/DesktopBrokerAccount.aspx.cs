﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;

namespace eclipseonlineweb.ClientViews
{
    public partial class DesktopBrokerAccount : UMABasePage
    {
        private string _cid = string.Empty;

        protected override void Intialise()
        {
            base.Intialise();
            
            ASXControl.Saved += (CID, CLID, CSID) =>
                                    {
                                        HideShowDesktopBrokerGrid(false);
                                        SaveASXData(CID, CLID, CSID, DatasetCommandTypes.Update);
                                    };
            ASXControl.Canceled += () => HideShowDesktopBrokerGrid(false);
            ASXControl.ValidationFailed += () => HideShowDesktopBrokerGrid(true);
            ASXControl.SaveData += (asxCid, ds) =>
            {
                ((DesktopBrokerAccountDS)ds).RepUnitCID = new Guid(_cid);
                if (asxCid == Guid.Empty.ToString())
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(asxCid, ds);
                }
                HideShowDesktopBrokerGrid(false);
            };

            ASXControlExisting.Saved += desktopBrokerAccountDs =>
            {
                ShowHideExisting(false);
                SaveASXData(desktopBrokerAccountDs, DatasetCommandTypes.Update);
            };
            ASXControlExisting.Canceled += () => ShowHideExisting(false);
            ASXControlExisting.ValidationFailed += () => ShowHideExisting(true);
            ASXControlExisting.SaveData += (asxCid, ds) =>
            {
                ((DesktopBrokerAccountDS)ds).RepUnitCID = new Guid(_cid);
                if (asxCid == Guid.Empty.ToString())
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(asxCid, ds);
                }
                ShowHideExisting(false);

            };
        }

        private void HideShowDesktopBrokerGrid(bool show)
        {
            OVER.Visible = ASXModal.Visible = show;
        }

        private void ShowHideExisting(bool show)
        {
            OVER.Visible = ASXModalExsiting.Visible = show;
        }
        private void SaveASXData(DesktopBrokerAccountDS desktopBrokerAccountDs, DatasetCommandTypes commandType)
        {
            desktopBrokerAccountDs.CommandType = commandType;
            SaveData(_cid, desktopBrokerAccountDs);
            PresentationGrid.Rebind();
        }

        private DataSet SaveASXData(Guid CID, Guid CLID, Guid CSID, DatasetCommandTypes commandType)
        {
            DesktopBrokerAccountDS ds = new DesktopBrokerAccountDS { CommandType = commandType };
            DataRow dr = ds.DesktopBrokerAccountsTable.NewRow();
            dr[ds.DesktopBrokerAccountsTable.CLID] = CLID;
            dr[ds.DesktopBrokerAccountsTable.CSID] = CSID;
            ds.Tables[ds.DesktopBrokerAccountsTable.TABLENAME].Rows.Add(dr);
            ds.RepUnitCID = new Guid(_cid);
            SaveData(_cid, ds);
            PresentationGrid.Rebind();
            return ds;

        }
        
        public override void LoadPage()
        {
            _cid = Request.QueryString["ins"];
            if (!IsPostBack)
            {
                var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
                if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
                {
                    btn.Visible = false;
                    ImageButton1.Visible = false;
                    PresentationGrid.Columns.FindByUniqueNameSafe("DeleteColumn").Visible = false;
                    PresentationGrid.Columns.FindByUniqueNameSafe("EditColumn").Visible = false;
                }
                UMABroker.ReleaseBrokerManagedComponent(objUser);
            }
        }
        private void GetData()
        {
            _cid = Request.QueryString["ins"];

            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(_cid));
            var ds = new DesktopBrokerAccountDS();
            clientData.GetData(ds);
            PresentationData = ds;
            var summaryView = new DataView(ds.Tables[ds.DesktopBrokerAccountsTable.TABLENAME]) { Sort = ds.DesktopBrokerAccountsTable.ACCOUNTNAME + " DESC" };
            PresentationGrid.DataSource = summaryView;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }
        
        protected void LnkbtnAddASXClick(object sender, EventArgs e)
        {
            Guid gCid = Guid.Empty;
            ASXControl.SetEntity(gCid);
            HideShowDesktopBrokerGrid(true);
        }

        protected void LnkbtnAddExistngASXClick(object sender, EventArgs e)
        {
            ASXControlExisting.VisibleExistingAccountGrid();
            ShowHideExisting(true);
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var asxCid = new Guid(dataItem["CID"].Text);
                    ASXControl.SetEntity(asxCid);
                    HideShowDesktopBrokerGrid(true);
                    break;
                case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var linkEntiyCID = new Guid(dataItem["CID"].Text);
                    var linkEntiyClid = new Guid(dataItem["CLID"].Text);
                    var linkEntiyCsid = new Guid(dataItem["CSID"].Text);
                    DataSet ds = SaveASXData(linkEntiyCID, linkEntiyClid, linkEntiyCsid, DatasetCommandTypes.Delete);
                    PresentationGrid.Rebind();
                    ScriptManager.RegisterStartupScript(Page, GetType(), "MyScript", "alert('" + ds.ExtendedProperties["Message"] + "');", true);
                    break;
            }
        }
    }
}