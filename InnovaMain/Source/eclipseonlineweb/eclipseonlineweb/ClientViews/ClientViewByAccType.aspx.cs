﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;

namespace eclipseonlineweb
{
    public partial class ClientViewByAccType : UMABasePage
    {
        string ccid = string.Empty;
        string selectAssetID = string.Empty;

        protected override void GetBMCData()
        {
            cid = Request.QueryString["ins"].ToString();
            if (!IsPostBack)
            {

                IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(cid));
                LossAndGainsReportDS ds = new LossAndGainsReportDS();

                clientData.GetData(ds);
                PresentationData = ds;

                PopulatePage(PresentationData);

                UMABroker.ReleaseBrokerManagedComponent(clientData);

                ScriptManager sm = ScriptManager.GetCurrent(Page);


                DataView view = new DataView(ds.Tables[LossAndGainsReportDS.TAXREPORTINGMATRIXTABLE]);
                view.Sort = LossAndGainsReportDS.ACCOUNTNAME + " ASC";

                PresentationGrid.DataSource = view.ToTable();
                PresentationGrid.DataBind();
            }
        }

        private void PopulateForm()
        {
        }

        public override void LoadPage()
        {

            base.LoadPage();
            ccid = Request.Params["ins"];
            PresentationGrid.ExportSettings.IgnorePaging = true;
            PresentationGrid.ExportSettings.FileName = "ModelDetailedStructure";
            PresentationGrid.ExportSettings.ExportOnlyData = true;

            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
            {
                PresentationGrid.Columns[PresentationGrid.Columns.Count - 1].Visible = false;
                PresentationGrid.Enabled = false;
            }

            UMABroker.ReleaseBrokerManagedComponent(objUser);
        }

        public override void PopulatePage(DataSet ds)
        {
        }

        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            return true;
        }

        protected void btnBackToListing(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Administration/AdministrationView.aspx");
        }

        #region Telerik Code


        protected void PresentationGridDetailTableDataBind(object source, GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = e.DetailTableView.ParentItem;

            switch (e.DetailTableView.Name)
            {
                case "Products":
                    {
                        string assetID = dataItem.GetDataKeyValue("AssetID").ToString();

                        var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                        var modelsDS = new ModelsDS { GetDetails = true, DetailsGUID = new Guid(cid) };

                        organization.GetData(modelsDS);

                        var view = new DataView(modelsDS.Tables[ModelsDS.MODELDETAILSLISTPRO])
                                       {
                                           Sort = ModelsDS.PRODESCRIPTION + " ASC",
                                           RowFilter = ModelsDS.ASSETID + "= '" + assetID + "'"
                                       };

                        e.DetailTableView.DataSource = view.ToTable();

                        UMABroker.ReleaseBrokerManagedComponent(organization);

                        break;
                    }
            }
        }

        protected void PresentationGridItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem && !e.Item.IsInEditMode)
            {
                var dataItem = e.Item as GridDataItem;
                string rowID = dataItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"].ToString();
                TableCell cellAccountType = dataItem["AccountType"];

                string baseURL = "ClientViewByAccTypeDetails.aspx?ins=" + cid + "&AccType=" + cellAccountType.Text + "&AccNo=" + rowID;
                if (cellAccountType.Text == "CASH" || cellAccountType.Text == "TDs" || cellAccountType.Text == "FUND" || cellAccountType.Text == "ASX")
                {
                    SetURLForDrillDown(dataItem, "INVESTMENT", baseURL + "&SysTran=Investment");
                    SetURLForDrillDown(dataItem, "TransferIn", baseURL + "&SysTran=Transfer In");
                    SetURLForDrillDown(dataItem, "TransferOut", baseURL + "&SysTran=Transfer Out");
                    SetURLForDrillDown(dataItem, "Interest", baseURL + "&SysTran=Interest");
                    SetURLForDrillDown(dataItem, "Rental", baseURL + "&SysTran=Rental");
                    SetURLForDrillDown(dataItem, "Dividend", baseURL + "&SysTran=Dividend");
                    SetURLForDrillDown(dataItem, "Distribution", baseURL + "&SysTran=Distribution");
                    SetURLForDrillDown(dataItem, "Income", baseURL + "&SysTran=Income");
                    SetURLForDrillDown(dataItem, "Expenses", baseURL + "&SysTran=Expense");
                }
            }
        }

        protected void SetURLForDrillDown(GridDataItem dataItem, string columnName, string baseURL)
        {
            TableCell cell = dataItem[columnName];
            if (!DBNull.Value.Equals(((DataRowView)(dataItem.DataItem)).Row[columnName]))
            {
                double value = Convert.ToDouble(((DataRowView)(dataItem.DataItem)).Row[columnName]);
                if (value != 0)
                    cell.Text = "<a style='color:Blue' href='" + baseURL + "'>" + value.ToString("C") + "</a>";
            }
        }

        protected void PresentationGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(cid));
            var ds = new LossAndGainsReportDS();

            if (C1InputStartDate.Date.HasValue)
                ds.StartDate = C1InputStartDate.Date.Value;

            if (C1InputEndDate.Date.HasValue)
                ds.EndDate = C1InputEndDate.Date.Value;

            clientData.GetData(ds);
            PresentationData = ds;

            PopulatePage(PresentationData);

            UMABroker.ReleaseBrokerManagedComponent(clientData);

            ScriptManager sm = ScriptManager.GetCurrent(Page);


            var view = new DataView(ds.Tables[LossAndGainsReportDS.TAXREPORTINGMATRIXTABLE]) { Sort = LossAndGainsReportDS.ACCOUNTNAME + " ASC" };

            PresentationGrid.DataSource = view.ToTable();
        }
        protected void GenerateReport(object sender, EventArgs e)
        {
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"].ToString()));
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            var ds = new LossAndGainsReportDS();

            if (C1InputStartDate.Date.HasValue)
                ds.StartDate = C1InputStartDate.Date.Value;

            if (C1InputEndDate.Date.HasValue)
                ds.EndDate = C1InputEndDate.Date.Value;

            clientData.GetData(ds);
            PresentationData = ds;
            var view = new DataView(ds.Tables[LossAndGainsReportDS.TAXREPORTINGMATRIXTABLE]) { Sort = LossAndGainsReportDS.ACCOUNTNAME + " ASC" };
            PresentationGrid.DataSource = view.ToTable();
            PresentationGrid.DataBind();
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        protected void BtnReportClick(object sender, EventArgs e)
        {
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(cid));
            var banktransacationDS = new BankTransactionDS();
            var misTransactionDS = new MISTransactionDS();
            var asxTransactionDS = new ASXTransactionDS();
            var divTransactionDS = new DIVTransactionDS();
            var tdTransactionDS = new TDTransactionDS();
            var manualTransactionDS = new ManualTransactionDS();

            clientData.GetData(banktransacationDS);
            clientData.GetData(misTransactionDS);
            clientData.GetData(asxTransactionDS);
            clientData.GetData(divTransactionDS);
            clientData.GetData(tdTransactionDS);
            clientData.GetData(manualTransactionDS);

            var excelDataset = new DataSet();
            excelDataset.Merge(PresentationData, true, MissingSchemaAction.Add);
            excelDataset.Merge(banktransacationDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(misTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(asxTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(divTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(tdTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(manualTransactionDS, true, MissingSchemaAction.Add);

            if (excelDataset.Tables.Contains("HoldingSummary"))
            {
                excelDataset.Tables["HoldingSummary"].Columns.Remove("TDID");
                excelDataset.Tables["HoldingSummary"].Columns.Remove("ProductID");
            }

            if (excelDataset.Tables.Contains("Cash Transactions"))
            {
                excelDataset.Tables["Cash Transactions"].Columns.Remove("InstitutionID");
                excelDataset.Tables["Cash Transactions"].Columns.Remove("ID");
                excelDataset.Tables["Cash Transactions"].Columns.Remove("DividendID");
            }

            if (excelDataset.Tables.Contains("MIS Transactions"))
                excelDataset.Tables["MIS Transactions"].Columns.Remove("ID");

            if (excelDataset.Tables.Contains("TDs Transactions"))
            {
                excelDataset.Tables["TDs Transactions"].Columns.Remove("ID");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("AccountName");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("DividendID");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("InstitutionID");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("Product");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("BSB");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("AccountType");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("AccountNo");
            }

            if (excelDataset.Tables.Contains("Manual Transactions"))
                excelDataset.Tables["Manual Transactions"].Columns.Remove("ID");

            if (excelDataset.Tables.Contains("ProductBreakDown"))
                excelDataset.Tables.Remove("ProductBreakDown");
            if (excelDataset.Tables.Contains("TDBreakDown"))
                excelDataset.Tables.Remove("TDBreakDown");
            if (excelDataset.Tables.Contains("ContactTable"))
                excelDataset.Tables.Remove("ContactTable");

            ExcelHelper.ToExcel(excelDataset, ClientHeaderInfo1.ClientName + ".xls", Page.Response);
        }

        protected void PresentationGridItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.Contains("Export"))
            {
                PresentationGrid.MasterTableView.HierarchyDefaultExpanded = true; // for the first level
                PresentationGrid.MasterTableView.DetailTables[0].HierarchyDefaultExpanded = true; // for the second level 
            }
            else if (e.CommandName.ToLower() == "delete")
            {
                if ("Assets".Equals(e.Item.OwnerTableView.Name))
                {
                    selectAssetID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["AssetID"].ToString();

                    UMABroker.SaveOverride = true;
                    var iorg = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                    var modelsDS = new ModelsDS
                                       {
                                           ModelDataSetOperationType = ModelDataSetOperationType.DeleteAsset,
                                           ModelEntityID = new Guid(cid),
                                           AssetEntityID = new Guid(selectAssetID)
                                       };

                    iorg.SetData(modelsDS);
                    UMABroker.SetComplete();
                    UMABroker.SetStart();
                    e.Item.Edit = false;
                    PopulateForm();
                }
                else if ("Products".Equals(e.Item.OwnerTableView.Name))
                {
                    string parentID = string.Empty;

                    var parentItem = e.Item.OwnerTableView.ParentItem;

                    if (parentItem != null)
                        parentID = parentItem.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["AssetID"].ToString();

                    string productID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ProID"].ToString();
                    UMABroker.SaveOverride = true;
                    var iorg = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                    var modelsDS = new ModelsDS();
                    modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.DeleteProduct;
                    modelsDS.ModelEntityID = new Guid(cid);
                    modelsDS.AssetEntityID = new Guid(parentID);
                    modelsDS.ProductEntityID = new Guid(productID);
                    iorg.SetData(modelsDS);

                    UMABroker.SetComplete();
                    UMABroker.SetStart();
                    e.Item.Edit = false;
                    PopulateForm();
                }
            }

            if (e.CommandName.ToLower() == "update")
            {
                if ("Products".Equals(e.Item.OwnerTableView.Name))
                {
                    var modelsDS = new ModelsDS();

                    string parentID = string.Empty;

                    var parentItem = e.Item.OwnerTableView.ParentItem;

                    if (parentItem != null)
                        parentID = parentItem.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["AssetID"].ToString();

                    var gdItem = (e.Item as GridDataItem);
                    modelsDS.ProMinAllocation = Double.Parse(((TextBox)(((GridEditFormItem)(e.Item))["ProMinAllocation"].Controls[0])).Text);
                    modelsDS.ProNeutralPercentage = Double.Parse(((TextBox)(((GridEditFormItem)(e.Item))["ProNeutralPercentage"].Controls[0])).Text);
                    modelsDS.ProDynamicPercentage = Double.Parse(((TextBox)(((GridEditFormItem)(e.Item))["ProDynamicPercentage"].Controls[0])).Text);
                    modelsDS.ProMaxAllocation = Double.Parse(((TextBox)(((GridEditFormItem)(e.Item))["ProMaxAllocation"].Controls[0])).Text);

                    string productID = e.Item.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["ProID"].ToString();
                    UMABroker.SaveOverride = true;
                    var iorg = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

                    modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.UpdateProduct;
                    modelsDS.ModelEntityID = new Guid(cid);
                    modelsDS.AssetEntityID = new Guid(parentID);
                    modelsDS.ProductEntityID = new Guid(productID);
                    iorg.SetData(modelsDS);

                    UMABroker.SetComplete();
                    UMABroker.SetStart();
                    e.Item.Edit = false;
                    UMABroker.ReleaseBrokerManagedComponent(iorg);
                }
            }
        }


        protected void PresentationGridInsertCommand(object source, GridCommandEventArgs e)
        {
            if ("Assets".Equals(e.Item.OwnerTableView.Name))
            {
                var editForm = (GridEditFormInsertItem)e.Item;
                var combo = (RadComboBox)editForm["AssetDescription"].Controls[0];
                string prodWorkstation = combo.SelectedItem.Text;

                UMABroker.SaveOverride = true;
                selectAssetID = ((RadComboBox)(((GridEditableItem)(e.Item))["AssetDescription"].Controls[0])).SelectedItem.Value;

                var iorg = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                var modelsDS = new ModelsDS();
                modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.NewAsset;
                modelsDS.ModelEntityID = new Guid(cid);
                modelsDS.AssetEntityID = new Guid(selectAssetID);
                iorg.SetData(modelsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Item.Edit = false;
                PresentationGrid.Rebind();

            }
            else if ("Products".Equals(e.Item.OwnerTableView.Name))
            {
                string parentID = string.Empty;

                var parentItem = e.Item.OwnerTableView.ParentItem;

                if (parentItem != null)
                    parentID = parentItem.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["AssetID"].ToString();

                var editForm = (GridEditFormInsertItem)e.Item;
                var combo = (RadComboBox)editForm["ProDescription"].Controls[0];
                string prodWorkstation = combo.SelectedItem.Text;

                string productID = ((RadComboBox)(((GridEditableItem)(e.Item))["ProDescription"].Controls[0])).SelectedItem.Value;
                UMABroker.SaveOverride = true;
                var iorg = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                var modelsDS = new ModelsDS();

                var gdItem = (e.Item as GridDataItem);
                modelsDS.ProMinAllocation = Double.Parse(((TextBox)(((GridEditFormItem)(e.Item))["ProMinAllocation"].Controls[0])).Text);
                modelsDS.ProNeutralPercentage = Double.Parse(((TextBox)(((GridEditFormItem)(e.Item))["ProNeutralPercentage"].Controls[0])).Text);
                modelsDS.ProDynamicPercentage = Double.Parse(((TextBox)(((GridEditFormItem)(e.Item))["ProDynamicPercentage"].Controls[0])).Text);
                modelsDS.ProMaxAllocation = Double.Parse(((TextBox)(((GridEditFormItem)(e.Item))["ProMaxAllocation"].Controls[0])).Text);

                modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.NewProduct;
                modelsDS.ModelEntityID = new Guid(cid);
                modelsDS.AssetEntityID = new Guid(parentID);
                modelsDS.ProductEntityID = new Guid(productID);

                iorg.SetData(modelsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Item.Edit = false;
                PresentationGrid.Rebind();
            }


        }

        protected void PresentationGridItemCreated(object sender, GridItemEventArgs e)
        {
            var item = e.Item as GridEditableItem;
            if (item != null && item.IsInEditMode && item.ItemIndex != -1)
            {
                if (item.OwnerTableView.Name == ((RadGrid)sender).MasterTableView.Name)
                {
                    (item.EditManager.GetColumnEditor("CustomerID").ContainerControl.Controls[0] as TextBox).Enabled = false;
                }
                else if (item.OwnerTableView.Name == "Details")
                {
                    (item.EditManager.GetColumnEditor("ProductID").ContainerControl.Controls[0] as TextBox).Enabled = false;
                }
            }
        }

        private void DisplayMessage(string text)
        {
            PresentationGrid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
        }

        #endregion
    }
}
