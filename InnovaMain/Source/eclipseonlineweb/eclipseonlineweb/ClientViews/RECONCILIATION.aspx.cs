﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.Commands;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.DataSets;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class Reconciliation : UMABasePage
    {
        private DataView _asxView;
        private DataView _misView;
        private DataView _bankView;



        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            GetData();
            var excelDataset = new DataSet();
            var asxtb=_asxView.ToTable();
            asxtb.Columns.Remove("CID");
            asxtb.Columns.Remove("EntityClientID");
            var mistb = _misView.ToTable();
            mistb.Columns.Remove("CID");
            mistb.Columns.Remove("EntityClientID");
            var banktb = _bankView.ToTable();
            banktb.Columns.Remove("CID");
            banktb.Columns.Remove("EntityClientID");
            excelDataset.Merge(asxtb, true, MissingSchemaAction.Add);
            excelDataset.Merge( mistb, true, MissingSchemaAction.Add);
            excelDataset.Merge(banktb, true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, ClientHeaderInfo1.ClientName + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", Page.Response);
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        private void GetData()
        {
            var ClientData = UMABroker.GetBMCInstance(Guid.Parse(Request.Params["ins"])) ;
            var accountsHoldingsDs = new ClientsASXAccountsHoldingsDS
            {
                Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() },
               
            };
            var misHoldingsDs = new ClientsMISAccountsHoldingsDS
            {
                Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() },

            };
            var BankHoldingsDs = new ClientsBankAccountsHoldingsDS
            {
                Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() },

            };
            if (ClientData != null)
            {
                ClientData.GetData(accountsHoldingsDs);
                ClientData.GetData(misHoldingsDs);
                ClientData.GetData(BankHoldingsDs);
                UMABroker.ReleaseBrokerManagedComponent(ClientData);
            }
            _asxView = new DataView(accountsHoldingsDs.ASXHoldingTable)
                              {
                                  Sort = accountsHoldingsDs.ASXHoldingTable.DIFFERENCE + " DESC"
                              };

         

            _misView = new DataView(misHoldingsDs.MisHoldingTable)
                              {
                                  Sort = misHoldingsDs.MisHoldingTable.DIFFERENCE + " DESC"

                              };

          

            _bankView = new DataView(BankHoldingsDs.BankAccountHoldingTable)
            {
                Sort = BankHoldingsDs.BankAccountHoldingTable.DIFFERENCE + " DESC",
                //RowFilter = "AccountType in ('CMA','CMT')"
            };
           



        }

        public override void LoadPage()
        {
            base.LoadPage();
            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
                sm.RegisterPostBackControl(btnDownload);

           
                GetData();
           

        }



        protected void ASXGrid_OnItemCreated(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridFilteringItem)) return;

            var filterItem = (GridFilteringItem)e.Item;
            foreach (GridColumn col in ASXGrid.MasterTableView.Columns)
            {
                var colUniqueName = col.UniqueName;
                var txtbox = (TextBox)filterItem[colUniqueName].Controls[0];
                txtbox.Attributes.Add("onkeydown", "doFilter(this,event,'" + colUniqueName + "','"+ASXGrid.ClientID+"')");
            }
        }

        protected void MISGrid_OnItemCreated(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridFilteringItem)) return;

            var filterItem = (GridFilteringItem)e.Item;
            foreach (GridColumn col in MISGrid.MasterTableView.Columns)
            {
                var colUniqueName = col.UniqueName;
                var txtbox = (TextBox)filterItem[colUniqueName].Controls[0];
                txtbox.Attributes.Add("onkeydown", "doFilter(this,event,'" + colUniqueName + "','" + MISGrid.ClientID + "')");
            }
        }
        protected void BankAccountGird_OnItemCreated(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridFilteringItem)) return;

            var filterItem = (GridFilteringItem)e.Item;
            foreach (GridColumn col in BankAccountGird.MasterTableView.Columns)
            {
                var colUniqueName = col.UniqueName;
                var txtbox = (TextBox)filterItem[colUniqueName].Controls[0];
                txtbox.Attributes.Add("onkeydown", "doFilter(this,event,'" + colUniqueName + "','" + BankAccountGird.ClientID + "')");
            }
        }

        protected void ASXGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            ASXGrid.DataSource = _asxView.ToTable();
        }

        protected void MisGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
          MISGrid.DataSource = _misView.ToTable();
        }
        protected void BankGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            BankAccountGird.DataSource = _bankView.ToTable();
        }
    }
}
