﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="ContactAssociation.aspx.cs" Inherits="eclipseonlineweb.ContactAssociation" %>

<%@ Register Src="../Controls/ContactAssociationControl.ascx" TagName="ContactAssociationControl"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register Src="../Controls/Individual.ascx" TagName="IndividualControl" TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <style>
        .holder
        {
            width: 100%;
            display: block;
            z-index: 6;
        }
        .content
        {
            background: #fff;
            z-index: 7; /*  padding: 28px 26px 33px 25px;*/
        }
        .popup
        {
            border-radius: 7px;
            background: #6b6a63;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 1200px;
            top: 10%;
            left: 30%;
            margin-left: -400px;
            margin-top: -40px;
            z-index: 6;
        }
        
        .overlay
        {
            width: 100%;
            opacity: 0.65;
            height: 100%;
            left: 0; /*IE*/
            top: 0;
            text-align: center;
            z-index: 5;
            position: fixed;
            background-color: #444444;
        }
        
        .wijmo-wijgrid .wijmo-wijgrid-groupheaderrow td
        {
            background-color: #DAE6F4;
            color: #000000;
            font-size: smaller;
            font-weight: bold;
            vertical-align: middle;
        }
        .wijmo-wijgrid .wijmo-wijgrid-footerrow td
        {
            font-weight: bolder;
            text-align: right;
            font-size: smaller;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <fieldset>
        <table width="100%">
            <tr>
                <td style="width: 5%">
                    <asp:ImageButton ID="btnNewContact" AlternateText="Add New Contact" ToolTip="Add New Contacts"
                        runat="server" OnClick="LnkbtnAddBankAccClick" ImageUrl="~/images/add-icon.png" />
                </td>
                <td style="width: 5%">
                    <asp:ImageButton ID="btnSave" AlternateText="Save" ToolTip="Associate Contacts" runat="server"
                        OnClick="LnkbtnAssociateContactsClick" ImageUrl="~/images/add_existing.png" Visible="true" />
                </td>
                <td style="width: 5%">
                </td>
                <td>
                </td>
                <td>
                </td>
                <td style="width: 85%;" class="breadcrumbgap">
                    <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                    <br />
                    <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <asp:HiddenField ID="hfCLID" runat="server" />
    <asp:HiddenField ID="hfCSID" runat="server" />
    <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
        PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
        GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
        AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource"
        OnItemCommand="PresentationGrid_OnItemCommand" OnItemDataBound="PresentationGrid_OnItemDataBound">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
            Name="Banks" TableLayout="Fixed">
            <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
            <HeaderStyle Font-Bold="True"></HeaderStyle>
            <Columns>
                <telerik:GridBoundColumn SortExpression="CID" ReadOnly="true" HeaderText="CID" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="CID" UniqueName="CID" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="CLID" ReadOnly="true" HeaderText="CLID"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="CLID" UniqueName="CLID" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="CSID" ReadOnly="true" HeaderText="CSID"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="CSID" UniqueName="CSID" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="FirstName" ReadOnly="true" HeaderText="Given Name"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="FirstName" UniqueName="FirstName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="true" SortExpression="MiddleName" HeaderText="Middle Name"
                    AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                    HeaderButtonType="TextButton" DataField="MiddleName" UniqueName="MiddleName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="LastName" HeaderText="Family Name" ReadOnly="true"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="LastName" UniqueName="LastName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="160px" HeaderStyle-Width="200px" SortExpression="EmailAddress"
                    ReadOnly="true" HeaderText="Email Address" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="EmailAddress"
                    UniqueName="EmailAddress">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn UniqueName="Share" HeaderText="Share" FilterControlWidth="130px"
                    DataField="Share" SortExpression="Share">
                    <ItemTemplate>
                        <telerik:RadTextBox ID="txtShare" runat="server" OnTextChanged="txtShare_OnTextChanged"
                            AutoPostBack="true">
                        </telerik:RadTextBox>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridButtonColumn ButtonType="LinkButton" CommandName="Detail" Text="Detail"
                    UniqueName="DetailColumn">
                    <HeaderStyle Width="5%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" CssClass="MyLinkButton"></ItemStyle>
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                    UniqueName="EditColumn">
                    <HeaderStyle Width="3%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn ConfirmText="Are you sure you want to remove the association?"
                    ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                    <HeaderStyle Width="3%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                </telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <asp:Panel runat="server" ID="ViewPanel" Visible="false">
        <div style="margin: 20px 0 -22px; position: relative; text-align: right;">
            <asp:ImageButton ID="Button1" runat="server" ImageAlign="AbsMiddle" ImageUrl="../images/cross_icon_normal.png"
                OnClick="Button1_OnClick" />
        </div>
        <div class="content">
            <uc:IndividualControl ID="IndividualControlView" runat="server" EnableTabs="False" />
        </div>
    </asp:Panel>
    <div id="IndividualModal" runat="server" visible="false" class="holder">
        <div class="popup">
            <div class="content">
                <uc:IndividualControl ID="IndividualControl" runat="server" />
            </div>
        </div>
    </div>
    <div id="ContactModal" runat="server" visible="false" class="holder">
        <div class="popup">
            <div class="content">
                <uc1:ContactAssociationControl ID="ContactControl" runat="server" />
            </div>
        </div>
    </div>
    <div class="overlay" id="OVER" visible="False" runat="server">
    </div>
</asp:Content>
