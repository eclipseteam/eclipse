﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Telerik.Web.UI;

namespace eclipseonlineweb.ClientViews
{
    public partial class ClientAccountProcess : UMABasePage
    {
        protected override void Intialise()
        {
            base.Intialise();

            #region Bank Account Events
            BankAccountDetails.Saved += (CLID, CSID) =>
            {
                OVER.Visible = BankAccountWindow.Visible = false;
                SaveAccountProcess(CLID, CSID, Guid.Empty);
            };
            BankAccountDetails.ValidationFailed += () =>
            {
                OVER.Visible = BankAccountWindow.Visible = true;
            };
            BankAccountDetails.Canceled += () => OVER.Visible = BankAccountWindow.Visible = false;
            #endregion

            #region ASX Events
            ASXControl.Canceled += () => OVER.Visible = DesktopBrokerWindow.Visible = false;
            ASXControl.ValidationFailed += () =>
            {
                OVER.Visible = DesktopBrokerWindow.Visible = true;
            };
            ASXControl.SaveData += (ASXCid, ds) =>
            {
                if (ASXCid == Guid.Empty.ToString())
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(ASXCid, ds);
                }
                OVER.Visible = DesktopBrokerWindow.Visible = false;
            };
            ASXControl.Saved += (desktopBrokerAccountDs) =>
            {
                OVER.Visible = DesktopBrokerWindow.Visible = false;
                var dr = desktopBrokerAccountDs.Tables[desktopBrokerAccountDs.DesktopBrokerAccountsTable.TABLENAME].Rows[0];
                var linkEntiyCLID = new Guid(dr[desktopBrokerAccountDs.DesktopBrokerAccountsTable.CLID].ToString());
                var linkEntiyCSID = new Guid(dr[desktopBrokerAccountDs.DesktopBrokerAccountsTable.CSID].ToString());
                SaveAccountProcess(linkEntiyCLID, linkEntiyCSID, Guid.Empty);
            };
            #endregion

            #region MIS Events
            MISControl.Saved += (CLID, CSID, FundID,reinvet) =>
            {
                OVER.Visible = MisAccountWindow.Visible = false;
                SaveAccountProcess(CLID, CSID, FundID);
            };
            MISControl.ValidationFailed += () =>
            {
                OVER.Visible = MisAccountWindow.Visible = true;
            };

            MISControl.Canceled += () => OVER.Visible = MisAccountWindow.Visible = false;

            #endregion
        }

        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];

            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");

            if (objUser.UserType != UserType.Innova)
            {

                gvAccountProcess.Columns[gvAccountProcess.Columns.Count - 2].Visible = false;
                gvAccountProcess.Columns[gvAccountProcess.Columns.Count - 3].Visible = false;
                btnRefreshPortfolioFromSm.Visible = false;
                btnRefreshDataFromSma.Visible = false;
            }
            UMABroker.ReleaseBrokerManagedComponent(objUser);
            //Here we get the page name
            string pageName = Request.QueryString["PageName"];
            btnBack.Visible = !string.IsNullOrEmpty(pageName);
        }

        protected void btnbacks_Onclick(object sender, EventArgs e)
        {
            Response.Redirect("~/SysAdministration/DuplicatedAssociationsClients.aspx");
        }

        protected void btnRefreshPortfolioFromSma_OnClick(object sender, EventArgs e)
        {
            ProcessPortfolio();
            Response.Redirect("ClientAccountProcess.aspx?ins=" + this.cid);
        }

        protected void btnRefreshDataFromSma_OnClick(object sender, EventArgs e)
        {
            ProcessData();
            Response.Redirect("ClientAccountProcess.aspx?ins=" + this.cid);
        }

        protected void btnRefreshAccountOtherID_OnClick(object sender, EventArgs e)
        {
            ProcessPortfolioSetOtherID();
            Response.Redirect("ClientAccountProcess.aspx?ins=" + this.cid);
        }

        private void ProcessPortfolioSetOtherID()
        {
            var ds = new SMAImportProcessDS();
            var orgUnit = this.UMABroker.GetBMCInstance(new Guid(this.cid)) as IOrganizationUnit;
            ds.Variables.Add("MemberCode", orgUnit.SuperMemberID);
            ds.Variables.Add("OrgCid", orgUnit.CID.ToString());
            ds.Variables.Add("EclipseID", orgUnit.ClientId.ToString());

            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportSMASetOtherID;
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            organization.GetData(ds);
        }

        private void ProcessPortfolio()
        {
            var ds = new ImportProcessDS();
            var orgUnit = this.UMABroker.GetBMCInstance(new Guid(this.cid)) as IOrganizationUnit;
            ds.Variables.Add("MemberCode", orgUnit.SuperMemberID);
            ds.Variables.Add("OrgCid", orgUnit.CID.ToString());
            ds.Variables.Add("EclipseID", orgUnit.ClientId.ToString());

            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportSMAClientPortfolioUpdate;
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            organization.GetData(ds);
        }

        private void ProcessData()
        {
            var ds = new SMAImportProcessDS { StartDate = DateTime.Now.AddYears(-2), EndDate = DateTime.Now };
            var orgUnit = UMABroker.GetBMCInstance(new Guid(this.cid)) as IOrganizationUnit;
            ds.Variables.Add("MemberCode", orgUnit.SuperMemberID);
            ds.Variables.Add("OrgCid", orgUnit.CID.ToString());
            ds.Variables.Add("EclipseID", orgUnit.ClientId.ToString());
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportSMATransactionsIndividual;
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            organization.GetData(ds);
        }

        private void SaveAccountProcess(Guid clid, Guid csid, Guid fundId)
        {
            var rowIndex = Convert.ToInt32(hfSelectedRowIndex.Value);
            var linkEntiyClid = clid;
            var linkEntiyCsid = csid;
            string linkedEntityType = string.Empty;
            var modelId = Guid.Empty;
            var assetId = Guid.Empty;
            var taskId = Guid.Empty;
            for (int i = 0; i < gvAccountProcess.Items.Count; i++)
            {
                if (i == rowIndex)
                {
                    linkedEntityType = gvAccountProcess.Items[i]["LinkedEntityType"].Text;
                    modelId = new Guid(gvAccountProcess.Items[i]["ModelID"].Text);
                    assetId = new Guid(gvAccountProcess.Items[i]["AssetID"].Text);
                    taskId = new Guid(gvAccountProcess.Items[i]["TaskID"].Text);
                    break;
                }
            }
            SaveAccountProcess(linkEntiyClid, linkEntiyCsid, linkedEntityType, taskId, assetId, modelId, fundId, DatasetCommandTypes.Update);
            gvAccountProcess.Rebind();
        }

        private void SaveAccountProcess(Guid linkEntiyClid, Guid linkEntiyCsid, string linkedEntityType, Guid taskId, Guid assetId, Guid modelId, Guid linkEntiyFundId, DatasetCommandTypes commandType)
        {
            var ds = new ClientAccountProcessDS { Command = commandType };

            var dr = ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE].NewRow();
            dr[ClientAccountProcessDS.MODELID] = modelId;
            dr[ClientAccountProcessDS.ASSETID] = assetId;
            dr[ClientAccountProcessDS.LINKENTIYCLID] = linkEntiyClid;
            dr[ClientAccountProcessDS.LINKENTIYCSID] = linkEntiyCsid;
            dr[ClientAccountProcessDS.LINKEDENTITYTYPE] = linkedEntityType;
            dr[ClientAccountProcessDS.TASKID] = taskId;
            dr[ClientAccountProcessDS.LINKENTIYFUNDID] = linkEntiyFundId;

            ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE].Rows.Add(dr);
            SaveData(cid, ds);
            hfSelectedRowIndex.Value = "-1";
        }


        protected void gvAccountProcess_PreRender(object sender, EventArgs e)
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType != UserType.Innova)
            {
                GridColumn gridColproduct = gvAccountProcess.MasterTableView.GetColumn("Asset");
                gridColproduct.HeaderStyle.Width = Unit.Pixel(100);

                GridColumn gridcoltaskdescription = gvAccountProcess.MasterTableView.GetColumn("Product");
                gridcoltaskdescription.HeaderStyle.Width = Unit.Pixel(200);

                GridColumn gridcolTask = gvAccountProcess.MasterTableView.GetColumn("Task");
                gridcolTask.HeaderStyle.Width = Unit.Pixel(700);

                GridColumn gridcolImgStatus = gvAccountProcess.MasterTableView.GetColumn("ImgStatus");
                gridcolImgStatus.HeaderStyle.Width = Unit.Pixel(40);
            }
        }

        protected void gvAccountProcess_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;
            var dataItem = (GridDataItem)e.Item;
            var imgEdit = dataItem["ImgStatus"].Controls[0] as Image;
            var poductname = dataItem["Product"].Text;
            switch (dataItem["Status"].Text)
            {
                case "Active":
                    if (imgEdit != null) imgEdit.ImageUrl = "../images/Status_active.png";
                    break;
                case "Pending":
                    if (imgEdit != null) imgEdit.ImageUrl = "../images/Status_pending.png";
                    break;

                default:
                    if (imgEdit != null) imgEdit.ImageUrl = "../images/status_deactive.png";
                    break;
            }
        }

        protected void gvAccountProcess_ItemCommand(object sender, GridCommandEventArgs e)
        {
            hfSelectedRowIndex.Value = e.Item.ItemIndex.ToString();
            var gDataItem = e.Item as GridDataItem;
            if (gDataItem != null)
            {
                var linkEntiyClid = new Guid(gDataItem["LinkEntiyCLID"].Text);
                var linkEntiyCsid = new Guid(gDataItem["LinkEntiyCSID"].Text);
                string linkedEntityType = gDataItem["LinkedEntityType"].Text;
                var modelId = new Guid(gDataItem["ModelID"].Text);
                var assetId = new Guid(gDataItem["AssetID"].Text);
                var taskId = new Guid(gDataItem["TaskID"].Text);
                switch (e.CommandName.ToLower())
                {
                    case "associate":
                        // hfSelectedRowIndex.Value = e.NewEditIndex.ToString();
                        var CID = Guid.Empty;
                        if (gDataItem["CID"].Text != "&nbsp;" && gDataItem["CID"].Text != string.Empty)
                            CID = gDataItem["CID"].Text == "" ? Guid.Empty : new Guid(gDataItem["CID"].Text);

                        var linkEntiyFundId = Guid.Empty;
                        if (gDataItem["LinkEntiyFundID"].Text != "&nbsp;" && gDataItem["LinkEntiyFundID"].Text != string.Empty)
                            linkEntiyFundId = new Guid(gDataItem["LinkEntiyFundID"].Text);
                        var type = (OrganizationType)Enum.Parse(typeof(OrganizationType), linkedEntityType);
                        switch (type)
                        {
                            case OrganizationType.BankAccount:
                            case OrganizationType.TermDepositAccount:
                                BankAccountDetails.SetEntity(CID, linkEntiyClid, linkEntiyCsid, linkedEntityType);
                                OVER.Visible = BankAccountWindow.Visible = true;
                                break;
                            case OrganizationType.DesktopBrokerAccount:
                                ASXControl.SelectedGridView = true;
                                ASXControl.SetEntity(CID);
                                OVER.Visible = DesktopBrokerWindow.Visible = true;
                                break;
                            case OrganizationType.ManagedInvestmentSchemesAccount:
                                MISControl.SetEntity(CID, linkEntiyFundId,null, true);
                                OVER.Visible = MisAccountWindow.Visible = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case "delete":
                        var linkEntiyFundIdDelete = new Guid(gDataItem["LinkEntiyFundID"].Text);
                        SaveAccountProcess(linkEntiyClid, linkEntiyCsid, linkedEntityType, taskId, assetId, modelId,
                                           linkEntiyFundIdDelete, DatasetCommandTypes.Delete);
                        gvAccountProcess.Rebind();
                        break;
                }
            }
        }

        protected void gd_DealerGroup_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var comp = this.UMABroker.GetBMCInstance(new Guid(cid)) as IBrokerManagedComponent;
            if (comp.ComponentInformation.DisplayName.ToLower() == "client e-clipse super")
            {
                btnRefreshPortfolioFromSm.Visible = false;
                btnRefreshDataFromSma.Visible = false;
            }
            var ds = new ClientAccountProcessDS();
            comp.GetData(ds);
            PresentationData = ds;
            var summaryView = new DataView(ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE])
            {
                Sort = ClientAccountProcessDS.ASSET + " ASC"
            };
            gvAccountProcess.DataSource = summaryView;
        }
    }
}