﻿<%@ Page Title="e-Clipse Online Portal - Configure Fees" Language="C#" MasterPageFile="ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="OrderDetails.aspx.cs" Inherits="eclipseonlineweb.OrderDetails" %>

<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <fieldset>
        <table width="100%">
            <tr>
                <td width="10%">
                    <asp:ImageButton runat="server" ImageUrl="~/images/window_previous.png" ToolTip="Back to Pending Orders"
                        AlternateText="Back to Pending Orders" ID="btnBack" OnClientClick="javascript:GoToPage('PendingOrders.aspx');" />
                </td>
                <td width="10%">
                </td>
                <td style="width: 30%;" class="breadcrumbgap">
                    <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                    <br />
                    <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="MainView" style="background-color: white">
                <table style="width: 100%; height: 200px">
                    <tr>
                        <td>
                            Investment:
                        </td>
                        <td>
                            <asp:Label ID="lblInvestment" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Order No:
                        </td>
                        <td>
                            <asp:Label ID="lblOrder" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Action:
                        </td>
                        <td>
                            <asp:Label ID="lblAction" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <telerik:RadGrid OnNeedDataSource="PresentationGridNeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGridDetailTableDataBind"
                OnItemCommand="PresentationGridItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="PresentationGridItemUpdated" OnItemDeleted="PresentationGridItemDeleted"
                OnItemInserted="PresentationGridItemInserted" OnInsertCommand="PresentationGridInsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGridItemCreated"
                OnItemDataBound="PresentationGridItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="PendingOrders" TableLayout="Fixed">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <Columns>
                        <telerik:GridBoundColumn FilterControlWidth="100px" ReadOnly="true" SortExpression="Status"
                            HeaderStyle-Width="20%" ItemStyle-Width="15%" HeaderText="Status" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Status" UniqueName="Status">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Quantity" ReadOnly="true"
                            HeaderStyle-Width="30%" ItemStyle-Width="30%" HeaderText="Quantity" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Quantity" UniqueName="Quantity">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="UnitPrice" ReadOnly="true"
                            HeaderStyle-Width="30%" ItemStyle-Width="30%" HeaderText="Unit Price" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="UnitPrice" UniqueName="UnitPrice">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" ReadOnly="true" SortExpression="Value"
                            HeaderStyle-Width="20%" ItemStyle-Width="15%" HeaderText="Value" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" DataFormatString="{0:c}" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="Value" UniqueName="Value">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" ReadOnly="true" SortExpression="Date"
                            HeaderStyle-Width="20%" ItemStyle-Width="15%" HeaderText="Date" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Date" UniqueName="Date">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="User" HeaderStyle-Width="10%"
                            ItemStyle-Width="10%" HeaderText="User" ReadOnly="true" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="User" UniqueName="User">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80px" ReadOnly="true" SortExpression="Comments"
                            HeaderStyle-Width="10%" ShowFilterIcon="false" ItemStyle-Width="10%" HeaderText="Comments"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                            DataField="Comments" UniqueName="Comments">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
