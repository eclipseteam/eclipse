﻿using System;
using System.Data;
using System.Globalization;
using System.Text;
using C1.Web.Wijmo.Controls.C1ComboBox;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;
namespace eclipseonlineweb.Administration
{
    public partial class ClientDivindendDetails : System.Web.UI.UserControl
    {
       
        public Action<Guid> Saved
        {
            get;
            set;
        }
        public Action<DataSet> SaveData
        {
            get;
            set;
        }
        
        public Action Canceled { get; set; }
        public Action ValidationFailed { get; set; }
       
        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Loadcombo();
        }

        private void Loadcombo()
        {
            cmbSecurites.DataSource = LoadSecurities().Tables[SecuritiesDS.SECLIST];
            cmbSecurites.DataTextField = "Code";
            cmbSecurites.DataValueField = "Code";
            cmbSecurites.DataBind();
        }
        private SecuritiesDS LoadSecurities()
        {
            var organization = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var securitiesDs = new SecuritiesDS();
            if (organization != null)
            {
                organization.GetData(securitiesDs);
                Broker.ReleaseBrokerManagedComponent(organization);
            }
            return securitiesDs;
        }
        private void HideShowSecurityDropDown(bool visible)
        {
            cmbSecurites.Visible = visible;
            TextInvestmentCode.Visible = !visible;
            TextUnitsOnHand.Enabled = !visible;
        }
       
        
        public void ClearEntity()
        {
            lblMessages.Text = string.Empty;
            TextInvestmentCode.Text = string.Empty;
            TextInterestRate.Text = string.Empty;
            TextInvestmentAmount.Text = string.Empty;
            TextInterestPaid.Text = string.Empty;
            TextUnitsOnHand.Text = string.Empty;
            TextRecordDate.Text = string.Empty;
            TextPaymentDate.Text = string.Empty;
            calRecodDate.SelectedDate = null;
            calPaymentDate.SelectedDate = null;
            TextCashManagementTransactionID.Value = Guid.Empty.ToString();
            TextExternalReferenceID.Value = Guid.Empty.ToString();
            TextInvestor.Value = string.Empty;
            TextID.Value = Guid.Empty.ToString();
            TextclientCSID.Value = Guid.Empty.ToString();
            TextCentsPerShare.Text = string.Empty;
            cmbAdministrationSystem.Text = "";
            cmbAdministrationSystem.SelectedValue = null;
            cmbTransactionType.Text = "";
            cmbTransactionType.SelectedValue = null;
            TextPaymentDate.Text = string.Empty;
            TextFrankedAmount.Text = string.Empty;
            TextUnfrankedAmount.Text = string.Empty;
            TextFrankedCredit.Text = string.Empty;
            TextTotalFrankingCredit.Text = string.Empty;
            TextTaxFree.Text = string.Empty;
            TextTaxDeferred.Text = string.Empty;
            TextReturnofCapital.Text = string.Empty;
            TextDomesticInterestAmount.Text = string.Empty;
            TextForeignInterestAmount.Text = string.Empty;
            TextDomesticOtherIncome.Text = string.Empty;
            TextForeignOtherIncome.Text = string.Empty;
            TextDomesticWithHoldingTax.Text = string.Empty;
            TextForeignWithHoldingTax.Text = string.Empty;
            TextDomesticImputationTaxCredits.Text = string.Empty;
            TextForeignImputationTaxCredits.Text = string.Empty;
            TextForeignInvestmentFunds.Text = string.Empty;
            TextDomesticIndexationCGT.Text = string.Empty;
            TextForeignIndexationCGT.Text = string.Empty;
            TextDomesticDiscountedCGT.Text = string.Empty;
            TextForeignDiscountedCGT.Text = string.Empty;
            TextDomesticOtherCGT.Text = string.Empty;
            TextForeignOtherCGT.Text = string.Empty;
            TextDomesticGrossAmount.Text = string.Empty;
            TextComment.Text = string.Empty;

            txtCurrency.Text = "AUD";
            cmbDividendType.Text = "";
            cmbDividendType.SelectedValue = null;
            txtBalanceDate.Text = "";
            txtBooksCloseDate.Text = "";

        }

        public DividendEntity GetEntity()
        {
            DividendEntity entity = new DividendEntity();
            entity.ID = new Guid(TextID.Value);
            entity.InvestmentCode = TextInvestmentCode.Text;
            entity.Investor = TextInvestor.Value;
            entity.CashManagementTransactionID = TextCashManagementTransactionID.Value;

            entity.AdministrationSystem = cmbAdministrationSystem.Text;
            entity.TransactionType = cmbTransactionType.Text;
            DateTime date;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            if (TextRecordDate.Text.Trim() != string.Empty)
            {
                if (DateTime.TryParse(TextRecordDate.Text.Trim(), info, DateTimeStyles.None, out date))
                {
                    entity.RecordDate = date;
                }
            }
            else
            {
                entity.RecordDate = null;
            }

            if (TextPaymentDate.Text.Trim() != string.Empty)
            {

                if (DateTime.TryParse(TextPaymentDate.Text.Trim(), info, DateTimeStyles.None, out date))
                {
                    entity.PaymentDate = date;
                }
            }
            else
            {
                entity.PaymentDate = null;
            }

            entity.InterestRate = TextInterestRate.Text != "" ? Convert.ToDecimal(TextInterestRate.Text) : 0;
            entity.InvestmentAmount = TextInvestmentAmount.Text != "" ? Convert.ToDecimal(TextInvestmentAmount.Text) : 0;
            entity.InterestPaid = TextInterestPaid.Text != "" ? Convert.ToDecimal(TextInterestPaid.Text) : 0;
            entity.CentsPerShare = (TextCentsPerShare.Text != "") ? Convert.ToDouble(TextCentsPerShare.Text) : 0;
            entity.UnitsOnHand = TextUnitsOnHand.Text != "" ? Convert.ToInt32(TextUnitsOnHand.Text) : 0;

            ClientDividendDetailsDS ds = GetDataSet(new Guid(TextID.Value), new Guid(TextclientCSID.Value));
            if (ds.Entity != null)
            {
                #region Update Income Transection
                entity.ReportCollection = ds.Entity.ReportCollection;
                

                entity.FrankedAmount = (TextFrankedAmount.Text != "") ? Convert.ToDouble(TextFrankedAmount.Text) : 0;
                entity.UnfrankedAmount = (TextUnfrankedAmount.Text != "") ? Convert.ToDouble(TextUnfrankedAmount.Text) : 0;
                entity.TaxFree = (TextTaxFree.Text != "") ? Convert.ToDouble(TextTaxFree.Text) : 0;
                entity.TaxDeferred = (TextTaxDeferred.Text != "") ? Convert.ToDouble(TextTaxDeferred.Text) : 0;
                entity.ReturnOfCapital = (TextReturnofCapital.Text != "") ? Convert.ToDouble(TextReturnofCapital.Text) : 0;
                entity.DomesticInterestAmount = (TextDomesticInterestAmount.Text != "") ? Convert.ToDouble(TextDomesticInterestAmount.Text) : 0;
                entity.ForeignInterestAmount = (TextForeignInterestAmount.Text != "") ? Convert.ToDouble(TextForeignInterestAmount.Text) : 0;
                entity.DomesticOtherIncome = (TextDomesticOtherIncome.Text != "") ? Convert.ToDouble(TextDomesticOtherIncome.Text) : 0;
                entity.ForeignOtherIncome = (TextForeignOtherIncome.Text != "") ? Convert.ToDouble(TextForeignOtherIncome.Text) : 0;
                entity.DomesticWithHoldingTax = (TextDomesticWithHoldingTax.Text != "") ? Convert.ToDouble(TextDomesticWithHoldingTax.Text) : 0;
                entity.ForeignWithHoldingTax = (TextForeignWithHoldingTax.Text != "") ? Convert.ToDouble(TextForeignWithHoldingTax.Text) : 0;
                entity.DomesticImputationTaxCredits = (TextDomesticImputationTaxCredits.Text != "") ? Convert.ToDouble(TextDomesticImputationTaxCredits.Text) : 0;
                entity.ForeignImputationTaxCredits = (TextForeignImputationTaxCredits.Text != "") ? Convert.ToDouble(TextForeignImputationTaxCredits.Text) : 0;
                entity.ForeignInvestmentFunds = (TextForeignInvestmentFunds.Text != "") ? Convert.ToDouble(TextForeignInvestmentFunds.Text) : 0;
                entity.DomesticIndexationCGT = (TextDomesticIndexationCGT.Text != "") ? Convert.ToDouble(TextDomesticIndexationCGT.Text) : 0;
                entity.ForeignIndexationCGT = (TextForeignIndexationCGT.Text != "") ? Convert.ToDouble(TextForeignIndexationCGT.Text) : 0;
                entity.DomesticDiscountedCGT = (TextDomesticDiscountedCGT.Text != "") ? Convert.ToDouble(TextDomesticDiscountedCGT.Text) : 0;
                entity.ForeignDiscountedCGT = (TextForeignDiscountedCGT.Text != "") ? Convert.ToDouble(TextForeignDiscountedCGT.Text) : 0;
                entity.DomesticOtherCGT = (TextDomesticOtherCGT.Text != "") ? Convert.ToDouble(TextDomesticOtherCGT.Text) : 0;
                entity.ForeignOtherCGT = (TextForeignOtherCGT.Text != "") ? Convert.ToDouble(TextForeignOtherCGT.Text) : 0;
                entity.DomesticGrossAmount = (TextDomesticGrossAmount.Text != "") ? Convert.ToDouble(TextDomesticGrossAmount.Text) : 0;

                //entity.PercentFrankedAmount = (TextFrankedAmount.Text != "") ? Convert.ToDouble(TextFrankedAmount.Text) : 0;
                //entity.PercentUnfrankedAmount = (TextUnfrankedAmount.Text != "") ? Convert.ToDouble(TextUnfrankedAmount.Text) : 0;
                //entity.PercentTaxFree = (TextTaxFree.Text != "") ? Convert.ToDouble(TextTaxFree.Text) : 0;
                //entity.PercentTaxDeferred = (TextTaxDeferred.Text != "") ? Convert.ToDouble(TextTaxDeferred.Text) : 0;
                //entity.PercentReturnOfCapital = (TextReturnofCapital.Text != "") ? Convert.ToDouble(TextReturnofCapital.Text) : 0;
                //entity.PercentDomesticInterestAmount = (TextDomesticInterestAmount.Text != "") ? Convert.ToDouble(TextDomesticInterestAmount.Text) : 0;
                //entity.PercentForeignInterestAmount = (TextForeignInterestAmount.Text != "") ? Convert.ToDouble(TextForeignInterestAmount.Text) : 0;
                //entity.PercentDomesticOtherIncome = (TextDomesticOtherIncome.Text != "") ? Convert.ToDouble(TextDomesticOtherIncome.Text) : 0;
                //entity.PercentForeignOtherIncome = (TextForeignOtherIncome.Text != "") ? Convert.ToDouble(TextForeignOtherIncome.Text) : 0;
                //entity.PercentDomesticWithHoldingTax = (TextDomesticWithHoldingTax.Text != "") ? Convert.ToDouble(TextDomesticWithHoldingTax.Text) : 0;
                //entity.PercentForeignWithHoldingTax = (TextForeignWithHoldingTax.Text != "") ? Convert.ToDouble(TextForeignWithHoldingTax.Text) : 0;
                //entity.PercentDomesticImputationTaxCredits = (TextDomesticImputationTaxCredits.Text != "") ? Convert.ToDouble(TextDomesticImputationTaxCredits.Text) : 0;
                //entity.PercentForeignImputationTaxCredits = (TextForeignImputationTaxCredits.Text != "") ? Convert.ToDouble(TextForeignImputationTaxCredits.Text) : 0;
                //entity.PercentForeignInvestmentFunds = (TextForeignInvestmentFunds.Text != "") ? Convert.ToDouble(TextForeignInvestmentFunds.Text) : 0;
                //entity.PercentDomesticIndexationCGT = (TextDomesticIndexationCGT.Text != "") ? Convert.ToDouble(TextDomesticIndexationCGT.Text) : 0;
                //entity.PercentForeignIndexationCGT = (TextForeignIndexationCGT.Text != "") ? Convert.ToDouble(TextForeignIndexationCGT.Text) : 0;
                //entity.PercentDomesticDiscountedCGT = (TextDomesticDiscountedCGT.Text != "") ? Convert.ToDouble(TextDomesticDiscountedCGT.Text) : 0;
                //entity.PercentForeignDiscountedCGT = (TextForeignDiscountedCGT.Text != "") ? Convert.ToDouble(TextForeignDiscountedCGT.Text) : 0;
                //entity.PercentDomesticOtherCGT = (TextDomesticOtherCGT.Text != "") ? Convert.ToDouble(TextDomesticOtherCGT.Text) : 0;
                //entity.PercentForeignOtherCGT = (TextForeignOtherCGT.Text != "") ? Convert.ToDouble(TextForeignOtherCGT.Text) : 0;
                //entity.PercentDomesticGrossAmount = (TextDomesticGrossAmount.Text != "") ? Convert.ToDouble(TextDomesticGrossAmount.Text) : 0;


                entity.Account.Clid = ds.Entity.Account.Clid;
                entity.Account.Csid = ds.Entity.Account.Csid;
                entity.ExternalReferenceID = ds.Entity.ExternalReferenceID;
                entity.Status = ds.Entity.Status;
                
                


                //entity.UnfrankedAmount = (TextUnfrankedAmount.Text != "") ? Convert.ToDouble(TextUnfrankedAmount.Text) : 0;
                //entity.TaxFree = (TextTaxFree.Text != "") ? Convert.ToDouble(TextTaxFree.Text) : 0;
                //entity.TaxDeferred = (TextTaxDeferred.Text != "") ? Convert.ToDouble(TextTaxDeferred.Text) : 0;
                //entity.ReturnOfCapital = (TextReturnofCapital.Text != "") ? Convert.ToDouble(TextReturnofCapital.Text) : 0;
                //entity.DomesticInterestAmount = (TextDomesticInterestAmount.Text != "") ? Convert.ToDouble(TextDomesticInterestAmount.Text) : 0;
                //entity.ForeignInterestAmount = (TextForeignInterestAmount.Text != "") ? Convert.ToDouble(TextForeignInterestAmount.Text) : 0;
                //entity.DomesticOtherIncome = (TextDomesticOtherIncome.Text != "") ? Convert.ToDouble(TextDomesticOtherIncome.Text) : 0;
                //entity.ForeignOtherIncome = (TextForeignOtherIncome.Text != "") ? Convert.ToDouble(TextForeignOtherIncome.Text) : 0;
                //entity.DomesticWithHoldingTax = (TextDomesticWithHoldingTax.Text != "") ? Convert.ToDouble(TextDomesticWithHoldingTax.Text) : 0;
                //entity.ForeignWithHoldingTax = (TextForeignWithHoldingTax.Text != "") ? Convert.ToDouble(TextForeignWithHoldingTax.Text) : 0;
                //entity.DomesticImputationTaxCredits = (TextDomesticImputationTaxCredits.Text != "") ? Convert.ToDouble(TextDomesticImputationTaxCredits.Text) : 0;
                //entity.ForeignImputationTaxCredits = (TextForeignImputationTaxCredits.Text != "") ? Convert.ToDouble(TextForeignImputationTaxCredits.Text) : 0;
                //entity.ForeignInvestmentFunds = (TextForeignInvestmentFunds.Text != "") ? Convert.ToDouble(TextForeignInvestmentFunds.Text) : 0;
                //entity.DomesticIndexationCGT = (TextDomesticIndexationCGT.Text != "") ? Convert.ToDouble(TextDomesticIndexationCGT.Text) : 0;
                //entity.ForeignIndexationCGT = (TextForeignIndexationCGT.Text != "") ? Convert.ToDouble(TextForeignIndexationCGT.Text) : 0;
                //entity.DomesticDiscountedCGT = (TextDomesticDiscountedCGT.Text != "") ? Convert.ToDouble(TextDomesticDiscountedCGT.Text) : 0;
                //entity.ForeignDiscountedCGT = (TextForeignDiscountedCGT.Text != "") ? Convert.ToDouble(TextForeignDiscountedCGT.Text) : 0;
                //entity.DomesticOtherCGT = (TextDomesticOtherCGT.Text != "") ? Convert.ToDouble(TextDomesticOtherCGT.Text) : 0;
                //entity.ForeignOtherCGT = (TextForeignOtherCGT.Text != "") ? Convert.ToDouble(TextForeignOtherCGT.Text) : 0;
                //entity.DomesticGrossAmount = (TextDomesticGrossAmount.Text != "") ? Convert.ToDouble(TextDomesticGrossAmount.Text) : 0;



                //entity.PercentFrankedAmount = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.FrankedAmount);
                //entity.PercentUnfrankedAmount = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.UnfrankedAmount);
                //entity.PercentTaxFree = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.TaxFree);
                //entity.PercentTaxDeferred = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.TaxDeferred);
                //entity.PercentReturnOfCapital = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.ReturnOfCapital);
                //entity.PercentDomesticInterestAmount = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.DomesticInterestAmount);
                //entity.PercentForeignInterestAmount = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.ForeignInterestAmount);
                //entity.PercentDomesticOtherIncome = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.DomesticOtherIncome);
                //entity.PercentForeignOtherIncome = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.ForeignOtherIncome);
                //entity.PercentDomesticWithHoldingTax = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.DomesticWithHoldingTax);
                //entity.PercentForeignWithHoldingTax = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.ForeignWithHoldingTax);
                //entity.PercentDomesticImputationTaxCredits = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.DomesticImputationTaxCredits);
                //entity.PercentForeignImputationTaxCredits = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.ForeignImputationTaxCredits);
                //entity.PercentForeignInvestmentFunds = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.ForeignInvestmentFunds);
                //entity.PercentDomesticIndexationCGT = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.DomesticIndexationCGT);
                //entity.PercentForeignIndexationCGT = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.ForeignIndexationCGT);
                //entity.PercentDomesticDiscountedCGT = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.DomesticDiscountedCGT);
                //entity.PercentForeignDiscountedCGT = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.ForeignDiscountedCGT);
                //entity.PercentDomesticOtherCGT = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.DomesticOtherCGT);
                //entity.PercentForeignOtherCGT = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.ForeignOtherCGT);
                //entity.PercentDomesticGrossAmount = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.DomesticGrossAmount);
                //ds.Entity.PercentDomesticInterestAmount = CalculatePercentage(entity.CentsPerShare, entity.UnitsOnHand, entity.DomesticInterestAmount);
                #endregion

            }
            else
            {
                #region Add New Income Transactions.

                entity.PercentFrankedAmount = (TextFrankedAmount.Text != "") ? Convert.ToDouble(TextFrankedAmount.Text) : 0;
                entity.PercentUnfrankedAmount = (TextUnfrankedAmount.Text != "") ? Convert.ToDouble(TextUnfrankedAmount.Text) : 0;
                entity.PercentTaxFree = (TextTaxFree.Text != "") ? Convert.ToDouble(TextTaxFree.Text) : 0;
                entity.PercentTaxDeferred = (TextTaxDeferred.Text != "") ? Convert.ToDouble(TextTaxDeferred.Text) : 0;
                entity.PercentReturnOfCapital = (TextReturnofCapital.Text != "") ? Convert.ToDouble(TextReturnofCapital.Text) : 0;
                entity.PercentDomesticInterestAmount = (TextDomesticInterestAmount.Text != "") ? Convert.ToDouble(TextDomesticInterestAmount.Text) : 0;
                entity.PercentForeignInterestAmount = (TextForeignInterestAmount.Text != "") ? Convert.ToDouble(TextForeignInterestAmount.Text) : 0;
                entity.PercentDomesticOtherIncome = (TextDomesticOtherIncome.Text != "") ? Convert.ToDouble(TextDomesticOtherIncome.Text) : 0;
                entity.PercentForeignOtherIncome = (TextForeignOtherIncome.Text != "") ? Convert.ToDouble(TextForeignOtherIncome.Text) : 0;
                entity.PercentDomesticWithHoldingTax = (TextDomesticWithHoldingTax.Text != "") ? Convert.ToDouble(TextDomesticWithHoldingTax.Text) : 0;
                entity.PercentForeignWithHoldingTax = (TextForeignWithHoldingTax.Text != "") ? Convert.ToDouble(TextForeignWithHoldingTax.Text) : 0;
                entity.PercentDomesticImputationTaxCredits = (TextDomesticImputationTaxCredits.Text != "") ? Convert.ToDouble(TextDomesticImputationTaxCredits.Text) : 0;
                entity.PercentForeignImputationTaxCredits = (TextForeignImputationTaxCredits.Text != "") ? Convert.ToDouble(TextForeignImputationTaxCredits.Text) : 0;
                entity.PercentForeignInvestmentFunds = (TextForeignInvestmentFunds.Text != "") ? Convert.ToDouble(TextForeignInvestmentFunds.Text) : 0;
                entity.PercentDomesticIndexationCGT = (TextDomesticIndexationCGT.Text != "") ? Convert.ToDouble(TextDomesticIndexationCGT.Text) : 0;
                entity.PercentForeignIndexationCGT = (TextForeignIndexationCGT.Text != "") ? Convert.ToDouble(TextForeignIndexationCGT.Text) : 0;
                entity.PercentDomesticDiscountedCGT = (TextDomesticDiscountedCGT.Text != "") ? Convert.ToDouble(TextDomesticDiscountedCGT.Text) : 0;
                entity.PercentForeignDiscountedCGT = (TextForeignDiscountedCGT.Text != "") ? Convert.ToDouble(TextForeignDiscountedCGT.Text) : 0;
                entity.PercentDomesticOtherCGT = (TextDomesticOtherCGT.Text != "") ? Convert.ToDouble(TextDomesticOtherCGT.Text) : 0;
                entity.PercentForeignOtherCGT = (TextForeignOtherCGT.Text != "") ? Convert.ToDouble(TextForeignOtherCGT.Text) : 0;
                entity.PercentDomesticGrossAmount = (TextDomesticGrossAmount.Text != "") ? Convert.ToDouble(TextDomesticGrossAmount.Text) : 0;


                entity.FrankedAmount = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentFrankedAmount);
                entity.UnfrankedAmount = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentUnfrankedAmount);
                entity.TaxFree = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentTaxFree);
                entity.TaxDeferred = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentTaxDeferred);
                entity.ReturnOfCapital = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentReturnOfCapital);
                entity.DomesticInterestAmount = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentDomesticInterestAmount);
                entity.ForeignInterestAmount = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentForeignInterestAmount);
                entity.DomesticOtherIncome = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentDomesticOtherIncome);
                entity.ForeignOtherIncome = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentForeignOtherIncome);
                entity.DomesticWithHoldingTax = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentDomesticWithHoldingTax);
                entity.ForeignWithHoldingTax = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentForeignWithHoldingTax);
                entity.DomesticImputationTaxCredits = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentDomesticImputationTaxCredits);
                entity.ForeignImputationTaxCredits = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentForeignImputationTaxCredits);
                entity.DomesticGrossAmount = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentDomesticGrossAmount);
                entity.ForeignOtherCGT = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentForeignOtherCGT);
                entity.DomesticOtherCGT = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentDomesticOtherCGT);
                entity.ForeignDiscountedCGT = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentForeignDiscountedCGT);
                entity.DomesticDiscountedCGT = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentDomesticDiscountedCGT);
                entity.ForeignIndexationCGT = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentForeignIndexationCGT);
                entity.DomesticIndexationCGT = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentDomesticIndexationCGT);
                entity.ForeignInvestmentFunds = CalculateAmount(entity.CentsPerShare, entity.UnitsOnHand, entity.PercentForeignInvestmentFunds);
                #endregion
            }


            
            entity.Comment = TextComment.Text;

            if (txtBalanceDate.Text.Trim() != string.Empty)
            {
                if (DateTime.TryParse(txtBalanceDate.Text.Trim(), info, DateTimeStyles.None, out date))
                {
                    entity.BalanceDate = date;
                }
            }
            else
            {
                entity.BalanceDate = null;
            }

            if (txtBooksCloseDate.Text.Trim() != string.Empty)
            {
                if (DateTime.TryParse(txtBooksCloseDate.Text.Trim(), info, DateTimeStyles.None, out date))
                {
                    entity.BooksCloseDate = date;
                }
            }
            else
            {
                entity.BooksCloseDate = null;
            }
            entity.Dividendtype = (DividendType)Enum.Parse(typeof(DividendType),cmbDividendType.Text);
            entity.Currency = txtCurrency.Text;
            
            return entity;
        }

        private double CalculateAmount(double centsPerShare, double units, double percentage)
        {
            return ((Math.Round(centsPerShare, 2) * units) / 100) * percentage;
        }
        private double CalculatePercentage(double centsPerShare, double units, double amount)
        {
            //return amount/((Math.Round(centsPerShare, 2) * units) / 100)  ;
            return ((Math.Round(centsPerShare, 2) * units) / 100) * amount;
        }

        public void SetEntity(Guid dividendId, Guid clientCsId,string clientId, string clientName,bool isAdmin)
        {
            ClearEntity();
            btnUpdate.Enabled = isAdmin;
            hf_clientid.Value = clientId;
            TextclientCSID.Value = clientCsId.ToString();
            ChangeLabel("%");
            HideShowSecurityDropDown(true);
            if (dividendId != Guid.Empty)
            {
                ClientDividendDetailsDS ds = GetDataSet(dividendId, clientCsId);
                var Entity = ds.Entity;
                ChangeLabel("$");
                HideShowSecurityDropDown(false);
                TextID.Value = dividendId.ToString();
                TextclientCSID.Value = clientCsId.ToString();
                TextInvestor.Value = Entity.Investor;
                TextInterestRate.Text = Entity.InterestRate.ToString();
                TextInvestmentAmount.Text = Entity.InvestmentAmount.ToString();
                TextInterestPaid.Text = Entity.InterestPaid.ToString();
                TextCentsPerShare.Text = Entity.CentsPerShare.ToString();
                cmbAdministrationSystem.Text = Entity.AdministrationSystem;
                cmbAdministrationSystem.SelectedValue = Entity.AdministrationSystem;
                cmbTransactionType.Text = Entity.TransactionType;
                cmbTransactionType.SelectedValue = Entity.TransactionType;
                TextInvestmentCode.Text = Entity.InvestmentCode;
                TextExternalReferenceID.Value = Entity.ExternalReferenceID;
                if (Entity.RecordDate.HasValue)
                {
                    calRecodDate.SelectedDate = Entity.RecordDate.Value;
                    TextRecordDate.Text = Entity.RecordDate.Value.ToString("dd/MM/yyyy");
                }
                if (Entity.PaymentDate.HasValue)
                {
                    calPaymentDate.SelectedDate = Entity.PaymentDate.Value;
                    TextPaymentDate.Text = Entity.PaymentDate.Value.ToString("dd/MM/yyyy");
                }
                TextUnitsOnHand.Text = Entity.UnitsOnHand.ToString();
                TextFrankedAmount.Text = Entity.FrankedAmount.ToString();
                TextUnfrankedAmount.Text = Entity.UnfrankedAmount.ToString();
                TextFrankedCredit.Text = Math.Round(Entity.FrankingCredit, 3).ToString();
                TextTotalFrankingCredit.Text = Math.Round(Entity.TotalFrankingCredit, 3).ToString();
                TextTaxFree.Text = Entity.TaxFree.ToString();
                TextTaxDeferred.Text = Entity.TaxDeferred.ToString();
                TextReturnofCapital.Text = Entity.ReturnOfCapital.ToString();
                TextDomesticInterestAmount.Text = Entity.DomesticInterestAmount.ToString();
                TextForeignInterestAmount.Text = Entity.ForeignInterestAmount.ToString();
                TextDomesticOtherIncome.Text = Entity.DomesticOtherIncome.ToString();
                TextForeignOtherIncome.Text = Entity.ForeignOtherIncome.ToString();
                TextDomesticWithHoldingTax.Text = Entity.DomesticWithHoldingTax.ToString();
                TextForeignWithHoldingTax.Text = Entity.ForeignWithHoldingTax.ToString();
                TextDomesticImputationTaxCredits.Text = Entity.DomesticImputationTaxCredits.ToString();
                TextForeignImputationTaxCredits.Text = Entity.ForeignImputationTaxCredits.ToString();
                TextForeignInvestmentFunds.Text = Entity.ForeignInvestmentFunds.ToString();
                TextDomesticIndexationCGT.Text = Entity.DomesticIndexationCGT.ToString();
                TextForeignIndexationCGT.Text = Entity.ForeignIndexationCGT.ToString();
                TextDomesticDiscountedCGT.Text = Entity.DomesticDiscountedCGT.ToString();
                TextForeignDiscountedCGT.Text = Entity.ForeignDiscountedCGT.ToString();
                TextDomesticOtherCGT.Text = Entity.DomesticOtherCGT.ToString();
                TextForeignOtherCGT.Text = Entity.ForeignOtherCGT.ToString();
                TextDomesticGrossAmount.Text = Entity.DomesticGrossAmount.ToString();
                TextComment.Text = Entity.Comment;

                //TextFrankedAmount.ToolTip =   Entity.PercentFrankedAmount.ToString();
                //TextUnfrankedAmount.ToolTip = Entity.PercentUnfrankedAmount.ToString();
                //TextTaxFree.ToolTip = Entity.PercentTaxFree.ToString();
                //TextTaxDeferred.ToolTip = Entity.PercentTaxDeferred.ToString();
                //TextReturnofCapital.ToolTip = Entity.PercentReturnOfCapital.ToString();
                //TextDomesticInterestAmount.ToolTip = Entity.PercentDomesticInterestAmount.ToString();
                //TextForeignInterestAmount.ToolTip = Entity.PercentForeignInterestAmount.ToString();
                //TextDomesticOtherIncome.ToolTip = Entity.PercentDomesticOtherIncome.ToString();
                //TextForeignOtherIncome.ToolTip = Entity.PercentForeignOtherIncome.ToString();
                //TextDomesticWithHoldingTax.ToolTip = Entity.PercentDomesticWithHoldingTax.ToString();
                //TextForeignWithHoldingTax.ToolTip = Entity.PercentForeignWithHoldingTax.ToString();
                //TextDomesticImputationTaxCredits.ToolTip = Entity.PercentForeignImputationTaxCredits.ToString();
                //TextForeignImputationTaxCredits.ToolTip = Entity.PercentForeignImputationTaxCredits.ToString();
                //TextForeignInvestmentFunds.ToolTip = Entity.PercentForeignInvestmentFunds.ToString();
                //TextDomesticIndexationCGT.ToolTip = Entity.PercentDomesticIndexationCGT.ToString();
                //TextForeignIndexationCGT.ToolTip = Entity.PercentForeignIndexationCGT.ToString();
                //TextDomesticDiscountedCGT.ToolTip = Entity.PercentDomesticDiscountedCGT.ToString();
                //TextForeignDiscountedCGT.ToolTip = Entity.PercentForeignDiscountedCGT.ToString();
                //TextDomesticOtherCGT.ToolTip = Entity.PercentDomesticOtherCGT.ToString();
                //TextForeignOtherCGT.ToolTip = Entity.PercentForeignOtherCGT.ToString();
                //TextDomesticGrossAmount.ToolTip = Entity.PercentDomesticGrossAmount.ToString();
                TextCashManagementTransactionID.Value = Entity.CashManagementTransactionID;
                if (Entity.BalanceDate.HasValue)
                {
                    calBanalceDate.SelectedDate = Entity.BalanceDate.Value;
                    txtBalanceDate.Text = Entity.BalanceDate.Value.ToString("dd/MM/yyyy");
                }

                if (Entity.BooksCloseDate.HasValue)
                {
                    calBooksCloseDate.SelectedDate = Entity.BooksCloseDate.Value;
                    txtBooksCloseDate.Text = Entity.BooksCloseDate.Value.ToString("dd/MM/yyyy");
                }
                txtCurrency.Text = Entity.Currency;
                cmbDividendType.Text = Entity.Dividendtype.ToString();
                cmbDividendType.SelectedValue = Entity.Dividendtype.ToString();
            }
        }

        //private string SetToolTip(string text)
        //{
        //    return text + "%";
        //}

        private ClientDividendDetailsDS GetDataSet(Guid entityID, Guid cid)
        {

            ClientDividendDetailsDS clientDividendDetailsDs = new ClientDividendDetailsDS();
            clientDividendDetailsDs.Cid = cid;
            clientDividendDetailsDs.ID = entityID;
            clientDividendDetailsDs.CommandType = DatasetCommandTypes.Get;

            if (Broker != null)
            {
                IBrokerManagedComponent clientData = this.Broker.GetBMCInstance(cid);

                clientData.GetData(clientDividendDetailsDs);

                Broker.ReleaseBrokerManagedComponent(clientData);


            }
            else
            {
                throw new Exception("Broker Not Found");
            }



            return clientDividendDetailsDs;

        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            //TextInvestmentCode.Text = cmbSecurites.Text;
            if (Validate() == false)
            {
                if (ValidationFailed != null)
                    ValidationFailed();
                return;
            }
           
            var id = UpdateData();

            if (Saved != null)
            {
                Saved(id);
            }
        }



        public bool Validate()
        {
            bool result = true;
            bool tempResult = false;
            lblMessages.Text = "";
            StringBuilder Messages = new StringBuilder();

            tempResult = TextInvestmentCode.ValidateAlphaNumeric(true);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Investment Code", Messages);

            tempResult = TextUnitsOnHand.ValidateNumeric(true, 1, 9);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Units on hand", Messages);


            if (!TextUnitsOnHand.Enabled)
            {
                double unitonhand = -1;

                double.TryParse(TextUnitsOnHand.Text.Trim(),out unitonhand);
                tempResult = unitonhand <= 0;
                result = tempResult == false ? false : result;
                AddMessageIn(tempResult, "Units on hand cannot be 0", Messages);
            }
            if (cmbTransactionType.Text.Trim() != "Interest Income Accrual")
            {
                double centspershare = -1;
                 double.TryParse(TextCentsPerShare.Text,out centspershare);
                 tempResult = centspershare > 0;
                 result = tempResult == false ? false : result;
                 AddMessageIn(tempResult, "Cents per share cannot be 0 or less then 0", Messages);
            }


            tempResult = cmbAdministrationSystem.ValidateCombo(true);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Administration System*", Messages);
            if (!tempResult)
            {
                cmbAdministrationSystem.SelectedValue = null;
            }
            else
            {
                cmbAdministrationSystem.SelectedValue = cmbAdministrationSystem.Text;
            }

            tempResult = cmbTransactionType.ValidateCombo(true);
            if (!tempResult)
            {
                cmbTransactionType.SelectedValue = null;
            }
            else
            {
                cmbTransactionType.SelectedValue = cmbTransactionType.Text;
            }

            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Transaction Type*", Messages);
            DateTime date;
            tempResult = TextRecordDate.ValidateDate(true, "dd/MM/yyyy", out date);
            if (tempResult)
            {
                calRecodDate.SelectedDate = date;
            }
            else
            {
                calRecodDate.SelectedDate = null;
            }

            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Record Date*", Messages);
            tempResult = TextPaymentDate.ValidateDate(true, "dd/MM/yyyy", out date);
            if (tempResult)
            {
                calPaymentDate.SelectedDate = date;
            }
            else
            {
                calPaymentDate.SelectedDate = null;
            }

            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Payment Date*", Messages);

            tempResult = TextFrankedAmount.ValidateDecimalMin(false,0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Franked Amount", Messages);

            tempResult = TextUnfrankedAmount.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid UnFranked Amount", Messages);

            tempResult = TextTaxFree.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Tax Free", Messages);

            tempResult = TextTaxDeferred.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Tax Deferred", Messages);

            tempResult = TextReturnofCapital.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Return on Capital", Messages);

            tempResult = TextDomesticInterestAmount.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Domestic Interes tAmount", Messages);

            tempResult = TextForeignInterestAmount.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Foreign Interest Amount", Messages);

            tempResult = TextDomesticOtherIncome.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Domestic Other Income", Messages);

            tempResult = TextForeignOtherIncome.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Foreign Other Income", Messages);

            tempResult = TextDomesticWithHoldingTax.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Domestic With Holding Tax", Messages);


            tempResult = TextForeignWithHoldingTax.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Foreign With Holding Tax", Messages);

            tempResult = TextDomesticImputationTaxCredits.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Domestic Imputation Tax Credits", Messages);


            tempResult = TextForeignImputationTaxCredits.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Foreign Imputation Tax Credits", Messages);


            tempResult = TextForeignInvestmentFunds.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Foreign Investment Funds", Messages);


            tempResult = TextDomesticIndexationCGT.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Domestic Indexation CGT", Messages);


            tempResult = TextForeignIndexationCGT.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Foreign Indexation CGT", Messages);


            tempResult = TextDomesticDiscountedCGT.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Domestic Discounted CGTt", Messages);


            tempResult = TextForeignDiscountedCGT.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Foreign Discounted CGT", Messages);

            tempResult = TextDomesticOtherCGT.ValidateDecimalMin(false, 0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Domestic Other CGT", Messages);

            tempResult = TextForeignOtherCGT.ValidateDecimalMin(false,0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Foreign Other CGT", Messages);

            tempResult = TextDomesticGrossAmount.ValidateDecimalMin(false,0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Domestic Gross Amount", Messages);

            tempResult = TextCentsPerShare.ValidateDecimal(true);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Cents Per Share", Messages);


            tempResult = TextInterestPaid.ValidateDecimalMin(false,0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Interest Paid", Messages);

            tempResult = TextInterestRate.ValidateDecimalMin(false,0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Interest Rate", Messages);

            tempResult = TextInvestmentAmount.ValidateDecimalMin(false,0);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Investment Amount", Messages);

            tempResult = cmbDividendType.ValidateCombo(true);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Dividend Type*", Messages);
            if (!tempResult)
            {
                cmbDividendType.SelectedValue = null;
            }
            else
            {
                cmbDividendType.SelectedValue = cmbDividendType.Text;
            }

            tempResult = txtBalanceDate.ValidateDate(true, "dd/MM/yyyy", out date);
            if (tempResult)
            {
                calBanalceDate.SelectedDate = date;
            }
            else
            {
                calBanalceDate.SelectedDate = null;
            }

            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Balance Date*", Messages);

            tempResult = txtBooksCloseDate.ValidateDate(true, "dd/MM/yyyy", out date);
            if (tempResult)
            {
                calBooksCloseDate.SelectedDate = date;
            }
            else
            {
                calBooksCloseDate.SelectedDate = null;
            }

            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Books Close Date*", Messages);


            //decimal Total = 0;

            //if (result)
            //{
            //    tempResult = ValidatePercentFields(out Total);
            //    result = tempResult == false ? false : result;
            //    AddMessageIn(tempResult, "Percentage total should be from 0 to 100: Current Total:" + Total, Messages);
            //}

            lblMessages.Text = Messages.ToString();

            return result;
        }
        private void AddMessageIn(bool result, string message, StringBuilder Messages)
        {
            if (!result)
            {
                Messages.Append(message + "<br/>");
            }
        }
        public bool ValidatePercentFields(out decimal Total)
        {
            Total = (TextDomesticDiscountedCGT.Text == "" ? 0 : Convert.ToDecimal(TextDomesticDiscountedCGT.Text))
                + (TextDomesticGrossAmount.Text == "" ? 0 : Convert.ToDecimal(TextDomesticGrossAmount.Text))
                + (TextDomesticImputationTaxCredits.Text == "" ? 0 : Convert.ToDecimal(TextDomesticImputationTaxCredits.Text))
                + (TextDomesticIndexationCGT.Text == "" ? 0 : Convert.ToDecimal(TextDomesticIndexationCGT.Text))
                + (TextDomesticInterestAmount.Text == "" ? 0 : Convert.ToDecimal(TextDomesticInterestAmount.Text))
                + (TextDomesticOtherCGT.Text == "" ? 0 : Convert.ToDecimal(TextDomesticOtherCGT.Text))
                + (TextDomesticOtherIncome.Text == "" ? 0 : Convert.ToDecimal(TextDomesticOtherIncome.Text))
                + (TextDomesticWithHoldingTax.Text == "" ? 0 : Convert.ToDecimal(TextDomesticWithHoldingTax.Text))
                + (TextForeignDiscountedCGT.Text == "" ? 0 : Convert.ToDecimal(TextForeignDiscountedCGT.Text))
                + (TextForeignImputationTaxCredits.Text == "" ? 0 : Convert.ToDecimal(TextForeignImputationTaxCredits.Text))
                + (TextForeignIndexationCGT.Text == "" ? 0 : Convert.ToDecimal(TextForeignIndexationCGT.Text))
                + (TextForeignInterestAmount.Text == "" ? 0 : Convert.ToDecimal(TextForeignInterestAmount.Text))
                + (TextForeignInvestmentFunds.Text == "" ? 0 : Convert.ToDecimal(TextForeignInvestmentFunds.Text))
                + (TextForeignOtherCGT.Text == "" ? 0 : Convert.ToDecimal(TextForeignOtherCGT.Text))
                + (TextForeignOtherIncome.Text == "" ? 0 : Convert.ToDecimal(TextForeignOtherIncome.Text))
                + (TextForeignWithHoldingTax.Text == "" ? 0 : Convert.ToDecimal(TextForeignWithHoldingTax.Text))
                + (TextFrankedAmount.Text == "" ? 0 : Convert.ToDecimal(TextFrankedAmount.Text))
                + (TextReturnofCapital.Text == "" ? 0 : Convert.ToDecimal(TextReturnofCapital.Text))
                + (TextTaxDeferred.Text == "" ? 0 : Convert.ToDecimal(TextTaxDeferred.Text))
                + (TextTaxFree.Text == "" ? 0 : Convert.ToDecimal(TextTaxFree.Text))
                + (TextUnfrankedAmount.Text == "" ? 0 : Convert.ToDecimal(TextUnfrankedAmount.Text));

            return Total >= 0 && Total <= 100;
        }
        private Guid UpdateData()
        {
            ClientDividendDetailsDS clientDividendDetailsDs = new ClientDividendDetailsDS();
            clientDividendDetailsDs.Cid = new Guid(TextclientCSID.Value);


            var id = new Guid(TextID.Value);
            if (id == Guid.Empty)
            {
                clientDividendDetailsDs.ID = Guid.NewGuid();
                clientDividendDetailsDs.CommandType = DatasetCommandTypes.Add;
            }
            else
            {
                clientDividendDetailsDs.ID = id;
                clientDividendDetailsDs.CommandType = DatasetCommandTypes.Update;
            }

            if (Broker != null)
            {
               
                clientDividendDetailsDs.Entity = GetEntity();
                clientDividendDetailsDs.Entity.ID = clientDividendDetailsDs.ID;
                if (SaveData != null)
                {
                    SaveData(clientDividendDetailsDs);
                }
            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            TextID.Value = clientDividendDetailsDs.Entity.ID.ToString();

            return clientDividendDetailsDs.ID;
        }
        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (Canceled != null)
            {
                Canceled();
            }
        }
        private void ChangeLabel(string symbol)
        {
            Label29.Text = symbol;
            Label30.Text = symbol;
            Label31.Text = symbol;
            Label32.Text = symbol;
            Label33.Text = symbol;
            Label35.Text = symbol;
            Label36.Text = symbol;
            Label37.Text = symbol;
            Label38.Text = symbol;
            Label39.Text = symbol;
            Label40.Text = symbol;
            Label41.Text = symbol;
            Label42.Text = symbol;
            Label43.Text = symbol;
            Label44.Text = symbol;
            Label45.Text = symbol;
            Label46.Text = symbol;
            Label47.Text = symbol;
            Label48.Text = symbol;
            Label49.Text = symbol;
            Label50.Text = symbol;
            Label51.Text = symbol;
        }
        public void SetMISFundUnitsForClient(string fundCode)
        {
            var org = this.Broker.GetWellKnownBMC(WellKnownCM.Organization);
            ClientTotalUnitsDS clientTotalUnitsDs = new ClientTotalUnitsDS();
            clientTotalUnitsDs.Command = (int)WebCommands.GetOrganizationUnitsByType;
            clientTotalUnitsDs.Unit = new Oritax.TaxSimp.Data.OrganizationUnit
            {
                CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                Type = ((int)OrganizationType.ManagedInvestmentSchemesAccount).ToString()
            };
            clientTotalUnitsDs.code = fundCode;
            clientTotalUnitsDs.clientID = hf_clientid.Value;
            org.GetData(clientTotalUnitsDs);
            Broker.ReleaseBrokerManagedComponent(org);
            if (clientTotalUnitsDs.Tables.Count > 0 && clientTotalUnitsDs.Tables[clientTotalUnitsDs.clientTotalUnitsTable.ToString()].Rows.Count>0)
            {
                DataTable dt = clientTotalUnitsDs.Tables[clientTotalUnitsDs.clientTotalUnitsTable.ToString()];
                TextUnitsOnHand.Text = dt.Rows[0][clientTotalUnitsDs.clientTotalUnitsTable.TOTALUNITS].ToString();
            }
            else
            {
               TextUnitsOnHand.Text = "0";
            }
        }
        public void SetASXUnitsForClient(string fundCode)
        {
            string cid = Request.QueryString["ins"];

            var client = Broker.GetBMCInstance(new Guid(cid));
            ClientTotalUnitsDS clientTotalUnitsDs = new ClientTotalUnitsDS();
            clientTotalUnitsDs.code = fundCode;
            clientTotalUnitsDs.clientID = hf_clientid.Value;
            client.GetData(clientTotalUnitsDs);
            Broker.ReleaseBrokerManagedComponent(client);
            if (clientTotalUnitsDs.Tables.Count > 0 && clientTotalUnitsDs.Tables[clientTotalUnitsDs.clientTotalUnitsTable.ToString()].Rows.Count > 0)
            {
                DataTable dt = clientTotalUnitsDs.Tables[clientTotalUnitsDs.clientTotalUnitsTable.ToString()];
                TextUnitsOnHand.Text = dt.Rows[0][clientTotalUnitsDs.clientTotalUnitsTable.TOTALUNITS].ToString();
            }
            else
            {
                TextUnitsOnHand.Text = "0";
            }

        }
        protected void cmbSecurities_OnSelectedIndexChanged(object sender, C1ComboBoxEventArgs args)
        {
            var dataRowView = cmbSecurites.Items[args.NewSelectedIndex].DataItem as System.Data.DataRowView;
            if (dataRowView != null)
            {
                DataRow dr =  dataRowView.Row;
                if (dr[SecuritiesDS.INVESTMENTTYPE].ToString() == "Managed Fund /Unit Trust")
                {
                    SetMISFundUnitsForClient(dr["Code"].ToString());
                }
                else
                {
                    SetASXUnitsForClient(dr["Code"].ToString());
                }
            }
            if (ValidationFailed != null)
                ValidationFailed();
        }
    }
}