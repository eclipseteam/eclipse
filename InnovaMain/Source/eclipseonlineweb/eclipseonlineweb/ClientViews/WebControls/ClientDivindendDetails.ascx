﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientDivindendDetails.ascx.cs"
    Inherits="eclipseonlineweb.Administration.ClientDivindendDetails" %>
<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" Assembly="C1.Web.Wijmo.Controls.4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<div class="popup_Container">
    <div class="popup_Titlebar" id="PopupHeader">
        <div class="TitlebarLeft">
            <asp:Label ID="Title" runat="server" Text="Divindends Details"></asp:Label>
        </div>
    </div>
    <div class="PopupBody">
        <table>
            <tr>
                <td>
                </td>
                <td>
                    <asp:HiddenField ID="TextID" runat="server" />
                    <asp:HiddenField ID="hf_clientid" runat="server" />
                    <asp:HiddenField ID="TextCashManagementTransactionID" runat="server" />
                    <asp:HiddenField ID="TextExternalReferenceID" runat="server" />
                    <asp:HiddenField ID="TextInvestor" runat="server" />

                </td>
                <td>
                </td>
                <td>
                    <asp:HiddenField ID="TextclientCSID" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelInvestmentCode" runat="server" Text="Investment Code"></asp:Label>
                </td>
                <td>
                   <c1:C1ComboBox ID="cmbSecurites" runat="server" AutoPostBack="true" EnableViewState="true" OnSelectedIndexChanged="cmbSecurities_OnSelectedIndexChanged"></c1:C1ComboBox>
                   
                   <asp:TextBox CssClass="Txt-Field"  ID="TextInvestmentCode" runat="server" />
                </td>
                <td>
                    <asp:Label ID="LabelUnitsOnHand" runat="server" Text="Units on Hand"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextUnitsOnHand" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Administration System"></asp:Label>
                </td>
                <td>
                    <c1:C1ComboBox ID="cmbAdministrationSystem" runat="server" ViewStateMode="Enabled">
                        <Items>
                            <c1:C1ComboBoxItem Value="Manual Transactions" Text="Manual Transactions"></c1:C1ComboBoxItem>
                        </Items>
                    </c1:C1ComboBox>
                </td>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Transaction Type"></asp:Label>
                </td>
                <td>
                    <c1:C1ComboBox ID="cmbTransactionType" runat="server" ViewStateMode="Enabled">
                        <Items>
                         <c1:C1ComboBoxItem Value="Interest Income Accrual" Text="Interest Income Accrual"></c1:C1ComboBoxItem>                            
                         <c1:C1ComboBoxItem Value="Dividend Accrual" Text="Dividend Accrual"></c1:C1ComboBoxItem>
                        </Items>
                    </c1:C1ComboBox>
                </td>
            </tr>
            <tr>
            <td>
                    <asp:Label ID="Label34" runat="server" Text="Balance Date"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtBalanceDate" runat="server" ReadOnly="false" />
                    <ajaxToolkit:CalendarExtender runat="server" ID="calBanalceDate" TargetControlID="txtBalanceDate"
                        Format="dd/MM/yyyy " />
                </td>
                <td>
                    <asp:Label ID="Label54" runat="server" Text="Dividend Type"></asp:Label>
                </td>
                <td>
                    <c1:C1ComboBox ID="cmbDividendType" runat="server" ViewStateMode="Enabled">
                        <Items>
                            <c1:C1ComboBoxItem Value="Interim" Text="Interim"></c1:C1ComboBoxItem>
                            <c1:C1ComboBoxItem Value="Final" Text="Final"></c1:C1ComboBoxItem>
                        </Items>
                    </c1:C1ComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Ex-Dividend Date"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextRecordDate" runat="server" ReadOnly="false" />
                    <ajaxToolkit:CalendarExtender runat="server" ID="calRecodDate" TargetControlID="TextRecordDate"
                        Format="dd/MM/yyyy " />
                </td>
                 <td>
                    <asp:Label ID="Label52" runat="server" Text="Books Close Date"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtBooksCloseDate" runat="server" ReadOnly="false" />
                    <ajaxToolkit:CalendarExtender runat="server" ID="calBooksCloseDate" TargetControlID="txtBooksCloseDate"
                        Format="dd/MM/yyyy " />
                </td>
                
                
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Payment Date"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextPaymentDate" runat="server" ReadOnly="false" />
                    <ajaxToolkit:CalendarExtender runat="server" ID="calPaymentDate" TargetControlID="TextPaymentDate"
                        Format="dd/MM/yyyy " />
                </td>

                <td>
                    <asp:Label ID="Label10" runat="server" Text="Cents per share ($)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextCentsPerShare" runat="server" />
                </td>
                
               
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Franked Amount"></asp:Label>
                    <asp:Label ID="Label29" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextFrankedAmount" runat="server" />
                </td>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Unfranked Amount"></asp:Label>
                    <asp:Label ID="Label30" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextUnfrankedAmount" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label53" runat="server" Text="Currency (Ccy)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCurrency" runat="server" />
                </td>
                <td>
                    <asp:Label ID="Label8" runat="server" Text="Tax Deferred"></asp:Label>
                    <asp:Label ID="Label32" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextTaxDeferred" runat="server" />
                </td>
                
                
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" Text="Tax Free"></asp:Label>
                    <asp:Label ID="Label31" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextTaxFree" runat="server" />
                </td>
                <td>
                    <asp:Label ID="Label9" runat="server" Text="Return on Capital"></asp:Label>
                    <asp:Label ID="Label33" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextReturnofCapital" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label24" runat="server" Text="Franking Credits (Cents)"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="TextFrankedCredit" runat="server"></asp:Label>
                   <%-- <asp:TextBox CssClass="Txt-Field"  ID="TextFrankedCredit" runat="server" />--%>
                </td>
                <td>
                    <asp:Label ID="Label25" runat="server" Text="Total Franking Credits ($)"></asp:Label>                    
                </td>
                <td>
                    <asp:Label ID="TextTotalFrankingCredit" runat="server"></asp:Label>  
                   <%-- <asp:TextBox CssClass="Txt-Field"  ID="TextTotalFrankingCredit" runat="server" />--%>
                </td>
            </tr>
            
            
              <tr>
                <td>
                    <asp:Label ID="Label26" runat="server" Text="Interest Rate"></asp:Label>
                    
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextInterestRate" runat="server" />
                </td>
                <td>
                    <asp:Label ID="Label27" runat="server" Text="Investment Amount"></asp:Label>
                    <asp:Label ID="Label35" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextInvestmentAmount" runat="server" />
                </td>
            </tr>
             <tr>
                <td>
                    <asp:Label ID="Label28" runat="server" Text="Interest Paid ($)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextInterestPaid" runat="server" />
                </td>
                <td>
                    
                </td>
                <td>                  
                  
                </td>
            </tr>

            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label13" runat="server" Text="Domestic"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label14" runat="server" Text="Foreign"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label11" runat="server" Text="Interest Amount"></asp:Label>
                     <asp:Label ID="Label36" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextDomesticInterestAmount" runat="server" />
                    <asp:Label ID="Label37" runat="server" Text=""></asp:Label>
                </td>
                <td>                
                <asp:TextBox CssClass="Txt-Field"  ID="TextForeignInterestAmount" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label12" runat="server" Text="Other Income"></asp:Label>
                    <asp:Label ID="Label38" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextDomesticOtherIncome" runat="server" />
                    <asp:Label ID="Label39" runat="server" Text=""></asp:Label>
                </td>
                <td>                
                    <asp:TextBox CssClass="Txt-Field"  ID="TextForeignOtherIncome" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label15" runat="server" Text="Withholding Tax"></asp:Label>
                    <asp:Label ID="Label40" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label41" runat="server" Text=""></asp:Label>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextDomesticWithHoldingTax" runat="server" />
                </td>
                <td>                
                    <asp:TextBox CssClass="Txt-Field"  ID="TextForeignWithHoldingTax" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label16" runat="server" Text="Imputation Tax Credits"></asp:Label>
                     <asp:Label ID="Label42" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextDomesticImputationTaxCredits" runat="server" />
                    <asp:Label ID="Label43" runat="server" Text=""></asp:Label>
                </td>
                <td>                   
                    <asp:TextBox CssClass="Txt-Field"  ID="TextForeignImputationTaxCredits" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label17" runat="server" Text="Foreign Investment Funds (FIF)"></asp:Label>
                    <asp:Label ID="Label44" runat="server" Text=""></asp:Label>
                </td>
                <td>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextForeignInvestmentFunds" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label18" runat="server" Text="Tax Components" Font-Bold="true"></asp:Label>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label19" runat="server" Text="Indexation CGT"></asp:Label>
                    <asp:Label ID="Label45" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    
                    <asp:TextBox CssClass="Txt-Field"  ID="TextDomesticIndexationCGT" runat="server" />
                    <asp:Label ID="Label46" runat="server" Text=""></asp:Label>
                </td>
                <td>                
                    <asp:TextBox CssClass="Txt-Field"  ID="TextForeignIndexationCGT" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label20" runat="server" Text="Discounted CGT"></asp:Label>
                     <asp:Label ID="Label47" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextDomesticDiscountedCGT" runat="server" />
                    <asp:Label ID="Label48" runat="server" Text=""></asp:Label>
                </td>
                <td>
                 
                    <asp:TextBox CssClass="Txt-Field"  ID="TextForeignDiscountedCGT" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label21" runat="server" Text="Other CGT"></asp:Label>
                     <asp:Label ID="Label49" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextDomesticOtherCGT" runat="server" />
                    <asp:Label ID="Label50" runat="server" Text=""></asp:Label>
                </td>
                <td>
                 
                    <asp:TextBox CssClass="Txt-Field"  ID="TextForeignOtherCGT" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label22" runat="server" Text="Gross Amount"></asp:Label>
                     <asp:Label ID="Label51" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field"  ID="TextDomesticGrossAmount" runat="server" />
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label23" runat="server" Text="Comment"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:TextBox CssClass="Txt-Field"  ID="TextComment" runat="server" Width="100%" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="2">
                    <asp:Label ID="lblMessages" runat="server" Text="" ForeColor="Red" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                    <asp:Button ID="btnUpdate" runat="server" Text="Save" OnClick="btnUpdate_OnClick" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_OnClick" />
                </td>
            </tr>
        </table>
    </div>
</div>
