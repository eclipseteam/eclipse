﻿using System;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;

namespace eclipseonlineweb.ClientViews.WebControls
{
    public partial class ClientDetails : System.Web.UI.UserControl
    {
        public Action<Guid> Saved { get; set; }
        public Action<DataSet> SaveData { get; set; }
        public Action Canceled { get; set; }
        public Action ValidationFailed { get; set; }
        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        private void FillStatusCombo()
        {
            ComboStatusFlag.Items.Clear();
            ComboStatusFlag.DataSource = GetDataSet("Client");
            ComboStatusFlag.DataTextField = "Name";
            ComboStatusFlag.DataValueField = "Name";
            ComboStatusFlag.DataBind();

        }

        private StatusDS GetDataSet(string typeFilter)
        {
            StatusDS statusDs = new StatusDS { CommandType = DatasetCommandTypes.Get, TypeFilter = typeFilter };

            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetWellKnownBMC(WellKnownCM.Organization);
                clientData.GetData(statusDs);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            return statusDs;
        }

        public void ClearEntity()
        {
            lblClientName.Text = string.Empty;
            ComboOrganizationType.Text = "";
            ComboOrganizationType.SelectedValue = null;
            ComboStatusFlag.Text = "";
            ComboStatusFlag.SelectedValue = null;
            CheckIsActive.Checked = false;
            TextClientID.Text = string.Empty;
            chkDOITYOURSELF.Checked = false;
            chkDOITFORME.Checked = false;
            chkDOITWITHME.Checked = false;
        }

        public void SetEntity(Guid cid)
        {
            ClearEntity();
            if (cid != Guid.Empty)
            {
                FillStatusCombo();
                ClientDetailsDS ds = GetDataSet(cid);
                DataTable dt = ds.Tables[ds.ClientDetailsTable.TABLENAME];
                lblClientName.Text = dt.Rows[0][ds.ClientDetailsTable.CLIENTNAME].ToString();
                TextClientID.Text = dt.Rows[0][ds.ClientDetailsTable.CLIENTID].ToString();
                chkDOITYOURSELF.Checked = Convert.ToBoolean(dt.Rows[0][ds.ClientDetailsTable.DOITYOURSELF]);
                chkDOITFORME.Checked = Convert.ToBoolean(dt.Rows[0][ds.ClientDetailsTable.DOITFORME]);
                chkDOITWITHME.Checked = Convert.ToBoolean(dt.Rows[0][ds.ClientDetailsTable.DOITWITHME]);
                CheckIsActive.Checked = Convert.ToBoolean(dt.Rows[0][ds.ClientDetailsTable.ISCLIENT]);
                ComboOrganizationType.SelectedValue = dt.Rows[0][ds.ClientDetailsTable.INVESTORTYPE].ToString();
                ComboStatusFlag.SelectedValue = dt.Rows[0][ds.ClientDetailsTable.STATUSFLAG].ToString();
            }
        }

        private ClientDetailsDS GetDataSet(Guid cid)
        {
            ClientDetailsDS clientDetailsDs = new ClientDetailsDS();
            clientDetailsDs.CommandType = DatasetCommandTypes.Get;

            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
                clientData.GetData(clientDetailsDs);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            return clientDetailsDs;
        }


    }
}