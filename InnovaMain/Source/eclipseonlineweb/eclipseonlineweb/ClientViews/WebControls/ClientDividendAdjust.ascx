﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientDividendAdjust.ascx.cs"
    Inherits="eclipseonlineweb.ClientViews.WebControls.ClientDividendAdjust" %>
<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" Assembly="C1.Web.Wijmo.Controls.4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<script language="javascript">

    // Calcualte Gross Dividend for current row
    function setRowGrossDividend(id) {
        var currentRowElementUnitAmount = document.getElementById("txt_UnitAmount" + id);
        var currentRowElementAutaxCredit = document.getElementById("txt_AutaxCredit" + id);
        var currentRowElementTaxWithHeld = document.getElementById("txt_TaxWithHeld" + id);
        var currentRowElementGrossDistribution = document.getElementById("txt_GrossDistribution" + id);

        var total = 0;
        total = total + (parseFloat(currentRowElementUnitAmount.value) || 0);
        total = total + (parseFloat(currentRowElementAutaxCredit.value) || 0);
        total = total + (parseFloat(currentRowElementTaxWithHeld.value) || 0);
        currentRowElementGrossDistribution.innerHTML = "$ " + total.toFixed(2);

    }

     function SetPercentage(id) {

        var australianTaxComponents = [882, 871, 880, 881, 883, 873, 897];
        var foreignTaxComponents = [872, 884, 885, 886];
        var capitalGainsTaxComponents = [874, 875, 876, 877, 878, 879, 894];
        var nonAssessableTaxComponents = [888, 889, 890, 899, 900];

        // Calcualte Gross Dividend for current row
        setRowGrossDividend(id);

        // initialize variables
        var unitAmountColumnTotal = 0;
        var autaxCreditColumnTotal = 0;
        var taxWithheldColumnTotal = 0;
        var grossDistributionColumnTotal = 0;
        var UnitAmountSectionTotal = 0;
        var AutaxCreditSectionTotal = 0;
        var TaxWithheldSectionTotal = 0;

        var GrossDistributionSectionToatl = 0;
        var tt = 0;
        var unitAmountElement;
        var ausTaxCreditElement;
        var taxWithheldElement;
        var GrossDistributionElement;

        // calculate Australian Income total
        for (var i = 0; i < australianTaxComponents.length; i++) {
            tt = 0;
            unitAmountElement = document.getElementById("txt_UnitAmount" + australianTaxComponents[i]);
            ausTaxCreditElement = document.getElementById("txt_AutaxCredit" + australianTaxComponents[i]);
            taxWithheldElement = document.getElementById("txt_TaxWithHeld" + australianTaxComponents[i]);
            GrossDistributionElement = document.getElementById("txt_GrossDistribution" + australianTaxComponents[i]);


            UnitAmountSectionTotal = UnitAmountSectionTotal + (parseFloat(unitAmountElement.value) || 0);
            AutaxCreditSectionTotal = AutaxCreditSectionTotal + (parseFloat(ausTaxCreditElement.value) || 0);
            TaxWithheldSectionTotal = TaxWithheldSectionTotal + (parseFloat(taxWithheldElement.value) || 0);

            tt = (parseFloat(unitAmountElement.value) || 0) + (parseFloat(ausTaxCreditElement.value) || 0) + (parseFloat(taxWithheldElement.value) || 0);

            GrossDistributionSectionToatl = GrossDistributionSectionToatl + tt;
        }
        unitAmountColumnTotal = unitAmountColumnTotal + UnitAmountSectionTotal;
        autaxCreditColumnTotal = autaxCreditColumnTotal + AutaxCreditSectionTotal;
        taxWithheldColumnTotal = taxWithheldColumnTotal + TaxWithheldSectionTotal;
        grossDistributionColumnTotal = grossDistributionColumnTotal + GrossDistributionSectionToatl;

        document.getElementById("lblUnitAmount_Australian").innerHTML = "$ " + UnitAmountSectionTotal.toFixed(2);
        document.getElementById("lblAutaxCredit_Australian").innerHTML = "$ " + AutaxCreditSectionTotal.toFixed(2);
        document.getElementById("lblTaxWithHel_Australian").innerHTML = "$ " + TaxWithheldSectionTotal.toFixed(2);
        document.getElementById("lblGrossDistribution_Australian").innerHTML = "$ " + GrossDistributionSectionToatl.toFixed(2);

        UnitAmountSectionTotal = 0;
        AutaxCreditSectionTotal = 0;
        TaxWithheldSectionTotal = 0;
        GrossDistributionSectionToatl = 0;

        // calcualte Foreign Income Total
        for (var i = 0; i < foreignTaxComponents.length; i++) {
            tt = 0;
            unitAmountElement = document.getElementById("txt_UnitAmount" + foreignTaxComponents[i]);
            ausTaxCreditElement = document.getElementById("txt_AutaxCredit" + foreignTaxComponents[i]);
            taxWithheldElement = document.getElementById("txt_TaxWithHeld" + foreignTaxComponents[i]);
            GrossDistributionElement = document.getElementById("txt_GrossDistribution" + foreignTaxComponents[i]);

            UnitAmountSectionTotal = UnitAmountSectionTotal + (parseFloat(unitAmountElement.value) || 0);
            AutaxCreditSectionTotal = AutaxCreditSectionTotal + (parseFloat(ausTaxCreditElement.value) || 0);
            TaxWithheldSectionTotal = TaxWithheldSectionTotal + (parseFloat(taxWithheldElement.value) || 0);

            tt = (parseFloat(unitAmountElement.value) || 0) + (parseFloat(ausTaxCreditElement.value) || 0) + (parseFloat(taxWithheldElement.value) || 0);

            GrossDistributionSectionToatl = GrossDistributionSectionToatl + tt;
        }
        unitAmountColumnTotal = unitAmountColumnTotal + UnitAmountSectionTotal;
        autaxCreditColumnTotal = autaxCreditColumnTotal + AutaxCreditSectionTotal;
        taxWithheldColumnTotal = taxWithheldColumnTotal + TaxWithheldSectionTotal;
        grossDistributionColumnTotal = grossDistributionColumnTotal + GrossDistributionSectionToatl;

        document.getElementById("lblUnitAmount_Foreign").innerHTML = "$ " + UnitAmountSectionTotal.toFixed(2);
        document.getElementById("lblAutaxCredit_Foreign").innerHTML = "$ " + AutaxCreditSectionTotal.toFixed(2);
        document.getElementById("lblTaxWithHel_Foreign").innerHTML = "$ " + TaxWithheldSectionTotal.toFixed(2);
        document.getElementById("lblGrossDistribution_Foreign").innerHTML = "$ " + GrossDistributionSectionToatl.toFixed(2);

        UnitAmountSectionTotal = 0;
        AutaxCreditSectionTotal = 0;
        TaxWithheldSectionTotal = 0;
        GrossDistributionSectionToatl = 0;


        // calcualte Capital Gains Total
        for (var i = 0; i < capitalGainsTaxComponents.length; i++) {
            tt = 0;

            unitAmountElement = document.getElementById("txt_UnitAmount" + capitalGainsTaxComponents[i]);
            ausTaxCreditElement = document.getElementById("txt_AutaxCredit" + capitalGainsTaxComponents[i]);
            taxWithheldElement = document.getElementById("txt_TaxWithHeld" + capitalGainsTaxComponents[i]);
            GrossDistributionElement = document.getElementById("txt_GrossDistribution" + capitalGainsTaxComponents[i]);

            UnitAmountSectionTotal = UnitAmountSectionTotal + (parseFloat(unitAmountElement.value) || 0);
            AutaxCreditSectionTotal = AutaxCreditSectionTotal + (parseFloat(ausTaxCreditElement.value) || 0);
            TaxWithheldSectionTotal = TaxWithheldSectionTotal + (parseFloat(taxWithheldElement.value) || 0);

            tt = (parseFloat(unitAmountElement.value) || 0) + (parseFloat(ausTaxCreditElement.value) || 0) + (parseFloat(taxWithheldElement.value) || 0);

            GrossDistributionSectionToatl = GrossDistributionSectionToatl + tt;
        }
        unitAmountColumnTotal = unitAmountColumnTotal + UnitAmountSectionTotal;
        autaxCreditColumnTotal = autaxCreditColumnTotal + AutaxCreditSectionTotal;
        taxWithheldColumnTotal = taxWithheldColumnTotal + TaxWithheldSectionTotal;
        grossDistributionColumnTotal = grossDistributionColumnTotal + GrossDistributionSectionToatl;

        document.getElementById("lblUnitAmount_CapitalGains").innerHTML = "$ " + UnitAmountSectionTotal.toFixed(2);
        document.getElementById("lblAutaxCredit_CapitalGains").innerHTML = "$ " + AutaxCreditSectionTotal.toFixed(2);
        document.getElementById("lblTaxWithHel_CapitalGains").innerHTML = "$ " + TaxWithheldSectionTotal.toFixed(2);
        document.getElementById("lblGrossDistribution_CapitalGains").innerHTML = "$ " + GrossDistributionSectionToatl.toFixed(2);

        UnitAmountSectionTotal = 0;
        AutaxCreditSectionTotal = 0;
        TaxWithheldSectionTotal = 0;
        GrossDistributionSectionToatl = 0;

        // calculate Other Non-Assessable total
        for (var i = 0; i < nonAssessableTaxComponents.length; i++) {
            tt = 0;
            unitAmountElement = document.getElementById("txt_UnitAmount" + nonAssessableTaxComponents[i]);
            ausTaxCreditElement = document.getElementById("txt_AutaxCredit" + nonAssessableTaxComponents[i]);
            taxWithheldElement = document.getElementById("txt_TaxWithHeld" + nonAssessableTaxComponents[i]);
            GrossDistributionElement = document.getElementById("txt_GrossDistribution" + nonAssessableTaxComponents[i]);


            UnitAmountSectionTotal = UnitAmountSectionTotal + (parseFloat(unitAmountElement.value) || 0);
            AutaxCreditSectionTotal = AutaxCreditSectionTotal + (parseFloat(ausTaxCreditElement.value) || 0);
            TaxWithheldSectionTotal = TaxWithheldSectionTotal + (parseFloat(taxWithheldElement.value) || 0);

            tt = (parseFloat(unitAmountElement.value) || 0) + (parseFloat(ausTaxCreditElement.value) || 0) + (parseFloat(taxWithheldElement.value) || 0);

            GrossDistributionSectionToatl = GrossDistributionSectionToatl + tt;
        }
        unitAmountColumnTotal = unitAmountColumnTotal + UnitAmountSectionTotal;
        autaxCreditColumnTotal = autaxCreditColumnTotal + AutaxCreditSectionTotal;
        taxWithheldColumnTotal = taxWithheldColumnTotal + TaxWithheldSectionTotal;
        grossDistributionColumnTotal = grossDistributionColumnTotal + GrossDistributionSectionToatl;



        document.getElementById("lblUnitAmount_nonAssessable").innerHTML = "$ " + UnitAmountSectionTotal.toFixed(2);
        document.getElementById("lblAutaxCredit_nonAssessable").innerHTML = "$ " + AutaxCreditSectionTotal.toFixed(2);
        document.getElementById("lblTaxWithHel_nonAssessable").innerHTML = "$ " + TaxWithheldSectionTotal.toFixed(2);
        document.getElementById("lblGrossDistribution_nonAssessable").innerHTML = "$ " + GrossDistributionSectionToatl.toFixed(2);

        UnitAmountSectionTotal = 0;
        AutaxCreditSectionTotal = 0;
        TaxWithheldSectionTotal = 0;
        GrossDistributionSectionToatl = 0;


        // column total
        document.getElementById("lblUnitAmount_netcashDist").innerHTML = "$ " + unitAmountColumnTotal.toFixed(2);
        document.getElementById("lblAutaxCredit_netcashDist").innerHTML = "$ " + autaxCreditColumnTotal.toFixed(2);
        document.getElementById("lblTaxWithHel_netcashDist").innerHTML = "$ " + taxWithheldColumnTotal.toFixed(2);
        document.getElementById("lblGrossDistribution_netcashDist").innerHTML = "$ " + grossDistributionColumnTotal.toFixed(2);


    };

</script>
<style type="text/css">

    table.poptable {
	    font-family: verdana,arial,sans-serif;
	    font-size:10px;
	    color:#333333;
	    border-width: 1px;
	    border-color: #666666;
	    border-collapse: collapse;
	    width:100%;
    }
    table.poptable th {
	    border-width: 1px;
	    padding: 8px;
	    border-style: solid;
	    border-color: #666666;
	    background-color: #dedede;
    }
    
    table.poptable tr:nth-child(even) 
    {
        background-color: #CC9999;
    }
    
    table.poptable td {
	    border-width: 1px;
	    padding: 8px;
	    border-style: solid;
	    border-color: #666666;
	    background-color: #ffffff;
    }
    
    .poptd 
    {
        text-align:right;
    }
    
    .PopupTextBox
    {
        width:100%;
        background-color:transparent;
        border: 0px solid;
        outline:none;
        text-align:right;
    }
    
    .PopupButton
    {
        background: white;
        border: solid 1px grey;
        font-family: Arial, sans-serif;
        font-size: 12px;
        font-weight: bold;
        color: #001563;    
        height: 25px;  
        width: 10%;
        margin-left: 20%;
    }

    .PopupButton:hover
    {
        background: white;
        border: solid 1px grey;
        font-family: Arial, sans-serif;
        font-size: 12px;
        font-weight: bold;
        color: Blue;
        height: 25px;
        width: 10%;
        margin-left: 20%;
    }    
    
    .PopupCalendar 
    {
        background-color:Gray;
        color:Black;
    }
    
    .PopupCalendar tr td 
    {
     padding: 2px; 
     margin: 2px;
     }    

</style>

<div class="popup_Container">
    <div class="popup_Titlebar" id="PopupHeaderAdjust">
        <div class="TitlebarLeft">
            <asp:Label ID="Title" runat="server" Text="Dividend Details"></asp:Label>
        </div>
    </div>

    <div style="display:none">
        <asp:HiddenField ID="TextDividendEntityID" runat="server" />
        <asp:HiddenField ID="TextClientCMCIID" runat="server" />
    </div>

    <div class="popup_Body" style="height: 95%;">
        <table class="poptable">
            <tr>
                <%-- Code--%>
                <td>
                    <asp:Label ID="LabelInvestmentCode" runat="server" Text="Code"></asp:Label>
                </td>
                <td>
                   <asp:TextBox CssClass="PopupTextBox"  ID="TextInvestmentCode" runat="server" ReadOnly="true"/>
                </td>
                <%-- Amount--%>
                <td>
                    <asp:Label ID="LabelUnitsOnHand" runat="server" Text="Amount"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="PopupTextBox"  ID="TextUnitsOnHand" runat="server"  ReadOnly="true" />
                </td>
                <%-- balance date--%>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Balance Date"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="PopupTextBox"  ID="TextBalanceDate" runat="server"  ReadOnly="true"/>
                </td>
                <%-- dividend type--%>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Dividend Type"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="PopupTextBox"  ID="TextDividendType" runat="server" ReadOnly="true"/>
                </td>
            </tr>

            <tr>
                <%-- Ex-Dividend Date--%>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Ex-Dividend Date"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="PopupTextBox"  ID="TextExDividendDate" runat="server"  ReadOnly="true"/>
                </td>
                <%-- Payment Date--%>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Payment Date"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="PopupTextBox"  ID="TextPaymentDate" runat="server" ReadOnly="false" />
                    <ajaxToolkit:CalendarExtender runat="server" ID="calPaymentDate" TargetControlID="TextPaymentDate"
                        Format="dd/MM/yyyy " CssClass="PopupCalendar"  />
                </td>
                <%-- Cents per share--%>
                <td>
                    <asp:Label ID="Label10" runat="server" Text="Cents per share"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="PopupTextBox"  ID="TextCentsPerShare" runat="server"  ReadOnly="true"/>
                </td>
                <%-- Books Close Date--%>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Books Close Date"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="PopupTextBox"  ID="TextBooksCloseDate" runat="server"  ReadOnly="true"/>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <table class="poptable">
            <tr>
                <td colspan="4">
                    <div style="width: 100%; overflow: auto; height: 28em">
                        <table id="rowsTable" width="100%" style="border-color: royalblue; border-width: thin;
                            border-style: solid" cellspacing="0" class="poptable">
                            <thead>
                                <tr style="border-bottom: thin; border-bottom-color: royalblue; border-bottom-style: solid">
                                    <th>
                                        Components of the Dividend
                                    </th>
                                    <th style="display:none">
                                        Code
                                    </th>
                                    <th>
                                        Dividend Payment ($)
                                    </th>
                                    <th>
                                        Autax Credit ($)
                                    </th>
                                    <th>
                                        Tax Withheld ($)
                                    </th>
                                    <th>
                                        Gross Dividend ($)
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="5" class="distHeads">
                                        Australian Income
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Interest (subject to NR WHT)
                                    </td>
                                    <td style="display:none">
                                        882
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount882" runat="server" onchange='SetPercentage("882");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit882" runat="server" onchange='SetPercentage("882");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld882" runat="server" onchange='SetPercentage("882");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution882" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Interest (not subject to NR WHT) 
                                    </td>
                                    <td style="display:none">
                                        871
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount871" runat="server" onchange='SetPercentage("871");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit871" runat="server" onchange='SetPercentage("871");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld871" runat="server" onchange='SetPercentage("871");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution871" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Dividends - Franked
                                    </td>
                                    <td style="display:none">
                                        880
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount880" runat="server" onchange='SetPercentage("880");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit880" runat="server" onchange='SetPercentage("880");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld880" runat="server" onchange='SetPercentage("880");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution880" runat="server" onchange='SetPercentage("880");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Dividends - Unfranked
                                    </td>
                                    <td style="display:none">
                                        881
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount881" runat="server" onchange='SetPercentage("881");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit881" runat="server" onchange='SetPercentage("881");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld881" runat="server" onchange='SetPercentage("881");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution881" runat="server" onchange='SetPercentage("881");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Other Income
                                    </td>
                                    <td style="display:none">
                                        883
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount883" runat="server" onchange='SetPercentage("883");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit883" runat="server" onchange='SetPercentage("883");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld883" runat="server" onchange='SetPercentage("883");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution883" runat="server" onchange='SetPercentage("883");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td>
                                        Dividends Unfranked - CFI
                                    </td>
                                    <td style="display:none">
                                        873
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount873" runat="server" onchange='SetPercentage("873");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit873" runat="server" onchange='SetPercentage("873");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld873" runat="server" onchange='SetPercentage("873");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution873" runat="server" onchange='SetPercentage("873");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td>
                                        Dividends Franked
                                    </td>
                                    <td style="display:none">
                                        897
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount897" runat="server" onchange='SetPercentage("897");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit897" runat="server" onchange='SetPercentage("897");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld897" runat="server" onchange='SetPercentage("897");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution897" runat="server" onchange='SetPercentage("897");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="totalsRow">
                                    <td>
                                        Australian Income Total
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblUnitAmount_Australian" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblAutaxCredit_Australian" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblTaxWithHel_Australian" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblGrossDistribution_Australian" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="distHeads">
                                        Foreign Income
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Foreign Sourced Income
                                    </td>
                                    <td style="display:none">
                                        872
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount872" runat="server" onchange='SetPercentage("872");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit872" runat="server" onchange='SetPercentage("872");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld872" runat="server" onchange='SetPercentage("872");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution872" runat="server" onchange='SetPercentage("872");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign Dividend
                                    </td>
                                    <td style="display:none">
                                        884
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount884" runat="server" onchange='SetPercentage("884");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit884" runat="server" onchange='SetPercentage("884");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld884" runat="server" onchange='SetPercentage("884");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution884" runat="server" onchange='SetPercentage("884");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign Interest
                                    </td>
                                    <td style="display:none">
                                        885
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount885" runat="server" onchange='SetPercentage("885");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit885" runat="server" onchange='SetPercentage("885");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld885" runat="server" onchange='SetPercentage("885");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution885" runat="server" onchange='SetPercentage("885");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign Other Income
                                    </td>
                                    <td style="display:none">
                                        886
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount886" runat="server" onchange='SetPercentage("886");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit886" runat="server" onchange='SetPercentage("886");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld886" runat="server" onchange='SetPercentage("886");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution886" runat="server" onchange='SetPercentage("886");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="totalsRow">
                                    <td>
                                        Foreign Income Total
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblUnitAmount_Foreign" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblAutaxCredit_Foreign" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblTaxWithHel_Foreign" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblGrossDistribution_Foreign" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="distHeads">
                                        Capital Gains
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign Capital Gain - Indexed
                                    </td>
                                    <td style="display:none">
                                        874
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount874" runat="server" onchange='SetPercentage("874");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit874" runat="server" onchange='SetPercentage("874");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld874" runat="server" onchange='SetPercentage("874");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution874" runat="server" onchange='SetPercentage("874");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign Capital Gain – Other
                                    </td>
                                    <td style="display:none">
                                        875
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount875" runat="server" onchange='SetPercentage("875");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit875" runat="server" onchange='SetPercentage("875");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld875" runat="server" onchange='SetPercentage("875");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution875" runat="server" onchange='SetPercentage("875");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign Discount Gain – Direct Investment
                                    </td>
                                    <td style="display:none">
                                        876
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount876" runat="server" onchange='SetPercentage("876");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit876" runat="server" onchange='SetPercentage("876");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld876" runat="server" onchange='SetPercentage("876");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution876" runat="server" onchange='SetPercentage("876");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign Discount Gain – Unit Trust
                                    </td>
                                    <td style="display:none">
                                        877
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount877" runat="server" onchange='SetPercentage("877");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit877" runat="server" onchange='SetPercentage("877");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld877" runat="server" onchange='SetPercentage("877");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution877" runat="server" onchange='SetPercentage("877");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign CGT Concession Amount - Direct Investment
                                    </td>
                                    <td style="display:none">
                                        878
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount878" runat="server" onchange='SetPercentage("878");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit878" runat="server" onchange='SetPercentage("878");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld878" runat="server" onchange='SetPercentage("878");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution878" runat="server" onchange='SetPercentage("878");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign CGT Concession Amount - Unit Trust
                                    </td>
                                    <td style="display:none">
                                        879
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount879" runat="server" onchange='SetPercentage("879");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit879" runat="server" onchange='SetPercentage("879");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld879" runat="server" onchange='SetPercentage("879");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution879" runat="server" onchange='SetPercentage("879");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        CGT Concession Amount
                                    </td>
                                    <td style="display:none">
                                        894
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount894" runat="server" onchange='SetPercentage("894");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit894" runat="server" onchange='SetPercentage("894");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld894" runat="server" onchange='SetPercentage("894");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution894" runat="server" onchange='SetPercentage("894");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr  style="display:none;">
                                    <td>
                                        Capital Gain
                                    </td>
                                    <td style="display:none">
                                        887
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount887" runat="server" onchange='SetPercentage("887");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit887" runat="server" onchange='SetPercentage("887");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld887" runat="server" onchange='SetPercentage("887");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution887" runat="server" onchange='SetPercentage("887");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="totalsRow">
                                    <td>
                                        Capital Gains Total
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblUnitAmount_CapitalGains" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblAutaxCredit_CapitalGains" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblTaxWithHel_CapitalGains" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblGrossDistribution_CapitalGains" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="5">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="distHeads">
                                        Other Non-Assessable Amount
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Tax-deferred Amounts
                                    </td>
                                    <td style="display:none">
                                        888
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount888" runat="server" onchange='SetPercentage("888");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit888" runat="server" onchange='SetPercentage("888");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld888" runat="server" onchange='SetPercentage("888");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution888" runat="server" onchange='SetPercentage("888");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Tax Free Income
                                    </td>
                                    <td style="display:none">
                                        889
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount889" runat="server" onchange='SetPercentage("889");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit889" runat="server" onchange='SetPercentage("889");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld889" runat="server" onchange='SetPercentage("889");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution889" runat="server" onchange='SetPercentage("889");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Other Income
                                    </td>
                                    <td style="display:none">
                                        890
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount890" runat="server" onchange='SetPercentage("890");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit890" runat="server" onchange='SetPercentage("890");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld890" runat="server" onchange='SetPercentage("890");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution890" runat="server" onchange='SetPercentage("890");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Trans-Tasman dividends
                                    </td>
                                    <td style="display:none">
                                        899
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount899" runat="server" onchange='SetPercentage("899");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit899" runat="server" onchange='SetPercentage("899");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld899" runat="server" onchange='SetPercentage("899");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution899" runat="server" onchange='SetPercentage("899");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Return of Capital
                                    </td>
                                    <td style="display:none">
                                        900
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_UnitAmount900" runat="server" onchange='SetPercentage("900");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_AutaxCredit900" runat="server" onchange='SetPercentage("900");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="PopupTextBox" ID="txt_TaxWithHeld900" runat="server" onchange='SetPercentage("900");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="txt_GrossDistribution900" runat="server" onchange='SetPercentage("900");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="totalsRow">
                                    <td>
                                        Other Non-Assessable Amount Total
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblUnitAmount_nonAssessable" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblAutaxCredit_nonAssessable" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblTaxWithHel_nonAssessable" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblGrossDistribution_nonAssessable" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        &nbsp;
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr class="totalsRow">
                                    <td style="font-style:normal;font-weight:bold;">
                                        Column Total
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblUnitAmount_netcashDist" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblAutaxCredit_netcashDist" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblTaxWithHel_netcashDist" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="text-align:right;">
                                        <asp:Label ID="lblGrossDistribution_netcashDist" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        <div>
                    <asp:Label ID="lblMessages" runat="server" Text="" ForeColor="#990000" Font-Bold="True"></asp:Label>
        </div>
        <div>
                    <br/>
                    <br/>
                    <asp:Button ID="btnUpdate" runat="server" Visible="true" Text="Save" OnClick="btnUpdate_OnClick" class="PopupButton"/>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_OnClick"  class="PopupButton"/>
        </div>
    </div>
</div>
