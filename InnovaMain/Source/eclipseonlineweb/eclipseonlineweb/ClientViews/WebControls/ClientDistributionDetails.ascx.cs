﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;

namespace eclipseonlineweb.ClientViews.WebControls
{
    public partial class ClientDistributionDetails : System.Web.UI.UserControl
    {
        public Action<Guid> Saved
        {
            get;
            set;
        }
        public Action<DataSet> SaveData
        {
            get;
            set;
        }
        public Action Canceled { get; set; }
        public Action ValidationFailed { get; set; }
        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void ClearEntity()
        {
            lblMessages.Text = string.Empty;
            TextGrossDistributionDPU.Text = string.Empty;
            TextNetDistributionDPU.Text = string.Empty;
            TextRecordDate.Text = string.Empty;
            TextPaymentDate.Text = string.Empty;
            calRecodDate.SelectedDate = null;
            calPaymentDate.SelectedDate = null;
            TextID.Value = Guid.Empty.ToString();
            TextClietCLID.Value = Guid.Empty.ToString();
            TxtFundCode.Text = string.Empty;
            txtFundName.Text = string.Empty;
            TextPaymentDate.Text = string.Empty;
            LblAccountName.Text = string.Empty;
            LblAccountCode.Text = string.Empty;
            LblUnitsHeld.Text = string.Empty;
            LblUnitHeldDate.Text = string.Empty;

            lblAutaxCredit_Australian.Text =lblAutaxCredit_Foreign.Text = lblAutaxCredit_nonAssessable.Text = lblGrossDistribution_netcashDist.Text = string.Empty;
            lblTaxWithHel_Australian.Text =lblTaxWithHel_Foreign.Text =lblTaxWithHel_nonAssessable.Text = lblTaxWithHel_netcashDist.Text = string.Empty;
            lblUnitAmount_Australian.Text = lblUnitAmount_Foreign.Text = lblUnitAmount_nonAssessable.Text = lblUnitAmount_netcashDist.Text = string.Empty;
            lblGrossDistribution_Australian.Text = lblGrossDistribution_Foreign.Text = lblGrossDistribution_nonAssessable.Text = lblGrossDistribution_netcashDist.Text = string.Empty;
                foreach (var control in this.Controls)
            {

                if (control is TextBox && (control as TextBox).ID.StartsWith("txt_"))
                {
                    (control as TextBox).Text = string.Empty;
                }
                else
                    if (control is Label && (control as Label).ID.StartsWith("txt_"))
                    {
                        (control as Label).Text = string.Empty;
                    }
            }





        }

        public DistributionIncomeEntity GetEntity()
        {
            var entity = new DistributionIncomeEntity();
            entity.ID = new Guid(TextID.Value);

            DateTime date;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            if (TextRecordDate.Text.Trim() != string.Empty)
            {
                if (DateTime.TryParse(TextRecordDate.Text.Trim(), info, DateTimeStyles.None, out date))
                {
                    entity.RecordDate = date;
                }
            }
            else
            {
                entity.RecordDate = null;
            }

            if (TextRecordDate.Text.Trim() != string.Empty)
            {

                if (DateTime.TryParse(TextPaymentDate.Text.Trim(), info, DateTimeStyles.None, out date))
                {
                    entity.PaymentDate = date;
                }
            }
            else
            {
                entity.PaymentDate = null;
            }


            ClientDistributionDetailsDS ds = GetDataSet(new Guid(TextID.Value), new Guid(TextClietCLID.Value));
            if (ds.Entity != null)
            {
                //entity.ProcessFlag = ds.Entity.ProcessFlag;
                entity.Components = ds.Entity.Components;
                entity.FundID = entity.FundID;
                entity.FundCode=entity.FundCode;
                 entity.FundName=entity.FundName;
                 entity.AccountName=entity.AccountName;
                entity.AccountNumber=entity.AccountNumber;
            }

            if (entity.Components == null)
            {
                entity.Components = new List<DistributionComponent>();
            }
            bool addnew = false;
            bool hasValues = false;
            foreach (var componentKey in DistributionComponent.ComponentsTypes.Keys)
            {
                addnew = false;
                hasValues = false;
                var conponent = entity.Components.Where(ss => ss.ComponentType == componentKey).FirstOrDefault();
                if (conponent == null)
                {
                    conponent = new DistributionComponent() { ComponentType = componentKey };
                    addnew = true;
                }

                var txt_UnitAmount = this.FindControl("txt_UnitAmount" + componentKey);
                if (txt_UnitAmount != null)
                {
                    conponent.Unit_Amount = ((txt_UnitAmount as TextBox).Text != "") ? Convert.ToDouble((txt_UnitAmount as TextBox).Text) : (double?)null;
                    hasValues = true;
                }

                var txt_AutaxCredit = this.FindControl("txt_AutaxCredit" + componentKey);
                if (txt_AutaxCredit != null)
                {
                    conponent.Autax_Credit = ((txt_AutaxCredit as TextBox).Text != "") ? Convert.ToDouble((txt_AutaxCredit as TextBox).Text) : (double?)null;
                    hasValues = true;
                }
                var txt_TaxWithHeld = this.FindControl("txt_TaxWithHeld" + componentKey);
                if (txt_TaxWithHeld != null)
                {
                    conponent.Tax_WithHeld = ((txt_TaxWithHeld as TextBox).Text != "") ? Convert.ToDouble((txt_TaxWithHeld as TextBox).Text) : (double?)null;
                    hasValues = true;
                }
                if (addnew && hasValues)
                {

                    entity.Components.Add(conponent);
                }


            }

            return entity;
        }


        public void SetEntity(Guid ID, Guid clid,string clientName)
        {
            ClearEntity();
            TextClietCLID.Value = clid.ToString();
            if (ID != Guid.Empty)
            {
                ClientDistributionDetailsDS ds = GetDataSet(ID, clid);
                var Entity = ds.Entity;

                TextID.Value = ID.ToString();
                TextClietCLID.Value = clid.ToString();
                TxtFundCode.Text = Entity.FundCode;
                txtFundName.Text = Entity.FundName;
                LblAccountName.Text = Entity.AccountName;
                LblAccountCode.Text = Entity.AccountNumber;
                LblUnitsHeld.Text = Entity.RecodDate_Shares.ToString();

                TextNetDistributionDPU.Text = Entity.NetDistributionDPU.ToString();
                TextGrossDistributionDPU.Text = Entity.GrossDistributionDPU.ToString();

                if (Entity.RecordDate.HasValue)
                {
                    LblUnitHeldDate.Text = Entity.RecordDate.Value.ToString("dd/MM/yyyy");
                    calRecodDate.SelectedDate = Entity.RecordDate.Value;
                    TextRecordDate.Text = Entity.RecordDate.Value.ToString("dd/MM/yyyy");
                }
                if (Entity.PaymentDate.HasValue)
                {
                    calPaymentDate.SelectedDate = Entity.PaymentDate.Value;
                    TextPaymentDate.Text = Entity.PaymentDate.Value.ToString("dd/MM/yyyy");
                }

                if (Entity.Components != null)
                {
                    foreach (var component in Entity.Components)
                    {

                        bool hasValue = false;
                        var unitamount = this.FindControl("txt_UnitAmount" + component.ComponentType);
                        if (unitamount != null)
                        {
                            if (component.Unit_Amount.HasValue)
                            {
                                (unitamount as TextBox).Text = component.Unit_Amount.Value.ToString();
                                hasValue = true;
                            }
                            else
                            {
                                (unitamount as TextBox).Text = "";
                            }
                        }

                        var txt_AutaxCredit = this.FindControl("txt_AutaxCredit" + component.ComponentType);
                        if (txt_AutaxCredit != null)
                        {
                            if (component.Autax_Credit.HasValue)
                            {
                                (txt_AutaxCredit as TextBox).Text = component.Autax_Credit.Value.ToString();
                                hasValue = true;
                            }
                            else
                            {
                                (txt_AutaxCredit as TextBox).Text = "";
                            }
                        }
                        var txt_TaxWithHeld = this.FindControl("txt_TaxWithHeld" + component.ComponentType);
                        if (txt_TaxWithHeld != null)
                        {
                            if (component.Tax_WithHeld.HasValue)
                            {
                                (txt_TaxWithHeld as TextBox).Text = component.Tax_WithHeld.Value.ToString();
                                hasValue = true;
                            }
                            else
                            {
                                (txt_TaxWithHeld as TextBox).Text = "";
                            }
                        }

                        var txt_GrossDistribution = this.FindControl("txt_GrossDistribution" + component.ComponentType);
                        if (txt_GrossDistribution != null)
                        {
                            if (hasValue)
                            {
                                (txt_GrossDistribution as Label).Text ="$"+ component.GrossDistribution.ToString();
                                hasValue = true;
                            }
                            else
                            {
                                (txt_GrossDistribution as Label).Text = "";
                            }
                        }




                    }

                    lblAutaxCredit_Australian.Text = "$" + Entity.Components.Where(ss => ss.Category == ComponentCategory.Australian_Income).Sum(ss => ss.Autax_Credit).ToString();
                    lblAutaxCredit_Foreign.Text = "$" + Entity.Components.Where(ss => ss.Category == ComponentCategory.Foreign_Income).Sum(ss => ss.Autax_Credit).ToString();
                    lblAutaxCredit_nonAssessable.Text = "$" + Entity.Components.Where(ss => ss.Category == ComponentCategory.Other_Non_Assessable_Amount).Sum(ss => ss.Autax_Credit).ToString();
                    lblGrossDistribution_netcashDist.Text = "$" + Entity.Components.Sum(ss => ss.Autax_Credit).ToString();


                    lblTaxWithHel_Australian.Text = "$" + Entity.Components.Where(ss => ss.Category == ComponentCategory.Australian_Income).Sum(ss => ss.Tax_WithHeld).ToString();
                    lblTaxWithHel_Foreign.Text = "$" + Entity.Components.Where(ss => ss.Category == ComponentCategory.Foreign_Income).Sum(ss => ss.Tax_WithHeld).ToString();
                    lblTaxWithHel_nonAssessable.Text = "$" + Entity.Components.Where(ss => ss.Category == ComponentCategory.Other_Non_Assessable_Amount).Sum(ss => ss.Tax_WithHeld).ToString();
                    lblTaxWithHel_netcashDist.Text = "$" + Entity.Components.Sum(ss => ss.Tax_WithHeld).ToString();

                    lblUnitAmount_Australian.Text = "$" + Entity.Components.Where(ss => ss.Category == ComponentCategory.Australian_Income).Sum(ss => ss.Unit_Amount).ToString();
                    lblUnitAmount_Foreign.Text = "$" + Entity.Components.Where(ss => ss.Category == ComponentCategory.Foreign_Income).Sum(ss => ss.Unit_Amount).ToString();
                    lblUnitAmount_nonAssessable.Text = "$" + Entity.Components.Where(ss => ss.Category == ComponentCategory.Other_Non_Assessable_Amount).Sum(ss => ss.Unit_Amount).ToString();
                    lblUnitAmount_netcashDist.Text = "$" + Entity.Components.Sum(ss => ss.Unit_Amount).ToString();

                    lblGrossDistribution_Australian.Text = "$" + Entity.Components.Where(ss => ss.Category == ComponentCategory.Australian_Income).Sum(ss => ss.GrossDistribution).ToString();
                    lblGrossDistribution_Foreign.Text = "$" + Entity.Components.Where(ss => ss.Category == ComponentCategory.Foreign_Income).Sum(ss => ss.GrossDistribution).ToString();
                    lblGrossDistribution_nonAssessable.Text = "$" + Entity.Components.Where(ss => ss.Category == ComponentCategory.Other_Non_Assessable_Amount).Sum(ss => ss.GrossDistribution).ToString();
                    lblGrossDistribution_netcashDist.Text = "$" + Entity.Components.Sum(ss => ss.GrossDistribution).ToString();  
               
                   
                   
                    
            
                }


            }
            
        }

        private ClientDistributionDetailsDS GetDataSet(Guid entityID, Guid cid)
        {

            ClientDistributionDetailsDS bankTransactionDetailsDS = new ClientDistributionDetailsDS();
            bankTransactionDetailsDS.Cid = cid;
            bankTransactionDetailsDS.ID = entityID;
            bankTransactionDetailsDS.CommandType = DatasetCommandTypes.Get;
            
            if (Broker != null)
            {
                IBrokerManagedComponent clientData = this.Broker.GetBMCInstance(cid);

                clientData.GetData(bankTransactionDetailsDS);

                Broker.ReleaseBrokerManagedComponent(clientData);


            }
            else
            {
                throw new Exception("Broker Not Found");
            }



            return bankTransactionDetailsDS;

        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            if (Validate() == false)
            {
                if (ValidationFailed != null)
                    ValidationFailed();
                return;
            }

            var id = UpdateData();
            if (Saved != null)
            {

                Saved(id);
            }
        }



        public bool Validate()
        {
            bool result = true;
            bool tempResult = false;
            lblMessages.Text = "";
            StringBuilder Messages = new StringBuilder();

            DateTime date;
            tempResult = TextRecordDate.ValidateDate(true, "dd/MM/yyyy", out date);
            if (tempResult)
            {
                calRecodDate.SelectedDate = date;
            }
            else
            {
                calRecodDate.SelectedDate = null;
            }

            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Record Date*", Messages);

            tempResult = TextPaymentDate.ValidateDate(true, "dd/MM/yyyy", out date);
            if (tempResult)
            {
                calPaymentDate.SelectedDate = date;
            }
            else
            {
                calPaymentDate.SelectedDate = null;
            }
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Payment Date*", Messages);


            tempResult = TxtFundCode.ValidateAlphaNumeric(1,1000);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Fund Code*", Messages);

            tempResult = txtFundName.ValidateAlphaNumeric(1, 1000);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Fund Name*", Messages);

            tempResult = LblAccountName.ValidateAlphaNumeric(1, 1000);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid  Account Name*", Messages);

            tempResult = LblAccountCode.ValidateAlphaNumeric(1, 1000);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Account Number*", Messages);

            tempResult = LblUnitsHeld.ValidateDecimal(true);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Units Held*", Messages);
            

            decimal total = 0;
            bool hasValue = false;
            foreach (var componentKey in DistributionComponent.ComponentsTypes.Keys)
            {

                total = 0;
                hasValue = false;
                var txt_UnitAmount = this.FindControl("txt_UnitAmount" + componentKey);
                if (txt_UnitAmount != null)
                {
                    tempResult = (txt_UnitAmount as TextBox).ValidateDecimal(false);
                    if ((txt_UnitAmount as TextBox).ValidateDecimal(true))
                    {
                        total += decimal.Parse((txt_UnitAmount as TextBox).Text.Trim());
                        hasValue = true;
                    }

                    result = tempResult == false ? false : result;
                    AddMessageIn(tempResult, "Invalid Unit Amount. -Code: " + componentKey, Messages);

                }

                var txt_AutaxCredit = this.FindControl("txt_AutaxCredit" + componentKey);
                if (txt_AutaxCredit != null)
                {
                    tempResult = (txt_AutaxCredit as TextBox).ValidateDecimal(false);
                    if ((txt_AutaxCredit as TextBox).ValidateDecimal(true))
                    {
                        total += decimal.Parse((txt_AutaxCredit as TextBox).Text.Trim());
                        hasValue = true;
                    }
                    result = tempResult == false ? false : result;
                    AddMessageIn(tempResult, "Invalid Autax Credit. Code- " + componentKey, Messages);
                }
                var txt_TaxWithHeld = this.FindControl("txt_TaxWithHeld" + componentKey);
                if (txt_TaxWithHeld != null)
                {
                    tempResult = (txt_TaxWithHeld as TextBox).ValidateDecimal(false);
                    if ((txt_TaxWithHeld as TextBox).ValidateDecimal(true))
                    {
                        total += decimal.Parse((txt_TaxWithHeld as TextBox).Text.Trim());
                        hasValue = true;
                    }
                    result = tempResult == false ? false : result;
                    AddMessageIn(tempResult, "Invalid Tax With Held. Code- " + componentKey, Messages);
                }
                var txt_GrossDistribution = this.FindControl("txt_GrossDistribution" + componentKey);
                
                    if (txt_GrossDistribution != null&&hasValue)
                    {
                        (txt_GrossDistribution as Label).Text = total.ToString();
                        (txt_GrossDistribution as Label).ForeColor = Color.Black;
                    }
               



            }


            lblMessages.Text = Messages.ToString();

            return result;
        }

        private void AddMessageIn(bool result, string message, StringBuilder Messages)
        {
            if (!result)
            {
                Messages.Append(message + "<br/>");
            }
        }

        private Guid UpdateData()
        {
            ClientDistributionDetailsDS distributionsDetailsDs = new ClientDistributionDetailsDS();
           // distributionsDetailsDs.SecID = new Guid(TextSecID.Value);
            var id = new Guid(TextID.Value);
            if (id == Guid.Empty)
            {
                distributionsDetailsDs.ID = Guid.NewGuid();
                distributionsDetailsDs.CommandType = DatasetCommandTypes.Add;
            }
            else
            {
                double shares = 1.0;
                Double.TryParse(LblUnitsHeld.Text.Trim(),out shares);
                distributionsDetailsDs.UnitsHeld = shares; 
                distributionsDetailsDs.ID = id;
                distributionsDetailsDs.CommandType = DatasetCommandTypes.Update;
            }

            if (Broker != null)
            {
                distributionsDetailsDs.Entity = GetEntity();
                distributionsDetailsDs.Entity.ID = distributionsDetailsDs.ID;
                if (SaveData != null)
                {
                    SaveData(distributionsDetailsDs);
                }

            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            TextID.Value = distributionsDetailsDs.Entity.ID.ToString();
            return distributionsDetailsDs.ID;
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (Canceled != null)
            {
                Canceled();
            }
        }


    }
}