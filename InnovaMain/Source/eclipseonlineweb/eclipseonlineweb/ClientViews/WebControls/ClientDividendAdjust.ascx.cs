﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;

namespace eclipseonlineweb.ClientViews.WebControls
{
    public partial class ClientDividendAdjust : System.Web.UI.UserControl
    {
        public Action<Guid> Saved
        {
            get;
            set;
        }
        public Action<DataSet> SaveData
        {
            get;
            set;
        }
        public Action Canceled { get; set; }
        public Action ValidationFailed { get; set; }
        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void InitializePopupControlValues()
        {
            lblMessages.Text = string.Empty;

            lblAutaxCredit_Australian.Text =lblAutaxCredit_Foreign.Text = lblAutaxCredit_nonAssessable.Text = lblGrossDistribution_netcashDist.Text = string.Empty;
            lblTaxWithHel_Australian.Text =lblTaxWithHel_Foreign.Text =lblTaxWithHel_nonAssessable.Text = lblTaxWithHel_netcashDist.Text = string.Empty;
            lblUnitAmount_Australian.Text = lblUnitAmount_Foreign.Text = lblUnitAmount_nonAssessable.Text = lblUnitAmount_netcashDist.Text = string.Empty;
            lblGrossDistribution_Australian.Text = lblGrossDistribution_Foreign.Text = lblGrossDistribution_nonAssessable.Text = lblGrossDistribution_netcashDist.Text = string.Empty;
                foreach (var control in this.Controls)
            {

                if (control is TextBox && (control as TextBox).ID.StartsWith("txt_"))
                {
                    (control as TextBox).Text = string.Empty;
                }
                else
                    if (control is Label && (control as Label).ID.StartsWith("txt_"))
                    {
                        (control as Label).Text = string.Empty;
                    }
            }





        }

        public DividendEntity GetEntityForDS()
        {
            var dividendEntity = new DividendEntity();

            // Get current entity value and copy it to UI entity.
            ClientDividendDetailsDS ds = GetDataSet(new Guid(TextDividendEntityID.Value), new Guid(TextClientCMCIID.Value));
            if (ds.Entity != null)
                ds.Entity.CopyTo(dividendEntity);

            dividendEntity.IsManualUpdated = true;

            dividendEntity.ID = new Guid(TextDividendEntityID.Value);
            dividendEntity.InvestmentCode = TextInvestmentCode.Text;

            double centsPerShare = 0;
            double.TryParse(TextCentsPerShare.Text.Trim(), out centsPerShare);
            dividendEntity.CentsPerShare = centsPerShare;

            int unitsOnHand = 0;
            int.TryParse(TextUnitsOnHand.Text.Trim(), out unitsOnHand);
            dividendEntity.UnitsOnHand = unitsOnHand;

            DateTime date;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            if (DateTime.TryParse(TextPaymentDate.Text.Trim(), info, DateTimeStyles.None, out date))
            {
                dividendEntity.PaymentDate = date;
            }

            if (dividendEntity.Components == null)
            {
                dividendEntity.Components = new List<DividendComponent>();
            }
            bool addnew = false;
            bool hasValues = false;
            foreach (var componentKey in DividendComponent.ComponentsTypes.Keys)
            {
                addnew = false;
                hasValues = false;
                var conponent = dividendEntity.Components.Where(ss => ss.ComponentType == componentKey).FirstOrDefault();
                if (conponent == null)
                {
                    conponent = new DividendComponent() { ComponentType = componentKey };
                    addnew = true;
                }

                var txt_UnitAmount = this.FindControl("txt_UnitAmount" + componentKey);
                if (txt_UnitAmount != null)
                {
                    conponent.Unit_Amount = ((txt_UnitAmount as TextBox).Text != "") ? Convert.ToDouble((txt_UnitAmount as TextBox).Text) : (double?)null;
                    hasValues = true;
                }

                var txt_AutaxCredit = this.FindControl("txt_AutaxCredit" + componentKey);
                if (txt_AutaxCredit != null)
                {
                    conponent.Autax_Credit = ((txt_AutaxCredit as TextBox).Text != "") ? Convert.ToDouble((txt_AutaxCredit as TextBox).Text) : (double?)null;
                    hasValues = true;
                }
                var txt_TaxWithHeld = this.FindControl("txt_TaxWithHeld" + componentKey);
                if (txt_TaxWithHeld != null)
                {
                    conponent.Tax_WithHeld = ((txt_TaxWithHeld as TextBox).Text != "") ? Convert.ToDouble((txt_TaxWithHeld as TextBox).Text) : (double?)null;
                    hasValues = true;
                }
                if (addnew && hasValues)
                {

                    dividendEntity.Components.Add(conponent);
                }


            }

            return dividendEntity;
        }

        private ClientDividendDetailsDS GetDataSet(Guid entityID, Guid cid)
        {

            ClientDividendDetailsDS clientDividendDetailsDs = new ClientDividendDetailsDS();
            clientDividendDetailsDs.Cid = cid;
            clientDividendDetailsDs.ID = entityID;
            clientDividendDetailsDs.CommandType = DatasetCommandTypes.Get;

            if (Broker != null)
            {
                IBrokerManagedComponent clientData = this.Broker.GetBMCInstance(cid);

                clientData.GetData(clientDividendDetailsDs);

                Broker.ReleaseBrokerManagedComponent(clientData);


            }
            else
            {
                throw new Exception("Broker Not Found");
            }



            return clientDividendDetailsDs;

        }

        public void PopulatePopupControls(Guid dividendEntityID, Guid clientCMCIID, string clientId, string clientName, bool isAdmin, Guid clientCLID)
        {
            InitializePopupControlValues();
            TextClientCMCIID.Value = clientCMCIID.ToString();

            btnUpdate.Enabled = isAdmin;
            if (dividendEntityID != Guid.Empty)
            {
                ClientDividendDetailsDS ds = GetDataSet(dividendEntityID, clientCMCIID);
                var Entity = ds.Entity;
                TextDividendEntityID.Value = dividendEntityID.ToString();
                TextCentsPerShare.Text = Math.Round(Entity.CentsPerShare, 4).ToString();
                TextInvestmentCode.Text = Entity.InvestmentCode;
                TextUnitsOnHand.Text = Entity.UnitsOnHand.ToString();
                this.TextDividendType.Text = Entity.Dividendtype.ToString();

                if (Entity.PaymentDate.HasValue)
                {
                    calPaymentDate.SelectedDate = Entity.PaymentDate.Value;
                    TextPaymentDate.Text = Entity.PaymentDate.Value.ToString("dd/MM/yyyy");
                }

                if (Entity.BalanceDate.HasValue)
                {
                    this.TextBalanceDate.Text = Entity.BalanceDate.Value.ToString("dd/MM/yyyy");
                }

                if (Entity.BooksCloseDate.HasValue)
                {
                    this.TextBooksCloseDate.Text = Entity.BooksCloseDate.Value.ToString("dd/MM/yyyy");
                }

                if (Entity.RecordDate.HasValue)
                {
                    this.TextExDividendDate.Text = Entity.RecordDate.Value.ToString("dd/MM/yyyy");
                }

                if (Entity.Components != null && Entity.Components.Count != 0)
                {
                    foreach (var component in Entity.Components)
                    {

                        bool hasValue = false;
                        var unitamount = this.FindControl("txt_UnitAmount" + component.ComponentType);
                        if (unitamount != null)
                        {
                            if (component.Unit_Amount.HasValue)
                            {
                                (unitamount as TextBox).Text = string.Format("{0:0.00}", component.Unit_Amount.Value);
                                hasValue = true;
                            }
                            else
                            {
                                (unitamount as TextBox).Text = "";
                            }
                        }

                        var txt_AutaxCredit = this.FindControl("txt_AutaxCredit" + component.ComponentType);
                        if (txt_AutaxCredit != null)
                        {
                            if (component.Autax_Credit.HasValue)
                            {
                                (txt_AutaxCredit as TextBox).Text =  string.Format("{0:0.00}", component.Autax_Credit.Value);
                                hasValue = true;
                            }
                            else
                            {
                                (txt_AutaxCredit as TextBox).Text = "";
                            }
                        }
                        var txt_TaxWithHeld = this.FindControl("txt_TaxWithHeld" + component.ComponentType);
                        if (txt_TaxWithHeld != null)
                        {
                            if (component.Tax_WithHeld.HasValue)
                            {
                                (txt_TaxWithHeld as TextBox).Text =  string.Format("{0:0.00}", component.Tax_WithHeld.Value);
                                hasValue = true;
                            }
                            else
                            {
                                (txt_TaxWithHeld as TextBox).Text = "";
                            }
                        }

                        var txt_GrossDistribution = this.FindControl("txt_GrossDistribution" + component.ComponentType);
                        if (txt_GrossDistribution != null)
                        {
                            if (hasValue)
                            {
                                (txt_GrossDistribution as Label).Text = component.GrossDistribution.FormatToCurrency();
                                hasValue = true;
                            }
                            else
                            {
                                (txt_GrossDistribution as Label).Text = "";
                            }
                        }




                    }

                    lblAutaxCredit_Australian.Text = Entity.Components.Where(ss => ss.Category == ComponentCategory.Australian_Income).Sum(ss => ss.Autax_Credit).FormatToCurrency();
                    lblAutaxCredit_Foreign.Text = Entity.Components.Where(ss => ss.Category == ComponentCategory.Foreign_Income).Sum(ss => ss.Autax_Credit).FormatToCurrency();
                    lblAutaxCredit_nonAssessable.Text = Entity.Components.Where(ss => ss.Category == ComponentCategory.Other_Non_Assessable_Amount).Sum(ss => ss.Autax_Credit).FormatToCurrency();
                    lblGrossDistribution_netcashDist.Text = Entity.Components.Sum(ss => ss.Autax_Credit).FormatToCurrency();


                    lblTaxWithHel_Australian.Text = Entity.Components.Where(ss => ss.Category == ComponentCategory.Australian_Income).Sum(ss => ss.Tax_WithHeld).FormatToCurrency();
                    lblTaxWithHel_Foreign.Text = Entity.Components.Where(ss => ss.Category == ComponentCategory.Foreign_Income).Sum(ss => ss.Tax_WithHeld).FormatToCurrency();
                    lblTaxWithHel_nonAssessable.Text = Entity.Components.Where(ss => ss.Category == ComponentCategory.Other_Non_Assessable_Amount).Sum(ss => ss.Tax_WithHeld).FormatToCurrency();
                    lblTaxWithHel_netcashDist.Text = Entity.Components.Sum(ss => ss.Tax_WithHeld).FormatToCurrency();

                    lblUnitAmount_Australian.Text = Entity.Components.Where(ss => ss.Category == ComponentCategory.Australian_Income).Sum(ss => ss.Unit_Amount).FormatToCurrency();
                    lblUnitAmount_Foreign.Text = Entity.Components.Where(ss => ss.Category == ComponentCategory.Foreign_Income).Sum(ss => ss.Unit_Amount).FormatToCurrency();
                    lblUnitAmount_nonAssessable.Text = Entity.Components.Where(ss => ss.Category == ComponentCategory.Other_Non_Assessable_Amount).Sum(ss => ss.Unit_Amount).FormatToCurrency();
                    lblUnitAmount_netcashDist.Text = Entity.Components.Sum(ss => ss.Unit_Amount).FormatToCurrency();

                    lblGrossDistribution_Australian.Text = Entity.Components.Where(ss => ss.Category == ComponentCategory.Australian_Income).Sum(ss => ss.GrossDistribution).FormatToCurrency();
                    lblGrossDistribution_Foreign.Text = Entity.Components.Where(ss => ss.Category == ComponentCategory.Foreign_Income).Sum(ss => ss.GrossDistribution).FormatToCurrency();
                    lblGrossDistribution_nonAssessable.Text = Entity.Components.Where(ss => ss.Category == ComponentCategory.Other_Non_Assessable_Amount).Sum(ss => ss.GrossDistribution).FormatToCurrency();
                    lblGrossDistribution_netcashDist.Text = Entity.Components.Sum(ss => ss.GrossDistribution).FormatToCurrency();
                }
                else
                {
                    // tax compnents not manually updated before.
                    InitializeComponentsValues(Entity);
                }


            }
        }

        private void InitializeComponentsValues(DividendEntity Entity)
        {
            // Australia Income -> Dividend Payment
            this.txt_UnitAmount871.Text = Entity.DomesticInterestAmount.FormatEditable();
            this.txt_UnitAmount880.Text =  Entity.FrankedAmount.FormatEditable();
            this.txt_UnitAmount881.Text =  Entity.UnfrankedAmount.FormatEditable();
            this.txt_UnitAmount883.Text =  Entity.DomesticOtherIncome.FormatEditable();

            // Australia Income -> Tax Withheld
            this.txt_TaxWithHeld883.Text = Entity.DomesticWithHoldingTax.FormatEditable();

            // Australia Income -> Tax Credit
            this.txt_AutaxCredit880.Text = (Entity.TotalFrankingCredit + Entity.DomesticImputationTaxCredits).FormatEditable();

            // Australia Income Total -> Dividend Payment
            lblUnitAmount_Australian.Text = Math.Round(Entity.DomesticInterestAmount + Entity.FrankedAmount + Entity.UnfrankedAmount + Entity.DomesticOtherIncome, 2).FormatToCurrency();
            // Australia Income Total -> Tax Credit
            lblAutaxCredit_Australian.Text = Math.Round(Entity.TotalFrankingCredit + Entity.DomesticImputationTaxCredits, 2).FormatToCurrency();
            // Australia Income Total -> Tax Withheld
            lblTaxWithHel_Australian.Text = Entity.DomesticWithHoldingTax.FormatToCurrency();

            // Australia Income Gross
            this.txt_GrossDistribution871.Text = Entity.DomesticInterestAmount.FormatToCurrency();
            this.txt_GrossDistribution880.Text = (Entity.FrankedAmount + Entity.TotalFrankingCredit + Entity.DomesticImputationTaxCredits).FormatToCurrency();
            this.txt_GrossDistribution881.Text = Entity.UnfrankedAmount.FormatToCurrency();
            this.txt_GrossDistribution883.Text = (Entity.DomesticOtherIncome + Entity.DomesticWithHoldingTax).FormatToCurrency();
            this.lblGrossDistribution_Australian.Text = (Entity.DomesticInterestAmount +
            Entity.FrankedAmount + Entity.TotalFrankingCredit + Entity.DomesticImputationTaxCredits+
            Entity.UnfrankedAmount +
            Entity.DomesticOtherIncome + Entity.DomesticWithHoldingTax).FormatToCurrency();

            // Foreign Income
            this.txt_UnitAmount885.Text =  Entity.ForeignInterestAmount.FormatEditable();
            this.txt_UnitAmount886.Text =  Entity.ForeignOtherIncome.FormatEditable();

            // Foreign income - Dividend Payment
            lblUnitAmount_Foreign.Text = Math.Round(Entity.ForeignInterestAmount + Entity.ForeignOtherIncome, 2).FormatToCurrency();

            // Forgien income - tax credit
            this.txt_AutaxCredit884.Text =  Entity.ForeignImputationTaxCredits.FormatEditable();
            lblAutaxCredit_Foreign.Text = Math.Round(Entity.ForeignImputationTaxCredits, 2).FormatToCurrency();

            // Foreign Income -> Tax Withheld
            this.txt_TaxWithHeld872.Text = Entity.ForeignWithHoldingTax.FormatEditable();

            // Foreign Income Gross
            this.txt_GrossDistribution872.Text = Entity.ForeignWithHoldingTax.FormatToCurrency();
            this.txt_GrossDistribution885.Text = Entity.ForeignInterestAmount.FormatToCurrency();
            this.txt_GrossDistribution886.Text = Entity.ForeignOtherIncome.FormatToCurrency();
            this.txt_GrossDistribution884.Text = Entity.ForeignImputationTaxCredits.FormatToCurrency();

            this.lblGrossDistribution_Foreign.Text = (Entity.ForeignInterestAmount +
                Entity.ForeignOtherIncome +
                Entity.ForeignImputationTaxCredits +
                Entity.ForeignWithHoldingTax).FormatToCurrency();


            // Capital Gains
            this.txt_UnitAmount874.Text =  (Entity.DomesticIndexationCGT + Entity.ForeignIndexationCGT).FormatEditable();
            this.txt_UnitAmount875.Text =  (Entity.DomesticOtherCGT + Entity.ForeignOtherCGT).FormatEditable();
            this.txt_UnitAmount876.Text =  (Entity.DomesticDiscountedCGT + Entity.ForeignDiscountedCGT).FormatEditable();

            lblUnitAmount_CapitalGains.Text = Math.Round(Entity.DomesticIndexationCGT + Entity.ForeignIndexationCGT +
                Entity.DomesticOtherCGT + Entity.ForeignOtherCGT +
                Entity.DomesticDiscountedCGT + Entity.ForeignDiscountedCGT
                , 2).FormatToCurrency();

            // Capital Gains Gross
            this.txt_GrossDistribution874.Text = (Entity.DomesticIndexationCGT + Entity.ForeignIndexationCGT).FormatToCurrency();
            this.txt_GrossDistribution875.Text = (Entity.DomesticOtherCGT + Entity.ForeignOtherCGT).FormatToCurrency();
            this.txt_GrossDistribution876.Text = (Entity.DomesticDiscountedCGT + Entity.ForeignDiscountedCGT).FormatToCurrency();

            this.lblGrossDistribution_CapitalGains.Text = (Entity.DomesticIndexationCGT + Entity.ForeignIndexationCGT +
                Entity.DomesticOtherCGT + Entity.ForeignOtherCGT + 
                Entity.DomesticDiscountedCGT + Entity.ForeignDiscountedCGT).FormatToCurrency();


            // Other Non-Assessable Amount
            this.txt_UnitAmount888.Text =  Entity.TaxDeferred.FormatEditable();
            this.txt_UnitAmount889.Text =  Entity.TaxFree.FormatEditable();
            this.txt_UnitAmount900.Text =  Entity.ReturnOfCapital.FormatEditable();

            lblUnitAmount_nonAssessable.Text = Math.Round(Entity.TaxDeferred + Entity.TaxFree + Entity.ReturnOfCapital, 2).FormatToCurrency();

            // Other Non-Assessable Gross
            this.txt_GrossDistribution888.Text = Entity.TaxDeferred.FormatToCurrency();
            this.txt_GrossDistribution889.Text = Entity.TaxFree.FormatToCurrency();
            this.txt_GrossDistribution900.Text = Entity.ReturnOfCapital.FormatToCurrency();

            this.lblGrossDistribution_nonAssessable.Text = (Entity.TaxDeferred +
                Entity.TaxFree +
            Entity.ReturnOfCapital).FormatToCurrency();

            // column total
            double dividendPaymentColumnTotal =  Entity.DomesticInterestAmount + Entity.FrankedAmount + Entity.UnfrankedAmount + Entity.DomesticOtherIncome +
                Entity.ForeignInterestAmount + Entity.ForeignOtherIncome +
                Entity.DomesticIndexationCGT + Entity.ForeignIndexationCGT +
                Entity.DomesticOtherCGT + Entity.ForeignOtherCGT +
                Entity.DomesticDiscountedCGT + Entity.ForeignDiscountedCGT +
                Entity.TaxDeferred + Entity.TaxFree + Entity.ReturnOfCapital;
            lblUnitAmount_netcashDist.Text = Math.Round(dividendPaymentColumnTotal, 2).FormatToCurrency();

            double auTaxCreditColumnTotal = Entity.TotalFrankingCredit + Entity.DomesticImputationTaxCredits + Entity.ForeignImputationTaxCredits;
            lblAutaxCredit_netcashDist.Text = Math.Round(auTaxCreditColumnTotal, 2).FormatToCurrency();

            double taxHeldColumnTotal = Entity.DomesticWithHoldingTax + Entity.ForeignWithHoldingTax;
            lblTaxWithHel_netcashDist.Text = Math.Round(taxHeldColumnTotal, 2).FormatToCurrency();

            lblGrossDistribution_netcashDist.Text = Math.Round(dividendPaymentColumnTotal + auTaxCreditColumnTotal + taxHeldColumnTotal, 2).FormatToCurrency();
        }


        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            if (Validate() == false)
            {
                if (ValidationFailed != null)
                    ValidationFailed();
                return;
            }

            var id = UpdateData();
            if (Saved != null)
            {

                Saved(id);
            }
        }



        public bool Validate()
        {
            bool result = true;
            bool tempResult = false;
            lblMessages.Text = "";
            StringBuilder Messages = new StringBuilder();

            DateTime date;

            // payment date
            tempResult = TextPaymentDate.ValidateDate(true, "dd/MM/yyyy", out date);
            if (tempResult)
            {
                calPaymentDate.SelectedDate = date;
            }
            else
            {
                calPaymentDate.SelectedDate = null;
            }
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Payment Date*", Messages);

            //// balance date
            //tempResult = this.TextBalanceDate.ValidateDate(true, "dd/MM/yyyy", out date);
            //if (tempResult)
            //{
            //    this.calBalanceDate.SelectedDate = date;
            //}
            //else
            //{
            //    calBalanceDate.SelectedDate = null;
            //}
            //result = tempResult == false ? false : result;
            //AddMessageIn(tempResult, "Invalid Balance Date*", Messages);

            //// Ex-Dividend Date
            //tempResult = this.TextExDividendDate.ValidateDate(true, "dd/MM/yyyy", out date);
            //if (tempResult)
            //{
            //    this.calExDividendDate.SelectedDate = date;
            //}
            //else
            //{
            //    calExDividendDate.SelectedDate = null;
            //}
            //result = tempResult == false ? false : result;
            //AddMessageIn(tempResult, "Invalid Ex-Dividend Date*", Messages);


            //// books close Date
            //tempResult = this.TextBooksCloseDate.ValidateDate(true, "dd/MM/yyyy", out date);
            //if (tempResult)
            //{
            //    this.calBooksCloseDate.SelectedDate = date;
            //}
            //else
            //{
            //    calBooksCloseDate.SelectedDate = null;
            //}
            //result = tempResult == false ? false : result;
            //AddMessageIn(tempResult, "Invalid Books close Date*", Messages);


            decimal total = 0;
            bool hasValue = false;
            foreach (var componentKey in DistributionComponent.ComponentsTypes.Keys)
            {

                total = 0;
                hasValue = false;
                var txt_UnitAmount = this.FindControl("txt_UnitAmount" + componentKey);
                if (txt_UnitAmount != null)
                {
                    tempResult = (txt_UnitAmount as TextBox).ValidateDecimal(false);
                    if ((txt_UnitAmount as TextBox).ValidateDecimal(true))
                    {
                        total += decimal.Parse((txt_UnitAmount as TextBox).Text.Trim());
                        hasValue = true;
                    }

                    result = tempResult == false ? false : result;
                    AddMessageIn(tempResult, "Invalid Unit Amount.", Messages);

                }

                var txt_AutaxCredit = this.FindControl("txt_AutaxCredit" + componentKey);
                if (txt_AutaxCredit != null)
                {
                    tempResult = (txt_AutaxCredit as TextBox).ValidateDecimal(false);
                    if ((txt_AutaxCredit as TextBox).ValidateDecimal(true))
                    {
                        total += decimal.Parse((txt_AutaxCredit as TextBox).Text.Trim());
                        hasValue = true;
                    }
                    result = tempResult == false ? false : result;
                    AddMessageIn(tempResult, "Invalid Autax Credit.", Messages);
                }
                var txt_TaxWithHeld = this.FindControl("txt_TaxWithHeld" + componentKey);
                if (txt_TaxWithHeld != null)
                {
                    tempResult = (txt_TaxWithHeld as TextBox).ValidateDecimal(false);
                    if ((txt_TaxWithHeld as TextBox).ValidateDecimal(true))
                    {
                        total += decimal.Parse((txt_TaxWithHeld as TextBox).Text.Trim());
                        hasValue = true;
                    }
                    result = tempResult == false ? false : result;
                    AddMessageIn(tempResult, "Invalid Tax With Held.", Messages);
                }
                var txt_GrossDistribution = this.FindControl("txt_GrossDistribution" + componentKey);

                if (txt_GrossDistribution != null && hasValue)
                {
                    (txt_GrossDistribution as Label).Text = total.ToString();
                    (txt_GrossDistribution as Label).ForeColor = Color.Black;
                }




            }


            lblMessages.Text = Messages.ToString();

            return result;
        }

        private void AddMessageIn(bool result, string message, StringBuilder Messages)
        {
            if (!result)
            {
                Messages.Append(message + "<br/>");
            }
        }

        private Guid UpdateData()
        {
            ClientDividendDetailsDS dividendDetailsDs = new ClientDividendDetailsDS();
            var dividendEntityId = new Guid(TextDividendEntityID.Value);
            if (dividendEntityId == Guid.Empty)
            {
                dividendDetailsDs.ID = Guid.NewGuid();
                dividendDetailsDs.CommandType = DatasetCommandTypes.Add;
            }
            else
            {
                double shares = 1.0;
                dividendDetailsDs.ID = dividendEntityId;
                dividendDetailsDs.CommandType = DatasetCommandTypes.Update;
            }

            if (Broker != null)
            {
                dividendDetailsDs.Entity = GetEntityForDS();
                dividendDetailsDs.Entity.ID = dividendDetailsDs.ID;
                if (SaveData != null)
                {
                    SaveData(dividendDetailsDs);
                }

            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            return dividendDetailsDs.ID;
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (Canceled != null)
            {
                Canceled();
            }
        }


    }
}