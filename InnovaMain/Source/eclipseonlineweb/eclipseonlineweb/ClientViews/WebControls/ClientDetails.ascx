﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientDetails.ascx.cs"
    Inherits="eclipseonlineweb.ClientViews.WebControls.ClientDetails" %>
<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" Assembly="C1.Web.Wijmo.Controls.4, Version=4.0.20121.56, Culture=neutral, PublicKeyToken=9b75583953471eea" %>
<table width="100%">
    <colgroup>
        <col style="width: 10%" />
        <col style="width: 35%" />
        <col style="width: 15%" />
        <col style="width: 40%" />
    </colgroup>
    <tr>
        <td>
            Client Name</
        </td>
        <td>
            <asp:Label runat="server" ID="lblClientName" Width="300"></asp:Label>
        </td>
        <td>
            Investor Type
        </td>
        <td>
            <c1:C1ComboBox ID="ComboOrganizationType" runat="server" ViewStateMode="Enabled"
                Width="300">
                <Items>
                    <c1:C1ComboBoxItem Text="SMSF – Individual Trustees" Value="ClientSMSFIndividualTrustee" />
                    <c1:C1ComboBoxItem Text="SMSF – Corporate Trustee" Value="ClientSMSFCorporateTrustee" />
                    <c1:C1ComboBoxItem Text="Other Trusts - Individual" Value="ClientOtherTrustsIndividual" />
                    <c1:C1ComboBoxItem Text="Other Trusts - Corporate" Value="ClientOtherTrustsCorporate" />
                    <c1:C1ComboBoxItem Text="Individuals Client" Value="ClientIndividual" />
                    <c1:C1ComboBoxItem Text="Corporation - Public" Value="ClientCorporationPublic" />
                    <c1:C1ComboBoxItem Text="Corporation - Private" Value="ClientCorporationPrivate" />
                    <c1:C1ComboBoxItem Text="Sole Trader" Value="ClientSoleTrader" />
                    <c1:C1ComboBoxItem Text="e-Clipse Super" Value="ClientEClipseSuper" />
                </Items>
            </c1:C1ComboBox>
        </td>
    </tr>
    <tr>
        <td>
            Status Flag
        </td>
        <td>
            <c1:C1ComboBox ID="ComboStatusFlag" Height="25" Width="150" runat="server" ViewStateMode="Enabled">
            </c1:C1ComboBox>
            <asp:CheckBox ID="CheckIsActive" Text="Is Client" Height="16" Checked="true" runat="server"
                Style="margin: 1em;" />
        </td>
        <td>
            Client ID
        </td>
        <td>
            <asp:Label runat="server" ID="TextClientID" Width="150"></asp:Label>
            <asp:CheckBox ID="chkDOITYOURSELF" Text="Do it yourself" Height="16" runat="server"
                Style="margin: 1em;" />
            <asp:CheckBox ID="chkDOITFORME" Text="Do it for me" Height="16" runat="server" Style="margin: 1em;" />
            <asp:CheckBox ID="chkDOITWITHME" Text="Do it with me" Height="16" runat="server"
                Style="margin: 1em;" />
        </td>
    </tr>
</table>
