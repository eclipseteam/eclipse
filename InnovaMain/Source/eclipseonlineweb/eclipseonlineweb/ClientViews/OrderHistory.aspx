﻿<%@ Page Title="e-Clipse Online Portal - Configure Fees" Language="C#" MasterPageFile="ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="OrderHistory.aspx.cs" Inherits="eclipseonlineweb.OrderHistory" %>

<%@ Register Src="../Controls/OrderHistory.ascx" TagName="OrderHistory" TagPrefix="uc1" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <uc1:OrderHistory ID="OrderHistoryControl" runat="server" />
</asp:Content>
