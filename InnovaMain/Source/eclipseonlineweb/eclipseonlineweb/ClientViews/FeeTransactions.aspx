﻿<%@ Page Title="e-Clipse Online Portal - Configure Fees" Language="C#" MasterPageFile="ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="FeeTransactions.aspx.cs" Inherits="eclipseonlineweb.FeeTransactions" %>

<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function GoToPage(url) {
            window.location = url + '?ins=<%:cid %>';
            return false;
        }
    </script>
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="10%">
                    </td>
                    <td width="10%">
                    </td>
                    <td style="width: 30%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
                OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
                OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
                OnItemDataBound="PresentationGrid_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="CalculatedFees" TableLayout="Fixed" EditMode="EditForms">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <Columns>
                        <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ID" UniqueName="ID" />
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="DESCRIPTION"
                            ReadOnly="true" HeaderStyle-Width="30%" ItemStyle-Width="30%" HeaderText="Description"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="DESCRIPTION" UniqueName="DESCRIPTION">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="MONTH" HeaderStyle-Width="10%"
                            ItemStyle-Width="10%" HeaderText="Month" ReadOnly="true" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="MONTH" UniqueName="MONTH">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80px" ReadOnly="true" DefaultInsertValue="2000"
                            SortExpression="YEAR" HeaderStyle-Width="10%" ShowFilterIcon="false" ItemStyle-Width="10%"
                            HeaderText="Year" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            HeaderButtonType="TextButton" DataField="YEAR" UniqueName="YEAR">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" ReadOnly="true" DefaultInsertValue="2000"
                            SortExpression="TRANSACTIONDATE" HeaderStyle-Width="20%" ItemStyle-Width="15%"
                            HeaderText="Transaction Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TRANSACTIONDATE"
                            UniqueName="TRANSACTIONDATE">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" ReadOnly="true" DefaultInsertValue="2000"
                            SortExpression="TRANSACTIONTYPE" HeaderStyle-Width="20%" ItemStyle-Width="15%"
                            HeaderText="Transaction Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TRANSACTIONTYPE"
                            UniqueName="TRANSACTIONTYPE">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" FooterAggregateFormatString="{0:C}"
                            Aggregate="Sum" ReadOnly="true" DefaultInsertValue="2000" SortExpression="CALCULATEDFEES"
                            HeaderStyle-Width="20%" ItemStyle-Width="15%" HeaderText="Calculated Fee" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" DataFormatString="{0:c}" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="CALCULATEDFEES" UniqueName="CALCULATEDFEES">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn UniqueName="FeeTemplateDetails" FilterControlWidth="5%"
                            ShowFilterIcon="false">
                            <HeaderStyle Width="7%" />
                            <ItemStyle Width="7%" />
                            <ItemTemplate>
                                <asp:LinkButton ID="Definition" runat="server" Text="Details" CommandArgument='<%# Eval("ID") + "," + Eval("INS") %>'
                                    CommandName="Detail"></asp:LinkButton>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridButtonColumn ConfirmText="Delete these details record?" ButtonType="ImageButton"
                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
