﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using C1.Web.Wijmo.Controls.C1ComboBox;
using Telerik.Web.UI;


namespace Oritax.TaxSimp.Extensions
{
    public static class ValidateInput
    {
        public static bool ValidateNumeric(this ITextControl text, int minLength)
        {
            bool result = new Regex(@"^\d{" + minLength + ",}$").Match(text.Text).Success;
            return result;
        }


        public static bool ValidateNumeric(this ITextControl text, int minLength, int maxLength)
        {

            bool result = new Regex(@"^\d{" + minLength + ",}$").Match(text.Text).Success;
            return result;
        }
        
        public static bool ValidateNumeric(this TextBox text, int minLength, int maxLength)
        {
            text.MaxLength = maxLength;
            bool result = new Regex(@"^\d{" + minLength + "," + maxLength + "}$").Match(text.Text).Success;
            return result;
        }

        public static bool ValidateNumeric(this TextBox text, bool Mandatory, int minLength, int maxLength)
        {
            text.MaxLength = maxLength;
            bool result = new Regex(@"^\d{" + minLength + "," + maxLength + "}$").Match(text.Text).Success;
            if (Mandatory)
            {
                return result;
            }
            else
            {
                if (text.Text.Length > 0)
                {

                    return result;
                }
                else
                    return true;
            }
        }
        
        public static bool ValidateCharacters(this ITextControl text, int minLength)
        {
            bool result = new Regex(@"^\D{" + minLength + ",}$").Match(text.Text).Success;
            return result;
        }
        
        public static bool ValidateDecimal(this ITextControl text, bool mandatory)
        {
            if (!mandatory & text.Text.Length == 0)
            {
                return true;
            }
            decimal temp;
            bool result = decimal.TryParse(text.Text, out temp);
            return result;

        }

        public static bool ValidateDecimalMin(this ITextControl text, bool mandatory, decimal min)
        {
            if (!mandatory & text.Text.Length == 0)
            {
                return true;
            }
            decimal temp;
            bool result = decimal.TryParse(text.Text, out temp);

            if(result)
            {
                result = temp >= min;
            }

            return result;

        }
        
        public static bool ValidateDecimalMax(this ITextControl text, bool mandatory, decimal max)
        {
            if (!mandatory & text.Text.Length == 0)
            {
                return true;
            }
            decimal temp;
            bool result = decimal.TryParse(text.Text, out temp);

            if (result)
            {
                result = temp <= max;
            }

            return result;

        }
        
        public static bool ValidateDecimalMinMax(this ITextControl text, bool mandatory, decimal min, decimal max)
        {
            if (!mandatory & text.Text.Length == 0)
            {
                return true;
            }
            decimal temp;
            bool result = decimal.TryParse(text.Text, out temp);

            if (result)
            {
                result = temp>=min && temp <= max;
            }

            return result;

        }

        public static bool ValidateCharacters(this ITextControl text, int minLength, int maxLength)
        {
            
            bool result = new Regex(@"^\D{" + minLength + "," + maxLength + "}$").Match(text.Text).Success;

            return result;
        }

        public static bool ValidateCharacters(this TextBox text, int minLength, int maxLength)
        {
            text.MaxLength = maxLength;
            bool result = new Regex(@"^\D{" + minLength + "," + maxLength + "}$").Match(text.Text).Success;

            return result;
        }

        public static bool ValidateAlphaNumeric(this ITextControl text, int minLength, int maxLength)
        {
          
            if (text.Text.Trim().Length < minLength || text.Text.Trim().Length > maxLength)
            {

                return false;
            }
            else
                return true;
        }

        public static bool ValidateAlphaNumeric(this ITextControl text, bool Mandatory)
        {
            
            if (text.Text == "" && Mandatory == true)
            {

                return false;
            }
            else
            {
                return true;
            }
        }
        
        public static bool ValidateAlphaNumeric(this TextBox text, int minLength, int maxLength)
        {
            text.MaxLength = maxLength;
            if (text.Text.Trim().Length < minLength || text.Text.Trim().Length > maxLength)
            {

                return false;
            }
            else
                return true;
        }

        public static bool ValidateAlphaNumeric(this TextBox text, bool Mandatory, int minLength, int maxLength)
        {
            text.MaxLength = maxLength;
            if (Mandatory)
            {
                if (text.Text.Trim().Length < minLength || text.Text.Trim().Length > maxLength)
                {

                    return false;
                }
                else
                    return true;
            }
            else
            {
                if (text.Text.Length > 0)
                {
                    if (text.Text.Trim().Length < minLength || text.Text.Trim().Length > maxLength)
                    {

                        return false;
                    }
                    else
                        return true;
                }
                else
                    return true;
            }


        }

        public static bool ValidateAlphaNumeric(this TextBox text, bool Mandatory)
        {
            if (text.Text.Trim() == "" && Mandatory == true)
            {

                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool ValidateEmail(this ITextControl text, bool Mandotory)
        {
            //text.MaxLength = 50;
            if (text.Text.Trim() == "" && Mandotory == true)
            {

                return false;
            }
            else if (text.Text.Trim() != "")
            {
                string pattern = @"^((?>[a-zA-Z\d!#$%&'*+\-/=?^_`{|}~]+\x20*|""((?=[\x01-\x7f])[^""\\]|\\[\x01-\x7f])*""\x20*)*(?<angle><))?((?!\.)(?>\.?[a-zA-Z\d!#$%&'*+\-/=?^_`{|}~]+)+|""((?=[\x01-\x7f])[^""\\]|\\[\x01-\x7f])*"")@(((?!-)[a-zA-Z\d\-]+(?<!-)\.)+[a-zA-Z]{2,}|\[(((?(?<!\[)\.)(25[0-5]|2[0-4]\d|[01]?\d?\d)){4}|[a-zA-Z\d\-]*[a-zA-Z\d]:((?=[\x01-\x7f])[^\\\[\]]|\\[\x01-\x7f])+)\])(?(angle)>)$";

                bool result = new Regex(pattern).Match(text.Text).Success;
                if (result)
                    return true;

                return false;
            }

            return true;
        }
        
        public static bool ValidateDate(this ITextControl Date, bool Mandotory, string format, out DateTime date)
        {
            date = DateTime.MinValue;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            if (!string.IsNullOrEmpty(format))
                info.ShortDatePattern = format;
            if (Date.Text.Trim() == "" && Mandotory)
            {

                return false;
            }
            if (Date.Text.Trim() != "" && DateTime.TryParse(Date.Text.Trim(), info, DateTimeStyles.None, out date))
            {
                return true;
            }

            return false;
        }
        
        public static bool ValidateCombo(this DropDownList combo, bool Mandotory)
        {
            if (combo.Items.Count > 0 && combo.SelectedIndex == -1)
            {

                return false;
            }
            else
            {
                return true;
            }
        }
        public static bool ValidateCombo(this RadComboBox combo, bool Mandotory)
        {
            if (combo.Items.Count > 0 && combo.SelectedIndex == -1)
            {

                return false;
            }
            else
            {
                return true;
            }
        }
        public static bool ValidateCombo(this ComboBox combo, bool Mandotory)
        {
            if (combo.Items.Count > 0 && combo.SelectedIndex == -1)
            {

                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool ValidateCombo(this C1ComboBox combo, bool Mandotory)
        {
            if (combo.Items.Count == 0 || combo.SelectedIndex == -1)
            {

                return false;
            }
            else
            {
                return true;
            }
        }
        public static bool RadValidateCombo(this RadComboBox combo, bool Mandotory)
        {
            if (combo.Items.Count == 0 || combo.SelectedIndex == -1)
            {

                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
