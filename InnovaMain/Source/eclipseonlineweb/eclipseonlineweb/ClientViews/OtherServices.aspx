﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="OtherServices.aspx.cs" Inherits="eclipseonlineweb.ClientViews.OtherServices" %>

<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" Assembly="C1.Web.Wijmo.Controls.4, Version=4.0.20121.56, Culture=neutral, PublicKeyToken=9b75583953471eea" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnSave" AlternateText="Save Changes" ToolTip="Save Changes"
                                OnClick="btnSave_OnClick" runat="server" ImageUrl="~/images/Save-Icon.png" />
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <div id="MainView" style="background-color: white">
            <asp:Panel ID="pnlOtherServers" runat="server">
                <table style="width: 99%; height: 205px">
                    <tr>
                        <td style="vertical-align: top">
                            <table>
                                <tr>
                                    <td style="font-weight: bold">
                                        <asp:Label ID="Label4" runat="server" Text="Select Access Facilities"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 5px">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkPhoneAccess" Text="Phone Access" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkOnlineAccess" Text="Online Access" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkDebitCard" Text="Debit Card" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkChequeBook" Text="Cheque Book (25 per book)" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkDepositeBook" Text="Deposit Book" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="vertical-align: top">
                            <table>
                                <tr>
                                    <td style="font-weight: bold">
                                        <asp:Label ID="Label5" runat="server" Text="Select Manner of Operation"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 5px">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkMOOne" Text="Any one of us to sign" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkMOTwo" Text="Any two of us to sign" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkMOAll" Text="All of us to sign" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="vertical-align: top">
                            <table>
                                <tr>
                                    <td style="font-weight: bold">
                                        <asp:Label ID="Label6" runat="server" Text="Select Exemption Category"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 5px">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkECOne" Text="Category 1" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkECTwo" Text="Category 2" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkECThree" Text="Category 3" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="vertical-align: top">
                            <table>
                                <tr>
                                    <td style="font-weight: bold">
                                        <asp:Label ID="Label7" runat="server" Text="BT Wrap"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 5px">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="Client Code:"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtAccountNumber" runat="server" CssClass="Txt-Field"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="BT Wrap Client Code:"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtAccountDescription" runat="server" CssClass="Txt-Field"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="vertical-align: top">
                            <table>
                                <tr>
                                    <td style="font-weight: bold">
                                        <asp:Label ID="Label8" runat="server" Text="Business Group"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 5px">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text="Select Business Group:"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <c1:C1ComboBox ID="cmbBusinessGroup" runat="server">
                                        </c1:C1ComboBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                    </tr>
                </table>

                </asp:Panel>

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
