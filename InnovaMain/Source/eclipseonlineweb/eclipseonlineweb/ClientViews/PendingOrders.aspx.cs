﻿using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class PendingOrders : UMABasePage
    {
        protected override void Intialise()
        {
            PendingOrdersControl.SaveData += SaveData;
        }

        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth" || CheckAdviserForOrderPad(objUser))
                return true;
            return objUser.UserType == UserType.Innova || objUser.Name == "Administrator";
        }

        public override void LoadPage()
        {
            if (!IsPostBack)
            {
                cid = Request.QueryString["ins"];
                var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
                PendingOrdersControl.IsAdmin = objUser.Administrator;
            }

        }
    }
}
