﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="BGLCOAMap.aspx.cs" Inherits="eclipseonlineweb.ClientViews.BGLCOAMap" %>

<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <div id="MainView" style="background-color: white">
                <fieldset>
                    <legend title="BGL MAP">BGL CHART OF ACCOUNTS MAP</legend>
                    <telerik:RadGrid ID="PresentationGrid" OnNeedDataSource="PresentationGrid_OnNeedDataSource"
                        runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="500"
                        AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
                        OnItemCommand="PresentationGrid_OnItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                        AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                        OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
                        OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
                        EnableViewState="true" ShowFooter="true" OnItemDataBound="PresentationGrid_ItemDataBound">
                        <PagerStyle Mode="NumericPages"></PagerStyle>
                        <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                            EditMode="EditForms" Name="DIVMAP" TableLayout="Fixed" Font-Size="7.5">
                            <GroupByExpressions>
                                <telerik:GridGroupByExpression>
                                    <SelectFields>
                                        <telerik:GridGroupByField FieldAlias="COA" FieldName="InvestmentAccFilterType"
                                            HeaderValueSeparator=" for: "></telerik:GridGroupByField>
                                    </SelectFields>
                                    <GroupByFields>
                                        <telerik:GridGroupByField FieldName="InvestmentAccFilterType" SortOrder="Ascending"></telerik:GridGroupByField>
                                    </GroupByFields>
                                </telerik:GridGroupByExpression>
                            </GroupByExpressions>
                            <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <Columns>
                                <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="50px" Visible="false"
                                    SortExpression="InvestmentCid" ReadOnly="true" DataType="System.String" HeaderText="InvestmentCid"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="InvestmentCid" UniqueName="InvestmentCid">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="30px" HeaderStyle-Width="10%" SortExpression="InvestmentCode"
                                    ReadOnly="true" DataType="System.String" HeaderText="Investment / Account Code"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="InvestmentCode" UniqueName="InvestmentCode">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="30px" HeaderStyle-Width="10%" SortExpression="INVESTMENTACCNO"
                                    ReadOnly="true" DataType="System.String" HeaderText="Account No" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="INVESTMENTACCNO" UniqueName="INVESTMENTACCNO">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="25%" SortExpression="InvestmentProvider"
                                    ReadOnly="true" DataType="System.String" HeaderText="Source" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="InvestmentProvider" UniqueName="InvestmentProvider">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="30%" SortExpression="InvestmentName"
                                    ReadOnly="true" DataType="System.String" HeaderText="Investment / Account Name"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="InvestmentName" UniqueName="InvestmentName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="30px" HeaderStyle-Width="7%" SortExpression="BGLCODE"
                                    DataType="System.String" EmptyDataText="Please Add Code" ReadOnly="true" HeaderText="BGL Code"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="BGLDEFAULTCODE" UniqueName="BGLDEFAULTCODE">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="30px" HeaderStyle-Width="7%" SortExpression="BGLCODE"
                                    DataType="System.String" ReadOnly="false" HeaderText="Custom Code" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="BGLCODE" UniqueName="BGLCODE">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="30px" HeaderStyle-Width="11%" SortExpression="InvestmentCOAType"
                                    ReadOnly="true" DataType="System.String" HeaderText="Transaction Type" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="InvestmentCOAType" UniqueName="InvestmentCOAType">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="35px" HeaderStyle-Width="8%" SortExpression="InvestmentType"
                                    ReadOnly="true" DataType="System.String" HeaderText="Asset Type" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="InvestmentType" UniqueName="InvestmentType">
                                </telerik:GridBoundColumn>
                                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                                    UniqueName="EditColumn">
                                    <HeaderStyle Width="2%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                                </telerik:GridButtonColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </fieldset>
            </div>
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
