﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="TDTransactions.aspx.cs" Inherits="eclipseonlineweb.TDTransactions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript">
        function cancel() {
            window.$find("ModalBehaviour").hide();
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnReport" AlternateText="Print" ToolTip="Print" runat="server"
                                OnClick="BtnReportClick" ImageUrl="~/images/download.png" />
                        </td>
                        <td style="width: 5%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/add-icon.png" OnClick="AddNewClick"
                                ID="btnAddNew" ToolTip="Add New" />
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                PageSize="18" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource"
                OnItemCommand="PresentationGrid_OnItemCommand">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="Banks" TableLayout="Fixed" Font-Size="7.5">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="ID" UniqueName="ID" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="BankCID" ReadOnly="true" HeaderText="BankCID"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="BankCID" UniqueName="BankCID" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="45px" HeaderStyle-Width="80px" SortExpression="ImportTransactionType"
                            ReadOnly="true" HeaderText="Import Transaction" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ImportTransactionType"
                            UniqueName="ImportTransactionType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="75%" SortExpression="SystemTransactionType"
                            ReadOnly="true" HeaderText="System Transaction" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="SystemTransactionType"
                            UniqueName="SystemTransactionType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="85px" SortExpression="Category"
                            ReadOnly="true" HeaderText="Category" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Category" UniqueName="Category">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="85px" SortExpression="AdministrationSystem"
                            ReadOnly="true" HeaderText="Admin Sys" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AdministrationSystem"
                            UniqueName="AdministrationSystem">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="85px" SortExpression="InstituteName"
                            ReadOnly="true" HeaderText="Institute Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InstituteName"
                            UniqueName="InstituteName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="85px" SortExpression="BankTransactionDate"
                            HeaderText="Transaction Date" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BankTransactionDate"
                            UniqueName="BankTransactionDate" DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="75%" ReadOnly="true" SortExpression="ContractNote"
                            HeaderText="Contract Note" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ContractNote"
                            UniqueName="ContractNote">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="85px" SortExpression="BankMaturityDate"
                            HeaderText="Maturity Date" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BankMaturityDate"
                            UniqueName="BankMaturityDate" DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="75%" ReadOnly="true" SortExpression="InterestRate"
                            ShowFilterIcon="true" HeaderText="Interest Rate" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" HeaderButtonType="TextButton" DataField="InterestRate"
                            UniqueName="InterestRate">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="75%" ReadOnly="true" SortExpression="BankAmount"
                            HeaderText="Amount" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BankAmount" UniqueName="BankAmount"
                            DataFormatString="{0:C}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="75%" ReadOnly="true" SortExpression="BankAdjustment"
                            HeaderText="Adjustment" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BankAdjustment"
                            UniqueName="BankAdjustment" DataFormatString="{0:C}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="75%" ReadOnly="true" SortExpression="AmountTotal"
                            HeaderText="Total" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AmountTotal" UniqueName="AmountTotal"
                            DataFormatString="{0:C}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="75%" ReadOnly="true" SortExpression="Comment"
                            HeaderText="Comments" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Comment" UniqueName="Comment"
                            Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="BSB" ReadOnly="true" HeaderText="BSB" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="BSB" UniqueName="BSB" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="AccountNo" ReadOnly="true" HeaderText="AccountNo"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="AccountNo" UniqueName="AccountNo" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="TransactionType" ReadOnly="true" HeaderText="TransactionType"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="TransactionType" UniqueName="TransactionType"
                            Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Product" ReadOnly="true" HeaderText="Product"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="Product" UniqueName="Product" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="InstitutionID" ReadOnly="true" HeaderText="InstitutionID"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="InstitutionID" UniqueName="InstitutionID"
                            Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                            UniqueName="EditColumn">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                        <telerik:GridButtonColumn ConfirmText="Are you sure you want to remove the transaction?"
                            ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:ModalPopupExtender ID="ModalPopupExtenderEditTran" runat="server" CancelControlID="btnCancel"
                TargetControlID="btnShowPopup" PopupControlID="pnlpopup" PopupDragHandleControlID="PopupHeader"
                Drag="true" BackgroundCssClass="ModalPopupBG" BehaviorID="ModalBehaviour">
            </asp:ModalPopupExtender>
            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
            <asp:Panel ID="pnlpopup" runat="server" BackColor="White" Style="display: none">
                <div class="popup_Container">
                    <div class="popup_Titlebar" id="PopupHeader">
                        <div class="TitlebarLeft">
                            Transaction Details
                        </div>
                        <div class="TitlebarRight" onclick="cancel();">
                        </div>
                    </div>
                    <div class="PopupBody">
                        <table>
                            <tr>
                                <td>
                                </td>
                                <td style="width: 400px">
                                    Bank Account Details:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxBankAccountList" DataTextField="BANKCODE" DataValueField="BankCID"
                                        runat="server" Width="400px" CssClass="" ViewStateMode="Enabled" Height="100px"
                                        EnableTheming="false" TriggerPosition="Right" ShowTrigger="true" ShowingAnimation-Animated-Disabled="true">
                                    </c1:C1ComboBox>
                                    <c1:C1InputMask runat="server" ID="lblBankDetails" Width="410px" Height="10px" DisableUserInput="true"
                                        Enabled="true">
                                    </c1:C1InputMask>
                                    <asp:Label Visible="false" runat="server" ID="lblTranID"></asp:Label><asp:Label Visible="false"
                                        runat="server" ID="lblBankCID"></asp:Label>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="width: 200px">
                                    Product:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxProductList" DataTextField="ProName" DataValueField="ProID"
                                        runat="server" Width="400px" CssClass="" ViewStateMode="Enabled" Height="20px"
                                        EnableTheming="false" TriggerPosition="Right" ShowTrigger="true" ShowingAnimation-Animated-Disabled="true"
                                        ForceSelectionText="True">
                                    </c1:C1ComboBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="width: 200px">
                                    Transaction Type:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxTranType" runat="server" Width="400px" CssClass="" ViewStateMode="Enabled"
                                        Height="20px" EnableTheming="false" TriggerPosition="Right" ShowTrigger="true"
                                        ShowingAnimation-Animated-Disabled="true">
                                        <Items>
                                            <c1:C1ComboBoxItem Value="TD" Text="TD" />
                                            <c1:C1ComboBoxItem Value="At Call" Text="At Call" />
                                        </Items>
                                    </c1:C1ComboBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="width: 200px">
                                    Institution:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxInstitution" DataTextField="LegalName" DataValueField="ID"
                                        runat="server" Width="400px" CssClass="" ViewStateMode="Enabled" Height="10px"
                                        EnableTheming="false" TriggerPosition="Right" ShowTrigger="true" ShowingAnimation-Animated-Disabled="true">
                                    </c1:C1ComboBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="width: 200px">
                                    Contract Note:
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <c1:C1InputMask runat="server" ID="ContractNote" Width="250px" Height="10px" Enabled="true">
                                                </c1:C1InputMask>
                                            </td>
                                            <td>
                                                &nbsp;<asp:CheckBox ID="chkConfirmed" runat="server" Text=" Transaction Confirmed" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Import Transaction Type:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxImport" runat="server" Width="400px" CssClass="" ViewStateMode="Enabled"
                                        Height="100px" EnableTheming="false" TriggerPosition="Right" ShowTrigger="true"
                                        ShowingAnimation-Animated-Disabled="true">
                                        <Items>
                                            <c1:C1ComboBoxItem Text="Deposit" Value="Deposit" />
                                            <c1:C1ComboBoxItem Text="Withdrawal" Value="Withdrawal" />
                                        </Items>
                                    </c1:C1ComboBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Category:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxCat" runat="server" Width="400px">
                                        <Items>
                                            <c1:C1ComboBoxItem Text="Benefit Payment" Value="Benefit Payment" />
                                            <c1:C1ComboBoxItem Text="Contribution" Value="Contribution" />
                                            <c1:C1ComboBoxItem Text="Expense" Value="Expense" />
                                            <c1:C1ComboBoxItem Text="Income" Value="Income" />
                                            <c1:C1ComboBoxItem Text="Investment" Value="Investment" />
                                            <c1:C1ComboBoxItem Text="TAX" Value="TAX" />
                                        </Items>
                                    </c1:C1ComboBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    System Transaction Type:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxSystem" runat="server" Width="400px">
                                        <Items>
                                            <c1:C1ComboBoxItem Text="Accounting Expense" Value="" />
                                            <c1:C1ComboBoxItem Text="Administration Fee" Value="" />
                                            <c1:C1ComboBoxItem Text="Advisory Fee" Value="" />
                                            <c1:C1ComboBoxItem Text="Application" Value="" />
                                            <c1:C1ComboBoxItem Text="Business Activity Statement" Value="" />
                                            <c1:C1ComboBoxItem Text="Commission Rebate" Value="" />
                                            <c1:C1ComboBoxItem Text="Distribution" Value="" />
                                            <c1:C1ComboBoxItem Text="Dividend" Value="" />
                                            <c1:C1ComboBoxItem Text="Employer Additional" Value="" />
                                            <c1:C1ComboBoxItem Text="Employer SG" Value="" />
                                            <c1:C1ComboBoxItem Text="Income Tax" Value="" />
                                            <c1:C1ComboBoxItem Text="Instalment Activity Statement" Value="" />
                                            <c1:C1ComboBoxItem Text="Insurance Premium" Value="" />
                                            <c1:C1ComboBoxItem Text="Interest" Value="" />
                                            <c1:C1ComboBoxItem Text="Internal Cash Movement" Value="" />
                                            <c1:C1ComboBoxItem Text="Internal Cash Movement" Value="" />
                                            <c1:C1ComboBoxItem Text="Investment Fee" Value="" />
                                            <c1:C1ComboBoxItem Text="Legal Expense" Value="" />
                                            <c1:C1ComboBoxItem Text="PAYG" Value="" />
                                            <c1:C1ComboBoxItem Text="Pension Payment" Value="" />
                                            <c1:C1ComboBoxItem Text="Personal" Value="" />
                                            <c1:C1ComboBoxItem Text="Property" Value="" />
                                            <c1:C1ComboBoxItem Text="Redemption" Value="" />
                                            <c1:C1ComboBoxItem Text="Regulatory Fee" Value="" />
                                            <c1:C1ComboBoxItem Text="Rental Income" Value="" />
                                            <c1:C1ComboBoxItem Text="Salary Sacrifice" Value="" />
                                            <c1:C1ComboBoxItem Text="Spouse" Value="" />
                                            <c1:C1ComboBoxItem Text="Tax Refund" Value="" />
                                            <c1:C1ComboBoxItem Text="Transfer In" Value="" />
                                            <c1:C1ComboBoxItem Text="Transfer Out" Value="" />
                                            <c1:C1ComboBoxItem Text="Withdrawal" Value="" />
                                        </Items>
                                    </c1:C1ComboBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Interest Rate:
                                </td>
                                <td>
                                    <c1:C1InputPercent Width="410px" ID="C1InputPercentInterestRate" runat="server" ShowSpinner="true"
                                        Value="0">
                                    </c1:C1InputPercent>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Amount:
                                </td>
                                <td>
                                    <c1:C1InputCurrency Width="410px" ID="Amount" runat="server" ShowSpinner="true" Value="50">
                                    </c1:C1InputCurrency>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Adjustment:
                                </td>
                                <td>
                                    <c1:C1InputCurrency Width="410px" ID="Adjustment" runat="server" ShowSpinner="true"
                                        Value="50">
                                    </c1:C1InputCurrency>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Total Amount:
                                </td>
                                <td>
                                    <c1:C1InputCurrency Width="410px" Enabled="false" ID="TotalAmount" runat="server"
                                        ShowSpinner="true" Value="50">
                                    </c1:C1InputCurrency>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Transaction Date:
                                </td>
                                <td>
                                    <c1:C1InputDate runat="server" ID="transactionDate" Width="410px" Height="10px" DateFormat="dd/MM/yyyy">
                                    </c1:C1InputDate>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Maturity Date:
                                </td>
                                <td>
                                    <c1:C1InputDate runat="server" ID="maturityDate" Width="410px" Height="10px" DateFormat="dd/MM/yyyy">
                                    </c1:C1InputDate>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Comments:
                                </td>
                                <td>
                                    <asp:TextBox CssClass="EditTextArea" TextMode="MultiLine" Width="405px" Rows="3"
                                        runat="server" ID="textComments"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div style="overflow-y: auto; max-height: 75px; width: 100%;">
                                        <asp:Label ForeColor="Red" runat="server" ID="MSG"></asp:Label></div>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnAdd" CommandName="Update" runat="server" Text="Add" OnClick="AddTransaction" />
                                    <asp:Button ID="btnUpdate" CommandName="Update" runat="server" Text="Update" OnClick="UpdateTransaction" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
