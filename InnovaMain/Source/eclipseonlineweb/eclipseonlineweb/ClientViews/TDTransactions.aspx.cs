﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb
{
    public partial class TDTransactions : UMABasePage
    {
        protected void UpdateTransaction(object sender, EventArgs e)
        {
            SaveTransactionValues(DataSetOperationType.UpdateSingleTD, Guid.Parse(lblTranID.Text), lblBankCID.Text);
            PresentationGrid.Rebind();
        }

        protected void AddTransaction(object sender, EventArgs e)
        {
            string accountCiD = null;
            if (C1ComboBoxBankAccountList.SelectedValue != string.Empty)
                accountCiD = C1ComboBoxBankAccountList.SelectedValue;

            else
            {
                var selectBankItem = C1ComboBoxBankAccountList.Items.FirstOrDefault(item => item.Text == C1ComboBoxBankAccountList.Text);
                if (selectBankItem != null)
                {
                    accountCiD = selectBankItem.Value;
                    selectBankItem.Selected = true;
                }
            }

            SaveTransactionValues(DataSetOperationType.NewSingleTD, Guid.NewGuid(), accountCiD);

            PresentationGrid.Rebind();
        }

        private void SaveTransactionValues(DataSetOperationType operation, Guid tranID, string accCID)
        {
            if (ValidateControls(tranID, accCID))
            {
                var bankTransactionDetailsDs = new BankTransactionDetailsDS
                    {
                        Unit = new OrganizationUnit
                            {
                                ClientId = ClientHeaderInfo1.ClientId,
                                CurrentUser = GetCurrentUser()
                            },
                        DataSetOperationType = operation
                    };
                DataRow row = bankTransactionDetailsDs.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].NewRow();
                row[BankTransactionDetailsDS.ID] = tranID;
                row[BankTransactionDetailsDS.IMPORTTRANTYPE] = C1ComboBoxImport.Text;
                row[BankTransactionDetailsDS.CATTRANTYPE] = C1ComboBoxCat.Text;
                row[BankTransactionDetailsDS.SYSTRANTYPE] = C1ComboBoxSystem.Text;
                row[BankTransactionDetailsDS.TRANTYPE] = C1ComboBoxTranType.Text;
                row[BankTransactionDetailsDS.AMOUNT] = Amount.Value;

                if (C1ComboBoxSystem.Text == "Redemption")
                {
                    if (Amount.Value > 0)
                        row[BankTransactionDetailsDS.AMOUNT] = Amount.Value * -1;
                    else
                        row[BankTransactionDetailsDS.AMOUNT] = Amount.Value;
                }
                row[BankTransactionDetailsDS.ADJUSTMENT] = Adjustment.Value;
                row[BankTransactionDetailsDS.COMMENT] = textComments.Text;
                
                if (transactionDate.Date.HasValue)
                    row[BankTransactionDetailsDS.TRANDATE] = transactionDate.Date.Value;
                else
                    row[BankTransactionDetailsDS.TRANDATE] = DateTime.Now;

                if (C1ComboBoxTranType.Text != "At Call")
                {
                    if (maturityDate.Date.HasValue)
                        row[BankTransactionDetailsDS.MATURITYDATE] = maturityDate.Date.Value;
                    else
                        row[BankTransactionDetailsDS.MATURITYDATE] = DateTime.Now;
                }
                row[BankTransactionDetailsDS.INTERESTRATE] = C1InputPercentInterestRate.Value;
                row[BankTransactionDetailsDS.CONTRACTNOTE] = ContractNote.Text;
                row[BankTransactionDetailsDS.ISCONFIRMED] = chkConfirmed.Checked;

                if (C1ComboBoxProductList.SelectedItem != null)
                    row[BankTransactionDetailsDS.PROID] = new Guid(C1ComboBoxProductList.SelectedItem.Value);
                else
                {
                    var selectedItem = C1ComboBoxProductList.Items.FirstOrDefault(item => item.Text == C1ComboBoxProductList.Text);
                    if (selectedItem != null)
                    {
                        row[BankTransactionDetailsDS.PROID] = new Guid(selectedItem.Value);
                        selectedItem.Selected = true;
                    }
                }

                if (C1ComboBoxInstitution.SelectedItem != null)
                    row[BankTransactionDetailsDS.INSTITUTEID] = new Guid(C1ComboBoxInstitution.SelectedItem.Value);
                else
                {
                    var selectedItem = C1ComboBoxInstitution.Items.FirstOrDefault(item => item.Text == C1ComboBoxInstitution.Text);
                    if (selectedItem != null)
                    {
                        row[BankTransactionDetailsDS.INSTITUTEID] = new Guid(selectedItem.Value);
                        selectedItem.Selected = true;
                    }
                }
                
                row[BankTransactionDetailsDS.ADMINSYS] = C1ComboBoxProductList.SelectedItem.Text + "-" + C1ComboBoxTranType.Text;

                bankTransactionDetailsDs.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows.Add(row);

                SaveData(accCID, bankTransactionDetailsDs);
            }
            else
            {
                ModalPopupExtenderEditTran.Show();
            }
        }

        private bool ValidateControls(Guid tranID, string accCID)
        {
            MSG.Text = string.Empty;
            bool result = Guid.Empty != tranID;
            if (!result)
                MSG.Text += "Invalid transaction <br/>";

            bool temp = (!string.IsNullOrEmpty(accCID) && Guid.Parse(accCID) != Guid.Empty);
            result = result & temp;
            if (!temp)
                MSG.Text += "Please select Bank Account Details  <br/>";
            temp = C1ComboBoxProductList.SelectedItem != null;
            result = result & temp;
            if (!temp)
                MSG.Text += "Please select Product:   <br/>";
            temp = !string.IsNullOrEmpty(C1ComboBoxTranType.Text);
            result = result & temp;

            if (!temp)
                MSG.Text += "Please select Transaction Type  <br/>";
            temp = C1ComboBoxInstitution.SelectedItem != null;
            result = result & temp;
            if (!temp)
                MSG.Text += "Please select Institution  <br/>";
            
            temp = !string.IsNullOrEmpty(C1ComboBoxImport.Text);

            result = result & temp;
            if (!temp)
                MSG.Text += "Please select Import Transaction Type  <br/>";
            temp = !string.IsNullOrEmpty(C1ComboBoxCat.Text);
            result = result & temp;
            if (!temp)
                MSG.Text += "Please select Category  <br/>";
            temp = !string.IsNullOrEmpty(C1ComboBoxSystem.Text);
            result = result & temp;
            if (!temp)
                MSG.Text += "Please select System Transaction Type  <br/>";
            
            return result;
        }

        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];

            ScriptManager sm = ScriptManager.GetCurrent(Page);

            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");

            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                btnAdd.Visible = false;
                btnAddNew.Visible = false;
                PresentationGrid.Columns.FindByUniqueNameSafe("DeleteColumn").Visible = false;
                PresentationGrid.Columns.FindByUniqueNameSafe("EditColumn").Visible = false;
            }

            if (sm != null)
                sm.RegisterPostBackControl(btnReport);

            UMABroker.ReleaseBrokerManagedComponent(objUser);
        }

        private void GetData(bool isCommand = false)
        {
            cid = Request.QueryString["ins"];
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(cid));

            var tdds = new TDTransactionDS();
            clientData.GetData(tdds);

            PresentationData = tdds;

            if (tdds.Tables.Count > 0)
            {
                if (tdds.Tables[TDTransactionDS.TDTRANTABLE] != null)
                {
                    var tdView = tdds.Tables[TDTransactionDS.TDTRANTABLE].DefaultView;
                    tdView.RowFilter = "Isnull(TransactionType,'') <> 'At Call'";
                    //  tdView.RowFilter = "TransactionType <> 'At Call'";

                    tdView.Sort = "TransactionDate DESC";
                    if (!isCommand)
                        PresentationGrid.DataSource = tdView;
                }

                if (C1ComboBoxProductList.Items.Count <= 0)
                {
                    C1ComboBoxProductList.DataSource = PresentationData.Tables[TDTransactionDS.PRODUCTLIST];
                    C1ComboBoxProductList.DataBind();
                }
                if (C1ComboBoxBankAccountList.Items.Count <= 0)
                {
                    C1ComboBoxBankAccountList.DataSource = PresentationData.Tables[TDTransactionDS.TDACCOUNTLIST];
                    C1ComboBoxBankAccountList.DataBind();
                }
            }
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void BtnReportClick(object sender, EventArgs e)
        {
            BulkTransactionsOutput(cid, ClientHeaderInfo1.ClientName);
        }

        protected void AddNewClick(object sender, EventArgs e)
        {
            C1ComboBoxBankAccountList.Visible = true;
            btnAdd.Visible = true;

            var orgCM = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            if (orgCM != null) C1ComboBoxInstitution.DataSource = orgCM.Institution;
            C1ComboBoxInstitution.DataBind();

            btnUpdate.Visible = false;
            lblBankDetails.Visible = false;
            ContractNote.DisableUserInput = false;

            ClearControl();
            ModalPopupExtenderEditTran.Show();
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var orgCM = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            if (orgCM != null) C1ComboBoxInstitution.DataSource = orgCM.Institution;
            C1ComboBoxInstitution.DataBind();

            var dataItem = (GridDataItem)e.Item;
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    ClearControl();
                    GetData(true);

                    string rowID = dataItem["ID"].Text;
                    DataRow selectedRow = PresentationData.Tables[TDTransactionDS.TDTRANTABLE].Select("ID='" + rowID + "'").FirstOrDefault();

                    //Selected Product
                    if (selectedRow != null)
                    {
                        var productID = (string)selectedRow["Product"];
                        if (orgCM != null)
                        {
                            var product = orgCM.Products.FirstOrDefault(proID => proID.ID == new Guid(productID));

                            if (product != null)
                            {
                                C1ComboBoxProductList.Items.FirstOrDefault(insCB => insCB.Value == product.ID.ToString()).Selected = true;
                            }
                        }

                        var insID = (string)selectedRow["InstitutionID"];
                        if (orgCM != null)
                        {
                            var insObj = orgCM.Institution.FirstOrDefault(ins => ins.ID == new Guid(insID));

                            if (insObj != null)
                            {
                                C1ComboBoxInstitution.Items.FirstOrDefault(insCB => insCB.Text == insObj.LegalName).Selected = true;
                            }
                        }

                        C1ComboBoxTranType.Text = (string)selectedRow["TransactionType"];
                        ContractNote.Text = (string)selectedRow["ContractNote"];
                        chkConfirmed.Checked = Convert.ToBoolean(selectedRow["IsConfirmed"]);
                        transactionDate.Date = (DateTime)selectedRow["BankTransactionDate"];
                        C1ComboBoxImport.Text = selectedRow["ImportTransactionType"].ToString();
                        C1ComboBoxCat.Text = selectedRow["Category"].ToString();
                        C1ComboBoxSystem.Text = selectedRow["SystemTransactionType"].ToString();
                        lblTranID.Text = selectedRow["ID"].ToString();
                        lblBankCID.Text = selectedRow["BankCID"].ToString();
                        lblBankDetails.Text = selectedRow["BSB"] + "-" + selectedRow["AccountNo"];
                        Amount.Value = Convert.ToDouble(selectedRow["Amount"]);
                        Adjustment.Value = Convert.ToDouble(selectedRow["Adjustment"]);
                        TotalAmount.Value = Convert.ToDouble(selectedRow["AmountTotal"]);
                        maturityDate.Date = Convert.ToDateTime(selectedRow["MaturityDate"]);
                        C1InputPercentInterestRate.Value = Convert.ToDouble(selectedRow["InterestRate"]);
                        textComments.Text = selectedRow[BankTransactionDetailsDS.COMMENT].ToString();
                        if (selectedRow["ImportTransactionType"].ToString() != "Deposit" &&
                            selectedRow["ImportTransactionType"].ToString() != "Withdrawal")
                            textComments.Text = selectedRow["ImportTransactionType"].ToString();

                        C1ComboBoxBankAccountList.Visible = false;
                        btnAdd.Visible = false;

                        btnUpdate.Visible = true;
                        lblBankDetails.Visible = true;

                        ModalPopupExtenderEditTran.Show();
                    }
                    break;
                case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var bankTransactionDetailsDS = new BankTransactionDetailsDS
                                                       {
                                                           Unit = new OrganizationUnit
                                                                   {
                                                                       ClientId = ClientHeaderInfo1.ClientId,
                                                                       CurrentUser = GetCurrentUser()
                                                                   },
                                                           DataSetOperationType = DataSetOperationType.DeletSingleTD
                                                       };

                    DataRow row = bankTransactionDetailsDS.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].NewRow();
                    row[BankTransactionDetailsDS.ID] = dataItem["ID"].Text;
                    bankTransactionDetailsDS.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows.Add(row);
                    SaveData(dataItem["BankCID"].Text, bankTransactionDetailsDS);
                    PresentationGrid.Rebind();
                    break;
            }
        }

        private void ClearControl()
        {
            MSG.Text = string.Empty;
            transactionDate.Date = DateTime.Today;
            C1ComboBoxImport.Text = string.Empty;
            C1ComboBoxImport.SelectedIndex = -1;
            C1ComboBoxCat.Text = string.Empty;
            C1ComboBoxCat.SelectedIndex = -1;
            C1ComboBoxSystem.Text = string.Empty;
            C1ComboBoxSystem.SelectedIndex = -1;
            lblTranID.Text = string.Empty;
            lblBankCID.Text = Guid.Empty.ToString();
            textComments.Text = string.Empty;
            lblBankDetails.Text = string.Empty;
            Amount.Value = 0;
            TotalAmount.Value = 0;
            Adjustment.Value = 0;
            ContractNote.Text = string.Empty;
            chkConfirmed.Checked = false;
            C1ComboBoxTranType.Text = string.Empty;
            C1ComboBoxTranType.SelectedIndex = -1;
            C1ComboBoxInstitution.Text = string.Empty;
            C1ComboBoxInstitution.SelectedIndex = -1;
            C1ComboBoxProductList.Text = string.Empty;
            C1ComboBoxProductList.SelectedIndex = -1;

            textComments.Text = string.Empty;
            C1InputPercentInterestRate.Value = 0;
            transactionDate.Date = null;
            maturityDate.Date = null;
            C1ComboBoxBankAccountList.Text = String.Empty;
            C1ComboBoxBankAccountList.SelectedIndex = -1;
        }
    }
}
