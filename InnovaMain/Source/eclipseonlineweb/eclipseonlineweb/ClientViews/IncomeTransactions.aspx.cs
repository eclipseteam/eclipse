﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class IncomeTransactions : UMABasePage
    {

        protected override void Intialise()
        {
            base.Intialise();

            DistributionDetails.Saved += x =>
            {
                ModalPopupExtender1.Hide();
                PresentationGrid.Rebind();
            };
            DistributionDetails.Canceled += () => ModalPopupExtender1.Hide();
            DistributionDetails.ValidationFailed += () => ModalPopupExtender1.Show();
            DistributionDetails.SaveData += ds => SaveData(cid, ds);

            AdjustDetails.Saved += x =>
            {
                ModalPopupExtenderAdjust.Hide();
                PresentationGrid.Rebind();
            };
            AdjustDetails.Canceled += () => ModalPopupExtenderAdjust.Hide();
            AdjustDetails.ValidationFailed += () => ModalPopupExtenderAdjust.Show();
            AdjustDetails.SaveData += ds => SaveData(cid, ds);
            
        }

        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];
            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
                sm.RegisterPostBackControl(btnReport);

            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");

            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                PresentationGrid.Columns.FindByUniqueNameSafe("DeleteColumn").Visible = false;
            }

            UMABroker.ReleaseBrokerManagedComponent(objUser);
        }

        protected void BtnReportClick(object sender, EventArgs e)
        {
            BulkTransactionsOutput(this.cid, this.ClientHeaderInfo1.ClientName);
        }

        private void GetData()
        {
            cid = Request.QueryString["ins"];
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(cid));

            var divDS = new DIVTransactionDS();
            clientData.GetData(divDS);
            PresentationData = divDS;

            if (divDS.Tables.Count > 0)
            {
                var divView = new DataView(divDS.Tables["Income Transactions"]) { Sort = "RecordDate DESC" };
                PresentationGrid.DataSource = divView;
                PresentationGrid.Columns.FindByUniqueNameSafe("RecordDate").HeaderTooltip = Tooltip.ExDividendDate;
                PresentationGrid.Columns.FindByUniqueNameSafe("PaymentDate").HeaderTooltip = Tooltip.PaymentDate;
                PresentationGrid.Columns.FindByUniqueNameSafe("BalanceDate").HeaderTooltip = Tooltip.BalanceDate;
                PresentationGrid.Columns.FindByUniqueNameSafe("BooksCloseDate").HeaderTooltip = Tooltip.BooksCloseDate;
            }
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            switch (e.CommandName.ToLower())
            {
                case "edit_old":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var id = new Guid(dataItem["ID"].Text);
                    var user = GetCurrentUser();
                    DistributionDetails.SetEntity(id, new Guid(cid), ClientHeaderInfo1.ClientId, ClientHeaderInfo1.ClientName, user.IsAdmin);
                    ModalPopupExtender1.Show();
                    break;
                case "edit":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var dividendEntityID = new Guid(dataItem["ID"].Text);
                    var userAdjust = GetCurrentUser();
                    AdjustDetails.PopulatePopupControls(dividendEntityID, new Guid(cid), ClientHeaderInfo1.ClientId, ClientHeaderInfo1.ClientName, userAdjust.IsAdmin, ClientHeaderInfo1.GetClientCLID());
                    ModalPopupExtenderAdjust.Show();
                    break;
                case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var divTransactionDetailsDS = new DivTransactionDetailsDS { DataSetOperationType = DataSetOperationType.DeletSingle };
                    DataRow row = divTransactionDetailsDS.Tables[DivTransactionDetailsDS.DIVTRANSACTIONDETAILSTABLE].NewRow();
                    row[DivTransactionDetailsDS.ID] = dataItem["ID"].Text;
                    divTransactionDetailsDS.Tables[DivTransactionDetailsDS.DIVTRANSACTIONDETAILSTABLE].Rows.Add(row);
                    SaveData(cid, divTransactionDetailsDS);
                    PresentationGrid.Rebind();
                    break;
            }
        }
    }
}
