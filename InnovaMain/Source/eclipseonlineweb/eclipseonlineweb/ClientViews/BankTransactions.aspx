﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="BankTransactions.aspx.cs" Inherits="eclipseonlineweb.BankTransactions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript">
        function cancel() {
            window.$find("ModalBehaviour").hide();
        }
    </script>
    <style>
        .rtWrapperContent
        {
            background-color: lightyellow !important;
            color: black !important;
            
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnReport" AlternateText="Print" ToolTip="Print" runat="server"
                                OnClick="BtnReportClick" ImageUrl="~/images/download.png" />
                        </td>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnAddtran" Visible="false" AlternateText="Add Transactions"
                                ToolTip="Add Transactions" runat="server" OnClick="BtnAddTran" ImageUrl="~/images/add-icon.png" />
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadToolTipManager ID="RadToolTipManager1" runat="server" AutoTooltipify="true"  Animation="Fade"
                Width="350px" AutoCloseDelay="25000" BorderColor="#CCCCCC">
            </telerik:RadToolTipManager>
            <telerik:RadGrid ID="PresentationGrid" OnNeedDataSource="PresentationGrid_OnNeedDataSource"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
                OnItemCommand="PresentationGrid_OnItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
                OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemDataBound="PresentationGrid_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="Banks" TableLayout="Fixed" Font-Size="7.5">
                    <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add Transaction">
                    </CommandItemSettings>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="ID" UniqueName="ID" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="BankCID" ReadOnly="true" HeaderText="BankCID"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="BankCID" UniqueName="BankCID" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn HeaderText="Account" Visible="false" SortExpression="AccountName"
                            UniqueName="DDLAccountList" DataField="AccountName" ColumnEditorID="GridDropDownListColumnEditorAccountList" />
                        <telerik:GridBoundColumn FilterControlWidth="45px" HeaderStyle-Width="80px" SortExpression="ImportTransactionType"
                            ReadOnly="true" HeaderText="Import Transaction" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ImportTransactionType"
                            UniqueName="ImportTransactionType">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn HeaderText="Import Transaction Type" Visible="false"
                            SortExpression="ImportTransactionType" UniqueName="DDLImportTranType" DataField="ImportTransactionType"
                            ColumnEditorID="GridDropDownColumnEditorImportTranType" />
                        <telerik:GridBoundColumn FilterControlWidth="75%" SortExpression="SystemTransactionType"
                            ReadOnly="true" HeaderText="System Transaction" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="SystemTransactionType"
                            UniqueName="SystemTransactionType">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn HeaderText="System Transaction Type" Visible="false"
                            SortExpression="SystemTransactionType" UniqueName="DDLSystemTranType" DataField="SystemTransactionType"
                            ColumnEditorID="GridDropDownListColumnEditorSystemTranType" />
                        <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="85px" SortExpression="Category"
                            ReadOnly="true" HeaderText="Category" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Category" UniqueName="Category">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn HeaderText="Transaction Category" Visible="false" SortExpression="Category"
                            UniqueName="DDLCategoryTranType" DataField="Category" ColumnEditorID="GridDropDownListColumnEditorCategory" />
                        <telerik:GridDateTimeColumn Visible="true" UniqueName="BankTransactionDate" PickerType="DatePicker"
                            HeaderText="Transaction Date" DataField="BankTransactionDate" DataFormatString="{0:dd/MM/yyyy}">
                            <ItemStyle Width="85px" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridBoundColumn FilterControlWidth="75%" ReadOnly="true" SortExpression="AccountNO" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" 
                            ShowFilterIcon="true" HeaderText="Account Number" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            HeaderButtonType="TextButton" DataField="AccountNO" UniqueName="AccountNO">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="35px" HeaderStyle-Width="70px" ReadOnly="true"
                            SortExpression="AccountType" HeaderText="Account Type" AutoPostBackOnFilter="true" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" 
                            ShowFilterIcon="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                            DataField="AccountType" UniqueName="AccountType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="75%" ReadOnly="false" SortExpression="BankAmount" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" 
                            HeaderText="Amount" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BankAmount" UniqueName="BankAmount"
                            DefaultInsertValue="0" DataFormatString="{0:C}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="75%" ReadOnly="false" SortExpression="BankAdjustment" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" 
                            DefaultInsertValue="0" HeaderText="Adjustment" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BankAdjustment"
                            UniqueName="BankAdjustment" DataFormatString="{0:C}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="75%" ReadOnly="true" SortExpression="AmountTotal" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" 
                            DefaultInsertValue="0" HeaderText="Total" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AmountTotal" UniqueName="AmountTotal"
                            DataFormatString="{0:C}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="75%" ReadOnly="false" SortExpression="Comment"
                            HeaderText="Comments" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Comment" UniqueName="Comment">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="75px" ReadOnly="true"
                            SortExpression="DividendStatus" HeaderText="Status" AutoPostBackOnFilter="true" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" 
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="DividendStatus" UniqueName="DividendStatus">
                        </telerik:GridBoundColumn>

                         <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="75px" ReadOnly="true" Display="false"
                            SortExpression="DividendID" HeaderText="DividendID" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains"  HeaderButtonType="TextButton"
                            DataField="DividendID" UniqueName="DividendID">
                        </telerik:GridBoundColumn>


                        <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                            UniqueName="EditColumn">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                        <telerik:GridButtonColumn ConfirmText="Are you sure you want to remove the transaction?"
                            ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorImportTranType"
                runat="server" DropDownStyle-Width="400px" />
            <telerik:GridDropDownListColumnEditor ID="GridDropDownListColumnEditorCategory" runat="server"
                DropDownStyle-Width="400px" />
            <telerik:GridDropDownListColumnEditor ID="GridDropDownListColumnEditorSystemTranType"
                runat="server" DropDownStyle-Width="400px" />
            <telerik:GridDateTimeColumnEditor ID="GridDropDownColumnEditorTransactionDate" runat="server">
            </telerik:GridDateTimeColumnEditor>
            <telerik:GridDropDownListColumnEditor ID="GridDropDownListColumnEditorAccountList"
                runat="server" DropDownStyle-Width="400px" />
            <asp:Button ID="Button1" runat="server" Style="display: none" />
            <asp:Button ID="btnOkay" runat="server" Style="display: none" />
            <asp:ModalPopupExtender ID="ModalPopupExtenderEditTran" runat="server" CancelControlID="btnCancel"
                TargetControlID="btnShowPopup" PopupControlID="pnlpopup" PopupDragHandleControlID="PopupHeader"
                Drag="true" BackgroundCssClass="ModalPopupBG" BehaviorID="ModalBehaviour">
            </asp:ModalPopupExtender>
            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
            <asp:Panel ID="pnlpopup" runat="server" BackColor="White" Style="display: none">
                <div class="popup_Container">
                    <div class="popup_Titlebar" id="PopupHeader">
                        <div class="TitlebarLeft">
                            Transaction Details
                        </div>
                        <div class="TitlebarRight" onclick="cancel();">
                        </div>
                    </div>
                    <div class="PopupBody">
                        <table>
                            <tr>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="width: 200px">
                                    Bank Account Details:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxBankAccountList" DataTextField="BANKCODE" DataValueField="BankCID"
                                        runat="server" Width="200px" CssClass="" ViewStateMode="Enabled" Height="100px"
                                        EnableTheming="false" TriggerPosition="Right" ShowTrigger="true" ShowingAnimation-Animated-Disabled="true">
                                    </c1:C1ComboBox>
                                    <c1:C1InputMask runat="server" ID="lblBankDetails" Width="210px" Height="10px" DisableUserInput="true"
                                        Enabled="true">
                                    </c1:C1InputMask>
                                    <asp:Label Visible="false" runat="server" ID="lblTranID"></asp:Label><asp:Label Visible="false"
                                        runat="server" ID="lblBankCID"></asp:Label>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Import Transaction Type:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxImport" runat="server" Width="200px" CssClass="" ViewStateMode="Enabled"
                                        Height="100px" EnableTheming="false" TriggerPosition="Right" ShowTrigger="true"
                                        ShowingAnimation-Animated-Disabled="true">
                                        <Items>
                                            <c1:C1ComboBoxItem Text="Deposit" Value="Deposit" />
                                            <c1:C1ComboBoxItem Text="Withdrawal" Value="Withdrawal" />
                                        </Items>
                                    </c1:C1ComboBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Category:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxCat" runat="server" Width="200px">
                                        <Items>
                                            <c1:C1ComboBoxItem Text="Benefit Payment" Value="Benefit Payment" />
                                            <c1:C1ComboBoxItem Text="Contribution" Value="Contribution" />
                                            <c1:C1ComboBoxItem Text="Expense" Value="Expense" />
                                            <c1:C1ComboBoxItem Text="Income" Value="Income" />
                                            <c1:C1ComboBoxItem Text="Investment" Value="Investment" />
                                            <c1:C1ComboBoxItem Text="TAX" Value="TAX" />
                                        </Items>
                                    </c1:C1ComboBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    System Transaction Type:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxSystem" runat="server" Width="200px">
                                        <Items>
                                            <c1:C1ComboBoxItem Text="Accounting Expense" Value="" />
                                            <c1:C1ComboBoxItem Text="Administration Fee" Value="" />
                                            <c1:C1ComboBoxItem Text="Advisory Fee" Value="" />
                                            <c1:C1ComboBoxItem Text="Application" Value="" />
                                            <c1:C1ComboBoxItem Text="Business Activity Statement" Value="" />
                                            <c1:C1ComboBoxItem Text="Commission Rebate" Value="" />
                                            <c1:C1ComboBoxItem Text="Contributions Tax" Value="" />
                                            <c1:C1ComboBoxItem Text="Capital Gains Tax" Value="" />
                                            <c1:C1ComboBoxItem Text="Distribution" Value="" />
                                            <c1:C1ComboBoxItem Text="Dividend" Value="" />
                                            <c1:C1ComboBoxItem Text="Employer Additional" Value="" />
                                            <c1:C1ComboBoxItem Text="Employer SG" Value="" />
                                            <c1:C1ComboBoxItem Text="Income Tax" Value="" />
                                            <c1:C1ComboBoxItem Text="Instalment Activity Statement" Value="" />
                                            <c1:C1ComboBoxItem Text="Insurance Premium" Value="" />
                                            <c1:C1ComboBoxItem Text="Interest" Value="" />
                                            <c1:C1ComboBoxItem Text="Internal Cash Movement" Value="" />
                                            <c1:C1ComboBoxItem Text="Internal Cash Movement" Value="" />
                                            <c1:C1ComboBoxItem Text="Investment Fee" Value="" />
                                            <c1:C1ComboBoxItem Text="Legal Expense" Value="" />
                                            <c1:C1ComboBoxItem Text="PAYG" Value="" />
                                            <c1:C1ComboBoxItem Text="Pension Payment" Value="" />
                                            <c1:C1ComboBoxItem Text="Personal" Value="" />
                                            <c1:C1ComboBoxItem Text="Property" Value="" />
                                            <c1:C1ComboBoxItem Text="Redemption" Value="" />
                                            <c1:C1ComboBoxItem Text="Regulatory Fee" Value="" />
                                            <c1:C1ComboBoxItem Text="Rental Income" Value="" />
                                            <c1:C1ComboBoxItem Text="Salary Sacrifice" Value="" />
                                            <c1:C1ComboBoxItem Text="Spouse" Value="" />
                                            <c1:C1ComboBoxItem Text="Tax Refund" Value="" />
                                            <c1:C1ComboBoxItem Text="Transfer In" Value="" />
                                            <c1:C1ComboBoxItem Text="Transfer Out" Value="" />
                                            <c1:C1ComboBoxItem Text="Withdrawal" Value="" />
                                        </Items>
                                    </c1:C1ComboBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Amount:
                                </td>
                                <td>
                                    <c1:C1InputCurrency Width="210px" ID="Amount" runat="server" ShowSpinner="true" Value="50">
                                    </c1:C1InputCurrency>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Adjustment:
                                </td>
                                <td>
                                    <c1:C1InputCurrency Width="210px" ID="Adjustment" runat="server" ShowSpinner="true"
                                        Value="50">
                                    </c1:C1InputCurrency>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Total Amount:
                                </td>
                                <td>
                                    <c1:C1InputCurrency Width="210px" Enabled="false" ID="TotalAmount" runat="server"
                                        ShowSpinner="true" Value="50">
                                    </c1:C1InputCurrency>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Transaction Date:
                                </td>
                                <td>
                                    <c1:C1InputDate runat="server" ID="transactionDate" Width="210px" Height="10px" DateFormat="dd/MM/yyyy">
                                    </c1:C1InputDate>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Comments:
                                </td>
                                <td>
                                    <asp:TextBox CssClass="EditTextArea" TextMode="MultiLine" Width="205px" Rows="3"
                                        runat="server" ID="textComments"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnAdd" CommandName="Update" runat="server" Text="Add" OnClick="AddTransaction" />
                                    <asp:Button ID="btnUpdate" CommandName="Update" runat="server" Text="Update" OnClick="UpdateTransaction" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
