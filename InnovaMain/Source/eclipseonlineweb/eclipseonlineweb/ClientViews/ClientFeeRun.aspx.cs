﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.CM.OrganizationUnit;
using System.Globalization;

namespace eclipseonlineweb
{
    public partial class ClientFeeRun : UMABasePage
    {
        protected void GetData()
        {
            this.cid = Request.QueryString["ins"].ToString();
            IOrganizationUnit clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            UMAEntityFeesDS umaEntityFeesDS = new UMAEntityFeesDS();
            
            if(this.C1ComboBoxMonth.SelectedItem != null)
                umaEntityFeesDS.Month = Int32.Parse(this.C1ComboBoxMonth.SelectedItem.Value);

            if (this.C1ComboBoxMonth.SelectedItem != null)
                umaEntityFeesDS.Year = Int32.Parse(this.C1ComboBoxYear.SelectedItem.Value); 

            clientData.GetData(umaEntityFeesDS);
            this.PresentationData = umaEntityFeesDS;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }


        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
        }

        public override void LoadPage()
        {
            this.cid = Request.QueryString["ins"].ToString();
            base.LoadPage();
        }

        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e){ }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
        }

        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e){}
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e){}
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e){}
        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
        }

        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e){}

        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem && !e.Item.IsInEditMode)
            {
                GridDataItem dataItem = e.Item as GridDataItem;
                string month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Int32.Parse(this.C1ComboBoxMonth.SelectedItem.Value));
                dataItem["MONTH"].Text = month;
            }
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
           
        }

        protected void btnFeeRun_Click(object sender, EventArgs e)
        {
            GetData();

            DataView View = new DataView(this.PresentationData.Tables[UMAEntityFeesDS.CALCULATEDFEESSUMMARYBYFEETYPE]);
            View.Sort = UMAEntityFeesDS.FEESUBTYPE + " ASC";
            this.PresentationGrid.DataSource = View.ToTable();
            this.PresentationGrid.DataBind();
        }

        protected void btnFeeRunWithTransaction_Click(object sender, EventArgs e)
        {
            this.cid = Request.QueryString["ins"].ToString();
            UMABroker.SaveOverride = true; 

            IOrganizationUnit clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            UMAEntityFeesDS umaEntityFeesDS = new UMAEntityFeesDS();
            umaEntityFeesDS.DataSetOperationType = DataSetOperationType.NewSingle;

            if (this.C1ComboBoxMonth.SelectedItem != null)
                umaEntityFeesDS.Month = Int32.Parse(this.C1ComboBoxMonth.SelectedItem.Value);

            if (this.C1ComboBoxMonth.SelectedItem != null)
                umaEntityFeesDS.Year = Int32.Parse(this.C1ComboBoxYear.SelectedItem.Value);

            clientData.GetData(umaEntityFeesDS);
            this.PresentationData = umaEntityFeesDS;
            clientData.SetData(umaEntityFeesDS);
            UMABroker.ReleaseBrokerManagedComponent(clientData);

            DataView View = new DataView(this.PresentationData.Tables[UMAEntityFeesDS.CALCULATEDFEESSUMMARYBYFEETYPE]);
            View.Sort = UMAEntityFeesDS.FEESUBTYPE + " ASC";
            this.PresentationGrid.DataSource = View.ToTable();
            this.PresentationGrid.DataBind();

            UMABroker.SetComplete();
            UMABroker.SetStart();
        }
    }
}
