﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="IncomeDistributions.aspx.cs" Inherits="eclipseonlineweb.IncomeDistributions" %>

<%@ Register TagPrefix="uc" TagName="Details" Src="WebControls/ClientDistributionDetails.ascx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnReport" AlternateText="Print" ToolTip="Print" runat="server"
                                OnClick="BtnReportClick" ImageUrl="~/images/download.png" />
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
              <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                PageSize="18" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource"
                OnItemCommand="PresentationGrid_OnItemCommand">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="Banks" TableLayout="Fixed" Font-Size="7.5">
                    <CommandItemSettings ShowAddNewRecordButton="false" ></CommandItemSettings>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <Columns>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="ID" HeaderText="ID" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="ID" UniqueName="ID" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="8%" FilterControlWidth="60%" SortExpression="FundCode"
                            ReadOnly="true" HeaderText="Fund Code" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="FundCode" UniqueName="FundCode">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="22%" FilterControlWidth="80%" SortExpression="FundName"
                            ReadOnly="true" HeaderText="Fund Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="FundName" UniqueName="FundName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="RecodDate_Shares" ReadOnly="true" HeaderText="Units Held"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="RecodDate_Shares" UniqueName="RecodDate_Shares">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="RecordDate" HeaderText="Record Date" ReadOnly="true"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="RecordDate" UniqueName="RecordDate"
                            DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="PaymentDate" HeaderText="Payment Date"
                            AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                            HeaderButtonType="TextButton" DataField="PaymentDate" UniqueName="PaymentDate"
                            DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="NetCashDistribution" HeaderText="Net Cash Distribution"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="NetCashDistribution" UniqueName="NetCashDistribution">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="GrossCashDistribution" HeaderText="Gross Distribution"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="GrossCashDistribution" UniqueName="GrossCashDistribution">
                        </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn ReadOnly="true" SortExpression="MaunuallyAdjusted" HeaderText="Maunually Adjusted"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="MaunuallyAdjusted" UniqueName="MaunuallyAdjusted">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="Status" HeaderText="Status" AutoPostBackOnFilter="true" 
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Status" 
                            UniqueName="Status">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                            UniqueName="EditColumn">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                        <telerik:GridButtonColumn ConfirmText="Are you sure you want to remove the distribution?"
                            ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
            <asp:Button ID="btnOkay" runat="server" Style="display: none" />
            <asp:Button ID="btnCancel" runat="server" Style="display: none" />
            <asp:ModalPopupExtender ID="ModalPopupExtender1" CancelControlID="btnCancel" OkControlID="btnOkay"
                BackgroundCssClass="ModalPopupBG" runat="server" Drag="true" TargetControlID="btnShowPopup"
                PopupControlID="panEdit" PopupDragHandleControlID="PopupHeader">
            </asp:ModalPopupExtender>
            <asp:Panel runat="server" ID="panEdit">
                <uc:Details ID="DistributionDetails" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
