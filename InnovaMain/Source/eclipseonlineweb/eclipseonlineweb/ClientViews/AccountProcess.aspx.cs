﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using C1.C1Report;
using System.Configuration;
using C1.Web.Wijmo.Extenders.C1Chart;
using C1.Web.Wijmo.Controls.C1Chart;
using System.Text;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class AccountProcess : UMABasePage
    {
        protected void BindBreakDownGridTD(string tdID)
        {
            C1GridView gridtest = new C1GridView();
           var ds= this.PresentationData as HoldingRptDataSet;
            DataView tdsummaryView = new DataView(ds.TDBreakDownTable);
            tdsummaryView.RowFilter = ds.TDBreakDownTable.TDID+"='" + tdID + "'";
            tdsummaryView.Sort = ds.TDBreakDownTable.TRANSTYPE+" ASC";
            DataTable sortedFilteredTable = tdsummaryView.ToTable();
        }

        protected void BindBreakDownGrid(string productID)
        {
            C1GridView gridtest = new C1GridView();
            var ds = this.PresentationData as HoldingRptDataSet;
            DataView prosummaryView = new DataView(ds.ProductBreakDownTable);
            prosummaryView.RowFilter = ds.ProductBreakDownTable.PRODUCTID+"='" + productID + "'";
            prosummaryView.Sort = ds.ProductBreakDownTable.INVESMENTCODE+" ASC";
            DataTable sortedFilteredTable = prosummaryView.ToTable();
        }

        protected void FinancialSummary_RowCommand(object sender, C1GridViewCommandEventArgs e)
        {
            if (e.CommandName != "filter" & e.CommandName != "sort")
            {
                C1GridView financialGrid = (C1GridView)sender;
                string[] args = e.CommandArgument.ToString().Split('_');

                string assetName = args[0];
                string tdID = args[1];
                string productID = args[2];

                if (assetName == "TDs")
                {
                    if (tdID != string.Empty)
                    {
                        this.BindBreakDownGridTD(tdID);
                        this.lnkBackToSummary.Visible = true;
                        this.FinancialSummary.Visible = false;
                    }
                }
                else
                {
                    if (productID != string.Empty)
                    {
                        this.BindBreakDownGrid(productID);
                        this.lnkBackToSummary.Visible = true;
                        this.FinancialSummary.Visible = false;
                    }
                }
            }
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            if (e.Item.Text == "FINANCIAL SUMMARY")
                MainView();
            else if (e.Item.Text == "BANK")
                Response.Redirect(@"BankTransactions.aspx?ins=" + this.cid);
            else if (e.Item.Text == "ASX")
                Response.Redirect(@"ASXTransactions.aspx?ins=" + this.cid);
            else if (e.Item.Text == "MANAGED FUNDS")
                Response.Redirect(@"ManagedFundsTransactions.aspx?ins=" + this.cid);
            else if (e.Item.Text == "ACCESS LIST")
                Response.Redirect(@"SecurityConfiguration.aspx?ins=" + this.cid);
            else if (e.Item.Text == "TERM DEPOSITS")
                Response.Redirect(@"TDTransactions.aspx?ins=" + this.cid);
            else if (e.Item.Text == "INCOME")
                Response.Redirect(@"IncomeTransactions.aspx?ins=" + this.cid);
            else if (e.Item.Text == "MANUAL ASSETS")
                Response.Redirect(@"ManualTransactions.aspx?ins=" + this.cid);
            else if (e.Item.Text == "ACCOUNT SUMMARY")
                Response.Redirect(@"AccountSummary.aspx?ins=" + this.cid);
            else if (e.Item.Text == "DISTRIBUTIONS")
                Response.Redirect(@"IncomeDistributions.aspx?ins=" + this.cid);
            else if (e.Item.Text.Contains("GOTO REPORTS"))
                Response.Redirect(@"~\Reports\ClientHoldingReport.aspx?ins=" + this.cid);
        }

        public override void LoadPage()
        {
            this.cid = Request.QueryString["ins"].ToString();
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            HoldingRptDataSet ds = new HoldingRptDataSet();

            if (this.C1FinancialSummaryDate.Date.HasValue)
                ds.ValuationDate = this.C1FinancialSummaryDate.Date.Value;

            clientData.GetData(ds);
            this.PresentationData = ds;

            this.PopulatePage(this.PresentationData);
            BindFinancialSummary(this.PresentationData);

            UMABroker.ReleaseBrokerManagedComponent(clientData);

            ScriptManager sm = ScriptManager.GetCurrent(Page);

            if (sm != null)
                sm.RegisterPostBackControl(this.btnReport);

            MenuItem mi = this.NavigationMenu.FindItem("TRANSACTIONS");//Retrieves the menuitem
            if (mi != null)
                mi.Text = " <div style='font-weight:bolder; text-decoration:underline; color:Black'>  " + mi.Text + " </div>";

            mi = this.NavigationMenu.FindItem("GOTO REPORTS");//Retrieves the menuitem
            if (mi != null)
                mi.Text = " <div style='font-weight:bolder; color:Black'>  " + mi.Text + " </div>";
            mi = this.NavigationMenu.FindItem("ACCOUNT INFO");
            if (mi != null)
                mi.Text = " <div style='font-weight:bolder; text-decoration:underline; color:Black'>  " + mi.Text + " </div>";
        }

        private void BindFinancialSummary(DataSet ds)
        {
            DataRow clientSummaryRow = ds.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0];
            var rptDs = ds as HoldingRptDataSet;
            DataView summaryView = new DataView(rptDs.HoldingSummaryTable);
            summaryView.Sort = rptDs.HoldingSummaryTable.SERVICETYPE + "," + rptDs.HoldingSummaryTable.ASSETNAME + " ASC";

            FinancialSummary.DataSource = summaryView;
            FinancialSummary.DataBind();

            this.lblClientID.Text = clientSummaryRow[HoldingRptDataSet.CLIENTID].ToString();
            this.lblClientName.Text = clientSummaryRow[HoldingRptDataSet.CLIENTNAME].ToString();
        }

        protected void BackToSummary(object sender, EventArgs args)
        {
            this.BindFinancialSummary(this.PresentationData);
            this.FinancialSummary.Visible = true;
            this.lnkBackToSummary.Visible = false;
        }

        protected void FinancialSummary_RowDataBound(object sender, C1GridViewRowEventArgs e)
        {
            if (e.Row.DataItem != null)
            {
                string assetName = ((System.Data.DataRowView)(e.Row.DataItem)).Row["AssetName"].ToString();
                string tdID = ((System.Data.DataRowView)(e.Row.DataItem)).Row["TDID"].ToString();
                string productID = ((System.Data.DataRowView)(e.Row.DataItem)).Row["ProductID"].ToString();

                if (assetName == "TDs")
                {
                    if (tdID != string.Empty)
                    {
                        LinkButton linkDesc = (LinkButton)e.Row.FindControl("linkDesc");
                        linkDesc.Visible = true;
                        e.Row.FindControl("lblDesc").Visible = false;
                    }
                }
                else
                {
                    if (productID != string.Empty)
                    {
                        LinkButton linkDesc = (LinkButton)e.Row.FindControl("linkDesc");
                        linkDesc.Visible = true;
                        e.Row.FindControl("lblDesc").Visible = false;
                    }
                }
            }
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "tming" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        private void MainView()
        {
            this.BindFinancialSummary(this.PresentationData);
            this.pnlFinancialSummary.Visible = true;
            this.pnlHoldingChart.Visible = false;
            this.pnlToolBar.Visible = true;
        }

        private void MainView_Click(object sender, ImageClickEventArgs e)
        {
            MainView();
        }

        protected void Refresh_Click(object sender, EventArgs e)
        {
            this.cid = Request.QueryString["ins"].ToString();
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            UMABroker.ReleaseBrokerManagedComponent(clientData);

            clientData = this.UMABroker.GetBMCInstance(new Guid(cid));

            HoldingRptDataSet ds = new HoldingRptDataSet();

            if (this.C1FinancialSummaryDate.Date.HasValue)
                ds.ValuationDate = this.C1FinancialSummaryDate.Date.Value;

            clientData.GetData(ds);
            this.PresentationData = ds;


            this.PopulatePage(this.PresentationData);
            BindFinancialSummary(this.PresentationData);

            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void Chart_Click(object sender, EventArgs e)
        {
            this.pnlFinancialSummary.Visible = false;
            this.pnlHoldingChart.Visible = true;
            this.pnlToolBar.Visible = true;
            var ds = this.PresentationData as HoldingRptDataSet;
            var holdingCollection = ds.HoldingSummaryTable.AsEnumerable();
            var holdingCollectionGroup = holdingCollection.GroupBy(holdColl => holdColl[ds.HoldingSummaryTable.ASSETNAME]);
            var difmCollection = holdingCollection.Where(holdColl => holdColl[ds.HoldingSummaryTable.SERVICETYPE].ToString() == "Do It For Me");
            var diyCollection = holdingCollection.Where(holdColl => holdColl[ds.HoldingSummaryTable.SERVICETYPE].ToString() == "Do It Yourself");
            var diwmCollection = holdingCollection.Where(holdColl => holdColl[ds.HoldingSummaryTable.SERVICETYPE].ToString() == "Do It With Me");
            var manCollection = holdingCollection.Where(holdColl => holdColl[ds.HoldingSummaryTable.SERVICETYPE].ToString() == "Manual Asset");

            C1.Web.Wijmo.Controls.C1Chart.BarChartSeries series = new C1.Web.Wijmo.Controls.C1Chart.BarChartSeries();

            if (difmCollection.Count() > 0)
            {
                C1.Web.Wijmo.Controls.C1Chart.BarChartSeries difmSeries = new C1.Web.Wijmo.Controls.C1Chart.BarChartSeries();
                difmSeries.Label = "DO IT FOR ME";

                var difmCollectionGroup = difmCollection.GroupBy(holdColl => holdColl[ds.HoldingSummaryTable.ASSETNAME]);

                foreach (var group in difmCollectionGroup)
                {
                    difmSeries.Data.X.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartXData(group.Key.ToString()));

                    Double subTotal = 0;

                    foreach (DataRow row in group)
                        subTotal += Convert.ToDouble(row[ds.HoldingSummaryTable.HOLDING].ToString());

                    difmSeries.Data.Y.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartYData(subTotal));
                }

                foreach (var group in holdingCollectionGroup)
                {
                    if (!difmSeries.Data.X.Values.Contains(group.Key))
                    {
                        difmSeries.Data.X.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartXData(group.Key.ToString()));
                        difmSeries.Data.Y.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartYData(0));
                    }
                }

                C1.Web.Wijmo.Controls.C1Chart.ChartStyle chartStyle = new C1.Web.Wijmo.Controls.C1Chart.ChartStyle();
                chartStyle.Stroke = Color.FromArgb(127, 199, 60);
                chartStyle.Opacity = 0.8;
                chartStyle.Fill.Color = Color.FromArgb(142, 222, 67);
                C1BarChartHoldingSumDIFM.SeriesStyles.Add(chartStyle);

                C1BarChartHoldingSumDIFM.SeriesList.Add(difmSeries);
                C1BarChartHoldingSumDIFM.Visible = true;
            }

            if (difmCollection.Count() > 0)
            {
                var difmCollectionGroup = difmCollection.GroupBy(holdColl => holdColl[ds.HoldingSummaryTable.ASSETNAME]);

                foreach (var group in difmCollectionGroup)
                {
                    C1.Web.Wijmo.Controls.C1Chart.PieChartSeries difmSeries = new C1.Web.Wijmo.Controls.C1Chart.PieChartSeries();
                    difmSeries.Label = group.Key.ToString();

                    Double subTotal = 0;

                    foreach (DataRow row in group)
                        subTotal += Convert.ToDouble(row[ds.HoldingSummaryTable.HOLDING].ToString());

                    difmSeries.Data = subTotal;

                    this.C1PieChart1.SeriesList.Add(difmSeries);
                }

            }

            if (diyCollection.Count() > 0)
            {
                C1.Web.Wijmo.Controls.C1Chart.BarChartSeries diySeries = new C1.Web.Wijmo.Controls.C1Chart.BarChartSeries();
                diySeries.Label = "DO IT YOURSELF";

                var diySeriesGroup = diyCollection.GroupBy(holdColl => holdColl[ds.HoldingSummaryTable.ASSETNAME]);

                foreach (var group in diySeriesGroup)
                {
                    diySeries.Data.X.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartXData(group.Key.ToString()));

                    Double subTotal = 0;

                    foreach (DataRow row in group)
                        subTotal += Convert.ToDouble(row[ds.HoldingSummaryTable.HOLDING].ToString());

                    diySeries.Data.Y.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartYData(subTotal));
                }

                foreach (var group in holdingCollectionGroup)
                {
                    if (!diySeries.Data.X.Values.Contains(group.Key))
                    {
                        diySeries.Data.X.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartXData(group.Key.ToString()));
                        diySeries.Data.Y.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartYData(0));
                    }
                }

                C1.Web.Wijmo.Controls.C1Chart.ChartStyle chartStyle = new C1.Web.Wijmo.Controls.C1Chart.ChartStyle();
                chartStyle.Stroke = Color.FromArgb(95, 153, 150);
                chartStyle.Opacity = 0.8;
                chartStyle.Fill.Color = Color.FromArgb(106, 171, 167);
                C1BarChartHoldingSumDIY.SeriesStyles.Add(chartStyle);

                C1BarChartHoldingSumDIY.SeriesList.Add(diySeries);
                C1BarChartHoldingSumDIY.Visible = true;
            }

            if (diwmCollection.Count() > 0)
            {
                C1.Web.Wijmo.Controls.C1Chart.BarChartSeries diwmSeries = new C1.Web.Wijmo.Controls.C1Chart.BarChartSeries();
                diwmSeries.Label = "DO IT WITH ME";

                var diwmSeriesGroup = diwmCollection.GroupBy(holdColl => holdColl[ds.HoldingSummaryTable.ASSETNAME]);

                foreach (var group in diwmSeriesGroup)
                {
                    diwmSeries.Data.X.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartXData(group.Key.ToString()));

                    Double subTotal = 0;

                    foreach (DataRow row in group)
                        subTotal += Convert.ToDouble(row[ds.HoldingSummaryTable.HOLDING].ToString());

                    diwmSeries.Data.Y.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartYData(subTotal));
                }

                foreach (var group in holdingCollectionGroup)
                {
                    if (!diwmSeries.Data.X.Values.Contains(group.Key))
                    {
                        diwmSeries.Data.X.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartXData(group.Key.ToString()));
                        diwmSeries.Data.Y.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartYData(0));
                    }
                }

                C1.Web.Wijmo.Controls.C1Chart.ChartStyle chartStyle = new C1.Web.Wijmo.Controls.C1Chart.ChartStyle();
                chartStyle.Stroke = Color.FromArgb(62, 95, 119);
                chartStyle.Opacity = 0.8;
                chartStyle.Fill.Color = Color.FromArgb(70, 106, 133);
                C1BarChartHoldingSumDIWM.SeriesStyles.Add(chartStyle);

                C1BarChartHoldingSumDIWM.SeriesList.Add(diwmSeries);
                C1BarChartHoldingSumDIWM.Visible = true;
            }

            if (manCollection.Count() > 0)
            {
                C1.Web.Wijmo.Controls.C1Chart.BarChartSeries manSeries = new C1.Web.Wijmo.Controls.C1Chart.BarChartSeries();
                manSeries.Label = "MANUAL ASSETS";

                var manSeriesGroup = manCollection.GroupBy(holdColl => holdColl[ds.HoldingSummaryTable.ASSETNAME]);

                foreach (var group in manSeriesGroup)
                {
                    manSeries.Data.X.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartXData(group.Key.ToString()));

                    Double subTotal = 0;

                    foreach (DataRow row in group)
                        subTotal += Convert.ToDouble(row[ds.HoldingSummaryTable.HOLDING].ToString());

                    manSeries.Data.Y.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartYData(subTotal));
                }

                foreach (var group in holdingCollectionGroup)
                {
                    if (!manSeries.Data.X.Values.Contains(group.Key))
                    {
                        manSeries.Data.X.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartXData(group.Key.ToString()));
                        manSeries.Data.Y.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartYData(0));
                    }
                }

                C1.Web.Wijmo.Controls.C1Chart.ChartStyle chartStyle = new C1.Web.Wijmo.Controls.C1Chart.ChartStyle();
                chartStyle.Stroke = Color.FromArgb(127, 199, 60);
                chartStyle.Opacity = 0.8;
                chartStyle.Fill.Color = Color.FromArgb(142, 222, 67);
                C1BarChartHoldingSumMAN.SeriesStyles.Add(chartStyle);

                C1BarChartHoldingSumMAN.SeriesList.Add(manSeries);
                C1BarChartHoldingSumMAN.Visible = true;
            }
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            BankTransactionDS banktransacationDS = new BankTransactionDS();
            MISTransactionDS mISTransactionDS = new Oritax.TaxSimp.DataSets.MISTransactionDS();
            ASXTransactionDS aSXTransactionDS = new Oritax.TaxSimp.DataSets.ASXTransactionDS();
            DIVTransactionDS dIVTransactionDS = new Oritax.TaxSimp.DataSets.DIVTransactionDS();
            TDTransactionDS tdTransactionDS = new Oritax.TaxSimp.DataSets.TDTransactionDS();
            ManualTransactionDS manualTransactionDS = new Oritax.TaxSimp.DataSets.ManualTransactionDS();

            clientData.GetData(banktransacationDS);
            clientData.GetData(mISTransactionDS);
            clientData.GetData(aSXTransactionDS);
            clientData.GetData(dIVTransactionDS);
            clientData.GetData(tdTransactionDS);
            clientData.GetData(manualTransactionDS);

            DataSet excelDataset = new DataSet();
            excelDataset.Merge(PresentationData, true, MissingSchemaAction.Add);
            excelDataset.Merge(banktransacationDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(mISTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(aSXTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(dIVTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(tdTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(manualTransactionDS, true, MissingSchemaAction.Add);

            if (excelDataset.Tables.Contains("HoldingSummary"))
            {
                excelDataset.Tables["HoldingSummary"].Columns.Remove("TDID");
                excelDataset.Tables["HoldingSummary"].Columns.Remove("ProductID");
            }

            if (excelDataset.Tables.Contains("Cash Transactions"))
            {
                excelDataset.Tables["Cash Transactions"].Columns.Remove("InstitutionID");
                excelDataset.Tables["Cash Transactions"].Columns.Remove("ID");
                excelDataset.Tables["Cash Transactions"].Columns.Remove("DividendID");
            }

            if (excelDataset.Tables.Contains("MIS Transactions"))
                excelDataset.Tables["MIS Transactions"].Columns.Remove("ID");

            if (excelDataset.Tables.Contains("TDs Transactions"))
            {
                excelDataset.Tables["TDs Transactions"].Columns.Remove("ID");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("AccountName");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("DividendID");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("InstitutionID");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("Product");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("BSB");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("AccountType");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("AccountNo");
            }

            if (excelDataset.Tables.Contains("Manual Transactions"))
                excelDataset.Tables["Manual Transactions"].Columns.Remove("ID");

            if (excelDataset.Tables.Contains("ProductBreakDown"))
                excelDataset.Tables.Remove("ProductBreakDown");
            if (excelDataset.Tables.Contains("TDBreakDown"))
                excelDataset.Tables.Remove("TDBreakDown");
            if (excelDataset.Tables.Contains("ContactTable"))
                excelDataset.Tables.Remove("ContactTable");

            ExcelHelper.ToExcel(excelDataset, this.lblClientName.Text + ".xls", this.Page.Response);
        }

        protected void GenerateFinancialSummary(object sender, EventArgs e)
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"].ToString()));

            HoldingRptDataSet ds = new HoldingRptDataSet();
            ds.ValuationDate = this.C1FinancialSummaryDate.Date.Value;
            clientData.GetData(ds);
            this.PresentationData = ds;

            this.BindFinancialSummary(this.PresentationData);
            this.pnlFinancialSummary.Visible = true;
            this.pnlHoldingChart.Visible = false;
            this.pnlToolBar.Visible = true;

            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void Filter(object sender, C1GridViewFilterEventArgs e)
        {
            e.Values[0] = ((string)e.Values[0]).Trim();
            var ds = PresentationData as HoldingRptDataSet;
            DataView summaryView = new DataView(ds.HoldingSummaryTable);
            summaryView.Sort = ds.HoldingSummaryTable.SERVICETYPE + "," + ds.HoldingSummaryTable.ASSETNAME + " ASC";

            this.FinancialSummary.DataSource = summaryView;
            FinancialSummary.DataBind();
        }

        protected void FinancialSummarySorting(object sender, C1GridViewSortEventArgs e)
        {
            MainView();
        }

    }
}
