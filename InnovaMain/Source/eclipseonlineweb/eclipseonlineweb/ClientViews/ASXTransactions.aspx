﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="ASXTransactions.aspx.cs" Inherits="eclipseonlineweb.ASXTransactions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .RadGrid_Metro .rgHeader, .RadGrid_Metro .rgHeader a
        {
            font-size: 7.5pt !important;
        }
        .RadGrid_Metro .rgRow a, .RadGrid_Metro .rgAltRow a, .RadGrid_Metro tr.rgEditRow a, .RadGrid_Metro .rgFooter a, .RadGrid_Metro .rgEditForm a
        {
            font-size: 7.5pt !important;
        }
    </style>
    <style>
        .rtWrapperContent
        {
            background-color: lightyellow !important;
            color: black !important;
        }
    </style>
    <script type="text/javascript">
        function cancel() {
            window.$find("ModalBehaviour").hide();
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnReport" AlternateText="Print" ToolTip="Print" runat="server"
                                OnClick="BtnReportClick" ImageUrl="~/images/download.png" />
                        </td>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnImportASXHistoricalCostbase" AlternateText="Import ASX Historical cost base of Transferred Assets"
                                ToolTip="Import ASX Historical cost base of Transferred Assets" runat="server"
                                OnClick="BtnImportASXHistorical" ImageUrl="~/images/excel.png" />
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <asp:Panel runat="server" ID="ASXPnl" Visible="false">
                <telerik:RadToolTipManager ID="RadToolTipManager1" runat="server" AutoTooltipify="true"
                    Animation="Fade" Width="350px" AutoCloseDelay="25000" BorderColor="#CCCCCC">
                </telerik:RadToolTipManager>
                <telerik:RadGrid ID="ASXTransGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False" OnItemUpdated="PresentationGrid_ItemUpdated"
                    PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                    GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                    AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnNeedDataSource="ASXTransGrid_OnNeedDataSource" OnInsertCommand="ASXTransGrid_InsertCommand"
                    OnItemCommand="ASXTransGrid_OnItemCommand" OnItemDataBound="ASXTransGrid_OnItemDataBound">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                        Name="Banks" TableLayout="Fixed" Font-Size="7.5">
                        <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add Transaction">
                        </CommandItemSettings>
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="TranstactionUniqueID" ReadOnly="true" HeaderText="TranstactionUniqueID"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="TranstactionUniqueID" UniqueName="TranstactionUniqueID"
                                Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ASXCID" ReadOnly="true" HeaderText="ASXCID"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ASXCID" UniqueName="ASXCID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70%" HeaderStyle-Width="80px" SortExpression="InvestmentCode"
                                ReadOnly="true" HeaderText="Investment Code" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InvestmentCode"
                                UniqueName="InvestmentCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70%" HeaderStyle-Width="9%" SortExpression="TransactionType"
                                ReadOnly="true" HeaderText="Transaction Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransactionType"
                                UniqueName="TransactionType">
                            </telerik:GridBoundColumn>

                            
                            <telerik:GridTemplateColumn FilterControlWidth="66%" ReadOnly="true" UniqueName="AccountNo"
                                DataField="AccountNo" SortExpression="AccountNo" HeaderText="Account No">
                                <HeaderStyle Font-Size="7"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblDesc" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                    <asp:LinkButton CommandName="ASXAdjustment" CommandArgument='<%# Eval("TranstactionUniqueID") %>'
                                        ForeColor="#5188FF" Font-Underline="false" Font-Size="X-Small" Visible="false"
                                        runat="server" ID="linkDesc" Text='<%# Eval("AccountNo") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                             <telerik:GridBoundColumn FilterControlWidth="66%" SortExpression="AccountNo" Display="false"
                                ReadOnly="true" HeaderText="AccountNo" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountNo"
                                UniqueName="AccountNoText">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="66%" SortExpression="FPSInstructionID"
                                ReadOnly="true" HeaderText="External Ref ID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="FPSInstructionID"
                                UniqueName="FPSInstructionID">
                            </telerik:GridBoundColumn>
                            
                               <telerik:GridDropDownColumn HeaderText="Account List" Visible="false"
                            SortExpression="AccountNo" UniqueName="DDLAccList" DataField="AccountNo"
                            ColumnEditorID="GridDropDownListColumnAccList" />

                             <telerik:GridDropDownColumn HeaderText="Transaction Type" Visible="false"
                            SortExpression="TransactionType" UniqueName="DDLTranType" DataField="TransactionType"
                            ColumnEditorID="GridDropDownColumnEditorTranType" />

                               <telerik:GridDropDownColumn HeaderText="Sec List" Visible="false"
                            SortExpression="InvestmentCode" UniqueName="DDLSecList" DataField="InvestmentCode"
                            ColumnEditorID="GridDropDownListColumnDDLSecList" />
                            <telerik:GridDateTimeColumn Visible="true" UniqueName="TradeDate" PickerType="DatePicker"
                                HeaderText="Trade Date" DataField="TradeDate" DataFormatString="{0:dd/MM/yyyy}" EditDataFormatString="dd/MM/yyyy">
                                <ItemStyle Width="85px" />
                            </telerik:GridDateTimeColumn>
                            <telerik:GridDateTimeColumn Visible="true" UniqueName="SettlementDate" PickerType="DatePicker"
                                HeaderText="Settlement Date" DataField="SettlementDate" DataFormatString="{0:dd/MM/yyyy}" EditDataFormatString="dd/MM/yyyy">
                                <ItemStyle Width="85px" />
                            </telerik:GridDateTimeColumn>
                            <telerik:GridNumericColumn FilterControlWidth="66%" ReadOnly="false" SortExpression="Units" DecimalDigits="4"
                                ShowFilterIcon="true" HeaderText="Units" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" DefaultInsertValue="0"
                                HeaderButtonType="TextButton" DataField="Units" UniqueName="Units" DataFormatString="{0:N4}">
                            </telerik:GridNumericColumn>
                           <telerik:GridNumericColumn FilterControlWidth="66%" ReadOnly="false" SortExpression="UnitPrice" Visible="true" DecimalDigits="6"
                                ShowFilterIcon="true" HeaderText="Unit Price" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" DefaultInsertValue="0"
                                HeaderButtonType="TextButton" DataField="UnitPrice" UniqueName="UnitPrice" DataFormatString="{0:N6}">
                            </telerik:GridNumericColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="GrossValue"
                                HeaderText="Gross Value" AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                                HeaderButtonType="TextButton" DataField="GrossValue" UniqueName="GrossValue" 
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridNumericColumn FilterControlWidth="66%" ReadOnly="false" SortExpression="BrokerageAmount" DecimalDigits="2"
                                HeaderText="Brokerage" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" DefaultInsertValue="0"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BrokerageAmount"
                                UniqueName="BrokerageAmount" DataFormatString="{0:C}">
                            </telerik:GridNumericColumn>
                            <telerik:GridBoundColumn FilterControlWidth="66%" ReadOnly="true" SortExpression="NetValue"
                                HeaderText="Net Value" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="NetValue" UniqueName="NetValue"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="66%" ReadOnly="false" SortExpression="Narrative"
                                HeaderText="Contract Note" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Narrative" UniqueName="Narrative">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="75px" ReadOnly="true"
                                SortExpression="DividendStatus" HeaderText="Status" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="DividendStatus" UniqueName="DividendStatus">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="75px" ReadOnly="true"
                                Display="false" SortExpression="DividendID" HeaderText="DividendID" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" HeaderButtonType="TextButton" DataField="DividendID"
                                UniqueName="DividendID">
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                                UniqueName="EditColumn">
                                <HeaderStyle Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                            <telerik:GridButtonColumn ConfirmText="Are you sure you want to remove the transaction?"
                                ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                <HeaderStyle Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                  <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorTranType"
                runat="server" DropDownStyle-Width="400px" />
                  <telerik:GridDropDownListColumnEditor ID="GridDropDownListColumnAccList"
                runat="server" DropDownStyle-Width="400px" />
                <telerik:GridDropDownListColumnEditor ID="GridDropDownListColumnDDLSecList"
                runat="server" DropDownStyle-Width="400px" />
                
            </asp:Panel>
            <asp:Label Visible="false" runat="server" ID="lblTranID"></asp:Label>
                                    <asp:Label Visible="false" runat="server" ID="lblAsxID"></asp:Label>
            <asp:Panel runat="server" ID="pnlASXAdjustments" EnableViewState="true" Visible="false">
                <asp:Label runat="server" ID="lblTranIDPatent" Visible="false"></asp:Label>
                <asp:Label runat="server" ID="lblASCCID" Visible="false"></asp:Label>
                <asp:LinkButton runat="server" Visible="true" ID="lnkBackToSummary" Text="Back to ASX Transaction Page"
                    OnClick="BackToSummary"> </asp:LinkButton></p>
                <fieldset title="Add Adjustment Transaction">
                    <legend>TRANSACTION BREAKDOWN</legend>
                    <br />
                    <asp:GridView AlternatingRowStyle-Font-Size="X-Small" FilterStyle-Font-Size="Smaller"
                        RowStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small" ID="ASXGridParent"
                        AllowPaging="false" PageSize="20" AllowSorting="false" runat="server" AutoGenerateColumns="false"
                        ShowFilter="false" Width="100%" ShowFooter="false">
                        <Columns>
                            <asp:BoundField DataField="TranstactionUniqueID" Visible="false" SortExpression="TranstactionUniqueID"
                                HeaderText="TranstactionUniqueID">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ASXCID" Visible="false" SortExpression="ASXCID" HeaderText="ASXCID">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="InvestmentCode" SortExpression="InvestmentCode" HeaderText="Investment Code">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TransactionType" SortExpression="TransactionType" HeaderText="Transaction Type">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="AccountNo" SortExpression="AccountNo" HeaderText="Account No">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FPSInstructionID" SortExpression="FPSInstructionID" HeaderText="External Ref ID">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TradeDate" SortExpression="TradeDate" DataFormatString="{0:dd/MM/yyyy}"
                                HeaderText="Trade Date">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SettlementDate" DataFormatString="{0:dd/MM/yyyy}" SortExpression="SettlementDate"
                                HeaderText="Settlement Date">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Units" SortExpression="Units" HeaderText="Units">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="GrossValue" DataFormatString="{0:C}" SortExpression="GrossValue"
                                HeaderText="Gross Value">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Charges" DataFormatString="{0:C}" SortExpression="Charges"
                                HeaderText="Charges">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="BrokerageGST" DataFormatString="{0:C}" SortExpression="BrokerageGST"
                                HeaderText="GST">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="BrokerageAmount" DataFormatString="{0:C}" SortExpression="BrokerageAmount"
                                HeaderText="Brokerage">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NetValue" DataFormatString="{0:C}" SortExpression="NetValue"
                                HeaderText="Net Value">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Narrative" SortExpression="Narrative" HeaderText="Contract Note">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                    <br />
                    <telerik:RadGrid ID="PresentationGrid" OnNeedDataSource="PresentationGridNeedDataSource"
                        runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                        AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnItemCommand="PresentationGridItemCommand"
                        GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                        AllowAutomaticUpdates="True" OnInsertCommand="PresentationGridInsertCommand"
                        EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGridItemCreated"
                        OnItemDataBound="PresentationGridItemDataBound">
                        <PagerStyle Mode="NumericPages"></PagerStyle>
                        <MasterTableView DataKeyNames="TransactionID" AllowMultiColumnSorting="True" Width="100%"
                            CommandItemDisplay="Top" Name="ASXDetails" TableLayout="Fixed">
                            <CommandItemSettings AddNewRecordText="Add New Transaction Adjustment" ShowExportToExcelButton="true"
                                ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                            <Columns>
                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                                    <HeaderStyle Width="3%"></HeaderStyle>
                                    <ItemStyle CssClass="MyImageButton"></ItemStyle>
                                </telerik:GridEditCommandColumn>
                                <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="ParentID" HeaderStyle-Width="5%"
                                    ItemStyle-Width="5%" HeaderText="ParentID" ReadOnly="true" Display="false" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="ParentID" UniqueName="ParentID">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="TransactionID"
                                    HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderText="TransactionID" ReadOnly="true"
                                    Display="false" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransactionID"
                                    UniqueName="TransactionID">
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn FilterControlWidth="190px" SortExpression="TradeDate"
                                    HeaderStyle-Width="17%" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Width="17%"
                                    HeaderText="Trade Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" DataField="TradeDate" UniqueName="TradeDate">
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDropDownColumn HeaderText="Transaction Type" SortExpression="TransactionType"
                                    UniqueName="TransactionTypeEditor" DataField="TransactionType" Visible="false"
                                    ColumnEditorID="GridDropDownColumnEditorTransacctionType" />
                                <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                                    SortExpression="TransactionType" HeaderText="Transaction Type" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="TransactionType" UniqueName="TransactionType" ReadOnly="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                                    SortExpression="UnitPrice" HeaderText="Unit Price" AutoPostBackOnFilter="true"
                                    DataFormatString="{0:N6}" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="UnitPrice" UniqueName="UnitPrice" ReadOnly="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                                    Aggregate="Sum" SortExpression="Units" HeaderText="Units" AutoPostBackOnFilter="true"
                                    DataFormatString="{0:N4}" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="Units" UniqueName="Units" ReadOnly="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                                    SortExpression="BrokerageAmount" HeaderText="Brokerage Amount" AutoPostBackOnFilter="true"
                                    DataFormatString="{0:C}" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="BrokerageAmount" UniqueName="BrokerageAmount"
                                    ReadOnly="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                                    Aggregate="Sum" SortExpression="NetValue" HeaderText="Net Value" AutoPostBackOnFilter="true"
                                    DataFormatString="{0:C}" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="NetValue" UniqueName="NetValue" ReadOnly="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridButtonColumn ConfirmText="Delete this transaction?" ButtonType="ImageButton"
                                    CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                    <HeaderStyle Width="3%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                                </telerik:GridButtonColumn>
                            </Columns>
                        </MasterTableView>
                        <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                            <Excel Format="Html"></Excel>
                        </ExportSettings>
                    </telerik:RadGrid>
                    <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorTransacctionType"
                        runat="server" DropDownStyle-Width="300px" />
                </fieldset>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
