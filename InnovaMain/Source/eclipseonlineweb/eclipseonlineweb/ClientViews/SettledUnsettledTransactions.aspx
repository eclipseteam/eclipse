﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="SettledUnsettledTransactions.aspx.cs" Inherits="eclipseonlineweb.ClientViews.SettledUnsettledTransactions" %>

<%@ Register Src="../Controls/SettledUnsettledTransactions.ascx" TagName="SettledUnsettledTransactions" TagPrefix="uc1" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <uc1:SettledUnsettledTransactions ID="SettledUnsettledTransactionsControl" runat="server" />
</asp:Content>
