﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="IncomeTransactions.aspx.cs" Inherits="eclipseonlineweb.IncomeTransactions" %>

<%@ Register TagPrefix="uc" TagName="Adjust" Src="WebControls/ClientDividendAdjust.ascx" %>
<%@ Register TagPrefix="uc" TagName="Details" Src="WebControls/ClientDivindendDetails.ascx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <style>
                    .wijmo-wijgrid .wijmo-wijgrid-headerrow .wijmo-wijgrid-headertext
                    {
                        padding: 0 !important;
                    }
                </style>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnReport" AlternateText="Print" ToolTip="Print" runat="server"
                                OnClick="BtnReportClick" ImageUrl="~/images/download.png" />
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                PageSize="18" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource"
                OnItemCommand="PresentationGrid_OnItemCommand">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="Banks" TableLayout="Fixed" Font-Size="7.5">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="ID" UniqueName="ID" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="5%" FilterControlWidth="55%" SortExpression="InvestmentCode"
                            ReadOnly="true" HeaderText="Code" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InvestmentCode"
                            UniqueName="InvestmentCode">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="10%" FilterControlWidth="75%" SortExpression="TransactionType"
                            ReadOnly="true" HeaderText="Transaction Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransactionType"
                            UniqueName="TransactionType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" SortExpression="BalanceDate" ReadOnly="true"
                            HeaderText="Balance Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BalanceDate" UniqueName="BalanceDate"
                            DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" SortExpression="DividendType" ReadOnly="true"
                            HeaderText="Dividend Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="DividendType"
                            UniqueName="DividendType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" SortExpression="RecordDate" HeaderText="Ex-Dividend Date"
                            ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="RecordDate" UniqueName="RecordDate"
                            DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="BooksCloseDate"
                            ShowFilterIcon="true" HeaderText="Books Close Date" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" HeaderButtonType="TextButton" DataField="BooksCloseDate"
                            UniqueName="BooksCloseDate" DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="PaymentDate"
                            HeaderText="Payment Date" AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                            HeaderButtonType="TextButton" DataField="PaymentDate" UniqueName="PaymentDate"
                            DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="UnitsOnHand"
                            HeaderText="Amount" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UnitsOnHand" UniqueName="UnitsOnHand">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="CentsPerShare"
                            HeaderText="Cents Per Share" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CentsPerShare"
                            UniqueName="CentsPerShare" DataFormatString="{0:N4}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="PaidDividend"
                            HeaderText="Paid Dividend" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="PaidDividend"
                            UniqueName="PaidDividend" DataFormatString="{0:C}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="FrankingCredits"
                            HeaderText="Franking Credits (CPS)" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="FrankingCredits"
                            UniqueName="FrankingCredits" DataFormatString="{0:N4}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="TotalFrankingCredits"
                            HeaderText="Total Franking Credits ($)" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TotalFrankingCredits"
                            UniqueName="TotalFrankingCredits" DataFormatString="{0:C}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="Currency"
                            HeaderText="Currency (Ccy)" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Currency" UniqueName="Currency">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="Status"
                            HeaderText="Status" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Status" UniqueName="Status">
                        </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="Adjusted" HeaderText="Adjusted"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="ManuallyAdjusted" UniqueName="Adjusted">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit" UniqueName="Adjust">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                        <%--<telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                            UniqueName="EditColumn">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>--%>
                        <telerik:GridButtonColumn ConfirmText="Are you sure you want to remove the transaction?"
                            ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
            <asp:Button ID="btnAdjust" runat="server" Style="display: none" />
            <asp:Button ID="btnCancel" runat="server" Style="display: none" />
            <asp:Button ID="btnOkay" runat="server" Style="display: none" />
            <asp:ModalPopupExtender ID="ModalPopupExtender1" CancelControlID="btnCancel" OkControlID="btnOkay"
                BackgroundCssClass="ModalPopupBG" runat="server" Drag="true" TargetControlID="btnShowPopup"
                PopupControlID="panEdit" PopupDragHandleControlID="PopupHeader">
            </asp:ModalPopupExtender>
            <asp:Panel runat="server" ID="panEdit" ScrollBars="Auto" Height="95%">
                <uc:Details ID="DistributionDetails" runat="server" />
            </asp:Panel>
            <asp:ModalPopupExtender ID="ModalPopupExtenderAdjust" CancelControlID="btnCancel" OkControlID="btnOkay"
                BackgroundCssClass="ModalPopupBG" runat="server" Drag="true" TargetControlID="btnAdjust"
                PopupControlID="panAdjust" PopupDragHandleControlID="PopupHeaderAdjust">
            </asp:ModalPopupExtender>
            <asp:Panel runat="server" ID="panAdjust" ScrollBars="Auto" Height="95%">
                <uc:Adjust ID="AdjustDetails" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
