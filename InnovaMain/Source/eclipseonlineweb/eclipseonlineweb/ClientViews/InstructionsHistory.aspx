﻿<%@ Page Title="e-Clipse Online Portal - Configure Fees" Language="C#" MasterPageFile="ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="InstructionsHistory.aspx.cs" Inherits="eclipseonlineweb.InstructionsHistory" %>

<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <fieldset>
        <table width="100%">
            <tr>
                <td width="10%">
                </td>
                <td width="10%">
                </td>
                <td style="width: 30%;" class="breadcrumbgap">
                    <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                    <br />
                    <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="MainView" style="background-color: white">
                <table style="width: 100%; height: 80px">
                    <tr>
                        <td style="width: 125px">
                            Transaction Status:
                        </td>
                        <td colspan="5">
                            <telerik:RadListBox ID="lstStatus" runat="server" CheckBoxes="true" Width="220px"
                                SelectionMode="Multiple">
                            </telerik:RadListBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date:
                        </td>
                        <td style="width: 50px">
                            From:
                        </td>
                        <td style="width: 200px">
                            <telerik:RadDatePicker ID="calFrom" runat="server">
                            </telerik:RadDatePicker>
                        </td>
                        <td style="width: 50px">
                            To:
                        </td>
                        <td style="width: 200px">
                            <telerik:RadDatePicker ID="calTo" runat="server">
                            </telerik:RadDatePicker>
                        </td>
                        <td>
                            <asp:Button ID="btnApplyFilter" runat="server" Text="Apply Filter" OnClick="btnApplyFilter_OnClick" />
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <asp:Label ID="lblFilter" runat="server" Text="Showing orders with status:"></asp:Label>
            <br />
            <telerik:RadGrid OnNeedDataSource="PresentationGridNeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGridDetailTableDataBind"
                OnItemCommand="PresentationGridItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="PresentationGridItemUpdated" OnItemDeleted="PresentationGridItemDeleted"
                OnItemInserted="PresentationGridItemInserted" OnInsertCommand="PresentationGridInsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGridItemCreated"
                OnItemDataBound="PresentationGridItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="PendingOrders" TableLayout="Fixed">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <Columns>
                        <telerik:GridBoundColumn ReadOnly="true" Visible="false" DataField="ID" UniqueName="ID" />
                        <telerik:GridTemplateColumn UniqueName="FeeTemplateDetails" HeaderText="Order ID"
                            FilterControlWidth="5%" ShowFilterIcon="false">
                            <HeaderStyle Width="7%" />
                            <ItemStyle Width="7%" />
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtnOrderID" runat="server" Text='<%# Eval("OrderID") %>' CommandName="details"
                                    CommandArgument='<%# Eval("OrderID") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="OrderID" ReadOnly="true"
                            HeaderStyle-Width="30%" ItemStyle-Width="30%" HeaderText="Order ID" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="OrderID" UniqueName="OrderID" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Securtiy" ReadOnly="true"
                            HeaderStyle-Width="30%" ItemStyle-Width="30%" HeaderText="Securtiy" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Securtiy" UniqueName="Securtiy">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Investor" HeaderStyle-Width="10%"
                            ItemStyle-Width="10%" HeaderText="Investor" ReadOnly="true" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Investor" UniqueName="Investor">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80px" ReadOnly="true" SortExpression="Type"
                            HeaderStyle-Width="10%" ShowFilterIcon="false" ItemStyle-Width="10%" HeaderText="Type"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                            DataField="Type" UniqueName="Type">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" ReadOnly="true" SortExpression="Status"
                            HeaderStyle-Width="20%" ItemStyle-Width="15%" HeaderText="Status" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Status" UniqueName="Status">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" ReadOnly="true" SortExpression="Value"
                            HeaderStyle-Width="20%" ItemStyle-Width="15%" HeaderText="Value" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" DataFormatString="{0:c}" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="Value" UniqueName="Value">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" ReadOnly="true" SortExpression="CreatedDate"
                            HeaderStyle-Width="20%" ItemStyle-Width="15%" HeaderText="Created" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="CreatedDate" UniqueName="CreatedDate">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" ReadOnly="true" SortExpression="PlacedBy"
                            HeaderStyle-Width="20%" ItemStyle-Width="15%" HeaderText="Placed By" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="PlacedBy" UniqueName="PlacedBy">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
