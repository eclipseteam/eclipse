﻿using System;
using System.Collections.Generic;
using Oritax.TaxSimp.Security;
using eclipseonlineweb.WebUtilities;

namespace eclipseonlineweb.ClientViews
{
    public partial class BuyASX : UMABasePage
    {
        protected override void Intialise()
        {
            BuyASXControl.SaveData += (orderCMCID, ds) =>
            {
                if (orderCMCID == Guid.Empty.ToString())
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(orderCMCID, ds);
                }
            };
        }

        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth" || (IsSMA() && CheckAdviserForOrderPad(objUser)))
                return true;
            return objUser.UserType == UserType.Innova || objUser.Name == "Administrator";
        }

        public override void LoadPage()
        {
            if (!IsPostBack)
            {
                cid = Request.QueryString["ins"];
                var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
                //Setting control required properties
                BuyASXControl.IsAdmin = objUser.Administrator;
                BuyASXControl.ClientCID = cid;
            }
        }

        private bool IsSMA()
        {
           return SMAUtilities.IsSMAClient(new Guid(Request.QueryString["ins"]), UMABroker);
        }
    }
}