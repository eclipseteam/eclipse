﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.Utilities;
using System.Threading;
using eclipseonlineweb.WebUtilities;

namespace eclipseonlineweb.ClientViews
{
    public partial class ClientViewMaster : MasterPage
    {
        protected string Cid = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            Cid = Request.Params["ins"];

            if (Master != null) ((HiddenField)Master.FindControl("hfMainMenuText")).Value = "HOME";

            UMABroker = new CMBroker(Context.User, DBConnection.Connection.ConnectionString);
            UMABroker.SetStart();

            bool isSMA = SMAUtilities.IsSMAClient(new Guid(Cid), UMABroker);
                
            var objUser = (DBUser)UMABroker.GetBMCInstance(Page.User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType == UserType.Client)
            {
                pnlClient.Visible = true;
                pnlAdviser.Visible = false;
                pnlAccountant.Visible = false; 
                pnlAdviserSMA.Visible = false; 
                pnlOverall.Visible = false;
                liClientReconciliation.Visible = false;
            }
            else if (objUser.UserType == UserType.Advisor)
            {
                pnlClient.Visible = false;
                pnlAccountant.Visible = false; 
                if(isSMA)
                    pnlAdviserSMA.Visible = true;
                else
                    pnlAdviser.Visible = true;
                pnlOverall.Visible = false;
                liClientReconciliation.Visible = false;
            }
            else if (objUser.UserType == UserType.Accountant)
            {
                pnlClient.Visible = false;
                pnlAccountant.Visible = true;
                pnlAdviserSMA.Visible = false;
                pnlAdviser.Visible = false;
                pnlOverall.Visible = false;
                liClientReconciliation.Visible = false;
            }
            else
            {
                pnlClient.Visible = false;
                pnlAccountant.Visible = false; 
                pnlAdviser.Visible = false;
                pnlAdviserSMA.Visible = false; 
                pnlOverall.Visible = true;
                liClientReconciliation.Visible = true;
            }

            adviserOD.Visible = (pnlClient.Visible && UMABasePage.CheckAdviserForOrderPad(objUser)) || IsAdminUser();
            if (adviserOD.Visible)
                buyASX.Visible = objUser.UserType == UserType.Innova;
            if (!IsPostBack)
            {
                atCallRatesClient.Visible =
                    atCallRatesOverAll.Visible = cashTransferClient.Visible = cashTransferOverAll.Visible = !isSMA;
                if (objUser.UserType == UserType.Client || objUser.UserType == UserType.Accountant || objUser.UserType == UserType.Advisor)
                {
                    adviserOD.Visible = UMABasePage.CheckAdviserForOrderPad(objUser);// order pad for advisor and Innova user
                    if (adviserOD.Visible)
                        buyASX.Visible = objUser.UserType == UserType.Innova;//buy ASx only for Innova User
                }
                misClient.Visible = misOverAll.Visible = !isSMA || Utilities.IsTestApp();
            }
        }

        public bool IsAdminUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(Page.User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType == UserType.Innova || objUser.Name == "Administrator")
                return true;
            else
                return false;
        }
        private ICMBroker UMABroker
        {
            get
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                return (ICMBroker)Thread.GetData(brokerSlot);
            }
            set
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (value == null)
                    Thread.FreeNamedDataSlot("Broker");

                Thread.SetData(brokerSlot, value);
            }
        }
    }
}