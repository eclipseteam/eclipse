﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="ClassSuperExport.aspx.cs" Inherits="eclipseonlineweb.ClientViews.ClassSuperExport" %>

<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <fieldset>
                <legend>Enable Class Super Data Feeds</legend>
                <br />
                <table width="50%">
                    <tr>
                        <td>
                            <telerik:RadButton ID="rbClassSuperEnable" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton"
                                Checked="false" AutoPostBack="true" OnClick="rbClassSuperEnable_Click">
                                <ToggleStates>
                                    <telerik:RadButtonToggleState Text="'Click here' to disable Super and e-Clipse Online Integration">
                                    </telerik:RadButtonToggleState>
                                    <telerik:RadButtonToggleState Text="'Click here' to enable Class Super and e-Clipse Online Integration">
                                    </telerik:RadButtonToggleState>
                                </ToggleStates>
                            </telerik:RadButton>
                        </td>
                        <td align="left">
                         <asp:Image ID="imgOnline" Visible="false" runat="server"  ImageUrl="~/images/01 icon_online.png" Height="50px" ImageAlign="Middle"  />
                         <asp:Image ID="imgOffline" Visible="false" runat="server"  ImageUrl="~/images/03 icon_offline.png" Height="50px" ImageAlign="Middle"  />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <fieldset>
                <legend>About e-Clpse Online & Class Super Integration</legend><span class="riLabel">
                    <b>e-Clipse Online</b> is built from the ground up with data feed automation. With
                    <b>Class Super</b> integration, <b>e-Clpse Online</b> can now share<br />
                    <br />
                    1. Bank Holdings / Transactions<br />
                    2. Management Funds Holdings / Transactions<br />
                    3. Desktop Broker Holdings / Transactions<br />
                    4. Term Deposits Holdings / Transactions from Australian Money Market & FiiG.<br />
                    <br />
                    Data is shared on a daily basis and where possible automatically reconciled.
                </span>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
