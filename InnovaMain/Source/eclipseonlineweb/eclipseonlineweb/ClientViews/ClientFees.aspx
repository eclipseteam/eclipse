﻿<%@ Page Title="Client Fees - e-Clipse Online Portal" Language="C#" MasterPageFile="ClientViewMaster.master"
    EnableViewState="false" AutoEventWireup="true" CodeBehind="ClientFees.aspx.cs"
    Inherits="eclipseonlineweb.ClientFees" %>

<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function GoToPage(url) {
            window.location = url + '?ins=<%:cid %>';
            return false;
        }
    </script>
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="10%">
                        <asp:ImageButton ID="ibtnSave" AlternateText="Update Account Summary" ToolTip="Save Changes"
                            OnClick="ibtnSave_Click" runat="server" ImageUrl="~/images/Save-Icon.png" />
                    </td>
                    <td style="width: 85%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlClientFeeDetails" Visible="true" EnableViewState="false">
                <fieldset>
                    <legend>Client Fee</legend>
                    <asp:Panel runat="server" ID="pnlUpFrontFee" EnableViewState="false" BackColor="White"
                        Height="50px">
                        <br />
                        <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                            IncrementSettings-InterceptMouseWheel="true" Label="Upfront fee (p.a. inc GST)$:"
                            LabelWidth="160px" runat="server" ID="UpFrontFee" Width="500px">
                        </telerik:RadNumericTextBox>
                        <br />
                        <br />
                        <asp:CheckBox Font-Size="12px" ForeColor="#666666" Font-Names="segoe ui" Text="Upfront Fee Paid?"
                            runat="server" ID="chkFeePaid" />
                    </asp:Panel>
                    <br />
                    <br />
                    <asp:Panel runat="server" Height="50px" ID="pnlSelection" BackColor="White">
                        <br />
                        <telerik:RadButton ID="btnOngoingFeeValue" OnCheckedChanged="RadioCheckChanged_Clicked"
                            OnClick="Button_Click" runat="server" ToggleType="Radio" ButtonType="StandardButton"
                            GroupName="FeeSelection">
                            <ToggleStates>
                                <telerik:RadButtonToggleState Text="Ongoing fee (p.a. inc GST) $" PrimaryIconCssClass="rbToggleRadioChecked" />
                                <telerik:RadButtonToggleState Text="Ongoing fee (p.a. inc GST) $" PrimaryIconCssClass="rbToggleRadio" />
                            </ToggleStates>
                        </telerik:RadButton>
                        <telerik:RadButton ID="btnOngoingFeePercent" OnCheckedChanged="RadioCheckChanged_Clicked"
                            OnClick="Button_Click" runat="server" ToggleType="Radio" ButtonType="StandardButton"
                            GroupName="FeeSelection">
                            <ToggleStates>
                                <telerik:RadButtonToggleState Text="Ongoing fee (p.a. inc GST) %" PrimaryIconCssClass="rbToggleRadioChecked" />
                                <telerik:RadButtonToggleState Text="Ongoing fee (p.a. inc GST) %" PrimaryIconCssClass="rbToggleRadio" />
                            </ToggleStates>
                        </telerik:RadButton>
                        <telerik:RadButton ID="btnOngoingFeeTered" OnCheckedChanged="RadioCheckChanged_Clicked"
                            OnClick="Button_Click" runat="server" ToggleType="Radio" ButtonType="StandardButton"
                            GroupName="FeeSelection">
                            <ToggleStates>
                                <telerik:RadButtonToggleState Text="Ongoing fee Tiered (p.a. inc GST) %" PrimaryIconCssClass="rbToggleRadioChecked" />
                                <telerik:RadButtonToggleState Text="Ongoing fee Tiered (p.a. inc GST) %" PrimaryIconCssClass="rbToggleRadio" />
                            </ToggleStates>
                        </telerik:RadButton>
                        <input type="hidden" name="btnSelected" id="btnSelected" value="" runat="server" />
                    </asp:Panel>
                    <br />
                    <br />
                    <asp:Panel runat="server" ID="pnlOngoingFeeVal" EnableViewState="false" BackColor="White"
                        Height="50px" Visible="false">
                        <br />
                        <telerik:RadNumericTextBox ShowSpinButtons="false" EnableViewState="true" IncrementSettings-InterceptArrowKeys="true"
                            IncrementSettings-InterceptMouseWheel="true" Label="Ongoing fee (p.a. inc GST) $:"
                            LabelWidth="160px" runat="server" ID="OngoingFeeVal" Width="500px">
                        </telerik:RadNumericTextBox>
                        <br />
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlOngoingFeePercent" EnableViewState="false" BackColor="White"
                        Height="50px" Visible="false">
                        <br />
                        <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                            IncrementSettings-InterceptMouseWheel="true" Label="Ongoing fee (p.a. inc GST) %:"
                            LabelWidth="160px" runat="server" ID="OngoingFeePercent" Width="500px">
                        </telerik:RadNumericTextBox>
                        <br />
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlOngoingFeeTiered" EnableViewState="false" BackColor="White"
                        Height="200px" Visible="false">
                        <br />
                        <telerik:RadNumericTextBox ShowSpinButtons="false" EnableViewState="false" IncrementSettings-InterceptArrowKeys="true"
                            IncrementSettings-InterceptMouseWheel="true" Label=" From:" LabelWidth="40px"
                            runat="server" ID="From1" Width="200px">
                        </telerik:RadNumericTextBox>
                        <telerik:RadNumericTextBox EnableViewState="false" ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                            IncrementSettings-InterceptMouseWheel="true" Label=" To:" LabelWidth="20px" runat="server"
                            ID="To1" Width="200px">
                        </telerik:RadNumericTextBox><telerik:RadNumericTextBox EnableViewState="false" ShowSpinButtons="false"
                            IncrementSettings-InterceptArrowKeys="true" IncrementSettings-InterceptMouseWheel="true"
                            Label=" =" LabelWidth="20px" runat="server" ID="Equal1" Width="200px">
                        </telerik:RadNumericTextBox>
                        <br />
                        <br />
                        <telerik:RadNumericTextBox ShowSpinButtons="false" EnableViewState="false" IncrementSettings-InterceptArrowKeys="true"
                            IncrementSettings-InterceptMouseWheel="true" Label=" From:" LabelWidth="40px"
                            runat="server" ID="From2" Width="200px">
                        </telerik:RadNumericTextBox>
                        <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                            IncrementSettings-InterceptMouseWheel="true" Label=" To:" LabelWidth="20px" runat="server"
                            ID="To2" Width="200px">
                        </telerik:RadNumericTextBox><telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                            IncrementSettings-InterceptMouseWheel="true" Label=" =" LabelWidth="20px" runat="server"
                            ID="Equal2" Width="200px">
                        </telerik:RadNumericTextBox>
                        <br />
                        <br />
                        <telerik:RadNumericTextBox ShowSpinButtons="false" EnableViewState="false" IncrementSettings-InterceptArrowKeys="true"
                            IncrementSettings-InterceptMouseWheel="true" Label=" From:" LabelWidth="40px"
                            runat="server" ID="From3" Width="200px">
                        </telerik:RadNumericTextBox>
                        <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                            IncrementSettings-InterceptMouseWheel="true" Label=" To:" LabelWidth="20px" runat="server"
                            ID="To3" Width="200px">
                        </telerik:RadNumericTextBox><telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                            IncrementSettings-InterceptMouseWheel="true" Label=" =" LabelWidth="20px" runat="server"
                            ID="Equal3" Width="200px">
                        </telerik:RadNumericTextBox>
                        <br />
                        <br />
                        <telerik:RadNumericTextBox ShowSpinButtons="false" EnableViewState="false" IncrementSettings-InterceptArrowKeys="true"
                            IncrementSettings-InterceptMouseWheel="true" Label=" From:" LabelWidth="40px"
                            runat="server" ID="From4" Width="200px">
                        </telerik:RadNumericTextBox>
                        <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                            IncrementSettings-InterceptMouseWheel="true" Label=" To:" LabelWidth="20px" runat="server"
                            ID="To4" Width="200px">
                        </telerik:RadNumericTextBox><telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                            IncrementSettings-InterceptMouseWheel="true" Label=" =" LabelWidth="20px" runat="server"
                            ID="Equal4" Width="200px">
                        </telerik:RadNumericTextBox>
                        <br />
                        <br />
                        <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                            IncrementSettings-InterceptMouseWheel="true" Label=" From:" LabelWidth="40px"
                            runat="server" ID="From5" Width="200px">
                        </telerik:RadNumericTextBox>
                        <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                            IncrementSettings-InterceptMouseWheel="true" Label=" To:" LabelWidth="20px" runat="server"
                            ID="To5" Width="200px">
                        </telerik:RadNumericTextBox><telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                            IncrementSettings-InterceptMouseWheel="true" Label=" =" LabelWidth="20px" runat="server"
                            ID="Equal5" Width="200px">
                        </telerik:RadNumericTextBox>
                    </asp:Panel>
                </fieldset>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
