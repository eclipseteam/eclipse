﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="ClientViewByAccType.aspx.cs" Inherits="eclipseonlineweb.ClientViewByAccType" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <style>
        .RadGrid .rgDataDiv
        {
            height: auto !important;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <c1:C1InputDate DateFormat="MMM/yyyy" ID="C1InputStartDate" runat="server" ShowTrigger="true">
                            </c1:C1InputDate>
                        </td>
                        <td style="width: 5%">
                            <c1:C1InputDate DateFormat="MMM/yyyy" ID="C1InputEndDate" runat="server" ShowTrigger="true">
                            </c1:C1InputDate>
                        </td>
                        <td style="width: 5%">
                            <asp:ImageButton OnClick="GenerateReport" runat="server" ID="imgCapitalReport" ImageUrl="~/images/database.png" />
                        </td>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnReport" AlternateText="Print" ToolTip="Print" runat="server"
                                OnClick="BtnReportClick" ImageUrl="~/images/download.png" />
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid OnNeedDataSource="PresentationGridNeedDataSource" ID="PresentationGrid"
                runat="server" AutoGenerateColumns="False" Width="100%" AllowSorting="True" AllowMultiRowSelection="False"
                OnDetailTableDataBind="PresentationGridDetailTableDataBind" OnItemCommand="PresentationGridItemCommand"
                GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" OnInsertCommand="PresentationGridInsertCommand"
                ShowFooter="true" OnItemCreated="PresentationGridItemCreated" OnItemDataBound="PresentationGridItemDataBound">
                <ClientSettings>
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" FrozenColumnsCount="3">
                    </Scrolling>
                </ClientSettings>
                <MasterTableView DataKeyNames="ID" Width="100%" CommandItemDisplay="Top" Name="FinancialSummaryByAccType"
                    TableLayout="Auto">
                    <CommandItemSettings ShowAddNewRecordButton="false" />
                    <Columns>
                        <telerik:GridBoundColumn FilterControlWidth="90px" SortExpression="AccountType" ReadOnly="true"
                            HeaderStyle-Width="110px" ItemStyle-Width="110px" HeaderText="Account Type" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="false" HeaderButtonType="TextButton"
                            DataField="AccountType" UniqueName="AccountType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="180px" SortExpression="AccountNO" ReadOnly="true"
                            HeaderStyle-Width="200px" ItemStyle-Width="200px" HeaderText="Account #" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="false" HeaderButtonType="TextButton"
                            DataField="AccountNO" UniqueName="AccountNO">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="100px" ItemStyle-Width="110px"
                            SortExpression="OpeningBalance" HeaderText="Opening" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="false" Aggregate="Sum" FooterAggregateFormatString="{0:C}"
                            DataFormatString="{0:C}" HeaderButtonType="TextButton" DataField="OpeningBalance"
                            UniqueName="OpeningBalance" ReadOnly="true">
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign='Right' />
                            <HeaderStyle HorizontalAlign="Right" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="110px" ItemStyle-Width="110px"
                            SortExpression="INVESTMENT" HeaderText="Investment" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="false" Aggregate="Sum" FooterAggregateFormatString="{0:C}"
                            DataFormatString="{0:C}" HeaderButtonType="TextButton" DataField="INVESTMENT"
                            UniqueName="INVESTMENT" ReadOnly="true">
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign='Right' />
                            <HeaderStyle HorizontalAlign="Right" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="110px" ItemStyle-Width="110px"
                            SortExpression="TransferIn" HeaderText="Transfer In" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="false" Aggregate="Sum" FooterAggregateFormatString="{0:C}"
                            DataFormatString="{0:C}" HeaderButtonType="TextButton" DataField="TransferIn"
                            UniqueName="TransferIn" ReadOnly="true">
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign='Right' />
                            <HeaderStyle HorizontalAlign="Right" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="110px" ItemStyle-Width="110px"
                            SortExpression="TransferOut" HeaderText="Transfer Out" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="false" Aggregate="Sum" FooterAggregateFormatString="{0:C}"
                            DataFormatString="{0:C}" HeaderButtonType="TextButton" DataField="TransferOut"
                            UniqueName="TransferOut" ReadOnly="true">
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign='Right' />
                            <HeaderStyle HorizontalAlign="Right" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="110px" ItemStyle-Width="110px"
                            SortExpression="Interest" HeaderText="Interest" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="false" Aggregate="Sum" FooterAggregateFormatString="{0:C}" DataFormatString="{0:C}"
                            HeaderButtonType="TextButton" DataField="Interest" UniqueName="Interest" ReadOnly="true">
                            <ItemStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign='Right' />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="110px" ItemStyle-Width="110px"
                            SortExpression="Rental" HeaderText="Rental" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="false" Aggregate="Sum" FooterAggregateFormatString="{0:C}" DataFormatString="{0:C}"
                            HeaderButtonType="TextButton" DataField="Rental" UniqueName="Rental" ReadOnly="true">
                            <ItemStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign='Right' />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="110px" ItemStyle-Width="110px"
                            SortExpression="Dividend" HeaderText="Dividend" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="false" Aggregate="Sum" FooterAggregateFormatString="{0:C}" DataFormatString="{0:C}"
                            HeaderButtonType="TextButton" DataField="Dividend" UniqueName="Dividend" ReadOnly="true">
                            <ItemStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign='Right' />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="110px" ItemStyle-Width="110px"
                            SortExpression="Distribution" HeaderText="Distribution" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="false" Aggregate="Sum" FooterAggregateFormatString="{0:C}"
                            DataFormatString="{0:C}" HeaderButtonType="TextButton" DataField="Distribution"
                            UniqueName="Distribution" ReadOnly="true">
                            <ItemStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign='Right' />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="110px" ItemStyle-Width="110px"
                            SortExpression="Income" HeaderText="Income" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="false" Aggregate="Sum" FooterAggregateFormatString="{0:C}" DataFormatString="{0:C}"
                            HeaderButtonType="TextButton" DataField="Income" UniqueName="Income" ReadOnly="true">
                            <ItemStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign='Right' />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="110px" ItemStyle-Width="110px"
                            SortExpression="Expenses" HeaderText="Expenses" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="false" Aggregate="Sum" FooterAggregateFormatString="{0:C}" DataFormatString="{0:C}"
                            HeaderButtonType="TextButton" DataField="Expenses" UniqueName="Expenses" ReadOnly="true">
                            <ItemStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign='Right' />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="110px" ItemStyle-Width="110px"
                            SortExpression="ChangeInMktVal" HeaderText="Change In Val." AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="false" Aggregate="Sum" FooterAggregateFormatString="{0:C}"
                            DataFormatString="{0:C}" HeaderButtonType="TextButton" DataField="ChangeInMktVal"
                            UniqueName="ChangeInMktVal" ReadOnly="true">
                            <ItemStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign='Right' />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="110px" ItemStyle-Width="110px"
                            SortExpression="ClosingBalance" HeaderText="Closing" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="false" Aggregate="Sum" FooterAggregateFormatString="{0:C}"
                            DataFormatString="{0:C}" HeaderButtonType="TextButton" DataField="ClosingBalance"
                            UniqueName="ClosingBalance" ReadOnly="true">
                            <ItemStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign='Right' />
                            <ItemStyle HorizontalAlign="Right" />
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn HeaderText="" AllowFiltering="False" HeaderStyle-Width="10px">
                            <ItemTemplate>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
