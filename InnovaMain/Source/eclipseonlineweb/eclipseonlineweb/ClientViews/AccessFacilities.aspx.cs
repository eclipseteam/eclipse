﻿using System;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb.ClientViews
{
    public partial class AccessFacilities : UMABasePage
    {
        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];

            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");

            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                btnSave.Visible = false;
            }

            UMABroker.ReleaseBrokerManagedComponent(objUser);
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            BWAAccessFacilitiesDIY.Save();
            BWAAccessFacilitiesDIWM.Save();
        }
    }
}