﻿using System;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class BuyTDCash : UMABasePage
    {
        protected override void Intialise()
        {
            BuyTDCashControl.SaveData += (orderCMCID, ds) =>
            {
                if (orderCMCID == Guid.Empty.ToString())
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(orderCMCID, ds);
                }
            };
        }

        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth" || CheckAdviserForOrderPad(objUser))
                return true;
            return objUser.UserType == UserType.Innova || objUser.Name == "Administrator";
        }

        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            //Setting control required properties
            BuyTDCashControl.IsAdmin = objUser.Administrator;
            BuyTDCashControl.ClientCID = cid;
        }

        
    }
}
