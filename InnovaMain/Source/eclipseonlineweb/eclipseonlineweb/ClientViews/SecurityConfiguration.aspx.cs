﻿using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System;

namespace eclipseonlineweb
{
    public partial class SecurityConfiguration : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if(objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            return true;
        }

        protected void RadListBox_Dropped(object sender, RadListBoxDroppedEventArgs e)
        {
         
        }

        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];
        }
    }
}
