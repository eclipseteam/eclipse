﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Telerik.Web.UI;
using Oritax.TaxSimp.CM;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Common;

namespace eclipseonlineweb.ClientViews
{
    public partial class BGLExport : UMABasePage
    {
        public override void LoadPage()
        {
            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
            {
                sm.RegisterPostBackControl(this.btnTrans);
            }
            cid = Request.QueryString["ins"];
            if (!IsPostBack)
            {
                LoadControls();
            }
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");

            if (objUser.UserType != UserType.Advisor  && objUser.UserType != UserType.Accountant && objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        private void LoadControls()
        {
            LoadData();
        }

        private void LoadData()
        {
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(cid));
            BGLExportDS ds = new BGLExportDS();
            clientData.GetData(ds);
            txtBGLAccountCode.Text = ds.BGLAccountCode;
            startDate.SelectedDate = ds.StartDate;
            endDate.SelectedDate = ds.EndDate;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void ExportTransactions(object sender, EventArgs e)
        {
            IOrganizationUnit clientData = UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            BGLExportDS ds = new BGLExportDS();
            ds.StartDate = this.startDate.SelectedDate.Value;
            ds.EndDate = this.endDate.SelectedDate.Value;
            ds.IsExport = true;
            clientData.GetData(ds);
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            
            txtBGLAccountCode.Text = ds.BGLAccountCode;
            string attachment = "attachment; filename=BGLExport-"+clientData.ClientId+"-"+ clientData.Name +" ["+ ds.StartDate.ToString("ddMMyyyy")+"-"+ ds.EndDate.ToString("ddMMyyyy")+"].xml";
            Response.ClearContent();
            Response.ContentType = "application/xml";
            Response.AddHeader("content-disposition", attachment);
            Response.Write(ds.BGLExportXMLString);
            Response.End(); 
        }

        protected void btnSave_OnClick(object sender, ImageClickEventArgs e)
        {
            SaveData();
        }

        private void SaveData()
        {
            BGLExportDS ds = new BGLExportDS();
            ds.BGLAccountCode = this.txtBGLAccountCode.Text;
            SaveData(cid, ds);
        }
    }
}