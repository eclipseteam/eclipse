﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Security;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;

namespace eclipseonlineweb.ClientViews
{
    public partial class BankAccounts : UMABasePage
    {
        private bool IsAdviserOrClient
        {
            get
            {
                return !string.IsNullOrEmpty(hfIsAdviserOrClient.Value) && Convert.ToBoolean(hfIsAdviserOrClient.Value);
            }
            set
            {
                hfIsAdviserOrClient.Value = value.ToString();
            }
        }

        protected override void Intialise()
        {
            base.Intialise();

            #region Bank Account control
            BankControl.Saved += (CID, CLID, CSID) =>
               {
                   HideShowBankGrid(false);
                   SaveBankData(CID, CLID, CSID, DatasetCommandTypes.Update);
                   PresentationGrid.Rebind();
               };
            BankControl.Canceled += () => HideShowBankGrid(false);
            BankControl.ValidationFailed += () => HideShowBankGrid(true);
            BankControl.SaveData += (bankCid, ds) =>
            {
                if (bankCid == Guid.Empty)
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(bankCid.ToString(), ds);
                }
                HideShowBankGrid(false);

            };
            #endregion

            #region add existing events
            BankAccountSearchControl.Canceled += () => OVER.Visible = BankModalExsiting.Visible = false;
            BankAccountSearchControl.ValidationFailed += () =>
            {
                OVER.Visible = BankModalExsiting.Visible = true;
            };

            BankAccountSearchControl.Saved += bankAccountDS =>
            {
                OVER.Visible = BankModalExsiting.Visible = false;
                SaveBankData(bankAccountDS, DatasetCommandTypes.Update);
            };
            #endregion
        }

        private void SaveBankData(BankAccountDS bankAccountDS, DatasetCommandTypes commandType)
        {
            bankAccountDS.CommandType = commandType;
            SaveData(cid, bankAccountDS);
            PresentationGrid.Rebind();
        }

        private DataSet SaveBankData(Guid CID, Guid CLID, Guid CSID, DatasetCommandTypes commandType)
        {
            IOrganizationUnit orgUnit = UMABroker.GetCMImplementation(CLID, CSID) as IOrganizationUnit;
            
            if (orgUnit.ClientEntity == null)
                orgUnit.ClientEntity = new BankAccountEntity(); 

            BankAccountEntity bankAccountEntity = orgUnit.ClientEntity as BankAccountEntity;
            bankAccountEntity.ParentCID = new Guid(this.cid);
            SaveData(orgUnit as IBrokerManagedComponent); 

            var ds = new BankAccountDS { CommandType = commandType };
            DataRow dr = ds.BankAccountsTable.NewRow();
            dr[ds.BankAccountsTable.CLID] = CLID;
            dr[ds.BankAccountsTable.CSID] = CSID;
            ds.Tables[ds.BankAccountsTable.TABLENAME].Rows.Add(dr);
            SaveData(cid, ds);
            PresentationGrid.Rebind();
            return ds;
        }

        private void HideShowBankGrid(bool show)
        {
            OVER.Visible = BankModal.Visible = show;
        }

        private void ShowHideExisting(bool show)
        {
            OVER.Visible = BankModalExsiting.Visible = show;
        }

        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];

            if (!IsPostBack)
            {
                var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
                if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
                {
                    IsAdviserOrClient = true;
                    BankControl.IsAdviserOrClient = true;
                    PresentationGrid.Columns.FindByUniqueNameSafe("Institution").Visible = false;
                    this.btn.Visible = false;
                }
                UMABroker.ReleaseBrokerManagedComponent(objUser);
            }
        }

        private void GetData()
        {
            cid = Request.QueryString["ins"];

            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(cid));
            var ds = new BankAccountDS();
            clientData.GetData(ds);
            PresentationData = ds;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            var summaryView = new DataView(ds.Tables[ds.BankAccountsTable.TABLENAME]) { Sort = ds.BankAccountsTable.ACCOUNTNAME + " DESC" };
            PresentationGrid.DataSource = summaryView;
        }

        protected void LnkbtnAddBankAccClick(object sender, EventArgs e)
        {
            Guid gCid = Guid.Empty;
            BankControl.SetEntity(gCid, false);
            HideShowBankGrid(true);
        }

        protected void LnkbtnAddExistngBankClick(object sender, EventArgs e)
        {
            BankAccountSearchControl.DisableExitingGrid();
            ShowHideExisting(true);
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var bankCid = new Guid(dataItem["Cid"].Text);
                    BankControl.SetEntity(bankCid, false);
                    HideShowBankGrid(true);
                    break;
                case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var linkEntiyCID = new Guid(dataItem["Cid"].Text);
                    var linkEntiyClid = new Guid(dataItem["CLid"].Text);
                    var linkEntiyCsid = new Guid(dataItem["CSid"].Text);
                    DataSet ds = SaveBankData(linkEntiyCID, linkEntiyClid, linkEntiyCsid, DatasetCommandTypes.Delete);
                    PresentationGrid.Rebind();
                    ScriptManager.RegisterStartupScript(Page, GetType(), "MyScript", "alert('" + ds.ExtendedProperties["Message"] + "');", true);
                    break;
            }
        }

        protected void PresentationGrid_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;

            if (IsAdviserOrClient && (!Convert.ToBoolean(dataItem["IsExternalAccount"].Text) || (Convert.ToBoolean(dataItem["IsExternalAccount"].Text) &&dataItem["AccountType"].Text == "TERMDEPOSIT")))
            {
                var imgEdit = dataItem["EditColumn"].Controls[0] as ImageButton;
                var imgDelete = dataItem["DeleteColumn"].Controls[0] as ImageButton;
                if (imgEdit != null) imgEdit.Visible = false;
                if (imgDelete != null) imgDelete.Visible = false;
            }
        }
    }
}