﻿using System;
using System.Text;
using System.Web.UI;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class OrderDetails : UMABasePage
    {
        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];

            base.LoadPage();
            if (!IsPostBack)
                LoadControls();
        }

        private void LoadControls()
        {
        }

        protected void PresentationGridDetailTableDataBind(object source, GridDetailTableDataBindEventArgs e) { }
        protected void PresentationGridItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "details":
                    break;
                case "undo":
                    break;
                case "settle":
                    break;
            }
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected void PresentationGridItemUpdated(object source, GridUpdatedEventArgs e) { }
        protected void PresentationGridItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void PresentationGridItemInserted(object source, GridInsertedEventArgs e) { }
        protected void PresentationGridInsertCommand(object source, GridCommandEventArgs e)
        {
        }

        protected void PresentationGridItemCreated(object sender, GridItemEventArgs e) { }

        protected void PresentationGridItemDataBound(object sender, GridItemEventArgs e)
        {
        }

        protected void PresentationGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }
    }
}
