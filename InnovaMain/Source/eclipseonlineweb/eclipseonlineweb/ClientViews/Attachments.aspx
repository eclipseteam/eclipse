﻿<%@ Page Title="e-Clipse Online Portal - Configure Fees" Language="C#" MasterPageFile="ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="Attachments.aspx.cs" Inherits="eclipseonlineweb.Attachments" %>

<%@ Register TagPrefix="uc1" TagName="Attachments" Src="~/Controls/Attachments.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function GoToPage(url) {
            window.location = url + '?ins=<%:cid %>';
            return false;
        }
    </script>
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="4%">
                    </td>
                    <td style="width: 85%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
     <uc1:Attachments ID="AttachmentControl" runat="server" />
</asp:Content>
