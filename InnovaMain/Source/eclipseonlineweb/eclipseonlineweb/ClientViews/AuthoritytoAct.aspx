﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="AuthoritytoAct.aspx.cs" Inherits="eclipseonlineweb.AuthoritytoAct" %>

<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="C1.Web.Wijmo.Controls.C1Dialog" Assembly="C1.Web.Wijmo.Controls.4, Version=4.0.20121.56, Culture=neutral, PublicKeyToken=9b75583953471eea" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <telerik:radbutton id="ibtnSave" runat="server" text="RadButton" width="60px" tooltip="Save Changes"
                                onclick="ibtnSave_Click">
                                <contenttemplate>
                                    <img alt="" src="../images/Save-Icon.png" class="btnImageWithText" />
                                    <span class="riLabel">Save</span>
                                </contenttemplate>
                            </telerik:radbutton>
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <div class="MainView">
                <fieldset>
                    <legend>Authority to Act</legend>
                    <br />
                    <a style="text-decoration: underline" onclick="$('#<%:AuthorityToActDialog.ClientID%>').c1dialog('open')">
                        Please click this link to read about e-Clipse Online and Australian Money Market
                        (AMM) Authority to Act Details</a>
                    <br />
                    <br />
                    Client wishes to grant a Limited Power of Attorney to:
                    <br />
                    <br />
                    1.
                    <asp:CheckBox Text="e-clipse Online" runat="server" ID="chkSelectLimitedPowerAttorneyEclipse" /><br />
                    2.
                    <asp:CheckBox Text="Australian Money Market (AMM)" runat="server" ID="chkLimitedPowerAttorneyAMM" />
                    <asp:CheckBox Visible="false" Text="Macquarie CMA" runat="server" ID="chkLimitedPowerAttorneyMacBankCMA" /><br />
                    <br />
                    <b>* If the Client DOES NOT wish to grant a Limited Power of Attorney, DO NOT SELECT
                        ANYTHING ABOVE.<br />
                        The client will be required to complete and sign all relevant account opening forms
                        to establish their e-Clipse UMA Account</b>
                    <h2>
                        Investor Status</h2>
                    <asp:RadioButton GroupName="InvestorStatus" Text=" Wholesale Investor" runat="server"
                        ID="rbWholesaleInvestor" /><br />
                    <asp:RadioButton GroupName="InvestorStatus" Text="" runat="server" ID="rbSophisticatedInvestor" />
                    <a style="text-decoration: underline" onclick="$('#<%:SophisticatedInvestorDialog.ClientID%>').c1dialog('open')">
                        Sophisticated Investor</a><asp:CheckBox Text="Certificate from Accountant is attached"
                            runat="server" ID="chkCertificateFromAccountantAttached" /><br />
                    <asp:RadioButton GroupName="InvestorStatus" Text=" Professional Investor" runat="server"
                        ID="rbProfessionalInvestor" /><br />
                    <asp:RadioButton GroupName="InvestorStatus" Text=" Retail Investor" runat="server"
                        ID="rbRetailInvestor" />
                    <br />
                    <br />                    
                </fieldset>
            </div>
            <cc1:C1Dialog ID="AuthorityToActDialog" runat="server" Width="700px" Height="500px"
                Modal="true" ShowOnLoad="false" Stack="True" CloseText="Close" MaintainStatesOnPostback="true"
                Title="Authority to Act">
                <Content>
                    <p>
                        The agreed limited power of attorney granted to e-Clipse and Australian Money Market
                        (AMM) is to undertake and perform the following on the clients behalf:</p>
                    <p>
                        1. apply for and open new bank and broker accounts with any chosen financial institution,
                        or any other investments;<br />
                        2. authorize for direct debit payments to be acted upon by any chosen institution
                        in order to transfer funds in the relation to new or existing accounts<br />
                        3. instruct in relation to rollovers, maturities, transfer requests of existing
                        investments;<br />
                        4. advise of changes to my/our contact details as advised from time to time;<br />
                        5. notify my/our TFN(s) or Exemption(s) in respect of any existing investment(s)
                        and or new purchase(s) made on my/our behalf.</p>
                    <br />
                    <p>
                        I/We understand that e-Clipse and AMM will provide me/us with a copy of the relevant
                        terms and conditions relating to any account which e-Clipse and AMM proposes to
                        open in my/our name prior to the account being opened. I/We am aware that I/we must
                        read and understand those terms and conditions and contact e-Clipse and AMM if I/we
                        do not agree to be bound by them.</p>
                    <p>
                        This authority is given only to act as per instructions given by me/us via the e-Clipse
                        UMA platform or by any other means of documented instruction.</p>
                </Content>
            </cc1:C1Dialog>
            <cc1:C1Dialog ID="SophisticatedInvestorDialog" runat="server" Width="700px" Height="500px"
                Modal="true" ShowOnLoad="false" Stack="True" CloseText="Close" Title="" MaintainStatesOnPostback="true">
                <Content>
                    <h2>
                        Wholesale or Retail Investors</h2>
                    <p>
                        To qualify as a Sophisticated or Professional Investor under Section 708 of the
                        Corporations Act you need to be able to meet one of the conditions below. Please
                        ensure that you include documentation to support your situation. The Investment
                        Program to be implemented for you under the Managed Account Service will need to
                        have regard to whether you are a wholesale or retail investor. A sophisticated investor
                        (see below) is a client equipped to be considered a wholesale client in relation
                        to the UMA service provided by e-Clipse. To be considered a wholesale investor as
                        defined under the Corporations Act 2001 (cth) (Act) you must:</p>
                    <p>
                        i. Be investing an amount of at least $500,000 in e-Clipse; or</p>
                    <p>
                        ii. Meet the "sophisticated investor" criteria under the Act (see below).</p>
                    <p>
                        In the absence of a certificate from a qualified accountant where the amount invested
                        is less than $500,000, we must consider you a retail investor.</p>
                    To qualify as a Sophisticated or Professional Investor under Section 708 of the
                    Corporations Act you need to be able to meet one of the conditions below.</label>
                    <h2>
                        Sophisticated Investors</h2>
                    <p>
                        To qualify as a sophisticated investor please supply a certificate provided by a
                        qualified accountant or eligible foreign professional body which states that you
                        have:</p>
                    a. net assets of at least $2.5 million; or<br />
                    b. gross income for each of the last two financial years of at least $250,000 a
                    year."<br />
                    <p>
                        The certificate must include:</p>
                    <p>
                        1. the class membership of the professional body to which the accountant belongs
                        (as outlined below)</p>
                    <p>
                        2. the date of issue</p>
                    <p>
                        3. the Chapter of Chapters of the Corporations Act under which it is issued</p>
                    <p>
                        If you control a company or trust and meet the requirements above, the company or
                        trust may also qualify for exempt status as a sophisticated investor.</p>
                    <h2>
                        Qualified Accountant</h2>
                    <p>
                        A qualified Accountant is defined in s88B of the Act as a person meeting the criteria
                        in a class declaration made by ASIC. Under ASIC's class order (CO 01/1256) you are
                        a qualified accountant if you: a. belong to one of the following professional bodies
                        in the declared membership classification (below) and; b. you comply with your body's
                        continuing professional education requirements.</p>
                    <table>
                        <tr>
                            <th align="left">
                                Professional Bodies
                            </th>
                            <th align="left">
                                Declared Membership Classification
                            </th>
                        </tr>
                        <tr class="odd">
                            <td height="25">
                                <label>
                                    The Insitute of Chartered Accountants in Australia</label>
                            </td>
                            <td>
                                <label>
                                    CA, ACA and FCA</label>
                            </td>
                        </tr>
                        <tr class="even">
                            <td height="25">
                                <label>
                                    CPA Australia</label>
                            </td>
                            <td>
                                <label>
                                    CPA and FCPA</label>
                            </td>
                        </tr>
                        <tr class="odd">
                            <td height="25">
                                <label>
                                    National Institute of Accountants in Australia</label>
                            </td>
                            <td>
                                <label>
                                    PNA, FPNA, MINA and FINA</label>
                            </td>
                        </tr>
                    </table>
                </Content>
                <ExpandingAnimation Duration="400" />
                <CollapsingAnimation Duration="300" />
            </cc1:C1Dialog>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
