﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Security;
using System.Collections;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.WebControls;

namespace eclipseonlineweb
{
    public partial class ManagedFundsTransactions : UMABasePage
    {

        string transactionCID = string.Empty; 

        protected override void Intialise()
        {
            base.Intialise();

            Details.Saved += (MISCID, TRANSID, fundCode) =>
             {
                 ModalPopupExtenderEditTran.Hide();
                 PresentationGrid.Rebind();
             };
            Details.Canceled += () => ModalPopupExtenderEditTran.Hide();
            Details.ValidationFailed += () => ModalPopupExtenderEditTran.Show();
            Details.SaveData += (miscid, ds) => SaveData(miscid.ToString(), ds);
        }

        protected void LnkbtnAddNewClick(object sender, EventArgs e)
        {
            Details.SetEntity(null, ClientHeaderInfo1.ClientId);
            ModalPopupExtenderEditTran.Show();
        }

        protected void BtnReportClick(object sender, EventArgs e)
        {
            BulkTransactionsOutput(this.cid, this.ClientHeaderInfo1.ClientName);
        }

        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");

            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                PresentationGrid.Columns.FindByUniqueNameSafe("DeleteColumn").Visible = false;
                PresentationGrid.Columns.FindByUniqueNameSafe("EditColumn").Visible = false;
            }
            ScriptManager sm = ScriptManager.GetCurrent(Page);

            if (sm != null)
                sm.RegisterPostBackControl(btnReport);
        }

        private void GetData(bool isCommand = false)
        {
            cid = Request.QueryString["ins"];
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(cid));

            var misDS = new MISTransactionDS();
            clientData.GetData(misDS);

            PresentationData = misDS;
            if (misDS.Tables.Count > 0)
            {
                var misView = new DataView(misDS.Tables[0]) { Sort = "TradeDate DESC" };
                if (!isCommand)
                    PresentationGrid.DataSource = misView;
            }
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        private void SetEditingData(GridEditManager editMan, ref GridDropDownListColumnEditor ddlTranType, ref GridDropDownListColumnEditor ddlAccList)
        {
            ddlTranType = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("DDLTranType"));
            RadComboBox ddlCatTranTypeRadCombo = ddlTranType.ComboBoxControl;
            ddlCatTranTypeRadCombo.Filter = RadComboBoxFilter.Contains;
            RadComboBoxItem ddlCatTranTypeRadComboDefaultItem = new RadComboBoxItem("", "");
            ddlCatTranTypeRadComboDefaultItem.Selected = true;
            ddlCatTranTypeRadCombo.Items.Add(ddlCatTranTypeRadComboDefaultItem);
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Adjustment Down", "AJ"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Adjustment Up", "RJ"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Application", "AP"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Bonus Issue", "AM"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Purchase", "AN"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Commutation", "RU"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Contribution", "AC"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Contribution Tax", "RL"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Deposit", "AD"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Expense", "EX"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Instalment", "LM"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Insurance Premium", "RH"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Lodgement", "AL"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Lump Sum Tax", "RK"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Manager Fee", "MA"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Opening (Initial) Balance", "IN"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("PAYG", "RY"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Pension Payment", "PR"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Pension Payment – Income Stream", "RR"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Purchase Reinvestment", "VB"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Reversal - Redemption / Withdrawal / Fee / Tax", "VA"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Rights Issue", "AE"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Redemption", "RA"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Surcharge Tax", "ST"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Switch In", "AS"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Switch Out", "RS"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Tax", "RX"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Tax Rebate", "AR"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Transfer In", "AT"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Transfer Out", "RT"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Withdrawal", "RW"));

            IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var securityList = iorg.Securities.Where(c => c.AsxCode.StartsWith("IV0")).OrderBy(c => c.AsxCode);

            ddlAccList = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("DDLAccountList"));
            RadComboBox ddlAccListRadCombo = ddlAccList.ComboBoxControl;
            ddlAccListRadCombo.Filter = RadComboBoxFilter.Contains;
            ddlAccListRadCombo.DataSource = securityList;
            ddlAccListRadCombo.DataTextField = "AsxCode";
            ddlAccListRadCombo.DataValueField = "AsxCode";
            ddlAccListRadCombo.DataBind();
            RadComboBoxItem ddlAccListRadComboItem = new RadComboBoxItem("", "");
            ddlAccListRadComboItem.Selected = true;
            ddlAccListRadCombo.Items.Add(ddlAccListRadComboItem);
        }


        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;
                GridDropDownListColumnEditor ddlTranType = null;
                GridDropDownListColumnEditor accListType = null;

                SetEditingData(editMan, ref ddlTranType, ref accListType);

                string impTranType = String.Empty;
                string accType = String.Empty;
                string accountCID = String.Empty;

                var pItem = ((GridEditFormItem)(e.Item)).ParentItem;
                if (pItem != null)
                {
                    impTranType = pItem["TransactionType"].Text == "&nbsp;" ? "" : pItem["TransactionType"].Text;
                    accType = pItem["Code"].Text == "&nbsp;" ? "" : pItem["Code"].Text;
                    accountCID = pItem["MISCID"].Text == "&nbsp;" ? "" : pItem["MISCID"].Text;
                }

                if (impTranType != string.Empty)
                    ddlTranType.ComboBoxControl.Items.FindItemByText(impTranType).Selected = true;

                if (accType != string.Empty)
                    accListType.ComboBoxControl.Items.FindItemByText(accType).Selected = true;
            }
        }

        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            IBrokerManagedComponent clientBMC = this.UMABroker.GetBMCInstance(new Guid(this.cid));

            GridEditFormInsertItem EditForm = (GridEditFormInsertItem)e.Item;
            Hashtable values = new Hashtable();
            EditForm.ExtractValues(values);

            RadComboBox comboTrans = (RadComboBox)EditForm["DDLTranType"].Controls[0];
            RadComboBox comboAccList = (RadComboBox)EditForm["DDLAccountList"].Controls[0];

            var misTransactionDetailsDS = new MISTransactionDetailsDS { DataSetOperationType = DataSetOperationType.NewSingle };
            DataRow row = misTransactionDetailsDS.Tables[MISTransactionDetailsDS.MISTRANSDETAILSTABLE].NewRow();
            row[MISTransactionDetailsDS.ID] = Guid.NewGuid();
            row[MISTransactionDetailsDS.CLIENTID] = clientBMC.EclipseClientID;
            row[MISTransactionDetailsDS.SYSTRANTYPE] = comboTrans.Text;
            row[MISTransactionDetailsDS.UNITPRICE] = ((RadNumericTextBox)(((GridEditableItem)(e.Item))["UnitPrice"].Controls[0])).Text;
            row[MISTransactionDetailsDS.SHARES] = ((RadNumericTextBox)(((GridEditableItem)(e.Item))["Shares"].Controls[0])).Text;
            row[MISTransactionDetailsDS.FUNDCODE] = comboAccList.SelectedValue;

            if (((RadDatePicker)(((GridEditableItem)(e.Item))["TradeDate"].Controls[0])).SelectedDate.Value != null)
                row[MISTransactionDetailsDS.TRADEDATE] = ((RadDatePicker)(((GridEditableItem)(e.Item))["TradeDate"].Controls[0])).SelectedDate.Value;

            misTransactionDetailsDS.Tables[MISTransactionDetailsDS.MISTRANSDETAILSTABLE].Rows.Add(row);

            SaveData("B354E928-A3C2-43D7-B61E-FEB215CF1BD3", misTransactionDetailsDS);
            e.Canceled = true;
            e.Item.Edit = false;
            this.GetData(true);
            this.PresentationGrid.Rebind();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    var dataItem = (GridDataItem)e.Item;
                    lblTranID.Text = dataItem["ID"].Text;
                    lblMISCID.Text = dataItem["MISCID"].Text;
                    lblOLDFUNDCODE.Text = dataItem["Code"].Text;
                    break;
                case "update":
                    {
                        IBrokerManagedComponent clientBMC = this.UMABroker.GetBMCInstance(new Guid(this.cid));
                        GridEditFormItem EditForm = (GridEditFormItem)e.Item;
                        RadComboBox comboTrans = (RadComboBox)EditForm["DDLTranType"].Controls[0];
                        RadComboBox comboAccList = (RadComboBox)EditForm["DDLAccountList"].Controls[0];

                        var misTransactionDetailsDS = new MISTransactionDetailsDS { DataSetOperationType = DataSetOperationType.UpdateSingle };
                        DataRow row = misTransactionDetailsDS.Tables[MISTransactionDetailsDS.MISTRANSDETAILSTABLE].NewRow();
                        row[MISTransactionDetailsDS.ID] = lblTranID.Text;
                        row[MISTransactionDetailsDS.FUNDCODEOLD] = lblOLDFUNDCODE.Text;
                        row[MISTransactionDetailsDS.SYSTRANTYPE] = comboTrans.Text;
                        row[MISTransactionDetailsDS.FUNDCODE] = comboAccList.Text;
                        row[MISTransactionDetailsDS.CLIENTID] = clientBMC.EclipseClientID;
                        row[MISTransactionDetailsDS.UNITPRICE] = ((RadNumericTextBox)(((GridEditableItem)(e.Item))["UnitPrice"].Controls[0])).Text;
                        row[MISTransactionDetailsDS.SHARES] = ((RadNumericTextBox)(((GridEditableItem)(e.Item))["Shares"].Controls[0])).Text;
                        if (((RadDatePicker)(((GridEditableItem)(e.Item))["TradeDate"].Controls[0])).SelectedDate.Value != null)
                            row[MISTransactionDetailsDS.TRADEDATE] = ((RadDatePicker)(((GridEditableItem)(e.Item))["TradeDate"].Controls[0])).SelectedDate.Value;
                        misTransactionDetailsDS.Tables[MISTransactionDetailsDS.MISTRANSDETAILSTABLE].Rows.Add(row);
                        SaveData(lblMISCID.Text, misTransactionDetailsDS);
                        e.Canceled = true;
                        e.Item.Edit = false;
                        this.GetData(true);
                        PresentationGrid.Rebind();
                        break;
                    }
                case "delete":
                    var dataItemDel = (GridDataItem)e.Item;
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var transactionDetailsDS = new MISTransactionDetailsDS();
                    DataRow rowDel = transactionDetailsDS.Tables[MISTransactionDetailsDS.MISTRANSDETAILSTABLE].NewRow();
                    rowDel[MISTransactionDetailsDS.ID] = new Guid(dataItemDel["ID"].Text);
                    rowDel[MISTransactionDetailsDS.FUNDCODE] = dataItemDel["Code"].Text;
                    transactionDetailsDS.Tables[MISTransactionDetailsDS.MISTRANSDETAILSTABLE].Rows.Add(rowDel);
                    transactionDetailsDS.DataSetOperationType = DataSetOperationType.DeletSingle;
                    SaveData(dataItemDel["MISCID"].Text, transactionDetailsDS);
                    PresentationGrid.Rebind();
                    break;
            }
        }
    }
}
