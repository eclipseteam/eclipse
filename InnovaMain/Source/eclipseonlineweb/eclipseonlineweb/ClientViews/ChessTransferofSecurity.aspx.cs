﻿using System.Web.UI;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class ChessTransferofSecurity : UMABasePage
    {
        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                btnSave.Visible = false;
            }
            UMABroker.ReleaseBrokerManagedComponent(objUser);
        }

        protected void btnSave_OnClick(object sender, ImageClickEventArgs e)
        {
            SecuritiesChessControl.Save();
        }
    }
}
