﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;

// ReSharper disable CheckNamespace
namespace eclipseonlineweb
// ReSharper restore CheckNamespace
{
    public partial class NewClientMainView : UMABasePage
    {
        private void BindBreakDownGridTD(string tdId)
        {
            var ds = PresentationData as HoldingRptDataSet;
            if (ds != null)
            {
                var tdsummaryView = new DataView(ds.TDBreakDownTable)
                {
                    RowFilter = ds.TDBreakDownTable.TDID + "='" + tdId + "'",
                    Sort = string.Format("{0} Asc,{1} Asc", ds.TDBreakDownTable.TRANSDATE, ds.TDBreakDownTable.TRANSTYPE)
                };
                var tdfilter = AddCumulativeAmount(tdsummaryView.ToTable(), ds.TDBreakDownTable.HOLDING).DefaultView;
                tdfilter.Sort = string.Format("{0} DESC,{1} DESC", ds.TDBreakDownTable.TRANSDATE, ds.TDBreakDownTable.TRANSTYPE);

                gvTDBreakDown.DataSource = tdfilter;
                gvTDBreakDown.DataBind();
            }
        }

        /// <summary>
        /// Calculate and Add Cumulative Amount Balance Column in datatable
        /// </summary>
        /// <param name="dataTable">Datatable to use</param>
        /// <param name="holdingCol">Values Column</param>
        /// <returns>Datatable</returns>
        private DataTable AddCumulativeAmount(DataTable dataTable, string holdingCol)
        {
            const string colName = "CumulativeAmount";
            if (!dataTable.Columns.Contains(colName))
                dataTable.Columns.Add(colName, typeof(decimal));

            decimal cumulativeAmount = 0;
            foreach (DataRow dr in dataTable.Rows)
            {
                decimal holding;
                decimal.TryParse(dr[holdingCol].ToString().Trim(), out holding);
                cumulativeAmount = cumulativeAmount + holding;
                dr[colName] = cumulativeAmount;
            }
            return dataTable;
        }

        private void BindBreakDownGrid(string productID, string serviceType)
        {
            var ds = PresentationData as HoldingRptDataSet;
            if (ds != null)
            {
                var prosummaryView = new DataView(ds.ProductBreakDownTable)
                {
                    RowFilter =
                        ds.ProductBreakDownTable.PRODUCTID + "='" + productID + "' and " +
                        ds.ProductBreakDownTable.SERVICETYPE + "='" + serviceType + "'",
                    Sort = ds.ProductBreakDownTable.INVESMENTCODE + " ASC"
                };
                DataTable sortedFilteredTable = prosummaryView.ToTable();

                gvBreakBown.DataSource = sortedFilteredTable;
                gvBreakBown.DataBind();
            }
        }
        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "linked")
            {
                string[] args = e.CommandArgument.ToString().Split('_');

                string assetName = args[0];
                string tdId = args[1];
                string productId = args[2];
                string description = args[3];
                string serviceType = args[4];
                if ((assetName == "FI" && tdId != string.Empty) || assetName == "TDs" || description.Contains("Term Deposit Account"))
                {
                    if (tdId != string.Empty)
                    {
                        BindBreakDownGridTD(tdId);
                        gvBreakBown.Visible = false;
                        lnkBackToSummary.Visible = true;
                        gvFinancialSummary.Visible = false;
                    }
                }
                else
                {
                    if (productId != string.Empty)
                    {
                        BindBreakDownGrid(productId, serviceType);
                        lnkBackToSummary.Visible = true;
                        gvBreakBown.Visible = true;
                        gvFinancialSummary.Visible = false;
                    }
                }
            }
        }
        protected void FinancialSummary_RowCommand(object sender, C1GridViewCommandEventArgs e)
        {
            if (e.CommandName != "filter" & e.CommandName != "sort")
            {
                string[] args = e.CommandArgument.ToString().Split('_');
                string assetName = args[0];
                string tdID = args[1];
                string productID = args[2];
                string description = args[3];
                string serviceType = args[4];
                if ((assetName == "FI" && tdID != string.Empty) || assetName == "TDs" || description.Contains("Term Deposit Account"))
                {
                    if (tdID != string.Empty)
                    {
                        BindBreakDownGridTD(tdID);
                        lnkBackToSummary.Visible = true;
                    }
                }
                else
                {
                    if (productID != string.Empty)
                    {
                        BindBreakDownGrid(productID, serviceType);
                        lnkBackToSummary.Visible = true;
                        gvBreakBown.Visible = false;
                    }
                }
            }
        }

        /// <summary>
        /// Seting User Type and Is admin hidden fileds
        /// </summary>
        private void SetUserTypes()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            hidUserType.Value = objUser.UserType.ToString();
            hidIsAdmin.Value = objUser.Administrator.ToString();
        }

        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];
            var clientData = UMABroker.GetBMCInstance(new Guid(cid));
            HoldingRptDataSet ds = new HoldingRptDataSet();
            if (!Page.IsPostBack)
            {
                ds.IsMainView = true;
                ds.CurrentUser = GetCurrentUser("Administrator");
            }
            SetUserTypes();
            //if (C1FinancialSummaryDate.Date.HasValue)
            //    ds.ValuationDate = C1FinancialSummaryDate.Date.Value;

            if (DpFinancialSummary.SelectedDate.HasValue)
                ds.ValuationDate = DpFinancialSummary.SelectedDate.Value;

            clientData.GetData(ds);
            FilterRecords(ds);
            PresentationData = ds;
            PopulatePage(PresentationData);
            BindFinancialSummary(ds);
            var sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
                sm.RegisterPostBackControl(btnExportReport);
            //Here we check the Page from where Ins come
            string pageName = Request.QueryString["PageName"];
            if (!Page.IsPostBack)
                DpFinancialSummary.SelectedDate = DateTime.Today;
        }

        /// <summary>
        /// Filter records with 0 Holding and 0 Total, excluding desktop broker and bank accounts with account type CMA.
        /// </summary>
        /// <param name="ds">HoldingRptDataSet</param>
        private void FilterRecords(HoldingRptDataSet ds)
        {
            //Filter Record for Ticket No. 893
            if (ds.Tables.Count != 0)
            {
                ds.Tables["HoldingSummary"].Columns.Add("RemoveRow", typeof(Boolean));
                foreach (DataRow objDr in ds.Tables["HoldingSummary"].Rows)
                {
                    if (objDr["LinkedEntityType"].ToString() != OrganizationType.DesktopBrokerAccount.ToString() &&
                        !(objDr["LinkedEntityType"].ToString() == OrganizationType.BankAccount.ToString() &&
                          objDr["ProductName"].ToString().ToLower().Contains("cma")))
                    {
                        if (Decimal.Parse(objDr["Holding"].ToString()) == 0 &&
                            Decimal.Parse(objDr["Total"].ToString()) == 0)
                        {
                            objDr["RemoveRow"] = true;
                        }
                    }
                }

                DataRow[] drRowsToDelete = ds.Tables["HoldingSummary"].Select("RemoveRow='true'");
                foreach (DataRow dr in drRowsToDelete)
                {
                    ds.Tables["HoldingSummary"].Rows.Remove(dr);
                }
            }
        }

        private void BindFinancialSummary(HoldingRptDataSet ds)
        {
            var summaryView = new DataView(ds.HoldingSummaryTable)
            {
                Sort =
                    string.Format("{0}, {1} ASC", ds.HoldingSummaryTable.SERVICETYPE, ds.HoldingSummaryTable.ASSETNAME)
            };
            gvFinancialSummary.DataSource = summaryView.ToTable();
            gvFinancialSummary.DataBind();
        }

        private void LoadFinancialGrid(HoldingRptDataSet ds)
        {
            var summaryView = new DataView(ds.HoldingSummaryTable)
            {
                Sort =
                    string.Format("{0}, {1} ASC", ds.HoldingSummaryTable.SERVICETYPE, ds.HoldingSummaryTable.ASSETNAME)
            };
            gvFinancialSummary.DataSource = summaryView.ToTable();
        }

        protected void gvFinancial_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            LoadFinancialGrid(PresentationData as HoldingRptDataSet);
        }

        protected void gvFinancialSummary_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridGroupHeaderItem)
            {
                GridGroupHeaderItem item = (GridGroupHeaderItem)e.Item;
                DataRowView groupDataRow = (DataRowView)e.Item.DataItem;
                item.DataCell.Text = string.Format("{0} : Settled ($) {1} : Unsettled ($) {2} : Total ($) {3}", groupDataRow["ServiceType"], Math.Round(Convert.ToDecimal(groupDataRow["Settled"]), 2), Convert.ToDouble(groupDataRow["Unsettled"]).ToString("###,##0.00"), Math.Round(Convert.ToDecimal(groupDataRow["Total"]), 2));
                item.DataCell.Font.Bold = true;
            }

            if (!(e.Item is GridDataItem)) return;



            var dataItem = e.Item as GridDataItem;
            string modelName = dataItem["MODELNAME"].Text;
            string serviceType = ReturnServicesType(modelName);
            if ((hidIsAdmin.Value.ToLower() == "true" || hidUserType.Value == UserType.Innova.ToString()) && !string.IsNullOrEmpty(serviceType))
            {
                dataItem["MODELNAME"].Text = modelName;
            }
            else
            {
                if (serviceType == "Do It For Me")
                {
                    dataItem["MODELNAME"].Text = modelName;
                }
                else
                {
                    dataItem["MODELNAME"].Text = serviceType;
                }
            }
            var assetName = dataItem["AssetName"].Text;
            var description = dataItem["Description"].Text;
            var tdId = dataItem["TDID"].Text;
            var productId = dataItem["ProductID"].Text;
            if (assetName == "TDs" || description.Contains("Term Deposit Account"))
            {
                if (tdId != string.Empty)
                {
                    var linkDesc = (LinkButton)e.Item.FindControl("linkDesc");
                    linkDesc.Visible = true;
                    e.Item.FindControl("lblDesc").Visible = false;
                }
            }
            else
            {
                if (productId != string.Empty && productId != "&nbsp;")
                {
                    var linkDesc = (LinkButton)e.Item.FindControl("linkDesc");
                    linkDesc.Visible = true;
                    e.Item.FindControl("lblDesc").Visible = false;
                }
            }
        }

        protected void FinancialSummary_RowDataBound(object sender, C1GridViewRowEventArgs e)
        {
            if (e.Row.DataItem != null)
            {
                //Do It For Me: 0111 - DIWM Bank, Broker, Fixed Interest & Innova
                string modelName = ((DataRowView)(e.Row.DataItem)).Row["MODELNAME"].ToString();
                string serviceType = ReturnServicesType(modelName);
                if ((hidIsAdmin.Value.ToLower() == "true" || hidUserType.Value == UserType.Innova.ToString()) && !string.IsNullOrEmpty(serviceType))
                {
                    //if user is Admin
                    e.Row.Cells[2].Text = modelName;
                }
                else
                {
                    if (serviceType == "Do It For Me")
                    {
                        e.Row.Cells[2].Text = modelName;
                    }
                    else
                    {
                        //if the selected User are Not Admin
                        e.Row.Cells[2].Text = serviceType;
                    }
                }

                var assetName = ((DataRowView)(e.Row.DataItem)).Row["AssetName"].ToString();
                var description = ((DataRowView)(e.Row.DataItem)).Row["Description"].ToString();
                var tdID = ((DataRowView)(e.Row.DataItem)).Row["TDID"].ToString();
                var productID = ((DataRowView)(e.Row.DataItem)).Row["ProductID"].ToString();

                if (assetName == "TDs" || description.Contains("Term Deposit Account"))
                {
                    if (tdID != string.Empty)
                    {
                        var linkDesc = (LinkButton)e.Row.FindControl("linkDesc");
                        linkDesc.Visible = true;
                        e.Row.FindControl("lblDesc").Visible = false;
                    }
                }
                else
                {
                    if (productID != string.Empty)
                    {
                        var linkDesc = (LinkButton)e.Row.FindControl("linkDesc");
                        linkDesc.Visible = true;
                        e.Row.FindControl("lblDesc").Visible = false;
                    }
                }
            }
        }

        protected void BackToSummary(object sender, EventArgs args)
        {
            BindFinancialSummary(PresentationData as HoldingRptDataSet);
            gvBreakBown.Visible = false;
            gvFinancialSummary.Visible = true;
            lnkBackToSummary.Visible = false;
            gvTDBreakDown.Visible = false;
        }
        protected void btnBack_Click(object sender, EventArgs args)
        {
            string PageName = Request.QueryString["PageName"];
            if (PageName == "GroupsFUM")
                Response.Redirect("~/GroupsFUM.aspx");
            else if (PageName == "AccountFumCompliance")
                Response.Redirect("~/SysAdministration/AccountsFUMCompliance.aspx");
            else if (PageName == "AccountFUM")
                Response.Redirect("~/AccountsFUM.aspx");
        }
        /// <summary>
        /// Get the Service Type
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string ReturnServicesType(string str)
        {
            string[] strArray = str.Split(':');
            if (strArray.Length > 0)
                return strArray[0];
            return string.Empty;
        }

        private void MainView()
        {
            BindFinancialSummary(PresentationData as HoldingRptDataSet);
            pnlFinancialSummary.Visible = true;
            pnlHoldingChart.Visible = false;
            pnlToolBar.Visible = true;
        }

        protected void GenerateFinancialSummary(object sender, EventArgs e)
        {
            var clientData = UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"]));

            //if (C1FinancialSummaryDate.Date != null)
            //{
            //    var ds = new HoldingRptDataSet { ValuationDate = C1FinancialSummaryDate.Date.Value };
            if (DpFinancialSummary.SelectedDate != null)
            {
                var ds = new HoldingRptDataSet { ValuationDate = DpFinancialSummary.SelectedDate.Value };

                clientData.GetData(ds);
                PresentationData = ds;

                BindFinancialSummary(ds);
            }
            pnlFinancialSummary.Visible = true;
            pnlHoldingChart.Visible = false;
            pnlToolBar.Visible = true;

            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }



        protected void Filter(object sender, C1GridViewFilterEventArgs e)
        {
            e.Values[0] = ((string)e.Values[0]).Trim();

            var ds = PresentationData as HoldingRptDataSet;
            if (ds != null)
            {
                var summaryView = new DataView(ds.HoldingSummaryTable)
                {
                    Sort =
                        string.Format("{0}, {1} ASC", ds.HoldingSummaryTable.SERVICETYPE, ds.HoldingSummaryTable.ASSETNAME)
                };
            }
        }

        protected void FinancialSummarySorting(object sender, C1GridViewSortEventArgs e)
        {
            MainView();
        }

        #region Events

        protected void MainView_Click(object sender, EventArgs e)
        {
            MainView();
        }

        protected void Refresh_Click(object sender, EventArgs e)
        {
            cid = Request.QueryString["ins"];
            var clientData = UMABroker.GetBMCInstance(new Guid(cid));
            UMABroker.ReleaseBrokerManagedComponent(clientData);

            clientData = UMABroker.GetBMCInstance(new Guid(cid));

            var ds = new HoldingRptDataSet();

            //if (C1FinancialSummaryDate.Date.HasValue)
            //    ds.ValuationDate = C1FinancialSummaryDate.Date.Value;

            if (DpFinancialSummary.SelectedDate.HasValue)
                ds.ValuationDate = DpFinancialSummary.SelectedDate.Value;

            clientData.GetData(ds);
            PresentationData = ds;


            PopulatePage(PresentationData);
            BindFinancialSummary(ds);

            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void Chart_Click(object sender, EventArgs e)
        {
            pnlFinancialSummary.Visible = false;
            pnlHoldingChart.Visible = true;
            pnlToolBar.Visible = true;
            var ds = PresentationData as HoldingRptDataSet;
            if (ds == null) return;
            var holdingCollection = ds.HoldingSummaryTable.AsEnumerable();
            var holdingCollectionGroup = holdingCollection.GroupBy(holdColl => holdColl[ds.HoldingSummaryTable.ASSETNAME]);
            var difmCollection = holdingCollection.Where(holdColl => holdColl[ds.HoldingSummaryTable.SERVICETYPE].ToString() == "Do It For Me");
            var diyCollection = holdingCollection.Where(holdColl => holdColl[ds.HoldingSummaryTable.SERVICETYPE].ToString() == "Do It Yourself");
            var diwmCollection = holdingCollection.Where(holdColl => holdColl[ds.HoldingSummaryTable.SERVICETYPE].ToString() == "Do It With Me");
            var manCollection = holdingCollection.Where(holdColl => holdColl[ds.HoldingSummaryTable.SERVICETYPE].ToString() == "Manual Asset");
            var collectionGroup = holdingCollectionGroup as IGrouping<object, DataRow>[] ?? holdingCollectionGroup.ToArray();
            if (difmCollection.Any())
            {
                var difmSeries = new C1.Web.Wijmo.Controls.C1Chart.BarChartSeries { Label = "DO IT FOR ME" };
                var difmCollectionGroup = difmCollection.GroupBy(holdColl => holdColl[ds.HoldingSummaryTable.ASSETNAME]);
                foreach (var group in difmCollectionGroup)
                {
                    difmSeries.Data.X.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartXData(@group.Key.ToString()));

                    var subTotal = @group.Sum(row => Convert.ToDouble(row[ds.HoldingSummaryTable.HOLDING].ToString()));

                    difmSeries.Data.Y.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartYData(subTotal));
                }
                foreach (var group in collectionGroup)
                {
                    if (!difmSeries.Data.X.Values.Contains(@group.Key))
                    {
                        difmSeries.Data.X.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartXData(@group.Key.ToString()));
                        difmSeries.Data.Y.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartYData(0));
                    }
                }
                var chartStyle = new C1.Web.Wijmo.Controls.C1Chart.ChartStyle
                {
                    Stroke = Color.FromArgb(127, 199, 60),
                    Opacity = 0.8,
                    Fill = { Color = Color.FromArgb(142, 222, 67) }
                };
                C1BarChartHoldingSumDIFM.SeriesStyles.Add(chartStyle);
                C1BarChartHoldingSumDIFM.SeriesList.Add(difmSeries);
                C1BarChartHoldingSumDIFM.Visible = true;
            }

            if (difmCollection.Any())
            {
                var difmCollectionGroup =
                    difmCollection.GroupBy(holdColl => holdColl[ds.HoldingSummaryTable.ASSETNAME]);
                foreach (var group in difmCollectionGroup)
                {
                    var difmSeries = new C1.Web.Wijmo.Controls.C1Chart.PieChartSeries
                    {
                        Label = @group.Key.ToString()
                    };

                    var subTotal = @group.Sum(row => Convert.ToDouble(row[ds.HoldingSummaryTable.HOLDING].ToString()));

                    difmSeries.Data = subTotal;

                    C1PieChart1.SeriesList.Add(difmSeries);
                }
            }
            if (diyCollection.Any())
            {
                var diySeries = new C1.Web.Wijmo.Controls.C1Chart.BarChartSeries { Label = "DO IT YOURSELF" };

                var diySeriesGroup = diyCollection.GroupBy(holdColl => holdColl[ds.HoldingSummaryTable.ASSETNAME]);

                foreach (var group in diySeriesGroup)
                {
                    diySeries.Data.X.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartXData(@group.Key.ToString()));

                    var subTotal = @group.Sum(row => Convert.ToDouble(row[ds.HoldingSummaryTable.HOLDING].ToString()));

                    diySeries.Data.Y.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartYData(subTotal));
                }

                foreach (var group in collectionGroup)
                {
                    if (!diySeries.Data.X.Values.Contains(@group.Key))
                    {
                        diySeries.Data.X.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartXData(@group.Key.ToString()));
                        diySeries.Data.Y.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartYData(0));
                    }
                }

                C1.Web.Wijmo.Controls.C1Chart.ChartStyle chartStyle = new C1.Web.Wijmo.Controls.C1Chart.ChartStyle
                {
                    Stroke = Color.FromArgb(95, 153, 150),
                    Opacity = 0.8,
                    Fill = { Color = Color.FromArgb(106, 171, 167) }
                };
                C1BarChartHoldingSumDIY.SeriesStyles.Add(chartStyle);

                C1BarChartHoldingSumDIY.SeriesList.Add(diySeries);
                C1BarChartHoldingSumDIY.Visible = true;
            }

            if (diwmCollection.Any())
            {
                var diwmSeries = new C1.Web.Wijmo.Controls.C1Chart.BarChartSeries { Label = "DO IT WITH ME" };

                var diwmSeriesGroup = diwmCollection.GroupBy(holdColl => holdColl[ds.HoldingSummaryTable.ASSETNAME]);

                foreach (var group in diwmSeriesGroup)
                {
                    diwmSeries.Data.X.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartXData(@group.Key.ToString()));

                    var subTotal = @group.Sum(row => Convert.ToDouble(row[ds.HoldingSummaryTable.HOLDING].ToString()));

                    diwmSeries.Data.Y.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartYData(subTotal));
                }

                foreach (var group in collectionGroup)
                {
                    if (!diwmSeries.Data.X.Values.Contains(@group.Key))
                    {
                        diwmSeries.Data.X.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartXData(@group.Key.ToString()));
                        diwmSeries.Data.Y.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartYData(0));
                    }
                }
                var chartStyle = new C1.Web.Wijmo.Controls.C1Chart.ChartStyle
                {
                    Stroke = Color.FromArgb(62, 95, 119),
                    Opacity = 0.8,
                    Fill = { Color = Color.FromArgb(70, 106, 133) }
                };
                C1BarChartHoldingSumDIWM.SeriesStyles.Add(chartStyle);
                C1BarChartHoldingSumDIWM.SeriesList.Add(diwmSeries);
                C1BarChartHoldingSumDIWM.Visible = true;
            }

            if (manCollection.Any())
            {
                var manSeries = new C1.Web.Wijmo.Controls.C1Chart.BarChartSeries { Label = "MANUAL ASSETS" };

                var manSeriesGroup = manCollection.GroupBy(holdColl => holdColl[ds.HoldingSummaryTable.ASSETNAME]);

                foreach (var group in manSeriesGroup)
                {
                    manSeries.Data.X.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartXData(@group.Key.ToString()));

                    var subTotal = @group.Sum(row => Convert.ToDouble(row[ds.HoldingSummaryTable.HOLDING].ToString()));

                    manSeries.Data.Y.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartYData(subTotal));
                }

                foreach (var group in collectionGroup)
                {
                    if (!manSeries.Data.X.Values.Contains(@group.Key))
                    {
                        manSeries.Data.X.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartXData(@group.Key.ToString()));
                        manSeries.Data.Y.Values.Add(new C1.Web.Wijmo.Controls.C1Chart.ChartYData(0));
                    }
                }

                var chartStyle = new C1.Web.Wijmo.Controls.C1Chart.ChartStyle
                {
                    Stroke = Color.FromArgb(127, 199, 60),
                    Opacity = 0.8,
                    Fill = { Color = Color.FromArgb(142, 222, 67) }
                };
                C1BarChartHoldingSumMAN.SeriesStyles.Add(chartStyle);

                C1BarChartHoldingSumMAN.SeriesList.Add(manSeries);
                C1BarChartHoldingSumMAN.Visible = true;
            }
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            BulkTransactionsOutput(cid, ClientHeaderInfo1.ClientName);
        }

        protected void btnExportReport_Click(object sender, EventArgs e)
        {
            BulkTransactionsOutput(cid, ClientHeaderInfo1.ClientName);
        }

        #endregion

    }
}
