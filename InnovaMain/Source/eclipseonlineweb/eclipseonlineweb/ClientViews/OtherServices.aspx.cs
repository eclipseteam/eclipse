﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb.ClientViews
{
    public partial class OtherServices : UMABasePage
    {
        private string _cid = string.Empty;

        public override void LoadPage()
        {
            _cid = Request.QueryString["ins"];
        
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");

            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                this.pnlOtherServers.Enabled = false;
            }
            if (!IsPostBack)
                LoadControls();
        }

        private void LoadControls()
        {
            //Bind Business Group Combo
            LoadBusinessGroupCombo();
            //Bind/Set Other Services Data
            LoadOtherServicesData();
        }

        private void LoadBusinessGroupCombo()
        {
            IOrganization organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            cmbBusinessGroup.Items.Clear();
            cmbBusinessGroup.DataSource = organization.BusinessGroups;
            cmbBusinessGroup.DataTextField = "Name";
            cmbBusinessGroup.DataValueField = "ID";
            cmbBusinessGroup.DataBind();
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        private void LoadOtherServicesData()
        {
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(_cid));
            OtherServicesDS ds = new OtherServicesDS();
            clientData.GetData(ds);
            PresentationData = ds;
            UMABroker.ReleaseBrokerManagedComponent(clientData);

            //AccessFacilities
            chkPhoneAccess.Checked = (bool)ds.Tables[ds.TABLENAME].Rows[0][ds.PHONEACCESS];
            chkOnlineAccess.Checked = (bool)ds.Tables[ds.TABLENAME].Rows[0][ds.ONLINEACCESS];
            chkDebitCard.Checked = (bool)ds.Tables[ds.TABLENAME].Rows[0][ds.DEBITCARD];
            chkChequeBook.Checked = (bool)ds.Tables[ds.TABLENAME].Rows[0][ds.CHEQUEBOOK];
            chkDepositeBook.Checked = (bool)ds.Tables[ds.TABLENAME].Rows[0][ds.DEPOSITBOOK];

            //OperationManner
            chkMOOne.Checked = (bool)ds.Tables[ds.TABLENAME].Rows[0][ds.ANYONEOFUSTOSIGN];
            chkMOTwo.Checked = (bool)ds.Tables[ds.TABLENAME].Rows[0][ds.ANYTWOOFUSTOSIGN];
            chkMOAll.Checked = (bool)ds.Tables[ds.TABLENAME].Rows[0][ds.ALLOFUSTOSIGN];

            //ExemptionCategory
            chkECOne.Checked = (bool)ds.Tables[ds.TABLENAME].Rows[0][ds.CATEGORY1];
            chkECTwo.Checked = (bool)ds.Tables[ds.TABLENAME].Rows[0][ds.CATEGORY2];
            chkECThree.Checked = (bool)ds.Tables[ds.TABLENAME].Rows[0][ds.CATEGORY3];

            //BTWrap
            txtAccountNumber.Text = ds.Tables[ds.TABLENAME].Rows[0][ds.ACCOUNTNUMBER].ToString();
            txtAccountDescription.Text = ds.Tables[ds.TABLENAME].Rows[0][ds.ACCOUNTDESCRIPTION].ToString();

            //BusinessGroupID
            Guid result;
            if (Guid.TryParse(ds.Tables[ds.TABLENAME].Rows[0][ds.BUSINESSGROUPID].ToString(), out result) && result != Guid.Empty)
                cmbBusinessGroup.SelectedValue = result.ToString();

        }

        protected void btnSave_OnClick(object sender, ImageClickEventArgs e)
        {
            SaveOtherServicesData();
        }

        private void SaveOtherServicesData()
        {
            OtherServicesDS ds = new OtherServicesDS();

            DataTable dt = ds.Tables[ds.TABLENAME];
            DataRow dr = dt.NewRow();

            //Setting AccessFacilities
            dr[ds.PHONEACCESS] = chkPhoneAccess.Checked;
            dr[ds.ONLINEACCESS] = chkOnlineAccess.Checked;
            dr[ds.DEBITCARD] = chkDebitCard.Checked;
            dr[ds.CHEQUEBOOK] = chkChequeBook.Checked;
            dr[ds.DEPOSITBOOK] = chkDepositeBook.Checked;

            //Settting OperationManner
            dr[ds.ANYONEOFUSTOSIGN] = chkMOOne.Checked;
            dr[ds.ANYTWOOFUSTOSIGN] = chkMOTwo.Checked;
            dr[ds.ALLOFUSTOSIGN] = chkMOAll.Checked;

            //Setting ExemptionCategory
            dr[ds.CATEGORY1] = chkECOne.Checked;
            dr[ds.CATEGORY2] = chkECTwo.Checked;
            dr[ds.CATEGORY3] = chkECThree.Checked;

            //Setting BTWrap
            dr[ds.ACCOUNTNUMBER] = txtAccountNumber.Text.Trim();
            dr[ds.ACCOUNTDESCRIPTION] = txtAccountDescription.Text.Trim();

            //Setting BusinessGroupID
            if (cmbBusinessGroup.SelectedValue != String.Empty)
                dr[ds.BUSINESSGROUPID] = cmbBusinessGroup.SelectedValue;
            else
                dr[ds.BUSINESSGROUPID] = Guid.Empty.ToString();

            dt.Rows.Add(dr);
            ds.Tables.Remove(ds.TABLENAME);
            ds.Tables.Add(dt);

            //Saving Data
            SaveData(_cid, ds);
        }
    }
}