﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="ChessTransferofSecurity.aspx.cs" Inherits="eclipseonlineweb.ChessTransferofSecurity" %>

<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register TagPrefix="uc4" TagName="SecuritiesChess" Src="~/Controls/SecuritiesChess.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnSave" AlternateText="Update" ToolTip="Save Changes" OnClick="btnSave_OnClick"
                                runat="server" ImageUrl="~/images/Save-Icon.png" />
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <fieldset>
                <legend>Securities</legend>
                <uc4:SecuritiesChess ID="SecuritiesChessControl" runat="server" />
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
