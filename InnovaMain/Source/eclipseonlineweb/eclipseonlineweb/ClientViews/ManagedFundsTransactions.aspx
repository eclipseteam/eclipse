﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="ManagedFundsTransactions.aspx.cs" Inherits="eclipseonlineweb.ManagedFundsTransactions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc" TagName="Details" Src="../Controls/MISTransactionControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript">
        function cancel() {
            window.$find("ModalBehaviour").hide();
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 15%">
                            <asp:ImageButton ID="btnReport" AlternateText="Print" ToolTip="Print" runat="server"
                                OnClick="BtnReportClick" ImageUrl="~/images/download.png" />
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                OnItemDataBound="PresentationGrid_ItemDataBound" OnInsertCommand="PresentationGrid_InsertCommand"
                AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource"
                OnItemCommand="PresentationGrid_OnItemCommand">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="Banks" TableLayout="Fixed" Font-Size="7.5">
                    <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add Transaction">
                    </CommandItemSettings>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="ID" UniqueName="ID" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="MISCID" ReadOnly="true" HeaderText="MISCID"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="MISCID" UniqueName="MISCID" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="66%" HeaderStyle-Width="80px" SortExpression="Code"
                            ReadOnly="true" HeaderText="Fund Code" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Code" UniqueName="Code">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn HeaderText="Code" Visible="false" SortExpression="Code"
                            UniqueName="DDLAccountList" DataField="Code" ColumnEditorID="GridDropDownListColumnEditorAccountList" />
                        <telerik:GridBoundColumn FilterControlWidth="70%" HeaderStyle-Width="25%" SortExpression="Description"
                            ReadOnly="true" HeaderText="Fund Description" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Description" UniqueName="Description">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="66%" SortExpression="TransactionType"
                            ReadOnly="true" HeaderText="Transaction Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransactionType"
                            UniqueName="TransactionType">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn HeaderText="Transaction Category" Visible="false" SortExpression="TransactionType"
                            UniqueName="DDLTranType" DataField="TransactionType" ColumnEditorID="GridDropDownListColumnEditorSystemTranType" />
                        <telerik:GridDateTimeColumn Visible="true" UniqueName="TradeDate" PickerType="DatePicker"
                            HeaderText="Trade Date" DataField="TradeDate" DataFormatString="{0:dd/MM/yyyy}">
                            <ItemStyle Width="85px" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridNumericColumn FilterControlWidth="66%" ReadOnly="false" SortExpression="Shares"
                            DecimalDigits="4" ShowFilterIcon="true" HeaderText="Units" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" DefaultInsertValue="0" HeaderButtonType="TextButton"
                            DataField="Shares" UniqueName="Shares" DataFormatString="{0:N4}">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn FilterControlWidth="66%" ReadOnly="false" SortExpression="UnitPrice"
                            Visible="true" DecimalDigits="6" ShowFilterIcon="true" HeaderText="Unit Price"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" DefaultInsertValue="0" NumericType=Number
                            HeaderButtonType="TextButton" DataField="UnitPrice" UniqueName="UnitPrice" DataFormatString="{0:N6}">
                        </telerik:GridNumericColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="Amount"
                            HeaderText="Amount" AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                            HeaderButtonType="TextButton" DataField="Amount" UniqueName="Amount" DataFormatString="{0:C}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="DistributionStatus"
                            HeaderText="Status" AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                            HeaderButtonType="TextButton" DataField="DistributionStatus" UniqueName="DistributionStatus" >
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                            UniqueName="EditColumn">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                        <telerik:GridButtonColumn ConfirmText="Are you sure you want to remove the transaction?"
                            ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <telerik:GridDropDownListColumnEditor ID="GridDropDownListColumnEditorSystemTranType"
                runat="server" DropDownStyle-Width="400px" />
            <telerik:GridDropDownListColumnEditor ID="GridDropDownListColumnEditorAccountList"
                runat="server" DropDownStyle-Width="400px" />
            <asp:Button ID="Button1" runat="server" Style="display: none" />
            <asp:Button ID="btnOkay" runat="server" Style="display: none" />
            <asp:Button ID="btnCancel" runat="server" Style="display: none" />
            <asp:ModalPopupExtender ID="ModalPopupExtenderEditTran" runat="server" CancelControlID="btnCancel"
                TargetControlID="btnShowPopup" PopupControlID="pnlpopup" PopupDragHandleControlID="PopupHeader"
                Drag="true" BackgroundCssClass="ModalPopupBG" BehaviorID="ModalBehaviour">
            </asp:ModalPopupExtender>
            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
            <asp:Panel ID="pnlpopup" runat="server" BackColor="White" Style="display: none">
                <div class="popup_Container">
                    <div class="popup_Titlebar" id="PopupHeader">
                        <div class="TitlebarLeft">
                            Transaction Details
                        </div>
                        <div class="TitlebarRight" onclick="cancel();">
                        </div>
                    </div>
                    <div class="PopupBody">
                        <uc:Details ID="Details" runat="server" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Label Visible="false" runat="server" ID="lblTranID"></asp:Label><asp:Label Visible="false"
                runat="server" ID="lblMISCID"></asp:Label><asp:Label Visible="false" runat="server"
                    ID="lblOLDFUNDCODE"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
