﻿using System;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb.ClientViews
{
    public partial class SettledUnsettledTransactions : UMABasePage
    {
        protected override void Intialise()
        {
            SettledUnsettledTransactionsControl.SaveData += SaveData;
        }

        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth" || CheckAdviserForOrderPad(objUser))
                return true;
            return objUser.UserType == UserType.Innova || objUser.Name == "Administrator";
        }


        public override void LoadPage()
        {
            if (!IsPostBack)
            {
                cid = Request.QueryString["ins"];
                var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
                SettledUnsettledTransactionsControl.IsAdmin = objUser.Administrator;
                if (Request.QueryString["OrderId"] != null)
                {
                    SettledUnsettledTransactionsControl.OrderId =Guid.Parse(Request.QueryString["OrderId"]);
                }
            }
        }
    }
}