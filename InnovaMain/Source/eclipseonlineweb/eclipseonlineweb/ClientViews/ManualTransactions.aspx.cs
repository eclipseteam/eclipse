﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp;
using C1.Web.Wijmo.Controls.C1ComboBox;

namespace eclipseonlineweb
{
    public partial class ManualTransactions : UMABasePage
    {
        protected void BtnAddMATran(object sender, EventArgs e)
        {
            lblTranID.Text = Guid.NewGuid().ToString();
            InvestmentCode.Text = String.Empty;
            InvestmentDesc.Text = String.Empty;
            SettlementDate.Date = DateTime.Now;
            TransactionDate.Date = DateTime.Now;
            UnitPrice.Value = 0;
            UnitsOnHand.Value = 0;
            Holding.Value = 0;
            IsSave.Checked = true;
            var ibmc = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            if (ibmc != null) C1ComboBoxManualAssets.DataSource = ibmc.ManualAsset;
            C1ComboBoxManualAssets.DataBind();
            C1ComboBoxManualAssets.Text = "";
            C1ComboBoxManual.Text = string.Empty;
            C1ComboBoxManualAssets.Visible = true;
            InvestmentDesc.Visible = false;
            InvestmentCode.Visible = false;
            lblMessage.Text = "";

            UMABroker.ReleaseBrokerManagedComponent(ibmc);
            ModalPopupExtenderEditTran.Show();
        }

        protected void BtnReportClick(object sender, EventArgs e)
        {
            BulkTransactionsOutput(this.cid, this.ClientHeaderInfo1.ClientName);
        }

        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];

            C1ComboBoxPriceType.DataSource = Enumeration.ToDictionary(typeof(PriceType));
            C1ComboBoxPriceType.DataValueField = "Key";
            C1ComboBoxPriceType.DataTextField = "Value";

            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");

            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                PresentationGrid.Columns.FindByUniqueNameSafe("DeleteColumn").Visible = false;
                PresentationGrid.Columns.FindByUniqueNameSafe("EditColumn").Visible = false;
                btn.Visible = false;
            }
            ScriptManager sm = ScriptManager.GetCurrent(Page);

            if (sm != null)
                sm.RegisterPostBackControl(btnReport);


        }

        private void GetData()
        {
            cid = Request.QueryString["ins"];
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(cid));
            var asxds = new ManualTransactionDS();
            clientData.GetData(asxds);
            PresentationData = asxds;
            if (asxds.Tables.Count > 0)
            {
                var asxView = new DataView(asxds.Tables[0]) { Sort = "TransactionDate DESC" };
                PresentationGrid.DataSource = asxView;
            }
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void UpdateTransaction(object sender, EventArgs e)
        {
            var manualTransactionDetailsDS = new ManualTransactionDetailsDS();
            DataRow row = manualTransactionDetailsDS.Tables[ManualTransactionDetailsDS.MANUALTRANSDETAILSTABLE].NewRow();

            if (IsSave.Checked)
            {
                manualTransactionDetailsDS.DataSetOperationType = DataSetOperationType.NewSingle;
                if (!string.IsNullOrEmpty(C1ComboBoxManualAssets.SelectedValue))
                {
                    row[ManualTransactionDetailsDS.INVESTID] = new Guid(C1ComboBoxManualAssets.SelectedItem.Value);
                    lblMessage.Text = "";
                }
                else
                {
                    lblMessage.Text = "Invalid investment name";
                    ModalPopupExtenderEditTran.Show();
                    return;
                }
            }
            else
                manualTransactionDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;

            row[ManualTransactionDetailsDS.ID] = lblTranID.Text;
            row[ManualTransactionDetailsDS.INVESTCODE] = InvestmentCode.Text;

            row[ManualTransactionDetailsDS.INVESTDESC] = InvestmentDesc.Text;
            if (SettlementDate.Date != null) row[ManualTransactionDetailsDS.SETTLEDATE] = SettlementDate.Date.Value;
            row[ManualTransactionDetailsDS.UNITSONHAND] = UnitsOnHand.Value;
            if (TransactionDate.Date != null) row[ManualTransactionDetailsDS.TRANDATE] = TransactionDate.Date.Value;
            row[ManualTransactionDetailsDS.TRANTYPE] = C1ComboBoxManual.Text;

            IDictionary<Enum, string> priceTypeList = Enumeration.ToDictionary(typeof(PriceType));

            var priceType = (PriceType)priceTypeList.Where(item => item.Value == C1ComboBoxPriceType.SelectedItem.Value).FirstOrDefault().Key;
            row[ManualTransactionDetailsDS.PRICETYPE] = priceType;

            manualTransactionDetailsDS.Tables[ManualTransactionDetailsDS.MANUALTRANSDETAILSTABLE].Rows.Add(row);

            SaveData(cid, manualTransactionDetailsDS);
            PresentationGrid.Rebind();
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            string rowID = dataItem["ID"].Text;
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    GetData();
                    DataRow selectedRow = PresentationData.Tables[0].Select("ID='" + rowID + "'").FirstOrDefault();
                    lblTranID.Text = rowID;
                    if (selectedRow != null)
                    {
                        InvestmentCode.Text = selectedRow["InvestmentCode"].ToString();
                        InvestmentDesc.Text = selectedRow["InvestmentDesc"].ToString();
                        C1ComboBoxManual.Text = selectedRow["TransactionType"].ToString();
                        SettlementDate.Date = (DateTime)selectedRow["SettlementDate"];
                        TransactionDate.Date = (DateTime)selectedRow["TransactionDate"];
                        UnitPrice.Value = Convert.ToDouble(selectedRow["UnitPrice"]);
                        UnitsOnHand.Value = Convert.ToDouble(selectedRow["UnitsOnHand"]);
                        Holding.Value = Convert.ToDouble(selectedRow["Holding"]);

                        var priceType = (PriceType)selectedRow["PriceType"];
                        C1ComboBoxItem selectedItem = C1ComboBoxPriceType.Items.Where(item => item.Text == Enumeration.GetDescription(priceType)).FirstOrDefault();
                        if (selectedItem != null) selectedItem.Selected = true;
                    }
                    C1ComboBoxManualAssets.Visible = false;
                    IsSave.Checked = false;
                    lblMessage.Text = "";
                    ModalPopupExtenderEditTran.Show();
                    break;
                case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    GetData();
                    DataRow selectedRow1 = PresentationData.Tables[0].Select("ID='" + rowID + "'").FirstOrDefault();
                    var manualTransactionDetailsDS = new ManualTransactionDetailsDS { DataSetOperationType = DataSetOperationType.DeletSingle };
                    DataRow row = manualTransactionDetailsDS.Tables[ManualTransactionDetailsDS.MANUALTRANSDETAILSTABLE].NewRow();
                    row[ManualTransactionDetailsDS.ID] = rowID;
                    if (selectedRow1 != null)
                    {
                        row[ManualTransactionDetailsDS.INVESTCODE] = selectedRow1["InvestmentCode"].ToString();
                        row[ManualTransactionDetailsDS.INVESTDESC] = selectedRow1["InvestmentDesc"].ToString();
                        row[ManualTransactionDetailsDS.SETTLEDATE] = (DateTime)selectedRow1["SettlementDate"];
                        row[ManualTransactionDetailsDS.TRANDATE] = (DateTime)selectedRow1["TransactionDate"];
                        row[ManualTransactionDetailsDS.UNITSONHAND] = Convert.ToDouble(selectedRow1["UnitsOnHand"]);
                    }

                    manualTransactionDetailsDS.Tables[ManualTransactionDetailsDS.MANUALTRANSDETAILSTABLE].Rows.Add(row);

                    SaveData(cid, manualTransactionDetailsDS);
                    PresentationGrid.Rebind();
                    break;
            }
        }
    }
}
