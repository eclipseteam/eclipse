﻿<%@ Page Title="e-Clipse Online Portal - Configure Fees" Language="C#" MasterPageFile="ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="PendingOrders.aspx.cs" Inherits="eclipseonlineweb.PendingOrders" %>

<%@ Register Src="../Controls/PendingOrders.ascx" TagName="PendingOrders" TagPrefix="uc1" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <uc1:PendingOrders ID="PendingOrdersControl" runat="server" />
</asp:Content>
