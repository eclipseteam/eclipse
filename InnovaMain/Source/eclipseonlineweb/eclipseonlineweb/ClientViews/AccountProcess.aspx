﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeBehind="AccountProcess.aspx.cs" Inherits="eclipseonlineweb.AccountProcess" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
    TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Calendar"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Chart"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ProgressBar"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Extenders.4" Namespace="C1.Web.Wijmo.Extenders.C1FormDecorator"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer"
    TagPrefix="C1ReportViewer" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Accordion"
    TagPrefix="C1Accordion" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Chart"
    TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Splitter"
    TagPrefix="c1" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .wijmo-wijgrid .wijmo-wijgrid-groupheaderrow td
        {
            background-color: #DAE6F4;
            color: #000000;
            font-size: smaller;
            font-weight: bold;
            vertical-align: middle;
        }
        .wijmo-wijgrid .wijmo-wijgrid-footerrow td
        {
            font-weight: bolder;
            text-align: right;
            font-size: smaller;
        }
    </style>
</asp:Content>
<asp:Content ID="SideMenu" ContentPlaceHolderID="SidebarMenu" runat="Server">
    <asp:UpdatePanel ID="UpdatPanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Menu ID="NavigationMenu" SkipLinkText="" StaticDisplayLevels="2" Width="200px"
                runat="server" CssClass="sidemenu" EnableViewState="false" IncludeStyleBlock="false"
                StaticSubMenuIndent="80px" Orientation="Vertical" OnMenuItemClick="Navigation_MenuItemClick">
                <Items>
                    <asp:MenuItem Text="FINANCIAL SUMMARY" Selected="true" />
                    <asp:MenuItem Selectable="false" Text="TRANSACTIONS">
                        <asp:MenuItem Text="BANK" />
                        <asp:MenuItem Text="ASX" />
                        <asp:MenuItem Text="MANAGED FUNDS" />
                        <asp:MenuItem Text="TERM DEPOSITS" />
                        <asp:MenuItem Text="INCOME" />
                        <asp:MenuItem Text="MANUAL ASSETS" />
                        <asp:MenuItem Text="DISTRIBUTIONS" />
                        <asp:MenuItem Text="ACCOUNTS PROCESS" />
                    </asp:MenuItem>
                    <asp:MenuItem Selectable="false" Text="ACCOUNT INFO">
                        <asp:MenuItem Text="ACCOUNT SUMMARY" />
                        <asp:MenuItem Text="ACCESS LIST" />
                    </asp:MenuItem>
                    <asp:MenuItem Text="GOTO REPORTS"></asp:MenuItem>
                </Items>
            </asp:Menu>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function hintContent() {
            return this.data.label + '<br/> ' + this.y + '';
        }
        function hintContentPie() {
            return this.data.toString() + " : " + Globalize.format(this.value / this.total, "p2");
        }

    </script>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlToolBar">
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnMainView" ToolTip="Main View" AlternateText="Main View" runat="server"
                                ImageUrl="~/images/4848/application.png" />
                        </td>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnChart" AlternateText="Graphs" ToolTip="Graph" runat="server"
                                OnClick="Chart_Click" ImageUrl="~/images/4848/chart.png" />
                        </td>
                        <td style="width: 5%">
                            <asp:ImageButton ID="ImageButton3" AlternateText="Refresh" ToolTip="Graph" runat="server"
                                OnClick="Refresh_Click" ImageUrl="~/images/4848/process.png" />
                        </td>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnReport" AlternateText="Print" ToolTip="Print" runat="server"
                                OnClick="btnReport_Click" ImageUrl="~/images/4848/window_down.png" />
                        </td>
                        <td>
                            <c1:C1InputDate DateFormat="dd/MMM/yyyy" ID="C1FinancialSummaryDate" runat="server"
                                ShowTrigger="true">
                            </c1:C1InputDate>
                        </td>
                        <td>
                            <asp:ImageButton OnClick="GenerateFinancialSummary" runat="server" ID="ImageButton2"
                                ImageUrl="~/images/3232/database_accept.png" />
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <asp:Label Font-Bold="true" ID="lblClientID" runat="server" Text="Client ID"></asp:Label>
                            <asp:Label Font-Bold="true" ID="Label1" runat="server" Text="-"></asp:Label>
                            <asp:Label Font-Bold="true" ID="lblClientName" runat="server" Text="Client ID"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div id="MainView">
                <p>
                    <asp:LinkButton runat="server" Visible="false" ID="lnkBackToSummary" Text="Back to Summary"
                        OnClick="BackToSummary"> </asp:LinkButton></p>
                <asp:Panel runat="server" ID="pnlFinancialSummary">
                    <c1:C1GridView AlternatingRowStyle-Font-Size="X-Small" FilterStyle-Font-Size="Smaller"
                        RowStyle-Font-Size="X-Small" HeaderStyle-Font-Size="Smaller" ID="FinancialSummary"
                        AllowSorting="true" OnSorting="FinancialSummarySorting" runat="server" AutogenerateColumns="false"
                        ShowFilter="true" OnFiltering="Filter" OnRowCommand="FinancialSummary_RowCommand"
                        OnRowDataBound="FinancialSummary_RowDataBound" ShowFooter="true">
                        <Columns>
                            <c1:C1BoundField DataField="TDID" Visible="false">
                            </c1:C1BoundField>
                            <c1:C1BoundField DataField="ProductID" Visible="false">
                            </c1:C1BoundField>
                            <c1:C1BoundField DataField="ServiceType" SortExpression="ServiceType" HeaderText="ServiceType"
                                FooterText="Total" Visible="false">
                                <GroupInfo OutlineMode="StartExpanded" Position="Header" />
                            </c1:C1BoundField>
                            <c1:C1BoundField DataField="AssetName" SortExpression="AssetName" Width="8%" HeaderText="Asset Name">
                                <ItemStyle HorizontalAlign="Center" />
                            </c1:C1BoundField>
                            <c1:C1BoundField DataField="ProductName" SortExpression="ProductName" Width="18%"
                                HeaderText="Product Name">
                                <ItemStyle HorizontalAlign="Center" />
                            </c1:C1BoundField>
                            <c1:C1TemplateField Width="26%" HeaderText="Description" SortExpression="Description">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblDesc" Text='<%# Bind("Description") %>'></asp:Label>
                                    <asp:LinkButton CommandArgument='<%# Eval("AssetName") +"_" + Eval("TDID")+"_" + Eval("ProductID")%>'
                                        ForeColor="#5188FF" Font-Underline="false" Font-Size="X-Small" Visible="false"
                                        runat="server" ID="linkDesc" Text='<%# Bind("Description") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </c1:C1TemplateField>
                            <c1:C1TemplateField>
                                <wijmo:C1ComboBox ID="ddlComboBox" runat="server">
                                </wijmo:C1ComboBox>
                            </c1:C1TemplateField>
                            <c1:C1CommandField ShowSelectButton="true" SelectText="Associate">
                            </c1:C1CommandField>
                        </Columns>
                    </c1:C1GridView>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlAssociation">
                    <table id="Table3" height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            Included users
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            Available users
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <wijmo:C1ComboBox ID="C1ComboBox1" runat="server" Width="250px">
                                            </wijmo:C1ComboBox>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnRemove" Width="100px" runat="server" CssClass="normalButtons100"
                                                Text="Remove >>"></asp:Button>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <wijmo:C1ComboBox ID="C1ComboBox2" runat="server" Width="250px">
                                            </wijmo:C1ComboBox>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAdd" Width="100px" runat="server" CssClass="normalButtons100"
                                                Text="<< Add"></asp:Button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:ListBox ID="lstIncludedUser" runat="server" Width="250px"></asp:ListBox>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:ListBox ID="lstExcludedUser" runat="server" Width="250px"></asp:ListBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel Visible="false" runat="server" ID="pnlHoldingChart">
                    <wijmo:C1PieChart runat="server" ID="C1PieChart1" Radius="140" Visible="false" Height="475"
                        Width="756" CssClass="ui-widget ui-widget-content ui-corner-all">
                        <Hint>
                            <Content Function="hintContentPie" />
                        </Hint>
                        <Legend Visible="true"></Legend>
                        <Header Text="Steam - Mac Hardware">
                        </Header>
                        <SeriesStyles>
                            <wijmo:ChartStyle StrokeWidth="1.5" Stroke="#AFE500">
                                <Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#C3FF00" ColorEnd="#AFE500">
                                </Fill>
                            </wijmo:ChartStyle>
                            <wijmo:ChartStyle StrokeWidth="1.5" Stroke="#7FC73C">
                                <Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#8EDE43" ColorEnd="#7FC73C">
                                </Fill>
                            </wijmo:ChartStyle>
                            <wijmo:ChartStyle StrokeWidth="1.5" Stroke="#5F9996">
                                <Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#6AABA7" ColorEnd="#5F9996">
                                </Fill>
                            </wijmo:ChartStyle>
                            <wijmo:ChartStyle StrokeWidth="1.5" Stroke="#3E5F77">
                                <Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#466A85" ColorEnd="#3E5F77">
                                </Fill>
                            </wijmo:ChartStyle>
                            <wijmo:ChartStyle StrokeWidth="1.5" Stroke="#959595">
                                <Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#A6A6A6" ColorEnd="#959595">
                                </Fill>
                            </wijmo:ChartStyle>
                        </SeriesStyles>
                        <Footer Compass="South" Visible="False">
                        </Footer>
                        <Axis>
                            <Y Visible="False" Compass="West">
                                <Labels TextAlign="Center">
                                </Labels>
                                <GridMajor Visible="True">
                                </GridMajor>
                            </Y>
                        </Axis>
                    </wijmo:C1PieChart>
                    <wijmo:C1BarChart Visible="false" Stacked="true" runat="server" ID="C1BarChartHoldingSumDIFM"
                        Height="350" Width="100%">
                        <Hint>
                            <Content Function="hintContent" />
                        </Hint>
                        <Axis>
                            <Y TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="HOLDING ($)">
                            </Y>
                            <X Text="">
                            </X>
                        </Axis>
                        <Header TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="HOLDING SUMMARY - DO IT FOR ME">
                        </Header>
                    </wijmo:C1BarChart>
                    <wijmo:C1BarChart Visible="false" Stacked="true" runat="server" ID="C1BarChartHoldingSumDIWM"
                        Height="350" Width="100%">
                        <Hint>
                            <Content Function="hintContent" />
                        </Hint>
                        <Axis>
                            <Y TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="HOLDING ($)">
                            </Y>
                            <X Text="">
                            </X>
                        </Axis>
                        <Header TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="HOLDING SUMMARY - DO IT WITH ME">
                        </Header>
                    </wijmo:C1BarChart>
                    <wijmo:C1BarChart Visible="false" Stacked="true" runat="server" ID="C1BarChartHoldingSumDIY"
                        Height="350" Width="100%">
                        <Hint>
                            <Content Function="hintContent" />
                        </Hint>
                        <Axis>
                            <Y TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="HOLDING ($)">
                            </Y>
                            <X Text="">
                            </X>
                        </Axis>
                        <Header TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="HOLDING SUMMARY - DO IT YOURSELF">
                        </Header>
                    </wijmo:C1BarChart>
                    <wijmo:C1BarChart Visible="false" Stacked="true" runat="server" ID="C1BarChartHoldingSumMAN"
                        Height="350" Width="100%">
                        <Hint>
                            <Content Function="hintContent" />
                        </Hint>
                        <Axis>
                            <Y TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="HOLDING ($)">
                            </Y>
                            <X Text="">
                            </X>
                        </Axis>
                        <Header TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="HOLDING SUMMARY - Manual Assets">
                        </Header>
                    </wijmo:C1BarChart>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
