﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Telerik.Web.UI;
using Oritax.TaxSimp.CM;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Common;

namespace eclipseonlineweb.ClientViews
{
    public partial class BGLCOAMap : UMABasePage
    {
        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");

            if (objUser.UserType != UserType.Advisor  && objUser.UserType != UserType.Accountant && objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(cid));
            var ds = new DIVTransactionDS();
            clientData.GetData(ds);
            var dsDis = new ClientDistributionsDS();
            clientData.GetData(dsDis);
            var dsBank = new BankAccountDS();
            clientData.GetData(dsBank);

            ds.Tables[DIVTransactionDS.INCOMETRANMAPTABLE].Merge(dsBank.Tables[DIVTransactionDS.INCOMETRANMAPTABLE]);
            ds.Tables[DIVTransactionDS.INCOMETRANMAPTABLE].Merge(dsDis.Tables[DIVTransactionDS.INCOMETRANMAPTABLE]);
            PresentationGrid.DataSource = ds.Tables[DIVTransactionDS.INCOMETRANMAPTABLE];
        }

        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
        }

        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) { }
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e) { }
        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e) { }

        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {

            switch (e.CommandName.ToLower())
            {
                case "update":
                    {
                        GridEditFormItem EditForm = (GridEditFormItem)e.Item;
                        IOrganizationUnit clientData = this.UMABroker.GetBMCInstance(new Guid(this.cid)) as IOrganizationUnit;
                        IClientUMAData clientUMAData = clientData.ClientEntity as IClientUMAData;
                        string investCode = ((TextBox)(((GridEditableItem)(e.Item))["InvestmentCode"].Controls[0])).Text;
                        string bglCode = ((TextBox)(((GridEditableItem)(e.Item))["BGLCODE"].Controls[0])).Text;
                        string investName = ((TextBox)(((GridEditableItem)(e.Item))["InvestmentName"].Controls[0])).Text;
                        string investType = ((TextBox)(((GridEditableItem)(e.Item))["InvestmentCOAType"].Controls[0])).Text;
                        string investCid = ((TextBox)(((GridEditableItem)(e.Item))["InvestmentCid"].Controls[0])).Text;


                        if (investType.StartsWith("Interest Income"))
                        {
                            IOrganizationUnit entity = this.UMABroker.GetBMCInstance(new Guid(investCid)) as IOrganizationUnit;
                            BankAccountEntity bankAccountEntity = entity.ClientEntity as BankAccountEntity;
                            bankAccountEntity.BGLCode = bglCode;
                            SaveData(entity as IBrokerManagedComponent);
                        }

                        if (investType.StartsWith("Commission Rebate"))
                        {
                            IOrganizationUnit entity = this.UMABroker.GetBMCInstance(new Guid(investCid)) as IOrganizationUnit;
                            BankAccountEntity bankAccountEntity = entity.ClientEntity as BankAccountEntity;
                            bankAccountEntity.BGLCodeCommissionRebate = bglCode;
                            SaveData(entity as IBrokerManagedComponent);
                        }

                        if (investType.StartsWith("Bank Code"))
                        {
                            IOrganizationUnit entity = this.UMABroker.GetBMCInstance(new Guid(investCid)) as IOrganizationUnit;
                            BankAccountEntity bankAccountEntity = entity.ClientEntity as BankAccountEntity;
                            bankAccountEntity.BGLCodeCashAccount = bglCode;
                            SaveData(entity as IBrokerManagedComponent);
                        }

                        if (investType.StartsWith("Suspense Account"))
                        {
                            IOrganizationUnit entity = this.UMABroker.GetBMCInstance(new Guid(investCid)) as IOrganizationUnit;
                            BankAccountEntity bankAccountEntity = entity.ClientEntity as BankAccountEntity;
                            bankAccountEntity.BGLCodeSuspense = bglCode;
                            SaveData(entity as IBrokerManagedComponent);
                        }

                        clientData = this.UMABroker.GetBMCInstance(new Guid(this.cid)) as IOrganizationUnit;
                        if (investType == "Distributions")
                        {
                            var filteredCollection = clientUMAData.DistributionIncomes.Where(dis => dis.FundCode == investCode);
                            foreach (DistributionIncomeEntity entity in filteredCollection)
                                entity.BGLCODE = bglCode;
                        }
                        else if (investType == "Dividends")
                        {
                            var filteredCollection = clientUMAData.DividendCollection.Where(c => c.InvestmentCode == investCode);

                            foreach (DividendEntity entity in filteredCollection)
                                entity.BGLCode = bglCode;
                        }

                        SaveData(clientData as IBrokerManagedComponent);

                        e.Canceled = true;
                        e.Item.Edit = false;
                        PresentationGrid.Rebind();
                        break;
                    }
            }
        }
    }
}