﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="NewClientMainView.aspx.cs" Inherits="eclipseonlineweb.NewClientMainView" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Chart"
    TagPrefix="wijmo" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript">
        //Resetting menu index
        localStorage['MenuIndex'] = '0';
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function hintContent() {
            return this.data.label + '<br/> ' + this.y + '';
        }
        function hintContentPie() {
            return this.data.toString() + " : " + window.Globalize.format(this.value / this.total, "p2");
        }

    </script>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportReport" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlToolBar">
                <fieldset>
                    <table width="100%">
                        <tr>
                            <td style="width: 5%">
                                <telerik:RadButton ID="btnExportReport" Text="Print" ToolTip="Print" OnClick="btnExportReport_Click"
                                    runat="server" Width="32px" Height="32px" BorderStyle="None">
                                    <Image ImageUrl="~/images/download.png" />
                                </telerik:RadButton>
                            </td>
                            <td>
                                <telerik:RadDatePicker ID="DpFinancialSummary" runat="server" Width="200px" ClientIDMode="AutoID">
                                    <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                                    </DateInput>
                                </telerik:RadDatePicker>
                            </td>
                            <td>
                                <asp:ImageButton OnClick="GenerateFinancialSummary" runat="server" ID="ImageButton2"
                                    Visible="True" ImageUrl="~/images/database.png" />
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBack" Visible="false" ToolTip="Back" AlternateText="back"
                                    runat="server" ImageUrl="~/images/window_previous.png" OnClick="btnBack_Click" />
                            </td>
                            <td style="width: 5%">
                                <asp:ImageButton ID="btnMainView" Visible="false" ToolTip="Main View" AlternateText="Main View"
                                    runat="server" ImageUrl="~/images/application.png" OnClick="MainView_Click" />
                            </td>
                            <td style="width: 5%">
                                <asp:ImageButton ID="btnChart" Visible="false" AlternateText="Graphs" ToolTip="Graph"
                                    runat="server" OnClick="Chart_Click" ImageUrl="~/images/chart.png" />
                            </td>
                            <td style="width: 5%">
                                <asp:ImageButton ID="ImageButton3" Visible="false" AlternateText="Refresh" ToolTip="Refresh"
                                    runat="server" OnClick="Refresh_Click" ImageUrl="~/images/process.png" />
                                <td style="width: 85%;" class="breadcrumbgap">
                                    <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                                    <br />
                                    <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                                </td>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </asp:Panel>
            <p>
                <asp:LinkButton runat="server" Visible="false" ID="lnkBackToSummary" Text="Back to Summary"
                    OnClick="BackToSummary"> </asp:LinkButton></p>
            <asp:Panel runat="server" ID="pnlFinancialSummary">
                <telerik:RadGrid ID="gvFinancialSummary" runat="server" AllowSorting="True" AllowMultiRowSelection="True"
                    OnItemDataBound="gvFinancialSummary_ItemDataBound" OnItemCommand="PresentationGrid_OnItemCommand"
                    AllowFilteringByColumn="True" OnNeedDataSource="gvFinancial_OnNeedDataSource"
                    ShowGroupPanel="False" AutoGenerateColumns="False" ShowStatusBar="true" ShowFooter="true"
                    GridLines="none">
                    <MasterTableView Width="100%" AllowFilteringByColumn="True">
                        <GroupByExpressions>
                            <telerik:GridGroupByExpression>
                                <SelectFields>
                                    <telerik:GridGroupByField FieldName="MODELNAME" FieldAlias="ServiceType"></telerik:GridGroupByField>
                                    <telerik:GridGroupByField FieldName="Holding" Aggregate="Sum" FormatString="{0:F2}"
                                        FieldAlias="Settled" />
                                    <telerik:GridGroupByField FieldName="Unsettled" Aggregate="Sum" FormatString="{0:F2}"
                                        FieldAlias="Unsettled" />
                                    <telerik:GridGroupByField FieldName="Total" Aggregate="Sum" FormatString="{0:F2}"
                                        FieldAlias="Total" />
                                </SelectFields>
                                <GroupByFields>
                                    <telerik:GridGroupByField FieldName="MODELNAME"></telerik:GridGroupByField>
                                </GroupByFields>
                            </telerik:GridGroupByExpression>
                        </GroupByExpressions>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="TDID" HeaderText="TDID" HeaderButtonType="TextButton"
                                Visible="False" UniqueName="TDID" DataField="TDID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ProductID" HeaderText="ProductID" HeaderButtonType="TextButton"
                                Visible="False" UniqueName="ProductID" DataField="ProductID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="MODELNAME" HeaderText="MODELNAME" HeaderButtonType="TextButton"
                                Visible="False" UniqueName="MODELNAME" DataField="MODELNAME">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="AssetName" HeaderText="Asset Name" HeaderButtonType="TextButton"
                                UniqueName="AssetName" DataField="AssetName" FilterControlWidth="50px" HeaderStyle-Width="85px"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ProductName" HeaderText="Product Name" HeaderButtonType="TextButton"
                                UniqueName="ProductName" DataField="ProductName" FilterControlWidth="125px" HeaderStyle-Width="120px"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="Description" HeaderText="Description" SortExpression="Description"
                                FooterText="Total">
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <ItemStyle Width="200px" Wrap="false" />
                                <HeaderStyle Width="200px" Wrap="false" />
                                <ItemTemplate>
                                    <asp:Label Font-Size="X-Small" runat="server" ID="lblDesc" Text='<%# Bind("Description") %>'></asp:Label>
                                    <asp:LinkButton CommandArgument='<%# Eval("AssetName") +"_" + Eval("TDID")+"_" + Eval("ProductID")+"_" + Eval("Description")+"_" + Eval("ServiceType")%>'
                                        ForeColor="#5188FF" Font-Underline="false" Font-Size="X-Small" Visible="false"
                                        CommandName="Linked" runat="server" ID="linkDesc" Text='<%# Bind("Description") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridNumericColumn SortExpression="AccountNo" HeaderText="Account#" HeaderButtonType="TextButton"
                                UniqueName="AccountNo" DataField="AccountNo" FilterControlWidth="90px" HeaderStyle-Width="120px"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </telerik:GridNumericColumn>
                            <telerik:GridNumericColumn SortExpression="Holding" HeaderText="Settled ($)" HeaderButtonType="TextButton"
                                UniqueName="Holding" DataField="Holding" FilterControlWidth="90px" HeaderStyle-Width="120px"
                                Aggregate="Sum" DataType="System.Decimal" DataFormatString="{0:C2}" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true">
                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </telerik:GridNumericColumn>
                            <telerik:GridNumericColumn SortExpression="Unsettled" HeaderText="Unsettled ($)"
                                DataType="System.Decimal" FilterControlWidth="90px" DataFormatString="{0:C2}"
                                Aggregate="Sum" HeaderButtonType="TextButton" UniqueName="Unsettled" DataField="Unsettled"
                                HeaderStyle-Width="120px" CurrentFilterFunction="Contains" ShowFilterIcon="true">
                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </telerik:GridNumericColumn>
                            <telerik:GridNumericColumn SortExpression="Total" HeaderText="Total ($)" HeaderButtonType="TextButton"
                                DataType="System.Decimal" FilterControlWidth="90px" DataFormatString="{0:C2}"
                                Aggregate="Sum" HeaderStyle-Width="120px" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                UniqueName="Total" DataField="Total">
                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </telerik:GridNumericColumn>
                            <telerik:GridDateTimeColumn SortExpression="PRICEATDATE" HeaderText="Price at Date"
                                HeaderButtonType="TextButton" FilterControlWidth="50px" HeaderStyle-Width="85px"
                                DataType="System.DateTime" DataFormatString="{0:dd/MM/yyyy}" CurrentFilterFunction="Contains"
                                ShowFilterIcon="False" UniqueName="PRICEATDATE" DataField="PRICEATDATE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </telerik:GridDateTimeColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings ReorderColumnsOnClient="True" AllowDragToGroup="True" AllowColumnsReorder="True">
                        <Selecting AllowRowSelect="True"></Selecting>
                        <Resizing AllowRowResize="True" AllowColumnResize="True" EnableRealTimeResize="True"
                            ResizeGridOnColumnResize="False"></Resizing>
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="true"></GroupingSettings>
                </telerik:RadGrid>
                <telerik:RadGrid ID="gvBreakBown" runat="server" AutoGenerateColumns="False" GridLines="None"
                    EnableViewState="True" ShowFooter="True">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AllowMultiColumnSorting="True" Width="100%">
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="InvestmentCode" ReadOnly="true" HeaderText="Investment Code"
                                HeaderStyle-Width="85px" HeaderButtonType="TextButton" DataField="InvestmentCode"
                                UniqueName="InvestmentCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="InvestmentName" ReadOnly="true" HeaderText="Description"
                                HeaderStyle-Width="200px" HeaderButtonType="TextButton" DataField="InvestmentName"
                                UniqueName="InvestmentName">
                            </telerik:GridBoundColumn>
                            <telerik:GridNumericColumn SortExpression="UnitPrice" ReadOnly="true" HeaderText="Unit Price"
                                HeaderStyle-Width="85px" DataType="System.Decimal" DataFormatString="{0:C4}"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="UnitPrice" UniqueName="UnitPrice">
                            </telerik:GridNumericColumn>
                            <telerik:GridNumericColumn SortExpression="Holding" ReadOnly="true" HeaderText="Settled Units"
                                HeaderStyle-Width="100px" Aggregate="Sum" DataType="System.Decimal" DataFormatString="{0:C2}"
                                HeaderButtonType="TextButton" DataField="Holding" UniqueName="Holding">
                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </telerik:GridNumericColumn>
                            <telerik:GridNumericColumn ReadOnly="true" SortExpression="UnsettledBuy" HeaderText="Unsettled Buy Units"
                                ShowFilterIcon="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                                HeaderStyle-Width="120px" Aggregate="Sum" DataType="System.Decimal" DataFormatString="{0:C2}"
                                DataField="UnsettledBuy" UniqueName="UnsettledBuy">
                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </telerik:GridNumericColumn>
                            <telerik:GridNumericColumn SortExpression="UnsettledSell" HeaderText="Unsettled Sell Units"
                                HeaderStyle-Width="100px" Aggregate="Sum" DataType="System.Decimal" DataFormatString="{0:C2}"
                                ReadOnly="true" HeaderButtonType="TextButton" DataField="UnsettledSell" UniqueName="UnsettledSell">
                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </telerik:GridNumericColumn>
                            <telerik:GridNumericColumn SortExpression="CurrentValue" ReadOnly="true" HeaderText="Settled ($)"
                                HeaderStyle-Width="80px" HeaderButtonType="TextButton" Aggregate="Sum" DataType="System.Decimal"
                                DataFormatString="{0:C2}" DataField="CurrentValue" UniqueName="CurrentValue">
                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </telerik:GridNumericColumn>
                            <telerik:GridNumericColumn SortExpression="Unsettled" ReadOnly="true" HeaderText="UnSettled ($)"
                                HeaderStyle-Width="100px" HeaderButtonType="TextButton" Aggregate="Sum" DataType="System.Decimal"
                                DataFormatString="{0:C2}" DataField="Unsettled" UniqueName="Unsettled">
                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </telerik:GridNumericColumn>
                            <telerik:GridNumericColumn SortExpression="Total" ReadOnly="true" HeaderText="Total ($)"
                                HeaderStyle-Width="120px" HeaderButtonType="TextButton" Aggregate="Sum" DataType="System.Decimal"
                                DataFormatString="{0:C2}" DataField="Total" UniqueName="Total">
                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </telerik:GridNumericColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <telerik:RadGrid ID="gvTDBreakDown" runat="server" AutoGenerateColumns="False" GridLines="None"
                    EnableViewState="True" ShowFooter="True">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AllowMultiColumnSorting="True" Width="100%">
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="BROKER" ReadOnly="true" HeaderText="BROKER"
                                HeaderStyle-Width="85px" HeaderButtonType="TextButton" DataField="BROKER" UniqueName="BROKER">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="InsName" ReadOnly="true" HeaderText="Institute Name"
                                HeaderStyle-Width="200px" HeaderButtonType="TextButton" DataField="InsName" UniqueName="InsName">
                            </telerik:GridBoundColumn>
                            <telerik:GridNumericColumn SortExpression="TransType" ReadOnly="true" HeaderText="Transaction Type"
                                HeaderStyle-Width="85px" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="TransType" UniqueName="TransType">
                            </telerik:GridNumericColumn>
                            <telerik:GridDateTimeColumn SortExpression="TRANSDATE" ReadOnly="true" HeaderText="Transaction Type"
                                HeaderStyle-Width="100px" DataFormatString="dd/MM/yyyy" HeaderButtonType="TextButton"
                                DataField="TRANSDATE" UniqueName="TRANSDATE">
                            </telerik:GridDateTimeColumn>
                            <telerik:GridNumericColumn ReadOnly="true" SortExpression="Holding" HeaderText="Holding"
                                ShowFilterIcon="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                                HeaderStyle-Width="120px" Aggregate="Sum" DataType="System.Decimal" DataFormatString="{0:C2}"
                                DataField="Holding" UniqueName="Holding">
                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </telerik:GridNumericColumn>
                            <telerik:GridNumericColumn SortExpression="CumulativeAmount" HeaderText="Cumulative Balance"
                                HeaderStyle-Width="100px" Aggregate="Sum" DataType="System.Decimal" DataFormatString="{0:C2}"
                                ReadOnly="true" HeaderButtonType="TextButton" DataField="CumulativeAmount" UniqueName="CumulativeAmount">
                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            </telerik:GridNumericColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </asp:Panel>
            <div class="WhiteBack">
                <asp:Panel Visible="false" runat="server" ID="pnlHoldingChart">
                    <%-- ReSharper disable UnknownCssClass --%>
                    <wijmo:C1PieChart runat="server" ID="C1PieChart1" Radius="140" Visible="false" Height="475"
                        Width="756" CssClass="ui-widget ui-widget-content ui-corner-all">
                        <%-- ReSharper restore UnknownCssClass --%>
                        <Hint>
                            <Content Function="hintContentPie" />
                        </Hint>
                        <Legend Visible="true"></Legend>
                        <Header Text="Steam - Mac Hardware">
                        </Header>
                        <SeriesStyles>
                            <wijmo:ChartStyle StrokeWidth="1.5" Stroke="#AFE500">
                                <Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#C3FF00" ColorEnd="#AFE500">
                                </Fill>
                            </wijmo:ChartStyle>
                            <wijmo:ChartStyle StrokeWidth="1.5" Stroke="#7FC73C">
                                <Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#8EDE43" ColorEnd="#7FC73C">
                                </Fill>
                            </wijmo:ChartStyle>
                            <wijmo:ChartStyle StrokeWidth="1.5" Stroke="#5F9996">
                                <Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#6AABA7" ColorEnd="#5F9996">
                                </Fill>
                            </wijmo:ChartStyle>
                            <wijmo:ChartStyle StrokeWidth="1.5" Stroke="#3E5F77">
                                <Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#466A85" ColorEnd="#3E5F77">
                                </Fill>
                            </wijmo:ChartStyle>
                            <wijmo:ChartStyle StrokeWidth="1.5" Stroke="#959595">
                                <Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#A6A6A6" ColorEnd="#959595">
                                </Fill>
                            </wijmo:ChartStyle>
                        </SeriesStyles>
                        <Footer Compass="South" Visible="False">
                        </Footer>
                        <Axis>
                            <Y Visible="False" Compass="West">
                                <Labels TextAlign="Center">
                                </Labels>
                                <GridMajor Visible="True">
                                </GridMajor>
                            </Y>
                        </Axis>
                    </wijmo:C1PieChart>
                    <wijmo:C1BarChart Visible="false" Stacked="true" runat="server" ID="C1BarChartHoldingSumDIFM"
                        Height="350" Width="100%">
                        <Hint>
                            <Content Function="hintContent" />
                        </Hint>
                        <Axis>
                            <Y TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="SETTLED ($)">
                            </Y>
                            <X Text="">
                            </X>
                        </Axis>
                        <Header TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="HOLDING SUMMARY - DO IT FOR ME">
                        </Header>
                    </wijmo:C1BarChart>
                    <wijmo:C1BarChart Visible="false" Stacked="true" runat="server" ID="C1BarChartHoldingSumDIWM"
                        Height="350" Width="100%">
                        <Hint>
                            <Content Function="hintContent" />
                        </Hint>
                        <Axis>
                            <Y TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="SETTLED ($)">
                            </Y>
                            <X Text="">
                            </X>
                        </Axis>
                        <Header TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="HOLDING SUMMARY - DO IT WITH ME">
                        </Header>
                    </wijmo:C1BarChart>
                    <wijmo:C1BarChart Visible="false" Stacked="true" runat="server" ID="C1BarChartHoldingSumDIY"
                        Height="350" Width="100%">
                        <Hint>
                            <Content Function="hintContent" />
                        </Hint>
                        <Axis>
                            <Y TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="SETTLED ($)">
                            </Y>
                            <X Text="">
                            </X>
                        </Axis>
                        <Header TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="HOLDING SUMMARY - DO IT YOURSELF">
                        </Header>
                    </wijmo:C1BarChart>
                    <wijmo:C1BarChart Visible="false" Stacked="true" runat="server" ID="C1BarChartHoldingSumMAN"
                        Height="350" Width="100%">
                        <Hint>
                            <Content Function="hintContent" />
                        </Hint>
                        <Axis>
                            <Y TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="SETTLED ($)">
                            </Y>
                            <X Text="">
                            </X>
                        </Axis>
                        <Header TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="HOLDING SUMMARY - Manual Assets">
                        </Header>
                    </wijmo:C1BarChart>
                </asp:Panel>
            </div>
            <div>
                <asp:HiddenField runat="server" ID="hidIsAdmin" />
                <asp:HiddenField runat="server" ID="hidUserType" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
