﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="ClientAddresses.aspx.cs" Inherits="eclipseonlineweb.ClientAddresses" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%--<%@ Register Src="../Controls/AddressControl.ascx" TagName="AddressControl" TagPrefix="uc2" %>--%>
<%@ Register Src="../Controls/AddressDetailControl.ascx" TagName="AddressControl"
    TagPrefix="uc2" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="10%">
                        <asp:ImageButton ID="btnSave" AlternateText="Add Client Addresses" ToolTip="Save Changes"
                            OnClick="btnSave_Click" runat="server" ImageUrl="~/images/Save-Icon.png" />
                    </td>
                    <td style="width: 5%">
                    </td>
                    <td style="width: 5%">
                    </td>
                    <td style="width: 5%">
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td style="width: 85%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <uc2:AddressControl ID="AddressDetailControl" runat="server" />
</asp:Content>
