﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="ClientViewByAccTypeDetails.aspx.cs" Inherits="eclipseonlineweb.ClientViewByAccTypeDetails" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <c1:C1InputDate Visible="false" DateFormat="MMM/yyyy" ID="C1InputStartDate" runat="server"
                                ShowTrigger="true">
                            </c1:C1InputDate>
                        </td>
                        <td style="width: 5%">
                            <c1:C1InputDate Visible="false" DateFormat="MMM/yyyy" ID="C1InputEndDate" runat="server"
                                ShowTrigger="true">
                            </c1:C1InputDate>
                        </td>
                        <td style="width: 5%">
                            <asp:ImageButton Visible="false" OnClick="GenerateReport" runat="server" ID="imgCapitalReport"
                                ImageUrl="~/images/database.png" />
                        </td>
                        <td style="width: 5%">
                            <asp:ImageButton Visible="false" ID="btnReport" AlternateText="Print" ToolTip="Print"
                                runat="server" OnClick="btnReport_Click" ImageUrl="~/images/download.png" />
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadSkinManager ID="QsfSkinManager" runat="server" ShowChooser="false" />
            <telerik:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="All"
                EnableRoundedCorners="false" />
            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
                <AjaxSettings>
                    <telerik:AjaxSetting AjaxControlID="PresentationGrid">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="PresentationGrid"></telerik:AjaxUpdatedControl>
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                </AjaxSettings>
            </telerik:RadAjaxManager>
            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
            </telerik:RadAjaxLoadingPanel>
            <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            </telerik:RadWindowManager>
            <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="50"
                Width="100%" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                OnDetailTableDataBind="PresentationGrid_DetailTableDataBind" OnItemCommand="PresentationGrid_ItemCommand"
                GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
                OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
                OnItemDataBound="PresentationGrid_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <HeaderStyle Width="200px" />
                <MasterTableView DataKeyNames="AccountNO" AllowMultiColumnSorting="True" Width="100%"
                    CommandItemDisplay="Top" Name="FinancialSummaryByAccType" TableLayout="Fixed">
                    <CommandItemSettings ShowAddNewRecordButton="false" />
                    <Columns>
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="AccountNO" ReadOnly="true"
                            HeaderStyle-Width="120px" ItemStyle-Width="120px" HeaderText="Account #" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="false" HeaderButtonType="TextButton"
                            DataField="AccountNO" UniqueName="AccountNO">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="1800px" SortExpression="Description"
                            ReadOnly="true" HeaderStyle-Width="250px" ItemStyle-Width="250px" HeaderText="Description"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                            HeaderButtonType="TextButton" DataField="Description" UniqueName="Description">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" SortExpression="TransactionCategory"
                            ReadOnly="true" HeaderStyle-Width="120px" ItemStyle-Width="120px" HeaderText="Category"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                            HeaderButtonType="TextButton" DataField="TransactionCategory" UniqueName="TransactionCategory">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" SortExpression="SysTransactionType"
                            ReadOnly="true" HeaderStyle-Width="120px" ItemStyle-Width="120px" HeaderText="System Transaction"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                            HeaderButtonType="TextButton" DataField="SysTransactionType" UniqueName="SysTransactionType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" SortExpression="TranDate" ReadOnly="true"
                            HeaderStyle-Width="120px" ItemStyle-Width="120px" HeaderText="Transaction Date"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                            DataFormatString="{0:dd/MM/yyyy}" HeaderButtonType="TextButton" DataField="TranDate"
                            UniqueName="TranDate">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="120px" ItemStyle-Width="120px"
                            SortExpression="Total" HeaderText="Total" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="false" Aggregate="Sum" FooterAggregateFormatString="{0:C}" DataFormatString="{0:C}"
                            HeaderButtonType="TextButton" DataField="Total" UniqueName="Total" ReadOnly="true">
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign='Right' />
                            <HeaderStyle HorizontalAlign="Right" />
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="Html"></Excel>
                </ExportSettings>
            </telerik:RadGrid>
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorProduct" runat="server"
                DropDownStyle-Width="200px" />
            <asp:Label ID="lblModelDetailID" runat="server" EnableViewState="false" Visible="false"></asp:Label>
            <telerik:GridDropDownListColumnEditor EnableViewState="false" ID="GridDropDownListColumnEditorAssets"
                runat="server" DropDownStyle-Width="250px" />
            <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
