﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using C1.Web.Wijmo.Controls.C1ComboBox;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp;
using System.Web;

namespace eclipseonlineweb
{
    public partial class ModelSummary : UMABasePage
    {
        public override void LoadPage()
        {
            this.cid = Request.QueryString["ins"].ToString();
            if (!this.IsPostBack)
            {
                LoadCombos();
            }

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");

            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                this.btnSave.Visible = false;
                this.chkDIFM.Enabled = false;
                this.chkDIWM.Enabled = false;
                this.chkDIYS.Enabled = false;
                this.lnkPrintFormsDIFM.Visible = false;
                this.lnkPrintFormsDIY.Visible = false;
                this.lnkPrintFormsDIWM.Visible = false;
                this.cmbDIFMModel.Enabled = false;
                this.cmbDIWMModel.Enabled = false;
                this.cmbDIYSModel.Enabled = false;
            }

            var sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
            {
                sm.RegisterPostBackControl(this.lnkPrintFormsDIFM);
                sm.RegisterPostBackControl(this.lnkPrintFormsDIWM);
                sm.RegisterPostBackControl(this.lnkPrintFormsDIY);
           
            }
            UMABroker.ReleaseBrokerManagedComponent(objUser);
        }

        private void LoadCombos()
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            HoldingRptDataSet ds = new HoldingRptDataSet();
            clientData.GetData(ds);
            this.PresentationData = ds;

            ClientModelsDetailsDS serviceTypes = new ClientModelsDetailsDS { CommandType = DatasetCommandTypes.Get };
            clientData.GetData(serviceTypes);

            this.PopulatePage(this.PresentationData);

            UMABroker.ReleaseBrokerManagedComponent(clientData);
            LoadControls();
            SetClientModels(serviceTypes);
        }

        private void SetClientModels(ClientModelsDetailsDS serviceTypes)
        {
            chkDIFM.Checked = serviceTypes.DoItForMe;
            chkDIWM.Checked = serviceTypes.DoItWithMe;
            chkDIYS.Checked = serviceTypes.DoItYourself;
            DataRow dr;

            #region do it for me
            if (serviceTypes.DoItForMeTable.Rows.Count > 0)
            {
                dr = serviceTypes.DoItForMeTable.Rows[0];
                if (dr[serviceTypes.DoItForMeTable.PROGRAMCODE] != DBNull.Value)
                {
                    cmbDIFMModel.SelectedValue = dr[serviceTypes.DoItForMeTable.PROGRAMCODE].ToString();
                }
                else
                {
                    cmbDIFMModel.SelectedValue = null;
                }

            }
            else
            {
                cmbDIFMModel.SelectedValue = null;
                cmbDIFMModel.Text = "";
            }
            #endregion

            #region do it with me
            if (serviceTypes.DoItWithMeTable.Rows.Count > 0)
            {
                dr = serviceTypes.DoItWithMeTable.Rows[0];
                if (dr[serviceTypes.DoItWithMeTable.PROGRAMCODE] != DBNull.Value)
                {
                    cmbDIWMModel.SelectedValue = dr[serviceTypes.DoItWithMeTable.PROGRAMCODE].ToString();
                }
                else
                {
                    cmbDIWMModel.SelectedValue = null;

                }
            }
            else
            {
                cmbDIWMModel.SelectedValue = null;
                cmbDIWMModel.Text = "";
            }
            #endregion

            #region do it yourself
            if (serviceTypes.DoItYourselfTable.Rows.Count > 0)
            {
                dr = serviceTypes.DoItYourselfTable.Rows[0];
                if (dr[serviceTypes.DoItYourselfTable.PROGRAMCODE] != DBNull.Value)
                {
                    cmbDIYSModel.SelectedValue = dr[serviceTypes.DoItYourselfTable.PROGRAMCODE].ToString();
                }
                else
                {
                    cmbDIYSModel.SelectedValue = null;
                }

            }
            else
            {
                cmbDIYSModel.SelectedValue = null;
                cmbDIYSModel.Text = "";
            }
            #endregion

        }

        private void LoadControls()
        {
            ModelsDS modelsDs = GetModels();
            LoadComboBox(modelsDs, cmbDIYSModel, ServiceTypes.DoItYourSelf);
            LoadComboBox(modelsDs, cmbDIFMModel, ServiceTypes.DoItForMe);
            LoadComboBox(modelsDs, cmbDIWMModel, ServiceTypes.DoItWithMe);
        }

        private void LoadComboBox(ModelsDS modelsDs, C1ComboBox comboBox, ServiceTypes serviceTypes)
        {
            comboBox.Items.Clear();
            DataView dataView = new DataView(modelsDs.Tables[ModelsDS.MODELLIST]);
            dataView.Sort = ModelsDS.MODELNAME;
            string serviceType = Enumeration.GetDescription(serviceTypes);
            dataView.RowFilter = ModelsDS.SERVICETYPE + " = '" + serviceType + "'";

            comboBox.DataSource = dataView.ToTable();

            comboBox.DataTextField = ModelsDS.MODELNAME;
            comboBox.DataValueField = ModelsDS.PROGRAMCODE;
            comboBox.DataBind();
        }

        private ModelsDS GetModels()
        {
            ModelsDS modelsDs = new ModelsDS { GetDetails = false };

            if (UMABroker != null)
            {
                IBrokerManagedComponent clientData = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
                clientData.GetData(modelsDs);
                UMABroker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            return modelsDs;
        }

        protected void BtnSave_Clicked(object sender, ImageClickEventArgs e)
        {
            SaveClientModels();
        }

        private void SaveClientModels()
        {
            ClientModelsDetailsDS serviceTypes = new ClientModelsDetailsDS { CommandType = DatasetCommandTypes.Update };
            serviceTypes.DoItForMe = chkDIFM.Checked;
            serviceTypes.DoItWithMe = chkDIWM.Checked;
            serviceTypes.DoItYourself = chkDIYS.Checked;

            DataRow dr;

            #region do it for  me

            dr = serviceTypes.DoItForMeTable.NewRow();
            dr[serviceTypes.DoItForMeTable.AUTHORIASTIONTYPE] = "D";
            if (cmbDIFMModel.SelectedItem != null)
            {
                dr[serviceTypes.DoItForMeTable.PROGRAMCODE] = cmbDIFMModel.SelectedItem.Value;
                dr[serviceTypes.DoItForMeTable.PROGRAMNAME] = cmbDIFMModel.SelectedItem.Text;
            }
            else
            {
                dr[serviceTypes.DoItForMeTable.PROGRAMCODE] = "";
                dr[serviceTypes.DoItForMeTable.PROGRAMNAME] = "";
            }
            serviceTypes.DoItForMeTable.Rows.Add(dr);

            #endregion

            #region do it with me

            dr = serviceTypes.DoItWithMeTable.NewRow();
            dr[serviceTypes.DoItWithMeTable.AUTHORIASTIONTYPE] = "A";
            dr[serviceTypes.DoItWithMeTable.DOITYOURSELF_AUTHORIASTIONTYPE] = "D";

            if (cmbDIWMModel.SelectedItem != null)
            {
                dr[serviceTypes.DoItWithMeTable.PROGRAMCODE] = cmbDIWMModel.SelectedItem.Value;
                dr[serviceTypes.DoItWithMeTable.PROGRAMNAME] = cmbDIWMModel.SelectedItem.Text;
            }
            else
            {
                dr[serviceTypes.DoItWithMeTable.PROGRAMCODE] = "";
                dr[serviceTypes.DoItWithMeTable.PROGRAMNAME] = "";
            }

            serviceTypes.DoItWithMeTable.Rows.Add(dr);

            #endregion

            #region do it yourself

            dr = serviceTypes.DoItYourselfTable.NewRow();
            dr[serviceTypes.DoItYourselfTable.AUTHORIASTIONTYPE] = "D";
            if (cmbDIYSModel.SelectedItem != null)
            {
                dr[serviceTypes.DoItYourselfTable.PROGRAMCODE] = cmbDIYSModel.SelectedItem.Value;
                dr[serviceTypes.DoItYourselfTable.PROGRAMNAME] = cmbDIYSModel.SelectedItem.Text;
            }
            else
            {
                dr[serviceTypes.DoItYourselfTable.PROGRAMCODE] = "";
                dr[serviceTypes.DoItYourselfTable.PROGRAMNAME] = "";
            }
            serviceTypes.DoItYourselfTable.Rows.Add(dr);

            #endregion

            this.SaveData(cid, serviceTypes);
        }
        protected void lnkPrintFormsDIWM_Click(object sender, EventArgs e)
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            ExportAppFormsDS exportAppFormsDS = new ExportAppFormsDS();
            exportAppFormsDS.serviceType = ServiceTypes.DoItWithMe;
            clientData.GetData(exportAppFormsDS);

            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            HttpContext.Current.Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
            Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
            Response.AppendHeader("Content-Type", "application/zip");
            Response.AppendHeader("Content-disposition", "attachment; filename=" + exportAppFormsDS.AppNo + ".zip");
            Response.BinaryWrite(Convert.FromBase64String(exportAppFormsDS.Contents));
            Response.Flush();
            Response.End();
        }

        protected void lnkPrintFormsDIY_Click(object sender, EventArgs e)
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            ExportAppFormsDS exportAppFormsDS = new ExportAppFormsDS();
            exportAppFormsDS.serviceType = ServiceTypes.DoItYourSelf;
            clientData.GetData(exportAppFormsDS);

            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            HttpContext.Current.Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
            Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
            Response.AppendHeader("Content-Type", "application/zip");
            Response.AppendHeader("Content-disposition", "attachment; filename=" + exportAppFormsDS.AppNo + ".zip");
            Response.BinaryWrite(Convert.FromBase64String(exportAppFormsDS.Contents));
            Response.Flush();
            Response.End();
        }
 
        protected void lnkPrintFormsDIFM_Click(object sender, EventArgs e)
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            ExportAppFormsDS exportAppFormsDS = new ExportAppFormsDS();
            exportAppFormsDS.serviceType = ServiceTypes.DoItForMe;
            clientData.GetData(exportAppFormsDS);

            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            HttpContext.Current.Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
            Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
            Response.AppendHeader("Content-Type", "application/zip");
            Response.AppendHeader("Content-disposition", "attachment; filename=" + exportAppFormsDS.AppNo + ".zip");
            Response.BinaryWrite(Convert.FromBase64String(exportAppFormsDS.Contents));
            Response.Flush();
            Response.End();
        }
    }
}
