﻿<%@ Page Title="e-Clipse Online Portal - Configure Fees" Language="C#" MasterPageFile="ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="ConfigureFees.aspx.cs" Inherits="eclipseonlineweb.ConfigureFees" %>

<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function GoToPage(url) {
            window.location = url + '?ins=<%:cid %>';
            return false;
        }
    </script>
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="4%">
                        <asp:ImageButton ID="btnBack" ToolTip="Back" AlternateText="back" runat="server"
                            ImageUrl="~/images/window_previous.png" OnClick="btnBack_Click" />
                    </td>
                    <td style="width: 85%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
                OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
                OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
                OnItemDataBound="PresentationGrid_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="Fees" TableLayout="Fixed" EditMode="EditForms">
                    <CommandItemSettings AddNewRecordText="Configure Fee" ShowExportToExcelButton="true"
                        ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                    <Columns>
                        <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ID" UniqueName="ID" />
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="FEETYPE" ReadOnly="true"
                            HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderText="Fee Type" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="FEETYPE" UniqueName="FEETYPE">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn HeaderText="Fee Template List" Visible="false" SortExpression="FEETYPE"
                            UniqueName="ServiceTypeEditor" DataField="FEETYPE" ColumnEditorID="GridDropDownColumnEditorModelServiceType" />
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="DESCRIPTION"
                            HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderText="Description" ReadOnly="true"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="DESCRIPTION" UniqueName="DESCRIPTION">
                        </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="COMMENT"
                            HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderText="Comment" ReadOnly="true"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="COMMENT" UniqueName="COMMENT">
                        </telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn Visible="true" UniqueName="STARTDATE" PickerType="DatePicker"
                            HeaderText="Start Date" DataField="STARTDATE" DataFormatString="{0:dd/MM/yyyy}">
                            <ItemStyle Width="10%" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridDateTimeColumn Visible="true" UniqueName="ENDDATE" PickerType="DatePicker"
                            HeaderText="End Date" DataField="ENDDATE" DataFormatString="{0:dd/MM/yyyy}">
                            <ItemStyle Width="10%" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridTemplateColumn UniqueName="FeeTemplateDetails">
                            <HeaderStyle Width="7%" />
                            <ItemStyle Width="7%" />
                            <ItemTemplate>
                                <asp:LinkButton ID="Definition" runat="server" Text="Fee Template" CommandArgument='<%# Eval("ID") + "," + Eval("INS") %>'
                                    CommandName="FeeTemplate"></asp:LinkButton>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridButtonColumn ConfirmText="Delete these details record?" ButtonType="ImageButton"
                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="Html"></Excel>
                </ExportSettings>
            </telerik:RadGrid>
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorFeeList" runat="server"
                DropDownStyle-Width="100px" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
