﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Utilities;

namespace eclipseonlineweb
{
    /// <summary>
    /// Keep commented lines intact - just in case
    /// </summary>
    public partial class AccountSummary : UMABasePage
    {

        private bool IsSMA
        {
            get
            {
                if (string.IsNullOrEmpty(hfIsSMA.Value))
                {
                    hfIsSMA.Value = "false";
                }
                return Convert.ToBoolean(hfIsSMA.Value);
            }
            set
            {
                hfIsSMA.Value = value.ToString();
            }
        }
        public override void LoadPage()
        {
            this.cid = Request.QueryString["ins"];
            if (!this.Page.IsPostBack)
            {
                SetStatusCombos();
                GetData();
                SetData();
                IsSMA = SMAUtilities.IsSMAClient(new Guid(cid), UMABroker);

                if (IsSMA)
                {
                    lblMdaExecutionDateClientCorpo.Visible = lblIndividualClientMda.Visible = false;
                    lblMDAExceDate.Visible = lblIndividualMDAExceDate.Visible = false;
                }
            }
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                this.ibtnSave.Visible = false;
                this.pnlCorporateClientDetails.Enabled = false;
            }
            UMABroker.ReleaseBrokerManagedComponent(objUser);

        }

        private void SetStatusCombos()
        {
            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var status = new StatusDS { TypeFilter = "Client" };
            org.GetData(status);

            ComboCorporateClientStatus.Items.Clear();
            ComboCorporateClientStatus.DataSource = status.Tables[StatusDS.TABLENAME];
            ComboCorporateClientStatus.DataTextField = StatusDS.NAME;
            ComboCorporateClientStatus.DataValueField = StatusDS.NAME;
            ComboCorporateClientStatus.DataBind();

            ComboIndividualClientStatus.Items.Clear();
            ComboIndividualClientStatus.DataSource = status.Tables[StatusDS.TABLENAME];
            ComboIndividualClientStatus.DataTextField = StatusDS.NAME;
            ComboIndividualClientStatus.DataValueField = StatusDS.NAME;
            ComboIndividualClientStatus.DataBind();
        }


        private void GetData()
        {
            IOrganizationUnit clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            ClientAccountsDetailsDS ds = new ClientAccountsDetailsDS();
            clientData.GetData(ds);
            this.PresentationData = ds;
            pnlCorporateClientDetails.Visible = clientData.IsCorporateType();
            pnlIndividualClientDetails.Visible = !clientData.IsCorporateType();
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        private void SetData()
        {
            var ds = PresentationData as ClientAccountsDetailsDS;
            var clientData = UMABroker.GetBMCInstance(new Guid(cid)) as OrganizationUnitCM;
            var user = GetCurrentUser();
            if (clientData != null)
            {
                btnRefresh.Visible = SMAUtilities.IsSMAClient(clientData.TypeName);
                if (clientData.IsCorporateType())
                {
                    var entity = ds.ClientEntity as CorporateEntity;
                    lblMDAExceDate.Enabled = lblCreatedDate.Enabled = user.IsAdmin;
                    lblCreatedDate.SelectedDate = clientData.CreationDate == DateTime.MinValue ? (DateTime?)null : clientData.CreationDate;

                    lblUpdateDate.Text = clientData.UpdationDate == DateTime.MinValue ? string.Empty : clientData.UpdationDate.ToString();
                    lblCliID.Text = entity.ClientId = clientData.ClientId;//r u there?
                    lblApplicationID.Text = clientData.ApplicationID;
                    lblAccountName.Text = entity.Name;
                    txtCorporateTrusteeName.Text = entity.CorporateTrusteeName;
                    txtEntityName.Text = entity.EntityName;
                    txtLegalName.Text = entity.LegalName;
                    ABN_InputControl1.SetEntity(entity.ABN);
                    TFN_InputControl1.SetEntity(entity.TFN);
                    ACN_InputControl2.SetEntity(entity.ACN);
                    lblAccountType.Text = clientData.ClientAccountType;
                    TxtAddressControl.SetEntity(entity.Address.BusinessAddress.Addressline1,
                                                entity.Address.BusinessAddress.Addressline2,
                                                entity.Address.BusinessAddress.Suburb,
                                                entity.Address.BusinessAddress.State,
                                                entity.Address.BusinessAddress.PostCode,
                                                entity.Address.BusinessAddress.Country);

                    lblAccountDesignation.Text = entity.AccountDesignation;
                    txtInvestmentDetails.Text = entity.InvestmentDetails;
                    txtTIN.Text = entity.TIN;
                    txtTrusteeType.Text = entity.TrusteeType;
                    txtTrustPassword.Text = entity.TrustPassword;
                    lblAdviserID.Text = ds.AdviserID;
                    lblAdviserFullName.Text = ds.AdviserFullName;

                    if (!string.IsNullOrEmpty(clientData.OrganizationStatus))
                        ComboCorporateClientStatus.SelectedValue = clientData.OrganizationStatus;
                    lblMDAExceDate.SelectedDate = entity.MDAExecutionDate;
                }
                else
                {
                    var entity = ds.ClientEntity as ClientIndividualEntity;
                    lblIndividualMDAExceDate.Enabled = Individual_CreatedDate.Enabled = user.IsAdmin;
                    Individual_CreatedDate.SelectedDate = clientData.CreationDate == DateTime.MinValue ? (DateTime?)null : clientData.CreationDate;
                    Individual_UpdateDate.Text = clientData.UpdationDate == DateTime.MinValue ? string.Empty : clientData.UpdationDate.ToString();
                    lblCliID.Text = entity.ClientId = clientData.ClientId;
                    lblApplicationID.Text = clientData.ApplicationID;
                    txtIndividual_Name.Text = entity.Name;
                    txtIndividual_ClientId.Text = clientData.ClientId;
                    txtIndividual_InvDetails.Text = entity.InvestmentDetails;
                    txtIndividual_LegalName.Text = entity.LegalName;
                    lblInd_ClientType.Text = clientData.ClientAccountType;
                    txtIndividual_Address.SetEntity(entity.Address.RegisteredAddress.Addressline1,
                                                    entity.Address.RegisteredAddress.Addressline2,
                                                    entity.Address.RegisteredAddress.Suburb,
                                                    entity.Address.RegisteredAddress.State,
                                                    entity.Address.RegisteredAddress.PostCode,
                                                    entity.Address.RegisteredAddress.Country);
                    txtIndividual_ABN.SetEntity(entity.ABN);
                    txtIndividual_TFN.SetEntity(entity.TFN);
                    lblAdviserIDIndividual.Text = ds.AdviserID;
                    lblAdviserNameIndividual.Text = ds.AdviserFullName;
                    if (!string.IsNullOrEmpty(clientData.OrganizationStatus))
                        ComboIndividualClientStatus.SelectedValue = clientData.OrganizationStatus;
                    lblIndividualMDAExceDate.SelectedDate = entity.MDAExecutionDate;
                }
            }
        }

        protected void btnSave_Onclick(object sender, EventArgs e)
        {
            var clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as OrganizationUnitCM;
            var dsNew = new ClientAccountsDetailsDS();
            if (clientData != null)
            {
                clientData.IsInvestableClient = true;
                clientData.UpdationDate = DateTime.Now;

                var user = GetCurrentUser();
                if (clientData.IsCorporateType())
                {
                    var entity = clientData.ClientEntity as CorporateEntity;
                    clientData.Name = this.lblAccountName.Text;
                    if (entity != null)
                    {
                        if (user.IsAdmin)
                        {
                            clientData.CreationDate = lblCreatedDate.SelectedDate.HasValue ? lblCreatedDate.SelectedDate.Value : DateTime.Now;
                            entity.MDAExecutionDate = lblMDAExceDate.SelectedDate.HasValue ? lblMDAExceDate.SelectedDate.Value.Date : (DateTime?)null;
                        }
                        entity.ClientId = this.lblCliID.Text;
                        entity.ClientName = this.lblAccountName.Text;
                        entity.Name = this.lblAccountName.Text;

                        entity.EntityName = this.txtEntityName.Text;
                        entity.CorporateTrusteeName = this.txtCorporateTrusteeName.Text;
                        entity.LegalName = this.txtLegalName.Text;
                        if (this.ABN_InputControl1.Validate())
                            entity.ABN = this.ABN_InputControl1.GetEntity();
                        else
                            return;//for now : skip saving

                        if (this.TFN_InputControl1.Validate())
                            entity.TFN = TFN_InputControl1.GetEntity();
                        else
                            return;//for now : skip saving

                        if (this.ACN_InputControl2.Validate())
                            entity.ACN = this.ACN_InputControl2.GetEntity();
                        else
                            return;//for now : skip saving

                        entity.Address.BusinessAddress.Addressline1 = TxtAddressControl.AddressLine1;
                        entity.Address.BusinessAddress.Addressline2 = TxtAddressControl.AddressLine2;
                        entity.Address.BusinessAddress.PostCode = TxtAddressControl.Postcode;
                        entity.Address.BusinessAddress.Suburb = TxtAddressControl.Subrub;
                        entity.Address.BusinessAddress.Country = TxtAddressControl.Country;
                        entity.Address.BusinessAddress.State = TxtAddressControl.State;
                        entity.AccountDesignation = this.lblAccountDesignation.Text;
                        entity.InvestmentDetails = this.txtInvestmentDetails.Text;
                        entity.TIN = this.txtTIN.Text;
                        entity.TrusteeType = this.txtTrusteeType.Text;
                        entity.TrustPassword = this.txtTrustPassword.Text;

                        if (!string.IsNullOrEmpty(ComboCorporateClientStatus.SelectedValue))
                        {
                            entity.OrganizationStatus = ComboCorporateClientStatus.SelectedValue;
                            clientData.OrganizationStatus = ComboCorporateClientStatus.SelectedValue;
                            StatusType tempstatus;
                            if (System.Enum.TryParse(ComboCorporateClientStatus.SelectedValue, true, out tempstatus))
                                clientData.StatusType = tempstatus;

                        }
                        dsNew.ClientEntity = entity;
                    }
                }
                else
                {
                    var entity = clientData.ClientEntity as ClientIndividualEntity;
                    clientData.Name = txtIndividual_Name.Text;
                    if (entity != null)
                    {
                        if (user.IsAdmin)
                        {

                            clientData.CreationDate = Individual_CreatedDate.SelectedDate.HasValue ? Individual_CreatedDate.SelectedDate.Value : DateTime.Now;
                            entity.MDAExecutionDate = lblIndividualMDAExceDate.SelectedDate.HasValue ? lblIndividualMDAExceDate.SelectedDate.Value : (DateTime?)null;
                        }
                        entity.ClientId = txtIndividual_ClientId.Text;
                        entity.Name = txtIndividual_Name.Text;
                        entity.InvestmentDetails = txtIndividual_InvDetails.Text;
                        entity.LegalName = txtIndividual_LegalName.Text;



                        entity.Address.MailingAddress.Addressline1 = txtIndividual_Address.AddressLine1;
                        entity.Address.MailingAddress.Addressline2 = txtIndividual_Address.AddressLine2;
                        entity.Address.MailingAddress.PostCode = txtIndividual_Address.Postcode;
                        entity.Address.MailingAddress.Suburb = txtIndividual_Address.Subrub;
                        entity.Address.MailingAddress.Country = txtIndividual_Address.Country;
                        entity.Address.MailingAddress.State = txtIndividual_Address.State;

                        entity.Address.ResidentialAddress.Addressline1 = txtIndividual_Address.AddressLine1;
                        entity.Address.ResidentialAddress.Addressline2 = txtIndividual_Address.AddressLine2;
                        entity.Address.ResidentialAddress.PostCode = txtIndividual_Address.Postcode;
                        entity.Address.ResidentialAddress.Suburb = txtIndividual_Address.Subrub;
                        entity.Address.ResidentialAddress.Country = txtIndividual_Address.Country;
                        entity.Address.ResidentialAddress.State = txtIndividual_Address.State;



                        if (txtIndividual_ABN.Validate())
                            entity.ABN = txtIndividual_ABN.GetEntity();
                        else
                            return;//for now : skip saving

                        if (txtIndividual_TFN.Validate())
                            entity.TFN = txtIndividual_TFN.GetEntity();
                        else
                            return;//for now : skip saving

                        if (!string.IsNullOrEmpty(ComboIndividualClientStatus.SelectedValue))
                        {
                            entity.OrganizationStatus = ComboIndividualClientStatus.SelectedValue;
                            clientData.OrganizationStatus = ComboIndividualClientStatus.SelectedValue;
                            StatusType tempstatus;
                            if (System.Enum.TryParse(ComboIndividualClientStatus.SelectedValue, true, out tempstatus))
                                clientData.StatusType = tempstatus;


                        }

                        dsNew.ClientEntity = entity;
                    }
                }

                UMABroker.SaveOverride = true;
                clientData.CalculateToken(true);
                UMABroker.SetComplete();
                UMABroker.SetStart();
            }
            GetData();
            SetData();
        }

        protected void ibtnRefreshSMA_Click(object sender, ImageClickEventArgs e)
        {
            SMAUtilities.RefreshClientData(new Guid(cid), UMABroker, GetCurrentUser());
        }
    }
}
