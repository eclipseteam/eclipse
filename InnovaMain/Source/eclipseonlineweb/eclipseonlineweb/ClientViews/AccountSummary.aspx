﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="AccountSummary.aspx.cs" Inherits="eclipseonlineweb.AccountSummary" %>

<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ABN_InputControl" Src="~/Controls/ABN_InputControl.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ACN_InputControl" Src="~/Controls/ACN_InputControl.ascx" %>
<%@ Register TagPrefix="uc2" TagName="TFN_InputControl" Src="~/Controls/TFN_InputControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register TagPrefix="uc2" TagName="AddressControl" Src="~/Controls/AddressControl.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .Headingtd
        {
            font-weight: normal;
            text-align: left;
            font-size: 12px;
            width: 25%;
        }
        .HeadingtdRight
        {
            font-weight: normal;
            text-align: right;
            font-size: 12px;
            width: 25%;
        }
        .HeadingtdBold
        {
            font-weight: bold;
            text-align: right;
            font-size: 12px;
            width: 25%;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <telerik:RadButton runat="server" ID="ibtnSave" ToolTip="Save Changes" OnClick="btnSave_Onclick"
                                Width="70PX">
                                <ContentTemplate>
                                    <img src="../images/Save-Icon.png" alt="" class="btnImageWithText" />
                                    <span class="riLabel">Save</span>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </td>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnRefresh" AlternateText="Refresh Super Data" ToolTip="Refresh Super Data"
                                OnClick="ibtnRefreshSMA_Click" runat="server" ImageUrl="~/images/refresh_small.png"
                                Visible="false" />
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <div id="MainView">
                <asp:Panel runat="server" ID="pnlCorporateClientDetails" Visible="false">
                    <fieldset>
                        <legend>Account Info</legend>
                        <table width="100%">
                            <tr>
                                <td class="Headingtd">
                                    Client ID:
                                </td>
                                <td style="width: 25%; text-align: left">
                                    <telerik:RadTextBox runat="server" ID="lblCliID" ReadOnly="True" Width="420px" Text="">
                                    </telerik:RadTextBox>
                                </td>
                                <td class="HeadingtdBold">
                                    Created On:
                                </td>
                                <td style="width: 25%; text-align: left">
                                    <telerik:RadDateTimePicker ID="lblCreatedDate" runat="server" Width="202px" TimePopupButton="false">
                                        <DateInput DateFormat="dd/MM/yyyy hh:mm tt" ReadOnly="true" DisplayDateFormat="dd/MM/yyyy hh:mm tt">
                                        </DateInput>
                                    </telerik:RadDateTimePicker>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd">
                                    Application ID:
                                </td>
                                <td style="width: 25%; text-align: left">
                                    <telerik:RadTextBox runat="server" ID="lblApplicationID" ReadOnly="True" Width="420px"
                                        Text="">
                                    </telerik:RadTextBox>
                                </td>
                                <td class="HeadingtdRight">
                                    Updated On:
                                </td>
                                <td style="width: 25%; text-align: left">
                                    <telerik:RadTextBox runat="server" ID="lblUpdateDate" ReadOnly="True" Width="150px"
                                        Text="">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd">
                                    Adviser ID:
                                </td>
                                <td style="text-align: left;">
                                    <telerik:RadTextBox runat="server" ID="lblAdviserID" ReadOnly="True" Width="420px"
                                        Text="">
                                    </telerik:RadTextBox>
                                </td>
                                <td class="HeadingtdRight">
                                    Status:
                                </td>
                                <td style="text-align: left;">
                                    <telerik:RadComboBox runat="server" ID="ComboCorporateClientStatus" Width="150px"
                                        AutoPostBack="false">
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    Adviser Name:
                                </td>
                                <td style="text-align: left;">
                                    <telerik:RadTextBox runat="server" ID="lblAdviserFullName" ReadOnly="True" Width="420px"
                                        Text="">
                                    </telerik:RadTextBox>
                                </td>
                                <td style="text-align: right;">
                                    <asp:Label runat="server" ID="lblMdaExecutionDateClientCorpo" Text="MDA Execution Date:"
                                        CssClass="HeadingtdRight"></asp:Label>
                                </td>
                                <td style="width: 25%; text-align: left">
                                    <telerik:RadDatePicker ID="lblMDAExceDate" runat="server" Width="177px" AutoPostBack="false"
                                        MinDate="01/01/1000" MaxDate="01/01/3000">
                                        <DateInput DateFormat="dd/MM/yyyy">
                                        </DateInput>
                                        <Calendar>
                                            <SpecialDays>
                                                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-CssClass="rcToday">
                                                </telerik:RadCalendarDay>
                                            </SpecialDays>
                                        </Calendar>
                                    </telerik:RadDatePicker>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    Account Name:
                                </td>
                                <td class="Headingtd" colspan="3">
                                    <telerik:RadTextBox runat="server" ID="lblAccountName" Width="420px" Text="">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    ABN:
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <uc2:ABN_InputControl ID="ABN_InputControl1" runat="server" InvalidMessage="Invalid ABN (e.g. 53 004 085 616)" />
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    ACN:
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <uc2:ACN_InputControl ID="ACN_InputControl2" runat="server" InvalidMessage="Invalid ACN (e.g. 010 499 966)" />
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    Account Type:
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <telerik:RadTextBox runat="server" ID="lblAccountType" Width="420px" Text="" ReadOnly="True">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    Account Designation:
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <telerik:RadTextBox runat="server" ID="lblAccountDesignation" Width="420px" Text="">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    Corporate Trustee Name:
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <telerik:RadTextBox runat="server" ID="txtCorporateTrusteeName" Width="420px" Text="">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    Entity Name:
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <telerik:RadTextBox runat="server" ID="txtEntityName" Width="420px" Text="">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    Investment Details:
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <telerik:RadTextBox runat="server" ID="txtInvestmentDetails" Width="420px" Text="">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    Legal Name:
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <telerik:RadTextBox runat="server" ID="txtLegalName" Width="420px" Text="">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    TFN:
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <uc2:TFN_InputControl ID="TFN_InputControl1" runat="server" InvalidMessage="Invalid TFN (e.g. 123 456 782)" />
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    TIN:
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <telerik:RadTextBox runat="server" ID="txtTIN" Width="420px" Text="">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    Trustee Type:
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <telerik:RadTextBox runat="server" ID="txtTrusteeType" Width="420px" Text="">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    Trust Password:
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <telerik:RadTextBox runat="server" ID="txtTrustPassword" Width="420px" Text="">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <uc2:AddressControl runat="server" ID="TxtAddressControl" ShowFillAsCombo="False"
                                        Title="Address" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlIndividualClientDetails" Visible="false" Width="100%">
                    <fieldset>
                        <legend>Account Info</legend>
                        <table width="100%">
                            <tr>
                                <td class="Headingtd">
                                    Client ID:
                                </td>
                                <td style="width: 25%; text-align: left">
                                    <telerik:RadTextBox runat="server" ID="txtIndividual_ClientId" Width="420px" Text=""
                                        ReadOnly="True">
                                    </telerik:RadTextBox>
                                </td>
                                <td class="HeadingtdRight">
                                    Created On:
                                </td>
                                <td style="width: 25%; text-align: left">
                                    <telerik:RadDateTimePicker ID="Individual_CreatedDate" runat="server" Width="202px">
                                        <DateInput DateFormat="dd/MM/yyyy hh:mm tt" ReadOnly="true" DisplayDateFormat="dd/MM/yyyy hh:mm tt">
                                        </DateInput>
                                    </telerik:RadDateTimePicker>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd">
                                    Name:
                                </td>
                                <td style="width: 25%; text-align: left">
                                    <telerik:RadTextBox runat="server" ID="txtIndividual_Name" Width="420px" Text=""
                                        Enabled="True">
                                    </telerik:RadTextBox>
                                </td>
                                <td class="HeadingtdRight">
                                    Updated On:
                                </td>
                                <td style="width: 25%; text-align: left">
                                    <telerik:RadTextBox runat="server" ID="Individual_UpdateDate" Width="150px" Text=""
                                        ReadOnly="True">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd">
                                    Adviser ID:
                                </td>
                                <td style="text-align: left;">
                                    <telerik:RadTextBox runat="server" ID="lblAdviserIDIndividual" Width="420px" Text=""
                                        ReadOnly="True">
                                    </telerik:RadTextBox>
                                </td>
                                <td class="HeadingtdRight">
                                    Status:
                                </td>
                                <td style="text-align: left;">
                                    <telerik:RadComboBox runat="server" ID="ComboIndividualClientStatus" Width="150px"
                                        AutoPostBack="false">
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    Adviser Name:
                                </td>
                                <td style="text-align: left;">
                                    <telerik:RadTextBox runat="server" ID="lblAdviserNameIndividual" Width="420px" Text=""
                                        ReadOnly="True">
                                    </telerik:RadTextBox>
                                </td>
                                <td style="text-align: right;">
                                    <asp:Label runat="server" ID="lblIndividualClientMda" Text="MDA Execution Date:"
                                        CssClass="HeadingtdRight"></asp:Label>
                                </td>
                                <td style="width: 25%; text-align: left">
                                    <telerik:RadDatePicker ID="lblIndividualMDAExceDate" runat="server" Width="177px"
                                        AutoPostBack="false" MinDate="01/01/1000" MaxDate="01/01/3000">
                                        <DateInput DateFormat="dd/MM/yyyy">
                                        </DateInput>
                                        <Calendar>
                                            <SpecialDays>
                                                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-CssClass="rcToday">
                                                </telerik:RadCalendarDay>
                                            </SpecialDays>
                                        </Calendar>
                                    </telerik:RadDatePicker>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    Investment Details:
                                </td>
                                <td style="text-align: left; font-size: 12px" colspan="3">
                                    <telerik:RadTextBox runat="server" ID="txtIndividual_InvDetails" Width="420px" Text="">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    Legal Name:
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <telerik:RadTextBox runat="server" ID="txtIndividual_LegalName" Width="420px" Text="">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    Account Type:
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <telerik:RadTextBox runat="server" ID="lblInd_ClientType" Width="420px" Text="" ReadOnly="True">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    TFN:
                                </td>
                                <td style="text-align: left" colspan="3">
                                    <uc2:TFN_InputControl ID="txtIndividual_TFN" runat="server" InvalidMessage="Invalid TFN (e.g. 123 456 782)" />
                                </td>
                            </tr>
                            <tr>
                                <td class="Headingtd" colspan="1">
                                    ABN:
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <uc2:ABN_InputControl ID="txtIndividual_ABN" runat="server" InvalidMessage="Invalid ABN (e.g. 53 004 085 616)" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <uc2:AddressControl runat="server" ID="txtIndividual_Address" ShowFillAsCombo="False"
                                        Title="Address" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
                <asp:HiddenField ID="hfIsSMA" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
