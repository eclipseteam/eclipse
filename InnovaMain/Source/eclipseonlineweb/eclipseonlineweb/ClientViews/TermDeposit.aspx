﻿<%@ Page Title="Term Deposit" Language="C#" MasterPageFile="ClientViewMaster.master" AutoEventWireup="true"
    CodeBehind="TermDeposit.aspx.cs" Inherits="eclipseonlineweb.ClientViews.TermDeposit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .wijmo-wijgrid .wijmo-wijgrid-groupheaderrow td
        {
            background-color: #DAE6F4;
            color: #000000;
            font-size: smaller;
            font-weight: bold;
            vertical-align: middle;
        }
        .wijmo-wijgrid .wijmo-wijgrid-footerrow td
        {
            font-weight: bolder;
            text-align: right;
            font-size: smaller;
        }
    </style>
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function cancel() {
            window.close();
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
           
            <%--<div id="Table-Grid">--%>
                <asp:Panel runat="server" ID="pnlBank" Visible="false">
                    <c1:C1GridView ClientSelectionMode="SingleRow" AlternatingRowStyle-Font-Size="X-Small"
                        FilterStyle-Font-Size="X-Small" AllowSorting="true" OnSorting="SortTermDeposit" RowStyle-Font-Size="X-Small"
                        HeaderStyle-Font-Size="X-Small" ID="TermDepositGrid" AllowPaging="true" PageSize="18"
                        FooterStyle-Font-Size="Small" runat="server" AutogenerateColumns="false" ShowFilter="true"
                        OnFiltering="FilterTermDeposit" OnPageIndexChanging="TermDeposit_PageIndexChanging" Height="100%"
                        Width="100%" ShowFooter="false" OnRowCommand="Grid_RowCommand">
                        <Columns>
                            <%--<c1:C1BoundField DataField="CSID" Visible="false" SortExpression="CSID" HeaderText="CSID">
                                <ItemStyle HorizontalAlign="Left" />
                            </c1:C1BoundField>
                            <c1:C1BoundField DataField="CLID" Visible="false" SortExpression="CLID" HeaderText="CLID">
                                <ItemStyle HorizontalAlign="Left" />
                            </c1:C1BoundField>--%>
                            <c1:C1BoundField DataField="AccountName" SortExpression="AccountName" HeaderText="Account Name">
                                <ItemStyle HorizontalAlign="Left" />
                            </c1:C1BoundField>
                            <c1:C1BoundField DataField="AccountType" SortExpression="AccountType" HeaderText="Account Type">
                                <ItemStyle HorizontalAlign="Left" />
                            </c1:C1BoundField>
                            <c1:C1BoundField DataField="BSBNo" SortExpression="BSBNo" HeaderText="BSB">
                                <ItemStyle HorizontalAlign="Left" />
                            </c1:C1BoundField>
                            <c1:C1BoundField DataField="AccountNo" SortExpression="AccountNo" HeaderText="Account Number">
                                <ItemStyle HorizontalAlign="Left" />
                            </c1:C1BoundField>
                            <c1:C1BoundField DataField="Institution" SortExpression="Institution" HeaderText="Institution">
                                <ItemStyle HorizontalAlign="Left" />
                            </c1:C1BoundField>
                            <%--<c1:C1CommandField ShowSelectButton="true" SelectText="Add">
                                <ItemStyle Font-Size="X-Small" />
                            </c1:C1CommandField>
                            <c1:C1CommandField ShowEditButton="true" EditText="Edit">
                                <ItemStyle Font-Size="X-Small" />
                            </c1:C1CommandField>--%>
                            <%--<c1:C1CommandField ShowDeleteButton="true" DeleteText="Delete">
                                <ItemStyle Font-Size="X-Small" />
                            </c1:C1CommandField>--%>
                        </Columns>
                    </c1:C1GridView>
                </asp:Panel>
            <%--</div>--%>
            <asp:ModalPopupExtender ID="ModalPopupExtenderEditTran" runat="server" CancelControlID="btnCancel"
                TargetControlID="btnShowPopup" PopupControlID="pnlpopup" PopupDragHandleControlID="PopupHeader"
                Drag="true" BackgroundCssClass="ModalPopupBG">
            </asp:ModalPopupExtender>
            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
            <asp:Panel ID="pnlpopup" runat="server" BackColor="White" Style="display: none">
                <div class="popup_Container">
                    <div class="popup_Titlebar" id="PopupHeader">
                        <div class="TitlebarLeft">
                            Transaction Details
                        </div>
                        <div class="TitlebarRight" onclick="cancel();">
                        </div>
                    </div>
                    <div class="PopupBody">
                        <table>
                            <tr>
                                <td>
                                </td>
                                <td style="width: 200px">
                                    Bank Account Details:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxBankAccountList" DataTextField="BANKCODE" DataValueField="BankCID"
                                        runat="server" Width="200px" CssClass="" ViewStateMode="Enabled" Height="100px"
                                        EnableTheming="false" TriggerPosition="Right" ShowTrigger="true" ShowingAnimation-Animated-Disabled="true">
                                    </c1:C1ComboBox>
                                    <c1:C1InputMask runat="server" ID="lblBankDetails" Width="210px" Height="10px" DisableUserInput="true"
                                        Enabled="true">
                                    </c1:C1InputMask>
                                    <asp:Label Visible="false" runat="server" ID="lblTranID"></asp:Label><asp:Label Visible="false"
                                        runat="server" ID="lblBankCID"></asp:Label>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Import Transaction Type:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxImport" runat="server" Width="200px" CssClass="" ViewStateMode="Enabled"
                                        Height="100px" EnableTheming="false" TriggerPosition="Right" ShowTrigger="true"
                                        ShowingAnimation-Animated-Disabled="true">
                                        <Items>
                                            <c1:C1ComboBoxItem Text="Please" Value="Please " />
                                            <c1:C1ComboBoxItem Text="Deposit" Value="Deposit" />
                                            <c1:C1ComboBoxItem Text="Withdrawal" Value="Withdrawal" />
                                        </Items>
                                    </c1:C1ComboBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Category:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxCat" runat="server" Width="200px">
                                        <Items>
                                            <c1:C1ComboBoxItem Text="Benefit Payment" Value="Benefit Payment" />
                                            <c1:C1ComboBoxItem Text="Contribution" Value="Contribution" />
                                            <c1:C1ComboBoxItem Text="Expense" Value="Expense" />
                                            <c1:C1ComboBoxItem Text="Income" Value="Income" />
                                            <c1:C1ComboBoxItem Text="Investment" Value="Investment" />
                                            <c1:C1ComboBoxItem Text="TAX" Value="TAX" />
                                        </Items>
                                    </c1:C1ComboBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    System Transaction Type:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxSystem" runat="server" Width="200px">
                                        <Items>
                                            <c1:C1ComboBoxItem Text="Accounting Expense" Value="" />
                                            <c1:C1ComboBoxItem Text="Administration Fee" Value="" />
                                            <c1:C1ComboBoxItem Text="Advisory Fee" Value="" />
                                            <c1:C1ComboBoxItem Text="Application" Value="" />
                                            <c1:C1ComboBoxItem Text="Business Activity Statement" Value="" />
                                            <c1:C1ComboBoxItem Text="Commission Rebate" Value="" />
                                            <c1:C1ComboBoxItem Text="Distribution" Value="" />
                                            <c1:C1ComboBoxItem Text="Dividend" Value="" />
                                            <c1:C1ComboBoxItem Text="Employer Additional" Value="" />
                                            <c1:C1ComboBoxItem Text="Employer SG" Value="" />
                                            <c1:C1ComboBoxItem Text="Income Tax" Value="" />
                                            <c1:C1ComboBoxItem Text="Instalment Activity Statement" Value="" />
                                            <c1:C1ComboBoxItem Text="Insurance Premium" Value="" />
                                            <c1:C1ComboBoxItem Text="Interest" Value="" />
                                            <c1:C1ComboBoxItem Text="Internal Cash Movement" Value="" />
                                            <c1:C1ComboBoxItem Text="Internal Cash Movement" Value="" />
                                            <c1:C1ComboBoxItem Text="Investment Fee" Value="" />
                                            <c1:C1ComboBoxItem Text="Legal Expense" Value="" />
                                            <c1:C1ComboBoxItem Text="PAYG" Value="" />
                                            <c1:C1ComboBoxItem Text="Pension Payment" Value="" />
                                            <c1:C1ComboBoxItem Text="Personal" Value="" />
                                            <c1:C1ComboBoxItem Text="Property" Value="" />
                                            <c1:C1ComboBoxItem Text="Redemption" Value="" />
                                            <c1:C1ComboBoxItem Text="Regulatory Fee" Value="" />
                                            <c1:C1ComboBoxItem Text="Rental Income" Value="" />
                                            <c1:C1ComboBoxItem Text="Salary Sacrifice" Value="" />
                                            <c1:C1ComboBoxItem Text="Spouse" Value="" />
                                            <c1:C1ComboBoxItem Text="Tax Refund" Value="" />
                                            <c1:C1ComboBoxItem Text="Transfer In" Value="" />
                                            <c1:C1ComboBoxItem Text="Transfer Out" Value="" />
                                            <c1:C1ComboBoxItem Text="Withdrawal" Value="" />
                                        </Items>
                                    </c1:C1ComboBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Amount:
                                </td>
                                <td>
                                    <c1:C1InputCurrency Width="210px" ID="Amount" runat="server" ShowSpinner="true" Value="50">
                                    </c1:C1InputCurrency>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Adjustment:
                                </td>
                                <td>
                                    <c1:C1InputCurrency Width="210px" ID="Adjustment" runat="server" ShowSpinner="true"
                                        Value="50">
                                    </c1:C1InputCurrency>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Total Amount:
                                </td>
                                <td>
                                    <c1:C1InputCurrency Width="210px" Enabled="false" ID="TotalAmount" runat="server"
                                        ShowSpinner="true" Value="50">
                                    </c1:C1InputCurrency>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Transaction Date:
                                </td>
                                <td>
                                    <c1:C1InputDate runat="server" ID="transactionDate" Width="210px" Height="10px" DateFormat="dd/MM/yyyy">
                                    </c1:C1InputDate>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Comments:
                                </td>
                                <td>
                                    <asp:TextBox CssClass="EditTextArea" TextMode="MultiLine" Width="205px" Rows="3"
                                        runat="server" ID="textComments"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnAdd" CommandName="Update" runat="server" Text="Add" OnClick="AddTransaction" />
                                    <asp:Button ID="btnUpdate" CommandName="Update" runat="server" Text="Update" OnClick="UpdateTransaction" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
            <asp:Button Visible="false" runat="server" ID="btnEdit" OnClick="EditTransaction"
                Text="Text" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
