﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="AccessFacilities.aspx.cs" Inherits="eclipseonlineweb.ClientViews.AccessFacilities" %>

<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register Src="../Controls/BWAAccessFacilities.ascx" TagName="BWAAccessFacilities"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <telerik:RadButton ID="btnSave" runat="server" Text="RadButton" Width="60px" ToolTip="Save Changes"
                                OnClick="btnSave_OnClick">
                                <ContentTemplate>
                                    <img alt="" src="../images/Save-Icon.png" class="btnImageWithText" />
                                    <span class="riLabel">Save</span>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <div class="MainView">
                <uc2:BWAAccessFacilities ID="BWAAccessFacilitiesDIY" runat="server" BWAAccessFacilityType="DoItYourSelf" />
                <uc2:BWAAccessFacilities ID="BWAAccessFacilitiesDIWM" runat="server" BWAAccessFacilityType="DoItWithMe" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
