﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.CM.OrganizationUnit;

namespace eclipseonlineweb
{
    public partial class ClientFees : UMABasePage
    {
        protected override void GetBMCData()
        {
            this.cid = Request.QueryString["ins"].ToString();
            IOrganizationUnit clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            ClientFeesDS clientFeesDS = new ClientFeesDS();
            clientData.GetData(clientFeesDS);
            this.PresentationData = clientFeesDS;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
        }

        public override void LoadPage()
        {
            base.LoadPage();

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                ibtnSave.Visible = false;
                chkFeePaid.Enabled = false; 
                UpFrontFee.ReadOnly = true;
                pnlSelection.Enabled = false;
            }

            if (!this.Page.IsPostBack)
            {
                GetData();
                SetFeePanels();
            }
        }

        protected void Button_Click(object sender, EventArgs e)
        {
        }

        protected void ibtnSave_Click(object sender, EventArgs e)
        {
            ClientFeesDS feeDS = new ClientFeesDS();
            feeDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
            SetData(feeDS);
            this.SaveData(this.cid, feeDS);
            this.PresentationData = feeDS;
            this.GetData();
        }
       
        protected void RadioCheckChanged_Clicked(Object sender, EventArgs e)
        {
            this.btnSelected.Value = ((Telerik.Web.UI.RadButton)sender).ID;
            SetFeePanels();
        }

        private void GetData()
        {
            ClientFeesDS clientFeesDS = (ClientFeesDS)this.PresentationData;

            ClearOnoigngValueControls();
            ClearOnoingPercentControls();
            ClearTieredControls();

            UpFrontFee.Text = clientFeesDS.ClientFeeEntity.UpFrontFee.ToString();
            chkFeePaid.Checked = clientFeesDS.ClientFeeEntity.UpFrontFeePaid;

            if (clientFeesDS.ClientFeeEntity.OngoingFeePercent)
            {
                OngoingFeePercent.Text = clientFeesDS.ClientFeeEntity.OngoingFeePercentage.ToString("N2");
                this.btnOngoingFeePercent.Checked = true;
                this.pnlOngoingFeePercent.Visible = true;
                this.btnSelected.Value = this.btnOngoingFeePercent.ID;
            }

            if (clientFeesDS.ClientFeeEntity.OnGoingFeeValue)
            {
                OngoingFeeVal.Text = clientFeesDS.ClientFeeEntity.OngoingFeeDollar.ToString("N2");
                this.pnlOngoingFeeVal.Visible = true;
                this.btnOngoingFeeValue.Checked = true;
                this.btnSelected.Value = this.btnOngoingFeeValue.ID;
            }

            if (clientFeesDS.ClientFeeEntity.OngoingTierFee)
            {
                To1.Text = clientFeesDS.ClientFeeEntity.Tier1To.ToString("N2");
                From1.Text = clientFeesDS.ClientFeeEntity.Tier1From.ToString("N2");
                Equal1.Text = clientFeesDS.ClientFeeEntity.Tier1Percentage.ToString("N2");

                To2.Text = clientFeesDS.ClientFeeEntity.Tier2To.ToString("N2");
                From2.Text = clientFeesDS.ClientFeeEntity.Tier2From.ToString("N2");
                Equal2.Text = clientFeesDS.ClientFeeEntity.Tier2Percentage.ToString("N2");

                To3.Text = clientFeesDS.ClientFeeEntity.Tier3To.ToString("N2");
                From3.Text = clientFeesDS.ClientFeeEntity.Tier3From.ToString("N2");
                Equal3.Text = clientFeesDS.ClientFeeEntity.Tier3Percentage.ToString("N2");

                To4.Text = clientFeesDS.ClientFeeEntity.Tier4To.ToString("N2");
                From4.Text = clientFeesDS.ClientFeeEntity.Tier4From.ToString("N2");
                Equal4.Text = clientFeesDS.ClientFeeEntity.Tier4Percentage.ToString("N2");

                To5.Text = clientFeesDS.ClientFeeEntity.Tier5To.ToString("N2");
                From5.Text = clientFeesDS.ClientFeeEntity.Tier5From.ToString("N2");
                Equal5.Text = clientFeesDS.ClientFeeEntity.Tier5Percentage.ToString("N2");

                this.pnlOngoingFeeTiered.Visible = true;
                this.btnOngoingFeeTered.Checked = true;
                this.btnSelected.Value = this.btnOngoingFeeTered.ID;

            }
        }

        private void SetData(ClientFeesDS clientFeesDS)
        {
            clientFeesDS.ClientFeeEntity.UpFrontFee = Convert.ToDecimal(GetDouble(UpFrontFee));
            clientFeesDS.ClientFeeEntity.UpFrontFeePaid = chkFeePaid.Checked;
            if (this.btnSelected.Value == this.btnOngoingFeePercent.ID)
            {
                clientFeesDS.ClientFeeEntity.OngoingFeePercentage = GetDouble(OngoingFeePercent);
                clientFeesDS.ClientFeeEntity.OngoingFeePercent = true;
            }

            else if (this.btnSelected.Value == this.btnOngoingFeeValue.ID)
            {
                clientFeesDS.ClientFeeEntity.OngoingFeeDollar = GetDouble(OngoingFeeVal);
                clientFeesDS.ClientFeeEntity.OnGoingFeeValue = true;
            }

            else if (this.btnSelected.Value == this.btnOngoingFeeTered.ID)
            {
                clientFeesDS.ClientFeeEntity.OngoingTierFee = true;
                clientFeesDS.ClientFeeEntity.Tier1To = GetDecimal(To1);
                clientFeesDS.ClientFeeEntity.Tier1From = GetDecimal(From1);
                clientFeesDS.ClientFeeEntity.Tier1Percentage = GetDecimal(Equal1);

                clientFeesDS.ClientFeeEntity.Tier2To = GetDecimal(To2);
                clientFeesDS.ClientFeeEntity.Tier2From = GetDecimal(From2);
                clientFeesDS.ClientFeeEntity.Tier2Percentage = GetDecimal(Equal2);

                clientFeesDS.ClientFeeEntity.Tier3To = GetDecimal(To3);
                clientFeesDS.ClientFeeEntity.Tier3From = GetDecimal(From3);
                clientFeesDS.ClientFeeEntity.Tier3Percentage = GetDecimal(Equal3);

                clientFeesDS.ClientFeeEntity.Tier4To = GetDecimal(To4);
                clientFeesDS.ClientFeeEntity.Tier4From = GetDecimal(From4);
                clientFeesDS.ClientFeeEntity.Tier4Percentage = GetDecimal(Equal4);

                clientFeesDS.ClientFeeEntity.Tier5To = GetDecimal(To5);
                clientFeesDS.ClientFeeEntity.Tier5From = GetDecimal(From5);
                clientFeesDS.ClientFeeEntity.Tier5Percentage = GetDecimal(Equal5);
            }
        }

        protected decimal GetDecimal(RadNumericTextBox tb)
        {
            if (string.IsNullOrWhiteSpace(tb.Text))
                return 0;
            return Decimal.Parse(tb.Text);
        }
        protected double GetDouble(RadNumericTextBox tb)
        {
            if (string.IsNullOrWhiteSpace(tb.Text))
                return 0;
            return Double.Parse(tb.Text);
        }

        private void SetFeePanels()
        {
            if (this.btnOngoingFeePercent.Checked)
            {
                pnlOngoingFeeVal.Visible = false;
                pnlOngoingFeePercent.Visible = true;
                pnlOngoingFeeTiered.Visible = false;

                ClearOnoigngValueControls();
                //ClearOnoingPercentControls();
                ClearTieredControls();
                this.btnSelected.Value = this.btnOngoingFeePercent.ID;
                this.btnOngoingFeePercent.Checked = true;
            }
            else if (this.btnOngoingFeeValue.Checked)
            {
                pnlOngoingFeeVal.Visible = true;
                pnlOngoingFeePercent.Visible = false;
                pnlOngoingFeeTiered.Visible = false;

                //ClearOnoigngValueControls();
                ClearOnoingPercentControls();
                ClearTieredControls();
                this.btnSelected.Value = this.btnOngoingFeeValue.ID;
                this.btnOngoingFeeValue.Checked = true;
            }

            else if (this.btnOngoingFeeTered.Checked)
            {
                pnlOngoingFeeVal.Visible = false;
                pnlOngoingFeePercent.Visible = false;
                pnlOngoingFeeTiered.Visible = true;

                ClearOnoigngValueControls();
                ClearOnoingPercentControls();
                //ClearTieredControls();
                this.btnSelected.Value = this.btnOngoingFeeTered.ID;
                this.btnOngoingFeeTered.Checked = true;
            }
            else
            {
                pnlOngoingFeeVal.Visible = false;
                pnlOngoingFeePercent.Visible = false;
                pnlOngoingFeeTiered.Visible = false;

                ClearOnoigngValueControls();
                ClearOnoingPercentControls();
                ClearTieredControls();
            }
        }

        private void ClearOnoingPercentControls()
        {
            OngoingFeePercent.Text = "0";
        }

        private void ClearOnoigngValueControls()
        {
            OngoingFeeVal.Text = "0";
        }

        private void ClearTieredControls()
        {

            To1.Text = "0";
            From1.Text = "0";
            Equal1.Text = "0";

            To2.Text = "0";
            From2.Text = "0";
            Equal2.Text = "0";

            To3.Text = "0";
            From3.Text = "0";
            Equal3.Text = "0";

            To4.Text = "0";
            From4.Text = "0";
            Equal4.Text = "0";

            To5.Text = "0";
            From5.Text = "0";
            Equal5.Text = "0";
        }


       
    }
}
