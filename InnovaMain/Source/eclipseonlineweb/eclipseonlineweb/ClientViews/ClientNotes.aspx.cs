﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Calculation;

namespace eclipseonlineweb
{
    public partial class ClientNotes : UMABasePage
    {
        protected override void Intialise()
        {
            base.Intialise();
            NotesControl.FillPresentationData += LoadPage;
        }
        protected override void GetBMCData()
        {
            cid = Request.QueryString["ins"];
            IOrganizationUnit clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            NoteListDS noteListDS = new NoteListDS();
            clientData.GetData(noteListDS);
            PresentationData = noteListDS;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");

            if (objUser.UserType != UserType.Innova && objUser.UserType != UserType.Advisor && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        /// <summary>
        /// The event arguments send with the NoteCommandHandler
        /// </summary>
        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
        }

        public override void LoadPage()
        {
            GetBMCData();
            NotesControl.CID = cid;
            NotesControl.User = UMABroker.UserContext;
            NotesControl.PresentationData = PresentationData;

        }


    }
}
