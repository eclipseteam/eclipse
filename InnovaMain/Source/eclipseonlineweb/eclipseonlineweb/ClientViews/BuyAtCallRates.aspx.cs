﻿using System;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb.ClientViews
{
    public partial class BuyAtCallRates : UMABasePage
    {
        protected override void Intialise()
        {
            BuyAtCallRatesControl.SaveData += (orderCMCID, ds) =>
            {
                if (orderCMCID == Guid.Empty.ToString())
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(orderCMCID, ds);
                }
            };
        }

        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth" || CheckAdviserForOrderPad(objUser))
                return true;
            return objUser.UserType == UserType.Innova || objUser.Name == "Administrator";
        }

        public override void LoadPage()
        {
            if (!IsPostBack)
            {
                cid = Request.QueryString["ins"];
                var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
                //Setting control required properties
                BuyAtCallRatesControl.IsAdmin = objUser.Administrator;
                BuyAtCallRatesControl.ClientCID = cid;
            }
        }
    }
}