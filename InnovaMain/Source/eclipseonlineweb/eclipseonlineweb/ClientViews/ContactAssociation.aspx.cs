﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using Oritax.TaxSimp.Common;
using System.Collections.Generic;
using Oritax.TaxSimp.CM.Organization;
using OTD = Oritax.TaxSimp.DataSets;

namespace eclipseonlineweb
{
    public partial class ContactAssociation : UMABasePage
    {
        private string _cid = string.Empty;
        private ICMBroker Broker
        {
            get
            {
                var umaBasePage = Page as UMABasePage;
                return umaBasePage != null ? umaBasePage.UMABroker : null;
            }
        }
        private DataView BusinessTitleDataView { get; set; }


        protected override void Intialise()
        {
            base.Intialise();
            ContactControl.CanceledPopup += () => HideShowContactAssociationGrid(false);

            ContactControl.PageRefresh += PresentationGrid.Rebind;
            IndividualControl.Canceled += () => HideShowIndividualGrid(false);
            // ReSharper disable InconsistentNaming
            IndividualControl.SaveData += (Cid, ds) =>
            // ReSharper restore InconsistentNaming
            {
                if (Cid == Guid.Empty.ToString())
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(Cid, ds);
                }
                HideShowIndividualGrid(false);
            };

            IndividualControl.ValidationFailed += () => HideShowIndividualGrid(true);

            // ReSharper disable InconsistentNaming
            IndividualControl.Saved += (CID, CLID, CSID) =>
                                           // ReSharper restore InconsistentNaming
                                           {
                                               HideShowIndividualGrid(false);
                                               SaveContacts(CID, CSID, CLID, "Others");
                                           };
        }

        // ReSharper disable InconsistentNaming
        private void SaveContacts(Guid CID, Guid CSID, Guid CLID, string businessTitles)
        // ReSharper restore InconsistentNaming
        {
            string clientID = Request.QueryString["ins"];
            var clientData = Broker.GetBMCInstance(new Guid(clientID)) as OrganizationUnitCM;
            UMABroker.SaveOverride = true;

            if (clientData != null)
            {
                var contact = clientData.SignatoriesApplicants.Where(associate => associate.Clid == CLID && associate.Csid == CSID).FirstOrDefault();

                if (contact == null)
                {
                    var identityCMDetail = new Oritax.TaxSimp.Common.IdentityCMDetail
                                               {
                                                   Clid = CLID,
                                                   Csid = CSID,
                                                   Cid = CID,
                                                   BusinessTitle = { Title = businessTitles }
                                               };
                    clientData.SignatoriesApplicants.Add(identityCMDetail);
                }
                else
                {
                    contact.BusinessTitle.Title = businessTitles;
                }
            }

            if (clientData != null) clientData.CalculateToken(true);
            UMABroker.SetComplete();
            UMABroker.SetStart();
            SaveShare(CID, CSID, CLID, "", false);
            PresentationGrid.Rebind();
        }


        private void SaveShare(Guid CID, Guid CLID, Guid CSID, string share, bool isRemove)
        {
            var individualData = Broker.GetBMCInstance(CID) as OrganizationUnitCM;
            _cid = Request.Params["ins"];
            if (individualData == null)
            {
                individualData = Broker.GetCMImplementation(CLID, CSID) as OrganizationUnitCM;
            }

            UMABroker.SaveOverride = true;
            if (individualData != null)
            {
                var ind = individualData.ClientEntity as IndividualEntity;

                if (ind.ClientShares == null)
                    ind.ClientShares = new List<ClientShare>();

                ClientShare individual = ind.ClientShares.FirstOrDefault(associate => associate.ClientCid == Guid.Parse(_cid));

                if (isRemove)
                {
                    if (individual != null)
                    {
                        //remove code here.
                        ind.ClientShares.Remove(individual);
                    }

                }
                else{ if (individual == null)
                {
                    var clientShare = new Oritax.TaxSimp.Common.ClientShare
                        {
                            ClientID = ClientHeaderInfo1.ClientId,
                            ClientCid = Guid.Parse(_cid),
                            ClientClid = new Guid(hfCLID.Value),
                            ClientCsid = new Guid(hfCSID.Value),
                           };

                    if (!string.IsNullOrEmpty(share))
                        clientShare.Share = Convert.ToDecimal(share);
                    else
                    {
                        clientShare.Share = null;
                    }
                    ind.ClientShares.Add(clientShare);
                }
                else
                {
                    if(!string.IsNullOrEmpty(share))
                    individual.Share = Convert.ToDecimal(share);
                    else
                    {
                        individual.Share = null;
                    }
                }}
            }

            if (individualData != null) individualData.CalculateToken(true);
            UMABroker.SetComplete();
            UMABroker.SetStart();

            PresentationGrid.Rebind();
        }

        public override void LoadPage()
        {
            _cid = Request.QueryString["ins"];

            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");

            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                btnSave.Visible = false;
                btnNewContact.Visible = false;
                PresentationGrid.Columns[PresentationGrid.Columns.Count - 1].Visible = false;
                PresentationGrid.Columns[PresentationGrid.Columns.Count - 2].Visible = false;
                PresentationGrid.Columns[PresentationGrid.Columns.Count - 4].Visible = false;
                PresentationGrid.Columns[PresentationGrid.Columns.Count - 5].Visible = false;
            }

            UMABroker.ReleaseBrokerManagedComponent(objUser);

            if (!IsPostBack)
                GetBusinessTitles();
           
            if (!string.IsNullOrEmpty(hfCLID.Value))
            {
                ContactControl.CLID = new Guid(hfCLID.Value);
                ContactControl.CSID = new Guid(hfCSID.Value);
            }
        }

        private void FillContactGrid()
        {
            var dt = new DataTable();
            dt.Columns.Add("CID");
            dt.Columns.Add("CLID");
            dt.Columns.Add("CSID");
            dt.Columns.Add("FirstName");
            dt.Columns.Add("MiddleName");
            dt.Columns.Add("LastName");
            dt.Columns.Add("EmailAddress");
            dt.Columns.Add("BusinessTitle");
            dt.Columns.Add("Share");
            var clientCid = new Guid(_cid);
            var clientData = UMABroker.GetBMCInstance(clientCid) as OrganizationUnitCM;
            if (clientData != null)
            {
                hfCLID.Value = clientData.Clid.ToString();
                hfCSID.Value = clientData.CSID.ToString();
                ContactControl.CLID = new Guid(hfCLID.Value);
                ContactControl.CSID = new Guid(hfCSID.Value);
                foreach (IdentityCMDetail identityCM in clientData.SignatoriesApplicants)
                {
                    var indvCM = UMABroker.GetCMImplementation(identityCM.Clid, identityCM.Csid) as IndividualCM;
                    if (indvCM != null)
                    {
                        var ind = indvCM.ClientEntity as IndividualEntity;

                        DataRow row = dt.NewRow();
                        row["CID"] = indvCM.CID;
                        row["CLID"] = identityCM.Clid;
                        row["CSID"] = identityCM.Csid;
                        if (identityCM.BusinessTitle != null)
                            row["BusinessTitle"] = identityCM.BusinessTitle.Title;
                        row["FirstName"] = ind.Name;
                        row["MiddleName"] = ind.MiddleName;
                        row["LastName"] = ind.Surname;
                        row["EmailAddress"] = ind.EmailAddress;

                        if (ind.ClientShares != null)
                        {
                            var individualShares = ind.ClientShares.FirstOrDefault(clientshare => clientshare.ClientCid == clientCid);
                            if (individualShares != null)
                                row["Share"] = individualShares.Share;
                        }


                        dt.Rows.Add(row);

                    }
                }
            }
            var dv = new DataView(dt) { Sort = dt.Columns["FirstName"] + " ASC" };
            PresentationGrid.DataSource = dv;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        private void HideShowContactAssociationGrid(bool show)
        {
            OVER.Visible = ContactModal.Visible = show;
        }
     
        protected void LnkbtnAssociateContactsClick(object sender, EventArgs e)
        {
            HideShowContactAssociationGrid(true);
        }

        protected void LnkbtnAddBankAccClick(object sender, ImageClickEventArgs e)
        {
            Guid gCid = Guid.Empty;
            IndividualControl.SetEntity(gCid);
            HideShowIndividualGrid(true);
        }

        private void HideShowIndividualGrid(bool show)
        {
            OVER.Visible = IndividualModal.Visible = show;
        }

        protected void Button1_OnClick(object sender, ImageClickEventArgs e)
        {
            ViewPanel.Visible = false;
        }

        protected void DdlBusinessTitle_OnSelectedIndexChanged(object sender, EventArgs e)
        {

            var ddl = (sender as RadComboBox);
            if (ddl != null && ddl.SelectedValue != null)
                SaveContacts(Guid.Parse(ddl.Attributes["CID"]), Guid.Parse(ddl.Attributes["CSID"]), Guid.Parse(ddl.Attributes["CLID"]), ddl.SelectedValue);
        }

        protected void txtShare_OnTextChanged(object sender, EventArgs e)
        {
            RadInputControl txt = sender as RadInputControl;
            if (txt != null)
            {
                SaveShare(Guid.Parse(txt.Attributes["CID"]), Guid.Parse(txt.Attributes["CLID"]), Guid.Parse(txt.Attributes["CSID"]), txt.Text, false);
            }
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            FillContactGrid();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            switch (e.CommandName.ToLower())
            {
                case "detail":
                    var sigCid = new Guid(dataItem["CID"].Text);
                    IndividualControlView.SetEntity(sigCid);
                    ViewPanel.Visible = true;
                    break;
                case "edit":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var sigCid1 = new Guid(dataItem["CID"].Text);
                    IndividualControl.SetEntity(sigCid1);
                    HideShowIndividualGrid(true);
                    break;
                case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    UMABroker.SaveOverride = true;
                    var linkEntiyCID = new Guid(dataItem["CID"].Text);
                    var linkEntiyCLID = new Guid(dataItem["CLID"].Text);
                    var linkEntiyCSID = new Guid(dataItem["CSID"].Text);
                    var clientData = Broker.GetBMCInstance(new Guid(_cid)) as OrganizationUnitCM;
                    if (clientData != null )
                    {
                        clientData.SignatoriesApplicants.RemoveAll(associate => associate.Clid == linkEntiyCLID && associate.Csid == linkEntiyCSID);
                       
                    }
                    if (clientData != null) clientData.CalculateToken(true);
                    UMABroker.SetComplete();
                    UMABroker.SetStart();

                    SaveShare(linkEntiyCID, linkEntiyCLID,linkEntiyCSID, "0", true);

                    PresentationGrid.Rebind();
                    break;
            }
        }

        protected void PresentationGrid_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            
            var datarow = ((DataRowView)(dataItem.DataItem)).Row;
            var txt = ((RadTextBox)(dataItem["Share"].Controls[1]));
            txt.Text = datarow["Share"].ToString();
            txt.Attributes.Add("CID", dataItem["CID"].Text);
            txt.Attributes.Add("CLID", dataItem["CLID"].Text);
            txt.Attributes.Add("CSID", dataItem["CSID"].Text);
        }

        private void GetBusinessTitles()
        {
            IOrganization organization =
                Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            OTD.BusinessTitleDS ds = new OTD.BusinessTitleDS();
            organization.GetData(ds);
            ds.BTitleTable.DefaultView.Sort = "Title ASC";
            this.BusinessTitleDataView = ds.BTitleTable.DefaultView;

            Broker.ReleaseBrokerManagedComponent(organization);
        }

        private void SetBusinessTitles(RadComboBox ddl)
        {
            ddl.DataTextField = "Title";
            ddl.DataValueField = "Value";

            ddl.DataSource = this.BusinessTitleDataView;
            ddl.DataBind();

            ddl.Items.Insert(0, new RadComboBoxItem(string.Empty, string.Empty));
        }

    }
}
