﻿<%@ Page Title="e-Clipse Online Portal - Fee Details" Language="C#" MasterPageFile="ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="FeeDetailsReadOnly.aspx.cs" Inherits="eclipseonlineweb.FeeDetailsReadOnly" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="4%">
                       <asp:ImageButton ID="btnBack" ToolTip="Back" AlternateText="back" runat="server"
                                    ImageUrl="~/images/window_previous.png" OnClick="btnBack_Click" />
                    </td>
                    <td width="100%" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <asp:Label Font-Bold="true" runat="server" ID="lblDetails" Text="Fee Details"></asp:Label>
                        <asp:HiddenField ID="hfSecID" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <h3>
        Ongoing Fee ($)</h3>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <telerik:RadGrid OnNeedDataSource="GridOngoingFeeValue_NeedDataSource" ID="GridOngoingFeeValue"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="GridOngoingFeeValue_DetailTableDataBind"
                OnItemCommand="GridOngoingFeeValue_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="GridOngoingFeeValue_ItemUpdated" OnItemDeleted="GridOngoingFeeValue_ItemDeleted"
                OnItemInserted="GridOngoingFeeValue_ItemInserted" OnInsertCommand="GridOngoingFeeValue_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="GridOngoingFeeValue_ItemCreated"
                OnItemDataBound="GridOngoingFeeValue_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="FeesOngoingValue" TableLayout="Fixed" EditMode="EditForms">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <Columns>
                        <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ID" UniqueName="ID" />
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Description"
                            HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderText="Description" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Description" UniqueName="Description">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Year" DefaultInsertValue="2000"
                            HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderText="Year" ReadOnly="true"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="Year" UniqueName="Year">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                            SortExpression="FeeValue" HeaderText="Fee Value" AutoPostBackOnFilter="true"
                            DataFormatString="{0:c}" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="FeeValue" UniqueName="FeeValue">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <br />
            <h3>
                Ongoing Fee (%)</h3>
            <telerik:RadGrid OnNeedDataSource="GridOngoingFeePercent_NeedDataSource" ID="GridOngoingFeePercent"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="GridOngoingFeePercent_DetailTableDataBind"
                OnItemCommand="GridOngoingFeePercent_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="GridOngoingFeePercent_ItemUpdated" OnItemDeleted="GridOngoingFeePercent_ItemDeleted"
                OnItemInserted="GridOngoingFeePercent_ItemInserted" OnInsertCommand="GridOngoingFeePercent_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="GridOngoingFeePercent_ItemCreated"
                OnItemDataBound="GridOngoingFeePercent_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="FeesOngoingPercent" TableLayout="Fixed" EditMode="EditForms">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <Columns>
                        <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ID" UniqueName="ID" />
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Description"
                            HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderText="Description" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Description" UniqueName="Description">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Year" DefaultInsertValue="2000"
                            HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderText="Year" ReadOnly="true"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="Year" UniqueName="Year">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                            SortExpression="PercentageFee" HeaderText="Fee %" AutoPostBackOnFilter="true"
                            DataFormatString="{0:n4}" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="PercentageFee" UniqueName="PercentageFee">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <br />
            <h3>
                Tiered Fee</h3>
            <telerik:RadGrid OnNeedDataSource="TieredFee_NeedDataSource" ID="TieredFee" runat="server"
                ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20" AllowSorting="True"
                AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="TieredFee_DetailTableDataBind"
                OnItemCommand="TieredFee_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="TieredFee_ItemUpdated" OnItemDeleted="TieredFee_ItemDeleted" OnItemInserted="TieredFee_ItemInserted"
                OnInsertCommand="TieredFee_InsertCommand" EnableViewState="true" ShowFooter="true"
                OnItemCreated="TieredFee_ItemCreated" OnItemDataBound="TieredFee_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="TieredFees" TableLayout="Fixed" EditMode="EditForms">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <Columns>
                        <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ID" UniqueName="ID" />
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Description"
                            HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderText="Description" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Description" UniqueName="Description">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Year" DefaultInsertValue="2000"
                            HeaderStyle-Width="10%" ItemStyle-Width="20%" HeaderText="Year" ReadOnly="true"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="Year" UniqueName="Year">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                            SortExpression="FromValue" HeaderText="From ($)" AutoPostBackOnFilter="true"
                            DataFormatString="{0:c}" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="FromValue" UniqueName="FromValue">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                            SortExpression="ToValue" HeaderText="To ($)" AutoPostBackOnFilter="true" DataFormatString="{0:c}"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="ToValue" UniqueName="ToValue">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                            SortExpression="PercentageFee" HeaderText="Fee %" AutoPostBackOnFilter="true"
                            DataFormatString="{0:n4}" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="PercentageFee" UniqueName="PercentageFee">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
