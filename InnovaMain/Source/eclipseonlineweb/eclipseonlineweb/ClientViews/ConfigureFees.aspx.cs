﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.CM.OrganizationUnit;

namespace eclipseonlineweb
{
    public partial class ConfigureFees : UMABasePage
    {
        protected override void GetBMCData()
        {
            this.cid = Request.QueryString["ins"].ToString();
            IOrganizationUnit clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            OrgUnitConfiguredFeesDS orgUnitConfiguredFeesDS = new OrgUnitConfiguredFeesDS();
            clientData.GetData(orgUnitConfiguredFeesDS);
            this.PresentationData = orgUnitConfiguredFeesDS;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }


        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
        }

        public override void LoadPage()
        {
            base.LoadPage();

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                PresentationGrid.Columns[PresentationGrid.Columns.Count - 1].Visible = false;
                PresentationGrid.Columns[PresentationGrid.Columns.Count - 2].Visible = false;
                PresentationGrid.MasterTableView.CommandItemSettings.ShowAddNewRecordButton = false;
            }
            string PrePageName = Request.QueryString["PrePage"];
            string PageName = Request.QueryString["PageName"];
            if (PageName != null && PageName != string.Empty)
                btnBack.Visible = true;
            else if (PrePageName != null && PrePageName != string.Empty)
                btnBack.Visible = true;
            else
                btnBack.Visible = false;

        }
        protected void btnBack_Click(object sender, EventArgs args)
        {
            string PageName = Request.QueryString["PageName"];
            string PrePageName = Request.QueryString["PrePage"];
            if (PageName == "ClientFeeAssociation" || PrePageName == "ClientFeeAssociation")
                Response.Redirect("~/SysAdministration/ClientsFeeAssociation.aspx");
        }
        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e) { }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;

                UMABroker.SaveOverride = true;

                Guid rowID = (Guid)gDataItem.OwnerTableView.DataKeyValues[gDataItem.ItemIndex]["ID"];

                IOrganizationUnit clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
                OrgUnitConfiguredFeesDS orgUnitConfiguredFeesDS = new OrgUnitConfiguredFeesDS();
                orgUnitConfiguredFeesDS.FeeEntityToConfigure = rowID;
                orgUnitConfiguredFeesDS.DataSetOperationType = DataSetOperationType.DeletSingle;
                clientData.SetData(orgUnitConfiguredFeesDS);
                this.UMABroker.ReleaseBrokerManagedComponent(clientData);
                UMABroker.SetComplete();

                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.PresentationGrid.Rebind();
            }
            if (e.CommandName == "FeeTemplate")
            {
                string[] IdIns = e.CommandArgument.ToString().Split(',');
                string PageName = Request.QueryString["PageName"];
                if (PageName == string.Empty)
                    Response.Redirect("~/ClientViews/FeeDetailsReadOnly.aspx?ID=" + IdIns[0] + "&INS=" + IdIns[1] + "&PageName=" + "ConfigureFee");
                else
                    Response.Redirect("~/ClientViews/FeeDetailsReadOnly.aspx?ID=" + IdIns[0] + "&INS=" + IdIns[1] + "&PageName=" + "ConfigureFee" + "&PrePage=" + PageName);
            }
        }

        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) { }
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e) { }
        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("Fees".Equals(e.Item.OwnerTableView.Name))
            {
                GridEditFormInsertItem EditForm = (GridEditFormInsertItem)e.Item;

                UMABroker.SaveOverride = true;
                RadComboBox combo = (RadComboBox)EditForm["ServiceTypeEditor"].Controls[0];

                IOrganizationUnit clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
                OrgUnitConfiguredFeesDS orgUnitConfiguredFeesDS = new OrgUnitConfiguredFeesDS();
                orgUnitConfiguredFeesDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                orgUnitConfiguredFeesDS.FeeEntityToConfigure = new Guid(combo.SelectedItem.Value);
                clientData.SetData(orgUnitConfiguredFeesDS);
                this.UMABroker.ReleaseBrokerManagedComponent(clientData);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.PresentationGrid.Rebind();
            }
        }

        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e) { }

        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;
                GridDropDownListColumnEditor editor = null;

                if ("Fees".Equals(e.Item.OwnerTableView.Name))
                {
                    editor = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("ServiceTypeEditor"));
                    editor.DataSource = this.PresentationData.Tables[OrgUnitConfiguredFeesDS.FEESLIST];
                    editor.DataValueField = "ID";
                    editor.DataTextField = "DESCRIPTION";
                    editor.DataBind();
                }
            }
            else
            {

            }
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetBMCData();

            DataView View = new DataView(this.PresentationData.Tables[OrgUnitConfiguredFeesDS.CONFIGUREDFEES]);
            View.Sort = OrgUnitConfiguredFeesDS.DESCRIPTION + " ASC";
            this.PresentationGrid.DataSource = View.ToTable();
        }
    }
}
