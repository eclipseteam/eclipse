﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;

namespace eclipseonlineweb
{
    public partial class MISAccounts : UMABasePage
    {
        protected override void Intialise()
        {
            base.Intialise();

            //misFund
            misFund.Saved += (CLID, CSID, FUNDID,reinvest) =>
                  {
                      ModalPopupExtender1.Hide();
                      UpdateClient(CLID, CSID, FUNDID,reinvest.Value, DatasetCommandTypes.Add);

                  };

            misFund.ValidationFailed += () => ModalPopupExtender1.Show();

            misFund.Canceled += () => ModalPopupExtender1.Hide();


        }

        private DataSet UpdateClient(Guid CLID, Guid CSID, Guid FUNDID, ReinvestmentOption reinvest, DatasetCommandTypes command)
        {
            var dsNew = new ManagedInvestmentAccountDS { CommandType = command };
            DataRow dr = dsNew.Tables[dsNew.FundAccountsTable.TABLENAME].NewRow();
            dr[dsNew.FundAccountsTable.CLID] = CLID;
            dr[dsNew.FundAccountsTable.CSID] = CSID;
            dr[dsNew.FundAccountsTable.FUNDID] = FUNDID;
            dr[dsNew.FundAccountsTable.REINVESTMENTOPTION] = reinvest;
            dsNew.Tables[dsNew.FundAccountsTable.TABLENAME].Rows.Add(dr);
            SaveData(cid, dsNew);
            PresentationGrid.Rebind();
            return dsNew;
        }

        protected void LnkbtnAddClick(object sender, EventArgs e)
        {
            misFund.SetEntity(Guid.Empty, Guid.Empty);
            ModalPopupExtender1.Show();
        }

        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];
            if (!IsPostBack)
            {
                var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
                if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
                {
                    btn.Visible = false;
                    PresentationGrid.Columns.FindByUniqueNameSafe("DeleteColumn").Visible = false;
                    PresentationGrid.Columns.FindByUniqueNameSafe("EditColumn").Visible = false;
                }
                UMABroker.ReleaseBrokerManagedComponent(objUser);
            }
        }

        private void GetData()
        {
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(cid));
            var misDS = new ManagedInvestmentAccountDS();
            clientData.GetData(misDS);
            if (misDS.Tables.Count > 0)
            {
                var misView = new DataView(misDS.FundAccountsTable) { Sort = misDS.FundAccountsTable.FUNDCODE + " ASC" };
                PresentationGrid.DataSource = misView;
            }
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }


        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var misCid = new Guid(dataItem["CID"].Text);
                    var fundId = new Guid(dataItem["FundID"].Text);
                    var reinvestmentOption = (ReinvestmentOption) Enum.Parse(typeof(ReinvestmentOption), dataItem["ReinvestmentOption"].Text);
                    misFund.SetEntity(misCid, fundId, reinvestmentOption);
                    ModalPopupExtender1.Show();
                    break;
                case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var linkEntiyClid = new Guid(dataItem["CLID"].Text);
                    var linkEntiyCsid = new Guid(dataItem["CSID"].Text);
                    var linkEntityFundId = new Guid(dataItem["FundID"].Text);
                    var linkEntityreinvestmentOption = (ReinvestmentOption)Enum.Parse(typeof(ReinvestmentOption), dataItem["ReinvestmentOption"].Text);
                    DataSet ds = UpdateClient(linkEntiyClid, linkEntiyCsid, linkEntityFundId, linkEntityreinvestmentOption, DatasetCommandTypes.Delete);
                    PresentationGrid.Rebind();
                    ScriptManager.RegisterStartupScript(Page, GetType(), "MyScript", "alert('" + ds.ExtendedProperties["Message"] + "');", true);
                    break;
            }
        }
    }
}
