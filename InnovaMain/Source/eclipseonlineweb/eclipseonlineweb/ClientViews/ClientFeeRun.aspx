﻿<%@ Page Title="e-Clipse Online Portal - Configure Fees" Language="C#" MasterPageFile="ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="ClientFeeRun.aspx.cs" Inherits="eclipseonlineweb.ClientFeeRun" %>

<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function GoToPage(url) {
            window.location = url + '?ins=<%:cid %>';
            return false;
        }
    </script>
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="10%">
                        <c1:C1ComboBox ID="C1ComboBoxMonth" runat="server" Width="100%">
                            <Items>
                                <c1:C1ComboBoxItem Text="JAN" Value="1" />
                                <c1:C1ComboBoxItem Text="FEB" Value="2" />
                                <c1:C1ComboBoxItem Text="MAR" Value="3" />
                                <c1:C1ComboBoxItem Text="APR" Value="4" />
                                <c1:C1ComboBoxItem Text="MAY" Value="5" />
                                <c1:C1ComboBoxItem Text="JUN" Value="6" />
                                <c1:C1ComboBoxItem Text="JUL" Value="7" />
                                <c1:C1ComboBoxItem Text="AUG" Value="8" />
                                <c1:C1ComboBoxItem Text="SEP" Value="9" />
                                <c1:C1ComboBoxItem Text="OCT" Value="10" />
                                <c1:C1ComboBoxItem Text="NOV" Value="11" />
                                <c1:C1ComboBoxItem Text="DEC" Value="12" />
                            </Items>
                        </c1:C1ComboBox>
                    </td>
                    <td width="10%">
                        <c1:C1ComboBox ID="C1ComboBoxYear" runat="server" Width="100%">
                            <Items>
                                <c1:C1ComboBoxItem Text="2000" Value="2000" />
                                <c1:C1ComboBoxItem Text="2001" Value="2001" />
                                <c1:C1ComboBoxItem Text="2002" Value="2002" />
                                <c1:C1ComboBoxItem Text="2003" Value="2003" />
                                <c1:C1ComboBoxItem Text="2004" Value="2004" />
                                <c1:C1ComboBoxItem Text="2005" Value="2005" />
                                <c1:C1ComboBoxItem Text="2006" Value="2006" />
                                <c1:C1ComboBoxItem Text="2008" Value="2008" />
                                <c1:C1ComboBoxItem Text="2009" Value="2009" />
                                <c1:C1ComboBoxItem Text="2010" Value="2010" />
                                <c1:C1ComboBoxItem Text="2011" Value="2011" />
                                <c1:C1ComboBoxItem Text="2012" Value="2012" />
                                <c1:C1ComboBoxItem Text="2013" Value="2013" />
                                <c1:C1ComboBoxItem Text="2014" Value="2014" />
                                <c1:C1ComboBoxItem Text="2015" Value="2015" />
                                <c1:C1ComboBoxItem Text="2016" Value="2016" />
                                <c1:C1ComboBoxItem Text="2017" Value="2017" />
                                <c1:C1ComboBoxItem Text="2018" Value="2018" />
                                <c1:C1ComboBoxItem Text="2019" Value="2019" />
                                <c1:C1ComboBoxItem Text="2020" Value="2020" />
                            </Items>
                        </c1:C1ComboBox>
                    </td>
                    <td width="15%">
                        <telerik:RadButton ID="btnFeeRun" Height="30px" Width="100%" runat="server" Text="RUN FEE"
                            OnClick="btnFeeRun_Click">
                        </telerik:RadButton>
                    </td>
                    <td width="1%">
                    </td>
                    <td width="15%">
                        <telerik:RadButton ID="btnFeeRunWithTransaction" Height="30px" Width="100%" runat="server"
                            Text="RUN FEE WITH TRANSACTION" OnClick="btnFeeRunWithTransaction_Click">
                        </telerik:RadButton>
                    </td>
                    <td style="width: 30%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
                OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
                OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
                OnItemDataBound="PresentationGrid_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="CalculatedFees" TableLayout="Fixed" EditMode="EditForms">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <GroupByExpressions>
                        <telerik:GridGroupByExpression>
                            <SelectFields>
                                <telerik:GridGroupByField FieldName="FEETYPE" HeaderValueSeparator=" : "></telerik:GridGroupByField>
                            </SelectFields>
                            <GroupByFields>
                                <telerik:GridGroupByField FieldName="FEETYPE" SortOrder="Ascending"></telerik:GridGroupByField>
                            </GroupByFields>
                        </telerik:GridGroupByExpression>
                    </GroupByExpressions>
                    <Columns>
                        <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ID" UniqueName="ID" />
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="FEESUBTYPE" ReadOnly="true"
                            HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderText="Fee Sub Type" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="FEESUBTYPE" UniqueName="FEESUBTYPE">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="MONTH" HeaderStyle-Width="20%"
                            ItemStyle-Width="20%" HeaderText="Month" ReadOnly="true" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="MONTH" UniqueName="MONTH">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" ReadOnly="true" DefaultInsertValue="2000"
                            SortExpression="YEAR" HeaderStyle-Width="20%" ItemStyle-Width="15%" HeaderText="Year"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="YEAR" UniqueName="YEAR">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" FooterAggregateFormatString="{0:C}"
                            Aggregate="Sum" ReadOnly="true" DefaultInsertValue="2000" SortExpression="CALCULATEDFEES"
                            HeaderStyle-Width="20%" ItemStyle-Width="15%" HeaderText="Calculated Fee" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" DataFormatString="{0:c}" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="CALCULATEDFEES" UniqueName="CALCULATEDFEES">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
