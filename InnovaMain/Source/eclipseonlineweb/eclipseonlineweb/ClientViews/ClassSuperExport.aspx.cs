﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Telerik.Web.UI;
using Oritax.TaxSimp.CM;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Common;

namespace eclipseonlineweb.ClientViews
{
    public partial class ClassSuperExport : UMABasePage
    {
        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];
            if(!this.Page.IsPostBack)
                LoadControls();
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");

            if (objUser.UserType != UserType.Advisor && objUser.UserType != UserType.Accountant && objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        private void LoadControls()
        {
            LoadData();
        }

        private void LoadData()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");

            if (objUser.UserType == UserType.Client || objUser.UserType == UserType.Accountant)
            {
                this.rbClassSuperEnable.ReadOnly = true;
            }

            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(cid));
            OtherServicesDS ds = new OtherServicesDS();
            clientData.GetData(ds);

            //SMSF Admin
            this.rbClassSuperEnable.Checked = ds.IsClassSuper;

            if (ds.IsClassSuper)
            {
                imgOffline.Visible = false;
                imgOnline.Visible = true;
            }
            else
            {
                imgOnline.Visible = false;
                imgOffline.Visible = true;
            }
        }

        private void SaveData()
        {
            OtherServicesDS ds = new OtherServicesDS();
            //SMSF Admin
            ds.IsClassSuper = rbClassSuperEnable.Checked;
            SaveData(cid, ds);
            LoadData();
        }

        protected void rbClassSuperEnable_Click(Object sender, EventArgs e)
        {
            SaveData(); 
        }     
    }
}