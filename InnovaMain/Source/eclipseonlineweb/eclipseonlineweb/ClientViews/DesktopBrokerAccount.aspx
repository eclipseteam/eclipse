﻿<%@ Page Title="" Language="C#" MasterPageFile="ClientViewMaster.master" AutoEventWireup="true"
    CodeBehind="DesktopBrokerAccount.aspx.cs" Inherits="eclipseonlineweb.ClientViews.DesktopBrokerAccount" %>

<%@ Register Src="../Controls/DesktopBrokerAccountControl.ascx" TagName="DesktopBrokerAccountControl"
    TagPrefix="uc1" %>
<%@ Register Src="../Controls/DesktopBrokerExistingAccountControl.ascx" TagName="DesktopBrokerExistingAccountControl"
    TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" runat="server">
    <style>
        .holder
        {
            width: 100%;
            display: block;
            z-index: 6;
        }
        .content
        {
            background: #fff;
            z-index: 7; /*  padding: 28px 26px 33px 25px;*/
        }
        .popup
        {
            border-radius: 7px;
            background: #6b6a63;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 1000px;
            top: 20%;
            left: 50%;
            margin-left: -400px;
            margin-top: -40px;
            z-index: 6;
        }
        
        .popup1
        {
            border-radius: 7px;
            background: #6b6a63;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 840px;
            top: 20%;
            left: 50%;
            margin-left: -400px;
            margin-top: -40px;
            z-index: 6;
        }
        .overlay
        {
            width: 100%;
            opacity: 0.65;
            height: 100%;
            left: 0; /*IE*/
            top: 0;
            text-align: center;
            z-index: 5;
            position: fixed;
            background-color: #444444;
        }
        
        .wijmo-wijgrid .wijmo-wijgrid-groupheaderrow td
        {
            background-color: #DAE6F4;
            color: #000000;
            font-size: smaller;
            font-weight: bold;
            vertical-align: middle;
        }
        .wijmo-wijgrid .wijmo-wijgrid-footerrow td
        {
            font-weight: bolder;
            text-align: right;
            font-size: smaller;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function cancel() {
            window.close();
        }
    </script>
    <%--<asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>--%>
    <fieldset>
        <table width="100%">
            <tr>
                <td style="width: 5%">
                    <asp:ImageButton ID="btn" AlternateText="Add ASX Broker Account" ToolTip="Add ASX Broker Account"
                        runat="server" OnClick="LnkbtnAddASXClick" ImageUrl="~/images/add-icon.png" />
                </td>
                <td style="width: 5%">
                    <asp:ImageButton ID="ImageButton1" AlternateText="Add Existing ASX Broker Account"
                        ToolTip="Add Existing ASX Broker Account" runat="server" OnClick="LnkbtnAddExistngASXClick"
                        ImageUrl="~/images/add_existing.png" />
                </td>
                <td style="width: 5%">
                </td>
                <td style="width: 5%">
                </td>
                <td>
                </td>
                <td>
                </td>
                <td style="width: 85%;" class="breadcrumbgap">
                    <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                    <br />
                    <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
        PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
        GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
        AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource"
        OnItemCommand="PresentationGrid_OnItemCommand">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
            Name="Banks" TableLayout="Fixed">
            <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
            <HeaderStyle Font-Bold="True"></HeaderStyle>
            <Columns>
                <telerik:GridBoundColumn SortExpression="CID" ReadOnly="true" HeaderText="CID" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="CID" UniqueName="CID" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="CLID" ReadOnly="true" HeaderText="CLID"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="CLID" UniqueName="CLID" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="CSID" ReadOnly="true" HeaderText="CSID"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="CSID" UniqueName="CSID" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="460px" HeaderStyle-Width="500px" SortExpression="AccountName"
                    ReadOnly="true" HeaderText="Account Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountName" UniqueName="AccountName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderStyle-Width="120px" ReadOnly="true" SortExpression="AccountNo"
                    HeaderText="Account Number" AutoPostBackOnFilter="true" ShowFilterIcon="true"
                    CurrentFilterFunction="Contains" HeaderButtonType="TextButton" DataField="AccountNo"
                    UniqueName="AccountNo">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="160px" HeaderStyle-Width="200px" SortExpression="AccountDesignation"
                    HeaderText="Account Designation" ReadOnly="true" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="AccountDesignation" UniqueName="AccountDesignation">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="120px" SortExpression="HINNumber"
                    ReadOnly="true" HeaderText="HIN Number" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="HINNumber" UniqueName="HINNumber">
                </telerik:GridBoundColumn>
                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                    UniqueName="EditColumn">
                    <HeaderStyle Width="3%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn ConfirmText="Are you sure you want to remove the association?"
                    ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                    <HeaderStyle Width="3%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                </telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
    <asp:Button ID="btnCancel" runat="server" Style="display: none" />
    <asp:Button ID="btnOkay" runat="server" Style="display: none" />
    <div id="ASXModal" runat="server" visible="false" class="holder">
        <div class="popup">
            <div class="content">
                <uc1:DesktopBrokerAccountControl ID="ASXControl" runat="server" />
            </div>
        </div>
    </div>
    <div class="overlay" id="OVER" visible="False" runat="server">
    </div>
    <div id="ASXModalExsiting" runat="server" visible="false" class="holder">
        <div class="popup1">
            <div class="content">
                <uc2:DesktopBrokerExistingAccountControl ID="ASXControlExisting" runat="server" />
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hfSelectedRowIndex" runat="server" />
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
