﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;

namespace eclipseonlineweb
{
    public partial class Attachments : UMABasePage
    {
        protected override void GetBMCData()
        {
            this.cid = Request.QueryString["ins"].ToString();
            IOrganizationUnit clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            AttachmentListDS attachmentListDS = new AttachmentListDS();
            clientData.GetData(attachmentListDS);
            this.PresentationData = attachmentListDS;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        public enum AttachmentStateEnum
        {
            List = 0, Import = 1
        }

        public enum AttachmentCommandEventTypeEnum { None = 0, Delete = 1, Copy = 2, Modify = 3, Add = 4, Cancel = 5, Sort = 6 };

        /// <summary>
        /// The event arguments send with the NoteCommandHandler
        /// </summary>
        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
        }

        public override void LoadPage()
        {
            GetBMCData();
            AttachmentControl.CID = cid;
            AttachmentControl.PresentationData = PresentationData;
            AttachmentControl.User = UMABroker.UserContext; 
        }
    }
}
