﻿using System;
using System.Text;
using System.Web.UI;
using Oritax.TaxSimp.Common;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

namespace eclipseonlineweb
{
    public partial class InstructionsHistory : UMABasePage
    {
        protected bool IsFiltered { get; set; }
        protected string Filter { get; set; }

        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];

            base.LoadPage();

            //DBUser objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            //if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
            //    ((SetupMaster)Master).IsAdmin = "1";
            if (!IsPostBack)
                LoadControls();
        }

        private void LoadControls()
        {
            //Status List binding
            lstStatus.DataSource = Enum.GetNames(typeof(InstructionStatus));
            lstStatus.DataBind();


            foreach (RadListBoxItem item in lstStatus.Items)
            {
                item.Checked = true;
            }

            ShowCheckedItems(lstStatus, lblFilter, false);
        }

        private void ShowCheckedItems(RadListBox listBox, Label label, bool isFilter)
        {
            var sb = new StringBuilder();
            IList<RadListBoxItem> collection = listBox.CheckedItems;
            foreach (RadListBoxItem item in collection)
            {
                sb.Append("'" + item.Value + "', ");
            }

            string filterStatus = sb.Remove(sb.ToString().Trim().Length - 1, 1).ToString();
            label.Text = "Showing orders with status: " + filterStatus.Replace("'", "");

            if (isFilter)
            {
                IsFiltered = true;
                Filter = "Status in (" + filterStatus + ")";
                //ToDo:after implementation of instruction
                //PresentationGrid.Rebind();
            }
        }
        protected void PresentationGridDetailTableDataBind(object source, GridDetailTableDataBindEventArgs e) { }
        protected void PresentationGridItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "details":
                    break;
            }
        }

        //public override bool AccessibleByUser()
        //{
        //    DBUser objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
        //    if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "cjohnson" || objUser.Name == "shepworth")
        //        return true;
        //    else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
        //        return false;
        //    else
        //        return true;
        //}

        protected void PresentationGridItemUpdated(object source, GridUpdatedEventArgs e) { }
        protected void PresentationGridItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void PresentationGridItemInserted(object source, GridInsertedEventArgs e) { }
        protected void PresentationGridInsertCommand(object source, GridCommandEventArgs e)
        {
        }

        protected void PresentationGridItemCreated(object sender, GridItemEventArgs e) { }

        protected void PresentationGridItemDataBound(object sender, GridItemEventArgs e)
        {
        }

        protected void PresentationGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

        protected void btnApplyFilter_OnClick(object sender, EventArgs e)
        {
            ShowCheckedItems(lstStatus, lblFilter, true);
        }
    }
}
