﻿using System;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class ClientAddresses : UMABasePage
    {
        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];

            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                btnSave.Visible = false;
            }

            if (!Page.IsPostBack)
                GetData();
        }
        
        private void GetData()
        {
            var clientData = UMABroker.GetBMCInstance(new Guid(cid)) as OrganizationUnitCM;
            if (clientData != null)
            {
                var ds = new AddressDetailsDS();
                clientData.GetData(ds);
                PresentationData = ds;
                UMABroker.ReleaseBrokerManagedComponent(clientData);
               AddressDetailControl.FillAddressControls(ds);
            }
        }
       
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cid))
            {
                AddressDetailsDS ds = AddressDetailControl.SetData();
                SaveData(cid,ds);
                AddressDetailControl.FillAddressControls(ds);
            }
        }
    }
}
