﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="ModelSummary.aspx.cs" Inherits="eclipseonlineweb.ModelSummary" %>

<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" Assembly="C1.Web.Wijmo.Controls.4, Version=4.0.20121.56, Culture=neutral, PublicKeyToken=9b75583953471eea" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnSave" AlternateText="Save" ToolTip="Save" runat="server"
                                OnClick="BtnSave_Clicked" ImageUrl="~/images/Save-Icon.png" OnClientClick="javascript:return confirm('Changes may effect account process.Are you sure you want save?');" />
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <div id="DoItForMe" align="left">
                <asp:Panel runat="server" ID="pnlDoItForMe">
                    <fieldset>
                        <legend>Do It For Me</legend>
                        <table width="100%">
                            <tr>
                                <td style="width: 200px;">
                                    <asp:CheckBox ID="chkDIFM" runat="server" Text="Do It For Me" />
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="cmbDIFMModel" Height="25" Width="250" runat="server" ViewStateMode="Enabled">
                                    </c1:C1ComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:LinkButton Text="Print BWA Forms" ID="lnkPrintFormsDIFM" runat="server" 
                                        onclick="lnkPrintFormsDIFM_Click"></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
            </div>
            <div id="DoItWithMe" align="left">
                <asp:Panel runat="server" ID="pnlDoItWithMe">
                    <fieldset>
                        <legend>Do It With Me</legend>
                        <table width="100%">
                            <tr>
                                <td>
                                    Accounts to be opened:
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px;">
                                    <asp:CheckBox ID="chkDIWM" runat="server" Text="Do It With Me" />
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="cmbDIWMModel" Height="25" Width="250" runat="server" ViewStateMode="Enabled">
                                    </c1:C1ComboBox>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <asp:LinkButton onclick="lnkPrintFormsDIWM_Click" Text="Print BWA Forms" ID="lnkPrintFormsDIWM" runat="server"></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
            </div>
            <div id="DoItYourself" align="left">
                <asp:Panel runat="server" ID="pnlDoItYourself">
                    <fieldset>
                        <legend>Do It Yourself</legend>
                        <table width="100%">
                            <tr>
                                <td>
                                    Accounts to be opened:
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px;">
                                    <asp:CheckBox ID="chkDIYS" runat="server" Text="Do It Yourself" />
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="cmbDIYSModel" Height="25" Width="250" runat="server" ViewStateMode="Enabled">
                                    </c1:C1ComboBox>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <asp:LinkButton Text="Print BWA Forms" ID="lnkPrintFormsDIY" onclick="lnkPrintFormsDIY_Click" runat="server"></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
