﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="ClientAccountProcess.aspx.cs" Inherits="eclipseonlineweb.ClientViews.ClientAccountProcess" %>

<%@ Register TagPrefix="uc" TagName="Details" Src="~/Controls/AccountProcessBankAccountMappingControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="DesktopBrokerExistingAccountControl" Src="../Controls/DesktopBrokerExistingAccountControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="MISFundControl" Src="../Controls/MISFundAccountDetails.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .holder
        {
            width: 100%;
            display: block;
            z-index: 6;
        }
        .content
        {
            background: #fff;
            z-index: 7; /*  padding: 28px 26px 33px 25px;*/
        }
        .popup
        {
            border-radius: 7px;
            background: #6b6a63;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 800px;
            top: 20%;
            left: 50%;
            margin-left: -400px;
            margin-top: -40px;
            z-index: 6;
        }
     
        .popup1
        {
            border-radius: 7px;
            background: #6b6a63;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 820px;
            top: 20%;
            left: 50%;
            margin-left: -400px;
            margin-top: -40px;
            z-index: 6;
        }
        .overlay
        {
            width: 100%;
            opacity: 0.65;
            height: 100%;
            left: 0; /*IE*/
            top: 0;
            text-align: center;
            z-index: 5;
            position: fixed;
            background-color: #444444;
        }
    </style>
    <style type="text/css">
        .RadGrid_Metro .rgGroupHeader
        {
            background: none repeat scroll 0 0 #DAE6F4 !important;
        }
        
        .RadGrid_Metro .rgMasterTable td.rgGroupCol, .RadGrid_Metro .rgMasterTable td.rgExpandCol
        {
            background: none repeat scroll 0 0 #DAE6F4 !important;
            border-color: #DAE6F4 !important;
        }
        
        .RadGrid_Metro .rgAltRow
        {
            background: none repeat scroll 0 0 #F2F5F9 !important;
        }
    </style>
    <script type="text/javascript">
        function beforeCellEdit(e, args) {
            alert("edit");
            return false;

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <telerik:RadButton runat="server" ID="btnRefreshAccountOtherID" Text="REFRESH ACCOUNT OTHER ID"
                            OnClick="btnRefreshAccountOtherID_OnClick" Width="200px">
                        </telerik:RadButton>
                    </td>
                    <td>
                        <telerik:RadButton runat="server" ID="btnRefreshPortfolioFromSm" Text="REFRESH PORTFOLIO FROM SUPER"
                            OnClick="btnRefreshPortfolioFromSma_OnClick" Width="200px">
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ID="btnBack" ToolTip="Back" OnClick="btnbacks_Onclick"
                            Width="100PX">
                            <ContentTemplate>
                                <img src="../images//window_previous.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Back</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </td>
                    <td>
                        <telerik:RadButton runat="server" ID="btnRefreshDataFromSma" Text="REFRESH DATA FROM SUPER"
                            OnClick="btnRefreshDataFromSma_OnClick" Width="200px">
                        </telerik:RadButton>
                    </td>
                    <td style="width: 5%">
                    </td>
                    <td style="width: 5%">
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td style="width: 85%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <asp:Panel runat="server" ID="pnlFinancialSummary">
        <telerik:RadGrid ID="gvAccountProcess" runat="server" AllowSorting="True" AllowMultiRowSelection="True"
            Width="100%" OnItemCommand="gvAccountProcess_ItemCommand" AllowFilteringByColumn="True"
            OnNeedDataSource="gd_DealerGroup_OnNeedDataSource" OnItemDataBound="gvAccountProcess_OnItemDataBound"
            ShowGroupPanel="False" AutoGenerateColumns="False" OnPreRender="gvAccountProcess_PreRender"
            GridLines="none">
            <MasterTableView Width="100%" AllowFilteringByColumn="True" HeaderStyle-Font-Bold="True"
                FooterStyle-Font-Bold="True" AlternatingItemStyle-Font-Size="8" ItemStyle-Font-Size="8"
                GroupHeaderItemStyle-Font-Bold="True" GroupHeaderItemStyle-Font-Size="8" GroupLoadMode="Client"
                HeaderStyle-HorizontalAlign="Center">
                <GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="ServiceType" FieldName="ServiceType"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldName="ServiceType" SortOrder="Ascending"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>
                <Columns>
                    <telerik:GridBoundColumn SortExpression="LinkEntiyCLID" HeaderText="LinkEntiyCLID"
                        HeaderButtonType="TextButton" Display="False" UniqueName="LinkEntiyCLID" DataField="LinkEntiyCLID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="LinkEntiyCSID" HeaderText="LinkEntiyCSID"
                        HeaderButtonType="TextButton" Display="False" UniqueName="LinkEntiyCSID" DataField="LinkEntiyCSID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="LinkedEntityType" HeaderText="LinkedEntityType"
                        HeaderButtonType="TextButton" Display="False" UniqueName="LinkedEntityType" DataField="LinkedEntityType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="ModelID" HeaderText="ModelID" HeaderButtonType="TextButton"
                        Display="False" UniqueName="ModelID" DataField="ModelID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="AssetID" HeaderText="AssetID" HeaderButtonType="TextButton"
                        Display="False" UniqueName="AssetID" DataField="AssetID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="TaskID" HeaderText="TaskID" HeaderButtonType="TextButton"
                        Display="False" UniqueName="TaskID" DataField="TaskID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="CID" HeaderText="CID" HeaderButtonType="TextButton"
                        Display="False" UniqueName="CID" DataField="CID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="LinkEntiyFundID" HeaderText="LinkEntiyFundID"
                        HeaderButtonType="TextButton" Display="False" UniqueName="LinkEntiyFundID" DataField="LinkEntiyFundID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="Asset" HeaderText="Asset Name" HeaderButtonType="TextButton"
                        FilterControlWidth="80%" HeaderStyle-Width="8%" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" UniqueName="Asset" DataField="Asset">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="Product" HeaderText="Product Name" HeaderButtonType="TextButton"
                        FilterControlWidth="70%" HeaderStyle-Width="18%" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" UniqueName="Product" DataField="Product">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="Task" HeaderText="Task Description" HeaderButtonType="TextButton"
                        FilterControlWidth="90%" HeaderStyle-Width="60%" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" UniqueName="Task" DataField="Task">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="Status" HeaderText="Status" HeaderButtonType="TextButton"
                        FilterControlWidth="100%" HeaderStyle-Width="3%" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" UniqueName="Status" EmptyDataText="Inactive" DataField="Status">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="Completed" HeaderText="Is Completed" HeaderButtonType="TextButton"
                        Display="False" UniqueName="Completed" DataField="Completed">
                    </telerik:GridBoundColumn>
                    <telerik:GridButtonColumn CommandName="Associate" Text="Associate" ButtonType="LinkButton"
                        HeaderTooltip="Associate" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                    </telerik:GridButtonColumn>
                    <telerik:GridButtonColumn CommandName="Delete" Text="Delete" ButtonType="LinkButton"
                        ConfirmText="Are you sure you want to remove the association?" HeaderStyle-Width="5%"
                        ItemStyle-Width="5%" HeaderTooltip="Delete">
                    </telerik:GridButtonColumn>
                    <telerik:GridImageColumn UniqueName="ImgStatus" ImageAlign="Middle" AllowFiltering="False"
                        HeaderStyle-Width="2%">
                    </telerik:GridImageColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings AllowGroupExpandCollapse="True" ReorderColumnsOnClient="True" AllowDragToGroup="True"
                AllowColumnsReorder="True">
            </ClientSettings>
            <GroupingSettings ShowUnGroupButton="true"></GroupingSettings>
        </telerik:RadGrid>
    </asp:Panel>
    <asp:HiddenField ID="hfSelectedRowIndex" runat="server" />
    <div class="overlay" id="OVER" visible="False" runat="server">
    </div>
    <div id="BankAccountWindow" runat="server" visible="false" class="holder">
        <div class="popup">
            <div class="content">
                <uc:Details ID="BankAccountDetails" runat="server" />
            </div>
        </div>
    </div>
    <div id="DesktopBrokerWindow" runat="server" visible="false" class="holder">
        <div class="popup1">
            <div class="content">
                <uc:DesktopBrokerExistingAccountControl ID="ASXControl" runat="server" />
            </div>
        </div>
    </div>
    <div id="MisAccountWindow" runat="server" visible="false" class="holder">
        <div class="popup1"  style="width: 500px">
            <div class="content">
                <uc:MISFundControl ID="MISControl" runat="server" ShowReinvestRow="false"/>
            </div>
        </div>
    </div>
</asp:Content>
