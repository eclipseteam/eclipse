﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="ClientMainView.aspx.cs" Inherits="eclipseonlineweb.ClientMainView" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Chart"
    TagPrefix="wijmo" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript">
        //Resetting menu index
        localStorage['MenuIndex'] = '0';
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function hintContent() {
            return this.data.label + '<br/> ' + this.y + '';
        }
        function hintContentPie() {
            return this.data.toString() + " : " + window.Globalize.format(this.value / this.total, "p2");
        }

    </script>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlToolBar">
                <fieldset>
                    <table width="100%">
                        <tr>
                            <td style="width: 5%">
                                <asp:ImageButton ID="btnReport" AlternateText="Print" ToolTip="Print" runat="server"
                                    OnClick="btnReport_Click" ImageUrl="~/images/download.png" />
                            </td>
                            <td>
                                <c1:C1InputDate DateFormat="dd/MM/yyyy" ID="C1FinancialSummaryDate" runat="server"
                                    ShowTrigger="true">
                                </c1:C1InputDate>
                            </td>
                            <td>
                                <asp:ImageButton OnClick="GenerateFinancialSummary" runat="server" ID="ImageButton2"
                                    ImageUrl="~/images/database.png" />
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBack" Visible="false" ToolTip="Back" AlternateText="back"
                                    runat="server" ImageUrl="~/images/window_previous.png" OnClick="btnBack_Click" />
                            </td>
                            <td style="width: 5%">
                                <asp:ImageButton ID="btnMainView" Visible="false" ToolTip="Main View" AlternateText="Main View"
                                    runat="server" ImageUrl="~/images/application.png" OnClick="MainView_Click" />
                            </td>
                            <td style="width: 5%">
                                <asp:ImageButton ID="btnChart" Visible="false" AlternateText="Graphs" ToolTip="Graph"
                                    runat="server" OnClick="Chart_Click" ImageUrl="~/images/chart.png" />
                            </td>
                            <td style="width: 5%">
                                <asp:ImageButton ID="ImageButton3" Visible="false" AlternateText="Refresh" ToolTip="Refresh"
                                    runat="server" OnClick="Refresh_Click" ImageUrl="~/images/process.png" />
                                <td style="width: 85%;" class="breadcrumbgap">
                                    <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                                    <br />
                                    <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                                </td>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </asp:Panel>
            <p>
                <asp:LinkButton runat="server" Visible="false" ID="lnkBackToSummary" Text="Back to Summary"
                    OnClick="BackToSummary"> </asp:LinkButton></p>
            <asp:Panel runat="server" ID="pnlFinancialSummary">
                <c1:C1GridView AlternatingRowStyle-Font-Size="X-Small" FilterStyle-Font-Size="X-Small"
                    RowStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small" ID="FinancialSummary"
                    AllowSorting="true" OnSorting="FinancialSummarySorting" runat="server" AutogenerateColumns="false"
                    ShowFilter="true" OnFiltering="Filter" OnRowCommand="FinancialSummary_RowCommand"
                    OnRowDataBound="FinancialSummary_RowDataBound" ShowFooter="true">
                    <FooterStyle Font-Size="11"></FooterStyle>
                    <Columns>
                        <c1:C1BoundField DataField="TDID" Visible="false">
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="ProductID" Visible="false">
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="MODELNAME" SortExpression="MODELNAME" FooterText="Total"
                            Visible="false">
                            <GroupInfo OutlineMode="StartExpanded" Position="Header" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="AssetName" SortExpression="AssetName" Width="9%" HeaderText="Asset Name">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="ProductName" SortExpression="ProductName" Width="13%"
                            HeaderText="Product Name">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1TemplateField Width="22%" HeaderText="Description" SortExpression="Description">
                            <ItemTemplate>
                                <asp:Label Font-Size="X-Small" runat="server" ID="lblDesc" Text='<%# Bind("Description") %>'></asp:Label>
                                <asp:LinkButton CommandArgument='<%# Eval("AssetName") +"_" + Eval("TDID")+"_" + Eval("ProductID")+"_" + Eval("Description")+"_" + Eval("ServiceType")%>'
                                    ForeColor="#5188FF" Font-Underline="false" Font-Size="X-Small" Visible="false"
                                    runat="server" ID="linkDesc" Text='<%# Bind("Description") %>'></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Font-Size="X-Small" />
                        </c1:C1TemplateField>
                        <c1:C1BoundField DataField="AccountNo" SortExpression="AccountNo" Width="12%" HeaderText="Account#">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="Holding" SortExpression="Holding" Width="12%" HeaderText="Settled ($)"
                            Aggregate="Sum">
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="Unsettled" SortExpression="Unsettled" Width="12%" HeaderText="Unsettled ($)"
                            Aggregate="Sum">
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="Total" SortExpression="Total" Width="12%" HeaderText="Total ($)"
                            Aggregate="Sum">
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="PRICEATDATE" SortExpression="PRICEATDATE" Width="8%"
                            HeaderText="Price at Date" DataFormatString="dd/MM/yyyy">
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                    </Columns>
                </c1:C1GridView>
                <c1:C1GridView Visible="false" AlternatingRowStyle-Font-Size="X-Small" FilterStyle-Font-Size="X-Small"
                    RowStyle-Font-Size="X-Small" HeaderStyle-Font-Size="Small" ID="BreakdownGrid"
                    runat="server" AutogenerateColumns="false" ShowFilter="false" OnFiltering="Filter"
                    ShowFooter="true" Width="100%">
                    <Columns>
                        <c1:C1BoundField DataField="InvestmentCode" SortExpression="InvestmentCode" Width="8%"
                            HeaderText="Investment Code">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="InvestmentName" SortExpression="InvestmentName" Width="29%"
                            HeaderText="Description">
                            <ItemStyle HorizontalAlign="Left" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="UnitPrice" DataFormatString="N4" SortExpression="UnitPrice"
                            Width="9%" HeaderText="Unit Price">
                            <ItemStyle HorizontalAlign="Center" />
                            <FooterStyle Font-Size="X-Small" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="Holding" SortExpression="Holding" Aggregate="Sum" Width="9%"
                            HeaderText="Settled Units">
                            <ItemStyle HorizontalAlign="Center" />
                            <FooterStyle Font-Size="X-Small" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="UnsettledBuy" SortExpression="UnsettledBuy" Aggregate="Sum"
                            Width="9%" HeaderText="Unsettled Buy Units">
                            <ItemStyle HorizontalAlign="Center" />
                            <FooterStyle Font-Size="X-Small" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="UnsettledSell" SortExpression="UnsettledSell"
                            Aggregate="Sum" Width="9%" HeaderText="Unsettled Sell Units">
                            <ItemStyle HorizontalAlign="Center" />
                            <FooterStyle Font-Size="X-Small" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="CurrentValue" DataFormatString="C" SortExpression="CurrentValue"
                            Aggregate="Sum" Width="9%" HeaderText="Settled ($)">
                            <ItemStyle HorizontalAlign="Center" />
                            <FooterStyle Font-Size="X-Small" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="Unsettled" DataFormatString="C" SortExpression="Unsettled"
                            Aggregate="Sum" Width="9%" HeaderText="Unsettled ($)">
                            <ItemStyle HorizontalAlign="Center" />
                            <FooterStyle Font-Size="X-Small" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="Total" DataFormatString="C" SortExpression="Total" Aggregate="Sum"
                            Width="9%" HeaderText="Total ($)">
                            <ItemStyle HorizontalAlign="Center" />
                            <FooterStyle Font-Size="X-Small" />
                        </c1:C1BoundField>
                    </Columns>
                </c1:C1GridView>
                <c1:C1GridView Visible="false" AlternatingRowStyle-Font-Size="X-Small" FilterStyle-Font-Size="X-Small"
                    RowStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small" ID="TDBreakDown"
                    runat="server" AutogenerateColumns="false" ShowFilter="false" OnFiltering="Filter"
                    FooterStyle-Font-Size="X-Small" ShowFooter="true" Width="100%">
                    <Columns>
                        <c1:C1BoundField DataField="BROKER" SortExpression="BROKER" Width="20%" HeaderText="Broker">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="InsName" SortExpression="InsName" Width="20%" HeaderText="Institute Name">
                            <ItemStyle HorizontalAlign="Left" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="TransType" SortExpression="TransType" Width="15%" HeaderText="Transaction Type">
                            <ItemStyle HorizontalAlign="Left" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="TRANSDATE" DataFormatString="dd/MM/yyyy" SortExpression="TRANSDATE"
                            Width="15%" HeaderText="Transaction Date">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="Holding" DataFormatString="C" SortExpression="Holding"
                            Width="15%" Aggregate="Sum" HeaderText="Holding">
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterStyle Font-Size="X-Small" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="CumulativeAmount" Width="15%" HeaderText="Cumulative Balance"
                            SortExpression="CumulativeAmount" DataFormatString="C">
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterStyle Font-Size="X-Small" />
                        </c1:C1BoundField>
                    </Columns>
                </c1:C1GridView>
            </asp:Panel>
            <div class="WhiteBack">
                <asp:Panel Visible="false" runat="server" ID="pnlHoldingChart">
                    <%-- ReSharper disable UnknownCssClass --%>
                    <wijmo:C1PieChart runat="server" ID="C1PieChart1" Radius="140" Visible="false" Height="475"
                        Width="756" CssClass="ui-widget ui-widget-content ui-corner-all">
                        <%-- ReSharper restore UnknownCssClass --%>
                        <Hint>
                            <Content Function="hintContentPie" />
                        </Hint>
                        <Legend Visible="true"></Legend>
                        <Header Text="Steam - Mac Hardware">
                        </Header>
                        <SeriesStyles>
                            <wijmo:ChartStyle StrokeWidth="1.5" Stroke="#AFE500">
                                <Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#C3FF00" ColorEnd="#AFE500">
                                </Fill>
                            </wijmo:ChartStyle>
                            <wijmo:ChartStyle StrokeWidth="1.5" Stroke="#7FC73C">
                                <Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#8EDE43" ColorEnd="#7FC73C">
                                </Fill>
                            </wijmo:ChartStyle>
                            <wijmo:ChartStyle StrokeWidth="1.5" Stroke="#5F9996">
                                <Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#6AABA7" ColorEnd="#5F9996">
                                </Fill>
                            </wijmo:ChartStyle>
                            <wijmo:ChartStyle StrokeWidth="1.5" Stroke="#3E5F77">
                                <Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#466A85" ColorEnd="#3E5F77">
                                </Fill>
                            </wijmo:ChartStyle>
                            <wijmo:ChartStyle StrokeWidth="1.5" Stroke="#959595">
                                <Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#A6A6A6" ColorEnd="#959595">
                                </Fill>
                            </wijmo:ChartStyle>
                        </SeriesStyles>
                        <Footer Compass="South" Visible="False">
                        </Footer>
                        <Axis>
                            <Y Visible="False" Compass="West">
                                <Labels TextAlign="Center">
                                </Labels>
                                <GridMajor Visible="True">
                                </GridMajor>
                            </Y>
                        </Axis>
                    </wijmo:C1PieChart>
                    <wijmo:C1BarChart Visible="false" Stacked="true" runat="server" ID="C1BarChartHoldingSumDIFM"
                        Height="350" Width="100%">
                        <Hint>
                            <Content Function="hintContent" />
                        </Hint>
                        <Axis>
                            <Y TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="SETTLED ($)">
                            </Y>
                            <X Text="">
                            </X>
                        </Axis>
                        <Header TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="HOLDING SUMMARY - DO IT FOR ME">
                        </Header>
                    </wijmo:C1BarChart>
                    <wijmo:C1BarChart Visible="false" Stacked="true" runat="server" ID="C1BarChartHoldingSumDIWM"
                        Height="350" Width="100%">
                        <Hint>
                            <Content Function="hintContent" />
                        </Hint>
                        <Axis>
                            <Y TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="SETTLED ($)">
                            </Y>
                            <X Text="">
                            </X>
                        </Axis>
                        <Header TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="HOLDING SUMMARY - DO IT WITH ME">
                        </Header>
                    </wijmo:C1BarChart>
                    <wijmo:C1BarChart Visible="false" Stacked="true" runat="server" ID="C1BarChartHoldingSumDIY"
                        Height="350" Width="100%">
                        <Hint>
                            <Content Function="hintContent" />
                        </Hint>
                        <Axis>
                            <Y TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="SETTLED ($)">
                            </Y>
                            <X Text="">
                            </X>
                        </Axis>
                        <Header TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="HOLDING SUMMARY - DO IT YOURSELF">
                        </Header>
                    </wijmo:C1BarChart>
                    <wijmo:C1BarChart Visible="false" Stacked="true" runat="server" ID="C1BarChartHoldingSumMAN"
                        Height="350" Width="100%">
                        <Hint>
                            <Content Function="hintContent" />
                        </Hint>
                        <Axis>
                            <Y TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="SETTLED ($)">
                            </Y>
                            <X Text="">
                            </X>
                        </Axis>
                        <Header TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="HOLDING SUMMARY - Manual Assets">
                        </Header>
                    </wijmo:C1BarChart>
                </asp:Panel>
            </div>
            <div>
                <asp:HiddenField runat="server" ID="hidIsAdmin" />
                <asp:HiddenField runat="server" ID="hidUserType" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
