﻿using System;
using System.Data;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.DataSets;

namespace eclipseonlineweb
{
    public partial class ClientEventLog : UMABasePage
    {
        private void ShowLog()
        {
            this.cid = this.Request["ins"];
            var log = UMABroker.CreateTransientComponentInstance(new Guid("D7FE4F30-D06F-11E0-9572-0800200C9A66"));
            EventLogDS data = new EventLogDS();
            data.CIDFilter = new Guid(cid); 
            log.GetData(data);
            this.PresentationData = data;
            LogGrid.DataSource = data;
            LogGrid.DataBind();
        }

        public override void LoadPage()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");

            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
                Response.Redirect("AccountsFUM.aspx");

            UMABroker.ReleaseBrokerManagedComponent(objUser);
            ShowLog();
        }
    }
}
