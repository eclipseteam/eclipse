﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Data;

namespace eclipseonlineweb
{
    public partial class IncomeDistributions : UMABasePage
    {

        protected override void Intialise()
        {
            base.Intialise();
            DistributionDetails.Saved += x =>
            {
                ModalPopupExtender1.Hide();
                PresentationGrid.Rebind();
            };
            DistributionDetails.Canceled += () => ModalPopupExtender1.Hide();
            DistributionDetails.ValidationFailed += () => ModalPopupExtender1.Show();
            DistributionDetails.SaveData += ds =>
            {
                SaveData(cid,ds);
                ModalPopupExtender1.Hide();
            };
        }

        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];
            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
                sm.RegisterPostBackControl(btnReport);
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");

            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                PresentationGrid.Columns.FindByUniqueNameSafe("DeleteColumn").Visible = false;
            }

            UMABroker.ReleaseBrokerManagedComponent(objUser);
        }

        protected void BtnReportClick(object sender, EventArgs e)
        {
            BulkTransactionsOutput(this.cid, this.ClientHeaderInfo1.ClientName);
        }

        private void GetData()
        {
            cid = Request.QueryString["ins"];
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(cid));
            var ds = new ClientDistributionsDS();
            clientData.GetData(ds);
            PresentationData = ds;
            var summaryView = new DataView(ds.Tables[ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE]) { Sort = "RecordDate DESC" };
            PresentationGrid.DataSource = summaryView;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var id = new Guid(dataItem["ID"].Text);
                    DistributionDetails.SetEntity(id, new Guid(cid), ClientHeaderInfo1.ClientName);
                    ModalPopupExtender1.Show();
                    break;
                case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var ds = new ClientDistributionsDS { CommandType = DatasetCommandTypes.Delete };
                    var tab = new DataTable(ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE);
                    tab.Columns.Add(new DataColumn("ID", typeof(Guid)));
                    ds.Tables.Add(tab);
                    DataRow dr = ds.Tables[ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE].NewRow();
                    dr["ID"] = dataItem["ID"].Text;
                    ds.Tables[ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE].Rows.Add(dr);
                    SaveData(cid, ds);
                    PresentationGrid.Rebind();
                    break;
            }
        }
    }
}
