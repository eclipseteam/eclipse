﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.Common.UMAFee;

namespace eclipseonlineweb
{
    public partial class FeeDetailsReadOnly : UMABasePage
    {
        string id = String.Empty;
        protected override void GetBMCData()
        {
            id = Request.QueryString["ID"].ToString();
            this.cid = Request.QueryString["INS"].ToString();
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
            umaFeesDetailsDS.FeeEntityID = new Guid(id);
            organization.GetData(umaFeesDetailsDS);
            this.PresentationData = umaFeesDetailsDS;
            lblDetails.Text = umaFeesDetailsDS.FeeEntity.Description + " - Breakdown";
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }


        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "cjohnson" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        public override void LoadPage()
        {
            base.LoadPage();

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
            {
                ((SetupMaster)Master).IsAdmin = "1";
            }
            //Here we get the page name
            string PageName = Request.QueryString["PageName"];
            btnBack.Visible = !string.IsNullOrEmpty(PageName);
        }
        protected void btnBack_Click(object sender, EventArgs args)
        {
            string PageName = Request.QueryString["PrePage"];
            if(PageName == string.Empty)
            Response.Redirect("~/ClientViews/ConfigureFees.aspx?ins=" + Request.QueryString["ins"].ToString());
            else
                Response.Redirect("~/ClientViews/ConfigureFees.aspx?ins=" + Request.QueryString["ins"].ToString() +"&PrePage=" + PageName);
        }

        #region Telerik Grid OngoingFee Value

        protected void GridOngoingFeeValue_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e){}
        protected void GridOngoingFeeValue_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                Guid rowID = new Guid(gDataItem["ID"].Text);
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                iorg.GetData(umaFeesDetailsDS);

                var feeEnitity = umaFeesDetailsDS.FeeEntity.OngoingFeeValueList.Where(fee => fee.ID == rowID).FirstOrDefault();
                if (feeEnitity != null)
                    umaFeesDetailsDS.FeeEntity.OngoingFeeValueList.Remove(feeEnitity);

                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.GridOngoingFeeValue.Rebind();

            }

            else if (e.CommandName.ToLower() == "update")
            {
                GridEditFormItem gDataItem = e.Item as GridEditFormItem;
                Guid rowID = new Guid(gDataItem.GetDataKeyValue("ID").ToString());
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                iorg.GetData(umaFeesDetailsDS);

                var feeEnitity = umaFeesDetailsDS.FeeEntity.OngoingFeeValueList.Where(fee => fee.ID == rowID).FirstOrDefault();

                if (feeEnitity != null)
                {
                    feeEnitity.FeeValue = Int32.Parse(((TextBox)(((GridEditableItem)(e.Item))["FeeValue"].Controls[0])).Text);
                    feeEnitity.Description = ((TextBox)(((GridEditableItem)(e.Item))["Description"].Controls[0])).Text;
                    feeEnitity.Year = umaFeesDetailsDS.FeeEntity.Year;
                }

                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.GridOngoingFeeValue.Rebind();
            }
        }
        protected void GridOngoingFeeValue_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e){}
        protected void GridOngoingFeeValue_ItemDeleted(object source, GridDeletedEventArgs e){}
        protected void GridOngoingFeeValue_ItemInserted(object source, GridInsertedEventArgs e){}
        protected void GridOngoingFeeValue_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("FeesOngoingValue".Equals(e.Item.OwnerTableView.Name))
            {
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                iorg.GetData(umaFeesDetailsDS); 

                OngoingFeeValue ongoingFeeValue = new Oritax.TaxSimp.Common.UMAFee.OngoingFeeValue();
                ongoingFeeValue.FeeValue = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["FeeValue"].Controls[0])).Text);
                ongoingFeeValue.Description = ((TextBox)(((GridEditableItem)(e.Item))["Description"].Controls[0])).Text;
                ongoingFeeValue.Year = umaFeesDetailsDS.FeeEntity.Year;
                umaFeesDetailsDS.FeeEntity.OngoingFeeValueList.Add(ongoingFeeValue);

                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.GridOngoingFeeValue.Rebind();
            }
        }
        protected void GridOngoingFeeValue_ItemCreated(object sender, GridItemEventArgs e){}
        protected void GridOngoingFeeValue_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e){}
        protected void GridOngoingFeeValue_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetBMCData();

            UMAFeesDetailsDS umaFeesDetailsDS = (UMAFeesDetailsDS)this.PresentationData;
            this.GridOngoingFeeValue.DataSource = umaFeesDetailsDS.FeeEntity.OngoingFeeValueList; 
        }

        #endregion 

        #region Telerik Grid OngoingFee Percentage

        protected void GridOngoingFeePercent_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e) { }
        protected void GridOngoingFeePercent_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                Guid rowID = new Guid(gDataItem["ID"].Text);
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                iorg.GetData(umaFeesDetailsDS);

                var feeEnitity = umaFeesDetailsDS.FeeEntity.OngoingFeePercentList.Where(fee => fee.ID == rowID).FirstOrDefault();
                if (feeEnitity != null)
                    umaFeesDetailsDS.FeeEntity.OngoingFeePercentList.Remove(feeEnitity); 

                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.GridOngoingFeePercent.Rebind();

            }

            else if (e.CommandName.ToLower() == "update")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                Guid rowID = new Guid(gDataItem["ID"].Text);
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                iorg.GetData(umaFeesDetailsDS);

                var feeEnitity = umaFeesDetailsDS.FeeEntity.OngoingFeePercentList.Where(fee => fee.ID == rowID).FirstOrDefault();
                
                if (feeEnitity != null)
                {
                    feeEnitity.PercentageFee = Int32.Parse(((TextBox)(((GridEditableItem)(e.Item))["PercentageFee"].Controls[0])).Text);
                    feeEnitity.Description = ((TextBox)(((GridEditableItem)(e.Item))["Description"].Controls[0])).Text;
                    feeEnitity.Year = umaFeesDetailsDS.FeeEntity.Year;
                }
                
                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.GridOngoingFeePercent.Rebind();
            }
        }
        protected void GridOngoingFeePercent_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) { }
        protected void GridOngoingFeePercent_ItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void GridOngoingFeePercent_ItemInserted(object source, GridInsertedEventArgs e) { }
        protected void GridOngoingFeePercent_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("FeesOngoingPercent".Equals(e.Item.OwnerTableView.Name))
            {
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                iorg.GetData(umaFeesDetailsDS);

                OngoingFeePercent ongoingFeePercent = new Oritax.TaxSimp.Common.UMAFee.OngoingFeePercent();
                ongoingFeePercent.PercentageFee = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["PercentageFee"].Controls[0])).Text);
                ongoingFeePercent.Description = ((TextBox)(((GridEditableItem)(e.Item))["Description"].Controls[0])).Text;
                ongoingFeePercent.Year = umaFeesDetailsDS.FeeEntity.Year;
                umaFeesDetailsDS.FeeEntity.OngoingFeePercentList.Add(ongoingFeePercent);

                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.GridOngoingFeePercent.Rebind();
            }
        }
        protected void GridOngoingFeePercent_ItemCreated(object sender, GridItemEventArgs e) { }
        protected void GridOngoingFeePercent_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e) { }
        protected void GridOngoingFeePercent_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetBMCData();

            UMAFeesDetailsDS umaFeesDetailsDS = (UMAFeesDetailsDS)this.PresentationData;
            this.GridOngoingFeePercent.DataSource = umaFeesDetailsDS.FeeEntity.OngoingFeePercentList; 
        }

        #endregion 

        #region Telerik Grid Fee Tiered 

        protected void TieredFee_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e) { }
        protected void TieredFee_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                Guid rowID = new Guid(gDataItem["ID"].Text);
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                iorg.GetData(umaFeesDetailsDS);

                var feeEnitity = umaFeesDetailsDS.FeeEntity.FeeTieredRangeList.Where(fee => fee.ID == rowID).FirstOrDefault();
                if (feeEnitity != null)
                    umaFeesDetailsDS.FeeEntity.FeeTieredRangeList.Remove(feeEnitity);

                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.TieredFee.Rebind();

            }
            else if (e.CommandName.ToLower() == "update")
            {
                GridEditFormItem gDataItem = e.Item as GridEditFormItem;
                Guid rowID = (Guid)gDataItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"];

                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                iorg.GetData(umaFeesDetailsDS);

                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;

                var feeEnitity = umaFeesDetailsDS.FeeEntity.FeeTieredRangeList.Where(fee => fee.ID == rowID).FirstOrDefault();

                if (feeEnitity != null)
                {
                    feeEnitity.Description = ((TextBox)(((GridEditableItem)(e.Item))["Description"].Controls[0])).Text;
                    feeEnitity.Year = umaFeesDetailsDS.FeeEntity.Year;
                    feeEnitity.FromValue = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["FromValue"].Controls[0])).Text);
                    feeEnitity.ToValue = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["ToValue"].Controls[0])).Text);
                    feeEnitity.PercentageFee = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["PercentageFee"].Controls[0])).Text);
                }

                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;

                GetBMCData();
                this.TieredFee.Rebind();
            }
        }
        protected void TieredFee_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) { }
        protected void TieredFee_ItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void TieredFee_ItemInserted(object source, GridInsertedEventArgs e) { }
        protected void TieredFee_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("TieredFees".Equals(e.Item.OwnerTableView.Name))
            {
                GridEditFormInsertItem EditForm = (GridEditFormInsertItem)e.Item;

                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                iorg.GetData(umaFeesDetailsDS);

                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                
                FeeTieredRange feeTieredRange = new FeeTieredRange();
                feeTieredRange.Description = ((TextBox)(((GridEditableItem)(e.Item))["Description"].Controls[0])).Text;
                feeTieredRange.Year = umaFeesDetailsDS.FeeEntity.Year;
                feeTieredRange.FromValue = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["FromValue"].Controls[0])).Text);
                feeTieredRange.ToValue = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["ToValue"].Controls[0])).Text);
                feeTieredRange.PercentageFee = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["PercentageFee"].Controls[0])).Text);

                umaFeesDetailsDS.FeeEntity.FeeTieredRangeList.Add(feeTieredRange);

                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.TieredFee.Rebind();
            }
        }
        protected void TieredFee_ItemCreated(object sender, GridItemEventArgs e) { }
        protected void TieredFee_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e) { }
        protected void TieredFee_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetBMCData();

            UMAFeesDetailsDS umaFeesDetailsDS = (UMAFeesDetailsDS)this.PresentationData;
            this.TieredFee.DataSource = umaFeesDetailsDS.FeeEntity.FeeTieredRangeList; 
        }

        #endregion 

    }
}
