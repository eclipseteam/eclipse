﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="ManualTransactions.aspx.cs" Inherits="eclipseonlineweb.ManualTransactions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnReport" AlternateText="Print" ToolTip="Print" runat="server"
                                OnClick="BtnReportClick" ImageUrl="~/images/download.png" />
                        </td>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btn" AlternateText="Add Manual Assets Transactions" ToolTip="Add Manual Assets Transactions"
                                runat="server" OnClick="BtnAddMATran" ImageUrl="~/images/add-icon.png" />
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                PageSize="18" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource"
                OnItemCommand="PresentationGrid_OnItemCommand">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="Banks" TableLayout="Fixed" Font-Size="8">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="ID" UniqueName="ID" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="InvestmentCode" ReadOnly="true" HeaderText="Investment Code"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="InvestmentCode" UniqueName="InvestmentCode">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="TransactionType" ReadOnly="true" HeaderText="Transaction Type"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="TransactionType" UniqueName="TransactionType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="20%" SortExpression="InvestmentDesc"
                            ReadOnly="true" HeaderText="Description" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InvestmentDesc"
                            UniqueName="InvestmentDesc">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="TransactionDate" HeaderText="Transaction Date"
                            ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransactionDate"
                            UniqueName="TransactionDate" DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="SettlementDate" ShowFilterIcon="true"
                            HeaderText="Settlement Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            HeaderButtonType="TextButton" DataField="SettlementDate" UniqueName="SettlementDate"
                            DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="UnitsOnHand" HeaderText="Units On Hand"
                            AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                            HeaderButtonType="TextButton" DataField="UnitsOnHand" UniqueName="UnitsOnHand"
                            DataFormatString="{0:N4}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="UnitPrice" HeaderText="Unit Price"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="UnitPrice" UniqueName="UnitPrice" DataFormatString="{0:N6}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="Holding" HeaderText="Holding"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="Holding" UniqueName="Holding" DataFormatString="{0:C}">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                            UniqueName="EditColumn">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                        <telerik:GridButtonColumn ConfirmText="Are you sure you want to remove the transaction?"
                            ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
            <asp:ModalPopupExtender ID="ModalPopupExtenderEditTran" runat="server" CancelControlID="btnCancel"
                TargetControlID="btnShowPopup" PopupControlID="pnlpopup" PopupDragHandleControlID="PopupHeader"
                Drag="true" BackgroundCssClass="ModalPopupBG">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlpopup" runat="server" BackColor="White" Style="display: none">
                <div class="popup_Container">
                    <div class="popup_Titlebar" id="PopupHeader">
                        <div class="TitlebarLeft">
                            Transaction Details
                        </div>
                        <div class="TitlebarRight" onclick="cancel();">
                        </div>
                    </div>
                    <div class="PopupBody">
                        <table>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Investment Code:
                                </td>
                                <td>
                                    <asp:Label Visible="false" runat="server" ID="lblTranID"></asp:Label>
                                    <asp:CheckBox runat="server" Visible="false" Checked="false" ID="IsSave" />
                                    <asp:TextBox ReadOnly="true" runat="server" ID="InvestmentCode" Width="400px"> </asp:TextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Investment Name:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxManualAssets" DataTextField="CompanyName" DataValueField="ID"
                                        runat="server" Width="400px" CssClass="" ViewStateMode="Enabled" Height="100px"
                                        EnableTheming="false" TriggerPosition="Right" ShowTrigger="true" ShowingAnimation-Animated-Disabled="true">
                                    </c1:C1ComboBox>
                                    <asp:TextBox ReadOnly="true" runat="server" ID="InvestmentDesc" Width="400px"> </asp:TextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Settlement Date:
                                </td>
                                <td>
                                    <c1:C1InputDate runat="server" ID="SettlementDate" Width="410px" Height="10px" DateFormat="dd/MM/yyyy">
                                    </c1:C1InputDate>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Transaction Date:
                                </td>
                                <td>
                                    <c1:C1InputDate runat="server" ID="TransactionDate" Width="410px" Height="10px" DateFormat="dd/MM/yyyy">
                                    </c1:C1InputDate>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Transaction Type:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxManual" runat="server" Width="400px" CssClass="" ViewStateMode="Enabled"
                                        Height="100px" EnableTheming="false" TriggerPosition="Right" ShowTrigger="true"
                                        ShowingAnimation-Animated-Disabled="true">
                                        <Items>
                                            <c1:C1ComboBoxItem Text="Adjustment Down" Value="AJ" />
                                            <c1:C1ComboBoxItem Text="Adjustment up" Value="RJ" />
                                            <c1:C1ComboBoxItem Text="Application" Value="AP" />
                                            <c1:C1ComboBoxItem Text="Bonus Issue" Value="AM" />
                                            <c1:C1ComboBoxItem Text="Buy / Purchase" Value="AN" />
                                            <c1:C1ComboBoxItem Text="Commutation" Value="RU" />
                                            <c1:C1ComboBoxItem Text="Contribution" Value="AC" />
                                            <c1:C1ComboBoxItem Text="Contribution Tax" Value="RL" />
                                            <c1:C1ComboBoxItem Text="Deposit" Value="AD" />
                                            <c1:C1ComboBoxItem Text="Expense" Value="EX" />
                                            <c1:C1ComboBoxItem Text="Instalment" Value="LM" />
                                            <c1:C1ComboBoxItem Text="Insurance Premium" Value="RH" />
                                            <c1:C1ComboBoxItem Text="Lodgement" Value="AL" />
                                            <c1:C1ComboBoxItem Text="Lump Sum Tax" Value="RK" />
                                            <c1:C1ComboBoxItem Text="Manager Fee" Value="MA" />
                                            <c1:C1ComboBoxItem Text="Opening (Initial) Balance" Value="IN" />
                                            <c1:C1ComboBoxItem Text="PAYG" Value="RY" />
                                            <c1:C1ComboBoxItem Text="Pension Payment" Value="PR" />
                                            <c1:C1ComboBoxItem Text="Pension Payment – Income Stream" Value="RR" />
                                            <c1:C1ComboBoxItem Text="Purchase Reinvestment" Value="VB" />
                                            <c1:C1ComboBoxItem Text="Reversal - Redemption / Withdrawal / Fee / Tax" Value="VA" />
                                            <c1:C1ComboBoxItem Text="Rights Issue" Value="AE" />
                                            <c1:C1ComboBoxItem Text="Sale" Value="RA" />
                                            <c1:C1ComboBoxItem Text="Surcharge Tax" Value="ST" />
                                            <c1:C1ComboBoxItem Text="Switch In" Value="AS" />
                                            <c1:C1ComboBoxItem Text="Switch Out" Value="RS" />
                                            <c1:C1ComboBoxItem Text="Tax" Value="RX" />
                                            <c1:C1ComboBoxItem Text="Tax Rebate" Value="AR" />
                                            <c1:C1ComboBoxItem Text="Transfer In" Value="AT" />
                                            <c1:C1ComboBoxItem Text="Transfer Out" Value="RT" />
                                            <c1:C1ComboBoxItem Text="Withdrawal" Value="RW" />
                                        </Items>
                                    </c1:C1ComboBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Units Holding:
                                </td>
                                <td>
                                    <c1:C1InputNumeric Width="410px" ID="UnitsOnHand" runat="server" DecimalPlaces="4"
                                        ShowSpinner="false" Value="50">
                                    </c1:C1InputNumeric>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Price Type:
                                </td>
                                <td>
                                    <c1:C1ComboBox ID="C1ComboBoxPriceType" runat="server" Width="400px" CssClass=""
                                        ViewStateMode="Enabled" Height="100px" EnableTheming="false" TriggerPosition="Right"
                                        ShowTrigger="true" ShowingAnimation-Animated-Disabled="true">
                                        <Items>
                                            <c1:C1ComboBoxItem Text="UNIT PRICE" Value="UNIT PRICE" Selected="true" />
                                            <c1:C1ComboBoxItem Text="NAV PRICE" Value="NAV PRICE" />
                                            <c1:C1ComboBoxItem Text="PUR PRICE" Value="PUR PRICE" />
                                        </Items>
                                    </c1:C1ComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Units Price:
                                </td>
                                <td>
                                    <c1:C1InputNumeric Width="410px" DisableUserInput="true" ID="UnitPrice" runat="server"
                                        DecimalPlaces="6" ShowSpinner="false" Value="50">
                                    </c1:C1InputNumeric>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Total Holding($):
                                </td>
                                <td>
                                    <c1:C1InputCurrency Width="410px" DisableUserInput="true" ID="Holding" runat="server"
                                        ShowSpinner="false" Value="50">
                                    </c1:C1InputCurrency>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblMessage" Text="" ForeColor="Red"></asp:Label>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnUpdate" CommandName="Update" runat="server" Text="Save" OnClick="UpdateTransaction" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
