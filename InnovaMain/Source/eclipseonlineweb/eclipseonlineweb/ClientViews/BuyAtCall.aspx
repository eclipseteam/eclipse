﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="BuyAtCall.aspx.cs" Inherits="eclipseonlineweb.ClientViews.BuyAtCall" %>

<%@ Register Src="../Controls/BuyAtCall.ascx" TagName="BuyAtCall" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:BuyAtCall ID="BuyAtCallControl" runat="server" />
</asp:Content>
