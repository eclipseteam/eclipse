﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="MISAccounts.aspx.cs" Inherits="eclipseonlineweb.MISAccounts" %>

<%@ Register TagPrefix="uc" TagName="MISFund" Src="~/Controls/MISFundAccountDetails.ascx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head" />
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btn" AlternateText="Add MIS Fund" ToolTip="Add MIS Fund" runat="server"
                                OnClick="LnkbtnAddClick" ImageUrl="~/images/add-icon.png" />
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource"
                OnItemCommand="PresentationGrid_OnItemCommand">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="Banks" TableLayout="Fixed">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="CID" ReadOnly="true" HeaderText="CID" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="CID" UniqueName="CID" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="CLID" ReadOnly="true" HeaderText="CLID"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="CLID" UniqueName="CLID" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="CSID" ReadOnly="true" HeaderText="CSID"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="CSID" UniqueName="CSID" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="FundID" ReadOnly="true" HeaderText="FundID"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="FundID" UniqueName="FundID" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="120px" ReadOnly="true" SortExpression="FundCode"
                            HeaderText="Fund Code" AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                            HeaderButtonType="TextButton" DataField="FundCode" UniqueName="FundCode">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="275px" SortExpression="FundDescription"
                            HeaderText="Fund Description" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="FundDescription"
                            UniqueName="FundDescription">
                        </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn FilterControlWidth="175px" SortExpression="ReinvestmentOption"
                            HeaderText="Reinvest option" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ReinvestmentOption"
                            UniqueName="ReinvestmentOption">
                        </telerik:GridBoundColumn>

                        <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                            UniqueName="EditColumn">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                        <telerik:GridButtonColumn ConfirmText="Are you sure you want to remove the association?"
                            ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:HiddenField ID="hfSelectedRowIndex" runat="server" />
            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
            <asp:Button ID="CancelButton" runat="server" Style="display: none" />
            <asp:Button ID="OkButton" runat="server" Style="display: none" />
            <asp:ModalPopupExtender ID="ModalPopupExtender1" DropShadow="true" BackgroundCssClass="ModalPopupBG"
                runat="server" Drag="true" TargetControlID="btnShowPopup" CancelControlID="CancelButton"
                OkControlID="OkButton" PopupControlID="panlAdd" PopupDragHandleControlID="PopupHeader">
            </asp:ModalPopupExtender>
            <asp:Panel runat="server" ID="panlAdd">
                <uc:MISFund ID="misFund" runat="server" ShowReinvestRow="true"/>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
