﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using C1.C1Report;
using System.Configuration;
using C1.Web.Wijmo.Extenders.C1Chart;
using C1.Web.Wijmo.Controls.C1Chart;
using System.Text;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using C1.Web.Wijmo.Controls.C1ComboBox;
using C1.Web.Wijmo.Controls.C1Dialog;
using System.Web.Services;
using System.Collections.Specialized;
using AjaxControlToolkit;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb.ClientViews
{
    public partial class TermDeposit : UMABasePage
    {
        private string _cid = string.Empty;

        protected override void Intialise()
        {
            base.Intialise();
            TermDepositGrid.RowEditing += new C1GridViewEditEventHandler(TermDepositGrid_RowEditing);
        }

        protected void TermDepositGrid_RowEditing(object sender, C1GridViewEditEventArgs e)
        {

        }

        protected void UpdateTransaction(object sender, EventArgs e)
        {
            
        }

        protected void AddTransaction(object sender, EventArgs e)
        {
           
        }

        protected void saImportTransactionTypeDDL_SelectedIndexChanged(object sender, EventArgs e)
        { }
        protected void EditTransaction(object sender, EventArgs e)
        { }

       
        public override void LoadPage()
        {
            _cid = Request.QueryString["ins"];

            TermDeposit_Click();

            ScriptManager sm = ScriptManager.GetCurrent(Page);

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");

            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                TermDepositGrid.Columns[TermDepositGrid.Columns.Count - 1].Visible = false;
                TermDepositGrid.Columns[TermDepositGrid.Columns.Count - 2].Visible = false;
                TermDepositGrid.Columns[TermDepositGrid.Columns.Count - 3].Visible = false;
            }
            if (sm != null)
            {
                sm.RegisterPostBackControl(btnEdit);
                
            }
          
        }

        private void TermDeposit_Click()
        {
            pnlBank.Visible = true;
            _cid = Request.QueryString["ins"];
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(_cid));
            TermDepositDS ds = new TermDepositDS();
            clientData.GetData(ds);
            PresentationData = ds;
            DataView summaryView = new DataView(ds.Tables[ds.TermDepositTable.TABLENAME]);
            summaryView.Sort = ds.TermDepositTable.ACCOUNTNAME + " DESC";
            TermDepositGrid.DataSource = summaryView;
            TermDepositGrid.DataBind();

            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(_cid));
            BankTransactionDS banktransacationDS = new BankTransactionDS();
            MISTransactionDS mISTransactionDS = new MISTransactionDS();
            ASXTransactionDS aSXTransactionDS = new ASXTransactionDS();
            DIVTransactionDS dIVTransactionDS = new DIVTransactionDS();
            TDTransactionDS tdTransactionDS = new TDTransactionDS();
            ManualTransactionDS manualTransactionDS = new ManualTransactionDS();

            clientData.GetData(banktransacationDS);
            clientData.GetData(mISTransactionDS);
            clientData.GetData(aSXTransactionDS);
            clientData.GetData(dIVTransactionDS);
            clientData.GetData(tdTransactionDS);
            clientData.GetData(manualTransactionDS);

            DataSet excelDataset = new DataSet();
            excelDataset.Merge(PresentationData, true, MissingSchemaAction.Add);
            excelDataset.Merge(banktransacationDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(mISTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(aSXTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(dIVTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(tdTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(manualTransactionDS, true, MissingSchemaAction.Add);

            if (excelDataset.Tables.Contains("HoldingSummary"))
            {
                excelDataset.Tables["HoldingSummary"].Columns.Remove("TDID");
                excelDataset.Tables["HoldingSummary"].Columns.Remove("ProductID");
            }

            if (excelDataset.Tables.Contains("Cash Transactions"))
            {
                excelDataset.Tables["Cash Transactions"].Columns.Remove("InstitutionID");
                excelDataset.Tables["Cash Transactions"].Columns.Remove("ID");
                excelDataset.Tables["Cash Transactions"].Columns.Remove("DividendID");
            }

            if (excelDataset.Tables.Contains("MIS Transactions"))
                excelDataset.Tables["MIS Transactions"].Columns.Remove("ID");

            if (excelDataset.Tables.Contains("TDs Transactions"))
            {
                excelDataset.Tables["TDs Transactions"].Columns.Remove("ID");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("AccountName");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("DividendID");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("InstitutionID");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("Product");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("BSB");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("AccountType");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("AccountNo");
            }

            if (excelDataset.Tables.Contains("Manual Transactions"))
                excelDataset.Tables["Manual Transactions"].Columns.Remove("ID");

            if (excelDataset.Tables.Contains("ProductBreakDown"))
                excelDataset.Tables.Remove("ProductBreakDown");
            if (excelDataset.Tables.Contains("TDBreakDown"))
                excelDataset.Tables.Remove("TDBreakDown");
            if (excelDataset.Tables.Contains("ContactTable"))
                excelDataset.Tables.Remove("ContactTable");

            ExcelHelper.ToExcel(excelDataset,   "bankaccounts.xls", Page.Response);
        }

        protected void Grid_RowCommand(object sender, C1GridViewCommandEventArgs e)
        {
            Dictionary<string, string> SourceImportTransactionType = CashManagementEntity.GetImportTransactionDataSource();

            if (e.CommandName == "edit")
            {
                C1ComboBoxImport.SelectedValue = string.Empty;
                C1ComboBoxCat.SelectedValue = string.Empty;
                C1ComboBoxSystem.SelectedValue = string.Empty;
                lblTranID.Text = string.Empty;
                lblBankCID.Text = string.Empty;
                textComments.Text = string.Empty;
                lblBankDetails.Text = string.Empty;

                string rowID = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[0].Text;
                DataRow selectedRow = PresentationData.Tables["Cash Transactions"].Select("ID='" + rowID + "'").FirstOrDefault();
                transactionDate.Date = (DateTime)selectedRow["BankTransactionDate"];
                C1ComboBoxImport.Text = selectedRow["ImportTransactionType"].ToString();
                C1ComboBoxCat.Text = selectedRow["Category"].ToString();
                C1ComboBoxSystem.Text = selectedRow["SystemTransactionType"].ToString();
                lblTranID.Text = selectedRow["ID"].ToString();
                lblBankCID.Text = selectedRow["BankCID"].ToString();
                lblBankDetails.Text = selectedRow["AccountName"].ToString();
                Amount.Value = Convert.ToDouble(selectedRow["Amount"]);
                Adjustment.Value = Convert.ToDouble(selectedRow["Adjustment"]);
                TotalAmount.Value = Convert.ToDouble(selectedRow["AmountTotal"]);
                textComments.Text = selectedRow[BankTransactionDetailsDS.COMMENT].ToString();

                if (selectedRow["ImportTransactionType"].ToString() != "Deposit" && selectedRow["ImportTransactionType"].ToString() != "Withdrawal")
                    textComments.Text = selectedRow["ImportTransactionType"].ToString();

                C1ComboBoxBankAccountList.Visible = false;
                btnAdd.Visible = false;

                btnUpdate.Visible = true;
                lblBankDetails.Visible = true;

                ModalPopupExtenderEditTran.Show();
            }

            else if (e.CommandName.ToLower() == "select")
            {
                C1ComboBoxBankAccountList.Visible = true;
                btnAdd.Visible = true;

                btnUpdate.Visible = false;
                lblBankDetails.Visible = false;

                C1ComboBoxBankAccountList.DataSource = PresentationData.Tables[BankTransactionDS.BANKACCOUNTLIST];
                C1ComboBoxBankAccountList.DataBind();
                transactionDate.Date = DateTime.Today;
                C1ComboBoxImport.SelectedValue = string.Empty;
                C1ComboBoxImport.Text = string.Empty;
                C1ComboBoxCat.SelectedValue = string.Empty;
                C1ComboBoxCat.Text = string.Empty;
                C1ComboBoxSystem.SelectedValue = string.Empty;
                C1ComboBoxSystem.Text = string.Empty;
                lblTranID.Text = string.Empty;
                lblBankCID.Text = Guid.NewGuid().ToString();
                textComments.Text = string.Empty;
                lblBankDetails.Text = string.Empty;
                Amount.Value = 0;
                TotalAmount.Value = 0;
                Adjustment.Value = 0;

                ModalPopupExtenderEditTran.Show();
            }
        }

        protected void ImportTransactionTypeDDLSelectedChange(object sender, EventArgs e)
        {

        }

        protected void CategoryTransactionTypeDDLSelectedChange(object sender, EventArgs e)
        { }

        protected void TermDeposit_PageIndexChanging(object sender, C1GridViewPageEventArgs e)
        {
            _cid = Request.QueryString["ins"];
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(_cid));
            TermDepositDS ds = new TermDepositDS();
            clientData.GetData(ds);
            DataView summaryView = new DataView(ds.Tables[ds.TermDepositTable.TABLENAME]);
            summaryView.Sort = ds.TermDepositTable.ACCOUNTNAME + " DESC";
            TermDepositGrid.DataSource = summaryView;

            TermDepositGrid.PageIndex = e.NewPageIndex;
            TermDepositGrid.DataBind();

            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void SortTermDeposit(object sender, C1GridViewSortEventArgs e)
        {
            _cid = Request.QueryString["ins"];
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(_cid));
            TermDepositDS ds = new TermDepositDS();
            clientData.GetData(ds);

            DataView summaryView = new DataView(ds.Tables[ds.TermDepositTable.TABLENAME]);
            summaryView.Sort = ds.TermDepositTable.ACCOUNTNAME + " DESC";

            TermDepositGrid.DataSource = summaryView;
            TermDepositGrid.DataBind();

            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void FilterTermDeposit(object sender, C1GridViewFilterEventArgs e)
        {
            _cid = Request.QueryString["ins"];
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(_cid));
            TermDepositDS ds = new TermDepositDS();
            clientData.GetData(ds);

            e.Values[0] = ((string)e.Values[0]).Trim();

            DataView summaryView = new DataView(ds.Tables[ds.TermDepositTable.TABLENAME]);
            summaryView.Sort = ds.TermDepositTable.ACCOUNTNAME + " DESC";

            TermDepositGrid.DataSource = summaryView.ToTable();
            TermDepositGrid.DataBind();

            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }
    }
}