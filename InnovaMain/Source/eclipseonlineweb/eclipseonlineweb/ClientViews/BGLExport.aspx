﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ClientViews/ClientViewMaster.master"
    AutoEventWireup="true" CodeBehind="BGLExport.aspx.cs" Inherits="eclipseonlineweb.ClientViews.BGLExport" %>

<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnSave" AlternateText="Save Changes" ToolTip="Save Changes"
                                OnClick="btnSave_OnClick" runat="server" ImageUrl="~/images/Save-Icon.png" />
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <div id="MainView" style="background-color: white">
                <fieldset>
                    <legend title="BGL EXPORT">BGL EXPORT</legend>
                    <table>
                        <tr>
                            <td>
                                <span class="riLabel">
                    <b>BGL Account Code:</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtBGLAccountCode" runat="server" CssClass="Txt-Field"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtBGLAccountCode"
                                    ForeColor="Red" Text="BGL Account Code is required. Please input and save BGL Account Code"
                                    runat="server" />
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table>
                        <tr>
                            <td>
                                <span class="riLabel">Set Start Date:</span><br />
                                <telerik:RadDatePicker MinDate="1800/1/1" ID="startDate" DateInput-DateFormat="dd/MM/yyyy"
                                    Width="150px" runat="server">
                                </telerik:RadDatePicker>
                            </td>
                            <td>
                                <span class="riLabel">Set End Date:</span><br />
                                <telerik:RadDatePicker MinDate="1800/1/1" ID="endDate" DateInput-DateFormat="dd/MM/yyyy"
                                    Width="150px" runat="server">
                                </telerik:RadDatePicker>
                            </td>
                            <td>
                                <span class="riLabel">Click here to generate 'BGL Import File':</span><br /> 
                                <telerik:RadButton ID="btnTrans" Width="210px" runat="server" OnClick="ExportTransactions"
                                    Text="Export Holdings & Transactions">
                                </telerik:RadButton>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
