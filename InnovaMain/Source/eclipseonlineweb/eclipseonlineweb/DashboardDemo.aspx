﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DashboardDemo.aspx.cs" Inherits="eclipseonlineweb.DashboardDemo"
    MasterPageFile="~/Home.master" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1Chart" Assembly="C1.Web.Wijmo.Controls.4, Version=4.0.20121.56, Culture=neutral, PublicKeyToken=9b75583953471eea" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc2" TagName="Alert" Src="~/JDash/Dashlets/Alerts.ascx" %>
<%@ Register TagPrefix="uc2" TagName="Notification" Src="~/JDash/Dashlets/Notifications.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .RadGrid_Metro .rgHeader, .RadGrid_Metro .rgHeader a
        {
            font-size: 8pt !important;
        }
        .RadGrid_Metro .rgRow a, .RadGrid_Metro .rgAltRow a, .RadGrid_Metro tr.rgEditRow a, .RadGrid_Metro .rgFooter a, .RadGrid_Metro .rgEditForm a
        {
            font-size: 8pt !important;
        }
    </style>
    <script type="text/javascript">
        //Resetting menu index
        localStorage['MenuIndex'] = '0';
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Dashboard"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <fieldset style="width: 60%">
                <legend>Action Required! (Alerts) </legend>
               
                    <uc2:Alert ID="Alert1" runat="server" Width="800px" />
               
            </fieldset>
            <br />
            <fieldset style="width: 60%">
                <legend>Notifications</legend>
               
                    <uc2:Notification ID="Notification1" runat="server" Width="800px" />
               
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
