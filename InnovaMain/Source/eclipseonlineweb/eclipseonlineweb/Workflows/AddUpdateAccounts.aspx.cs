﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using Oritax.TaxSimp.Common;
using System.Collections.Generic;
using Oritax.TaxSimp.CM.Organization;
using eclipseonlineweb.Controls;
using OTD = Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.CM;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Calculation;

namespace eclipseonlineweb
{
    public partial class AddUpdateAccounts : UMABasePage
    {
        private readonly IndividualListDS _individualDS = new IndividualListDS();

        private DataView BusinessTitleDataView { get; set; }

        private void SaveAll()
        {
            if (txtAccountName.Text.Trim() != string.Empty)
            {
                cid = Request.QueryString["ins"];
                if (!string.IsNullOrEmpty(cid))
                {
                    UMABroker.SaveOverride = true;
                    var clientCID = new Guid(cid);
                    var clientBMC = UMABroker.GetBMCInstance(clientCID) as IOrganizationUnit;
                    if (clientBMC != null)
                    {
                        clientBMC.Name = txtAccountName.Text;

                        var clientUMAData = clientBMC.ClientEntity as IClientUMAData;

                        if (clientUMAData != null)
                        {
                            clientUMAData.Name = txtAccountName.Text;

                            SetInvestorStatus(clientUMAData);

                            //clientUMAData.ClientWorkflowEntity.ModelID = rbComboBoxInvestmentProgram.SelectedItem !=
                            //                                             null
                            //                                                 ? new Guid(
                            //                                                       rbComboBoxInvestmentProgram
                            //                                                           .SelectedValue)
                            //                                                 : Guid.Empty;

                            clientUMAData.AuthoritytoAct.EclipseOnline = chkAuthrotiyToActEclipseOnline.Checked;
                            clientUMAData.AuthoritytoAct.AustralianMoneyMarket = chkAuthrotiyToActAMM.Checked;

                            if (clientUMAData.ClientWorkflowEntity == null)
                                clientUMAData.ClientWorkflowEntity = new ClientWorkflowEntity();
                            
                            // Do it Yourself
                            clientUMAData.Servicetype.DO_IT_YOURSELF = chkUseDIY.Checked;
                            if (txtDIYAmount.Text != string.Empty) clientUMAData.Servicetype.DoItYourSelf.DIYInitialInvestment = decimal.Parse(txtDIYAmount.Text);
                            clientUMAData.Servicetype.DoItYourSelf.UseDIYBWA = rbDIYBWA.Checked;
                            clientUMAData.Servicetype.DoItYourSelf.UseDIYAMM = rdDIYAMM.Checked;
                            clientUMAData.Servicetype.DoItYourSelf.UseDIYFIIG = rbDIYFIGG.Checked;
                            clientUMAData.Servicetype.DoItYourSelf.UseDIYDesktopBroker = rdDIYASX.Checked;
                            
                            // Do it with me
                            clientUMAData.Servicetype.DO_IT_WITH_ME = chkUseDIWM.Checked;
                            if (txtDIWMAmount.Text != string.Empty) clientUMAData.Servicetype.DoItWithMe.DIWMInitialInvestment = decimal.Parse(txtDIWMAmount.Text);
                            clientUMAData.Servicetype.DoItWithMe.UseDIWMBWA = rdDIWMBWA.Checked;
                            clientUMAData.Servicetype.DoItWithMe.UseDIWMAMM = rdDIWMAMM.Checked;
                            clientUMAData.Servicetype.DoItWithMe.UseDIWMFIIG = rdDIWMFIIG.Checked;
                            clientUMAData.Servicetype.DoItWithMe.UseDIWMDesktopBroker = rdDIWMASX.Checked;
                            clientUMAData.Servicetype.DoItWithMe.UseDIWMSS = rbDIWMSS.Checked;

                            rdSelectedFunds.Items.Clear();
                            clientUMAData.Servicetype.DoItWithMe.PreferredFunds.Clear();
                            foreach (RadComboBoxItem item in rbPreferredFunds.Items)
                            {
                                CheckBox chk = (CheckBox)item.FindControl("chk1");
                                if (chk.Checked)
                                {
                                    rdSelectedFunds.Items.Add(new RadListBoxItem(item.Text, item.Value));
                                    clientUMAData.Servicetype.DoItWithMe.PreferredFunds.Add(new Guid(item.Value));
                                }
                            }

                            //Common Proc
                            SetMaqAccount(clientUMAData);
                            clientUMAData.BTWrap.AccountNumber = txtInvestorNumber.Text;
                            clientUMAData.BTWrap.AccountDescription = txtAdviserCode.Text;
                            if (clientUMAData.EclipseSuper == null) clientUMAData.EclipseSuper = new BTWrapEntity();
                            clientUMAData.EclipseSuper.AccountNumber = txtEclipseInvestorNoAccNo.Text;
                            clientUMAData.EclipseSuper.AccountDescription = txtEclipseAdviserNoCode.Text;

                            // Do it for me
                            clientUMAData.Servicetype.DO_IT_FOR_ME = chkDTFM.Checked;
                            if (txtDIFMAmount.Text != string.Empty) clientUMAData.Servicetype.DoItForMe.DIFMInitialInvestment = decimal.Parse(txtDIFMAmount.Text);
                            clientUMAData.Servicetype.DoItForMe.ProgramName = InvestmentProgramName;
                            clientUMAData.Servicetype.DoItForMe.ProgramCode = InvestmentProgramCode;
                            clientUMAData.Servicetype.DoItForMe.UseMinBalance = rbUseMinBalReBal.Checked;
                            clientUMAData.Servicetype.DoItForMe.UseMinBalanceCurr = rbMinBalRebalByCurrency.Checked;
                            clientUMAData.Servicetype.DoItForMe.UseMinBalancePercentage = rbMinBalRebalByPercentage.Checked;
                            clientUMAData.Servicetype.DoItForMe.UseMinTrade = rbUseMinTradeBal.Checked;
                            clientUMAData.Servicetype.DoItForMe.UseMinTradeCurr = rbUserMinTradeBalByCurrency.Checked;
                            clientUMAData.Servicetype.DoItForMe.UseMinTradePercentage = rbUserMinTradeBalByPercent.Checked;
                            clientUMAData.Servicetype.DoItForMe.SellTheHolding = rbSellTheHolding.Checked;
                            if (txtMinBalReBalCurrency.Text != string.Empty)
                                clientUMAData.Servicetype.DoItForMe.MinBalance = decimal.Parse(txtMinBalReBalCurrency.Text);
                            if (txtMinBalReBalPercent.Text != string.Empty)
                                clientUMAData.Servicetype.DoItForMe.MinBalancePercentage = decimal.Parse(txtMinBalReBalPercent.Text);
                            if (txtrbUserMinTradeBalByCurrency.Text != string.Empty)
                                clientUMAData.Servicetype.DoItForMe.MinTrade = decimal.Parse(txtrbUserMinTradeBalByCurrency.Text);
                            if (txtrbUserMinTradeBalByPercent.Text != string.Empty)
                                clientUMAData.Servicetype.DoItForMe.MinTradePercentage = decimal.Parse(txtrbUserMinTradeBalByPercent.Text);
                            clientUMAData.Servicetype.DoItForMe.AssignedAllocatedValueExclusion = rbHandlingInvestmentDueToExclusionAssign.Checked;
                            clientUMAData.Servicetype.DoItForMe.DistributedAllocatedValueExclusion = rbHandlingInvestmentDueToExclusionDistribute.Checked;
                            clientUMAData.Servicetype.DoItForMe.AssignedAllocatedValueHoldSell = rbHandlingInvestmentDueToSellRatingAssignValue.Checked;
                            clientUMAData.Servicetype.DoItForMe.DistributedAllocatedValueHoldSell = rbHandlingInvestmentDueToSellRatingDistributeValue.Checked;
                            clientUMAData.Servicetype.DoItForMe.AssignedAllocatedValueMinTradeHolding = rbHandlingInvestmentDueToMinConstraintAssign.Checked;
                            clientUMAData.Servicetype.DoItForMe.DistributedAllocatedValueMinTradeHolding = rbHandlingInvestmentDueToMinConstraintDistribute.Checked;

                            // TFN / ABN
                            //clientUMAData.ClientWorkflowEntity.Sig1TFN = TFNSig1.Text;
                            //clientUMAData.ClientWorkflowEntity.Sig2TFN = TFNSig2.Text;
                            //clientUMAData.ClientWorkflowEntity.Sig3TFN = TFNSig3.Text;
                            //clientUMAData.ClientWorkflowEntity.Sig4TFN = TFNSig4.Text;
                            clientUMAData.TFN = TFNTrustSuperFund.Text;
                            //clientUMAData.ClientWorkflowEntity.ACN = txtACN.Text;
                            clientUMAData.ABN = txtABN.Text;
                            clientUMAData.Country = CountryAbnForTaxWithHolding;

                            #region Checklist

                            SaveCheckList(clientUMAData);

                            #endregion
                        }

                        if (clientBMC.IsCorporateType())
                        {
                            var clientUMADataCorpTrust = clientBMC.ClientEntity as IClientUMADataCorpTrust;
                            if (clientUMADataCorpTrust != null)
                            {
                                clientUMADataCorpTrust.ACN = txtACN.Text;
                                clientUMADataCorpTrust.CorporateTrusteeName = txtTrustName.Text;
                                clientUMADataCorpTrust.Country = "Australia";

                                if (clientBMC.TypeName == "ClientSMSFIndividualTrustee" ||
                                    clientBMC.TypeName == "ClientSMSFCorporateTrustee")
                                    clientUMADataCorpTrust.IsSMSFTrust = true;
                                else if (clientBMC.TypeName == "ClientOtherTrustsIndividual" ||
                                         clientBMC.TypeName == "ClientOtherTrustsCorporate")
                                    clientUMADataCorpTrust.IsOtherTrust = true;
                            }
                        }
                        SaveFeeData(clientBMC);
                        clientBMC.CalculateToken(true);
                    }
                    UMABroker.SetComplete();
                    UMABroker.SetStart();
                    SecuritiesChessControl.Save();
                    SaveAddressDetails();
                    BWAAccessFacilitiesDIY.Save();
                    BWAAccessFacilitiesDIWM.Save();
                    SetFeePanels();
                }
            }
            else
                MessageBox.RadAlert("Please ensure Account Name is entered properly", 330, 180, "Error(s)", "", "");
        }

        public String CountryAbnForTaxWithHolding
        {
            get
            {
                return TFNABNCountryList.SelectedItem.Text;
            }
            set
            {
                if (value == null) value = string.Empty;
                var comboItem = TFNABNCountryList.Items.FirstOrDefault(item => item.Text.ToLower() == value.ToLower());
                if (comboItem != null)
                {
                    TFNABNCountryList.SelectedValue = comboItem.Value;
                }
                else
                {
                    TFNABNCountryList.Text = value;    
                }
            }
        }

        public String InvestmentProgramName
        {
            get
            {
                return rbComboBoxInvestmentProgram.SelectedItem.Text;
            }
            set
            {
                if (value == null) value = string.Empty;
                var comboItem = rbComboBoxInvestmentProgram.Items.FirstOrDefault(item => item.Text.ToLower() == value.ToLower());
                if (comboItem != null)
                {
                    rbComboBoxInvestmentProgram.SelectedValue = comboItem.Value;
                }
                else
                {
                    rbComboBoxInvestmentProgram.Text = value;
                }
            }
        }

        public String InvestmentProgramCode
        {
            get
            {
                return rbComboBoxInvestmentProgram.SelectedItem.Value;
            }
            set
            {
                rbComboBoxInvestmentProgram.SelectedValue = value;
            }
        }

        private void SaveCheckList(IClientUMAData clientUMAData)
        {
            clientUMAData.ClientWorkflowEntity.ProofID = chkProofID.Checked;
            clientUMAData.ClientWorkflowEntity.TrustDeed = chkTrustDeed.Checked;
            clientUMAData.ClientWorkflowEntity.LetterOfAccountant = chkLetterFrom.Checked;
            clientUMAData.ClientWorkflowEntity.BusinessNameReg = chkBusinessName.Checked;
            clientUMAData.ClientWorkflowEntity.IssueSponsorChessSponsor = chkIssueSponsorHolding.Checked;
            clientUMAData.ClientWorkflowEntity.IssueSponsorChessSponsorHoldsingStatements = chkIssuerSponsoredHoldingStatement.Checked;
            clientUMAData.ClientWorkflowEntity.OffMarketTransfer = chkMarketTransfer.Checked;
            clientUMAData.ClientWorkflowEntity.ChangeOfClient = chkChangeClientDetail.Checked;
            clientUMAData.ClientWorkflowEntity.BrokerToBroker = chkBrokerToBrokerTransfer.Checked;
            clientUMAData.ClientWorkflowEntity.ProofOfAddress = chkProofAddressPassPortProvided.Checked;
        }

        private void SaveAddressDetails()
        {
            AddressDetailsDS addressDetailsDSds = WorkFlowAddressDetailControl.SetData();
            SaveData(cid, addressDetailsDSds);
        }

        private void PopulateData(IOrganizationUnit clientBMC)
        {
            NotesControl.CID = cid;
            txtAccountName.Text = clientBMC.Name;
            var clientUMAData = clientBMC.ClientEntity as IClientUMAData;
            if (clientUMAData != null)
            {
                txtAccountName.Text = clientUMAData.Name;

                GetInvestorStatus(clientUMAData);

                chkAuthrotiyToActEclipseOnline.Checked = clientUMAData.AuthoritytoAct.EclipseOnline;
                chkAuthrotiyToActAMM.Checked = clientUMAData.AuthoritytoAct.AustralianMoneyMarket;

                if (clientUMAData.ClientWorkflowEntity == null)
                    clientUMAData.ClientWorkflowEntity = new ClientWorkflowEntity();

                if (!Page.IsPostBack)
                {

                    chkUseDIY.Checked = clientUMAData.Servicetype.DO_IT_YOURSELF;

                    rbDIYBWA.Checked = clientUMAData.Servicetype.DoItYourSelf.UseDIYBWA;
                    rdDIYAMM.Checked = clientUMAData.Servicetype.DoItYourSelf.UseDIYAMM;
                    rbDIYFIGG.Checked = clientUMAData.Servicetype.DoItYourSelf.UseDIYFIIG;
                    rdDIYASX.Checked = clientUMAData.Servicetype.DoItYourSelf.UseDIYDesktopBroker;
                    pnlDIYSettings.Visible = clientUMAData.Servicetype.DO_IT_YOURSELF;
                    txtDIYAmount.Text = clientUMAData.Servicetype.DoItYourSelf.DIYInitialInvestment.ToString();

                    chkUseDIWM.Checked = clientUMAData.Servicetype.DO_IT_WITH_ME;

                    rdDIWMBWA.Checked = clientUMAData.Servicetype.DoItWithMe.UseDIWMBWA;
                    rdDIWMAMM.Checked = clientUMAData.Servicetype.DoItWithMe.UseDIWMAMM;
                    rdDIWMFIIG.Checked = clientUMAData.Servicetype.DoItWithMe.UseDIWMFIIG;
                    rdDIWMASX.Checked = clientUMAData.Servicetype.DoItWithMe.UseDIWMDesktopBroker;
                    rbDIWMSS.Checked = clientUMAData.Servicetype.DoItWithMe.UseDIWMSS;
                    pnlDIWMSettings.Visible = clientUMAData.Servicetype.DO_IT_WITH_ME;
                    txtDIWMAmount.Text = clientUMAData.Servicetype.DoItWithMe.DIWMInitialInvestment.ToString();
                    GetMaqAccount(clientUMAData);

                    rbPreferredFunds.ClearCheckedItems();
                    rdSelectedFunds.Items.Clear();
                    if (clientUMAData.Servicetype.DoItWithMe.PreferredFunds != null)
                    {
                        string checkedText = string.Empty;
                        foreach (Guid fundID in clientUMAData.Servicetype.DoItWithMe.PreferredFunds)
                        {
                            var preferItem = rbPreferredFunds.Items.FirstOrDefault(c => c.Value == fundID.ToString());
                            if (preferItem != null)
                            {
                                CheckBox chk = (CheckBox)preferItem.FindControl("chk1");
                                chk.Checked = true;
                                rdSelectedFunds.Items.Add(new RadListBoxItem(preferItem.Text, preferItem.Value));
                                if (checkedText == string.Empty)
                                {
                                    checkedText += preferItem.Text;
                                }
                                else
                                {
                                    checkedText += "," + preferItem.Text;
                                }
                            }
                        }
                        rbPreferredFunds.Text = checkedText;
                    }

                    TFNTrustSuperFund.Text = clientUMAData.TFN;
                    var clientUmaDataCorpTrust = clientBMC.ClientEntity as IClientUMADataCorpTrust;
                    if (clientUmaDataCorpTrust != null)
                    {
                        trACN.Visible = true;
                        txtACN.Text = clientUmaDataCorpTrust.ACN;
                    }
                    else
                    {
                        trACN.Visible = false;
                        txtACN.Text = string.Empty;
                    }
                    txtABN.Text = clientUMAData.ABN;
                    CountryAbnForTaxWithHolding = clientUMAData.Country;

                    txtInvestorNumber.Text = clientUMAData.BTWrap.AccountNumber;
                    txtAdviserCode.Text = clientUMAData.BTWrap.AccountDescription;
                    if (clientUMAData.EclipseSuper == null) clientUMAData.EclipseSuper = new BTWrapEntity();
                    txtEclipseInvestorNoAccNo.Text = clientUMAData.EclipseSuper.AccountNumber;
                    txtEclipseAdviserNoCode.Text = clientUMAData.EclipseSuper.AccountDescription;
                }

                #region Checklist
                GetCheckList(clientUMAData);
                #endregion

            }

            if (!Page.IsPostBack)
            {
                chkDTFM.Checked = clientUMAData.Servicetype.DO_IT_FOR_ME;

                if (chkDTFM.Checked)
                    pnlDIFMSettings.Visible = true;

                if (clientUMAData != null)
                {
                    txtDIFMAmount.Text = clientUMAData.Servicetype.DoItForMe.DIFMInitialInvestment.ToString();
                    InvestmentProgramName = clientUMAData.Servicetype.DoItForMe.ProgramName;
                    rbSellTheHolding.Checked = clientUMAData.Servicetype.DoItForMe.SellTheHolding;
                    txtMinBalReBalCurrency.Text = clientUMAData.Servicetype.DoItForMe.MinBalance.ToString();
                    txtMinBalReBalPercent.Text = clientUMAData.Servicetype.DoItForMe.MinBalancePercentage.ToString();

                    txtrbUserMinTradeBalByCurrency.Text = clientUMAData.Servicetype.DoItForMe.MinTrade.ToString();
                    txtrbUserMinTradeBalByPercent.Text =
                        clientUMAData.Servicetype.DoItForMe.MinTradePercentage.ToString();

                    rbHandlingInvestmentDueToExclusionAssign.Checked =
                        clientUMAData.Servicetype.DoItForMe.AssignedAllocatedValueExclusion;
                    rbHandlingInvestmentDueToExclusionDistribute.Checked =
                        clientUMAData.Servicetype.DoItForMe.DistributedAllocatedValueExclusion;
                    rbHandlingInvestmentDueToSellRatingAssignValue.Checked =
                        clientUMAData.Servicetype.DoItForMe.AssignedAllocatedValueHoldSell;
                    rbHandlingInvestmentDueToSellRatingDistributeValue.Checked =
                        clientUMAData.Servicetype.DoItForMe.DistributedAllocatedValueHoldSell;
                    rbHandlingInvestmentDueToMinConstraintAssign.Checked =
                        clientUMAData.Servicetype.DoItForMe.AssignedAllocatedValueMinTradeHolding;
                    rbHandlingInvestmentDueToMinConstraintDistribute.Checked =
                        clientUMAData.Servicetype.DoItForMe.DistributedAllocatedValueMinTradeHolding;

                    rbUseMinBalReBal.Checked = clientUMAData.Servicetype.DoItForMe.UseMinBalance;
                    rbMinBalRebalByCurrency.Checked = clientUMAData.Servicetype.DoItForMe.UseMinBalanceCurr;
                    rbMinBalRebalByPercentage.Checked = clientUMAData.Servicetype.DoItForMe.UseMinBalancePercentage;

                    rbUseMinTradeBal.Checked = clientUMAData.Servicetype.DoItForMe.UseMinTrade;
                    rbUserMinTradeBalByCurrency.Checked = clientUMAData.Servicetype.DoItForMe.UseMinTradeCurr;
                    rbUserMinTradeBalByPercent.Checked = clientUMAData.Servicetype.DoItForMe.UseMinTradePercentage;
                }
            }

            if (!rbUseMinTradeBal.Checked)
            {
                rbUserMinTradeBalByCurrency.Visible = false;
                rbUserMinTradeBalByPercent.Visible = false;
                txtrbUserMinTradeBalByCurrency.Visible = false;
                txtrbUserMinTradeBalByPercent.Visible = false;
            }

            if (!rbUseMinBalReBal.Checked)
            {
                rbMinBalRebalByCurrency.Visible = false;
                rbMinBalRebalByPercentage.Visible = false;
                txtMinBalReBalPercent.Visible = false;
                txtMinBalReBalCurrency.Visible = false;
            }

            if (rbUserMinTradeBalByPercent.Checked)
            {
                txtrbUserMinTradeBalByCurrency.Visible = false;
                txtrbUserMinTradeBalByPercent.Visible = true;
            }

            if (rbMinBalRebalByPercentage.Checked)
            {
                txtMinBalReBalPercent.Visible = true;
                txtMinBalReBalCurrency.Visible = false;
            }

            if (rbUserMinTradeBalByCurrency.Checked)
            {
                txtrbUserMinTradeBalByCurrency.Visible = true;
                txtrbUserMinTradeBalByPercent.Visible = false;
            }

            if (rbMinBalRebalByCurrency.Checked)
            {
                txtMinBalReBalPercent.Visible = false;
                txtMinBalReBalCurrency.Visible = true;
            }

            if (clientBMC.IsCorporateType())
            {
                if (!Page.IsPostBack)
                {
                    var clientUmaDataCorpTrust = clientBMC.ClientEntity as IClientUMADataCorpTrust;
                    if (clientUmaDataCorpTrust != null) txtTrustName.Text = clientUmaDataCorpTrust.CorporateTrusteeName;
                }
            }

            var clientFeesDS = new ClientFeesDS();
            clientBMC.GetData(clientFeesDS);
            var addressDs = new AddressDetailsDS();
            clientBMC.GetData(addressDs);
            GetData(clientFeesDS);
            GetData(addressDs);
        }

        private void GetCheckList(IClientUMAData clientUMAData)
        {
            chkProofID.Checked = clientUMAData.ClientWorkflowEntity.ProofID;
            chkTrustDeed.Checked = clientUMAData.ClientWorkflowEntity.TrustDeed;
            chkLetterFrom.Checked = clientUMAData.ClientWorkflowEntity.LetterOfAccountant;
            chkBusinessName.Checked = clientUMAData.ClientWorkflowEntity.BusinessNameReg;
            chkIssueSponsorHolding.Checked = clientUMAData.ClientWorkflowEntity.IssueSponsorChessSponsor;
            chkIssuerSponsoredHoldingStatement.Checked =
                clientUMAData.ClientWorkflowEntity.IssueSponsorChessSponsorHoldsingStatements;
            chkMarketTransfer.Checked = clientUMAData.ClientWorkflowEntity.OffMarketTransfer;
            chkChangeClientDetail.Checked = clientUMAData.ClientWorkflowEntity.ChangeOfClient;
            chkBrokerToBrokerTransfer.Checked = clientUMAData.ClientWorkflowEntity.BrokerToBroker;
            chkProofAddressPassPortProvided.Checked = clientUMAData.ClientWorkflowEntity.ProofOfAddress;
        }

        private void SetMaqAccount(IClientUMAData clientUMAData)
        {
            clientUMAData.Servicetype.DoItYourSelf.UseDIYMAQ = rdDIYMAQ.Checked;
            if (rdDIYMAQ.Checked)
            {
                clientUMAData.Servicetype.DoItYourSelf.DIYMaqACCName = txtMaqAccountName.Text;
                clientUMAData.Servicetype.DoItYourSelf.DIYMaqACCNo = txtMaqAccountNo.Text;
                clientUMAData.Servicetype.DoItYourSelf.DIYMaqBSBNo = txtMaqBSBNo.Text;
            }
            else
            {
                clientUMAData.Servicetype.DoItYourSelf.DIYMaqACCName = String.Empty;
                clientUMAData.Servicetype.DoItYourSelf.DIYMaqACCNo = String.Empty;
                clientUMAData.Servicetype.DoItYourSelf.DIYMaqBSBNo = String.Empty;
            }

            clientUMAData.Servicetype.DoItWithMe.UseDIWMMAQ = rbUseDIWMMaq.Checked;
            if (rbUseDIWMMaq.Checked)
            {
                clientUMAData.Servicetype.DoItWithMe.DIWMMaqACCName = txtMaqAccountNameDIWM.Text;
                clientUMAData.Servicetype.DoItWithMe.DIWMMaqACCNo = txtMaqAccountNoDIWM.Text;
                clientUMAData.Servicetype.DoItWithMe.DIWMMaqBSBNo = txtMaqBSBNoDIWM.Text;
            }
            else
            {
                clientUMAData.Servicetype.DoItWithMe.DIWMMaqACCName = String.Empty;
                clientUMAData.Servicetype.DoItWithMe.DIWMMaqACCNo = String.Empty;
                clientUMAData.Servicetype.DoItWithMe.DIWMMaqBSBNo = String.Empty;
            }
        }

        private void GetMaqAccount(IClientUMAData clientUMAData)
        {
            rdDIYMAQ.Checked = clientUMAData.Servicetype.DoItYourSelf.UseDIYMAQ;
            if (rdDIYMAQ.Checked)
            {
                txtMaqAccountName.Text = clientUMAData.Servicetype.DoItYourSelf.DIYMaqACCName;
                txtMaqAccountNo.Text = clientUMAData.Servicetype.DoItYourSelf.DIYMaqACCNo;
                txtMaqBSBNo.Text = clientUMAData.Servicetype.DoItYourSelf.DIYMaqBSBNo;
                pnlMacquarie.Visible = true;
            }
            else
            {
                txtMaqAccountName.Text = String.Empty;
                txtMaqAccountNo.Text = String.Empty;
                txtMaqBSBNo.Text = String.Empty;
                pnlMacquarie.Visible = false;
            }

            rbUseDIWMMaq.Checked = clientUMAData.Servicetype.DoItWithMe.UseDIWMMAQ;
            if (rbUseDIWMMaq.Checked)
            {
                txtMaqAccountNameDIWM.Text = clientUMAData.Servicetype.DoItWithMe.DIWMMaqACCName;
                txtMaqAccountNoDIWM.Text = clientUMAData.Servicetype.DoItWithMe.DIWMMaqACCNo;
                txtMaqBSBNoDIWM.Text = clientUMAData.Servicetype.DoItWithMe.DIWMMaqBSBNo;
                pnlMacquarieDIWM.Visible = true;
            }
            else
            {
                txtMaqAccountNameDIWM.Text = String.Empty;
                txtMaqAccountNoDIWM.Text = String.Empty;
                txtMaqBSBNoDIWM.Text = String.Empty;
                pnlMacquarieDIWM.Visible = false;
            }
        }

        private void SetInvestorStatus(IClientUMAData clientUMAData)
        {
            clientUMAData.AuthoritytoAct.InvestorStatusWholesaleInvestor = rbWholesaleInvestor.Checked;
            clientUMAData.AuthoritytoAct.InvestorStatusSophisticatedInvestor = rbSophisticatedInvestor.Checked;
            clientUMAData.AuthoritytoAct.InvestorStatusProfessionalInvestor = rbProfessionalInvestor.Checked;
            clientUMAData.AuthoritytoAct.InvestorStatusRetailInvestor = rbRetailInvestor.Checked;
            clientUMAData.AuthoritytoAct.CertificateFromAccountantIsAttached = chkCertificateFromAccountant.Checked;
        }

        private void GetInvestorStatus(IClientUMAData clientUMAData)
        {
            if (clientUMAData.AuthoritytoAct != null)
            {
                rbWholesaleInvestor.Checked = clientUMAData.AuthoritytoAct.InvestorStatusWholesaleInvestor;
                rbSophisticatedInvestor.Checked = clientUMAData.AuthoritytoAct.InvestorStatusSophisticatedInvestor;
                rbProfessionalInvestor.Checked = clientUMAData.AuthoritytoAct.InvestorStatusProfessionalInvestor;
                rbRetailInvestor.Checked = clientUMAData.AuthoritytoAct.InvestorStatusRetailInvestor;
                chkCertificateFromAccountant.Checked = clientUMAData.AuthoritytoAct.CertificateFromAccountantIsAttached;
            }
        }

        private void FillValidationMessages()
        {
            var clientCid = new Guid(cid);
            var clientData = UMABroker.GetBMCInstance(clientCid) as IOrganizationUnit;
            var clientValidationDS = new ClientValidationDS();
            if (clientData != null) clientData.GetData(clientValidationDS);
            var view = new DataView(clientValidationDS.Tables[ClientValidationDS.ACCOUNTVALIDATIONTABLE])
                {
                    Sort = ClientValidationDS.VALIDATIONMESSAGETYPE + " ASC"
                };
            ValidationsInformationGrid.DataSource = view.ToTable();
        }

        private void FillContactGrid()
        {
            var dt = new DataTable();
            dt.Columns.Add("CID");
            dt.Columns.Add("CLID");
            dt.Columns.Add("CSID");
            dt.Columns.Add("FirstName");
            dt.Columns.Add("MiddleName");
            dt.Columns.Add("LastName");
            dt.Columns.Add("EmailAddress");
            dt.Columns.Add("BusinessTitle");
            dt.Columns.Add("Share");
            var clientCid = new Guid(cid);
            var clientData = UMABroker.GetBMCInstance(clientCid) as OrganizationUnitCM;
            if (clientData != null)
            {
                hfCLID.Value = clientData.Clid.ToString();
                hfCSID.Value = clientData.CSID.ToString();
                ContactControl.CLID = new Guid(hfCLID.Value);
                ContactControl.CSID = new Guid(hfCSID.Value);
                foreach (Oritax.TaxSimp.Common.IdentityCMDetail identityCM in clientData.SignatoriesApplicants)
                {
                    var indvCM = UMABroker.GetCMImplementation(identityCM.Clid, identityCM.Csid) as IndividualCM;
                    if (indvCM != null)
                    {
                        var ind = indvCM.ClientEntity as IndividualEntity;

                        DataRow row = dt.NewRow();
                        row["CID"] = indvCM.CID;
                        row["CLID"] = identityCM.Clid;
                        row["CSID"] = identityCM.Csid;
                        if (identityCM.BusinessTitle != null)
                            row["BusinessTitle"] = identityCM.BusinessTitle.Title;
                        if (ind != null)
                        {
                            row["FirstName"] = ind.Name;
                            row["MiddleName"] = ind.MiddleName;
                            row["LastName"] = ind.Surname;
                            row["EmailAddress"] = ind.EmailAddress;

                            if (ind.ClientShares != null)
                            {
                                var individualShares =
                                    ind.ClientShares.FirstOrDefault(clientshare => clientshare.ClientCid == clientCid);
                                if (individualShares != null)
                                    row["Share"] = individualShares.Share;
                            }
                        }


                        dt.Rows.Add(row);

                    }
                }
            }
            var dv = new DataView(dt) { Sort = dt.Columns["FirstName"] + " ASC" };
            PresentationGrid.DataSource = dv;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        private void HideShowContactAssociationGrid(bool show)
        {
            OVER.Visible = ContactModal.Visible = show;
        }

        protected void LnkbtnAssociateContactsClick(object sender, EventArgs e)
        {
            HideShowContactAssociationGrid(true);
        }

        protected void LnkbtnAddBankAccClick(object sender, ImageClickEventArgs e)
        {
            var gCid = Guid.Empty;
            IndividualControl.SetEntity(gCid);
            HideShowIndividualGrid(true);
        }

        private void HideShowIndividualGrid(bool show)
        {
            OVER.Visible = IndividualModal.Visible = show;
        }

        protected void Button1_OnClick(object sender, ImageClickEventArgs e)
        {
            ViewPanel.Visible = false;
        }

        protected void DdlBusinessTitle_OnSelectedIndexChanged(object sender, EventArgs e)
        {

            var ddl = (sender as RadComboBox);
            if (ddl != null && ddl.SelectedValue != null)
                SaveContacts(Guid.Parse(ddl.Attributes["CID"]), Guid.Parse(ddl.Attributes["CSID"]),
                             Guid.Parse(ddl.Attributes["CLID"]), ddl.SelectedValue);
        }

        protected void txtShare_OnTextChanged(object sender, EventArgs e)
        {
            var txt = sender as RadInputControl;
            if (txt != null)
            {
                SaveShare(Guid.Parse(txt.Attributes["CID"]), Guid.Parse(txt.Attributes["CLID"]),
                          Guid.Parse(txt.Attributes["CSID"]), txt.Text, false);
            }
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(cid) && cid != Guid.Empty.ToString())
                FillContactGrid();
        }

        protected void ValidationsInformationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(cid) && cid != Guid.Empty.ToString())
                FillValidationMessages();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            switch (e.CommandName.ToLower())
            {
                case "detail":
                    var sigCid = new Guid(dataItem["CID"].Text);
                    IndividualControlView.SetEntity(sigCid);
                    ViewPanel.Visible = true;
                    break;
                case "edit":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var sigCid1 = new Guid(dataItem["CID"].Text);
                    IndividualControl.SetEntity(sigCid1);
                    HideShowIndividualGrid(true);
                    break;
                case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    UMABroker.SaveOverride = true;
                    var linkEntiyCID = new Guid(dataItem["CID"].Text);
                    var linkEntiyCLID = new Guid(dataItem["CLID"].Text);
                    var linkEntiyCSID = new Guid(dataItem["CSID"].Text);
                    var clientData = UMABroker.GetBMCInstance(new Guid(cid)) as OrganizationUnitCM;
                    if (clientData != null)
                    {
                        clientData.SignatoriesApplicants.RemoveAll(
                            associate => associate.Clid == linkEntiyCLID && associate.Csid == linkEntiyCSID);
                    }
                    if (clientData != null) clientData.CalculateToken(true);
                    UMABroker.SetComplete();
                    UMABroker.SetStart();

                    SaveShare(linkEntiyCID, linkEntiyCLID, linkEntiyCSID, "0", true);

                    PresentationGrid.Rebind();
                    break;
            }
        }

        protected void PresentationGrid_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            var datarow = ((DataRowView)(dataItem.DataItem)).Row;
            var txt = ((RadTextBox)(dataItem["Share"].Controls[1]));
            txt.Text = datarow["Share"].ToString();
            txt.Attributes.Add("CID", dataItem["CID"].Text);
            txt.Attributes.Add("CLID", dataItem["CLID"].Text);
            txt.Attributes.Add("CSID", dataItem["CSID"].Text);
        }

        public void ReFillUserControlPresentationData()
        {
            if (!string.IsNullOrEmpty(cid))
            {
                var clientCID = new Guid(cid);
                var clientBMC = UMABroker.GetBMCInstance(clientCID) as IOrganizationUnit;
                var noteListDS = new NoteListDS();
                if (clientBMC != null)
                {
                    clientBMC.GetData(noteListDS);
                    LoadData(clientBMC);
                }
                btnSave.Visible = true;
                NotesControl.CID = cid;
                NotesControl.PresentationData = noteListDS;
                NotesControl.User = UMABroker.UserContext;
            }
        }

        public override void LoadPage()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            cid = Request.QueryString["ins"];
            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                btnSave.Visible = false;
                btnNewContact.Visible = false;
                PresentationGrid.Columns[PresentationGrid.Columns.Count - 1].Visible = false;
                PresentationGrid.Columns[PresentationGrid.Columns.Count - 2].Visible = false;
                PresentationGrid.Columns[PresentationGrid.Columns.Count - 4].Visible = false;
                PresentationGrid.Columns[PresentationGrid.Columns.Count - 5].Visible = false;
            }

            if (!Page.IsPostBack)
            {
                var orgCM = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                AttachmentControl.User = UMABroker.UserContext;
                var ifaDs = new IFADS
                    {
                        CommandType = DatasetCommandTypes.Get,
                        Command = (int)WebCommands.GetOrganizationUnitsByType,
                        Unit = new Oritax.TaxSimp.Data.OrganizationUnit
                            {
                                CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                                Type = ((int)OrganizationType.IFA).ToString(),
                            }
                    };

                orgCM.GetData(ifaDs);
                ddlAdviserList.DataSource = ifaDs.ifaTable;
                ddlAdviserList.DataValueField = ifaDs.ifaTable.CID;
                ddlAdviserList.DataTextField = ifaDs.ifaTable.TRADINGNAME;
                ddlAdviserList.DataBind();
                var item = new RadComboBoxItem("", "");
                ddlAdviserList.Items.Add(item);
                item.Selected = true;

                var filterdFundSecList = orgCM.Securities.Where(sec => sec.AsxCode.StartsWith("IV0"));
                foreach (var secItem in filterdFundSecList)
                {
                    secItem.CompanyName = secItem.CompanyName;
                }

                rbPreferredFunds.DataSource = filterdFundSecList;
                rbPreferredFunds.DataTextField = "CompanyName";
                rbPreferredFunds.DataValueField = "ID";
                rbPreferredFunds.DataBind();

                UMABroker.ReleaseBrokerManagedComponent(orgCM);

                rbComboBoxInvestmentProgram.DataSource = orgCM.Model.Where(c => c.ServiceType == ServiceTypes.DoItForMe);
                rbComboBoxInvestmentProgram.DataTextField = "Name";
                //rbComboBoxInvestmentProgram.DataValueField = "ID";
                rbComboBoxInvestmentProgram.DataValueField = "ProgramCode";
                rbComboBoxInvestmentProgram.DataBind();


                if (!string.IsNullOrEmpty(cid))
                {
                    var clientCID = new Guid(cid);
                    var clientBMC = UMABroker.GetBMCInstance(clientCID) as IOrganizationUnit;
                    var clientDetailsDS = new ClientDetailsDS();
                    var attachmentListDS = new AttachmentListDS();
                    var noteListDS = new NoteListDS();
                    if (clientBMC != null)
                    {
                        clientBMC.GetData(attachmentListDS);
                        clientBMC.GetData(clientDetailsDS);
                        clientBMC.GetData(_individualDS);
                        clientBMC.GetData(noteListDS);
                        LoadData(clientBMC);
                    }
                    btnSave.Visible = true;
                    NotesControl.CID = cid;
                    NotesControl.PresentationData = noteListDS;
                    NotesControl.User = UMABroker.UserContext;
                    AttachmentControl.CID = cid;
                    AttachmentControl.User = UMABroker.UserContext;
                    AttachmentControl.PresentationData = attachmentListDS;
                }
                else
                {
                    foreach (RadTab tab in ClientAddUpdateWorkflowTab.Tabs)
                    {
                        if (tab.Index != 0)
                            tab.Enabled = false;
                    }
                    AttachmentControl.PresentationData = new DataSet();
                    NotesControl.CID = cid;
                    NotesControl.PresentationData = new DataSet();
                    btnNewSave.Visible = true;
                }
            }

            if (!string.IsNullOrEmpty(hfCLID.Value))
            {
                ContactControl.CLID = new Guid(hfCLID.Value);
                ContactControl.CSID = new Guid(hfCSID.Value);
            }

            ((HiddenField)Master.FindControl("hfMainMenuText")).Value = "WORKFLOW";
        }

        protected void Timer_Save(object sender, EventArgs e)
        {
            SaveAll();
        }

        protected void Button_Click(object sender, EventArgs e)
        {
        }

        protected void SaveFeeData(IOrganizationUnit clientBMC)
        {
            var feeDS = new ClientFeesDS { DataSetOperationType = DataSetOperationType.UpdateSingle };
            SetData(feeDS);
            clientBMC.SetData(feeDS);
        }

        protected void chkUseDIY_OnCheckedChanged(object sender, EventArgs e)
        {
            if (chkUseDIY.Checked)
                pnlDIYSettings.Visible = true;
            else
            {
                pnlDIYSettings.Visible = false;
                txtDIYAmount.Text = string.Empty;
            }
        }

        protected void btnSelectInvestmentProgram_Click(Object sender, EventArgs e)
        {
            SaveAll();
        }

        protected void btnSelectPreferredFundDIWM_Click(Object sender, EventArgs e)
        {
            rdSelectedFunds.Items.Clear();
            string checkedText = string.Empty;
            foreach (RadComboBoxItem item in rbPreferredFunds.Items)
            {
                CheckBox chk = (CheckBox)item.FindControl("chk1");
                if (chk.Checked) rdSelectedFunds.Items.Add(new RadListBoxItem(item.Text, item.Value));
            }
        }

        protected void rbUseMinBalReBal_Clicked(Object sender, EventArgs e)
        {
            if (rbUseMinBalReBal.Checked)
            {
                rbMinBalRebalByCurrency.Visible = true;
                rbMinBalRebalByPercentage.Visible = true;
                txtMinBalReBalCurrency.Visible = true;
                txtMinBalReBalCurrency.Text = "2500";
                txtMinBalReBalPercent.Text = "00.00";
            }
            else
            {
                rbMinBalRebalByCurrency.Visible = false;
                rbMinBalRebalByPercentage.Visible = false;
                txtMinBalReBalCurrency.Visible = false;
                txtMinBalReBalCurrency.Text = "2500";
                txtMinBalReBalPercent.Visible = false;
                txtMinBalReBalPercent.Text = "00.00";
            }
        }

        protected void rbMinBalRebalByCurrency_Clicked(Object sender, EventArgs e)
        {
            if (rbMinBalRebalByCurrency.Checked)
            {
                txtMinBalReBalCurrency.Visible = true;
                txtMinBalReBalCurrency.Text = "2500";
                txtMinBalReBalPercent.Visible = false;
                txtMinBalReBalPercent.Text = "00.00";
            }
        }

        protected void rbMinBalRebalByPercent_Clicked(Object sender, EventArgs e)
        {
            if (rbMinBalRebalByPercentage.Checked)
            {
                txtMinBalReBalPercent.Visible = true;
                txtMinBalReBalPercent.Text = "00.00";
                txtMinBalReBalCurrency.Visible = false;
                txtMinBalReBalCurrency.Text = "2500";
            }
        }

        protected void rbUserMinTradeBal_Clicked(Object sender, EventArgs e)
        {
            if (rbUseMinTradeBal.Checked)
            {
                rbUserMinTradeBalByCurrency.Visible = true;
                rbUserMinTradeBalByPercent.Visible = true;
                txtrbUserMinTradeBalByCurrency.Visible = true;
                txtrbUserMinTradeBalByCurrency.Text = "2500";
                txtrbUserMinTradeBalByPercent.Text = "00.00";
            }
            else
            {
                rbUserMinTradeBalByCurrency.Visible = false;
                rbUserMinTradeBalByPercent.Visible = false;
                txtrbUserMinTradeBalByCurrency.Visible = false;
                txtrbUserMinTradeBalByCurrency.Text = "2500";
                txtrbUserMinTradeBalByPercent.Visible = false;
                txtrbUserMinTradeBalByPercent.Text = "00.00";
            }
        }

        protected void rbUserMinTradeBalByCurrency_Clicked(Object sender, EventArgs e)
        {
            if (rbUserMinTradeBalByCurrency.Checked)
            {
                txtrbUserMinTradeBalByCurrency.Visible = true;
                txtrbUserMinTradeBalByCurrency.Text = "2500";
                txtrbUserMinTradeBalByPercent.Visible = false;
                txtrbUserMinTradeBalByPercent.Text = "00.00";
            }
        }

        protected void rbUserMinTradeBalByPercent_Clicked(Object sender, EventArgs e)
        {
            if (rbUserMinTradeBalByPercent.Checked)
            {
                txtrbUserMinTradeBalByPercent.Visible = true;
                txtrbUserMinTradeBalByPercent.Text = "00.00";
                txtrbUserMinTradeBalByCurrency.Visible = false;
                txtrbUserMinTradeBalByCurrency.Text = "2500";
            }
        }

        protected void chkUseDIYWM_OnCheckedChanged(object sender, EventArgs e)
        {
            if (chkUseDIWM.Checked)
                pnlDIWMSettings.Visible = true;
            else
            {
                pnlDIWMSettings.Visible = false;
                txtDIWMAmount.Text = string.Empty;
            }
        }

        protected void rbDIYBWA_click(Object sender, EventArgs e)
        {
            if (rbDIYBWA.Checked)
            {
                rdDIYMAQ.Checked = false;
                pnlMacquarie.Visible = false;
                txtMaqAccountNo.Text = string.Empty;
                txtMaqBSBNo.Text = string.Empty;
                txtMaqAccountName.Text = string.Empty;
            }
        }

        protected void rdDIWMBWA_click(Object sender, EventArgs e)
        {
            if (rdDIWMBWA.Checked)
            {
                rbUseDIWMMaq.Checked = false;
                pnlMacquarieDIWM.Visible = false;
                txtMaqAccountNoDIWM.Text = string.Empty;
                txtMaqBSBNoDIWM.Text = string.Empty;
                txtMaqAccountNameDIWM.Text = string.Empty;
            }
        }

        protected void rdDIYMAQ_click(Object sender, EventArgs e)
        {
            if (rdDIYMAQ.Checked)
            {
                rbDIYBWA.Checked = false;
                pnlMacquarie.Visible = true;
            }
            else
            {
                pnlMacquarie.Visible = false;
                txtMaqAccountNo.Text = string.Empty;
                txtMaqBSBNo.Text = string.Empty;
                txtMaqAccountName.Text = string.Empty;
            }
        }

        protected void rbUseDIWMMaq_click(Object sender, EventArgs e)
        {
            if (rbUseDIWMMaq.Checked)
            {
                rdDIWMBWA.Checked = false;
                pnlMacquarieDIWM.Visible = true;
            }
            else
            {
                pnlMacquarieDIWM.Visible = false;
                txtMaqAccountNoDIWM.Text = string.Empty;
                txtMaqBSBNoDIWM.Text = string.Empty;
                txtMaqAccountNameDIWM.Text = string.Empty;
            }
        }

        protected void btnSelectPreferredFunds_Click(Object sender, EventArgs e)
        {
        }

        protected void rbDIWMSS_Click(Object sender, EventArgs e)
        {
            if (rbDIWMSS.Checked)
            {
                rbPreferredFunds.Visible = true;
                rdSelectedFunds.Visible = true;
                btnSelectPreferredFundDIWM.Visible = true;

            }
            else
            {
                rdSelectedFunds.Items.Clear();
                rdSelectedFunds.Visible = false;
                btnSelectPreferredFundDIWM.Visible = false;
                rbPreferredFunds.ClearCheckedItems();
                rbPreferredFunds.Visible = false;
            }
        }

        protected void InvestorStatus_Clicked(Object sender, EventArgs e)
        {
            if (rbWholesaleInvestor.Checked || rbSophisticatedInvestor.Checked)
            {
                rbDIWMSS.Visible = true;
                rbPreferredFunds.Visible = true;
                rdSelectedFunds.Visible = true;
            }
            else
            {
                rbDIWMSS.Visible = false;
                rbPreferredFunds.ClearCheckedItems();
                rbPreferredFunds.Visible = false;
                rdSelectedFunds.Visible = false;
                rdSelectedFunds.Items.Clear();
            }
        }

        protected void RadioCheckChanged_Clicked(Object sender, EventArgs e)
        {
            btnSelected.Value = ((RadButton)sender).ID;
            SetFeePanels();
        }

        private void GetData(ClientFeesDS clientFeesDS)
        {
            ClearOnoigngValueControls();
            ClearOnoingPercentControls();
            ClearTieredControls();

            UpFrontFee.Text = clientFeesDS.ClientFeeEntity.UpFrontFee.ToString();

            if (clientFeesDS.ClientFeeEntity.OngoingFeePercent)
            {
                OngoingFeePercent.Text = clientFeesDS.ClientFeeEntity.OngoingFeePercentage.ToString("N2");
                btnOngoingFeePercent.Checked = true;
                pnlOngoingFeePercent.Visible = true;
                btnSelected.Value = btnOngoingFeePercent.ID;
            }

            if (clientFeesDS.ClientFeeEntity.OnGoingFeeValue)
            {
                OngoingFeeVal.Text = clientFeesDS.ClientFeeEntity.OngoingFeeDollar.ToString("N2");
                pnlOngoingFeeVal.Visible = true;
                btnOngoingFeeValue.Checked = true;
                btnSelected.Value = btnOngoingFeeValue.ID;
            }

            if (clientFeesDS.ClientFeeEntity.OngoingTierFee)
            {
                To1.Text = clientFeesDS.ClientFeeEntity.Tier1To.ToString("N2");
                From1.Text = clientFeesDS.ClientFeeEntity.Tier1From.ToString("N2");
                Equal1.Text = clientFeesDS.ClientFeeEntity.Tier1Percentage.ToString("N2");

                To2.Text = clientFeesDS.ClientFeeEntity.Tier2To.ToString("N2");
                From2.Text = clientFeesDS.ClientFeeEntity.Tier2From.ToString("N2");
                Equal2.Text = clientFeesDS.ClientFeeEntity.Tier2Percentage.ToString("N2");

                To3.Text = clientFeesDS.ClientFeeEntity.Tier3To.ToString("N2");
                From3.Text = clientFeesDS.ClientFeeEntity.Tier3From.ToString("N2");
                Equal3.Text = clientFeesDS.ClientFeeEntity.Tier3Percentage.ToString("N2");

                To4.Text = clientFeesDS.ClientFeeEntity.Tier4To.ToString("N2");
                From4.Text = clientFeesDS.ClientFeeEntity.Tier4From.ToString("N2");
                Equal4.Text = clientFeesDS.ClientFeeEntity.Tier4Percentage.ToString("N2");

                To5.Text = clientFeesDS.ClientFeeEntity.Tier5To.ToString("N2");
                From5.Text = clientFeesDS.ClientFeeEntity.Tier5From.ToString("N2");
                Equal5.Text = clientFeesDS.ClientFeeEntity.Tier5Percentage.ToString("N2");

                pnlOngoingFeeTiered.Visible = true;
                btnOngoingFeeTered.Checked = true;
                btnSelected.Value = btnOngoingFeeTered.ID;

            }
        }

        private void SetData(ClientFeesDS clientFeesDS)
        {
            clientFeesDS.ClientFeeEntity.UpFrontFee = Convert.ToDecimal(GetDouble(UpFrontFee));
            clientFeesDS.ClientFeeEntity.UpFrontFeePaid = false;
            if (btnSelected.Value == btnOngoingFeePercent.ID)
            {
                clientFeesDS.ClientFeeEntity.OngoingFeePercentage = GetDouble(OngoingFeePercent);
                clientFeesDS.ClientFeeEntity.OngoingFeePercent = true;
            }

            else if (btnSelected.Value == btnOngoingFeeValue.ID)
            {
                clientFeesDS.ClientFeeEntity.OngoingFeeDollar = GetDouble(OngoingFeeVal);
                clientFeesDS.ClientFeeEntity.OnGoingFeeValue = true;
            }

            else if (btnSelected.Value == btnOngoingFeeTered.ID)
            {
                clientFeesDS.ClientFeeEntity.OngoingTierFee = true;
                clientFeesDS.ClientFeeEntity.Tier1To = GetDecimal(To1);
                clientFeesDS.ClientFeeEntity.Tier1From = GetDecimal(From1);
                clientFeesDS.ClientFeeEntity.Tier1Percentage = GetDecimal(Equal1);

                clientFeesDS.ClientFeeEntity.Tier2To = GetDecimal(To2);
                clientFeesDS.ClientFeeEntity.Tier2From = GetDecimal(From2);
                clientFeesDS.ClientFeeEntity.Tier2Percentage = GetDecimal(Equal2);

                clientFeesDS.ClientFeeEntity.Tier3To = GetDecimal(To3);
                clientFeesDS.ClientFeeEntity.Tier3From = GetDecimal(From3);
                clientFeesDS.ClientFeeEntity.Tier3Percentage = GetDecimal(Equal3);

                clientFeesDS.ClientFeeEntity.Tier4To = GetDecimal(To4);
                clientFeesDS.ClientFeeEntity.Tier4From = GetDecimal(From4);
                clientFeesDS.ClientFeeEntity.Tier4Percentage = GetDecimal(Equal4);

                clientFeesDS.ClientFeeEntity.Tier5To = GetDecimal(To5);
                clientFeesDS.ClientFeeEntity.Tier5From = GetDecimal(From5);
                clientFeesDS.ClientFeeEntity.Tier5Percentage = GetDecimal(Equal5);
            }
        }

        private void GetData(AddressDetailsDS addressDetailsDS)
        {
            WorkFlowAddressDetailControl.FillAddressControls(addressDetailsDS);
        }

        protected decimal GetDecimal(RadNumericTextBox tb)
        {
            if (string.IsNullOrWhiteSpace(tb.Text))
                return 0;
            return Decimal.Parse(tb.Text);
        }

        protected double GetDouble(RadNumericTextBox tb)
        {
            if (string.IsNullOrWhiteSpace(tb.Text))
                return 0;
            return Double.Parse(tb.Text);
        }

        private void SetFeePanels()
        {
            if (btnOngoingFeePercent.Checked)
            {
                pnlOngoingFeeVal.Visible = false;
                pnlOngoingFeePercent.Visible = true;
                pnlOngoingFeeTiered.Visible = false;

                ClearOnoigngValueControls();
                ClearTieredControls();
                btnSelected.Value = btnOngoingFeePercent.ID;
                btnOngoingFeePercent.Checked = true;
            }
            else if (btnOngoingFeeValue.Checked)
            {
                pnlOngoingFeeVal.Visible = true;
                pnlOngoingFeePercent.Visible = false;
                pnlOngoingFeeTiered.Visible = false;
                ClearOnoingPercentControls();
                ClearTieredControls();
                btnSelected.Value = btnOngoingFeeValue.ID;
                btnOngoingFeeValue.Checked = true;
            }

            else if (btnOngoingFeeTered.Checked)
            {
                pnlOngoingFeeVal.Visible = false;
                pnlOngoingFeePercent.Visible = false;
                pnlOngoingFeeTiered.Visible = true;

                ClearOnoigngValueControls();
                ClearOnoingPercentControls();
                btnSelected.Value = btnOngoingFeeTered.ID;
                btnOngoingFeeTered.Checked = true;
            }
            else
            {
                pnlOngoingFeeVal.Visible = false;
                pnlOngoingFeePercent.Visible = false;
                pnlOngoingFeeTiered.Visible = false;

                ClearOnoigngValueControls();
                ClearOnoingPercentControls();
                ClearTieredControls();
            }
        }

        private void ClearOnoingPercentControls()
        {
            OngoingFeePercent.Text = "0";
        }

        private void ClearOnoigngValueControls()
        {
            OngoingFeeVal.Text = "0";
        }

        private void ClearTieredControls()
        {

            To1.Text = "0";
            From1.Text = "0";
            Equal1.Text = "0";

            To2.Text = "0";
            From2.Text = "0";
            Equal2.Text = "0";

            To3.Text = "0";
            From3.Text = "0";
            Equal3.Text = "0";

            To4.Text = "0";
            From4.Text = "0";
            Equal4.Text = "0";

            To5.Text = "0";
            From5.Text = "0";
            Equal5.Text = "0";
        }

        private void LoadData(IOrganizationUnit orgUnit)
        {
            txtAccountName.Text = orgUnit.Name;
            RadComboBoxItem item = dllAccount.FindItemByValue(orgUnit.TypeName);
            item.Selected = true;
            txtTrustName.Visible = true;
            dllAccount.Visible = false;
            txtAccountType.Visible = true;
            txtAccountType.Text = item.Text;
            txtClientID.Text = orgUnit.ClientId;

            var clientUMAData = orgUnit.ClientEntity as IClientUMAData;
            //if (clientUMAData != null) TFNTrustSuperFund.Text = clientUMAData.TFN;
            var parent = orgUnit as ICmHasParent;
            if (parent != null && (parent.Parent != null && parent.Parent.Clid != Guid.Empty))
            {
                var adviserBMC = UMABroker.GetBMCInstance(parent.Parent.Cid) as IOrganizationUnit;
                var cmHasParent = (ICmHasParent)adviserBMC;
                if (cmHasParent != null && (cmHasParent.Parent.Clid != Guid.Empty && cmHasParent.Parent.Csid != Guid.Empty))
                {
                    ICalculationModule ifaCM = UMABroker.GetCMImplementation(cmHasParent.Parent.Clid, cmHasParent.Parent.Csid);
                    txtAdviserName.Text = ifaCM.Name;
                    txtAdviserName.Visible = true;
                    UMABroker.ReleaseBrokerManagedComponent(ifaCM);
                }
                else
                    ddlAdviserList.Visible = true;

                UMABroker.ReleaseBrokerManagedComponent(adviserBMC);
            }
            else
                ddlAdviserList.Visible = true;

            PopulateData(orgUnit);
        }

        protected void ClientAddUpdateWorkflowTab_TabClick(object sender, RadTabStripEventArgs e)
        {
            //"Selected tab: " + ClientAddUpdateWorkflowTab.SelectedTab.Text;
        }

        protected override void Intialise()
        {
            base.Intialise();
            ContactControl.CanceledPopup += () => HideShowContactAssociationGrid(false);

            ContactControl.PageRefresh += PresentationGrid.Rebind;
            IndividualControl.Canceled += () => HideShowIndividualGrid(false);
            IndividualControl.SaveData += (Cid, ds) =>
                {
                    if (Cid == Guid.Empty.ToString())
                    {
                        SaveOrganizanition(ds);
                    }
                    else
                    {
                        SaveData(Cid, ds);
                    }
                    HideShowIndividualGrid(false);
                };

            IndividualControl.ValidationFailed += () => HideShowIndividualGrid(true);

            IndividualControl.Saved += (CID, CLID, CSID) =>
                {
                    HideShowIndividualGrid(false);
                    SaveContacts(CID, CSID, CLID, "Others");
                };
            NotesControl.FillPresentationData += ReFillUserControlPresentationData;
        }

        private void SaveContacts(Guid CID, Guid CSID, Guid CLID, string businessTitles)
        {
            string clientID = Request.QueryString["ins"];
            var clientData = UMABroker.GetBMCInstance(new Guid(clientID)) as OrganizationUnitCM;
            UMABroker.SaveOverride = true;

            if (clientData != null)
            {
                var contact = clientData.SignatoriesApplicants.FirstOrDefault(associate => associate.Clid == CLID && associate.Csid == CSID);

                if (contact == null)
                {
                    var identityCMDetail = new Oritax.TaxSimp.Common.IdentityCMDetail
                        {
                            Clid = CLID,
                            Csid = CSID,
                            Cid = CID,
                            BusinessTitle = { Title = businessTitles }
                        };
                    clientData.SignatoriesApplicants.Add(identityCMDetail);
                }
                else
                {
                    contact.BusinessTitle.Title = businessTitles;
                }
            }

            if (clientData != null) clientData.CalculateToken(true);
            UMABroker.SetComplete();
            UMABroker.SetStart();
            SaveShare(CID, CSID, CLID, "", false);
            PresentationGrid.Rebind();
        }

        private void SaveShare(Guid CID, Guid CLID, Guid CSID, string share, bool isRemove)
        {
            var individualData = UMABroker.GetBMCInstance(CID) as OrganizationUnitCM;
            cid = Request.Params["ins"];
            if (individualData == null)
            {
                individualData = UMABroker.GetCMImplementation(CLID, CSID) as OrganizationUnitCM;
            }

            UMABroker.SaveOverride = true;
            if (individualData != null)
            {
                var ind = individualData.ClientEntity as IndividualEntity;

                if (ind != null)
                {
                    if (ind.ClientShares == null)
                        ind.ClientShares = new List<Oritax.TaxSimp.Common.ClientShare>();

                    Oritax.TaxSimp.Common.ClientShare individual = ind.ClientShares.FirstOrDefault(associate => associate.ClientCid == Guid.Parse(cid));

                    if (isRemove)
                    {
                        if (individual != null)
                        {
                            //remove code here.
                            ind.ClientShares.Remove(individual);
                        }
                    }
                    else
                    {
                        if (individual == null)
                        {
                            var clientShare = new Oritax.TaxSimp.Common.ClientShare
                                {
                                    ClientID = ClientHeaderInfo1.ClientId,
                                    ClientCid = Guid.Parse(cid),
                                    ClientClid = new Guid(hfCLID.Value),
                                    ClientCsid = new Guid(hfCSID.Value),
                                };

                            if (!string.IsNullOrEmpty(share))
                                clientShare.Share = Convert.ToDecimal(share);
                            ind.ClientShares.Add(clientShare);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(share))
                                individual.Share = Convert.ToDecimal(share);
                        }
                    }
                }
            }

            if (individualData != null) individualData.CalculateToken(true);
            UMABroker.SetComplete();
            UMABroker.SetStart();

            PresentationGrid.Rebind();
        }

        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType == UserType.Client || objUser.UserType == UserType.Accountant ||
                objUser.UserType == UserType.Advisor)
            {
                UMABroker.ReleaseBrokerManagedComponent(objUser);
                return false;
            }
            UMABroker.ReleaseBrokerManagedComponent(objUser);
            return true;
        }

        protected void SaveDataNewClick(object sender, EventArgs e)
        {
            if (dllAccount.SelectedValue != string.Empty && txtAccountName.Text.Trim() != string.Empty)
            {
                UMABroker.SaveOverride = true;
                IOrganizationUnit adviserBMC = null;
                var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
                if (objUser.AdviserCID != Guid.Empty)
                    adviserBMC = UMABroker.GetBMCInstance(objUser.AdviserCID) as IOrganizationUnit;

                var orgCM = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                ILogicalModule created = UMABroker.CreateLogicalCM(dllAccount.SelectedValue, txtAccountName.Text, Guid.Empty, null);
                if (orgCM != null)
                {
                    ILogicalModule organization = UMABroker.GetLogicalCM(orgCM.CLID);
                    organization.AddChildLogicalCM(created);
                }
                var orgUnit = (IOrganizationUnit)created[created.CurrentScenario];
                orgUnit.CreateUniqueID();
                orgUnit.IsInvestableClient = true;

                orgUnit.ClientEntity = orgUnit.IsCorporateType() ? (IClientEntity)new CorporateEntity() : new ClientIndividualEntity();

                var clientUMAData = orgUnit.ClientEntity as IClientUMAData;
                clientUMAData.Name = txtAccountName.Text;
                if (adviserBMC != null)
                {
                    clientUMAData.Parent = new IdentityCM
                        {
                            Cid = adviserBMC.CID,
                            Csid = adviserBMC.CSID,
                            Clid = adviserBMC.CLID
                        };
                }
                UMABroker.ReleaseBrokerManagedComponent(objUser);
                UMABroker.ReleaseBrokerManagedComponent(orgCM);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                Response.Redirect(Request.Url.AbsoluteUri + "?ins=" + orgUnit.CID.ToString());
            }
            else
                MessageBox.RadAlert("Please ensure Account Type is selected and Account Name is entered properly", 330, 180, "Error(s)", "", "");
        }

        protected void SaveDataClick(object sender, EventArgs e)
        {
            SaveAll();
        }

        protected void chkDTFM_OnCheckedChanged(object sender, EventArgs e)
        {
            if (chkDTFM.Checked)
                pnlDIFMSettings.Visible = true;
            else
            {
                pnlDIFMSettings.Visible = false;
                txtDIFMAmount.Text = string.Empty;
            }
        }
    }
}
