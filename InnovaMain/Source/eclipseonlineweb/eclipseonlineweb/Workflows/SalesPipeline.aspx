﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeBehind="SalesPipeline.aspx.cs" Inherits="eclipseonlineweb.SalesPipeline" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .wijmo-wijgrid .wijmo-wijgrid-groupheaderrow td
        {
            background-color: #DAE6F4;
            color: #000000;
            font-size: smaller;
            font-weight: bold;
            vertical-align: middle;
        }
        .wijmo-wijgrid .wijmo-wijgrid-footerrow td
        {
            font-weight: bolder;
            text-align: right;
            font-size: smaller;
        }
    </style>
    <script type="text/javascript">
        function DownloadFile(appno) {
            $.fileDownload('../UMAForm/DownloadForm.aspx?AppNo=' + appno, {
                successCallback: function (url) {

                    $preparingFileModal.dialog('close');
                },
                failCallback: function (responseHtml, url) {

                    $preparingFileModal.dialog('close');

                }
            });
            return false;
        };
    </script>
    <script type="text/javascript">
        function GoToPage(url) {
            window.location = url;
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="SideMenu" ContentPlaceHolderID="SidebarMenu" runat="Server">
    <asp:UpdatePanel ID="UpdatPanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <ul class="menu" runat="server" id="ulMenu">
                <li class="item1" runat="server" id="liPendingAccounts"><a href="#" onclick="javascript:GoToPage('TempAccounts.aspx');">
                    PENDING ACCOUNTS</a></li>
                <li class="item2" runat="server" id="liActiveAccounts"><a href="#" onclick="javascript:GoToPage('ActiveAccounts.aspx');">
                    ACTIVE ACCOUNTS</a></li>
                <li class="item3" runat="server" id="liSalesPipeline"><a href="#" onclick="javascript:GoToPage('SalesPipeline.aspx');"
                    id="A1">SALES PIPELINE</a></li>
                <li class="item4" runat="server" id="liAddAccounts"><a href="#" onclick="javascript:GoToPage('../UMAForm/UMAOneFormProcess.aspx');">
                    ADD ACCOUNTS</a></li>
                <li class="item5" runat="server" id="testliAddUpdateAccounts"><a href="#" onclick="javascript:GoToPage('AddUpdateAccounts.aspx');"
                    id="A2">ADD/UPDATE ACCOUNTS (NEW)</a></li>
            </ul>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function hintContent() {
            return this.data.label + '<br/> ' + this.y + '';
        }
        function hintContentPie() {
            return this.data.toString() + " : " + Globalize.format(this.value / this.total, "p2");
        }

    </script>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                            <asp:ImageButton ID="btnReport" AlternateText="Print" ToolTip="Print" runat="server"
                                OnClick="btnReport_Click" ImageUrl="~/images/download.png" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Sales Pipeline"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <c1:C1GridView AlternatingRowStyle-Font-Size="X-Small" FilterStyle-Font-Size="Smaller"
                RowStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small" ID="PendingAccountsGrid"
                AllowPaging="false" PageSize="20" AllowSorting="true" OnSorting="SortPendingAccountsGrid"
                runat="server" AutogenerateColumns="false" ShowFilter="true" OnFiltering="FilterPendingAccountsGrid"
                OnPageIndexChanging="PendingAccountsGrid_PageIndexChanging" OnRowCommand="Grid_RowCommand"
                Width="100%" ShowFooter="true">
                <CallbackSettings Action="Sorting" />
                <Columns>
                    <c1:C1BoundField DataField="ApplicationNo" SortExpression="ApplicationNo" HeaderText="Application No">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1BoundField>
                    <c1:C1BoundField DataField="AccountName" SortExpression="AccountName" HeaderText="Account Name">
                        <ItemStyle HorizontalAlign="Left" />
                        <HeaderStyle HorizontalAlign="Left" />
                    </c1:C1BoundField>
                    <c1:C1BoundField DataField="AccountType" SortExpression="AccountType" HeaderText="Account Type">
                        <ItemStyle HorizontalAlign="Left" />
                        <HeaderStyle HorizontalAlign="Left" />
                    </c1:C1BoundField>
                    <c1:C1BoundField DataField="Status" SortExpression="Status" HeaderText="Status">
                        <ItemStyle HorizontalAlign="Left" />
                        <HeaderStyle HorizontalAlign="Left" />
                    </c1:C1BoundField>
                    <c1:C1BoundField Aggregate="Sum" DataField="SALESDIFM" SortExpression="SALESDIFM"
                        HeaderText="DIFM" DataFormatString="C">
                        <ItemStyle HorizontalAlign="Right" />
                        <FooterStyle HorizontalAlign="Right" />
                        <HeaderStyle HorizontalAlign="Right" />
                    </c1:C1BoundField>
                    <c1:C1BoundField Aggregate="Sum" DataField="SALESDIWM" SortExpression="SALESDIWM"
                        HeaderText="DIWM" DataFormatString="C">
                        <ItemStyle HorizontalAlign="Right" />
                        <HeaderStyle HorizontalAlign="Right" />
                        <FooterStyle HorizontalAlign="Right" />
                    </c1:C1BoundField>
                    <c1:C1BoundField Aggregate="Sum" DataField="SALESDIY" SortExpression="SALESDIY" HeaderText="DIY"
                        DataFormatString="C">
                        <ItemStyle HorizontalAlign="Right" />
                        <HeaderStyle HorizontalAlign="Right" />
                        <FooterStyle HorizontalAlign="Right" />
                    </c1:C1BoundField>
                </Columns>
            </c1:C1GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script src="<%: Page.ResolveClientUrl("~/Scripts/jquery.fileDownload.js")%>" type="text/javascript"></script>
</asp:Content>
