﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/Site.master"
    ViewStateMode="Enabled" AutoEventWireup="true" CodeBehind="AddUpdateAccounts.aspx.cs"
    Inherits="eclipseonlineweb.AddUpdateAccounts" %>

<%@ Register Src="../Controls/ContactAssociationControl.ascx" TagName="ContactAssociationControl"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register Src="../Controls/Individual.ascx" TagName="IndividualControl" TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="Notes" Src="~/Controls/NotesControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Attachments" Src="~/Controls/Attachments.ascx" %>
<%@ Register TagPrefix="uc1" TagName="breadcrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register TagPrefix="uc2" TagName="SecurityConfigurationControl" Src="~/Controls/SecurityConfigurationControl.ascx" %>
<%@ Register Src="../Controls/SecuritiesChess.ascx" TagName="SecuritiesChess" TagPrefix="uc4" %>
<%@ Register Src="../Controls/WorkFlowAddressDetail.ascx" TagName="WorkFlowAddressDetail"
    TagPrefix="uc5" %>
<%@ Register Src="../Controls/BWAAccessFacilities.ascx" TagName="BWAAccessFacilities"
    TagPrefix="uc3" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .RadListBox
        {
            margin: 0 auto !important;
        }
        
        .RadListBox ul
        {
            display: list-item;
            list-style-type: square;
            margin: 0;
            padding: 0;
        }
        
        .RadListBox li
        {
            text-align: center;
            float: left;
            clear: left;
            padding: 0.1em 1em;
        }
        .ffbutton
        {
            vertical-align: top !important;
        }
        .PositionRight
        {
            float: right;
            padding-right: 25px;
        }
    </style>
    <script type="text/javascript">
        function OnClientLoad(sender, args) {
            var timer = $find("<%:Timer1.ClientID %>");

            startCounter();
        }


        //======================================== UI update for the remaining seconds =======================================//
        var initialSeconds = 300;
        var currentSeconds = initialSeconds;
        var interval = null;
        function startCounter() {
            if (!interval) {
                currentSeconds = initialSeconds;
                interval = window.setInterval(function () {
                    if (currentSeconds > 0) {
                        currentSeconds--;
                    }
                    else {
                        currentSeconds = initialSeconds;
                    }

                    var span = document.getElementById("remainingSeconds");
                    span.innerHTML = currentSeconds;

                }, 1000);
            }
        }

        function stopCounter() {
            if (interval) window.clearInterval(interval);
            interval = null;
        }

        //===============================================================================================================//
        var _hidediv = null;
        function showdiv(id) {
            if (_hidediv)
                _hidediv();
            var div = document.getElementById(id);
            div.style.display = 'block';
            _hidediv = function () { div.style.display = 'none'; };
        }
        function GoToPage(url) {
            window.location = url;
            return false;
        }
        function onCheckBoxClick(chk) {
            var combo = $find("<%= rbPreferredFunds.ClientID %>");
            var text = "";
            var values = "";
            var items = combo.get_items();
            for (var i = 0; i < items.get_count() ; i++) {
                var item = items.getItem(i);
                var chk1 = $get(combo.get_id() + "_i" + i + "_chk1");
                if (chk1.checked) {
                    text += item.get_text() + ",";
                    values += item.get_value() + ",";
                }
            }
            text = removeLastComma(text);
            values = removeLastComma(values);

            if (text.length > 0) {
                combo.set_text(text);
            }
            else {
                combo.set_text("");
            }
        }
        function removeLastComma(str) {
            return str.replace(/,$/, "");
        }
        function onDropDownClosing(sender, e) {
            var combo = $find("<%= rbPreferredFunds.ClientID %>");
            var text = "";
            var items = combo.get_items();
            for (var i = 0; i < items.get_count() ; i++) {
                var item = items.getItem(i);
                var chk1 = $get(combo.get_id() + "_i" + i + "_chk1");
                if (chk1.checked) {
                    text += item.get_text() + ",";
                }
            }
            text = removeLastComma(text);

            if (text.length > 0) {
                combo.set_text(text);
            }
            else {
                combo.set_text("");
            }
        }
        function StopPropagation(e) {
            e.cancelBubble = true;
            if (e.stopPropagation) {
                e.stopPropagation();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="SideMenu" ContentPlaceHolderID="SidebarMenu" runat="Server">
    <asp:UpdatePanel ID="UpdatPanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <ul class="menu" runat="server" id="ulMenu">
                <li class="item1" runat="server" id="liPendingAccounts"><a href="#" onclick="javascript:GoToPage('TempAccounts.aspx');">
                    PENDING ACCOUNTS</a></li>
                <li class="item2" runat="server" id="liActiveAccounts"><a href="#" onclick="javascript:GoToPage('ActiveAccounts.aspx');">
                    ACTIVE ACCOUNTS</a></li>
                <li class="item3" runat="server" id="liSalesPipeline"><a href="#" onclick="javascript:GoToPage('SalesPipeline.aspx');"
                    id="A1">SALES PIPELINE</a></li>
                <li class="item4" runat="server" id="liAddAccounts"><a href="#" onclick="javascript:GoToPage('../UMAForm/UMAOneFormProcess.aspx');">
                    ADD ACCOUNTS</a></li>
                <li class="item5" runat="server" id="testliAddUpdateAccounts"><a href="#" onclick="javascript:GoToPage('AddUpdateAccounts.aspx');"
                    id="A2">ADD/UPDATE ACCOUNTS (NEW)</a></li>
            </ul>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function hintContent() {
            return this.data.label + '<br/> ' + this.y + '';
        }
        function hintContentPie() {
            return this.data.toString() + " : " + Globalize.format(this.value / this.total, "p2");
        }

    </script>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Timer1"></asp:AsyncPostBackTrigger>
        </Triggers>
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:breadcrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <p>
                <asp:Timer ID="Timer1" runat="server" Interval="300000" OnTick="Timer_Save">
                </asp:Timer>
            </p>
            <br />
            <telerik:RadTabStrip OnClientLoad="OnClientLoad" Skin="MetroTouch" ID="ClientAddUpdateWorkflowTab"
                runat="server" MultiPageID="ClientAddUpdateMultiPage" Align="Left" AutoPostBack="false">
                <Tabs>
                    <telerik:RadTab PageViewID="ClientMainDetails" Text="1: Account" Selected="true"
                        ToolTip="Main account details" />
                    <telerik:RadTab PageViewID="FeeDetails" Text="2: Fee" ToolTip="Setup adviser fee structure" />
                    <telerik:RadTab PageViewID="TFNABNDetails" Text="3: TFN / ABN" ToolTip="Setup account TFN, ABN and Signatories TFN/ABN settings" />
                    <telerik:RadTab PageViewID="AuthorityToAct" Text="4: Authority To Act" ToolTip="Authrotiy Act settings for e-Clipse Online and Australian Money Market" />
                    <telerik:RadTab PageViewID="InvestorStatus" Text="5: Investor Status" ToolTip="Investor Status" />
                    <telerik:RadTab PageViewID="DIYDIWM" Text="6: DIY / DIWM / Platforms" ToolTip="DO IT YOURSELF, DO IT WITH ME SERVICE SETTINGS & OTHER PLATFORMS" />
                    <telerik:RadTab PageViewID="DIFM" Text="7: DIFM" ToolTip="DO IT FOR ME SERVICE SETTINGS" />
                    <telerik:RadTab PageViewID="Signatories" Text="8: Signatories" ToolTip="Setup signatories / contacts for the account" />
                    <telerik:RadTab PageViewID="BWA" Text="9: BWA" ToolTip="BWA">
                    </telerik:RadTab>
                    <telerik:RadTab PageViewID="Securities" Text="10: Securities" ToolTip="Securities">
                    </telerik:RadTab>
                    <telerik:RadTab PageViewID="Address" Text="11: Address" ToolTip="Address">
                    </telerik:RadTab>
                    <telerik:RadTab PageViewID="Checklist" Text="12: Checklist" ToolTip="Information Checklist" />
                    <telerik:RadTab PageViewID="Users" Text="13: Users" ToolTip="Users">
                    </telerik:RadTab>
                    <telerik:RadTab PageViewID="Validations" Text="14: Validations" ToolTip="Validation Errors and Warnings" />
                    <telerik:RadTab PageViewID="DOCUMENTS" Text="Documents" ToolTip="Documents & Attachments"
                        Font-Bold="true" Font-Underline="true" />
                    <telerik:RadTab PageViewID="NOTES" Text="Notes" ToolTip="General Notes and Comments"
                        Font-Bold="true" Font-Underline="true" />
                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage ID="ClientAddUpdateMultiPage" runat="server" Width="100%">
                <telerik:RadPageView ID="ClientMainDetails" runat="server" BackColor="White" Selected="true">
                    <br />
                    <table>
                        <tr>
                            <td width="300px">
                                <span class="riLabel">Client ID</span>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtClientID" runat="server" CssClass="Txt-Field" ReadOnly="true"
                                    Width="400px">
                                </telerik:RadTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="300px">
                                <span class="riLabel">Account Type*</span>
                            </td>
                            <td>
                                <telerik:RadTextBox Visible="false" ReadOnly="true" ID="txtAccountType" runat="server"
                                    CssClass="Txt-Field" ValidationGroup="VGIndividual" Width="400px">
                                </telerik:RadTextBox>
                                <telerik:RadComboBox runat="server" ID="dllAccount" Width="400px" Filter="Contains">
                                    <Items>
                                        <telerik:RadComboBoxItem runat="server" Text="" Value="" Selected="true" />
                                        <telerik:RadComboBoxItem runat="server" Text="Corporation Private" Value="ClientCorporationPrivate" />
                                        <telerik:RadComboBoxItem runat="server" Text="Corporation Public" Value="ClientCorporationPublic" />
                                        <telerik:RadComboBoxItem runat="server" Text="Individual or Joint" Value="ClientIndividual" />
                                        <telerik:RadComboBoxItem runat="server" Text="Other Trusts (Corporate)" Value="ClientOtherTrustsCorporate" />
                                        <telerik:RadComboBoxItem runat="server" Text="Other Trusts (Individual or Joint)"
                                            Value="ClientOtherTrustsIndividual" />
                                        <telerik:RadComboBoxItem runat="server" Text="SMSF Corporate Trustee" Value="ClientSMSFCorporateTrustee" />
                                        <telerik:RadComboBoxItem runat="server" Text="SMSF Trustee (Individual or Joint)"
                                            Value="ClientSMSFIndividualTrustee" />
                                    </Items>
                                </telerik:RadComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="300px">
                                <span class="riLabel">Account Name*</span>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtAccountName" runat="server" CssClass="Txt-Field" Width="400px">
                                </telerik:RadTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td width="300px">
                                <span class="riLabel">Trustee Name/s</span>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtTrustName" runat="server" Width="400px" CssClass="Txt-Field"
                                    Visible="False">
                                </telerik:RadTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="300px">
                                <span class="riLabel">Adviser Name</span>
                            </td>
                            <td>
                                <telerik:RadTextBox Visible="false" ReadOnly="true" ID="txtAdviserName" runat="server"
                                    CssClass="Txt-Field" Width="400px">
                                </telerik:RadTextBox>
                                <telerik:RadComboBox Visible="false" runat="server" ID="ddlAdviserList" Width="400px"
                                    Filter="Contains">
                                </telerik:RadComboBox>
                            </td>
                        </tr>
                    </table>
                </telerik:RadPageView>
                <telerik:RadPageView ID="FeeDetails" runat="server" BackColor="White">
                    <div class="MainView" style="padding-top: 5px;">
                        <table>
                            <tr>
                                <td>
                                    <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                                        IncrementSettings-InterceptMouseWheel="true" Label="Upfront fee (p.a. inc GST)$:"
                                        LabelWidth="160px" runat="server" ID="UpFrontFee" Width="500px">
                                    </telerik:RadNumericTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    <telerik:RadButton ID="btnOngoingFeeValue" Skin="Metro" OnCheckedChanged="RadioCheckChanged_Clicked"
                                        OnClick="Button_Click" runat="server" ToggleType="Radio" ButtonType="StandardButton"
                                        GroupName="FeeSelection">
                                        <ToggleStates>
                                            <telerik:RadButtonToggleState Text="Ongoing fee (p.a. inc GST) $" PrimaryIconCssClass="rbToggleRadioChecked" />
                                            <telerik:RadButtonToggleState Text="Ongoing fee (p.a. inc GST) $" PrimaryIconCssClass="rbToggleRadio" />
                                        </ToggleStates>
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="btnOngoingFeePercent" OnCheckedChanged="RadioCheckChanged_Clicked"
                                        OnClick="Button_Click" runat="server" ToggleType="Radio" ButtonType="StandardButton"
                                        GroupName="FeeSelection">
                                        <ToggleStates>
                                            <telerik:RadButtonToggleState Text="Ongoing fee (p.a. inc GST) %" PrimaryIconCssClass="rbToggleRadioChecked" />
                                            <telerik:RadButtonToggleState Text="Ongoing fee (p.a. inc GST) %" PrimaryIconCssClass="rbToggleRadio" />
                                        </ToggleStates>
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="btnOngoingFeeTered" OnCheckedChanged="RadioCheckChanged_Clicked"
                                        OnClick="Button_Click" runat="server" ToggleType="Radio" ButtonType="StandardButton"
                                        GroupName="FeeSelection">
                                        <ToggleStates>
                                            <telerik:RadButtonToggleState Text="Ongoing fee Tiered (p.a. inc GST) %" PrimaryIconCssClass="rbToggleRadioChecked" />
                                            <telerik:RadButtonToggleState Text="Ongoing fee Tiered (p.a. inc GST) %" PrimaryIconCssClass="rbToggleRadio" />
                                        </ToggleStates>
                                    </telerik:RadButton>
                                    <input type="hidden" name="btnSelected" id="btnSelected" value="" runat="server" />
                                    <br />
                                    <br />
                                    <asp:Panel runat="server" ID="pnlOngoingFeeVal" EnableViewState="false" BackColor="White"
                                        Height="50px" Visible="false">
                                        <telerik:RadNumericTextBox ShowSpinButtons="false" EnableViewState="true" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" Label="Ongoing fee (p.a. inc GST) $:"
                                            LabelWidth="160px" runat="server" ID="OngoingFeeVal" Width="500px">
                                        </telerik:RadNumericTextBox>
                                        <br />
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="pnlOngoingFeePercent" EnableViewState="false" BackColor="White"
                                        Height="50px" Visible="false">
                                        <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" Label="Ongoing fee (p.a. inc GST) %:"
                                            LabelWidth="160px" runat="server" ID="OngoingFeePercent" Width="500px">
                                        </telerik:RadNumericTextBox>
                                        <br />
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="pnlOngoingFeeTiered" EnableViewState="false" BackColor="White"
                                        Height="200px" Visible="false">
                                        <telerik:RadNumericTextBox ShowSpinButtons="false" EnableViewState="false" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" Label=" From:" LabelWidth="40px"
                                            runat="server" ID="From1" Width="200px">
                                        </telerik:RadNumericTextBox>
                                        <telerik:RadNumericTextBox EnableViewState="false" ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" Label=" To:" LabelWidth="20px" runat="server"
                                            ID="To1" Width="200px">
                                        </telerik:RadNumericTextBox>
                                        <telerik:RadNumericTextBox EnableViewState="false" ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" Label=" =" LabelWidth="20px" runat="server"
                                            ID="Equal1" Width="200px">
                                        </telerik:RadNumericTextBox>
                                        <br />
                                        <br />
                                        <telerik:RadNumericTextBox ShowSpinButtons="false" EnableViewState="false" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" Label=" From:" LabelWidth="40px"
                                            runat="server" ID="From2" Width="200px">
                                        </telerik:RadNumericTextBox>
                                        <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" Label=" To:" LabelWidth="20px" runat="server"
                                            ID="To2" Width="200px">
                                        </telerik:RadNumericTextBox>
                                        <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" Label=" =" LabelWidth="20px" runat="server"
                                            ID="Equal2" Width="200px">
                                        </telerik:RadNumericTextBox>
                                        <br />
                                        <br />
                                        <telerik:RadNumericTextBox ShowSpinButtons="false" EnableViewState="false" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" Label=" From:" LabelWidth="40px"
                                            runat="server" ID="From3" Width="200px">
                                        </telerik:RadNumericTextBox>
                                        <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" Label=" To:" LabelWidth="20px" runat="server"
                                            ID="To3" Width="200px">
                                        </telerik:RadNumericTextBox>
                                        <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" Label=" =" LabelWidth="20px" runat="server"
                                            ID="Equal3" Width="200px">
                                        </telerik:RadNumericTextBox>
                                        <br />
                                        <br />
                                        <telerik:RadNumericTextBox ShowSpinButtons="false" EnableViewState="false" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" Label=" From:" LabelWidth="40px"
                                            runat="server" ID="From4" Width="200px">
                                        </telerik:RadNumericTextBox>
                                        <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" Label=" To:" LabelWidth="20px" runat="server"
                                            ID="To4" Width="200px">
                                        </telerik:RadNumericTextBox>
                                        <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" Label=" =" LabelWidth="20px" runat="server"
                                            ID="Equal4" Width="200px">
                                        </telerik:RadNumericTextBox>
                                        <br />
                                        <br />
                                        <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" Label=" From:" LabelWidth="40px"
                                            runat="server" ID="From5" Width="200px">
                                        </telerik:RadNumericTextBox>
                                        <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" Label=" To:" LabelWidth="20px" runat="server"
                                            ID="To5" Width="200px">
                                        </telerik:RadNumericTextBox>
                                        <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" Label=" =" LabelWidth="20px" runat="server"
                                            ID="Equal5" Width="200px">
                                        </telerik:RadNumericTextBox>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="TFNABNDetails" runat="server" BackColor="White">
                    <br />
                    <table>
                        <tr style="display:none;">
                            <td width="300px">
                                <span class="riLabel">TFN (Investor/Signatory 1)</span>
                            </td>
                            <td>
                                <telerik:RadMaskedTextBox ID="TFNSig1" runat="server" SelectionOnFocus="SelectAll"
                                    Width="400px" Mask="###-###-###">
                                </telerik:RadMaskedTextBox>
                            </td>
                        </tr>
                        <tr style="display:none;">
                            <td width="300px">
                                <span class="riLabel">TFN (Investor/Signatory 2)</span>
                            </td>
                            <td>
                                <telerik:RadMaskedTextBox ID="TFNSig2" runat="server" SelectionOnFocus="SelectAll"
                                    Width="400px" Mask="###-###-###">
                                </telerik:RadMaskedTextBox>
                            </td>
                        </tr>
                        <tr style="display:none;">
                            <td width="300px">
                                <span class="riLabel">TFN (Investor/Signatory 3)</span>
                            </td>
                            <td>
                                <telerik:RadMaskedTextBox ID="TFNSig3" runat="server" SelectionOnFocus="SelectAll"
                                    Width="400px" Mask="###-###-###">
                                </telerik:RadMaskedTextBox>
                            </td>
                        </tr>
                        <tr style="display:none;">
                            <td width="300px">
                                <span class="riLabel">TFN (Investor/Signatory 4)</span>
                            </td>
                            <td>
                                <telerik:RadMaskedTextBox ID="TFNSig4" runat="server" SelectionOnFocus="SelectAll"
                                    Width="400px" Mask="###-###-###">
                                </telerik:RadMaskedTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="300px">
                                <span class="riLabel">TFN (Trust / Superfund)</span>
                            </td>
                            <td>
                                <telerik:RadMaskedTextBox ID="TFNTrustSuperFund" runat="server" SelectionOnFocus="SelectAll"
                                    Width="400px" Mask="###-###-###">
                                </telerik:RadMaskedTextBox>
                            </td>
                        </tr>
                        <tr id="trACN" runat="server">
                            <td width="300px">
                                <span class="riLabel">ACN (Company)</span>
                            </td>
                            <td>
                                <telerik:RadMaskedTextBox ID="txtACN" runat="server" SelectionOnFocus="SelectAll"
                                    Width="400px" Mask="###-###-###">
                                </telerik:RadMaskedTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="300px">
                                <span class="riLabel">ABN (Company/Superfund)</span>
                            </td>
                            <td>
                                <telerik:RadMaskedTextBox ID="txtABN" runat="server" SelectionOnFocus="SelectAll"
                                    Width="400px" Mask="##-###-###-###">
                                </telerik:RadMaskedTextBox>
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td>
                                <span class="riLabel">If you are a non-resident for tax purposes, please provide your
                                    country of residence</span>
                            </td>
                            <td>
                                <telerik:RadComboBox ID="TFNABNCountryList" runat="server" Filter="Contains" Width="265px">
                                    <Items>
                                        <telerik:RadComboBoxItem Value="" Text="" Selected="true" />
                                        <telerik:RadComboBoxItem Value="AF" Text="Afghanistan" />
                                        <telerik:RadComboBoxItem Value="AL" Text="Albania" />
                                        <telerik:RadComboBoxItem Value="DZ" Text="Algeria" />
                                        <telerik:RadComboBoxItem Value="AS" Text="American Samoa" />
                                        <telerik:RadComboBoxItem Value="AD" Text="Andorra" />
                                        <telerik:RadComboBoxItem Value="AO" Text="Angola" />
                                        <telerik:RadComboBoxItem Value="AI" Text="Anguilla" />
                                        <telerik:RadComboBoxItem Value="AQ" Text="Antarctica" />
                                        <telerik:RadComboBoxItem Value="AG" Text="Antigua And Barbuda" />
                                        <telerik:RadComboBoxItem Value="AR" Text="Argentina" />
                                        <telerik:RadComboBoxItem Value="AM" Text="Armenia" />
                                        <telerik:RadComboBoxItem Value="AW" Text="Aruba" />
                                        <telerik:RadComboBoxItem Value="AU" Text="Australia" Selected="True" />
                                        <telerik:RadComboBoxItem Value="AT" Text="Austria" />
                                        <telerik:RadComboBoxItem Value="AZ" Text="Azerbaijan" />
                                        <telerik:RadComboBoxItem Value="BS" Text="Bahamas" />
                                        <telerik:RadComboBoxItem Value="BH" Text="Bahrain" />
                                        <telerik:RadComboBoxItem Value="BD" Text="Bangladesh" />
                                        <telerik:RadComboBoxItem Value="BB" Text="Barbados" />
                                        <telerik:RadComboBoxItem Value="BY" Text="Belarus" />
                                        <telerik:RadComboBoxItem Value="BE" Text="Belgium" />
                                        <telerik:RadComboBoxItem Value="BZ" Text="Belize" />
                                        <telerik:RadComboBoxItem Value="BJ" Text="Benin" />
                                        <telerik:RadComboBoxItem Value="BM" Text="Bermuda" />
                                        <telerik:RadComboBoxItem Value="BT" Text="Bhutan" />
                                        <telerik:RadComboBoxItem Value="BO" Text="Bolivia" />
                                        <telerik:RadComboBoxItem Value="BA" Text="Bosnia And Herzegowina" />
                                        <telerik:RadComboBoxItem Value="BW" Text="Botswana" />
                                        <telerik:RadComboBoxItem Value="BV" Text="Bouvet Island" />
                                        <telerik:RadComboBoxItem Value="BR" Text="Brazil" />
                                        <telerik:RadComboBoxItem Value="IO" Text="British Indian Ocean Territory" />
                                        <telerik:RadComboBoxItem Value="BN" Text="Brunei Darussalam" />
                                        <telerik:RadComboBoxItem Value="BG" Text="Bulgaria" />
                                        <telerik:RadComboBoxItem Value="BF" Text="Burkina Faso" />
                                        <telerik:RadComboBoxItem Value="BI" Text="Burundi" />
                                        <telerik:RadComboBoxItem Value="KH" Text="Cambodia" />
                                        <telerik:RadComboBoxItem Value="CM" Text="Cameroon" />
                                        <telerik:RadComboBoxItem Value="CA" Text="Canada" />
                                        <telerik:RadComboBoxItem Value="CV" Text="Cape Verde" />
                                        <telerik:RadComboBoxItem Value="KY" Text="Cayman Islands" />
                                        <telerik:RadComboBoxItem Value="CF" Text="Central African Republic" />
                                        <telerik:RadComboBoxItem Value="TD" Text="Chad" />
                                        <telerik:RadComboBoxItem Value="CL" Text="Chile" />
                                        <telerik:RadComboBoxItem Value="CN" Text="China" />
                                        <telerik:RadComboBoxItem Value="CX" Text="Christmas Island" />
                                        <telerik:RadComboBoxItem Value="CC" Text="Cocos (Keeling) Islands" />
                                        <telerik:RadComboBoxItem Value="CO" Text="Colombia" />
                                        <telerik:RadComboBoxItem Value="KM" Text="Comoros" />
                                        <telerik:RadComboBoxItem Value="CG" Text="Congo" />
                                        <telerik:RadComboBoxItem Value="CK" Text="Cook Islands" />
                                        <telerik:RadComboBoxItem Value="CR" Text="Costa Rica" />
                                        <telerik:RadComboBoxItem Value="CI" Text="Cote D'Ivoire" />
                                        <telerik:RadComboBoxItem Value="HR" Text="Croatia (Local Name: Hrvatska)" />
                                        <telerik:RadComboBoxItem Value="CU" Text="Cuba" />
                                        <telerik:RadComboBoxItem Value="CY" Text="Cyprus" />
                                        <telerik:RadComboBoxItem Value="CZ" Text="Czech Republic" />
                                        <telerik:RadComboBoxItem Value="DK" Text="Denmark" />
                                        <telerik:RadComboBoxItem Value="DJ" Text="Djibouti" />
                                        <telerik:RadComboBoxItem Value="DM" Text="Dominica" />
                                        <telerik:RadComboBoxItem Value="DO" Text="Dominican Republic" />
                                        <telerik:RadComboBoxItem Value="TP" Text="East Timor" />
                                        <telerik:RadComboBoxItem Value="EC" Text="Ecuador" />
                                        <telerik:RadComboBoxItem Value="EG" Text="Egypt" />
                                        <telerik:RadComboBoxItem Value="SV" Text="El Salvador" />
                                        <telerik:RadComboBoxItem Value="GQ" Text="Equatorial Guinea" />
                                        <telerik:RadComboBoxItem Value="ER" Text="Eritrea" />
                                        <telerik:RadComboBoxItem Value="EE" Text="Estonia" />
                                        <telerik:RadComboBoxItem Value="ET" Text="Ethiopia" />
                                        <telerik:RadComboBoxItem Value="FK" Text="Falkland Islands (Malvinas)" />
                                        <telerik:RadComboBoxItem Value="FO" Text="Faroe Islands" />
                                        <telerik:RadComboBoxItem Value="FJ" Text="Fiji" />
                                        <telerik:RadComboBoxItem Value="FI" Text="Finland" />
                                        <telerik:RadComboBoxItem Value="FR" Text="France" />
                                        <telerik:RadComboBoxItem Value="GF" Text="French Guiana" />
                                        <telerik:RadComboBoxItem Value="PF" Text="French Polynesia" />
                                        <telerik:RadComboBoxItem Value="TF" Text="French Southern Territories" />
                                        <telerik:RadComboBoxItem Value="GA" Text="Gabon" />
                                        <telerik:RadComboBoxItem Value="GM" Text="Gambia" />
                                        <telerik:RadComboBoxItem Value="GE" Text="Georgia" />
                                        <telerik:RadComboBoxItem Value="DE" Text="Germany" />
                                        <telerik:RadComboBoxItem Value="GH" Text="Ghana" />
                                        <telerik:RadComboBoxItem Value="GI" Text="Gibraltar" />
                                        <telerik:RadComboBoxItem Value="GR" Text="Greece" />
                                        <telerik:RadComboBoxItem Value="GL" Text="Greenland" />
                                        <telerik:RadComboBoxItem Value="GD" Text="Grenada" />
                                        <telerik:RadComboBoxItem Value="GP" Text="Guadeloupe" />
                                        <telerik:RadComboBoxItem Value="GU" Text="Guam" />
                                        <telerik:RadComboBoxItem Value="GT" Text="Guatemala" />
                                        <telerik:RadComboBoxItem Value="GN" Text="Guinea" />
                                        <telerik:RadComboBoxItem Value="GW" Text="Guinea-Bissau" />
                                        <telerik:RadComboBoxItem Value="GY" Text="Guyana" />
                                        <telerik:RadComboBoxItem Value="HT" Text="Haiti" />
                                        <telerik:RadComboBoxItem Value="HM" Text="Heard And Mc Donald Islands" />
                                        <telerik:RadComboBoxItem Value="VA" Text="Holy See (Vatican City State)" />
                                        <telerik:RadComboBoxItem Value="HN" Text="Honduras" />
                                        <telerik:RadComboBoxItem Value="HK" Text="Hong Kong" />
                                        <telerik:RadComboBoxItem Value="HU" Text="Hungary" />
                                        <telerik:RadComboBoxItem Value="IS" Text="Icel And" />
                                        <telerik:RadComboBoxItem Value="IN" Text="India" />
                                        <telerik:RadComboBoxItem Value="ID" Text="Indonesia" />
                                        <telerik:RadComboBoxItem Value="IR" Text="Iran (Islamic Republic Of)" />
                                        <telerik:RadComboBoxItem Value="IQ" Text="Iraq" />
                                        <telerik:RadComboBoxItem Value="IE" Text="Ireland" />
                                        <telerik:RadComboBoxItem Value="IL" Text="Israel" />
                                        <telerik:RadComboBoxItem Value="IT" Text="Italy" />
                                        <telerik:RadComboBoxItem Value="JM" Text="Jamaica" />
                                        <telerik:RadComboBoxItem Value="JP" Text="Japan" />
                                        <telerik:RadComboBoxItem Value="JO" Text="Jordan" />
                                        <telerik:RadComboBoxItem Value="KZ" Text="Kazakhstan" />
                                        <telerik:RadComboBoxItem Value="KE" Text="Kenya" />
                                        <telerik:RadComboBoxItem Value="KI" Text="Kiribati" />
                                        <telerik:RadComboBoxItem Value="KP" Text="Korea, Dem People'S Republic" />
                                        <telerik:RadComboBoxItem Value="KR" Text="Korea, Republic Of" />
                                        <telerik:RadComboBoxItem Value="KW" Text="Kuwait" />
                                        <telerik:RadComboBoxItem Value="KG" Text="Kyrgyzstan" />
                                        <telerik:RadComboBoxItem Value="LA" Text="Lao People'S Dem Republic" />
                                        <telerik:RadComboBoxItem Value="LV" Text="Latvia" />
                                        <telerik:RadComboBoxItem Value="LB" Text="Lebanon" />
                                        <telerik:RadComboBoxItem Value="LS" Text="Lesotho" />
                                        <telerik:RadComboBoxItem Value="LR" Text="Liberia" />
                                        <telerik:RadComboBoxItem Value="LY" Text="Libyan Arab Jamahiriya" />
                                        <telerik:RadComboBoxItem Value="LI" Text="Liechtenstein" />
                                        <telerik:RadComboBoxItem Value="LT" Text="Lithuania" />
                                        <telerik:RadComboBoxItem Value="LU" Text="Luxembourg" />
                                        <telerik:RadComboBoxItem Value="MO" Text="Macau" />
                                        <telerik:RadComboBoxItem Value="MK" Text="Macedonia" />
                                        <telerik:RadComboBoxItem Value="MG" Text="Madagascar" />
                                        <telerik:RadComboBoxItem Value="MW" Text="Malawi" />
                                        <telerik:RadComboBoxItem Value="MY" Text="Malaysia" />
                                        <telerik:RadComboBoxItem Value="MV" Text="Maldives" />
                                        <telerik:RadComboBoxItem Value="ML" Text="Mali" />
                                        <telerik:RadComboBoxItem Value="MT" Text="Malta" />
                                        <telerik:RadComboBoxItem Value="MH" Text="Marshall Islands" />
                                        <telerik:RadComboBoxItem Value="MQ" Text="Martinique" />
                                        <telerik:RadComboBoxItem Value="MR" Text="Mauritania" />
                                        <telerik:RadComboBoxItem Value="MU" Text="Mauritius" />
                                        <telerik:RadComboBoxItem Value="YT" Text="Mayotte" />
                                        <telerik:RadComboBoxItem Value="MX" Text="Mexico" />
                                        <telerik:RadComboBoxItem Value="FM" Text="Micronesia, Federated States" />
                                        <telerik:RadComboBoxItem Value="MD" Text="Moldova, Republic Of" />
                                        <telerik:RadComboBoxItem Value="MC" Text="Monaco" />
                                        <telerik:RadComboBoxItem Value="MN" Text="Mongolia" />
                                        <telerik:RadComboBoxItem Value="MS" Text="Montserrat" />
                                        <telerik:RadComboBoxItem Value="MA" Text="Morocco" />
                                        <telerik:RadComboBoxItem Value="MZ" Text="Mozambique" />
                                        <telerik:RadComboBoxItem Value="MM" Text="Myanmar" />
                                        <telerik:RadComboBoxItem Value="NA" Text="Namibia" />
                                        <telerik:RadComboBoxItem Value="NR" Text="Nauru" />
                                        <telerik:RadComboBoxItem Value="NP" Text="Nepal" />
                                        <telerik:RadComboBoxItem Value="NL" Text="Netherlands" />
                                        <telerik:RadComboBoxItem Value="AN" Text="Netherlands Ant Illes" />
                                        <telerik:RadComboBoxItem Value="NC" Text="New Caledonia" />
                                        <telerik:RadComboBoxItem Value="NZ" Text="New Zealand" />
                                        <telerik:RadComboBoxItem Value="NI" Text="Nicaragua" />
                                        <telerik:RadComboBoxItem Value="NE" Text="Niger" />
                                        <telerik:RadComboBoxItem Value="NG" Text="Nigeria" />
                                        <telerik:RadComboBoxItem Value="NU" Text="Niue" />
                                        <telerik:RadComboBoxItem Value="NF" Text="Norfolk Island" />
                                        <telerik:RadComboBoxItem Value="MP" Text="Northern Mariana Islands" />
                                        <telerik:RadComboBoxItem Value="NO" Text="Norway" />
                                        <telerik:RadComboBoxItem Value="OM" Text="Oman" />
                                        <telerik:RadComboBoxItem Value="PK" Text="Pakistan" />
                                        <telerik:RadComboBoxItem Value="PW" Text="Palau" />
                                        <telerik:RadComboBoxItem Value="PA" Text="Panama" />
                                        <telerik:RadComboBoxItem Value="PG" Text="Papua New Guinea" />
                                        <telerik:RadComboBoxItem Value="PY" Text="Paraguay" />
                                        <telerik:RadComboBoxItem Value="PE" Text="Peru" />
                                        <telerik:RadComboBoxItem Value="PH" Text="Philippines" />
                                        <telerik:RadComboBoxItem Value="PN" Text="Pitcairn" />
                                        <telerik:RadComboBoxItem Value="PL" Text="Poland" />
                                        <telerik:RadComboBoxItem Value="PT" Text="Portugal" />
                                        <telerik:RadComboBoxItem Value="PR" Text="Puerto Rico" />
                                        <telerik:RadComboBoxItem Value="QA" Text="Qatar" />
                                        <telerik:RadComboBoxItem Value="RE" Text="Reunion" />
                                        <telerik:RadComboBoxItem Value="RO" Text="Romania" />
                                        <telerik:RadComboBoxItem Value="RU" Text="Russian Federation" />
                                        <telerik:RadComboBoxItem Value="RW" Text="Rwanda" />
                                        <telerik:RadComboBoxItem Value="KN" Text="Saint K Itts And Nevis" />
                                        <telerik:RadComboBoxItem Value="LC" Text="Saint Lucia" />
                                        <telerik:RadComboBoxItem Value="VC" Text="Saint Vincent, The Grenadines" />
                                        <telerik:RadComboBoxItem Value="WS" Text="Samoa" />
                                        <telerik:RadComboBoxItem Value="SM" Text="San Marino" />
                                        <telerik:RadComboBoxItem Value="ST" Text="Sao Tome And Principe" />
                                        <telerik:RadComboBoxItem Value="SA" Text="Saudi Arabia" />
                                        <telerik:RadComboBoxItem Value="SN" Text="Senegal" />
                                        <telerik:RadComboBoxItem Value="SC" Text="Seychelles" />
                                        <telerik:RadComboBoxItem Value="SL" Text="Sierra Leone" />
                                        <telerik:RadComboBoxItem Value="SG" Text="Singapore" />
                                        <telerik:RadComboBoxItem Value="SK" Text="Slovakia (Slovak Republic)" />
                                        <telerik:RadComboBoxItem Value="SI" Text="Slovenia" />
                                        <telerik:RadComboBoxItem Value="SB" Text="Solomon Islands" />
                                        <telerik:RadComboBoxItem Value="SO" Text="Somalia" />
                                        <telerik:RadComboBoxItem Value="ZA" Text="South Africa" />
                                        <telerik:RadComboBoxItem Value="GS" Text="South Georgia , S Sandwich Is." />
                                        <telerik:RadComboBoxItem Value="ES" Text="Spain" />
                                        <telerik:RadComboBoxItem Value="LK" Text="Sri Lanka" />
                                        <telerik:RadComboBoxItem Value="SH" Text="St. Helena" />
                                        <telerik:RadComboBoxItem Value="PM" Text="St. Pierre And Miquelon" />
                                        <telerik:RadComboBoxItem Value="SD" Text="Sudan" />
                                        <telerik:RadComboBoxItem Value="SR" Text="Suriname" />
                                        <telerik:RadComboBoxItem Value="SJ" Text="Svalbard, Jan Mayen Islands" />
                                        <telerik:RadComboBoxItem Value="SZ" Text="Sw Aziland" />
                                        <telerik:RadComboBoxItem Value="SE" Text="Sweden" />
                                        <telerik:RadComboBoxItem Value="CH" Text="Switzerland" />
                                        <telerik:RadComboBoxItem Value="SY" Text="Syrian Arab Republic" />
                                        <telerik:RadComboBoxItem Value="TW" Text="Taiwan" />
                                        <telerik:RadComboBoxItem Value="TJ" Text="Tajikistan" />
                                        <telerik:RadComboBoxItem Value="TZ" Text="Tanzania, United Republic Of" />
                                        <telerik:RadComboBoxItem Value="TH" Text="Thailand" />
                                        <telerik:RadComboBoxItem Value="TG" Text="Togo" />
                                        <telerik:RadComboBoxItem Value="TK" Text="Tokelau" />
                                        <telerik:RadComboBoxItem Value="TO" Text="Tonga" />
                                        <telerik:RadComboBoxItem Value="TT" Text="Trinidad And Tobago" />
                                        <telerik:RadComboBoxItem Value="TN" Text="Tunisia" />
                                        <telerik:RadComboBoxItem Value="TR" Text="Turkey" />
                                        <telerik:RadComboBoxItem Value="TM" Text="Turkmenistan" />
                                        <telerik:RadComboBoxItem Value="TC" Text="Turks And Caicos Islands" />
                                        <telerik:RadComboBoxItem Value="TV" Text="Tuvalu" />
                                        <telerik:RadComboBoxItem Value="UG" Text="Uganda" />
                                        <telerik:RadComboBoxItem Value="UA" Text="Ukraine" />
                                        <telerik:RadComboBoxItem Value="AE" Text="United Arab Emirates" />
                                        <telerik:RadComboBoxItem Value="GB" Text="United Kingdom" />
                                        <telerik:RadComboBoxItem Value="US" Text="United States" />
                                        <telerik:RadComboBoxItem Value="UM" Text="United States Minor Is." />
                                        <telerik:RadComboBoxItem Value="UY" Text="Uruguay" />
                                        <telerik:RadComboBoxItem Value="UZ" Text="Uzbekistan" />
                                        <telerik:RadComboBoxItem Value="VU" Text="Vanuatu" />
                                        <telerik:RadComboBoxItem Value="VE" Text="Venezuela" />
                                        <telerik:RadComboBoxItem Value="VN" Text="Viet Nam" />
                                        <telerik:RadComboBoxItem Value="VG" Text="Virgin Islands (British)" />
                                        <telerik:RadComboBoxItem Value="VI" Text="Virgin Islands (U.S.)" />
                                        <telerik:RadComboBoxItem Value="WF" Text="Wallis And Futuna Islands" />
                                        <telerik:RadComboBoxItem Value="EH" Text="Western Sahara" />
                                        <telerik:RadComboBoxItem Value="YE" Text="Yemen" />
                                        <telerik:RadComboBoxItem Value="YU" Text="Yugoslavia" />
                                        <telerik:RadComboBoxItem Value="ZR" Text="Zaire" />
                                        <telerik:RadComboBoxItem Value="ZM" Text="Zambia" />
                                        <telerik:RadComboBoxItem Value="ZW" Text="Zimbabwe" />
                                    </Items>
                                </telerik:RadComboBox>
                            </td>
                        </tr>
                    </table>
                </telerik:RadPageView>
                <telerik:RadPageView runat="server" BackColor="White" ID="AuthorityToAct">
                    <br />
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadButton Skin="Metro" ID="chkAuthrotiyToActEclipseOnline" runat="server"
                                    ToggleType="CheckBox" ButtonType="ToggleButton" Checked="false" AutoPostBack="false">
                                    <ToggleStates>
                                        <telerik:RadButtonToggleState Text="Client wishes to grant a Limited Power of Attorney to e-Clipse Online">
                                        </telerik:RadButtonToggleState>
                                        <telerik:RadButtonToggleState Text="Client does not wishes to grant a Limited Power of Attorney to e-Clipse Online">
                                        </telerik:RadButtonToggleState>
                                    </ToggleStates>
                                </telerik:RadButton>
                                <br />
                                <telerik:RadButton ID="chkAuthrotiyToActAMM" runat="server" ToggleType="CheckBox"
                                    ButtonType="ToggleButton" Checked="false" AutoPostBack="false">
                                    <ToggleStates>
                                        <telerik:RadButtonToggleState Text="Client wishes to grant a Limited Power of Attorney toAustralian Money Market (AMM) ">
                                        </telerik:RadButtonToggleState>
                                        <telerik:RadButtonToggleState Text="Client does not wishes to grant a Limited Power of Attorney to Australian Money Market (AMM) ">
                                        </telerik:RadButtonToggleState>
                                    </ToggleStates>
                                </telerik:RadButton>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table>
                        <tr>
                            <td>
                                <fieldset>
                                    <legend>Authority To Act</legend><span class="riLabel">
                                        <p>
                                            The agreed limited power of attorney granted to e-Clipse and Australian Money Market
                                            (AMM) is to undertake and perform the following on the clients behalf:</p>
                                        <p>
                                            1. apply for and open new bank and broker accounts with any chosen financial institution,
                                            or any other investments;<br />
                                            2. authorise for direct debit payments to be acted upon by any chosen institution
                                            in order to transfer funds in the relation to new or existing accounts<br />
                                            3. instruct in relation to rollovers, maturities, transfer requests of existing
                                            investments;<br />
                                            4. advise of changes to my/our contact details as advised from time to time;<br />
                                            5. notify my/our TFN(s) or Exemption(s) in respect of any existing investment(s)
                                            and or new purchase(s) made on my/our behalf.</p>
                                        <p>
                                            I/We understand that e-Clipse and AMM will provide me/us with a copy of the relevant
                                            terms and conditions relating to any account which e-Clipse and AMM proposes to
                                            open in my/our name prior to the account being opened. I/We am aware that I/we must
                                            read and understand those terms and conditions and contact e-Clipse and AMM if I/we
                                            do not agree to be bound by them.</p>
                                        <p>
                                            This authority is given only to act as per instructions given by me/us via the e-Clipse
                                            UMA platform or by any other means of documented instruction.</p>
                                    </span>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </telerik:RadPageView>
                <telerik:RadPageView runat="server" BackColor="White" ID="InvestorStatus">
                    <br />
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadButton ID="rbWholesaleInvestor" Skin="Metro" OnCheckedChanged="InvestorStatus_Clicked"
                                    Width="300px" OnClick="Button_Click" runat="server" ToggleType="Radio" ButtonType="StandardButton"
                                    GroupName="InvestorStatus" AutoPostBack="False">
                                    <ToggleStates>
                                        <telerik:RadButtonToggleState Text="Wholesale Investor" PrimaryIconCssClass="rbToggleRadioChecked" />
                                        <telerik:RadButtonToggleState Text="Wholesale Investor" PrimaryIconCssClass="rbToggleRadio" />
                                    </ToggleStates>
                                </telerik:RadButton>
                                <br />
                                <telerik:RadButton ID="rbSophisticatedInvestor" OnCheckedChanged="InvestorStatus_Clicked"
                                    Width="300px" OnClick="Button_Click" runat="server" ToggleType="Radio" ButtonType="StandardButton"
                                    GroupName="InvestorStatus" AutoPostBack="False">
                                    <ToggleStates>
                                        <telerik:RadButtonToggleState Text="Sophisticated Investor" PrimaryIconCssClass="rbToggleRadioChecked" />
                                        <telerik:RadButtonToggleState Text="Sophisticated Investor" PrimaryIconCssClass="rbToggleRadio" />
                                    </ToggleStates>
                                </telerik:RadButton>
                                &nbsp;<telerik:RadButton ID="chkCertificateFromAccountant" runat="server" ToggleType="CheckBox"
                                    ButtonType="StandardButton" AutoPostBack="False">
                                    <ToggleStates>
                                        <telerik:RadButtonToggleState Text="Certificate from Accountant is attached" PrimaryIconCssClass="rbToggleCheckboxChecked" />
                                        <telerik:RadButtonToggleState Text="Certificate from Accountant is attached" PrimaryIconCssClass="rbToggleCheckbox" />
                                    </ToggleStates>
                                </telerik:RadButton>
                                <br />
                                <telerik:RadButton ID="rbProfessionalInvestor" OnCheckedChanged="InvestorStatus_Clicked"
                                    Width="300px" OnClick="Button_Click" runat="server" ToggleType="Radio" ButtonType="StandardButton"
                                    GroupName="InvestorStatus" AutoPostBack="False">
                                    <ToggleStates>
                                        <telerik:RadButtonToggleState Text="Professional Investor" PrimaryIconCssClass="rbToggleRadioChecked" />
                                        <telerik:RadButtonToggleState Text="Professional Investor" PrimaryIconCssClass="rbToggleRadio" />
                                    </ToggleStates>
                                </telerik:RadButton>
                                <br />
                                <telerik:RadButton ID="rbRetailInvestor" OnCheckedChanged="InvestorStatus_Clicked"
                                    Width="300px" OnClick="Button_Click" runat="server" ToggleType="Radio" ButtonType="StandardButton"
                                    GroupName="InvestorStatus" AutoPostBack="False">
                                    <ToggleStates>
                                        <telerik:RadButtonToggleState Text="Retail Investor" PrimaryIconCssClass="rbToggleRadioChecked" />
                                        <telerik:RadButtonToggleState Text="Retail Investor " PrimaryIconCssClass="rbToggleRadio" />
                                    </ToggleStates>
                                </telerik:RadButton>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table>
                        <tr>
                            <td>
                                <fieldset>
                                    <legend>Investor Status Notes</legend><span class="riLabel"><b>Wholesale or Retail Investors</b>
                                        <p>
                                            To qualify as a Sophisticated or Professional Investor under Section 708 of the
                                            Corporations Act you need to be able to meet one of the conditions below. Please
                                            ensure that you include documentation to support your situation. The Investment
                                            Program to be implemented for you under the Managed Account Service will need to
                                            have regard to whether you are a wholesale or retail investor. A sophisticated investor
                                            (see below) is a client equipped to be considered a wholesale client in relation
                                            to the UMA service provided by e-Clipse. To be considered a wholesale investor as
                                            defined under the Corporations Act 2001 (cth) (Act) you must:</p>
                                        <p>
                                            i. Be investing an amount of at least $500,000 in e-Clipse; or</p>
                                        <p>
                                            ii. Meet the "sophisticated investor" criteria under the Act (see below).</p>
                                        <p>
                                            In the absence of a certificate from a qualified accountant where the amount invested
                                            is less than $500,000, we must consider you a retail investor.</p>
                                        To qualify as a Sophisticated or Professional Investor under Section 708 of the
                                        Corporations Act you need to be able to meet one of the conditions below.</label><br />
                                        <br />
                                        <b>Sophisticated Investors</b>
                                        <p>
                                            To qualify as a sophisticated investor please supply a certificate provided by a
                                            qualified accountant or eligible foreign professional body which states that you
                                            have:</p>
                                        a. net assets of at least $2.5 million; or<br />
                                        b. gross income for each of the last two financial years of at least $250,000 a
                                        year."<br />
                                        <p>
                                            The certificate must include:</p>
                                        <p>
                                            1. the class membership of the professional body to which the accountant belongs
                                            (as outlined below)</p>
                                        <p>
                                            2. the date of issue</p>
                                        <p>
                                            3. the Chapter of Chapters of the Corporations Act under which it is issued</p>
                                        <p>
                                            If you control a company or trust and meet the requirements above, the company or
                                            trust may also qualify for exempt status as a sophisticated investor.</p>
                                        <b>Qualified Accountant</b>
                                        <p>
                                            A qualified Accountant is defined in s88B of the Act as a person meeting the criteria
                                            in a class declaration made by ASIC. Under ASIC's class order (CO 01/1256) you are
                                            a qualified accountant if you: a. belong to one of the following professional bodies
                                            in the declared membership classification (below) and; b. you comply with your body's
                                            continuing professional education requirements.</p>
                                    </span>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </telerik:RadPageView>
                <telerik:RadPageView ID="DIYDIWM" BackColor="White" runat="server">
                    <br />
                    <fieldset>
                        <legend>DO IT YOURSELF</legend>
                        <table>
                            <tr>
                                <td width="300px">
                                    <telerik:RadButton ID="chkUseDIY" runat="server" AutoPostBack="True" ToggleType="CheckBox"
                                        OnCheckedChanged="chkUseDIY_OnCheckedChanged" ButtonType="ToggleButton">
                                    </telerik:RadButton>
                                    <span class="riLabel">Use <b>'Do It Yourself'</b> Service for this Account</span>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:Panel ID="pnlDIYSettings" runat="server" Visible="false">
                            <span class="riLabel">Estimated Investment Amount:</span>
                            <telerik:RadMaskedTextBox ID="txtDIYAmount" runat="server" SelectionOnFocus="SelectAll"
                                Width="400px" Mask="###############">
                            </telerik:RadMaskedTextBox>
                            <br />
                            <telerik:RadButton Skin="Metro" ID="rbDIYBWA" runat="server" ToggleType="CheckBox"
                                ButtonType="ToggleButton" Checked="false" AutoPostBack="true" OnClick="rbDIYBWA_click">
                                <ToggleStates>
                                    <telerik:RadButtonToggleState Text="Use BWA CMA (for Bank Account)"></telerik:RadButtonToggleState>
                                    <telerik:RadButtonToggleState Text="Do not use BWA CMA (for Bank Account)"></telerik:RadButtonToggleState>
                                </ToggleStates>
                            </telerik:RadButton>
                            <br />
                            <telerik:RadButton Skin="Metro" ID="rdDIYMAQ" runat="server" ToggleType="CheckBox"
                                ButtonType="ToggleButton" Checked="false" AutoPostBack="true" OnClick="rdDIYMAQ_click">
                                <ToggleStates>
                                    <telerik:RadButtonToggleState Text="Use MACQUARIE CMA (for Bank Account)"></telerik:RadButtonToggleState>
                                    <telerik:RadButtonToggleState Text="Do not use MACQUARIE CMA (for Bank Account)">
                                    </telerik:RadButtonToggleState>
                                </ToggleStates>
                            </telerik:RadButton>
                            <asp:Panel ID="pnlMacquarie" runat="server" Visible="false">
                                <br />
                                <table>
                                    <tr>
                                        <td width="50px">
                                        </td>
                                        <td width="300px">
                                            <span class="riLabel">BSB</span>
                                        </td>
                                        <td>
                                            <telerik:RadMaskedTextBox ID="txtMaqBSBNo" runat="server" SelectionOnFocus="SelectAll"
                                                Width="400px" Mask="###-###" EmptyMessage="Enter BSB Number">
                                            </telerik:RadMaskedTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50px">
                                        </td>
                                        <td width="300px">
                                            <span class="riLabel">Account No</span>
                                        </td>
                                        <td>
                                            <telerik:RadMaskedTextBox ID="txtMaqAccountNo" runat="server" SelectionOnFocus="SelectAll"
                                                Width="400px" Mask="############" EmptyMessage="Enter Account Number">
                                            </telerik:RadMaskedTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50px">
                                        </td>
                                        <td width="300px">
                                            <span class="riLabel">Account Name</span>
                                        </td>
                                        <td>
                                            <telerik:RadTextBox ID="txtMaqAccountName" runat="server" CssClass="Txt-Field" Width="400px">
                                            </telerik:RadTextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <br />
                            <telerik:RadButton ID="rdDIYASX" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton"
                                Checked="false" AutoPostBack="false">
                                <ToggleStates>
                                    <telerik:RadButtonToggleState Text="Use Desktop Broker (for HIN)"></telerik:RadButtonToggleState>
                                    <telerik:RadButtonToggleState Text="Do not use Desktop Broker (for HIN)"></telerik:RadButtonToggleState>
                                </ToggleStates>
                            </telerik:RadButton>
                            <br />
                            <telerik:RadButton ID="rbDIYFIGG" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton"
                                Checked="false" AutoPostBack="false">
                                <ToggleStates>
                                    <telerik:RadButtonToggleState Text="Use FIIG (for TDs & cash)"></telerik:RadButtonToggleState>
                                    <telerik:RadButtonToggleState Text="Do not use FIIG (for TDs & cash)"></telerik:RadButtonToggleState>
                                </ToggleStates>
                            </telerik:RadButton>
                            <br />
                            <telerik:RadButton ID="rdDIYAMM" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton"
                                Checked="false" AutoPostBack="false">
                                <ToggleStates>
                                    <telerik:RadButtonToggleState Text="Use Australian Money Market (for TDs & cash)">
                                    </telerik:RadButtonToggleState>
                                    <telerik:RadButtonToggleState Text="Do not use Australian Money Market (for TDs & cash)">
                                    </telerik:RadButtonToggleState>
                                </ToggleStates>
                            </telerik:RadButton>
                        </asp:Panel>
                    </fieldset>
                    <br />
                    <fieldset>
                        <legend>DO IT WIH ME</legend>
                        <table>
                            <tr>
                                <td width="300px">
                                    <telerik:RadButton ID="chkUseDIWM" runat="server" AutoPostBack="True" ToggleType="CheckBox"
                                        OnCheckedChanged="chkUseDIYWM_OnCheckedChanged" ButtonType="ToggleButton">
                                    </telerik:RadButton>
                                    <span class="riLabel">Use <b>'Do It With Me'</b> Service for this Account</span>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:Panel ID="pnlDIWMSettings" runat="server" Visible="false">
                            <table>
                                <tr>
                                    <td>
                                        <span class="riLabel">Estimated Investment Amount:</span>
                                        <telerik:RadMaskedTextBox ID="txtDIWMAmount" runat="server" SelectionOnFocus="SelectAll"
                                            Width="400px" Mask="###############">
                                        </telerik:RadMaskedTextBox><br />
                                        <telerik:RadButton Skin="Metro" ID="rdDIWMBWA" runat="server" ToggleType="CheckBox"
                                            ButtonType="ToggleButton" Checked="false" AutoPostBack="true" OnClick="rdDIWMBWA_click">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Use BWA CMA (for Bank Account)"></telerik:RadButtonToggleState>
                                                <telerik:RadButtonToggleState Text="Do not use BWA CMA (for Bank Account)"></telerik:RadButtonToggleState>
                                            </ToggleStates>
                                        </telerik:RadButton>
                                        <br />
                                        <telerik:RadButton Skin="Metro" ID="rbUseDIWMMaq" runat="server" ToggleType="CheckBox"
                                            ButtonType="ToggleButton" Checked="false" AutoPostBack="true" OnClick="rbUseDIWMMaq_click">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Use MACQUARIE CMA (for Bank Account)"></telerik:RadButtonToggleState>
                                                <telerik:RadButtonToggleState Text="Do not use MACQUARIE CMA (for Bank Account)">
                                                </telerik:RadButtonToggleState>
                                            </ToggleStates>
                                        </telerik:RadButton>
                                        <asp:Panel ID="pnlMacquarieDIWM" runat="server" Visible="false">
                                            <br />
                                            <table>
                                                <tr>
                                                    <td width="50px">
                                                    </td>
                                                    <td width="300px">
                                                        <span class="riLabel">BSB</span>
                                                    </td>
                                                    <td>
                                                        <telerik:RadMaskedTextBox ID="txtMaqBSBNoDIWM" runat="server" SelectionOnFocus="SelectAll"
                                                            Width="400px" Mask="###-###" EmptyMessage="Enter BSB Number">
                                                        </telerik:RadMaskedTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="50px">
                                                    </td>
                                                    <td width="300px">
                                                        <span class="riLabel">Account No</span>
                                                    </td>
                                                    <td>
                                                        <telerik:RadMaskedTextBox ID="txtMaqAccountNoDIWM" runat="server" SelectionOnFocus="SelectAll"
                                                            Width="400px" Mask="############" EmptyMessage="Enter Account Number">
                                                        </telerik:RadMaskedTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="50px">
                                                    </td>
                                                    <td width="300px">
                                                        <span class="riLabel">Account Name</span>
                                                    </td>
                                                    <td>
                                                        <telerik:RadTextBox ID="txtMaqAccountNameDIWM" runat="server" CssClass="Txt-Field"
                                                            Width="400px">
                                                        </telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <br />
                                        <telerik:RadButton ID="rdDIWMASX" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton"
                                            Checked="false" AutoPostBack="false">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Use Desktop Broker (for HIN)"></telerik:RadButtonToggleState>
                                                <telerik:RadButtonToggleState Text="Do not use Desktop Broker (for HIN)"></telerik:RadButtonToggleState>
                                            </ToggleStates>
                                        </telerik:RadButton>
                                        <br />
                                        <telerik:RadButton ID="rdDIWMFIIG" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton"
                                            Checked="false" AutoPostBack="false">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Use FIIG (for TDs & cash)"></telerik:RadButtonToggleState>
                                                <telerik:RadButtonToggleState Text="Do not use FIIG (for TDs & cash)"></telerik:RadButtonToggleState>
                                            </ToggleStates>
                                        </telerik:RadButton>
                                        <br />
                                        <telerik:RadButton ID="rdDIWMAMM" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton"
                                            Checked="false" AutoPostBack="false">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Use Australian Money Market (for TDs & cash)">
                                                </telerik:RadButtonToggleState>
                                                <telerik:RadButtonToggleState Text="Do not use Australian Money Market (for TDs & cash)">
                                                </telerik:RadButtonToggleState>
                                            </ToggleStates>
                                        </telerik:RadButton>
                                        <br />
                                        <telerik:RadButton ID="rbDIWMSS" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton"
                                            Checked="false" AutoPostBack="true" OnClick="rbDIWMSS_Click">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Use State Street (for Innova). If required please select preferred funds below.">
                                                </telerik:RadButtonToggleState>
                                                <telerik:RadButtonToggleState Text="Do not use State Street (for Innova)"></telerik:RadButtonToggleState>
                                            </ToggleStates>
                                        </telerik:RadButton>
                                        <br />
                                        <table>
                                            <tr>
                                                <td>
                                                    <telerik:RadComboBox Skin="Metro" Width="400px" ID="rbPreferredFunds" Filter="Contains"
                                                        CheckBoxes="false" runat="server" EmptyMessage="Select" HighlightTemplatedItems="true" OnClientDropDownClosing="onDropDownClosing">
                                                        <ItemTemplate>
                                                           <div onclick="StopPropagation(event)">
                                                                <label>
                                                                     <asp:CheckBox runat="server" ID="chk1" Checked="false" onclick="onCheckBoxClick(this)"/>
                                                                     <%# Eval("CompanyName") %>
                                                                </label>
                                                           </div>
                                                        </ItemTemplate>
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <telerik:RadButton Text="SELECT" ID="btnSelectPreferredFundDIWM" runat="server" OnClick="btnSelectPreferredFundDIWM_Click">
                                                    </telerik:RadButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="riLabel">Selected Preferred Funds</span><br />
                                                    <telerik:RadListBox Skin="Metro" ButtonSettings-RenderButtonText="true" ButtonSettings-AreaHeight="20px"
                                                        AllowDelete="true" runat="server" ID="rdSelectedFunds">
                                                    </telerik:RadListBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </fieldset>
                    <br />
                    <fieldset>
                        <legend>BT WRAP</legend>
                        <table>
                            <tr>
                                <td width="300px">
                                    <span class="riLabel">Investor No. / Account No.</span>
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="txtInvestorNumber" runat="server" CssClass="Txt-Field" ReadOnly="false"
                                        Width="400px">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="300px">
                                    <span class="riLabel">Adviser No. / Code</span>
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="txtAdviserCode" runat="server" CssClass="Txt-Field" ReadOnly="false"
                                        Width="400px">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>                            
                        </table>
                    </fieldset>
                    <br />
                    <fieldset>
                        <legend>e-Clipse Super</legend>
                        <table>
                            <tr>
                                <td width="300px">
                                    <span class="riLabel">Investor No. / Account No</span>
                                </td>
                                <td>
                                    <telerik:RadTextBox runat="server" ID="txtEclipseInvestorNoAccNo" Width="400px">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="300px">
                                    <span class="riLabel">Adviser No. / Code</span>
                                </td>
                                <td>
                                    <telerik:RadTextBox runat="server" ID="txtEclipseAdviserNoCode" Width="400px">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>                            
                        </table>
                    </fieldset>
                </telerik:RadPageView>
                <telerik:RadPageView ID="DIFM" BackColor="White" runat="server">
                    <br />
                    <fieldset>
                        <legend>DO IT FOR ME</legend>
                        <table>
                            <tr>
                                <td colspan="2">
                                    <telerik:RadButton ID="chkDTFM" runat="server" AutoPostBack="True" ToggleType="CheckBox"
                                        Text="Use Do It For Me Service for this Account" OnCheckedChanged="chkDTFM_OnCheckedChanged"
                                        ButtonType="ToggleButton">
                                    </telerik:RadButton>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:Panel ID="pnlDIFMSettings" runat="server" Visible="false">
                            <table>
                                <tr>
                                    <td>
                                        <span class="riLabel">Estimated Investment Amount:</span>
                                    </td>
                                    <td>
                                        <telerik:RadMaskedTextBox ID="txtDIFMAmount" runat="server" SelectionOnFocus="SelectAll"
                                            Width="400px" Mask="$###############">
                                        </telerik:RadMaskedTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="riLabel">Please select the Investment Program: </span>
                                    </td>
                                    <td>
                                        <telerik:RadComboBox Skin="Metro" Width="400px" ID="rbComboBoxInvestmentProgram"
                                            Filter="Contains" runat="server">
                                        </telerik:RadComboBox>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table>
                                <tr>
                                    <td width="400px">
                                        <span class="riLabel"><b>Handling of unmanaged investments:</b></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadButton Skin="Metro" ID="rbSellTheHolding" runat="server" ToggleType="CheckBox"
                                            ButtonType="ToggleButton" Checked="true" AutoPostBack="false">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Sell the holding"></telerik:RadButtonToggleState>
                                                <telerik:RadButtonToggleState Text="Do not Sell the holding"></telerik:RadButtonToggleState>
                                            </ToggleStates>
                                        </telerik:RadButton>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        <telerik:RadButton Skin="Metro" ID="rbUseMinBalReBal" runat="server" ToggleType="CheckBox"
                                            ButtonType="ToggleButton" Checked="true" AutoPostBack="true" OnClick="rbUseMinBalReBal_Clicked">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Please enter the minimum balance per investment in a rebalance. *Note: Default is $500">
                                                </telerik:RadButtonToggleState>
                                                <telerik:RadButtonToggleState Text="Do not use minimum balance for rebalance"></telerik:RadButtonToggleState>
                                            </ToggleStates>
                                        </telerik:RadButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadButton ID="rbMinBalRebalByCurrency" Skin="Metro" OnClick="rbMinBalRebalByCurrency_Clicked"
                                            Width="400px" runat="server" ToggleType="Radio" ButtonType="StandardButton" GroupName="MinBalReBalGroup"
                                            Checked="true" AutoPostBack="true">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Selected="true" Text="Min. balance for rebalance by currency value"
                                                    PrimaryIconCssClass="rbToggleRadioChecked" />
                                                <telerik:RadButtonToggleState Text="Min. balance for rebalance by currency value"
                                                    PrimaryIconCssClass="rbToggleRadio" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                        <telerik:RadMaskedTextBox ID="txtMinBalReBalCurrency" runat="server" SelectionOnFocus="SelectAll"
                                            Width="400px" Mask="$###############" Text="2500">
                                        </telerik:RadMaskedTextBox>
                                        <br />
                                        <telerik:RadButton ID="rbMinBalRebalByPercentage" OnClick="rbMinBalRebalByPercent_Clicked"
                                            Width="400px" runat="server" ToggleType="Radio" ButtonType="StandardButton" GroupName="MinBalReBalGroup"
                                            AutoPostBack="true">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Min. balance for rebalance by currency percentage"
                                                    PrimaryIconCssClass="rbToggleRadioChecked" />
                                                <telerik:RadButtonToggleState Text="Min. balance for rebalance by currency percentage"
                                                    PrimaryIconCssClass="rbToggleRadio" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                        <telerik:RadMaskedTextBox ID="txtMinBalReBalPercent" Visible="false" runat="server"
                                            SelectionOnFocus="SelectAll" Width="400px" Mask="%##.##">
                                        </telerik:RadMaskedTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        <telerik:RadButton Skin="Metro" ID="rbUseMinTradeBal" runat="server" ToggleType="CheckBox"
                                            ButtonType="ToggleButton" Checked="true" AutoPostBack="true" OnClick="rbUserMinTradeBal_Clicked">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Please enter the minimum trade amount in a rebalance. *Note: Default is $2500">
                                                </telerik:RadButtonToggleState>
                                                <telerik:RadButtonToggleState Text="Do not use minimum balance for rebalance"></telerik:RadButtonToggleState>
                                            </ToggleStates>
                                        </telerik:RadButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadButton ID="rbUserMinTradeBalByCurrency" Skin="Metro" OnClick="rbUserMinTradeBalByCurrency_Clicked"
                                            Width="400px" runat="server" ToggleType="Radio" ButtonType="StandardButton" GroupName="MinBalReBalGroupByTrade"
                                            Checked="true" AutoPostBack="true">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Selected="true" Text="Min. balance for rebalance by currency value"
                                                    PrimaryIconCssClass="rbToggleRadioChecked" />
                                                <telerik:RadButtonToggleState Text="Min. balance for rebalance by currency value"
                                                    PrimaryIconCssClass="rbToggleRadio" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                        <telerik:RadMaskedTextBox ID="txtrbUserMinTradeBalByCurrency" runat="server" SelectionOnFocus="SelectAll"
                                            Width="400px" Mask="$###############" Text="2500">
                                        </telerik:RadMaskedTextBox>
                                        <br />
                                        <telerik:RadButton ID="rbUserMinTradeBalByPercent" OnClick="rbUserMinTradeBalByPercent_Clicked"
                                            Width="400px" runat="server" ToggleType="Radio" ButtonType="StandardButton" GroupName="MinBalReBalGroupByTrade"
                                            AutoPostBack="true">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Min. balance for rebalance by currency percentage"
                                                    PrimaryIconCssClass="rbToggleRadioChecked" />
                                                <telerik:RadButtonToggleState Text="Min. balance for rebalance by currency percentage"
                                                    PrimaryIconCssClass="rbToggleRadio" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                        <telerik:RadMaskedTextBox ID="txtrbUserMinTradeBalByPercent" Visible="false" runat="server"
                                            SelectionOnFocus="SelectAll" Width="400px" Mask="%##.##">
                                        </telerik:RadMaskedTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="400px">
                                        <br />
                                        <span class="riLabel">Handling of investments that are not purchased due to a hold or
                                            sell rating in the Portfolio</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadButton ID="rbHandlingInvestmentDueToSellRatingAssignValue" Skin="Metro"
                                            Width="400px" runat="server" ToggleType="Radio" ButtonType="StandardButton" GroupName="HandlingInvestmentDueToSellRating"
                                            Checked="true" AutoPostBack="false">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Selected="true" Text="Assign allocated value to cash (Default)"
                                                    PrimaryIconCssClass="rbToggleRadioChecked" />
                                                <telerik:RadButtonToggleState Text="Do not assign allocated value to cash" PrimaryIconCssClass="rbToggleRadio" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                        <br />
                                        <telerik:RadButton ID="rbHandlingInvestmentDueToSellRatingDistributeValue" Width="400px"
                                            runat="server" ToggleType="Radio" ButtonType="StandardButton" GroupName="HandlingInvestmentDueToSellRating"
                                            AutoPostBack="false">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Distribute allocated value to other investments"
                                                    PrimaryIconCssClass="rbToggleRadioChecked" />
                                                <telerik:RadButtonToggleState Text="Do not Distribute allocated value to other investments"
                                                    PrimaryIconCssClass="rbToggleRadio" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="400px">
                                        <br />
                                        <span class="riLabel">Handling of investments that are not purchased due to an exclusion</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadButton ID="rbHandlingInvestmentDueToExclusionAssign" Skin="Metro" Width="400px"
                                            runat="server" ToggleType="Radio" ButtonType="StandardButton" GroupName="HandlingInvestmentDueToExclusion"
                                            Checked="true" AutoPostBack="false">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Selected="true" Text="Assign allocated value to cash (Default)"
                                                    PrimaryIconCssClass="rbToggleRadioChecked" />
                                                <telerik:RadButtonToggleState Text="Do not assign allocated value to cash" PrimaryIconCssClass="rbToggleRadio" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                        <br />
                                        <telerik:RadButton ID="rbHandlingInvestmentDueToExclusionDistribute" Width="400px"
                                            runat="server" ToggleType="Radio" ButtonType="StandardButton" GroupName="HandlingInvestmentDueToExclusion"
                                            AutoPostBack="false">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Distribute allocated value to other investments"
                                                    PrimaryIconCssClass="rbToggleRadioChecked" />
                                                <telerik:RadButtonToggleState Text="Do not Distribute allocated value to other investments"
                                                    PrimaryIconCssClass="rbToggleRadio" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="400px">
                                        <br />
                                        <span class="riLabel">Handling of investments that are not purchased due to a minimum
                                            trade or minimum holding constraint</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadButton ID="rbHandlingInvestmentDueToMinConstraintAssign" Skin="Metro"
                                            Width="400px" runat="server" ToggleType="Radio" ButtonType="StandardButton" GroupName="HandlingInvestmentDueToMinConstraint"
                                            Checked="true" AutoPostBack="false">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Selected="true" Text="Assign allocated value to cash (Default)"
                                                    PrimaryIconCssClass="rbToggleRadioChecked" />
                                                <telerik:RadButtonToggleState Text="Do not assign allocated value to cash" PrimaryIconCssClass="rbToggleRadio" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                        <br />
                                        <telerik:RadButton ID="rbHandlingInvestmentDueToMinConstraintDistribute" Width="400px"
                                            runat="server" ToggleType="Radio" ButtonType="StandardButton" GroupName="HandlingInvestmentDueToMinConstraint"
                                            AutoPostBack="false">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Distribute allocated value to other investments"
                                                    PrimaryIconCssClass="rbToggleRadioChecked" />
                                                <telerik:RadButtonToggleState Text="Do not Distribute allocated value to other investments"
                                                    PrimaryIconCssClass="rbToggleRadio" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </fieldset>
                </telerik:RadPageView>
                <telerik:RadPageView ID="Signatories" runat="server" BackColor="White">
                    <fieldset>
                        <table width="100%">
                            <tr>
                                <td style="width: 5%">
                                    <asp:ImageButton ID="btnNewContact" AlternateText="Add New Contact" ToolTip="Add New Contacts"
                                        runat="server" OnClick="LnkbtnAddBankAccClick" ImageUrl="~/images/add-icon.png" />
                                </td>
                                <td style="width: 5%">
                                    <asp:ImageButton ID="ImageButton1" AlternateText="Save" ToolTip="Associate Contacts"
                                        runat="server" OnClick="LnkbtnAssociateContactsClick" ImageUrl="~/images/add_existing.png"
                                        Visible="true" />
                                </td>
                                <td style="width: 5%">
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <asp:HiddenField ID="hfCLID" runat="server" />
                    <asp:HiddenField ID="hfCSID" runat="server" />
                    <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                        PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                        GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                        AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource"
                        OnItemCommand="PresentationGrid_OnItemCommand" OnItemDataBound="PresentationGrid_OnItemDataBound">
                        <PagerStyle Mode="NumericPages"></PagerStyle>
                        <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                            Name="Banks" TableLayout="Fixed">
                            <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <Columns>
                                <telerik:GridBoundColumn SortExpression="CID" ReadOnly="true" HeaderText="CID" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="CID" UniqueName="CID" Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="CLID" ReadOnly="true" HeaderText="CLID"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="CLID" UniqueName="CLID" Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="CSID" ReadOnly="true" HeaderText="CSID"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="CSID" UniqueName="CSID" Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="FirstName" ReadOnly="true" HeaderText="Given Name"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="FirstName" UniqueName="FirstName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn ReadOnly="true" SortExpression="MiddleName" HeaderText="Middle Name"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                                    HeaderButtonType="TextButton" DataField="MiddleName" UniqueName="MiddleName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="LastName" HeaderText="Family Name" ReadOnly="true"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="LastName" UniqueName="LastName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="160px" HeaderStyle-Width="200px" SortExpression="EmailAddress"
                                    ReadOnly="true" HeaderText="Email Address" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="EmailAddress"
                                    UniqueName="EmailAddress">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn UniqueName="Share" HeaderText="Share" FilterControlWidth="130px"
                                    DataField="Share" SortExpression="Share">
                                    <ItemTemplate>
                                        <telerik:RadTextBox ID="txtShare" runat="server" OnTextChanged="txtShare_OnTextChanged"
                                            AutoPostBack="true">
                                        </telerik:RadTextBox>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridButtonColumn ButtonType="LinkButton" CommandName="Detail" Text="Detail"
                                    UniqueName="DetailColumn">
                                    <HeaderStyle Width="5%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyLinkButton"></ItemStyle>
                                </telerik:GridButtonColumn>
                                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                                    UniqueName="EditColumn">
                                    <HeaderStyle Width="3%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                                </telerik:GridButtonColumn>
                                <telerik:GridButtonColumn ConfirmText="Are you sure you want to remove the association?"
                                    ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                    <HeaderStyle Width="3%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                                </telerik:GridButtonColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <asp:Panel runat="server" ID="ViewPanel" Visible="false">
                        <div style="margin: 20px 0 -22px; position: relative; text-align: right;">
                            <asp:ImageButton ID="Button1" runat="server" ImageAlign="AbsMiddle" ImageUrl="../images/cross_icon_normal.png"
                                OnClick="Button1_OnClick" />
                        </div>
                        <asp:Panel ID="Panel1" runat="server" Enabled="false">
                            <div class="content">
                                <uc:IndividualControl ID="IndividualControlView" runat="server" />
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                    <div id="IndividualModal" runat="server" visible="false" class="holder">
                        <div class="popup">
                            <div class="content">
                                <uc:IndividualControl ID="IndividualControl" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div id="ContactModal" runat="server" visible="false" class="holder">
                        <div class="popup">
                            <div class="content">
                                <uc1:ContactAssociationControl ID="ContactControl" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="overlay" id="OVER" visible="False" runat="server">
                    </div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="BWA" BackColor="White" runat="server">
                    <uc3:BWAAccessFacilities ID="BWAAccessFacilitiesDIY" BWAAccessFacilityType="DoItYourSelf"
                        runat="server" />
                    <uc3:BWAAccessFacilities ID="BWAAccessFacilitiesDIWM" BWAAccessFacilityType="DoItWithMe"
                        runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="Securities" BackColor="white" runat="server">
                    <uc4:SecuritiesChess ID="SecuritiesChessControl" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="Address" runat="server" BackColor="White">
                    <uc5:WorkFlowAddressDetail ID="WorkFlowAddressDetailControl" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="Checklist" BackColor="White" runat="server">
                    <div class="MainView" style="padding-top: 5px;">
                        <fieldset>
                            <legend>UMA Document Checklist</legend>
                            <telerik:RadButton ID="chkProofID" runat="server" Text="Proof of ID (Drivers Licence / passport) - Certified Copy"
                                AutoPostBack="false" ToggleType="CheckBox" ButtonType="ToggleButton">
                            </telerik:RadButton>
                            <br />
                            <telerik:RadButton ID="chkTrustDeed" runat="server" Text="Trust Deed (If a Trust) - Certified Copy of Title Page, Contents/Recitals Page, Execution Page"
                                AutoPostBack="false" ToggleType="CheckBox" ButtonType="ToggleButton">
                            </telerik:RadButton>
                            <br />
                            <telerik:RadButton ID="chkLetterFrom" runat="server" Text="Letter from your Accountant (If a sophisticated Investor)"
                                AutoPostBack="false" ToggleType="CheckBox" ButtonType="ToggleButton">
                            </telerik:RadButton>
                            <br />
                            <telerik:RadButton ID="chkBusinessName" runat="server" Text="Business Name Registration Certificate (If a business account)"
                                AutoPostBack="false" ToggleType="CheckBox" ButtonType="ToggleButton">
                            </telerik:RadButton>
                            <br />
                            <telerik:RadButton ID="chkIssueSponsorHolding" runat="server" Text="Issuer Sponsored Holdings to CHESS Sponsorship Conversion"
                                AutoPostBack="false" ToggleType="CheckBox" ButtonType="ToggleButton">
                            </telerik:RadButton>
                            <br />
                            <telerik:RadButton ID="chkIssuerSponsoredHoldingStatement" runat="server" Text="Issuer Sponsored Holding Statements"
                                AutoPostBack="false" ToggleType="CheckBox" ButtonType="ToggleButton">
                            </telerik:RadButton>
                            <br />
                            <telerik:RadButton ID="chkMarketTransfer" runat="server" Text="Off Market Transfer"
                                AutoPostBack="false" ToggleType="CheckBox" ButtonType="ToggleButton">
                            </telerik:RadButton>
                            <br />
                            <telerik:RadButton ID="chkChangeClientDetail" runat="server" Text="Change of Client Details Form"
                                AutoPostBack="false" ToggleType="CheckBox" ButtonType="ToggleButton">
                            </telerik:RadButton>
                            <br />
                            <telerik:RadButton ID="chkBrokerToBrokerTransfer" runat="server" Text="Broker to Broker Transfer Form"
                                AutoPostBack="false" ToggleType="CheckBox" ButtonType="ToggleButton">
                            </telerik:RadButton>
                            <br />
                            <telerik:RadButton ID="chkProofAddressPassPortProvided" runat="server" Text="Proof of Address (when passport used for ID) - copy of utilities bill, bank statement or tax assessment notice will be accepted"
                                AutoPostBack="false" ToggleType="CheckBox" ButtonType="ToggleButton">
                            </telerik:RadButton>
                        </fieldset>
                    </div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="Users" BackColor="White" runat="server">
                    <div class="MainView">
                        <uc2:SecurityConfigurationControl ID="UsersWorkFlow" runat="server" />
                    </div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="Validations" BackColor="White" runat="server">
                    <telerik:RadGrid runat="server" ID="ValidationsInformationGrid" AllowPaging="false"
                        AllowSorting="True" OnNeedDataSource="ValidationsInformationGrid_OnNeedDataSource"
                        AutoGenerateColumns="False" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                        AllowAutomaticDeletes="True" ShowStatusBar="True" GridLines="None" PageSize="4"
                        CellSpacing="0">
                        <PagerStyle Mode="NumericPages" AlwaysVisible="true"></PagerStyle>
                        <MasterTableView Width="100%" CommandItemDisplay="Top">
                            <Columns>
                                <telerik:GridBoundColumn ReadOnly="true" SortExpression="VALIDATIONMESSAGETYPE" HeaderText="Type"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                                    HeaderButtonType="TextButton" DataField="VALIDATIONMESSAGETYPE" UniqueName="VALIDATIONMESSAGETYPE">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn ReadOnly="true" SortExpression="VALIDATIONMESSAGE" HeaderText="Message Desc."
                                    AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                                    HeaderButtonType="TextButton" DataField="VALIDATIONMESSAGE" UniqueName="VALIDATIONMESSAGE">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn ReadOnly="true" SortExpression="VALIDATIONMESSAGEDETAIL"
                                    HeaderText="Message Detail" AutoPostBackOnFilter="true" ShowFilterIcon="true"
                                    CurrentFilterFunction="Contains" HeaderButtonType="TextButton" DataField="VALIDATIONMESSAGEDETAIL"
                                    UniqueName="VALIDATIONMESSAGEDETAIL">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn ReadOnly="true" SortExpression="ISSUESOURCE" HeaderText="Message Source"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                                    HeaderButtonType="TextButton" DataField="ISSUESOURCE" UniqueName="ISSUESOURCE">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <EditFormSettings>
                                <EditColumn ButtonType="ImageButton">
                                </EditColumn>
                            </EditFormSettings>
                        </MasterTableView>
                    </telerik:RadGrid>
                </telerik:RadPageView>
                <telerik:RadPageView ID="DOCUMENTS" BackColor="White" runat="server">
                    <uc1:Attachments ID="AttachmentControl" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="NOTES" BackColor="White" runat="server">
                    <uc1:Notes ID="NotesControl" runat="server" />
                </telerik:RadPageView>
            </telerik:RadMultiPage>
            <br />
            <fieldset>
                &nbsp;&nbsp;
                <telerik:RadButton runat="server" ID="btnNewSave" Text="SAVE NEW ACCOUNT" Width="150px"
                    Visible="false" OnClick="SaveDataNewClick">
                </telerik:RadButton>
                &nbsp;&nbsp;
                <telerik:RadButton runat="server" ID="btnSave" Text="SAVE" Width="150px" Visible="false"
                    OnClick="SaveDataClick">
                </telerik:RadButton>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Workflow will be saved every 5 minutes(300 seconds):
                <span style="color: #bb0000; font-size: 15px;" id="remainingSeconds"></span>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
    <telerik:RadWindowManager Skin="Metro" ID="MessageBox" runat="server" ReloadOnShow="true"
        ShowContentDuringLoad="False" Behavior="Default" InitialBehavior="None" EnableEmbeddedBaseStylesheet="true"
        Width="400px" Height="400px" VisibleOnPageLoad="false" DestroyOnClose="True">
    </telerik:RadWindowManager>
</asp:Content>
