﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.Utilities;
using eclipseonlineweb.WebUtilities;

namespace eclipseonlineweb
{
    public partial class TempAccounts : UMABasePage
    {
        private static ICMBroker _UMABroker
        {
            get
            {
                var brokerSlot = Thread.GetNamedDataSlot("Broker");

                return (ICMBroker)Thread.GetData(brokerSlot);
            }
            set
            {
                var brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (value == null)
                    Thread.FreeNamedDataSlot("Broker");

                Thread.SetData(brokerSlot, value);
            }
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
        }

        public override void LoadPage()
        {
            ScriptManager sm = ScriptManager.GetCurrent(Page);

            if (sm != null)
            {
                sm.RegisterPostBackControl(btnExportAdvisersFee);

            }

            LoadPendingAccountsGrid();
            if (Master != null)
                ((HiddenField)Master.FindControl("hfMainMenuText")).Value = "WORKFLOW&nbsp; > &nbsp;PENDING ACCOUNTS";

            _UMABroker = new CMBroker(Context.User, DBConnection.Connection.ConnectionString);
            _UMABroker.SetStart();

            var objUser = (DBUser)_UMABroker.GetBMCInstance(Page.User.Identity.Name, "DBUser_1_1");
            var dbUserDetailsDs = new DBUserDetailsDS();
            objUser.GetData(dbUserDetailsDs);

            if (Page.User.Identity.Name.ToLower() != "administrator")
            {
                if (objUser.UserType == UserType.Advisor)
                {
                    liSalesPipeline.Visible = false;
                }
            }
        }

        protected override void Intialise()
        {
            base.Intialise();
            this.PendingAccountsGrid.RowEditing += new C1GridViewEditEventHandler(PendingAccountsGrid_RowEditing);
            this.PendingAccountsGrid.RowDeleting += new C1GridViewDeleteEventHandler(PendingAccountsGrid_RowDeleting);
        }

        protected void PendingAccountsGrid_RowEditing(object sender, C1GridViewEditEventArgs e)
        {

        }

        protected void PendingAccountsGrid_RowDeleting(object sender, C1GridViewDeleteEventArgs e)
        {

        }


        protected void Grid_RowCommand(object sender, C1GridViewCommandEventArgs e)
        {
            Dictionary<string, string> SourceImportTransactionType = CashManagementEntity.GetImportTransactionDataSource();
            string appNo = string.Empty;

            if (e.CommandName == "edit")
            {
                appNo = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[0].Text;
                Response.Redirect("~/UMAForm/UMAOneFormProcess.aspx?AppNo=" + appNo);
            }
            else if (e.CommandName == "delete")
            {
                appNo = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[0].Text;
                this.UMABroker.SaveOverride = true;
                IBrokerManagedComponent orgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
                UMAFormDS ds = new UMAFormDS();
                ds.AppNo = appNo;
                ds.UMAFormDSOperation = UMAFormDSOperation.Delete;
                orgCM.SetData(ds);
                this.UMABroker.ReleaseBrokerManagedComponent(orgCM);
                this.UMABroker.SetComplete();
                this.UMABroker.SetStart();
                this.LoadPendingAccountsGrid();
            }

            else if (e.CommandName == "select")
            {
                appNo = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[0].Text;
                this.UMABroker.SaveOverride = true;
                IBrokerManagedComponent orgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
                UMAFormDS ds = new UMAFormDS();
                ds.AppNo = appNo;
                ds.UMAFormDSOperation = UMAFormDSOperation.CreateClient;
                orgCM.SetData(ds);
                this.UMABroker.ReleaseBrokerManagedComponent(orgCM);
                this.UMABroker.SetComplete();
                this.UMABroker.SetStart();
                this.LoadPendingAccountsGrid();
            }
        }

        protected void LoadPendingAccountsGrid()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.Page.User.Identity.Name, "DBUser_1_1");
            IBrokerManagedComponent orgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            UMAFormsListingDS ds = new UMAFormsListingDS();
            orgCM.GetData(ds);

            DataTable accountsList = ds.Tables[UMAFormsListingDS.ACCOUNTSLISTTABLE];
            string rowFlter = string.Empty;
            rowFlter = "ClientID IN ('')";

            if (objUser.UserType == UserType.Innova || objUser.Name == "Administrator")
            {
                this.btnAssociateAllClients.Visible = true;
                this.btnExportAdvisersFee.Visible = true;
                PendingAccountsGrid.Columns[PendingAccountsGrid.Columns.Count - 1].Visible = true;

                DataTable flteredTable = ds.Tables[UMAFormsListingDS.ACCOUNTSLISTTABLE];
                DataView filteredVew = new DataView(flteredTable);
                filteredVew.RowFilter = rowFlter;

                this.PendingAccountsGrid.DataSource = filteredVew.ToTable();
                PendingAccountsGrid.DataBind();
            }
            else if (objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA)
            {
                DataTable dt = accountsList.Clone();
                foreach (DataRow dr in accountsList.Rows)
                {
                    if (dr[UMAFormsListingDS.CREATEDBY].ToString() == this.User.Identity.Name)
                    {
                        dt.ImportRow(dr);
                    }
                    else
                    {
                        var userentity = GetCurrentUser();
                        var aapNo = dr[UMAFormsListingDS.APPLICATIONNO];

                        var rows = ds.userTable.Select(string.Format("{0}='{1}' and {2}='{3}'", ds.userTable.UMAFORMENTITYID, aapNo, ds.userTable.USERCID, userentity.CID));

                        if (rows.Length > 0)
                        {
                            dt.ImportRow(dr);

                        }
                    }
                }
                DataView view = new DataView(dt);
               // view.RowFilter = "CreatedBy='" + this.User.Identity.Name + "'";
                DataTable flteredTable = view.ToTable();
                DataView filteredVew = new DataView(flteredTable);
                filteredVew.RowFilter = rowFlter;

                this.PendingAccountsGrid.DataSource = filteredVew.ToTable();
                PendingAccountsGrid.DataBind();
            }
            else
            {
                DataTable flteredTable = ds.Tables[UMAFormsListingDS.ACCOUNTSLISTTABLE];
                DataView filteredVew = new DataView(flteredTable);
                filteredVew.RowFilter = rowFlter;

                this.PendingAccountsGrid.DataSource = filteredVew.ToTable();
                PendingAccountsGrid.DataBind();
            }

            this.UMABroker.ReleaseBrokerManagedComponent(orgCM);
            this.UMABroker.ReleaseBrokerManagedComponent(objUser);
        }

        protected void PendingAccountsGrid_PageIndexChanging(object sender, C1GridViewPageEventArgs e)
        {
            this.PendingAccountsGrid.PageIndex = e.NewPageIndex;
            LoadPendingAccountsGrid();
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType == UserType.Client || objUser.UserType == UserType.Accountant)
                return false;
            else
                return true;
        }

        protected void FilterPendingAccountsGrid(object sender, C1GridViewFilterEventArgs e)
        {
            LoadPendingAccountsGrid();
        }

        protected void SortPendingAccountsGrid(object sender, C1GridViewSortEventArgs e)
        {
            LoadPendingAccountsGrid();
        }

        protected void btnAssociateAllClients_Click(object sender, EventArgs e)
        {
            this.UMABroker.SaveOverride = true;
            IBrokerManagedComponent orgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            UMAFormDS ds = new UMAFormDS();
            ds.UMAFormDSOperation = UMAFormDSOperation.AssociateAllClients;
            orgCM.SetData(ds);
            this.UMABroker.SetComplete();
            this.UMABroker.SetStart();
            this.LoadPendingAccountsGrid();
            this.UMABroker.ReleaseBrokerManagedComponent(orgCM);

        }

        protected void btnExportAdvisersFee_Click(object sender, EventArgs e)
        {
            IBrokerManagedComponent orgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            UMAFormDS ds = new UMAFormDS();
            ds.UMAFormDSOperation = UMAFormDSOperation.ExportAdviserFees;
            orgCM.GetData(ds);
            this.UMABroker.ReleaseBrokerManagedComponent(orgCM);

            DataSet excelDataset = new DataSet();
            DataView entityView = new DataView(ds.Tables[UMAFormDS.ADVISERFEELISTING]);
            entityView.Sort = UMAFormDS.ACCOUNTNAME + " ASC";
            DataTable entityTable = entityView.ToTable();

            excelDataset.Merge(entityTable, true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, "Adviser Proposed Fee Export-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", this.Page.Response);

        }
    }
}
