﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.Security;
using eclipseonlineweb.WebUtilities;

namespace eclipseonlineweb
{
    public partial class SalesPipeline : UMABasePage
    {
        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
        }

        public override void LoadPage()
        {
           LoadPendingAccountsGrid();
           ((HiddenField)Master.FindControl("hfMainMenuText")).Value = "WORKFLOW";
          
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            DataView filteredVew; 
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.Page.User.Identity.Name, "DBUser_1_1");
            IBrokerManagedComponent orgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            UMAFormsListingDS ds = new UMAFormsListingDS();
            orgCM.GetData(ds);

            DataTable accountsList = ds.Tables[UMAFormsListingDS.ACCOUNTSLISTTABLE];
            string rowFlter = string.Empty;
            rowFlter = "ClientID IN ('')";

            if (objUser.Name.Contains("auremovic") || objUser.Name == "tming" || objUser.Name == "shepworth" || objUser.Name == "Administrator" || objUser.Name == "bwestbrook@innovapm.com.au")
            {
                PendingAccountsGrid.Columns[PendingAccountsGrid.Columns.Count - 1].Visible = true;

                DataTable flteredTable = ds.Tables[UMAFormsListingDS.ACCOUNTSLISTTABLE];
                filteredVew = new DataView(flteredTable);
                filteredVew.RowFilter = rowFlter;
            }
            else if (objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA)
            {
                DataView view = new DataView(accountsList);
                view.RowFilter = "CreatedBy='" + this.User.Identity.Name + "'";

                DataTable flteredTable = view.ToTable();
                filteredVew = new DataView(flteredTable);
                filteredVew.RowFilter = rowFlter;
            }
            else
            {
                DataTable flteredTable = ds.Tables[UMAFormsListingDS.ACCOUNTSLISTTABLE];
                filteredVew = new DataView(flteredTable);
                filteredVew.RowFilter = rowFlter;
            }

            this.UMABroker.ReleaseBrokerManagedComponent(orgCM);
            this.UMABroker.ReleaseBrokerManagedComponent(objUser);

            ExcelHelper.ToExcel(filteredVew.ToTable(), "SalesPipeline-" + DateTime.Now.ToString("dd/MMM/yyyy") + ".xls", this.Page.Response);
        }

        protected override void Intialise()
        {
            base.Intialise();
            this.PendingAccountsGrid.RowEditing += new C1GridViewEditEventHandler(PendingAccountsGrid_RowEditing);
            this.PendingAccountsGrid.RowDeleting +=new C1GridViewDeleteEventHandler(PendingAccountsGrid_RowDeleting);
        }

        protected void PendingAccountsGrid_RowEditing(object sender, C1GridViewEditEventArgs e)
        {

        }

        protected void PendingAccountsGrid_RowDeleting(object sender, C1GridViewDeleteEventArgs e)
        {

        }
        

        protected void Grid_RowCommand(object sender, C1GridViewCommandEventArgs e)
        {
            Dictionary<string, string> SourceImportTransactionType = CashManagementEntity.GetImportTransactionDataSource();
            string appNo = string.Empty;

            if (e.CommandName == "edit")
            {
                appNo = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[0].Text;
                Response.Redirect("~/UMAForm/UMAOneFormProcess.aspx?AppNo=" + appNo);
            }
            else if (e.CommandName == "delete")
            {
                appNo = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[0].Text;
                this.UMABroker.SaveOverride = true;
                IBrokerManagedComponent orgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
                UMAFormDS ds = new UMAFormDS();
                ds.AppNo = appNo;
                ds.UMAFormDSOperation = UMAFormDSOperation.Delete;
                orgCM.SetData(ds);
                this.UMABroker.ReleaseBrokerManagedComponent(orgCM);
                this.UMABroker.SetComplete();
                this.UMABroker.SetStart();
                this.LoadPendingAccountsGrid();
            }

            else if (e.CommandName == "select")
            {
                appNo = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[0].Text;
                this.UMABroker.SaveOverride = true;
                IBrokerManagedComponent orgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
                UMAFormDS ds = new UMAFormDS();
                ds.AppNo = appNo;
                ds.UMAFormDSOperation = UMAFormDSOperation.CreateClient;
                orgCM.SetData(ds);
                this.UMABroker.ReleaseBrokerManagedComponent(orgCM);
                this.UMABroker.SetComplete();
                this.UMABroker.SetStart();
                this.LoadPendingAccountsGrid();
            }
        }

        protected void LoadPendingAccountsGrid()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.Page.User.Identity.Name, "DBUser_1_1");
            IBrokerManagedComponent orgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            UMAFormsListingDS ds = new UMAFormsListingDS();
            orgCM.GetData(ds);

            DataTable accountsList = ds.Tables[UMAFormsListingDS.ACCOUNTSLISTTABLE];
            string rowFlter = string.Empty;
            rowFlter = "ClientID IN ('')";

            if (objUser.Name.Contains("auremovic") || objUser.Name == "tming" || objUser.Name == "shepworth" || objUser.Name == "Administrator" || objUser.Name == "bwestbrook@innovapm.com.au")
            {
                PendingAccountsGrid.Columns[PendingAccountsGrid.Columns.Count - 1].Visible = true;

                DataTable flteredTable = ds.Tables[UMAFormsListingDS.ACCOUNTSLISTTABLE];
                DataView filteredVew = new DataView(flteredTable); 
                filteredVew.RowFilter = rowFlter;
                
                this.PendingAccountsGrid.DataSource = filteredVew.ToTable(); 
                PendingAccountsGrid.DataBind();
            }
            else if (objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA)
            {
                DataView view = new DataView(accountsList);
                view.RowFilter = "CreatedBy='" + this.User.Identity.Name + "'";

                DataTable flteredTable = view.ToTable();
                DataView filteredVew = new DataView(flteredTable);
                filteredVew.RowFilter = rowFlter;

                this.PendingAccountsGrid.DataSource = filteredVew.ToTable(); 
                PendingAccountsGrid.DataBind();
            }
            else
            {
                DataTable flteredTable = ds.Tables[UMAFormsListingDS.ACCOUNTSLISTTABLE];
                DataView filteredVew = new DataView(flteredTable);
                filteredVew.RowFilter = rowFlter;

                this.PendingAccountsGrid.DataSource = filteredVew.ToTable(); 
                PendingAccountsGrid.DataBind();
            }

            this.UMABroker.ReleaseBrokerManagedComponent(orgCM);
            this.UMABroker.ReleaseBrokerManagedComponent(objUser);
        }

        protected void PendingAccountsGrid_PageIndexChanging(object sender, C1GridViewPageEventArgs e)
        {
            this.PendingAccountsGrid.PageIndex = e.NewPageIndex;
            LoadPendingAccountsGrid();
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType ==  UserType.Client || objUser.UserType ==  UserType.Accountant)
                return false;
            else
                return true;
        }

        protected void FilterPendingAccountsGrid(object sender, C1GridViewFilterEventArgs e)
        {
            LoadPendingAccountsGrid();
        }

        protected void SortPendingAccountsGrid(object sender, C1GridViewSortEventArgs e)
        {
            LoadPendingAccountsGrid();
        }
    }
}
