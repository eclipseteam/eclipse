using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Oritax.TaxSimp.FileImport;

namespace eclipseonlineweb
{
    /// <summary>
    /// Summary description for FileImport.
    /// </summary>
    public class FileImport : UMABasePage
    {

        public override void LoadPage()
        {
            Guid gFileUploadID = new Guid(Request["FileID"].ToString());

            UMABroker.SetStart();

            // Import the stream into a FileImportSM
            FileImportSM smFileImport = (FileImportSM)this.UMABroker.GetBMCInstance(gFileUploadID);
            if (smFileImport != null)
            {

                FileImportDS dsFileImport = new FileImportDS();
                smFileImport.ExtractData(dsFileImport);


                Response.ContentType = "application/octet-stream";

                int iLastIndex = dsFileImport.OriginPath.LastIndexOf("\\");
                String sDownLoadFileName = "attachment; filename=";

                if (iLastIndex > 0)
                {
                    sDownLoadFileName += dsFileImport.OriginPath.Substring(iLastIndex + 1);
                }
                else
                {
                    sDownLoadFileName += dsFileImport.OriginPath;
                }

                Response.AddHeader("content-disposition", sDownLoadFileName);

                long lFileSize = dsFileImport.Stream.Length;
                byte[] byteContent = new byte[(int) lFileSize];
                dsFileImport.Stream.Read(byteContent, 0, (int) lFileSize);
                dsFileImport.Stream.Close();

                Response.BinaryWrite(byteContent);
            }
            UMABroker.SetComplete();
        }

    }
}