﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AttachExistingTransaction.aspx.cs"
    Inherits="eclipseonlineweb.ClientViews.AttachExistingTransaction" %>

<%@ Register Src="~/Controls/AttachExistingConfirmation.ascx" TagName="AttachExistingConfirmation"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Attach Existing Transaction</title>
    <link href="<%# Page.ResolveClientUrl("~/Styles/style.css")%>" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <script type="text/javascript">
            function CloseAndRebind() {
                GetRadWindow().BrowserWindow.refreshGrid();
                GetRadWindow().close();
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

                return oWindow;
            }
            function CancelEdit() {
                GetRadWindow().close();
            }
        </script>
        <asp:ScriptManager ID="ScriptManager" runat="server" AsyncPostBackTimeout="0">
        </asp:ScriptManager>
        <uc1:AttachExistingConfirmation ID="ConfirmationControl" runat="server" />
    </div>
    </form>
</body>
</html>
