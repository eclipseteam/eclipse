﻿using System;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.C1Menu;
using Oritax.TaxSimp.Utilities;
using System.Web.UI;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Services.CMBroker;
using System.Threading;

namespace eclipseonlineweb
{
    public partial class UMAFormMaster : System.Web.UI.MasterPage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (IsTestApp())
                Img1.Visible = false;
            else
                ImgTest.Visible = false;

            //hfIsTest.Value = IsTestApp().ToString().ToLower();
            Page.Header.DataBind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.UMABroker = new CMBroker(Context.User, DBConnection.Connection.ConnectionString);
            this.UMABroker.SetStart();

            if (NavigationMenu != null)
            {
                MenuItem adminmenu = this.NavigationMenu.FindItem("ADMINISTRATION");
                MenuItem workflow = this.NavigationMenu.FindItem("WORKFLOW");
                MenuItem setup = this.NavigationMenu.FindItem("INVESTMENT");
                DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.Page.User.Identity.Name, "DBUser_1_1");
                DBUserDetailsDS dbUserDetailsDS = new DBUserDetailsDS();
                objUser.GetData(dbUserDetailsDS);

                if (objUser.UserType == UserType.Advisor)
                {
                    ((Label)this.HeadLoginView.FindControl("lblAdv")).Visible = true;
                    ((Label)this.HeadLoginView.FindControl("lblAdv")).Text = " - (" + dbUserDetailsDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0][DBUserDetailsDS.ADVISERID].ToString() + ")";
                }


                if (this.Page.User.Identity.Name.ToLower() != "administrator")
                {
                    if (objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA)
                    {
                        this.NavigationMenu.Items.Remove(adminmenu);
                    }

                    if (objUser.UserType == UserType.Client || objUser.UserType == UserType.Accountant)
                    {
                        this.NavigationMenu.Items.Remove(workflow);
                        this.NavigationMenu.Items.Remove(adminmenu);
                        this.NavigationMenu.Items.Remove(setup);
                    }
                }


                if (IsTestApp() && (objUser.UserType == UserType.Advisor || this.Page.User.Identity.Name.ToLower() == "administrator" || objUser.UserType == UserType.Innova))
                {
                    MenuItem obp = new MenuItem();
                    obp.Text = "OBP";
                    obp.NavigateUrl = "~/OBP/Individuals.aspx";
                    if (!NavigationMenu.Items.Contains(obp))
                        NavigationMenu.Items.Add(obp);
                }
            }
        }

        private static bool IsTestApp()
        {
            return DBConnection.Connection.ConnectionString.Contains("Test");
        }

        public ICMBroker UMABroker
        {
            get
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (brokerSlot == null)
                    return null;

                return (ICMBroker)Thread.GetData(brokerSlot);
            }
            set
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (brokerSlot == null)
                    brokerSlot = Thread.AllocateNamedDataSlot("Broker");

                if (value == null)
                    Thread.FreeNamedDataSlot("Broker");

                Thread.SetData(brokerSlot, value);
            }
        }
        protected void MenuItemClick(object sender, C1MenuEventArgs e)
        {

        }

    }
}
