﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.CM.Organization.Data;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using C1.Web.Wijmo.Controls.C1Chart;

namespace eclipseonlineweb
{
    public partial class GroupsFUM : UMABasePage
    {
        private DataView _entityView;
        
        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            var excelDataset = new DataSet();
            PopulateData();
            _entityView = new DataView(PresentationData.Tables["Entities_Table"]) { RowFilter = "IsClient='true'", Sort = "ENTITYNAME_FIELD ASC" };
            DataTable entityTable = _entityView.ToTable();
            entityTable.Columns.Remove("ENTITYCLID_FIELD");
            entityTable.Columns.Remove("ENTITYCSID_FIELD");
            entityTable.Columns.Remove("ENTITYCIID_FIELD");
            entityTable.Columns.Remove("ENTITYCTID_FIELD");
            entityTable.Columns.Remove("ENTITYSCTYPE_FIELD");
            entityTable.Columns.Remove("ENTITYSCSTATUS_FIELD");
            entityTable.Columns.Remove("ENTITYSORT_FIELD");

            excelDataset.Merge(entityTable, true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, "Groups-FUM-[" + DateTime.Today.ToString("dd-MMM-yyyy") +"].xls", Page.Response);
        }
        
        protected void Filter(object sender, C1GridViewFilterEventArgs e)
        {
            e.Values[0] = ((string)e.Values[0]).Trim();
            _entityView = new DataView(PresentationData.Tables["Entities_Table"]) { RowFilter = "IsClient='true'", Sort = "ENTITYNAME_FIELD ASC" };
        }

        protected override void GetBMCData()
        {
            PopulateData();
        }

        protected void GenerateAccountsFUM(object sender, EventArgs e)
        {
            PopulateData();
        }

        private void PopulateData()
        {
            IBrokerManagedComponent iBMC = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            var ds = new OrganisationListingDS
                         {
                             OrganisationListingOperationType = OrganisationListingOperationType.ClientsWithFUM,
                             AsOfDate = C1FinancialSummaryDate.Date.HasValue ? C1FinancialSummaryDate.Date.Value : DateTime.Now
                         };

            iBMC.GetData(ds);
            PresentationData = ds;
            UMABroker.ReleaseBrokerManagedComponent(iBMC);
            BindControls(ds);
        }

        private void BindControls(DataSet ds)
        {
            _entityView = new DataView(ds.Tables["Entities_Table"]) { RowFilter = "IsClient='true'", Sort = "ENTITYNAME_FIELD ASC" };
            DataTable entityTable = _entityView.ToTable();

            double holdingTotal = Convert.ToDouble(entityTable.Select().Sum(row => Convert.ToDecimal(row["Holding"])));
            double unsettled = Convert.ToDouble(entityTable.Select().Sum(row => Convert.ToDecimal(row["Unsettled"])));
            double total = Convert.ToDouble(entityTable.Select().Sum(row => Convert.ToDecimal(row["Total"])));
       }

        public override void LoadPage()
        {
            base.LoadPage();

            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
                sm.RegisterPostBackControl(btnDownload);
        }

        public override void PopulatePage(DataSet ds)
        {
            _entityView = new DataView(ds.Tables["Entities_Table"]) { RowFilter = "IsClient='true'", Sort = "ENTITYNAME_FIELD ASC" };
            DataTable entityTable = _entityView.ToTable();
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            PresentationGrid.DataSource = _entityView.ToTable();
        }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                string Ins = e.CommandArgument.ToString();
                Response.Redirect("ClientViews/ClientMainView.aspx?INS=" + Ins + "&PageName=" + "GroupsFUM");
            }
        }
    }
}
