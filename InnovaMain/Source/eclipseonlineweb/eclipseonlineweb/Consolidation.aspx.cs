﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.CM.Organization.Data;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using C1.Web.Wijmo.Controls.C1Chart;
using Oritax.TaxSimp.Security;
using System.Collections;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Common;

namespace eclipseonlineweb
{
    public partial class Consolidation : UMABasePage
    {
        private DataView _entityView;
        
        protected void DownloadXLS(object sender, EventArgs e)
        {
            var excelDataset = new DataSet();
            PopulateData();
            _entityView = new DataView(PresentationData.Tables["Entities_Table"]) { RowFilter = "IsClient='true'", Sort = "ENTITYNAME_FIELD ASC" };
            DataTable entityTable = _entityView.ToTable();
            entityTable.Columns.Remove("ENTITYCLID_FIELD");
            entityTable.Columns.Remove("ENTITYCSID_FIELD");
            entityTable.Columns.Remove("ENTITYCIID_FIELD");
            entityTable.Columns.Remove("ENTITYCTID_FIELD");
            entityTable.Columns.Remove("ENTITYSCTYPE_FIELD");
            entityTable.Columns.Remove("ENTITYSCSTATUS_FIELD");
            entityTable.Columns.Remove("ENTITYSORT_FIELD");

            excelDataset.Merge(entityTable, true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, "ConsolidationEntities-" + DateTime.Today.ToString("dd-MMM-yyyy") +".xls", Page.Response);
        }
        
        protected void Filter(object sender, C1GridViewFilterEventArgs e)
        {
            e.Values[0] = ((string)e.Values[0]).Trim();
            _entityView = new DataView(PresentationData.Tables["Entities_Table"]) { RowFilter = "IsClient='true'", Sort = "ENTITYNAME_FIELD ASC" };
        }

        protected override void GetBMCData()
        {
            PopulateData();
        }

        protected void GenerateAccountsFUM(object sender, EventArgs e)
        {
            PopulateData();
        }

        private void PopulateData()
        {
            IBrokerManagedComponent iBMC = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            OrganisationListingDS ds = new OrganisationListingDS();
            ds.OrganisationListingOperationType = OrganisationListingOperationType.ConsolidationEntities;
            iBMC.GetData(ds);
            PresentationData = ds;
            UMABroker.ReleaseBrokerManagedComponent(iBMC);
        }

        public override void LoadPage()
        {
            base.LoadPage();

            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");

            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
                this.PresentationGrid.MasterTableView.CommandItemSettings.ShowAddNewRecordButton = false; 

     }

        public override void PopulatePage(DataSet ds)
        {
            _entityView = new DataView(ds.Tables["Entities_Table"]) {Sort = "ENTITYNAME_FIELD ASC" };
            DataTable entityTable = _entityView.ToTable();
        }

        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            GridEditFormInsertItem EditForm = (GridEditFormInsertItem)e.Item;

            string name = ((TextBox)(((GridEditableItem)(e.Item))["ENTITYNAME_FIELD"].Controls[0])).Text;

            if (name != string.Empty)
            {
                this.UMABroker.SaveOverride = true;
                IOrganization orgCM = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                ILogicalModule created = null;
                ILogicalModule organization = null;
                created = this.UMABroker.CreateLogicalCM("ConsoleClient", name, Guid.Empty, null);
                organization = this.UMABroker.GetLogicalCM(orgCM.CLID);
                organization.AddChildLogicalCM(created);
                IOrganizationUnit orgUNIT = (IOrganizationUnit)created[created.CurrentScenario];
                orgUNIT.Name = name;
                if (orgUNIT.ClientEntity == null)
                    orgUNIT.ClientEntity = new ConsoleClient(); 
                ConsoleClient consolClient = orgUNIT.ClientEntity as ConsoleClient;
                consolClient.Name = name;
                consolClient.LegalName = name;
                orgUNIT.CreateUniqueID();
                orgUNIT.IsInvestableClient = true;
                UMABroker.ReleaseBrokerManagedComponent(orgCM);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                Response.Redirect(Request.Url.AbsoluteUri);
            }
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            PresentationGrid.DataSource = _entityView.ToTable();
        }

        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                string Ins = e.Item.Cells[3].Text;
                Response.Redirect("Consolidation/ConsolMainView.aspx?INS=" + Ins + "&PageName=" + "AccountFUM");
            }
            else if (e.CommandName.ToLower() == "delete")
            {
                e.Item.Edit = false;
                e.Canceled = true;
                var requestInstanceId = new Guid(e.Item.Cells[3].Text);
                UMABroker.SaveOverride = true;
                UMABroker.DeleteCMInstance(requestInstanceId);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                Response.Redirect(Request.Url.AbsoluteUri);
            }

            else if (e.CommandName.ToLower() == "update")
            {
                e.Item.Edit = false;
                e.Canceled = true;
                string name = ((TextBox)(((GridEditableItem)(e.Item))["ENTITYNAME_FIELD"].Controls[0])).Text;

                if(name != string.Empty) 
                {
                    var requestInstanceId = new Guid(e.Item.Cells[3].Text);
                    UMABroker.SaveOverride = true;
                    IOrganizationUnit unit = UMABroker.GetBMCInstance(requestInstanceId) as IOrganizationUnit;
                    unit.Name = name;
                    if (unit.ClientEntity == null)
                        unit.ClientEntity = new ConsoleClient(); 
                    ConsoleClient consolClient = unit.ClientEntity as ConsoleClient;
                    consolClient.Name = name;
                    consolClient.LegalName = name;
                    UMABroker.SetComplete();
                    UMABroker.SetStart();
                    Response.Redirect(Request.Url.AbsoluteUri);
                }
            }
        }
    }
}
