﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Individuals.aspx.cs" Inherits="eclipseonlineweb.OBP.Individuals"
    MasterPageFile="OBP.Master" %>

<%@ Register TagPrefix="uc" TagName="IndividualControl" Src="~/Controls/Individual.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <style type="text/css">
        .holder
        {
            width: 100%;
            display: block;
            z-index: 6;
        }
        .content
        {
            background: #fff;
            z-index: 7; /*  padding: 28px 26px 33px 25px;*/
        }
        .popup
        {
            border-radius: 7px;
            background: #6b6a63;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 900px;
            top: 20%;
            left: 50%;
            margin-left: -400px;
            margin-top: -40px;
            z-index: 6;
        }
        .overlay
        {
            width: 100%;
            opacity: 0.65;
            height: 100%;
            left: 0; /*IE*/
            top: 10px;
            text-align: center;
            z-index: 5;
            position: fixed;
            background-color: #444444;
        }
    </style>
    <fieldset>
        <legend>Search Individual </legend>
        
        <asp:TextBox ID="txtName" runat="server" Text=""></asp:TextBox>
        <telerik:RadButton runat="server" ID="btnSearch" ToolTip="Search Individuals"
            Width="70px" Height="20px" AutoPostBack="true" OnClick="btnSearch_Click" >
            <ContentTemplate>
                <img src="../images/search.png" alt="" style="vertical-align: middle; height: 15px; margin-right: 4px" />
                <span>Search</span>
            </ContentTemplate>
        </telerik:RadButton>
        <telerik:RadButton runat="server" ID="btnNewIndividual" ToolTip="Create an Individual" Width="140px" Height="20px" AutoPostBack="true" OnClick="lnkbtnAddASX_Click" >
            <ContentTemplate>
                <img src="../images/OBP/newuser-icon.png" alt="" style="vertical-align: middle; height: 15px; margin-right: 4px" />
                <span>Add New Individual</span>
            </ContentTemplate>
        </telerik:RadButton>
    </fieldset>
 <%--
    CurrentFilterFunction="Contains" FilterControlWidth="50px"
                        ItemStyle-Width="15%" HeaderStyle-Width="8%" HeaderButtonType="TextButton" ShowFilterIcon="true"--%>

    <div id="dvIndividuals" runat="server" style="width: 100%">
         <telerik:RadGrid EnableViewState="true" ID="RGDIndividuals" runat="server" ShowStatusBar="true"  
            AutoGenerateColumns="false" Width="100%" PageSize="10" AllowPaging="true" AllowSorting="true"
            OnItemCommand="RGDIndividuals_OnItemCommand" OnNeedDataSource="RGDIndividuals_OnNeedDataSource">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowMultiColumnSorting="True" CommandItemDisplay="Top" TableLayout="Fixed"
                EnableViewState="true" AllowFilteringByColumn="true" AllowSorting="true">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false"
                    ShowExportToWordButton="false" ShowExportToPdfButton="false"></CommandItemSettings>
                <Columns>
                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                        UniqueName="EditColumn" HeaderText="Edit" ImageUrl="../images/OBP/edit-profile-icon.png">
                        <HeaderStyle Width="5%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                    </telerik:GridButtonColumn>
                    <telerik:GridHyperLinkColumn Text="Projections" DataNavigateUrlFields="Cid" UniqueName="SelectIndividual"
                        DataNavigateUrlFormatString="AdviceListing.aspx?ins={0}" HeaderText="Select"
                        AutoPostBackOnFilter="false" ItemStyle-Width="15%"
                        Visible="true">
                    </telerik:GridHyperLinkColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" SortExpression="Cid" HeaderText="Cid"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Cid" UniqueName="Cid" ReadOnly="true"
                        Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" SortExpression="Clid" HeaderText="Clid"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Clid" UniqueName="Clid" ReadOnly="true"
                        Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" SortExpression="Csid" HeaderText="Csid"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Csid" UniqueName="Csid" ReadOnly="true"
                        Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" SortExpression="AdviserCid" HeaderText="AdviserCid"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="AdviserCid" UniqueName="AdviserCid"
                        ReadOnly="true" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" SortExpression="AdviserClid" HeaderText="AdviserClid"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="AdviserClid" UniqueName="AdviserClid"
                        ReadOnly="true" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" SortExpression="AdviserCsid" HeaderText="AdviserCsid"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="AdviserCsid" UniqueName="AdviserCsid"
                        ReadOnly="true" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                        SortExpression="ClientId" HeaderText="Client ID" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="ClientId" UniqueName="ClientId" ReadOnly="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                        SortExpression="Name" HeaderText="Given Name" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Name" UniqueName="Name"
                        ReadOnly="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                        SortExpression="MiddleName" HeaderText="Middle Name" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="MiddleName" UniqueName="MiddleName" ReadOnly="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                        SortExpression="SurName" HeaderText="Family Name" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="SurName" UniqueName="SurName" ReadOnly="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="170px" HeaderStyle-Width="25%" ItemStyle-Width="25%"
                        SortExpression="EmailAddress" HeaderText="Email" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="EmailAddress" UniqueName="EmailAddress" ReadOnly="true">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                <Excel Format="Html"></Excel>
            </ExportSettings>
        </telerik:RadGrid>
    </div>
    <div id="IndividualModal" runat="server" visible="false" class="holder">
        <div class="popup">
            <div class="content">
                <uc:IndividualControl ID="IndividualControl" runat="server" />
            </div>
        </div>
    </div>
    <div id="OVER" runat="server" class="overlay" visible="False">
    </div>
   
</asp:Content>
