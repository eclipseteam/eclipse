﻿<%@ Page Title="e-Clipse Online Portal - OBP" Language="C#" MasterPageFile="OBP.Master"
    AutoEventWireup="true" CodeBehind="AdviceListing.aspx.cs" Inherits="eclipseonlineweb.OBP.AdviceListing" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <link rel="stylesheet" href="../Styles/OBP/style.css" />
    <asp:HiddenField ID="hfAdviceID" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hfIndividualClientID" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hfIndividualClientName" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hfIndividualClientDOB" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hfIndividualClientGender" runat="server" EnableViewState="true" />
    <div>
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <telerik:RadButton runat="server" ID="hrefAddNewAdvice" ToolTip="Add New Projection"
                            Width="140px" Height="20px" AutoPostBack="False">
                            <ContentTemplate>
                                <img src="../images/OBP/add-icon.jpg" alt="" style="vertical-align: middle; height: 15px;" />
                                <span>Add New Projection</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </td>
                    <td style="width: 30%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb2" runat="server" />
                        <br />
                        <uc2:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    <div>
     <h2>Projections</h2>
        <p>
            <telerik:RadGrid ID="AdvicesGrid" runat="server" PageSize="20" AllowPaging="true"
                GridLines="Horizontal" EnableViewState="True" ShowFooter="True" OnNeedDataSource="AdvicesGrid_OnNeedDataSource"
                OnItemCommand="AdvicesGrid_OnItemCommand" OnItemDataBound="AdvicesGrid_OnItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AutoGenerateColumns="false" DataKeyNames="CID,ID">
                    <Columns>
                        <telerik:GridHyperLinkColumn UniqueName="EditAdvice" HeaderText="Edit" Text="Edit"
                            AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" FilterControlWidth="50px"
                            HeaderStyle-Width="1%" HeaderButtonType="TextButton" ShowFilterIcon="true" ImageUrl="../images/OBP/edit-icon.png">
                        </telerik:GridHyperLinkColumn>
                        <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" Text="Delete"
                            HeaderText="Delete" ConfirmText="Are you sure you want to delete this advice?"
                            UniqueName="DeleteColumn">
                            <HeaderStyle Width="1%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn FilterControlWidth="120px" HeaderStyle-Width="5%" ItemStyle-Width="5%"
                            HeaderText="Individual ClientId" HeaderButtonType="None" DataField="IndividualClientID"
                            UniqueName="IndividualClientID">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="120px" HeaderStyle-Width="15%" ItemStyle-Width="5%"
                            HeaderText="Title" HeaderButtonType="None" DataField="TITLE" UniqueName="Title">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="120px" HeaderStyle-Width="5%" ItemStyle-Width="5%"
                            HeaderText="Modified On" HeaderButtonType="None" DataField="Modifiedon" UniqueName="Modifiedon">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </p>
    </div>
</asp:Content>
