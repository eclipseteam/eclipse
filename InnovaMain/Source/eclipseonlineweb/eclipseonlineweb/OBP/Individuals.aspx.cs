﻿using System;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;

namespace eclipseonlineweb.OBP
{
    public partial class Individuals : UMABasePage
    {
        #region Private Variables
        private string _cid;
        #endregion

        #region Page Events

        protected override void OnInit(EventArgs e)
        {
            IndividualControl.Canceled += () => HideShowIndividualGrid(false);
            IndividualControl.SaveData += (Cid, ds) =>
            {
                if (Cid == Guid.Empty.ToString())
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(Cid, ds);
                }
                HideShowIndividualGrid(false);
                GetIndividuals(txtName.Text);
            };

            IndividualControl.ValidationFailed += () => HideShowIndividualGrid(true);

        }

        public override void LoadPage()
        {
            base.LoadPage();
            _cid = Request.QueryString["ins"];
            if (!IsPostBack)
            {
                if (Session["Search_Individual_By"] != null && (string.Compare(Session["Search_Individual_By"].ToString(), txtName.Text) != 0))
                {
                    txtName.Text = Session["Search_Individual_By"].ToString();
                    GetIndividuals(txtName.Text);
                }
                else
                {
                    if (!this.GetCurrentUser().IsAdmin)
                        GetIndividuals(string.Empty);
                }
            }
            Session["AdvicesListingURL"] = null;
        }

        #endregion

        #region Control Events

        protected void RGDIndividuals_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var Cid = new Guid(dataItem["Cid"].Text);
                    IndividualModal.Visible = true;
                    OVER.Visible = true;
                    IndividualControl.SetEntity(Cid);
                    break;
            }

        }


        protected void RGDIndividuals_OnPageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            GetIndividuals(txtName.Text);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Session["Search_Individual_By"] = txtName.Text;
            //GetIndividuals(txtName.Text);
            RGDIndividuals.Rebind();
        }
        
        protected void lnkbtnAddASX_Click(object sender, EventArgs e)
        {
            IndividualModal.Visible = true;
            OVER.Visible = true;
            IndividualControl.SetEntity(Guid.Empty);
        }

        #endregion

        #region Private Methods

        private void HideShowIndividualGrid(bool show)
        {
            OVER.Visible = IndividualModal.Visible = show;
        }

        private void GetIndividuals(string text)
        {
            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            IndividualDS allIndividuals = new IndividualDS();
            allIndividuals.Command = (int)WebCommands.GetOrganizationUnitsByType;
            allIndividuals.Unit = new OrganizationUnit { Name = text, CurrentUser = (Page as UMABasePage).GetCurrentUser(), Type = ((int)OrganizationType.Individual).ToString() };
            org.GetData(allIndividuals);
            DataView summaryView = new DataView(allIndividuals.Tables[allIndividuals.IndividualTable.TABLENAME]);
            summaryView.Sort = allIndividuals.IndividualTable.NAME + " ASC";
            RGDIndividuals.DataSource = summaryView;

            UMABroker.ReleaseBrokerManagedComponent(org);
            //RGDIndividuals.Rebind();
        }

        #endregion

        protected void RGDIndividuals_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (IsPostBack)
                GetIndividuals(txtName.Text);
        }
    }
}