﻿function ShowAssetGraph() {

    Math.roundNumber = function(number, precision) {
        precision = Math.abs(parseInt(precision)) || 0;
        var coefficient = Math.pow(10, precision);
        return Math.round(number * coefficient) / coefficient;
    };


    $.ajax({
        type: "POST",
        url: "Advices.aspx/GetProjectionAsJSON",
        contentType: "application/json",
        dataType: "json",
        data: "{ 'GraphType': 'Asset'}",
        success: function (graphData) {
            //console.log(graphData);
            if (graphData.d !== 'Error') {
                var data = JSON.parse(graphData.d);
                console.log("ASSETALL");
                console.log(data);

                //if (parseInt(data.majorUnit) > 1000)
                //    data.majorUnit = parseInt(data.majorUnit).toFixed(-3);
                //else if (parseInt(data.majorUnit) > 100000)
                //    data.majorUnit = parseInt(data.majorUnit).toFixed(-4);
                PlotAssetGraph2(data.CurrentAge, data.d, data.majorUnit, data.min, data.max);
            }
        }
    });
}



function PlotAssetGraph2(currentAge, json, majorUnit, minValue, maxValue) {

    var categoryAxisData = [];
    $.each(json, function (i, item) {

        //console.log(item.Timestep);
        var age = parseInt(item.Timestep) + parseInt(currentAge);
        if (categoryAxisData.indexOf(age) == -1)
            categoryAxisData.push(age);
    });

    var valueAxisOptions = {
        labels: {
            visible: true,
            template: "$#=kendo.format('{0:N0}',value)#"
        },
        majorUnit: majorUnit,
        max: maxValue,
        min: minValue,
        title: { text: "Assets Value" }
    };

    if (maxValue == "0" || majorUnit == "0") {
        valueAxisOptions = { title: { text: "Assets Value"} };
    }
    
    //console.log("categoryAxisData");
    //console.log(categoryAxisData);

    var percentiles = [];
    $.each(json, function (i, item) {

        if (percentiles.indexOf(item.Percentile) == -1) {
            percentiles.push(item.Percentile);
        }

    });

    //console.log("percentiles");
    //console.log(percentiles);

    var seriesAsPercentileData = [];
    $.each(percentiles, function (i, percentile) {
        var color = getAssetsPercentileColor(percentile);

        var legend;
        if (percentile == '10' || percentile == '90')
            legend = 'Less Likely';
        else if (percentile == '50')
            legend = 'Most Likely';
        else
            legend = '';

        var series = { "name": legend, "data": [], "color": color, "tooltipDisplayData": [] };

        $.each(json, function (j, value) {
            if (value.Percentile == percentile) {

                //console.log(value.Value);
                //console.log(Math.round(value.Value));

                series.data.push(Math.round(value.Value));
                series.tooltipDisplayData.push(Math.round(value.Value));
            }
        });

        seriesAsPercentileData.push(series);
    });

    //Setting max value for assets graph 2--------------------------------------------------------------------------------------
    var maxAxisValue = new Array();
    for (var mValue = 0; mValue < seriesAsPercentileData.length; mValue++)
        maxAxisValue.push(Math.max.apply(null, seriesAsPercentileData[mValue].data));

    valueAxisOptions.max = Math.max.apply(null, maxAxisValue);
    valueAxisOptions.max = valueAxisOptions.max + parseInt(parseFloat(valueAxisOptions.max * 0.10));
    valueAxisOptions.majorUnit = parseInt(parseFloat(valueAxisOptions.max * 0.16));
    valueAxisOptions.majorUnit = roundValue(valueAxisOptions.majorUnit + "");

    var dataMax = GetLocalStorage("AssetGraphScaleMaxValue");
    if (dataMax != null)
        if (parseInt(dataMax) > parseInt(valueAxisOptions.max))
            valueAxisOptions.max = dataMax;

    StoreLocally('AssetGraphScaleMaxValue', valueAxisOptions.max);
    //------------------------------------------------------------------------------------------------------------------

    seriesAsPercentileData = TransformSeriesData(percentiles, seriesAsPercentileData);

    // console.log("seriesAsPercentileData");
    // console.log(seriesAsPercentileData);
    var ds = new kendo.data.DataSource({
        change: function (e) {
            var data = this.data();
            var series = [];
            for (var i = 0; i < data.length; i++) {
                var oneSeries = data[i];
                series.push({
                    data: oneSeries.data,
                    tooltipDisplayData: deepCopy(oneSeries.tooltipDisplayData),
                    name: oneSeries.name,
                    color: oneSeries.color,
                    border: {
                        color: oneSeries.color
                    }
                });
            }
            var chart = $("#chart_Asset").data('kendoChart');
            chart.options.series = series;
            chart.refresh();
        }
    });

    $("#chart_Asset").kendoChart({
        title: {
            text: "Total Wealth Distribution"
        },
        legend: {
            position: "top"
        },
        categoryAxis: {
            categories: categoryAxisData,
            title: {
                text: "Age"
            }
              ,
            labels: {
                step: 5
            }
        },
        seriesDefaults: {
            overlay: {
                gradient: "glass"
            },
            type: "column",
            stack: true
        },
        valueAxis: valueAxisOptions,
        tooltip: {
            visible: true,
            //template: "Age:#=category#, Wealth:$#=kendo.format('{0:N0}', value)#"
            //template: "Age:#=category#, Original:#=console.log(data)#, Transformed:#=console.log(data.series.tooltipDisplayData)#",
            template: "Age:#=category#, Wealth:$#=kendo.format('{0:N0}', data.series.data.indexOf(value) == -1 ? value : data.series.tooltipDisplayData[data.series.data.indexOf(value)])#"
        },
        series: [{ data: [], name: 'Loading...', tooltipDisplayData: []}]
    });

    ds.data(seriesAsPercentileData);
}

//Changing the base point of drawing for each percentile
function TransformSeriesData(percentiles, seriesAsPercentileData) {
    if (percentiles == null || percentiles.length <= 0) return null;
    if (seriesAsPercentileData == null || seriesAsPercentileData.length <= 0) return null;

    $.each(percentiles, function (p, percentile) {
        if (p < percentiles.length - 1) {
            for (var s = 0; s < seriesAsPercentileData[percentiles.length - 1].data.length; s++) {
                if (seriesAsPercentileData[percentiles.length - p - 1].data[s] >= seriesAsPercentileData[percentiles.length - p - 2].data[s]) {
                    seriesAsPercentileData[percentiles.length - p - 1].data[s] = seriesAsPercentileData[percentiles.length - p - 1].data[s] - seriesAsPercentileData[percentiles.length - p - 2].data[s];
                }
            }
        }
    });

    return seriesAsPercentileData;
}