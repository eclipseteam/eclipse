﻿function ShowProbabilityGraph() {

    $.ajax({
        async: false,
        type: "POST",
        url: "Advices.aspx/GetProjectionAsJSON",
        contentType: "application/json",
        dataType: "json",
        data: "{ 'GraphType': 'Probability'}",
        success: function (graphData) {
            if (graphData.d !== 'Error') {
                var data = JSON.parse(graphData.d);
                console.log("PROBABILTY");
                console.log(data);
                StoreLocally('Probability', data);
                //PlotProbabilityGraph(data, data.categoryAxis);
                ShowProbability(null);
            }
        }
    });
}

function ShowProbability(step) {
    if (step == null)
        step = $(".txtNumTimeSteps").val();
    var showAll = $("#chkShowAllNumTimeSteps").prop('checked');
    if (showAll == true)
        step = -1;
    
    if (step == null || step == "" || parseInt(step) == 0)
        step = -1;
   
    var data = GetLocalStorage('Probability');
    data.TargetStep = step;
    PlotProbabilityGraph(data, data.categoryAxis);
}

function PlotProbabilityGraph(json, categoryAxisData) {
    
    var currentAge = json.CurrentAge;
    var targetStep = json.TargetStep;
    var seriesAllGoal = [];
    var colors = ['Purple', 'Gray', 'Blue', 'Orange', 'Brown'];
    
    //Order the data in the following sequence -- Wealth, Income, Lump Sum, Liquid Assets, Bequest------------------
    if (categoryAxisData != null && categoryAxisData != undefined && categoryAxisData.length > 0) {

        var wealthData = new Array();
        var incomeData = new Array();
        var lumpSumData = new Array();
        var liquidAssetsData = new Array();
        var bequestData = new Array();

        for (var d = 0; d < categoryAxisData.length; d++) {

            if (categoryAxisData[d] == "Wealth") {
                wealthData.push(categoryAxisData[d]);
            }
            if (categoryAxisData[d] == "Income") {
                incomeData.push(categoryAxisData[d]);
            }
            if (categoryAxisData[d] == "LumpSum") {
                lumpSumData.push(categoryAxisData[d]);
            }
            if (categoryAxisData[d] == "Liquidity") {
                liquidAssetsData.push(categoryAxisData[d]);
            }
            if (categoryAxisData[d] == "Bequest") {
                bequestData.push(categoryAxisData[d]);
            }
        }

    }
    //--------------------------------------------------------------------------------------------------------------

    $.each(json.d, function (g, goal) {
        var goalColor = colors[g];
        var found = false;

        $.each(goal.d, function (j, item) {

            var age = parseInt(item.Timestep) + parseInt(currentAge);
            if (age == json.TargetStep) {
                found = true;
                var val = parseInt(Math.round(item.Value * 100));
                seriesAllGoal.push({ "value": val, "color": goalColor });
            }

        });
        if (!found)
            seriesAllGoal.push({ "value": 0, "color": goalColor });

    });

    var toolTipText = "#=category# Probability: #=value# %";
    if (targetStep == -1)
        toolTipText = "#=category# - Probability at All Times: #=value# %";
    else
        toolTipText = "#=category# - Probability at age " + targetStep + ": #=value# %";

    $("#chart_Probability").kendoChart({
        dataSource: {
            data: seriesAllGoal
        },
        title: { text: "Probability of Meeting Goals" },
        legend: { position: "top" },
        seriesDefaults: { type: "column",
            labels: {
                visible: true,
                background: "transparent", format: "{0}%"
            }
        },
        series: [{ field: "value",
            colorField: "color"
        }
                            ],
        categoryAxis: {
            categories: categoryAxisData,
            title: { text: "Goals" },
            labels: { rotation: 320 }
        },
        valueAxis: {
            labels: { format: "{0}%" },
            majorUnit: 10,
            max: 100,
            min: 0
        },
        tooltip: { visible: true, template: toolTipText }
    });
}