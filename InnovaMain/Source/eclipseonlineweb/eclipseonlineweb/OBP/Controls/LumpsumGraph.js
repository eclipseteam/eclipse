﻿function ShowLumpSumGraph() {
    $.ajax({
        type: "POST",
        url: "Advices.aspx/GetProjectionAsJSON",
        contentType: "application/json",
        dataType: "json",
        data: "{ 'GraphType': 'LumpSum'}",
        success: function (graphData) {
            if (graphData.d !== 'Error') {
                var data = JSON.parse(graphData.d);
                console.log("LUMPSUM");
                console.log(data);
                PlotLumpSumGraph(data.CurrentAge, data.d, data.majorUnit, data.min, data.max);
            }
        }
    });
}

function PlotLumpSumGraph(currentAge,json, majorUnit, minValue, maxValue) {
    
    var categoryAxisData = [];
    $.each(json, function (i, item) {
        if (parseInt(item.Timestep) != -1) 
        {
            var age = parseInt(item.Timestep) + parseInt(currentAge);
            if (categoryAxisData.indexOf(age) == -1)
                categoryAxisData.push(age);
        }
    });

    //Added due to the axis-Value scaling --------------------------------------------------------------------
    var valueAxisOptions = {
        majorUnit: majorUnit,
        max: maxValue,
        min: minValue,
        title: { text: "Value" }
    };

    if (maxValue == "0" || majorUnit == "0") {
        valueAxisOptions = { title: { text: "Value"} };
    }
    //--------------------------------------------------------------------------------------------------------
    
    var percentiles = [];
    $.each(json, function (i, item) {
    if (percentiles.indexOf(item.Percentile) == -1) {
            percentiles.push(item.Percentile);
        }
    });

    var seriesAsPercentileData = [];
    $.each(percentiles, function (p, percentile) {
        
        var color = getLumpSumPercentileColor(percentile);

        //var series = { "name": (percentile == 0 ? "All ages" : percentile + " Percentile"), "data": [], "color": color };
        var legend;
        if (percentile == '10' || percentile == '90')
            legend = 'Less Likely';
        else if (percentile == '50')
            legend = 'Most Likely';

        var series = { "name": legend, "data": [], "color": color };
        
        $.each(json, function (j, value) {
            if (value.Percentile == percentile && parseInt(value.Timestep) != -1) {
                series.data.push(value.Value);
            }
        });

        seriesAsPercentileData.push(series);
    });


    //Setting max value for Lump Sum graph --------------------------------------------------------------------------------------
    var maxAxisValue = new Array();
    for (var mValue = 0; mValue < seriesAsPercentileData.length; mValue++)
        maxAxisValue.push(Math.max.apply(null, seriesAsPercentileData[mValue].data));

    valueAxisOptions.max = Math.max.apply(null, maxAxisValue);
    valueAxisOptions.max = valueAxisOptions.max + parseInt(parseFloat(valueAxisOptions.max * 0.10));
    valueAxisOptions.max = valueAxisOptions.max * percentiles.length;
    valueAxisOptions.majorUnit = parseInt(parseFloat(valueAxisOptions.max * 0.16));
    valueAxisOptions.majorUnit = roundValue(valueAxisOptions.majorUnit + "");
    //------------------------------------------------------------------------------------------------------------------
    
    var ds = new kendo.data.DataSource({
        change: function (e) {
            var data = this.data();
            var series = [];
            for (var d = 0; d < data.length; d++) {
                var oneSeries = data[d];
                series.push({
                    data: oneSeries.data,
                    name: oneSeries.name,
                    color: oneSeries.color
                });
            }
            var chart = $("#chart_LumpSum").data('kendoChart');
            chart.options.series = series;
            chart.refresh();
        }
    });

    $("#chart_LumpSum").kendoChart({
        title: {
            text: "Lump Sum Distribution"
        },
        legend: {
            position: "top"
        },
        categoryAxis: {
            categories: categoryAxisData,
            title: {
                text: "Age"
            },
            labels: {
                step: 5
            }
        },
        seriesDefaults: {
            overlay: {
                gradient: "glass"
            },
            type: "column",
            stack: true
        },
        valueAxis: valueAxisOptions,
        tooltip: {
            visible: true,
            template: "Age:#=category#, Wealth:$#=kendo.format('{0:N0}',value)#"
        },
        series: [{ data: [], name: 'Loading...'}]
    });

    ds.data(seriesAsPercentileData);
}
