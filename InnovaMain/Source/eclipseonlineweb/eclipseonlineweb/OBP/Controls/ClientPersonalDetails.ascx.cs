﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.DataSets;
using System.Data;

namespace eclipseonlineweb.OBP.Controls
{
    public partial class ClientPersonalDetails : System.Web.UI.UserControl
    {
        #region Control Level Properties
        //Personal Details ------------------------------------------------------------------------------------
        public string ClientName
        {
            get
            {
                if( !this.IsValidated) 
                    throw new ValidationException(this.ValidationMessage);
                
                return this.txtPDName.Text;
            } 
            set { this.txtPDName.Text = value; }
        }
        public DateTime? DateOfBirth 
        {
            get
            {
                if (!this.IsValidated)
                    throw new ValidationException(this.ValidationMessage);
                return this.txtPDDOB.SelectedDate;
            }
            set { this.txtPDDOB.SelectedDate = value; } 
        }
        public int RetirementAge
        {
            get
            {
                if (!this.IsValidated)
                    throw new ValidationException(this.ValidationMessage);
                return Convert.ToInt32(this.txtPDRetirementAge.Value.HasValue ? txtPDRetirementAge.Value : 0);
            }
            set { this.txtPDRetirementAge.Value = value; } 
        }
        public int PlanAge 
        {
            get
            {
                if (!this.IsValidated)
                    throw new ValidationException(this.ValidationMessage);
                return Convert.ToInt32(this.txtPDPlanAge.Value);
            }
            set { this.txtPDPlanAge.Value = value; } 
        }
        public string Gender 
        {
            get
            {
                if (!this.IsValidated)
                    throw new ValidationException(this.ValidationMessage);
                return this.txtPDGender.Text;
            }
            set { this.txtPDGender.Text = value; }
        }
        public decimal Salary 
        {
            get
            {
                if (!this.IsValidated)
                    throw new ValidationException(this.ValidationMessage);
                return this.txtPDSalary.Value.HasValue ? Convert.ToDecimal(this.txtPDSalary.Value) : 0;
            }
            set { this.txtPDSalary.Value = Convert.ToDouble(value); }
        }
        //-----------------------------------------------------------------------------------------------------
        
        //Validation 
        public string ValidationMessage { get; set; }
        public bool IsValidated { get; set;}
        public bool ClientSideValidationRequired
        {
            set { this.hfClientSideValidationRequired.Value = value ? "1" : "0"; }
            get
            {
                if (string.IsNullOrEmpty(this.hfClientSideValidationRequired.Value) ||
                    this.hfClientSideValidationRequired.Value == "0") return false;
                return true;
            }
        }

        #endregion

        #region Private Methods

        public void SetPersonalDetailsDefaultFields(IndividualDS ds)
        {
            if (ds == null)
                throw new ArgumentNullException();
            if (ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                throw new InvalidDataException("No data was provided for Personal Details default values");
            
            DataRow dr = ds.IndividualTable.Rows[0];

            litGetDate.Text = "Current Age:";
            //hidCurrentAge.Value = string.Empty;

            if (!(dr[ds.IndividualTable.DOB] is DBNull))
            {
                DateTime d = (DateTime)dr[ds.IndividualTable.DOB];

                if (DateTime.Compare(d, txtPDDOB.MinDate) >= 0)
                {
                    this.DateOfBirth = d;
                    if (dr[ds.IndividualTable.DOB].ToString() != string.Empty)
                    {
                        int strCurrentAge = WebUtilities.Utilities.CalculateCurrentAge(d);
                        litGetDate.Text = "Current Age:" + strCurrentAge.ToString();
                        //hidCurrentAge.Value = strCurrentAge.ToString();
                    }
                }
            }
            if (!string.IsNullOrEmpty(dr[ds.IndividualTable.GENDER].ToString()))
            {
                this.Gender = dr[ds.IndividualTable.GENDER].ToString();
            }
            
        }
        public void SetPersonalDetails(PersonalDetailsTable pdTable)
        {
            if( pdTable == null) 
                throw new ArgumentNullException();
            if( pdTable.Rows.Count == 0)    
                throw new InvalidDataException("No data was provided for Personal Details");

            ClearPersonalDetails();

            DataRow row = pdTable.Rows[0];
            this.ClientName = row[pdTable.NAME].ToString();
            this.DateOfBirth = Convert.ToDateTime(row[pdTable.DOB]);
            this.RetirementAge = Convert.ToInt32(row[pdTable.RETIREMENTAGE]);
            this.PlanAge = Convert.ToInt32(row[pdTable.PLANAGE].ToString());
            this.Salary = Convert.ToDecimal(row[pdTable.SALARY]);
        }
        public bool GetPersonalDetails(PersonalDetailsTable pdTable)
        {
            if (!IsValidated) return false;

            DataRow dr = pdTable.NewRow();

            dr[pdTable.NAME] = this.ClientName;
            if (this.DateOfBirth != null)
                dr[pdTable.DOB] = this.DateOfBirth;

            if (this.RetirementAge > 0)
                dr[pdTable.RETIREMENTAGE] = this.RetirementAge;

            if (this.Salary > 0)
                dr[pdTable.SALARY] = this.Salary;

            if (this.PlanAge > 0)
                dr[pdTable.PLANAGE] = this.PlanAge;

            pdTable.Rows.Add(dr);
            
            return true;
        }
        public bool ValidatePersonalDetails()
        {
            IsValidated = true;

            if (!txtPDDOB.SelectedDate.HasValue)
            {
                ValidationMessage += "\r\n Personal Details -> Date of Birth is invalid";
                IsValidated = false;
            }

            if (!txtPDPlanAge.Value.HasValue)
            {
                ValidationMessage += "\r\n Personal Details -> Plan Age is invalid";
                IsValidated = false;
            }

            if (!txtPDRetirementAge.Value.HasValue)
            {
                ValidationMessage += "\r\n Personal Details -> Retirement Age is invalid";
                IsValidated = false;
            }

            if (!txtPDSalary.Value.HasValue)
            {
                ValidationMessage += "\r\n Personal Details -> Salary is invalid";
                IsValidated = false;
            }

            long currentAge = WebUtilities.Utilities.CalculateCurrentAge(this.DateOfBirth.GetValueOrDefault());
            if (currentAge > PlanAge)
            {
                ValidationMessage += "\r\n Personal Details -> Plan Age should be greater than Current Age.";
                IsValidated = false;
            }

            return IsValidated;
        }
        private void ClearPersonalDetails()
        {
            this.ClientName = string.Empty;
            this.DateOfBirth = null;
            this.RetirementAge = 0;
            this.PlanAge = 0;
            this.Salary = 0;
        }
        
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}