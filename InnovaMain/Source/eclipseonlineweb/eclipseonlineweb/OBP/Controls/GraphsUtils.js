﻿/*My Local/Session Storage API*/

var IS_STORAGE_SUPPORTED = (function () {
    var uid = new Date,
        storage,
        result;
    try {
        (storage = window.sessionStorage).setItem(uid, uid);
        result = storage.getItem(uid) == uid;
        storage.removeItem(uid);
        return result && storage;
    } catch (e) { }
}());

var STORAGE = window.sessionStorage;


function ClearStorage() {

    if (IS_STORAGE_SUPPORTED) {
        console.log("ClearStorage:");
        RemoveLocalStorage('IncomeGraphScaleMaxValue');
        RemoveLocalStorage('AssetGraphScaleMaxValue');
        StoreLocally('IncomeGraphScaleMaxValue', 0);
        StoreLocally('AssetGraphScaleMaxValue', 0);
    }
}

function RemoveLocalStorage(key) {
    if (IS_STORAGE_SUPPORTED) {

        STORAGE.removeItem(key);
    }
}

function GetLocalStorage(key) {

    if (IS_STORAGE_SUPPORTED) {
        //console.log("getting from STORAGE:" + key);
        return JSON.parse(STORAGE.getItem(key));
    }
    else {
        // Sorry! No web storage support..
        return null;
    }
}

function StoreLocally(key, json) {

    if (IS_STORAGE_SUPPORTED) {
        //alert("storing in ss");
        //console.log("saving in STORAGE:" + key);
        STORAGE.removeItem(key);
        STORAGE.setItem(key, JSON.stringify(json));
        return true;
    }
    return false;

}

function IsStoredLocally(key) {
    
    if (IS_STORAGE_SUPPORTED) {
        //STORAGE.removeItem("FormsJSON");
        console.log("STORAGE check for " + key);
        console.log(STORAGE.getItem(key));
        return STORAGE.getItem(key) !== null;
    }
    else
        return false;
}
/**
* Transposes a given array.
* @id Array.prototype.transpose
* @author Shamasis Bhattacharya
*
* @type Array
* @return The Transposed Array
* @compat=ALL
*/
Array.prototype.transpose = function () {

    // Calculate the width and height of the Array
    var maxLength = 0;
    $.each(this, function (i, arr) {
        if (arr.length > maxLength)
            maxLength = arr.length;

    });
    console.log("maxLength:" + maxLength);
    var a = this,
      w = a.length ? a.length : 0,
      h = maxLength;
    //h = a[0] instanceof Array ? a[0].length : 0;

    // In case it is a zero matrix, no transpose routine needed.
    if (h === 0 || w === 0) { return []; }

    /**
     * @var {Number} i Counter
     * @var {Number} j Counter
     * @var {Array} t Transposed data is stored in this array.
     */
    var i, j, t = [];

    // Loop through every item in the outer array (height)
    for (i = 0; i < h; i++) {

        // Insert a new row (array)
        t[i] = [];

        // Loop through every item per item in outer array (width)
        for (j = 0; j < w; j++) {

            // Save transposed data.
            t[i][j] = a[j][i];
        }
    }

    return t;
};

function GetColor(factorValue) {
    var colors = new Array();
    colors = Gradient (0, 100);

    //console.log(factorValue);
    var index = Math.round(factorValue * 100);
    //console.log(index);

    if (index > colors.length - 1) {
        //console.log("Index out of Bound:" + index);
        return '#FFFFFF'; //white
    }
    //console.log('GetColor: ' + factorValue + '  ' + colors[index]);
    return colors[index];
}



function Gradient (min, max)
{
    var middle = ( min + max ) / 2;
    var scale = 255 / ( middle - min );
    var colors = new Array();

    for (var i = min; i <= max; i++ ) 
    {
        if (i <= min)
            colors[i] = "#FF0000";
        else if (i >= max)
            colors[i] = "#00FF00";
        else if (i < middle)
            colors[i] = '#FF' + DecimalToHex(Math.round((i - min) * scale), 2) + '00'; //sprintf('FF%02X00', Math.round((i - min) * scale));
        else
            colors[i] = '#' + DecimalToHex((255 - Math.round((i - middle) * scale)), 2) + 'FF00';  //sprintf('%02XFF00', 255 - Math.round((i - middle) * scale));
        //console.log("Gradient:" + i + ' ' + colors[i]);
    }
    return colors;      
}

function ZeroPad(num, places) {
    var zero = places - num.toString().length;
    return Array(+(zero > 0 && zero)).join('0') + num;
}

function DecimalToHex(d, padding) 
{
    var hex = Number(d).toString(16);
    padding = typeof (padding) === "undefined" || padding === null ? padding = 2 : padding;

    while (hex.length < padding) 
    {
        hex = "0" + hex;
    }

    return hex;
}

function getPercentileColor(percentile) {
    
    //console.log(percentile);
    if (percentile == "10" || percentile == "90")
        //return "LightGray"; //Changed as asked by Farooq
        return "White";
    else if (percentile == "20" || percentile == "80")
        return "Yellow";
    else if (percentile == "30" || percentile == "70")
        return "Orange";
    else if (percentile == "40" || percentile == "60") 
        return "Red";   //LIGHT RED
        //return "#FF9A8D";   //LIGHT RED
    else if  (percentile == "50")
        return "Maroon";

}

function getAssetsPercentileColor(percentile) {

    //console.log(percentile);
    if (percentile == "20" || percentile == "90")
         return "Yellow";
    else if (percentile == "30" || percentile == "80")
        return "Orange";
    else if (percentile == "40" || percentile == "70")
        return "Red";
    else if (percentile == "50" || percentile == "60")
        return "Maroon";
    else if (percentile == "10")
        return "White";

}

function getIncomePercentileColor(percentile) {

    //console.log(percentile);
    if (percentile == "10" || percentile == "90")
    //return "LightGray"; //Changed as asked by Farooq
    //return "White";
        return "lemonchiffon";
    else if (percentile == "20" || percentile == "80")
        return "Yellow";
    else if (percentile == "30" || percentile == "70")
        return "Orange";
    else if (percentile == "40" || percentile == "60")
        return "Red";   //LIGHT RED
    //return "#FF9A8D";   //LIGHT RED
    else if (percentile == "50")
        return "Maroon";

}

function getLumpSumPercentileColor(percentile) {

    //console.log(percentile);
    if (percentile == "10" || percentile == "90")
        return "LightGray"; 
    else if (percentile == "20" || percentile == "80")
        return "Yellow";
    else if (percentile == "30" || percentile == "70")
        return "Orange";
    else if (percentile == "40" || percentile == "60")
        return "Red";   //LIGHT RED
    //return "#FF9A8D";   //LIGHT RED
    else if (percentile == "50")
        return "Maroon";
}

function roundValue(value) {

    if (value == 0 || value == "0" || value == "" || value == null || value == "undefined") return value;
    
    var fix = 1;
    for (var i = 1; i < value.length; i++)
        fix *= 10;
    
    var roundedValue = fix * parseInt(value.substring(0, 1));
    if (value > roundedValue)
        roundedValue += fix;

    return roundedValue;
}

function deepCopy(obj) {
    var out = [];
    for (var i = 0; i < obj.length; i++)
        out.push(obj[i]);
    
    return out;
}

//Changing the base point of drawing for each percentile
function TransformSeriesData(percentiles, seriesAsPercentileData) {
    if (percentiles == null || percentiles.length <= 0) return null;
    if (seriesAsPercentileData == null || seriesAsPercentileData.length <= 0) return null;

    $.each(percentiles, function (p, percentile) {
        var tooltipDisplayData = deepCopy(seriesAsPercentileData[percentiles.length - p - 1].data);
        seriesAsPercentileData[percentiles.length - p - 1].tooltipDisplayData = tooltipDisplayData;
        if (p < percentiles.length - 1) {
            for (var s = 0; s < seriesAsPercentileData[percentiles.length - 1].data.length; s++) {
                if (seriesAsPercentileData[percentiles.length - p - 1].data[s] >= seriesAsPercentileData[percentiles.length - p - 2].data[s]) {
                    seriesAsPercentileData[percentiles.length - p - 1].data[s] = seriesAsPercentileData[percentiles.length - p - 1].data[s] - seriesAsPercentileData[percentiles.length - p - 2].data[s];
                }
            }
        }
    });

    return seriesAsPercentileData;
}