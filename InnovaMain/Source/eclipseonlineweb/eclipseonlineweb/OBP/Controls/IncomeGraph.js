﻿function ShowIncomeGraph() {
    $.ajax({
        type: "POST",
        url: "Advices.aspx/GetProjectionAsJSON",
        contentType: "application/json",
        dataType: "json",
        data: "{ 'GraphType': 'Income'}",
        success: function (graphData) {
            if (graphData.d !== 'Error') {
                var data = JSON.parse(graphData.d);
                console.log("INCOMEALL");
                console.log(data);
                PlotIncomeGraph(data.CurrentAge, data.d, data.majorUnit, data.min, data.max);
            }
        }
    });
}

function PlotIncomeGraph(currentAge,json, majorUnit, minValue, maxValue) {
    var categoryAxisData = [];
    $.each(json, function (i, item) {
        var age = parseInt(item.Timestep) + parseInt(currentAge);
        if (categoryAxisData.indexOf(age) == -1)
            categoryAxisData.push(age);        
    });

    var valueAxisOptions = {
        labels: {
            visible: true,
            template: "$#=kendo.format('{0:N0}',value)#"
        },
        majorUnit: majorUnit,
        max: maxValue,
        min: minValue,
        title: { text: "Income Values" }
    };

    if (maxValue == "0" || majorUnit == "0") {
        valueAxisOptions = { title: { text: "Income Values" } };
    }

    var percentiles = [];
    $.each(json, function (i, item) {
        if (percentiles.indexOf(item.Percentile) == -1) {
            percentiles.push(item.Percentile);
        }
    });

    var seriesAsPercentileData = [];
    $.each(percentiles, function (p, percentile) {

        var legend;
        if (percentile == '10' )
            legend = 'worst case';
        else if (percentile == '90')
            legend = 'best case';
//        if (percentile == '10' || percentile == '90')
//            legend = 'Less Likely';
        else if (percentile == '50')
            legend = 'most likely';

        var color = GetColor(percentile / 100);      
        //var color = getIncomePercentileColor(percentile );       
        //var series = { "name": percentile + " Percentile", "data": [], "color": color };
        var series = { "name": legend, "data": [], "color": color, "tooltipDisplayData": [] };

        $.each(json, function (j, value) {
            if (value.Percentile == percentile) {
                series.data.push(Math.round(value.Value));
                series.tooltipDisplayData.push(Math.round(value.Value));
            }
        });

        seriesAsPercentileData.push(series);
    });

    //Setting max value for graph --------------------------------------------------------------------------------------
    var maxAxisValue = new Array();
    for (var mValue = 0; mValue < seriesAsPercentileData.length; mValue++)
        maxAxisValue.push(Math.max.apply(null, seriesAsPercentileData[mValue].data));

    valueAxisOptions.max = Math.max.apply(null, maxAxisValue);
    valueAxisOptions.max = valueAxisOptions.max + parseInt(parseFloat(valueAxisOptions.max * 0.10));
    valueAxisOptions.majorUnit = parseInt(parseFloat(valueAxisOptions.max * 0.16));
    valueAxisOptions.majorUnit = roundValue(valueAxisOptions.majorUnit + "");

    var dataMax = GetLocalStorage("IncomeGraphScaleMaxValue");
    if (dataMax != null)
        if (parseInt(dataMax) > parseInt(valueAxisOptions.max))
            valueAxisOptions.max = dataMax;

    StoreLocally('IncomeGraphScaleMaxValue', valueAxisOptions.max);
    //------------------------------------------------------------------------------------------------------------------
    
    seriesAsPercentileData = TransformSeriesData(percentiles, seriesAsPercentileData);
    var ds = new kendo.data.DataSource({
        change: function (e) {
            var data = this.data();
            var series = [];
            for (var i = 0; i < data.length; i++) {
                var oneSeries = data[i];
                series.push({
                    data: oneSeries.data,
                    tooltipDisplayData: deepCopy(oneSeries.tooltipDisplayData),
                    name: oneSeries.name,
                    color: oneSeries.color
                });
            }
            var chart = $("#chart_Income").data('kendoChart');
            chart.options.series = series;
            chart.refresh();
        }
    });

    $("#chart_Income").kendoChart({
        title: {

            text: "Total Income Distribution"
        },
        legend: {
            position: "top"
        },
        categoryAxis: {
            categories: categoryAxisData,
            title: {
                text: "Age"
            },
            labels: {
                step: 5
            }
        },
        seriesDefaults: {
            type: "area",            
            stack: true
        },
        valueAxis: valueAxisOptions,
        tooltip: {
            visible: true,
            template: "Age:#=category#, Income:$#=kendo.format('{0:N0}',data.series.data.indexOf(value) == -1 ? value : data.series.tooltipDisplayData[data.series.data.indexOf(value)])#"
            //template: "Age:#=category#, Income:$#=kendo.format('{0:N0}', console.log(data))#"
        },
        series: [{ data: [], name: 'Loading...'}]
    });

    ds.data(seriesAsPercentileData);
}

/*
//Changing the base point of drawing for each percentile
function TransformSeriesData(percentiles, seriesAsPercentileData) {
    if (percentiles == null || percentiles.length <= 0) return null;
    if (seriesAsPercentileData == null || seriesAsPercentileData.length <= 0) return null;

    $.each(percentiles, function (p, percentile) {
        //var tooltipDisplayData = deepCopy(seriesAsPercentileData[p].data);
        seriesAsPercentileData[p].tooltipDisplayData = deepCopy(seriesAsPercentileData[p].tooltipDisplayData); 
        if (p < percentiles.length - 1) {
            var i = percentiles.length - p - 1;
//            var tooltipDisplayData = deepCopy(seriesAsPercentileData[i].tooltipDisplayData);
            seriesAsPercentileData[i].tooltipDisplayData = deepCopy(seriesAsPercentileData[i].tooltipDisplayData);
            for (var s = 0; s < seriesAsPercentileData[percentiles.length - 1].data.length; s++) {
                if (seriesAsPercentileData[i].data[s] >= seriesAsPercentileData[i-1].data[s]) {
                    seriesAsPercentileData[i].data[s] = seriesAsPercentileData[i].data[s] - seriesAsPercentileData[i - 1].data[s];

                    //console.log("TransformSeriesData:p " + p + ', i ' + i + ', s ' + s + ', tooltips ' + seriesAsPercentileData[i].tooltipDisplayData[s] + ', data ' + seriesAsPercentileData[i].data[s]);
                }
            }
        }
    });

    return seriesAsPercentileData;
}
*/
