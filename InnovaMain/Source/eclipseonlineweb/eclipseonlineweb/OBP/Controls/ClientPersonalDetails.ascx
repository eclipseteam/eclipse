﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPersonalDetails.ascx.cs" Inherits="eclipseonlineweb.OBP.Controls.ClientPersonalDetails" %>
    <h2 class="StepTitle">
        <span style="text-align: left; display: inline-block; width: 24%">Personal Details</span>
        <span style="text-align: right; display: inline-block; width: 75%; font-weight: normal">
            <telerik:RadButton runat="server" ID="btnEditIndividual" ToolTip="Edit Personal Details"
                Width="140px" Height="20px" AutoPostBack="False">
                <ContentTemplate>
                    <img src="../../images/OBP/edit-profile-icon.png" alt="" style="vertical-align: middle;
                        height: 15px; margin-right: 4px" />
                    <span>Edit Personal Details</span>
                </ContentTemplate>
            </telerik:RadButton>
        </span>
    </h2>
    <table>
        <tr>
            <td>
                Name
            </td>
            <td>
                <telerik:RadTextBox ID="txtPDName" ReadOnly="true" runat="server" Width="250px" Enabled="false">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr style="display: none;">
            <td>
                Advise Title<span style="color: Red">*</span>
            </td>
            <td>
                <telerik:RadTextBox ID="txtPDTitle" Text="" runat="server" Width="300px" TabIndex="1">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td>
                Date of Birth<span style="color: Red">*</span>
            </td>
            <td>
                <telerik:RadDatePicker ID="txtPDDOB" runat="server" Width="110px" MinDate="1/1/1900 12:00:00 AM"
                    FocusedDate="1/1/1970 12:00:00 AM" Enabled="false">
                    <DateInput DateFormat="dd/MM/yyyy" ReadOnly="true" DisplayDateFormat="dd/MM/yyyy">
                    </DateInput>
                    <ClientEvents OnDateSelected="OnDateSelected" />
                </telerik:RadDatePicker>
                <asp:Label runat="server" ID="litGetDate"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Retirement Age<span style="color: Red">*</span>
            </td>
            <td>
                <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtPDRetirementAge"
                    TabIndex="2" runat="server" Style="text-align: right" Width="100" MinValue="0"
                    MaxValue="125" Value="65">
                    <NumberFormat DecimalDigits="0" />
                </telerik:RadNumericTextBox>
            </td>
        </tr>
        <tr>
            <td>
                Gender<span style="color: Red">*</span>
            </td>
            <td>
                <telerik:RadTextBox ID="txtPDGender" ReadOnly="true" runat="server" Width="82px"
                    Enabled="false">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td>
                Salary<span style="color: Red">*</span>
            </td>
            <td>
                <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtPDSalary"
                    TabIndex="3" runat="server" Style="text-align: right" Width="100" MinValue="0">
                    <NumberFormat DecimalDigits="2" />
                </telerik:RadNumericTextBox>
                <span>Zero or any positive value</span>
            </td>
        </tr>
        <tr>
            <td>
                Plan Age<span style="color: Red">*</span>
            </td>
            <td>
                <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtPDPlanAge"
                    TabIndex="4" runat="server" Style="text-align: right" Width="100" MinValue="0"
                    MaxValue="125" Value="90">
                    <NumberFormat DecimalDigits="0" />
                    <ClientEvents OnBlur="ComparePlanAgeWithCurrentAge"></ClientEvents>
                </telerik:RadNumericTextBox>
                <span>Greater than current age</span>
            </td>
        </tr>
    </table>
    <asp:HiddenField runat="server" ID="hfClientSideValidationRequired" Value="0"/>

    <!-- Advices -- Personal Details Script Start -->
    <script type="text/javascript">
        
        function ComparePlanAgeWithCurrentAge() {
            
            var getage = getAge(GetTextBoxValue("<%: txtPDDOB.ClientID %>"));
            var checkPlane = GetTextBoxValue("<%: txtPDPlanAge.ClientID %>");
            var checkValidate = checkPlane - getage;
            if (checkValidate <= 0) {
                var makePlaneAge = parseInt(getage, 10) + 1;
                SetTextBoxValue("<%: txtPDPlanAge.ClientID %>", makePlaneAge);
            }
        }

        function ValidateProfileTabFinish() {
            
            var isClientSideValidationRequired = GetElement("<%: hfClientSideValidationRequired.ClientID %>").value;
            if (isClientSideValidationRequired == "0") return true;
            
            var isStepValid = true;
            if (GetTextBoxValue("<%: txtPDName.ClientID %>") != "" && GetTextBoxValue("<%: txtPDDOB.ClientID %>") != "" && GetTextBoxValue("<%: txtPDRetirementAge.ClientID %>") != "" && GetTextBoxValue("<%: txtPDSalary.ClientID %>") != "" && GetTextBoxValue("<%: txtPDPlanAge.ClientID %>") != "") {
                var getage = getAge(GetTextBoxValue("<%: txtPDDOB.ClientID %>"));
                var checkPlane = GetTextBoxValue("<%: txtPDPlanAge.ClientID %>");
                var checkValidate = checkPlane - getage;
                if (checkValidate > 0) {
                    isStepValid = true;
                }
                else {
                    isStepValid = false;
                }
            }
            else {
                isStepValid = false;
            }
            return isStepValid;
        }

        function ValidateProfileTab() {

            var isClientSideValidationRequired = GetElement("<%: hfClientSideValidationRequired.ClientID %>").value;
            if (isClientSideValidationRequired == "0") return true;

            var isStepValid = true;
            if ($("#<%: txtPDRetirementAge.ClientID %>").val() == null || $("#<%: txtPDRetirementAge.ClientID %>").val() == "" || parseInt($("#<%: txtPDRetirementAge.ClientID %>").val()) == 0) {
                alert('Invalid Retirement Age value.');
                $("#<%: txtPDRetirementAge.ClientID %>").focus();
                return false;
            }

            if ($("#<%: txtPDSalary.ClientID %>").val() == null || $("#<%: txtPDSalary.ClientID %>").val() == "" || parseInt($("#<%: txtPDSalary.ClientID %>").val()) == 0) {
                alert('Invalid Salary value.');
                $("#<%: txtPDSalary.ClientID %>").focus();
                return false;
            }

            if ($("#<%: txtPDPlanAge.ClientID %>").val() == null || $("#<%: txtPDPlanAge.ClientID %>").val() == "" || parseInt($("#<%: txtPDPlanAge.ClientID %>").val()) == 0) {
                alert('Invalid Salary value.');
                $("#<%: txtPDPlanAge.ClientID %>").focus();
                return false;
            }

            if (GetTextBoxValue("<%: txtPDName.ClientID %>") != "" && GetTextBoxValue("<%: txtPDDOB.ClientID %>") != "" && GetTextBoxValue("<%: txtPDRetirementAge.ClientID %>") != "" && GetTextBoxValue("<%: txtPDSalary.ClientID %>") != "" && GetTextBoxValue("<%: txtPDPlanAge.ClientID %>") != "") {
                var getage = getAge(GetTextBoxValue("<%: txtPDDOB.ClientID %>"));
                var checkPlane = GetTextBoxValue("<%: txtPDPlanAge.ClientID %>");
                var checkValidate = checkPlane - getage;
                if (checkValidate > 0) {
                    isStepValid = true;
                }
                else {
                    isStepValid = false;
                    alert("Plan Age should be greater than Current Age.");
                }
            }
            else {
                isStepValid = false;
            }
            return isStepValid;
        }

        function GetElement(elementId) {

            return document.getElementById(elementId);
        }

        function GetTextBoxValue(textboxId) {
            var txtBox = $find(textboxId);
            if (txtBox != null) {
                if (txtBox.get_value)
                    return txtBox.get_value();
            }

            txtBox = $('#' + textboxId);
            if (txtBox != null)
                return txtBox.val();
        }

        function SetTextBoxValue(textboxId, value) {
            var txtBox = $find(textboxId);
            if (txtBox != null) {
                if (txtBox.set_value) {
                    txtBox.set_value(value);
                    return;
                }
            }
            txtBox = $('#' + textboxId);
            if (txtBox != null)
                txtBox.val(value);
        }

        function SetTextBoxStatus(textboxId, shouldDisable) {

            if ($('#' + textboxId).size() == 0) return;
            if (shouldDisable) {
                $('#' + textboxId).attr('disabled', 'disabled');
                $('#' + textboxId).css('background-color', 'lightgray');
                $('#' + textboxId).addClass('riDisabled');
            }
            else {
                $('#' + textboxId).removeAttr('disabled');
                $('#' + textboxId).css('background-color', 'white');
                $('#' + textboxId).removeClass('riDisabled');
            }

        }

        function getAge(dateString) {
            var today = new Date();
            var birthDate = new Date(dateString);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        }
        
    </script>
    <!-- Advices -- Personal Details Script End -->
                                    