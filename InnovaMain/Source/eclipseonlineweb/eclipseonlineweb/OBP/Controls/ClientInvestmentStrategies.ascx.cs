﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.DataSets;
using System.Data;

namespace eclipseonlineweb.OBP.Controls
{
    public partial class ClientInvestmentStrategies : System.Web.UI.UserControl
    {
        #region Private Methods

        private void PopulateInvestmentStrategies()
        {
            try
            {
                DataSet ds = WebUtilities.MillimanServiceUtility.GetInvestmentStrategies();
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        ds.Tables[0].Rows[5].Delete();
                        rcbInvestmentStrategy.DataSource = ds.Tables[0];

                        rcbInvestmentStrategy.DataValueField = ds.Tables[0].Columns["ID"].ColumnName;
                        rcbInvestmentStrategy.DataTextField = ds.Tables[0].Columns["Name"].ColumnName;

                        rcbInvestmentStrategy.DataBind();
                        rcbInvestmentStrategy.SelectedValue = ds.Tables[0].Rows[0][0].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                RegisterErrorMessageScript("Unable to load Investment Strategies. Please reload the page again.", true, ex);
            }
        }

        //Error and Logging Method
        private void RegisterErrorMessageScript(string errorMessage, bool shouldLogException, Exception exp)
        {
            if (shouldLogException)
                WebUtilities.Utilities.LogException(exp, Request, Context);

            hfErrorMsg.Value = errorMessage;
            Page.ClientScript.RegisterStartupScript(GetType(), "Call my function", "ShowError();", true);
        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}