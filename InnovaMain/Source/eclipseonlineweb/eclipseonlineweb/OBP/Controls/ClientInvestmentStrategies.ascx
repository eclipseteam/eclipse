﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientInvestmentStrategies.ascx.cs"
    Inherits="eclipseonlineweb.OBP.Controls.ClientInvestmentStrategies" %>
    <h2 class="StepTitle">
    Investment Strategies</h2>
    <br />
    <div style="text-align: center; width: 100%;">
    <telerik:RadComboBox ID="rcbInvestmentStrategy" runat="server" Width="30%" TabIndex="16"
        OnClientSelectedIndexChanged="OnClientSelectedIndexChanged">
    </telerik:RadComboBox>
</div>
    <br />
    <div style="width: 100%;">
    <asp:Panel ID="palAsCurrentInvestMenet" runat="server" Style="display: none;" Width="100%">
        <table class="tblInvestmentStrategies">
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblSuperR1" Text="Super"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblPensionR2" Text="Pension"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="Label2" Text="Ordinary"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblHCurrentAustrialEquity" Text="Australian Equity"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCurrentAustrilR1" Text="23000000%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCurrentAustriR2" Text="0%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="Label3" Text="0%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblhCurrentInternationEqulity" Text="International Equity"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCurrentInternationR1" Text="0%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCurrentInternationR2" Text="0%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="Label4" Text="0%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblhCurrentProperty" Text="Property"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCurrentPropertyR1" Text="0%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCurrentPropertyR2" Text="0%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="Label5" Text="0%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblhCurrentFixedInterest" Text="Fixed Interest"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCurretnFixedInterestR1" Text="0%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCurretnFixedInterestR2" Text="0%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="Label6" Text="0%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblhCurrentCast" Text="Cash"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCurretnCastR1" Text="0%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCurretnCastR2" Text="0%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="Label7" Text="0%"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pal100Equality" runat="server" Style="display: none;" Width="100%">
        <table class="tblInvestmentStrategies">
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblEqulityBeforReterimentR1" Text="Before Retirement"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblEqulityAfterReterimentR1" Text="After Retirement"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblhEqulityAustrial" Text="Australian Equity"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblEqulityAusrtriltR1" Text="50%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblEqulityAusrtriltR2" Text="50%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblhEqulity100Internation" Text="International Equity"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblEqulityInternationtR1" Text="50%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblEqulityInternationtR2" Text="50%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblhEqulity100Property" Text="Property"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblEqulityPropertytR1" Text="0%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblEqulityPropertytR2" Text="0%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblhEqulity100FixedInterest" Text="Fixed Interest"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblEqulityFixedInterestR1" Text="0%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblEqulityFixedInterestR2" Text="0%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblhEqulity100Cast" Text="Cash"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblEqulityCastR1" Text="0%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblEqulityCastR2" Text="0%"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="palGrothFund" Style="display: none;" Width="100%">
        <table class="tblInvestmentStrategies">
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblBeforReterimetnGrothR1" Text="Before Retirement"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblAfterReterimetnGrothR2" Text="After Retirement"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblHAustrialGroth" Text="Australian Equity"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblAustrilGrothR1" Text="35%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblAustrilGrothR2" Text="35%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblHInternalGroth" Text="International Equity"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblInternationnGrothR1" Text="40%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblInternationnGrothR2" Text="40%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblHPropertyGroth" Text="Property"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblPropertynGrothR1" Text="10%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblPropertynGrothR2" Text="10%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblHFixedInterestGroth" Text="Fixed Interest"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblFixedInterestGrothR1" Text="10%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblFixedInterestGrothR2" Text="10%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblHCastGroth" Text="Cash"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCastnGrothR1" Text="5%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCastnGrothR2" Text="5%"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="PalBalFund" Style="display: none;" Width="100%">
        <table class="tblInvestmentStrategies">
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblBeforReterimentBalFundR1" Text="Before Retirement"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblAfterReterimentBalFundR2" Text="After Retirement"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblHAustrialBalFund" Text="Australian Equity"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblAustrialBalFundR1" Text="30%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblAustrialBalFundR2" Text="30%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblHInternationalBalFund" Text="International Equity"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="LalblInternationBalFundR1" Text="30%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="LalblInternationBalFundR2" Text="30%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblHPropertyBalFund" Text="Property"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblPropertyBalFundR1" Text="10%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblPropertyBalFundR2" Text="10%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblHFixedFund" Text="Fixed Interest"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblFixedInterestBalFundR1" Text="20%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblFixedInterestBalFundR2" Text="20%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblHCastBalFund" Text="Cash"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCastBalFundR1" Text="10%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCastBalFundR2" Text="10%"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="PalConservertionFund" Style="display: none;" Width="100%">
        <table class="tblInvestmentStrategies">
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblBeforeReterimetnConverstionFunctionR1" Text="Before Retirement"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblAftereReterimetnConverstionFunctionR2" Text="After Retirement"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblHAustrialConversertionFund" Text="Australian Equity"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblAustrialConverstionFunctionR1" Text="20%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblAustrialConverstionFunctionR2" Text="20%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblHInternationConversertionFund" Text="International Equity"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblInternationConverstionFunctionR1" Text="20%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblInternationConverstionFunctionR2" Text="20%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblHPropertyConversertionFund" Text="Property"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblPropertyConverstionFunctionR1" Text="0%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblPropertyConverstionFunctionR2" Text="0%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblHFixedInterestConversertionFund" Text="Fixed Interest"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblFixedInterestConverstionFunctionR1" Text="40%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblFixedInterestConverstionFunctionR2" Text="40%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblHCashConversertionFund" Text="Cash"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCastnConverstionFunctionR1" Text="20%"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCastnConverstionFunctionR2" Text="20%"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
</div>
    <asp:HiddenField ID="hfErrorMsg" Value="xyz" runat="server" EnableViewState="true" />
    <script type="text/javascript">

        //Trigger Investment Strategies dropdown forcefully to show the grid on first-seight
        if ($('#<%:rcbInvestmentStrategy.ClientID %>').val() != null && $('#<%:rcbInvestmentStrategy.ClientID %>').val() != "")
            ShowInvestmentStrategiesPanel($('#<%:rcbInvestmentStrategy.ClientID %>').val());

        //Here we call investment stratgeis on drop down change start
        function OnClientSelectedIndexChanged(sender, eventArgs) {
            var selectedItem = eventArgs.get_item();
            var selectedText = selectedItem.get_text();
            ShowInvestmentStrategiesPanel(selectedText);
        }

        function ShowInvestmentStrategiesPanel(text) {

            switch (text) {
                case "As Per Current Investments":
                    GetElement("<%:palAsCurrentInvestMenet.ClientID %>").ShowElementAsInline();
                    GetElement("<%:pal100Equality.ClientID %>").HideElement();
                    GetElement("<%:palGrothFund.ClientID %>").HideElement();
                    GetElement("<%:PalBalFund.ClientID %>").HideElement();
                    GetElement("<%:PalConservertionFund.ClientID %>").HideElement();
                    break;

                case "100% Equity":
                    GetElement("<%:pal100Equality.ClientID %>").ShowElementAsInline();
                    GetElement("<%:palAsCurrentInvestMenet.ClientID %>").HideElement();
                    GetElement("<%:palGrothFund.ClientID %>").HideElement();
                    GetElement("<%:PalBalFund.ClientID %>").HideElement();
                    GetElement("<%:PalConservertionFund.ClientID %>").HideElement();
                    break;

                case "Growth Fund":
                    GetElement("<%:palGrothFund.ClientID %>").ShowElementAsInline();
                    GetElement("<%:pal100Equality.ClientID %>").HideElement();
                    GetElement("<%:palAsCurrentInvestMenet.ClientID %>").HideElement();
                    GetElement("<%:PalBalFund.ClientID %>").HideElement();
                    GetElement("<%:PalConservertionFund.ClientID %>").HideElement();
                    break;

                case "Balanced Fund":
                    GetElement("<%:PalBalFund.ClientID %>").ShowElementAsInline();
                    GetElement("<%:palGrothFund.ClientID %>").HideElement();
                    GetElement("<%:pal100Equality.ClientID %>").HideElement();
                    GetElement("<%:palAsCurrentInvestMenet.ClientID %>").HideElement();
                    GetElement("<%:PalConservertionFund.ClientID %>").HideElement();
                    break;

                case "Conservative Fund":
                    GetElement("<%:PalConservertionFund.ClientID %>").ShowElementAsInline();
                    GetElement("<%:PalBalFund.ClientID %>").HideElement();
                    GetElement("<%:palGrothFund.ClientID %>").HideElement();
                    GetElement("<%:pal100Equality.ClientID %>").HideElement();
                    GetElement("<%:palAsCurrentInvestMenet.ClientID %>").HideElement();
                    break;

                default:
                    //Visible false
                    GetElement("<%:PalConservertionFund.ClientID %>").HideElement();
                    GetElement("<%:PalBalFund.ClientID %>").HideElement();
                    GetElement("<%:palGrothFund.ClientID %>").HideElement();
                    GetElement("<%:pal100Equality.ClientID %>").HideElement();
                    GetElement("<%:palAsCurrentInvestMenet.ClientID %>").HideElement();
                    break;
            }
        }

        function ShowError() {
            var errorMsgObj = GetElement("<%: hfErrorMsg.ClientID %>").value;
            alert(errorMsgObj);
        }

        function GetElement(elementId) {

            return document.getElementById(elementId);
        }

    </script>
