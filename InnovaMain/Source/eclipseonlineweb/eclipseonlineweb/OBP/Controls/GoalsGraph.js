﻿function ShowGoalsGraph() {
    $.ajax({
        type: "POST",
        url: "Advices.aspx/GetProjectionAsJSON",
        contentType: "application/json",
        dataType: "json",
        data: "{ 'GraphType': 'Goals'}",
        success: function (graphData) {
            var data = JSON.parse(graphData.d);
            if (graphData.d !== 'Error') {
                console.log("GOALS");
                console.log(data);

                $('#chart_goals_Color_Legend').empty();
                var xaxis = '';
                var index = 0;
                for (var colorIndex = -0.01; colorIndex <= 1; colorIndex = colorIndex + 0.01) {
                    var value = Math.round(colorIndex * 100) + 1;
                    var spanHtml = '<span class="legend" title="' + value + "%" + '" style="display:inline-block; width:0.99%; background-color:' + GetColor(colorIndex) + ';font-size:0.7em;">&nbsp;</span>';
                    $('#chart_goals_Color_Legend').append(spanHtml);
                    if (value % 10 == 0) {
                        if (index == 0) {
                            xaxis = '<span class="legend" style="display:inline-block; width:0.7%; font-size:0.7em;">' + value + '%</span>';
                            index = 1;
                        } else
                            xaxis = '<span class="legend" style="padding-left:9%; display:inline-block; width:0.7%; font-size:0.7em;">' + value + '%</span>';
                        $('#chart_goals_Color_Legend_XAxis_Labels').append(xaxis);

                    }
                }

                $.each(data.d, function (i, goal) {
                    PlotGoalHeatmapGraph(data.CurrentAge, goal);
                });

                PlotGoalHeatmapGraphAxis(parseInt(data.CurrentAge));
            }
        }
    });
}

function getValue(step, goal) {
    $.each(goal.d, function (j, item) {
        if (step == parseInt(item.Timestep)) {
            return item.Value;
        }
    });
}

function PlotGoalHeatmapGraph(currentAge, goal) {

    var goalStart = parseInt(goal.GoalStart);
    var goalEnd = parseInt(goal.GoalEnd);
    var currentage = parseInt(currentAge);
    var planAge = $('.txtPDPlanAge').val();


    console.log("PlotGoalHeatmapGraph: " + goal.GoalType + goal.GoalEnd);
    if (goalEnd <= 0)
        return;

    var seriesDataForGraph = [];
    //initialise series
    for (var age = currentage; age <= planAge; age++)
        seriesDataForGraph.push({ data: [1], color: "white", age: age ,value:0});

    //set colors
    var list = goal.d;
    for (var i = 0; i < list.length; i++) {
        var item = list[i];
        var step = parseInt(item.Timestep);
        if (step != -1 && step <= goalEnd - currentAge) {
            var colorHex = GetColor(item.Value);
            var age = currentage + step;
            //seriesDataForGraph[step] = { data: [50], color: colorHex, value: item.Value };
            seriesDataForGraph[step] = { data: [1], color: colorHex, age: age, value: item.Value };
        }
    }


    var ds = new kendo.data.DataSource({
        change: function (e) {
            var data = this.data();
            var series = [];
            for (var d = 0; d < data.length; d++) {
                var oneSeries = data[d];
                series.push({
                    data: oneSeries.data,
                    name: oneSeries.name,
                    color: oneSeries.color,
                    value: Math.round(oneSeries.value * 100),
                    age:oneSeries.age
                });
            }
            
            var chart = $("#chart_goals_" + goal.GoalType).data('kendoChart');
            chart.options.series = series;
            chart.refresh();
            //console.log("PlotGoalHeatmapGraph chart.refresh: " + goal.GoalType + series.length);
        }
    });


    $("#chart_goals_" + goal.GoalType).kendoChart({
        chartArea: {
            margin: 5
        },
        legend: {
            visible: false
        },
        categoryAxis: {
            //categories: [goal.GoalType],
            line: {
                visible: false
            },
            minorGridLines: {
                visible: false
            },
            majorGridLines: {
                visible: false
            },
            labels: {
                step: 5
            }
        },
        seriesDefaults: {
            type: "bar",
            stack: true
        },

        valueAxis: {
            visible: false,
            line: {
                visible: false
            },
            minorGridLines: {
                visible: false
            },
            majorGridLines: {
                visible: false
            },
                    
            labels: {
                step: 5
            }
        },
        tooltip: {
            visible: true,
            template: "Age:#=data.series.age#, Probability: #=data.series.value#%"           
        },
        series: [{ data: [], name: 'Loading...'}]
    });
    ds.data(seriesDataForGraph);
}

function PlotGoalHeatmapGraphAxis(currentAge) {

    var planAge = $('.txtPDPlanAge').val(); 
    var seriesDataForGraph = [];
    for (var age = currentAge; age <= planAge; age++) {
        seriesDataForGraph.push({ data: [1], color: "white", value: age });
       }

    var ds = new kendo.data.DataSource({
        change: function (e) {
            var data = this.data();
            var series = [];
            for (var d = 0; d < data.length; d++) {
                var oneSeries = data[d];
                series.push({
                    data: oneSeries.data,
                    //name: oneSeries.name,
                    color: oneSeries.color,
                    value: oneSeries.value
                });
            }

            var chart = $("#chart_goals_Age_Axis").data('kendoChart');
            chart.options.series = series;
            chart.refresh();
        }
    });


    $("#chart_goals_Age_Axis").kendoChart({
        chartArea: {
            margin: 5
        },
        legend: {
            visible: false
        },
        categoryAxis: {
            //categories: ["Age"],
            line: {
                visible: false
            },
            minorGridLines: {
                visible: false
            },
            majorGridLines: {
                visible: false
            }
        },
        seriesDefaults: {
            type: "bar",
            stack: true
        },

        valueAxis: {
            line: {
                visible: true
            },
            minorGridLines: {
                visible: true
            },
            majorGridLines: {
                visible: true
            },
            min: currentAge,
            max: planAge
        },
        tooltip: {
            visible: true,
            template: "Age: #=data.series.value#"    
        },
        series: [{ name: 'Loading...'}]
    });
    ds.data(seriesDataForGraph);
}