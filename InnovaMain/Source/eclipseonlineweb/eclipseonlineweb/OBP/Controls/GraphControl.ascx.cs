﻿using eclipseonlineweb.OS.Milliman.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Collections;
using eclipseonlineweb.OS.Milliman.Actions;

namespace eclipseonlineweb.OBP.Controls
{
    public enum GraphType
    {
        Asset, Income, LumpSum, Probability, Goals
    }

    public partial class GraphControl : System.Web.UI.UserControl
    {
        public string ProjectionRunsForGraphCacheID { get; set; }
        public string GraphVisibilitySessionID { get; set; }
        public string ProjectionsSessionID { get; set; }
        public string GoalsSessionID { get; set; }
        public string CurrentProjectionRun { get; set; }
        public bool IsNewProjectionRun { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ShowGraphs())
                return;

            if (Cache[ProjectionRunsForGraphCacheID] == null)
                Cache[ProjectionRunsForGraphCacheID] = new SortedDictionary<string, string>();

            //Output Divs and store corresponding json data in memory
            switch (GraphType)
            {
                case GraphType.Asset:

                    Session[GraphType.ToString()] = GetAssetProjectionAsJSON();

                    // Output corresponding Script function calls
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "ScriptFor" + this.GraphType, "ShowAssetGraph();", true);

                    break;

                case GraphType.Income:

                    //CreateDiv("chart_" + GraphType);
                    Session[GraphType.ToString()] = GetIncomeProjectionAsJSON();
                    // Output corresponding Script function calls
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "ScriptFor" + this.GraphType, "ShowIncomeGraph();", true);
                    break;

                case GraphType.LumpSum:

                    //CreateDiv("chart_" + GraphType);
                    Session[GraphType.ToString()] = GetLumpSumProjectionAsJSON();
                    // Output corresponding Script function calls
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "ScriptFor" + this.GraphType, "ShowLumpSumGraph();", true);
                    break;

                case GraphType.Probability:

                    //CreateDiv("chart_" + GraphType);
                    Session[GraphType.ToString()] = GetGoalsProjectionAsJSON();
                    // Output corresponding Script function calls
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "ScriptFor" + this.GraphType, "ShowProbabilityGraph();", true);
                    break;
                case GraphType.Goals:

                    //CreateDiv("chart_goals_Wealth");
                    //CreateDiv("chart_goals_Income");
                    //CreateDiv("chart_goals_Liquidity" );
                    //CreateDiv("chart_goals_Bequest");
                    //CreateDiv("chart_goals_LumpSum");

                    Session[GraphType.ToString()] = GetGoalsProjectionAsJSON();
                    if (IsNewProjectionRun || !IsPostBack)
                    {
                        SortedDictionary<string, string> htProjectionRunsGraphData = Cache[ProjectionRunsForGraphCacheID] as SortedDictionary<string, string>;
                        if (Session[GraphType.ToString()] != null && htProjectionRunsGraphData != null)
                        {
                            htProjectionRunsGraphData[CurrentProjectionRun] = Session[GraphType.ToString()].ToString();
                        }
                    }
                    // Output corresponding Script function calls
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "ScriptFor" + this.GraphType, "ShowGoalsGraph();", true);
                    break;

                default:

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Call my function", "alert('Graph Type not specified in some Graph Control!')", true);
                    return;

            }


        }

        private bool ShowGraphs()
        {
            return Session[GraphVisibilitySessionID] == null ? false : Convert.ToBoolean(Session[GraphVisibilitySessionID]);
        }
        #region Control Props

        public static byte CurrentAge { get; set; }

        public GraphType GraphType { get; set; }

        #endregion

        #region Helper Methods

        private void CreateDiv(string divId)
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes.Add("id", divId);
            div.Style.Add("width", "100%");
            //config css

            if (divId == "chart_goals_LumpSum" || divId == "chart_goals_Wealth" || divId == "chart_goals_Income" || divId == "chart_goals_Liquidity" || divId == "chart_goals_Bequest")
            {

                div.Style.Add("height", "200px");
            }

            Controls.Add(div);

        }

        public string JSON_Goals(DataSet ds)
        {
            StringBuilder JsonString = new StringBuilder();
            JsonString.Append("\"d\":[");
            DataTable copy = new DataTable();
            if (ds.Tables.Count > 0)
                copy = ds.Tables[0].Copy();

            for (int i = 0; i < copy.Rows.Count; i++)
            {
                JsonString.Append("{ ");

                for (int j = 0; j < copy.Columns.Count; j++)
                {
                    if (j < copy.Columns.Count - 1)
                    {
                        JsonString.Append("\"" + copy.Columns[j].ColumnName + "\":\"" + copy.Rows[i][j].ToString() + "\",");
                    }
                    else if (j == copy.Columns.Count - 1)
                    {
                        JsonString.Append("\"" + copy.Columns[j].ColumnName + "\":\"" + copy.Rows[i][j].ToString() + "\"");
                    }
                }
                /*end Of String*/
                if (i == copy.Rows.Count - 1)
                {
                    JsonString.Append("} ");
                }
                else
                {
                    JsonString.Append("}, ");
                }
            }

            JsonString.Append("]");

            string json = JsonString.ToString();

            return json;
        }

        static double Round(double value, double n)
        {
            if (n < 1)
                n = 1;
            if (value < n)
                value = n;

            return Math.Ceiling(value / n) * n;
        }

        public static string JSON_DataTable(DataTable dt)
        {
            double min = 0, max = 10000000, majorUnit = 500;
            try
            {
                var query = from row in dt.AsEnumerable()
                            group row by new
                            {
                                Timestep = row.Field<long>("Timestep")

                            } into grp

                            select new
                            {
                                Key = grp.Key,


                                SumValue = grp.Sum(r => r.Field<double>("Value")),
                                //MinValue = grp.Min(r => decimal.Parse(r.Field<string>("Value"))),

                            };

                max = query.Max(m => m.SumValue);
                max = Math.Round(Math.Ceiling(max));

                if (max > 0)
                {
                    majorUnit = Math.Round(max * 0.20);

                    max = max + majorUnit;

                }
                else
                    max = 0;

                min = 0;// query.Min(m => m.SumValue);

            }
            catch { }

            StringBuilder JsonString = new StringBuilder();

            JsonString.Append("{");
            JsonString.Append("\"min\":\"" + min + "\",");
            JsonString.Append("\"max\":\"" + max + "\",");
            JsonString.Append("\"CurrentAge\":\"" + CurrentAge + "\",");

            JsonString.Append("\"majorUnit\":\"" + majorUnit + "\",");
            JsonString.Append("\"d\":[");
            //JsonString.Append("\"ROW\":[ ");

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                //CurrentAge
                JsonString.Append("{ ");
                //JsonString.Append("\"COL\":[ ");

                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (j < dt.Columns.Count - 1)
                    {
                        JsonString.Append("\"" + dt.Columns[j].ColumnName + "\":\"" + dt.Rows[i][j].ToString() + "\",");
                    }
                    else if (j == dt.Columns.Count - 1)
                    {
                        JsonString.Append("\"" + dt.Columns[j].ColumnName + "\":\"" + dt.Rows[i][j].ToString() + "\"");
                    }
                }
                /*end Of String*/
                if (i == dt.Rows.Count - 1)
                {
                    JsonString.Append("} ");
                }
                else
                {
                    JsonString.Append("}, ");
                }
            }

            JsonString.Append("]}");
            //var json=JsonConvert.SerializeObject(JsonString.ToString());
            var json = JsonString.ToString();
            return json;
        }

        #endregion


        #region Projection Methods

        public string GetAssetProjectionAsJSON()
        {
            try
            {
                this.Controls.Clear();

                SortedList<string, DataSet> PROJECTIONS = HttpContext.Current.Session[ProjectionsSessionID] as SortedList<string, DataSet>;

                DataSet ds = PROJECTIONS["AssetPercentiles"];

                string json = JSON_DataTable(ds.Tables[0]);

                return json;
            }
            catch (Exception ex)
            {
                WebUtilities.Utilities.LogException(ex, this.Request, this.Context);
                return "Error";
            }
        }

        public string GetIncomeProjectionAsJSON()
        {
            try
            {
                this.Controls.Clear();

                SortedList<string, DataSet> PROJECTIONS = HttpContext.Current.Session[ProjectionsSessionID] as SortedList<string, DataSet>;
                DataSet ds = PROJECTIONS["IncomePercentiles"];
                string json = JSON_DataTable(ds.Tables[0]);
                return json;

            }
            catch (Exception ex)
            {
                WebUtilities.Utilities.LogException(ex, this.Request, this.Context);
                return "Error";
            }
        }

        public string GetLumpSumProjectionAsJSON()
        {
            try
            {
                this.Controls.Clear();
                SortedList<string, DataSet> PROJECTIONS = HttpContext.Current.Session[ProjectionsSessionID] as SortedList<string, DataSet>;

                var rs = PROJECTIONS.Where(type => type.Key.Contains("LumpSumPercentile")).SingleOrDefault();
                var table = rs.Value.Tables[0];
                string json = JSON_DataTable(table);
                return json;

                //return JsonConvert.SerializeObject(table, Formatting.None, new JsonSerializerSettings { ReferenceLoopHandling =ReferenceLoopHandling.Ignore});
            }
            catch (Exception ex)
            {
                WebUtilities.Utilities.LogException(ex, this.Request, this.Context);
                return "Error";
            }
        }

        public string GetGoalsProjectionAsJSON()
        {
            try
            {
                this.Controls.Clear();

                StringBuilder JsonString = new StringBuilder();

                SortedList<string, DataSet> PROJECTIONS = HttpContext.Current.Session[ProjectionsSessionID] as SortedList<string, DataSet>;
                if (PROJECTIONS == null)
                    return "";
                JsonString.Append("{");
                JsonString.Append("\"TargetStep\":\"-1\",");
                JsonString.Append("\"CurrentAge\":\"" + CurrentAge + "\",");
                JsonString.Append("\"d\":[");
                List<string> categoryAxis = new List<string>();
                List<Goal> goals = HttpContext.Current.Session[GoalsSessionID] as List<Goal>;
                List<string> goalTypes =  new List<string>{"Wealth", "Income", "LumpSum", "Liquidity", "Bequest"};

                foreach (string gType in goalTypes)
                {                                 
                    string goalType= gType;                        
                    if (goalType == "Liquidity")
                        goalType = "LIQUIDASSETS";


                    long lGoalStartAge = 0;
                    long lGoalEndAge = 0;
                        DataSet ds = new DataSet();  
             
                    var projGoalResults = PROJECTIONS.Where(g => g.Key.Contains("Goal") & g.Key.Contains(gType)).OrderBy(g => g.Key);
                    if (projGoalResults != null)
                    {
                        foreach (var item in projGoalResults)
                        {
                            int index = item.Key.IndexOf("Goal");
                            //string goalType = item.Key.Substring(0, index);
                            int goalNumber = 1;
                            if (index + 4 < item.Key.Length)
                            {
                                string goalNumStr = item.Key.Substring(index + 4);
                                int.TryParse(goalNumStr, out goalNumber);
                            }

                            ds.Merge(item.Value);
                            if (goals != null)
                            {
                                List<Goal> typeGoals = goals.Where(g => g.GoalType == goalType.ToUpper()).ToList();
                                foreach (Goal goalSet in typeGoals)
                                {
                                    if (goalSet.StartAge > 0 && (lGoalStartAge == 0 || goalSet.StartAge < lGoalStartAge))
                                        lGoalStartAge = goalSet.StartAge;
                                    if (lGoalEndAge < goalSet.EndAge)
                                        lGoalEndAge = goalSet.EndAge;
                                }
                             }
                           }
                        }//projGoalsResults
                      
                    
                    if (goalType == "LIQUIDASSETS")
                        goalType = "Liquidity";
 
                    categoryAxis.Add(gType);

                    JsonString.Append("{\"GoalType\":\"" + goalType + "\",");
                    JsonString.Append("\"GoalStart\":\"" + lGoalStartAge.ToString() + "\",");
                    JsonString.Append("\"GoalEnd\":\"" + lGoalEndAge.ToString() + "\",");

                    var one_goal_json = JSON_Goals(ds);
                    JsonString.Append(one_goal_json);
                    JsonString.Append("},");
                }//gType in goalTypes

                var jsonX = JsonConvert.SerializeObject(categoryAxis);
                string jsonString = JsonString.ToString().Substring(0, JsonString.ToString().Length - 1);
                return jsonString + "], \"categoryAxis\":" + jsonX + "}";

            }
            catch (Exception ex)
            {
                WebUtilities.Utilities.LogException(ex, this.Request, this.Context);
                return "Error";
            }
        }

        #endregion


    }
}