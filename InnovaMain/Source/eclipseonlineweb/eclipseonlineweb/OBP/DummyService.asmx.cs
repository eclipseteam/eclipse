﻿using System.Web.Services;

namespace eclipseonlineweb.OBP
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class DummyService : System.Web.Services.WebService
    {
        [WebMethod(Description = "Keeps your current session alive", EnableSession = true)]
        public void PingSession()
        {
        }
    }
}
