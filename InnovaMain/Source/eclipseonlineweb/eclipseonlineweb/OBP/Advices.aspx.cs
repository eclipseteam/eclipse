﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using eclipseonlineweb.Controls;
using eclipseonlineweb.OBP.Controls;
using eclipseonlineweb.OS.Milliman.Actions;
using eclipseonlineweb.OS.Milliman.Client;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using OBPCommon = Oritax.TaxSimp.Common;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;


namespace eclipseonlineweb.OBP
{
    public partial class Advices : UMABasePage
    {

        #region Page Level Variables

        //Client Details

        private string lClientCode = "JBOURNE";

        //Person Details
        private string lEclipseId = "JBOURNE";
        private bool isValid;
        private string errorMsg = string.Empty;
        private static int CurrentAge = 0;
        private Guid _cid;
        private bool IsProjectionRun = false;

        //Goals grid sessions related IDs and variables for graph input and output data.
        private string goalsWealthSessionID = "goalsWealthSessionID";
        private string goalsIncomeSessionID = "goalsIncomeSessionID";
        private string goalsLumpSumSessionID = "goalsLumpSumSessionID";
        private string goalsLiquidAssetsSessionID = "goalsLiquidAssetsSessionID";
        private string goalsBequestSessionID = "goalsBequestSessionID";
        private string projectionsSessionID = "PROJECTIONS";
        private string goalsSessionID = "GOALS";
        private string graphVisibilitySessionID = "SHOW_GRAPHS";
        private string multimanResultSetSessionID = "MILLIMANRESULTSET";
        private string advicesListingURLSessionID = "AdvicesListingURL";
        private string graphInputDataSessionID = "graphInputData";
        private string graphOutputDataSessionID = "graphOutputData";
        private string projectionRunSessionID = "projectionRun";
        private string ProjectionRunsForGraphCacheID { get { return "projectionRunsForGraph_" + HttpContext.Current.Session.SessionID + "_" + Request.Params["id"]; } }

        //TextBox related default values
        private const double amountIncrementalStep = 1000;
        private const int amountDecimalDigits = 2;
        private const double maxContributionLimit = 35000;

        //Graph input and output data container variables.
        private string graphInputData = string.Empty;
        private string graphOutputData = string.Empty;

        #endregion DataMemeber

        #region PageEvent - Miscellaneous page related events like click, databound etc.

        protected override void OnInit(EventArgs e)
        {
            grdGoalsWealth.ItemDataBound += grdGoalsWealth_ItemDataBound;
            grdGoalsIncome.ItemDataBound += grdGoalsIncome_ItemDataBound;
            grdGoalsLumpSum.ItemDataBound += grdGoalsLumpSum_ItemDataBound;
            grdGoalsLiquidAssets.ItemDataBound += grdGoalsLiquidAssets_ItemDataBound;
            grdGoalsBequest.ItemDataBound += grdGoalsBequest_ItemDataBound;
            IndividualControl.Canceled += () => HideShowIndividualGrid(false);
            IndividualControl.SaveData += (Cid, ds) =>
            {
                if (Cid == Guid.Empty.ToString())
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(Cid, ds);
                }
                HideShowIndividualGrid(false);
                SetControlsDefaultValues(GetIndividualDS());
            };
            IndividualControl.ValidationFailed += () => HideShowIndividualGrid(true);
        }
        public override void LoadPage()
        {
            base.LoadPage();
            InitializePageVariables();
            ActionsOnPageLoad();
            HttpCompress(Context.ApplicationInstance);
        }
        private void grdGoalsWealth_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //if it is auxiliary row just to generate the footer, hide it.
            if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem ||
                e.Item.ItemType == GridItemType.SelectedItem)
            {
                if (((Label)e.Item.FindControl("lblTotalAssetsWorth")).Text == "-1")
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    item.Display = false;
                }
                else
                {
                    (e.Item.FindControl("imgRemoveRow") as ImageButton).CommandArgument =
                        ((DataRowView)e.Item.DataItem)["GoalId"].ToString();
                    e.Item.Attributes.Add("dataItemRow", "1");
                }
            }
            else if (e.Item.ItemType == GridItemType.Footer)
            {
                RadNumericTextBox txtGoalsWealthDollarsFromAge =
                    e.Item.FindControl("txtGoalsWealth_DollarsFromAge") as RadNumericTextBox;
                RadNumericTextBox txtGoalsWealth_TotalAssetsWorth =
                    e.Item.FindControl("txtGoalsWealth_TotalAssetsWorth") as RadNumericTextBox;
                if (txtGoalsWealth_TotalAssetsWorth != null)
                    txtGoalsWealth_TotalAssetsWorth.IncrementSettings.Step = amountIncrementalStep * 10;

                if (IsEdit.Value == "0")
                {
                    if (txtGoalsWealthDollarsFromAge != null)
                        if (string.IsNullOrEmpty(txtGoalsWealthDollarsFromAge.Text))
                        {
                            txtGoalsWealthDollarsFromAge.Text = txtPDRetirementAge.Text;
                            txtGoalsWealthDollarsFromAge.Value = Convert.ToInt32(txtPDRetirementAge.Text);
                        }
                }

                //Incrementing age for next input.-----------------------------------------------------
                DataTable dtTable = Session[goalsWealthSessionID] as DataTable;
                if (dtTable != null && dtTable.Rows.Count > 1)
                {
                    int maxAge = Convert.ToInt32(dtTable.Compute("MAX(Age)", ""));
                    int planAge = txtPDPlanAge.Value.HasValue ? Convert.ToInt32(txtPDPlanAge.Value) : 0;
                    maxAge++;
                    if (maxAge <= planAge)
                    {
                        if (txtGoalsWealthDollarsFromAge != null)
                        {
                            txtGoalsWealthDollarsFromAge.Text = maxAge.ToString();
                            txtGoalsWealthDollarsFromAge.Value = maxAge;
                        }
                    }
                }
                //-------------------------------------------------------------------------------------
            }
        }
        private void grdGoalsIncome_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //if it is auxiliary row just to generate the footer, hide it.
            if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem ||
                e.Item.ItemType == GridItemType.SelectedItem)
            {
                if (((Label)e.Item.FindControl("lblTotalIncomeRequired")).Text == "-1")
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    item.Display = false;
                }
                else
                {
                    (e.Item.FindControl("imgRemoveRow") as ImageButton).CommandArgument =
                        ((DataRowView)e.Item.DataItem)["GoalId"].ToString();
                    e.Item.Attributes.Add("dataItemRow", "1");
                }
            }
            else if (e.Item.ItemType == GridItemType.Footer)
            {
                RadNumericTextBox txtGoalsIncomeDollarsFromAge =
                    e.Item.FindControl("txtGoalsIncome_DollarsFromAge") as RadNumericTextBox;
                RadNumericTextBox txtGoalsIncomeTotalIncomeRequired =
                    e.Item.FindControl("txtGoalsIncome_TotalIncomeRequired") as RadNumericTextBox;
                if (txtGoalsIncomeTotalIncomeRequired != null)
                    txtGoalsIncomeTotalIncomeRequired.IncrementSettings.Step = amountIncrementalStep;

                if (IsEdit.Value == "0")
                {
                    if (txtGoalsIncomeDollarsFromAge != null)
                        if (string.IsNullOrEmpty(txtGoalsIncomeDollarsFromAge.Text))
                        {
                            txtGoalsIncomeDollarsFromAge.Text = txtPDRetirementAge.Text;
                            txtGoalsIncomeDollarsFromAge.Value = Convert.ToInt32(txtPDRetirementAge.Text);
                        }
                    if (string.IsNullOrEmpty(txtGoalsIncomeTotalIncomeRequired.Text))
                    {
                        decimal totalSalary = string.IsNullOrEmpty(txtPDSalary.Text)
                                                  ? 0
                                                  : Convert.ToDecimal(txtPDSalary.Text);
                        if (totalSalary > 0)
                        {
                            txtGoalsIncomeTotalIncomeRequired.Text =
                                Convert.ToString(Convert.ToInt32((totalSalary * (decimal)0.75)));
                            txtGoalsIncomeTotalIncomeRequired.Value =
                                Convert.ToDouble(Convert.ToInt32(totalSalary * (decimal)0.75));
                        }
                        else
                        {
                            txtGoalsIncomeTotalIncomeRequired.Text = "0";
                            txtGoalsIncomeTotalIncomeRequired.Value = 0;
                        }
                    }
                }

                //Incrementing age for next input.-----------------------------------------------------
                DataTable dtTable = Session[goalsIncomeSessionID] as DataTable;
                if (dtTable != null && dtTable.Rows.Count > 1)
                {
                    int maxAge = Convert.ToInt32(dtTable.Compute("MAX(Age)", ""));
                    int planAge = txtPDPlanAge.Value.HasValue ? Convert.ToInt32(txtPDPlanAge.Value) : 0;
                    maxAge++;
                    if (maxAge <= planAge)
                    {
                        if (txtGoalsIncomeDollarsFromAge != null)
                        {
                            txtGoalsIncomeDollarsFromAge.Text = maxAge.ToString();
                            txtGoalsIncomeDollarsFromAge.Value = maxAge;
                        }
                    }
                }
                //-------------------------------------------------------------------------------------
            }
        }
        private void grdGoalsLumpSum_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //if it is auxiliary row just to generate the footer, hide it.
            if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem ||
                e.Item.ItemType == GridItemType.SelectedItem)
            {
                if (((Label)e.Item.FindControl("lblTotalLumpSumRequired")).Text == "-1")
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    item.Display = false;
                }
                else
                {
                    (e.Item.FindControl("imgRemoveRow") as ImageButton).CommandArgument =
                        ((DataRowView)e.Item.DataItem)["GoalId"].ToString();
                    e.Item.Attributes.Add("dataItemRow", "1");
                }
            }
            else if (e.Item.ItemType == GridItemType.Footer)
            {
                RadNumericTextBox txtGoalsLumpSumDollarsFromAge =
                    e.Item.FindControl("txtGoalsLumpSum_DollarsFromAge") as RadNumericTextBox;
                RadNumericTextBox txtGoalsLumpSum_TotalLumpSum =
                    e.Item.FindControl("txtGoalsLumpSum_TotalLumpSum") as RadNumericTextBox;
                if (txtGoalsLumpSum_TotalLumpSum != null)
                    txtGoalsLumpSum_TotalLumpSum.IncrementSettings.Step = amountIncrementalStep * 10;

                if (IsEdit.Value == "0")
                {
                    if (txtGoalsLumpSumDollarsFromAge != null)
                        if (string.IsNullOrEmpty(txtGoalsLumpSumDollarsFromAge.Text))
                        {
                            txtGoalsLumpSumDollarsFromAge.Text = txtPDRetirementAge.Text;
                            txtGoalsLumpSumDollarsFromAge.Value = Convert.ToInt32(txtPDRetirementAge.Text);
                        }
                }

                //Incrementing age for next input.-----------------------------------------------------
                DataTable dtTable = Session[goalsLumpSumSessionID] as DataTable;
                if (dtTable != null && dtTable.Rows.Count > 1)
                {
                    int maxAge = Convert.ToInt32(dtTable.Compute("MAX(Age)", ""));
                    int planAge = txtPDPlanAge.Value.HasValue ? Convert.ToInt32(txtPDPlanAge.Value) : 0;
                    maxAge++;
                    if (maxAge <= planAge)
                    {
                        if (txtGoalsLumpSumDollarsFromAge != null)
                        {
                            txtGoalsLumpSumDollarsFromAge.Text = maxAge.ToString();
                            txtGoalsLumpSumDollarsFromAge.Value = maxAge;
                        }
                    }
                }
                //-------------------------------------------------------------------------------------
            }
        }
        private void grdGoalsLiquidAssets_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //if it is auxiliary row just to generate the footer, hide it.
            if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem ||
                e.Item.ItemType == GridItemType.SelectedItem)
            {
                if (((Label)e.Item.FindControl("lblTotalLiquidAssetsRequired")).Text == "-1")
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    item.Display = false;
                }
                else
                {
                    (e.Item.FindControl("imgRemoveRow") as ImageButton).CommandArgument =
                        ((DataRowView)e.Item.DataItem)["GoalId"].ToString();
                    e.Item.Attributes.Add("dataItemRow", "1");
                }
            }
            else if (e.Item.ItemType == GridItemType.Footer)
            {
                RadNumericTextBox txtGoalsLiquidAssetsDollarsFromAge =
                    e.Item.FindControl("txtGoalsLiquidAssets_DollarsFromAge") as RadNumericTextBox;
                RadNumericTextBox txtGoalsLiquidAssetsTotalLiquidAssets =
                    e.Item.FindControl("txtGoalsLiquidAssets_TotalLiquidAssets") as RadNumericTextBox;
                if (txtGoalsLiquidAssetsTotalLiquidAssets != null)
                    txtGoalsLiquidAssetsTotalLiquidAssets.IncrementSettings.Step = amountIncrementalStep * 10;

                if (IsEdit.Value == "0")
                {
                    if (txtGoalsLiquidAssetsDollarsFromAge != null)
                        if (string.IsNullOrEmpty(txtGoalsLiquidAssetsDollarsFromAge.Text))
                        {
                            txtGoalsLiquidAssetsDollarsFromAge.Text = txtPDRetirementAge.Text;
                            txtGoalsLiquidAssetsDollarsFromAge.Value = Convert.ToInt32(txtPDRetirementAge.Text);
                        }
                }

                //Incrementing age for next input.-----------------------------------------------------
                DataTable dtTable = Session[goalsLiquidAssetsSessionID] as DataTable;
                if (dtTable != null && dtTable.Rows.Count > 1)
                {
                    int maxAge = Convert.ToInt32(dtTable.Compute("MAX(Age)", ""));
                    int planAge = txtPDPlanAge.Value.HasValue ? Convert.ToInt32(txtPDPlanAge.Value) : 0;
                    maxAge++;
                    if (maxAge <= planAge)
                    {
                        if (txtGoalsLiquidAssetsDollarsFromAge != null)
                        {
                            txtGoalsLiquidAssetsDollarsFromAge.Text = maxAge.ToString();
                            txtGoalsLiquidAssetsDollarsFromAge.Value = maxAge;
                        }
                    }
                }
                //-------------------------------------------------------------------------------------
            }
        }
        private void grdGoalsBequest_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //if it is auxiliary row just to generate the footer, hide it.
            if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem ||
                e.Item.ItemType == GridItemType.SelectedItem)
            {
                if (((Label)e.Item.FindControl("lblTotalBequestRequired")).Text == "-1")
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    item.Display = false;
                }
                else
                {
                    (e.Item.FindControl("imgRemoveRow") as ImageButton).CommandArgument =
                        ((DataRowView)e.Item.DataItem)["GoalId"].ToString();
                    e.Item.Attributes.Add("dataItemRow", "1");
                }
            }
            else if (e.Item.ItemType == GridItemType.Footer)
            {
                RadNumericTextBox txtGoalsBequestDollarsFromAge =
                    e.Item.FindControl("txtGoalsBequest_DollarsFromAge") as RadNumericTextBox;
                RadNumericTextBox txtGoalsBequest_TotalBequest =
                    e.Item.FindControl("txtGoalsBequest_TotalBequest") as RadNumericTextBox;
                if (txtGoalsBequest_TotalBequest != null)
                    txtGoalsBequest_TotalBequest.IncrementSettings.Step = amountIncrementalStep * 10;

                if (IsEdit.Value == "0")
                {
                    if (txtGoalsBequestDollarsFromAge != null)
                        if (string.IsNullOrEmpty(txtGoalsBequestDollarsFromAge.Text))
                        {
                            txtGoalsBequestDollarsFromAge.Text = txtPDPlanAge.Text;
                            txtGoalsBequestDollarsFromAge.Value = Convert.ToInt32(txtPDPlanAge.Text);
                        }
                }

                //Incrementing age for next input.-----------------------------------------------------
                DataTable dtTable = Session[goalsBequestSessionID] as DataTable;
                if (dtTable != null && dtTable.Rows.Count > 1)
                {
                    int maxAge = Convert.ToInt32(dtTable.Compute("MAX(Age)", ""));
                    int planAge = txtPDPlanAge.Value.HasValue ? Convert.ToInt32(txtPDPlanAge.Value) : 0;
                    maxAge++;
                    if (maxAge <= planAge)
                    {
                        if (txtGoalsBequestDollarsFromAge != null)
                        {
                            txtGoalsBequestDollarsFromAge.Text = maxAge.ToString();
                            txtGoalsBequestDollarsFromAge.Value = maxAge;
                        }
                    }
                }
                //-------------------------------------------------------------------------------------
            }
        }
        protected void imgGoalsWealthAddRow_OnClick(object sender, EventArgs e)
        {
            foreach (GridFooterItem footeritem in grdGoalsWealth.MasterTableView.GetItems(GridItemType.Footer))
            {
                RadNumericTextBox txtGoalsWealth_TotalAssetsWorth =
                    footeritem["TotalAssetsWorth"].FindControl("txtGoalsWealth_TotalAssetsWorth") as RadNumericTextBox;
                RadNumericTextBox txtGoalsWealth_DollarsFromAge =
                    footeritem["DollarsFromAge"].FindControl("txtGoalsWealth_DollarsFromAge") as RadNumericTextBox;
                bool isAdded = AddGoalsGridRow(grdGoalsWealth, txtGoalsWealth_TotalAssetsWorth,
                                               txtGoalsWealth_DollarsFromAge, goalsWealthSessionID, "Wealth");
                if (isAdded)
                {
                    chkWealth.Checked = isAdded;
                    txtGoalsWealth_TotalAssetsWorth.Focus();
                    hfGridFocusControlID.Value = txtGoalsWealth_TotalAssetsWorth.ClientID;
                }
            }
        }
        protected void imgGoalsIncomeAddRow_OnClick(object sender, EventArgs e)
        {
            foreach (GridFooterItem footeritem in grdGoalsIncome.MasterTableView.GetItems(GridItemType.Footer))
            {
                RadNumericTextBox txtGoalsIncome_TotalIncomeRequired =
                    footeritem["TotalIncomeAmount"].FindControl("txtGoalsIncome_TotalIncomeRequired") as
                    RadNumericTextBox;
                RadNumericTextBox txtGoalsIncome_DollarsFromAge =
                    footeritem["DollarsFromAge"].FindControl("txtGoalsIncome_DollarsFromAge") as RadNumericTextBox;
                bool isAdded = AddGoalsGridRow(grdGoalsIncome, txtGoalsIncome_TotalIncomeRequired,
                                               txtGoalsIncome_DollarsFromAge, goalsIncomeSessionID, "Income");
                if (isAdded)
                {
                    chkIncome.Checked = isAdded;
                    txtGoalsIncome_TotalIncomeRequired.Focus();
                    hfGridFocusControlID.Value = txtGoalsIncome_TotalIncomeRequired.ClientID;
                }
            }
        }
        protected void imgGoalsLumpSumAddRow_OnClick(object sender, EventArgs e)
        {
            foreach (GridFooterItem footeritem in grdGoalsLumpSum.MasterTableView.GetItems(GridItemType.Footer))
            {
                RadNumericTextBox txtGoalsLumpSum_TotalLumpSum =
                    footeritem["TotalLumpSumAmount"].FindControl("txtGoalsLumpSum_TotalLumpSum") as RadNumericTextBox;
                RadNumericTextBox txtGoalsLumpSum_DollarsFromAge =
                    footeritem["DollarsFromAge"].FindControl("txtGoalsLumpSum_DollarsFromAge") as RadNumericTextBox;
                bool isAdded = AddGoalsGridRow(grdGoalsLumpSum, txtGoalsLumpSum_TotalLumpSum,
                                               txtGoalsLumpSum_DollarsFromAge, goalsLumpSumSessionID, "Lump Sum");
                if (isAdded)
                {
                    chkLumpSum.Checked = isAdded;
                    txtGoalsLumpSum_TotalLumpSum.Focus();
                    hfGridFocusControlID.Value = txtGoalsLumpSum_TotalLumpSum.ClientID;
                }
            }
        }
        protected void imgGoalsLiquidAssetsAddRow_OnClick(object sender, EventArgs e)
        {
            foreach (GridFooterItem footeritem in grdGoalsLiquidAssets.MasterTableView.GetItems(GridItemType.Footer))
            {
                RadNumericTextBox txtGoalsLiquidAssets_TotalLiquidAssets =
                    footeritem["TotalLiquidAssetsAmount"].FindControl("txtGoalsLiquidAssets_TotalLiquidAssets") as
                    RadNumericTextBox;
                RadNumericTextBox txtGoalsLiquidAssets_DollarsFromAge =
                    footeritem["DollarsFromAge"].FindControl("txtGoalsLiquidAssets_DollarsFromAge") as RadNumericTextBox;
                bool isAdded = AddGoalsGridRow(grdGoalsLiquidAssets, txtGoalsLiquidAssets_TotalLiquidAssets,
                                               txtGoalsLiquidAssets_DollarsFromAge, goalsLiquidAssetsSessionID,
                                               "Liquid Assets");
                if (isAdded)
                {
                    chkLiquidAssets.Checked = isAdded;
                    txtGoalsLiquidAssets_TotalLiquidAssets.Focus();
                    hfGridFocusControlID.Value = txtGoalsLiquidAssets_TotalLiquidAssets.ClientID;
                }
            }
        }
        protected void imgGoalsBequestAddRow_OnClick(object sender, EventArgs e)
        {
            foreach (GridFooterItem footeritem in grdGoalsBequest.MasterTableView.GetItems(GridItemType.Footer))
            {
                RadNumericTextBox txtGoalsLiquidAssets_DollarsFromAge =
                    footeritem["TotalBequestAmount"].FindControl("txtGoalsBequest_TotalBequest") as RadNumericTextBox;
                RadNumericTextBox txtGoalsBequest_DollarsFromAge =
                    footeritem["DollarsFromAge"].FindControl("txtGoalsBequest_DollarsFromAge") as RadNumericTextBox;
                bool isAdded = AddGoalsGridRow(grdGoalsBequest, txtGoalsLiquidAssets_DollarsFromAge,
                                               txtGoalsBequest_DollarsFromAge, goalsBequestSessionID, "Bequest");
                if (isAdded)
                {
                    chkBequest.Checked = isAdded;
                    txtGoalsLiquidAssets_DollarsFromAge.Focus();
                    hfGridFocusControlID.Value = txtGoalsLiquidAssets_DollarsFromAge.ClientID;
                }
            }
        }
        protected void imgGoalsWealthRemoveRow_OnClick(object sender, EventArgs e)
        {
            RemoveGoalsGridRow(sender, grdGoalsWealth, goalsWealthSessionID);
        }
        protected void imgGoalsIncomeRemoveRow_OnClick(object sender, EventArgs e)
        {
            RemoveGoalsGridRow(sender, grdGoalsIncome, goalsIncomeSessionID);
        }
        protected void imgGoalsLumpSumRemoveRow_OnClick(object sender, EventArgs e)
        {
            RemoveGoalsGridRow(sender, grdGoalsLumpSum, goalsLumpSumSessionID);
        }
        protected void imgGoalsLiquidAssetsRemoveRow_OnClick(object sender, EventArgs e)
        {
            RemoveGoalsGridRow(sender, grdGoalsLiquidAssets, goalsLiquidAssetsSessionID);
        }
        protected void imgGoalsBequestRemoveRow_OnClick(object sender, EventArgs e)
        {
            RemoveGoalsGridRow(sender, grdGoalsBequest, goalsBequestSessionID);
        }
        protected void btnRefreshCurrentAssest_OnClick(object sender, EventArgs e)
        {
            hfIsRefreshCurrentAssetsPostback.Value = "1";
            IndividualCurrentAssetsDS ds = GetCurrentAssests(Guid.Parse(Request.Params["ins"]));
            SetCurrentAssets(ds);
        }
        protected void rcProjectionRuns_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            //PopulateProjectionDataFromCache();
        }
        public void OnFinish(object sender, EventArgs e)
        {
            OnWizardFinish(true);
        }

        #endregion PageEvent

        #region Page-Controls Events

        protected void btnPopupSave_OnClick(object sender, EventArgs e)
        {
            SaveAdvice(true);
        }

        protected void btnClearAllRuns_Click(object sender, EventArgs e)
        {
            ClearGraphsData();
            Response.Redirect(string.Format("~/OBP/AdviceListing.aspx?ins={0}", Guid.Parse(Request.Params["ins"].ToString().Replace("?ins=", string.Empty))));
        }

        #endregion

        #region Milliman - Miscellaneous methods to set the parameters of the API needed by the Projection Action Service

        private Person SetPerson()
        {
            //Specially commented out due to Milliman changes....................................................
            //DataSet lds = lClient.AddOrUpdateClient(lClientCode, lClientName);
            //...................................................................................................

            Person lPerson = new Person();
            //SmallPerson[] lPersonArray = { lPerson };

            //Person Set

            //lPerson.EclipseID = lEclipseId;
            //lPerson.name = lPersonName;
            isValid = true;
            long CurrentAge = 0;
            if (txtPDDOB.SelectedDate.HasValue)
            {
                TimeSpan? ts = DateTime.Now - txtPDDOB.SelectedDate;
                CurrentAge = (long)(ts.Value.TotalDays / 365);
            }

            if (txtPDDOB.SelectedDate.HasValue)
            {
                lPerson.date_of_birth = Convert.ToDateTime(txtPDDOB.SelectedDate.Value);
                lPerson.date_of_birthSpecified = true;
            }
            else
            {
                errorMsg = errorMsg + "\r\n" + "Personal Details -> DOB is invalid";
                isValid = false;
            }
            if (txtPDPlanAge.Value.HasValue)
            {
                lPerson.plan_age = Convert.ToInt64(txtPDPlanAge.Value);
            }
            else
            {
                errorMsg = errorMsg + "\r\n" + "Personal Details -> plan_age is invalid";
                isValid = false;
            }

            if (txtPDRetirementAge.Value.HasValue)
            {
                lPerson.retirement_age = Convert.ToInt64(txtPDRetirementAge.Value);
                lPerson.retirement_ageSpecified = true;
            }
            else
            {
                errorMsg = errorMsg + "\r\n" + "Personal Details -> retirement_age is invalid";
                isValid = false;
            }
            if (txtPDSalary.Value.HasValue)
            {
                lPerson.salary = Convert.ToDouble(txtPDSalary.Value);
                lPerson.salarySpecified = true;
            }
            else
            {
                errorMsg = errorMsg + "\r\n" + "Personal Details -> salary is invalid";
                isValid = false;
            }

            txtPDPlanAge.MinValue = CurrentAge;

            if (CurrentAge > lPerson.plan_age)
            {
                errorMsg = errorMsg + "\r\n" + "Personal Details -> plan_age should be greater than Current age.";
                isValid = false;
            }

            if (isValid)
            {
                //Commented Out as changed by Milliman services
                //lClient.SetPeopleForClient(lClientCode, lPersonArray);

                //Logging Input Data
                PrepareGraphInputData("Personal", lPerson);

                return lPerson;
            }
            else
                return null;
        }
        private OS.Milliman.Actions.Holding[] SetHoldingAll()
        {
            #region UI_Control_Arrays

            CheckBox[] chkBoxes = new CheckBox[] { chkNonSuperInvestments, chkSuperannuation, chkPension };

            RadNumericTextBox[][] texboxes =
                {
                    new RadNumericTextBox[]
                        {
                            txtNEquity,
                            txtNFixedInterest,
                            txtNInternationalEquity,
                            txtNCash,
                            txtNProperty,
                            txtNGold,
                        },
                    new RadNumericTextBox[]
                        {
                            txtSEquity,
                            txtSFixedInterest,
                            txtSInternationalEquity,
                            txtSCash,
                            txtSProperty,
                            txtSGold,
                        },
                    new RadNumericTextBox[]
                        {
                            txtPEquity,
                            txtPFixedInterest,
                            txtPInternationalEquity,
                            txtPCash,
                            txtPProperty,
                            txtPGold,
                        }
                };

            #endregion UI_Control_Arrays

            List<OS.Milliman.Actions.Holding> listHolding =
                new List<OS.Milliman.Actions.Holding>();
            for (int i = 0; i < chkBoxes.Length; i++)
                for (int j = 0; j < texboxes[i].Length; j++)
                    SetHolding(chkBoxes[i], texboxes[i][j], listHolding);

            if (listHolding.Count > 0)
            {
                //Commented Out due to the change by Milliman
                //lClient.SetHoldingsForPerson(lEclipseId, listHolding.ToArray());

                //Logging Input Data
                PrepareGraphInputData("Assets", listHolding);

                return listHolding.ToArray();
            }
            else
            {
                errorMsg = errorMsg + "\r\n" + "Minimum One holding should be provided.";
                isValid = false;

                return null;
            }
        }
        private List<OS.Milliman.Actions.Holding> SetHolding(CheckBox chk, RadInputControl textBox, List<OS.Milliman.Actions.Holding> listHolding)
        {
            string product = string.Empty;
            string value = string.Empty;

            if (chk.Checked)
            {
                product = textBox.Attributes["Product"];
                value = textBox.Text;

                OS.Milliman.Actions.Holding lHolding = new OS.Milliman.Actions.Holding();

                lHolding.TaxWrapper = chk.Attributes["TaxWrapper"];
                lHolding.HeldProduct = product;
                if (!string.IsNullOrEmpty(value))
                {
                    lHolding.AmountHeld = Convert.ToDouble(value);
                    lHolding.AmountHeldSpecified = true;
                }
                if (lHolding.AmountHeld > 0)
                    listHolding.Add(lHolding);
            }
            return listHolding;
        }
        private long SetInvestmentStrategy()
        {
            if (!string.IsNullOrEmpty(rcbInvestmentStrategy.SelectedValue))
            {
                long lInvestmentStrategyId = Convert.ToInt64(rcbInvestmentStrategy.SelectedValue);
                //lClient.SetInvestmentStrategyForPerson(lEclipseId, lInvestmentStrategyId, true);

                //Logging Input Data
                PrepareGraphInputData("Investment", lInvestmentStrategyId);

                return lInvestmentStrategyId;
            }
            else
            {
                errorMsg = errorMsg + "\r\n" +
                           "Investment Strategy is mendatory field. You have to select one from the list.";
                isValid = false;

                return -1;
            }
        }
        private OS.Milliman.Actions.Action[] SetActions()
        {
            int lActionNumber = 1;

            //For Input Data Logging
            OS.Milliman.Actions.Action[] lActionsArray = new OS.Milliman.Actions.Action[4];
            List<OS.Milliman.Actions.Action> lActionArrayInfo = new List<OS.Milliman.Actions.Action>();


            if (chkSuperGuaranteeContributions.Checked)
            {
                OS.Milliman.Actions.Action lAction = new OS.Milliman.Actions.Action();
                //OS.Milliman.Actions.Action[] lActionArray = { lAction };

                lAction.Type = "ADDSUPERGUARANTEE";
                if (txtSGCPercentSalary.Value.HasValue)
                {
                    lAction.Percentage = Convert.ToDouble(txtSGCPercentSalary.Value);
                    lAction.PercentageSpecified = true;
                }
                else
                {
                    errorMsg = errorMsg + "\r\n" + "SuperGuaranteeContributions->PercentSalary is invalid.";
                    isValid = false;
                }
                if (txtPDRetirementAge.Value.HasValue)
                {
                    lAction.EndAge = Convert.ToInt64(txtPDRetirementAge.Value);
                    lAction.EndAgeSpecified = true;
                }
                else
                {
                    errorMsg = errorMsg + "\r\n" +
                               "SuperGuaranteeContributions->EndAge (i.e. Retirement Age) is invalid.";
                    isValid = false;
                }
                lAction.StartAge = 1;
                lAction.StartAgeSpecified = true;

                lAction.ActionNumber = lActionNumber++;
                lAction.Amount = 0;
                lAction.AmountSpecified = true;
                if (isValid)
                {
                    //lClient.SetActionsForPerson(lEclipseId, lActionArray);

                    //Logging Input Data
                    //PrepareGraphInputData("Actions", lActionArray);

                    lActionArrayInfo.Add(lAction);
                    lActionsArray[0] = lAction;
                }
            }

            if (chkSuperSalarySacrificeContributions.Checked)
            {
                OS.Milliman.Actions.Action lAction = new OS.Milliman.Actions.Action();
                //OS.Milliman.Client.SmallAction[] lActionArray = { lAction };

                lAction.Type = "MOVESALARYSACRIFICECONTRIBUTION";
                if (!string.IsNullOrEmpty(txtSSSCPercentSalary.Text.Trim()))
                {
                    if (txtSSSCPercentSalary.Value.HasValue)
                    {
                        if (txtSSSCPercentSalary.Value > 0)
                        {
                            lAction.Percentage = Convert.ToDouble(txtSSSCPercentSalary.Value);
                            lAction.PercentageSpecified = true;
                        }
                        else
                        {
                            errorMsg = errorMsg + "\r\n" +
                                       "SuperSalarySacrificeContributions -> PercentSalary is invalid";
                            isValid = false;
                        }
                    }
                    else
                    {
                        errorMsg = errorMsg + "\r\n" + "SuperSalarySacrificeContributions -> PercentSalary is invalid";
                        isValid = false;
                    }

                    lAction.Amount = 0;
                    lAction.AmountSpecified = true;
                }
                else
                {
                    if (txtSSSCAmount.Value.HasValue)
                    {
                        if (txtSSSCAmount.Value > 0)
                        {
                            lAction.Amount = Convert.ToDouble(txtSSSCAmount.Value);
                            lAction.AmountSpecified = true;
                        }
                        else
                        {
                            errorMsg = errorMsg + "\r\n" + "SuperSalarySacrificeContributions -> Amount is invalid";
                            isValid = false;
                        }
                    }
                    else
                    {
                        errorMsg = errorMsg + "\r\n" + "SuperSalarySacrificeContributions -> Amount is invalid";
                        isValid = false;
                    }

                    lAction.PercentageSpecified = false;
                }

                #region Setting StartEnd age -- Common Logic for above both cases

                if (txtSSSCStartAge.Value.HasValue)
                {
                    lAction.StartAge = Convert.ToInt64(txtSSSCStartAge.Value);
                    lAction.StartAgeSpecified = true;
                }
                else
                {
                    errorMsg = errorMsg + "\r\n" + "SuperSalarySacrificeContributions -> StartAge is invalid";
                    isValid = false;
                }
                if (txtSSSCStopAge.Value.HasValue)
                {
                    lAction.EndAge = Convert.ToInt64(txtSSSCStopAge.Value);
                    lAction.EndAgeSpecified = true;
                }
                else
                {
                    errorMsg = errorMsg + "\r\n" + "SuperSalarySacrificeContributions -> StopAge is invalid";
                    isValid = false;
                }

                #endregion Setting StartEnd age -- Common Logic for above both cases

                lAction.ActionNumber = lActionNumber++;
                if (isValid)
                {
                    //lClient.SetActionsForPerson(lEclipseId, lActionArray);
                    //Logging Input Data
                    //PrepareGraphInputData("Actions", lActionArray);
                    lActionArrayInfo.Add(lAction);
                    lActionsArray[1] = lAction;
                }
            }
            if (chkSuperAfterTaxContributions.Checked)
            {
                OS.Milliman.Actions.Action lAction = new OS.Milliman.Actions.Action();
                //OS.Milliman.Client.SmallAction[] lActionArray = { lAction };

                lAction.Type = "AFTERTAXCONTRIBUTION";
                if (txtSATCAmount.Value.HasValue)
                {
                    lAction.Amount = Convert.ToDouble(txtSATCAmount.Value);
                    lAction.AmountSpecified = true;
                }
                else
                {
                    errorMsg = errorMsg + "\r\n" + "SuperAfterTaxContributions -> Amount is invalid";
                    isValid = false;
                }
                if (txtSATCStartAge.Value.HasValue)
                {
                    lAction.StartAge = Convert.ToInt64(txtSATCStartAge.Value);
                    lAction.StartAgeSpecified = true;
                }
                else
                {
                    errorMsg = errorMsg + "\r\n" + "SuperAfterTaxContributions -> StartAGE is invalid";
                    isValid = false;
                }
                if (txtSATCStopAge.Value.HasValue)
                {
                    lAction.EndAge = Convert.ToInt64(txtSATCStopAge.Value);
                    lAction.EndAgeSpecified = true;
                }
                else
                {
                    errorMsg = errorMsg + "\r\n" + "SuperAfterTaxContributions -> StopAge is invalid";
                    isValid = false;
                }
                lAction.ActionNumber = lActionNumber++;
                lAction.PercentageSpecified = false;
                if (isValid)
                {
                    //lClient.SetActionsForPerson(lEclipseId, lActionArray);
                    //Logging Input Data
                    //PrepareGraphInputData("Actions", lActionArray);
                    lActionArrayInfo.Add(lAction);
                    lActionsArray[2] = lAction;
                }
            }
            if (chkNonSuperInvestmentsSavings.Checked)
            {
                OS.Milliman.Actions.Action lAction = new OS.Milliman.Actions.Action();
                //OS.Milliman.Client.SmallAction[] lActionArray = { lAction };

                lAction.Type = "ADDSAVINGS";
                if (txtNSISAmountSaved.Value.HasValue)
                {
                    int freq = Convert.ToInt32(cmbFreq.SelectedValue);
                    lAction.Amount = (double)(txtNSISAmountSaved.Value) * freq;
                    lAction.AmountSpecified = true;
                }
                else
                {
                    errorMsg = errorMsg + "\r\n" + "NonSuperInvestmentsSavings -> AmountSaved is invalid";
                    isValid = false;
                }
                if (txtNSISStartAge.Value.HasValue)
                {
                    lAction.StartAge = Convert.ToInt64(txtNSISStartAge.Value);
                    lAction.StartAgeSpecified = true;
                }
                else
                {
                    errorMsg = errorMsg + "\r\n" + "NonSuperInvestmentsSavings -> StartAge is invalid";
                    isValid = false;
                }
                if (txtNSISStopAge.Value.HasValue)
                {
                    lAction.EndAge = Convert.ToInt64(txtNSISStopAge.Value);
                    lAction.EndAgeSpecified = true;
                }
                else
                {
                    errorMsg = errorMsg + "\r\n" + "NonSuperInvestmentsSavings -> StopAge is invalid";
                    isValid = false;
                }
                lAction.ActionNumber = lActionNumber++;
                lAction.PercentageSpecified = false;

                if (isValid)
                {
                    //lClient.SetActionsForPerson(lEclipseId, lActionArray);
                    //Logging Input Data
                    //PrepareGraphInputData("Actions", lActionArray);
                    lActionArrayInfo.Add(lAction);
                    lActionsArray[3] = lAction;
                }
            }

            if (lActionArrayInfo.Count > 0)
                PrepareGraphInputData("Actions", lActionArrayInfo.ToArray());

            //if (lActionNumber <= 0)
            //{
            //    isValid = false;
            //    errorMsg = errorMsg + "\r\n" + "At least one Action should be set.";
            //}

            return lActionArrayInfo.ToArray();
        }
        private OS.Milliman.Actions.Goal[] SetGoals()
        {
            //OWais -------------------------------------------------------------------------------------------------------------------
            List<OS.Milliman.Actions.Goal> listlGoals = new List<OS.Milliman.Actions.Goal>();
            GetGoalsGridData(chkWealth, "WEALTH", grdGoalsWealth, goalsWealthSessionID, listlGoals, "TotalAssetsWorth", "DollarsFromAge", "txtGoalsWealth_TotalAssetsWorth", "txtGoalsWealth_DollarsFromAge");
            GetGoalsGridData(chkIncome, "INCOME", grdGoalsIncome, goalsIncomeSessionID, listlGoals, "TotalIncomeAmount", "DollarsFromAge", "txtGoalsIncome_TotalIncomeRequired", "txtGoalsIncome_DollarsFromAge");
            GetGoalsGridData(chkLumpSum, "LUMPSUM", grdGoalsLumpSum, goalsLumpSumSessionID, listlGoals, "TotalLumpSumAmount", "DollarsFromAge", "txtGoalsLumpSum_TotalLumpSum", "txtGoalsLumpSum_DollarsFromAge");
            GetGoalsGridData(chkLiquidAssets, "LIQUIDASSETS", grdGoalsLiquidAssets, goalsLiquidAssetsSessionID, listlGoals, "TotalLiquidAssetsAmount", "DollarsFromAge", "txtGoalsLiquidAssets_TotalLiquidAssets", "txtGoalsLiquidAssets_DollarsFromAge");
            GetGoalsGridData(chkBequest, "BEQUEST", grdGoalsBequest, goalsBequestSessionID, listlGoals, "TotalBequestAmount", "DollarsFromAge", "txtGoalsBequest_TotalBequest", "txtGoalsBequest_DollarsFromAge");
            //-------------------------------------------------------------------------------------------------------------------------

            if (listlGoals.Count > 0)
            {
                Session[goalsSessionID] = listlGoals;
                PrepareGraphInputData("Goals", listlGoals);
                return listlGoals.ToArray();
            }

            isValid = false;
            errorMsg = errorMsg + "\r\n" + "At least one goal should be set.";
            return null;
        }
        private void Projection(Person personInformation, OS.Milliman.Actions.Holding[] assetsInformation, OS.Milliman.Actions.Goal[] goalsInformation, OS.Milliman.Actions.Action[] actionsInformation, long investmentStrategyInformation, InvestmentStrategyProducts[] products)
        {
            try
            {
                //Projection Data
                string lProjectionId;
                lProjectionId = WebUtilities.MillimanServiceUtility.RunProjectionForClient(lEclipseId, lClientCode, personInformation,
                                                                   investmentStrategyInformation.ToString(),
                                                                   assetsInformation, actionsInformation,
                                                                   goalsInformation, products);

                if (!string.IsNullOrEmpty(lProjectionId))
                {
                    Regex regex = new Regex("SUCCESS: ProjectionID:([0-9]+)");
                    Match matches = regex.Match(lProjectionId);
                    if (matches.Success)
                    {
                        string lOperatorId = string.Empty;
                        lProjectionId = matches.Groups[1].Value;
                        var opIds = WebUtilities.MillimanServiceUtility.GetOperatorListForProjectionId(long.Parse(lProjectionId), true);
                        SortedList<string, DataSet> projections = new SortedList<string, DataSet>();

                        foreach (DataRow dt in opIds.Tables[0].Rows)
                        {
                            lOperatorId = dt[0].ToString();
                            //if (!lOperatorId.Contains("Goal") || lOperatorId.Contains("Percentile"))
                            //{
                            DataSet projectionResult =
                                WebUtilities.MillimanServiceUtility.GetProjectionResultsForClient(
                                    long.Parse(lProjectionId), true, lOperatorId);
                            projections.Add(lOperatorId, projectionResult);

                            //Generate Output Data Log
                            PrepareGraphOutputData(lOperatorId, projections.First(type => type.Key == lOperatorId).Value);
                            //}
                        }

                        //Merging of service datasets ---------------------------------------------------------------
                        //DataSet projectionGoalsResult = new DataSet();
                        //DataSet projectionGoalsTempResult = null;
                        //string previouslOperatorId = string.Empty;
                        //string previouslTempOperatorId = string.Empty;
                        //foreach (DataRow dt in opIds.Tables[0].Rows)
                        //{
                        //    lOperatorId = dt[0].ToString();
                        //    if (lOperatorId.Contains("Goal") && !lOperatorId.Contains("Percentile"))
                        //    {
                        //        previouslOperatorId = Regex.Match(lOperatorId, @"\d+").Value;
                        //        previouslOperatorId = lOperatorId.Substring(0, lOperatorId.IndexOf(previouslOperatorId));
                        //        if (previouslOperatorId == previouslTempOperatorId || previouslTempOperatorId == string.Empty)
                        //        {
                        //            previouslTempOperatorId = previouslOperatorId;
                        //            projectionGoalsTempResult = WebUtilities.MillimanServiceUtility.GetProjectionResultsForClient(long.Parse(lProjectionId), true, lOperatorId);
                        //            if (projectionGoalsTempResult != null)
                        //            {
                        //                projectionGoalsResult.Merge(projectionGoalsTempResult, false);
                        //            }
                        //        }
                        //        else
                        //        {
                        //            projections.Add(previouslOperatorId, projectionGoalsResult);
                        //            PrepareGraphOutputData(previouslOperatorId, projections.First(type => type.Key == previouslOperatorId).Value);
                        //            previouslTempOperatorId = string.Empty;
                        //            projectionGoalsResult = new DataSet();
                        //        }
                        //    }
                        //}

                        //if (previouslTempOperatorId != string.Empty)
                        //{
                        //    projections.Add(previouslOperatorId, projectionGoalsResult);
                        //}
                        ////---------------------------------------------------------------


                        //Set Probability Timestamp textbox value to the minimum goal + start age ----------------------
                        if (goalsInformation.Length > 0)
                        {
                            txtNumTimeSteps.MinValue = goalsInformation.Min(m => m.StartAge);
                            txtNumTimeSteps.Value = txtNumTimeSteps.MinValue;

                            txtNumTimeStepsCompareRuns.MinValue = goalsInformation.Min(m => m.StartAge);
                            txtNumTimeStepsCompareRuns.Value = txtNumTimeStepsCompareRuns.MinValue;
                        }
                        else
                        {
                            if (txtPDDOB.SelectedDate.HasValue)
                            {
                                txtNumTimeSteps.MinValue = Convert.ToInt32(Math.Floor(Convert.ToDouble((DateTime.Today - txtPDDOB.SelectedDate.Value).TotalDays / 365)));
                                txtNumTimeSteps.Value = txtNumTimeSteps.MinValue;

                                txtNumTimeStepsCompareRuns.MinValue = Convert.ToInt32(Math.Floor(Convert.ToDouble((DateTime.Today - txtPDDOB.SelectedDate.Value).TotalDays / 365)));
                                txtNumTimeStepsCompareRuns.Value = txtNumTimeStepsCompareRuns.MinValue;
                            }
                        }

                        //----------------------------------------------------------------------------------------------
                        Session[projectionsSessionID] = projections;
                    }
                    else
                    {
                        regex = new Regex("ERROR(.*)");
                        matches = regex.Match(lProjectionId);
                        if (matches.Success)
                        {
                            errorMsg = errorMsg + "\r\n" + matches.Groups[1].Value;
                            isValid = false;
                        }
                        else
                        {
                            errorMsg = errorMsg + "\r\n" + "Unexpected error while executing Projection.";
                        }
                    }
                }
                else
                {
                    errorMsg = errorMsg + "\r\n" + "RunProjectionForClient returned NULL projection ID.";
                    isValid = false;
                }
            }
            catch (Exception ex)
            {
                errorMsg = errorMsg + "\r\n" + "Unexpected error while executing Projection.\r\nException: " +
                           ex.Message;
                WebUtilities.Utilities.LogException(ex, Request, Context);
                isValid = false;
            }
        }

        #endregion Milliman

        #region Private Methods - Miscellaneous methods like getting and saving of data for different controls like textbox, grid etc.

        //General Methods related to saving and getting of Advices related data.
        private void AddProjectionRun(int selectedValue)
        {
            rcProjectionRuns.Items.Add(new RadComboBoxItem("Run " + (rcProjectionRuns.Items.Count + 1), Convert.ToString(rcProjectionRuns.Items.Count + 1)));
            if (selectedValue >= 1)
                rcProjectionRuns.SelectedValue = selectedValue.ToString();

        }
        private void PopulateInvestmentStrategies()
        {
            try
            {
                //wsGetValueLists wsLists = new wsGetValueLists();
                //DataSet ds = wsLists.GetInvestmentStrategies();
                DataSet ds = WebUtilities.MillimanServiceUtility.GetInvestmentStrategies();
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        ds.Tables[0].Rows[5].Delete();
                        rcbInvestmentStrategy.DataSource = ds.Tables[0];

                        rcbInvestmentStrategy.DataValueField = ds.Tables[0].Columns["ID"].ColumnName;
                        rcbInvestmentStrategy.DataTextField = ds.Tables[0].Columns["Name"].ColumnName;

                        rcbInvestmentStrategy.DataBind();
                        rcbInvestmentStrategy.SelectedValue = ds.Tables[0].Rows[0][0].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                RegisterErrorMessageScript("Unable to load Investment Strategies. Please reload the page again.", true, ex);
            }
        }

        [WebMethod(EnableSession = true)]
        public static string GetInvestmentStrategyAssetWeightings(string InvestmentStrategyID)
        {
            DataSet ds = WebUtilities.MillimanServiceUtility.GetInvestmentStrategyAssetWeightings(InvestmentStrategyID);

            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                    return GetJson(ds.Tables[0]);
            }
            return "error";
        }

        private IndividualDS GetIndividualDS()
        {
            IndividualControl.SetEntity(_cid);

            var individualcm = UMABroker.GetBMCInstance(_cid);
            IndividualDS ds = new IndividualDS();
            if (individualcm != null)
            {
                ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
                individualcm.GetData(ds);
                UMABroker.ReleaseBrokerManagedComponent(individualcm);
            }
            return ds;
        }
        private void SetControlsDefaultValues(IndividualDS ds)
        {
            if (ds.Tables.Count > 0 && ds.IndividualTable.Rows.Count > 0)
            {
                DataRow dr = ds.IndividualTable.Rows[0];

                txtPDName.Text = dr[ds.IndividualTable.FULLNAME].ToString();
                if (string.IsNullOrEmpty(Request.Params["isedit"]))
                {
                    txtAdviceName.Text = string.Format("{0} advice", txtPDName.Text);
                }

                litGetDate.Text = "Current Age:";
                hidCurrentAge.Value = string.Empty;

                if (!(dr[ds.IndividualTable.DOB] is DBNull))
                {
                    DateTime d = (DateTime)dr[ds.IndividualTable.DOB];

                    if (DateTime.Compare(d, txtPDDOB.MinDate) >= 0)
                    {
                        txtPDDOB.SelectedDate = d;
                        if (dr[ds.IndividualTable.DOB].ToString() != string.Empty)
                        {
                            int strCurrentAge = WebUtilities.Utilities.CalculateCurrentAge(d);
                            litGetDate.Text = "Current Age:" + strCurrentAge.ToString();
                            hidCurrentAge.Value = strCurrentAge.ToString();
                        }
                    }
                }
                if (!string.IsNullOrEmpty(dr[ds.IndividualTable.GENDER].ToString()))
                {
                    txtPDGender.Text = dr[ds.IndividualTable.GENDER].ToString();
                }
            }
        }

        private AdvicesDS SaveAdvice(bool shouldPersist)
        {
            var adviceDS = new AdvicesDS
            {
                CommandType = DatasetCommandTypes.Add,
                Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() },
            };

            SetAdvice(adviceDS);
            SetPersonalDetails(adviceDS.PersonalDetailsTable);
            SetCurrentAssets(adviceDS.CurrentAssetsTable);
            SetAdviceActions(adviceDS.AdviceActionTable);
            SetGoals(adviceDS.GoalsTable);
            if (shouldPersist)
            {
                SaveWellKnownComponent(adviceDS, WellKnownCM.OBP);
            }
            return adviceDS;
        }

        private void SetAdvice(AdvicesDS adviceDS)
        {
            AdvicesRow dr = adviceDS.AdvicesTable.GetNewRow();
            dr[adviceDS.AdvicesTable.TITLE] = txtAdviceName.Text;
            lblProjectionName.Text = txtAdviceName.Text;
            dr[adviceDS.AdvicesTable.CLIENTCID] = Guid.Empty;
            dr[adviceDS.AdvicesTable.CLIENTID] = string.Empty;
            dr[adviceDS.AdvicesTable.INDIVIDUALCID] = _cid;
            dr[adviceDS.AdvicesTable.INDIVIDUALCLIENTID] = ClientHeaderInfo1.ClientId;
            dr[adviceDS.AdvicesTable.USER] = GetCurrentUser().CID;
            dr.MillimanResultSet = (SortedList<string, DataSet>)Session[projectionsSessionID];
            dr[adviceDS.AdvicesTable.CREATEDON] = DateTime.Now;
            dr[adviceDS.AdvicesTable.MODIFIEDON] = DateTime.Now;
            dr[adviceDS.AdvicesTable.INVESTMENTSTRATEGY] = rcbInvestmentStrategy.SelectedItem.Text;

            adviceDS.Tables[adviceDS.AdvicesTable.TableName].Rows.Add(dr);
        }
        private void SetPersonalDetails(PersonalDetailsTable pdTable)
        {
            DataRow dr = pdTable.NewRow();

            dr[pdTable.NAME] = txtPDName.Text;
            if (txtPDDOB.SelectedDate != null)
                dr[pdTable.DOB] = txtPDDOB.SelectedDate.Value;

            if (!string.IsNullOrEmpty(txtPDRetirementAge.Text))
                dr[pdTable.RETIREMENTAGE] = decimal.Parse(txtPDRetirementAge.Text);

            if (!string.IsNullOrEmpty(txtPDSalary.Text))
                dr[pdTable.SALARY] = decimal.Parse(txtPDSalary.Text);

            if (!string.IsNullOrEmpty(txtPDPlanAge.Text))
                dr[pdTable.PLANAGE] = int.Parse(txtPDPlanAge.Text);

            pdTable.Rows.Add(dr);
        }
        private void SetCurrentAssets(CurrentAssetsTable caTable)
        {
            DataRow dr = caTable.NewRow();

            #region Superannuation

            if (chkSuperannuation.Checked)
            {
                dr = caTable.NewRow();
                dr[caTable.ASSETTYPE] = OBPCommon.AssetType.Superannuation;

                if (!string.IsNullOrEmpty(txtSEquity.Text))
                    dr[caTable.EQUITY] = decimal.Parse(txtSEquity.Text);

                if (!string.IsNullOrEmpty(txtSFixedInterest.Text))
                    dr[caTable.FIXEDINTEREST] = decimal.Parse(txtSFixedInterest.Text);

                if (!string.IsNullOrEmpty(txtSInternationalEquity.Text))
                    dr[caTable.INTERNATIONALEQUITY] = decimal.Parse(txtSInternationalEquity.Text);

                if (!string.IsNullOrEmpty(txtSCash.Text))
                    dr[caTable.CASH] = decimal.Parse(txtSCash.Text);

                if (!string.IsNullOrEmpty(txtSGold.Text))
                    dr[caTable.GOLD] = decimal.Parse(txtSGold.Text);

                if (!string.IsNullOrEmpty(txtSProperty.Text))
                    dr[caTable.PROPERTY] = decimal.Parse(txtSProperty.Text);


                caTable.Rows.Add(dr);
            }

            #endregion

            #region Pension

            if (chkPension.Checked)
            {
                dr = caTable.NewRow();
                dr[caTable.ASSETTYPE] = OBPCommon.AssetType.Pension;

                if (!string.IsNullOrEmpty(txtPEquity.Text))
                    dr[caTable.EQUITY] = decimal.Parse(txtPEquity.Text);

                if (!string.IsNullOrEmpty(txtPFixedInterest.Text))
                    dr[caTable.FIXEDINTEREST] = decimal.Parse(txtPFixedInterest.Text);

                if (!string.IsNullOrEmpty(txtPInternationalEquity.Text))
                    dr[caTable.INTERNATIONALEQUITY] = decimal.Parse(txtPInternationalEquity.Text);

                if (!string.IsNullOrEmpty(txtPCash.Text))
                    dr[caTable.CASH] = decimal.Parse(txtPCash.Text);

                if (!string.IsNullOrEmpty(txtPGold.Text))
                    dr[caTable.GOLD] = decimal.Parse(txtPGold.Text);

                if (!string.IsNullOrEmpty(txtPProperty.Text))
                    dr[caTable.PROPERTY] = decimal.Parse(txtPProperty.Text);


                caTable.Rows.Add(dr);
            }

            #endregion

            #region NonSuperInvestments

            if (chkNonSuperInvestments.Checked)
            {
                dr = caTable.NewRow();
                dr[caTable.ASSETTYPE] = OBPCommon.AssetType.NonSuperInvestments;

                if (!string.IsNullOrEmpty(txtNEquity.Text))
                    dr[caTable.EQUITY] = decimal.Parse(txtNEquity.Text);

                if (!string.IsNullOrEmpty(txtNFixedInterest.Text))
                    dr[caTable.FIXEDINTEREST] = decimal.Parse(txtNFixedInterest.Text);

                if (!string.IsNullOrEmpty(txtNInternationalEquity.Text))
                    dr[caTable.INTERNATIONALEQUITY] = decimal.Parse(txtNInternationalEquity.Text);

                if (!string.IsNullOrEmpty(txtNCash.Text))
                    dr[caTable.CASH] = decimal.Parse(txtNCash.Text);

                if (!string.IsNullOrEmpty(txtNGold.Text))
                    dr[caTable.GOLD] = decimal.Parse(txtNGold.Text);

                if (!string.IsNullOrEmpty(txtNProperty.Text))
                    dr[caTable.PROPERTY] = decimal.Parse(txtNProperty.Text);


                caTable.Rows.Add(dr);
            }

            #endregion
        }
        private void SetAdviceActions(AdviceActionTable aaTable)
        {
            DataRow dr = aaTable.NewRow();

            #region Super Guarantee Contributions

            if (chkSuperGuaranteeContributions.Checked)
            {
                dr = aaTable.NewRow();
                dr[aaTable.ACTIONTYPE] = OBPCommon.ActionType.SuperGuaranteeContributions;

                if (!string.IsNullOrEmpty(txtSGCPercentSalary.Text))
                    dr[aaTable.PERCENTOFSALARY] = decimal.Parse(txtSGCPercentSalary.Text);

                aaTable.Rows.Add(dr);
            }

            #endregion

            #region Super Salary Sacrifice Contributions

            if (chkSuperSalarySacrificeContributions.Checked)
            {
                dr = aaTable.NewRow();
                dr[aaTable.ACTIONTYPE] = OBPCommon.ActionType.SuperSalarySacrificeContributions;


                //Owais -----------------------------------------------------------------------------------------
                //if (!string.IsNullOrEmpty(txtSSSCPercentSalary.Text))
                //dr[aaTable.PERCENTOFSALARY] = int.Parse(txtSSSCPercentSalary.Text);
                //Owais -----------------------------------------------------------------------------------------


                if (txtSSSCPercentSalary.Value.HasValue)
                    dr[aaTable.PERCENTOFSALARY] = decimal.Parse(txtSSSCPercentSalary.Text);

                //Owais -----------------------------------------------------------------------------------------
                //if (!string.IsNullOrEmpty(txtSSSCAmount.Text))
                //dr[aaTable.AMOUNT] = decimal.Parse(txtSSSCAmount.Text);
                //Owais -----------------------------------------------------------------------------------------

                if (txtSSSCAmount.Value.HasValue)
                    dr[aaTable.AMOUNT] = decimal.Parse(txtSSSCAmount.Text);

                if (!string.IsNullOrEmpty(txtSSSCStartAge.Text))
                    dr[aaTable.CONTRIBUTIONSTARTAGE] = int.Parse(txtSSSCStartAge.Text);

                if (!string.IsNullOrEmpty(txtSSSCStopAge.Text))
                    dr[aaTable.CONTRIBUTIONENDAGE] = int.Parse(txtSSSCStopAge.Text);

                aaTable.Rows.Add(dr);
            }

            #endregion

            #region Super After Tax Contributions

            if (chkSuperAfterTaxContributions.Checked)
            {
                dr = aaTable.NewRow();
                dr[aaTable.ACTIONTYPE] = OBPCommon.ActionType.SuperAfterTaxContributions;

                if (!string.IsNullOrEmpty(txtSATCAmount.Text))
                    dr[aaTable.AMOUNT] = decimal.Parse(txtSATCAmount.Text);

                if (!string.IsNullOrEmpty(txtSATCStartAge.Text))
                    dr[aaTable.CONTRIBUTIONSTARTAGE] = int.Parse(txtSATCStartAge.Text);

                if (!string.IsNullOrEmpty(txtSATCStopAge.Text))
                    dr[aaTable.CONTRIBUTIONENDAGE] = int.Parse(txtSATCStopAge.Text);

                aaTable.Rows.Add(dr);
            }

            #endregion

            #region Non Super Investments Savings

            if (chkNonSuperInvestmentsSavings.Checked)
            {
                dr = aaTable.NewRow();
                dr[aaTable.ACTIONTYPE] = OBPCommon.ActionType.NonSuperInvestments;

                if (!string.IsNullOrEmpty(txtNSISAmountSaved.Text))
                    dr[aaTable.AMOUNT] = decimal.Parse(txtNSISAmountSaved.Text);

                if (!string.IsNullOrEmpty(txtNSISStartAge.Text))
                    dr[aaTable.CONTRIBUTIONSTARTAGE] = int.Parse(txtNSISStartAge.Text);

                if (!string.IsNullOrEmpty(txtNSISStopAge.Text))
                    dr[aaTable.CONTRIBUTIONENDAGE] = int.Parse(txtNSISStopAge.Text);

                aaTable.Rows.Add(dr);
            }

            #endregion
        }

        private void SetGoals(GoalsTable gTable)
        {
            //Syed Owais Ahmed
            SetGoalsGridData(chkWealth, grdGoalsWealth, OBPCommon.GoalType.Wealth, goalsWealthSessionID, gTable, "TotalAssetsWorth", "DollarsFromAge", "txtGoalsWealth_TotalAssetsWorth", "txtGoalsWealth_DollarsFromAge");
            SetGoalsGridData(chkIncome, grdGoalsIncome, OBPCommon.GoalType.Income, goalsIncomeSessionID, gTable, "TotalIncomeAmount", "DollarsFromAge", "txtGoalsIncome_TotalIncomeRequired", "txtGoalsIncome_DollarsFromAge");
            SetGoalsGridData(chkLumpSum, grdGoalsLumpSum, OBPCommon.GoalType.LumpSum, goalsLumpSumSessionID, gTable, "TotalLumpSumAmount", "DollarsFromAge", "txtGoalsLumpSum_TotalLumpSum", "txtGoalsLumpSum_DollarsFromAge");
            SetGoalsGridData(chkLiquidAssets, grdGoalsLiquidAssets, OBPCommon.GoalType.LiquidAssets, goalsLiquidAssetsSessionID, gTable, "TotalLiquidAssetsAmount", "DollarsFromAge", "txtGoalsLiquidAssets_TotalLiquidAssets", "txtGoalsLiquidAssets_DollarsFromAge");
            SetGoalsGridData(chkBequest, grdGoalsBequest, OBPCommon.GoalType.Bequest, goalsBequestSessionID, gTable, "TotalBequestAmount", "DollarsFromAge", "txtGoalsBequest_TotalBequest", "txtGoalsBequest_DollarsFromAge");
        }

        private void OnEditAdvice()
        {
            var adviceID = new Guid(Request.Params["id"]);

            ShowSaveAs();

            var ds = new AdvicesDS
            {
                CommandType = DatasetCommandTypes.Details,
                AdviceID = adviceID,
                Unit = new Oritax.TaxSimp.Data.OrganizationUnit
                {
                    CurrentUser = GetCurrentUser(),
                },
            };

            var obpcm = UMABroker.GetWellKnownBMC(WellKnownCM.OBP);
            obpcm.GetData(ds);
            UMABroker.ReleaseBrokerManagedComponent(obpcm);

            PopulateAdvice(ds);
        }
        private void ShowSaveAs()
        {
            btnSave.Style.Add("display", "none");
            btnSaveAs.Style.Add("display", "inline");
        }
        private void PopulateAdvice(AdvicesDS ds)
        {
            txtAdviceName.Text = ds.AdvicesTable.Rows[0][ds.AdvicesTable.TITLE].ToString();
            PopulatePersonalDetails(ds.PersonalDetailsTable);
            PopulateCurrentAssets(ds.CurrentAssetsTable);
            PopulateActions(ds.AdviceActionTable);
            PopulateGoals(ds.GoalsTable);
            try
            {
                SetGoalsForGraphs();
            }
            catch
            {
            }
            string investmentStrategy = ds.AdvicesTable.Rows[0][ds.AdvicesTable.INVESTMENTSTRATEGY].ToString();
            if (!string.IsNullOrEmpty(investmentStrategy) && rcbInvestmentStrategy.Items.Count > 0)
            {
                rcbInvestmentStrategy.SelectedIndex = -1;
                rcbInvestmentStrategy.Items.FindItemByText(investmentStrategy).Selected = true;
                hfInvestementStrategy.Value = rcbInvestmentStrategy.SelectedValue;
            }
        }

        private void SetGoalsForGraphs()
        {
            //OWais -------------------------------------------------------------------------------------------------------------------
            List<Goal> listlGoals = new List<Goal>();
            SetGoalsGridDataForGraph(chkWealth, "WEALTH", grdGoalsWealth, goalsWealthSessionID, listlGoals);
            SetGoalsGridDataForGraph(chkIncome, "INCOME", grdGoalsIncome, goalsIncomeSessionID, listlGoals);
            SetGoalsGridDataForGraph(chkLumpSum, "LUMPSUM", grdGoalsLumpSum, goalsLumpSumSessionID, listlGoals);
            SetGoalsGridDataForGraph(chkLiquidAssets, "LIQUIDASSETS", grdGoalsLiquidAssets, goalsLiquidAssetsSessionID,
                                     listlGoals);
            SetGoalsGridDataForGraph(chkBequest, "BEQUEST", grdGoalsBequest, goalsBequestSessionID, listlGoals);
            //-------------------------------------------------------------------------------------------------------------------------

            if (listlGoals.Count > 0)
            {
                Session[goalsSessionID] = listlGoals;
            }
        }
        private void PopulatePersonalDetails(PersonalDetailsTable pdTable)
        {
            DataRow dr = pdTable.Rows[0];

            if (dr[pdTable.RETIREMENTAGE] != null)
                txtPDRetirementAge.Text = dr[pdTable.RETIREMENTAGE].ToString();

            if (dr[pdTable.SALARY] != null)
                txtPDSalary.Text = dr[pdTable.SALARY].ToString();

            if (dr[pdTable.PLANAGE] != null)
                txtPDPlanAge.Text = dr[pdTable.PLANAGE].ToString();
        }
        private void PopulateCurrentAssets(CurrentAssetsTable caTable)
        {
            chkSuperannuation.Checked = false;

            txtSEquity.Text = "0";
            txtSFixedInterest.Text = "0";
            txtSInternationalEquity.Text = "0";
            txtSCash.Text = "0";
            txtSGold.Text = "0";
            txtSProperty.Text = "0";

            chkPension.Checked = false;

            txtPEquity.Text = "0";
            txtPFixedInterest.Text = "0";
            txtPInternationalEquity.Text = "0";
            txtPCash.Text = "0";
            txtPGold.Text = "0";
            txtPProperty.Text = "0";

            chkNonSuperInvestments.Checked = false;

            txtNEquity.Text = "0";
            txtNFixedInterest.Text = "0";
            txtNInternationalEquity.Text = "0";
            txtNCash.Text = "0";
            txtNGold.Text = "0";
            txtNProperty.Text = "0";

            foreach (DataRow dr in caTable.Rows)
            {
                switch ((OBPCommon.AssetType)dr[caTable.ASSETTYPE])
                {
                    case OBPCommon.AssetType.Superannuation:
                        PopulateSuperannuation(caTable, dr);
                        break;
                    case OBPCommon.AssetType.Pension:
                        PopulatePension(caTable, dr);
                        break;
                    case OBPCommon.AssetType.NonSuperInvestments:
                        PopulateNonSuperInvestments(caTable, dr);
                        break;
                }
            }
        }
        private void PopulateSuperannuation(CurrentAssetsTable caTable, DataRow dr)
        {
            if (dr[caTable.EQUITY] != null)
                txtSEquity.Text = dr[caTable.EQUITY].ToString();

            if (dr[caTable.FIXEDINTEREST] != null)
                txtSFixedInterest.Text = dr[caTable.FIXEDINTEREST].ToString();

            if (dr[caTable.INTERNATIONALEQUITY] != null)
                txtSInternationalEquity.Text = dr[caTable.INTERNATIONALEQUITY].ToString();

            if (dr[caTable.CASH] != null)
                txtSCash.Text = dr[caTable.CASH].ToString();

            if (dr[caTable.GOLD] != null)
                txtSGold.Text = dr[caTable.GOLD].ToString();

            if (dr[caTable.PROPERTY] != null)
                txtSProperty.Text = dr[caTable.PROPERTY].ToString();

            if (txtSEquity.Text != "0" || txtSFixedInterest.Text != "0" || txtSInternationalEquity.Text != "0" || txtSCash.Text != "0" || txtSGold.Text != "0" || txtSProperty.Text != "0")
                chkSuperannuation.Checked = true;

        }
        private void PopulatePension(CurrentAssetsTable caTable, DataRow dr)
        {
            if (dr[caTable.EQUITY] != null)
                txtPEquity.Text = dr[caTable.EQUITY].ToString();

            if (dr[caTable.FIXEDINTEREST] != null)
                txtPFixedInterest.Text = dr[caTable.FIXEDINTEREST].ToString();

            if (dr[caTable.INTERNATIONALEQUITY] != null)
                txtPInternationalEquity.Text = dr[caTable.INTERNATIONALEQUITY].ToString();

            if (dr[caTable.CASH] != null)
                txtPCash.Text = dr[caTable.CASH].ToString();

            if (dr[caTable.GOLD] != null)
                txtPGold.Text = dr[caTable.GOLD].ToString();

            if (dr[caTable.PROPERTY] != null)
                txtPProperty.Text = dr[caTable.PROPERTY].ToString();

            if (txtPEquity.Text != "0" || txtPFixedInterest.Text != "0" || txtPInternationalEquity.Text != "0" || txtPCash.Text != "0" || txtPGold.Text != "0" || txtPProperty.Text != "0")
                chkPension.Checked = true;
        }
        private void PopulateNonSuperInvestments(CurrentAssetsTable caTable, DataRow dr)
        {
            if (dr[caTable.EQUITY] != null)
                txtNEquity.Text = dr[caTable.EQUITY].ToString();

            if (dr[caTable.FIXEDINTEREST] != null)
                txtNFixedInterest.Text = dr[caTable.FIXEDINTEREST].ToString();

            if (dr[caTable.INTERNATIONALEQUITY] != null)
                txtNInternationalEquity.Text = dr[caTable.INTERNATIONALEQUITY].ToString();

            if (dr[caTable.CASH] != null)
                txtNCash.Text = dr[caTable.CASH].ToString();

            if (dr[caTable.GOLD] != null)
                txtNGold.Text = dr[caTable.GOLD].ToString();

            if (dr[caTable.PROPERTY] != null)
                txtNProperty.Text = dr[caTable.PROPERTY].ToString();

            if (txtNEquity.Text != "0" || txtNFixedInterest.Text != "0" || txtNInternationalEquity.Text != "0" || txtNCash.Text != "0" || txtNGold.Text != "0" || txtNProperty.Text != "0")
                chkNonSuperInvestments.Checked = true;
        }
        private void PopulateActions(AdviceActionTable aaTable)
        {
            foreach (DataRow dr in aaTable.Rows)
            {
                switch ((OBPCommon.ActionType)dr[aaTable.ACTIONTYPE])
                {
                    case OBPCommon.ActionType.SuperGuaranteeContributions:
                        PopulateSuperGuaranteeContributions(aaTable, dr);
                        break;
                    case OBPCommon.ActionType.SuperSalarySacrificeContributions:
                        PopulateSuperSalarySacrificeContributions(aaTable, dr);
                        break;
                    case OBPCommon.ActionType.SuperAfterTaxContributions:
                        PopulateSuperAfterTaxContributions(aaTable, dr);
                        break;
                    case OBPCommon.ActionType.NonSuperInvestments:
                        PopulateNonSuperInvestmentsSavings(aaTable, dr);
                        break;
                }
            }
        }
        private void PopulateSuperGuaranteeContributions(AdviceActionTable aaTable, DataRow dr)
        {
            chkSuperGuaranteeContributions.Checked = true;

            if (dr[aaTable.PERCENTOFSALARY] != null)
                txtSGCPercentSalary.Text = dr[aaTable.PERCENTOFSALARY].ToString();
        }
        private void PopulateSuperSalarySacrificeContributions(AdviceActionTable aaTable, DataRow dr)
        {
            chkSuperSalarySacrificeContributions.Checked = true;

            if (dr[aaTable.PERCENTOFSALARY] != null)
                txtSSSCPercentSalary.Text = dr[aaTable.PERCENTOFSALARY].ToString();

            if (dr[aaTable.AMOUNT] != null)
                txtSSSCAmount.Text = dr[aaTable.AMOUNT].ToString();

            if (dr[aaTable.CONTRIBUTIONSTARTAGE] != null)
                txtSSSCStartAge.Text = dr[aaTable.CONTRIBUTIONSTARTAGE].ToString();

            if (dr[aaTable.CONTRIBUTIONENDAGE] != null)
                txtSSSCStopAge.Text = dr[aaTable.CONTRIBUTIONENDAGE].ToString();
        }
        private void PopulateSuperAfterTaxContributions(AdviceActionTable aaTable, DataRow dr)
        {
            chkSuperAfterTaxContributions.Checked = true;

            if (dr[aaTable.AMOUNT] != null)
                txtSATCAmount.Text = dr[aaTable.AMOUNT].ToString();

            if (dr[aaTable.CONTRIBUTIONSTARTAGE] != null)
                txtSATCStartAge.Text = dr[aaTable.CONTRIBUTIONSTARTAGE].ToString();

            if (dr[aaTable.CONTRIBUTIONENDAGE] != null)
                txtSATCStopAge.Text = dr[aaTable.CONTRIBUTIONENDAGE].ToString();
        }
        private void PopulateNonSuperInvestmentsSavings(AdviceActionTable aaTable, DataRow dr)
        {
            chkNonSuperInvestmentsSavings.Checked = true;

            if (dr[aaTable.AMOUNT] != null)
                txtNSISAmountSaved.Text = dr[aaTable.AMOUNT].ToString();

            if (dr[aaTable.CONTRIBUTIONSTARTAGE] != null)
                txtNSISStartAge.Text = dr[aaTable.CONTRIBUTIONSTARTAGE].ToString();

            if (dr[aaTable.CONTRIBUTIONENDAGE] != null)
                txtNSISStopAge.Text = dr[aaTable.CONTRIBUTIONENDAGE].ToString();
        }
        private void PopulateGoals(GoalsTable gTable)
        {
            if (!gTable.Columns.Contains("RowStatus"))
                gTable.Columns.Add("RowStatus", "".GetType());

            DataColumn[] primaryKey = new DataColumn[1];
            primaryKey[0] = gTable.Columns["GoalId"];
            gTable.PrimaryKey = primaryKey;

            PopulateWealth(gTable);
            PopulateIncome(gTable);
            PopulateLumpSum(gTable);
            PopulateLiquidAssets(gTable);
            PopulateBequest(gTable);

            //Set Probability Timestamp textbox value to the minimum goal + start age ----------------------

            if (gTable.Rows.Count > 0)
            {
                txtNumTimeSteps.MinValue = Convert.ToDouble(gTable.Compute("MIN(Age)", ""));
                txtNumTimeSteps.Value = txtNumTimeSteps.MinValue;

                txtNumTimeStepsCompareRuns.MinValue = Convert.ToDouble(gTable.Compute("MIN(Age)", ""));
                txtNumTimeStepsCompareRuns.Value = txtNumTimeStepsCompareRuns.MinValue;

                IsProjectionRun = true;
            }
            else
            {
                if (txtPDDOB.SelectedDate.HasValue)
                {
                    txtNumTimeSteps.MinValue = Convert.ToInt32(Math.Floor(Convert.ToDouble((DateTime.Today - txtPDDOB.SelectedDate.Value).TotalDays / 365)));
                    txtNumTimeSteps.Value = txtNumTimeSteps.MinValue;

                    txtNumTimeStepsCompareRuns.MinValue = Convert.ToInt32(Math.Floor(Convert.ToDouble((DateTime.Today - txtPDDOB.SelectedDate.Value).TotalDays / 365)));
                    txtNumTimeStepsCompareRuns.Value = txtNumTimeStepsCompareRuns.MinValue;
                }
            }

            //----------------------------------------------------------------------------------------------

        }
        private void PopulateWealth(GoalsTable gTable)
        {
            PopulateGoalsGrid(chkWealth, grdGoalsWealth, OBPCommon.GoalType.Wealth, gTable, goalsWealthSessionID);
        }
        private void PopulateIncome(GoalsTable gTable)
        {
            PopulateGoalsGrid(chkIncome, grdGoalsIncome, OBPCommon.GoalType.Income, gTable, goalsIncomeSessionID);
        }
        private void PopulateLumpSum(GoalsTable gTable)
        {
            PopulateGoalsGrid(chkLumpSum, grdGoalsLumpSum, OBPCommon.GoalType.LumpSum, gTable, goalsLumpSumSessionID);
        }
        private void PopulateLiquidAssets(GoalsTable gTable)
        {
            PopulateGoalsGrid(chkLiquidAssets, grdGoalsLiquidAssets, OBPCommon.GoalType.LiquidAssets, gTable,
                              goalsLiquidAssetsSessionID);
        }
        private void PopulateBequest(GoalsTable gTable)
        {
            PopulateGoalsGrid(chkBequest, grdGoalsBequest, OBPCommon.GoalType.Bequest, gTable, goalsBequestSessionID);
        }
        private void HideShowIndividualGrid(bool show)
        {
            string display = (show == true ? "display:block" : "display:none");

            divOver.Attributes["style"] = display;
            divIndividualModal.Attributes["style"] = display;
        }
        private void InitializeTextBoxes()
        {
            //Profile tab
            InitializeTextBoxes(txtPDSalary);

            //Asset Tab - SuperAnnuation
            InitializeTextBoxes(txtSEquity);
            InitializeTextBoxes(txtSFixedInterest);
            InitializeTextBoxes(txtSInternationalEquity);
            InitializeTextBoxes(txtSCash);
            InitializeTextBoxes(txtSProperty);
            InitializeTextBoxes(txtSGold);

            //Asset Tab - Pension
            InitializeTextBoxes(txtPEquity);
            InitializeTextBoxes(txtPFixedInterest);
            InitializeTextBoxes(txtPInternationalEquity);
            InitializeTextBoxes(txtPCash);
            InitializeTextBoxes(txtPProperty);
            InitializeTextBoxes(txtPGold);

            //Asset Tab - Non-Super investments
            InitializeTextBoxes(txtNEquity);
            InitializeTextBoxes(txtNFixedInterest);
            InitializeTextBoxes(txtNInternationalEquity);
            InitializeTextBoxes(txtNCash);
            InitializeTextBoxes(txtNProperty);
            InitializeTextBoxes(txtNGold) ;

            //Action Tab - Super Guarantee Contribution
            //InitializeTextBoxes(txtSGCPercentSalary);
            txtSGCPercentSalary.IncrementSettings.Step = 1;
            txtSGCPercentSalary.NumberFormat.DecimalDigits = amountDecimalDigits;

            //Action Tab - Super Salary Sacrifice Contribution
            //InitializeTextBoxes(txtSSSCPercentSalary);
            txtSSSCPercentSalary.IncrementSettings.Step = 1;
            txtSSSCPercentSalary.NumberFormat.DecimalDigits = amountDecimalDigits;

            InitializeTextBoxes(txtSSSCAmount, maxContributionLimit);
            //InitializeTextBoxes(txtSSSCStartAge);
            //InitializeTextBoxes(txtSSSCStopAge);

            //Action Tab - Super After Tax Contribution
            InitializeTextBoxes(txtSATCAmount);
            //InitializeTextBoxes(txtSATCStartAge);
            //InitializeTextBoxes(txtSATCStopAge);

            //Action Tab - Non-Super Investment (savings)
            InitializeTextBoxes(txtNSISAmountSaved);
            //InitializeTextBoxes(txtNSISStartAge);
            //InitializeTextBoxes(txtNSISStopAge);
        }
        private void InitializeTextBoxes(RadNumericTextBox textBox)
        {
            textBox.IncrementSettings.Step = amountIncrementalStep;
            textBox.NumberFormat.DecimalDigits = amountDecimalDigits;
        }


        private void InitializeTextBoxes(RadNumericTextBox textBox, double maxContributionLimit)
        {
            textBox.IncrementSettings.Step = amountIncrementalStep;
            textBox.NumberFormat.DecimalDigits = amountDecimalDigits;
            textBox.MaxValue = maxContributionLimit;
        }

        private void SetCurrentAssets(IndividualCurrentAssetsDS ds)
        {
            //set Controls

            txtNEquity.Text = txtPEquity.Text = txtSEquity.Text = ds.assetsDetailsTable.AustralianEquities.ToString();
            txtNFixedInterest.Text =
                txtPFixedInterest.Text =
                txtSFixedInterest.Text = ds.assetsDetailsTable.AustralianFixedInterest.ToString();
            txtNInternationalEquity.Text =
                txtPInternationalEquity.Text =
                txtSInternationalEquity.Text = ds.assetsDetailsTable.InternationalEquities.ToString();
            txtNCash.Text = txtPCash.Text = txtSCash.Text = ds.assetsDetailsTable.Cash.ToString();
            txtNProperty.Text = txtPProperty.Text = txtSProperty.Text = ds.assetsDetailsTable.Property.ToString();
            txtNGold.Text = txtPGold.Text = txtSGold.Text = ds.assetsDetailsTable.Gold.ToString();
        }
        private IndividualCurrentAssetsDS GetCurrentAssests(Guid individualCid)
        {
            IndividualCurrentAssetsDS ds = new IndividualCurrentAssetsDS()
            {
                Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() }
            };

            var individualCm = UMABroker.GetBMCInstance(individualCid);
            individualCm.GetData(ds);
            UMABroker.ReleaseBrokerManagedComponent(individualCm);
            return ds;
        }

        //Goals Tab related methods for setting and getting the user-data
        private DataTable PrepareGoalsGridData(DataTable dt)
        {
            DataView dv = dt.DefaultView;
            dv.Sort = "RowStatus";
            dv.RowFilter = "ISNULL(RowStatus, '') = '' OR RowStatus = 'N'";
            return dv.ToTable();
        }

        //Goals Tab related methods for setting and getting the user-data
        private static DataTable RemoveGoalsGridDummyRow(DataTable dt)
        {
            if (dt == null) return null;

            DataView dv = dt.DefaultView;
            string sortingKey = dv.Sort;
            dv.Sort = "Amount";
            dv.RowFilter = "ISNULL(Amount, -1) >= 0";
            dv = dv.ToTable().DefaultView;
            dv.Sort = "Age ASC";
            dt = dv.ToTable();
            DataColumn[] primaryKey = new DataColumn[1];
            primaryKey[0] = dt.Columns["GoalId"];
            dt.PrimaryKey = primaryKey;
            return dt;
        }

        private bool AddGoalsGridRow(RadGrid radGrid, RadNumericTextBox txtAmount, RadNumericTextBox txtAge, string sessionKeyID, string goalType)
        {
            errorMsg = string.Empty;
            isValid = true;
            if (!txtAmount.Value.HasValue)
            {
                errorMsg = errorMsg + "\r\n" + "Goals -> " + goalType + " -> Amount is invalid";
                isValid = false;
            }
            if (!txtAge.Value.HasValue)
            {
                errorMsg = errorMsg + "\r\n" + "Goals -> " + goalType + " -> Start Age is invalid";
                isValid = false;
            }
            if (!txtPDPlanAge.Value.HasValue)
            {
                errorMsg = errorMsg + "\r\n" + "Goals -> " + goalType + " -> End Age is invalid";
                isValid = false;
            }

            if (!isValid)
            {
                RegisterErrorMessageScript(errorMsg, false, null);
                return false;
            }

            Decimal amount = Convert.ToDecimal(txtAmount.Value);
            Int64 age = Convert.ToInt64(txtAge.Value);
            Int64 currentAge = (long)(DateTime.Now - txtPDDOB.SelectedDate).Value.TotalDays / 365;
            Int64 planAge = Convert.ToInt64(txtPDPlanAge.Value);

            if (!(age >= currentAge && age <= planAge))
            {
                errorMsg = errorMsg + "\r\n" + "Goals -> " + goalType + " ->  Age should be between Current Age " +
                           CurrentAge + " and Plan Age " + planAge;
                isValid = false;
            }

            if (!isValid)
            {
                RegisterErrorMessageScript(errorMsg, false, null);
                return false;
            }

            bool isLowerAge = false;
            DataTable dt = Session[sessionKeyID] as DataTable;
            DataView dv = dt.DefaultView;
            dv.Sort = "Age";
            dv.RowFilter = "RowStatus = 'N' OR ISNULL(RowStatus, '') = ''";
            object maxAge = dv.ToTable().Compute("MAX(Age)", "");
            if (maxAge != DBNull.Value && Convert.ToInt64(maxAge) > 0)
            {
                if (age <= Convert.ToInt64(maxAge))
                    isLowerAge = true;
            }

            if (isLowerAge)
            {
                errorMsg = errorMsg + "\r\n" + "Goals -> " + goalType +
                           " ->  Age should be greater than already defined age (" + maxAge + ")";
                isValid = false;
            }

            if (!isValid)
            {
                RegisterErrorMessageScript(errorMsg, false, null);
                return false;
            }

            DataRow dr = dt.NewRow();
            dr[txtAmount.Attributes["DataColumn"].ToString()] = amount;
            dr[txtAge.Attributes["DataColumn"].ToString()] = age;
            dr["RowStatus"] = "N";
            dr["GoalId"] = Guid.NewGuid().ToString();
            dt.Rows.Add(dr);

            radGrid.DataSource = PrepareGoalsGridData(dt);
            radGrid.Rebind();
            Session[sessionKeyID] = dt;

            return true;
        }
        private void RemoveGoalsGridRow(object sender, RadGrid radGrid, string sessionKeyID)
        {
            string rowId = ((ImageButton)sender).CommandArgument;
            DataTable dt = Session[sessionKeyID] as DataTable;
            DataRow dr = dt.Rows.Find(rowId);
            if (dr["RowStatus"].ToString() == "N")
                dt.Rows.Remove(dr);
            else
                dr["RowStatus"] = "D";

            dt.AcceptChanges();
            radGrid.DataSource = PrepareGoalsGridData(dt);
            radGrid.Rebind();
            Session[sessionKeyID] = dt;
        }
        private List<OS.Milliman.Actions.Goal> GetGoalsGridData(CheckBox chkSelected, string goalType, RadGrid radGrid, string sessionKeyID, List<OS.Milliman.Actions.Goal> lstlGoals, string amountColumnName, string ageColumnName, string amountTextBoxName, string ageTextBoxName)
        {
            if (!chkSelected.Checked) return lstlGoals;
            OS.Milliman.Actions.Goal smallGoal = null;
            Int64 goalNumber = 0;
            DataTable dtGoals = Session[sessionKeyID] as DataTable;
            if (dtGoals == null || dtGoals.Rows.Count == 0) return lstlGoals;

            foreach (DataRow row in dtGoals.Rows)
            {
                string lblAmount = row["Amount"].ToString();
                string lblAge = row["Age"].ToString();

                if (!string.IsNullOrEmpty(lblAmount) && lblAmount != "-1")
                {
                    goalNumber += 1;
                    smallGoal = new OS.Milliman.Actions.Goal
                    {
                        GoalType = goalType,
                        Amount = Convert.ToDouble(lblAmount),
                        StartAge = Convert.ToInt64(lblAge),
                        EndAge = goalType == "LUMPSUM" ? Convert.ToInt64(lblAge) : Convert.ToInt64(txtPDPlanAge.Value),
                        GoalNumber = goalNumber,
                        AmountSpecified = true,
                        StartAgeSpecified = true,
                        EndAgeSpecified = true,
                        GoalNumberSpecified = true
                    };

                    lstlGoals.Add(smallGoal);
                }
            }

            string lblAmountFooter = string.Empty;
            string lblAgeFooter = string.Empty;

            //if there is data defined in the footer row while running it, do treat that as input as well.
            foreach (GridFooterItem footeritem in radGrid.MasterTableView.GetItems(GridItemType.Footer))
            {
                RadNumericTextBox txtGoalsWealthTotalAssetsWorth =
                    footeritem[amountColumnName].FindControl(amountTextBoxName) as RadNumericTextBox;
                RadNumericTextBox txtGoalsWealthDollarsFromAge =
                    footeritem[ageColumnName].FindControl(ageTextBoxName) as RadNumericTextBox;

                lblAmountFooter = txtGoalsWealthTotalAssetsWorth.Text;
                lblAgeFooter = txtGoalsWealthDollarsFromAge.Text;

                if (!string.IsNullOrEmpty(lblAmountFooter) && !string.IsNullOrEmpty(lblAgeFooter))
                {
                    goalNumber += 1;
                    smallGoal = new OS.Milliman.Actions.Goal
                    {
                        GoalType = goalType,
                        Amount = Convert.ToDouble(lblAmountFooter),
                        StartAge = Convert.ToInt64(lblAgeFooter),
                        EndAge = goalType == "LUMPSUM" ? Convert.ToInt64(lblAgeFooter) : Convert.ToInt64(txtPDPlanAge.Value),
                        GoalNumber = goalNumber,
                        AmountSpecified = true,
                        StartAgeSpecified = true,
                        EndAgeSpecified = true,
                        GoalNumberSpecified = true
                    };

                    lstlGoals.Add(smallGoal);
                }

            }

            var lstFilteredGoals = lstlGoals.FindAll(g => g.GoalType == goalType);
            lstFilteredGoals.OrderBy(o => o.StartAge);

            if (goalType == "LUMPSUM")
            {
                foreach (Goal g in lstFilteredGoals)
                {
                    Goal sg = lstlGoals.First(f => f.GoalNumber == g.GoalNumber && f.GoalType == g.GoalType);
                    sg.EndAge = sg.StartAge;
                }
            }
            else
            {
                if (lstFilteredGoals.Count() > 1)
                {
                    for (var g = 0; g < lstFilteredGoals.Count(); g++)
                    {
                        Goal cg = lstFilteredGoals[g];
                        if (g + 1 < lstFilteredGoals.Count())
                        {
                            Goal ng = lstFilteredGoals[g + 1];
                            Goal sg = lstlGoals.First(f => f.GoalNumber == cg.GoalNumber && f.GoalType == cg.GoalType);
                            sg.EndAge = ng.StartAge - 1;
                        }
                        else
                        {
                            Goal sg = lstlGoals.First(f => f.GoalNumber == cg.GoalNumber && f.GoalType == cg.GoalType);
                            sg.EndAge = Convert.ToInt64(txtPDPlanAge.Value);
                        }
                    }
                }
            }

            //Restore the grids data after the projection is run again.-------------------------------------------------
            radGrid.DataSource = dtGoals;
            radGrid.Rebind();

            foreach (GridFooterItem footeritem in radGrid.MasterTableView.GetItems(GridItemType.Footer))
            {
                RadNumericTextBox txtGoalsWealthTotalAssetsWorth =
                    footeritem[amountColumnName].FindControl(amountTextBoxName) as RadNumericTextBox;
                RadNumericTextBox txtGoalsWealthDollarsFromAge =
                    footeritem[ageColumnName].FindControl(ageTextBoxName) as RadNumericTextBox;

                txtGoalsWealthTotalAssetsWorth.Text = lblAmountFooter;
                txtGoalsWealthDollarsFromAge.Text = lblAgeFooter;
            }
            //---------------------------------------------------------------------------------------------------------

            return lstlGoals;
        }
        private List<Goal> SetGoalsGridDataForGraph(CheckBox chkSelected, string goalType, RadGrid radGrid, string sessionKeyID, List<Goal> lstlGoals)
        {
            if (!chkSelected.Checked) return lstlGoals;
            Goal smallGoal = null;
            Int64 GoalNumber = 0;
            foreach (
                GridDataItem dataItem in
                    radGrid.MasterTableView.GetItems(GridItemType.Item, GridItemType.AlternatingItem))
            {
                Label lblAmount = (Label)dataItem.Controls[2].Controls[1];
                Label lblAge = (Label)dataItem.Controls[3].Controls[1];

                if (!string.IsNullOrEmpty(lblAmount.Text) && lblAmount.Text != "-1")
                {
                    smallGoal = new Goal
                    {
                        GoalType = goalType,
                        Amount = Convert.ToDouble(lblAmount.Text),
                        StartAge = Convert.ToInt64(lblAge.Text),
                        EndAge = Convert.ToInt64(txtPDPlanAge.Value),//Convert.ToInt64(lblAge.Text) < Convert.ToInt64(txtPDPlanAge.Value) ? Convert.ToInt64(lblAge.Text) : Convert.ToInt64(txtPDPlanAge.Value),
                        GoalNumber = GoalNumber + 1,
                        AmountSpecified = true,
                        StartAgeSpecified = true,
                        EndAgeSpecified = true,
                        GoalNumberSpecified = true
                    };

                    lstlGoals.Add(smallGoal);
                }
            }

            return lstlGoals;
        }

        private void SetGoalsGridData(CheckBox chkSelected, RadGrid radGrid, OBPCommon.GoalType goalType, string sessionKey, GoalsTable gTable, string amountColumnName, string ageColumnName, string amountTextBoxName, string ageTextBoxName)
        {
            DataRow dr = null;
            DataTable dtGoals = Session[sessionKey] as DataTable;
            if (dtGoals == null || dtGoals.Rows.Count == 0) return;

            string lblAmountFooter = string.Empty;
            string lblAgeFooter = string.Empty;

            if (chkSelected.Checked)
            {
                foreach (DataRow row in dtGoals.Rows)
                {
                    string lblAmount = row[gTable.AMOUNT].ToString();
                    string lblAge = row[gTable.AGE].ToString();
                    string lblRowId = row[gTable.GOALID].ToString();
                    if (!string.IsNullOrEmpty(lblAmount) && lblAmount != "-1")
                    {
                        dr = gTable.NewRow();
                        dr[gTable.GOALTYPE] = goalType;

                        if (!string.IsNullOrEmpty(lblAmount))
                            dr[gTable.AMOUNT] = decimal.Parse(lblAmount);

                        if (!string.IsNullOrEmpty(lblAge))
                            dr[gTable.AGE] = int.Parse(lblAge);

                        dr[gTable.GOALID] = new Guid(lblRowId);
                        if ((Guid)dr[gTable.GOALID] == Guid.Empty)
                            dr[gTable.GOALID] = Guid.NewGuid();
                        gTable.Rows.Add(dr);
                    }
                }

                foreach (GridFooterItem footeritem in radGrid.MasterTableView.GetItems(GridItemType.Footer))
                {
                    RadNumericTextBox txtGoalsWealthTotalAssetsWorth =
                        footeritem[amountColumnName].FindControl(amountTextBoxName) as RadNumericTextBox;
                    RadNumericTextBox txtGoalsWealthDollarsFromAge =
                        footeritem[ageColumnName].FindControl(ageTextBoxName) as RadNumericTextBox;

                    lblAmountFooter = txtGoalsWealthTotalAssetsWorth.Text;
                    lblAgeFooter = txtGoalsWealthDollarsFromAge.Text;
                    
                    if (!string.IsNullOrEmpty(lblAmountFooter) && !string.IsNullOrEmpty(lblAgeFooter))
                    {
                        dr = gTable.NewRow();
                        dr[gTable.GOALTYPE] = goalType;

                        if (!string.IsNullOrEmpty(lblAmountFooter))
                            dr[gTable.AMOUNT] = decimal.Parse(lblAmountFooter);

                        if (!string.IsNullOrEmpty(lblAgeFooter))
                            dr[gTable.AGE] = int.Parse(lblAgeFooter);

                        dr[gTable.GOALID] = Guid.NewGuid();

                        gTable.Rows.Add(dr);
                    }
                }
            }

            radGrid.DataSource = dtGoals;
            radGrid.Rebind();

            foreach (GridFooterItem footeritem in radGrid.MasterTableView.GetItems(GridItemType.Footer))
            {
                RadNumericTextBox txtGoalsWealthTotalAssetsWorth =
                    footeritem[amountColumnName].FindControl(amountTextBoxName) as RadNumericTextBox;
                RadNumericTextBox txtGoalsWealthDollarsFromAge =
                    footeritem[ageColumnName].FindControl(ageTextBoxName) as RadNumericTextBox;

                txtGoalsWealthTotalAssetsWorth.Text = lblAmountFooter;
                txtGoalsWealthDollarsFromAge.Text = lblAgeFooter;
            }
        }

        private void PopulateGoalsGrid(CheckBox chkSelected, RadGrid rdGrid, OBPCommon.GoalType goalType, GoalsTable gTable, string sessionKeyID)
        {
            gTable.DefaultView.RowFilter = gTable.GOALTYPE + " = " + (Int16)goalType;
            DataTable dt = gTable.DefaultView.ToTable();
            DataColumn[] primaryKey = new DataColumn[1];
            primaryKey[0] = dt.Columns["GoalId"];
            dt.PrimaryKey = primaryKey;

            bool shouldSelected = true;
            //Dummy Row to create dummy grid row
            if (dt.Rows.Count == 0)
            {
                DataRow row = dt.NewRow();
                row["Amount"] = -1;
                row["GoalId"] = new Guid();
                dt.Rows.Add(row);
                dt.AcceptChanges();
                shouldSelected = false;
            }

            rdGrid.DataSource = dt;
            rdGrid.DataBind();
            chkSelected.Checked = shouldSelected;
            Session[sessionKeyID] = dt;
        }

        //When the Wizard is finished, this method gets executed.
        private bool OnWizardFinish(bool shouldSaveCurrentProjection)
        {
            try
            {
                AddProjectionRun(rcProjectionRuns.Items.Count + 1);

                Person personInformation = null;
                OS.Milliman.Actions.Holding[] assetsInformation = null;
                OS.Milliman.Actions.Goal[] goalsInformation = null;
                OS.Milliman.Actions.Action[] actionsInformation = null;
                long investmentStrategyInformation = -1;
                OS.Milliman.Actions.InvestmentStrategyProducts[] products = null;

                personInformation = SetPerson();
                assetsInformation = SetHoldingAll();
                goalsInformation = SetGoals();
                actionsInformation = SetActions();
                investmentStrategyInformation = SetInvestmentStrategy();

                if (personInformation != null && assetsInformation != null && goalsInformation != null &&
                    actionsInformation != null && investmentStrategyInformation >= 0)
                {
                    Projection(personInformation, assetsInformation, goalsInformation, actionsInformation,
                               investmentStrategyInformation, products);

                    divGraphInputData.InnerHtml = graphInputData;
                    divGraphOutputData.InnerHtml = graphOutputData;

                    //Save the current Projection temporarily
                    if (shouldSaveCurrentProjection)
                    {
                        SaveCurrentProjectionInCache(projectionRunSessionID, SaveAdvice(false));
                        SaveCurrentProjectionInCache(projectionsSessionID, Session[projectionsSessionID]);
                        SaveCurrentProjectionInCache(goalsSessionID, Session[goalsSessionID]);
                        SaveCurrentProjectionInCache(graphVisibilitySessionID, true);
                        if (Session[multimanResultSetSessionID] != null)
                            SaveCurrentProjectionInCache(multimanResultSetSessionID, Session[multimanResultSetSessionID]);
                        SaveCurrentProjectionInCache(graphInputDataSessionID, divGraphInputData.InnerHtml);
                        SaveCurrentProjectionInCache(graphOutputDataSessionID, divGraphOutputData.InnerHtml);
                    }

                    graphInputData = string.Empty;
                    graphOutputData = string.Empty;
                }
            }
            catch (Exception ex)
            {
                isValid = false;
                throw ex;
            }
            return isValid;
        }

        //Graph Input and Output data related methods.----------------------------------------------
        private void PrepareGraphOutputData(string section, object data)
        {
            if (string.IsNullOrEmpty(graphOutputData))
                graphOutputData = "<div class='graphDataSection'>" + section + "</div>";
            else
                graphOutputData = graphOutputData + "<div class='graphDataSection'>" + section + "</div>";

            graphOutputData = graphOutputData + "<div>" + WebUtilities.Utilities.ConvertToXML(data).Replace("<", "&lt").Replace(">", "&gt") + "</div>";
        }
        private void PrepareGraphInputData(string section, object data)
        {
            if (section == "Personal")
            {
                if (string.IsNullOrEmpty(graphInputData))
                    graphInputData = "<div class='graphDataSection'>Person Details</div>";
                else
                    graphInputData = graphInputData + "<div class='graphDataSection'>Personal Details</div>";
            }
            else if (section == "Assets")
            {
                if (string.IsNullOrEmpty(graphInputData))
                    graphInputData = "<div class='graphDataSection'>Assets</div>";
                else
                    graphInputData = graphInputData + "<div class='graphDataSection'>Assets</div>";
            }
            else if (section == "Actions")
            {
                if (string.IsNullOrEmpty(graphInputData))
                    graphInputData = "<div class='graphDataSection'>Actions</div>";
                else
                    graphInputData = graphInputData + "<div class='graphDataSection'>Actions</div>";
            }
            else if (section == "Goals")
            {
                if (string.IsNullOrEmpty(graphInputData))
                    graphInputData = "<div class='graphDataSection'>Goals</div>";
                else
                    graphInputData = graphInputData + "<div class='graphDataSection'>Goals</div>";
            }
            else if (section == "Investment")
            {
                if (string.IsNullOrEmpty(graphInputData))
                    graphInputData = "<div class='graphDataSection'>Investment Strategies</div>";
                else
                    graphInputData = graphInputData + "<div class='graphDataSection'>Investment Strategies</div>";
            }

            graphInputData = graphInputData + "<div>" + WebUtilities.Utilities.ConvertToXML(data).Replace("<", "&lt").Replace(">", "&gt") + "</div>";
        }

        //------------------------------------------------------------------------------------------

        //Page Startup methods
        private void InitializePageVariables()
        {
            _cid = Guid.Parse(Request.Params["ins"].ToString().Replace("?ins=", string.Empty));
            hfAdviceID.Value = Request.Params["id"];

            //Added due to the feedback ------------------------------------------------------------------------------
            lClientCode = _cid.ToString();
            lEclipseId = _cid.ToString();
            //---------------------------------------------------------------------------------------------------------

            goalsWealthSessionID = goalsWealthSessionID + "_" +
                                   (string.IsNullOrEmpty(Request.Params["isedit"]) || Request.Params["isedit"] == "0"
                                        ? "0"
                                        : Request.Params["id"]);
            goalsIncomeSessionID = goalsIncomeSessionID + "_" +
                                   (string.IsNullOrEmpty(Request.Params["isedit"]) || Request.Params["isedit"] == "0"
                                        ? "0"
                                        : Request.Params["id"]);
            goalsLumpSumSessionID = goalsLumpSumSessionID + "_" +
                                    (string.IsNullOrEmpty(Request.Params["isedit"]) || Request.Params["isedit"] == "0"
                                         ? "0"
                                         : Request.Params["id"]);
            goalsLiquidAssetsSessionID = goalsLiquidAssetsSessionID + "_" +
                                         (string.IsNullOrEmpty(Request.Params["isedit"]) ||
                                          Request.Params["isedit"] == "0"
                                              ? "0"
                                              : Request.Params["id"]);
            goalsBequestSessionID = goalsBequestSessionID + "_" +
                                    (string.IsNullOrEmpty(Request.Params["isedit"]) || Request.Params["isedit"] == "0"
                                         ? "0"
                                         : Request.Params["id"]);

            hfGoalsWealthSessionKey.Value = goalsWealthSessionID;
            hfGoalsIncomeSessionKey.Value = goalsIncomeSessionID;
            hfGoalsLumpSumSessionKey.Value = goalsLumpSumSessionID;
            hfGoalsLiquidAssetsSessionKey.Value = goalsLiquidAssetsSessionID;
            hfGoalsBequestSessionKey.Value = goalsBequestSessionID;

            projectionsSessionID = projectionsSessionID + "_" + (string.IsNullOrEmpty(Request.Params["isedit"]) || Request.Params["isedit"] == "0" ? "0" : Request.Params["id"]);
            goalsSessionID = goalsSessionID + "_" + (string.IsNullOrEmpty(Request.Params["isedit"]) || Request.Params["isedit"] == "0" ? "0" : Request.Params["id"]);
            graphVisibilitySessionID = graphVisibilitySessionID + "_" + (string.IsNullOrEmpty(Request.Params["isedit"]) || Request.Params["isedit"] == "0" ? "0" : Request.Params["id"]);
            //multimanResultSetSessionID = multimanResultSetSessionID + "_" + (string.IsNullOrEmpty(Request.Params["isedit"]) || Request.Params["isedit"] == "0" ? "0" : Request.Params["id"]);
            advicesListingURLSessionID = advicesListingURLSessionID + "_" + (string.IsNullOrEmpty(Request.Params["isedit"]) || Request.Params["isedit"] == "0" ? "0" : Request.Params["id"]);
            graphInputDataSessionID = graphInputDataSessionID + "_" + (string.IsNullOrEmpty(Request.Params["isedit"]) || Request.Params["isedit"] == "0" ? "0" : Request.Params["id"]);
            graphOutputDataSessionID = graphOutputDataSessionID + "_" + (string.IsNullOrEmpty(Request.Params["isedit"]) || Request.Params["isedit"] == "0" ? "0" : Request.Params["id"]);
            projectionRunSessionID = projectionRunSessionID + "_" + (string.IsNullOrEmpty(Request.Params["isedit"]) || Request.Params["isedit"] == "0" ? "0" : Request.Params["id"]);

            lblProjectionName.Text = string.IsNullOrEmpty(Request.Params["projectionName"]) ? "" : Request.Params["projectionName"].ToString();

            OBPGraph1.ProjectionRunsForGraphCacheID = ProjectionRunsForGraphCacheID;
            OBPGraph2.ProjectionRunsForGraphCacheID = ProjectionRunsForGraphCacheID;
            OBPGraph3.ProjectionRunsForGraphCacheID = ProjectionRunsForGraphCacheID;
            OBPGraph4.ProjectionRunsForGraphCacheID = ProjectionRunsForGraphCacheID;

            OBPGraph1.GraphVisibilitySessionID = graphVisibilitySessionID;
            OBPGraph2.GraphVisibilitySessionID = graphVisibilitySessionID;
            OBPGraph3.GraphVisibilitySessionID = graphVisibilitySessionID;
            OBPGraph4.GraphVisibilitySessionID = graphVisibilitySessionID;

            OBPGraph1.ProjectionsSessionID = projectionsSessionID;
            OBPGraph2.ProjectionsSessionID = projectionsSessionID;
            OBPGraph3.ProjectionsSessionID = projectionsSessionID;
            OBPGraph4.ProjectionsSessionID = projectionsSessionID;

            OBPGraph1.GoalsSessionID = goalsSessionID;
            OBPGraph2.GoalsSessionID = goalsSessionID;
            OBPGraph3.GoalsSessionID = goalsSessionID;
            OBPGraph4.GoalsSessionID = goalsSessionID;

            if (Request.Params["__EVENTTARGET"] != null)
            {
                if (Request.Params["__EVENTTARGET"].Replace("$", "_") == rcProjectionRuns.ClientID)
                {
                    OBPGraph1.CurrentProjectionRun = rcProjectionRuns.SelectedValue;
                    OBPGraph2.CurrentProjectionRun = rcProjectionRuns.SelectedValue;
                    OBPGraph3.CurrentProjectionRun = rcProjectionRuns.SelectedValue;
                    OBPGraph4.CurrentProjectionRun = rcProjectionRuns.SelectedValue;

                    PopulateProjectionDataFromCache();
                    return;
                }
            }

            if (hfIsFinished.Value == "1")
            {
                OBPGraph1.CurrentProjectionRun = Convert.ToString(rcProjectionRuns.Items.Count + 1);
                OBPGraph2.CurrentProjectionRun = Convert.ToString(rcProjectionRuns.Items.Count + 1);
                OBPGraph3.CurrentProjectionRun = Convert.ToString(rcProjectionRuns.Items.Count + 1);
                OBPGraph4.CurrentProjectionRun = Convert.ToString(rcProjectionRuns.Items.Count + 1);

                OBPGraph1.IsNewProjectionRun = true;
                OBPGraph2.IsNewProjectionRun = true;
                OBPGraph3.IsNewProjectionRun = true;
                OBPGraph4.IsNewProjectionRun = true;
            }
            else
            {
                OBPGraph1.CurrentProjectionRun = rcProjectionRuns.SelectedValue;
                OBPGraph2.CurrentProjectionRun = rcProjectionRuns.SelectedValue;
                OBPGraph3.CurrentProjectionRun = rcProjectionRuns.SelectedValue;
                OBPGraph4.CurrentProjectionRun = rcProjectionRuns.SelectedValue;

                OBPGraph1.IsNewProjectionRun = false;
                OBPGraph2.IsNewProjectionRun = false;
                OBPGraph3.IsNewProjectionRun = false;
                OBPGraph4.IsNewProjectionRun = false;
            }
        }

        private void ActionsOnPageLoad()
        {
            if (!IsPostBack)
            {
                DateTime currentDate = DateTime.Now;
                txtPDDOB.MinDate = new DateTime(currentDate.Year - 125, currentDate.Month, currentDate.Day, 12, 0, 0);
                txtPDDOB.MaxDate = currentDate;

                PopulateInvestmentStrategies();

                //Disable Control Assets
                ScriptManager.RegisterStartupScript(this, typeof(string), "Error", "toggleStatusDisableControl();",
                                                    true);

                SetControlsDefaultValues(GetIndividualDS());

                if (!string.IsNullOrEmpty(Request.Params["isedit"]))
                {
                    try
                    {
                        Session[goalsWealthSessionID] = null;
                        Session[goalsIncomeSessionID] = null;
                        Session[goalsLumpSumSessionID] = null;
                        Session[goalsLiquidAssetsSessionID] = null;
                        Session[goalsBequestSessionID] = null;
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }

                    Guid adviceID = new Guid(Request.Params["id"]);

                    if (Session[multimanResultSetSessionID] != null)
                    {
                        Session[graphVisibilitySessionID] = ((Hashtable)Session[multimanResultSetSessionID])[adviceID] != null;
                        Session[projectionsSessionID] = (SortedList<string, DataSet>)((Hashtable)Session[multimanResultSetSessionID])[adviceID];
                    }

                    OnEditAdvice();

                    if (txtPDDOB.SelectedDate != null)
                    {
                        GraphControl.CurrentAge = (byte)((DateTime.Today - txtPDDOB.SelectedDate.Value).TotalDays / 365);
                    }
                    IsEdit.Value = "1";
                    hfIsGraphWizardDisabled.Value = "0";

                    //Multi-Projection
                    if (Session[multimanResultSetSessionID] != null)
                    {
                        if (((Hashtable)Session[multimanResultSetSessionID])[adviceID] != null)
                        {
                            AddProjectionRun(-1);
                            SaveCurrentProjectionInCache(projectionRunSessionID, SaveAdvice(false));
                            SaveCurrentProjectionInCache(projectionsSessionID, Session[projectionsSessionID]);
                            SaveCurrentProjectionInCache(goalsSessionID, Session[goalsSessionID]);
                            SaveCurrentProjectionInCache(graphVisibilitySessionID, Session[graphVisibilitySessionID]);
                            SaveCurrentProjectionInCache(multimanResultSetSessionID, Session[multimanResultSetSessionID]);
                            SaveCurrentProjectionInCache(graphInputDataSessionID, divGraphInputData.InnerHtml);
                            SaveCurrentProjectionInCache(graphOutputDataSessionID, divGraphOutputData.InnerHtml);
                        }
                    }
                    Page.ClientScript.RegisterStartupScript(GetType(), "ClearStorage", "ClearStorage();", true);
                }
                else
                {
                    IsEdit.Value = "0";
                    hfIsGraphWizardDisabled.Value = "1";
                    PopulateGoals(new AdvicesDS().GoalsTable);
                    Session[graphVisibilitySessionID] = false;
                }

                Session[advicesListingURLSessionID] = string.Format("AdviceListing.aspx?ins={0}",
                                                                    Request.Params["ins"].ToString());
                InitializeTextBoxes();

                ClearGraphsData();
            }
            else
            {
                if (!string.IsNullOrEmpty(hfIsFinished.Value))
                {
                    if (!OnWizardFinish(true))
                    {
                        Session[graphVisibilitySessionID] = false;
                        RegisterErrorMessageScript(errorMsg, false, null);
                    }
                    else
                    {
                        Session[graphVisibilitySessionID] = true;
                        if (txtPDDOB.SelectedDate != null)
                            GraphControl.CurrentAge =
                                (byte)((DateTime.Today - txtPDDOB.SelectedDate.Value).TotalDays / 365);

                        hfIsGraphWizardDisabled.Value = "0";
                        IsProjectionRun = true;
                        btnClearAllRuns.Visible = (rcProjectionRuns.Items.Count > 0);
                    }
                }
                else
                {
                    Session[projectionsSessionID] = null;
                }
            }

            if (!IsProjectionRun)
            {
                if (txtPDDOB.SelectedDate != null)
                {
                    txtNumTimeSteps.MinValue = Convert.ToInt32(Math.Floor(Convert.ToDouble((DateTime.Today - txtPDDOB.SelectedDate.Value).TotalDays / 365)));
                    txtNumTimeSteps.Value = txtNumTimeSteps.MinValue;

                    txtNumTimeStepsCompareRuns.MinValue = Convert.ToInt32(Math.Floor(Convert.ToDouble((DateTime.Today - txtPDDOB.SelectedDate.Value).TotalDays / 365)));
                    txtNumTimeStepsCompareRuns.Value = txtNumTimeStepsCompareRuns.MinValue;
                }
            }
            if (txtPDPlanAge.Value.HasValue)
            {
                txtNumTimeSteps.MaxValue = Convert.ToInt32(txtPDPlanAge.Value);
                txtNumTimeStepsCompareRuns.MaxValue = Convert.ToInt32(txtPDPlanAge.Value);
            }

            rcProjectionRuns.Visible = false;
            if (rcProjectionRuns.Items.Count > 0)
            {
                rcProjectionRuns.Visible = true;
            }
        }

        private void ClearGraphsData()
        {
            HttpContext.Current.Session["Asset"] = null;
            HttpContext.Current.Session["Income"] = null;
            HttpContext.Current.Session["LumpSum"] = null;
            HttpContext.Current.Session["Goals"] = null;
            HttpContext.Current.Session["Probability"] = null;
        }

        private void RemoveProjectionDataFromCache(string key)
        {
            Cache.Remove(key);
        }

        private void RemoveAllProjectionDataFromCache()
        {
            if (rcProjectionRuns.Items.Count == 0) return;

            var cacheKey = string.Empty;
            for (var r = 1; r <= rcProjectionRuns.Items.Count; r++)
            {
                cacheKey = Session.SessionID + "_" + r + "_" + projectionRunSessionID;
                RemoveProjectionDataFromCache(cacheKey);
                cacheKey = Session.SessionID + "_" + r + "_" + projectionsSessionID;
                RemoveProjectionDataFromCache(cacheKey);
                cacheKey = Session.SessionID + "_" + r + "_" + goalsSessionID;
                RemoveProjectionDataFromCache(cacheKey);
                cacheKey = Session.SessionID + "_" + r + "_" + graphVisibilitySessionID;
                RemoveProjectionDataFromCache(cacheKey);
                cacheKey = Session.SessionID + "_" + r + "_" + multimanResultSetSessionID;
                RemoveProjectionDataFromCache(cacheKey);
                cacheKey = Session.SessionID + "_" + r + "_" + graphInputDataSessionID;
                RemoveProjectionDataFromCache(cacheKey);
                cacheKey = Session.SessionID + "_" + r + "_" + graphOutputDataSessionID;
                RemoveProjectionDataFromCache(cacheKey);
            }

            RemoveProjectionDataFromCache(ProjectionRunsForGraphCacheID);
        }

        private void SaveCurrentProjectionInCache(string key, object data)
        {
            if (!string.IsNullOrEmpty(rcProjectionRuns.SelectedValue))
            {
                string cacheKey = Session.SessionID + "_" + rcProjectionRuns.SelectedValue + "_" + key;
                RemoveProjectionDataFromCache(cacheKey);
                Cache[cacheKey] = data;
            }
        }

        private void PopulateProjectionDataFromCache()
        {
            PopulateAdvice(GetProjectionFromCache(projectionRunSessionID) as AdvicesDS);
            Session[projectionsSessionID] = GetProjectionFromCache(projectionsSessionID);
            Session[goalsSessionID] = GetProjectionFromCache(goalsSessionID);
            Session[graphVisibilitySessionID] = GetProjectionFromCache(graphVisibilitySessionID);
            Session[multimanResultSetSessionID] = GetProjectionFromCache(multimanResultSetSessionID);
            divGraphInputData.InnerHtml = GetProjectionFromCache(graphInputDataSessionID) as string;
            divGraphOutputData.InnerHtml = GetProjectionFromCache(graphOutputDataSessionID) as string;
        }

        private object GetProjectionFromCache(string key)
        {
            if (!string.IsNullOrEmpty(rcProjectionRuns.SelectedValue))
            {
                string cacheKey = Session.SessionID + "_" + rcProjectionRuns.SelectedValue + "_" + key;
                return Cache[cacheKey];
            }
            else
                return null;
        }

        //Error and Logging Method
        private void RegisterErrorMessageScript(string errorMessage, bool shouldLogException, Exception exp)
        {
            if (shouldLogException)
                WebUtilities.Utilities.LogException(exp, Request, Context);

            hfErrorMsg.Value = errorMessage;
            Page.ClientScript.RegisterStartupScript(GetType(), "Call my function", "ShowError();", true);
        }

        private void HttpCompress(HttpApplication app)
        {
            try
            {
                string accept = app.Request.Headers["Accept-Encoding"];
                if (!string.IsNullOrEmpty(accept))
                {
                    if (CompressScript(Request.ServerVariables["SCRIPT_NAME"]))
                    {
                        System.IO.Stream stream = app.Response.Filter;
                        accept = accept.ToLower();
                        if (accept.Contains("gzip"))
                        {
                            app.Response.Filter = new System.IO.Compression.GZipStream(stream, System.IO.Compression.CompressionMode.Compress);
                            app.Response.AppendHeader("Content-Encoding", "gzip");
                        }
                        else if (accept.Contains("deflate"))
                        {
                            app.Response.Filter = new System.IO.Compression.DeflateStream(stream, System.IO.Compression.CompressionMode.Compress);
                            app.Response.AppendHeader("Content-Encoding", "deflate");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
throw new Exception(ex.Message);             }
        }
        private bool CompressScript(string scriptName)
        {
            if (scriptName.ToLower().Contains(".aspx")) return true;
            if (scriptName.ToLower().Contains(".axd")) return false;
            if (scriptName.ToLower().Contains(".js")) return true;
            return true;
        }
        #endregion

        #region Web Method - Called when the graph gets rendered

        [WebMethod(EnableSession = false)]
        public static string GetProjectionAsJSON(string GraphType)
        {
            string toReturn = HttpContext.Current.Session[GraphType].ToString();
            return toReturn;
        }

        [WebMethod(EnableSession = false)]
        public static string GetAllGoalsProjectionAsJSON(string adviceID)
        {
            var projectionRunsForGraph = "projectionRunsForGraph_" + HttpContext.Current.Session.SessionID + "_" + adviceID;
            if (HttpContext.Current.Cache[projectionRunsForGraph] == null)
                return "Error";

            var htProjectionRunsGraphData = HttpContext.Current.Cache[projectionRunsForGraph] as SortedDictionary<string, string>;
            if (htProjectionRunsGraphData == null || htProjectionRunsGraphData.Count == 0)
                return "Error";

            IDictionaryEnumerator projections = htProjectionRunsGraphData.GetEnumerator();
            string projectionsRunsJSON = "{\"projectionsRuns\":[";
            while (projections.MoveNext())
            {
                projectionsRunsJSON += projections.Value + ",";
            }

            if (!string.IsNullOrEmpty(projectionsRunsJSON))
            {
                projectionsRunsJSON = projectionsRunsJSON.Substring(0, projectionsRunsJSON.Length - 1);
                projectionsRunsJSON += "]}";
            }

            return projectionsRunsJSON;
        }

        [WebMethod(EnableSession = true)]
        //public static string AddGridRow(object gridRowInformation)
        public static string AddGridRow(string key, string amountColumn, string ageColumn, decimal amount, int age)
        {
            DataTable dt = System.Web.HttpContext.Current.Session[key] as DataTable;
            if (dt != null)
            {
                DataRow dr = dt.NewRow();
                dr[amountColumn] = amount;
                dr[ageColumn] = age;
                dr["RowStatus"] = "N";
                dr["GoalId"] = Guid.NewGuid().ToString();
                dt.Rows.Add(dr);
                dt.AcceptChanges();
                dt = RemoveGoalsGridDummyRow(dt);
                System.Web.HttpContext.Current.Session[key] = dt;
                return GetJson(dt);
            }
            return "error";
        }

        [WebMethod(EnableSession = true)]
        //public static string AddGridRow(object gridRowInformation)
        public static string UpdateGridRow(string key, string guid, string amountColumn, string ageColumn, decimal amount, int age)
        {
            DataTable dt = System.Web.HttpContext.Current.Session[key] as DataTable;
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows.Find(guid);
                dr[amountColumn] = amount;
                dr[ageColumn] = age;
                dt.AcceptChanges();
                dt = RemoveGoalsGridDummyRow(dt);
                System.Web.HttpContext.Current.Session[key] = dt;
                return GetJson(dt);
            }
            return "error";
        }

        [WebMethod(EnableSession = true)]
        public static string RemoveGridRow(string key, string guid)
        {
            DataTable dt = System.Web.HttpContext.Current.Session[key] as DataTable;
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows.Find(guid);
                dt.Rows.Remove(row);
                dt.AcceptChanges();
                dt = RemoveGoalsGridDummyRow(dt);
                System.Web.HttpContext.Current.Session[key] = dt;
                return GetJson(dt);
            }

            return "error";
        }

        private static string GetJson(DataTable dt)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        #endregion
    }
}