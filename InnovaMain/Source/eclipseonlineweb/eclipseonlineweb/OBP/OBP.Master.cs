﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.Utilities;
using System.Threading;


namespace eclipseonlineweb
{
    public partial class OBPMaster : System.Web.UI.MasterPage
    {
        public string cid = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            cid = Request.Params["ins"];

            ((HiddenField)Master.FindControl("hfMainMenuText")).Value = "OBP";

            UMABroker = new CMBroker(Context.User, DBConnection.Connection.ConnectionString);
            UMABroker.SetStart();

            var objUser = (DBUser)UMABroker.GetBMCInstance(Page.User.Identity.Name, "DBUser_1_1");
            var dbUserDetailsDS = new DBUserDetailsDS();
            objUser.GetData(dbUserDetailsDS);

            if (Page.User.Identity.Name.ToLower() != "administrator")
            {
                if (objUser.UserType == UserType.Advisor)
                {
                }
            }

        }

        public ICMBroker UMABroker
        {
            get
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (brokerSlot == null)
                    return null;

                return (ICMBroker)Thread.GetData(brokerSlot);
            }
            set
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (brokerSlot == null)
                    brokerSlot = Thread.AllocateNamedDataSlot("Broker");

                if (value == null)
                    Thread.FreeNamedDataSlot("Broker");

                Thread.SetData(brokerSlot, value);
            }
        }
    }
}