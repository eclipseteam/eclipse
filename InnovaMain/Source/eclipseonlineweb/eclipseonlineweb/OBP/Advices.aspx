﻿<%@ Page Title="e-Clipse Online Portal - OBP" Language="C#" MasterPageFile="OBP.Master"
    AutoEventWireup="true" CodeBehind="Advices.aspx.cs" Inherits="eclipseonlineweb.OBP.Advices" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="obp" TagName="OBPGraph" Src="~/OBP/Controls/GraphControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="IndividualControl" Src="~/Controls/Individual.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <link rel="stylesheet" href="../Styles/OBP/style.css" />
    <link rel="stylesheet" href="../Styles/OBP/smart_wizard.css" />
    <link type="text/css" href="../Styles/OBP/kendo.common.min.css" />
    <link type="text/css" href="../Styles/OBP/kendo.default.min.css" />
    <style type="text/css">
        .holder
        {
            width: 100%;
            display: block;
            z-index: 6;
        }
        .content
        {
            background: #fff;
            z-index: 7; /*  padding: 28px 26px 33px 25px;*/
        }
        .popup
        {
            border-radius: 7px;
            background: #6b6a63;
            margin: 30px auto 0;
            padding: 6px;
            position: fixed;
            width: 900px;
            top: 10%;
            left: 50%;
            margin-left: -400px;
            margin-top: -40px;
            z-index: 9999;
        }
        .popup1
        {
            border-radius: 7px;
            background: #F9F9F9;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 400px;
            top: 20%;
            left: 50%;
            margin-left: -10%;
            margin-top: 2%;
            z-index: 9999;
        }
        .overlay
        {
            width: 100%;
            opacity: 0.65;
            height: 100%;
            left: 0; /*IE*/
            top: 10px;
            text-align: center;
            z-index: 5;
            position: fixed;
            background-color: #444444;
        }
        
        .tblProjectionsComparison
        {
            border-collapse: collapse;
            width: 100%;
            font: 11px Verdana,Arial,Helvetica,sans-serif;
        }
        
        .tblProjectionsComparison, .tblProjectionsComparison th, .tblProjectionsComparison td
        {
            border: 1px solid black;
            text-align: center;
        }
        
        .tblProjectionsComparison tr:nth-child(1) th
        {
            font-weight: bold;
            background: lightgray;
        }
        
        
        .tblInvestmentStrategies
        {
            border-collapse: collapse;
            width: 100%;
        }
        
        .tblInvestmentStrategies, .tblInvestmentStrategies th, .tblInvestmentStrategies td
        {
            border: 1px solid lightgray;
        }
        
        .tblInvestmentStrategies tr:nth-child(1) td
        {
            font-weight: bold;
        }
        .tblInvestmentStrategies tr td:nth-child(1)
        {
            font-weight: bold;
        }
        
        .tblAssets
        {
            border-collapse: collapse;
        }
        
        .tblAssets, .tblAssets th, .tblAssets td
        {
            border: 1px solid lightgray;
        }
        
        #divWealth table, #divIncome table, #divLiquidAssets table, #divLumpSum table, #divBequest table
        {
            border-collapse: collapse;
        }
        
        #dtActionTable
        {
            border-collapse: collapse;
        }
        
        #dtActionTable th, #dtActionTable td
        {
            border: 1px solid lightgray;
        }
        
        #divWealth .rgMaster.rgMasterTable, #divWealth .rgMasterTable th, #divWealth .rgMasterTable td, #divIncome .rgMasterTable, #divIncome .rgMasterTable th, #divIncome .rgMasterTable td, #divLiquidAssets .rgMasterTable, #divLiquidAssets .rgMasterTable th, #divLiquidAssets .rgMasterTable td, #divLumpSum .rgMasterTable, #divLumpSum .rgMasterTable th, #divLumpSum .rgMasterTable td, #divBequest .rgMasterTable, #divBequest .rgMasterTable th, #divBequest .rgMasterTable td
        {
            border: 1px solid lightgray;
        }
        
        #accordion
        {
            position: relative;
            width: 120%;
            left: -200px;
            height: 630px;
            padding-left: 10px;
            width: 100%;
            width: 117.6%;
        }
        
        #accordion #profile
        {
            width: 48.5%;
            float: left;
        }
        #accordion #results
        {
            width: 51%;
            float: right;
            position: relative;
            left: -10px;
        }
        
        #accordion #profile #panels, #accordion #results #tabs
        {
            top: 15px;
            height: 545px;
        }
        
        .gridItemRow
        {
            height: 8px;
        }
        
        .divViewInputGraphData, .divViewOutputGraphData
        {
            z-index: 100000;
            background: lightgray;
        }
        
        .graphDataSection
        {
            background: white;
            color: black;
            font-weight: bold;
            border: 1px solid gray;
        }
        
        .graphDataSection:hover
        {
            background: #908FA8;
        }
        
        #divCompareRunsAge
        {
            text-align: center;
            color: #7B7B7B;
            font-family: Arial,Helvetica,sans-serif;
            font-size: 12px;
            line-height: 18px;
        }
        
        
        body
        {
            min-width: 1160px;
        }
        
        
        #Sidebar
        {
            display: block;
            float: left;
            height: auto;
            margin: 0;
            padding: 0;
            width: 15%;
        }
        
        #Content
        {
            display: block;
            float: left;
            height: auto;
            margin: 20px 0 0 5px;
            width: 99.5%;
        }
        
        
        #Content-Right
        {
            display: block;
            float: left;
            height: auto;
            padding: 0;
            width: 100%;
            margin: 0px !important;
        }
        
        #accordion
        {
            height: 630px;
            padding-left: 10px;
            position: relative;
            width: 98.5%;
            left: 0px !important;
        }
        
        #Footer
        {
            background: none repeat scroll 0 0 #FFFFFF;
            border: 1px solid #C4C4C4;
            box-shadow: 0 0 3px 0 #CDCDCD;
            clear: both;
            color: #7B7B7B;
            float: left;
            font-family: Arial,Helvetica,sans-serif;
            font-size: 12px;
            margin: 20px 0;
            padding: 10px 0;
            text-align: center;
            width: 99%;
        }
        
        .box
        {
            display: block;
            float: left;
            margin: 0 auto;
            padding: 0 10px;
            position: relative;
            width: 98%;
        }
        
        #userNav
        {
            position: absolute;
            right: 15px;
            top: 32px;
        }
    </style>
    <!-- start New Tab Use -->
    <link href="../Styles/OBP/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/OBP/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../Scripts/OBP/jquery-ui.js" type="text/javascript"></script>
    <!-- End New Tab -->
    <script src="../Scripts/OBP/kendo.all.min.js" type="text/javascript"></script>
    <script src="Controls/GraphsUtils.js" type="text/javascript"></script>
    <script src="Controls/AssetGraph.js" type="text/javascript"></script>
    <script src="Controls/IncomeGraph.js" type="text/javascript"></script>
    <script src="Controls/LumpsumGraph.js" type="text/javascript"></script>
    <script src="Controls/ProbabilityGraph.js" type="text/javascript"></script>
    <script src="Controls/GoalsGraph.js" type="text/javascript"></script>
    <script type="text/javascript">
        function ShowError() {
            var errorMsgObj = GetElement("<%: hfErrorMsg.ClientID %>").value;
            alert(errorMsgObj);
        }
    </script>
    <div id="divInputGraphData" runat="server" class="divViewInputGraphData popup" style="display: none;">
        <div class="divViewInputGraphDataClose" style="height: 25px; margin-bottom: 5px;
            background: #3E3B6A; color: white">
            <span style="display: inline-block; width: 96%; text-align: left; font-size: 1.3em">
                Graph Input Data</span>
            <img style="display: inline-block; text-align: right; position: relative; top: 4px;
                right: -10px" src="../images/delete.png" class="imgViewInputGraphDataClose" />
        </div>
        <div>
            <div id="divGraphInputData" runat="server" style="max-height: 500px; overflow: auto">
            </div>
        </div>
    </div>
    <div id="divOutputGraphData" runat="server" class="divViewOutputGraphData popup"
        style="display: none;">
        <div class="divViewInputGraphDataClose" style="height: 25px; margin-bottom: 5px;
            background: #3E3B6A; color: white">
            <span style="display: inline-block; width: 96%; text-align: left; font-size: 1.3em">
                Graph Output Data</span>
            <img style="display: inline-block; text-align: right; position: relative; top: 4px;
                right: -10px" src="../images/delete.png" class="imgViewInputGraphDataClose" />
        </div>
        <div>
            <div id="divGraphOutputData" runat="server" style="max-height: 500px; overflow: auto">
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlCommands" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <telerik:RadButton runat="server" ID="btnSave" ToolTip="Save" Height="20px" OnClientClicked="SaveAdvice"
                                        AutoPostBack="false">
                                        <ContentTemplate>
                                            <img src="../images/OBP/Save.png" alt="" class="btnImageWithText" style="height: 15px;
                                                width: 15px; margin-right: 4px" />
                                            <span class="riLabel">Save</span>
                                        </ContentTemplate>
                                    </telerik:RadButton>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblProjectionName"></asp:Label>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <telerik:RadButton runat="server" ID="btnSaveAs" ToolTip="Save selected Run as a Projection..."
                                        Width="30px" Height="30px" OnClientClicked="SaveAdvice" AutoPostBack="false"
                                        Style="display: none;">
                                        <ContentTemplate>
                                            <img src="../images/OBP/Saveas.png" alt="" style="vertical-align: middle; height: 15px;
                                                margin-right: 4px" />
                                            <span>Save As</span>
                                        </ContentTemplate>
                                    </telerik:RadButton>
                                </td>
                                <td>
                                    <telerik:RadButton ID="btnClearAllRuns" runat="server" Text="Clear All Runs" Height="15px" Visible="false"
                                    OnClientClicked="ClearAllRuns_Click" OnClick="btnClearAllRuns_Click"></telerik:RadButton>
                                </td>
                                <td>
                                    <telerik:RadButton runat="server" ID="btnIndividual" ToolTip="Select Individual"
                                        Style="display: none;" Height="25px">
                                        <ContentTemplate>
                                            <img src="../images/OBP/Client.jpg" alt="" class="btnImageWithText" />
                                            <span class="riLabel">Select Individual</span>
                                        </ContentTemplate>
                                    </telerik:RadButton>
                                </td>
                                <td>
                                    <telerik:RadButton ID="RadButton3" runat="server" Text="Run" Visible="false" OnClick="OnFinish"
                                        Height="25px">
                                    </telerik:RadButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 30%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb2" runat="server" />
                        <br />
                        <uc2:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <div id="accordion">
        <div id="profile" style="height: 600px; position: relative; left: -6px; z-index: 1; display:none;">
            <h3 style="position: relative">
                <span>Wizard</span>
                <telerik:RadComboBox ID="rcProjectionRuns" runat="server" Width="100px" AutoPostBack="True"
                    Style="position: absolute; right: 2px; top: 1px" OnSelectedIndexChanged="rcProjectionRuns_OnSelectedIndexChanged"
                    CssClass="rcProjectionRuns">
                </telerik:RadComboBox>
            </h3>
            <div id="panels" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <!-- Smart Wizard class="swMain"-->
                            <div id="wizard" class="swMain">
                                <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                                    <li><a href="#step-1" onclick="ShowActionBar(this);">
                                        <label class="stepNumber">
                                            1</label>
                                        <span class="stepDesc">Profile</span> </a></li>
                                    <li><a href="#step-2" onclick="ShowActionBar(this);">
                                        <label class="stepNumber">
                                            2</label>
                                        <span class="stepDesc">Assets</span> </a></li>
                                    <li><a href="#step-3" onclick="ShowActionBar(this);">
                                        <label class="stepNumber">
                                            3</label>
                                        <span class="stepDesc">Actions</span> </a></li>
                                    <li><a href="#step-4" onclick="ShowActionBar(this);">
                                        <label class="stepNumber">
                                            4</label>
                                        <span class="stepDesc">Goals</span> </a></li>
                                    <li><a href="#step-5" onclick="ShowActionBar(this);">
                                        <label class="stepNumber">
                                            5</label>
                                        <span class="stepDesc">Investment<br />
                                            <small>Strategies</small> </span></a></li>
                                </ul>
                                <div id="step-1">
                                    <h2 class="StepTitle">
                                        <span style="text-align: left; display: inline-block; width: 24%">Personal Details</span>
                                        <span style="text-align: right; display: inline-block; width: 75%; font-weight: normal">
                                            <telerik:RadButton runat="server" ID="btnEditIndividual" ToolTip="Edit Personal Details"
                                                Width="140px" Height="20px" AutoPostBack="False">
                                                <ContentTemplate>
                                                    <img src="../images/OBP/edit-profile-icon.png" alt="" style="vertical-align: middle;
                                                        height: 15px; margin-right: 4px" />
                                                    <span>Edit Personal Details</span>
                                                </ContentTemplate>
                                            </telerik:RadButton>
                                        </span>
                                    </h2>
                                    <p>
                                        <table>
                                            <tr>
                                                <td>
                                                    Name
                                                </td>
                                                <td>
                                                    <telerik:RadTextBox ID="txtPDName" ReadOnly="true" runat="server" Width="250px" Enabled="false">
                                                    </telerik:RadTextBox>
                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td>
                                                    Advise Title<span style="color: Red">*</span>
                                                </td>
                                                <td>
                                                    <telerik:RadTextBox ID="txtPDTitle" Text="" runat="server" Width="300px" TabIndex="1">
                                                    </telerik:RadTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Date of Birth<span style="color: Red">*</span>
                                                </td>
                                                <td>
                                                    <telerik:RadDatePicker ID="txtPDDOB" runat="server" Width="110px" MinDate="1/1/1900 12:00:00 AM"
                                                        FocusedDate="1/1/1970 12:00:00 AM" Enabled="false">
                                                        <DateInput DateFormat="dd/MM/yyyy" ReadOnly="true" DisplayDateFormat="dd/MM/yyyy">
                                                        </DateInput>
                                                        <ClientEvents OnDateSelected="OnDateSelected" />
                                                    </telerik:RadDatePicker>
                                                    <asp:Label runat="server" ID="litGetDate"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Retirement Age<span style="color: Red">*</span>
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtPDRetirementAge"
                                                        TabIndex="2" runat="server" Style="text-align: right" Width="100" MinValue="0"
                                                        MaxValue="125" Value="65">
                                                        <NumberFormat DecimalDigits="0" />
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Gender<span style="color: Red">*</span>
                                                </td>
                                                <td>
                                                    <telerik:RadTextBox ID="txtPDGender" ReadOnly="true" runat="server" Width="82px"
                                                        Enabled="false">
                                                    </telerik:RadTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Salary<span style="color: Red">*</span>
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtPDSalary"
                                                        TabIndex="3" runat="server" Style="text-align: right" Width="100" MinValue="0">
                                                        <NumberFormat DecimalDigits="2" />
                                                    </telerik:RadNumericTextBox>
                                                    <span>Zero or any positive value</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Plan Age<span style="color: Red">*</span>
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtPDPlanAge"
                                                        TabIndex="4" runat="server" Style="text-align: right" Width="100" MinValue="0"
                                                        MaxValue="125" Value="90" CssClass="txtPDPlanAge">
                                                        <NumberFormat DecimalDigits="0" />
                                                        <ClientEvents OnBlur="ComparePlaneAgeWithCurrent"></ClientEvents>
                                                    </telerik:RadNumericTextBox>
                                                    <span>Greater than current age</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </p>
                                </div>
                                <div id="step-2">
                                    <h2 class="StepTitle">
                                        <span style="text-align: left; display: inline-block; width: 24%">Current Assets</span>
                                        <span style="text-align: right; display: inline-block; width: 75%; font-weight: normal">
                                            <telerik:RadButton runat="server" ID="btnRefreshCurrentAssest" ToolTip="Refresh Current Assets..."
                                                Width="140px" Height="20px" AutoPostBack="true" OnClick="btnRefreshCurrentAssest_OnClick">
                                                <ContentTemplate>
                                                    <img src="../images/OBP/refresh-data-icon.png" alt="" style="vertical-align: middle;
                                                        height: 15px;" />
                                                    <span>Refresh Current Assets</span>
                                                </ContentTemplate>
                                            </telerik:RadButton>
                                        </span>
                                    </h2>
                                    <br />
                                    <span style="color: #0000FF">At least one asset should be provided.</span>
                                    <p>
                                        <table style="width: 100%;" class="tblAssets">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <span>Assets</span>
                                                    </th>
                                                    <th>
                                                        <asp:CheckBox ID="chkSuperannuation" runat="server" Text="Superannuation" TaxWrapper="Super"
                                                            TabIndex="1" onclick="toggleStatusSuperannuation();" AutoPostBack="false" Font-Bold="true" />
                                                    </th>
                                                    <th>
                                                        <asp:CheckBox ID="chkPension" runat="server" Text="Pension" TaxWrapper="Pension"
                                                            TabIndex="8" onclick="toggleStatusPension();" Font-Bold="true" />
                                                    </th>
                                                    <th>
                                                        <asp:CheckBox ID="chkNonSuperInvestments" runat="server" Text="Non Super Investments"
                                                            TabIndex="15" onclick="toggleStatuschkNonSuperInvestments();" TaxWrapper="Ordinary"
                                                            Font-Bold="true" />
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        Australian Equities
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="2" ID="txtSEquity" runat="server" Style="text-align: right" Width="100px"
                                                            MinValue="0" Product="Australian Equities">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="9" ID="txtPEquity" runat="server" Style="text-align: right" Width="100"
                                                            MinValue="0" Product="Australian Equities">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="16" ID="txtNEquity" runat="server" Style="text-align: right" Width="100"
                                                            MinValue="0" Product="Australian Equities">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Fixed Interest
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="3" ID="txtSFixedInterest" runat="server" Style="text-align: right"
                                                            Width="100" MinValue="0" Product="Australian Fixed Interest">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="10" ID="txtPFixedInterest" runat="server" Style="text-align: right"
                                                            Width="100" MinValue="0" Product="Australian Fixed Interest">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="17" ID="txtNFixedInterest" runat="server" Style="text-align: right"
                                                            Width="100" MinValue="0" Product="Australian Fixed Interest">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        International Equities
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="4" ID="txtSInternationalEquity" runat="server" Style="text-align: right"
                                                            Width="100" MinValue="0" Product="International Equities">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="11" ID="txtPInternationalEquity" runat="server" Style="text-align: right"
                                                            Width="100" MinValue="0" Product="International Equities">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="18" ID="txtNInternationalEquity" runat="server" Style="text-align: right"
                                                            Width="100" MinValue="0" Product="International Equities">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Cash
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="5" ID="txtSCash" runat="server" Style="text-align: right" Width="100"
                                                            MinValue="0" Product="Cash">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="12" ID="txtPCash" runat="server" Style="text-align: right" Width="100"
                                                            MinValue="0" Product="Cash">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="19" ID="txtNCash" runat="server" Style="text-align: right" Width="100"
                                                            MinValue="0" Product="Cash">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Property
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="6" ID="txtSProperty" runat="server" Style="text-align: right" Width="100"
                                                            MinValue="0" Product="Property">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="13" ID="txtPProperty" runat="server" Style="text-align: right" Width="100"
                                                            MinValue="0" Product="Property">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="20" ID="txtNProperty" runat="server" Style="text-align: right" Width="100"
                                                            MinValue="0" Product="Property">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Alternatives
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="7" ID="txtSGold" runat="server" Style="text-align: right" Width="100"
                                                            MinValue="0" Product="Gold">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="14" ID="txtPGold" runat="server" Style="text-align: right" Width="100"
                                                            MinValue="0" Product="Gold">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" Value="0" EnableViewState="true"
                                                            TabIndex="21" ID="txtNGold" runat="server" Style="text-align: right" Width="100"
                                                            MinValue="0" Product="Gold">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </p>
                                </div>
                                <div id="step-3">
                                    <h2 class="StepTitle">
                                        Actions</h2>
                                    <br />
                                    <span style="color: #0000FF; text-align: left; display: block">Start and End Age should
                                        be between Current Age
                                        <asp:Label runat="server" ID="litMinAge" ForeColor="Black"></asp:Label>
                                        and Plan Age
                                        <asp:Label runat="server" ID="litMaxAge" ForeColor="Black"></asp:Label></span>
                                    <p>
                                        <table id="dtActionTable">
                                            <tr>
                                                <td>
                                                    <span style="font-weight: bold">Actions</span>
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkSuperGuaranteeContributions" runat="server" Text="Super Guarantee Contributions"
                                                        TabIndex="1" onclick="toggleStatusSuperContribution();" Font-Bold="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkSuperSalarySacrificeContributions" runat="server" Text="Super Salary Sacrifice Contributions"
                                                        TabIndex="3" onclick="toggleSalarySacrifice();" Font-Bold="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkSuperAfterTaxContributions" runat="server" Text="Super After Tax Contributions"
                                                        TabIndex="8" onclick="toggleStatusSuperAfterTax();" Font-Bold="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkNonSuperInvestmentsSavings" runat="server" Text="Non Super Investments (Yearly Savings)"
                                                        TabIndex="12" onclick="toggleStatusSuperInvestment();" Font-Bold="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Percent of Salary
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtSGCPercentSalary"
                                                        TabIndex="2" ToolTip="between 1 and 100" runat="server" Style="text-align: right"
                                                        Width="100px" MinValue="0" MaxValue="100" Value="9.25">
                                                        <NumberFormat DecimalDigits="2" />
                                                        <ClientEvents OnBlur="OnBlurPercentSalary" />
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtSSSCPercentSalary"
                                                        TabIndex="4" ToolTip="between 1 and 1000" runat="server" Style="text-align: right"
                                                        Width="100px" MinValue="0" MaxValue="1000" Value="5">
                                                        <NumberFormat DecimalDigits="2" />
                                                        <ClientEvents OnBlur="OnBlurPercentSalary" />
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Amount
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtSSSCAmount"
                                                        TabIndex="5" ToolTip="Zero or any Possitive number" runat="server" Style="text-align: right"
                                                        Width="100px" MinValue="0">
                                                        <NumberFormat DecimalDigits="2" />
                                                        <ClientEvents OnBlur="OnBlurAmount"></ClientEvents>
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtSATCAmount"
                                                        TabIndex="9" ToolTip="Zero or any Possitive number" runat="server" Style="text-align: right"
                                                        Width="100px" MinValue="0">
                                                        <NumberFormat DecimalDigits="2" />
                                                        <ClientEvents OnBlur="OnBlurAmount"></ClientEvents>
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtNSISAmountSaved"
                                                        TabIndex="13" ToolTip="Zero or any Possitive number" runat="server" Style="text-align: right"
                                                        Width="100px" MinValue="1">
                                                        <NumberFormat DecimalDigits="2" />
                                                        <ClientEvents OnBlur="OnBlurAmount"></ClientEvents>
                                                    </telerik:RadNumericTextBox>
                                                    <telerik:RadComboBox ID="cmbFreq" runat="server" Width="40px" TabIndex="14" Style="display: none">
                                                        <Items>
                                                            <telerik:RadComboBoxItem Value="1" Text="Year" />
                                                            <telerik:RadComboBoxItem Value="2" Text="Month" />
                                                            <telerik:RadComboBoxItem Value="3" Text="Fortnight" />
                                                            <telerik:RadComboBoxItem Value="4" Text="Week" />
                                                            <telerik:RadComboBoxItem Value="5" Text="One off" />
                                                        </Items>
                                                    </telerik:RadComboBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    At What age will contribution/saving start
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtSSSCStartAge"
                                                        TabIndex="6" ToolTip="greater than or equal to your current age" runat="server"
                                                        Style="text-align: right" Width="100px" MinValue="1">
                                                        <NumberFormat DecimalDigits="0" />
                                                        <ClientEvents OnBlur="OnBlurStartAge" />
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtSATCStartAge"
                                                        TabIndex="10" ToolTip="greater than or equal to your current age" runat="server"
                                                        Style="text-align: right" Width="100px" MinValue="0">
                                                        <NumberFormat DecimalDigits="0" />
                                                        <ClientEvents OnBlur="OnBlurStartAge" />
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtNSISStartAge"
                                                        TabIndex="15" ToolTip="greater than or equal to your current age" runat="server"
                                                        Style="text-align: right" Width="100px" MinValue="0">
                                                        <NumberFormat DecimalDigits="0" />
                                                        <ClientEvents OnBlur="OnBlurStartAge" />
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    At What age will contribution/saving end
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtSSSCStopAge"
                                                        TabIndex="7" ToolTip="greater than or equal to your current age and start age"
                                                        runat="server" Style="text-align: right" Width="100px" MinValue="0">
                                                        <NumberFormat DecimalDigits="0" />
                                                        <ClientEvents OnBlur="OnBlurEndAge"></ClientEvents>
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtSATCStopAge"
                                                        TabIndex="11" ToolTip="greater than or equal to your current age and start age"
                                                        runat="server" Style="text-align: right" Width="100px" MinValue="0">
                                                        <NumberFormat DecimalDigits="0" />
                                                        <ClientEvents OnBlur="OnBlurEndAge"></ClientEvents>
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtNSISStopAge"
                                                        TabIndex="16" ToolTip="greater than or equal to your current age and start age"
                                                        runat="server" Style="text-align: right" Width="100px" MinValue="0">
                                                        <NumberFormat DecimalDigits="0" />
                                                        <ClientEvents OnBlur="OnBlurEndAge"></ClientEvents>
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </p>
                                </div>
                                <div id="step-4">
                                    <h2 class="StepTitle">
                                        General Goals
                                    </h2>
                                    <br />
                                    <div>
                                        <span style="color: #0000FF; text-align: left">At least one Goal should be provided.</span>
                                        <span style="color: #0000FF; text-align: left; display: block">Start and End Age should
                                            be between Current Age
                                            <asp:Label runat="server" ID="lblMinAgeGoal" ForeColor="Black"></asp:Label>
                                            and Plan Age
                                            <asp:Label runat="server" ID="lblMaxAgeGoal" ForeColor="Black"></asp:Label></span>
                                    </div>
                                    <p>
                                        <table width="100%">
                                            <tr>
                                                <td width="100%" colspan="2">
                                                    <asp:CheckBox ID="chkWealth" runat="server" Text="Wealth" Font-Bold="true" TabIndex="1" />
                                                </td>
                                            </tr>
                                            <tr class="clsTrGoalsWealth">
                                                <td width="20%">
                                                </td>
                                                <td width="80%">
                                                    <div id="divWealth">
                                                        <table width="100%">
                                                            <tr>
                                                                <td colspan="5">
                                                                    <telerik:RadGrid ID="grdGoalsWealth" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                                                                        Width="100%" PageSize="25" AllowSorting="False" AllowMultiRowSelection="False"
                                                                        AllowPaging="False" GridLines="Both" AllowFilteringByColumn="False" EnableViewState="true"
                                                                        ShowFooter="True">
                                                                        <MasterTableView AllowFilteringByColumn="False" TableLayout="Auto" Width="100%">
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText="I want to have Total Assets Worth $" UniqueName="TotalAssetsWorth"
                                                                                    AllowFiltering="false" HeaderStyle-Width="50px">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label runat="server" ID="lblTotalAssetsWorth" Text='<%# Eval("Amount") %>' CssClass="clsGoalsAmount"></asp:Label>
                                                                                        <asp:Label runat="server" ID="lblGUID" Text='<%# Eval("GoalId") %>' Style="display: none"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtGoalsWealth_TotalAssetsWorth"
                                                                                            TabIndex="1" runat="server" Style="text-align: right" Width="100" MinValue="1"
                                                                                            Enabled="true" DataColumn="Amount" CssClass="riTextBox riHover txtGoalsWealth_TotalAssetsWorth">
                                                                                        </telerik:RadNumericTextBox>
                                                                                    </FooterTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Right" CssClass="gridItemRow"></ItemStyle>
                                                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="From Age" UniqueName="DollarsFromAge" AllowFiltering="false"
                                                                                    HeaderStyle-Width="50px">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label runat="server" ID="lblDollarsFromAge" Text='<%# Eval("Age") %>' CssClass="clsGoalsAge"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtGoalsWealth_DollarsFromAge"
                                                                                            TabIndex="1" runat="server" Style="text-align: right" Width="100" MinValue="1"
                                                                                            Enabled="true" DataColumn="Age" CssClass="riTextBox riHover txtGoalsWealth_DollarsFromAge_Footer">
                                                                                            <NumberFormat DecimalDigits="0"></NumberFormat>
                                                                                        </telerik:RadNumericTextBox>
                                                                                    </FooterTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Action" UniqueName="Action" AllowFiltering="false"
                                                                                    HeaderStyle-Width="50px">
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton runat="server" ID="imgRemoveRow" ImageUrl="../images/OBP/delete_octagon.png"
                                                                                            OnClick="imgGoalsWealthRemoveRow_OnClick" ToolTip="Remove Row" CssClass="clsGoalsWealthGridRowRemove clsGoalsGridRowRemove" />
                                                                                        <asp:ImageButton runat="server" ID="imgEditRow" ImageUrl="../images/OBP/edit2-icon.png"
                                                                                            ToolTip="Edit Row" Height="16" Width="16" CssClass="clsGoalsGridRowEdit clsGoalsWealthGridRowEdit" />
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:ImageButton runat="server" ID="imgAddRow" ImageUrl="../images/OBP/add_octagon.png"
                                                                                            OnClick="imgGoalsWealthAddRow_OnClick" ToolTip="Add Row" Height="16" Width="16"
                                                                                            CssClass="clsGoalsWealthGridRowAdd" />
                                                                                        <asp:ImageButton runat="server" ID="imgUpdateEdit" ImageUrl="../images/OBP/tick_octagon.png"
                                                                                            ToolTip="Update Row" Height="16" Width="16" Style="display: none" CssClass="clsGoalsWealthGridRowUpdateEdit" />
                                                                                        <asp:ImageButton runat="server" ID="imgCancelEdit" ImageUrl="../images/OBP/reset_octagon.png"
                                                                                            ToolTip="Cancel Edit" Height="16" Width="16" Style="display: none" CssClass="clsGoalsWealthGridRowCancelEdit" />
                                                                                    </FooterTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                    <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                                                </telerik:GridTemplateColumn>
                                                                            </Columns>
                                                                            <NoRecordsTemplate>
                                                                                <div style="text-align: center; font-weight: bold">
                                                                                    <span>Add Record</span>
                                                                                </div>
                                                                            </NoRecordsTemplate>
                                                                        </MasterTableView>
                                                                        <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                                                                            <Excel Format="Html"></Excel>
                                                                        </ExportSettings>
                                                                    </telerik:RadGrid>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" colspan="2">
                                                    <asp:CheckBox ID="chkIncome" runat="server" Text="Income" Font-Bold="true" TabIndex="4" />
                                                </td>
                                            </tr>
                                            <tr class="clsTrGoalsIncome">
                                                <td width="20%">
                                                </td>
                                                <td width="80%">
                                                    <div id="divIncome">
                                                        <table width="100%">
                                                            <tr>
                                                                <td colspan="5">
                                                                    <telerik:RadGrid ID="grdGoalsIncome" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                                                                        Width="100%" PageSize="25" AllowSorting="False" AllowMultiRowSelection="False"
                                                                        AllowPaging="False" GridLines="Both" AllowFilteringByColumn="False" EnableViewState="true"
                                                                        ShowFooter="True">
                                                                        <MasterTableView AllowFilteringByColumn="False" TableLayout="Auto" Width="100%">
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText="I want to have these many dollars annually"
                                                                                    UniqueName="TotalIncomeAmount" AllowFiltering="false" HeaderStyle-Width="50px">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label runat="server" ID="lblTotalIncomeRequired" Text='<%# Eval("Amount") %>'
                                                                                            CssClass="clsGoalsAmount"></asp:Label>
                                                                                        <asp:Label runat="server" ID="lblGUID" Text='<%# Eval("GoalId") %>' Style="display: none"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtGoalsIncome_TotalIncomeRequired"
                                                                                            TabIndex="1" runat="server" Style="text-align: right" Width="100" Enabled="true"
                                                                                            DataColumn="Amount" CssClass="riTextBox riHover txtGoalsIncome_TotalIncomeRequired">
                                                                                        </telerik:RadNumericTextBox>
                                                                                    </FooterTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="From Age" UniqueName="DollarsFromAge" AllowFiltering="false"
                                                                                    HeaderStyle-Width="50px">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label runat="server" ID="lblDollarsFromAge" Text='<%# Eval("Age") %>' CssClass="clsGoalsAge"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtGoalsIncome_DollarsFromAge"
                                                                                            TabIndex="1" runat="server" Style="text-align: right" Width="100" MinValue="1"
                                                                                            Enabled="true" DataColumn="Age" CssClass="riTextBox riHover txtGoalsIncome_DollarsFromAge_Footer">
                                                                                            <NumberFormat DecimalDigits="0"></NumberFormat>
                                                                                        </telerik:RadNumericTextBox>
                                                                                    </FooterTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Action" UniqueName="Action" AllowFiltering="false"
                                                                                    HeaderStyle-Width="50px">
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton runat="server" ID="imgRemoveRow" ImageUrl="../images/OBP/delete_octagon.png"
                                                                                            OnClick="imgGoalsIncomeRemoveRow_OnClick" ToolTip="Remove Row" CssClass="clsGoalsIncomeGridRowRemove clsGoalsGridRowRemove" />
                                                                                        <asp:ImageButton runat="server" ID="imgEditRow" ImageUrl="../images/OBP/edit2-icon.png"
                                                                                            ToolTip="Edit Row" Height="16" Width="16" CssClass="clsGoalsGridRowEdit clsGoalsIncomeGridRowEdit" />
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:ImageButton runat="server" ID="imgAddRow" ImageUrl="../images/OBP/add_octagon.png"
                                                                                            OnClick="imgGoalsIncomeAddRow_OnClick" ToolTip="Add Row" Height="16" Width="16"
                                                                                            CssClass="clsGoalsIncomeGridRowAdd" />
                                                                                            
                                                                                        <asp:ImageButton runat="server" ID="imgUpdateEdit" ImageUrl="../images/OBP/tick_octagon.png"
                                                                                            ToolTip="Update Row" Height="16" Width="16" Style="display: none" CssClass="clsGoalsIncomeGridRowUpdateEdit" />
                                                                                        <asp:ImageButton runat="server" ID="imgCancelEdit" ImageUrl="../images/OBP/reset_octagon.png"
                                                                                            ToolTip="Cancel Edit" Height="16" Width="16" Style="display: none" CssClass="clsGoalsIncomeGridRowCancelEdit" />

                                                                                    </FooterTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                    <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                                                </telerik:GridTemplateColumn>
                                                                            </Columns>
                                                                            <NoRecordsTemplate>
                                                                                <div style="text-align: center; font-weight: bold">
                                                                                    <span>Add Record</span>
                                                                                </div>
                                                                            </NoRecordsTemplate>
                                                                        </MasterTableView>
                                                                        <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                                                                            <Excel Format="Html"></Excel>
                                                                        </ExportSettings>
                                                                    </telerik:RadGrid>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" colspan="2">
                                                    <asp:CheckBox ID="chkLumpSum" runat="server" Text="Lump Sum" Font-Bold="true" TabIndex="7" />
                                                </td>
                                            </tr>
                                            <tr class="clsTrGoalsLumpSum">
                                                <td width="20%">
                                                </td>
                                                <td width="80%">
                                                    <div id="divLumpSum">
                                                        <table width="100%">
                                                            <tr>
                                                                <td colspan="5">
                                                                    <telerik:RadGrid ID="grdGoalsLumpSum" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                                                                        Width="100%" PageSize="25" AllowSorting="False" AllowMultiRowSelection="False"
                                                                        AllowPaging="False" GridLines="Both" AllowFilteringByColumn="False" EnableViewState="true"
                                                                        ShowFooter="True">
                                                                        <MasterTableView AllowFilteringByColumn="False" TableLayout="Auto" Width="100%">
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText=" I want to take a Lump Sum of $" UniqueName="TotalLumpSumAmount"
                                                                                    AllowFiltering="false" HeaderStyle-Width="50px">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label runat="server" ID="lblTotalLumpSumRequired" Text='<%# Eval("Amount") %>'
                                                                                            CssClass="clsGoalsAmount"></asp:Label>
                                                                                        <asp:Label runat="server" ID="lblGUID" Text='<%# Eval("GoalId") %>' Style="display: none"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtGoalsLumpSum_TotalLumpSum"
                                                                                            TabIndex="1" runat="server" Style="text-align: right" Width="100" MinValue="1"
                                                                                            Enabled="true" DataColumn="Amount" CssClass="riTextBox riHover txtGoalsLumpSum_TotalLumpSum">
                                                                                        </telerik:RadNumericTextBox>
                                                                                    </FooterTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="At Age" UniqueName="DollarsFromAge" AllowFiltering="false"
                                                                                    HeaderStyle-Width="50px">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label runat="server" ID="lblDollarsFromAge" Text='<%# Eval("Age") %>' CssClass="clsGoalsAge"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtGoalsLumpSum_DollarsFromAge"
                                                                                            TabIndex="1" runat="server" Style="text-align: right" Width="100" MinValue="1"
                                                                                            Enabled="true" DataColumn="Age" CssClass="riTextBox riHover txtGoalsLumpSum_DollarsFromAge_Footer">
                                                                                            <NumberFormat DecimalDigits="0"></NumberFormat>
                                                                                        </telerik:RadNumericTextBox>
                                                                                    </FooterTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Action" UniqueName="Action" AllowFiltering="false"
                                                                                    HeaderStyle-Width="50px">
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton runat="server" ID="imgRemoveRow" ImageUrl="../images/OBP/delete_octagon.png"
                                                                                            OnClick="imgGoalsLumpSumRemoveRow_OnClick" ToolTip="Remove Row" CssClass="clsGoalsLumpSumGridRowRemove clsGoalsGridRowRemove" />
                                                                                        <asp:ImageButton runat="server" ID="imgEditRow" ImageUrl="../images/OBP/edit2-icon.png"
                                                                                            ToolTip="Edit Row" Height="16" Width="16" CssClass="clsGoalsGridRowEdit clsGoalsLumpSumGridRowEdit" />   
                                                                                </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:ImageButton runat="server" ID="imgAddRow" ImageUrl="../images/OBP/add_octagon.png"
                                                                                            OnClick="imgGoalsLumpSumAddRow_OnClick" ToolTip="Add Row" Height="16" Width="16"
                                                                                            CssClass="clsGoalsLumpSumGridRowAdd" />
                                                                                        <asp:ImageButton runat="server" ID="imgUpdateEdit" ImageUrl="../images/OBP/tick_octagon.png"
                                                                                            ToolTip="Update Row" Height="16" Width="16" Style="display: none" CssClass="clsGoalsLumpSumGridRowUpdateEdit" />
                                                                                        <asp:ImageButton runat="server" ID="imgCancelEdit" ImageUrl="../images/OBP/reset_octagon.png"
                                                                                            ToolTip="Cancel Edit" Height="16" Width="16" Style="display: none" CssClass="clsGoalsLumpSumGridRowCancelEdit" />    
    
                                                                                    </FooterTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                    <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                                                </telerik:GridTemplateColumn>
                                                                            </Columns>
                                                                            <NoRecordsTemplate>
                                                                                <div style="text-align: center; font-weight: bold">
                                                                                    <span>Add Record</span>
                                                                                </div>
                                                                            </NoRecordsTemplate>
                                                                        </MasterTableView>
                                                                        <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                                                                            <Excel Format="Html"></Excel>
                                                                        </ExportSettings>
                                                                    </telerik:RadGrid>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" colspan="2">
                                                    <asp:CheckBox ID="chkLiquidAssets" runat="server" Text="Liquid Assets" Font-Bold="true"
                                                        TabIndex="10" />
                                                </td>
                                            </tr>
                                            <tr class="clsTrGoalsLiquidAssets">
                                                <td width="20%">
                                                </td>
                                                <td width="80%">
                                                    <div id="divLiquidAssets">
                                                        <table width="100%">
                                                            <tr>
                                                                <td colspan="5">
                                                                    <telerik:RadGrid ID="grdGoalsLiquidAssets" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                                                                        Width="100%" PageSize="25" AllowSorting="False" AllowMultiRowSelection="False"
                                                                        AllowPaging="False" GridLines="Both" AllowFilteringByColumn="False" EnableViewState="true"
                                                                        ShowFooter="True">
                                                                        <MasterTableView AllowFilteringByColumn="False" TableLayout="Auto" Width="100%">
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText="I want to have $" UniqueName="TotalLiquidAssetsAmount"
                                                                                    AllowFiltering="false" HeaderStyle-Width="50px">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label runat="server" ID="lblTotalLiquidAssetsRequired" Text='<%# Eval("Amount") %>'
                                                                                            CssClass="clsGoalsAmount"></asp:Label>
                                                                                        <asp:Label runat="server" ID="lblGUID" Text='<%# Eval("GoalId") %>' Style="display: none"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtGoalsLiquidAssets_TotalLiquidAssets"
                                                                                            TabIndex="1" runat="server" Style="text-align: right" Width="100" MinValue="1"
                                                                                            Enabled="true" DataColumn="Amount" CssClass="riTextBox riHover txtGoalsLiquidAssets_TotalLiquidAssets">
                                                                                        </telerik:RadNumericTextBox>
                                                                                    </FooterTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="From Age" UniqueName="DollarsFromAge" AllowFiltering="false"
                                                                                    HeaderStyle-Width="50px">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label runat="server" ID="lblDollarsFromAge" Text='<%# Eval("Age") %>' CssClass="clsGoalsAge"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtGoalsLiquidAssets_DollarsFromAge"
                                                                                            TabIndex="1" runat="server" Style="text-align: right" Width="100" MinValue="1"
                                                                                            Enabled="true" DataColumn="Age" CssClass="riTextBox riHover txtGoalsLiquidAssets_DollarsFromAge_Footer">
                                                                                            <NumberFormat DecimalDigits="0"></NumberFormat>
                                                                                        </telerik:RadNumericTextBox>
                                                                                    </FooterTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Action" UniqueName="Action" AllowFiltering="false"
                                                                                    HeaderStyle-Width="50px">
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton runat="server" ID="imgRemoveRow" ImageUrl="../images/OBP/delete_octagon.png"
                                                                                            OnClick="imgGoalsLiquidAssetsRemoveRow_OnClick" ToolTip="Remove Row" CssClass="clsGoalsLiquidAssetsGridRowRemove clsGoalsGridRowRemove" />
                                                                                        <asp:ImageButton runat="server" ID="imgEditRow" ImageUrl="../images/OBP/edit2-icon.png"
                                                                                            ToolTip="Edit Row" Height="16" Width="16" CssClass="clsGoalsGridRowEdit clsGoalsLiquidityGridRowEdit" />
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:ImageButton runat="server" ID="imgAddRow" ImageUrl="../images/OBP/add_octagon.png"
                                                                                            OnClick="imgGoalsLiquidAssetsAddRow_OnClick" ToolTip="Add Row" Height="16" Width="16"
                                                                                            CssClass="clsGoalsLiquidAssetsGridRowAdd" />
                                                                                        <asp:ImageButton runat="server" ID="imgUpdateEdit" ImageUrl="../images/OBP/tick_octagon.png"
                                                                                            ToolTip="Update Row" Height="16" Width="16" Style="display: none" CssClass="clsGoalsLiquidityGridRowUpdateEdit" />
                                                                                        <asp:ImageButton runat="server" ID="imgCancelEdit" ImageUrl="../images/OBP/reset_octagon.png"
                                                                                            ToolTip="Cancel Edit" Height="16" Width="16" Style="display: none" CssClass="clsGoalsLiquidityGridRowCancelEdit" />
                                                                                    </FooterTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                    <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                                                </telerik:GridTemplateColumn>
                                                                            </Columns>
                                                                            <NoRecordsTemplate>
                                                                                <div style="text-align: center; font-weight: bold">
                                                                                    <span>Add Record</span>
                                                                                </div>
                                                                            </NoRecordsTemplate>
                                                                        </MasterTableView>
                                                                        <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                                                                            <Excel Format="Html"></Excel>
                                                                        </ExportSettings>
                                                                    </telerik:RadGrid>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" colspan="2">
                                                    <asp:CheckBox ID="chkBequest" runat="server" Text="Bequest" Font-Bold="true" TabIndex="13" />
                                                </td>
                                            </tr>
                                            <tr class="clsTrGoalsBequest">
                                                <td width="20%">
                                                </td>
                                                <td width="80%">
                                                    <div id="divBequest">
                                                        <table width="100%">
                                                            <tr>
                                                                <td colspan="5">
                                                                    <telerik:RadGrid ID="grdGoalsBequest" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                                                                        Width="100%" PageSize="25" AllowSorting="False" AllowMultiRowSelection="False"
                                                                        AllowPaging="False" GridLines="Both" AllowFilteringByColumn="False" EnableViewState="true"
                                                                        ShowFooter="True">
                                                                        <MasterTableView AllowFilteringByColumn="False" TableLayout="Auto" Width="100%">
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText="I want to leave my family with ($)" UniqueName="TotalBequestAmount"
                                                                                    AllowFiltering="false" HeaderStyle-Width="50px">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label runat="server" ID="lblTotalBequestRequired" Text='<%# Eval("Amount") %>'
                                                                                            CssClass="clsGoalsAmount"></asp:Label>
                                                                                        <asp:Label runat="server" ID="lblGUID" Text='<%# Eval("GoalId") %>' Style="display: none"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtGoalsBequest_TotalBequest"
                                                                                            TabIndex="1" runat="server" Style="text-align: right" Width="100" MinValue="1"
                                                                                            Enabled="true" DataColumn="Amount" CssClass="riTextBox riHover txtGoalsBequest_TotalBequest">
                                                                                        </telerik:RadNumericTextBox>
                                                                                    </FooterTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="When I'm Age" UniqueName="DollarsFromAge"
                                                                                    AllowFiltering="false" HeaderStyle-Width="50px">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label runat="server" ID="lblDollarsFromAge" Text='<%# Eval("Age") %>' CssClass="clsGoalsAge"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtGoalsBequest_DollarsFromAge"
                                                                                            TabIndex="1" runat="server" Style="text-align: right" Width="100" MinValue="1"
                                                                                            Enabled="true" DataColumn="Age" CssClass="riTextBox riHover txtGoalsBequest_DollarsFromAge_Footer">
                                                                                            <NumberFormat DecimalDigits="0"></NumberFormat>
                                                                                        </telerik:RadNumericTextBox>
                                                                                    </FooterTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Action" UniqueName="Action" AllowFiltering="false"
                                                                                    HeaderStyle-Width="50px">
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton runat="server" ID="imgRemoveRow" ImageUrl="../images/OBP/delete_octagon.png"
                                                                                            OnClick="imgGoalsBequestRemoveRow_OnClick" ToolTip="Remove Row" CssClass="clsGoalsBequestGridRowRemove clsGoalsGridRowRemove" />
                                                                                        <asp:ImageButton runat="server" ID="imgEditRow" ImageUrl="../images/OBP/edit2-icon.png"
                                                                                            ToolTip="Edit Row" Height="16" Width="16" CssClass="clsGoalsGridRowEdit clsGoalsBequestGridRowEdit" />
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:ImageButton runat="server" ID="imgAddRow" ImageUrl="../images/OBP/add_octagon.png"
                                                                                            OnClick="imgGoalsBequestAddRow_OnClick" ToolTip="Add Row" Height="16" Width="16"
                                                                                            CssClass="clsGoalsBequestGridRowAdd" />
                                                                                        <asp:ImageButton runat="server" ID="imgUpdateEdit" ImageUrl="../images/OBP/tick_octagon.png"
                                                                                            ToolTip="Update Row" Height="16" Width="16" Style="display: none" CssClass="clsGoalsBequestGridRowUpdateEdit" />
                                                                                        <asp:ImageButton runat="server" ID="imgCancelEdit" ImageUrl="../images/OBP/reset_octagon.png"
                                                                                            ToolTip="Cancel Edit" Height="16" Width="16" Style="display: none" CssClass="clsGoalsBequestGridRowCancelEdit" />

                                                                                    </FooterTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                    <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                                                </telerik:GridTemplateColumn>
                                                                            </Columns>
                                                                            <NoRecordsTemplate>
                                                                                <div style="text-align: center; font-weight: bold">
                                                                                    <span>Add Record</span>
                                                                                </div>
                                                                            </NoRecordsTemplate>
                                                                        </MasterTableView>
                                                                        <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                                                                            <Excel Format="Html"></Excel>
                                                                        </ExportSettings>
                                                                    </telerik:RadGrid>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </p>
                                </div>
                                <div id="step-5">
                                    <h2 class="StepTitle">
                                        Investment Strategies</h2>
                                    <br />
                                    <div style="text-align: center; width: 100%;">
                                        <telerik:RadComboBox ID="rcbInvestmentStrategy" runat="server" Width="30%" TabIndex="16"
                                            OnClientSelectedIndexChanged="OnClientSelectedIndexChanged">
                                        </telerik:RadComboBox>
                                    </div>
                                    <br />
                                    <div style="width: 100%;">
                                        <table id="tblInvestmentStrategyAssetWeightings" class="tblInvestmentStrategies"></table>    
                                        <asp:Panel ID="palAsCurrentInvestMenet" runat="server" Style="display: none;" Width="100%">
                                            <table class="tblInvestmentStrategies">
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblSuperR1" Text="Super"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblPensionR2" Text="Pension"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="Label2" Text="Ordinary"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblHCurrentAustrialEquity" Text="Australian Equity"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblCurrentAustrilR1" Text="23%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblCurrentAustriR2" Text="0%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="Label3" Text="0%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblhCurrentInternationEqulity" Text="International Equity"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblCurrentInternationR1" Text="0%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblCurrentInternationR2" Text="0%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="Label4" Text="0%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblhCurrentProperty" Text="Property"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblCurrentPropertyR1" Text="0%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblCurrentPropertyR2" Text="0%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="Label5" Text="0%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblhCurrentFixedInterest" Text="Fixed Interest"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblCurretnFixedInterestR1" Text="0%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblCurretnFixedInterestR2" Text="0%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="Label6" Text="0%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblhCurrentCast" Text="Cash"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblCurretnCastR1" Text="0%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblCurretnCastR2" Text="0%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="Label7" Text="0%"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pal100Equality" runat="server" Style="display: none;" Width="100%">
                                            <table class="tblInvestmentStrategies">
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblEqulityBeforReterimentR1" Text="Before Retirement"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblEqulityAfterReterimentR1" Text="After Retirement"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblhEqulityAustrial" Text="Australian Equity"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblEqulityAusrtriltR1" Text="50%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblEqulityAusrtriltR2" Text="50%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblhEqulity100Internation" Text="International Equity"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblEqulityInternationtR1" Text="50%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblEqulityInternationtR2" Text="50%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblhEqulity100Property" Text="Property"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblEqulityPropertytR1" Text="0%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblEqulityPropertytR2" Text="0%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblhEqulity100FixedInterest" Text="Fixed Interest"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblEqulityFixedInterestR1" Text="0%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblEqulityFixedInterestR2" Text="0%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblhEqulity100Cast" Text="Cash"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblEqulityCastR1" Text="0%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblEqulityCastR2" Text="0%"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="palGrothFund" Style="display: none;" Width="100%">
                                            <table class="tblInvestmentStrategies">
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblBeforReterimetnGrothR1" Text="Before Retirement"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblAfterReterimetnGrothR2" Text="After Retirement"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblHAustrialGroth" Text="Australian Equity"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblAustrilGrothR1" Text="35%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblAustrilGrothR2" Text="35%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblHInternalGroth" Text="International Equity"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblInternationnGrothR1" Text="40%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblInternationnGrothR2" Text="40%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblHPropertyGroth" Text="Property"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblPropertynGrothR1" Text="10%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblPropertynGrothR2" Text="10%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblHFixedInterestGroth" Text="Fixed Interest"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblFixedInterestGrothR1" Text="10%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblFixedInterestGrothR2" Text="10%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblHCastGroth" Text="Cash"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblCastnGrothR1" Text="5%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblCastnGrothR2" Text="5%"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="PalBalFund" Style="display: none;" Width="100%">
                                            <table class="tblInvestmentStrategies">
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblBeforReterimentBalFundR1" Text="Before Retirement"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblAfterReterimentBalFundR2" Text="After Retirement"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblHAustrialBalFund" Text="Australian Equity"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblAustrialBalFundR1" Text="30%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblAustrialBalFundR2" Text="30%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblHInternationalBalFund" Text="International Equity"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="LalblInternationBalFundR1" Text="30%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="LalblInternationBalFundR2" Text="30%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblHPropertyBalFund" Text="Property"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblPropertyBalFundR1" Text="10%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblPropertyBalFundR2" Text="10%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblHFixedFund" Text="Fixed Interest"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblFixedInterestBalFundR1" Text="20%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblFixedInterestBalFundR2" Text="20%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblHCastBalFund" Text="Cash"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblCastBalFundR1" Text="10%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblCastBalFundR2" Text="10%"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="PalConservertionFund" Style="display: none;" Width="100%">
                                            <table class="tblInvestmentStrategies">
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblBeforeReterimetnConverstionFunctionR1" Text="Before Retirement"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblAftereReterimetnConverstionFunctionR2" Text="After Retirement"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblHAustrialConversertionFund" Text="Australian Equity"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblAustrialConverstionFunctionR1" Text="20%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblAustrialConverstionFunctionR2" Text="20%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblHInternationConversertionFund" Text="International Equity"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblInternationConverstionFunctionR1" Text="20%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblInternationConverstionFunctionR2" Text="20%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblHPropertyConversertionFund" Text="Property"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblPropertyConverstionFunctionR1" Text="0%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblPropertyConverstionFunctionR2" Text="0%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblHFixedInterestConversertionFund" Text="Fixed Interest"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblFixedInterestConverstionFunctionR1" Text="40%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblFixedInterestConverstionFunctionR2" Text="40%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblHCashConversertionFund" Text="Cash"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblCastnConverstionFunctionR1" Text="20%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblCastnConverstionFunctionR2" Text="20%"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                            <!-- End SmartWizard Content -->
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="results" style="display:none;">
            <h3>
                <span style="display: inline-block; width: 90%; text-algin: left;">Graphs</span>
                <a href="#" class="lnkViewInputGraphData" style="display: inline-block; width: 38%;
                    text-algin: right; color: white; width: 12px; height: 12px" title="Click to view graph input data">
                    <img src="../images/OBP/input.png" />Input </a>&nbsp;&nbsp; <a href="#" class="lnkViewOutputGraphData"
                        style="display: inline-block; width: 38%; text-algin: right; color: white; width: 12px;
                        height: 12px" title="Click to view graph output data">
                        <img src="../images/OBP/output.png" />Output </a>
            </h3>
            <div id="tabs">
                <ul>
                    <li style="width: 80px"><a id="lnkAssets" href="#tabs-1">Assets</a></li>
                    <li style="width: 80px"><a id="lnkIncome" href="#tabs-2">Income</a></li>
                    <li style="width: 85px"><a id="lnkProbability" href="#tabs-3">Probability</a></li>
                    <li style="width: 95px"><a id="lnkGoals" href="#tabs-4">Goal Heatmap</a></li>
                    <li style="width: 120px"><a id="lnkResults" href="#tabs-5">Compare Results</a></li>
                </ul>
                <div id="tabs-1" style="height: 480px; position: relative; top: 7px">
                    <div class="tab-container">
                        <obp:OBPGraph runat="server" ID="OBPGraph1" GraphType="Asset" />
                        <div id="chart_Asset_interpolate" style="width: 580px; height: 220px; display: none">
                            <%--<span style="text-align: center">No data to display</span>--%>
                        </div>
                        <div id="chart_Asset" style="width: 580px; height: 440px;">
                            <%--<span style="text-align: center">No data to display</span>--%>
                        </div>
                    </div>
                </div>
                <div id="tabs-2" style="height: 480px; position: relative; top: 7px">
                    <div class="tab-container">
                        <obp:OBPGraph runat="server" ID="OBPGraph2" GraphType="Income" />
                        <div id="chart_Income" style="width: 580px; height: 440px;">
                            <%--<span style="text-align: center">No data to display</span>--%>
                        </div>
                        <div id="chart_Income_TestInterpolate" style="display: none">
                        </div>
                    </div>
                </div>
                <div id="tabs-3" style="height: 480px; position: relative; top: 7px">
                    <div class="tab-container">
                        <obp:OBPGraph runat="server" ID="OBPGraph3" GraphType="Probability" />
                        <p id="p_cmbage" runat="server">
                            I want to see my probability of achieving my goals at age :
                            <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtNumTimeSteps"
                                TabIndex="1" runat="server" Style="text-align: right" Width="100" Enabled="true"
                                CssClass="riTextBox riHover txtNumTimeSteps">
                                <NumberFormat DecimalDigits="0"></NumberFormat>
                            </telerik:RadNumericTextBox>
                            <input id="chkShowAllNumTimeSteps_old" type="checkbox" checked="checked" style="display: none" />
                            <span style="display: none">All Times</span>
                        </p>
                        <div id="chart_Probability" style="width: 580px; height: 403px;">
                            <%--<span style="text-align: center">No data to display</span>--%>
                        </div>
                    </div>
                </div>
                <div id="tabs-4" style="height: 415px; position: relative; top: 7px">
                    <div class="tab-container">
                        <obp:OBPGraph runat="server" ID="OBPGraph4" GraphType="Goals" />
                        <span style="padding:20% 10% 20% 2%;font: 12px Arial,Helvetica,sans-serif">Legend</span>
                        <div id="chart_goals_Color_Legend" style="margin: 0% 15% 0 15%; width: 70%; height: 15px; text-align:left">
                        </div>
                        <div id="chart_goals_Color_Legend_XAxis_Labels" style="margin: 0% 0% 0 15%; width: 70%; height: 30px; text-align:left;"></div>
                        <asp:Label runat="server" ID="Label9" Text="Wealth" style="padding:10px; font: 12px Arial,Helvetica,sans-serif"></asp:Label>
                        <div id="chart_goals_Wealth" style="width: 580px; height: 43px;">
                            <%--<span style="text-align: center">No data to display</span>--%>
                        </div>
                        <asp:Label runat="server" ID="Label1" Text="Income" style="padding:10px; font: 12px Arial,Helvetica,sans-serif"></asp:Label>
                        <div id="chart_goals_Income" style="width: 580px; height: 43px;">
                            <%--<span style="text-align: center">No data to display</span>--%>
                        </div>
                        <asp:Label runat="server" ID="Label8" Text="Lump Sum" style="padding:10px; font: 12px Arial,Helvetica,sans-serif"></asp:Label>
                        <div id="chart_goals_LumpSum" style="width: 580px; height: 43px;">
                            <%--<span style="text-align: center">No data to display</span>--%>
                        </div>
                        <asp:Label runat="server" ID="Label10" Text="Liquidity" style="padding:10px; font: 12px Arial,Helvetica,sans-serif"></asp:Label>
                        <div id="chart_goals_Liquidity" style="width: 580px; height: 43px;">
                            <%--<span style="text-align: center">No data to display</span>--%>
                        </div>
                        <asp:Label runat="server" ID="Label11" Text="Bequest" style="padding:10px; font: 12px Arial,Helvetica,sans-serif"></asp:Label>
                        <div id="chart_goals_Bequest" style="width: 580px; height: 43px;">
                            <%--<span style="text-align: center">No data to display</span>--%>
                        </div>
                        <div id="chart_goals_Age_Axis" style="width: 500px; height: 43px;">
                            <%--<span style="text-align: center">No data to display</span>--%>
                        </div>
                    </div>
                </div>
                <div id="tabs-5" style="height: 480px; position: relative; top: 7px">
                    <div class="tab-container">
                        <div id="divCompareRunsAge">
                            Age:
                            <telerik:RadNumericTextBox ShowSpinButtons="true" EnableViewState="true" ID="txtNumTimeStepsCompareRuns"
                                TabIndex="1" runat="server" Style="text-align: right" Width="100" Enabled="true"
                                CssClass="riTextBox riHover txtNumTimeStepsCompareRuns">
                                <NumberFormat DecimalDigits="0"></NumberFormat>
                            </telerik:RadNumericTextBox>
                            <input id="chkShowAllNumTimeStepsCompareRuns_old" type="checkbox" checked="checked"
                                style="display: none" />
                            <span style="display: none">All Times</span>
                        </div>
                        <table class="tblProjectionsComparison" style="width: 100%; border-spacing: none"
                            cellspacing="0" cellpadding="0" border="0">
                            <thead>
                                <tr>
                                    <th style="width: 5%">
                                        Run
                                    </th>
                                    <th style="width: 19%">
                                        Wealth
                                    </th>
                                    <th style="width: 19%">
                                        Income
                                    </th>
                                    <th style="width: 19%">
                                        Lump Sum
                                    </th>
                                    <th style="width: 19%">
                                        Liquidity
                                    </th>
                                    <th style="width: 19%">
                                        Bequest
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="divIndividualModal" runat="server" style="display: none;" class="holder">
        <div class="popup">
            <div class="content">
                <uc:IndividualControl ID="IndividualControl" runat="server" />
            </div>
        </div>
    </div>
    <div id="divAdviceName" style="display: none;">
        <div class="popup1" style="text-align: center;">
            <span>Save this projection as</span>
            <telerik:RadTextBox ID="txtAdviceName" runat="server" Width="300px" MaxLength="70">
            </telerik:RadTextBox>
            <br />
            <telerik:RadButton ID="btnPopupSave" runat="server" Text="Save Projection" OnClick="btnPopupSave_OnClick">
            </telerik:RadButton>
            <telerik:RadButton ID="btnCancel" runat="server" AutoPostBack="false" Text="Cancel"
                OnClientClicked="btnCancel_OnClick">
            </telerik:RadButton>
        </div>
    </div>
    <div id="divOver" runat="server" class="overlay" style="display: none;">
    </div>
    <asp:HiddenField ID="hfGoalsWealthSessionKey" runat="server" EnableViewState="true"
        Value="" />
    <asp:HiddenField ID="hfGoalsIncomeSessionKey" runat="server" EnableViewState="true"
        Value="" />
    <asp:HiddenField ID="hfGoalsLumpSumSessionKey" runat="server" EnableViewState="true"
        Value="" />
    <asp:HiddenField ID="hfGoalsLiquidAssetsSessionKey" runat="server" EnableViewState="true"
        Value="" />
    <asp:HiddenField ID="hfGoalsBequestSessionKey" runat="server" EnableViewState="true"
        Value="" />
    <asp:HiddenField ID="hfGridFocusControlID" runat="server" EnableViewState="true"
        Value="0" />
    <asp:HiddenField ID="hfIsRefreshCurrentAssetsPostback" runat="server" EnableViewState="true"
        Value="0" />
    <asp:HiddenField ID="hfIsGraphWizardDisabled" runat="server" EnableViewState="true"
        Value="1" />
    <asp:HiddenField ID="hfCurrentGoalsWizardTab" runat="server" EnableViewState="true"/>
    <asp:HiddenField ID="hfIsFinished" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hfErrorMsg" Value="xyz" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hfIndividualClientName" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hfIndividualClientDOB" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hfIndividualClientGender" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hidGetNextButtonClick" runat="server" />
    <asp:HiddenField ID="hidNoteTabPass" runat="server" />
    <asp:HiddenField ID="IsEdit" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hidCurrentAge" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hfAdviceID" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hfInvestementStrategy" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hfCurrentGraphTab" runat="server" EnableViewState="true" />
    <script src="../Scripts/OBP/jquery.tmpl.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/OBP/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript">

        var isWizardPreviousStep = false;

        $(document).ready(function () {

            //Avoid submitting form to server on ENTER key
            $(document).bind('keypress', function (e) {
                if (e.which == 13) //ENTER key
                    return false;
            });

            //When button is clicked to show the calendar, increase the z-index value due to master page z-index
            $('.rcCalPopup').bind('click', function () {
                $('.rcbSlide').css('z-index', 100005);
                $('.RadCalendar').css('z-index', 100005);
                $('.RadCalendarPopup').css('z-index', 100005);
            });

            //Slide Up/Down the graph Input/Output data section
            $('.graphDataSection').bind('click', function () {

                if ($(this).next('div').css('display') == "none") {
                    $(this).next('div').slideDown();
                    $(this).text($(this).text().replace("...", ""));
                    $(this).attr('title', 'Click to hide the section');
                }
                else {
                    $(this).next('div').slideUp();
                    $(this).text($(this).text() + "...");
                    $(this).attr('title', 'Click to show the section');
                }
            });

            //Close the Input graph popup
            $('.imgViewInputGraphDataClose').bind('click', function () {
                $('.overlay').hide();
                $('.divViewInputGraphData').hide();
                $('.lnkViewInputGraphData').show();
                return false;
            });

            //View Graph Data when clicked
            $('.lnkViewInputGraphData').bind('click', function () {
                $('.overlay').show();
                $('.divViewInputGraphData').show();
                $('.lnkViewInputGraphData').hide();
                return false;
            });

            //Close the Input graph popup
            $('.imgViewInputGraphDataClose').bind('click', function () {
                $('.overlay').hide();
                $('.divViewOutputGraphData').hide();
                $('.lnkViewOutputGraphData').show();
                return false;
            });

            //View Graph Data when clicked
            $('.lnkViewOutputGraphData').bind('click', function () {
                $('.overlay').show();
                $('.divViewOutputGraphData').show();
                $('.lnkViewOutputGraphData').hide();
                return false;
            });

            //Set focus on the input on the grid while a new row is added
            if (!($('#<%: hfGridFocusControlID.ClientID %>').val() == null || $('#<%: hfGridFocusControlID.ClientID %>').val() == "" || $('#<%: hfGridFocusControlID.ClientID %>').val() == "0")) {
                $('#' + $('#<%: hfGridFocusControlID.ClientID %>').val()).focus();
                $('#<%: hfGridFocusControlID.ClientID %>').val("");
            }

            $('#<%: btnEditIndividual.ClientID %>').bind('click', function () {
                EditIndividual_Click();
            });

            //if Goals checkboxes are checked, set the color to transparent else light gray-----------------------------
            if ($('#<%: chkWealth.ClientID %>').prop('checked') == true)
                $('#<%: chkWealth.ClientID %>').closest('tr').css('background-color', 'transparent');
            else {
                $('#<%: chkWealth.ClientID %>').closest('tr').css('background-color', 'lightgray');
                $('.clsTrGoalsWealth').hide();
            }

            if ($('#<%: chkIncome.ClientID %>').prop('checked') == true)
                $('#<%: chkIncome.ClientID %>').closest('tr').css('background-color', 'transparent');
            else {
                $('#<%: chkIncome.ClientID %>').closest('tr').css('background-color', 'lightgray');
                $('.clsTrGoalsIncome').hide();
            }

            if ($('#<%: chkLumpSum.ClientID %>').prop('checked') == true)
                $('#<%: chkLumpSum.ClientID %>').closest('tr').css('background-color', 'transparent');
            else {
                $('#<%: chkLumpSum.ClientID %>').closest('tr').css('background-color', 'lightgray');
                $('.clsTrGoalsLumpSum').hide();
            }

            if ($('#<%: chkLiquidAssets.ClientID %>').prop('checked') == true)
                $('#<%: chkLiquidAssets.ClientID %>').closest('tr').css('background-color', 'transparent');
            else {
                $('#<%: chkLiquidAssets.ClientID %>').closest('tr').css('background-color', 'lightgray');
                $('.clsTrGoalsLiquidAssets').hide();
            }

            if ($('#<%: chkBequest.ClientID %>').prop('checked') == true)
                $('#<%: chkBequest.ClientID %>').closest('tr').css('background-color', 'transparent');
            else {
                $('#<%: chkBequest.ClientID %>').closest('tr').css('background-color', 'lightgray');
                $('.clsTrGoalsBequest').hide();
            }
            //----------------------------------------------------------------------------------------------------------------

            //Enable disable the Probabiltiy Tab Graph textbox
            $('#chkShowAllNumTimeSteps').bind('click', function () {
                if ($(this).prop('checked')) {

                    //SetTextBoxStatus($('.txtNumTimeSteps').attr('id'), true);
                    //$('.txtNumTimeSteps').siblings('a.riUp').hide();
                    //$('.txtNumTimeSteps').siblings('a.riDown').hide();

                    SetTextBoxStatus($('.txtNumTimeSteps').attr('id'), false);
                    $('.txtNumTimeSteps').siblings('a.riUp').show();
                    $('.txtNumTimeSteps').siblings('a.riDown').show();
                }
                else {
                    SetTextBoxStatus($('.txtNumTimeSteps').attr('id'), false);
                    $('.txtNumTimeSteps').siblings('a.riUp').show();
                    $('.txtNumTimeSteps').siblings('a.riDown').show();
                }
                ShowProbability(e.currentTarget.RadInputValidationValue);
            });

            //Enable disable the Compare Runs Tab Graph textbox
            $('#chkShowAllNumTimeStepsCompareRuns').bind('click', function () {
                if ($(this).prop('checked')) {

                    //SetTextBoxStatus($('.txtNumTimeStepsCompareRuns').attr('id'), true);
                    //$('.txtNumTimeStepsCompareRuns').siblings('a.riUp').hide();
                    //$('.txtNumTimeStepsCompareRuns').siblings('a.riDown').hide();

                    SetTextBoxStatus($('.txtNumTimeStepsCompareRuns').attr('id'), false);
                    $('.txtNumTimeStepsCompareRuns').siblings('a.riUp').show();
                    $('.txtNumTimeStepsCompareRuns').siblings('a.riDown').show();
                }
                else {
                    SetTextBoxStatus($('.txtNumTimeStepsCompareRuns').attr('id'), false);
                    $('.txtNumTimeStepsCompareRuns').siblings('a.riUp').show();
                    $('.txtNumTimeStepsCompareRuns').siblings('a.riDown').show();
                }
                ShowMultipleProjectionsRun(false, e.currentTarget.RadInputValidationValue);
            });

            //Set the Investment Strategies dropdown
            var InvestmentStrategy = GetElement("<%: hfInvestementStrategy.ClientID %>").value;

            if (InvestmentStrategy != null && InvestmentStrategy != "")
                GetInvestmentStrategyAssetWeightings(InvestmentStrategy);
            else
                GetInvestmentStrategyAssetWeightings(1);

            //Bind the graph probability dropdown
            SetTextBoxStatus($('.txtNumTimeSteps').attr('id'), false);
            $('.txtNumTimeSteps').siblings('a.riUp').show();
            $('.txtNumTimeSteps').siblings('a.riDown').show();
            $('.txtNumTimeSteps').bind('change blur', function (e) {
                ShowProbability(e.currentTarget.RadInputValidationValue);
                return false;
            });

            $('.txtNumTimeSteps').bind('keyup', function (e) {
                if (e.keyCode == 38 || e.keyCode == 40 || e.keyCode == 13) {
                    ShowProbability(e.currentTarget.RadInputValidationValue);
                }
                return false;
            });

            //Bind the graph Compare Runs textbox
            SetTextBoxStatus($('.txtNumTimeStepsCompareRuns').attr('id'), false);
            $('.txtNumTimeStepsCompareRuns').siblings('a.riUp').show();
            $('.txtNumTimeStepsCompareRuns').siblings('a.riDown').show();
            $('.txtNumTimeStepsCompareRuns').bind('change blur', function (e) {
                ShowMultipleProjectionsRun(false, e.currentTarget.RadInputValidationValue);
                return false;
            });

            $('.txtNumTimeStepsCompareRuns').bind('keyup', function (e) {
                if (e.keyCode == 38 || e.keyCode == 40 || e.keyCode == 13) {
                    ShowMultipleProjectionsRun(false, e.currentTarget.RadInputValidationValue);
                }
                return false;
            });

            $('#<%: chkWealth.ClientID %>').bind('click', function () {
                if ($(this).prop('checked')) {
                    $(this).closest('tr').css('background-color', 'transparent');
                    $('tr.clsTrGoalsWealth').show('slow');
                }
                else {
                    $(this).closest('tr').css('background-color', 'lightgray');
                    $('tr.clsTrGoalsWealth').hide('slow');
                }
            });

            $('.txtGoalsIncome_TotalIncomeRequired').bind('blur', function () {
                var totalSalary = $(this).val();
                if (totalSalary == null || totalSalary == "" || parseInt(totalSalary) == 0) {
                    $('#<%: txtPDSalary.ClientID %>').trigger('blur');
                }
            });

            $('#<%: chkIncome.ClientID %>').bind('click', function () {
                if ($(this).prop('checked')) {
                    var totalSalary = $('.txtGoalsIncome_TotalIncomeRequired').val();
                    if (totalSalary == null || totalSalary == "") {
                        $('#<%: txtPDSalary.ClientID %>').trigger('blur');
                    }
                    $(this).closest('tr').css('background-color', 'transparent');
                    $('tr.clsTrGoalsIncome').show('slow');
                }
                else {
                    $(this).closest('tr').css('background-color', 'lightgray');
                    $('tr.clsTrGoalsIncome').hide('slow');
                }
            });

            $('#<%: chkLumpSum.ClientID %>').bind('click', function () {
                if ($(this).prop('checked')) {
                    $(this).closest('tr').css('background-color', 'transparent');
                    $('tr.clsTrGoalsLumpSum').show('slow');
                }
                else {
                    $(this).closest('tr').css('background-color', 'lightgray');
                    $('tr.clsTrGoalsLumpSum').hide('slow');
                }
            });

            $('#<%: chkLiquidAssets.ClientID %>').bind('click', function () {
                if ($(this).prop('checked')) {
                    $(this).closest('tr').css('background-color', 'transparent');
                    $('tr.clsTrGoalsLiquidAssets').show('slow');
                }
                else {
                    $(this).closest('tr').css('background-color', 'lightgray');
                    $('tr.clsTrGoalsLiquidAssets').hide('slow');
                }
            });

            $('#<%: chkBequest.ClientID %>').bind('click', function () {
                if ($(this).prop('checked')) {
                    $(this).closest('tr').css('background-color', 'transparent');
                    $('tr.clsTrGoalsBequest').show('slow');
                }
                else {
                    $(this).closest('tr').css('background-color', 'lightgray');
                    $('tr.clsTrGoalsBequest').hide('slow');
                }
            });

            //On bluring of Salary field, set the amount to 75% of it to Goals --> Income Grid --> amount field if the advice is in a new mode.
            $('#<%: txtPDSalary.ClientID %>').bind('blur', function () {
                var totalSalary = $(this).val();
                var isEditMode = $('#<%: IsEdit.ClientID %>').val();
                if (isEditMode == "0" || isEditMode == "") {
                    if (totalSalary != null && totalSalary != "") {
                        totalSalary = totalSalary.replace(/\,/g, ''); // repalce comma from the amount
                        var id = $('.txtGoalsIncome_TotalIncomeRequired').prop('id');
                        var txtTotalIncomeRequired = window.$find(id);
                        txtTotalIncomeRequired.set_value(parseFloat(totalSalary) * 0.75);
                    }
                }
            });

            //Bind All Grid Edit-Cancel image button for validation---------------------------------------------------
            $('.clsGoalsWealthGridRowUpdateEdit').bind('click', function (e) {

                var gridId = $(this).closest('table').attr('Id');
                var chkSelected = $('#<%: chkWealth.ClientID %>');
                var sectionName = $(chkSelected).siblings('label').first().text();

                if (!ValidateGoalsGridOnAction(gridId, chkSelected, sectionName, 'Update', this)) return false;
                if (!UpdateWealthGridRow(this)) return false;

                return false;
            });

            //Bind All Grid Edit-Cancel image button for validation---------------------------------------------------
            $('.clsGoalsIncomeGridRowUpdateEdit').bind('click', function (e) {

                var gridId = $(this).closest('table').attr('Id');
                var chkSelected = $('#<%: chkIncome.ClientID %>');
                var sectionName = $(chkSelected).siblings('label').first().text();

                if (!ValidateGoalsGridOnAction(gridId, chkSelected, sectionName, 'Update', this)) return false;
                if (!UpdateIncomeGridRow(this)) return false;

                return false;
            });

            //Bind All Grid Edit-Cancel image button for validation---------------------------------------------------
            $('.clsGoalsLumpSumGridRowUpdateEdit').bind('click', function (e) {

                var gridId = $(this).closest('table').attr('Id');
                var chkSelected = $('#<%: chkLumpSum.ClientID %>');
                var sectionName = $(chkSelected).siblings('label').first().text();

                if (!ValidateGoalsGridOnAction(gridId, chkSelected, sectionName, 'Update', this)) return false;
                if (!UpdateLumpSumGridRow(this)) return false;

                return false;
            });

            //Bind All Grid Edit-Cancel image button for validation---------------------------------------------------
            $('.clsGoalsLiquidityGridRowUpdateEdit').bind('click', function (e) {

                var gridId = $(this).closest('table').attr('Id');
                var chkSelected = $('#<%: chkLiquidAssets.ClientID %>');
                var sectionName = $(chkSelected).siblings('label').first().text();

                if (!ValidateGoalsGridOnAction(gridId, chkSelected, sectionName, 'Update', this)) return false;
                if (!UpdateLiquidAssetsGridRow(this)) return false;

                return false;
            });

            //Bind All Grid Edit-Cancel image button for validation---------------------------------------------------
            $('.clsGoalsBequestGridRowUpdateEdit').bind('click', function (e) {

                var gridId = $(this).closest('table').attr('Id');
                var chkSelected = $('#<%: chkBequest.ClientID %>');
                var sectionName = $(chkSelected).siblings('label').first().text();

                if (!ValidateGoalsGridOnAction(gridId, chkSelected, sectionName, 'Update', this)) return false;
                if (!UpdateBequestGridRow(this)) return false;

                return false;
            });


            //Bind All Grid Edit-Cancel image button for validation---------------------------------------------------
            $('.clsGoalsWealthGridRowCancelEdit').on('click', function (e) {
                CancelGoalGridRowEditMode(this, GetGoalDMLObject("WEALTH"));
                return false;
            });

            //Bind All Grid Edit-Cancel image button for validation---------------------------------------------------
            $('.clsGoalsIncomeGridRowCancelEdit').on('click', function (e) {
                CancelGoalGridRowEditMode(this, GetGoalDMLObject("INCOME"));
                return false;
            });

            //Bind All Grid Edit-Cancel image button for validation---------------------------------------------------
            $('.clsGoalsLumpSumGridRowCancelEdit').on('click', function (e) {
                CancelGoalGridRowEditMode(this, GetGoalDMLObject("LUMPSUM"));
                return false;
            });

            //Bind All Grid Edit-Cancel image button for validation---------------------------------------------------
            $('.clsGoalsLiquidityGridRowCancelEdit').on('click', function (e) {
                CancelGoalGridRowEditMode(this, GetGoalDMLObject("LIQUIDITY"));
                return false;
            });

            //Bind All Grid Edit-Cancel image button for validation---------------------------------------------------
            $('.clsGoalsBequestGridRowCancelEdit').on('click', function (e) {
                CancelGoalGridRowEditMode(this, GetGoalDMLObject("BEQUEST"));
                return false;
            });

            //Bind All Grid Edit image button for validation---------------------------------------------------
            $('.clsGoalsWealthGridRowEdit').on('click', function (e) {
                ShowGoalGridRowInEditMode(this, GetGoalDMLObject('WEALTH'));
                return false;
            });

            //Bind All Grid Edit image button for validation---------------------------------------------------
            $('.clsGoalsIncomeGridRowEdit').on('click', function (e) {
                ShowGoalGridRowInEditMode(this, GetGoalDMLObject('INCOME'));
                return false;
            });


            //Bind All Grid Edit image button for validation---------------------------------------------------
            $('.clsGoalsLumpSumGridRowEdit').on('click', function (e) {
                ShowGoalGridRowInEditMode(this, GetGoalDMLObject('LUMPSUM'));
                return false;
            });

            //Bind All Grid Edit image button for validation---------------------------------------------------
            $('.clsGoalsLiquidityGridRowEdit').on('click', function (e) {
                ShowGoalGridRowInEditMode(this, GetGoalDMLObject('LIQUIDITY'));
                return false;
            });

            //Bind All Grid Edit image button for validation---------------------------------------------------
            $('.clsGoalsBequestGridRowEdit').on('click', function (e) {
                ShowGoalGridRowInEditMode(this, GetGoalDMLObject('BEQUEST'));
                return false;
            });

            //Bind All Grid Add image button for validation---------------------------------------------------
            $('.clsGoalsWealthGridRowAdd').bind('click', function (e) {
                var source = e.target || e.srcElement;
                if (source.originalEvent) {
                    if (source.originalEvent.clientX == 0 && source.originalEvent.clientY == 0) return false;
                }
                var gridId = $(this).closest('table').attr('Id');
                var chkSelected = $('#<%: chkWealth.ClientID %>');
                var sectionName = $(chkSelected).siblings('label').first().text();

                if (!ValidateGoalsGridOnAction(gridId, chkSelected, sectionName, 'Add', this)) return false;
                if (!AddWealthGridRow(this)) return false;

                return false;
            });

            $('.clsGoalsIncomeGridRowAdd').bind('click', function (e) {
                var source = e.target || e.srcElement;
                if (source.originalEvent) {
                    if (source.originalEvent.clientX == 0 && source.originalEvent.clientY == 0) return false;
                }

                var gridId = $(this).closest('table').attr('Id');
                var chkSelected = $('#<%: chkIncome.ClientID %>');
                var sectionName = $(chkSelected).siblings('label').first().text();

                if (!ValidateGoalsGridOnAction(gridId, chkSelected, sectionName, 'Add', this)) return false;
                if (!AddIncomeGridRow(this)) return false;
            });

            $('.clsGoalsLumpSumGridRowAdd').bind('click', function (e) {
                var source = e.target || e.srcElement;
                if (source.originalEvent) {
                    if (source.originalEvent.clientX == 0 && source.originalEvent.clientY == 0) return false;
                }

                var gridId = $(this).closest('table').attr('Id');
                var chkSelected = $('#<%: chkLumpSum.ClientID %>');
                var sectionName = $(chkSelected).siblings('label').first().text();

                if (!ValidateGoalsGridOnAction(gridId, chkSelected, sectionName, 'Add', this)) return false;
                if (!AddLumpSumGridRow(this)) return false;
            });

            $('.clsGoalsLiquidAssetsGridRowAdd').bind('click', function (e) {
                var source = e.target || e.srcElement;
                if (source.originalEvent) {
                    if (source.originalEvent.clientX == 0 && source.originalEvent.clientY == 0) return false;
                }

                var gridId = $(this).closest('table').attr('Id');
                var chkSelected = $('#<%: chkLiquidAssets.ClientID %>');
                var sectionName = $(chkSelected).siblings('label').first().text();

                if (!ValidateGoalsGridOnAction(gridId, chkSelected, sectionName, 'Add', this)) return false;
                if (!AddLiquidAssetsGridRow(this)) return false;
            });

            $('.clsGoalsBequestGridRowAdd').bind('click', function (e) {
                var source = e.target || e.srcElement;
                if (source.originalEvent) {
                    if (source.originalEvent.clientX == 0 && source.originalEvent.clientY == 0) return false;
                }

                var gridId = $(this).closest('table').attr('Id');
                var chkSelected = $('#<%: chkBequest.ClientID %>');
                var sectionName = $(chkSelected).siblings('label').first().text();

                if (!ValidateGoalsGridOnAction(gridId, chkSelected, sectionName, 'Add', this)) return false;
                if (!AddBequestGridRow(this)) return false;
            });


            //Bind All Grid Remove image button for deletion of row---------------------------------------------------
            $('.clsGoalsWealthGridRowRemove').bind('click', function () {
                if (!RemoveWealthGridRow(this)) return false;
                return false;
            });

            $('.clsGoalsIncomeGridRowRemove').bind('click', function () {
                if (!RemoveIncomeGridRow(this)) return false;
                return false;
            });

            $('.clsGoalsLumpSumGridRowRemove').bind('click', function () {
                if (!RemoveLumpSumGridRow(this)) return false;
                return false;
            });

            $('.clsGoalsLiquidAssetsGridRowRemove').bind('click', function () {
                if (!RemoveLiquidAssetsGridRow(this)) return false;
                return false;
            });

            $('.clsGoalsBequestGridRowRemove').bind('click', function () {
                if (!RemoveBequestGridRow(this)) return false;
                return false;
            });
            //-----------------------------------------------------------------------------------------------

            $("#profile").height(600);
            $("#results").height(600);
            $(".buttonFinish").hide();

            var hfIsFinished = GetElement("<%: hfIsFinished.ClientID %>").value;
            var varErrorMsg = GetElement("<%: hfErrorMsg.ClientID %>").value;
            var hIsEdit = GetElement("<%: IsEdit.ClientID %>").value;
            if (hIsEdit == '1') {

                var currentStep = $('#<%: hfCurrentGoalsWizardTab.ClientID %>').val();
                if (currentStep != undefined && currentStep != '') {
                    currentStep = parseInt(currentStep) - 1;
                }

                $('#wizard').smartWizard({
                    onFinish: onFinishCallback,
                    onLeaveStep: leaveAStepCallback,
                    onBeforePrevious: onBeforePrevious,
                    labelFinish: 'RUN',
                    selected: currentStep,
                    enableAllSteps: true,
                    enableFinishButton: true
                });

                var strMinValue = GetElement("<%:hidCurrentAge.ClientID %>").value;
                var strMaxValue = GetTextBoxValue("<%:txtPDPlanAge.ClientID %>");
                if (strMinValue != "" && strMaxValue != "") {
                    SetMinMaxValue(strMinValue, strMaxValue);
                    GetElement("<%: litMinAge.ClientID %>").innerHTML = strMinValue;
                    GetElement("<%: litMaxAge.ClientID %>").innerHTML = strMaxValue;
                    //Goal
                    GetElement("<%: lblMinAgeGoal.ClientID %>").innerHTML = strMinValue;
                    GetElement("<%: lblMaxAgeGoal.ClientID %>").innerHTML = strMaxValue;
                }

                //if the run button is clicked, it will be 1
                if (hfIsFinished == '1') {

                    $('.lnkViewInputGraphData').show();
                    $('.lnkViewOutputGraphData').show();
                }
                else {
                    $('.lnkViewInputGraphData').hide();
                    $('.lnkViewOutputGraphData').hide();
                }
            }
            else if (hIsEdit == '0') {

                $('.lnkViewInputGraphData').hide();
                $('.lnkViewOutputGraphData').hide();

                if ($('#<%: hfCurrentGoalsWizardTab.ClientID %>').val() == "2") {
                    $('#wizard').smartWizard({
                        onFinish: onFinishCallback,
                        onLeaveStep: leaveAStepCallback,
                        onBeforePrevious: onBeforePrevious,
                        labelFinish: 'RUN',
                        selected: 1,
                        enableAllSteps: true,
                        enableFinishButton: false
                    });
                }
                else if (hfIsFinished == '1') {

                    //Wizard
                    $('#wizard').smartWizard({
                        onFinish: onFinishCallback,
                        onLeaveStep: leaveAStepCallback,
                        onBeforePrevious: onBeforePrevious,
                        labelFinish: 'RUN',
                        selected: 2,
                        enableAllSteps: true,
                        enableFinishButton: true
                    });

                    $('.lnkViewInputGraphData').show();
                    $('.lnkViewOutputGraphData').show();
                }
                else if ($('#<%: hfCurrentGoalsWizardTab.ClientID %>').val() == "3") {
                    $('#wizard').smartWizard({
                        onFinish: onFinishCallback,
                        onLeaveStep: leaveAStepCallback,
                        onBeforePrevious: onBeforePrevious,
                        labelFinish: 'RUN',
                        selected: 3,
                        enableAllSteps: true,
                        enableFinishButton: false
                    });
                }
                else {

                    $('#wizard').smartWizard({
                        onFinish: onFinishCallback,
                        onLeaveStep: leaveAStepCallback,
                        onBeforePrevious: onBeforePrevious,
                        labelFinish: 'RUN'
                    });

                }
            }

            EnableDisableAssest();

            if ($("#<%: hfIsRefreshCurrentAssetsPostback.ClientID %>").val() == "1") {
                OnRefreshCurrentAsset();
                $("#<%: hfIsRefreshCurrentAssetsPostback.ClientID %>").val("0");
            }

            if (varErrorMsg != '') {
                document.getElementById("<%: hfErrorMsg.ClientID %>").value = '';
            }
            else if (hfIsFinished != '') {
                $("#profile").show();
                $("#results").show();
                document.getElementById("<%: hfIsFinished.ClientID %>").value = '';
            }

            //Adjust the position of Wizard buttons by moving them from bottom to top.-------------------------------------
            var buttonSection = $('.actionBar').clone(true);
            $('.actionBar').addClass('buttonCloned');
            $('.stepContainer').before(buttonSection);
            $('.buttonCloned').remove();
            //------------------------------------------------------------------------------------------------------------

            //Show Multiple Projection Run Result in tabular format-------------------------------------------------------
            ShowMultipleProjectionsRun(false, null);
            //------------------------------------------------------------------------------------------------------------

            $("#profile").show();
            $("#results").show();
        });

        $(function () {

            //if the advice is in new mode and has not yet run/finish the process, hide the graph wizard, else show it.
            if ($('#<%:hfIsGraphWizardDisabled.ClientID %>').val() == "1") {
                $("#tabs").tabs({ disabled: [0, 1, 2, 3, 4] });
            } else {
                $("#tabs").tabs();
            }

            //handling wizard and graph styling programmatically-------------------------------------------------------
            $('#results #tabs ul[role="tablist"]').removeAttr('class');
            $('#results #tabs ul[role="tablist"] li').css('border-top-left-radius', '0');
            $('#results #tabs ul[role="tablist"] li').css('border-top-right-radius', '0');
            //---------------------------------------------------------------------------------------------------------

            var shouldShowGraph = '<%: Session["SHOW_GRAPHS"] == null ? "False" : Convert.ToBoolean(Session["SHOW_GRAPHS"]).ToString() %>';
            var isProbabilityGraphInError = '<%: Session["Probability"] %>';
            if (isProbabilityGraphInError == "Error")
                $('#<%: p_cmbage.ClientID %>').hide();
            else
                $('#<%: p_cmbage.ClientID %>').show();

        });

        function GetMultipleProjectionsRunData(localData) {

            var projections = null;
            if (localData)
                projections = GetLocalStorage('CompareRuns');
            else {
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "Advices.aspx/GetAllGoalsProjectionAsJSON",
                    contentType: "application/json",
                    dataType: "json",
                    data: "{'adviceID' : '" + GetElement('<%:hfAdviceID.ClientID %>').value + "'}",
                    success: function (graphData) {
                        if (graphData.d !== 'Error') {
                            projections = JSON.parse(graphData.d);
                            RemoveLocalStorage('CompareRuns');
                            StoreLocally('CompareRuns', projections);
                        }
                    }
                });
            }

            return projections;
        }

        function ShowMultipleProjectionsRun(localData, step) {

            var projections = GetMultipleProjectionsRunData(localData);
            if (projections == null) return false;

            var data = null;
            if (step == null)
                step = $(".txtNumTimeStepsCompareRuns").val();
            var showAll = $("#chkShowAllNumTimeStepsCompareRuns").prop('checked');
            if (showAll == true)
                step = -1;

            if (step == null || step == "" || parseInt(step) == 0)
                step = -1;

            var wealthData = null;
            var incomeData = null;
            var lumpSumData = null;
            var liquidityData = null;
            var bequestData = null;

            var backgroundColor = null;
            var wealthPercent = null;
            var incomePercent = null;
            var lumpSumPercent = null;
            var liquidityPercent = null;
            var bequestPercent = null;

            console.log("ShowMultipleProjectionsRun" + step);
            projections = projections.projectionsRuns;
            $('.tblProjectionsComparison tbody').empty();
            
            var combo = $find("<%: rcProjectionRuns.ClientID %>");
            
            for (var p = 0; p < combo.get_items().get_count(); p++) {

                wealthData = null;
                incomeData = null;
                lumpSumData = null;
                liquidityData = null;
                bequestData = null;
                backgroundColor = GetColor(0);
                wealthPercent = '<span style="display:inline-block; width:100%; background:' + backgroundColor + '">0%</span>';
                incomePercent = '<span style="display:inline-block; width:100%; background:' + backgroundColor + '">0%</span>';
                lumpSumPercent = '<span style="display:inline-block; width:100%; background:' + backgroundColor + '">0%</span>';
                liquidityPercent = '<span style="display:inline-block; width:100%; background:' + backgroundColor + '">0%</span>';
                bequestPercent = '<span style="display:inline-block; width:100%; background:' + backgroundColor + '">0%</span>';

                data = projections[p];
                for (var goal = 0; goal < data.d.length; goal++) {

                    if (data.d[goal].GoalType.toUpperCase() == "WEALTH")
                        wealthData = data.d[goal].d;
                    if (data.d[goal].GoalType.toUpperCase() == "INCOME")
                        incomeData = data.d[goal].d;
                    if (data.d[goal].GoalType.toUpperCase() == "LUMPSUM")
                        lumpSumData = data.d[goal].d;
                    if (data.d[goal].GoalType.toUpperCase() == "LIQUIDITY")
                        liquidityData = data.d[goal].d;
                    if (data.d[goal].GoalType.toUpperCase() == "BEQUEST")
                        bequestData = data.d[goal].d;
                }

                data.TargetStep = step;
                if (wealthData != null) {
                    if (step > 0) {
                        var wealthItem = $.grep(wealthData, function (d) { return parseInt(d.Timestep) == parseInt(data.TargetStep) || (parseInt(d.Timestep) + parseInt(data.CurrentAge)) == parseInt(data.TargetStep); });
                        if (wealthItem != null && wealthItem.length > 0) {
                            backgroundColor = GetColor(wealthItem[0].Value);
                            wealthPercent = "<span style='display:inline-block; width:100%; background:" + backgroundColor + "'>" + Math.round((wealthItem[0].Value * 100)) + '%' + "</span>";
                        }
                    }
                    else {
                        backgroundColor = GetColor(wealthData[wealthData.length - 1].Value);
                        wealthPercent = "<span style='display:inline-block; width:100%; background:" + backgroundColor + "'>" + Math.round((wealthData[wealthData.length - 1].Value * 100)) + '%' + "</span>";
                    }
                }

                if (incomeData != null) {
                    if (step > 0) {
                        var incomeItem = $.grep(incomeData, function (d) { return parseInt(d.Timestep) == parseInt(data.TargetStep) || (parseInt(d.Timestep) + parseInt(data.CurrentAge)) == parseInt(data.TargetStep); });
                        if (incomeItem != null && incomeItem.length > 0) {
                            backgroundColor = GetColor(incomeItem[0].Value);
                            incomePercent = "<span style='display:inline-block; width:100%; background:" + backgroundColor + "'>" + Math.round((incomeItem[0].Value * 100)) + '%' + "</span>";
                        }
                    }
                    else {
                        backgroundColor = GetColor(incomeData[incomeData.length - 1].Value);
                        incomePercent = "<span style='display:inline-block; width:100%; background:" + backgroundColor + "'>" + Math.round((incomeData[incomeData.length - 1].Value * 100)) + '%' + "</span>";
                    }
                }

                if (lumpSumData != null) {
                    if (step > 0) {
                        var lumpSumItem = $.grep(lumpSumData, function (d) { return parseInt(d.Timestep) == parseInt(data.TargetStep) || (parseInt(d.Timestep) + parseInt(data.CurrentAge)) == parseInt(data.TargetStep); });
                        if (lumpSumItem != null && lumpSumItem.length > 0) {
                            backgroundColor = GetColor(lumpSumItem[0].Value);
                            lumpSumPercent = "<span style='display:inline-block; width:100%; background:" + backgroundColor + "'>" + Math.round((lumpSumItem[0].Value * 100)) + '%' + "</span>";
                        }
                    }
                    else {
                        backgroundColor = GetColor(lumpSumData[lumpSumData.length - 1].Value);
                        lumpSumPercent = "<span style='display:inline-block; width:100%; background:" + backgroundColor + "'>" + Math.round((lumpSumData[lumpSumData.length - 1].Value * 100)) + '%' + "</span>";
                    }
                }

                if (liquidityData != null) {
                    if (step > 0) {
                        var liquidityItem = $.grep(liquidityData, function (d) { return parseInt(d.Timestep) == parseInt(data.TargetStep) || (parseInt(d.Timestep) + parseInt(data.CurrentAge)) == parseInt(data.TargetStep); });
                        if (liquidityItem != null && liquidityItem.length > 0) {
                            backgroundColor = GetColor(liquidityItem[0].Value);
                            liquidityPercent = "<span style='display:inline-block; width:100%; background:" + backgroundColor + "'>" + Math.round((liquidityItem[0].Value * 100)) + '%' + "</span>";
                        }
                    }
                    else {
                        backgroundColor = GetColor(liquidityData[liquidityData.length - 1].Value);
                        liquidityPercent = "<span style='display:inline-block; width:100%; background:" + backgroundColor + "'>" + Math.round((liquidityData[liquidityData.length - 1].Value * 100)) + '%' + "</span>";
                    }
                }

                if (bequestData != null) {
                    if (step > 0) {
                        var bequestItem = $.grep(bequestData, function (d) { return parseInt(d.Timestep) == parseInt(data.TargetStep) || (parseInt(d.Timestep) + parseInt(data.CurrentAge)) == parseInt(data.TargetStep); });
                        if (bequestItem != null && bequestItem.length > 0) {
                            backgroundColor = GetColor(bequestItem[0].Value);
                            bequestPercent = "<span style='display:inline-block; width:100%; background:" + backgroundColor + "'>" + Math.round((bequestItem[0].Value * 100)) + '%' + "</span>";
                        }
                    }
                    else {
                        backgroundColor = GetColor(bequestData[bequestData.length - 1].Value);
                        bequestPercent = "<span style='display:inline-block; width:100%; background:" + backgroundColor + "'>" + Math.round((bequestData[bequestData.length - 1].Value * 100)) + '%' + "</span>";
                    }
                }

                var rowTemplate = "<tr><td>" + (p + 1) + "</td><td>" + wealthPercent + "</td><td>" + incomePercent + "</td><td>" + lumpSumPercent + "</td><td>" + liquidityPercent + "</td><td>" + bequestPercent + "</td></tr>";
                $('.tblProjectionsComparison tbody').append(rowTemplate);
            }

            return false;
        }

        function EditIndividual_Click() {
            GetElement('<%: divOver.ClientID %>').ShowElementAsBlock();
            GetElement('<%: divIndividualModal.ClientID %>').ShowElementAsBlock();
        }

        function btnCancel_OnClick() {
            GetElement('<%: divOver.ClientID %>').HideElement();
            GetElement('divAdviceName').HideElement();
        }

        function SaveAdvice() {
            GetElement('<%: divOver.ClientID %>').ShowElementAsBlock();
            GetElement('divAdviceName').ShowElementAsBlock();
        }

        function OnDateSelected() {
            var getage = getAge(GetTextBoxValue("<%: txtPDDOB.ClientID %>"));
            GetElement("<%: litGetDate.ClientID %>").innerHTML = " Current Age:" + getage;
        }

        function onBeforePrevious(obj, stepIndex) {

            switch (obj) {
                case 0:
                    $('.buttonPrevious').addClass('buttonDisabled');
                    break;
                case 3:
                    $('.buttonNext').removeClass('buttonDisabled');
                    break;
            }

            isWizardPreviousStep = true;
            return isWizardPreviousStep;
        }

        function leaveAStepCallback(obj, context) {

            if (isWizardPreviousStep == true) {
                isWizardPreviousStep = false;
                return true;
            }

            var stepnumber = obj.attr('rel'); // get the current step number
            var isStepValid = true;

            //here we get the step where user present and validate that step
            var strMinValue = GetElement("<%:hidCurrentAge.ClientID %>").value;
            var strMaxValue = GetElement("<%:txtPDPlanAge.ClientID %>").value;
            if (strMinValue != "" && strMaxValue != "") {

                SetMinMaxValue(strMinValue, strMaxValue);
                GetElement("<%: litMinAge.ClientID %>").innerHTML = strMinValue;
                GetElement("<%: litMaxAge.ClientID %>").innerHTML = strMaxValue;
                //goal
                GetElement("<%: lblMinAgeGoal.ClientID %>").innerHTML = strMinValue;
                GetElement("<%: lblMaxAgeGoal.ClientID %>").innerHTML = strMaxValue;
            }

            if (GetElement("<%: hidGetNextButtonClick.ClientID %>").value == "") {
                toggleStatusDisableControl();
                GetElement("<%: hidGetNextButtonClick.ClientID %>").value = "1";
            }

            switch (stepnumber) {
                case "1":
                    isStepValid = ValidateProfileTab();
                    GetElement("<%: hidGetNextButtonClick.ClientID %>").value = "1";

                    if (isStepValid)
                        $('.buttonPrevious').removeClass('buttonDisabled');
                    break;
                case "2":
                    isStepValid = ValidateAssestTab();
                    GetElement("<%: hidGetNextButtonClick.ClientID %>").value = "1";
                    break;
                case "3":
                    isStepValid = ValidateActionTab();
                    GetElement("<%: hidGetNextButtonClick.ClientID %>").value = "1";
                    if (isStepValid)
                        $('.buttonFinish').removeClass('buttonDisabled');
                    break;
                case "4":
                    isStepValid = ValidateGoalTab(true);
                    GetElement("<%: hidGetNextButtonClick.ClientID %>").value = "1";
                    if (isStepValid)
                        $('.buttonNext').addClass('buttonDisabled');
                    break;
                default:
            }

            return isStepValid;
        }

        function getAge(dateString) {
            var today = new Date();
            var birthDate = new Date(dateString);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        }

        function ValidateProfileTab() {
            //Here we Validate the  Profile Tab
            var isStepValid = true;
            if ($("#<%: txtPDRetirementAge.ClientID %>").val() == null || $("#<%: txtPDRetirementAge.ClientID %>").val() == "" || parseInt($("#<%: txtPDRetirementAge.ClientID %>").val()) == 0) {
                alert('Invalid Retirement Age value.');
                $("#<%: txtPDRetirementAge.ClientID %>").focus();
                return false;
            }

            if ($("#<%: txtPDSalary.ClientID %>").val() == null || $("#<%: txtPDSalary.ClientID %>").val() == "" || parseInt($("#<%: txtPDSalary.ClientID %>").val()) == 0) {
                alert('Invalid Salary value.');
                $("#<%: txtPDSalary.ClientID %>").focus();
                return false;
            }

            if ($("#<%: txtPDPlanAge.ClientID %>").val() == null || $("#<%: txtPDPlanAge.ClientID %>").val() == "" || parseInt($("#<%: txtPDPlanAge.ClientID %>").val()) == 0) {
                alert('Invalid Salary value.');
                $("#<%: txtPDPlanAge.ClientID %>").focus();
                return false;
            }

            if (GetTextBoxValue("<%: txtPDName.ClientID %>") != "" && GetTextBoxValue("<%: txtPDDOB.ClientID %>") != "" && GetTextBoxValue("<%: txtPDRetirementAge.ClientID %>") != "" && GetTextBoxValue("<%: txtPDSalary.ClientID %>") != "" && GetTextBoxValue("<%: txtPDPlanAge.ClientID %>") != "") {
                var getage = getAge(GetTextBoxValue("<%: txtPDDOB.ClientID %>"));
                var checkPlane = GetTextBoxValue("<%: txtPDPlanAge.ClientID %>");
                var checkValidate = checkPlane - getage;
                if (checkValidate > 0) {
                    isStepValid = true;
                }
                else {
                    isStepValid = false;
                    alert("Plan Age should be greater than Current Age.");
                }
            }
            else {
                isStepValid = false;
            }
            return isStepValid;
        }

        function ValidateProfileTabFinish() {
            //Here we Validate the  Profile Tab
            var isStepValid = true;
            if (GetTextBoxValue("<%: txtPDName.ClientID %>") != "" && GetTextBoxValue("<%: txtPDDOB.ClientID %>") != "" && GetTextBoxValue("<%: txtPDRetirementAge.ClientID %>") != "" && GetTextBoxValue("<%: txtPDSalary.ClientID %>") != "" && GetTextBoxValue("<%: txtPDPlanAge.ClientID %>") != "") {
                var getage = getAge(GetTextBoxValue("<%: txtPDDOB.ClientID %>"));
                var checkPlane = GetTextBoxValue("<%: txtPDPlanAge.ClientID %>");
                var checkValidate = checkPlane - getage;
                if (checkValidate > 0) {
                    isStepValid = true;
                }
                else {
                    isStepValid = false;
                }
            }
            else {
                isStepValid = false;
            }
            return isStepValid;
        }


        function ValidateAssestTab() {
            //Here we validate the Assest Tab 0.00
            var count = 0;
            if (GetElement("<%: chkSuperannuation.ClientID %>").checked) {
                if ((GetTextBoxValue("<%: txtSEquity.ClientID %>") != "" && GetTextBoxValue("<%: txtSEquity.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtSFixedInterest.ClientID %>") != "" && GetTextBoxValue("<%: txtSFixedInterest.ClientID %>") != "0.00") ||
                    (GetTextBoxValue("<%: txtSInternationalEquity.ClientID %>") != "" && GetTextBoxValue("<%: txtSInternationalEquity.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtSCash.ClientID  %>") != "" && GetTextBoxValue("<%: txtSCash.ClientID  %>") != "0.00") ||
                    (GetTextBoxValue("<%: txtSProperty.ClientID %>") != "" && GetTextBoxValue("<%: txtSProperty.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtSGold.ClientID %>") != "" && GetTextBoxValue("<%: txtSGold.ClientID %>") != "0.00")) {
                    count = count + 1;
                }
                else {
                    alert("In Superannuation, At least one field should be non empty.");
                    count = 0;
                    return false;
                }
            }
            else if (GetElement("<%: chkPension.ClientID %>").checked) {
                if ((GetTextBoxValue("<%: txtPEquity.ClientID %>") != "" && GetTextBoxValue("<%: txtPEquity.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtPFixedInterest.ClientID %>") != "" && GetTextBoxValue("<%: txtPFixedInterest.ClientID %>") != "0.00") ||
                    (GetTextBoxValue("<%: txtPInternationalEquity.ClientID %>") != "" && GetTextBoxValue("<%: txtPInternationalEquity.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtPCash.ClientID %>") != "" && GetTextBoxValue("<%: txtPCash.ClientID %>") != "0.00") ||
                    (GetTextBoxValue("<%: txtPProperty.ClientID %>") != "" && GetTextBoxValue("<%: txtPProperty.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtPGold.ClientID %>") != "" && GetTextBoxValue("<%: txtPGold.ClientID %>") != "0.00")) {
                    count = count + 1;
                }
                else {
                    alert("In Pension, At least one field should be non empty.");
                    count = 0;
                    return false;
                }
            }
            else if (GetElement("<%: chkNonSuperInvestments.ClientID %>").checked) {
                if ((GetTextBoxValue("<%: txtNEquity.ClientID %>") != "" && GetTextBoxValue("<%: txtNEquity.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtNFixedInterest.ClientID %>") != "" && GetTextBoxValue("<%: txtNFixedInterest.ClientID %>") != "0.00") ||
                    (GetTextBoxValue("<%: txtNInternationalEquity.ClientID %>") != "" && GetTextBoxValue("<%: txtNInternationalEquity.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtNCash.ClientID %>") != "" && GetTextBoxValue("<%: txtNCash.ClientID %>") != "0.00") ||
                    (GetTextBoxValue("<%: txtNProperty.ClientID %>") != "" && GetTextBoxValue("<%: txtNProperty.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtNGold.ClientID %>") != "" && GetTextBoxValue("<%: txtNGold.ClientID %>") != "0.00")) {
                    count = count + 1;
                }
                else {
                    alert("In Non Super Investments, At least one field should be non empty.");
                    count = 0;
                    return false;
                }
            }
            else {
                alert("You need to mention some asset value.");
                count = 0;
            }
            if (count == 0) {
                return false;
            }
            else {
                return true;
            }
        }


        function ValidateAssestTabFinish() {
            //Here we validate the Assest Tab 0.00
            var count = 0;
            if (GetElement("<%: chkSuperannuation.ClientID %>").checked) {
                if ((GetTextBoxValue("<%: txtSEquity.ClientID %>") != "" && GetTextBoxValue("<%: txtSEquity.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtSFixedInterest.ClientID %>") != "" && GetTextBoxValue("<%: txtSFixedInterest.ClientID %>") != "0.00") ||
                    (GetTextBoxValue("<%: txtSInternationalEquity.ClientID %>") != "" && GetTextBoxValue("<%: txtSInternationalEquity.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtSCash.ClientID  %>") != "" && GetTextBoxValue("<%: txtSCash.ClientID  %>") != "0.00") ||
                    (GetTextBoxValue("<%: txtSProperty.ClientID %>") != "" && GetTextBoxValue("<%: txtSProperty.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtSGold.ClientID %>") != "" && GetTextBoxValue("<%: txtSGold.ClientID %>") != "0.00")) {
                    count = count + 1;
                }
                else {
                    count = 0;
                    return false;
                }
            }
            else if (GetElement("<%: chkPension.ClientID %>").checked) {
                if ((GetTextBoxValue("<%: txtPEquity.ClientID %>") != "" && GetTextBoxValue("<%: txtPEquity.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtPFixedInterest.ClientID %>") != "" && GetTextBoxValue("<%: txtPFixedInterest.ClientID %>") != "0.00") ||
                    (GetTextBoxValue("<%: txtPInternationalEquity.ClientID %>") != "" && GetTextBoxValue("<%: txtPInternationalEquity.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtPCash.ClientID %>") != "" && GetTextBoxValue("<%: txtPCash.ClientID %>") != "0.00") ||
                    (GetTextBoxValue("<%: txtPProperty.ClientID %>") != "" && GetTextBoxValue("<%: txtPProperty.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtPGold.ClientID %>") != "" && GetTextBoxValue("<%: txtPGold.ClientID %>") != "0.00")) {
                    count = count + 1;
                }
                else {
                    count = 0;
                    return false;
                }
            }
            else if (GetElement("<%: chkNonSuperInvestments.ClientID %>").checked) {
                if ((GetTextBoxValue("<%: txtNEquity.ClientID %>") != "" && GetTextBoxValue("<%: txtNEquity.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtNFixedInterest.ClientID %>") != "" && GetTextBoxValue("<%: txtNFixedInterest.ClientID %>") != "0.00") ||
                    (GetTextBoxValue("<%: txtNInternationalEquity.ClientID %>") != "" && GetTextBoxValue("<%: txtNInternationalEquity.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtNCash.ClientID %>") != "" && GetTextBoxValue("<%: txtNCash.ClientID %>") != "0.00") ||
                    (GetTextBoxValue("<%: txtNProperty.ClientID %>") != "" && GetTextBoxValue("<%: txtNProperty.ClientID %>") != "0.00") || (GetTextBoxValue("<%: txtNGold.ClientID %>") != "" && GetTextBoxValue("<%: txtNGold.ClientID %>") != "0.00")) {
                    count = count + 1;
                }
                else {
                    count = 0;
                    return false;
                }
            }
            else {
                count = 0;
            }
            if (count == 0) {
                return false;
            }
            else {
                return true;
            }
        }

        function ValidateActionTab() {
            //Here we validate the Action Tab
            var msgText = "At least one field should be non empty";
            var count = 0;
            if (GetElement("<%: chkSuperGuaranteeContributions.ClientID %>").checked) {
                if (GetTextBoxValue("<%: txtSGCPercentSalary.ClientID %>") != "" && GetTextBoxValue("<%: txtSGCPercentSalary.ClientID %>") != "0") {
                    count = count + 1;
                }
                else {
                    count = 0;
                    alert(msgText);
                    return false;
                }
            }

            if (GetElement("<%: chkSuperSalarySacrificeContributions.ClientID %>").checked) {
                if ((GetTextBoxValue("<%: txtSSSCPercentSalary.ClientID %>") != "" && GetTextBoxValue("<%: txtSSSCPercentSalary.ClientID %>") != "0") ||
                    (GetTextBoxValue("<%: txtSSSCAmount.ClientID %>") != "" && GetTextBoxValue("<%: txtSSSCAmount.ClientID %>") != "0")) {
                    count = count + 1;

                    //here we check Start age End AGE
                    if (GetTextBoxValue("<%: txtSSSCStartAge.ClientID %>") != "" && GetTextBoxValue("<%: txtSSSCStartAge.ClientID %>") != "0" &&
                        GetTextBoxValue("<%: txtSSSCStopAge.ClientID %>") != "" && GetTextBoxValue("<%: txtSSSCStopAge.ClientID %>") != "0") {

                        //here we compare two ages Start Age and End age
                        var x = GetTextBoxValue("<%: txtSSSCStartAge.ClientID %>");
                        var y = GetTextBoxValue("<%: txtSSSCStopAge.ClientID %>");
                        var z = GetElement("<%: hidCurrentAge.ClientID %>").value;
                        var planeAge = GetTextBoxValue("<%: txtPDPlanAge.ClientID %>");

                        if (parseInt(x, 10) <= parseInt(y, 10)) {
                            count = count + 1;
                        }
                        else {
                            alert("In Super Salary Sacrifice Contributions, Start Age should be earlier or equal than End Age.");
                            count = 0;
                            return false;
                        }

                        if (parseInt(x, 10) < parseInt(z, 10) || parseInt(x, 10) > parseInt(planeAge)) {
                            alert("In Super Salary Sacrifice Contributions, Start Age should lies b/w Current Age and Plan Age.");
                            count = 0;
                            return false;
                        }
                        if (parseInt(y, 10) > parseInt(planeAge) || parseInt(y, 10) < parseInt(z)) {
                            alert("In Super Salary Sacrifice Contributions, End Age should lies b/w Current Age and Plan Age.");
                            count = 0;
                            return false;
                        }
                    }
                    else {
                        if (GetTextBoxValue("<%: txtSSSCStartAge.ClientID %>") == "") {
                            alert("In Super Salary Sacrifice Contributions, Enter Start Age.");
                        }
                        if (GetTextBoxValue("<%: txtSSSCStopAge.ClientID %>") == "") {
                            alert("In Super Salary Sacrifice Contributions, Enter End Age.");
                        }
                        count = 0;
                        return false;
                    }
                }
                else {

                    if (GetTextBoxValue("<%: txtSSSCAmount.ClientID %>") == "") {
                        alert("In Super Salary Sacrifice Contributions, Enter Amount.");
                    }
                    count = 0;
                    return false;
                }
            }

            if (GetElement("<%: chkSuperAfterTaxContributions.ClientID %>").checked) {
                if (GetTextBoxValue("<%: txtSATCAmount.ClientID  %>") != "" && GetTextBoxValue("<%: txtSATCAmount.ClientID  %>") != "0" &&
                    GetTextBoxValue("<%: txtSATCStartAge.ClientID  %>") != "" && GetTextBoxValue("<%: txtSATCStartAge.ClientID  %>") != "0" &&
                    GetTextBoxValue("<%: txtSATCStopAge.ClientID  %>") != "" && GetTextBoxValue("<%: txtSATCStopAge.ClientID  %>") != "0") {

                    var x2 = GetTextBoxValue("<%: txtSATCStartAge.ClientID  %>");
                    var y2 = GetTextBoxValue("<%: txtSATCStopAge.ClientID  %>");
                    var z2 = GetElement("<%: hidCurrentAge.ClientID %>").value;
                    var planeAge2 = GetTextBoxValue("<%: txtPDPlanAge.ClientID %>");

                    if (parseInt(x2, 10) <= parseInt(y2, 10)) {
                        count = count + 1;
                    }
                    else {
                        alert("In Super After Tax Contributions, End Age should be later than Start Age.");
                        count = 0;
                        return false;
                    }
                    if (parseInt(x2, 10) < parseInt(z2, 10) || parseInt(x2, 10) > parseInt(planeAge2, 10)) {
                        alert("In Super After Tax Contributions, Start Age should not be less than Current Age.");
                        count = 0;
                        return false;
                    }
                    if (parseInt(y2, 10) > parseInt(planeAge2, 10) || parseInt(y2, 10) < parseInt(z2)) {
                        alert("In Super After Tax Contributions, End Age should not be later than Plan Age");
                        count = 0;
                        return false;
                    }
                }
                else {
                    //here we check one by one fields is empty
                    if (GetTextBoxValue("<%: txtSATCAmount.ClientID  %>") == "") {
                        alert("In Super After Tax Contributions, Enter Amount.");
                    }
                    if (GetTextBoxValue("<%: txtSATCStartAge.ClientID  %>") == "") {
                        alert("In Super After Tax Contributions, Enter Start Age.");
                    }
                    if (GetTextBoxValue("<%: txtSATCStopAge.ClientID  %>") == "") {
                        alert("In Super After Tax Contributions, End Start Age.");
                    }
                    count = 0;
                    return false;
                }
            }


            if (GetElement("<%: chkNonSuperInvestmentsSavings.ClientID %>").checked) {
                if (GetTextBoxValue("<%: txtNSISAmountSaved.ClientID  %>") != "" && GetTextBoxValue("<%: txtNSISAmountSaved.ClientID  %>") != "0" &&
                    GetTextBoxValue("<%: txtNSISStartAge.ClientID  %>") != "" && GetTextBoxValue("<%: txtNSISStartAge.ClientID  %>") != "0" &&
                    GetTextBoxValue("<%: txtNSISStopAge.ClientID  %>") != "" && GetTextBoxValue("<%: txtNSISStopAge.ClientID  %>") != "0") {

                    //here we compare two ages Start Age and End age
                    var x1 = GetTextBoxValue("<%: txtNSISStartAge.ClientID  %>");
                    var y1 = GetTextBoxValue("<%: txtNSISStopAge.ClientID  %>");
                    var z1 = GetElement("<%: hidCurrentAge.ClientID %>").value;
                    var planeAge1 = GetTextBoxValue("<%: txtPDPlanAge.ClientID %>");

                    if (parseInt(x1, 10) <= parseInt(y1, 10)) {
                        count = count + 1;
                    }
                    else {
                        alert("In Non Super Investments (Savings), Start Age should be earlier than End Age.");
                        count = 0;
                        return false;
                    }
                    if (parseInt(x1, 10) < parseInt(z1, 10) || parseInt(x1, 10) > parseInt(planeAge1, 10)) {
                        alert("In Non Super Investments (Savings), Start Age should not be less than Current Age.");
                        count = 0;
                        return false;
                    }
                    if (parseInt(y1, 10) > parseInt(planeAge1, 10) || parseInt(y1, 10) < parseInt(z1, 10)) {
                        alert("In Non Super Investments (Savings), End Age should not be later than Plan Age.");
                        count = 0;
                        return false;
                    }
                }
                else {
                    //Here we check which field is empty
                    if (GetTextBoxValue("<%: txtNSISAmountSaved.ClientID  %>") == "") {
                        alert("In Non Super Investments (Savings), Enter Amount");
                    }
                    if (GetTextBoxValue("<%: txtNSISStartAge.ClientID  %>") == "") {
                        alert("In Non Super Investments (Savings), Enter Start Age");
                    }
                    if (GetTextBoxValue("<%: txtNSISStopAge.ClientID  %>") == "") {
                        alert("In Non Super Investments (Savings), Enter End Age");
                    }
                    count = 0;
                    return false;
                }
            }
            if (count == 0) {
                return false;
            }
            else {
                return true;
            }
        }

        function ValidateActionTabForFinish() {
            //Here we validate the Action Tab
            var count = 0;
            if (GetElement("<%: chkSuperGuaranteeContributions.ClientID %>").checked) {
                if (GetTextBoxValue("<%: txtSGCPercentSalary.ClientID %>") != "" && GetTextBoxValue("<%: txtSGCPercentSalary.ClientID %>") != "0") {
                    count = count + 1;
                }
                else {
                    count = 0;
                    return false;
                }
            }

            if (GetElement("<%: chkSuperSalarySacrificeContributions.ClientID %>").checked) {
                if ((GetTextBoxValue("<%: txtSSSCPercentSalary.ClientID %>") != "" && GetTextBoxValue("<%: txtSSSCPercentSalary.ClientID %>") != "0") ||
                    (GetTextBoxValue("<%: txtSSSCAmount.ClientID %>") != "" && GetTextBoxValue("<%: txtSSSCAmount.ClientID %>") != "0")) {
                    count = count + 1;

                    //here we check Start age End AGE
                    if (GetTextBoxValue("<%: txtSSSCStartAge.ClientID %>") != "" && GetTextBoxValue("<%: txtSSSCStartAge.ClientID %>") != "0" &&
                        GetTextBoxValue("<%: txtSSSCStopAge.ClientID %>") != "" && GetTextBoxValue("<%: txtSSSCStopAge.ClientID %>") != "0") {

                        //here we compare two ages Start Age and End age
                        var x = GetTextBoxValue("<%: txtSSSCStartAge.ClientID %>");
                        var y = GetTextBoxValue("<%: txtSSSCStopAge.ClientID %>");
                        var z = GetElement("<%: hidCurrentAge.ClientID %>").value;
                        var planeAge = GetTextBoxValue("<%: txtPDPlanAge.ClientID %>");

                        if (parseInt(x, 10) <= parseInt(y, 10)) {
                            count = count + 1;
                        }
                        else {
                            count = 0;
                            return false;
                        }
                        if (parseInt(x, 10) < parseInt(z, 10) || parseInt(x, 10) > parseInt(planeAge)) {
                            count = 0;
                            return false;
                        }
                        if (parseInt(y, 10) > parseInt(planeAge) || parseInt(y, 10) < parseInt(z)) {
                            count = 0;
                            return false;
                        }
                    }
                    else {
                        count = 0;
                        return false;
                    }
                }
                else {
                    count = 0;
                    return false;
                }
            }

            if (GetElement("<%: chkSuperAfterTaxContributions.ClientID %>").checked) {
                if (GetTextBoxValue("<%: txtSATCAmount.ClientID  %>") != "" && GetTextBoxValue("<%: txtSATCAmount.ClientID  %>") != "0" &&
                    GetTextBoxValue("<%: txtSATCStartAge.ClientID  %>") != "" && GetTextBoxValue("<%: txtSATCStartAge.ClientID  %>") != "0" &&
                    GetTextBoxValue("<%: txtSATCStopAge.ClientID  %>") != "" && GetTextBoxValue("<%: txtSATCStopAge.ClientID  %>") != "0") {

                    var x2 = GetTextBoxValue("<%: txtSATCStartAge.ClientID  %>");
                    var y2 = GetTextBoxValue("<%: txtSATCStopAge.ClientID  %>");
                    var z2 = GetElement("<%: hidCurrentAge.ClientID %>").value;
                    var planeAge2 = GetTextBoxValue("<%: txtPDPlanAge.ClientID %>");

                    if (parseInt(x2, 10) <= parseInt(y2, 10)) {
                        count = count + 1;
                    }
                    else {
                        count = 0;
                        return false;
                    }
                    if (parseInt(x2, 10) < parseInt(z2, 10) || parseInt(x2, 10) > parseInt(planeAge2, 10)) {
                        count = 0;
                        return false;
                    }
                    if (parseInt(y2, 10) > parseInt(planeAge2, 10) || parseInt(y2, 10) < parseInt(z2)) {
                        count = 0;
                        return false;
                    }
                }
                else {
                    count = 0;
                    return false;
                }
            }


            if (GetElement("<%: chkNonSuperInvestmentsSavings.ClientID %>").checked) {
                if (GetTextBoxValue("<%: txtNSISAmountSaved.ClientID  %>") != "" && GetTextBoxValue("<%: txtNSISAmountSaved.ClientID  %>") != "0" &&
                    GetTextBoxValue("<%: txtNSISStartAge.ClientID  %>") != "" && GetTextBoxValue("<%: txtNSISStartAge.ClientID  %>") != "0" &&
                    GetTextBoxValue("<%: txtNSISStopAge.ClientID  %>") != "" && GetTextBoxValue("<%: txtNSISStopAge.ClientID  %>") != "0") {

                    //here we compare two ages Start Age and End age
                    var x1 = GetTextBoxValue("<%: txtNSISStartAge.ClientID  %>");
                    var y1 = GetTextBoxValue("<%: txtNSISStopAge.ClientID  %>");
                    var z1 = GetElement("<%: hidCurrentAge.ClientID %>").value;
                    var planeAge1 = GetTextBoxValue("<%: txtPDPlanAge.ClientID %>");

                    if (parseInt(x1, 10) <= parseInt(y1, 10)) {
                        count = count + 1;
                    }
                    else {
                        count = 0;
                        return false;
                    }
                    if (parseInt(x1, 10) < parseInt(z1, 10) || parseInt(x1, 10) > parseInt(planeAge1, 10)) {
                        count = 0;
                        return false;
                    }
                    if (parseInt(y1, 10) > parseInt(planeAge1, 10) || parseInt(y1, 10) < parseInt(z1, 10)) {
                        count = 0;
                        return false;
                    }
                }
                else {
                    count = 0;
                    return false;
                }
            }
            if (count == 0) {
                return false;
            }
            else {
                return true;
            }
        }

        function ValidateGoalTab(showMessage) {

            if (!$('#<%: chkWealth.ClientID %>').prop('checked')
                && !$('#<%: chkIncome.ClientID %>').prop('checked')
                && !$('#<%: chkLumpSum.ClientID %>').prop('checked')
                && !$('#<%: chkLiquidAssets.ClientID %>').prop('checked')
                && !$('#<%: chkBequest.ClientID %>').prop('checked')) {
                if (showMessage == true)
                    alert('Please input atleast one option in Goals Tab.');
                return false;
            }

            var isValid = true;

            if ($('#<%: chkWealth.ClientID %>').prop('checked')) {

                isValid = $('#<%: grdGoalsWealth.ClientID %> tbody tr[dataItemRow="1"]').size() >= 1;
                if (!isValid)
                    if (($('.txtGoalsWealth_TotalAssetsWorth').val() == null || $.trim($('.txtGoalsWealth_TotalAssetsWorth').val()) == "" || parseFloat($('.txtGoalsWealth_TotalAssetsWorth').val()) <= 0) ||
                        ($('.txtGoalsWealth_DollarsFromAge_Footer').val() == null || $.trim($('.txtGoalsWealth_DollarsFromAge_Footer').val()) == "" || parseInt($('.txtGoalsWealth_DollarsFromAge_Footer').val()) <= 0))
                        isValid = false;
                    else
                        isValid = true;
            }
            if (!isValid) {
                if (showMessage == true) {
                    alert('Please input atleast one option in Goals Tab --> Wealth.');
                }
                $('.txtGoalsWealth_TotalAssetsWorth').focus();
                return false;
            }

            isValid = $('#<%: grdGoalsWealth.ClientID %> tbody tr.editModeFooterRow').size() <= 0;
            if (!isValid)
                if(showMessage == true) {
                    alert('Please either commit or cancel the edit mode in Goals Tab --> Wealth.');
                    return false;
                }
            

            if ($('#<%: chkIncome.ClientID %>').prop('checked')) {
                isValid = $('#<%: grdGoalsIncome.ClientID %> tbody tr[dataItemRow="1"]').size() >= 1;

                if (!isValid)
                    if (($('.txtGoalsIncome_TotalIncomeRequired').val() == null || $.trim($('.txtGoalsIncome_TotalIncomeRequired').val()) == "" || parseFloat($('.txtGoalsIncome_TotalIncomeRequired').val()) <= 0) ||
                        ($('.txtGoalsIncome_DollarsFromAge_Footer').val() == null || $.trim($('.txtGoalsIncome_DollarsFromAge_Footer').val()) == "" || parseInt($('.txtGoalsIncome_DollarsFromAge_Footer').val()) <= 0))
                        isValid = false;
                    else
                        isValid = true;
            }
            if (!isValid) {
                if (showMessage == true) {
                    alert('Please input atleast one option in Goals Tab --> Income.');
                }
                $('.txtGoalsIncome_TotalIncomeRequired').focus();
                return false;
            }

            isValid = $('#<%: grdGoalsIncome.ClientID %> tbody tr.editModeFooterRow').size() <= 0;
            if (!isValid)
                if (showMessage == true) {
                    alert('Please either commit or cancel the edit mode in Goals Tab --> Income.');
                    return false;
                }
            

            if ($('#<%: chkLumpSum.ClientID %>').prop('checked')) {
                isValid = $('#<%: grdGoalsLumpSum.ClientID %> tbody tr[dataItemRow="1"]').size() >= 1;

                if (!isValid)
                    if (($('.txtGoalsLumpSum_TotalLumpSum').val() == null || $.trim($('.txtGoalsLumpSum_TotalLumpSum').val()) == "" || parseFloat($('.txtGoalsLumpSum_TotalLumpSum').val()) <= 0) ||
                        ($('.txtGoalsLumpSum_DollarsFromAge_Footer').val() == null || $.trim($('.txtGoalsLumpSum_DollarsFromAge_Footer').val()) == "" || parseInt($('.txtGoalsLumpSum_DollarsFromAge_Footer').val()) <= 0))
                        isValid = false;
                    else
                        isValid = true;

            }
            if (!isValid) {
                if (showMessage == true)
                    alert('Please input atleast one option in Goals Tab --> Lump Sum.');
                $('.txtGoalsLumpSum_TotalLumpSum').focus();
                return false;
            }

            isValid = $('#<%: grdGoalsLumpSum.ClientID %> tbody tr.editModeFooterRow').size() <= 0;
            if (!isValid)
                if (showMessage == true) {
                    alert('Please either commit or cancel the edit mode in Goals Tab --> Lump Sum.');
                    return false;
                }

            if ($('#<%: chkLiquidAssets.ClientID %>').prop('checked')) {
                isValid = $('#<%: grdGoalsLiquidAssets.ClientID %> tbody tr[dataItemRow="1"]').size() >= 1;

                if (!isValid)
                    if (($('.txtGoalsLiquidAssets_TotalLiquidAssets').val() == null || $.trim($('.txtGoalsLiquidAssets_TotalLiquidAssets').val()) == "" || parseFloat($('.txtGoalsLiquidAssets_TotalLiquidAssets').val()) <= 0) ||
                        ($('.txtGoalsLiquidAssets_DollarsFromAge_Footer').val() == null || $.trim($('.txtGoalsLiquidAssets_DollarsFromAge_Footer').val()) == "" || parseInt($('.txtGoalsLiquidAssets_DollarsFromAge_Footer').val()) <= 0))
                        isValid = false;
                    else
                        isValid = true;

            }
            if (!isValid) {
                if (showMessage == true)
                    alert('Please input atleast one option in Goals Tab --> Liquid Assets.');
                $('.txtGoalsLiquidAssets_TotalLiquidAssets').focus();
                return false;
            }

            isValid = $('#<%: grdGoalsLiquidAssets.ClientID %> tbody tr.editModeFooterRow').size() <= 0;
            if (!isValid)
                if (showMessage == true) {
                    alert('Please either commit or cancel the edit mode in Goals Tab --> Liquid Assets.');
                    return false;
                }

            if ($('#<%: chkBequest.ClientID %>').prop('checked')) {
                isValid = $('#<%: grdGoalsBequest.ClientID %> tbody tr[dataItemRow="1"]').size() >= 1;

                if (!isValid)
                    if (($('.txtGoalsBequest_TotalBequest').val() == null || $.trim($('.txtGoalsBequest_TotalBequest').val()) == "" || parseFloat($('.txtGoalsBequest_TotalBequest').val()) <= 0) ||
                        ($('.txtGoalsBequest_DollarsFromAge_Footer').val() == null || $.trim($('.txtGoalsBequest_DollarsFromAge_Footer').val()) == "" || parseInt($('.txtGoalsBequest_DollarsFromAge_Footer').val()) <= 0))
                        isValid = false;
                    else
                        isValid = true;
            }

            if (!isValid) {
                if (showMessage == true)
                    alert('Please input atleast one option in Goals Tab --> Bequest.');
                $('.txtGoalsBequest_TotalBequest').focus();
                return false;
            }

            isValid = $('#<%: grdGoalsBequest.ClientID %> tbody tr.editModeFooterRow').size() <= 0;
            if (!isValid)
                if (showMessage == true) {
                    alert('Please either commit or cancel the edit mode in Goals Tab --> Bequest.');
                    return false;
                }
            
            return isValid;
        }

        function ValidateGoalTabForFinish() {

            if (ValidateGoalTab(false) == false) return false;
            return true;
        }

        function ValidateGoalsGridOnAction(gridId, chkSelected, sectionName, actionType, eventSource) {

            var amount = 0;
            var age = 0;
            var maxAge = 0;
            var currentAge = $('#<%:hidCurrentAge.ClientID %>').val();
            var planAge = $('#<%:txtPDPlanAge.ClientID %>').val();

            amount = parseInt($('#' + gridId).find('input[DataColumn="Amount"]').val());
            age = parseInt($('#' + gridId).find('input[DataColumn="Age"]').val());
            if (actionType == "Update") {
                amount = parseInt($(eventSource).closest('tr').find('input[DataColumn="Amount"]').val());
                age = parseInt($(eventSource).closest('tr').find('input[DataColumn="Age"]').val());
            }

            if ($(chkSelected).prop('checked')) {

                var errorMsg = '';
                var txtAmount = null;
                var txtAge = null;

                txtAmount = $('#' + gridId).find('input[DataColumn="Amount"]');
                txtAge = $('#' + gridId).find('input[DataColumn="Age"]');

                if (actionType == "Update") {
                    txtAmount = $(eventSource).closest('tr').find('input[DataColumn="Amount"]');
                    txtAge = $(eventSource).closest('tr').find('input[DataColumn="Age"]');
                }

                if (currentAge == null || currentAge == '' || isNaN(currentAge) == true) {
                    if (!(errorMsg == null || errorMsg == ''))
                        errorMsg = "\nCurrent Age is invalid or empty.";
                }

                if (planAge == null || planAge == '' || isNaN(planAge) == true) {
                    if (!(errorMsg == null || errorMsg == ''))
                        errorMsg = "\nPlan Age is invalid or empty.";
                }

                if (amount == null || amount == '' || isNaN(amount) == true) {
                    if (!(errorMsg == null || errorMsg == ''))
                        errorMsg += "\nIn " + sectionName + " section, amount is invalid or empty.";
                    else
                        errorMsg = "In " + sectionName + " section, amount is invalid or empty.";

                    txtAmount.focus();
                }

                if (!(errorMsg == null || errorMsg == '')) {
                    alert(errorMsg);
                    return false;
                }

                if (age == null || age == '' || isNaN(age) == true) {
                    if (!(errorMsg == null || errorMsg == ''))
                        errorMsg += "\nIn " + sectionName + " section, age is invalid or empty.";
                    else
                        errorMsg = "In " + sectionName + " section, age is invalid or empty.";

                    txtAge.focus();
                }

                if (!(errorMsg == null || errorMsg == '')) {
                    alert(errorMsg);
                    return false;
                }

                if (!(age >= currentAge && age <= planAge)) {
                    if (!(errorMsg == null || errorMsg == ''))
                        errorMsg += "\nIn " + sectionName + " section, age should be between " + currentAge + " and " + planAge;
                    else
                        errorMsg = "In " + sectionName + " section, age should be between " + currentAge + " and " + planAge;

                    txtAge.focus();
                }

                if (!(errorMsg == null || errorMsg == '')) {
                    alert(errorMsg);
                    return false;
                }

                if (actionType == "Update") {

                    $(eventSource).closest('tr').prevAll('tr:visible').find('td .clsGoalsAge').each(function () {
                        var tempAge = parseInt($(this).text());
                        if (tempAge > parseInt(maxAge))
                            maxAge = tempAge;
                    });

                    var isAgeLessThanDefinedAge = false;
                    if (age <= maxAge) {
                        if (!(errorMsg == null || errorMsg == ''))
                            errorMsg += "\nIn " + sectionName + " section, age should be greater than already defined age (" + maxAge + ")";
                        else
                            errorMsg = "In " + sectionName + " section, age should be greater than already defined age (" + maxAge + ")";

                        isAgeLessThanDefinedAge = true;
                        txtAge.focus();
                    }

                    if (!isAgeLessThanDefinedAge) {
                        maxAge = 0;
                        $(eventSource).closest('tr').nextAll('tr:visible').find('td .clsGoalsAge').each(function () {
                            var tempAge = parseInt($(this).text());
                            if (age == parseInt(tempAge)) {
                                maxAge = tempAge;
                                return;
                            }
                        });

                        if (age == maxAge) {
                            if (!(errorMsg == null || errorMsg == ''))
                                errorMsg += "\nIn " + sectionName + " section, age should not be equal to already defined age (" + maxAge + ")";
                            else
                                errorMsg = "In " + sectionName + " section, age should not be equal to already defined age (" + maxAge + ")";

                            txtAge.focus();
                        }
                    }

                }
                else {
                    $('#' + gridId + ' tbody tr td .clsGoalsAge').each(function () {

                        var tempAge = parseInt($(this).text());
                        if (tempAge > parseInt(maxAge))
                            maxAge = tempAge;
                    });

                    if (age <= maxAge) {
                        if (!(errorMsg == null || errorMsg == ''))
                            errorMsg += "\nIn " + sectionName + " section, age should be greater than already defined age (" + maxAge + ")";
                        else
                            errorMsg = "In " + sectionName + " section, age should be greater than already defined age (" + maxAge + ")";

                        txtAge.focus();
                    }
                }
                if (!(errorMsg == null || errorMsg == '')) {
                    alert(errorMsg);
                    return false;
                }

            }
            return true;
        }


        function ShowActionBar(obj) {
            $('div.actionBar').show();
            if (obj.className == "disabled") return;
            var stepnumber = parseInt($(obj).find('label').text());
            $("#<%: hfCurrentGoalsWizardTab.ClientID %>").val(stepnumber);            
            
            switch (stepnumber) {
                case 1:
                    $('.buttonPrevious').addClass('buttonDisabled');
                    $('.buttonNext').removeClass('buttonDisabled');
                    break;
                case 2:
                    $('.buttonPrevious').removeClass('buttonDisabled');
                    $('.buttonNext').removeClass('buttonDisabled');
                    break;
                case 3:
                    $('.buttonPrevious').removeClass('buttonDisabled');
                    $('.buttonNext').removeClass('buttonDisabled');
                    break;
                case 4:
                    $('.buttonPrevious').removeClass('buttonDisabled');
                    $('.buttonNext').removeClass('buttonDisabled');
                    break;
                case 5:
                    $('.buttonPrevious').removeClass('buttonDisabled');
                    $('.buttonNext').addClass('buttonDisabled');
                    break;
            }
        }

        function SlideDown(obj) {
            obj.style.display = 'inline';
            document.getElementById(obj).style.display = 'inline';
        }
        function onNextCallback(objs, context) {
            alert('OnNext');
        }
        function onFinishCallback(objs, context) {
            var isStepValidProfile = ValidateProfileTabFinish();
            var isStepValidAssest = ValidateAssestTabFinish();
            var isStepValidAction = ValidateActionTabForFinish();
            var isStepValidGoal = ValidateGoalTabForFinish();
            var tabName = "";

            if (isStepValidProfile == false) {
                tabName += "Profile,";
            }
            if (isStepValidAssest == false) {
                tabName += "Assest,";
            }
            //if (isStepValidAction == false) {
            //    tabName += "Action,";
            //}

            if (isStepValidGoal == false) {
                tabName += "Goal,";
            }

            if (isStepValidProfile && isStepValidAssest && isStepValidAction && isStepValidGoal) {
                DisableSaveButton();
                GetElement("<%: hidGetNextButtonClick.ClientID %>").value = "1";
                GetElement("<%: hfIsFinished.ClientID %>").value = "1";
                $('form').submit();
                return true;
            }
            else {
                var withoutLastOneChars = tabName.slice(0, -1);
                alert("Some Mandatory fields are empty.Please check these tabs: " + withoutLastOneChars);
                return false;
            }
        }
        function OnEditAdvice() {
            $('a.buttonNext').click();
            $('div.actionBar').show();
        }

        function ClearALLControls() {
            for (i = 0; i < document.forms[0].elements.length; i++) {
                elm = document.forms[0].elements[i]
                switch (elm.type) {
                    case 'checkbox':
                        elm.checked = false;
                        break;
                    default:
                        break;
                }
            }
            var lstRadNumericTextBox = GetElementsByClass('riTextBox riEnabled');
            for (i = 0; i < lstRadNumericTextBox.length; i++) {
                lstRadNumericTextBox[i].value = '';
            }
        }

        function GetElementsByClass(className) {
            var matchingItems = [];
            var allElements = document.getElementsByTagName("*");

            for (var i = 0; i < allElements.length; i++) {
                if (allElements[i].className == className) {
                    matchingItems.push(allElements[i]);
                }
            }
            return matchingItems;
        }


        function GoToPage(url) {
            window.location = url + '?ins=<%:cid %>';
            return false;
        }

        //validation of the Checkbox and of the Page Load
        function toggleStatusSuperannuation() {
            var isChecked = GetElement("<%: chkSuperannuation.ClientID  %>").checked;
            SetTextBoxStatus("<%: txtSEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtSFixedInterest.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtSInternationalEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtSCash.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtSProperty.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtSGold.ClientID %>", !isChecked);
        }

        function toggleStatusPension() {
            var isChecked = GetElement("<%: chkPension.ClientID %>").checked;

            SetTextBoxStatus("<%: txtPEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtPFixedInterest.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtPInternationalEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtPCash.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtPProperty.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtPGold.ClientID %>", !isChecked);
        }

        function toggleStatuschkNonSuperInvestments() {
            var isChecked = GetElement("<%: chkNonSuperInvestments.ClientID %>").checked;

            SetTextBoxStatus("<%: txtNEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtNFixedInterest.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtNInternationalEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtNCash.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtNProperty.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtNGold.ClientID %>", !isChecked);
        }

        function toggleStatusDisableControl() {
            //Assets
            var txtSEquity = window.$find("<%: txtSEquity.ClientID %>");
            if (txtSEquity != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtSEquity.ClientID %>", true);
            }
            var txtSFixedInterest = window.$find("<%: txtSFixedInterest.ClientID %>");
            if (txtSFixedInterest != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtSFixedInterest.ClientID %>", true);
            }
            var txtSInternationalEquity = window.$find("<%: txtSInternationalEquity.ClientID %>");
            if (txtSInternationalEquity != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtSInternationalEquity.ClientID %>", true);
            }
            var txtSCash = window.$find("<%: txtSCash.ClientID %>");
            if (txtSCash != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtSCash.ClientID %>", true);
            }
            var txtSProperty = window.$find("<%: txtSProperty.ClientID %>");
            if (txtSProperty != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtSProperty.ClientID %>", true);
            }
            var txtSGold = window.$find("<%: txtSGold.ClientID %>");
            if (txtSGold != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtSGold.ClientID %>", true);
            }
            var txtPEquity = window.$find("<%: txtPEquity.ClientID %>");
            if (txtPEquity != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtPEquity.ClientID %>", true);
            }
            var txtPFixedInterest = window.$find("<%: txtPFixedInterest.ClientID %>");
            if (txtPFixedInterest != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtPFixedInterest.ClientID %>", true);
            }
            var txtPInternationalEquity = window.$find("<%: txtPInternationalEquity.ClientID %>");
            if (txtPInternationalEquity != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtPInternationalEquity.ClientID %>", true);
            }
            var txtPCash = window.$find("<%: txtPCash.ClientID %>");
            if (txtPCash != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtPCash.ClientID %>", true);
            }
            var txtPProperty = window.$find("<%: txtPProperty.ClientID %>");
            if (txtPProperty != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtPProperty.ClientID %>", true);
            }
            var txtPGold = window.$find("<%: txtPGold.ClientID %>");
            if (txtPGold != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtPGold.ClientID %>", true);
            }
            var txtNEquity = window.$find("<%: txtNEquity.ClientID %>");
            if (txtNEquity != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtNEquity.ClientID %>", true);
            }
            var txtNFixedInterest = window.$find("<%: txtNFixedInterest.ClientID %>");
            if (txtNFixedInterest != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtNFixedInterest.ClientID %>", true);
            }
            var txtNInternationalEquity = window.$find("<%: txtNInternationalEquity.ClientID %>");
            if (txtNInternationalEquity != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtNInternationalEquity.ClientID %>", true);
            }
            var txtNCash = window.$find("<%: txtNCash.ClientID %>");
            if (txtNCash != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtNCash.ClientID %>", true);
            }
            var txtNProperty = window.$find("<%: txtNProperty.ClientID %>");
            if (txtNProperty != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtNProperty.ClientID %>", true);
            }
            var txtNGold = window.$find("<%: txtNGold.ClientID %>");
            if (txtNGold != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtNGold.ClientID %>", true);
            }
            //Action
            var txtNsisAmountSaved = window.$find("<%: txtNSISAmountSaved.ClientID %>");
            if (txtNsisAmountSaved != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtNSISAmountSaved.ClientID %>", true);
            }
            var cmbFreq = window.$find("<%: cmbFreq.ClientID %>");
            if (cmbFreq != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                cmbFreq.disable();
            }
            var txtNsisStartAge = window.$find("<%: txtNSISStartAge.ClientID %>");
            if (txtNsisStartAge != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtNSISStartAge.ClientID %>", true);
            }
            var txtnsistopAge = window.$find("<%: txtNSISStopAge.ClientID %>");
            if (txtnsistopAge != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtNSISStopAge.ClientID %>", true);
            }
            var txtSatcAmount = window.$find("<%: txtSATCAmount.ClientID %>");
            if (txtSatcAmount != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtSATCAmount.ClientID %>", true);
            }
            var txtSstcStartAge = window.$find("<%: txtSATCStartAge.ClientID %>");
            if (txtSstcStartAge != null && document.getElementById("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtSATCStartAge.ClientID %>", true);
            }
            var txtSatcStopAge = window.$find("<%: txtSATCStopAge.ClientID %>");
            if (txtSatcStopAge != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtSATCStopAge.ClientID %>", true);
            }
            var txtsssCPercentSalary = window.$find("<%: txtSSSCPercentSalary.ClientID %>");
            if (txtsssCPercentSalary != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtSSSCPercentSalary.ClientID %>", true);
            }
            var txtsssCAmount = window.$find("<%: txtSSSCAmount.ClientID %>");
            if (txtsssCAmount != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtSSSCAmount.ClientID %>", true);
            }
            var txtsssCStartAge = window.$find("<%: txtSSSCStartAge.ClientID %>");
            if (txtsssCStartAge != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtSSSCStartAge.ClientID %>", true);
            }
            var txtsssCStopAge = window.$find("<%: txtSSSCStopAge.ClientID %>");
            if (txtsssCStopAge != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtSSSCStopAge.ClientID %>", true);
            }
            var txtSgcPercentSalary = window.$find("<%: txtSGCPercentSalary.ClientID %>");
            if (txtSgcPercentSalary != null && GetElement("<%: IsEdit.ClientID %>").value == "0") {
                SetTextBoxStatus("<%: txtSGCPercentSalary.ClientID %>", true);
            }

        }

        function toggleStatusSuperContribution() {
            if (GetElement("<%: chkSuperGuaranteeContributions.ClientID  %>").checked) {

                SetTextBoxStatus("<%: txtSGCPercentSalary.ClientID %>", false);
                if (GetTextBoxValue("<%: txtSGCPercentSalary.ClientID%>") == "") {
                    SetTextBoxValue("<%: txtSGCPercentSalary.ClientID %>", 9.25);
                }
            }
            else {
                SetTextBoxStatus("<%: txtSGCPercentSalary.ClientID %>", true);
                //SetTextBoxValue("<%: txtSGCPercentSalary.ClientID %>", "");
            }
        }

        function toggleSalarySacrifice() {

            var isChecked = GetElement("<%: chkSuperSalarySacrificeContributions.ClientID %>").checked;
            if (isChecked) {
                SetTextBoxStatus("<%: txtSSSCPercentSalary.ClientID %>", !isChecked);
                if (GetTextBoxValue("<%: txtSSSCPercentSalary.ClientID%>") == "") {
                    var totalSalary = GetTextBoxValue("<%: txtPDSalary.ClientID %>");
                    SetTextBoxValue("<%: txtSSSCAmount.ClientID %>", Math.round(totalSalary * 5 / 100.0, 2));
                    SetTextBoxValue("<%: txtSSSCPercentSalary.ClientID %>", 5);
                }

                SetTextBoxStatus("<%: txtSSSCAmount.ClientID %>", !isChecked);
                SetTextBoxStatus("<%: txtSSSCStartAge.ClientID %>", !isChecked);

                if (GetTextBoxValue("<%: txtSSSCStartAge.ClientID%>") == "") {
                    var currentAge = GetElement("<%: hidCurrentAge.ClientID %>").value;
                    SetTextBoxValue("<%: txtSSSCStartAge.ClientID %>", currentAge);
                }

                SetTextBoxStatus("<%: txtSSSCStopAge.ClientID %>", !isChecked);
                if (GetTextBoxValue("<%: txtSSSCStopAge.ClientID%>") == "") {
                    var retirementAge = GetTextBoxValue("<%: txtPDRetirementAge.ClientID %>");
                    SetTextBoxValue("<%: txtSSSCStopAge.ClientID %>", retirementAge);
                }
            }
            else {
                SetTextBoxStatus("<%: txtSSSCPercentSalary.ClientID %>", !isChecked);
                //GetTextBoxValue("<%: txtSSSCPercentSalary.ClientID%>");
                SetTextBoxStatus("<%: txtSSSCAmount.ClientID %>", !isChecked);
                //GetTextBoxValue("<%: txtSSSCAmount.ClientID%>");
                SetTextBoxStatus("<%: txtSSSCStartAge.ClientID %>", !isChecked);
                //GetTextBoxValue("<%: txtSSSCStartAge.ClientID%>");
                SetTextBoxStatus("<%: txtSSSCStopAge.ClientID %>", !isChecked);
                //GetTextBoxValue("<%: txtSSSCStopAge.ClientID%>");
            }
        }

        function toggleStatusSuperAfterTax() {

            var isChecked = GetElement("<%: chkSuperAfterTaxContributions.ClientID %>").checked;
            if (isChecked) {

                SetTextBoxStatus("<%: txtSATCAmount.ClientID %>", !isChecked);
                if (GetTextBoxValue("<%: txtSATCAmount.ClientID %>") == "") {
                    SetTextBoxValue("<%: txtSATCAmount.ClientID %>", 1000);
                }

                SetTextBoxStatus("<%: txtSATCStartAge.ClientID %>", !isChecked);
                if (GetTextBoxValue("<%: txtSATCStartAge.ClientID%>") == "") {
                    var currentAge = GetElement("<%: hidCurrentAge.ClientID %>").value;
                    SetTextBoxValue("<%: txtSATCStartAge.ClientID %>", currentAge);
                }

                SetTextBoxStatus("<%: txtSATCStopAge.ClientID %>", !isChecked);
                if (GetTextBoxValue("<%: txtSATCStopAge.ClientID%>") == "") {
                    var retirementAge = GetTextBoxValue("<%: txtPDRetirementAge.ClientID %>");
                    SetTextBoxValue("<%: txtSATCStopAge.ClientID %>", retirementAge);
                }
            }
            else {
                SetTextBoxStatus("<%: txtSATCAmount.ClientID %>", !isChecked);
                //SetTextBoxValue("<%: txtSATCAmount.ClientID%>", "");
                SetTextBoxStatus("<%: txtSATCStartAge.ClientID %>", !isChecked);
                //SetTextBoxValue("<%: txtSATCStartAge.ClientID%>", "");
                SetTextBoxStatus("<%: txtSATCStopAge.ClientID %>", !isChecked);
                //SetTextBoxValue("<%: txtSATCStopAge.ClientID%>", "");
            }
        }

        function toggleStatusSuperInvestment() {
            var isChecked = document.getElementById("<%: chkNonSuperInvestmentsSavings.ClientID %>").checked;

            if (isChecked) {
                window.$find("<%: txtNSISAmountSaved.ClientID %>").enable();
                if (GetTextBoxValue("<%: txtNSISAmountSaved.ClientID %>") == "") {
                    SetTextBoxValue("<%: txtNSISAmountSaved.ClientID %>", 1000);
                }

                SetTextBoxStatus("<%: txtNSISAmountSaved.ClientID%>", !isChecked);
                SetTextBoxStatus("<%: txtNSISStartAge.ClientID %>", !isChecked);

                if (GetTextBoxValue("<%: txtNSISStartAge.ClientID%>") == "") {
                    var currentAge = GetTextBoxValue("<%: hidCurrentAge.ClientID %>");
                    SetTextBoxValue("<%: txtNSISStartAge.ClientID %>", currentAge);
                }

                SetTextBoxStatus("<%: txtNSISStopAge.ClientID %>", !isChecked);
                if (GetTextBoxValue("<%: txtNSISStopAge.ClientID%>") == "") {
                    var retirementAge = GetTextBoxValue("<%: txtPDRetirementAge.ClientID %>");
                    SetTextBoxValue("<%: txtNSISStopAge.ClientID %>", retirementAge);
                }
            }
            else {
                window.$find("<%: cmbFreq.ClientID %>").disable();

                SetTextBoxStatus("<%: txtNSISAmountSaved.ClientID %>", !isChecked);
                //SetTextBoxValue("<%: txtNSISAmountSaved.ClientID%>", "");

                SetTextBoxStatus("<%: txtNSISStartAge.ClientID%>", !isChecked);
                //SetTextBoxValue("<%: txtNSISStartAge.ClientID%>", "");

                SetTextBoxStatus("<%: txtNSISStopAge.ClientID %>", !isChecked);
                //SetTextBoxValue("<%: txtNSISStopAge.ClientID%>", "");
            }
        }

        function DisableSaveButton(sender, args) {

            GetElement('light').ShowElementAsBlock();
            GetElement('fade').ShowElementAsBlock();
        }

        function EnableProgressBar(sender, args) {

            GetElement('light').HideElement();
            GetElement('fade').HideElement();
        }

        function CheckBrowser() {
            //This is for Browser is IE
            var browserIe = navigator.appName;
            return browserIe;
        }

        function OnRefreshCurrentAsset() {

            //Super Annuation
            var txtSEquity = $('#<%:txtSEquity.ClientID %>');
            var txtSFixedInterest = $('#<%:txtSFixedInterest.ClientID %>');
            var txtSInternationalEquity = $('#<%:txtSInternationalEquity.ClientID %>');
            var txtSCash = $('#<%:txtSCash.ClientID %>');
            var txtSProperty = $('#<%:txtSProperty.ClientID %>');
            var txtSGold = $('#<%:txtSGold.ClientID %>');

            if (!(txtSEquity.val() == null || txtSEquity.val() == "" || parseInt(txtSEquity.val()) == 0) ||
                !(txtSFixedInterest.val() == null || txtSFixedInterest.val() == "" || parseInt(txtSFixedInterest.val()) == 0) ||
                !(txtSInternationalEquity.val() == null || txtSInternationalEquity.val() == "" || parseInt(txtSInternationalEquity.val()) == 0) ||
                !(txtSCash.val() == null || txtSCash.val() == "" || parseInt(txtSCash.val()) == 0) ||
                !(txtSProperty.val() == null || txtSProperty.val() == "" || parseInt(txtSProperty.val()) == 0) ||
                !(txtSGold.val() == null || txtSGold.val() == "" || parseInt(txtSGold.val()) == 0)) {

                $('#<%: chkSuperannuation.ClientID %>').attr('checked', true);
                SetTextBoxStatus($(txtSEquity).attr('id'), false);
                SetTextBoxStatus($(txtSFixedInterest).attr('id'), false);
                SetTextBoxStatus($(txtSInternationalEquity).attr('id'), false);
                SetTextBoxStatus($(txtSCash).attr('id'), false);
                SetTextBoxStatus($(txtSProperty).attr('id'), false);
                SetTextBoxStatus($(txtSGold).attr('id'), false);
            }
            else {
                $('#<%: chkSuperannuation.ClientID %>').attr('checked', false);
                SetTextBoxStatus($(txtSEquity).attr('id'), true);
                SetTextBoxStatus($(txtSFixedInterest).attr('id'), true);
                SetTextBoxStatus($(txtSInternationalEquity).attr('id'), true);
                SetTextBoxStatus($(txtSCash).attr('id'), true);
                SetTextBoxStatus($(txtSProperty).attr('id'), true);
                SetTextBoxStatus($(txtSGold).attr('id'), true);
            }

            //Pension
            var txtPEquity = $('#<%:txtPEquity.ClientID %>');
            var txtPFixedInterest = $('#<%:txtPFixedInterest.ClientID %>');
            var txtPInternationalEquity = $('#<%:txtPInternationalEquity.ClientID %>');
            var txtPCash = $('#<%:txtPCash.ClientID %>');
            var txtPProperty = $('#<%:txtPProperty.ClientID %>');
            var txtPGold = $('#<%:txtPGold.ClientID %>');

            if (!(txtPEquity.val() == null || txtPEquity.val() == "" || parseInt(txtPEquity.val()) == 0) ||
                !(txtPFixedInterest.val() == null || txtPFixedInterest.val() == "" || parseInt(txtPFixedInterest.val()) == 0) ||
                !(txtPInternationalEquity.val() == null || txtPInternationalEquity.val() == "" || parseInt(txtPInternationalEquity.val()) == 0) ||
                !(txtPCash.val() == null || txtPCash.val() == "" || parseInt(txtPCash.val()) == 0) ||
                !(txtPProperty.val() == null || txtPProperty.val() == "" || parseInt(txtPProperty.val()) == 0) ||
                !(txtPGold.val() == null || txtPGold.val() == "" || parseInt(txtPGold.val()) == 0)) {

                $('#<%: chkPension.ClientID %>').attr('checked', true);
                SetTextBoxStatus($(txtPEquity).attr('id'), false);
                SetTextBoxStatus($(txtPFixedInterest).attr('id'), false);
                SetTextBoxStatus($(txtPInternationalEquity).attr('id'), false);
                SetTextBoxStatus($(txtPCash).attr('id'), false);
                SetTextBoxStatus($(txtPProperty).attr('id'), false);
                SetTextBoxStatus($(txtPGold).attr('id'), false);
            }
            else {
                $('#<%: chkPension.ClientID %>').attr('checked', false);
                SetTextBoxStatus($(txtPEquity).attr('id'), true);
                SetTextBoxStatus($(txtPFixedInterest).attr('id'), true);
                SetTextBoxStatus($(txtPInternationalEquity).attr('id'), true);
                SetTextBoxStatus($(txtPCash).attr('id'), true);
                SetTextBoxStatus($(txtPProperty).attr('id'), true);
                SetTextBoxStatus($(txtPGold).attr('id'), true);
            }


            //Non Super Investments
            var txtNEquity = $('#<%:txtNEquity.ClientID %>');
            var txtNFixedInterest = $('#<%:txtNFixedInterest.ClientID %>');
            var txtNInternationalEquity = $('#<%:txtNInternationalEquity.ClientID %>');
            var txtNCash = $('#<%:txtNCash.ClientID %>');
            var txtNProperty = $('#<%:txtNProperty.ClientID %>');
            var txtNGold = $('#<%:txtNGold.ClientID %>');

            if (!(txtNEquity.val() == null || txtNEquity.val() == "" || parseInt(txtNEquity.val()) == 0) ||
                !(txtNFixedInterest.val() == null || txtPFixedInterest.val() == "" || parseInt(txtPFixedInterest.val()) == 0) ||
                !(txtNInternationalEquity.val() == null || txtNInternationalEquity.val() == "" || parseInt(txtNInternationalEquity.val()) == 0) ||
                !(txtNCash.val() == null || txtNCash.val() == "" || parseInt(txtNCash.val()) == 0) ||
                !(txtNProperty.val() == null || txtNProperty.val() == "" || parseInt(txtNProperty.val()) == 0) ||
                !(txtNGold.val() == null || txtNGold.val() == "" || parseInt(txtNGold.val()) == 0)) {

                $('#<%: chkNonSuperInvestments.ClientID %>').attr('checked', true);
                SetTextBoxStatus($(txtNEquity).attr('id'), false);
                SetTextBoxStatus($(txtNFixedInterest).attr('id'), false);
                SetTextBoxStatus($(txtNInternationalEquity).attr('id'), false);
                SetTextBoxStatus($(txtNCash).attr('id'), false);
                SetTextBoxStatus($(txtNProperty).attr('id'), false);
                SetTextBoxStatus($(txtNGold).attr('id'), false);
            }
            else {
                $('#<%: chkNonSuperInvestments.ClientID %>').attr('checked', false);
                SetTextBoxStatus($(txtNEquity).attr('id'), true);
                SetTextBoxStatus($(txtNFixedInterest).attr('id'), true);
                SetTextBoxStatus($(txtNInternationalEquity).attr('id'), true);
                SetTextBoxStatus($(txtNCash).attr('id'), true);
                SetTextBoxStatus($(txtNProperty).attr('id'), true);
                SetTextBoxStatus($(txtNGold).attr('id'), true);
            }

        }

        //This function 
        function EnableDisableAssest() {
            //This function is used to Enable disable on Edit Mode
            var getBrowserName = CheckBrowser();
            if (getBrowserName == "Microsoft Internet Explorer") {
                //For  Assesst Tab
                toggleStatusSuperannuationEnableDisbaleIE();
                toggleStatusPensionEnableDisbaleIE();
                toggleStatuschkNonSuperInvestmentsEnableDisableIE();
                //For Action Tab
                toggleActionTabEnableDisbaleForIE();
            }
            else {
                //                //For Assest Tab
                toggleStatusSuperannuatEnableDisableForFireForeChor();
                toggleStatusPensionEnableDisbaleFireFoxChor();
                toggleStatusStatuschkNonSuperInvestmentsEnableDisableFireChor();
                //                //Fort Action Tab
                toggleActionTabEnableDisbaleFireFoxChor();
                //                //For Goal Tab
            }
        }

        function toggleStatusSuperannuatEnableDisableForFireForeChor() {

            var isChecked = document.getElementById("<%: chkSuperannuation.ClientID %>").checked;
            SetTextBoxStatus("<%: txtSEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtSFixedInterest.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtSInternationalEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtSCash.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtSProperty.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtSGold.ClientID %>", !isChecked);
        }

        function toggleStatusSuperannuationEnableDisbaleIE() {

            var isChecked = document.getElementById("<%: chkSuperannuation.ClientID %>").checked;
            SetTextBoxStatus("<%: txtSEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtSFixedInterest.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtSInternationalEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtSCash.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtSProperty.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtSGold.ClientID %>", !isChecked);
        }

        function toggleStatusPensionEnableDisbaleFireFoxChor() {

            var isChecked = document.getElementById("<%: chkPension.ClientID %>").checked;
            SetTextBoxStatus("<%: txtPEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtPFixedInterest.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtPInternationalEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtPCash.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtPProperty.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtPGold.ClientID %>", !isChecked);
        }

        function toggleStatusPensionEnableDisbaleIE() {

            var isChecked = document.getElementById("<%: chkPension.ClientID %>").checked;
            SetTextBoxStatus("<%: txtPEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtPFixedInterest.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtPInternationalEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtPCash.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtPProperty.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtPGold.ClientID %>", !isChecked);
        }

        function toggleStatusStatuschkNonSuperInvestmentsEnableDisableFireChor() {

            var isChecked = document.getElementById("<%: chkNonSuperInvestments.ClientID %>").checked;
            SetTextBoxStatus("<%: txtNEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtNFixedInterest.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtNInternationalEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtNCash.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtNProperty.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtNGold.ClientID %>", !isChecked);
        }

        function toggleStatuschkNonSuperInvestmentsEnableDisableIE() {

            var isChecked = document.getElementById("<%: chkNonSuperInvestments.ClientID %>").checked;
            SetTextBoxStatus("<%: txtNEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtNFixedInterest.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtNInternationalEquity.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtNCash.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtNProperty.ClientID %>", !isChecked);
            SetTextBoxStatus("<%: txtNGold.ClientID %>", !isChecked);
        }

        function toggleActionTabEnableDisbaleForIE() {

            SetTextBoxStatus("<%: txtSGCPercentSalary.ClientID %>", !document.getElementById("<%: chkSuperGuaranteeContributions.ClientID %>").checked);

            var chkSuperSalarySacrificeContributions = document.getElementById("<%: chkSuperSalarySacrificeContributions.ClientID %>").checked;
            SetTextBoxStatus("<%: txtSSSCPercentSalary.ClientID %>", !chkSuperSalarySacrificeContributions);
            SetTextBoxStatus("<%: txtSSSCAmount.ClientID %>", !chkSuperSalarySacrificeContributions);
            SetTextBoxStatus("<%: txtSSSCStartAge.ClientID %>", !chkSuperSalarySacrificeContributions);
            SetTextBoxStatus("<%: txtSSSCStopAge.ClientID %>", !chkSuperSalarySacrificeContributions);

            var chkSuperAfterTaxContributions = document.getElementById("<%: chkSuperAfterTaxContributions.ClientID %>").checked;
            SetTextBoxStatus("<%: txtSATCAmount.ClientID  %>", !chkSuperAfterTaxContributions);
            SetTextBoxStatus("<%: txtSATCStartAge.ClientID  %>", !chkSuperAfterTaxContributions);
            SetTextBoxStatus("<%: txtSATCStopAge.ClientID  %>", !chkSuperAfterTaxContributions);

            var chkNonSuperInvestmentsSavings = document.getElementById("<%: chkNonSuperInvestmentsSavings.ClientID %>").checked;
            SetTextBoxStatus("<%: txtNSISAmountSaved.ClientID  %>", !chkNonSuperInvestmentsSavings);
            SetTextBoxStatus("<%: txtNSISStartAge.ClientID  %>", !chkNonSuperInvestmentsSavings);
            SetTextBoxStatus("<%: txtNSISStopAge.ClientID  %>", !chkNonSuperInvestmentsSavings);

            if (document.getElementById("<%: chkNonSuperInvestmentsSavings.ClientID %>").checked) {
                var cmbFreq = document.getElementById("<%: cmbFreq.ClientID  %>").value;
                if (cmbFreq != null) {
                    document.getElementById("<%:cmbFreq.ClientID%>").enable = true;
                }
            }
            else {
                document.getElementById("<%:cmbFreq.ClientID%>").disabled = true;
            }
        }

        function toggleActionTabEnableDisbaleFireFoxChor() {

            //Super Guarantee Contributions
            SetTextBoxStatus("<%: txtSGCPercentSalary.ClientID %>", !document.getElementById("<%: chkSuperGuaranteeContributions.ClientID %>").checked);

            //Super Salary Sacrifice Contributions
            var chkSuperSalarySacrificeContributions = document.getElementById("<%: chkSuperSalarySacrificeContributions.ClientID %>").checked;
            SetTextBoxStatus("<%: txtSSSCPercentSalary.ClientID %>", !chkSuperSalarySacrificeContributions);
            SetTextBoxStatus("<%: txtSSSCAmount.ClientID %>", !chkSuperSalarySacrificeContributions);
            SetTextBoxStatus("<%: txtSSSCStartAge.ClientID %>", !chkSuperSalarySacrificeContributions);
            SetTextBoxStatus("<%: txtSSSCStopAge.ClientID %>", !chkSuperSalarySacrificeContributions);

            //Super After Tax Contributions
            var chkSuperAfterTaxContributions = document.getElementById("<%: chkSuperAfterTaxContributions.ClientID %>").checked;
            SetTextBoxStatus("<%: txtSATCAmount.ClientID %>", !chkSuperAfterTaxContributions);
            SetTextBoxStatus("<%: txtSATCStartAge.ClientID %>", !chkSuperAfterTaxContributions);
            SetTextBoxStatus("<%: txtSATCStopAge.ClientID %>", !chkSuperAfterTaxContributions);

            //Non Super Investments (Savings)
            var chkNonSuperInvestmentsSavings = document.getElementById("<%: chkNonSuperInvestmentsSavings.ClientID %>").checked;
            SetTextBoxStatus("<%: txtNSISAmountSaved.ClientID %>", !chkNonSuperInvestmentsSavings);
            SetTextBoxStatus("<%: txtNSISStartAge.ClientID %>", !chkNonSuperInvestmentsSavings);
            SetTextBoxStatus("<%: txtNSISStopAge.ClientID %>", !chkNonSuperInvestmentsSavings);

            if (chkNonSuperInvestmentsSavings) {

                var cmbFreq = window.$find("<%: cmbFreq.ClientID %>");
                if (cmbFreq != null) {
                    window.$find("<%: cmbFreq.ClientID %>").enable();
                }
            }
            else {
                var cmbFreqUncheck = window.$find("<%: cmbFreq.ClientID %>");
                if (cmbFreqUncheck != null) {
                    window.$find("<%: cmbFreq.ClientID %>").disable();

                }
            }
        }


        function SetMinMaxValue(strMinValues, strMaxValues) {

            var txtSsscstartAgeMin = $find("<%: txtSSSCStartAge.ClientID %>");
            if (txtSsscstartAgeMin != null)
                txtSsscstartAgeMin.set_minValue(strMinValues);

            var txtSsscstartAgeMax = $find("<%: txtSSSCStartAge.ClientID %>");
            if (txtSsscstartAgeMax != null)
                txtSsscstartAgeMax.set_maxValue(strMaxValues);

            var txtSsscstopAgeMin = $find("<%: txtSSSCStopAge.ClientID %>");
            if (txtSsscstopAgeMin != null)
                txtSsscstopAgeMin.set_minValue(strMinValues);

            var txtSsscstopAgeMax = $find("<%: txtSSSCStopAge.ClientID %>");
            if (txtSsscstopAgeMax != null)
                txtSsscstopAgeMax.set_maxValue(strMaxValues);

            var txtSatcsStartAgeMin = $find("<%: txtSATCStartAge.ClientID %>");
            if (txtSatcsStartAgeMin != null)
                txtSatcsStartAgeMin.set_minValue(strMinValues);
            var txtSatcstartAgeMax = $find("<%: txtSATCStartAge.ClientID %>");
            if (txtSatcstartAgeMax != null)
                txtSatcstartAgeMax.set_maxValue(strMaxValues);

            var txtSatcstopAgeMin = $find("<%: txtSATCStopAge.ClientID %>");
            if (txtSatcstopAgeMin != null)
                txtSatcstopAgeMin.set_minValue(strMinValues);

            var txtSstcstopAgeMax = $find("<%: txtSATCStopAge.ClientID %>");
            if (txtSstcstopAgeMax != null)
                txtSstcstopAgeMax.set_maxValue(strMaxValues);

            var txtNsisstartAgeMin = $find("<%: txtNSISStartAge.ClientID %>");
            if (txtNsisstartAgeMin != null)
                txtNsisstartAgeMin.set_minValue(strMinValues);

            var txtNsissStartAgeMax = $find("<%: txtNSISStartAge.ClientID %>");
            if (txtNsissStartAgeMax != null)
                txtNsissStartAgeMax.set_maxValue(strMaxValues);

            var txtNsissStopAgeMin = $find("<%: txtNSISStopAge.ClientID %>");
            if (txtNsissStopAgeMin != null)
                txtNsissStopAgeMin.set_minValue(strMinValues);

            var txtNSisstopAgeMax = $find("<%: txtNSISStopAge.ClientID %>");
            if (txtNSisstopAgeMax != null)
                txtNSisstopAgeMax.set_maxValue(strMaxValues);

            //Goal tab set min-max value
            var txtGoalsWealthDollarsFromAgeFooter = $find($('.txtGoalsWealth_DollarsFromAge_Footer').attr('id'));
            if (txtGoalsWealthDollarsFromAgeFooter != null) {

                txtGoalsWealthDollarsFromAgeFooter.set_minValue(strMinValues);
                txtGoalsWealthDollarsFromAgeFooter.set_maxValue(strMaxValues);
            }
            var txtGoalsIncomeDollarsFromAgeFooter = $find($('.txtGoalsIncome_DollarsFromAge_Footer').attr('id'));
            if (txtGoalsIncomeDollarsFromAgeFooter != null) {

                txtGoalsIncomeDollarsFromAgeFooter.set_minValue(strMinValues);
                txtGoalsIncomeDollarsFromAgeFooter.set_maxValue(strMaxValues);
            }
            var txtGoalsLumpSumDollarsFromAgeFooter = $find($('.txtGoalsLumpSum_DollarsFromAge_Footer').attr('id'));
            if (txtGoalsLumpSumDollarsFromAgeFooter != null) {

                txtGoalsLumpSumDollarsFromAgeFooter.set_minValue(strMinValues);
                txtGoalsLumpSumDollarsFromAgeFooter.set_maxValue(strMaxValues);
            }
            var txtGoalsLiquidAssetsDollarsFromAgeFooter = $find($('.txtGoalsLiquidAssets_DollarsFromAge_Footer').attr('id'));
            if (txtGoalsLiquidAssetsDollarsFromAgeFooter != null) {

                txtGoalsLiquidAssetsDollarsFromAgeFooter.set_minValue(strMinValues);
                txtGoalsLiquidAssetsDollarsFromAgeFooter.set_maxValue(strMaxValues);
            }
            var txtGoalsBequestDollarsFromAgeFooter = $find($('.txtGoalsBequest_DollarsFromAge_Footer').attr('id'));
            if (txtGoalsBequestDollarsFromAgeFooter != null) {

                txtGoalsBequestDollarsFromAgeFooter.set_minValue(strMinValues);
                txtGoalsBequestDollarsFromAgeFooter.set_maxValue(strMaxValues);
            }
        }

        function OnBlurPercentSalary() {
            if (GetTextBoxValue("<%: txtSSSCPercentSalary.ClientID %>") != "" && parseFloat(GetTextBoxValue("<%: txtSSSCPercentSalary.ClientID %>")) > 0 && GetElement("<%:chkSuperSalarySacrificeContributions.ClientID %>").checked) {
                var totalSalary = GetTextBoxValue("<%: txtPDSalary.ClientID %>");
                var percent = GetTextBoxValue("<%: txtSSSCPercentSalary.ClientID %>");
                SetTextBoxValue("<%: txtSSSCAmount.ClientID %>", Math.round((totalSalary * 1.0 * percent * 1.0) / 100.0, 2));
            }
            else {
                SetTextBoxValue("<%: txtSSSCAmount.ClientID %>", 0);
            }
        }


        function OnBlurStartAge() {
            var strMinValue = GetElement("<%:hidCurrentAge.ClientID %>").value;
            if (GetElement("<%:chkSuperSalarySacrificeContributions.ClientID %>").checked) {
                if (GetTextBoxValue("<%: txtSSSCStartAge.ClientID %>") == "")
                    SetTextBoxValue("<%: txtSSSCStartAge.ClientID %>", strMinValue);
                else
                    OnBlurSSSCAge();
            }
            //
            if (GetElement("<%:chkSuperAfterTaxContributions.ClientID %>").checked) {
                if (GetTextBoxValue("<%: txtSATCStartAge.ClientID %>") == "")
                    SetTextBoxValue("<%: txtSATCStartAge.ClientID %>", strMinValue);
                else
                    OnBlurSATCAge();
            }
            // 
            if (GetElement("<%:chkNonSuperInvestmentsSavings.ClientID %>").checked) {
                if (GetTextBoxValue("<%: txtNSISStartAge.ClientID %>") == "")
                    SetTextBoxValue("<%: txtNSISStartAge.ClientID %>", strMinValue);
                else
                    OnBlurNSISAge();
            }
        }


        function OnBlurEndAge() {
            var strMaxValue = GetTextBoxValue("<%:txtPDPlanAge.ClientID %>");
            if (GetElement("<%:chkSuperSalarySacrificeContributions.ClientID %>").checked) {
                if (GetTextBoxValue("<%: txtSSSCStopAge.ClientID %>") == "")
                    SetTextBoxValue("<%: txtSSSCStopAge.ClientID %>", strMaxValue);
                else
                    OnBlurSSSCAge();
            }
            //
            if (GetElement("<%:chkSuperAfterTaxContributions.ClientID %>").checked) {
                if (GetTextBoxValue("<%: txtSATCStopAge.ClientID %>") == "")
                    SetTextBoxValue("<%: txtSATCStopAge.ClientID %>", strMaxValue);
                else
                    OnBlurSATCAge();
            }
            // 
            if (GetElement("<%:chkNonSuperInvestmentsSavings.ClientID %>").checked) {
                if (GetTextBoxValue("<%: txtNSISStopAge.ClientID %>") == "")
                    SetTextBoxValue("<%: txtNSISStopAge.ClientID %>", strMaxValue);
                else
                    OnBlurNSISAge();
            }
        }

        function OnBlurSSSCAge() {
            var startAGE = GetTextBoxValue("<%: txtSSSCStartAge.ClientID %>");
            var endAGE = GetTextBoxValue("<%: txtSSSCStopAge.ClientID %>");

            if (startAGE > endAGE)
                SetTextBoxValue("<%: txtSSSCStopAge.ClientID %>", startAGE);
        }

        function OnBlurSATCAge() {
            var startAGE = GetTextBoxValue("<%: txtSATCStartAge.ClientID %>");
            var endAGE = GetTextBoxValue("<%: txtSATCStopAge.ClientID %>");

            if (startAGE > endAGE)
                SetTextBoxValue("<%: txtSATCStopAge.ClientID %>", startAGE);
        }

        function OnBlurNSISAge() {
            var startAGE = GetTextBoxValue("<%: txtNSISStartAge.ClientID %>");
            var endAGE = GetTextBoxValue("<%: txtNSISStopAge.ClientID %>");

            if (startAGE > endAGE)
                SetTextBoxValue("<%: txtNSISStopAge.ClientID %>", startAGE);
        }

        function OnBlurAmount() {
            if (GetTextBoxValue("<%: txtSSSCAmount.ClientID %>") != ""
                && GetTextBoxValue("<%: txtSSSCAmount.ClientID %>") != "0"
                && GetElement("<%:chkSuperSalarySacrificeContributions.ClientID %>").checked) {

                var totalSalary = GetTextBoxValue("<%: txtPDSalary.ClientID %>");
                var totalAmount = GetTextBoxValue("<%: txtSSSCAmount.ClientID %>");
                if (Math.round(((totalAmount * 1.0) / (totalSalary * 1.0) * 100.0), 2) > 1000) {
                    SetTextBoxValue("<%: txtSSSCAmount.ClientID %>", totalSalary * 10);
                    SetTextBoxValue("<%: txtSSSCPercentSalary.ClientID %>", Math.round(1000, 2));
                }
                else {
                    SetTextBoxValue("<%: txtSSSCPercentSalary.ClientID %>", Math.roundNumber(((totalAmount * 1.0) / (totalSalary * 1.0) * 100.0), 2));
                }
            }
            else {
                SetTextBoxValue("<%: txtSSSCPercentSalary.ClientID %>", 0);
            }
            if (GetTextBoxValue("<%: txtSATCAmount.ClientID %>") == "" && GetElement("<%:chkSuperAfterTaxContributions.ClientID %>").checked) {
                SetTextBoxValue("<%: txtSATCAmount.ClientID %>", 1000);
            }
            if (GetTextBoxValue("<%: txtNSISAmountSaved.ClientID %>") == "" && GetElement("<%:chkNonSuperInvestmentsSavings.ClientID %>").checked) {
                SetTextBoxValue("<%: txtNSISAmountSaved.ClientID %>", 1000);
            }
        }


        function ComparePlaneAgeWithCurrent() {
            var getage = getAge(GetTextBoxValue("<%: txtPDDOB.ClientID %>"));
            var checkPlane = GetTextBoxValue("<%: txtPDPlanAge.ClientID %>");
            var checkValidate = checkPlane - getage;
            if (checkValidate <= 0) {
                var makePlaneAge = parseInt(getage, 10) + 1;
                SetTextBoxValue("<%: txtPDPlanAge.ClientID %>", makePlaneAge);
            }
        }

        //Here we call investment stratgeis on drop down change start
        function OnClientSelectedIndexChanged(sender, eventArgs) {
            var selectedItem = eventArgs.get_item();
            var selectedText = selectedItem.get_text();
            GetInvestmentStrategyAssetWeightings(selectedItem.get_value());
        }

        function GetInvestmentStrategyAssetWeightings(InvestmentStrategyID) {
        
            var parameters = "{'InvestmentStrategyID': '" + InvestmentStrategyID + "'}";
            $.ajax({
                type: "POST",
                url: "Advices.aspx/GetInvestmentStrategyAssetWeightings",
                data: parameters,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    
                    var table = document.getElementById('tblInvestmentStrategyAssetWeightings');
                    table.innerHTML = "";

                    if (result.d !== 'Error') {
                        var data = JSON.parse(result.d);
                        PopulateInvestmentStrategyAssetWeightings(data, table);
                    }
                }
            });
        }

        function PopulateInvestmentStrategyAssetWeightings(data, table) {

            //Header Row
            var headerRow = table.insertRow(0);
            var headerCellName = headerRow.insertCell(0);
            headerCellName.innerHTML = '';

            var headerCellWeight = headerRow.insertCell(1);
            headerCellWeight.innerHTML = 'Weight';

            $.each(data, function (key, value) {
                var rowCount = table.rows.length;
                var row = table.insertRow(rowCount);

                var cellName = row.insertCell(0);

                //1. replace underscore with space
                value.Name = value.Name.replace('_', ' ');
                //2. remove AUD
                value.Name = value.Name.replace('AUD', '');

                cellName.innerHTML = value.Name.replace('_', ' ');

                var cellWeight = row.insertCell(1);
                cellWeight.innerHTML = (value.Weight * 100) + '%';
            });
        }
        
        function ShowInvestmentStrategiesPanel(text) {

            switch (text) {
                case "As Per Current Investments":
                    GetElement("<%:palAsCurrentInvestMenet.ClientID %>").ShowElementAsInline();
                    GetElement("<%:pal100Equality.ClientID %>").HideElement();
                    GetElement("<%:palGrothFund.ClientID %>").HideElement();
                    GetElement("<%:PalBalFund.ClientID %>").HideElement();
                    GetElement("<%:PalConservertionFund.ClientID %>").HideElement();
                    break;

                case "100% Equity":
                    GetElement("<%:pal100Equality.ClientID %>").ShowElementAsInline();
                    GetElement("<%:palAsCurrentInvestMenet.ClientID %>").HideElement();
                    GetElement("<%:palGrothFund.ClientID %>").HideElement();
                    GetElement("<%:PalBalFund.ClientID %>").HideElement();
                    GetElement("<%:PalConservertionFund.ClientID %>").HideElement();
                    break;

                case "Growth Fund":
                    GetElement("<%:palGrothFund.ClientID %>").ShowElementAsInline();
                    GetElement("<%:pal100Equality.ClientID %>").HideElement();
                    GetElement("<%:palAsCurrentInvestMenet.ClientID %>").HideElement();
                    GetElement("<%:PalBalFund.ClientID %>").HideElement();
                    GetElement("<%:PalConservertionFund.ClientID %>").HideElement();
                    break;

                case "Balanced Fund":
                    GetElement("<%:PalBalFund.ClientID %>").ShowElementAsInline();
                    GetElement("<%:palGrothFund.ClientID %>").HideElement();
                    GetElement("<%:pal100Equality.ClientID %>").HideElement();
                    GetElement("<%:palAsCurrentInvestMenet.ClientID %>").HideElement();
                    GetElement("<%:PalConservertionFund.ClientID %>").HideElement();
                    break;

                case "Conservative Fund":
                    GetElement("<%:PalConservertionFund.ClientID %>").ShowElementAsInline();
                    GetElement("<%:PalBalFund.ClientID %>").HideElement();
                    GetElement("<%:palGrothFund.ClientID %>").HideElement();
                    GetElement("<%:pal100Equality.ClientID %>").HideElement();
                    GetElement("<%:palAsCurrentInvestMenet.ClientID %>").HideElement();
                    break;

                default:
                    //Visible false
                    GetElement("<%:PalConservertionFund.ClientID %>").HideElement();
                    GetElement("<%:PalBalFund.ClientID %>").HideElement();
                    GetElement("<%:palGrothFund.ClientID %>").HideElement();
                    GetElement("<%:pal100Equality.ClientID %>").HideElement();
                    GetElement("<%:palAsCurrentInvestMenet.ClientID %>").HideElement();
                    break;
            }
        }

        Element.prototype.HideElement = function () {

            this.style.display = 'none';
        };

        Element.prototype.ShowElementAsInline = function () {

            this.style.display = 'inline';
        };

        Element.prototype.ShowElementAsBlock = function () {

            this.style.display = 'block';
        };

        function GetElement(elementId) {

            return document.getElementById(elementId);
        }

        function GetTextBoxValue(textboxId) {
            var txtBox = $find(textboxId);
            if (txtBox != null) {
                if (txtBox.get_value)
                    return txtBox.get_value();
            }

            txtBox = $('#' + textboxId);
            if (txtBox != null)
                return txtBox.val();
        }

        function SetTextBoxValue(textboxId, value) {
            var txtBox = $find(textboxId);
            if (txtBox != null) {
                if (txtBox.set_value) {
                    txtBox.set_value(value);
                    return;
                }
            }
            txtBox = $('#' + textboxId);
            if (txtBox != null)
                txtBox.val(value);
        }

        function SetTextBoxStatus(textboxId, shouldDisable) {

            if ($('#' + textboxId).size() == 0) return;
            if (shouldDisable) {
                $('#' + textboxId).attr('disabled', 'disabled');
                $('#' + textboxId).css('background-color', 'lightgray');
                $('#' + textboxId).addClass('riDisabled');
            }
            else {
                $('#' + textboxId).removeAttr('disabled');
                $('#' + textboxId).css('background-color', 'white');
                $('#' + textboxId).removeClass('riDisabled');
            }

        }

        function CancelGoalGridRowEditMode(eventSource, goalDMLClass) {

            var gridId = $(eventSource).closest('table').attr('Id');

            $('#' + gridId).find('tbody tr').find('.' + goalDMLClass.goalRowDelClass).hide().end().find('.' + goalDMLClass.goalRowDelClass).show();
            $('#' + gridId).find('tbody tr').find('.' + goalDMLClass.goalRowDelClass2).hide().end().find('.' + goalDMLClass.goalRowDelClass2).show();
            $('#' + gridId).find('tbody tr').find('.' + goalDMLClass.goalRowEditClass).hide().end().find('.' + goalDMLClass.goalRowEditClass).show();
            $('#' + gridId).find('tbody tr').find('.' + goalDMLClass.goalRowEditClass2).hide().end().find('.' + goalDMLClass.goalRowEditClass2).show();

            $(eventSource).closest('tr').prev('tr.editModeNormalRow').removeClass('editModeNormalRow').show();
            $(eventSource).closest('tr.editModeFooterRow').remove();
            $('#' + gridId).find('tfoot tr:last').show().find('[DataColumn="Amount"]').select().focus();
        }

        function ShowGoalGridRowInEditMode(eventSource, goalDMLClass) {

            var gridId = $(eventSource).closest('table').attr('Id');
            var currentRow = $(eventSource).closest('tr');
            var amountColumn = "Amount";
            var ageColumn = "Age";
            var amount = $(currentRow).find('.clsGoalsAmount').text();
            var age = $(currentRow).find('.clsGoalsAge').text();
            var footerRow = $('#' + gridId).find('tfoot tr:last').clone(true, true);

            $(footerRow).find('[DataColumn="' + amountColumn + '"]').val(amount);
            $(footerRow).find('[DataColumn="' + ageColumn + '"]').val(age);
            $(footerRow).find('.' + goalDMLClass.goalRowCancelClass).show();
            $(footerRow).find('.' + goalDMLClass.goalRowUpdateClass).show();
            $(footerRow).find('.' + goalDMLClass.goalRowAddClass).hide();

            footerRow.addClass('editModeFooterRow');
            footerRow.insertAfter(currentRow);
            currentRow.addClass('editModeNormalRow').hide();

            $('#' + gridId).find('tbody tr').find('.' + goalDMLClass.goalRowDelClass).hide().end().find('.' + goalDMLClass.goalRowDelClass2).hide();
            $('#' + gridId).find('tbody tr').find('.' + goalDMLClass.goalRowEditClass).hide().end().find('.' + goalDMLClass.goalRowEditClass2).hide();
            $('#' + gridId).find('tfoot tr:last').hide();
            $(footerRow).find('[DataColumn="' + amountColumn + '"]').select().focus();
        }

        function AddWealthGridRow(eventSource) {

            var row = $(eventSource).closest('tr');
            var gridId = '<%: grdGoalsWealth.ClientID %>';
            var sessionKey = $('#<%: hfGoalsWealthSessionKey.ClientID %>').val();
            var amountColumn = "Amount";
            var ageColumn = "Age";
            var amount = $(row).find('[DataColumn="' + amountColumn + '"]').val();
            var age = $(row).find('[DataColumn="' + ageColumn + '"]').val();
            amount = amount.replace(/\,/g, '');

            AddGridRowOnServer(gridId, sessionKey, amountColumn, ageColumn, amount, age, RemoveWealthGridRow, "WEALTH");

            SetTextBoxValue($(row).find('[DataColumn="' + amountColumn + '"]').attr('id'), '');
            SetTextBoxValue($(row).find('[DataColumn="' + ageColumn + '"]').attr('id'), '');
            $(row).find('[DataColumn="' + amountColumn + '"]').focus();

            return false;
        }

        function UpdateWealthGridRow(eventSource) {

            var row = $(eventSource).closest('tr');
            var gridId = '<%: grdGoalsWealth.ClientID %>';
            var sessionKey = $('#<%: hfGoalsWealthSessionKey.ClientID %>').val();
            var guid = $(eventSource).closest('tr').prev().find('td:first span:last').text();
            var amountColumn = "Amount";
            var ageColumn = "Age";
            var amount = $(row).find('[DataColumn="' + amountColumn + '"]').val();
            var age = $(row).find('[DataColumn="' + ageColumn + '"]').val();
            amount = amount.replace(/\,/g, '');

            UpdateGridRowOnServer(gridId, sessionKey, guid, amountColumn, ageColumn, amount, age, RemoveWealthGridRow, "WEALTH");
            $('#' + gridId + ' tfoot tr:last').find('[DataColumn="' + amountColumn + '"]').focus();

            return false;
        }

        function UpdateIncomeGridRow(eventSource) {

            var row = $(eventSource).closest('tr');
            var gridId = '<%: grdGoalsIncome.ClientID %>';
            var sessionKey = $('#<%: hfGoalsIncomeSessionKey.ClientID %>').val();
            var guid = $(eventSource).closest('tr').prev().find('td:first span:last').text();
            var amountColumn = "Amount";
            var ageColumn = "Age";
            var amount = $(row).find('[DataColumn="' + amountColumn + '"]').val();
            var age = $(row).find('[DataColumn="' + ageColumn + '"]').val();
            amount = amount.replace(/\,/g, '');

            UpdateGridRowOnServer(gridId, sessionKey, guid, amountColumn, ageColumn, amount, age, RemoveIncomeGridRow, "INCOME");
            $('#' + gridId + ' tfoot tr:last').find('[DataColumn="' + amountColumn + '"]').focus();

            return false;
        }

        function UpdateLumpSumGridRow(eventSource) {

            var row = $(eventSource).closest('tr');
            var gridId = '<%: grdGoalsLumpSum.ClientID %>';
            var sessionKey = $('#<%: hfGoalsLumpSumSessionKey.ClientID %>').val();
            var guid = $(eventSource).closest('tr').prev().find('td:first span:last').text();
            var amountColumn = "Amount";
            var ageColumn = "Age";
            var amount = $(row).find('[DataColumn="' + amountColumn + '"]').val();
            var age = $(row).find('[DataColumn="' + ageColumn + '"]').val();
            amount = amount.replace(/\,/g, '');

            UpdateGridRowOnServer(gridId, sessionKey, guid, amountColumn, ageColumn, amount, age, RemoveLumpSumGridRow, "LUMPSUM");
            $('#' + gridId + ' tfoot tr:last').find('[DataColumn="' + amountColumn + '"]').focus();

            return false;
        }

        function UpdateLiquidAssetsGridRow(eventSource) {

            var row = $(eventSource).closest('tr');
            var gridId = '<%: grdGoalsLiquidAssets.ClientID %>';
            var sessionKey = $('#<%: hfGoalsLiquidAssetsSessionKey.ClientID %>').val();
            var guid = $(eventSource).closest('tr').prev().find('td:first span:last').text();
            var amountColumn = "Amount";
            var ageColumn = "Age";
            var amount = $(row).find('[DataColumn="' + amountColumn + '"]').val();
            var age = $(row).find('[DataColumn="' + ageColumn + '"]').val();
            amount = amount.replace(/\,/g, '');

            UpdateGridRowOnServer(gridId, sessionKey, guid, amountColumn, ageColumn, amount, age, RemoveLiquidAssetsGridRow, "LIQUIDITY");
            $('#' + gridId + ' tfoot tr:last').find('[DataColumn="' + amountColumn + '"]').focus();

            return false;
        }

        function UpdateBequestGridRow(eventSource) {

            var row = $(eventSource).closest('tr');
            var gridId = '<%: grdGoalsBequest.ClientID %>';
            var sessionKey = $('#<%: hfGoalsBequestSessionKey.ClientID %>').val();
            var guid = $(eventSource).closest('tr').prev().find('td:first span:last').text();
            var amountColumn = "Amount";
            var ageColumn = "Age";
            var amount = $(row).find('[DataColumn="' + amountColumn + '"]').val();
            var age = $(row).find('[DataColumn="' + ageColumn + '"]').val();
            amount = amount.replace(/\,/g, '');

            UpdateGridRowOnServer(gridId, sessionKey, guid, amountColumn, ageColumn, amount, age, RemoveBequestGridRow, "BEQUEST");
            $('#' + gridId + ' tfoot tr:last').find('[DataColumn="' + amountColumn + '"]').focus();

            return false;
        }
        
        function RemoveWealthGridRow(eventSource) {

            var gridId = '<%: grdGoalsWealth.ClientID %>';
            var sessionKey = $('#<%: hfGoalsWealthSessionKey.ClientID %>').val();
            var guid = $(eventSource).closest('tr').find('td:first span:last').text();
            RemoveGridRowOnServer(gridId, sessionKey, guid, RemoveWealthGridRow, "WEALTH");

            return false;
        }

        function AddIncomeGridRow(eventSource) {

            var row = $(eventSource).closest('tr');
            var gridId = '<%: grdGoalsIncome.ClientID %>';
            var sessionKey = $('#<%: hfGoalsIncomeSessionKey.ClientID %>').val();
            var amountColumn = "Amount";
            var ageColumn = "Age";
            var amount = $(row).find('[DataColumn="' + amountColumn + '"]').val();
            var age = $(row).find('[DataColumn="' + ageColumn + '"]').val();
            amount = amount.replace(/\,/g, '');

            AddGridRowOnServer(gridId, sessionKey, amountColumn, ageColumn, amount, age, RemoveIncomeGridRow, "INCOME");

            SetTextBoxValue($(row).find('[DataColumn="' + amountColumn + '"]').attr('id'), '');
            SetTextBoxValue($(row).find('[DataColumn="' + ageColumn + '"]').attr('id'), '');
            $(row).find('[DataColumn="' + amountColumn + '"]').focus();

            return false;
        }

        function RemoveIncomeGridRow(eventSource) {

            var gridId = '<%: grdGoalsIncome.ClientID %>';
            var sessionKey = $('#<%: hfGoalsIncomeSessionKey.ClientID %>').val();
            var guid = $(eventSource).closest('tr').find('td:first span:last').text();
            RemoveGridRowOnServer(gridId, sessionKey, guid, RemoveIncomeGridRow, "INCOME");

            return false;
        }

        function AddLumpSumGridRow(eventSource) {

            var row = $(eventSource).closest('tr');
            var gridId = '<%: grdGoalsLumpSum.ClientID %>';
            var sessionKey = $('#<%: hfGoalsLumpSumSessionKey.ClientID %>').val();
            var amountColumn = "Amount";
            var ageColumn = "Age";
            var amount = $(row).find('[DataColumn="' + amountColumn + '"]').val();
            var age = $(row).find('[DataColumn="' + ageColumn + '"]').val();
            amount = amount.replace(/\,/g, '');

            AddGridRowOnServer(gridId, sessionKey, amountColumn, ageColumn, amount, age, RemoveLumpSumGridRow, "LUMPSUM");

            SetTextBoxValue($(row).find('[DataColumn="' + amountColumn + '"]').attr('id'), '');
            SetTextBoxValue($(row).find('[DataColumn="' + ageColumn + '"]').attr('id'), '');
            $(row).find('[DataColumn="' + amountColumn + '"]').focus();

            return false;
        }

        function RemoveLumpSumGridRow(eventSource) {

            var gridId = '<%: grdGoalsLumpSum.ClientID %>';
            var sessionKey = $('#<%: hfGoalsLumpSumSessionKey.ClientID %>').val();
            var guid = $(eventSource).closest('tr').find('td:first span:last').text();
            RemoveGridRowOnServer(gridId, sessionKey, guid, RemoveLumpSumGridRow, "LUMPSUM");

            return false;
        }

        function AddLiquidAssetsGridRow(eventSource) {

            var row = $(eventSource).closest('tr');
            var gridId = '<%: grdGoalsLiquidAssets.ClientID %>';
            var sessionKey = $('#<%: hfGoalsLiquidAssetsSessionKey.ClientID %>').val();
            var amountColumn = "Amount";
            var ageColumn = "Age";
            var amount = $(row).find('[DataColumn="' + amountColumn + '"]').val();
            var age = $(row).find('[DataColumn="' + ageColumn + '"]').val();
            amount = amount.replace(/\,/g, '');

            AddGridRowOnServer(gridId, sessionKey, amountColumn, ageColumn, amount, age, RemoveLiquidAssetsGridRow, "LIQUIDITY");

            SetTextBoxValue($(row).find('[DataColumn="' + amountColumn + '"]').attr('id'), '');
            SetTextBoxValue($(row).find('[DataColumn="' + ageColumn + '"]').attr('id'), '');
            $(row).find('[DataColumn="' + amountColumn + '"]').focus();

            return false;
        }

        function RemoveLiquidAssetsGridRow(eventSource) {

            var gridId = '<%: grdGoalsLiquidAssets.ClientID %>';
            var sessionKey = $('#<%: hfGoalsLiquidAssetsSessionKey.ClientID %>').val();
            var guid = $(eventSource).closest('tr').find('td:first span:last').text();
            RemoveGridRowOnServer(gridId, sessionKey, guid, RemoveLiquidAssetsGridRow, "LIQUIDITY");

            return false;
        }

        function AddBequestGridRow(eventSource) {

            var row = $(eventSource).closest('tr');
            var gridId = '<%: grdGoalsBequest.ClientID %>';
            var sessionKey = $('#<%: hfGoalsBequestSessionKey.ClientID %>').val();
            var amountColumn = "Amount";
            var ageColumn = "Age";
            var amount = $(row).find('[DataColumn="' + amountColumn + '"]').val();
            var age = $(row).find('[DataColumn="' + ageColumn + '"]').val();
            amount = amount.replace(/\,/g, '');

            AddGridRowOnServer(gridId, sessionKey, amountColumn, ageColumn, amount, age, RemoveBequestGridRow, "BEQUEST");

            SetTextBoxValue($(row).find('[DataColumn="' + amountColumn + '"]').attr('id'), '');
            SetTextBoxValue($(row).find('[DataColumn="' + ageColumn + '"]').attr('id'), '');
            $(row).find('[DataColumn="' + amountColumn + '"]').focus();

            return false;
        }

        function RemoveBequestGridRow(eventSource) {

            var gridId = '<%: grdGoalsBequest.ClientID %>';
            var sessionKey = $('#<%: hfGoalsBequestSessionKey.ClientID %>').val();
            var guid = $(eventSource).closest('tr').find('td:first span:last').text();
            RemoveGridRowOnServer(gridId, sessionKey, guid, RemoveBequestGridRow, "BEQUEST");

            return false;
        }

        function AddGridRowOnServer(gridId, sessionKey, amountColumn, ageColumn, amount, age, removeRowCallback, goalType) {
            var parameters = "{'key': '" + sessionKey + "', 'amountColumn' : '" + amountColumn + "', 'ageColumn' : '" + ageColumn + "', 'amount' : " + amount + ", 'age' : " + age + "}";
            $.ajax({
                type: "POST",
                url: "Advices.aspx/AddGridRow",
                data: parameters,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    OnGoalsActionSuccess(msg, gridId, removeRowCallback, ShowGoalGridRowInEditMode, "Add", goalType);
                }
            });

        }

        function UpdateGridRowOnServer(gridId, sessionKey, guid, amountColumn, ageColumn, amount, age, removeRowCallback, goalType) {

            var parameters = "{'key': '" + sessionKey + "', 'guid' : '" + guid + "', 'amountColumn' : '" + amountColumn + "', 'ageColumn' : '" + ageColumn + "', 'amount' : " + amount + ", 'age' : " + age + "}";
            $.ajax({
                type: "POST",
                url: "Advices.aspx/UpdateGridRow",
                data: parameters,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    OnGoalsActionSuccess(msg, gridId, removeRowCallback, ShowGoalGridRowInEditMode, "Update", goalType);
                }
            });
        }

        function RemoveGridRowOnServer(gridId, sessionKey, guid, removeRowCallback, goalType) {

            var parameters = "{'key': '" + sessionKey + "', 'guid' : '" + guid + "'}";
            $.ajax({
                type: "POST",
                url: "Advices.aspx/RemoveGridRow",
                data: parameters,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    OnGoalsActionSuccess(msg, gridId, removeRowCallback, ShowGoalGridRowInEditMode, "Remove", goalType);
                }
            });

        }

        function OnGoalsActionSuccess(msg, gridId, removeRowCallback, editRowCallback, actionType, goalType) {

            if (msg.d == "error" && actionType == "Remove")
                alert('There was some problem while removing row on the server.');
            else if (msg.d == "error" && actionType == "Add")
                alert('There was some problem while adding row on the server.');
            else if (msg.d == "error" && actionType == "Update")
                alert('There was some problem while updating row on the server.');
            else {
                var goalDMLClass = GetGoalDMLObject(goalType);
                var gridDataSource = eval("(" + msg.d + ")");

                $('#' + gridId + ' tbody').empty();
                $('#gridTemplate').tmpl(gridDataSource).appendTo($('#' + gridId + ' tbody'));
                $('#' + gridId + ' tbody tr:odd').addClass('rgRow');
                $('#' + gridId + ' tbody tr:even').addClass('rgAltRow');
                $('#' + gridId + ' tbody tr').find('.' + goalDMLClass.goalRowDelClass2).bind('click', function () {
                    removeRowCallback(this);
                    return false;
                });

                $('#' + gridId + ' tbody tr').find('.' + goalDMLClass.goalRowEditClass2).bind('click', function () {
                    editRowCallback(this, goalDMLClass);
                    return false;
                });

                $('#' + gridId + ' tfoot tr:last').show();
            }
        }

        function GetGoalDMLObject(goalType) {

            var goalDML = {
                goalRowAddClass: '',
                goalRowDelClass: '',
                goalRowDelClass2: '',
                goalRowEditClass: '',
                goalRowEditClass2: '',
                goalRowUpdateClass: '',
                goalRowCancelClass: ''
            };

            if (goalType.toUpperCase() == "WEALTH") {

                goalDML.goalRowAddClass = 'clsGoalsWealthGridRowAdd';
                goalDML.goalRowDelClass = 'clsGoalsWealthGridRowRemove';
                goalDML.goalRowEditClass = 'clsGoalsWealthGridRowEdit';
                goalDML.goalRowUpdateClass = 'clsGoalsWealthGridRowUpdateEdit';
                goalDML.goalRowCancelClass = 'clsGoalsWealthGridRowCancelEdit';
            }
            else if (goalType.toUpperCase() == "INCOME") {

                goalDML.goalRowAddClass = 'clsGoalsIncomeGridRowAdd';
                goalDML.goalRowDelClass = 'clsGoalsIncomeGridRowRemove';
                goalDML.goalRowEditClass = 'clsGoalsIncomeGridRowEdit';
                goalDML.goalRowUpdateClass = 'clsGoalsIncomeGridRowUpdateEdit';
                goalDML.goalRowCancelClass = 'clsGoalsIncomeGridRowCancelEdit';
            }

            else if (goalType.toUpperCase() == "LUMPSUM") {

                goalDML.goalRowAddClass = 'clsGoalsLumpSumGridRowAdd';
                goalDML.goalRowDelClass = 'clsGoalsLumpSumGridRowRemove';
                goalDML.goalRowEditClass = 'clsGoalsLumpSumGridRowEdit';
                goalDML.goalRowUpdateClass = 'clsGoalsLumpSumGridRowUpdateEdit';
                goalDML.goalRowCancelClass = 'clsGoalsLumpSumGridRowCancelEdit';
            }
            else if (goalType.toUpperCase() == "LIQUIDITY") {

                goalDML.goalRowAddClass = 'clsGoalsLiquidAssetsGridRowAdd';
                goalDML.goalRowDelClass = 'clsGoalsLiquidAssetsGridRowRemove';
                goalDML.goalRowEditClass = 'clsGoalsLiquidityGridRowEdit';
                goalDML.goalRowUpdateClass = 'clsGoalsLiquidityGridRowUpdateEdit';
                goalDML.goalRowCancelClass = 'clsGoalsLiquidityGridRowCancelEdit';
            }
            else if (goalType.toUpperCase() == "BEQUEST") {

                goalDML.goalRowAddClass = 'clsGoalsBequestGridRowAdd';
                goalDML.goalRowDelClass = 'clsGoalsBequestGridRowRemove';
                goalDML.goalRowEditClass = 'clsGoalsBequestGridRowEdit';
                goalDML.goalRowUpdateClass = 'clsGoalsBequestGridRowUpdateEdit';
                goalDML.goalRowCancelClass = 'clsGoalsBequestGridRowCancelEdit';
            }
                
            goalDML.goalRowEditClass2 = 'clsGoalsGridRowEdit';
            goalDML.goalRowDelClass2 = 'clsGoalsGridRowRemove';

            return goalDML;
        }

        function ClearAllRuns_Click(button, args) {
            if (window.confirm("Are you sure you want to clear all Runs?")) {
                button.set_autoPostBack(true);
            }
            else {
                button.set_autoPostBack(false);
            }
        }

        $('#tabs').click('tabsselect', function (event, ui) {
            var selectedTab = $("#tabs").tabs('option', 'active');
            $('#<%: hfCurrentGraphTab.ClientID %>').val(selectedTab);
        });

        var tabIndex = $('#<%: hfCurrentGraphTab.ClientID %>').val();
        if (tabIndex != undefined && tabIndex != '') {
            switch (tabIndex) {
                case "0":
                    document.getElementById("lnkAssets").click();
                    break;
                case "1":
                    document.getElementById("lnkIncome").click();
                    break;
                case "2":
                    document.getElementById("lnkProbability").click();
                    break;
                case "3":
                    document.getElementById("lnkGoals").click();
                    break;
                case "4":
                    document.getElementById("lnkResults").click();
                    break;
            }
        }
        else
            $("#tabs").tabs();
       
    </script>
    <script id="gridTemplate" type="text/x-jquery-tmpl">
        <tr dataitemrow="1">
            <td class="gridItemRow" align="right">
                <span class="clsGoalsAmount">${Amount}</span>
                <span style="display: none">${GoalId}</span>
            </td>
            <td align="right">
                <span  class="clsGoalsAge">${Age}</span>
            </td>
        
            <td align="center">
                <input type="image" src="../images/OBP/delete_octagon.png" title="Remove Row" class="clsGoalsGridRowRemove">
                <input type="image" src="../images/OBP/edit2-icon.png" title="Edit Row" class="clsGoalsGridRowEdit">
            </td>
        </tr>
    </script>
</asp:Content>
