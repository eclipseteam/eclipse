﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI.WebControls;
using eclipseonlineweb.OS.Milliman.Actions;
using eclipseonlineweb.OS.Milliman.Client;
using eclipseonlineweb.OS.Milliman.ValueLists;
using System.Linq;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using OBPCommon = Oritax.TaxSimp.Common;
using System.Web.UI;
using Newtonsoft.Json;
using System.Collections;

namespace eclipseonlineweb.OBP
{
    public partial class AdviceListing : UMABasePage
    {
        private DataView _advicesView;
        private Guid _cid = new Guid();

        #region PageEvent

        public override void LoadPage()
        {
            //HttpContext.Current.Session["PROJECTIONS"]=null;
            HttpContext.Current.Session["MILLIMANRESULTSET"] = null;
            _cid = Guid.Parse(Request.Params["ins"].ToString().Replace("?ins=", string.Empty));
            if (!IsPostBack)
            {
                hrefAddNewAdvice.NavigateUrl = string.Format("Advices.aspx?ins={0}", Request.Params["ins"].ToString());
                GetAdvices();
            }
            Session["AdvicesListingURL"] = null;
        }

        #endregion

        #region Grid Events

        protected void AdvicesGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetAdvices();
        }

        protected void AdvicesGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            GridDataItem Item = e.Item as GridDataItem;

            var cid = new Guid(Item.GetDataKeyValue("CID").ToString());
            var id = new Guid(Item.GetDataKeyValue("ID").ToString());

            switch (e.CommandName.ToLower())
            {
                case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    OnDeleteAdvice(id);
                    break;
            }
        }

        protected void AdvicesGrid_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                Guid ID = (Guid)item.GetDataKeyValue("ID");
                HyperLink hlink = ((HyperLink)(item["EditAdvice"].Controls[0]));
                String projectionName = item["Title"].Text;

                hlink.NavigateUrl = string.Format("Advices.aspx?ins={0}&id={1}&isedit=1&projectionName={2}", Request.Params["ins"].ToString(), ID, projectionName);
            }
        }
        #endregion

        #region Private Methods

        private void GetAdvices()
        {
            var indiDs = GetIndividualDS();
            SetControlsDefaultValues(indiDs);

            var ds = new AdvicesDS
            {
                CommandType = DatasetCommandTypes.Check,
                Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = this.GetCurrentUser(), },
                Command = (int)WebCommands.GetOrganizationUnitsByType,
                IndividualCID = _cid
            };

            var obpCM = UMABroker.GetWellKnownBMC(WellKnownCM.OBP) ;
            obpCM.GetData(ds);
            UMABroker.ReleaseBrokerManagedComponent(obpCM);

            foreach (DataRow dr in ds.AdvicesTable.Rows)
            {
                if (indiDs.IndividualTable.Rows.Count > 0)
                    dr[ds.AdvicesTable.INDIVIDUALCLIENTID] = indiDs.IndividualTable.Rows[0][indiDs.IndividualTable.CLIENTID];
            }

            SetMillimanResultSet(ds);
            _advicesView = ds.AdvicesTable.DefaultView;
            _advicesView.Sort = "Modifiedon DESC";
            
            AdvicesGrid.DataSource = _advicesView;
        }

        private void SetControlsDefaultValues(IndividualDS ds)
        {
            if (ds.Tables.Count > 0 && ds.IndividualTable.Rows.Count > 0)
            {
                DataRow dr = ds.IndividualTable.Rows[0];

                hfIndividualClientID.Value = dr[ds.IndividualTable.CLID].ToString();
                hfIndividualClientName.Value = dr[ds.IndividualTable.FULLNAME].ToString();
                hfIndividualClientDOB.Value = dr[ds.IndividualTable.DOB].ToString();

                if (!string.IsNullOrEmpty(dr[ds.IndividualTable.GENDER].ToString()))
                {
                    hfIndividualClientGender.Value = dr[ds.IndividualTable.GENDER].ToString();
                }
            }
        }

        private IndividualDS GetIndividualDS()
        {
            var individualcm = UMABroker.GetBMCInstance(_cid);
            IndividualDS ds = new IndividualDS();
            if (individualcm != null)
            {
                ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
                individualcm.GetData(ds);
                UMABroker.ReleaseBrokerManagedComponent(individualcm);
            }
            return ds;
        }

        private void SetMillimanResultSet(AdvicesDS ds)
        {
            if (HttpContext.Current.Session["MILLIMANRESULTSET"] != null)
                ((Hashtable)HttpContext.Current.Session["MILLIMANRESULTSET"]).Clear();

            Hashtable ht = new Hashtable();
            foreach (DataRow dr in ds.AdvicesTable.Rows)
            {
                SortedList<string, DataSet> lst = ((AdvicesRow)dr).MillimanResultSet;
                if (lst == null || lst.Count == 0)
                    continue;
                ht.Add(dr[ds.AdvicesTable.ID], lst);
            }
            HttpContext.Current.Session["MILLIMANRESULTSET"] = ht;
        }

        private void OnDeleteAdvice(Guid AdviceID)
        {
            var ds = new AdvicesDS
            {
                CommandType = DatasetCommandTypes.Delete,
                Unit = new Oritax.TaxSimp.Data.OrganizationUnit
                {
                    CurrentUser = GetCurrentUser(),
                },
            };

            var dr = ds.AdvicesTable.NewRow();
            dr[ds.AdvicesTable.ID] = AdviceID;
            ds.AdvicesTable.Rows.Add(dr);

            SaveWellKnownComponent(ds, WellKnownCM.OBP);
            AdvicesGrid.Rebind();
        }
        #endregion
    }
}