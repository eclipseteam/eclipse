﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JDash;
using JDash.Models;

namespace eclipseonlineweb
{
    /// <summary>
    /// The class is under development.
    /// </summary>
    public partial class ManageDashboards : UMABasePage
    {
        protected override void Intialise()
        {
            base.Intialise();
            this.cdCreateDashboard.DashboardCreated += (isCreated, newDashboard) =>
            {
                if (isCreated)
                {
                    cmbDashboards.DataSource = GetDashboards();
                    cmbDashboards.SelectedValue = newDashboard.id;
                    cmbDashboards.DataBind();
                }
            };
        }

        public override void LoadPage()
        {
            base.LoadPage();
            if (!Page.IsPostBack)
            {
                //listRepeater.DataSource = GetDashboards();
                //listRepeater.DataBind();

                cmbDashboards.DataSource = GetDashboards();
                cmbDashboards.DataBind();
            }
        }

        //protected void createBtn_Click(object sender, EventArgs e)
        //{
        //    var newDashboard = new DashboardModel()
        //    {
        //        title = dashTitle.Text,
        //        share = ShareModel.shared

        //    };

        //    newDashboard.metaData.created = DateTime.Now;
        //    //newDashboard.metaData.createdBy = Session["USER"].ToString();
        //    newDashboard.metaData.shared = DateTime.Now;
        //    newDashboard.metaData.sharedBy = "ali";
        //    newDashboard.authorization.Add(
        //        new KeyValuePair<string, PermissionModel>
        //            ("EditorTeam",
        //            new PermissionModel()
        //            {
        //                authTarget = AuthTarget.roleName,
        //                permission = Permission.edit
        //            }));

        //    JDashManager.Provider.CreateDashboard(newDashboard);

        //    listRepeater.DataSource = GetDashboards();
        //    listRepeater.DataBind();
        //}

        private IEnumerable<DashboardModel> GetDashboards()
        {
            //var query = new DynamicQuery()
            //{
            //    filter = new FilterParam()
            //};

            //query.filter.op = FilterOperator.or;
            //query.filter.filters.Add(new Filter()
            //{
            //    field = "metaData.createdBy",
            //    value = Session["USER"],
            //    op = CompareOperator.eq
            //});

            //query.filter.filters.Add(new Filter()
            //{
            //    field = "metaData.createdBy",
            //    value = "admin",
            //    op = CompareOperator.eq
            //});

            var dashboards = JDashManager.Provider.SearchDashboards().data;
            return dashboards;
        }

        protected void btnCreateDashboard_Click(object sender, EventArgs e)
        {
            this.popupCreateDashboard.Show();

        }

        protected void btnAddDashlet_Click(object sender, EventArgs e)
        {
            this.popupAddDashlets.Show();
        }
    }
}