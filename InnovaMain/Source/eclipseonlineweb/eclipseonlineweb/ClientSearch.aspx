﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeBehind="ClientSearch.aspx.cs" Inherits="eclipseonlineweb.ClientSearch" %>

<%@ Register Src="Controls/BreadCrumb.ascx" TagName="BreadCrumb" TagPrefix="uc1" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        function GoToPage(url) {
            window.location = url;
            return false;
        }
        //Resetting menu index
        localStorage['MenuIndex'] = '0';
    </script>
</asp:Content>
<asp:Content ID="SideMenu" ContentPlaceHolderID="SidebarMenu" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <ul class="menu">
                <li class="item1"><a href="#" onclick="javascript:GoToPage('ClientSearch.aspx');"
                    id="SelectMenu">ACCOUNTS SEARCH </a></li>
                <li class="item2"><a href="#" onclick="javascript:GoToPage('AccountsFUM.aspx');">UMA
                    ACCOUNTS - $FUM </a></li>
                <li class="item3" id="li1" runat="server"><a href="#" onclick="javascript:GoToPage('SuperAccountsFUM.aspx');">
                    E-CLIPSE SUPER - $FUM </a></li>
                 <li class="item2"><a href="#" onclick="javascript:GoToPage('Consolidation.aspx');">CONSOLIDATION</a></li>
            </ul>
            <!--News End-->
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Accounts Search"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <div id="Search">
                <table>
                    <tr>
                        <td>
                            <telerik:RadTextBox DisplayText="Type in Client Name, e-Clipse ID or e-Clipse Super Member ID"
                                Height="32px" runat="server" ID="txtSearchBox" Width="450px">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <asp:Button ID="BtnSearch" runat="server" CssClass="MediumButton" Text="Search" OnClick="BtnSearchClick" />
                        </td>
                    </tr>
                </table>
            </div>
            <div style="padding-top: 50px;">
                <telerik:RadGrid ID="PresentationGrid" Skin="Metro" runat="server" ShowStatusBar="true"
                    AutoGenerateColumns="False" PageSize="20" AllowSorting="false" AllowMultiRowSelection="False"
                    AllowPaging="True" GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="false"
                    AllowAutomaticInserts="True" AllowAutomaticUpdates="True" EnableViewState="true"
                    ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource" OnItemCommand="PresentationGrid_OnItemCommand">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                        Name="Banks" TableLayout="Fixed">
                        <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridButtonColumn ButtonType="LinkButton" CommandName="Select" Text="Select"
                                UniqueName="DetailColumn">
                                <HeaderStyle Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyLinkButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                            <telerik:GridBoundColumn SortExpression="ENTITYCIID_FIELD" ReadOnly="true" HeaderText="ENTITYCIID_FIELD"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ENTITYCIID_FIELD" UniqueName="ENTITYCIID_FIELD"
                                Visible="true" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ClientID" ReadOnly="true" HeaderText="e-Clipse ID"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ClientID" UniqueName="ClientID">
                                <HeaderStyle Width="6%"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="OtherID" ReadOnly="true" HeaderText="Member ID"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="OtherID" UniqueName="OtherID">
                                <HeaderStyle Width="7%"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ENTITYNAME_FIELD" ReadOnly="true" HeaderText="Account Name"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ENTITYNAME_FIELD" UniqueName="ENTITYNAME_FIELD">
                                <HeaderStyle Width="20%"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="AccountType" ReadOnly="true" HeaderText="Account Type"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="AccountType" UniqueName="AccountType">
                                <HeaderStyle Width="10%"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="Status" ReadOnly="true" HeaderText="Status"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="Status" UniqueName="Status">
                                <HeaderStyle Width="6%"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="AdviserFullName" ReadOnly="true" HeaderText="Adviser"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="AdviserFullName" UniqueName="AdviserFullName">
                                <HeaderStyle Width="12%"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="Total" ReadOnly="true" HeaderText="Total Holding"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="Total" UniqueName="Total" DataFormatString="{0:C}">
                                <HeaderStyle HorizontalAlign="Right" Width="8%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn ConfirmText="Are you sure you want to delete this record? Warning the action is not reversible."
                                ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                <HeaderStyle Width="3%" HorizontalAlign="Right"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </div>
            <br />
            <asp:Panel runat="server" ID="pnlAdminFunctions" Visible="false">
                <fieldset>
                    <legend>Admin Functions</legend>
                    <div>
                        <table>
                            <tr>
                                <td style="width: 30%">
                                    <asp:Button ID="btnSetTransactions" runat="server" Text="Set Transactions" OnClick="BtnSetTransaction"
                                        Width="200" />
                                </td>
                                <td>
                                    Ability to fix transactions for all client accounts
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%">
                                    <asp:Button ID="btnExportAdvisers" class="save submit" runat="server" Text="Export Advisers with ID"
                                        OnClick="BtnExportAdvisersClick" Width="200" />
                                </td>
                                <td>
                                    Exports all Advisers with IDs
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%">
                                    <asp:Button ID="btnFixPhoneNumbers" class="save submit" runat="server" Text="Fix Phone Numbers"
                                        OnClick="btnFixPhoneNumbersClick" Width="200" Visible="false" />
                                </td>
                                <td>
                                    Fix Phone Numbers
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clear">
                    </div>
                </fieldset>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlSuperAdmin" Visible="false">
                <br />
                <fieldset>
                    <legend>Super Admin Functions</legend>
                    <div>
                        <table>
                            <%--  <tr>
                                <td style="width: 30%">
                                    <asp:Button ID="btnDeployIDSToDD" class="save submit" runat="server" Text="Deploy IDs to Database"
                                        OnClick="btnDeployIDSToDDClick" Width="200" Visible="true" />
                                </td>
                                <td>
                                    Deploy IDs to Database
                                </td>
                            </tr>--%>
                            <%--<tr>
                                <td style="width: 30%">
                                    <asp:Button ID="btnExportUsersWithPassword" class="save submit" runat="server" Text="Export Users"
                                        OnClick="BtnExportAllUsersWithPassword" Width="200" Visible="true" />
                                </td>
                                <td>
                                    Exports all users with passwords
                                </td>
                            </tr>--%>
                            <tr>
                                <td style="width: 30%">
                                    <asp:Button ID="btnFixIndividualCinstanceName" class="save submit" runat="server"
                                        Text="Fix Individual Names" OnClick="btnFixIndividualCinstanceNameClick" Width="200"
                                        Visible="true" />
                                </td>
                                <td>
                                    Fix Individual Names
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%">
                                    <asp:Button ID="Button5" class="save submit" runat="server" Text="Fix Adviser Names"
                                        OnClick="btnFixAdviserinstanceNameClick" Width="200" Visible="true" />
                                </td>
                                <td>
                                    Fix Advisers Names
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="width: 30%">
                                    <asp:Button ID="Button1" class="save submit" runat="server" Text="Fix e-Clipse Super Names"
                                        OnClick="btnFixEclipseSuperNameClick" Width="200" Visible="true" />
                                </td>
                                <td>
                                    Fix e-Clipse Super Names
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%">
                                    <asp:Button ID="Button8" class="save submit" runat="server" Text="Fix e-Clipse Super DIFM"
                                        OnClick="btnFixEclipseSuperNameDIFM" Width="200" Visible="true" />
                                </td>
                                <td>
                                    Fix e-Clipse Super DFIM
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%">
                                    <asp:Button ID="Button9" class="save submit" runat="server" Text="Fix e-Clipse Super Clear TDs"
                                        OnClick="btnFixEclipseSuperClearTDTrans" Width="200" Visible="true" />
                                </td>
                                <td>
                                    Fix e-Clipse Super Clear TD Transactions
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%">
                                    <asp:Button ID="Button2" class="save submit" runat="server" Text="Fix Super Bank Cust No"
                                        OnClick="btnFixBankCustNo" Width="200" Visible="true" />
                                </td>
                                <td>
                                    Fix Super Bank Cust No
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%">
                                    <asp:Button ID="Button3" class="save submit" runat="server" Text="Set Default Process"
                                        OnClick="btnSetDefaultProcess" Width="200" Visible="true" />
                                </td>
                                <td>
                                    Set Default Process
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%">
                                    <asp:Button ID="Button4" class="save submit" runat="server" Text="Set Bank ID" OnClick="btnBankAccountID"
                                        Width="200" Visible="true" />
                                </td>
                                <td>
                                    Set Bank ID
                                </td>
                            </tr>
                            </tr>
                            <tr>
                                <td style="width: 30%">
                                    <asp:Button ID="Button6" class="save submit" runat="server" Text="Clear Deleted MIS Trans"
                                        OnClick="btnClearDeletedRecordsMIS" Width="200" Visible="true" />
                                </td>
                                <td>
                                    Clear Deleted MIS Trans
                                </td>
                            </tr>

                            <tr>
                                <td style="width: 40%">
                                    <asp:Button ID="Button7" class="save submit" runat="server" Text="Clear Wrong Annual Attachments 2013"
                                        OnClick="deleteWrongAttachments" Width="200" Visible="true" />
                                </td>
                                <td>
                                    Clear Wrong Annual Attachments 2013
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clear">
                    </div>
                </fieldset>
            </asp:Panel>
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
