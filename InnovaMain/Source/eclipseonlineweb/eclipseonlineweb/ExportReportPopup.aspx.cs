﻿using System;
using System.Web;
using System.IO;

namespace eclipseonlineweb
{
    public partial class ExportReportPopup : UMABasePage
    {
        public override void LoadPage()
        {
            base.LoadPage();           
                ExportFiles();

            if (!string.IsNullOrEmpty(Request.Params["filepath"]))
            {
                string filePath = Request.Params["filepath"];
                DownloadFile(filePath, true);
            }
        }

        private void ExportFiles()
        {
            try
            {
                var bytes = Session["ExportFileBytes"] as byte[];
                Session.Remove("ExportFileBytes");
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                HttpContext.Current.Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
                Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
                Response.AppendHeader("Content-Type", "application/" + Request.Params["filetype"]);
                Response.AppendHeader("Content-disposition", "attachment; filename=" + Request.Params["filename"] + "." + Request.Params["filetype"]);
                if (bytes != null) Response.BinaryWrite(bytes);
                Response.Flush();
                Response.End();         
            }
            catch(Exception ex)
            {
                string strScript = "<script>";
                strScript = strScript + "alert('" + ex.Message + "');";
                strScript = strScript + "</script>";
                ClientScript.RegisterStartupScript(GetType(), "ClientScript", strScript);
            }
        }
        //Method Created by Mansoor Ali 05-12-2013
        /// <summary>
        /// Download a file 
        /// </summary>
        /// <param name="filePath">Full file path</param>
        /// <param name="deleteSourceFile">True=Delete Source</param>
        public void DownloadFile(string filePath, bool deleteSourceFile)
        {
            try
            {
                if (File.Exists(filePath))
                {
                    String fullFileName = filePath;
                    string fileName = Path.GetFileName(fullFileName);
                    string contentType;
                    string extension = Path.GetExtension(fullFileName);

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.ClearHeaders();
                    HttpContext.Current.Response.ClearContent();

                    //Set the appropriate ContentType.
                    if (extension == ".pdf")
                        contentType = "Application/pdf";
                    else if (extension == ".zip")
                        contentType = "text/plain";
                    else if (extension == ".csv" || extension == ".txt")
                        contentType = "text/csv";
                    else
                        contentType = "Application/pdf";

                    HttpContext.Current.Response.ContentType = contentType;
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName);
                    HttpContext.Current.Response.WriteFile(fullFileName);
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    //HttpContext.Current.Response.End();

                    if (deleteSourceFile)
                        File.Delete(fullFileName);
                }
                else
                {
                    throw new ApplicationException("Could not find file [" + filePath + "]");
                }
            }
            catch (Exception ex)
            {
                string strScript = "<script>";
                strScript = strScript + "alert('" + ex.Message + "');";
                strScript = strScript + "</script>";
                //Page.RegisterStartupScript("ClientScript", strScript);
                ClientScript.RegisterStartupScript(GetType(), "ClientScript", strScript);
            }
        }
    }
}