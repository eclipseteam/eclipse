﻿<%@ Page Title="e-Clipse Online Portal" EnableViewState="true" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeBehind="AccountsFUM.aspx.cs" Inherits="eclipseonlineweb.AccountsFUM" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Chart"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="breadcrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        function GoToPage(url) {
            window.location = url;
            return false;
        }
        localStorage['MenuIndex'] = '1';
    </script>
</asp:Content>
<asp:Content ID="SideMenu" ContentPlaceHolderID="SidebarMenu" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <ul class="menu">
                <li class="item1"><a href="#" onclick="javascript:GoToPage('ClientSearch.aspx');">ACCOUNTS
                    SEARCH </a></li>
                <li class="item2"><a href="#" onclick="javascript:GoToPage('AccountsFUM.aspx');"
                    id="SelectMenu">UMA ACCOUNTS - $FUM </a></li>
                <li class="item3" id="li1" runat="server"><a href="#" onclick="javascript:GoToPage('SuperAccountsFUM.aspx');">E-CLIPSE SUPER - $FUM </a></li>
                <li class="item2"><a href="#" onclick="javascript:GoToPage('Consolidation.aspx');"
                    id="A1">CONSOLIDATION </a></li>
            </ul>
            <!--News End-->
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        function hintContentPie() {
            return Globalize.format(this.y, "N");
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                            <span class="riLabel">Valuation Date:</span>
                            <telerik:RadDatePicker ID="financialSummaryDate" runat="server">
                            </telerik:RadDatePicker>
                        </td>
                        <td>
                            <span class="riLabel">Service Type:</span><br />
                            <telerik:RadComboBox runat="server" ID="comboBoxServiceType">
                                <Items>
                                    <telerik:RadComboBoxItem Selected="True" Value="all" Text="All" />
                                    <telerik:RadComboBoxItem Value="doitforme" Text="Do It For Me" />
                                    <telerik:RadComboBoxItem Value="doitwithme" Text="Do It With Me" />
                                    <telerik:RadComboBoxItem Value="doityourself" Text="Do It Yourself" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <br />
                            <telerik:RadButton runat="server" ID="BtnDownloadReport" OnClick="DownloadXLS" Text="Download Report">
                            </telerik:RadButton>
                        </td>
                        <td>
                            <br />
                            <telerik:RadButton runat="server" ID="BtnGenerateReport" OnClick="GenerateAccountsFUM"
                                Text="Generate FUM">
                            </telerik:RadButton>
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:breadcrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="UMA Accounts - $FUM"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" ShowFooter="true"
                AutoGenerateColumns="False" PageSize="15" AllowSorting="True" AllowMultiRowSelection="False"
                AllowPaging="True" GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true"
                AllowAutomaticInserts="True" OnItemCommand="PresentationGrid_ItemCommand" AllowAutomaticUpdates="True"
                EnableViewState="true" OnNeedDataSource="PresentationGrid_OnNeedDataSource" OnPreRender="PresentationGrid_OnPreRender"
                EnableLinqExpressions="False">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="ActiveOrders" TableLayout="Fixed">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <FooterStyle HorizontalAlign="Right" />
                    <Columns>
                        <telerik:GridButtonColumn ButtonType="LinkButton" CommandName="Select" Text="Select"
                            UniqueName="DetailColumn">
                            <HeaderStyle Width="4%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyLinkButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn SortExpression="ENTITYCIID_FIELD" ReadOnly="true" HeaderText="ENTITYCIID_FIELD"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="ENTITYCIID_FIELD" UniqueName="ENTITYCIID_FIELD"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="ClientID" ReadOnly="true" HeaderText="e-Clipse ID"
                            FilterControlWidth="75%" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ClientID" UniqueName="ClientID">
                            <HeaderStyle Width="8%"></HeaderStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="ENTITYNAME_FIELD" ReadOnly="true" HeaderText="Account Name"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            FilterControlWidth="75%" HeaderButtonType="TextButton" DataField="ENTITYNAME_FIELD"
                            UniqueName="ENTITYNAME_FIELD">
                            <HeaderStyle Width="20%"></HeaderStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="AccountType" ReadOnly="true" HeaderText="Account Type"
                            FilterControlWidth="75%" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountType" UniqueName="AccountType">
                            <HeaderStyle Width="10%"></HeaderStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="AdviserFullName" ReadOnly="true" HeaderText="Adviser"
                            FilterControlWidth="75%" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" FooterText="Total of all Accounts:"
                            DataField="AdviserFullName" UniqueName="AdviserFullName">
                            <HeaderStyle Width="10%"></HeaderStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="true" Aggregate="Sum" SortExpression="Holding"
                            ShowFilterIcon="true" HeaderText="Settled ($)" FilterControlWidth="75%" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" HeaderButtonType="TextButton" DataField="Holding"
                            UniqueName="Holding" DataFormatString="{0:C}">
                            <HeaderStyle HorizontalAlign="Right" Width="11%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="true" Aggregate="Sum" SortExpression="Unsettled"
                            ShowFilterIcon="true" FilterControlWidth="75%" HeaderText="Unsettled ($)" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" HeaderButtonType="TextButton" DataField="Unsettled"
                            UniqueName="Unsettled" DataFormatString="{0:C}">
                            <HeaderStyle HorizontalAlign="Right" Width="11%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="200px" Aggregate="Sum" FilterControlWidth="75%"
                            ReadOnly="true" SortExpression="Total" ShowFilterIcon="true" HeaderText="Holding ($)"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                            DataField="Total" UniqueName="Total" DataFormatString="{0:C}">
                            <HeaderStyle HorizontalAlign="Right" Width="11%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Status" ReadOnly="true" HeaderText="Status"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            FilterControlWidth="75%" HeaderButtonType="TextButton" DataField="Status" UniqueName="Status">
                            <HeaderStyle Width="6%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn SortExpression="MDAEXECUTIONDATE" ReadOnly="true" PickerType="DatePicker"
                            HeaderText="MDA Exec Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" FilterControlWidth="75%" DataFormatString="{0:dd/MM/yyyy}"
                            FilterDateFormat="dd/MM/yyyy" HeaderButtonType="TextButton" DataField="MDAEXECUTIONDATE"
                            UniqueName="MDAEXECUTIONDATE">
                            <HeaderStyle Width="19%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridDateTimeColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <asp:Panel runat="server" ID="pnlHoldingChart">
        <table width="100%">
            <tr>
                <td style="width: 28%">
                    <c1:C1BarChart runat="server" ID="ClientsCount" Height="240" Width="100%">
                        <Hint>
                            <Content Function="hintContentPie" />
                        </Hint>
                        <Legend Visible="false"></Legend>
                        <Header TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="Account Counts">
                        </Header>
                    </c1:C1BarChart>
                </td>
                <td style="width: 28%">
                    <table width="100%" style="font-size: 10px; background-color: #DBEEF4">
                        <tr>
                        </tr>
                        <tr>
                            <td>Corporation Private
                            </td>
                            <td align="right">
                                <asp:Label ViewStateMode="Disabled" EnableViewState="false" runat="server" ID="lblClientCorporationPrivate"
                                    Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Corporation Public
                            </td>
                            <td align="right">
                                <asp:Label runat="server" ID="lblClientCorporationPublic" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>e-Clipse Super
                            </td>
                            <td align="right">
                                <asp:Label runat="server" ID="lblClientEClipseSuper" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Individual
                            </td>
                            <td align="right">
                                <asp:Label runat="server" ID="lblClientIndividual" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Other Trusts Corporate
                            </td>
                            <td align="right">
                                <asp:Label runat="server" ID="lblClientOtherTrustsCorporate" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Other Trusts Individual
                            </td>
                            <td align="right">
                                <asp:Label runat="server" ID="lblClientOtherTrustsIndividual" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Corporate SMSF Trustee
                            </td>
                            <td align="right">
                                <asp:Label runat="server" ID="lblClientSMSFCorporateTrustee" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Individual SMSF Trustee
                            </td>
                            <td align="right">
                                <asp:Label runat="server" ID="lblClientSMSFIndividualTrustee" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Sole Trader
                            </td>
                            <td align="right">
                                <asp:Label runat="server" ID="lblClientSoleTrader" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">TOTAL FUM$
                            </td>
                            <td align="right" style="font-weight: bold">
                                <asp:Label runat="server" ID="lblFUMTOTAL" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">TOTAL # OF ACCOUNTS
                            </td>
                            <td align="right" style="font-weight: bold">
                                <asp:Label runat="server" ID="lblTotalAccounts" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                        </tr>
                    </table>
                </td>
                <td style="width: 44%">
                    <c1:C1BarChart runat="server" Horizontal="false" ID="BarChartSetteledUnsettled" Height="240"
                        Width="100%">
                        <Legend Visible="true"></Legend>
                        <Hint>
                            <Content Function="hintContent" />
                        </Hint>
                        <Header TextStyle-FontSize="12px" TextStyle-FontWeight="bold" Text="Holdings">
                        </Header>
                    </c1:C1BarChart>
                </td>
            </tr>
            <tr>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
