﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageDashboards.aspx.cs"
    Inherits="eclipseonlineweb.ManageDashboards" MasterPageFile="~/Home.master" %>

<%@ Register TagPrefix="CC" TagName="CreateDashboard" Src="JDash/Controls/CreateDashboard.ascx" %>
<%@ Register tagPrefix="CC" tagName="AddDashlet" src="JDash/Controls/AddDashlets.ascx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <fieldset>
            <table>
                <tr>
                    <td style="width: 250px;">
                        <telerik:RadComboBox ID="cmbDashboards" runat="server" DataValueField="id" DataTextField="title"
                            Label="Dashboards:" Font-Bold="True">
                            <DefaultItem Text="Select a dashboard..."></DefaultItem>
                        </telerik:RadComboBox>
                    </td>
                    <td style="width: 10px;">
                    </td>
                    <td>
                        <telerik:RadButton runat="server" ID="btnCreateDashboard" ToolTip="Create Dashboard"
                            OnClick="btnCreateDashboard_Click" >
                            <ContentTemplate>
                                <span class="riLabel">Create</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                       
                        <asp:ModalPopupExtender runat="server" ID="popupCreateDashboard" CancelControlID="divCloseCreateDashboard"
                            TargetControlID="btnCreateDashboard" BackgroundCssClass="ModalPopupBG"
                            Drag="true" PopupControlID="pnlCreateDashboard" PopupDragHandleControlID="popupHeaderCreateDashboard"  />
                        <asp:Panel runat="server" ID="pnlCreateDashboard" BackColor="White" Style="display: none">
                            <div class="popup_Container" >
                                <div class="popup_Titlebar" id="popupHeaderCreateDashboard">
                                    <div class="TitlebarLeft">
                                        Create Dashboard
                                    </div>
                                    <div id="divCloseCreateDashboard" class="TitlebarRight" >
                                    </div>
                                </div>
                                <div class="popupBody" style="padding: 5px; width: 400px " >
                                    <CC:CreateDashboard ID="cdCreateDashboard" runat="server"  />
                                </div>
                            </div>
                        </asp:Panel>
                    </td>
                    <td style="width: 10px;" />
                    <td>
                        <telerik:RadButton runat="server" ID="btnAddDashlet"
                        OnClick="btnAddDashlet_Click" ToolTip="Add Dashlet in Dashboard">
                            <ContentTemplate>
                                <span class="riLabel">Add Dashlet</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                         <asp:ModalPopupExtender runat="server" ID="popupAddDashlets" CancelControlID="divCloseAddDashlet"
                            TargetControlID="btnAddDashlet" BackgroundCssClass="ModalPopupBG"
                            Drag="true" PopupControlID="pnlAddDashlets" PopupDragHandleControlID="popupHeaderAddDashlet"  />
                        <asp:Panel runat="server" ID="pnlAddDashlets" BackColor="White" Style="display: none">
                            <div class="popup_Container" >
                                <div class="popup_Titlebar" id="popupHeaderAddDashlet">
                                    <div class="TitlebarLeft">
                                        Add Dashlets
                                    </div>
                                    <div id="divCloseAddDashlet" class="TitlebarRight" >
                                    </div>
                                </div>
                                <div class="popupBody" style="padding: 5px; width: 400px " >
                                    <CC:AddDashlet ID="adAddDashlets" runat="server"  />
                                </div>
                            </div>
                        </asp:Panel>
                    </td>
                    <td style="width: 10px;" />
                    <td>
                        <telerik:RadButton runat="server" ID="btnEditDashboard" ToolTip="Edit Dashboard">
                            <ContentTemplate>
                                <span class="riLabel">Edit</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </td>
                    <td style="width: 10px;" />
                    <td>
                        <telerik:RadButton runat="server" ID="btnDeleteDashboard" ToolTip="Delete Dashboard">
                            <ContentTemplate>
                                <span class="riLabel">Delete</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </td>
                    <td style="width: 10px;" />
                    <td>
                        <telerik:RadButton runat="server" ToggleType="CheckBox" ButtonType="LinkButton">
                            <ToggleStates>
                                <telerik:RadButtonToggleState Text="Set as Active Dashboard" PrimaryIconCssClass="rbToggleCheckboxChecked" />
                                <telerik:RadButtonToggleState Text="Set as Active Dashboard" PrimaryIconCssClass="rbToggleCheckbox" />
                            </ToggleStates>
                        </telerik:RadButton>
                    </td>
                </tr>
            </table>
            </fieldset>
            

          <%--  <asp:TextBox ID="dashTitle" runat="server">
            </asp:TextBox>
            <asp:Button ID="createBtn" runat="server" Text="Create Dashboard" />
            <br />
            <asp:Label ID="Label1" Text="Dashboards" runat="server" />
            <br />
            <asp:Repeater runat="server" ID="listRepeater">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" Text='<%# Eval("title") %>'
                        NavigateUrl='<%# string.Format("/MyDashboard.aspx?id={0}", Eval("Id")) %>'></asp:HyperLink>
                    <br />
                </ItemTemplate>
            </asp:Repeater>--%>
            

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
