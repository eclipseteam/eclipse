﻿using System.Web;
using System.Net;

namespace eclipseonlineweb.WebUtilities
{
    public class FileHelper
    {
        public const string CutOffTimeFilePath = "~/Templates/CutOffTime/";
        public const string CutOffTimeFileName = "OrderPadcutofftimes.pdf";

        public static void DownLoadFile(string filePath, string fileName)
        {
            string strUrl = filePath + fileName;
            var req = new WebClient();
            HttpResponse response = HttpContext.Current.Response;
            response.Clear();
            response.ClearContent();
            response.ClearHeaders();
            response.Buffer = true;
            response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
            byte[] data = req.DownloadData(HttpContext.Current.Server.MapPath(strUrl));
            response.BinaryWrite(data);
            response.End();
        }
        
        public static string GetFileName(string path)
        {
            string fileName = string.Empty;
            string[] pathArray = path.Split('\\');
            if (pathArray.Length > 0)
            {
                fileName = pathArray[pathArray.Length - 1];
            }
            return fileName;
        }
    }
}