﻿using System;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using System.Data;
using Oritax.TaxSimp.Commands;

namespace eclipseonlineweb.WebUtilities
{
    public class BrokerUtility
    {
        public static void DeleteOrgUnit(Guid clid, Guid csid, UserEntity currentUser, OrganizationType type, Action<DataSet> callBack)
        {
            var ds = new InstanceCMDS
            {
                CommandType = DatasetCommandTypes.Delete,
                Unit = new OrganizationUnit
                {
                    Clid = clid,
                    Csid = csid,
                    CurrentUser = currentUser,
                    Type = ((int)type).ToString(),
                },
                Command = (int)WebCommands.DeleteOrganizationUnit
            };

            callBack(ds);

        }
    }
}