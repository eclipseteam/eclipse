﻿using System;
using System.Data;
using System.Data.OleDb;
using System.IO;

namespace eclipseonlineweb.WebUtilities
{
    public class ExcelReader
    {

        public static DataTable ReadExeclToDataTable(string filePath)
        {
            string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=Excel 12.0;";

            OleDbConnection con = new OleDbConnection(connectionString);
            con.Open();
            DataTable dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            string sheetName = "Sheet1$";
            if (dt != null && dt.Rows.Count > 0)
            {
                sheetName = dt.Rows[0]["TABLE_NAME"].ToString();
            }

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Connection = con;
            OleDbDataAdapter dAdapter = new OleDbDataAdapter(cmd);
            DataTable dtExcelRecords = new DataTable();

            cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
            dAdapter.SelectCommand = cmd;
            dAdapter.Fill(dtExcelRecords);
            con.Close();
            return dtExcelRecords;
        }

        public static DataSet ReadExeclAllSheets(string filePath)
        {

            SpreadsheetGear.IWorkbook workbook = SpreadsheetGear.Factory.GetWorkbook(filePath);
            DataSet ds = workbook.GetDataSet(SpreadsheetGear.Data.GetDataFlags.None);
           
           // string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=Excel 12.0;";

           // OleDbConnection con = new OleDbConnection(connectionString);
           // con.Open();
           // DataTable dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
           //DataSet ds=new DataSet();
           // if (dt != null)
           // {
           //     OleDbCommand cmd = new OleDbCommand();
           //     cmd.CommandType = System.Data.CommandType.Text;
           //     cmd.Connection = con;
           //     OleDbDataAdapter dAdapter = new OleDbDataAdapter(cmd);
           //     foreach (DataRow dr  in dt.Rows)
           //     {
           //         var sheetName = dr["TABLE_NAME"].ToString();
           //         DataTable dtExcelRecords = new DataTable();
           //         dtExcelRecords.TableName = sheetName.Replace("$","").Replace("'","").Trim();
           //         cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
           //         dAdapter.SelectCommand = cmd;
           //         dAdapter.Fill(dtExcelRecords);
           //         ds.Tables.Add(dtExcelRecords);
           //     }
           //     con.Close();
                
           // }


            return ds;
        }


    }



}