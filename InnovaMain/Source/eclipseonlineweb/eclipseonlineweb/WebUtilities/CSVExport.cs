﻿using System;
using System.Data;
using System.Text;

namespace eclipseonlineweb.WebUtilities
{
    public class CSVExport
    {
        /// <summary>
        /// This will convert the given DataTable to a Csv.
        /// </summary>
        /// <param name="targetData">This is the data to convert.</param>
        /// <returns>This is a Csv list.</returns>
        public static string ConvertToCsv(DataSet targetData)
        {
            string myCsv = "";

            if (targetData == null)
            {
                throw new System.ApplicationException("The given object, targetData, is null.");
            }

            //DataTable myDataTable = targetData.Copy();
            //DataSet myDataSet = new DataSet();
            //myDataSet.Tables.Add(myDataTable);

            //Call a helper.
            myCsv = ConvertToCsvFromDataSet(targetData);

            return myCsv;
        }
        /// <summary>
        /// This will convert the given DataSet to a Csv.
        /// </summary>
        /// <param name="targetData">This is the DataSet to convert, at most 1 DataTable.</param>
        /// <returns>This is a Csv list.</returns>
        public static string ConvertToCsvFromDataSet(DataSet targetData)
        {
            string myCsv = "";

            if (targetData == null)
            {
                throw new System.ApplicationException("The given object, targetData, is null.");
            }

            if (targetData.Tables == null)
            {
                throw new System.ApplicationException("The given object, targetData.Tables, is null.");
            }

            //const int DefaultRequiredTableCount = 1;

            //if (targetData.Tables.Count != DefaultRequiredTableCount)
            //{
            //    throw new System.ApplicationException("ActualTableCount='" + targetData.Tables.Count.ToString() +
            //        "' must equal RequiredTableCount='" + DefaultRequiredTableCount.ToString() + "'.");
            //}

            if (targetData.Tables[0] == null)
            {
                throw new System.ApplicationException("The given object, targetData.Tables[0], is null.");
            }

            if (targetData.Tables[0].Columns == null)
            {
                throw new System.ApplicationException("The given object, targetData.Tables[0].Columns, is null.");
            }

            if (targetData.Tables[0].Rows == null)
            {
                throw new System.ApplicationException("The given object, targetData.Tables[0].Rows, is null.");
            }

            //Get a helper.
            StringBuilder myBuilder = new StringBuilder();

            //Add a row in the Csv that contains the columns-names from the DataTable.

            bool isFirst1 = true;

            foreach (DataColumn myColumnTemp1 in targetData.Tables[0].Columns)
            {
                string myValueTemp1 = "";

                if (isFirst1)
                {
                    isFirst1 = false;
                }
                else
                {
                    myBuilder.Append(",");
                }

                myBuilder.Append("\"");
                myValueTemp1 = myColumnTemp1.ColumnName;
                myValueTemp1 = myValueTemp1.Replace("\"", "\"\"");
                myBuilder.Append(myColumnTemp1.ColumnName);
                myBuilder.Append("\"");
            }

            //Add a row in the Csv for each row in the DataTable.

            foreach (DataRow myRowTemp in targetData.Tables[0].Rows)
            {
                myBuilder.Append(Environment.NewLine);
                bool isFirst2 = true;

                foreach (DataColumn myColumnTemp2 in targetData.Tables[0].Columns)
                {
                    string myValueTemp2 = "";

                    if (isFirst2)
                    {
                        isFirst2 = false;
                    }
                    else
                    {
                        myBuilder.Append(",");
                    }

                    myBuilder.Append("\"");

                    if ((myRowTemp[myColumnTemp2.ColumnName].GetType() == typeof(bool)) ||
                        (myRowTemp[myColumnTemp2.ColumnName].GetType() == typeof(byte)) ||
                        (myRowTemp[myColumnTemp2.ColumnName].GetType() == typeof(char)) ||
                        (myRowTemp[myColumnTemp2.ColumnName].GetType() == typeof(decimal)) ||
                        (myRowTemp[myColumnTemp2.ColumnName].GetType() == typeof(double)) ||
                        (myRowTemp[myColumnTemp2.ColumnName].GetType() == typeof(float)) ||
                        (myRowTemp[myColumnTemp2.ColumnName].GetType() == typeof(int)) ||
                        (myRowTemp[myColumnTemp2.ColumnName].GetType() == typeof(long)) ||
                        (myRowTemp[myColumnTemp2.ColumnName].GetType() == typeof(sbyte)) ||
                        (myRowTemp[myColumnTemp2.ColumnName].GetType() == typeof(short)) ||
                        (myRowTemp[myColumnTemp2.ColumnName].GetType() == typeof(string)) ||
                        (myRowTemp[myColumnTemp2.ColumnName].GetType() == typeof(uint)) ||
                        (myRowTemp[myColumnTemp2.ColumnName].GetType() == typeof(ulong)) ||
                        (myRowTemp[myColumnTemp2.ColumnName].GetType() == typeof(ushort)))
                    {
                        myValueTemp2 = myRowTemp[myColumnTemp2.ColumnName].ToString();
                        myValueTemp2 = myValueTemp2.Replace("\"", "\"\"");
                    }
                    else
                    {
                        myValueTemp2 = "";
                    }

                    myBuilder.Append(myValueTemp2);
                    myBuilder.Append("\"");
                }
            }

            myCsv = myBuilder.ToString();

            return myCsv;
        }
        //public static string ConvertToCsvFromHoldingDataSetDIFM(OrganisationListingDS organisationListingDS)
        //{
        //    StringBuilder csvstring = new StringBuilder();
        //    csvstring.Append("ClientID");
        //    csvstring.Append(",InvestmentCode");
        //    csvstring.Append(",MarketCode");
        //    csvstring.Append(",CurrencyCode");
        //    csvstring.Append(",CashAccountName");
        //    csvstring.Append(",PurchaseDate");
        //    csvstring.Append(",PurchasePrice");
        //    csvstring.Append(",Units");
        //    csvstring.Append(",ReservedUnits");
        //    csvstring.Append(",CostBase");
        //    csvstring.Append(",RebalanceUnits");
        //    csvstring.Append(",AdvisorID");
        //    csvstring.Append("\n");
        //    foreach (DataRow record in organisationListingDS.Tables["Entities_Table"].Rows)
        //    {
        //        csvstring.Append(record.ClientID);
        //        csvstring.Append("," + record.InvestmentCode);
        //        csvstring.Append("," + record.MarketCode);
        //        csvstring.Append("," + record.CurrencyCode);
        //        csvstring.Append("," + record.CashAccountName);
        //        csvstring.Append("," + (record.PurchaseDate.HasValue ? record.PurchaseDate.Value.ToString("dd/MM/yyyy") : ""));
        //        csvstring.Append("," + record.PurchasePrice);
        //        csvstring.Append("," + record.Units);
        //        csvstring.Append("," + record.ReservedUnits);
        //        csvstring.Append("," + record.CostBase);
        //        csvstring.Append("," + record.RebalanceUnits);
        //        csvstring.Append("," + record.AdvisorID);
        //        csvstring.Append("\n");
        //    }
        //    return csvstring.ToString();
        //}

    }
}