﻿namespace eclipseonlineweb.WebUtilities
{
    public static class Tooltip
    {
        public const string ExDividendDate = "The ex dividend date occurs four business days before the company's Record Date. To be entitled to a dividend a shareholder must have purchased the shares before the ex dividend date. If you purchase shares on or after that date, the previous owner of the shares (and not you) is entitled to the dividend. A company's share price may move up as the ex dividend date approaches and then fall after the ex dividend date.";
        public const string BalanceDate = "";
        public const string BooksCloseDate = "The Record Date is 5.00pm on the date a company closes its share register to to determine which shareholders are entitled to receive the current dividend.  It is the date where all changes to registration details must be finalised.";
        public const string PaymentDate = "The Date Payable is the date on which a company's dividend is paid to shareholders.";
        //Order Pad Status Tool Tips
        public const string OrderPadActive = "Order is sent for Validation.";
        public const string OrderPadValidated = "Order is sent for Approval.";
        public const string OrderPadValidationFailed = "ValidationMsg will guide you why the Validation is failed. You can either correct the problem and run Validation again, or cancel this order.";
        public const string OrderPadApproved = "Once the order is Exported, it will be marked Submitted.";
        public const string OrderPadSubmitted = "Exported transactions can be seen in Order Details tab. Once confirmation is received, order will be completed.";
        public const string OrderPadCompleted = "All transactions are settled after receiving their confirmations. Financial Summary is updated.";
        public const string OrderPadCancelled = "You can either Delete this order or make it Active again.";
        //Order Pad ASX Manual Buy/Sell Tool Tip
        public const string OrderPadManualASX = "This order will be placed with the broker based on the instruction received below. \nPlease note that the price used to value your investment is the latest price held on our system which may differ from the actual price of the security when the trade is placed on the market.";
        public const string OrderPadManualASXBuy = "This order will be placed with the broker based on the instruction received below. Please note that the price used to value your investment is the latest price held on our system which may differ from the actual price of the security when the trade is placed on the market. These amounts exclude Brokerage Charges, these will be added to the trade when it is placed.";
        public const string OrderPadBuyAtCall = "Please note that these are honeymoon rates for first time investors only. A honeymoon rate is on offer for a few months. At the end of this period the rate will drop to the standard variable rate";
        //BuyAtCallAccount
        public static string BuyAtCallAccount(string nomonth, bool islineBreak = false)
        {
            //return "Please note that this is a honeymoon rate for first time investors only. The honeymoon rate is on offer for " + nomonth + " months." + LineBreak + "At the end of this period, the rate will drop to the standard variable rate.";
            return string.Format("{0} {1} {2}{3}{4}",
                                 "Please note that this is a honeymoon rate for first time investors only. The honeymoon rate is on offer for",
                                 nomonth, "months.", islineBreak ? "<br/>" : "",
                                 "At the end of this period, the rate will drop to the standard variable rate.");
        }



    }
}