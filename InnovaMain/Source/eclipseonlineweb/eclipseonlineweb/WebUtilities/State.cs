﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eclipseonlineweb.WebUtilities
{
    public class State
        {

            public string Code { get; set; }
            public string Name { get; set; }
            public string CountryCode { get; set; }
            public string ShortName { get; set; }

        }

        public class States : List<State>
        {

            public States(string countryCode)
            {
                this.Clear();
                switch (countryCode)
                {
                    case "Australia":
                        this.Add(new State { Code = "ACT", ShortName = "ACT", Name = "Australian Capital Territory", CountryCode = countryCode });
                        this.Add(new State { Code = "NSW", ShortName = "NSW", Name = "New South Wales", CountryCode = countryCode });
                        this.Add(new State { Code = "NT", ShortName = "NT", Name = "Northern Territory", CountryCode = countryCode });
                        this.Add(new State { Code = "QLD", ShortName = "QLD", Name = "Queensland", CountryCode = countryCode });
                        this.Add(new State { Code = "SA", ShortName = "SA", Name = "South Australia", CountryCode = countryCode });
                        this.Add(new State { Code = "TAS", ShortName = "TAS", Name = "Tasmania", CountryCode = countryCode });
                        this.Add(new State { Code = "VIC", ShortName = "VIC", Name = "Victoria", CountryCode = countryCode });
                        this.Add(new State { Code = "WA", ShortName = "WA", Name = "Western Australia", CountryCode = countryCode });
                        break;
                    default:
                        this.Add(new State { Code = "OTHER", Name = "Other", CountryCode = countryCode });
                        break;
                }

            }


        }
    }
