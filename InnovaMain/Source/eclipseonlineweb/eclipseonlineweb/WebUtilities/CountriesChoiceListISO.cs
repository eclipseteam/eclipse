﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eclipseonlineweb.WebUtilities
{
    public class CountriesChoiceListISO
    {
        public const string NONE = "";
        public const string AFGHANISTAN = "AFG";
        public const string ALBANIA = "ALB";
        public const string AlANDISLANDS = "ALA";
        public const string ALGERIA = "DZA";
        public const string AMERICAN_SAMOA = "ASM";
        public const string ANDORRA = "AND";
        public const string ANGOLA = "AGO";
        public const string ANGUILLA = "AIA";
        public const string ANTARCTICA = "ATA";
        public const string ANTIGUA_AND_BARBUDA = "ATG";
        public const string ARGENTINA = "ARG";
        public const string ARMENIA = "ARM";
        public const string ARUBA = "ABW";
        public const string AUSTRALIA = "AUS";
        public const string AUSTRIA = "AUT";
        public const string AZERBAIJAN = "AZE";
        public const string BAHAMAS = "BHS";
        public const string BAHRAIN = "BHR";
        public const string BANGLADESH = "BGD";
        public const string BARBADOS = "BRB";
        public const string BELARUS = "BLR";
        public const string BELGIUM = "BEL";
        public const string BELIZE = "BLZ";
        public const string BENIN = "BEN";
        public const string BERMUDA = "BMU";
        public const string BHUTAN = "BTN";
        public const string BOLIVIA = "BOL";
        public const string BOSNIA_AND_HERZEGOVINA = "BIH";
        public const string BOTSWANA = "BWA";
        public const string BOUVET_ISLAND = "BVT";

        public const string BRAZIL = "BRA";
        public const string BRITISH_INDIAN_OCEAN_TERRITORY = "IOT";
        public const string BRITISH_VIRGIN_ISLANDS = "VGB";
        public const string BRUNEI_DARUSSALAM = "BRN";
        public const string BULGARIA = "BGR";
        public const string BURKINA_FASO = "BFA";
        public const string BURUNDI = "BDI";
        public const string CAMBODIA = "KHM";
        public const string CAMEROON = "CMR";
        public const string CANADA = "CAN";
        public const string CAPE_VERDE = "CPV";
        public const string CAYMAN_ISLANDS = "CYM";
        public const string CENTRAL_AFRICAN_REP = "CAF";
        public const string CHAD = "TCD";
        public const string CHILE = "CHL";
        public const string CHINA = "CHN";
        public const string CHRISTMAS_ISLAND = "CXR";
        public const string COCOS_KEELING_ISLANDS = "CCK";
        public const string HONG_KONG = "HKG";
        public const string CONGODEMOCRATEREPOFZARIE = "COD";
        public const string MACAO = "MAC";
        public const string COLOMBIA = "COL";
        public const string COMOROS = "COM";
        public const string CONGO = "COG";
        public const string COOK_ISLANDS = "COK";
        public const string COSTA_RICA = "CRI";
        public const string COTE_DIVOIRE = "CIV";
        public const string CROATIA = "HRV";
        public const string CUBA = "CUB";
        public const string CYPRUS = "CYP";
        public const string CZECH_REPUBLIC = "CZE";
        public const string KOREA = "PRK";
        public const string DENMARK = "DNK";
        public const string DJIBOUTI = "DJI";
        public const string DOMINICA = "DMA";
        public const string DOMINICAN_REPUBLIC = "DOM";
        public const string EAST_TIMOR = "TMP";
        public const string ECUADOR = "ECU";
        public const string EGYPT = "EGY";
        public const string EL_SALVADOR = "SLV";
        public const string EQUATORIAL_GUINEA = "GNQ";
        public const string ERITREA = "ERI";
        public const string ESTONIA = "EST";
        public const string ETHIOPIA = "ETH";
        public const string FAEROE_ISLANDS = "FRO";
        public const string FALKLAND_ISLANDS = "FLK";
        public const string FIJI = "FJI";
        public const string FINLAND = "FIN";
        public const string FRANCE = "FRA";
        public const string FRANCE_METROPOLITAN = "FXX";
        public const string FRENCH_GUIANA = "GUF";
        public const string FRENCH_POLYNESIA = "PYF";
        public const string FRENCH_SOUTHERN_TERRITORIES = "ATF";
        public const string GABON = "GAB";
        public const string GAMBIA = "GMB";
        public const string GEORGIA = "GEO";
        public const string GERMANY = "DEU";
        public const string GHANA = "GHA";
        public const string GIBRALTAR = "GIB";
        public const string GREECE = "GRC";
        public const string GREENLAND = "GRL";
        public const string GRENADA = "GRD";
        public const string GUADELOUPE = "GLP";
        public const string GUAM = "GUM";
        public const string GUATEMALA = "GTM";
        public const string GUERNSEY = "GGY";
        public const string GUINEA = "GIN";
        public const string GUINEA_BISSAU = "GNB";
        public const string GUYANA = "GUY";
        public const string HAITI = "HTI";
        public const string HEARD_AND_MC_DONALD_ISLANDS = "HMD";
        public const string HOLY_SEE = "VAT";
        public const string HONDURAS = "HND";
        public const string HUNGARY = "HUN";
        public const string ICELAND = "ISL";
        public const string INDIA = "IND";
        public const string INDONESIA = "IDN";
        public const string IRAN = "IRN";
        public const string IRAQ = "IRQ";
        public const string IRELAND = "IRL";
        public const string ISLE_OF_MAN = "IMN";
        public const string ISRAEL = "ISR";
        public const string ITALY = "ITA";
        public const string JAMAICA = "JAM";
        public const string JAPAN = "JPN";
        public const string JERSEY = "JEY";
        public const string JORDAN = "JOR";
        public const string KAZAKHSTAN = "KAZ";
        public const string KENYA = "KEN";
        public const string KIRIBATI = "KIR";
        public const string KUWAIT = "KWT";
        public const string KYRGYZSTAN = "KGZ";
        public const string LAO = "LAO";
        public const string LATVIA = "LVA";
        public const string LEBANON = "LBN";
        public const string LESOTHO = "LSO";
        public const string LIBERIA = "LBR";
        public const string LIBYAN_ARAB_JAMAHIRIYA = "LBY";
        public const string LIECHTENSTEIN = "LIE";
        public const string LITHUANIA = "LTU";
        public const string LUXEMBOURG = "LUX";
        public const string MADAGASCAR = "MDG";
        public const string MALAWI = "MWI";
        public const string MALAYSIA = "MYS";
        public const string MALDIVES = "MDV";
        public const string MALI = "MLI";
        public const string MALTA = "MLT";
        public const string MARSHALL_ISLANDS = "MHL";
        public const string MARTINIQUE = "MTQ";
        public const string MAURITANIA = "MRT";
        public const string MAURITIUS = "MUS";
        public const string MAYOTTE = "MYT";
        public const string MEXICO = "MEX";
        public const string MICRONESIA = "FSM";
        public const string MONACO = "MCO";
        public const string MONGOLIA = "MNG";
        public const string MONTSERRAT = "MSR";
        public const string MOROCCO = "MAR";
        public const string MOZAMBIQUE = "MOZ";
        public const string MYANMAR = "MMR";
        public const string NAMIBIA = "NAM";
        public const string NAURU = "NRU";
        public const string NEPAL = "NPL";
        public const string NETHERLANDS = "NLD";
        public const string NETHERLANDS_ANTILLES = "ANT";
        public const string NEW_CALEDONIA = "NCL";
        public const string NEW_ZEALAND = "NZL";
        public const string NICARAGUA = "NIC";
        public const string NIGER = "NER";
        public const string NIGERIA = "NGA";
        public const string NIUE = "NIU";
        public const string NORFOLK_ISL = "NFK";
        public const string NTHN_MARIANA_ISL = "MNP";
        public const string NORWAY = "NOR";

        public const string PSEOccupied = "PSE";
        public const string SERBIA = "SRB";
        public const string OMAN = "OMN";
        public const string PAKISTAN = "PAK";
        public const string PALAU = "PLW";
        public const string PANAMA = "PAN";
        public const string PAPUA_NEW_GUINEA = "PNG";
        public const string PARAGUAY = "PRY";
        public const string PERU = "PER";
        public const string PHILIPPINES = "PHL";
        public const string PITCAIRN = "PCN";
        public const string POLAND = "POL";
        public const string PORTUGAL = "PRT";
        public const string PUERTO_RICO = "PRI";
        public const string QATAR = "QAT";
        public const string REPUBLIC_OF_KOREA = "KOR";
        public const string REPUBLIC_OF_MOLDOVA = "MDA";
        public const string REUNION = "REU";
        public const string ROMANIA = "ROU";
        public const string RUSSIAN_FEDERATION = "RUS";
        public const string RWANDA = "RWA";
        public const string ST_HELENA = "SHN";
        public const string ST_KITTS_AND_NEVIS = "KNA";
        public const string ST_LUCIA = "LCA";
        public const string ST_PIERRE_AND_MIQUELON = "SPM";
        public const string ST_VINCENT_GRENADINES = "VCT";
        public const string SAMOA = "WSM";
        public const string SAN_MARINO = "SMR";
        public const string SAO_TOME_AND_PRINCIPE = "STP";
        public const string SAUDI_ARABIA = "SAU";
        public const string SENEGAL = "SEN";

        public const string SEYCHELLES = "SYC";
        public const string SIERRA_LEONE = "SLE";
        public const string SINGAPORE = "SGP";
        public const string SLOVAKIA = "SVK";
        public const string SLOVENIA = "SVN";
        public const string SOLOMON_ISL = "SLB";
        public const string SOMALIA = "SOM";
        public const string SOUTH_AFRICA = "ZAF";
        public const string SOUTH_GEORGIA_AND_THE_SOUTH_SANDWICH_ISLANDS = "SGS";
        public const string SPAIN = "ESP";
        public const string SRI_LANKA = "LKA";
        public const string SUDAN = "SDN";
        public const string SURINAME = "SUR";
        public const string SVALBARD_JAN_MAYEN_ISL = "SJM";
        public const string SWAZILAND = "SWZ";
        public const string SWEDEN = "SWE";
        public const string SWITZERLAND = "CHE";
        public const string SYRIAN_ARAB_REPUBLIC = "SYR";
        public const string TAIWAN_PROVINCE_OF_CHINA = "TWN";
        public const string TAJIKISTAN = "TJK";
        public const string THAILAND = "THA";
        public const string MACEDONIA = "MKD";
        public const string TIMOR_LESTE = "TLS";
        public const string TOGO = "TGO";
        public const string TOKELAU = "TKL";
        public const string TONGA = "TON";
        public const string TRINIDAD_AND_TOBAGO = "TTO";
        public const string TUNISIA = "TUN";
        public const string TURKEY = "TUR";
        public const string TURKMENISTAN = "TKM";
        public const string TURKS_AND_CAICOS_ISL = "TCA";
        public const string TUVALU = "TUV";
        public const string UGANDA = "UGA";
        public const string UKRAINE = "UKR";
        public const string UNITED_ARAB_EMIRATES = "ARE";
        public const string UK_AND_NORTHERN_IRE = "GBR";
        public const string REPUBLIC_OF_TANZANIA = "TZA";
        public const string UNITED_STATES_OF_AMERICA = "USA";
        public const string UNITED_STATES_MINOR_OUTLYING_ISLANDS = "UMI";
        public const string UNITED_STATES_VIRGIN_ISL = "VIR";
        public const string URUGUAY = "URY";
        public const string UZBEKISTAN = "UZB";
        public const string VANUATU = "VUT";
        public const string VENEZUELA = "VEN";
        public const string VIETNAM = "VNM";
        public const string WALLIS_AND_FUTUNA_ISL = "WLF";
        public const string WESTERN_SAHARA = "ESH";
        public const string YEMEN = "YEM";
        //public const string YUGOSLAVIA			= "YUG";
        //public const string ZAIRE					= "ZAR";
        public const string ZAMBIA = "ZMB";
        public const string ZIMBABWE = "ZWE";

        public const string ST_BARTHELEMY = "BLM";
        public const string ST_MARTIN = "MAF";

        public const string Default = "";

        private string m_strValue;

        public CountriesChoiceListISO()
        {
            this.m_strValue = Default;
        }

        public CountriesChoiceListISO(string strValue)
        {
            this.m_strValue = strValue;
        }

        public static ChoiceNameValue[] NameValuePairs
        {
            get
            {
                return new ChoiceNameValue[]
                           {
                               new ChoiceNameValue("Afghanistan", AFGHANISTAN),
                               new ChoiceNameValue("Albania", ALBANIA),
                               new ChoiceNameValue("Aland Islands", AlANDISLANDS),
                               new ChoiceNameValue("Algeria", ALGERIA),
                               new ChoiceNameValue("American Samoa", AMERICAN_SAMOA),
                               new ChoiceNameValue("Andorra", ANDORRA),
                               new ChoiceNameValue("Angola", ANGOLA),
                               new ChoiceNameValue("Anguilla", ANGUILLA),
                               new ChoiceNameValue("Antarctica", ANTARCTICA),
                               new ChoiceNameValue("Antigua and Barbuda", ANTIGUA_AND_BARBUDA),
                               new ChoiceNameValue("Argentina", ARGENTINA),
                               new ChoiceNameValue("Armenia", ARMENIA),
                               new ChoiceNameValue("Aruba", ARUBA),
                               new ChoiceNameValue("Australia", AUSTRALIA),
                               new ChoiceNameValue("Austria", AUSTRIA),
                               new ChoiceNameValue("Azerbaijan", AZERBAIJAN),
                               new ChoiceNameValue("Bahamas", BAHAMAS),
                               new ChoiceNameValue("Bahrain", BAHRAIN),
                               new ChoiceNameValue("Bangladesh", BANGLADESH),
                               new ChoiceNameValue("Barbados", BARBADOS),
                               new ChoiceNameValue("Belarus", BELARUS),
                               new ChoiceNameValue("Belgium", BELGIUM),
                               new ChoiceNameValue("Belize", BELIZE),
                               new ChoiceNameValue("Benin", BENIN),
                               new ChoiceNameValue("Bermuda", BERMUDA),
                               new ChoiceNameValue("Bhutan", BHUTAN),
                               new ChoiceNameValue("Bolivia", BOLIVIA),
                               new ChoiceNameValue("Bosnia and Herzegovina", BOSNIA_AND_HERZEGOVINA),
                               new ChoiceNameValue("Botswana", BOTSWANA),
                               new ChoiceNameValue("Bouvet Island", BOUVET_ISLAND),
                               new ChoiceNameValue("Brazil", BRAZIL),
                               new ChoiceNameValue("British Indian Ocean Territory", BRITISH_INDIAN_OCEAN_TERRITORY),

                               new ChoiceNameValue("Brunei Darussalam", BRUNEI_DARUSSALAM),
                               new ChoiceNameValue("Bulgaria", BULGARIA),
                               new ChoiceNameValue("Burkina Faso", BURKINA_FASO),
                               new ChoiceNameValue("Burundi", BURUNDI),
                               new ChoiceNameValue("Cambodia", CAMBODIA),
                               new ChoiceNameValue("Cameroon", CAMEROON),
                               new ChoiceNameValue("Canada", CANADA),
                               new ChoiceNameValue("Cape Verde", CAPE_VERDE),
                               new ChoiceNameValue("Cayman Islands", CAYMAN_ISLANDS),
                               new ChoiceNameValue("Central African Republic ", CENTRAL_AFRICAN_REP),
                               new ChoiceNameValue("Chad ", CHAD),
                               new ChoiceNameValue("Chile ", CHILE),
                               new ChoiceNameValue("China ", CHINA),
                               new ChoiceNameValue("Christmas Island ", CHRISTMAS_ISLAND),
                               new ChoiceNameValue("Cocos (Keeling) Islands ", COCOS_KEELING_ISLANDS),
                               new ChoiceNameValue("Congo, Democratic Republic of (was Zaire)", CONGODEMOCRATEREPOFZARIE)
                               ,
                               new ChoiceNameValue("Colombia", COLOMBIA),
                               new ChoiceNameValue("Comoros", COMOROS),
                               new ChoiceNameValue("Congo, Peoples Republic of", CONGO),
                               new ChoiceNameValue("Cook Islands", COOK_ISLANDS),
                               new ChoiceNameValue("Costa Rica", COSTA_RICA),
                               new ChoiceNameValue("Cote D'Ivoire (Ivory Coast)", COTE_DIVOIRE),
                               new ChoiceNameValue("Croatia (Hrvatska)", CROATIA),
                               new ChoiceNameValue("Cuba", CUBA),
                               new ChoiceNameValue("Cyprus", CYPRUS),
                               new ChoiceNameValue("Czech Republic", CZECH_REPUBLIC),


                               new ChoiceNameValue("Denmark", DENMARK),
                               new ChoiceNameValue("Djibouti", DJIBOUTI),
                               new ChoiceNameValue("Dominica", DOMINICA),
                               new ChoiceNameValue("Dominican Republic", DOMINICAN_REPUBLIC),
                               new ChoiceNameValue("Timor-Leste (East Timor)", EAST_TIMOR),
                               new ChoiceNameValue("Ecuador", ECUADOR),
                               new ChoiceNameValue("Egypt", EGYPT),
                               new ChoiceNameValue("El Salvador", EL_SALVADOR),
                               new ChoiceNameValue("Equatorial Guinea", EQUATORIAL_GUINEA),
                               new ChoiceNameValue("Eritrea", ERITREA),
                               new ChoiceNameValue("Estonia", ESTONIA),
                               new ChoiceNameValue("Ethiopia", ETHIOPIA),
                               new ChoiceNameValue("Falkland Islands (Malvinas)", FALKLAND_ISLANDS),
                               new ChoiceNameValue("Faroe Islands", FAEROE_ISLANDS),
                               new ChoiceNameValue("Fiji", FIJI),
                               new ChoiceNameValue("Finland", FINLAND),
                               new ChoiceNameValue("France", FRANCE),
                               new ChoiceNameValue("France, Metropolitan", FRANCE_METROPOLITAN),
                               new ChoiceNameValue("French Guiana", FRENCH_GUIANA),
                               new ChoiceNameValue("French Polynesia", FRENCH_POLYNESIA),
                               new ChoiceNameValue("French Southern Territories", FRENCH_SOUTHERN_TERRITORIES),
                               new ChoiceNameValue("Gabon", GABON),
                               new ChoiceNameValue("Gambia", GAMBIA),
                               new ChoiceNameValue("Georgia", GEORGIA),
                               new ChoiceNameValue("Germany", GERMANY),
                               new ChoiceNameValue("Ghana", GHANA),
                               new ChoiceNameValue("Gibraltar", GIBRALTAR),
                               new ChoiceNameValue("Greece", GREECE),
                               new ChoiceNameValue("Greenland", GREENLAND),
                               new ChoiceNameValue("Grenada", GRENADA),
                               new ChoiceNameValue("Guadeloupe ", GUADELOUPE),
                               new ChoiceNameValue("Guam", GUAM),
                               new ChoiceNameValue("Guatemala", GUATEMALA),
                               new ChoiceNameValue("Guernsey", GUERNSEY),
                               new ChoiceNameValue("Guinea", GUINEA),
                               new ChoiceNameValue("Guinea-Bissau", GUINEA_BISSAU),
                               new ChoiceNameValue("Guyana", GUYANA),
                               new ChoiceNameValue("Haiti", HAITI),
                               new ChoiceNameValue("Heard and McDonald Islands", HEARD_AND_MC_DONALD_ISLANDS),
                               new ChoiceNameValue("Holy See (Vatican City State)", HOLY_SEE),
                               new ChoiceNameValue("Honduras", HONDURAS),
                               new ChoiceNameValue("Hong Kong", HONG_KONG),
                               new ChoiceNameValue("Hungary", HUNGARY),
                               new ChoiceNameValue("Iceland", ICELAND),
                               new ChoiceNameValue("India", INDIA),
                               new ChoiceNameValue("Indonesia", INDONESIA),
                               new ChoiceNameValue("Iran", IRAN),
                               new ChoiceNameValue("Iraq", IRAQ),
                               new ChoiceNameValue("Ireland", IRELAND),
                               new ChoiceNameValue("Isle of Man, The", ISLE_OF_MAN),
                               new ChoiceNameValue("Israel", ISRAEL),
                               new ChoiceNameValue("Italy", ITALY),
                               new ChoiceNameValue("Jamaica", JAMAICA),
                               new ChoiceNameValue("Japan", JAPAN),
                               new ChoiceNameValue("Jersey", JERSEY),
                               new ChoiceNameValue("Jordan", JORDAN),
                               new ChoiceNameValue("Kazakhstan", KAZAKHSTAN),
                               new ChoiceNameValue("Kenya", KENYA),
                               new ChoiceNameValue("Kiribati", KIRIBATI),
                               new ChoiceNameValue("Korea, Democratic People's Republic of (North Korea)", KOREA),
                               new ChoiceNameValue("Korea, Republic of (South Korea)", REPUBLIC_OF_KOREA),
                               new ChoiceNameValue("Kuwait", KUWAIT),
                               new ChoiceNameValue("Kyrgyz", KYRGYZSTAN),
                               new ChoiceNameValue("Laos", LAO),
                               new ChoiceNameValue("Latvia", LATVIA),
                               new ChoiceNameValue("Lebanon", LEBANON),
                               new ChoiceNameValue("Lesotho", LESOTHO),
                               new ChoiceNameValue("Liberia", LIBERIA),
                               new ChoiceNameValue("Libya", LIBYAN_ARAB_JAMAHIRIYA),
                               new ChoiceNameValue("Liechtenstein", LIECHTENSTEIN),
                               new ChoiceNameValue("Lithuania", LITHUANIA),
                               new ChoiceNameValue("Luxembourg", LUXEMBOURG),
                               new ChoiceNameValue("Macau", MACAO),
                               new ChoiceNameValue("Montenegro", MACAO),
                               new ChoiceNameValue("Macedonia, The Former Yugoslav Republic Of", MACEDONIA),
                               new ChoiceNameValue("Madagascar", MADAGASCAR),
                               new ChoiceNameValue("Malawi", MALAWI),
                               new ChoiceNameValue("Malaysia", MALAYSIA),
                               new ChoiceNameValue("Maldives", MALDIVES),
                               new ChoiceNameValue("Mali", MALI),
                               new ChoiceNameValue("Malta", MALTA),
                               new ChoiceNameValue("Marshall Islands", MARSHALL_ISLANDS),
                               new ChoiceNameValue("Martinique", MARTINIQUE),
                               new ChoiceNameValue("Mauritania", MAURITANIA),
                               new ChoiceNameValue("Mauritius", MAURITIUS),
                               new ChoiceNameValue("Mayotte", MAYOTTE),
                               new ChoiceNameValue("Mexico", MEXICO),
                               new ChoiceNameValue("Micronesia, Federated State of", MICRONESIA),
                               new ChoiceNameValue("Moldova", REPUBLIC_OF_MOLDOVA),
                               new ChoiceNameValue("Monaco", MONACO),
                               new ChoiceNameValue("Mongolia", MONGOLIA),
                               new ChoiceNameValue("Montserrat", MONTSERRAT),
                               new ChoiceNameValue("Morocco", MOROCCO),
                               new ChoiceNameValue("Mozambique", MOZAMBIQUE),
                               new ChoiceNameValue("Myanmar (was Burma)", MYANMAR),
                               new ChoiceNameValue("Namibia", NAMIBIA),
                               new ChoiceNameValue("Nauru", NAURU),
                               new ChoiceNameValue("Nepal", NEPAL),
                               new ChoiceNameValue("Netherlands, The", NETHERLANDS),
                               new ChoiceNameValue("Netherlands Antilles", NETHERLANDS_ANTILLES),
                               new ChoiceNameValue("New Caledonia", NEW_CALEDONIA),
                               new ChoiceNameValue("New Zealand", NEW_ZEALAND),
                               new ChoiceNameValue("Nicaragua", NICARAGUA),
                               new ChoiceNameValue("Niger", NIGER),
                               new ChoiceNameValue("Nigeria", NIGERIA),
                               new ChoiceNameValue("Niue", NIUE),
                               new ChoiceNameValue("Norfolk Island", NORFOLK_ISL),
                               new ChoiceNameValue("Northern Mariana Islands", NTHN_MARIANA_ISL),
                               new ChoiceNameValue("Norway", NORWAY),

                               new ChoiceNameValue("Oman", OMAN),
                               new ChoiceNameValue("Palestinian Territory (Occupied)", PSEOccupied),
                               new ChoiceNameValue("Pakistan", PAKISTAN),
                               new ChoiceNameValue("Palau", PALAU),
                               new ChoiceNameValue("Panama", PANAMA),
                               new ChoiceNameValue("Papua New Guinea", PAPUA_NEW_GUINEA),
                               new ChoiceNameValue("Paraguay", PARAGUAY),
                               new ChoiceNameValue("Peru", PERU),
                               new ChoiceNameValue("Philippines", PHILIPPINES),
                               new ChoiceNameValue("Pitcairn Island", PITCAIRN),
                               new ChoiceNameValue("Poland", POLAND),
                               new ChoiceNameValue("Portugal", PORTUGAL),
                               new ChoiceNameValue("Puerto Rico", PUERTO_RICO),
                               new ChoiceNameValue("Qatar", QATAR),


                               new ChoiceNameValue("Réunion", REUNION),
                               new ChoiceNameValue("Romania", ROMANIA),
                               new ChoiceNameValue("Russian Federation", RUSSIAN_FEDERATION),
                               new ChoiceNameValue("Rwanda", RWANDA),

                               new ChoiceNameValue("St. Kitts and Nevis", ST_KITTS_AND_NEVIS),
                               new ChoiceNameValue("St. Lucia", ST_LUCIA),

                               new ChoiceNameValue("Saint Vincent And The Grenadines", ST_VINCENT_GRENADINES),
                               new ChoiceNameValue("Samoa", SAMOA),
                               new ChoiceNameValue("San Marino", SAN_MARINO),
                               new ChoiceNameValue("Sao Tome and Principe", SAO_TOME_AND_PRINCIPE),
                               new ChoiceNameValue("Saudi Arabia", SAUDI_ARABIA),
                               new ChoiceNameValue("Senegal", SENEGAL),
                               new ChoiceNameValue("Serbia", SERBIA),
                               new ChoiceNameValue("Seychelles", SEYCHELLES),
                               new ChoiceNameValue("Sierra Leone", SIERRA_LEONE),
                               new ChoiceNameValue("Singapore", SINGAPORE),
                               new ChoiceNameValue("Slovakia (Slovak Republic)", SLOVAKIA),
                               new ChoiceNameValue("Slovenia", SLOVENIA),
                               new ChoiceNameValue("Solomon Islands", SOLOMON_ISL),
                               new ChoiceNameValue("Somalia", SOMALIA),
                               new ChoiceNameValue("South Africa", SOUTH_AFRICA),
                               new ChoiceNameValue("South Georgia and the South Sandwich Islands",
                                                   SOUTH_GEORGIA_AND_THE_SOUTH_SANDWICH_ISLANDS),

                               new ChoiceNameValue("Spain", SPAIN),
                               new ChoiceNameValue("Sri Lanka", SRI_LANKA),
                               new ChoiceNameValue("St. Helena", ST_HELENA),
                               new ChoiceNameValue("St. Pierre And Miquelon", ST_PIERRE_AND_MIQUELON),
                               new ChoiceNameValue("Sudan", SUDAN),
                               new ChoiceNameValue("Suriname", SURINAME),
                               new ChoiceNameValue("Svalbard and Jan Mayen Islands", SVALBARD_JAN_MAYEN_ISL),
                               new ChoiceNameValue("Swaziland", SWAZILAND),
                               new ChoiceNameValue("Sweden", SWEDEN),
                               new ChoiceNameValue("Switzerland", SWITZERLAND),
                               new ChoiceNameValue("Syria", SYRIAN_ARAB_REPUBLIC),
                               new ChoiceNameValue("Taiwan", TAIWAN_PROVINCE_OF_CHINA),

                               new ChoiceNameValue("Tajikistan", TAJIKISTAN),
                               new ChoiceNameValue("Tanzania, United Republic of", REPUBLIC_OF_TANZANIA),
                               new ChoiceNameValue("Thailand", THAILAND),

                               new ChoiceNameValue("Togo", TOGO),
                               new ChoiceNameValue("Tokelau", TOKELAU),
                               new ChoiceNameValue("Tonga", TONGA),
                               new ChoiceNameValue("Trinidad And Tobago", TRINIDAD_AND_TOBAGO),
                               new ChoiceNameValue("Tunisia", TUNISIA),
                               new ChoiceNameValue("Turkey", TURKEY),
                               new ChoiceNameValue("Turkmenistan", TURKMENISTAN),
                               new ChoiceNameValue("Turks And Caicos Islands", TURKS_AND_CAICOS_ISL),
                               new ChoiceNameValue("Tuvalu", TUVALU),
                               new ChoiceNameValue("Uganda", UGANDA),
                               new ChoiceNameValue("Ukraine", UKRAINE),
                               new ChoiceNameValue("United Arab Emirates", UNITED_ARAB_EMIRATES),
                               new ChoiceNameValue("United Kingdom", UK_AND_NORTHERN_IRE),

                               new ChoiceNameValue("United States of America", UNITED_STATES_OF_AMERICA),
                               new ChoiceNameValue("United States Minor Outlying Islands",
                                                   UNITED_STATES_MINOR_OUTLYING_ISLANDS),


                               new ChoiceNameValue("Uruguay", URUGUAY),
                               new ChoiceNameValue("Uzbekistan", UZBEKISTAN),
                               new ChoiceNameValue("Vanuatu", VANUATU),
                               new ChoiceNameValue("Venezuela", VENEZUELA),
                               new ChoiceNameValue("Vietnam", VIETNAM),
                               new ChoiceNameValue("British Virgin Islands", BRITISH_VIRGIN_ISLANDS),
                               new ChoiceNameValue("United States Virgin Islands", UNITED_STATES_VIRGIN_ISL),
                               new ChoiceNameValue("Wallis And Futuna Islands", WALLIS_AND_FUTUNA_ISL),
                               new ChoiceNameValue("Western Sahara", WESTERN_SAHARA),
                               new ChoiceNameValue("Yemen", YEMEN),
                               //new ChoiceNameValue("Yugoslavia", YUGOSLAVIA), 
                               //new ChoiceNameValue("Zaire", ZAIRE),


                               new ChoiceNameValue("Zambia", ZAMBIA),
                               new ChoiceNameValue("Zimbabwe", ZIMBABWE),

                               new ChoiceNameValue("Saint Barthelemy", ST_BARTHELEMY),
                               new ChoiceNameValue("Saint Martin (French part)", ST_MARTIN)
                           };
            }
        }

        public CountriesChoiceListISO(CountriesChoiceListISO objType)
        {
            this.m_strValue = objType.m_strValue;
        }

        public override string ToString()
        {
            foreach (ChoiceNameValue objType in NameValuePairs)
            {
                if (this.m_strValue == objType.Value)
                    return objType.Name;
            }
            return String.Empty;
        }

        public string GetValue()
        {
            return this.m_strValue;
        }

        public static implicit operator CountriesChoiceListISO(string strValue)
        {
            return new CountriesChoiceListISO(strValue);
        }

        public static bool operator ==(CountriesChoiceListISO val1, CountriesChoiceListISO val2)
        {
            return val1.m_strValue == val2.m_strValue;
        }

        public static bool operator !=(CountriesChoiceListISO val1, CountriesChoiceListISO val2)
        {
            return val1.m_strValue != val2.m_strValue;
        }

        public static bool operator ==(CountriesChoiceListISO objCountriesChoiceListISO1, string strString2)
        {
            // check if both are null, which means that they are the same
            if ((object) objCountriesChoiceListISO1 == null &&
                (strString2 == null || strString2 == String.Empty))
                return true;

            // check if one is null then the other can't be
            if ((object) objCountriesChoiceListISO1 == null &&
                (strString2 != null || strString2 != String.Empty))
                return false;

            if ((object) objCountriesChoiceListISO1 != null &&
                (strString2 == null || strString2 == String.Empty))
                return false;

            // if we reach this point then both are not null, so we can go ahead and
            // just check them
            foreach (ChoiceNameValue objType in NameValuePairs)
            {
                if (objType.Value == strString2)
                    return true;
            }
            // if we reached this point, then we couldn't find the name
            // and so they are not equal
            return false;
        }

        public static bool operator !=(CountriesChoiceListISO objCountriesChoiceList1, string strString2)
        {
            // check if both are null, which means that they are the same
            if ((object) objCountriesChoiceList1 == null &&
                (strString2 == null || strString2 == String.Empty))
                return false;

            // check if one is null then the other can't be
            if ((object) objCountriesChoiceList1 == null &&
                (strString2 != null || strString2 != String.Empty))
                return true;

            if ((object) objCountriesChoiceList1 != null &&
                (strString2 == null || strString2 == String.Empty))
                return true;

            // if we reach this point then both are not null, so we can go ahead and
            // just check them
            foreach (ChoiceNameValue objType in NameValuePairs)
            {
                if (objType.Value == strString2)
                    return false;
            }
            // if we reached this point, then we couldn't find the name
            // and so they are not equal
            return true;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
