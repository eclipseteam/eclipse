﻿using eclipseonlineweb.OS.Milliman.Actions;
using System.Data;
using eclipseonlineweb.OS.Milliman.ValueLists;

namespace eclipseonlineweb.WebUtilities
{
    public class MillimanServiceUtility
    {
        public static string RunProjectionForClient(string advisorID, string clientCode, Person person,
                                                    string investmentStrategyId, Holding[] holdings, Action[] actions,
                                                    Goal[] goals, InvestmentStrategyProducts[] products)
        {
            wsProjectionActions lProjection = new wsProjectionActions();
            return lProjection.RunProjectionForClient(advisorID, clientCode, person, investmentStrategyId,holdings, actions, goals, products);
        }

        public static DataSet GetOperatorListForProjectionId(long projectionId, bool projectionIdSpecified)
        {
            wsProjectionActions lProjection = new wsProjectionActions();
            return lProjection.GetOperatorListForProjectionId(projectionId, projectionIdSpecified);
        }

        public static DataSet GetProjectionResultsForClient(long projectionId, bool projectionIdSpecified,
                                                            string operatorId)
        {
            wsProjectionActions lProjection = new wsProjectionActions();
            return lProjection.GetProjectionResultsForClient(projectionId, projectionIdSpecified, operatorId);
        }

        public static DataSet GetInvestmentStrategies()
        {
            wsGetValueLists wsLists = new wsGetValueLists();
            return wsLists.GetInvestmentStrategies();
        }

        public static DataSet GetInvestmentStrategyAssetWeightings(string investmentStrategyID)
        {
            wsGetValueLists wsLists = new wsGetValueLists();
            return wsLists.GetInvestmentStrategyAssetWeightings(investmentStrategyID);
        }
    }
}
