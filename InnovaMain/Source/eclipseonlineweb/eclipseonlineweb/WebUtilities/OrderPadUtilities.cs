﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;
using Telerik.Web.UI;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb.WebUtilities
{
    public static class OrderPadUtilities
    {
        public static void FillBankNAvailableFunds(RadComboBox comboBox, HoldingRptDataSet ds, string filter, ClientManagementType clientManagementType, ICMBroker broker)
        {
            DataRow[] cashAccounts = ds.HoldingSummaryTable.Select(filter);
            comboBox.Items.Clear();
            //Getting Total Holdings
            decimal totalHolding = (decimal)ds.HoldingSummaryTable.Compute(string.Format("Sum({0})", ds.HoldingSummaryTable.TOTAL), "");
            foreach (var row in cashAccounts)
            {
                var item = new RadComboBoxItem
                {
                    Text = row[ds.HoldingSummaryTable.DESCRIPTION] + " : " + row[ds.HoldingSummaryTable.ACCOUNTNO],
                    Value = row[ds.HoldingSummaryTable.ACCOUNTCID].ToString()
                };

                decimal holding;
                decimal unsettledBuy;
                decimal.TryParse(row[ds.HoldingSummaryTable.HOLDING].ToString(), out holding);
                decimal.TryParse(row[ds.HoldingSummaryTable.UNSETTLEDBUY].ToString(), out unsettledBuy);
                decimal availableFund = holding + unsettledBuy;
                //Calculating MinCashValue For SMA and Setting Available Funds.
                if (clientManagementType == ClientManagementType.SMA)
                {
                    decimal minValue;
                    decimal maxValue;
                    var percentage = CalculateSMAPercentage(ds, broker, out minValue, out maxValue);

                    decimal minCashValue = totalHolding * percentage;
                    if (minCashValue < minValue)
                        minCashValue = minValue;
                    else if (minCashValue > maxValue)
                        minCashValue = maxValue;
                    //Setting Attributes for Cash Balance and Min Cash Value to display
                    item.Attributes.Add("CurrentCashBalance", availableFund.ToString());
                    item.Attributes.Add("MinCashValue", minCashValue.ToString());
                    //Setting Available Funds for SMA
                    availableFund = availableFund - minCashValue;
                }
                item.Attributes.Add("AvailableFunds", availableFund.ToString());
                item.Attributes.Add("AccountNo", row[ds.HoldingSummaryTable.ACCOUNTNO].ToString());
                item.Attributes.Add("BSB", row[ds.HoldingSummaryTable.BSB].ToString());

                comboBox.Items.Add(item);
                item.DataBind();
            }
        }

        public static void FillBankNAvailableFunds(RadComboBox comboBox, HoldingRptDataSet ds, string filter, OrderAccountType orderAccountType, OrderItemType orderItemType, string serviceType, ClientManagementType clientManagementType, ICMBroker broker)
        {
            DataRow[] cashAccounts = ds.HoldingSummaryTable.Select(filter);
            comboBox.Items.Clear();
            //Getting Total Holdings
            decimal totalHolding = (decimal)ds.HoldingSummaryTable.Compute(string.Format("Sum({0})", ds.HoldingSummaryTable.TOTAL), "");

            foreach (var row in cashAccounts)
            {
                var item = new RadComboBoxItem
                {
                    Text = row[ds.HoldingSummaryTable.DESCRIPTION] + " : " + row[ds.HoldingSummaryTable.ACCOUNTNO],
                    Value = row[ds.HoldingSummaryTable.ACCOUNTCID].ToString()
                };

                decimal holding;
                decimal unsettledBuy;
                decimal.TryParse(row[ds.HoldingSummaryTable.HOLDING].ToString(), out holding);
                decimal.TryParse(row[ds.HoldingSummaryTable.UNSETTLEDBUY].ToString(), out unsettledBuy);
                decimal availableFund = holding + unsettledBuy;

                if ((orderAccountType == OrderAccountType.ASX || orderAccountType == OrderAccountType.StateStreet) && orderItemType == OrderItemType.Buy)
                {
                    var unsettledSell = CalculateUnsettledFunds(orderAccountType, ds, serviceType);
                    availableFund += unsettledSell;
                }
                //Calculating MinCashValue For SMA and Setting Available Funds.
                if (clientManagementType == ClientManagementType.SMA)
                {
                    decimal minValue;
                    decimal maxValue;
                    var percentage = CalculateSMAPercentage(ds, broker, out minValue, out maxValue);

                    decimal minCashValue = totalHolding * percentage;
                    if (minCashValue < minValue)
                        minCashValue = minValue;
                    else if (minCashValue > maxValue)
                        minCashValue = maxValue;
                    //Setting Attributes for Cash Balance and Min Cash Value to display
                    item.Attributes.Add("CurrentCashBalance", availableFund.ToString());
                    item.Attributes.Add("MinCashValue", minCashValue.ToString());
                    //Setting Available Funds for SMA
                    availableFund = availableFund - minCashValue;
                }

                item.Attributes.Add("AvailableFunds", availableFund.ToString());
                item.Attributes.Add("AccountNo", row[ds.HoldingSummaryTable.ACCOUNTNO].ToString());
                item.Attributes.Add("BSB", row[ds.HoldingSummaryTable.BSB].ToString());

                comboBox.Items.Add(item);
                item.DataBind();
            }
        }

        private static decimal CalculateSMAPercentage(HoldingRptDataSet ds, ICMBroker broker, out decimal minValue, out decimal maxValue)
        {
            decimal percentage = 0;
            minValue = 0;
            maxValue = 0;
            var clientName = ds.Tables[BaseClientDetails.CLIENTSUMMARYTABLE].Rows[0][BaseClientDetails.CLIENTNAME].ToString().ToLower();

            IBrokerManagedComponent iBMC = broker.GetWellKnownBMC(WellKnownCM.Organization);
            var cashBalanceds = new CashBalanceDS { CommandType = DatasetCommandTypes.Get };
            iBMC.GetData(cashBalanceds);
            broker.ReleaseBrokerManagedComponent(iBMC);

            foreach (DataRow row in cashBalanceds.CashBalanceTable.Rows)
            {
                string accType = row[cashBalanceds.CashBalanceTable.SUPERACCOUNTTYPE].ToString();
                if (clientName.Contains("pension)") && accType != SuperAccountType.Pension.ToString())
                {
                    continue;
                }
                percentage = Convert.ToDecimal(row[cashBalanceds.CashBalanceTable.PERCENTAGE].ToString());
                minValue = Convert.ToDecimal(row[cashBalanceds.CashBalanceTable.MINVALUE].ToString());
                maxValue = Convert.ToDecimal(row[cashBalanceds.CashBalanceTable.MAXVALUE].ToString());
                break;
            }
            return percentage;
        }

        private static decimal CalculateUnsettledFunds(OrderAccountType orderAccountType, HoldingRptDataSet ds, string serviceType)
        {
            string orgType = string.Empty;
            switch (orderAccountType)
            {
                case OrderAccountType.StateStreet:
                    orgType = OrganizationType.ManagedInvestmentSchemesAccount.ToString();
                    break;
                case OrderAccountType.ASX:
                    orgType = OrganizationType.DesktopBrokerAccount.ToString();
                    break;
            }

            var query = string.Format("{0}='{1}' and {2}='{3}'", ds.HoldingSummaryTable.LINKEDENTITYTYPE, orgType, ds.HoldingSummaryTable.SERVICETYPE, serviceType);
            decimal unsettledSellByUnits = GetSumOfColumn(ds.HoldingSummaryTable, ds.HoldingSummaryTable.UNSETTLEDSELLBUYUNITS, query);
            decimal unsettledSellByAmount = GetSumOfColumn(ds.HoldingSummaryTable, ds.HoldingSummaryTable.UNSETTLEDSELLBYAMOUNT, query);

            decimal unsettledSell = 0;
            unsettledSell += unsettledSellByUnits + unsettledSellByAmount;

            return unsettledSell;
        }

        public static void FillServiceTypes(RadComboBox comboBox, HoldingRptDataSet ds, bool isAdmin, OrderAccountType accountType, bool isAdviser = false, bool isCashTransfer = false)
        {
            //Getting Service type according to user type
            var dv = ds.HoldingSummaryTable.DefaultView.ToTable(true, ds.HoldingSummaryTable.SERVICETYPE).DefaultView;
            string filter;

            switch (accountType)
            {
                case OrderAccountType.StateStreet:
                    filter = string.Format("{0} in ('{1}'", ds.HoldingSummaryTable.SERVICETYPE, ServiceTypes.DoItWithMe.GetDescription());
                    break;
                default:
                    filter = string.Format("{0} in ('{1}','{2}'", ds.HoldingSummaryTable.SERVICETYPE, ServiceTypes.DoItWithMe.GetDescription(), ServiceTypes.DoItYourSelf.GetDescription());
                    break;
            }

            if (isAdmin || (isAdviser && isCashTransfer))
            {
                filter += string.Format(",'{0}')", ServiceTypes.DoItForMe.GetDescription());
            }
            else
            {
                filter += ")";
            }
            dv.RowFilter = filter;

            comboBox.Items.Clear();
            comboBox.DataSource = dv;
            comboBox.DataTextField = ds.HoldingSummaryTable.SERVICETYPE;
            comboBox.DataValueField = ds.HoldingSummaryTable.SERVICETYPE;
            comboBox.DataBind();
            if (comboBox.Items.Count > 0)
            {
                comboBox.SelectedIndex = 0;
            }
        }

        public static Guid IsOrderCmExists(ICMBroker broker, UserEntity user)
        {
            var unit = new OrganizationUnit
            {
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = user
            };

            var ds = new OrderPadDS
            {
                CommandType = DatasetCommandTypes.Check,
                Unit = unit,
                Command = (int)WebCommands.GetOrganizationUnitsByType
            };

            var org = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            org.GetData(ds);
            broker.ReleaseBrokerManagedComponent(org);

            Guid orderCMCID = Guid.Empty;
            if (ds.OrdersTable.Rows.Count > 0)
            {
                //Getting ORDER CID 
                orderCMCID = new Guid(ds.OrdersTable.Rows[0][ds.OrdersTable.CID].ToString());
            }

            return orderCMCID;
        }

        public static Guid IsSettledUnSettledCmExists(ICMBroker broker, UserEntity user, string clientAccId)
        {
            var unit = new OrganizationUnit
            {
                ClientId = clientAccId,
                Name = clientAccId,
                Type = ((int)OrganizationType.SettledUnsettled).ToString(),
                CurrentUser = user
            };

            var ds = new SettledUnsetteledDS
            {
                CommandType = DatasetCommandTypes.Check,
                Unit = unit,
                Command = (int)WebCommands.GetOrganizationUnitsByType
            };

            var org = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            org.GetData(ds);
            broker.ReleaseBrokerManagedComponent(org);

            Guid settledUnsettledCMCID = Guid.Empty;
            if (ds.Unit.Cid != Guid.Empty)
            {
                //Getting CID 
                settledUnsettledCMCID = ds.Unit.Cid;
            }

            return settledUnsettledCMCID;
        }

        public static void SetStatusToolTip(GridDataItem dataItem, OrderStatus orderStatus)
        {
            string toolTip = string.Empty;

            switch (orderStatus)
            {
                case OrderStatus.Active:
                    toolTip = Tooltip.OrderPadActive;
                    break;
                case OrderStatus.ValidationFailed:
                    toolTip = Tooltip.OrderPadValidationFailed;
                    break;
                case OrderStatus.Validated:
                    toolTip = Tooltip.OrderPadValidated;
                    break;
                case OrderStatus.Approved:
                    toolTip = Tooltip.OrderPadApproved;
                    break;
                case OrderStatus.Submitted:
                    toolTip = Tooltip.OrderPadSubmitted;
                    break;
                case OrderStatus.Complete:
                    toolTip = Tooltip.OrderPadCompleted;
                    break;
                case OrderStatus.Cancelled:
                    toolTip = Tooltip.OrderPadCancelled;
                    break;
            }

            foreach (GridColumn column in dataItem.OwnerTableView.RenderColumns)
            {
                if (column.UniqueName.ToLower() != "actions" && column.UniqueName.ToLower() != "bulkcancel")
                {
                    dataItem[column.UniqueName].ToolTip = toolTip;
                }
            }
        }

        public static void UpdateExportStatus(ExportDS ds, ICMBroker broker, UserEntity user)
        {
            Guid orderCID = IsOrderCmExists(broker, user);
            if (orderCID != Guid.Empty)
            {
                broker.SaveOverride = true;
                var orderCM = broker.GetBMCInstance(orderCID);
                orderCM.SetData(ds);
                broker.SetComplete();
                broker.SetStart();
            }
        }

        public static void GetSecurityAvaiableFundsForSMA(HoldingRptDataSet holdingDs, string investmentCode, decimal securityHoldingLimit, decimal unitPrice, OrganizationType organizationType, out decimal secHoldingLimit, out decimal secCurrentHolding, out decimal secAvailableFund)
        {
            //Getting Total Holdings
            decimal totalHolding = (decimal)holdingDs.HoldingSummaryTable.Compute(string.Format("Sum({0})", holdingDs.HoldingSummaryTable.TOTAL), "");

            //Getting Current Holding of Security
            var prodDrs = holdingDs.ProductBreakDownTable.Select(string.Format("{0}='{1}' and {2}='{3}'", holdingDs.ProductBreakDownTable.INVESMENTCODE, investmentCode, holdingDs.ProductBreakDownTable.LINKEDENTITYTYPE, organizationType));
            decimal currentHolding = prodDrs.Sum(dr => Convert.ToDecimal(dr[holdingDs.ProductBreakDownTable.TOTAL]));

            //Calculating Current Holding
            secCurrentHolding = currentHolding;

            //Calculating Holding Limit 
            secHoldingLimit = (totalHolding * securityHoldingLimit) / 100;

            //Calculating Available Funds
            secAvailableFund = secHoldingLimit - secCurrentHolding;
        }

        public static void GetTDAvaiableFundsForSMA(HoldingRptDataSet holdingDs, string broker, string bank, decimal holdingLimit, out decimal tdHoldingLimit, out decimal tdCurrentHolding, out decimal tdAvailableFund)
        {
            //Getting Total Holdings
            decimal totalHolding = (decimal)holdingDs.HoldingSummaryTable.Compute(string.Format("Sum({0})", holdingDs.HoldingSummaryTable.TOTAL), "");

            //Getting Current Holding of td by broker and bank/institute
            var tdDrs = holdingDs.HoldingSummaryTable.Select(string.Format("{0}='{1}-TD' and {2}='{3}' and {4}='{5}'", holdingDs.HoldingSummaryTable.PRODUCTNAME, broker, holdingDs.HoldingSummaryTable.DESCRIPTION, bank, holdingDs.HoldingSummaryTable.LINKEDENTITYTYPE, OrganizationType.TermDepositAccount));
            decimal currentHolding = tdDrs.Sum(dr => Convert.ToDecimal(dr[holdingDs.HoldingSummaryTable.TOTAL]));

            //Calculating Current Holding
            tdCurrentHolding = currentHolding;

            //Calculating Holding Limit 
            tdHoldingLimit = (totalHolding * holdingLimit) / 100;

            //Calculating Available Funds
            tdAvailableFund = tdHoldingLimit - tdCurrentHolding;
        }

        private static decimal GetSumOfColumn(DataTable dt, string colName, string query)
        {
            decimal value = 0;
            string colToCompute = string.Format("sum({0})", colName);
            var sum = dt.Compute(colToCompute, query);
            if (sum != DBNull.Value)
                value = (decimal)sum;
            return value;
        }

        public static void SetAvailableFunds(RadComboBox comboBox, Label lblAvailableFunds, Label lblCashBalance, Label lblMinCash, ClientManagementType clientManagementType)
        {
            lblAvailableFunds.Text = String.Empty;
            lblCashBalance.Text = String.Empty;
            lblMinCash.Text = String.Empty;

            if (comboBox.Items.Count > 0)
            {
                comboBox.SelectedIndex = 0;
                lblAvailableFunds.Text = Convert.ToDecimal(comboBox.SelectedItem.Attributes["AvailableFunds"]).ToString("C");
                if (clientManagementType == ClientManagementType.SMA)
                {
                    lblCashBalance.Text = Convert.ToDecimal(comboBox.SelectedItem.Attributes["CurrentCashBalance"]).ToString("C");
                    lblMinCash.Text = Convert.ToDecimal(comboBox.SelectedItem.Attributes["MinCashValue"]).ToString("C");
                }
            }
            else
            {
                lblAvailableFunds.Text = "$0.00";
                if (clientManagementType == ClientManagementType.SMA)
                {
                    lblCashBalance.Text = "$0.00";
                    lblMinCash.Text = "$0.00";
                }
            }
        }
    }
}