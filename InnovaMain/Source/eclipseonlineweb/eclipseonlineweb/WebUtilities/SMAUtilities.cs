﻿using System;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;

namespace eclipseonlineweb.WebUtilities
{
    public class SMAUtilities
    {
        public static void RefreshClientData(Guid cid, ICMBroker broker, UserEntity user)
        {
            var ds = new SMAImportProcessDS();
            var orgUnit = broker.GetBMCInstance(cid) as IOrganizationUnit;
            if (orgUnit != null)
            {
                ds.Variables.Add("MemberCode", orgUnit.SuperMemberID);
                ds.Variables.Add("OrgCid", orgUnit.CID.ToString());
                broker.ReleaseBrokerManagedComponent(orgUnit);
                ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit {CurrentUser = user};
                ds.StartDate = DateTime.Now.AddDays(-1);
                ds.EndDate = DateTime.Now;
                ds.Command = (int) WebCommands.ImportSMATransactionsIndividual;
                var organization = broker.GetWellKnownBMC(WellKnownCM.Organization) as OrganizationCM;
                if (organization != null)
                {
                    bool callService = false;
                    if (organization.OrganizationalSettings!=null)
                    {
                        callService=organization.OrganizationalSettings.ReadFromSMAService;
                    }
                    if (callService)
                    organization.GetData(ds);
                   // broker.ReleaseBrokerManagedComponent(organization);
                }
            }
        }

        public static bool IsSMAClient(Guid cid, ICMBroker broker)
        {
            bool result = false;
            var orgUnit = broker.GetBMCInstance(cid) as IOrganizationUnit;
            if (orgUnit != null)
            {
                result = IsSMAClient(orgUnit.TypeName);
                broker.ReleaseBrokerManagedComponent(orgUnit);
            }
            return result;
        }

        public static bool IsSMAClient(string typeName)
        {
            var clientType = (OrganizationType)Enum.Parse(typeof(OrganizationType), typeName);

            return clientType == OrganizationType.ClientEClipseSuper;
        }
    }
}