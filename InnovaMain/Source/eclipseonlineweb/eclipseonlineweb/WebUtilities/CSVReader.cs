﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Text;
using Oritax.TaxSimp.Data;

namespace eclipseonlineweb.WebUtilities
{
    public class CSVReader
    {

        public static DataTable ReadCSVToDataTable(string filePath, bool hasHeaderRow)
        {
            string fileName = Path.GetFileName(filePath);
            var folderPath = Path.GetDirectoryName(filePath);
            string Header = hasHeaderRow ? "Yes" : "No";
            string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + folderPath + @";Extended Properties=""Text;HDR=" + Header + @";FMT=Delimited""";

            OleDbConnection con = new OleDbConnection(connectionString);
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Connection = con;
            OleDbDataAdapter dAdapter = new OleDbDataAdapter(cmd);
            DataTable dtExcelRecords = new DataTable();
            con.Open();
            cmd.CommandText = "SELECT * FROM [" + fileName + "]";
            dAdapter.SelectCommand = cmd;
            dAdapter.Fill(dtExcelRecords);
            con.Close();
            return dtExcelRecords;
        }

    }

    public static class ConversorTransactionCsv
    {
        public static string SelectRows(string csvString, string startwith)
        {
            //split the rows
            var sep = new[] { "\n", "\r" };
            string[] rows = csvString.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            string cashTransactionData = null;

            for (int i = 0; i < rows.Length; i++)
            {
                //Create each row
                if (i > 0)
                {
                    if (!string.IsNullOrEmpty(startwith))
                    {
                        if (rows[i].StartsWith(startwith))
                            cashTransactionData += rows[i] + "\r\n";
                    }
                    else
                    {
                        cashTransactionData += rows[i] + "\r\n";
                    }
                }
            }
            return cashTransactionData;
        }

        public static string FilterRow(string csvString, string filterwith)
        {
            if (string.IsNullOrEmpty(csvString) || string.IsNullOrEmpty(csvString.Trim()) || string.IsNullOrEmpty(filterwith) || string.IsNullOrEmpty(filterwith.Trim()))
                return null;

            var sep = new[] { "\n", "\r" };
            string[] rows = csvString.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            string cashTransactionData = null;
            for (int i = 0; i < rows.Length; i++)
            {
                if (i > 0)
                {
                    string[] arr = rows[i].Split(',');
                    if (arr[3] == filterwith)
                    {
                        cashTransactionData += rows[i] + "\r\n";
                    }
                }
            }
            return cashTransactionData;
        }

        public static DataTable CreateTdTransaction(string tdTranscationcsv)
        {
            if (string.IsNullOrEmpty(tdTranscationcsv) || string.IsNullOrEmpty(tdTranscationcsv.Trim()))
                return null;
            var sep = new[] { "\r", "\n" };
            var source = tdTranscationcsv.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            DataTable dt = new DataTable();
            dt.TableName = "AtTDTransaction";
            dt.Columns.Add("AccountNumber", typeof(string));
            dt.Columns.Add("DealID", typeof(string));
            dt.Columns.Add("DepositAccountNumber", typeof(string));
            dt.Columns.Add("TDTRAN", typeof(string));
            dt.Columns.Add("ContractID", typeof(string));
            dt.Columns.Add("TransactionDate", typeof(string));
            dt.Columns.Add("TransactionAmount", typeof(string));
            dt.Columns.Add("TransactionType", typeof(string));
            foreach (var line in source)
            {
                StringBuilder sb = new StringBuilder();
                string[] splitName = line.Split(',');
                DataRow dr = dt.NewRow();
                dr["AccountNumber"] = splitName[0];
                dr["DealID"] = splitName[1];
                dr["DepositAccountNumber"] = splitName[2];
                dr["TDTRAN"] = splitName[3];
                dr["ContractID"] = splitName[4];
                dr["TransactionDate"] = splitName[5];
                dr["TransactionAmount"] = splitName[6];
                dr["TransactionType"] = splitName[7];
                dt.Rows.Add(dr);
                sb.Clear();
            }
            return dt;
        }

        public static DataTable CreateAtCallAmmClientTrans(string TransactionsCsv)
        {

            if (string.IsNullOrEmpty(TransactionsCsv) || string.IsNullOrEmpty(TransactionsCsv.Trim()))
                return null;


            var sep = new[] { "\r", "\n" };
            var source = TransactionsCsv.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            DataTable dt = new DataTable();
            dt.TableName = "AtCallAmm";
            dt.Columns.Add("AccountNumber", typeof(string));
            dt.Columns.Add("DealID", typeof(string));
           // dt.Columns.Add("DepositAccountNumber", typeof(string));
            dt.Columns.Add("TDTRAN", typeof(string));
            dt.Columns.Add("ContractID", typeof(string));
            dt.Columns.Add("TransactionDate", typeof(string));
            dt.Columns.Add("TransactionAmount", typeof(string));
            dt.Columns.Add("TransactionRate", typeof(string));
            dt.Columns.Add("TransactionType", typeof(string));
            foreach (var line in source)
            {
                StringBuilder sb = new StringBuilder();
                string[] splitName = line.Split(',');
                DataRow dr = dt.NewRow();
                dr["AccountNumber"] = splitName[0];
                dr["DealID"] = splitName[1];
               // dr["DepositAccountNumber"] = splitName[2];
                dr["TDTRAN"] = splitName[3];
                dr["ContractID"] = splitName[4];
                dr["TransactionDate"] = splitName[5];
                dr["TransactionAmount"] = splitName[6];
                dr["TransactionRate"] = splitName[7];
                dr["TransactionType"] = splitName[8];
                dt.Rows.Add(dr);
                sb.Clear();
            }
            return dt;
        }

        public static DataTable CreateAtTdTrans(string transactionsCsv)
        {

            if (string.IsNullOrEmpty(transactionsCsv) || string.IsNullOrEmpty(transactionsCsv.Trim()))
                return null;


            var sep = new[] { "\r", "\n" };
            var source = transactionsCsv.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            DataTable dt = new DataTable();
            dt.TableName = "AtCallAmm";
            dt.Columns.Add("AccountNumber", typeof(string));
            dt.Columns.Add("DealID", typeof(string));
            dt.Columns.Add("DepositAccountNumber", typeof(string));
            dt.Columns.Add("TDTRAN", typeof(string));
            dt.Columns.Add("ContractID", typeof(string));
            dt.Columns.Add("TransactionDate", typeof(string));
            dt.Columns.Add("TransactionAmount", typeof(string));
            dt.Columns.Add("TransactionType", typeof(string));
            dt.Columns.Add("HasErrors", typeof(string));
            dt.Columns.Add("Message", typeof(string));

            foreach (var line in source)
            {
                StringBuilder sb = new StringBuilder();
                string[] splitName = line.Split(',');
                DataRow dr = dt.NewRow();
                dr["AccountNumber"] = splitName[0];
                dr["DealID"] = splitName[1];
                dr["DepositAccountNumber"] = splitName[2];
                dr["TDTRAN"] = splitName[3];
                dr["ContractID"] = splitName[4];
                dr["TransactionDate"] = splitName[5];
                dr["TransactionAmount"] = splitName[6];
                dr["TransactionType"] = splitName[7];
                if (splitName[0].Length > 7 || splitName[0].Length < 7 || splitName[0] == string.Empty || splitName[1] == string.Empty || splitName[2] == string.Empty || splitName[2].Length > 8 || splitName[2].Length < 8)
                    dr["HasErrors"] = true;
                else
                    dr["HasErrors"] = false;

                if (splitName[0].Length < 7 || splitName[0].Length > 7 || splitName[0] == string.Empty)
                    sb.Append("Invalid Account No ");
                if (splitName[1] == string.Empty)
                    sb.Append("DealID InCorrect ");
                if (splitName[2] == string.Empty || splitName[2].Length > 8 || splitName[2].Length < 8)
                    sb.Append("Deposit Account Number InCorrect ");

                dr["Message"] = sb.ToString();

                dt.Rows.Add(dr);
                sb.Clear();
            }
            return dt;
        }

        public static DataTable CreateCashTransactioList(string TransactionsCsv)
        {

            if (string.IsNullOrEmpty(TransactionsCsv) || string.IsNullOrEmpty(TransactionsCsv.Trim()))
            {
                return null;

            }
            var sep = new[] { "\r", "\n" };
            var source = TransactionsCsv.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            DataTable dt = new DataTable();
            dt.TableName = "CashTransctions";
            dt.Columns.Add("TransactionIndicator", typeof(string));
            dt.Columns.Add("BankwestReferenceNumber", typeof(string));
            dt.Columns.Add("BSBNAccountNumber", typeof(string));
            dt.Columns.Add("DateOfTransaction", typeof(string));
            dt.Columns.Add("Amount", typeof(string));
            dt.Columns.Add("TransactionType", typeof(string));
            dt.Columns.Add("TransactionDescription", typeof(string));

            foreach (var line in source)
            {
                string[] splitName = line.Split(',');
                DataRow dr = dt.NewRow();
                dr["TransactionIndicator"] = splitName[0];
                dr["BankwestReferenceNumber"] = splitName[1];
                dr["BSBNAccountNumber"] = splitName[2];
                dr["DateOfTransaction"] = splitName[3];
                dr["Amount"] = splitName[4];
                dr["TransactionType"] = splitName[5];
                dr["TransactionDescription"] = splitName[7];

                dt.Rows.Add(dr);

            }

            return dt;
        }



        public static DataTable CreateMacquireTransactioList(string TransactionsCsv)
        {

            if (string.IsNullOrEmpty(TransactionsCsv) || string.IsNullOrEmpty(TransactionsCsv.Trim()))
            {
                return null;
            }
            Dictionary<string, string> SystemTrans = new Dictionary<string, string>{
            {"INTEREST","Interest"},
            {"DIVIDEND","Dividend"},
            {"FEES","Investment fees"},
            };

            var sep = new[] { "\r", "\n" };
            var source = TransactionsCsv.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            DataTable dt = new DataTable();

            dt.TableName = "MACQUARIEBANKTRANSACTIONSTABLE";
            dt.Columns.Add("AccountNumber", typeof(string));
            dt.Columns.Add("TransactionDate", typeof(string));
            dt.Columns.Add("ReversalFlag", typeof(string));
            dt.Columns.Add("DebitCredit", typeof(string));
            dt.Columns.Add("Amount", typeof(string));
            dt.Columns.Add("Narrative", typeof(string));
            dt.Columns.Add("TransactionType", typeof(string));
            dt.Columns.Add("DateModified", typeof(string));
            dt.Columns.Add("TransactionCategory", typeof(string));
            dt.Columns.Add("SystemTransactionType", typeof(string));
            dt.Columns.Add("ImportTransactionTypeDetail", typeof(string));

            int count = 0;
            var accountName = source[0].Split(',')[0];
            string AccountNumber = accountName.Substring(0, accountName.IndexOf('-')).Trim();
            foreach (var line in source)
            {
                if (count <= 1)
                {
                    count++;
                    continue;

                }


                string[] splitName = line.Split(',');
                DataRow dr = dt.NewRow();

                dr["AccountNumber"] = AccountNumber;
                dr["TransactionDate"] = splitName[0];
                dr["ReversalFlag"] = "N";
                var debitCredit = splitName[1] == "WITHDRAWAL" ? "D" : "C";
                dr["DebitCredit"] = debitCredit;

                var amount = splitName[1] == "WITHDRAWAL" ? splitName[3] : splitName[4];
                dr["Amount"] = amount;
                dr["Narrative"] = splitName[2];

                var categoryType = splitName[6].Trim().ToUpper();

                //If we are populating System transaction type, please use the algorithm:
                //   Check column G (category type) in the spread sheet
                //•	 If it has Interest, Dividends or fees, set transaction type as Interest, Dividends or Investment fees
                //•  Otherwise set to then “ Transfer Our” if debit or “Transfer In” if credit
                dr["SystemTransactionType"] = SystemTrans.ContainsKey(categoryType) ? SystemTrans[categoryType] : (debitCredit == "C" ? "Transfer In" : "Transfer Out");

                //   Transaction type is either Income or expense – depending on whether amount is debit = expense or credit=income
                dr["ImportTransactionTypeDetail"] = (debitCredit == "C" ? "Income" : "Expense");
                dr["TransactionCategory"] = "";
                dr["DateModified"] = "";
                dt.Rows.Add(dr);

            }

            return dt;
        }

        public static DataTable CreateOpeningBalanceList(string openingBalCsv)
        {
            if (string.IsNullOrEmpty(openingBalCsv) || string.IsNullOrEmpty(openingBalCsv.Trim()))
            {
                return null;

            }
            var sep = new[] { "\r\n" };
            var source = openingBalCsv.Split(sep, StringSplitOptions.RemoveEmptyEntries);

            DataTable dt = new DataTable();
            dt.TableName = "HoldingBalances";

            dt.Columns.Add("TransactionIndicator", typeof(string));
            dt.Columns.Add("BSBNAccountNumber", typeof(string));
            dt.Columns.Add("DateOfHolding", typeof(string));
            dt.Columns.Add("HoldingAmount", typeof(string));
            dt.Columns.Add("UnsettledOrder", typeof(string));
            dt.Columns.Add("TransactionType", typeof(string));
            dt.Columns.Add("BankwestReferenceNumber", typeof(string));
            dt.Columns.Add("TransactionDescription", typeof(string));

            foreach (var line in source)
            {
                string[] splitName = line.Split(',');
                DataRow dr = dt.NewRow();
                dr["TransactionIndicator"] = splitName[0];
                dr["BSBNAccountNumber"] = splitName[1];
                dr["DateOfHolding"] = splitName[2];
                dr["HoldingAmount"] = splitName[3];
                dr["UnsettledOrder"] = splitName[4];
                dr["TransactionType"] = splitName[5];
                dr["BankwestReferenceNumber"] = splitName[6];
                dr["TransactionDescription"] = splitName[7];
                dt.Rows.Add(dr);

            }
            return dt;

        }

        public static DataTable CreateASXPriceList(string TransactionsCsv, string pricedate, string currency)
        {

            if (string.IsNullOrEmpty(TransactionsCsv) || string.IsNullOrEmpty(TransactionsCsv.Trim()))
            {
                return null;
            }
            var sep = new[] { "\r", "\n" };
            var source = TransactionsCsv.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            DataTable dt = new DataTable();
            dt.TableName = "ASXPrice";
            dt.Columns.Add("Code", typeof(string));
            dt.Columns.Add("CompanyName", typeof(string));
            dt.Columns.Add("Last", typeof(string));
            dt.Columns.Add("Add_Sub", typeof(string));
            dt.Columns.Add("Percent", typeof(string));
            dt.Columns.Add("Open", typeof(string));
            dt.Columns.Add("High", typeof(string));
            dt.Columns.Add("Low", typeof(string));
            dt.Columns.Add("Trades", typeof(string));
            dt.Columns.Add("Volume", typeof(string));
            dt.Columns.Add("BuyPrice", typeof(string));
            dt.Columns.Add("SellPrice", typeof(string));
            dt.Columns.Add("Rating", typeof(string));
            dt.Columns.Add("PriceDate", typeof(string));
            dt.Columns.Add("Currency", typeof(string));
            int i = -1;
            foreach (var line in source)
            {
                i++;
                if (i == 0)
                    continue;
                string[] splitName = line.Split(',');
                DataRow dr = dt.NewRow();
                dr["Code"] = splitName[0];
                dr["CompanyName"] = splitName[1];
                dr["Last"] = splitName[2];
                dr["Add_Sub"] = splitName[3];
                dr["Percent"] = splitName[4];
                dr["Open"] = splitName[5];
                dr["High"] = splitName[6];
                dr["Low"] = splitName[7];
                dr["Trades"] = splitName[8];
                dr["Volume"] = splitName[9];
                dr["BuyPrice"] = splitName[10];
                dr["SellPrice"] = splitName[11];
                dr["Rating"] = splitName[12];
                dr["PriceDate"] = pricedate;
                dr["Currency"] = currency;
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public static DataTable CreateStateStreetPriceList(string TransactionsCsv)
        {

            if (string.IsNullOrEmpty(TransactionsCsv) || string.IsNullOrEmpty(TransactionsCsv.Trim()))
            {
                return null;
            }
            var sep = new[] { "\r", "\n" };
            var source = TransactionsCsv.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            DataTable dt = new DataTable();
            dt.TableName = "StateStreetPrice";
            dt.Columns.Add("FundCode", typeof(string));
            dt.Columns.Add("FundName", typeof(string));
            dt.Columns.Add("PriceDate", typeof(string));
            dt.Columns.Add("Price(NAV)", typeof(string));
            dt.Columns.Add("Price(PUR)", typeof(string));
            dt.Columns.Add("Price(RDM)", typeof(string));
            dt.Columns.Add("Currency", typeof(string));
            dt.Columns.Add("ReinvestmentPrice", typeof(string));

            foreach (var line in source)
            {
                string[] splitName = line.Split(',');
                DataRow dr = dt.NewRow();
                dr["FundCode"] = splitName[0];
                dr["FundName"] = splitName[1];
                dr["PriceDate"] = splitName[2];
                dr["Price(NAV)"] = splitName[3];
                dr["Price(PUR)"] = splitName[4];
                dr["Price(RDM)"] = splitName[5];
                dr["Currency"] = splitName[6];
                dr["ReinvestmentPrice"] = splitName[7];
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public static DataTable CreateProductSecurityPriceList(string TransactionsCsv)
        {

            if (string.IsNullOrEmpty(TransactionsCsv) || string.IsNullOrEmpty(TransactionsCsv.Trim()))
            {
                return null;
            }
            var sep = new[] { "\r", "\n" };
            var source = TransactionsCsv.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            DataTable dt = new DataTable();
            dt.TableName = "ProductSecurityPrice";
            dt.Columns.Add("StockCode", typeof(string));
            dt.Columns.Add("Market", typeof(string));
            dt.Columns.Add("Currency", typeof(string));
            dt.Columns.Add("Recommendation", typeof(string));
            dt.Columns.Add("Weighting", typeof(string));
            dt.Columns.Add("Comment", typeof(string));
            dt.Columns.Add("BuyPrice", typeof(string));
            dt.Columns.Add("SellPrice", typeof(string));
            dt.Columns.Add("DynamicRatingOption", typeof(string));

            foreach (var line in source)
            {
                string[] splitName = line.Split(',');
                DataRow dr = dt.NewRow();
                dr["StockCode"] = splitName[0];
                dr["Market"] = splitName[1];
                dr["Currency"] = splitName[2];
                dr["Recommendation"] = splitName[3];
                dr["Weighting"] = splitName[4];
                dr["Comment"] = splitName[5];
                dr["BuyPrice"] = splitName[7];
                dr["SellPrice"] = splitName[8];
                dr["DynamicRatingOption"] = splitName[9];
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public static DataTable CreateTermDepositPriceList(string TransactionsCsv, string pricedate)
        {

            if (string.IsNullOrEmpty(TransactionsCsv) || string.IsNullOrEmpty(TransactionsCsv.Trim()))
            {
                return null;
            }
            var sep = new[] { "\r", "\n" };
            var source = TransactionsCsv.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            DataTable dt = new DataTable();
            dt.TableName = "TermDepositPrice";
            dt.Columns.Add("Provider", typeof(string));
            dt.Columns.Add("ProviderType", typeof(string));
            dt.Columns.Add("SPLongTermRating", typeof(string));
            dt.Columns.Add("Min", typeof(string));
            dt.Columns.Add("Max", typeof(string));
            dt.Columns.Add("MaximumBrokeage", typeof(string));
            dt.Columns.Add("Days30", typeof(string));
            dt.Columns.Add("Days60", typeof(string));
            dt.Columns.Add("Days90", typeof(string));
            dt.Columns.Add("Days120", typeof(string));
            dt.Columns.Add("Days150", typeof(string));
            dt.Columns.Add("Days180", typeof(string));
            dt.Columns.Add("Days270", typeof(string));
            dt.Columns.Add("Years1", typeof(string));
            dt.Columns.Add("Years2", typeof(string));
            dt.Columns.Add("Years3", typeof(string));
            dt.Columns.Add("Years4", typeof(string));
            dt.Columns.Add("Years5", typeof(string));
            dt.Columns.Add("Date", typeof(string));
            dt.Columns.Add("Status", typeof(string));

            foreach (var line in source)
            {
                string[] splitName = line.Split(',');
                DataRow dr = dt.NewRow();
                dr["Provider"] = splitName[0].RemoveQuotes();
                dr["ProviderType"] = splitName[1].RemoveQuotes();
                //dr["SPLongTermRating"] = splitName[2].RemoveQuotes().Trim() == "" ? "Unrated" : splitName[2].RemoveQuotes();
                dr["SPLongTermRating"] = (!string.IsNullOrEmpty(splitName[2].RemoveQuotes().Trim()) && InstituteCombos.SPLongTermRatings.ContainsKey(splitName[2].RemoveQuotes().Trim())) ? splitName[2].RemoveQuotes().Trim() : "Unrated";
                dr["Min"] = splitName[3].RemoveQuotes();
                dr["Max"] = splitName[4].RemoveQuotes();
                dr["MaximumBrokeage"] = splitName[5].RemoveQuotes();
                dr["Days30"] = splitName[6].RemoveQuotes();
                dr["Days60"] = splitName[7].RemoveQuotes();
                dr["Days90"] = splitName[8].RemoveQuotes();
                dr["Days120"] = splitName[9].RemoveQuotes();
                dr["Days150"] = splitName[10].RemoveQuotes();
                dr["Days180"] = splitName[11].RemoveQuotes();
                dr["Days270"] = splitName[12].RemoveQuotes();
                dr["Years1"] = splitName[13].RemoveQuotes();
                dr["Years2"] = splitName[14].RemoveQuotes();
                dr["Years3"] = splitName[15].RemoveQuotes();
                dr["Years4"] = splitName[16].RemoveQuotes();
                dr["Years5"] = splitName[17].RemoveQuotes();
                dr["Date"] = pricedate;
                dr["Status"] = "Uploaded";
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public static string RemoveQuotes(this string s)
        {
            return s.Replace("\"", "");
        }
        public static DataTable CreateMISFundTransactionsList(string transactionsCsv)
        {
            if (string.IsNullOrEmpty(transactionsCsv) || string.IsNullOrEmpty(transactionsCsv.Trim()))
            {
                return null;
            }

            var sep = new[] { "\r", "\n" };
            var source = transactionsCsv.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            DataTable dt = new DataTable();
            dt.TableName = "MISFundTransactionsList";
            dt.Columns.Add("TradeDate", typeof(string));
            dt.Columns.Add("FundCode", typeof(string));
            dt.Columns.Add("FundName", typeof(string));
            dt.Columns.Add("Account", typeof(string));
            dt.Columns.Add("SubAccount", typeof(string));
            dt.Columns.Add("TransactionType", typeof(string));
            dt.Columns.Add("Shares", typeof(string));
            dt.Columns.Add("UnitPrice", typeof(string));
            dt.Columns.Add("Amount", typeof(string));
            dt.Columns.Add("InvestorCategory", typeof(string));

            foreach (var line in source)
            {
                string lineObject = line;

                if (lineObject.Contains("\t"))
                    lineObject = lineObject.Replace("\t", ",");

                var splitName = lineObject.Split(',');

                DataRow dr = dt.NewRow();
                dr["TradeDate"] = splitName[0].Trim();
                dr["FundCode"] = splitName[1].Trim();
                dr["FundName"] = splitName[2].Trim();
                dr["Account"] = splitName[3].Trim();
                dr["SubAccount"] = splitName[4].Trim();
                dr["TransactionType"] = splitName[5].Trim();
                dr["Shares"] = splitName[6].Trim();
                dr["UnitPrice"] = splitName[7].Trim();
                dr["Amount"] = splitName[8].Trim();
                dr["InvestorCategory"] = splitName[9].Trim();
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public static DataTable CreateMisUnitHoldList(string TransactionsCsv)
        {

            if (string.IsNullOrEmpty(TransactionsCsv) || string.IsNullOrEmpty(TransactionsCsv.Trim()))
            {
                return null;
            }
            var sep = new[] { "\r", "\n" };
            var source = TransactionsCsv.Split(sep, StringSplitOptions.RemoveEmptyEntries);

            DataTable dt = new DataTable();
            dt.TableName = "MISUnitHoldList";
            dt.Columns.Add("Account", typeof(string)).DefaultValue = string.Empty;
            dt.Columns.Add("FundCode", typeof(string)).DefaultValue = string.Empty;
            dt.Columns.Add("FundName", typeof(string)).DefaultValue = string.Empty;
            dt.Columns.Add("TotalShares", typeof(string)).DefaultValue = string.Empty;
            dt.Columns.Add("RedemptionPrice", typeof(string)).DefaultValue = string.Empty;
            dt.Columns.Add("CurrentValue", typeof(string)).DefaultValue = string.Empty;
            dt.Columns.Add("AsOfDate", typeof(string)).DefaultValue = string.Empty;
            dt.Columns.Add("SubAccount", typeof(string)).DefaultValue = string.Empty;
            dt.Columns.Add("InvestorCategory", typeof(string)).DefaultValue = string.Empty;

            foreach (var line in source)
            {
                string lineObject = line;

                if (lineObject.Contains("\t"))
                    lineObject = lineObject.Replace("\t", ",");

                var splitName = lineObject.Split(',');
                DataRow dr = dt.NewRow();
                dr["Account"] = splitName[0].Trim();
                dr["FundCode"] = splitName[1].Trim();
                dr["FundName"] = splitName[2].Trim();
                dr["TotalShares"] = splitName[3].Trim() != string.Empty ? splitName[3].Trim() : "0";
                dr["RedemptionPrice"] = splitName[4].Trim() != string.Empty ? splitName[4].Trim() : "0";
                dr["CurrentValue"] = splitName[5].Trim().Contains("\"") ? splitName[5].Trim().Replace("\"", "") + splitName[6].Trim().Replace("\"", "") : splitName[5].Trim();
                if (String.IsNullOrEmpty(dr["CurrentValue"].ToString().Trim()))
                    dr["CurrentValue"] = "0";
                dr["AsOfDate"] = splitName[5].Trim().Contains("\"") ? splitName[7].Trim() : splitName[6].Trim();
                dt.Rows.Add(dr);
            }
            return dt;
        }

    }

}