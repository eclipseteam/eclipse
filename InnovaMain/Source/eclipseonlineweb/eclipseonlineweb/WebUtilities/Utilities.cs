﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Text;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Data;

namespace eclipseonlineweb.WebUtilities
{
    public class Utilities
    {
        public static IDictionary<string,string> GetEnumList<TEnum>() where TEnum : struct
        {
            var enumerationType = typeof(TEnum);

            if (!enumerationType.IsEnum)
                throw new ArgumentException("Enumeration type is expected.");

            var dictionary = new Dictionary<string,string>();

            foreach (var value in Enum.GetValues(enumerationType))
            {
                var name = Enum.GetName(enumerationType,value);
                dictionary.Add(name, value.ToString());
            }

            return dictionary;
        }

        public static DateTime? GetDateFromString(string s,string ShortDateformat)
        {
            DateTime date;
            DateTimeFormatInfo info = new DateTimeFormatInfo() { ShortDatePattern = ShortDateformat };
            bool parsed = false;
            if (DateTime.TryParse(s, info, DateTimeStyles.None, out date))
            {
                parsed = true;
            }

   
            return parsed?date:(DateTime?) null;
        }

        public static void ShowMessageForWebForm(string message)
        {
            // Cleans the message to allow single quotation marks
            string cleanMessage = message.Replace("'", "\'");
            string script = "alert('" + cleanMessage + "');";


            // Gets the executing web page
            Page page = HttpContext.Current.CurrentHandler as Page;
            page.ClientScript.RegisterStartupScript(page.GetType(), System.Guid.NewGuid().ToString(), "alert('" + cleanMessage + "')");

            // Checks if the handler is a Page and that the script isn't allready on the Page
            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
            {
                page.ClientScript.RegisterClientScriptBlock(typeof(string), "alert", script, true);
            }
        }

        public static DateTime LastDayOfMonthFromDateTime(DateTime dateTime)
        {
            DateTime firstDayOfTheMonth = new DateTime(dateTime.Year, dateTime.Month, 1);
            return firstDayOfTheMonth.AddMonths(1).AddDays(-1);
        }

        public static DateTime FirstDayOfMonthFromDateTime(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }
        public static void WriteToFile(string strPath, ref byte[] Buffer)
        {
            // Create a file
            FileStream newFile = new FileStream(strPath, FileMode.Create);

            // Write data to the file
            newFile.Write(Buffer, 0, Buffer.Length);

            // Close file
            newFile.Close();
        }

        public static string Datatable_to_csv(DataTable dt, string csvFile)
        {
            // Create the CSV file to which Datatable data will be exported.            
            StringBuilder sb = new StringBuilder();

            int iColCount = dt.Columns.Count;

            // First we will write the headers if IsFirstRowColumnNames is true:
            for (int i = 0; i < iColCount; i++)
            {
                sb.Append(dt.Columns[i]);
                if (i < iColCount - 1)
                {
                    sb.Append(',');
                }
            }
            sb.Append(Environment.NewLine);           

            // Now write all the rows.
            foreach (DataRow dr in dt.Rows)
            {
                for (int i = 0; i < iColCount; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        sb.Append(dr[i].ToString().Contains(",")? "\"" + dr[i] + "\"":dr[i].ToString());
                    }                    
                    if (i < iColCount - 1)
                    {
                        sb.Append(',');
                    }
                }
                sb.Append(Environment.NewLine);
            }

            return sb.ToString();            
        }

        public static byte[] StrToByteArray(string str)
        {
            UnicodeEncoding encoding = new UnicodeEncoding();
            return encoding.GetBytes(str);
        }
        public static string LogException(Exception exception,HttpRequest request,HttpContext context)
        {
            string strErrorLogFile = ErrorCollector.LogException(exception, request.PhysicalApplicationPath, context);
            return strErrorLogFile;
        }

        public static int CalculateCurrentAge(DateTime dob)
        {
            DateTime now = DateTime.Today;
            int age = now.Year - dob.Year;
            if (dob > now.AddYears(-age)) age--;
            return age;
        }

        public static string ConvertToXML(object obj)
        {
            var stringWriter = new System.IO.StringWriter();
            var serializer = new System.Xml.Serialization.XmlSerializer(obj.GetType());
            serializer.Serialize(stringWriter, obj);
            return stringWriter.ToString();
        }

        public static void SaveToFile(string messag, string path)
        {
            System.IO.File.WriteAllText(path, messag);
        }

        public static bool IsTestApp()
        {
            return DBConnection.IsTestMode;
        }

        public static int CalculateExecutionDays(int reminderDuration, AlertDurationType durationType, AlertExecutionMode executionMode)
        {
            int executionDays = 0;
            int calculationDays = 0;
            switch (durationType)
            {
                case AlertDurationType.Day:
                    calculationDays = reminderDuration * 1;
                    break;
                case AlertDurationType.Week:
                    calculationDays = reminderDuration * 7;
                    break;
                case AlertDurationType.Month:
                    calculationDays = reminderDuration * 30;
                    break;
            }
            switch (executionMode)
            {
                case AlertExecutionMode.Before:
                    executionDays = calculationDays * -1;
                    break;
                case AlertExecutionMode.OnDueDate:
                    executionDays = calculationDays * 0;
                    break;
                case AlertExecutionMode.After:
                    executionDays = calculationDays;
                    break;
            }
            return executionDays;
        }
    }
}
