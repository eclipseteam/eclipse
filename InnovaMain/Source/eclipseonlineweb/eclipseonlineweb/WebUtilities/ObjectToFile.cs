﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;

namespace eclipseonlineweb.WebUtilities
{
    public class ObjectToFile<T> where T : new()
    {
        private string filename;
        public ObjectToFile(string FileName)
        {
            filename = FileName;
        }

        public T AccessFile( T TT, bool write)
        {
            T t;
            t = new T();
            lock (this)
            {
                if (write)
                {

                    SerializeObject( TT);
                }
                else
                {
                    t = DeSerializeObject();
                }
                // Critical code section
            }
            return t;
        }
        private void SerializeObject( T T)
        {
            Stream stream = File.Open(filename, FileMode.Create, FileAccess.Write, FileShare.None);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, T);
            stream.Close();
        }

        private T DeSerializeObject()
        {
            T T;
            try
            {
                Stream stream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.Read);

                BinaryFormatter bFormatter = new BinaryFormatter();
                T = (T)bFormatter.Deserialize(stream);
                stream.Close();
                return T;
            }
            catch (Exception)
            {
                return new T();
            }
        }
    }

}

