﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eclipseonlineweb.WebUtilities
{
    public class ChoiceNameValue
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string ShortForm { get; set; }
        public ChoiceNameValue(string name, string shortForm)
        {
            this.Name = name;
            this.Value = name;
            this.ShortForm = shortForm;
        }
    }
}