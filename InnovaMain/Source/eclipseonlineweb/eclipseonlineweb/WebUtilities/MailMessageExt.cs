﻿using System;
using System.Net.Mail;
using System.IO;
using System.Reflection;

namespace eclipseonlineweb.WebUtilities
{
    public static class MailMessageExt
    {
        private static MemoryStream _fileStream;

        public static void Save(MailMessage message, Stream msr)
        {
            Assembly assembly = typeof(SmtpClient).Assembly;
            Type mailWriterType = assembly.GetType("System.Net.Mail.MailWriter");
            _fileStream = new MemoryStream();

            ConstructorInfo mailWriterContructor = mailWriterType.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { typeof(Stream) }, null);

            object mailWriter = mailWriterContructor.Invoke(new object[] { _fileStream });
            MethodInfo sendMethod = typeof(MailMessage).GetMethod("Send", BindingFlags.Instance | BindingFlags.NonPublic);
            //introduced try catch because of argument difference in .Net Framework 4.0 and 4.5 as one more argument was added in 4.5 - Samee suggested to keep both
            try
            {
                //4.0
                sendMethod.Invoke(message, BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { mailWriter, true }, null);
            }
            catch
            {
                //4.5
                sendMethod.Invoke(message, BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { mailWriter, true, true }, null);    
            }
            
            MethodInfo closeMethod = mailWriter.GetType().GetMethod("Close", BindingFlags.Instance | BindingFlags.NonPublic);
            _fileStream.WriteTo(msr);
            closeMethod.Invoke(mailWriter, BindingFlags.Instance | BindingFlags.NonPublic, null, new object[] { }, null);
        }
    }
}