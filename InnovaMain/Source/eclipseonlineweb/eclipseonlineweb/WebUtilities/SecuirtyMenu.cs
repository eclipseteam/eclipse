﻿using System.Web;
using System.Web.UI.WebControls;

namespace eclipseonlineweb.WebUtilities
{
    public class SecuirtyMenu
    {
        public static void RedirectPage(string menutext, string secid, HttpResponse response)
        {
            if (menutext == "SECURITIES")
                response.Redirect(@"Securities.aspx");
            else if (menutext == "DISTRIBUTIONS")
                response.Redirect(@"SecurityDistributions.aspx?ID=" + secid);
            else if (menutext == "DISTRIBUTIONS REPORT")
                response.Redirect(@"SecurityDistributionsReport.aspx?ID=" + secid);
            else if (menutext == "HISTORICAL DATA")
                response.Redirect(@"SecurityDetails.aspx?ID=" + secid);
            else if (menutext == "DIVIDENDS")
                response.Redirect(@"SecurityDividends.aspx?ID=" + secid);
            else if (menutext == "DIVIDENDS REPORT")
                response.Redirect(@"SecurityDividendsReport.aspx?ID=" + secid);
        }

        public static string SetSecurityMenu(string code)
        {
            return code.StartsWith("IV0") ? "1" : "";
        }

        public static void SetMenu(MenuItemCollection menuItemCollection, string code, string selected)
        {
            foreach (MenuItem menuItem in menuItemCollection)
            {
                if (code.StartsWith("IV0"))
                {
                    if (menuItem.Text == "SECURITIES")
                    {
                        menuItem.ChildItems.Add(GetMenuItem("DISTRIBUTIONS", selected));
                        //  menuItem.ChildItems.Add(GetMenuItem("DISTRIBUTIONS REPORT", selected));
                    }
                }
                else
                {
                    if (menuItem.Text == "SECURITIES")
                    {
                        menuItem.ChildItems.Add(GetMenuItem("DIVIDENDS", selected));
                        menuItem.ChildItems.Add(GetMenuItem("DIVIDENDS REPORT", selected));
                    }
                }
                if (menuItem.ChildItems.Count > 0)
                {
                    SetMenu(menuItem.ChildItems, code, selected);
                }
            }
        }

        private static MenuItem GetMenuItem(string text, string selected)
        {
            var menu = new MenuItem() { Text = text, Selected = (text == selected) };
            return menu;
        }
    }
}