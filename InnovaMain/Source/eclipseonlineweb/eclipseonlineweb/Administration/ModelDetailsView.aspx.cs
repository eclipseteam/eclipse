﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace eclipseonlineweb
{
    public partial class ModelDetailsView : UMABasePage
    {
        string selectAssetID = string.Empty;
        bool isExport = false;
        private Hashtable _ordersExpandedState;

        protected override void GetBMCData()
        {
            this.cid = Request.QueryString["ModelID"].ToString();
            PopulateForm();
            this.lblModelName.Text = PresentationData.Tables[ModelsDS.MODELTABLE].Rows[0][ModelsDS.PROGRAMCODE].ToString() + " - " + PresentationData.Tables[ModelsDS.MODELTABLE].Rows[0][ModelsDS.MODELNAME].ToString();
        }

        protected void PopulateForm(bool rebind=false)
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            ModelsDS modelsDS = new Oritax.TaxSimp.DataSets.ModelsDS();
            modelsDS.GetDetails = true;
            modelsDS.DetailsGUID = new Guid(this.cid);

            organization.GetData(modelsDS);
            this.PresentationData = modelsDS;

            if (!this.IsPostBack || rebind)
            {
                DataView view = new DataView(modelsDS.Tables[ModelsDS.MODELDETAILSLISTASSETS]);
                view.Sort = ModelsDS.ASSETDESCRIPTION + " ASC";

                this.RadGrid1.DataSource = view.ToTable();
                RadGrid1.DataBind();
            }

            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        public override void LoadPage()
        {
            base.LoadPage();

            //if (!IsPostBack)
            //{
            //    //reset states
            //    this._ordersExpandedState = null;
            //    this.Session["_ordersExpandedState"] = null;
            //}

            RadGrid1.ExportSettings.IgnorePaging = true;
            RadGrid1.ExportSettings.FileName = "ModelDetailedStructure";
            RadGrid1.ExportSettings.ExportOnlyData = true;

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
            {
                RadGrid1.Columns[RadGrid1.Columns.Count - 1].Visible = false;
                RadGrid1.Enabled = false;
                ((SetupMaster)Master).IsAdmin = "1";
            }

            UMABroker.ReleaseBrokerManagedComponent(objUser);
        }

        public override void PopulatePage(DataSet ds)
        {
            DataView ModelListView = new DataView(ds.Tables[ModelsDS.MODELLIST]);
            ModelListView.Sort = ModelsDS.MODELNAME + " ASC";

        }

        protected void btnBackToListing(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Administration/AdministrationView.aspx");
        }


        #region Telerik Code

        protected void RadGrid1_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e)
        {

        }

        protected void RadGrid1_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;

            switch (e.DetailTableView.Name)
            {
                case "Products":
                    {
                        string assetID = dataItem.GetDataKeyValue("AssetID").ToString();

                        IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                        ModelsDS modelsDS = new Oritax.TaxSimp.DataSets.ModelsDS();

                        modelsDS.GetDetails = true;
                        modelsDS.DetailsGUID = new Guid(this.cid);
                        organization.GetData(modelsDS);

                        DataView view = new DataView(modelsDS.Tables[ModelsDS.MODELDETAILSLISTPRO]);
                        view.Sort = ModelsDS.PRODESCRIPTION + " ASC";
                        view.RowFilter = ModelsDS.ASSETID + "= '" + assetID + "'";

                        e.DetailTableView.DataSource = view.ToTable();

                        this.UMABroker.ReleaseBrokerManagedComponent(organization);

                        break;
                    }
            }
        }

        protected void RadGrid1_DataBound (object sender, EventArgs e)
        {
           //SetExpandedItems();
        }
        protected void RadGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                if ("Assets".Equals(e.Item.OwnerTableView.Name) && ((RadComboBox)(((GridEditableItem)(e.Item))["AssetDescription"].Controls[0])).SelectedItem != null)
                    selectAssetID = ((RadComboBox)(((GridEditableItem)(e.Item))["AssetDescription"].Controls[0])).SelectedItem.Value;

                GridEditableItem editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;
                GridDropDownListColumnEditor editor = null;
                IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

                if ("Assets".Equals(e.Item.OwnerTableView.Name))
                {
                    editor = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("AssetDescription"));
                    editor.ComboBoxControl.Filter = RadComboBoxFilter.Contains;
                    editor.DataSource = organization.Assets.OrderBy(ass => ass.Description);
                    editor.DataTextField = "Description";
                    editor.DataValueField = "ID";
                    editor.DataBind();

                    if (!(e.Item.DataItem is GridInsertionObject))
                    {

                        string id = e.Item.OwnerTableView.ParentItem[ModelsDS.ASSETID].Text;

                        if (id != string.Empty)
                            editor.ComboBoxControl.FindItemByText(id).Selected = true;
                    }
                }
                else
                {

                    editor = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("ProDescription"));
                    editor.ComboBoxControl.Filter = RadComboBoxFilter.Contains;
                    editor.DataSource = organization.Products.OrderBy(pro => pro.Description);
                    editor.DataTextField = "Description";
                    editor.DataValueField = "ID";
                    editor.DataBind();

                    //GridDataItem item = (GridDataItem)e.Item;
                    //item.OwnerTableView.Columns.FindByUniqueName("ProDescription").Visible = true;
                    //item.OwnerTableView.Columns.FindByUniqueName("ProDescriptionText").Visible = false;  

                    if (!(e.Item.DataItem is GridInsertionObject))
                    {
                        string id = ((System.Data.DataRowView)(e.Item.DataItem)).Row[ModelsDS.PROID].ToString();
                        if (id != string.Empty)
                            editor.ComboBoxControl.FindItemByValue(id).Selected = true;
                    }
                    
                }

                UMABroker.ReleaseBrokerManagedComponent(organization);
            }
            else if (e.Item is GridFooterItem && !e.Item.IsInEditMode && "Assets".Equals(e.Item.OwnerTableView.Name))
            {
                GridFooterItem footer = (GridFooterItem)e.Item;
                string strtxt = footer["AssetNeutralPercentage"].Text;

                decimal value;

                decimal.TryParse(strtxt, out value);

                if (value != 100)
                {
                    ((System.Web.UI.WebControls.WebControl)(footer["AssetNeutralPercentage"])).ToolTip = "Total of Neutral allocations must equal 100";
                    ((System.Web.UI.WebControls.WebControl)(footer["AssetNeutralPercentage"])).BackColor = Color.Red;
                }


                string strtdynamic = footer["AssetDynamicPercentage"].Text;

                decimal.TryParse(strtdynamic, out value);
                if (value != 100)
                {
                    ((System.Web.UI.WebControls.WebControl)(footer["AssetDynamicPercentage"])).ToolTip = "Total of Dynamic allocations must equal 100";
                    ((System.Web.UI.WebControls.WebControl)(footer["AssetDynamicPercentage"])).BackColor = Color.Red;
                }
            }
        }



        protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            ModelsDS modelsDS = new Oritax.TaxSimp.DataSets.ModelsDS();
            modelsDS.GetDetails = true;
            modelsDS.DetailsGUID = new Guid(this.cid);

            organization.GetData(modelsDS);
            this.PresentationData = modelsDS;


            DataView view = new DataView(modelsDS.Tables[ModelsDS.MODELDETAILSLISTASSETS]);
            view.Sort = ModelsDS.ASSETDESCRIPTION + " ASC";

            this.RadGrid1.DataSource = view.ToTable();
        }

        protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
        {

            if (e.CommandName == RadGrid.ExpandCollapseCommandName)
            {
                foreach (GridItem item in e.Item.OwnerTableView.Items)
                {
                    if (item.Expanded && item != e.Item)
                        item.Expanded = false;
                }
            }

            if (e.CommandName.Contains("Export"))
            {
                isExport = true;
                RadGrid1.MasterTableView.HierarchyDefaultExpanded = true; // for the first level
                RadGrid1.MasterTableView.DetailTables[0].HierarchyDefaultExpanded = true; // for the second level 
                RadGrid1.ExportSettings.IgnorePaging = false;
            }
            else if (e.CommandName.ToLower() == "delete")
            {
                if ("Assets".Equals(e.Item.OwnerTableView.Name))
                {
                    selectAssetID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["AssetID"].ToString();

                    UMABroker.SaveOverride = true;
                    IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                    ModelsDS modelsDS = new ModelsDS();
                    modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.DeleteAsset;
                    modelsDS.ModelEntityID = new Guid(this.cid);
                    modelsDS.AssetEntityID = new Guid(selectAssetID);

                    iorg.SetData(modelsDS);
                    UMABroker.SetComplete();
                    UMABroker.SetStart();
                    e.Item.Edit = false;
                }
                else if ("Products".Equals(e.Item.OwnerTableView.Name))
                {
                    string parentID = string.Empty;

                    GridDataItem parentItem = (GridDataItem)(e.Item.OwnerTableView.ParentItem);

                    if (parentItem != null)
                        parentID = parentItem.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["AssetID"].ToString();

                    string productID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ProID"].ToString();
                    UMABroker.SaveOverride = true;
                    IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                    ModelsDS modelsDS = new ModelsDS();
                    modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.DeleteProduct;
                    modelsDS.ModelEntityID = new Guid(this.cid);
                    modelsDS.AssetEntityID = new Guid(parentID);
                    modelsDS.ProductEntityID = new Guid(productID);
                    iorg.SetData(modelsDS);

                    UMABroker.SetComplete();
                    UMABroker.SetStart();
                    e.Item.Edit = false;

                }
            }
            ////save the expanded/selected state in the session
            //else if (e.CommandName == RadGrid.ExpandCollapseCommandName)
            //{
            //    //Is the item about to be expanded or collapsed
            //    if (!e.Item.Expanded)
            //    {
            //        //Save its unique index among all the items in the hierarchy
            //        this.ExpandedStates[e.Item.ItemIndexHierarchical] = true;
            //    }
            //    else //collapsed
            //    {
            //        this.ExpandedStates.Remove(e.Item.ItemIndexHierarchical);
            //        this.ClearExpandedChildren(e.Item.ItemIndexHierarchical);
            //    }
            //}
            if (e.CommandName.ToLower() == "update")
            {
                if ("Products".Equals(e.Item.OwnerTableView.Name))
                {
                    ModelsDS modelsDS = PopulateDatasetWithProductDataUpdate(e, ModelDataSetOperationType.UpdateProduct);

                    UMABroker.SaveOverride = true;
                    IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                    iorg.SetData(modelsDS);


                    UMABroker.SetComplete();
                    UMABroker.SetStart();
                    e.Item.Edit = false;
                }
                this.PopulateForm(true);
                e.Canceled = true;
            }
        }

        private ModelsDS PopulateDatasetWithProductData(GridCommandEventArgs e, ModelDataSetOperationType type)
        {
            ModelsDS modelsDS = new ModelsDS();

            string parentID = string.Empty;
            GridDataItem parentItem = (GridDataItem)(e.Item.OwnerTableView.ParentItem);
            if (parentItem != null)
                parentID = parentItem.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["AssetID"].ToString();

            GridDataItem gdItem = (e.Item as GridDataItem);
            modelsDS.ProMinAllocation = Double.Parse(((TextBox)(gdItem["ProMinAllocation"].Controls[0])).Text);
            modelsDS.ProNeutralPercentage = Double.Parse(((TextBox)(gdItem["ProNeutralPercentage"].Controls[0])).Text);
            modelsDS.ProDynamicPercentage = Double.Parse(((TextBox)(gdItem["ProDynamicPercentage"].Controls[0])).Text);
            modelsDS.ProMaxAllocation = Double.Parse(((TextBox)(gdItem["ProMaxAllocation"].Controls[0])).Text);

            //string productID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ProID"].ToString();
            string productID = ((RadComboBox)gdItem["ProDescription"].Controls[0]).SelectedValue;

            modelsDS.ModelEntityID = new Guid(this.cid);
            modelsDS.AssetEntityID = new Guid(parentID);
            modelsDS.ProductEntityID = new Guid(productID);

            modelsDS.ModelDataSetOperationType = type;
            return modelsDS;
        }

        private ModelsDS PopulateDatasetWithProductDataUpdate(GridCommandEventArgs e, ModelDataSetOperationType type)
        {
            ModelsDS modelsDS = new ModelsDS();

            string parentID = string.Empty;
            GridDataItem parentItem = (GridDataItem)(e.Item.OwnerTableView.ParentItem);
            if (parentItem != null)
                parentID = parentItem.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["AssetID"].ToString();

            GridEditFormItem gdItem = (e.Item as GridEditFormItem);
            modelsDS.ProMinAllocation = Double.Parse(((TextBox)(gdItem["ProMinAllocation"].Controls[0])).Text);
            modelsDS.ProNeutralPercentage = Double.Parse(((TextBox)(gdItem["ProNeutralPercentage"].Controls[0])).Text);
            modelsDS.ProDynamicPercentage = Double.Parse(((TextBox)(gdItem["ProDynamicPercentage"].Controls[0])).Text);
            modelsDS.ProMaxAllocation = Double.Parse(((TextBox)(gdItem["ProMaxAllocation"].Controls[0])).Text);

            //string productID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ProID"].ToString();
            string productID = ((RadComboBox)gdItem["ProDescription"].Controls[0]).SelectedValue;

            modelsDS.ModelEntityID = new Guid(this.cid);
            modelsDS.AssetEntityID = new Guid(parentID);
            modelsDS.ProductEntityID = new Guid(productID);

            modelsDS.ModelDataSetOperationType = type;
            return modelsDS;
        }

       
        private ModelsDS PopulateDatasetWithProductDataInsert(GridCommandEventArgs e, ModelDataSetOperationType type)
        {
            ModelsDS modelsDS = new ModelsDS();

            string parentID = string.Empty;
            GridDataItem parentItem = (GridDataItem)(e.Item.OwnerTableView.ParentItem);
            if (parentItem != null)
                parentID = parentItem.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["AssetID"].ToString();

            GridEditFormInsertItem gdItem = (e.Item as GridEditFormInsertItem);
            modelsDS.ProMinAllocation = Double.Parse(((TextBox)(gdItem["ProMinAllocation"].Controls[0])).Text);
            modelsDS.ProNeutralPercentage = Double.Parse(((TextBox)(gdItem["ProNeutralPercentage"].Controls[0])).Text);
            modelsDS.ProDynamicPercentage = Double.Parse(((TextBox)(gdItem["ProDynamicPercentage"].Controls[0])).Text);
            modelsDS.ProMaxAllocation = Double.Parse(((TextBox)(gdItem["ProMaxAllocation"].Controls[0])).Text);

            //string productID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ProID"].ToString();
            string productID = ((RadComboBox)gdItem["ProDescription"].Controls[0]).SelectedValue;

            modelsDS.ModelEntityID = new Guid(this.cid);
            modelsDS.AssetEntityID = new Guid(parentID);
            modelsDS.ProductEntityID = new Guid(productID);

            modelsDS.ModelDataSetOperationType = type;
            return modelsDS;
        }

        

        protected void RadGrid1_ItemInserted(object source, GridInsertedEventArgs e)
        {

        }

        protected void RadGrid1_ItemDeleted(object source, GridDeletedEventArgs e)
        {

        }

        protected void RadGrid1_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("Assets".Equals(e.Item.OwnerTableView.Name))
            {
                GridEditFormInsertItem EditForm = (GridEditFormInsertItem)e.Item;
                RadComboBox combo = (RadComboBox)EditForm["AssetDescription"].Controls[0];
                string ProdWorkstation = combo.SelectedItem.Text;

                UMABroker.SaveOverride = true;
                selectAssetID = ((RadComboBox)(((GridEditableItem)(e.Item))["AssetDescription"].Controls[0])).SelectedItem.Value;

                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                ModelsDS modelsDS = new ModelsDS();
                modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.NewAsset;
                modelsDS.ModelEntityID = new Guid(this.cid);
                modelsDS.AssetEntityID = new Guid(selectAssetID);
                iorg.SetData(modelsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();


            }
            else if ("Products".Equals(e.Item.OwnerTableView.Name))
            {
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                ModelsDS modelsDS = PopulateDatasetWithProductDataInsert(e, ModelDataSetOperationType.NewProduct);

                iorg.SetData(modelsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
            }
            e.Item.Edit = false;
            e.Canceled = true;
            this.RadGrid1.Rebind();
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (isExport)
            {
                this.RadGrid1.MasterTableView.BackColor = System.Drawing.Color.Gray;
                this.RadGrid1.MasterTableView.DetailTables[0].BackColor = System.Drawing.Color.LightGray;
            }
            else
            {
                this.RadGrid1.MasterTableView.BackColor = System.Drawing.Color.White;
                this.RadGrid1.MasterTableView.DetailTables[0].BackColor = System.Drawing.Color.LightGray;
            }
            base.OnPreRender(e);
        }
        protected override void OnPreRenderComplete(EventArgs e)
        {
            base.OnPreRenderComplete(e);
        }

        protected void RadGrid1_OnGridExporting(object source, GridExportingArgs e)
        {

        }

        protected void RadGrid1_ItemCreated(object sender, GridItemEventArgs e)
        {
            //if (isExport && e.Item is GridHeaderItem)
            //{
            //    if (e.Item.OwnerTableView.Name == ((RadGrid)sender).MasterTableView.Name)
            //        e.Item.OwnerTableView.BackColor = System.Drawing.Color.Gray;
            //    else if (e.Item.OwnerTableView.Name == "Details")
            //        e.Item.OwnerTableView.BackColor = System.Drawing.Color.LightGray;
            //}

            GridEditableItem item = e.Item as GridEditableItem;
            if (item != null  && item.ItemIndex != -1)
            {
                if (item.IsInEditMode)
                {
                    if (item.OwnerTableView.Name == ((RadGrid)sender).MasterTableView.Name)
                    {
                        (item.EditManager.GetColumnEditor("CustomerID").ContainerControl.Controls[0] as TextBox).Enabled = false;
                    }
                    else if (item.OwnerTableView.Name == "Details")
                    {
                        (item.EditManager.GetColumnEditor("ProductID").ContainerControl.Controls[0] as TextBox).Enabled = false;
                    }
                }
            }
        }

        private void DisplayMessage(string text)
        {
            RadGrid1.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
        }

        //Save/load expanded states Hash from the session
        //this can also be implemented in the ViewState
        private Hashtable ExpandedStates
        {
            get
            {
                if (this._ordersExpandedState == null)
                {
                    _ordersExpandedState = this.Session["_ordersExpandedState"] as Hashtable;
                    if (_ordersExpandedState == null)
                    {
                        _ordersExpandedState = new Hashtable();
                        this.Session["_ordersExpandedState"] = _ordersExpandedState;
                    }
                }

                return this._ordersExpandedState;
            }
        }

        //Clear the state for all expanded children if a parent item is collapsed
        private void ClearExpandedChildren(string parentHierarchicalIndex)
        {
            string[] indexes = new string[this.ExpandedStates.Keys.Count];
            this.ExpandedStates.Keys.CopyTo(indexes, 0);
            foreach (string index in indexes)
            {
                //all indexes of child items
                if (index.StartsWith(parentHierarchicalIndex + "_") ||
                    index.StartsWith(parentHierarchicalIndex + ":"))
                {
                    this.ExpandedStates.Remove(index);
                }
            }
        }
        /// <summary>
        /// Expand all items using  custom storage
        /// </summary>
        private void SetExpandedItems()
        {
            string[] indexes = new string[this.ExpandedStates.Keys.Count];
            this.ExpandedStates.Keys.CopyTo(indexes, 0);

            ArrayList arr = new ArrayList(indexes);
            //Sort so we can guarantee that a parent item is expanded before any of its children
            arr.Sort();

            foreach (string key in arr)
            {
                bool value = (bool)this.ExpandedStates[key];
                short intKey = -1;
                Int16.TryParse(key, out intKey);
                if (value && RadGrid1.Items[intKey] != null)
                {
                    RadGrid1.Items[intKey].Expanded = true;
                }
            }
        }
        #endregion
    }
}
