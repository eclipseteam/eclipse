﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using System.Linq;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using C1.Web.Wijmo.Controls.C1ComboBox;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Security;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Telerik.Web.UI;
using Telerik.Charting;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using C1.C1Preview;
using C1.Web.Wijmo.Controls.C1Chart;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using eclipseonlineweb.Administration;
using System.Configuration;
using Oritax.TaxSimp.Utilities;

namespace eclipseonlineweb
{
    /// <summary>
    /// Keep commented lines intact - just in case
    /// </summary>
    public partial class InvestmentProfile2 : UMABasePage
    {
        private DataSet presentationGridData;


        public override void LoadPage()
        {
            //GetReportData();
        }

        private void BindReportData(Guid productId)
        {
                GetReportData(productId);
                DataTable products = PresentationData.Tables[ProductsDS.PRODUCTSTABLE];
                DataRow r = FindRow(new Guid(this.cid), products);
                this.lblName.Text = r[ProductsDS.NAME].ToString() + " - " + r[ProductsDS.DESCRIPTION].ToString();

                string reportName = "Report_" + Guid.NewGuid().ToString();
                C1ReportViewer.RegisterDocument(reportName, MakeDocPerformance);
                C1ReportViewer1.FileName = reportName;
                C1ReportViewer1.ReportName = reportName;
        }

        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Report")
            {
                this.cid = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"].ToString();

                BindReportData(new Guid(this.cid));
            }
        }

        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                GridCommandItem item = (GridCommandItem)e.Item;
                //hide refresh icon  
                ((Button)item.FindControl("RefreshButton")).Visible = false;
            }  
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetPresentationGridData();
            //var f = PresentationGrid.MasterTableView.FilterExpression;

            DataView view = new DataView(presentationGridData.Tables[ProductsDS.PRODUCTSTABLE]);
            view.Sort = ProductsDS.NAME + " ASC";
            view.RowFilter = string.Format("EntityType = '{0}' and Description not like '%GBP%' and Description not like '%Illiquid%' and Description not like '%Protected%' and Description not like '%Passive%'", 
                    OrganizationType.ManagedInvestmentSchemesAccount);

            if (!this.IsPostBack && view.Count > 0)
            {
                cid = view[0]["ID"].ToString();
                this.lblName.Text = view[0][ProductsDS.NAME].ToString() + " - " + view[0][ProductsDS.DESCRIPTION].ToString();
                BindReportData(new Guid(this.cid));
            }
            this.PresentationGrid.DataSource = view.ToTable();
        }

        protected  void GetPresentationGridData()
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            ProductsDS productsDS = new ProductsDS();

            organization.GetData(productsDS);
            presentationGridData = productsDS;

            UMABroker.ReleaseBrokerManagedComponent(organization);
        }
        public C1PrintDocument MakeDocPerformance()
        {
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();

            //AddHeaderToReport(doc, "", this.lblName.Text);
            AddHeaderToReport(doc, this.lblName.Text);

            RenderTable mainTable = CreateTable(doc, 3, 2);

            DataTable table = PresentationData.Tables[InvestmentDS.INVESTMENTMANAGEMENT];
            DataRow reportRow = GetReportDataRow();
            DataRow secRow = GetSecurityDataRow();         

            if (table.Rows.Count > 0)
            {
                DataRow invManagementRow = table.Rows[0];

                RenderText objTitle = CreateTitleText(doc, "OBJECTIVE:");
                doc.Body.Children.Add(objTitle);
                doc.Body.Children.Add(CreateBodyText(doc, invManagementRow[InvestmentDS.OBJECTIVE].ToString()));

                doc.Body.Children.Add(mainTable);     
           
                RenderTable tableInfo = CreateTable(doc, 1, 2);
                mainTable.Cells[0, 0].RenderObject = tableInfo; 
                PopulateBlock_General(doc, tableInfo, reportRow, secRow, invManagementRow);

                RenderTable tableFees = CreateTable(doc, 1, 2);
                mainTable.Cells[1, 0].RenderObject = tableFees;
                PopulateBlock_Fees(doc, tableFees, reportRow, secRow, invManagementRow);
            }


            DataTable performGrowthTable = this.PresentationData.Tables[PerfReportDS.PERFSUMMARYINCOMEGROWTHTABLE];
            DataTable performSummaryTable = this.PresentationData.Tables[PerfReportDS.PERFSUMMARYTABLE];
            DataTable allocationsTable = this.PresentationData.Tables[InvestmentDS.ALLOCATIONS];

            string code = "";
            string benchmarkCode = "";
            if (performSummaryTable.Rows.Count > 0)
                code = performSummaryTable.Rows[0][PerfReportDS.DESCRIPTION].ToString();
            if (performSummaryTable.Rows.Count > 1)
            {
                benchmarkCode = performSummaryTable.Rows[1][PerfReportDS.DESCRIPTION].ToString();
                performSummaryTable.Rows[1][PerfReportDS.DESCRIPTION] = "Benchmark";
            }

            SetPerformanceSummaryGrid(doc, performSummaryTable, "PERFORMANCE SUMMARY");

            System.Drawing.Image growthImage = BuildGrowthChart(new DataView(performGrowthTable), code, benchmarkCode);
            RenderImage img1 = new RenderImage(doc);
            img1.Image = growthImage;
            mainTable.Cells[1, 1].RenderObject = img1;

            RenderImage managersMix = new RenderImage(doc);
            managersMix.Image = BuildManagersMixChart(allocationsTable);
            mainTable.Cells[0, 1].RenderObject = managersMix;

            return doc;
        }

        private void PopulateBlock_General(C1PrintDocument doc, RenderTable childTable, DataRow reportRow, DataRow secRow, DataRow invManagementRow)
        {
            int i = 0;
            SetLine(doc, childTable, i++, "SUMMARY", " ", true);
            SetLine(doc, childTable, i++, "Investment Manager:", invManagementRow[InvestmentDS.INVESTMENTMANAGER].ToString());
            SetLine(doc, childTable, i++, "Portfolo Manager:", invManagementRow[InvestmentDS.PORTFOLOMANAGER].ToString());
            SetLine(doc, childTable, i++, "Trustee:", "Innova Portfolio Management Ltd");
            SetLine(doc, childTable, i++, "Custodian:", "State Street Australia Ltd"); //***
            SetLine(doc, childTable, i++, "Product Manager:", "e-Clipse Online");
            SetLine(doc, childTable, i++, "Fund Type:", invManagementRow[InvestmentDS.INFO].ToString());
            SetLine(doc, childTable, i++, "Asset type:", invManagementRow[InvestmentDS.ASSETTYPE].ToString());
            SetLine(doc, childTable, i++, "Inception Date:", invManagementRow[InvestmentDS.INCEPTIONDATE].ToString());
            SetLine(doc, childTable, i++, "Region:", invManagementRow[InvestmentDS.REGION].ToString());
            SetLine(doc, childTable, i++, "Fund size:", invManagementRow[InvestmentDS.FINDSIZE].ToString());
            SetLine(doc, childTable, i++, "As of:", AccountingFinancialYear.LastDayOfMonthFromDateTime(DateTime.Now.AddMonths(-1)).ToShortDateString());

            string distribType = "";
            string distribFreq = "";
            if (secRow != null)
            {
                distribType = Enum.GetName(typeof(SecurityDistributionsType), (int)secRow[SecuritiesDS.DISTRIBUTIONTYPE]);
                distribFreq = Enum.GetName(typeof(SecurityDistributionsFrequency), (int)secRow[SecuritiesDS.DISTRIBUTIONFREQUENCY]);
            }
            SetLine(doc, childTable, i++, "Minimum investment:", invManagementRow[InvestmentDS.MININVESTMENT].ToString());
            SetLine(doc, childTable, i++, "Minimum Withdrawal:", invManagementRow[InvestmentDS.MINWITHRAWAL].ToString());
            SetLine(doc, childTable, i++, "Minimum Addition:", invManagementRow[InvestmentDS.MINADDITION].ToString());
            SetLine(doc, childTable, i++, "Distributions:", distribType);
            SetLine(doc, childTable, i++, "Distributions frequency:", distribFreq);
            SetLine(doc, childTable, i++, "Recommended investment timeframe:", invManagementRow[InvestmentDS.INVESTMENTTIME].ToString());
        }

        private void PopulateBlock_Fees(C1PrintDocument doc, RenderTable childTable, DataRow reportRow, DataRow secRow, DataRow invManagementRow)
        {
            int i = 0;

            SetLine(doc, childTable, i++, "\n", " ");
            SetLine(doc, childTable, i++, "FEES AND COSTS", " ", true);
            SetLine(doc, childTable, i++, "Upfront", " ", true);
            SetLine(doc, childTable, i++, "Entry Fee:", invManagementRow[InvestmentDS.ENTRYFEE].ToString() + " %");
            SetLine(doc, childTable, i++, "Exit Fee:", invManagementRow[InvestmentDS.EXITFEE].ToString() + " %");
            SetLine(doc, childTable, i++, "Buy/Sell Spread +/-:", invManagementRow[InvestmentDS.BUYSELLSPREAD].ToString() + " %");

            SetLine(doc, childTable, i++, "Ongoing ", " ", true);

            SetLine(doc, childTable, i++, "Management Fee:", invManagementRow[InvestmentDS.MANAGEMENTFEE].ToString() + " %");
            SetLine(doc, childTable, i++, "As of:", invManagementRow[InvestmentDS.MANAGEMENTFEEOF].ToString());
            SetLine(doc, childTable, i++, "ICR:", invManagementRow[InvestmentDS.ICR].ToString() + " %");


        }
        private DataRow GetReportDataRow()
        {
            DataTable invManagement = this.PresentationData.Tables[InvestmentDS.INVESTMENTMANAGEMENT];
            DataTable productTable = this.PresentationData.Tables[ProductsDS.PRODUCTSTABLE];
            DataTable secTable = PresentationData.Tables[SecuritiesDS.SECLIST];
            DataTable fundsTable = PresentationData.Tables["FundAccountList"];

            var result=
                (from  prod in productTable.AsEnumerable()   
                     join inv in invManagement.AsEnumerable() on prod.Field<Guid>(ProductsDS.ID)  equals inv.Field<Guid>(InvestmentDS.ID)
                     join fund in fundsTable.AsEnumerable() on prod.Field<Guid>(ProductsDS.FundAccounts) equals fund.Field<Guid>("FundID")
                     join sec in secTable.AsEnumerable() on fund.Field<string>("FundCode") equals sec.Field<string>(SecuritiesDS.CODE)                     
                     into  jointRec
                 from rec in jointRec.DefaultIfEmpty() where inv.Field<Guid>(InvestmentDS.ID) == new Guid(this.cid) select rec).FirstOrDefault();                    
            return result;
        }
        private DataRow GetSecurityDataRow()
        {
            DataTable invManagement = this.PresentationData.Tables[InvestmentDS.INVESTMENTMANAGEMENT];
            DataTable productTable = this.PresentationData.Tables[ProductsDS.PRODUCTSTABLE];
            DataTable secTable = PresentationData.Tables[SecuritiesDS.SECLIST];
            DataTable fundsTable = PresentationData.Tables["FundAccountList"];

            var result =
                (from prod in productTable.AsEnumerable()
                 join fund in fundsTable.AsEnumerable() on prod.Field<Guid>(ProductsDS.FundAccounts) equals fund.Field<Guid>("FundID")
                 join sec in secTable.AsEnumerable() on fund.Field<string>("FundCode") equals sec.Field<string>(SecuritiesDS.CODE)
                 where prod.Field<Guid>(InvestmentDS.ID) == new Guid(this.cid)
                 select sec).FirstOrDefault();
            return result;
        }
        private void SetLine(C1PrintDocument doc, RenderTable childTable, int line, string title,  string value, bool isBold = false)
        {
            childTable.Cells[line, 0].RenderObject = CreateBodyText(doc, title);
            childTable.Cells[line, 1].RenderObject = CreateBodyText(doc, value);
            if (isBold)
                childTable.Cells[line, 0].RenderObject.Style.FontBold = true;
        }

        public static void AddHeaderToReport(C1PrintDocument doc, string reportname)
        {
            doc.PageLayout.PageSettings.Landscape = false;
            doc.PageLayout.PageSettings.TopMargin = .20;
            doc.PageLayout.PageSettings.LeftMargin = .60;
            doc.PageLayout.PageSettings.RightMargin = .60;

            RenderText title = new RenderText(doc);
            title.Text = reportname + "\n";
            title.Style.TextColor = Color.FromArgb(46, 44, 83);
            title.Style.TextAlignHorz = AlignHorzEnum.Left;
            title.Style.FontSize = 12;
            title.Style.FontBold = true;
            title.Style.WordWrapMode = WordWrapMode.Normal;
            title.Style.WordWrap = true;

            string imageDirectory = ConfigurationManager.AppSettings["Images"];
            RenderImage logoE = new RenderImage();
            logoE.Image = System.Drawing.Image.FromFile(imageDirectory + "\\E-Clipse-logo-onwhite.png", true);


            RenderImage logoI = new RenderImage();
            logoI.Image = System.Drawing.Image.FromFile(imageDirectory + "\\InnovaLogo.png", true);

            RenderText pagesText = new RenderText("Page [PageNo] of [PageCount]");
            pagesText.Style.FontItalic = true;
            RenderTable header = new RenderTable(doc);

            header.Cells[2, 0].RenderObject = title;
            header.Cells[2, 0].SpanRows = 2;
            header.Cells[3, 1].RenderObject = pagesText;            


            header.Cells[0, 0].SpanRows = 2;
            header.Cells[0, 0].RenderObject = logoI;

            header.Cells[0, 1].SpanRows = 2;
            header.Cells[0, 1].RenderObject = logoE;
            header.Cells[0, 1].Style.ImageAlign.AlignHorz = C1.C1Preview.ImageAlignHorzEnum.Right;


            RenderText celltext = new RenderText(doc);
            celltext.Text = "\n\n\n\n";
            header.Cells[4, 0].RenderObject = celltext;
  

            doc.PageLayout.PageHeader = header;
            doc.PageLayout.PageHeader.Style.TextAlignHorz = AlignHorzEnum.Right;
            doc.PageLayout.PageHeader.Style.TextAlignVert = AlignVertEnum.Center;
            doc.PageLayout.PageHeader.Height = "3cm";
        }


        private static RenderText CreateBodyText(C1PrintDocument doc, string text)
        {
            RenderText celltext = new RenderText(doc);
            celltext = new RenderText(doc);
            celltext.Text = text + "\n";
            celltext.Style.TextColor = Color.FromArgb(46, 44, 83);
            celltext.Style.TextAlignHorz = AlignHorzEnum.Left;
            return celltext;
        }
        private static RenderText CreateTitleText(C1PrintDocument doc, string text)
        {
            RenderText celltext = CreateBodyText(doc, text);           
            celltext.Style.FontBold = true;
            return celltext;
        }
        private static RenderTable CreateTable(C1PrintDocument doc, int r, int c)
        {
            RenderTable table = new C1.C1Preview.RenderTable(doc);
            for (int row = 0; row < r; ++row)
            {
                for (int col = 0; col < c; ++col)
                {
                    RenderText celltext = new C1.C1Preview.RenderText(doc);
                    celltext.Text = string.Format("", row, col);

                    // Add empty cells.
                    table.Cells[row, col].RenderObject = celltext;
                }
            }
            return table;
        }

        private System.Drawing.Image BuildManagersMixChart(DataTable table)
        {
            RadChart radChart = new RadChart();
            radChart.ChartTitle.TextBlock.Text = "Current managers in the mix";
            radChart.ChartImageFormat = ImageFormat.Png;
            radChart.Skin = "LightBlue";
            radChart.AutoLayout = true;
            radChart.ItemDataBound += new EventHandler<ChartItemDataBoundEventArgs>(ManagersMix_ItemDataBound);

            radChart.Legend.Appearance.Visible = false;

            Telerik.Charting.ChartSeries chartSeries = new Telerik.Charting.ChartSeries();
            chartSeries.Name = this.lblName.Text;
            chartSeries.Type = Telerik.Charting.ChartSeriesType.Pie;
            chartSeries.DataLabelsColumn = InvestmentDS.NAME;
            chartSeries.DataYColumn = InvestmentDS.PERCENTAGE;
            chartSeries.Appearance.ShowLabels = true;
            radChart.Series.Add(chartSeries);

            radChart.DataSource = table;
            radChart.DataBind();

            return radChart.GetBitmap();
        }
        protected void ManagersMix_ItemDataBound(object sender, ChartItemDataBoundEventArgs e)
        {
            if (e.SeriesItem == null) return;

            DataRowView dtView = e.DataItem as DataRowView;
            if (dtView != null)
            {
                e.SeriesItem.Label.TextBlock.Text = string.Format("{1} - {0}%", dtView[InvestmentDS.PERCENTAGE], dtView[InvestmentDS.NAME]);
            }
        }

        private System.Drawing.Image BuildGrowthChart(DataView data, string secCode, string bencmarkCode)
        {
            RadChart radChart = new RadChart();
            radChart.ChartTitle.TextBlock.Text = "Growth of 10K";
            radChart.ChartImageFormat = ImageFormat.Png;
            radChart.Skin = "LightBrown";
            radChart.AutoLayout = true;

            radChart.Legend.Appearance.Position.AlignedPosition = Telerik.Charting.Styles.AlignedPositions.TopLeft;
            radChart.Legend.Appearance.Location = Telerik.Charting.Styles.LabelLocation.OutsidePlotArea;

            Telerik.Charting.ChartSeries chartSeries = AddGrowthChartSeries(radChart, data, secCode);
            chartSeries.Name = this.lblName.Text;


            if (!string.IsNullOrWhiteSpace(bencmarkCode))
            {
                Telerik.Charting.ChartSeries chartSeries2 = AddGrowthChartSeries(radChart, data, bencmarkCode);
                chartSeries2.Name = "Benchmark";
            }


            chartSeries.PlotArea.XAxis.Appearance.LabelAppearance.RotationAngle = 45;
            chartSeries.PlotArea.XAxis.Appearance.ValueFormat = Telerik.Charting.Styles.ChartValueFormat.General;
            chartSeries.PlotArea.XAxis.AutoScale = false;

            chartSeries.PlotArea.YAxis.IsZeroBased = false;
            return radChart.GetBitmap();
        }

        private Telerik.Charting.ChartSeries AddGrowthChartSeries(RadChart radChart, DataView data, string code)
        {
            Telerik.Charting.ChartSeries chartSeries = new Telerik.Charting.ChartSeries();
            radChart.AddChartSeries(chartSeries);
            bool addAxisLabels = false;
            if (radChart.Series.Count == 1)
                addAxisLabels = true;

            chartSeries.Name = this.lblName.Text;
            chartSeries.Type = Telerik.Charting.ChartSeriesType.Line;
            chartSeries.Appearance.ShowLabels = false;


            data.RowFilter = string.Format("{0} = '{1}'", PerfReportDS.INVESMENTCODE, code);
            for (int i = 0; i < data.Count; i++)
            {
                double value = double.Parse(data[i][PerfReportDS.OPENINGBAL].ToString());
                chartSeries.AddItem(value);

                if (addAxisLabels)
                {
                    DateTime dt = (DateTime)data[i][PerfReportDS.STARTDATE];
                    string label = dt.Month + " / " + dt.Year;
                    chartSeries.PlotArea.XAxis.AddItem(label);
                    //chartSeries.PlotArea.XAxis.AddItem(dt.ToShortDateString());
                }
            }
            return chartSeries;
        }

         private void SetPerformanceSummaryGrid(C1PrintDocument doc, DataTable performSummaryTable, string title)
        {
            int index = 1;

            if (performSummaryTable.Rows.Count > 0)
            {
                RenderTable table = new RenderTable();
                table.RowGroups[0, 1].PageHeader = true;
                table.Style.FontSize = 8;

                table.Rows[0].Height = .2;
                table.Rows[0].Style.FontBold = true;
                table.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                table.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                table.Rows[0].Style.FontBold = true;
                table.Rows[0].Style.FontSize = 7;
                table.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                table.Cells[0, 0].Text = "Description";
                table.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Left;
                table.Cells[0, 1].Text = "1 Month";
                table.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                table.Cells[0, 2].Text = "3 Month";
                table.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                table.Cells[0, 3].Text = "6 Month";
                table.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                table.Cells[0, 4].Text = "1 Year";
                table.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                table.Cells[0, 5].Text = "2 Years";
                table.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                table.Cells[0, 6].Text = "3 Years";
                table.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                table.Cells[0, 7].Text = "5 Years";
                table.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                table.Cols[0].Width = 1.5;
                table.Cols[1].Width = 1.2;
                table.Cols[2].Width = 1.2;
                table.Cols[3].Width = 1.2;
                table.Cols[4].Width = 1.2;
                table.Cols[5].Width = 1.2;
                table.Cols[6].Width = 1.2;
                table.Cols[7].Width = 1.2;

                foreach (DataRow row in performSummaryTable.Rows)
                {

                    table.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    table.Rows[index].Height = .2;

                    table.Cells[index, 0].Text = row[PerfReportDS.DESCRIPTION].ToString();
                    table.Cells[index, 1].Text = row[PerfReportDS.ONEMONTH].ToString();
                    table.Cells[index, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                    table.Cells[index, 2].Text = row[PerfReportDS.THREEMONTH].ToString();
                    table.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                    table.Cells[index, 3].Text = row[PerfReportDS.SIXMONTH].ToString();
                    table.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                    table.Cells[index, 4].Text = row[PerfReportDS.ONEYEAR].ToString();
                    table.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                    table.Cells[index, 5].Text = row[PerfReportDS.TWOYEAR].ToString();
                    table.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                    table.Cells[index, 6].Text = row[PerfReportDS.THREEYEAR].ToString();
                    table.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                    table.Cells[index, 7].Text = row[PerfReportDS.FIVEYEAR].ToString();
                    table.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    index++;
                }


                table.Rows[index].Height = .2;
                table.Rows[index].Style.FontBold = true;
                table.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                table.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                table.Rows[index].Style.FontBold = true;
                table.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = "\n" + title + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(table);
            }
        }

        private void GetReportData(Guid productId)
        {
            OrganizationCM cm = this.UMABroker.GetWellKnownCM(WellKnownCM.Organization) as OrganizationCM;
            InvestmentDS ds = new InvestmentDS();
            ds.InvestmentID = productId;
            cm.GetData(ds);
            this.PresentationData = ds;
            UMABroker.ReleaseBrokerManagedComponent(cm);

            ManagedInvestmentAccountDS fundDS = GetFundDataSet();
            this.PresentationData.Merge(fundDS);
        }
        private ManagedInvestmentAccountDS GetFundDataSet()
        {
            Guid StateStreetCid = new Guid("b354e928-a3c2-43d7-b61e-feb215cf1bd3");
            ManagedInvestmentAccountDS misDS = new ManagedInvestmentAccountDS();
            misDS.FundID = Guid.Empty;
            if (this.UMABroker != null)
            {
                IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(StateStreetCid);
                misDS.CommandType = DatasetCommandTypes.Get;
                clientData.GetData(misDS);
                UMABroker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            return misDS;

        }



        private DataRow FindRow(Guid cidGuid, DataTable table)
        {
            InvestmentDS ds = this.PresentationData as InvestmentDS;
            //DataTable table = PresentationData.Tables[InvestmentDS.INVESTMENTMANAGEMENT];
            //Guid cidGuid = new Guid(cid);
            DataRow r;

            if (table.Rows.Count == 0)
            {
                r = table.NewRow();
                table.Rows.Add(r);
            }
            else
            {
                var a = table.AsEnumerable().Where(row => row.Field<Guid>(InvestmentDS.ID) == cidGuid);
                if (a.AsDataView().Count > 0)
                {
                    r = a.AsDataView()[0].Row;
                }
                else
                {
                    r = table.NewRow();
                    table.Rows.Add(r);
                }
            }
            r[InvestmentDS.ID] = cidGuid;
            return r;
        }


        protected void btnBackToListing(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Administration/Products.aspx");
        }
        private void DisplayMessage(string text)
        {
            this.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
        }


    }
}
