﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="SecurityDetailsMaster.master"
    AutoEventWireup="true" CodeBehind="SecurityDetails.aspx.cs" Inherits="eclipseonlineweb.SecurityDetails" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Chart"
    TagPrefix="c1" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc" TagName="Details" Src="Controls/HistoricalPriceControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        //Resetting sub menu index
        localStorage['SubMenuIndex'] = '0';

        function hintContent() {
            return this.data.label + '<br/> ' + this.y + '';
        }
        function hintContentPie() {
            return this.data.toString() + " : " + Globalize.format(this.value / this.total, "p2");
        }

    </script>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                                ID="btnDownload" />
                            <a href="Securities.aspx">
                                <img src="../images/window_previous.png" border="0" style="border: 0 !important;" /></a>
                            <asp:ImageButton runat="server" ImageUrl="~/images/add-icon.png" OnClick="BtnAddNewPriceClicked"
                                ID="btaddNew" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblCode" Text=""></asp:Label>
                            <asp:Label Font-Bold="true" runat="server" ID="Label1" Text="-"></asp:Label>
                            <asp:Label Font-Bold="true" runat="server" ID="lblSecurityDesc" Text="Description"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <div class="WhiteBack">
                <div id="chart">
                    <c1:C1LineChart ID="C1WebChart1" runat="server" Height="200px" Width="100%" ShowChartLabels="false">
                        <SeriesTransition Duration="2000" />
                        <Hint>
                            <Content Function="hintContent" />
                        </Hint>
                        <Animation Duration="2000" />
                        <SeriesList>
                            <c1:LineChartSeries Label="SampleData" LegendEntry="True">
                                <Markers Visible="True">
                                </Markers>
                                <Data>
                                    <X DoubleValues="1, 2, 3, 4, 5" />
                                    <Y DoubleValues="20, 22, 19, 24, 25" />
                                </Data>
                            </c1:LineChartSeries>
                        </SeriesList>
                        <SeriesStyles>
                            <c1:ChartStyle Stroke="#4B6C9E" StrokeWidth="4" Opacity="0.8">
                            </c1:ChartStyle>
                        </SeriesStyles>
                        <SeriesHoverStyles>
                            <c1:ChartStyle Stroke="#4B6C9E" StrokeWidth="6" Opacity="0.9">
                            </c1:ChartStyle>
                        </SeriesHoverStyles>
                        <Footer Compass="South" Visible="False">
                        </Footer>
                        <Legend Visible="false"></Legend>
                        <Axis>
                            <Y Compass="West" Visible="False">
                                <Labels TextAlign="Center">
                                </Labels>
                                <GridMajor Visible="True">
                                </GridMajor>
                            </Y>
                        </Axis>
                        <DataBindings>
                            <c1:C1ChartBinding XField="Date" XFieldType="DateTime" YField="UnitPrice" YFieldType="Number" />
                        </DataBindings>
                    </c1:C1LineChart>
                </div>
                <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                    PageSize="10" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                    GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                    AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource"
                    OnItemCommand="PresentationGrid_OnItemCommand">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                        Name="Banks" TableLayout="Fixed">
                        <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ID" UniqueName="ID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="SecID" ReadOnly="true" HeaderText="SecID"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="SecID" UniqueName="SecID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="Date" ReadOnly="true" HeaderText="Date"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="Date" UniqueName="Date" DataFormatString="{0:dd/MM/yyyy}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="Currency" ReadOnly="true" HeaderText="Currency"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="Currency" UniqueName="Currency">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="UnitPrice" HeaderText="Unit Price" ReadOnly="true"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="UnitPrice" UniqueName="UnitPrice" DataFormatString="{0:N6}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" SortExpression="PriceNAV" ShowFilterIcon="true"
                                HeaderText="Price NAV" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                HeaderButtonType="TextButton" DataField="PriceNAV" UniqueName="PriceNAV" DataFormatString="{0:N6}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" SortExpression="PricePUR" HeaderText="Price PUR"
                                AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                                HeaderButtonType="TextButton" DataField="PricePUR" UniqueName="PricePUR" DataFormatString="{0:N6}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" SortExpression="InterestRate" HeaderText="Interest Rate"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="InterestRate" UniqueName="InterestRate"
                                DataFormatString="{0:N6}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="AdjustedPrice" ReadOnly="true" HeaderText="Adjusted Price"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="AdjustedPrice" UniqueName="AdjustedPrice"
                                DataFormatString="{0:N6}">
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn ConfirmText="Are you sure you want to remove price?" ButtonType="ImageButton"
                                CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                <HeaderStyle Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </div>
            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
            <asp:Button ID="btnCancel" runat="server" Style="display: none" />
            <asp:Button ID="btnOkay" runat="server" Style="display: none" />
            <asp:ModalPopupExtender ID="ModalPopupExtender1" CancelControlID="btnCancel" OkControlID="btnOkay"
                BackgroundCssClass="ModalPopupBG" runat="server" Drag="true" TargetControlID="btnShowPopup"
                PopupControlID="panEdit" PopupDragHandleControlID="PopupHeader">
            </asp:ModalPopupExtender>
            <asp:Panel runat="server" ID="panEdit">
                <uc:Details ID="PriceDetails" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
