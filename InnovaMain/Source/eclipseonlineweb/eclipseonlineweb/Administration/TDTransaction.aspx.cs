﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Telerik.Web.UI;
using System.Data;
using System.Web.UI.WebControls;

namespace eclipseonlineweb
{
    public partial class TDTransaction : UMABasePage
    {
        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            if (!btnBack.Visible)
            {
                ConfigureExport(radGrid1);
                radGrid1.MasterTableView.ExportToExcel();
            }
            else
            {
                ConfigureExport(radGrid);
                radGrid.MasterTableView.ExportToExcel();
            }
        }

        public void ConfigureExport(RadGrid r)
        {
            r.ExportSettings.ExportOnlyData = true;
            r.ExportSettings.IgnorePaging = true;
            r.ExportSettings.OpenInNewWindow = true;
        }
                
        protected void BtnBackToListing(object sender, ImageClickEventArgs e)
        {
            InstitutionGrid.Visible = true;
            ScrollPanel.Visible = false;
            btnBack.Visible = false;
            lblTDTransactionListing.Text = "TD Rates";
        }
        
        protected void GetInstituteData(object sender, GridNeedDataSourceEventArgs e)
        {
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            
            //ToDo:Need to create new DS fro Institues info
            radGrid1.DataSource = organization.Institution;

            UMABroker.ReleaseBrokerManagedComponent(organization);

        }

        protected void InstituteGridItemCommand(object sender, GridCommandEventArgs e)
        {
            var dataItem = (e.Item as GridDataItem);
            switch (e.CommandName.ToLower())
            {
                case "details":
                    if (dataItem != null)
                    {
                        string rowID = dataItem["ID"].Text;
                        if (lblInstituteID.Text != rowID)
                        {
                            ClearFilter(radGrid);
                            lblInstituteID.Text = rowID;
                        };
                        ScrollPanel.Visible = true;
                        InstitutionGrid.Visible = false;
                        lblTDTransactionListing.Text = dataItem["Name"].Text;
                        btnBack.Visible = true;

                        
                        radGrid.Rebind();
                    }
                    break;
            }
        }

        private void ClearFilter(RadGrid radGrid)
        {
            foreach (GridColumn column in radGrid.MasterTableView.OwnerGrid.Columns)
            {
                column.CurrentFilterValue = string.Empty;
            }
            //Sorting Retained to to unforeseen user handling requirements
            //radGrid.MasterTableView.SortExpressions.Clear();
            radGrid.MasterTableView.FilterExpression = string.Empty;
        }
        
        protected void GetData(object sender, GridNeedDataSourceEventArgs e)
        {
            btnDownload.Enabled = true;
            if (lblInstituteID.Text == null)
            {
                var tdPrice = new System.Collections.ObjectModel.ObservableCollection<TDInstitutePriceEntity>();
                radGrid.DataSource = tdPrice;
                ScrollPanel.Visible = true;
                return;
            }
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var institute = organization.Institution.FirstOrDefault(p => p.ID == new Guid(lblInstituteID.Text));
            
            if (institute == null)
                return;

            var tdPrices = institute.TDPrices;
            if (tdPrices != null)
            {
                radGrid.DataSource = 
                    from t in tdPrices
                    select new
                    {
                        InstituteName = organization.Institution.FirstOrDefault(p => p.ID == t.InstituteID) == null ? string.Empty : organization.Institution.FirstOrDefault(p => p.ID == t.InstituteID).Name,
                        tdPrice = t
                    };
            }
            else
            {
                tdPrices = new System.Collections.ObjectModel.ObservableCollection<TDInstitutePriceEntity>();
                radGrid.DataSource = tdPrices;
            }
            ScrollPanel.Visible = true;
        }

        protected void radGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            OrderPadRatesDS ds;
            GridDataItem Item = e.Item as GridDataItem;

            switch (e.CommandName.ToLower())
            {
                case "edit":
                    hdnPriceID.Value = Item.GetDataKeyValue("tdPrice.ID").ToString();
                    break;
                case "update":
                    ds = PopulateDataSetWithData(e, DatasetCommandTypes.Update);
                    e.Item.Edit = false;
                    e.Canceled = true;
                    SaveOrganizanition(ds);
                    radGrid.Rebind();
                    break;
                case "delete":
                    hdnPriceID.Value = Item.GetDataKeyValue("tdPrice.ID").ToString();
                    ds = PopulateDataSetWithData(e, DatasetCommandTypes.Delete);
                    e.Item.Edit = false;
                    e.Canceled = true;
                    SaveOrganizanition(ds);
                    radGrid.Rebind();
                    break;
            }
        }

        private OrderPadRatesDS PopulateDataSetWithData(GridCommandEventArgs e, DatasetCommandTypes command)
        {
            var ds = new OrderPadRatesDS();
            DataRow row = ds.Tables[OrderPadRatesDS.ORDERPADRATESTABLE].NewRow();
            ds.CommandType = command;

            GridDataItem Item = e.Item as GridDataItem;
            row[OrderPadRatesDS.ID] = hdnPriceID.Value;

            ds.InstituteId = new Guid(lblInstituteID.Text);

            if (command == DatasetCommandTypes.Update)
            {
                var editForm = (GridEditFormItem)e.Item;

                var onGoing = (TextBox)editForm["OnGoing"].Controls[0];
                row[OrderPadRatesDS.ONGOING] = string.IsNullOrEmpty(onGoing.Text) ? "0" : onGoing.Text;

                var min = (TextBox)editForm["Min"].Controls[0];
                row[OrderPadRatesDS.MIN] = string.IsNullOrEmpty(min.Text) ? "0" : min.Text;

                var max = (TextBox)editForm["Max"].Controls[0];
                row[OrderPadRatesDS.MAX] = string.IsNullOrEmpty(max.Text) ? "0" : max.Text;

                var maximumBrokeage = (TextBox)editForm["MaximumBrokeage"].Controls[0];
                row[OrderPadRatesDS.MAXIMUMBROKEAGE] = string.IsNullOrEmpty(maximumBrokeage.Text) ? "0" : maximumBrokeage.Text;

                var days30 = (TextBox)editForm["Days30"].Controls[0];
                row[OrderPadRatesDS.DAYS30] = string.IsNullOrEmpty(days30.Text) ? "0" : days30.Text;

                var days60 = (TextBox)editForm["Days60"].Controls[0];
                row[OrderPadRatesDS.DAYS60] = string.IsNullOrEmpty(days60.Text) ? "0" : days60.Text;

                var days90 = (TextBox)editForm["Days90"].Controls[0];
                row[OrderPadRatesDS.DAYS90] = string.IsNullOrEmpty(days90.Text) ? "0" : days90.Text;

                var days120 = (TextBox)editForm["Days120"].Controls[0];
                row[OrderPadRatesDS.DAYS120] = string.IsNullOrEmpty(days120.Text) ? "0" : days120.Text;

                var days150 = (TextBox)editForm["Days150"].Controls[0];
                row[OrderPadRatesDS.DAYS150] = string.IsNullOrEmpty(days150.Text) ? "0" : days150.Text;

                var days180 = (TextBox)editForm["Days180"].Controls[0];
                row[OrderPadRatesDS.DAYS180] = string.IsNullOrEmpty(days180.Text) ? "0" : days180.Text;

                var days270 = (TextBox)editForm["Days270"].Controls[0];
                row[OrderPadRatesDS.DAYS270] = string.IsNullOrEmpty(days270.Text) ? "0" : days270.Text;

                var years1 = (TextBox)editForm["Years1"].Controls[0];
                row[OrderPadRatesDS.YEARS1] = string.IsNullOrEmpty(years1.Text) ? "0" : years1.Text;

                var years2 = (TextBox)editForm["Years2"].Controls[0];
                row[OrderPadRatesDS.YEARS2] = string.IsNullOrEmpty(years2.Text) ? "0" : years2.Text;

                var years3 = (TextBox)editForm["Years3"].Controls[0];
                row[OrderPadRatesDS.YEARS3] = string.IsNullOrEmpty(years3.Text) ? "0" : years3.Text;

                var years4 = (TextBox)editForm["Years4"].Controls[0];
                row[OrderPadRatesDS.YEARS4] = string.IsNullOrEmpty(years4.Text) ? "0" : years4.Text;

                var years5 = (TextBox)editForm["Years5"].Controls[0];
                row[OrderPadRatesDS.YEARS5] = string.IsNullOrEmpty(years5.Text) ? "0" : years5.Text;

            }
            ds.Tables[OrderPadRatesDS.ORDERPADRATESTABLE].Rows.Add(row);
            return ds;
        }
    }
}