﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="SetupMaster.master"
    AutoEventWireup="true" CodeBehind="InvestmentManagement.aspx.cs" Inherits="eclipseonlineweb.InvestmentManagement" %>

<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" Assembly="C1.Web.Wijmo.Controls.4, Version=4.0.20121.56, Culture=neutral, PublicKeyToken=9b75583953471eea" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer"
    TagPrefix="C1ReportViewer" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton  runat="server" ImageUrl="~/images/window_previous.png" AlternateText="Back to Listing"
                                ToolTip="Back to Listing" OnClick="btnBackToListing" ID="btnBack" />
                        </td>
                        <td style="width: 5%">
                            <asp:ImageButton ID="btnSave" AlternateText="Update Account Summary" ToolTip="Save Changes"
                                OnClick="btnSave_Click" runat="server" ImageUrl="~/images/Save-Icon.png" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:breadcrumb id="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblName" Text="Investrment management"></asp:Label>
                            <asp:Label Font-Bold="true" Visible="false" runat="server" ID="lblID"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <div id="MainView">
                <asp:Panel runat="server" ID="pnlInvestmentManagement" Visible="true">
                    <fieldset>
                        <legend>Investment Management</legend>
                        <table width="100%">
                              <tr>
                                <td style="font-weight: bold">
                                    Objective:
                                </td>
                                <td align="left">
                                    <asp:TextBox runat="server" MaxLength="2000" ID="txtOBJECTIVE" 
                                        TextMode="MultiLine" Width="410px" Rows="5" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <telerik:RadTextBox ID="txtInvestmentManager" Label="Investment Manager:" runat="server"  EnableViewState="true" LabelWidth="160px" Width="300px">
                                    </telerik:RadTextBox>
                                </td>
                                <td align="left">                                    
                                    <telerik:RadComboBox ID="cmbBenchmark" Label="Benchmark:" runat="server"  EnableViewState="true" LabelWidth="160px" Width="340px">
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <telerik:RadTextBox ID="txtPortfoloManager" Label="Portfolo Manager:" runat="server"  EnableViewState="true" LabelWidth="160px" Width="300px">
                                    </telerik:RadTextBox>
                                </td>
                                <td align="left">
                                    <telerik:RadTextBox ID="txtInvestTime" Label="Recommended invest. time:" runat="server"  EnableViewState="true" LabelWidth="160px" Width="233px">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <telerik:RadTextBox ID="txtInfo" Label="Fund Type:" runat="server"  EnableViewState="true" LabelWidth="160px" Width="300px">
                                    </telerik:RadTextBox>
                                </td>
                                <td align="left">
                                    <telerik:RadDateInput ID="RadInceptionDate" runat="server" Width="120px"  LabelWidth="160px" 
                                        InvalidStyleDuration="100" Label="Inception Date:" DateFormat="d" ></telerik:RadDateInput><br/>
                                </td>
                            </tr>  
                            <tr>
                                <td align="left">
                                    <telerik:RadTextBox ID="txtAssetType" Label="Asset Type:" runat="server"  EnableViewState="true" LabelWidth="160px" Width="300px">
                                    </telerik:RadTextBox>
                                </td>
                                <td align="left">
                                     <telerik:RadNumericTextBox ShowSpinButtons="false" EnableViewState="true" IncrementSettings-InterceptArrowKeys="true"
                                        IncrementSettings-InterceptMouseWheel="true" Label="Entry Fee:"
                                        LabelWidth="160px" runat="server" ID="txtENTRYFEE" Width="120px" Type="Percent" MaxValue="100" MinValue = "0">
                                    </telerik:RadNumericTextBox>
                                </td>
                            </tr>                                          
                            <tr>
                             <td align="left">
                                <telerik:RadTextBox ID="txtREGION" Label="Region:" runat="server"  EnableViewState="true" LabelWidth="160px" Width="300px">
                                </telerik:RadTextBox>
                            </td> 
                                <td align="left">
                                     <telerik:RadNumericTextBox ShowSpinButtons="false" EnableViewState="true" IncrementSettings-InterceptArrowKeys="true"
                                        IncrementSettings-InterceptMouseWheel="true" Label="Exit Fee:"
                                        LabelWidth="160px" runat="server" ID="txtEXITFEE" Width="120px" Type="Percent" MaxValue="100" MinValue = "0" >
                                    </telerik:RadNumericTextBox>
                                </td>
                            </tr>

                            <tr>
                             <td align="left">
                                <telerik:RadTextBox ID="txtFINDSIZE" Label="Find Size:" runat="server"  EnableViewState="true" LabelWidth="160px" Width="300px">
                                </telerik:RadTextBox>
                            </td> 
                                <td align="left">
                                 <telerik:RadNumericTextBox ShowSpinButtons="false" EnableViewState="true" IncrementSettings-InterceptArrowKeys="true"
                                    IncrementSettings-InterceptMouseWheel="true" Label="Buy/Sell Spread:"
                                    LabelWidth="160px" runat="server" ID="txtBUYSELLSPREAD" Width="120px" Type="Percent" MaxValue="100" MinValue = "0">
                                </telerik:RadNumericTextBox>
                                </td>
                            </tr>
                            <tr>
                             <td align="left">
                                <telerik:RadNumericTextBox ID="txtMINADDITION" Label="Minimum Addition:" runat="server"  EnableViewState="true" LabelWidth="160px" Width="300px" Type="Currency">
                                </telerik:RadNumericTextBox>
                            </td>                                
                            <td align="left">
                                     <telerik:RadNumericTextBox ShowSpinButtons="false" EnableViewState="true" Label="ICR:"
                                        LabelWidth="160px" runat="server" ID="txtICR" Width="120px" Type="Percent" MaxValue="100" MinValue = "0">
                                    </telerik:RadNumericTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                            <telerik:RadNumericTextBox ID="txtMINWITHRAWAL" Label="Minimum Withrawal:" runat="server"  EnableViewState="true" LabelWidth="160px" Width="300px" Type="Currency">
                            </telerik:RadNumericTextBox>
                            </td>
                                <td align="left">
                                     <telerik:RadNumericTextBox ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                                        IncrementSettings-InterceptMouseWheel="true" Label="Management Fee:"
                                        LabelWidth="160px" runat="server" ID="txtMANAGEMENTFEE" Width="120px" Type="Percent" MaxValue="100" MinValue = "0">
                                    </telerik:RadNumericTextBox>
                                </td>

                            </tr> 
                            <tr>
                                <td align="left">
                                     <telerik:RadNumericTextBox ShowSpinButtons="false" EnableViewState="true" Label="Minimum Investment:"
                                        LabelWidth="160px" runat="server" ID="txtMinInvest" Width="300px" Type="Currency" MaxValue="100" MinValue = "0">
                                    </telerik:RadNumericTextBox>
                                </td>
                                <td align="left">
                                    <telerik:RadDateInput ID="RadManagementOf" runat="server" Width="120px"  LabelWidth="160px" 
                                        InvalidStyleDuration="100" Label="Fee As Of:" DateFormat="d" >
                                    </telerik:RadDateInput>

                                </td>
                            </tr>                                                                                                        
                        </table>
                        <table>
                        <tr>
                           <td style="font-weight: bold">
                                 Current managers in the mix
                           </td>
                        </tr>
                             <tr>
                                <td align="left">
                                    <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
                                    runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                                    AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" 
                                    OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                                    AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                                    OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
                                    OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
                                    EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
                                    OnItemDataBound="PresentationGrid_ItemDataBound"  ViewStateMode="Enabled">
                                    <PagerStyle Mode="NumericPages"></PagerStyle>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                                    <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                                    Name="Managers" TableLayout="Fixed" EditMode="PopUp" ViewStateMode="Enabled" ShowFooter="true">
                                    <CommandItemSettings AddNewRecordText="Add New Manager" ShowExportToExcelButton="false" 
                                        ShowExportToWordButton="false" ShowExportToPdfButton="false"></CommandItemSettings>
                                    <Columns>
                                        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                                            <HeaderStyle Width="5%"></HeaderStyle>
                                            <ItemStyle CssClass="MyImageButton"></ItemStyle>
                                        </telerik:GridEditCommandColumn>
                                        <telerik:GridDropDownColumn UniqueName="cmbProductSecuritiesId" HeaderText="Product Security"
                                            DataField="ID" Visible="false" ColumnEditorID="CEProductSecurities" />
                                        <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="35%" ItemStyle-Width="35%"
                                            SortExpression="Name" HeaderText="Product Security" AutoPostBackOnFilter="false"
                                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                            DataField="Name" UniqueName="Name" ColumnEditorID="CEProductSecurities">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="35%" ItemStyle-Width="35%"
                                            SortExpression="PercentageAllocation" HeaderText="Allocation %" AutoPostBackOnFilter="false"
                                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                            DataField="PercentageAllocation" UniqueName="PercentageAllocation"  DataFormatString="{0:N2}" 
                                            Aggregate="Sum" FooterAggregateFormatString="{0:N2}">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridButtonColumn ConfirmText="Are you sure you want to delete this record?"
                                            ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                                            <HeaderStyle Width="5%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                                        </telerik:GridButtonColumn>
                                    </Columns>
                                </MasterTableView>
                                </telerik:RadGrid>
                                <telerik:GridDropDownListColumnEditor ID="CEProductSecurities" runat="server" DropDownStyle-Width="200px" />
                                </td>
                            </tr>                                                  
                        </table>
                     </fieldset>
                </asp:Panel>
             </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
