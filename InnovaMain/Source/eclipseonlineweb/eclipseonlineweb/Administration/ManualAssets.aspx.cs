﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using Telerik.Web.UI.HtmlChart;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class ManualAssets : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            return objUser.UserType == UserType.Innova || objUser.Name == "Administrator";
        }

        protected override void GetBMCData()
        {
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var manualAssetsDS = new ManualAssetsDS();
            organization.GetData(manualAssetsDS);
            PresentationData = manualAssetsDS;
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            if (pnlMainGrid.Visible)
            {
                var excelDataset = new DataSet();
                excelDataset.Merge(PresentationData.Tables[ManualAssetsDS.MANIST], true, MissingSchemaAction.Add);
                excelDataset.Tables[0].Columns.Remove("ID");
                ExcelHelper.ToExcel(excelDataset, "Manual Assets-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", Page.Response);
            }
            else
            {
                var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                var manualAssetsDS = new ManualAssetsDS { IsDetails = true, ItemID = new Guid(lblManAssetsID.Text) };

                if (organization != null)
                {
                    organization.GetData(manualAssetsDS);
                    UMABroker.ReleaseBrokerManagedComponent(organization);
                }

                var excelDataset = new DataSet();
                excelDataset.Merge(manualAssetsDS.Tables[ManualAssetsDS.MANPRICEHISTORY], true, MissingSchemaAction.Add);
                excelDataset.Tables[0].Columns.Remove("ManualHistoryID");
                ExcelHelper.ToExcel(excelDataset, lblManAssets.Text + "-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", Page.Response);
            }
        }

        protected void BtnBackToListing(object sender, ImageClickEventArgs e)
        {
            pnlMainGrid.Visible = true;
            pnlDetails.Visible = false;
            btnBack.Visible = false;
            lblManAssets.Text = "Manual Assets";
        }

        public override void PopulatePage(DataSet ds)
        {
            var sm = ScriptManager.GetCurrent(Page);

            if (sm != null)
                sm.RegisterPostBackControl(btnDownload);
        }

        protected void BackToSummary(object sender, EventArgs args)
        {
            pnlMainGrid.Visible = true;
            pnlDetails.Visible = false;
        }

        #region Telerik

        #region ManualAssetGrid

        protected void ManualAssetGridItemCommand(object sender, GridCommandEventArgs e)
        {
            var dataItem = (e.Item as GridDataItem);
            switch (e.CommandName.ToLower())
            {
                case "details":
                    if (dataItem != null)
                    {
                        string rowID = dataItem["ID"].Text;

                        lblManAssets.Text = dataItem["txtCode"].Text + " - " + dataItem["txtDescription"].Text;
                        lblManAssetsID.Text = rowID;

                        pnlMainGrid.Visible = false;
                        pnlDetails.Visible = true;
                        btnBack.Visible = true;
                        PriceHistoryGrid.Rebind();
                    }
                    break;
                case "delete":
                    if (dataItem != null)
                    {
                        var id = new Guid(dataItem["ID"].Text);
                        var manualAssetDs = new ManualAssetsDS { ItemID = id, CommandType = DatasetCommandTypes.Delete };
                        SaveOrganizanition(manualAssetDs);
                        ManualAssetGrid.Rebind();
                    }
                    break;
                case "update":
                    var editableItem = (GridEditableItem)e.Item;
                    if (editableItem != null)
                    {
                        var id = new Guid(editableItem["ID"].Text);
                        var manualAssetsDS = new ManualAssetsDS();
                        var dt = manualAssetsDS.Tables[ManualAssetsDS.MANIST];
                        var dr = dt.NewRow();
                        manualAssetsDS.ItemID = id;
                        var cmbInvestmentType = (RadComboBox)editableItem["cmbInvestmentType"].Controls[0];
                        var cmbRating = (RadComboBox)editableItem["cmbRating"].Controls[0];
                        var cmbStatus = (RadComboBox)editableItem["cmbStatus"].Controls[0];

                        var txtCode = (TextBox)editableItem["txtCode"].Controls[0];
                        var txtMarket = (TextBox)editableItem["txtMarket"].Controls[0];
                        var txtDescription = (TextBox)editableItem["txtDescription"].Controls[0];
                        //var txtUnitPriceBaseCurrency = (RadNumericTextBox)editableItem["txtUnitPriceBaseCurrency"].Controls[0];
                        var txtUnitSize = (RadNumericTextBox)editableItem["txtUnitSize"].Controls[0];
                        var txtMinSecurityBal = (RadNumericTextBox)editableItem["txtMinSecurityBal"].Controls[0];
                        var txtMinTradedLotSize = (RadNumericTextBox)editableItem["txtMinTradedLotSize"].Controls[0];
                        var txtPriceAsAtDate = (RadNumericTextBox)editableItem["txtPriceAsAtDate"].Controls[0];

                        var chkIsUnitised = (CheckBox)editableItem["chkIsUnitised"].Controls[0];
                        var chkIgnoreMinSecurityBal = (CheckBox)editableItem["chkIgnoreMinSecurityBal"].Controls[0];
                        var chkIgnoreMinTradedLotSize = (CheckBox)editableItem["chkIgnoreMinTradedLotSize"].Controls[0];
                        var chkIsPrefferedInvestment = (CheckBox)editableItem["chkIsPrefferedInvestment"].Controls[0];

                        var calIssueDate = (RadDatePicker)editableItem["calIssueDate"].Controls[0];

                        if (!Validate(txtCode))
                        {
                            txtCode.Focus();
                            e.Canceled = true;
                            return;
                        }
                        if (!Validate(txtMarket))
                        {
                            txtMarket.Focus();
                            e.Canceled = true;
                            return;
                        }
                        if (!Validate(txtDescription))
                        {
                            txtDescription.Focus();
                            e.Canceled = true;
                            return;
                        }

                        double minSecurityBal = 0;
                        double.TryParse(txtMinSecurityBal.Text, out minSecurityBal);
                        double minTradedLotSize = 0;
                        double.TryParse(txtMinTradedLotSize.Text, out minTradedLotSize);
                        double priceAsAtDate = 0;
                        double.TryParse(txtPriceAsAtDate.Text, out priceAsAtDate);
                        double unitSize = 0;
                        double.TryParse(txtUnitSize.Text, out unitSize);

                        dr[ManualAssetsDS.ID] = manualAssetsDS.ItemID;
                        dr[ManualAssetsDS.INVESTMENTTYPE] = cmbInvestmentType.SelectedValue;
                        dr[ManualAssetsDS.CODE] = txtCode.Text;
                        dr[ManualAssetsDS.RATING] = cmbRating.SelectedValue;
                        dr[ManualAssetsDS.STATUS] = cmbStatus.SelectedValue;
                        dr[ManualAssetsDS.MARKET] = txtMarket.Text;
                        dr[ManualAssetsDS.DESCRIPTION] = txtDescription.Text;
                        dr[ManualAssetsDS.ISUNITISED] = chkIsUnitised.Checked;
                        //dr[ManualAssetsDS.UNITPRICEBASECURRENCY] = txtUnitPriceBaseCurrency.Text;
                        dr[ManualAssetsDS.UNITSIZE] = unitSize;
                        dr[ManualAssetsDS.MINSECURITYBAL] = minSecurityBal;
                        dr[ManualAssetsDS.IGNOREMINSECURITYBAL] = chkIgnoreMinSecurityBal.Checked;
                        dr[ManualAssetsDS.MINTRADEDLOTSIZE] = minTradedLotSize;
                        dr[ManualAssetsDS.IGNOREMINTRADEDLOTSIZE] = chkIgnoreMinTradedLotSize.Checked;
                        if (calIssueDate.SelectedDate != null)
                            dr[ManualAssetsDS.ISSUEDATE] = calIssueDate.SelectedDate.Value;
                        dr[ManualAssetsDS.PRICEASATDATE] = priceAsAtDate;
                        dr[ManualAssetsDS.ISPREFFEREDINVESTMENT] = chkIsPrefferedInvestment.Checked;

                        dt.Rows.Add(dr);

                        manualAssetsDS.Tables.Remove(ManualAssetsDS.MANIST);
                        manualAssetsDS.Tables.Add(dt);
                        manualAssetsDS.CommandType = DatasetCommandTypes.Update;

                        SaveOrganizanition(manualAssetsDS);

                        e.Canceled = true;
                        e.Item.Edit = false;

                        ManualAssetGrid.Rebind();
                    }
                    break;
            }
        }

        protected void ManualAssetGridInsertCommand(object source, GridCommandEventArgs e)
        {
            if (!"ManualAsset".Equals(e.Item.OwnerTableView.Name)) return;
            var manualAssetsDS = new ManualAssetsDS();
            var dt = manualAssetsDS.Tables[ManualAssetsDS.MANIST];
            var dr = dt.NewRow();
            var editableItem = (GridEditableItem)e.Item;
            manualAssetsDS.ItemID = Guid.NewGuid();
            var cmbInvestmentType = (RadComboBox)editableItem["cmbInvestmentType"].Controls[0];
            var cmbRating = (RadComboBox)editableItem["cmbRating"].Controls[0];
            var cmbStatus = (RadComboBox)editableItem["cmbStatus"].Controls[0];

            var txtCode = (TextBox)editableItem["txtCode"].Controls[0];
            var txtMarket = (TextBox)editableItem["txtMarket"].Controls[0];
            var txtDescription = (TextBox)editableItem["txtDescription"].Controls[0];
            //var txtUnitPriceBaseCurrency = (RadNumericTextBox)editableItem["txtUnitPriceBaseCurrency"].Controls[0];
            var txtUnitSize = (RadNumericTextBox)editableItem["txtUnitSize"].Controls[0];
            var txtMinSecurityBal = (RadNumericTextBox)editableItem["txtMinSecurityBal"].Controls[0];
            var txtMinTradedLotSize = (RadNumericTextBox)editableItem["txtMinTradedLotSize"].Controls[0];
            var txtPriceAsAtDate = (RadNumericTextBox)editableItem["txtPriceAsAtDate"].Controls[0];

            var chkIsUnitised = (CheckBox)editableItem["chkIsUnitised"].Controls[0];
            var chkIgnoreMinSecurityBal = (CheckBox)editableItem["chkIgnoreMinSecurityBal"].Controls[0];
            var chkIgnoreMinTradedLotSize = (CheckBox)editableItem["chkIgnoreMinTradedLotSize"].Controls[0];
            var chkIsPrefferedInvestment = (CheckBox)editableItem["chkIsPrefferedInvestment"].Controls[0];

            var calIssueDate = (RadDatePicker)editableItem["calIssueDate"].Controls[0];

            if (!Validate(txtCode))
            {
                txtCode.Focus();
                e.Canceled = true;
                return;
            }
            if (!Validate(txtMarket))
            {
                txtMarket.Focus();
                e.Canceled = true;
                return;
            }
            if (!Validate(txtDescription))
            {
                txtDescription.Focus();
                e.Canceled = true;
                return;
            }


            double minSecurityBal = 0;
            double.TryParse(txtMinSecurityBal.Text, out minSecurityBal);
            double minTradedLotSize = 0;
            double.TryParse(txtMinTradedLotSize.Text, out minTradedLotSize);
            double priceAsAtDate = 0;
            double.TryParse(txtPriceAsAtDate.Text, out priceAsAtDate);
            double unitSize = 0;
            double.TryParse(txtUnitSize.Text, out unitSize);

            dr[ManualAssetsDS.ID] = manualAssetsDS.ItemID;
            dr[ManualAssetsDS.INVESTMENTTYPE] = cmbInvestmentType.SelectedValue;
            dr[ManualAssetsDS.CODE] = txtCode.Text;
            dr[ManualAssetsDS.RATING] = cmbRating.SelectedValue;
            dr[ManualAssetsDS.STATUS] = cmbStatus.SelectedValue;
            dr[ManualAssetsDS.MARKET] = txtMarket.Text;
            dr[ManualAssetsDS.DESCRIPTION] = txtDescription.Text;
            dr[ManualAssetsDS.ISUNITISED] = chkIsUnitised.Checked;
            //dr[ManualAssetsDS.UNITPRICEBASECURRENCY] = txtUnitPriceBaseCurrency.Text;
            dr[ManualAssetsDS.UNITSIZE] = unitSize;
            dr[ManualAssetsDS.MINSECURITYBAL] = minSecurityBal;
            dr[ManualAssetsDS.IGNOREMINSECURITYBAL] = chkIgnoreMinSecurityBal.Checked;
            dr[ManualAssetsDS.MINTRADEDLOTSIZE] = minTradedLotSize;
            dr[ManualAssetsDS.IGNOREMINTRADEDLOTSIZE] = chkIgnoreMinTradedLotSize.Checked;
            if (calIssueDate.SelectedDate != null)
            {
                dr[ManualAssetsDS.ISSUEDATE] = calIssueDate.SelectedDate.Value;
            }
            dr[ManualAssetsDS.PRICEASATDATE] = priceAsAtDate;
            dr[ManualAssetsDS.ISPREFFEREDINVESTMENT] = chkIsPrefferedInvestment.Checked;

            dt.Rows.Add(dr);

            manualAssetsDS.Tables.Remove(ManualAssetsDS.MANIST);
            manualAssetsDS.Tables.Add(dt);
            manualAssetsDS.CommandType = DatasetCommandTypes.Add;

            SaveOrganizanition(manualAssetsDS);

            e.Canceled = true;
            e.Item.Edit = false;

            ManualAssetGrid.Rebind();
        }

        protected void ManualAssetGridItemCreated(object sender, GridItemEventArgs e)
        {
        }

        protected void ManualAssetGridItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridEditableItem) || !e.Item.IsInEditMode) return;

            var editedItem = e.Item as GridEditableItem;
            var editMan = editedItem.EditManager;

            //binding Combos
            var cmbInvestmentType = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("cmbInvestmentType"));
            cmbInvestmentType.DataSource = InvestmentTypes;
            cmbInvestmentType.DataBind();

            var cmbRrating = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("cmbRating"));
            cmbRrating.DataSource = _ratingList;
            cmbRrating.DataTextField = "Key";
            cmbRrating.DataValueField = "Value";
            cmbRrating.DataBind();

            var cmbStatus = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("cmbStatus"));
            cmbStatus.DataSource = StatusList;
            cmbStatus.DataBind();

            //setting combo value
            if ((e.Item is IGridInsertItem)) return;
            var dataItem = (e.Item.DataItem as DataRowView);
            if (dataItem == null) return;
            cmbInvestmentType.SelectedValue = dataItem["InvestmentType"].ToString();
            cmbRrating.SelectedValue = dataItem["Rating"].ToString();
            cmbStatus.SelectedValue = dataItem["Status"].ToString();
            editedItem["ID"].Text = dataItem["ID"].ToString();
        }

        protected void ManualAssetGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var manualAssetsDS = new ManualAssetsDS();
            if (organization != null)
            {
                organization.GetData(manualAssetsDS);
                UMABroker.ReleaseBrokerManagedComponent(organization);
            }
            PresentationData = manualAssetsDS;
            var manualAssetsListView = new DataView(manualAssetsDS.Tables[ManualAssetsDS.MANIST]) { Sort = ManualAssetsDS.CODE + " ASC" };
            ManualAssetGrid.DataSource = manualAssetsListView.ToTable();

        }

        #endregion

        #region PriceHistoryGrid

        protected void PriceHistoryGridItemCommand(object sender, GridCommandEventArgs e)
        {
            var dataItem = (e.Item as GridDataItem);
            var gridEditForm = (e.Item as GridEditFormItem);

            switch (e.CommandName.ToLower())
            {
                case "delete":
                    if (dataItem != null)
                    {
                        var id = new Guid(dataItem["ID"].Text);
                        ManualAssetsDS manualAssetDs = new ManualAssetsDS { IsDetails = true, ItemID = new Guid(lblManAssetsID.Text), DetailsGUID = id, CommandType = DatasetCommandTypes.Delete };
                        SaveOrganizanition(manualAssetDs);
                        PriceHistoryGrid.Rebind();
                    }
                    break;
                case "update":
                    if (gridEditForm != null)
                    {
                        var id = new Guid(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ManualHistoryID"].ToString());
                        ManualAssetsDS manualAssetsDS = new ManualAssetsDS { IsDetails = true, ItemID = new Guid(lblManAssetsID.Text), DetailsGUID = id, CommandType = DatasetCommandTypes.Update };
                        var dt = manualAssetsDS.Tables[ManualAssetsDS.MANPRICEHISTORY];
                        var dr = dt.NewRow();
                        var editableItem = (GridEditableItem)e.Item;

                        var calDate = (RadDatePicker)editableItem["calDate"].Controls[0];

                        var txtCurrency = (TextBox)editableItem["txtCurrency"].Controls[0];
                        var txtUnitPrice = (RadNumericTextBox)editableItem["txtUnitPrice"].Controls[0];
                        var txtNavPrice = (RadNumericTextBox)editableItem["txtNavPrice"].Controls[0];
                        var txtPurPrice = (RadNumericTextBox)editableItem["txtPurPrice"].Controls[0];

                        dr[ManualAssetsDS.MANHISID] = manualAssetsDS.DetailsGUID;
                        if (calDate.SelectedDate != null)
                        {
                            dr[ManualAssetsDS.DATE] = calDate.SelectedDate.Value;
                        }
                        else
                        {
                            calDate.Focus();
                            DisplayMessage(PriceHistoryGrid, "<font color=\"red\">Please Select a Date*</font>");
                            e.Canceled = true;
                            return;
                        }
                        dr[ManualAssetsDS.CURRENCY] = txtCurrency.Text;
                        if (!Validate(txtUnitPrice))
                        {
                            txtUnitPrice.Focus();
                            e.Canceled = true;
                            return;
                        }


                        double unitPrice = 0;
                        double navPrice = 0;
                        double purPrice = 0;

                        double.TryParse(txtUnitPrice.Text, out unitPrice);
                        double.TryParse(txtNavPrice.Text, out navPrice);
                        double.TryParse(txtPurPrice.Text, out purPrice);

                        dr[ManualAssetsDS.UNITPRICE] = unitPrice;
                        dr[ManualAssetsDS.NAVPRICE] = navPrice;
                        dr[ManualAssetsDS.PURPRICE] = purPrice;

                        dt.Rows.Add(dr);

                        manualAssetsDS.Tables.Remove(ManualAssetsDS.MANPRICEHISTORY);
                        manualAssetsDS.Tables.Add(dt);
                        SaveOrganizanition(manualAssetsDS);
                        e.Canceled = true;
                        e.Item.Edit = false;
                        PriceHistoryGrid.Rebind();
                    }
                    break;
            }
        }

        protected void PriceHistoryGridInsertCommand(object source, GridCommandEventArgs e)
        {
            if (!"PriceHistory".Equals(e.Item.OwnerTableView.Name)) return;
            var manualAssetsDS = new ManualAssetsDS
                                     {
                                         IsDetails = true,
                                         ItemID = new Guid(lblManAssetsID.Text),
                                         DetailsGUID = Guid.NewGuid()
                                     };
            var dt = manualAssetsDS.Tables[ManualAssetsDS.MANPRICEHISTORY];
            var dr = dt.NewRow();
            var editableItem = (GridEditableItem)e.Item;

            var calDate = (RadDatePicker)editableItem["calDate"].Controls[0];

            var txtCurrency = (TextBox)editableItem["txtCurrency"].Controls[0];
            var txtUnitPrice = (RadNumericTextBox)editableItem["txtUnitPrice"].Controls[0];
            var txtNavPrice = (RadNumericTextBox)editableItem["txtNavPrice"].Controls[0];
            var txtPurPrice = (RadNumericTextBox)editableItem["txtPurPrice"].Controls[0];

            dr[ManualAssetsDS.MANHISID] = manualAssetsDS.DetailsGUID;
            if (calDate.SelectedDate != null)
            {
                dr[ManualAssetsDS.DATE] = calDate.SelectedDate.Value;
            }
            else
            {
                calDate.Focus();
                DisplayMessage(PriceHistoryGrid, "<font color=\"red\">Please Select a Date*</font>");
                e.Canceled = true;
                return;
            }
            dr[ManualAssetsDS.CURRENCY] = txtCurrency.Text;
            if (!Validate(txtUnitPrice))
            {
                txtUnitPrice.Focus();
                e.Canceled = true;
                return;
            }


            double unitPrice = 0;
            double navPrice = 0;
            double purPrice = 0;

            double.TryParse(txtUnitPrice.Text, out unitPrice);
            double.TryParse(txtNavPrice.Text, out navPrice);
            double.TryParse(txtPurPrice.Text, out purPrice);

            dr[ManualAssetsDS.UNITPRICE] = unitPrice;
            dr[ManualAssetsDS.NAVPRICE] = navPrice;
            dr[ManualAssetsDS.PURPRICE] = purPrice;

            dt.Rows.Add(dr);

            manualAssetsDS.Tables.Remove(ManualAssetsDS.MANPRICEHISTORY);
            manualAssetsDS.Tables.Add(dt);
            manualAssetsDS.CommandType = DatasetCommandTypes.Add;

            SaveOrganizanition(manualAssetsDS);

            e.Canceled = true;
            e.Item.Edit = false;

            PriceHistoryGrid.Rebind();
        }

        protected void PriceHistoryGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetPriceHistory(lblManAssetsID.Text);
        }


        #endregion

        #endregion

        #region Fill Lists
        public string[] InvestmentTypes = {
                                              "Artwork",
                                              "Investment Property" ,
                                              "Unit Trust",
                                              "Residential Property",
                                              "Collectables ",
                                              "Other"
                                          };

        public string[] StatusList = { "Active", "Closed", "Frozen", "No Buy", "No Sell" };

        readonly Dictionary<string, string> _ratingList = new Dictionary<string, string>{
                                              {"Buy","buy"} ,
                                              {"Hold","hold"},
                                              {"Sell","sell"},
                                              {"Strong Buy","strongbuy"} };
        #endregion

        private void GetPriceHistory(string id)
        {
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var manualAssetsDS = new ManualAssetsDS { IsDetails = true, ItemID = new Guid(id) };

            if (organization != null)
            {
                organization.GetData(manualAssetsDS);
                UMABroker.ReleaseBrokerManagedComponent(organization);
            }

            var view = new DataView(manualAssetsDS.Tables[ManualAssetsDS.MANPRICEHISTORY]) { Sort = ManualAssetsDS.DATE + " DESC" };

            PriceHistoryGrid.DataSource = view.ToTable();
            BindChart(view);
        }

        private void BindChart(DataView view)
        {
            //binding Rad Chart after sortign via date asc
            view.Sort = string.Format("{0} Asc", ManualAssetsDS.DATE);
            radChart1.DataSource = view.ToTable();
            var series = new LineSeries
                {
                    DataFieldX = "Date",
                    DataFieldY = "UnitPrice",
                };
            radChart1.PlotArea.Series.Add(series);
            radChart1.DataBind();

            radChart1.PlotArea.XAxis.Items.Clear();

            for (int i = 0; i < view.ToTable().Rows.Count; i++)
            {
                var newAxisItem = new AxisItem();
                //show text in the x-axis labels only for every label
                if (i % 1 == 0)
                {
                    //format the DateTime object
                    string formattedLabelText = string.Format("{0:MMM yy}", view.ToTable().Rows[i]["Date"]);
                    newAxisItem.LabelText = formattedLabelText;
                }
                else
                {
                    newAxisItem.LabelText = "";
                }
                radChart1.PlotArea.XAxis.Items.Add(newAxisItem);
            }
        }

        private void DisplayMessage(RadGrid grid, string text)
        {
            grid.Controls.Add(new LiteralControl(text));
        }


        public bool Validate(RadNumericTextBox textBox)
        {
            bool result = true;
            if (string.IsNullOrEmpty(textBox.Text))
            {
                result = false;
                if (textBox.ID == "RNTB_txtUnitPrice")
                    DisplayMessage(PriceHistoryGrid, "<font color=\"red\">Please Enter Unit Price*</font>");
            }
            return result;
        }

        public bool Validate(TextBox textBox)
        {
            bool result = true;
            if (string.IsNullOrEmpty(textBox.Text))
            {
                result = false;
                if (textBox.ID == "RTB_txtCode")
                    DisplayMessage(ManualAssetGrid, "<font color=\"red\">Please Enter Manual Asset Code*</font>");
                else if (textBox.ID == "RTB_txtMarket")
                    DisplayMessage(ManualAssetGrid, "<font color=\"red\">Please Enter Market*</font>");
                else if (textBox.ID == "RTB_txtDescription")
                    DisplayMessage(ManualAssetGrid, "<font color=\"red\">Please Enter Company Name*</font>");
            }
            return result;
        }
    }
}
