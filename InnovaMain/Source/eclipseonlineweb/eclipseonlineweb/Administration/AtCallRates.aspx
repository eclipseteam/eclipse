﻿<%@ Page Title="" Language="C#" MasterPageFile="SetupMaster.master" AutoEventWireup="true"
    CodeBehind="AtCallRates.aspx.cs" Inherits="eclipseonlineweb.Administration.AtCallRates" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                        </td>
                        <td width="80%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="At Call Rates"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid OnNeedDataSource="PresentationGrid_OnNeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnItemCommand="PresentationGridItemCommand"
                GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" OnInsertCommand="PresentationGridInsertCommand"
                EnableViewState="true" ShowFooter="false" OnItemDataBound="PresentationGridItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="AtCallRates" TableLayout="Fixed" EditMode="EditForms">
                    <CommandItemSettings AddNewRecordText="Add New"></CommandItemSettings>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="ID" HeaderText="ID" ReadOnly="true" Display="false"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="ID" UniqueName="ID">
                        </telerik:GridBoundColumn>
                        <telerik:GridEditCommandColumn HeaderStyle-Width="30px" ButtonType="ImageButton"
                            UniqueName="EditCommandColumn2">
                            <ItemStyle CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridEditCommandColumn>
                        <telerik:GridBoundColumn SortExpression="IsEditable" HeaderText="IsEditableCol" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            ReadOnly="true" Visible="False" DataField="IsEditable" UniqueName="IsEditableCol">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="InstituteID" HeaderText="InstituteID" ReadOnly="true"
                            Display="false" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InstituteID" UniqueName="InstituteID">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn  FilterControlWidth="65px"  HeaderStyle-Width="100px" ItemStyle-Width="100px" SortExpression="InstituteName"
                            HeaderText="Broker" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" ReadOnly="true" DataField="InstituteName"
                            UniqueName="InstituteName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="PriceInstituteID" HeaderText="PriceInstituteID"
                            ReadOnly="true" Display="false" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="PriceInstituteID"
                            UniqueName="PriceInstituteID">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="65px" SortExpression="PriceInstituteName" HeaderText="Bank" AutoPostBackOnFilter="true"
                            HeaderStyle-Width="110px" ItemStyle-Width="110px" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" ReadOnly="true" DataField="PriceInstituteName"
                            UniqueName="PriceInstituteName">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn HeaderText="Broker" SortExpression="InstituteName"
                            UniqueName="BrokerEditor" HeaderStyle-Width="100px" ItemStyle-Width="100px" DataField="InstituteName"
                            Visible="false" ColumnEditorID="GridDropDownColumnEditorModelBrokerankList" />
                        <telerik:GridDropDownColumn HeaderText="Bank" SortExpression="PriceInstituteName"
                            HeaderStyle-Width="100px" ItemStyle-Width="100px" UniqueName="BankEditor" DataField="PriceInstituteName"
                            Visible="false" ColumnEditorID="GridDropDownColumnEditorModelBankList" />
                        <telerik:GridBoundColumn SortExpression="AccountTier" HeaderText="Description" AutoPostBackOnFilter="true"
                            HeaderStyle-Width="150px" ItemStyle-Width="150px" FilterControlWidth="110px"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="AccountTier" UniqueName="AccountTier">
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn SortExpression="Min" HeaderStyle-Width="200px" ItemStyle-Width="250px"
                            HeaderText="Min" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Min" UniqueName="Min"
                            DataFormatString="{0:C}" NumericType="Currency">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn SortExpression="Max" HeaderStyle-Width="200px" ItemStyle-Width="250px"
                            HeaderText="Max" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Max" UniqueName="Max"
                            DataFormatString="{0:C}" NumericType="Currency">
                        </telerik:GridNumericColumn>
                        <telerik:GridTemplateColumn SortExpression="Rate" FilterControlWidth="270px" HeaderStyle-Width="310px"
                            ItemStyle-Width="310px" HeaderText="Rate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Rate" UniqueName="Rate">
                            <ItemTemplate>
                                <asp:Label ID="lblRate" runat="server" Text='<%# Eval("Rate") %>'></asp:Label>
                                <%--                                <asp:TextBox CssClass="EditTextArea" TextMode="MultiLine" Width="150px" Rows="3"
                                    runat="server" ID="Rate"></asp:TextBox>--%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox CssClass="EditTextArea" TextMode="MultiLine" Width="180px" Rows="3"
                                    Text='<%# Eval("Rate") %>' runat="server" ID="Rate"></asp:TextBox>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn SortExpression="MaximumBrokerage" HeaderText="MaximumBrokerage"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="MaximumBrokerage" UniqueName="MaximumBrokerage"
                            ReadOnly="true" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" ReadOnly="true" SortExpression="Type"
                            HeaderText="Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Type" UniqueName="Type">
                        </telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn FilterControlWidth="100px" PickerType="DatePicker" SortExpression="ApplicableFrom"
                            HeaderText="Applicable From" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ApplicableFrom"
                            DataFormatString="{0:dd/MM/yyyy}" UniqueName="ApplicableFrom" EditDataFormatString="dd/MM/yyyy"
                            FilterDateFormat="dd/MM/yyyy">
                        </telerik:GridDateTimeColumn>
                        <telerik:GridTemplateColumn HeaderText="Honeymoon period" FilterControlWidth="100px"
                            Visible="False">
                            <EditItemTemplate>
                                <telerik:RadNumericTextBox runat="server" ID="HoneymoonPeriod" DisplayText='<%# Eval("HoneymoonPeriod") %>'
                                    MinValue="0">
                                    <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                </telerik:RadNumericTextBox>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderStyle-Width="130px" HeaderText="Applicability"
                            UniqueName="Applicability">
                            <ItemTemplate>
                                <asp:Label ID="lblApplicability" runat="server" Text='<%# Eval("Applicability") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <telerik:RadListBox ID="lstApplicability" runat="server" CheckBoxes="true" SelectionMode="Multiple">
                                    <Items>
                                        <telerik:RadListBoxItem runat="server" Text="Individual" />
                                        <telerik:RadListBoxItem runat="server" Text="Company" />
                                        <telerik:RadListBoxItem runat="server" Text="Trust" />
                                        <telerik:RadListBoxItem runat="server" Text="Partnership" />
                                        <telerik:RadListBoxItem runat="server" Text="Superfund" />
                                    </Items>
                                </telerik:RadListBox>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridCheckBoxColumn HeaderStyle-Width="70px" SortExpression="IsEditable"
                            HeaderText="Is Editable" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="false" HeaderButtonType="TextButton" DataField="IsEditable" UniqueName="IsEditable"
                            DefaultInsertValue="True" Visible="false" ReadOnly="true">
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridButtonColumn HeaderStyle-Width="30px" ConfirmText="Are you sure you want to delete it?"
                            ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="Html"></Excel>
                </ExportSettings>
            </telerik:RadGrid>
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorModelBrokerList"
                runat="server" DropDownStyle-Width="200px" />
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorModelBankList"
                runat="server" DropDownStyle-Width="200px" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
