﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="SecurityDetailsMaster.master"
    AutoEventWireup="true" CodeBehind="SecurityDividends.aspx.cs" Inherits="eclipseonlineweb.SecurityDividends" %>

<%@ Register TagPrefix="uc" TagName="Details" Src="SecurityDivindendDetails.ascx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                                ID="btnDownload" />
                            <a href="Securities.aspx">
                                <img src="../images/window_previous.png" border="0" style="border: 0 !important;" /></a>
                            <asp:ImageButton runat="server" ImageUrl="~/images/add-icon.png" OnClick="AddNewClick"
                                ID="btnAddNew" ToolTip="Add New" />
                                <telerik:RadButton runat="server" ID="btnProcessAllDividend" OnClick="btnProcessAllDividend_Click"
                                    ToolTip="Process All Dividends">
                                    <ContentTemplate>
                                        <img src="../images/distribution.png" alt="" class="btnImageWithText" />
                                        <span class="riLabel">Process Dividends</span>
                                    </ContentTemplate>
                                </telerik:RadButton>
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblCode" Text=""></asp:Label>
                            <asp:Label Font-Bold="true" runat="server" ID="Label1" Text="-"></asp:Label>
                            <asp:Label Font-Bold="true" runat="server" ID="lblSecurityDesc" Text="Description"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource"
                OnItemCommand="PresentationGrid_OnItemCommand">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="Banks" TableLayout="Fixed">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="ID" UniqueName="ID" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="SecID" ReadOnly="true" HeaderText="SecID"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="SecID" UniqueName="SecID" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="60%" HeaderStyle-Width="6%" SortExpression="InvestmentCode"
                            ReadOnly="true" HeaderText="Code" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InvestmentCode"
                            UniqueName="InvestmentCode">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" HeaderStyle-Width="11%" SortExpression="TransactionType"
                            ReadOnly="true" HeaderText="Transaction Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransactionType"
                            UniqueName="TransactionType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" SortExpression="BalanceDate" ReadOnly="true"
                            HeaderText="Balance Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BalanceDate" UniqueName="BalanceDate"
                            DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" SortExpression="DividendType" ReadOnly="true"
                            HeaderText="Dividend Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="DividendType"
                            UniqueName="DividendType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="RecordDate"
                            HeaderText="Ex-Dividend Date" AutoPostBackOnFilter="true" ShowFilterIcon="true"
                            CurrentFilterFunction="Contains" HeaderButtonType="TextButton" DataField="RecordDate"
                            UniqueName="RecordDate" DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" SortExpression="BooksCloseDate"
                            ReadOnly="true" HeaderText="Books Close Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BooksCloseDate"
                            UniqueName="BooksCloseDate" DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="PaymentDate"
                            HeaderText="Payment Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="PaymentDate" UniqueName="PaymentDate"
                            DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" SortExpression="CentsPerShare"
                            ReadOnly="true" HeaderText="Cents Per Share" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CentsPerShare"
                            UniqueName="CentsPerShare" DataFormatString="{0:N4}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" SortExpression="Currency" ReadOnly="true"
                            HeaderText="Currency (Ccy)" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Currency" UniqueName="Currency">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ButtonType="LinkButton" CommandName="Details" Text="Details"
                            UniqueName="DetailColumn">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyLinkButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                        <telerik:GridButtonColumn ButtonType="LinkButton" CommandName="Process" Text="Process"
                            UniqueName="ProcessColumn">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyLinkButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                        <telerik:GridButtonColumn ConfirmText="Are you sure you want to remove this entry?"
                            ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
            <asp:Button ID="btnCancel" runat="server" Style="display: none" />
            <asp:Button ID="btnOkay" runat="server" Style="display: none" />
            <asp:ModalPopupExtender ID="ModalPopupExtender1" CancelControlID="btnCancel" OkControlID="btnOkay"
                BackgroundCssClass="ModalPopupBG" runat="server" Drag="true" TargetControlID="btnShowPopup"
                PopupControlID="panEdit" PopupDragHandleControlID="PopupHeader">
            </asp:ModalPopupExtender>
            <asp:Panel runat="server" ID="panEdit">
                <uc:Details ID="DividendDetails" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
