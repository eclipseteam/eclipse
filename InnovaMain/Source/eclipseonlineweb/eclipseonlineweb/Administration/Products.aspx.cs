﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using Oritax.TaxSimp;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;

namespace eclipseonlineweb
{
    public partial class Products : UMABasePage
    {
        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }
        private ICMBroker UMABroker1
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        //ProductType combo items
        static SortedList<string, string> ProductTypes = new SortedList<string, string>{
                                              {"unlisted","Unlisted"} ,
                                              {"listed","Listed"}
       
        };
        //Type combo items
        static SortedList<int, string> OrganizationTypes = new SortedList<int, string>() { 
         {(int)OrganizationType.BankAccount, "Bank Account"},
         {(int)OrganizationType.DesktopBrokerAccount, "Desktop Broker Account"},
         {(int)OrganizationType.TermDepositAccount, "Term Deposit Account"},
         {(int)OrganizationType.ManagedInvestmentSchemesAccount, "Managed Investment Schemes Account"}
        };
        //BankAccountTypes combo items
        static SortedList<string, string> BankAccountTypes = new SortedList<string, string>() { 
         {"saving", "Saving"},
         {"cheque", "Cheque"},
         {"cma", "CMA"},
         {"cmt", "CMT"},
         {"mma", "MMA"},
         {"termdeposit", "Term Deposit"},
         {"other", "Other"}
        };

        protected override void GetBMCData()
        {
            IOrganization organization = this.UMABroker1.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            ProductsDS productsDS = new ProductsDS();

            organization.GetData(productsDS);
            this.PresentationData = productsDS;

            UMABroker1.ReleaseBrokerManagedComponent(organization);
        }


        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {

            //DataSet excelDataset = new DataSet();
            //excelDataset.Merge(PresentationData.Tables[SecuritiesDS.SECLIST], true, MissingSchemaAction.Add);
            //excelDataset.Tables[0].Columns.Remove("ID");
            //ExcelHelper.ToExcel(excelDataset, "Securities-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", this.Page.Response);

        }
        public override void LoadPage()
        {
            base.LoadPage();

            DBUser objUser = (DBUser)UMABroker1.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType != UserType.Innova))
            {
                PresentationGrid.MasterTableView.GetColumn("Management").Display = false;
                PresentationGrid.MasterTableView.GetColumn("Report").Display = false;
                PresentationGrid.MasterTableView.GetColumn("EntityType").Display = false;
            }
        }


        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {

        }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "details")
            {
                //string ID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"].ToString();
                //Response.Redirect("SecurityDetails.aspx?ID=" + ID);
            }
            else if (e.CommandName == "Management")
            {
                string ID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"].ToString();
                Response.Redirect("InvestmentManagement.aspx?ID=" + ID);
            }
            else if (e.CommandName == "Report")
            {
                string ID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"].ToString();
                Response.Redirect("InvestmentProfile.aspx?ID=" + ID);
            }
            else if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                Guid ID = new Guid(gDataItem["ID"].Text);
                ProductsDS DS = new ProductsDS();
                DS.CommandType = DatasetCommandTypes.Delete;
                DataTable dt = DS.Tables[ProductsDS.PRODUCTSTABLE];
                DataRow dr = dt.NewRow();
                dr[ProductsDS.ID] = ID;
                dt.Rows.Add(dr);

                base.SaveOrganizanition(DS);
                GetBMCData();
                this.PresentationGrid.Rebind();

            }
            else if (e.CommandName.ToLower() == "update")
            {

                ProductsDS DS = new ProductsDS();
                DS.CommandType = DatasetCommandTypes.Update;
                DataTable dt = DS.Tables[ProductsDS.PRODUCTSTABLE];
                DataRow row = dt.NewRow();
                FillRowData(e, row, DS.CommandType);
                
                if (!e.Canceled)
                {
                    dt.Rows.Add(row);
                    SaveOrganizanition(DS);
                    e.Canceled = true;
                    e.Item.Edit = false;
                    GetBMCData();
                    this.PresentationGrid.Rebind();
                }
                
            }
        }
        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e)
        {

        }
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e)
        {

        }
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e)
        {

        }
        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("Products".Equals(e.Item.OwnerTableView.Name))
            {

                UMABroker1.SaveOverride = true;
                IOrganization iorg = this.UMABroker1.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                ProductsDS DS = new ProductsDS();
                DS.CommandType = DatasetCommandTypes.Add;

                DataRow row = DS.Tables[ProductsDS.PRODUCTSTABLE].NewRow();
                FillRowData(e, row, DS.CommandType);
                if (!e.Canceled)
                {
                    DS.Tables[ProductsDS.PRODUCTSTABLE].Rows.Add(row);
                    SaveOrganizanition(DS);
                    e.Canceled = true;
                    e.Item.Edit = false;
                    GetBMCData();
                    this.PresentationGrid.Rebind();
                }

              
                //}
            }
        }

        private void FillRowData(GridCommandEventArgs e, DataRow row, DatasetCommandTypes cmdType)
        {
            #region Validation
            if (!ValidateComboSelection(((RadComboBox)(((GridEditableItem)(e.Item))["cmbOrgType"].Controls[0]))))
            {
                e.Canceled = true;
                return;
            }

            if (!ValidateComboSelection(((RadComboBox)(((GridEditableItem)(e.Item))["ComboInstitute"].Controls[0]))))
            {
                e.Canceled = true;
                return;
            }
       
            
            if(!ValidateTextInput(((TextBox)(((GridEditableItem)(e.Item))["txtName"].Controls[0]))))
            {
                e.Canceled = true;
                return;
            }




            OrganizationType type;
            if (!System.Enum.TryParse(((RadComboBox)(((GridEditableItem)(e.Item))["cmbOrgType"].Controls[0])).SelectedItem.Value, out type))
            {
                //error please select type
            }

            #endregion

            if (cmdType == DatasetCommandTypes.Update)
                row[ProductsDS.ID] = ((TextBox)(((GridEditableItem)(e.Item))["ID"].Controls[0])).Text;

            row[ProductsDS.NAME] = ((TextBox)(((GridEditableItem)(e.Item))["txtName"].Controls[0])).Text;
            row[ProductsDS.DESCRIPTION] = ((TextBox)(((GridEditableItem)(e.Item))["txtDescription"].Controls[0])).Text;
            row[ProductsDS.EntityType] =(int) type;

            if (((RadComboBox)(((GridEditableItem)(e.Item))["cmbProductType"].Controls[0])).SelectedItem != null)
                row[ProductsDS.ProductType] = ((RadComboBox)(((GridEditableItem)(e.Item))["cmbProductType"].Controls[0])).SelectedItem.Value;

            row[ProductsDS.ProductAPIR] = ((RadMaskedTextBox)(((GridEditableItem)(e.Item))["ProductAPIRNo"].Controls[0])).Text;
            row[ProductsDS.ProductISIN] = ((RadMaskedTextBox)(((GridEditableItem)(e.Item))["ProductISINNo"].Controls[0])).Text;


            if (((RadComboBox)(((GridEditableItem)(e.Item))["cmbProductSecuritiesId"].Controls[0])).SelectedItem != null)
                row[ProductsDS.ProductSecuritiesId] = ((RadComboBox)(((GridEditableItem)(e.Item))["cmbProductSecuritiesId"].Controls[0])).SelectedItem.Value;
            else
                row[ProductsDS.ProductSecuritiesId] = Guid.Empty;


            row[ProductsDS.IsDefaultProductSecurity] = false;


            

            switch (type)
            {
                case OrganizationType.BankAccount:
                    if (((RadComboBox)(((GridEditableItem)(e.Item))["ComboInstitute"].Controls[0])).SelectedItem != null)
                        row[ProductsDS.EntityId] = ((RadComboBox)(((GridEditableItem)(e.Item))["ComboInstitute"].Controls[0])).SelectedItem.Value;
                    else
                        row[ProductsDS.EntityId] = Guid.Empty;
                    row[ProductsDS.EntityCsId] = Guid.Empty;
                    if (((RadComboBox)(((GridEditableItem)(e.Item))["ComboAccountCode"].Controls[0])).SelectedItem != null)
                        row[ProductsDS.BankAccountType] = ((RadComboBox)(((GridEditableItem)(e.Item))["ComboAccountCode"].Controls[0])).SelectedItem.Value;
                    else
                        row[ProductsDS.BankAccountType] = string.Empty;

                   
                    if (!ValidateComboSelection(((RadComboBox)(((GridEditableItem)(e.Item))["ComboAccountCode"].Controls[0]))))
                    {
                        e.Canceled = true;
                        return;
                    }
                    break;
                case OrganizationType.DesktopBrokerAccount:

                    if (((RadComboBox)(((GridEditableItem)(e.Item))["ComboInstitute"].Controls[0])).SelectedItem != null)
                        row[ProductsDS.EntityId] = ((RadComboBox)(((GridEditableItem)(e.Item))["ComboInstitute"].Controls[0])).SelectedItem.Value;
                    else
                        row[ProductsDS.EntityId] = Guid.Empty;

                    row[ProductsDS.EntityCsId] = Guid.Empty;
                    row[ProductsDS.IsDefaultProductSecurity] = ((CheckBox)(((GridEditableItem)(e.Item))["chkIsDef"].Controls[0])).Checked;

                    break;
                case OrganizationType.TermDepositAccount:

                    Guid Clid = Guid.Empty;

                    if (((RadComboBox)(((GridEditableItem)(e.Item))["ComboInstitute"].Controls[0])).SelectedItem != null)
                        Clid = Guid.Parse(((RadComboBox)(((GridEditableItem)(e.Item))["ComboInstitute"].Controls[0])).SelectedItem.Value);
                    //else
                    //    row[ProductsDS.EntityId] = Guid.Empty;
                    TermDepositBrokerBasicDS TDs = GetTDList();
                    var brokerrow = TDs.TermDepositBasicTable.AsEnumerable().FirstOrDefault(rows => rows.Field<Guid>(TDs.TermDepositBasicTable.CLID) == Clid);

                    row[ProductsDS.EntityId] = Clid;

                    if (brokerrow == null)
                        row[ProductsDS.EntityCsId] = Guid.Empty;
                    else
                        row[ProductsDS.EntityCsId] = brokerrow[TDs.TermDepositBasicTable.CSID];


                    break;
                case OrganizationType.ManagedInvestmentSchemesAccount:

                    //entity.EntityId = new IdentityCM { Clid = (ComboInstitute.SelectedItem as ManagedInvestmentSchemesAccountEntityCsidClidDetail).Clid, Csid = (ComboInstitute.SelectedItem as ManagedInvestmentSchemesAccountEntityCsidClidDetail).Csid };
                    //entity.FundAccounts = new List<Guid> { ((KeyValuePair<Guid, string>)(ComboAccountCode.SelectedItem)).Key };
                    Guid ClidMIS = Guid.Empty;

                    if (((RadComboBox)(((GridEditableItem)(e.Item))["ComboInstitute"].Controls[0])).SelectedItem != null)
                        ClidMIS = Guid.Parse(((RadComboBox)(((GridEditableItem)(e.Item))["ComboInstitute"].Controls[0])).SelectedItem.Value);
                    //else
                    //    row[ProductsDS.EntityId] = Guid.Empty;
                    MISBasicDS MISs = GetMISList();
                    var misrow = MISs.MISBasicTable.AsEnumerable().FirstOrDefault(rows => rows.Field<Guid>(MISs.MISBasicTable.CLID) == ClidMIS);

                    row[ProductsDS.EntityId] = ClidMIS;

                    if (misrow == null)
                        row[ProductsDS.EntityCsId] = Guid.Empty;
                    else
                        row[ProductsDS.EntityCsId] = misrow[MISs.MISBasicTable.CSID];


                    if (((RadComboBox)(((GridEditableItem)(e.Item))["ComboAccountCode"].Controls[0])).SelectedItem != null)
                        row[ProductsDS.FundAccounts] = ((RadComboBox)(((GridEditableItem)(e.Item))["ComboAccountCode"].Controls[0])).SelectedItem.Value;
                    else
                        row[ProductsDS.FundAccounts] = Guid.Empty;
                    
                    if (!ValidateComboSelection(((RadComboBox)(((GridEditableItem)(e.Item))["ComboAccountCode"].Controls[0]))))
                    {
                        e.Canceled = true;
                        return;
                    }
                    break;
            }
        }



        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;
                IOrganization organization = this.UMABroker1.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

                string selectedProductType = DataBinder.Eval(editedItem.DataItem, "ProductType").ToString();
                GridDropDownListColumnEditor CEProductType = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("cmbProductType"));
                CEProductType.DataSource = ProductTypes;
                CEProductType.DataValueField = "Key";
                CEProductType.DataTextField = "Value";
                CEProductType.DataBind();
                if (!string.IsNullOrEmpty(selectedProductType))
                    CEProductType.SelectedValue = selectedProductType;


                string selectedProdSec = DataBinder.Eval(editedItem.DataItem, "ProductSecuritiesId").ToString();
                GridDropDownListColumnEditor CEProductSecurities = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("cmbProductSecuritiesId"));
                CEProductSecurities.DataSource = organization.ProductSecurities;
                CEProductSecurities.DataValueField = "ID";
                CEProductSecurities.DataTextField = "Name";

                CEProductSecurities.DataBind();
                if (!string.IsNullOrEmpty(selectedProdSec))
                    CEProductSecurities.SelectedValue = selectedProdSec;

                string selectedEntityType = DataBinder.Eval(editedItem.DataItem, "EntityType").ToString();
                #region alternate way
                // OR
                // if (!(e.Item is IGridInsertItem))
                // CEType.SelectedValue = ((DataRowView)e.Item.DataItem)["EntityType"].ToString();
                #endregion
                GridDropDownListColumnEditor CEType = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("cmbOrgType"));
                CEType.DataSource = OrganizationTypes;
                CEType.DataValueField = "Key";
                CEType.DataTextField = "Value";                
                CEType.DataBind();

                if (!string.IsNullOrEmpty(selectedEntityType))
                {
                    int key = ((int)System.Enum.Parse(typeof(OrganizationType), selectedEntityType));
                    CEType.SelectedValue = key.ToString();

                    //load inst based on CEType.SelectedValue
                    cmbOrgType_SelectedIndexChanged((RadComboBox)editedItem["cmbOrgType"].Controls[0], null);
                    //get/set old val
                    string selectedComboInstitute = DataBinder.Eval(editedItem.DataItem, "EntityId").ToString();
                    GridDropDownListColumnEditor CEEntityId = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("ComboInstitute"));
                    CEEntityId.SelectedValue = selectedComboInstitute;//set

                    //load bank acc combo 
                    ComboInstitute_SelectedIndexChanged((RadComboBox)editedItem["ComboInstitute"].Controls[0], null);

                    GridDropDownListColumnEditor CEBankAccountType = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("ComboAccountCode"));

                    OrganizationType orgType;
                    System.Enum.TryParse(selectedEntityType, out orgType);
                    switch (orgType)
                    {
                        case OrganizationType.BankAccount:

                            CEBankAccountType.SelectedValue = DataBinder.Eval(editedItem.DataItem, "BankAccountType").ToString();
                            break;
                        case OrganizationType.ManagedInvestmentSchemesAccount:

                            CEBankAccountType.SelectedValue = DataBinder.Eval(editedItem.DataItem, "FundAccounts").ToString();
                            break;


                    }

                }
                else
                {
                    cmbOrgType_SelectedIndexChanged((RadComboBox)editedItem["cmbOrgType"].Controls[0], null);                    
                    ComboInstitute_SelectedIndexChanged((RadComboBox)editedItem["ComboInstitute"].Controls[0], null);
                }


            }
        }

        public bool ValidateTextInput(TextBox txt)
        {
            bool result = true;
            if (string.IsNullOrEmpty(txt.Text))
            {
                result = false;
                if (txt.ID == "ctl10") //txtName
                    DisplayMessage("<font color=\"red\">Name is required</font>");                

            }
            return result;
        }

        public bool ValidateComboSelection(RadComboBox cmb)
        {
            bool result = true;
            if (string.IsNullOrEmpty(cmb.SelectedValue))
            {
                result = false;
                if (cmb.ID == "RCB_cmbOrgType")
                    DisplayMessage("<font color=\"red\">Please Select Type *</font>");
                if (cmb.ID == "RCB_ComboInstitute")
                    DisplayMessage("<font color=\"red\">Please Select Financial Institute</font>");
                if (cmb.ID == "RCB_ComboAccountCode")
                    DisplayMessage("<font color=\"red\">Please Select Account Code</font>");
                
            }
            return result;
        }
        private void DisplayMessage(string text)
        {
            PresentationGrid.Controls.Add(new LiteralControl(text));
        }

        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
            {
                GridEditFormItem EditForm = (GridEditFormItem)e.Item;
                RadComboBox cmbOrgType = (RadComboBox)EditForm["cmbOrgType"].Controls[0];
                cmbOrgType.AutoPostBack = true;
                cmbOrgType.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(cmbOrgType_SelectedIndexChanged);

                RadComboBox ComboInstitute = (RadComboBox)EditForm["ComboInstitute"].Controls[0];
                ComboInstitute.AutoPostBack = true;
                ComboInstitute.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(ComboInstitute_SelectedIndexChanged);


                #region ClientSide Validations
                GridEditableItem item = e.Item as GridEditableItem;
                GridTextBoxColumnEditor editor_Name = (GridTextBoxColumnEditor)item.EditManager.GetColumnEditor("txtName");
                GridDropDownListColumnEditor editor_EntityId = (GridDropDownListColumnEditor)(item.EditManager.GetColumnEditor("ComboInstitute"));
                TableCell cell = (TableCell)editor_Name.TextBoxControl.Parent;

                RequiredFieldValidator validator_Name = new RequiredFieldValidator();
                editor_Name.TextBoxControl.ID = "ID_for_txtName";
                validator_Name.ControlToValidate = editor_Name.TextBoxControl.ID;
                validator_Name.ErrorMessage = " Name Required";
                //editor_Name.TextBoxControl.BorderColor = System.Drawing.Color.Red;
                cell.Controls.Add(validator_Name);

                RequiredFieldValidator validator_EntityId = new RequiredFieldValidator();
                editor_EntityId.ComboBoxControl.ID = "ID_for_ComboInstitute";
                validator_EntityId.ControlToValidate = editor_EntityId.ComboBoxControl.ID;
                validator_EntityId.ErrorMessage = "<font color=\"red\">Institution is required</font>";
                cell.Controls.Add(validator_Name);
                #endregion

            }
        }

        void cmbOrgType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox cmb = ((RadComboBox)sender);
            GridEditableItem editableItem = (GridEditableItem)cmb.NamingContainer;
            GridEditManager editMan = editableItem.EditManager;

            RadComboBox cmbOrgType = (RadComboBox)editableItem["cmbOrgType"].Controls[0];
            RadComboBox ComboAccountCode = (RadComboBox)editableItem["ComboAccountCode"].Controls[0];
            int key = ((int)System.Enum.Parse(typeof(OrganizationType), cmbOrgType.SelectedValue));

            #region logic for intituition and AcountFundCombo's
            GridDropDownListColumnEditor CEEntityId = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("ComboInstitute"));
            IOrganization organization = this.UMABroker1.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            switch ((OrganizationType)key)
            {
                case OrganizationType.BankAccount:
                case OrganizationType.DesktopBrokerAccount:
                    CEEntityId.DataSource = organization.Institution;
                    CEEntityId.DataValueField = "ID";
                    CEEntityId.DataTextField = "Name";
                    CEEntityId.DataBind();
                    ComboAccountCode.Enabled = false;
                    break;
                case OrganizationType.TermDepositAccount:

                    TermDepositBrokerBasicDS ds1 = GetTDList();
                    CEEntityId.DataSource = ds1.TermDepositBasicTable;
                    CEEntityId.DataValueField = ds1.TermDepositBasicTable.CLID;
                    CEEntityId.DataTextField = ds1.TermDepositBasicTable.BROKERNAME;
                    CEEntityId.DataBind();
                    CEEntityId.SelectedText = "Default";
                    ComboAccountCode.Enabled = false;
                    break;
                case OrganizationType.ManagedInvestmentSchemesAccount:

                    MISBasicDS ds2 = GetMISList();
                    CEEntityId.DataSource = ds2.MISBasicTable;
                    CEEntityId.DataValueField = ds2.MISBasicTable.CLID;
                    CEEntityId.DataTextField = ds2.MISBasicTable.NAME;
                    CEEntityId.DataBind();

                    ComboAccountCode.Enabled = true;
                    ManagedInvestmentAccountDS ds = GetFundDataSet(new Guid(GetMISList().Tables[GetMISList().MISBasicTable.TABLENAME].Rows[0][GetMISList().MISBasicTable.CID].ToString()));
                    FundAccountTable funds = ds.FundAccountsTable;
                    GridDropDownListColumnEditor CEBankAccountType = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("ComboAccountCode"));
                    CEBankAccountType.DataSource = funds;
                    CEBankAccountType.DataTextField = funds.FUNDCODE;
                    CEBankAccountType.DataValueField = funds.FUNDID;
                    CEBankAccountType.DataBind();

                    break;

            }
            #endregion

        }

        void ComboInstitute_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox cmb = ((RadComboBox)sender);
            GridEditableItem editableItem = (GridEditableItem)cmb.NamingContainer;
            GridEditManager editMan = editableItem.EditManager;

            RadComboBox ComboInstitute = (RadComboBox)editableItem["ComboInstitute"].Controls[0];
            RadComboBox ComboAccountCode = (RadComboBox)editableItem["ComboAccountCode"].Controls[0];
            RadComboBox cmbOrgType = (RadComboBox)editableItem["cmbOrgType"].Controls[0];
            int key = ((int)System.Enum.Parse(typeof(OrganizationType), cmbOrgType.SelectedValue));


            #region logic for intituition and AcountFundCombo's
            GridDropDownListColumnEditor CEBankAccountType = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("ComboAccountCode"));
            IOrganization organization = this.UMABroker1.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            switch ((OrganizationType)key)
            {
                case OrganizationType.BankAccount:
                    ComboAccountCode.Enabled = true;
                    CEBankAccountType.SelectedIndex = -1;
                    CEBankAccountType.DataSource = BankAccountTypes;
                    CEBankAccountType.DataValueField = "Key";
                    CEBankAccountType.DataTextField = "Value";
                    CEBankAccountType.DataBind();

                    break;
                case OrganizationType.DesktopBrokerAccount:

                    CEBankAccountType.SelectedIndex = -1;
                    CEBankAccountType.DataSource = null;

                    break;
                case OrganizationType.TermDepositAccount:

                    //var termDepositAccount = ComboInstitute.SelectedItem;
                    //if (termDepositAccount == null)
                    //{
                    //    return;
                    //}
                    //    if (termDepositAccount.Clid == Guid.Empty && termDepositAccount.Csid == Guid.Empty && termDepositAccount.Entity.Name == "Default")
                    //    {
                    //        return;
                    //    }
                    //GetTermDepositLinkedAccounts(termDepositAccount);

                    break;
                case OrganizationType.ManagedInvestmentSchemesAccount:
                    ComboAccountCode.Enabled = true;
                    //Guid mis = Guid.Parse(ComboInstitute.SelectedValue.ToString());

                    MISBasicDS misDS = GetMISList();
                    ManagedInvestmentAccountDS ds = GetFundDataSet(new Guid(misDS.Tables[misDS.MISBasicTable.TABLENAME].Rows[0][misDS.MISBasicTable.CID].ToString()));
                    FundAccountTable funds = ds.FundAccountsTable;
                    CEBankAccountType.DataSource = funds;
                    CEBankAccountType.DataTextField = funds.FUNDCODE;
                    CEBankAccountType.DataValueField = funds.FUNDID;
                    CEBankAccountType.DataBind();

                    //FundAccountTable dt = GetMISLinkedAccList(mis);
                    //CEBankAccountType.DataSource = dt;
                    //CEBankAccountType.DataValueField = dt.FUNDDESCRIPTION;
                    //CEBankAccountType.DataTextField = dt.FUNDID;
                    //CEEntityId.DataBind();

                    break;

            }
            #endregion

        }

        private MISBasicDS GetMISList()
        {

            var org = UMABroker1.GetWellKnownBMC(WellKnownCM.Organization);
            MISBasicDS MIS = new MISBasicDS();
            MIS.Command = (int)Oritax.TaxSimp.Commands.WebCommands.GetOrganizationUnitsByType;
            MIS.Unit = new Oritax.TaxSimp.Data.OrganizationUnit
            {
                CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                Type = ((int)OrganizationType.ManagedInvestmentSchemesAccount).ToString()

            };
            org.GetData(MIS);
            return MIS;
        }

        private FundAccountTable GetMISLinkedAccList(Guid MISClid)
        {
            ManagedInvestmentAccountDS ds = GetFundDataSet(MISClid);
            FundAccountTable funds = ds.FundAccountsTable;

            return funds;
        }
        private ManagedInvestmentAccountDS GetFundDataSet(Guid MISCid)
        {
            ManagedInvestmentAccountDS misDS = new ManagedInvestmentAccountDS();
            misDS.FundID = Guid.Empty;
            if (Broker != null)
            {
                IBrokerManagedComponent clientData = this.Broker.GetBMCInstance(MISCid);
                misDS.CommandType = DatasetCommandTypes.Get;
                clientData.GetData(misDS);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }



            return misDS;

        }

        private TermDepositBrokerBasicDS GetTDList()
        {
            //IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(_cid));
            //TermDepositDS ds = new TermDepositDS();
            //clientData.GetData(ds);

            var org = UMABroker1.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            TermDepositBrokerBasicDS TD = new TermDepositBrokerBasicDS();
            TD.Command = (int)Oritax.TaxSimp.Commands.WebCommands.GetOrganizationUnitsByType;
            TD.Unit = new Oritax.TaxSimp.Data.OrganizationUnit
            {
                CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                Type = ((int)OrganizationType.TermDepositAccount).ToString()
            };
            org.GetData(TD);
            DataRow defaultTD = TD.TermDepositBasicTable.NewRow();
            defaultTD[TD.TermDepositBasicTable.BROKERNAME] = "Default";
            defaultTD[TD.TermDepositBasicTable.CLID] = Guid.Empty;
            defaultTD[TD.TermDepositBasicTable.CSID] = Guid.Empty;
            TD.TermDepositBasicTable.Rows.Add(defaultTD);

            return TD;
        }
        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetBMCData();
            var f = PresentationGrid.MasterTableView.FilterExpression;

            DataView View = new DataView(this.PresentationData.Tables[ProductsDS.PRODUCTSTABLE]);
            //View.Sort = ProductsDS.NAME + " ASC";
            this.PresentationGrid.DataSource = View.ToTable();
            //PresentationGrid.DataBind();
        }


    }
}
