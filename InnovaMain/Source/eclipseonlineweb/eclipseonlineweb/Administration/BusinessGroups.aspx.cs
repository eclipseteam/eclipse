﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;

namespace eclipseonlineweb
{
    public partial class BusinessGroups : UMABasePage
    {
        private ICMBroker umaBroker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected override void GetBMCData()
        {
            IOrganization organization = 
                this.umaBroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            BusinessGroupsDS businessGroupsDs = new BusinessGroupsDS();
            organization.GetData(businessGroupsDs);
            this.PresentationData = businessGroupsDs;
            umaBroker.ReleaseBrokerManagedComponent(organization);
        }
        public override void LoadPage()
        {
            base.LoadPage();
            DBUser objUser = 
                (DBUser)umaBroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
            {
                ((SetupMaster)Master).IsAdmin = "1";
            }
        }
        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
        }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "details")
            {
            }
            else if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                Guid ID = new Guid(gDataItem["ID"].Text);
                BusinessGroupsDS DS = new BusinessGroupsDS();
                DS.CommandType = DatasetCommandTypes.Delete;
                DataTable dt = DS.Tables[BusinessGroupsDS.TABLENAME];
                DataRow dr = dt.NewRow();
                dr[BusinessGroupsDS.ID] = ID;
                dt.Rows.Add(dr);

                base.SaveOrganizanition(DS);
                GetBMCData();
                this.PresentationGrid.Rebind();
            }
            else if (e.CommandName.ToLower() == "update")
            {
                BusinessGroupsDS DS = new BusinessGroupsDS();
                DS.CommandType = DatasetCommandTypes.Delete;
                DataTable dt = DS.Tables[BusinessGroupsDS.TABLENAME];
                DataRow row = dt.NewRow();
                row[BusinessGroupsDS.ID] = ((TextBox)(((GridEditableItem)(e.Item))["ID"].Controls[0])).Text;
                row[BusinessGroupsDS.NAME] = ((TextBox)(((GridEditableItem)(e.Item))["txtName"].Controls[0])).Text;
                row[BusinessGroupsDS.DESCRIPTION] = ((TextBox)(((GridEditableItem)(e.Item))["txtDescription"].Controls[0])).Text;
                dt.Rows.Add(row);
                DS.CommandType = DatasetCommandTypes.Update;
                SaveOrganizanition(DS);
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.PresentationGrid.Rebind();
            }
        }
        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e)
        {

        }
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e)
        {

        }
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e)
        {

        }
        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("BusinessGroups".Equals(e.Item.OwnerTableView.Name))
            {
                umaBroker.SaveOverride = true;
                IOrganization iorg = this.umaBroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                BusinessGroupsDS DS = new BusinessGroupsDS();
                DS.CommandType = DatasetCommandTypes.Add;
                DataRow row = DS.Tables[BusinessGroupsDS.TABLENAME].NewRow();
                row[BusinessGroupsDS.NAME] = ((TextBox)(((GridEditableItem)(e.Item))["txtName"].Controls[0])).Text;
                row[BusinessGroupsDS.DESCRIPTION] = ((TextBox)(((GridEditableItem)(e.Item))["txtDescription"].Controls[0])).Text;
                DS.Tables[BusinessGroupsDS.TABLENAME].Rows.Add(row);

                iorg.SetData(DS);
                umaBroker.SetComplete();
                umaBroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.PresentationGrid.Rebind();
            }
        }
        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
        }
        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
        }
        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetBMCData();
            DataView View = new DataView(this.PresentationData.Tables[BusinessGroupsDS.TABLENAME]);
            View.Sort = BusinessGroupsDS.NAME + " ASC";
            this.PresentationGrid.DataSource = View.ToTable();
        }
    }
}
