﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eclipseonlineweb
{
    public partial class InstituteDetailsMaster : MasterPage
    {
        public string SecurityMenu
        {
            get
            {
                return hfInsMenu.Value;
            }
            set
            {
                hfInsMenu.Value = value;
            }
        }

        public string SecurityID
        {
            get
            {
                return hfInsID.Value;
            }
            set
            {
                hfInsID.Value = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ((HiddenField)Master.FindControl("hfMainMenuText")).Value = "INVESTMENT";
            if (!Page.IsPostBack && Request.Params["ID"] != null)
               SecurityID = Request.Params["ID"];
        }

    }
}