﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb
{
    public partial class ViewModelClients : UMABasePage
    {
        private string ProgarmCode;
        protected override void GetBMCData()
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var serviceTypes = new ClientModelsDetailsDS { Command = (int)WebCommands.GetAllClientDetails, CommandType = DatasetCommandTypes.Get, Unit = new OrganizationUnit { CurrentUser = GetCurrentUser(), } };
            organization.GetData(serviceTypes);
            this.PresentationData = serviceTypes;
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }
        public override void LoadPage()
        {
            base.LoadPage();
            if (Request.Params["ProgramCode"] != null)
            {
                ProgarmCode = Request.Params["ProgramCode"];
            }
        }


        public override void PopulatePage(DataSet ds)
        {
            ScriptManager sm = ScriptManager.GetCurrent(Page);

            if (sm != null)
                sm.RegisterPostBackControl(this.btnDownload);
        }



        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            if (pnlMainGrid.Visible)
            {
                IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                var serviceTypes = new ClientModelsDetailsDS { Command = (int)WebCommands.GetAllClientDetails, CommandType = DatasetCommandTypes.Get, Unit = new OrganizationUnit { CurrentUser = GetCurrentUser(), } };
                organization.GetData(serviceTypes);

                UMABroker.ReleaseBrokerManagedComponent(organization);

                DataTable dt = serviceTypes.ClientModelsTable;

                string name = "All";
                if (!string.IsNullOrEmpty(ProgarmCode))
                {
                    name = ProgarmCode;
                    var rows = serviceTypes.ClientModelsTable.Select(string.Format("{0}='{1}'", serviceTypes.ClientModelsTable.PROGRAMCODE, ProgarmCode));
                    dt = rows.Length > 0 ? rows.CopyToDataTable() : serviceTypes.ClientModelsTable.Clone();
                }
                DataSet excelDataset = new DataSet();
                excelDataset.Merge(dt, true, MissingSchemaAction.Add);


                ExcelHelper.ToExcel(excelDataset, name + "-ClientsModel-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", this.Page.Response);
            }

        }



        #region Telerik Code



        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var Ds = (this.PresentationData as ClientModelsDetailsDS);

            DataTable dt = Ds.ClientModelsTable;

            if (!string.IsNullOrEmpty(ProgarmCode))
            {
                var rows = Ds.ClientModelsTable.Select(string.Format("{0}='{1}'", Ds.ClientModelsTable.PROGRAMCODE, ProgarmCode));
                dt = rows.Length > 0 ? rows.CopyToDataTable() : Ds.ClientModelsTable.Clone();
            }
            var view = new DataView(dt);
            view.Sort = Ds.ClientModelsTable.CLIENTNAME + " ASC";
            this.PresentationGrid.DataSource = view.ToTable();
        }



        #endregion
    }
}
