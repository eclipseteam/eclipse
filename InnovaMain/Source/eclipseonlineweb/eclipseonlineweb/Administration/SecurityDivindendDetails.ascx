﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecurityDivindendDetails.ascx.cs"
    Inherits="eclipseonlineweb.Administration.SecurityDivindendDetails" %>
<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" Assembly="C1.Web.Wijmo.Controls.4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<div class="popup_Container">
    <div class="popup_Titlebar" id="PopupHeader">
        <div class="TitlebarLeft">
            <asp:Label ID="Title" runat="server" Text="Divindends Details"></asp:Label>
        </div>
    </div>
    <div class="popup_Body">
        <table>
            <tr>
                <td>
                </td>
                <td>
                    <asp:HiddenField ID="TextID" runat="server" />
                </td>
                <td>
                </td>
                <td>
                    <asp:HiddenField ID="TextSecID" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Administration System"></asp:Label>
                </td>
                <td>
                    <c1:C1ComboBox ID="cmbAdministrationSystem" runat="server" ViewStateMode="Enabled" TabIndex="0">
                        <Items>
                            <c1:C1ComboBoxItem Value="Manual Transactions" Text="Manual Transactions"></c1:C1ComboBoxItem>
                        </Items>
                    </c1:C1ComboBox>
                </td>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Transaction Type"></asp:Label>
                </td>
                <td>
                    <c1:C1ComboBox ID="cmbTransactionType" runat="server" ViewStateMode="Enabled" TabIndex="1">
                        <Items>
                            <c1:C1ComboBoxItem Value="Dividend Accrual" Text="Dividend Accrual"></c1:C1ComboBoxItem>
                        </Items>
                    </c1:C1ComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label24" runat="server" Text="Balance Date"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtBalanceDate" runat="server" ReadOnly="false" TabIndex="2" />
                    <ajaxToolkit:CalendarExtender runat="server" ID="calBanalceDate" TargetControlID="txtBalanceDate"
                        Format="dd/MM/yyyy " />
                </td>
                <td>
                    <asp:Label ID="Label27" runat="server" Text="Dividend Type"></asp:Label>
                </td>
                <td>
                    <c1:C1ComboBox ID="cmbDividendType" runat="server" ViewStateMode="Enabled" TabIndex="3">
                        <Items>
                            <c1:C1ComboBoxItem Value="Interim" Text="Interim"></c1:C1ComboBoxItem>
                            <c1:C1ComboBoxItem Value="Final" Text="Final"></c1:C1ComboBoxItem>
                            <c1:C1ComboBoxItem Value="CapitalReturn" Text="CapitalReturn"></c1:C1ComboBoxItem>
                        </Items>
                    </c1:C1ComboBox>
                </td>
                
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Ex-Dividend Date"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextRecordDate" runat="server" ReadOnly="false" TabIndex="4" />
                    <ajaxToolkit:CalendarExtender runat="server" ID="calRecodDate" TargetControlID="TextRecordDate"
                        Format="dd/MM/yyyy " />
                </td>
                <td>
                    <asp:Label ID="Label25" runat="server" Text="Books Close Date"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtBooksCloseDate" runat="server" ReadOnly="false" 
                        TabIndex="5" />
                    <ajaxToolkit:CalendarExtender runat="server" ID="calBooksCloseDate" TargetControlID="txtBooksCloseDate"
                        Format="dd/MM/yyyy " />
                </td>
                
            </tr>            
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Payment Date"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextPaymentDate" runat="server" ReadOnly="false" 
                        TabIndex="6" />
                    <ajaxToolkit:CalendarExtender runat="server" ID="calPaymentDate" TargetControlID="TextPaymentDate"
                        Format="dd/MM/yyyy " />
                </td>
                <td>
                    <asp:Label ID="Label10" runat="server" Text="Cents per share ($)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextCentsPerShare" runat="server" TabIndex="7" />
                </td>
                
                
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Franked Amount (%)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextFrankedAmount" runat="server" TabIndex="8" />
                </td>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Unfranked Amount (%)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextUnfrankedAmount" runat="server" TabIndex="9" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label26" runat="server" Text="Currency (Ccy)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCurrency" runat="server" TabIndex="10" />
                </td>                
                <td>
                    <asp:Label ID="Label8" runat="server" Text="Tax Deferred (%)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextTaxDeferred" runat="server" TabIndex="11" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" Text="Tax Free (%)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextTaxFree" runat="server" TabIndex="12" />
                </td>
                <td>
                    <asp:Label ID="Label9" runat="server" Text="Return on Capital (%)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextReturnofCapital" runat="server" TabIndex="13" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label13" runat="server" Text="Domestic"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label14" runat="server" Text="Foreign"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label11" runat="server" Text="Interest Amount (%)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextDomesticInterestAmount" runat="server" TabIndex="14" />
                </td>
                <td>
                    <asp:TextBox ID="TextForeignInterestAmount" runat="server" TabIndex="15" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label12" runat="server" Text="Other Income (%)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextDomesticOtherIncome" runat="server" TabIndex="16" />
                </td>
                <td>
                    <asp:TextBox ID="TextForeignOtherIncome" runat="server" TabIndex="17" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label15" runat="server" Text="Withholding Tax (%)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextDomesticWithHoldingTax" runat="server" TabIndex="18" />
                </td>
                <td>
                    <asp:TextBox ID="TextForeignWithHoldingTax" runat="server" TabIndex="19" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label16" runat="server" Text="Imputation Tax Credits (%)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextDomesticImputationTaxCredits" runat="server" 
                        TabIndex="20" />
                </td>
                <td>
                    <asp:TextBox ID="TextForeignImputationTaxCredits" runat="server" 
                        TabIndex="21" />
                </td>
                <td>
                </td>
            </tr>
            <tr style="display:none">
                <td>
                    <asp:Label ID="Label17" runat="server" Text="Foreign Investment Funds (FIF) (%)"></asp:Label>
                </td>
                <td>
                </td>
                <td>
                    <asp:TextBox ID="TextForeignInvestmentFunds" runat="server" TabIndex="22" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label18" runat="server" Text="Tax Components" Font-Bold="true"></asp:Label>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label19" runat="server" Text="Indexation CGT (%)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextDomesticIndexationCGT" runat="server" TabIndex="23" />
                </td>
                <td>
                    <asp:TextBox ID="TextForeignIndexationCGT" runat="server" TabIndex="24" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label20" runat="server" Text="Discounted CGT (%)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextDomesticDiscountedCGT" runat="server" TabIndex="25" />
                </td>
                <td>
                    <asp:TextBox ID="TextForeignDiscountedCGT" runat="server" TabIndex="26" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label21" runat="server" Text="Other CGT (%)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextDomesticOtherCGT" runat="server" TabIndex="27" />
                </td>
                <td>
                    <asp:TextBox ID="TextForeignOtherCGT" runat="server" TabIndex="28" />
                </td>
                <td>
                </td>
            </tr>
            <tr style="display:none">
                <td>
                    <asp:Label ID="Label22" runat="server" Text="Gross Amount (%)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextDomesticGrossAmount" runat="server" TabIndex="29" />
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label23" runat="server" Text="Comment"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="TextComment" runat="server" Width="100%" TabIndex="30" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="2">
                    <asp:Label ID="lblMessages" runat="server" Text="" ForeColor="Red" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                    <asp:Button ID="btnUpdate" runat="server" Text="Save" 
                        OnClick="btnUpdate_OnClick" TabIndex="31" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                        OnClick="btnCancel_OnClick" TabIndex="32" />
                </td>
            </tr>
        </table>
    </div>
</div>
