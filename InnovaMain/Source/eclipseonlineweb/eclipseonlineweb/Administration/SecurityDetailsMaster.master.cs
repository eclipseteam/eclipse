﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eclipseonlineweb
{
    public partial class SecurityDetailsMaster : MasterPage
    {
        public string SecurityMenu
        {
            get
            {
                return hfSecMenu.Value;
            }
            set
            {
                hfSecMenu.Value = value;
            }
        }

        public string SecurityID
        {
            get
            {
                return hfSecID.Value;
            }
            set
            {
                hfSecID.Value = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ((HiddenField)Master.FindControl("hfMainMenuText")).Value = "INVESTMENT";
        }

    }
}