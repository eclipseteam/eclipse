﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/NoMenu.Master" AutoEventWireup="true" CodeBehind="InvestmentProfile2.aspx.cs" Inherits="eclipseonlineweb.InvestmentProfile2" %>

<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" Assembly="C1.Web.Wijmo.Controls.4, Version=4.0.20121.56, Culture=neutral, PublicKeyToken=9b75583953471eea" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer" TagPrefix="C1ReportViewer" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<%--<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>--%>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:breadcrumb id="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="Label1" Text="INNOVA FUNDS PERFORMANCE     "></asp:Label>
                            <asp:Label Font-Bold="true" Font-Italic ="true" Font-color="blue" runat="server" ID="lblName" Text="" Visible="false"></asp:Label>
                            <asp:Label Font-Bold="true" Visible="false" runat="server" ID="lblID"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <div id="MainView">
            
            <asp:Panel runat="server" ID="Performance" Visible="true">
<%--            <asp:Label Font-Bold="true" Font-Italic ="true" Font-color="blue" runat="server" ID="Label2" Text="Click on an icon to generate performance report for the selected fund" ></asp:Label>
--%>
<%--                <telerik:RadGrid  ID="PresentationGrid" OnNeedDataSource="PresentationGrid_NeedDataSource" HorizontalAlign="Center"
                runat="server" ShowStatusBar="false" AutoGenerateColumns="False" PageSize="20" ShowHeader="false"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"  ShowFooter="false" 
                OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" BorderStyle="Solid" BorderColor="DarkOrange" BorderWidth="1px" Width="50%"
                EnableViewState="true" OnItemDataBound="PresentationGrid_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" CommandItemDisplay="Top" Name="Products" TableLayout="Fixed" EditMode="PopUp">
                    <CommandItemSettings AddNewRecordText="Add New Product" ShowExportToExcelButton="false"  
                        ShowExportToWordButton="false" ShowExportToPdfButton="false" ShowAddNewRecordButton="false"  ></CommandItemSettings>
                     <ItemStyle Height="15px" />                    
                         <Columns>
                        <telerik:GridBoundColumn ReadOnly="true" Visible="false" DataField="ID" UniqueName="ID" />
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Name" HeaderStyle-Width="18%" Visible="false" 
                            ItemStyle-Width="18%" HeaderText="Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Name" UniqueName="txtName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="28%" ItemStyle-Width="28%"
                            SortExpression="Description" HeaderText="Description" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Description" UniqueName="txtDescription">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn CommandName="Report" Text="Generate Performance Report" ButtonType="LinkButton"
                            ItemStyle-Width="24px"  ImageUrl="~/images/pie_chart.png"
                            HeaderStyle-Width="25%">
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <C1ReportViewer:C1ReportViewer CollapseToolsPanel="true" Cache-Enabled="false" Cache-ShareBetweenSessions="false"
                FileName="InMemoryBasicTable" runat="server" ID="C1ReportViewer1" 
                Width="100%" Zoom="75%" Height="550px">
            </C1ReportViewer:C1ReportViewer>--%>

                <table width="100%">
                   <tr>
                    <td width="50%">
                        <telerik:RadGrid  ID="PresentationGrid" OnNeedDataSource="PresentationGrid_NeedDataSource"
                runat="server" ShowStatusBar="false" AutoGenerateColumns="False" PageSize="20" ShowHeader="false"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" 
                OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" 
                EnableViewState="true" ShowFooter="false" OnItemDataBound="PresentationGrid_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top" Name="Products" TableLayout="Fixed" EditMode="PopUp">
                    <CommandItemSettings AddNewRecordText="Add New Product" ShowExportToExcelButton="false" 
                        ShowExportToWordButton="false" ShowExportToPdfButton="false" ShowAddNewRecordButton="false"  ></CommandItemSettings>
                    <Columns>
                        <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ID" UniqueName="ID" />
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Name" HeaderStyle-Width="18%" visible="false"
                            ItemStyle-Width="18%" HeaderText="Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Name" UniqueName="txtName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="28%" ItemStyle-Width="28%"
                            SortExpression="Description" HeaderText="Description" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Description" UniqueName="txtDescription">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn CommandName="Report" Text="Generate Performance Report" ButtonType="LinkButton" 
                            ItemStyle-Width="24px" ItemStyle-Height="24px" ImageUrl="~/images/pie_chart.png"
                            HeaderStyle-Width="15%">
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>

                    </td>
                    <td width="50%" height="100%">       
                        <C1ReportViewer:C1ReportViewer CollapseToolsPanel="true" Cache-Enabled="false" Cache-ShareBetweenSessions="false"
                            FileName="InMemoryBasicTable" runat="server" ID="C1ReportViewer1" 
                            Width="100%" Zoom="75%" Height="550px">
                        </C1ReportViewer:C1ReportViewer>
                    </td>
                </tr>
                </table>

                 </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
