﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="SetupMaster.master"
    AutoEventWireup="true" CodeBehind="ModelDetailsView.aspx.cs" Inherits="eclipseonlineweb.ModelDetailsView" %>
<%@ Register TagPrefix="uc1" TagName="breadcrumb" Src="~/Controls/BreadCrumb.ascx" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function hintContent() {
            return this.data.label + '<br/> ' + this.y + '';
        }
        function hintContentPie() {
            return this.data.toString() + " : " + Globalize.format(this.value / this.total, "p2");
        }

    </script>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/window_previous.png" AlternateText="Back to Listing"
                                ToolTip="Back to Listing" OnClick="btnBackToListing" ID="btnBack" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:breadcrumb id="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblModelName" Text="Financial Models"></asp:Label>
                            <asp:Label Font-Bold="true" Visible="false" runat="server" ID="lblModelID"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
                <AjaxSettings>
                    <telerik:AjaxSetting AjaxControlID="RadGrid1">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="RadGrid1"></telerik:AjaxUpdatedControl>
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                </AjaxSettings>
            </telerik:RadAjaxManager>
            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
            </telerik:RadAjaxLoadingPanel>
            <script type="text/javascript">
                function onRequestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("Button1") >= 0)
                        args.set_enableAjax(false);
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                            args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                            args.get_eventTarget().indexOf("ExportToPdfButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }
            </script>
            <telerik:RadGrid OnNeedDataSource="RadGrid1_NeedDataSource" ID="RadGrid1" runat="server"
                ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20" AllowSorting="True"
                AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="RadGrid1_DetailTableDataBind"
                OnItemCommand="RadGrid1_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="false" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnGridExportingEvent="RadGrid1_OnGridExporting" OnItemUpdated="RadGrid1_ItemUpdated"
                OnItemDeleted="RadGrid1_ItemDeleted" OnItemInserted="RadGrid1_ItemInserted" OnInsertCommand="RadGrid1_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="RadGrid1_ItemCreated"
                OnItemDataBound="RadGrid1_ItemDataBound" OnDataBound="RadGrid1_DataBound" OnItemDataBinding="RadGrid1_ItemDataBinding">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView HierarchyDefaultExpanded="false" DataKeyNames="AssetID" AllowMultiColumnSorting="True"
                    Width="100%" CommandItemDisplay="Top" Name="Assets" TableLayout="Fixed" AllowFilteringByColumn="false">
                    <CommandItemSettings AddNewRecordText="Add New Asset" ShowExportToExcelButton="true"
                        ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                    <DetailTables>
                        <telerik:GridTableView EditMode="EditForms" DataKeyNames="ProID" Width="100%" runat="server"
                            CommandItemDisplay="Top" Name="Products" AllowFilteringByColumn="false">
                            <CommandItemSettings AddNewRecordText="Add New Product"></CommandItemSettings>
                            <ParentTableRelation>
                                <telerik:GridRelationFields DetailKeyField="AssetID" MasterKeyField="AssetID"></telerik:GridRelationFields>
                            </ParentTableRelation>
                            <Columns>
                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                                    <HeaderStyle Width="2%"></HeaderStyle>
                                    <ItemStyle CssClass="MyImageButton"></ItemStyle>
                                </telerik:GridEditCommandColumn>
                                <telerik:GridBoundColumn ReadOnly="true" SortExpression="ProDescription" CurrentFilterFunction="Contains"
                                    HeaderStyle-Width="33%" ItemStyle-Width="31%" ShowFilterIcon="false" HeaderText="Name"
                                    AutoPostBackOnFilter="true" HeaderButtonType="TextButton" DataField="ProDescription"
                                    UniqueName="ProDescriptionText">
                                </telerik:GridBoundColumn>
                                <telerik:GridDropDownColumn Visible="false" HeaderText="Name" SortExpression="Name"
                                    HeaderStyle-Width="15%" ItemStyle-Width="15%" UniqueName="ProDescription" DataField="ProDescription"
                                    ListTextField="Description" ListValueField="ID" ColumnEditorID="GridDropDownColumnEditorProduct" />
                                <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="15%" DefaultInsertValue="0"
                                    SortExpression="ProMinAllocation" HeaderText="Min Allocation" AutoPostBackOnFilter="true"
                                    DataFormatString="{0:N2}" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                    HeaderButtonType="TextButton" DataField="ProMinAllocation" UniqueName="ProMinAllocation">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="15%" DefaultInsertValue="0"
                                    SortExpression="ProNeutralPercentage" Aggregate="Sum" FooterAggregateFormatString="{0:N2}"
                                    DataFormatString="{0:N2}" HeaderText="Neutral" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" HeaderButtonType="TextButton" DataField="ProNeutralPercentage"
                                    UniqueName="ProNeutralPercentage">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="15%" DefaultInsertValue="0"
                                    SortExpression="ProDynamicPercentage" FooterAggregateFormatString="{0:N2}" DataFormatString="{0:N2}"
                                    Aggregate="Sum" HeaderText="Dynamic" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" HeaderButtonType="TextButton" DataField="ProDynamicPercentage"
                                    UniqueName="ProDynamicPercentage">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="15%" DefaultInsertValue="0"
                                    SortExpression="ProMaxAllocation" HeaderText="Max Allocation" AutoPostBackOnFilter="true"
                                    DataFormatString="{0:N2}" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                    HeaderButtonType="TextButton" DataField="ProMaxAllocation" UniqueName="ProMaxAllocation">
                                </telerik:GridBoundColumn>
                                <telerik:GridButtonColumn ConfirmText="Delete these details record?" ButtonType="ImageButton"
                                    CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                                    <HeaderStyle Width="5%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                                </telerik:GridButtonColumn>
                            </Columns>
                            <EditFormSettings EditFormType="AutoGenerated">
                            </EditFormSettings>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="ProDescription"></telerik:GridSortExpression>
                            </SortExpressions>
                        </telerik:GridTableView>
                    </DetailTables>
                    <Columns>
                        <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="AssetDescription"
                            ReadOnly="true" HeaderStyle-Width="35%" ItemStyle-Width="30%" HeaderText="Asset Desc."
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                            HeaderButtonType="TextButton" DataField="AssetDescription" UniqueName="AssetDescriptionText">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn HeaderText="Asset Class" SortExpression="Name" UniqueName="AssetDescription"
                            DataField="AssetDescription" Visible="false" ListTextField="Description" ListValueField="ID"
                            ColumnEditorID="GridDropDownListColumnEditorAssets" />
                        <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                            SortExpression="AssetMinAllocation" HeaderText="Min Allocation" AutoPostBackOnFilter="true"
                            DataFormatString="{0:N2}" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                            HeaderButtonType="TextButton" DataField="AssetMinAllocation" UniqueName="AssetMinAllocation"
                            ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                            SortExpression="AssetNeutralPercentage" Aggregate="Sum" FooterAggregateFormatString="{0:N2}"
                            DataFormatString="{0:N2}" HeaderText="Neutral" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="false" HeaderButtonType="TextButton" DataField="AssetNeutralPercentage"
                            UniqueName="AssetNeutralPercentage" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                            SortExpression="AssetDynamicPercentage" Aggregate="Sum" FooterAggregateFormatString="{0:N2}"
                            DataFormatString="{0:N2}" HeaderText="Dynamic" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="false" HeaderButtonType="TextButton" DataField="AssetDynamicPercentage"
                            UniqueName="AssetDynamicPercentage" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                            SortExpression="AssetMaxAllocation" HeaderText="Max Allocation" AutoPostBackOnFilter="true"
                            DataFormatString="{0:N2}" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                            HeaderButtonType="TextButton" DataField="AssetMaxAllocation" UniqueName="AssetMaxAllocation"
                            ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ConfirmText="Delete this asset?" ButtonType="ImageButton"
                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="Html"></Excel>
                </ExportSettings>
            </telerik:RadGrid>
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorProduct" runat="server"
                EnableViewState="true" DropDownStyle-Width="400px" />
            <asp:Label ID="lblModelDetailID" runat="server" EnableViewState="false" Visible="false"></asp:Label>
            <telerik:GridDropDownListColumnEditor EnableViewState="true" ID="GridDropDownListColumnEditorAssets"
                runat="server" DropDownStyle-Width="450px" />
            <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
