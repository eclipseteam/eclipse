﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;

namespace eclipseonlineweb
{
    public partial class Assets : UMABasePage
    {
        protected override void GetBMCData()
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            AssetDS AssetDS = new AssetDS();

            organization.GetData(AssetDS);
            this.PresentationData = AssetDS;

            UMABroker.ReleaseBrokerManagedComponent(organization);
        }


        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {

            //DataSet excelDataset = new DataSet();
            //excelDataset.Merge(PresentationData.Tables[SecuritiesDS.SECLIST], true, MissingSchemaAction.Add);
            //excelDataset.Tables[0].Columns.Remove("ID");
            //ExcelHelper.ToExcel(excelDataset, "Securities-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", this.Page.Response);

        }
        public override void LoadPage()
        {
            base.LoadPage();

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
            {
                ((SetupMaster)Master).IsAdmin = "1";
            }
        }


        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {

        }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "details")
            {
                //string ID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"].ToString();
                //Response.Redirect("SecurityDetails.aspx?ID=" + ID);
            }
            else if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                Guid ID = new Guid(gDataItem["ID"].Text);
                AssetDS DS = new AssetDS();
                DS.CommandType = DatasetCommandTypes.Delete;
                DataTable dt = DS.Tables[AssetDS.TABLENAME];
                DataRow dr = dt.NewRow();
                dr[AssetDS.ID] = ID;
                dt.Rows.Add(dr);

                base.SaveOrganizanition(DS);
                GetBMCData();
                this.PresentationGrid.Rebind();

            }
            else if (e.CommandName.ToLower() == "update")
            {
                AssetDS DS = new AssetDS();
                DS.CommandType = DatasetCommandTypes.Delete;
                DataTable dt = DS.Tables[AssetDS.TABLENAME];
                DataRow row = dt.NewRow();

                row[AssetDS.ID] = ((TextBox)(((GridEditableItem)(e.Item))["ID"].Controls[0])).Text;
                row[AssetDS.NAME] = ((TextBox)(((GridEditableItem)(e.Item))["txtName"].Controls[0])).Text;
                row[AssetDS.DESCRIPTION] = ((TextBox)(((GridEditableItem)(e.Item))["txtDescription"].Controls[0])).Text;

                dt.Rows.Add(row);
                //DS.Tables.Add(dt);
                DS.CommandType = DatasetCommandTypes.Update;
                SaveOrganizanition(DS);
                e.Canceled = true;
                e.Item.Edit = false;

                GetBMCData();
                this.PresentationGrid.Rebind();
            }
        }
        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e)
        {

        }
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e)
        {

        }
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e)
        {

        }

        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("Assets".Equals(e.Item.OwnerTableView.Name))
            {
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                AssetDS DS = new AssetDS();
                DS.CommandType = DatasetCommandTypes.Add;
                DataRow row = DS.Tables[AssetDS.TABLENAME].NewRow();
                row[AssetDS.NAME] = ((TextBox)(((GridEditableItem)(e.Item))["txtName"].Controls[0])).Text;
                row[AssetDS.DESCRIPTION] = ((TextBox)(((GridEditableItem)(e.Item))["txtDescription"].Controls[0])).Text;
                DS.Tables[AssetDS.TABLENAME].Rows.Add(row);

                iorg.SetData(DS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.PresentationGrid.Rebind();
            }
        }

        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            //if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
            //{
            //    GridEditFormItem EditForm = (GridEditFormItem)e.Item;
            //    RadComboBox combo = (RadComboBox)EditForm["cmbTransactionType"].Controls[0];
            //    combo.AutoPostBack = true;
            //    combo.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(combo_SelectedIndexChanged);
            //}
        }


        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {

        }



        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetBMCData();

            DataView View = new DataView(this.PresentationData.Tables[AssetDS.TABLENAME]);
            View.Sort = AssetDS.NAME + " ASC";
            this.PresentationGrid.DataSource = View.ToTable();
            //PresentationGrid.DataBind();
        }


    }
}
