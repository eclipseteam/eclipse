﻿using System;
using System.Data;
using System.Web.UI;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;

namespace eclipseonlineweb
{
    public partial class InstitutionsContactDetail : UMABasePage
    {
        protected override void Intialise()
        {
            base.Intialise();
            ExistingContactsControl1.CanceledPopup += () => OVER.Visible = ContactPopup.Visible = false;
            ExistingContactsControl1.ValidationFailed += () => ShowHideExisting(true);
            ExistingContactsControl1.SaveData += (contactDetail) =>
            {
                SaveInstitute(contactDetail,DatasetCommandTypes.Update);
                ShowHideExisting(false);
            };
        }

        public void SaveInstitute(InstitutionContactsDS institutionContactsDs, DatasetCommandTypes cmd)
        {

            institutionContactsDs.CommandType = cmd;
            institutionContactsDs.Id = new Guid(Request.QueryString["ID"].Trim());
            SaveOrganizanition(institutionContactsDs);
            RedGDContacts.Rebind();
        }
        protected void BtnBackToListing(object sender, ImageClickEventArgs e)
        {
            //InstitutionGrid.Visible = true;
            //btnBack.Visible = false;
            //lblTDTransactionListing.Text = "Financial Institutions";
        }
        protected void lnkbtnAddExistngContact_Click(object sender, EventArgs e)
        {
            ExistingContactsControl1.ClearEntity();
            ShowHideExisting(true);
        }
        private void ShowHideExisting(bool show)
        {
            OVER.Visible = ContactPopup.Visible = show;
        }
        protected void btnClose_OnClick(object sender, EventArgs e)
        {
            ShowHideExisting(false);
        }

        protected void GetInstituteContactData(object sender, GridNeedDataSourceEventArgs e)
        {
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var ContactDS = new InstitutionContactsDS();
            ContactDS.Id = new Guid(Request.QueryString["ID"].Trim());
           if (organization != null)
            {
                organization.GetData(ContactDS);
                UMABroker.ReleaseBrokerManagedComponent(organization);
            }
           lblInsName.Text = ContactDS.InstituteName;
           

            RedGDContacts.DataSource = ContactDS;
        }

        protected void InstituteGridItemCommand(object sender, GridCommandEventArgs e)
        {
            var dataItem = (e.Item as GridDataItem);
            switch (e.CommandName.ToLower())
            {
                case "delete":
                    string clid = dataItem["Clid"].Text;
                    string csid = dataItem["Csid"].Text;
                    var InstContactDS = new InstitutionContactsDS();
                    DataRow dr = InstContactDS.IndividualTable.NewRow();
                    dr[InstContactDS.IndividualTable.CLID] = new Guid(clid);
                    dr[InstContactDS.IndividualTable.CSID] = new Guid(csid);
                    InstContactDS.IndividualTable.Rows.Add(dr);
                    SaveInstitute(InstContactDS, DatasetCommandTypes.Delete);
                    break;
            }
        }
    }

}