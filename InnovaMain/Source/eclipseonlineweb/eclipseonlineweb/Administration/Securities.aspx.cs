﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;


namespace eclipseonlineweb
{
    public partial class Securities : UMABasePage
    {
        public string[] InvestmentTypes = {"Artwork","Bank Bill" ,"Bond","Cash","Debenture" ,"Fixed Interest" ,"Foreign Exchange","Index","Investment Property",
                                           "Managed Fund /Unit Trust","Option","Other CGT Assessable" ,"Other CGT Exempt" ,"Residential Property","Rightslssue",
                                           "Share","Term Deposit" ,"Warrant",};

        public string[] Statuses = { "Active", "Closed", "Frozen", "No Buy", "No Sell", };
        public string[] RatingList = { "Buy", "Hold", "Sell", "Strong Buy", };

        protected override void GetBMCData()
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            SecuritiesDS securitiesDS = new Oritax.TaxSimp.DataSets.SecuritiesDS();
            organization.GetData(securitiesDS);
            this.PresentationData = securitiesDS;
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        protected void GetSecurityData()
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            SecuritiesDS SecurityDS = new Oritax.TaxSimp.DataSets.SecuritiesDS();
            SecurityDS.SecID = new Guid(hfSecID.Value);
            organization.GetData(SecurityDS);
            this.PresentationData = SecurityDS;
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        public override void LoadPage()
        {
            base.LoadPage();
            btnProcessAllDividend.Attributes.Add("onclick", "javascript:return confirm('This process may take several minutes. Are you sure you want to process all dividends?');");
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
            {
                //1 means client or adviser
                ((SetupMaster)Master).IsAdmin = "1";
            }
            //Only Admin User can Process All and Add/Edit/Delete
            btnProcessAllDividend.Visible = objUser.Administrator;
            SecurityGrid.MasterTableView.CommandItemSettings.ShowAddNewRecordButton = objUser.Administrator;
            SecurityGrid.Columns.FindByUniqueNameSafe("DeleteColumn2").Visible = SecurityGrid.Columns.FindByUniqueNameSafe("EditCommandColumn2").Visible = objUser.Administrator;
        }

        private void RunCommand(Guid SecID, DatasetCommandTypes command)
        {
            SecuritiesDS securitiesDs = new SecuritiesDS();
            securitiesDs.SecID = SecID;
            securitiesDs.CommandType = command;
            this.SaveOrganizanition(securitiesDs);
        }

        protected void SecurityGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {

        }

        protected void SecurityGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "details")
            {
                string ID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"].ToString();
                Response.Redirect("SecurityDetails.aspx?ID=" + ID);
            }
            else if (e.CommandName.ToLower() == "delete")
            {
                Guid SecID = new Guid(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"].ToString());
                RunCommand(SecID, DatasetCommandTypes.Delete);
                GetBMCData();
            }
            else if (e.CommandName.ToLower() == "update")
            {
                Guid SecID = new Guid(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"].ToString());
                SecuritiesDS securitiesDs = new SecuritiesDS();
                DataTable dt = securitiesDs.Tables[SecuritiesDS.SECLIST];
                DataRow dr = dt.NewRow();
                securitiesDs.SecID = SecID;
                dr[SecuritiesDS.ID] = securitiesDs.SecID;

                PopulateDataRow(dr, e);
                dt.Rows.Add(dr);
                securitiesDs.Tables.Remove(SecuritiesDS.SECLIST);
                securitiesDs.Tables.Add(dt);
                securitiesDs.CommandType = DatasetCommandTypes.Update;
                SaveOrganizanition(securitiesDs);
                e.Canceled = true;
                e.Item.Edit = false;
                this.SecurityGrid.Rebind();

            }
        }

        protected void PopulateDataRow(DataRow dr, GridCommandEventArgs e)
        {
            GridEditableItem item = (GridEditableItem)e.Item;
            RadComboBox combo = (RadComboBox)item["cmbTransactionType"].Controls[0];
            if (!Validate(combo))
            {
                e.Canceled = true;
                return;
            }
            else if (!Validate(((RadComboBox)(((GridEditableItem)(e.Item))["cmbAsset"].Controls[0]))))
            {
                e.Canceled = true;
                return;
            }
            else if (!Validate(((RadComboBox)(((GridEditableItem)(e.Item))["cmbRating"].Controls[0]))))
            {
                e.Canceled = true;
                return;
            }
            else if (!Validate(((RadComboBox)(((GridEditableItem)(e.Item))["cmbStatus"].Controls[0]))))
            {
                e.Canceled = true;
                return;
            }

            dr[SecuritiesDS.INVESTMENTTYPE] = combo.SelectedItem.Text;

            dr[SecuritiesDS.ASSET] = new Guid(((RadComboBox)(((GridEditableItem)(e.Item))["cmbAsset"].Controls[0])).SelectedItem.Value);
            dr[SecuritiesDS.RATING] = ((RadComboBox)(((GridEditableItem)(e.Item))["cmbRating"].Controls[0])).SelectedItem.Value;
            dr[SecuritiesDS.STATUS] = ((RadComboBox)(((GridEditableItem)(e.Item))["cmbStatus"].Controls[0])).SelectedItem.Value;
            dr[SecuritiesDS.MARKET] = ((TextBox)(((GridEditableItem)(e.Item))["txtMarket"].Controls[0])).Text;
            dr[SecuritiesDS.ISINCODE] = ((TextBox)(((GridEditableItem)(e.Item))["txtISINCode"].Controls[0])).Text;
            string strUnits = ((TextBox)(((GridEditableItem)(e.Item))["txtUnitHeld"].Controls[0])).Text;
            if (string.IsNullOrEmpty(strUnits))
                dr[SecuritiesDS.UNITSHELD] = 0.0;
            else
                dr[SecuritiesDS.UNITSHELD] = Convert.ToDouble(strUnits);
            dr[SecuritiesDS.DESCRIPTION] = ((TextBox)(((GridEditableItem)(e.Item))["Description"].Controls[0])).Text;
            if (combo.SelectedItem.Text == "Managed Fund /Unit Trust")
                dr[SecuritiesDS.CODE] = ((RadComboBox)(((GridEditableItem)(e.Item))["cmbCategoryCode"].Controls[0])).SelectedItem.Text;
            else
                dr[SecuritiesDS.CODE] = ((TextBox)(((GridEditableItem)(e.Item))["Code"].Controls[0])).Text;

            DateTime date;
            if (((RadDatePicker)(((GridEditableItem)(e.Item))["calDate"].Controls[0])).SelectedDate != null)
                if (DateTime.TryParse(((RadDatePicker)(((GridEditableItem)(e.Item))["calDate"].Controls[0])).SelectedDate.Value.ToString(), out date))
                    dr[SecuritiesDS.PRICEDATE] = date;

            dr[SecuritiesDS.PRICINGSOURCE] = ((RadComboBox)(((GridEditableItem)(e.Item))["cmbPricingSource"].Controls[0])).SelectedItem.Value;
            dr[SecuritiesDS.DISTRIBUTIONTYPE] = ((RadComboBox)(((GridEditableItem)(e.Item))["cmbDistributionType"].Controls[0])).SelectedItem.Value;
            dr[SecuritiesDS.DISTRIBUTIONFREQUENCY] = ((RadComboBox)(((GridEditableItem)(e.Item))["cmbDistributionFrequency"].Controls[0])).SelectedItem.Value;
            dr[SecuritiesDS.ISSMAAPPROVED] = ((CheckBox)(((GridEditableItem)(e.Item))["IsSMAApproved"].Controls[0])).Checked;
            var holdingLimit = ((RadNumericTextBox)(((GridEditableItem)(e.Item))["SMAHoldingLimit"].Controls[0])).Value;
            if (holdingLimit == null)
                holdingLimit = 0;
            dr[SecuritiesDS.SMAHOLDINGLIMIT] = holdingLimit;
        }

        public bool Validate(RadComboBox cmb)
        {
            bool result = true;
            if (string.IsNullOrEmpty(cmb.SelectedValue))
            {
                result = false;
                if (cmb.ID == "RCB_cmbAsset")
                    DisplayMessage("<font color=\"red\">Please Select Asset*</font>");
                if (cmb.ID == "RCB_cmbRating")
                    DisplayMessage("<font color=\"red\">Please Select Rating</font>");
                if (cmb.ID == "RCB_cmbStatus")
                    DisplayMessage("<font color=\"red\">Please Select Status</font>");
                if (cmb.ID == "RCB_cmbTransactionType")
                    DisplayMessage("<font color=\"red\">Please Select Investment Type*</font>");
            }
            return result;
        }

        protected void SecurityGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e)
        {

        }

        protected void SecurityGrid_ItemDeleted(object source, GridDeletedEventArgs e)
        {

        }

        protected void SecurityGrid_ItemInserted(object source, GridInsertedEventArgs e)
        {

        }

        protected void SecurityGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("Security".Equals(e.Item.OwnerTableView.Name))
            {
                SecuritiesDS securitiesDs = new SecuritiesDS();
                DataTable dt = securitiesDs.Tables[SecuritiesDS.SECLIST];
                DataRow dr = dt.NewRow();
                GridEditFormInsertItem EditForm = (GridEditFormInsertItem)e.Item;
                securitiesDs.SecID = Guid.NewGuid();
                dr[SecuritiesDS.ID] = securitiesDs.SecID;

                PopulateDataRow(dr, e);

                dt.Rows.Add(dr);
                securitiesDs.Tables.Remove(SecuritiesDS.SECLIST);
                securitiesDs.Tables.Add(dt);
                securitiesDs.CommandType = DatasetCommandTypes.Add;
                SaveOrganizanition(securitiesDs);
                e.Canceled = true;
                e.Item.Edit = false;
                this.SecurityGrid.Rebind();
            }
        }

        protected void SecurityGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
            {
                GridEditFormItem EditForm = (GridEditFormItem)e.Item;
                RadComboBox combo = (RadComboBox)EditForm["cmbTransactionType"].Controls[0];
                combo.AutoPostBack = true;
                combo.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(combo_SelectedIndexChanged);

                var txtSMAHoldingLimit = (RadNumericTextBox)EditForm["SMAHoldingLimit"].Controls[0];
                txtSMAHoldingLimit.MinValue = 0;
                txtSMAHoldingLimit.MaxValue = 100;

            }
        }

        private MISBasicDS FillSecurity()
        {
            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            MISBasicDS MIS = new MISBasicDS();
            MIS.Command = (int)WebCommands.GetOrganizationUnitsByType;
            MIS.Unit = new Oritax.TaxSimp.Data.OrganizationUnit
            {
                CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                Type = ((int)OrganizationType.ManagedInvestmentSchemesAccount).ToString()
            };
            org.GetData(MIS);
            return MIS;
        }

        void combo_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox cmb = ((RadComboBox)sender);

            GridEditableItem editableItem = (GridEditableItem)cmb.NamingContainer;
            GridEditManager editMan = editableItem.EditManager;

            RadComboBox comboBox = (RadComboBox)editableItem["cmbCategoryCode"].Controls[0];
            TextBox textBox = (TextBox)editableItem["Code"].Controls[0];



            if (cmb.SelectedItem.Text == "Managed Fund /Unit Trust")
            {
                comboBox.Enabled = true;
                textBox.Enabled = false;

                if (FillSecurity().MISBasicTable.Rows.Count > 0)
                {
                    ManagedInvestmentAccountDS ds = GetFundDataSet(new Guid(FillSecurity().Tables[FillSecurity().MISBasicTable.TABLENAME].Rows[0][FillSecurity().MISBasicTable.CID].ToString()));
                    FundAccountTable funds = ds.FundAccountsTable;
                    comboBox.DataSource = funds;
                    comboBox.DataTextField = funds.FUNDCODE;
                    comboBox.DataValueField = funds.FUNDID;
                    comboBox.DataBind();
                }
            }
            else
            {
                comboBox.Enabled = false;
                textBox.Enabled = true;
            }

        }

        private void DisplayMessage(string text)
        {
            SecurityGrid.Controls.Add(new LiteralControl(text));
        }

        private ManagedInvestmentAccountDS GetFundDataSet(Guid MISCid)
        {
            ManagedInvestmentAccountDS misDS = new ManagedInvestmentAccountDS();
            misDS.FundID = Guid.Empty;
            if (UMABroker != null)
            {
                IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(MISCid);
                misDS.CommandType = DatasetCommandTypes.Get;
                clientData.GetData(misDS);
                UMABroker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }
            return misDS;
        }

        protected void SecurityGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;
                IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                GridDropDownListColumnEditor AssetCombo = null;
                AssetCombo = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("cmbAsset"));
                AssetCombo.DataSource = organization.Assets;
                AssetCombo.DataTextField = "Description";
                AssetCombo.DataValueField = "ID";
                AssetCombo.DataBind();
                if (!(e.Item is IGridInsertItem))
                    AssetCombo.SelectedValue = ((DataRowView)e.Item.DataItem)["Asset"].ToString();
                GridDropDownListColumnEditor InvestmentTypeCombo = null;
                InvestmentTypeCombo = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("cmbTransactionType"));
                InvestmentTypeCombo.DataSource = InvestmentTypes;
                InvestmentTypeCombo.DataBind();
                if (!(e.Item is IGridInsertItem))
                {
                    string invType = ((DataRowView)e.Item.DataItem)["InvestmentType"].ToString();
                    if (invType == "Term Depose")
                        InvestmentTypeCombo.SelectedText = "Term Deposit";
                    else
                        InvestmentTypeCombo.SelectedText = invType;
                    if (InvestmentTypeCombo.SelectedText == "Managed Fund /Unit Trust")
                    {
                        ManagedInvestmentAccountDS ds = GetFundDataSet(new Guid(FillSecurity().Tables[FillSecurity().MISBasicTable.TABLENAME].Rows[0][FillSecurity().MISBasicTable.CID].ToString()));
                        FundAccountTable funds = ds.FundAccountsTable;
                        ((GridDropDownListColumnEditor)(editMan.GetColumnEditor("cmbCategoryCode"))).DataSource = funds;
                        ((GridDropDownListColumnEditor)(editMan.GetColumnEditor("cmbCategoryCode"))).DataTextField = funds.FUNDCODE;
                        ((GridDropDownListColumnEditor)(editMan.GetColumnEditor("cmbCategoryCode"))).DataValueField = funds.FUNDID;
                        ((GridDropDownListColumnEditor)(editMan.GetColumnEditor("cmbCategoryCode"))).DataBind();
                        ((GridDropDownListColumnEditor)(editMan.GetColumnEditor("cmbCategoryCode"))).Visible = true;
                        ((GridDropDownListColumnEditor)(editMan.GetColumnEditor("cmbCategoryCode"))).SelectedText = ((DataRowView)e.Item.DataItem)["Code"].ToString();
                        ((TextBox)(((GridEditableItem)(e.Item))["Code"].Controls[0])).Text = "";
                        ((TextBox)(((GridEditableItem)(e.Item))["Code"].Controls[0])).Enabled = false;
                    }
                    else
                    {
                        ((RadComboBox)(((GridEditableItem)(e.Item))["cmbCategoryCode"].Controls[0])).Enabled = false;
                    }
                }
                if (e.Item is IGridInsertItem)
                {
                    ((RadComboBox)(((GridEditableItem)(e.Item))["cmbCategoryCode"].Controls[0])).Enabled = false;
                }

                UMABroker.ReleaseBrokerManagedComponent(organization);
                GridDropDownListColumnEditor RatingCombo = null;
                RatingCombo = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("cmbRating"));
                RatingCombo.DataSource = RatingList;
                RatingCombo.DataBind();
                if (!(e.Item is IGridInsertItem))
                    RatingCombo.SelectedText = ((DataRowView)e.Item.DataItem)["Rating"].ToString();

                GridDropDownListColumnEditor StatusCombo = null;
                StatusCombo = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("cmbStatus"));
                StatusCombo.DataSource = Statuses;
                StatusCombo.DataBind();
                if (!(e.Item is IGridInsertItem))
                    StatusCombo.SelectedText = ((DataRowView)e.Item.DataItem)["Status"].ToString();
                ((TextBox)(((GridEditableItem)(e.Item))["txtMarket"].Controls[0])).Width = Unit.Pixel(250);
                ((TextBox)(((GridEditableItem)(e.Item))["Description"].Controls[0])).Width = Unit.Pixel(250);

                BindEnumerableCombo((GridDropDownListColumnEditor)editMan.GetColumnEditor("cmbPricingSource"), typeof(SecurityPricingSource), (GridEditableItem)e.Item, SecuritiesDS.PRICINGSOURCE);
                BindEnumerableCombo((GridDropDownListColumnEditor)editMan.GetColumnEditor("cmbDistributionType"), typeof(SecurityDistributionsType), (GridEditableItem)e.Item, SecuritiesDS.DISTRIBUTIONTYPE);
                BindEnumerableCombo((GridDropDownListColumnEditor)editMan.GetColumnEditor("cmbDistributionFrequency"), typeof(SecurityDistributionsFrequency), (GridEditableItem)e.Item, SecuritiesDS.DISTRIBUTIONFREQUENCY);

            }

        }

        private static void BindEnumerableCombo(GridDropDownListColumnEditor combo, Type enumType, GridEditableItem item, string columnName)
        {
            SortedList<int, string> enumeratedValues = new SortedList<int, string>();
            foreach (int value in Enum.GetValues(enumType))
            {
                enumeratedValues.Add(value, Enum.GetName(enumType, value));
            }

            combo.DataSource = enumeratedValues;
            combo.DataTextField = "Value";
            combo.DataValueField = "Key";
            combo.DataBind();
            combo.SelectedIndex = 0;

            if (item is IGridInsertItem)
                combo.SelectedIndex = 0;
            else
            {
                if (((DataRowView)item.DataItem)[columnName] is DBNull)
                    combo.SelectedIndex = 0;
                else
                    combo.SelectedValue = ((DataRowView)item.DataItem)[columnName].ToString();
            }
        }

        protected void SecurityGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            SecuritiesDS securityDS = new Oritax.TaxSimp.DataSets.SecuritiesDS();
            organization.GetData(securityDS);
            this.PresentationData = securityDS;
            DataView SecurityListView = new DataView(securityDS.Tables[SecuritiesDS.SECLIST]);
            SecurityListView.Sort = SecuritiesDS.DESCRIPTION + " ASC";
            this.SecurityGrid.DataSource = SecurityListView.ToTable();
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        protected void btnProcessAllDividend_Click(object sender, EventArgs e)
        {
            ProcessDividendForAllSecurities();
        }

        private void ProcessDividendForAllSecurities()
        {
            SecurityDividendDetailsDS dividendDetailsDs = new SecurityDividendDetailsDS();
            dividendDetailsDs.CommandType = DatasetCommandTypes.ProcessAll;
            this.SaveOrganizanition(dividendDetailsDs);
        }


        protected void btnProcessAllDistributions_Click(object sender, EventArgs e)
        {
            ProcessDistributionsForAllSecurities();
        }
        private void ProcessDistributionsForAllSecurities()
        {
            SecurityDistributionsDetailsDS distributionsDetailsDs = new SecurityDistributionsDetailsDS();
            distributionsDetailsDs.CommandType = DatasetCommandTypes.ProcessAll;
            this.SaveOrganizanition(distributionsDetailsDs);
        }


        protected void btnRemoveInvalidDistributions_Click(object sender, EventArgs e)
        {
            RemoveInvalidDistributionsForAllSecurities();
        }
        private void RemoveInvalidDistributionsForAllSecurities()
        {
            SecurityDistributionsDetailsDS distributionsDetailsDs = new SecurityDistributionsDetailsDS();
            distributionsDetailsDs.CommandType = DatasetCommandTypes.RemoveInvalid;
            this.SaveOrganizanition(distributionsDetailsDs);
        }

        protected void btnPaging_OnClick(object sender, EventArgs e)
        {
            switch (btnPaging.Text)
            {
                case "Open Bulk Edit Mode":
                    btnPaging.Text = "Close Bulk Edit Mode";
                    btnBulkUpdate.Visible = true;
                    SecurityGrid.AllowPaging = false;
                    SecurityGrid.Columns.FindByUniqueNameSafe("IsSMAApproved").Visible = false;
                    SecurityGrid.Columns.FindByUniqueNameSafe("SMAHoldingLimit").Visible = false;
                    SecurityGrid.Columns.FindByUniqueNameSafe("ChkIsSMAApproved").Visible = true;
                    SecurityGrid.Columns.FindByUniqueNameSafe("txtSMAHoldingLimit").Visible = true;
                    break;
                default:
                    btnPaging.Text = "Open Bulk Edit Mode";
                    btnBulkUpdate.Visible = false;
                    SecurityGrid.AllowPaging = true;
                    SecurityGrid.Columns.FindByUniqueNameSafe("IsSMAApproved").Visible = true;
                    SecurityGrid.Columns.FindByUniqueNameSafe("SMAHoldingLimit").Visible = true;
                    SecurityGrid.Columns.FindByUniqueNameSafe("ChkIsSMAApproved").Visible = false;
                    SecurityGrid.Columns.FindByUniqueNameSafe("txtSMAHoldingLimit").Visible = false;
                    break;
            }
            SecurityGrid.Rebind();
        }

        protected void btnDownload_OnClick(object sender, EventArgs e)
        {
            DataSet excelDataset = new DataSet();
            excelDataset.Merge(PresentationData.Tables[SecuritiesDS.SECLIST], true, MissingSchemaAction.Add);
            excelDataset.Tables[0].Columns.Remove("ID");
            ExcelHelper.ToExcel(excelDataset, "Securities-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", this.Page.Response);
        }

        protected void btnBulkUpdate_OnClick(object sender, EventArgs e)
        {
            var securitiesDs = new SecuritiesDS();
            foreach (GridDataItem dataItem in SecurityGrid.MasterTableView.Items)
            {
                var txtSMAHoldingLimit = (dataItem.FindControl("txtSMAHoldingLimit") as RadNumericTextBox);
                var chkSMAApproved = (dataItem.FindControl("chkSMAApproved") as CheckBox);
                if (txtSMAHoldingLimit != null && chkSMAApproved != null)
                {
                    var secId = new Guid(dataItem["ID"].Text);
                    DataRow dr = securitiesDs.Tables[SecuritiesDS.SECLIST].NewRow();
                    dr[SecuritiesDS.ID] = secId;
                    dr[SecuritiesDS.ISSMAAPPROVED] = chkSMAApproved.Checked;
                    dr[SecuritiesDS.SMAHOLDINGLIMIT] = txtSMAHoldingLimit.Value;
                    securitiesDs.Tables[SecuritiesDS.SECLIST].Rows.Add(dr);
                }
            }
            securitiesDs.CommandType = DatasetCommandTypes.UpdatedSMAProperties;
            SaveOrganizanition(securitiesDs);
            SecurityGrid.Rebind();
        }
    }
}
