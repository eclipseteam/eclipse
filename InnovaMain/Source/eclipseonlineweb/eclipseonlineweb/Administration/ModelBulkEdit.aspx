﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="SetupMaster.master"
    AutoEventWireup="true" CodeBehind="ModelBulkEdit.aspx.cs" Inherits="eclipseonlineweb.ModelBulkEdit" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript">
        //Resetting menu index
        localStorage['MenuIndex'] = '1';
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/3232/down.png" OnClick="DownloadXLS"
                                AlternateText="Download Model List" ToolTip="Download Model List" ID="btnDownload" />
                            <asp:ImageButton runat="server" Visible="false" ImageUrl="~/images/3232/process.png"
                                OnClick="ReCalculationAllocation" AlternateText="Re Calculation" ToolTip="Re Calculation"
                                ID="btnReCalculationAllocation" />
                            <asp:ImageButton runat="server" ImageUrl="~/images/3232/process.png" OnClick="FixModel"
                                AlternateText="Fix Model" ToolTip="Fix Model" ID="btnFixModel" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblModelName" Text="Financial Models"></asp:Label>
                            <asp:Label Font-Bold="true" Visible="false" runat="server" ID="lblModelID"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <asp:Panel runat="server" ID="pnlMainGrid">
                <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
                    runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="500"
                    AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
                    OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                    AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                    OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
                    OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
                    EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
                    OnItemDataBound="PresentationGrid_ItemDataBound">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="ID" EditMode="EditForms" AllowMultiColumnSorting="True"
                        Width="100%" CommandItemDisplay="Top" Name="Models" TableLayout="Fixed">
                        <CommandItemSettings ShowExportToExcelButton="true" AddNewRecordText="Add New Record"
                            ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                        <Columns>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridEditCommandColumn>
                            <telerik:GridBoundColumn FilterControlWidth="150px" Display="false" ReadOnly="true"
                                SortExpression="AssetID" HeaderStyle-Width="35%" ItemStyle-Width="30%" HeaderText="AssetID"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="AssetID" UniqueName="AssetID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn Display="false" FilterControlWidth="150px" ReadOnly="true"
                                SortExpression="ProID" HeaderStyle-Width="35%" ItemStyle-Width="30%" HeaderText="ProID"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ProID" UniqueName="ProID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="150px" ReadOnly="true" SortExpression="ModelName"
                                HeaderStyle-Width="12%" ItemStyle-Width="12%" HeaderText="Model Name" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ModelName" UniqueName="ModelName">
                            </telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn HeaderText="Models" SortExpression="ModelName" UniqueName="ModelDropDownBoxEditor"
                                DataField="ModelName" Visible="false" ColumnEditorID="ModelDropDownBox" />
                            <telerik:GridDropDownColumn HeaderText="Assets" SortExpression="AssetDescription"
                                UniqueName="AssetDescriptionEditor" DataField="AssetDescription" Visible="false"
                                ColumnEditorID="AssetLitingDropDownBox" />
                            <telerik:GridDropDownColumn HeaderText="Products" SortExpression="ProDescription"
                                UniqueName="ProDescriptionEditor" DataField="ProDescription" Visible="false"
                                ColumnEditorID="ProductListingDropDownBox" />
                            <telerik:GridBoundColumn FilterControlWidth="20px" HeaderStyle-Width="5%" ItemStyle-Width="5%"
                                SortExpression="ProgramCode" HeaderText="Code" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ProgramCode" UniqueName="ProgramCode"
                                ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="100px" ReadOnly="true" HeaderStyle-Width="15%"
                                ItemStyle-Width="15%" SortExpression="AssetDescription" HeaderText="Asset Description"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="AssetDescription" UniqueName="AssetDescription">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="100px" ReadOnly="true" HeaderStyle-Width="15%"
                                ItemStyle-Width="15%" SortExpression="ProDescription" HeaderText="Pro Description"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ProDescription" UniqueName="ProDescription">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="40px" Aggregate="Sum" ReadOnly="false"
                                HeaderStyle-Width="5%" DefaultInsertValue="0" ItemStyle-Width="5%" SortExpression="ProMinAllocation"
                                HeaderText="Min" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ProMinAllocation"
                                UniqueName="ProMinAllocation">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="40px" Aggregate="Sum" ReadOnly="false"
                                HeaderStyle-Width="5%" DefaultInsertValue="0" ItemStyle-Width="5%" SortExpression="ProDynamicPercentage"
                                HeaderText="Dynamic" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ProDynamicPercentage"
                                UniqueName="ProDynamicPercentage">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="40px" Aggregate="Sum" ReadOnly="false"
                                HeaderStyle-Width="5%" DefaultInsertValue="0" ItemStyle-Width="5%" SortExpression="ProNeutralPercentage"
                                HeaderText="Neutral" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ProNeutralPercentage"
                                UniqueName="ProNeutralPercentage">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="40px" Aggregate="Sum" ReadOnly="false"
                                HeaderStyle-Width="5%" DefaultInsertValue="0" ItemStyle-Width="5%" SortExpression="ProMaxAllocation"
                                HeaderText="Max" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ProMaxAllocation"
                                UniqueName="ProMaxAllocation">
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn CommandName="DelAsset" Text="Del Asset" ButtonType="LinkButton"
                                ItemStyle-Width="24px" ItemStyle-Height="24px" HeaderStyle-Width="6%" UniqueName="DelAsset">
                                <ItemStyle HorizontalAlign="Center" />
                            </telerik:GridButtonColumn>
                            <telerik:GridButtonColumn ConfirmText="Delete this product?" ButtonType="ImageButton"
                                CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                <HeaderStyle Width="6%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                    <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                        <Excel Format="Html"></Excel>
                    </ExportSettings>
                </telerik:RadGrid>
                <telerik:GridDropDownListColumnEditor ID="ModelDropDownBox" runat="server" DropDownStyle-Width="400px" />
                <telerik:GridDropDownListColumnEditor ID="AssetLitingDropDownBox" runat="server"
                    DropDownStyle-Width="400px" />
                <telerik:GridDropDownListColumnEditor ID="ProductListingDropDownBox" runat="server"
                    DropDownStyle-Width="400px" />
                <asp:Label ID="Label1" runat="server" EnableViewState="false" Visible="false"></asp:Label>
            </asp:Panel>
            <asp:Label ID="lblModelDetailID" runat="server" Visible="false"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
