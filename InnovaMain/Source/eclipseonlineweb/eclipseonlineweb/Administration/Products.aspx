﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="SetupMaster.master"
    AutoEventWireup="true" CodeBehind="Products.aspx.cs" Inherits="eclipseonlineweb.Products" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel>
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="4%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                                ID="btnDownload" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblSecurities" Text="Products"></asp:Label>
                            <asp:HiddenField ID="hfSecID" runat="server" />
                            <asp:HiddenField ID="hfIsAdmin" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
                OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
                OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
                OnItemDataBound="PresentationGrid_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="Products" TableLayout="Fixed" EditMode="PopUp">
                    <CommandItemSettings AddNewRecordText="Add New Product" ShowExportToExcelButton="true"
                        ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridEditCommandColumn>
                        <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ID" UniqueName="ID" />
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Name" HeaderStyle-Width="18%"
                            ItemStyle-Width="18%" HeaderText="Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Name" UniqueName="txtName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="28%" ItemStyle-Width="28%"
                            SortExpression="Description" HeaderText="Description" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Description" UniqueName="txtDescription">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn UniqueName="cmbProductType" HeaderText="Product Type"
                            DataField="ProductType" Visible="false" ColumnEditorID="CEProductType" />
                        <telerik:GridDropDownColumn UniqueName="cmbProductSecuritiesId" HeaderText="Product Security"
                            DataField="ProductSecuritiesId" Visible="false" ColumnEditorID="CEProductSecurities" />
                        <telerik:GridCheckBoxColumn HeaderText="Default" UniqueName="chkIsDef" DataField="IsDefaultProductSecurity" Visible=false />
                        <telerik:GridMaskedColumn HeaderText="Product APIR" Mask="aaaaaaaaa" UniqueName="ProductAPIRNo"
                            DataField="ProductAPIR" />
                        <telerik:GridMaskedColumn HeaderText="ProductISIN" Mask="aaaaaaaaaaaa" UniqueName="ProductISINNo"
                            DataField="ProductISIN" />
                        <telerik:GridDropDownColumn UniqueName="cmbOrgType" HeaderText="Type" DataField="EntityType"
                            Visible="false" ColumnEditorID="CEType" />
                            <telerik:GridBoundColumn FilterControlWidth="150px" ReadOnly="true" HeaderStyle-Width="15%"
                                ItemStyle-Width="15%" SortExpression="EntityType" HeaderText="Type"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="EntityType" UniqueName="EntityType">
                            </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn UniqueName="ComboInstitute" HeaderText="Financial Institute/Broker"
                            DataField="EntityId" Visible="false" ColumnEditorID="CEEntityId" />
                        <telerik:GridDropDownColumn UniqueName="ComboAccountCode" HeaderText="Account / Fund Code "
                            DataField="BankAccountType" Visible="false" ColumnEditorID="CEBankAccountType" />
                        <telerik:GridButtonColumn CommandName="Management" Text="Management Details" ButtonType="LinkButton"
                                HeaderStyle-Width="8%" ItemStyle-Width="8%" UniqueName="Management">
                            </telerik:GridButtonColumn>
                            <telerik:GridButtonColumn CommandName="Report" Text="Report" ButtonType="ImageButton"
                                ItemStyle-Width="24px" ItemStyle-Height="24px" ImageUrl="~/images/pie_chart.png"
                                HeaderStyle-Width="5%">
                                <ItemStyle HorizontalAlign="Center" />
                            </telerik:GridButtonColumn>
                        <telerik:GridButtonColumn ConfirmText="Are you sure you want to delete this record?"
                            ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="Html"></Excel>
                </ExportSettings>
            </telerik:RadGrid>
            <telerik:GridDropDownListColumnEditor ID="CEProductType" runat="server" DropDownStyle-Width="200px" />
            <telerik:GridDropDownListColumnEditor ID="CEProductSecurities" runat="server" DropDownStyle-Width="200px" />
            <telerik:GridDropDownListColumnEditor ID="CEType" runat="server" DropDownStyle-Width="200px" />
            <telerik:GridDropDownListColumnEditor ID="CEEntityId" runat="server" DropDownStyle-Width="200px" />
            <telerik:GridDropDownListColumnEditor ID="CEBankAccountType" runat="server" DropDownStyle-Width="200px" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
