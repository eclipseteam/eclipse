﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="SetupMaster.master"
    AutoEventWireup="true" CodeBehind="TDTransaction.aspx.cs" Inherits="eclipseonlineweb.TDTransaction" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="20%">
                        <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                            ID="btnDownload" />
                        <asp:ImageButton runat="server" ImageUrl="~/images/window_previous.png" Visible="false"
                            OnClick="BtnBackToListing" ID="btnBack" />
                    </td>
                    <td width="100%" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <asp:Label Font-Bold="true" runat="server" ID="lblTDTransactionListing" Text="TD RATES"></asp:Label>
                        <asp:Label Font-Bold="true" Visible="false" runat="server" ID="lblInstituteID"></asp:Label>
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <asp:Panel runat="server" ID="InstitutionGrid">
        <telerik:RadGrid EnableViewState="true" ID="radGrid1" AutoGenerateColumns="false"
            OnItemCommand="InstituteGridItemCommand" Width="100%" runat="server" AutoGenerateDeleteColumn="false"
            AutoGenerateEditColumn="false" PageSize="20" OnNeedDataSource="GetInstituteData"
            AllowPaging="true">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowMultiColumnSorting="True" CommandItemDisplay="Top" TableLayout="Fixed"
                EditMode="PopUp" EnableViewState="true" AllowFilteringByColumn="true" AllowSorting="true">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false"
                    ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                <Columns>
                    <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                        SortExpression="ID" HeaderText="ID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ID" UniqueName="ID"
                        ReadOnly="true" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                        SortExpression="InstitutionCode" HeaderText="Institution Code" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="TextInstitutionCode" UniqueName="InstitutionCode" ReadOnly="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                        SortExpression="Name" HeaderText="Institute Name" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="Name" UniqueName="Name" ReadOnly="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                        SortExpression="Description" HeaderText="Description" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="Description" UniqueName="Description" ReadOnly="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridButtonColumn CommandName="Details" Text="Details" ButtonType="LinkButton"
                        HeaderStyle-Width="10%" ItemStyle-Width="10%">
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
            <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                <Excel Format="Html"></Excel>
            </ExportSettings>
        </telerik:RadGrid>
    </asp:Panel>
    <asp:Panel runat="server" ScrollBars="Horizontal" ID="ScrollPanel" Visible="false"
        Width="100%">
        <telerik:RadGrid EnableViewState="true" ID="radGrid" AutoGenerateColumns="false"
            Width="180%" runat="server" AutoGenerateDeleteColumn="false" AutoGenerateEditColumn="false"
            PageSize="20" OnNeedDataSource="GetData" OnItemCommand="radGrid_OnItemCommand"
            AllowPaging="true">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView DataKeyNames="tdPrice.ID" AllowMultiColumnSorting="True" CommandItemDisplay="Top"
                TableLayout="Fixed" EditMode="EditForms" EnableViewState="true" AllowFilteringByColumn="true"
                AllowSorting="true">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false"
                    ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                <Columns>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditColumn">
                        <HeaderStyle Width="3%"></HeaderStyle>
                        <ItemStyle CssClass="MyImageButton"></ItemStyle>
                    </telerik:GridEditCommandColumn>
                    <telerik:GridBoundColumn FilterControlWidth="120px" HeaderStyle-Width="8%" ItemStyle-Width="8%"
                        HeaderText="Rate ID" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="tdPrice.ImportRateID" ReadOnly="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="120px" HeaderStyle-Width="8%" ItemStyle-Width="8%"
                        HeaderText="Provider ID" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="tdPrice.ProviderID" ReadOnly="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="120px" HeaderStyle-Width="8%" ItemStyle-Width="8%"
                        SortExpression="InstituteName" HeaderText="Institute Name" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="InstituteName" UniqueName="InstituteName" ReadOnly="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridDateTimeColumn UniqueName="Date" PickerType="DatePicker" HeaderStyle-Width="8%"
                        CurrentFilterFunction="EqualTo" ItemStyle-Width="8%" HeaderText="Date" DataField="tdPrice.Date"
                        ReadOnly="true" DataFormatString="{0:dd/MM/yyyy}">
                    </telerik:GridDateTimeColumn>
                    <telerik:GridBoundColumn FilterControlWidth="40px" SortExpression="tdPrice.Min" HeaderText="Min"
                        HeaderStyle-Width="5%" ItemStyle-Width="5%" AutoPostBackOnFilter="true" CurrentFilterFunction="EqualTo"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="tdPrice.Min" UniqueName="Min"
                        DataFormatString="{0:$###,###,###}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="40px" SortExpression="tdPrice.Max" HeaderText="Max"
                        HeaderStyle-Width="5%" ItemStyle-Width="5%" AutoPostBackOnFilter="true" CurrentFilterFunction="EqualTo"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="tdPrice.Max" UniqueName="Max"
                        DataFormatString="{0:$###,###,###}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="30px" SortExpression="tdPrice.MaximumBrokeage"
                        HeaderText="Maximum Brokeage" AutoPostBackOnFilter="true" HeaderStyle-Width="5%"
                        ItemStyle-Width="5%" CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="tdPrice.MaximumBrokeage" UniqueName="MaximumBrokeage" DataFormatString="{0:###,##0.00}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="30px" SortExpression="tdPrice.OnGoing"
                        HeaderText="On Going" HeaderStyle-Width="5%" ItemStyle-Width="5%" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="tdPrice.OnGoing" UniqueName="OnGoing" DataFormatString="{0:###,##0.00}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="30px" SortExpression="tdPrice.Days30"
                        HeaderText="30 Days" HeaderStyle-Width="5%" ItemStyle-Width="5%" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="tdPrice.Days30" UniqueName="Days30" DataFormatString="{0:##0.00}%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="30px" SortExpression="tdPrice.Days60"
                        HeaderText="60 Days" HeaderStyle-Width="5%" ItemStyle-Width="5%" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="tdPrice.Days60" UniqueName="Days60" DataFormatString="{0:##0.00}%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="30px" SortExpression="tdPrice.Days90"
                        HeaderText="90 Days" HeaderStyle-Width="5%" ItemStyle-Width="5%" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="tdPrice.Days90" UniqueName="Days90" DataFormatString="{0:##0.00}%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="30px" SortExpression="tdPrice.Days120"
                        HeaderText="120 Days" HeaderStyle-Width="5%" ItemStyle-Width="5%" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="tdPrice.Days120" UniqueName="Days120" DataFormatString="{0:##0.00}%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="30px" SortExpression="tdPrice.Days150"
                        HeaderText="150 Days" HeaderStyle-Width="5%" ItemStyle-Width="5%" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="tdPrice.Days150" UniqueName="Days150" DataFormatString="{0:##0.00}%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="30px" SortExpression="tdPrice.Days180"
                        HeaderText="180 Days" HeaderStyle-Width="5%" ItemStyle-Width="5%" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="tdPrice.Days180" UniqueName="Days180" DataFormatString="{0:##0.00}%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="30px" SortExpression="tdPrice.Days270"
                        HeaderText="270 Days" HeaderStyle-Width="5%" ItemStyle-Width="5%" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="tdPrice.Days270" UniqueName="Days270" DataFormatString="{0:##0.00}%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="30px" SortExpression="tdPrice.Years1"
                        HeaderText="1 Years" HeaderStyle-Width="5%" ItemStyle-Width="5%" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="tdPrice.Years1" UniqueName="Years1" DataFormatString="{0:##0.00}%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="30px" SortExpression="tdPrice.Years2"
                        HeaderText="2 Years" HeaderStyle-Width="5%" ItemStyle-Width="5%" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="tdPrice.Years2" UniqueName="Years2" DataFormatString="{0:##0.00}%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="30px" SortExpression="tdPrice.Years3"
                        HeaderText="3 Years" HeaderStyle-Width="5%" ItemStyle-Width="5%" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="tdPrice.Years3" UniqueName="Years3" DataFormatString="{0:##0.00}%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="30px" SortExpression="tdPrice.Years4"
                        HeaderText="4 Years" HeaderStyle-Width="5%" ItemStyle-Width="5%" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="tdPrice.Years4" UniqueName="Years4" DataFormatString="{0:##0.00}%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="30px" SortExpression="tdPrice.Years5"
                        HeaderText="5 Years" HeaderStyle-Width="5%" ItemStyle-Width="5%" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="tdPrice.Years5" UniqueName="Years5" DataFormatString="{0:##0.00}%">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" SortExpression="tdPrice.Status"
                        HeaderText="Status" AutoPostBackOnFilter="true" HeaderStyle-Width="120px" ItemStyle-Width="120px"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="tdPrice.Status" UniqueName="Status" ReadOnly="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" SortExpression="tdPrice.RateEffectiveFor"
                        HeaderText="Rate Effective For" AutoPostBackOnFilter="true" HeaderStyle-Width="120px"
                        ItemStyle-Width="120px" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="tdPrice.RateEffectiveFor" UniqueName="RateEffectiveFor"
                        ReadOnly="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridButtonColumn  ConfirmText="Are you sure you want to delete it?" ButtonType="ImageButton"
                        CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                        <HeaderStyle Width="25px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
            <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                <Excel Format="Html"></Excel>
            </ExportSettings>
        </telerik:RadGrid>
        <asp:HiddenField ID="hdnPriceID" runat="server" EnableViewState="true" />
    </asp:Panel>
</asp:Content>
