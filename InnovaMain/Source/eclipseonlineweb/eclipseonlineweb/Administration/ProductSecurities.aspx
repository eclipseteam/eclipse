﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="SetupMaster.master"
    AutoEventWireup="true" CodeBehind="ProductSecurities.aspx.cs" Inherits="eclipseonlineweb.ProductSecurities" %>

<%@ Register TagPrefix="uc1" TagName="breadcrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExcel" />
            <asp:PostBackTrigger ControlID="btnPDF" />
        </Triggers>
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 40%;">
                            <telerik:RadButton ID="btnBack" runat="server" ToolTip="Back" OnClick="btnBack_OnClick" Visible="False">
                                <ContentTemplate>
                                    <img src="../images/window_previous.png" alt="" class="btnImageWithText" />
                                    <span class="riLabel">Back</span>
                                </ContentTemplate>
                            </telerik:RadButton>
                            <telerik:RadButton runat="server" ID="btnExcel" ToolTip="Export as Excel" OnClick="btnExcel_OnClick">
                                <ContentTemplate>
                                    <img src="../images/export_excel.png" alt="" class="btnImageWithText" />
                                    <span class="riLabel">Export</span>
                                </ContentTemplate>
                            </telerik:RadButton>
                            <telerik:RadButton runat="server" ID="btnPDF" ToolTip="Export as PDF" OnClick="btnPDF_OnClick">
                                <ContentTemplate>
                                    <img src="../images/pdf.png" alt="" class="btnImageWithText" />
                                    <span class="riLabel">Export</span>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </td>
                        <td style="width: 60%;" class="breadcrumbgap">
                            <uc1:breadcrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblProSecurities" Text="Product Securities"></asp:Label>
                            <asp:Label Font-Bold="true" Visible="false" runat="server" ID="lblProSecuritiesID"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <asp:Panel runat="server" ID="pnlMainGrid" Visible="true">
                <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                    PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                    OnInsertCommand="PresentationGrid_InsertCommand" GridLines="None" AllowAutomaticDeletes="True"
                    OnItemDataBound="PresentationGrid_ItemDataBound" AllowFilteringByColumn="true"
                    AllowAutomaticInserts="True" AllowAutomaticUpdates="True" EnableViewState="true"
                    ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource" OnItemCreated="PresentationGrid_OnItemCreated"
                    OnItemCommand="PresentationGrid_OnItemCommand">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                        Name="ProductSecurities" TableLayout="Fixed">
                        <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add New Product Security">
                        </CommandItemSettings>
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridEditCommandColumn>
                            <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ID" UniqueName="ID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="80%" HeaderStyle-Width="22%" SortExpression="Name"
                                ReadOnly="false" HeaderText="Product Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Name" UniqueName="Name">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="80%" HeaderStyle-Width="70%" SortExpression="Description"
                                ItemStyle-Width="300px" ReadOnly="false" HeaderText="Description" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="Description" UniqueName="Description">
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn ButtonType="LinkButton" CommandName="Details" Text="Details"
                                UniqueName="DetailColumn">
                                <HeaderStyle Width="8%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyLinkButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                            <telerik:GridButtonColumn ConfirmText="Delete these details record?" ButtonType="ImageButton"
                                CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlDetails" Visible="false">
                <telerik:RadGrid ID="PresentationDetailGrid" runat="server" ShowStatusBar="true"
                    AutoGenerateColumns="False" PageSize="20" AllowSorting="True" AllowMultiRowSelection="False"
                    OnInsertCommand="PresentationGrid_InsertCommand" OnItemCommand="PresentationGrid_OnItemCommand"
                    AllowPaging="True" GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="false"
                    OnItemDataBound="PresentationGrid_ItemDataBound" AllowAutomaticInserts="True"
                    OnItemCreated="PresentationDetailGrid_OnItemCreated" AllowAutomaticUpdates="True"
                    EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationDetailGrid_OnNeedDataSource">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                        Name="ProductSecuritiesDetails" TableLayout="Fixed">
                        <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add New Security Record">
                        </CommandItemSettings>
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridEditCommandColumn>
                            <telerik:GridBoundColumn SortExpression="Name" ReadOnly="true" HeaderText="ASX Code"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="Name" UniqueName="Name">
                                <HeaderStyle Width="8%"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="Description" ReadOnly="true" HeaderText="Description"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="Description" UniqueName="Description">
                            </telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn HeaderText="Sec List" Visible="false" SortExpression="Name"
                                UniqueName="DDLSecList" DataField="Name" ColumnEditorID="GridDropDownListColumnDDLSecList" />
                            <telerik:GridNumericColumn SortExpression="BuyPrice" ReadOnly="false" HeaderText="Buy Price"
                                DefaultInsertValue="0" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BuyPrice" UniqueName="BuyPrice"
                                Visible="true">
                                <HeaderStyle Width="8%"></HeaderStyle>
                            </telerik:GridNumericColumn>
                            <telerik:GridNumericColumn SortExpression="SellPrice" ReadOnly="false" HeaderText="Sell Price"
                                DefaultInsertValue="99.99" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="SellPrice" UniqueName="SellPrice"
                                Visible="true">
                                <HeaderStyle Width="8%"></HeaderStyle>
                            </telerik:GridNumericColumn>
                            <telerik:GridCheckBoxColumn SortExpression="IsDeactive" ReadOnly="false" HeaderText="Is Deactive"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="IsDeactive" UniqueName="IsDeactive">
                                <HeaderStyle Width="8%"></HeaderStyle>
                            </telerik:GridCheckBoxColumn>
                            <telerik:GridNumericColumn SortExpression="Allocation" ReadOnly="false" HeaderText="Allocation"
                                DefaultInsertValue="0" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Allocation" UniqueName="Allocation">
                                <HeaderStyle Width="8%"></HeaderStyle>
                            </telerik:GridNumericColumn>
                            <telerik:GridBoundColumn SortExpression="Rating" ReadOnly="true" HeaderText="Rating"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="Rating" UniqueName="Rating">
                                <HeaderStyle Width="10%"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="RatingOptions" ReadOnly="true" HeaderText="Rating Options"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="RatingOptions" UniqueName="RatingOptions">
                                <HeaderStyle Width="10%"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="Description" Visible="false" HeaderText="Error Message"
                                DataField="Description" ColumnEditorID="GridErrorMessage" UniqueName="ErrorMessage">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <telerik:GridDropDownListColumnEditor ID="GridDropDownListColumnDDLSecList" runat="server"
                    DropDownStyle-Width="400px" />
                <telerik:GridTextBoxColumnEditor ID="GridErrorMessage" TextBoxStyle-Width="400px"
                    TextBoxStyle-ForeColor="Red" runat="server">
                </telerik:GridTextBoxColumnEditor>
            </asp:Panel>
            <telerik:RadWindowManager ID="RadWindowManagerAlert" runat="server" EnableShadow="true">
            </telerik:RadWindowManager>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
