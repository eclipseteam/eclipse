﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HistoricalPriceControl.ascx.cs"
    Inherits="eclipseonlineweb.Administration.Controls.HistoricalPriceControl" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<div class="popup_Container">

    <div class="popup_Titlebar" id="PopupHeader">
        <div class="TitlebarLeft">
            <asp:Label ID="Title" runat="server" Text="Historical Price Details"></asp:Label>
        </div>
    </div>
    <div class="popup_Body">
    <asp:HiddenField runat="server" ID="txtID"/>
    <asp:HiddenField runat="server" ID="txtSecID"/>
        <table>
            <tr>
                <td>
                    <strong>Date:</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtDate" runat="server" ReadOnly="false" />
                    <ajaxToolkit:CalendarExtender runat="server" ID="calDate" TargetControlID="txtDate"
                        Format="dd/MM/yyyy " />
                </td>
                <td>
                    <strong>Unit Price:</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtUnitPrice" CssClass="numeric" runat="server" Width="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Literal runat="server" ID="lblPriceNAV" ><strong>Price (NAV):</strong></asp:Literal>  
                </td>
                <td>
                    <asp:TextBox ID="txtPriceNAV" CssClass="numeric" runat="server" Width="100"></asp:TextBox>
                </td>
                <td>
                    <asp:Literal runat="server" ID="lblPricePUR" ><strong>Price (PUR):</strong></asp:Literal> 
                </td>
                <td>
                    <asp:TextBox ID="txtPricePUR" CssClass="numeric" runat="server" Width="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Adjustment Price:</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtAdjustmentPrice" CssClass="numeric" runat="server" Width="100"></asp:TextBox>
                </td>
                <td>
                    <strong>Interest Rate:</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtInterestRate"  runat="server" Width="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Currency:</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtCurrency" runat="server" Width="100"></asp:TextBox>
                </td>
                <td colspan="2">
                
                </td>
            </tr>

                <tr>
                <td>
                </td>
               
                <td colspan="2">
                    <asp:Label ID="lblMessages" runat="server" Text="" ForeColor="#990000" Font-Bold="True"></asp:Label>
                </td> <td>
                </td>
            </tr>
            <tr>
                <td>
                    
                </td>
                <td>
                    
                </td>
                <td colspan="2">
                <asp:Button ID="btnUpdate" runat="server"  Text="Save" OnClick="btnUpdate_OnClick" />
                 <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_OnClick" />
                </td>
            </tr>

        </table> 
        <script src="../Scripts/jquery.numeric.js" type="text/javascript"></script>
        <script language="javascript">
            var prm = Sys.WebForms.PageRequestManager.getInstance();

            prm.add_endRequest(function () {
                // re-bind your jQuery events here
                $(".numeric").numeric();
            });

           
        
        </script>
    </div>
</div>
