﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;
using System.Data;

namespace eclipseonlineweb.Administration.Controls
{
    public partial class HistoricalPriceControl : System.Web.UI.UserControl
    {
        public Action<Guid> Saved
        {
            get;
            set;
        }
        public Action<DataSet> SaveData
        {
            get;
            set;
        }
        public Action Canceled { get; set; }
        public Action ValidationFailed { get; set; }
        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private SecurityDetailsDS GetEntity()
        {
            double temp = 0;
            SecurityDetailsDS securitiesDS = new SecurityDetailsDS();
            securitiesDS.RequiredDataType = SecurityDetailTypes.HistoricalPrices;


            securitiesDS.DetailsGUID = Guid.Parse(txtSecID.Value);

            var id = new Guid(txtID.Value);
            if (id == Guid.Empty)
            {
                securitiesDS.CommandType = DatasetCommandTypes.Add;
            }
            else
            {
                securitiesDS.CommandType = DatasetCommandTypes.Update;
            }


            DataTable secHisDetails = securitiesDS.Tables[securitiesDS.PriceTable.TB_Name];
            DataRow row = secHisDetails.NewRow();
            row[securitiesDS.PriceTable.ID] = txtID.Value;
            row[securitiesDS.PriceTable.SECID] = txtSecID.Value;

            row[securitiesDS.PriceTable.DATE] = calDate.SelectedDate;
            row[securitiesDS.PriceTable.CURRENCY] = txtCurrency.Text;
            row[securitiesDS.PriceTable.UNITPRICE] = Convert.ToDouble(txtUnitPrice.Text);

            if (double.TryParse(txtAdjustmentPrice.Text, out temp))
                row[securitiesDS.PriceTable.ADJUSTEDPRICE] = temp;

            if (double.TryParse(txtInterestRate.Text, out temp))
                row[securitiesDS.PriceTable.INTERESTRATE] = temp;

            if (double.TryParse(txtPriceNAV.Text, out temp))
                row[securitiesDS.PriceTable.PRICENAV] = temp;

            if (double.TryParse(txtPricePUR.Text, out temp))
                row[securitiesDS.PriceTable.PRICEPUR] = temp;

            secHisDetails.Rows.Add(row);
            return securitiesDS;
        }
        public void ClearEntity()
        {
            txtID.Value = Guid.Empty.ToString();
            if (Request.Params["ID"] != null)
            {
                txtSecID.Value = Request.Params["ID"];

            }

            txtDate.Text = string.Empty;
            txtUnitPrice.Text = string.Empty;
            txtAdjustmentPrice.Text = string.Empty;
            txtInterestRate.Text = string.Empty;
            txtCurrency.Text = "AUD";
            txtPriceNAV.Text = txtPricePUR.Text = string.Empty;
        }
        public bool Validate()
        {
            bool result = true;
            bool tempResult = false;
            lblMessages.Text = "";
            StringBuilder Messages = new StringBuilder();

            DateTime date;
            tempResult = txtDate.ValidateDate(true, "dd/MM/yyyy", out date);
            if (tempResult)
            {
                calDate.SelectedDate = date;
            }
            else
            {
                calDate.SelectedDate = null;
            }

            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Price Date*", Messages);

            tempResult = txtUnitPrice.ValidateDecimal(true);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Unit Price.", Messages);



            tempResult = txtAdjustmentPrice.ValidateDecimal(false);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Adjustment Price.", Messages);


            tempResult = txtInterestRate.ValidateDecimal(false);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Interest Rate. ", Messages);

            tempResult = txtCurrency.ValidateCharacters(1);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Currency. ", Messages);


            tempResult = txtPriceNAV.ValidateDecimal(false);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Price NAV.", Messages);

            tempResult = txtPricePUR.ValidateDecimal(false);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Price PUR.", Messages);


            lblMessages.Text = Messages.ToString();

            return result;
        }
        private void AddMessageIn(bool result, string message, StringBuilder Messages)
        {
            if (!result)
            {
                Messages.Append(message + "<br/>");
            }
        }
        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            if (Validate() == false)
            {
                if (ValidationFailed != null)
                    ValidationFailed();
                return;
            }

            var id = UpdateData();
            if (id != Guid.Empty && Saved != null)
            {
                Saved(id);
            }
        }

        private Guid UpdateData()
        {
            SecurityDetailsDS ds = GetEntity();
            Guid id = Guid.Empty;
            if (Broker != null)
            {

                this.Broker.SaveOverride = true;

                if (SaveData != null)
                {
                    SaveData(ds);

                    if (ds.ExtendedProperties["Result"].ToString() == OperationResults.Failed.ToString())
                    {
                        lblMessages.Text = ds.ExtendedProperties["Message"].ToString();
                        if (ValidationFailed != null)
                        {
                            ValidationFailed();
                        }
                    }
                    else
                    {
                        id = (Guid)ds.Tables[ds.PriceTable.TB_Name].Rows[0][ds.PriceTable.ID];
                    }
                }


            }
            else
            {
                throw new Exception("Broker Not Found");
            }
            return id;
        }
        protected void btnCancel_OnClick(object sender, EventArgs e)
        {

        }

        public void ShowFundControls(bool visible)
        {
            lblPriceNAV.Visible = txtPriceNAV.Visible = lblPricePUR.Visible = txtPricePUR.Visible = visible;
        }
    }
}