﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="SetupMaster.master"
    AutoEventWireup="true" CodeBehind="BusinessGroups.aspx.cs" Inherits="eclipseonlineweb.BusinessGroups" %>
<%@ Register TagPrefix="uc1" TagName="breadcrumb" Src="~/Controls/BreadCrumb.ascx" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="4%">
                        &nbsp;
                    </td>
                    <td width="100%" class="breadcrumbgap">
                        <uc1:breadcrumb id="BreadCrumb1" runat="server" />
                        <br />
                        <asp:Label Font-Bold="true" runat="server" ID="lblBusinessGroups" Text="Business Groups"></asp:Label>
                        <asp:HiddenField ID="hfSecID" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
        runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
        AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
        OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
        AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
        OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
        OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
        EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
        OnItemDataBound="PresentationGrid_ItemDataBound">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
            Name="BusinessGroups" TableLayout="Fixed" EditMode="PopUp">
            <CommandItemSettings AddNewRecordText="Add New Business Group" ShowExportToExcelButton="true"
                ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
            <Columns>
                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                    <HeaderStyle Width="5%"></HeaderStyle>
                    <ItemStyle CssClass="MyImageButton"></ItemStyle>
                </telerik:GridEditCommandColumn>
                <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ID" UniqueName="ID" />
                <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Name" HeaderStyle-Width="20%"
                    ItemStyle-Width="20%" HeaderText="Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Name" UniqueName="txtName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="35%" ItemStyle-Width="35%"
                    SortExpression="Description" HeaderText="Description" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="Description" UniqueName="txtDescription">
                </telerik:GridBoundColumn>
                <telerik:GridButtonColumn ConfirmText="Delete these details record?" ButtonType="ImageButton"
                    CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                    <HeaderStyle Width="5%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                </telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
        <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
            <Excel Format="Html"></Excel>
        </ExportSettings>
    </telerik:RadGrid>
</asp:Content>
