﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;

namespace eclipseonlineweb
{
    public partial class ModelBulkEdit : UMABasePage
    {
        protected override void Intialise()
        {
            base.Intialise();
        }

        protected override void GetBMCData()
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            ModelsDS modelsDS = new Oritax.TaxSimp.DataSets.ModelsDS();
            modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.ModelExport;
            organization.GetData(modelsDS);
            this.PresentationData = modelsDS;

            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        public override void LoadPage()
        {
            base.LoadPage();

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
            {
                ((SetupMaster)Master).IsAdmin = "1";
            }
            //Only Admin User can Fix, Update Client Model and Add/Edit/Delete
            btnFixModel.Visible = objUser.Administrator;
            PresentationGrid.MasterTableView.CommandItemSettings.ShowAddNewRecordButton = objUser.Administrator;
            PresentationGrid.Columns.FindByUniqueNameSafe("DelAsset").Visible = PresentationGrid.Columns.FindByUniqueNameSafe("DeleteColumn").Visible = PresentationGrid.Columns.FindByUniqueNameSafe("EditCommandColumn2").Visible = objUser.Administrator;
        }

        public override void PopulatePage(DataSet ds)
        {
            ScriptManager sm = ScriptManager.GetCurrent(Page);
        }

        protected void ReCalculationAllocation(object sender, ImageClickEventArgs e)
        {
            ModelsDS modelsDS = new Oritax.TaxSimp.DataSets.ModelsDS();
            modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.ReCalculationAllocation;
            this.SaveOrganizanition(modelsDS);
        }
            
        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            if (pnlMainGrid.Visible)
            {
                IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

                ModelsDS modelsDS = new Oritax.TaxSimp.DataSets.ModelsDS();
                modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.ModelExport;
                organization.GetData(modelsDS);

                UMABroker.ReleaseBrokerManagedComponent(organization);

                DataSet excelDataset = new DataSet();
                excelDataset.Merge(modelsDS.Tables[ModelsDS.MODELLISTTABLEASSETPRODUCTS], true, MissingSchemaAction.Add);
                excelDataset.Tables[0].Columns.Remove("ID");
                excelDataset.Tables[0].Columns.Remove(ModelsDS.PROID);
                excelDataset.Tables[0].Columns.Remove(ModelsDS.ASSETID);
                ExcelHelper.ToExcel(excelDataset, "Models-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", this.Page.Response);
            }
        }

        protected void FixModel(object sender, ImageClickEventArgs e)
        {
            UMABroker.SaveOverride = true;
            
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            ModelsDS modelsDS = new Oritax.TaxSimp.DataSets.ModelsDS();
            modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.BulkUpdate;
            organization.SetData(modelsDS);
            UMABroker.SetComplete();
            UMABroker.SetStart();
        }

        #region Telerik Code

        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e)
        {

        }

        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {

        }

        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;
                GridDropDownListColumnEditor modelEditor = null;
                GridDropDownListColumnEditor assEditor = null;
                GridDropDownListColumnEditor proEditor = null;

                if ("Models".Equals(e.Item.OwnerTableView.Name))
                {
                   IOrganization orgCM = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

                    IDictionary<Enum, string> servicesValue = Enumeration.ToDictionary(typeof(ServiceTypes));

                    modelEditor = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("ModelDropDownBoxEditor"));
                    modelEditor.ComboBoxControl.Filter = RadComboBoxFilter.Contains;
                    modelEditor.DataSource = orgCM.Model.OrderBy(obj => obj.Name);
                    modelEditor.DataValueField = "ID";
                    modelEditor.DataTextField = "Name";
                    modelEditor.DataBind();

                    assEditor = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("AssetDescriptionEditor"));
                    assEditor.ComboBoxControl.Filter = RadComboBoxFilter.Contains;
                    assEditor.DataSource = orgCM.Assets.OrderBy(obj => obj.Name);
                    assEditor.DataValueField = "ID";
                    assEditor.DataTextField = "Description";
                    assEditor.DataBind();

                    proEditor = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("ProDescriptionEditor"));
                    proEditor.ComboBoxControl.Filter = RadComboBoxFilter.Contains;
                    proEditor.DataSource = orgCM.Products.OrderBy(obj => obj.Name);
                    proEditor.DataValueField = "ID";
                    proEditor.DataTextField = "Description";
                    proEditor.DataBind();

                    if (!(e.Item.DataItem is GridInsertionObject))
                    {
                        assEditor.DropDownListControl.Enabled = false;
                        modelEditor.DropDownListControl.Enabled = false;
                        proEditor.DropDownListControl.Enabled = false;

                        string value = ((System.Data.DataRowView)(e.Item.DataItem)).Row["ModelName"].ToString();
                        if (value != string.Empty)
                        {
                            value = ((System.Data.DataRowView)(e.Item.DataItem)).Row["ID"].ToString();
                            RadComboBoxItem item = modelEditor.ComboBoxControl.FindItemByValue(value);
                            if (item != null)
                                item.Selected = true;
                        }

                        value = ((System.Data.DataRowView)(e.Item.DataItem)).Row["AssetDescription"].ToString();
                        if (value != string.Empty)
                        {
                            value = ((System.Data.DataRowView)(e.Item.DataItem)).Row["ID"].ToString();
                            RadComboBoxItem item = assEditor.ComboBoxControl.FindItemByValue(value);
                            if (item != null)
                                item.Selected = true;
                        }

                        value = ((System.Data.DataRowView)(e.Item.DataItem)).Row["ProDescription"].ToString();
                        if (value != string.Empty)
                        {
                            value = ((System.Data.DataRowView)(e.Item.DataItem)).Row["ID"].ToString();
                            RadComboBoxItem item = proEditor.ComboBoxControl.FindItemByValue(value);
                            if (item != null)
                                item.Selected = true;
                        }
                    }
                }
            }
            e.Canceled = true;
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetBMCData();
            DataView ModelListView = new DataView(this.PresentationData.Tables[ModelsDS.MODELLISTTABLEASSETPRODUCTS]);
            ModelListView.Sort = ModelsDS.MODELNAME + " ASC";
            this.PresentationGrid.DataSource = ModelListView.ToTable();
        }

        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {

            if (e.CommandName.ToLower() == "delasset")
            {
                GridDataItem gdItem = (e.Item as GridDataItem);

                Guid selectedassetID = new Guid(gdItem["AssetID"].Text);
                Guid selectedModelID = (Guid)e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"];
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                ModelsDS modelsDS = new ModelsDS();
                modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.DeleteAsset;
                modelsDS.ModelEntityID = selectedModelID;
                modelsDS.AssetEntityID = selectedassetID;

                iorg.SetData(modelsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Item.Edit = false;

                PresentationGrid.Rebind();
            }

            if (e.CommandName.ToLower() == "delete")
            {
                string parentID = string.Empty;
                GridDataItem gdItem = (e.Item as GridDataItem);

                Guid selectedassetID = new Guid(gdItem["AssetID"].Text);
                Guid selectedProductID = new Guid(gdItem["ProID"].Text);
                Guid selectedModelID = (Guid)e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"];
                
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                ModelsDS modelsDS = new ModelsDS();
                modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.DeleteProduct;
                modelsDS.ModelEntityID = selectedModelID;
                modelsDS.AssetEntityID = selectedassetID;
                modelsDS.ProductEntityID = selectedProductID;

                iorg.SetData(modelsDS);

                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Item.Edit = false;

                PresentationGrid.Rebind();
            }

            if (e.CommandName.ToLower() == "update")
            {
                GridDataItem gdItem = (e.Item as GridDataItem);


                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
              
                ModelsDS modelsDS = new ModelsDS();

                GridEditFormItem editForm = (GridEditFormItem)e.Item;

                Hashtable values = new Hashtable();
                editForm.ExtractValues(values);

                TextBox assetIDControl = (TextBox)editForm["AssetID"].Controls[0];
                TextBox proIDControl = (TextBox)editForm["ProID"].Controls[0];
                TextBox proMinControl = (TextBox)editForm["ProMinAllocation"].Controls[0];
                TextBox proNuetralControl = (TextBox)editForm["ProNeutralPercentage"].Controls[0];
                TextBox proDynamicControl = (TextBox)editForm["ProDynamicPercentage"].Controls[0];
                TextBox proMaxControl = (TextBox)editForm["ProMaxAllocation"].Controls[0];


                Guid selectedassetID = new Guid(assetIDControl.Text);
                Guid selectedProductID = new Guid(proIDControl.Text);
                Guid selectedModelID = (Guid)e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"];

                modelsDS.ProMinAllocation = Double.Parse(proMinControl.Text);
                modelsDS.ProNeutralPercentage = Double.Parse(proNuetralControl.Text);
                modelsDS.ProDynamicPercentage = Double.Parse(proDynamicControl.Text);
                modelsDS.ProMaxAllocation = Double.Parse(proMaxControl.Text);

                modelsDS.ModelEntityID = selectedModelID;
                modelsDS.AssetEntityID = selectedassetID;
                modelsDS.ProductEntityID = selectedProductID;

                modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.UpdateProduct;
                iorg.SetData(modelsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Item.Edit = false;

                PresentationGrid.Rebind();
            }
        }

        private void SaveAndRefresh(ModelsDS modelsDS)
        {
          
        }

        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e)
        {

        }

        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e)
        {

        }

        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("Models".Equals(e.Item.OwnerTableView.Name))
            {

                GridEditFormItem editForm = (GridEditFormItem)e.Item;

                Hashtable values = new Hashtable();
                editForm.ExtractValues(values);


                RadComboBox modelCombo = (RadComboBox)editForm["ModelDropDownBoxEditor"].Controls[0];
                RadComboBox assetCombo = (RadComboBox)editForm["AssetDescriptionEditor"].Controls[0];
                RadComboBox productCombo = (RadComboBox)editForm["ProDescriptionEditor"].Controls[0];

                Guid selectedModelID = new Guid(modelCombo.SelectedItem.Value);
                Guid selectedassetID = new Guid(assetCombo.SelectedItem.Value);
                Guid selectedproductID = new Guid(productCombo.SelectedItem.Value);
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                var selectedModel = iorg.Model.Where(mod => mod.ID == selectedModelID);
                var selectedAss = iorg.Assets.Where(mod => mod.ID == selectedassetID);
                var selectedpro = iorg.Products.Where(mod => mod.ID == selectedproductID);


                if (selectedModel != null)
                {
                    UMABroker.SaveOverride = true;


                    ModelsDS modelsDS = new ModelsDS();
                    modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.NewAsset;
                    modelsDS.ModelEntityID = selectedModelID;
                    modelsDS.AssetEntityID = selectedassetID;
                    iorg.SetData(modelsDS);
                    UMABroker.SaveOverride = true;

                    modelsDS = new ModelsDS();
                    modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.NewProduct;
                    modelsDS.ModelEntityID = selectedModelID;
                    modelsDS.AssetEntityID = selectedassetID;
                    modelsDS.ProductEntityID = selectedproductID;

                    TextBox proMinControl = (TextBox)editForm["ProMinAllocation"].Controls[0];
                    TextBox proNuetralControl = (TextBox)editForm["ProNeutralPercentage"].Controls[0];
                    TextBox proDynamicControl = (TextBox)editForm["ProDynamicPercentage"].Controls[0];
                    TextBox proMaxControl = (TextBox)editForm["ProMaxAllocation"].Controls[0];

                    modelsDS.ProMinAllocation = Double.Parse(proMinControl.Text);
                    modelsDS.ProNeutralPercentage = Double.Parse(proNuetralControl.Text);
                    modelsDS.ProDynamicPercentage = Double.Parse(proDynamicControl.Text);
                    modelsDS.ProMaxAllocation = Double.Parse(proMaxControl.Text);

                    iorg.SetData(modelsDS);

                    UMABroker.SetComplete();
                    UMABroker.SetStart();
                }

                e.Item.Edit = false;
                e.Canceled = true;
                this.PresentationGrid.Rebind();
            }
        }

        private ModelsDS PopulateDatasetWithData(GridCommandEventArgs e, ModelDataSetOperationType type)
        {
            GridEditFormItem editForm = (GridEditFormItem)e.Item;

            Hashtable values = new Hashtable();
            editForm.ExtractValues(values);

            TextBox modelName = (TextBox)editForm["ModelName"].Controls[0];
            RadComboBox combo = (RadComboBox)editForm["ServiceTypeEditor"].Controls[0];
            CheckBox chkIsPrivate = (CheckBox)editForm["IsPrivate"].Controls[0];

           
            ModelsDS modelsDS = new ModelsDS();
            modelsDS.ModelDataSetOperationType = type;
            DataRow row = modelsDS.Tables[ModelsDS.MODELLISTTABLEASSETPRODUCTS].NewRow();
            row[ModelsDS.MODELNAME] = modelName.Text;

            if (string.IsNullOrWhiteSpace(combo.SelectedValue))
                row[ModelsDS.SERVICETYPE] = "";
            else
                row[ModelsDS.SERVICETYPE] = (ServiceTypes)Enum.Parse(typeof(ServiceTypes), combo.SelectedValue);

            TextBox programCode = (TextBox)editForm["ProgramCode"].Controls[0];
            row[ModelsDS.PROGRAMCODE] = programCode.Text;   

            row[ModelsDS.ISPRIVATE] = chkIsPrivate.Checked;            
            if (type == ModelDataSetOperationType.UpdateModel)
            {
                row[ModelsDS.ID] = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"];
            }

            modelsDS.Tables[ModelsDS.MODELLISTTABLEASSETPRODUCTS].Rows.Add(row);
            return modelsDS;
        }

        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;
            if (item != null && item.IsInEditMode && item.ItemIndex != -1)
            {

            }
        }

        private void DisplayMessage(string text)
        {
            PresentationGrid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
        }

        #endregion
    }
}
