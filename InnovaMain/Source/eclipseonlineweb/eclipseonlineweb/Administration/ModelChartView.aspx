﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="SetupMaster.master"
    AutoEventWireup="true" CodeBehind="ModelChartView.aspx.cs" Inherits="eclipseonlineweb.ModelChartView" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Charting" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function hintContent() {
            return this.data.label + '<br/> ' + this.y + '';
        }
        function hintContentPie() {
            return this.data.toString() + " : " + Globalize.format(this.value / this.total, "p2");
        }

    </script>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/3232/previous.png" OnClick="BtnBackToListing"
                                AlternateText="Back to Listing" ToolTip="Back to Listing" ID="btnBack" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblModelName" Text="Financial Models"></asp:Label>
                            <asp:Label Font-Bold="true" Visible="false" runat="server" ID="lblModelID"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadChart ID="RadChart" OnItemDataBound="RadChartItemDataBound" runat="server"
                DefaultType="Pie" Width="1000px" AutoTextWrap="true" OnClick="RadChart1Click"
                Skin="Telerik" Legend-Visible="false">
                <Series>
                    <telerik:ChartSeries Name="Series 1" Type="Pie" DataLabelsColumn="Name" DataYColumn="AssetNeutralPercentage">
                        <Appearance LegendDisplayMode="ItemLabels">
                        </Appearance>
                    </telerik:ChartSeries>
                </Series>
            </telerik:RadChart>
            <br />
            <telerik:RadChart ID="RadChart1" OnItemDataBound="RadChart1ItemDataBound" runat="server"
                Skin="Telerik" DefaultType="Pie" Width="1000px" AutoTextWrap="true" Visible="false"
                Legend-Visible="false">
                <Series>
                    <telerik:ChartSeries Name="Series 1" Type="Pie" DataLabelsColumn="ProName" DataYColumn="ProNeutralPercentage">
                        <Appearance LegendDisplayMode="ItemLabels" ExplodePercent="0">
                        </Appearance>
                    </telerik:ChartSeries>
                </Series>
            </telerik:RadChart>
            <br />
            <fieldset>
                <legend>Chart View</legend>
                <asp:PlaceHolder ID="modelChartArea" runat="server"></asp:PlaceHolder>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
