﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecurityDistributionDetails.ascx.cs"
    Inherits="eclipseonlineweb.Administration.SecurityDistributionDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<script language="javascript">


    SetPercentage = function (id) {
        var unitamount = document.getElementById("txt_UnitAmount" + id);

        var txAutaxCredit = document.getElementById("txt_AutaxCredit" + id);

        var txTaxWithHeld = document.getElementById("txt_TaxWithHeld" + id);

        var txGrossDistribution = document.getElementById("txt_GrossDistribution" + id);

        var total = 0;

        total = total + (parseInt(unitamount.value) || 0);
        total = total + (parseInt(txAutaxCredit.value) || 0);
        total = total + (parseInt(txTaxWithHeld.value) || 0);

        if (total > 100) {

            txGrossDistribution.style.color = "Red";
        }
        else {

            txGrossDistribution.style.color = "Black";
        }
        txGrossDistribution.innerHTML = total.toFixed(2) + " %";

    };

</script>
<div class="popup_Container">
    <div class="popup_Titlebar" id="PopupHeader">
        <div class="TitlebarLeft">
            <asp:Label ID="Title" runat="server" Text="Distribution Details"></asp:Label>
        </div>
    </div>
    <div class="popup_Body" style="height: 95%;">
        <table>
            <tr>
                <td>
                </td>
                <td>
                    <asp:HiddenField ID="TextID" runat="server" />
                </td>
                <td>
                </td>
                <td>
                    <asp:HiddenField ID="TextSecID" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Fund Code
                </td>
                <td>
                    <asp:Label ID="TxtFundCode" runat="server" Text="" Font-Bold="true"></asp:Label>
                </td>
                <td>
                    Fund Name
                </td>
                <td>
                    <asp:Label ID="txtFundName" runat="server" Text="" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Record Date
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field" ID="TextRecordDate" runat="server" ReadOnly="false" />
                    <ajaxToolkit:CalendarExtender runat="server" ID="calRecodDate" TargetControlID="TextRecordDate"
                        Format="dd/MM/yyyy " />
                </td>
                <td>
                    Payment Date
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field" ID="TextPaymentDate" runat="server" ReadOnly="false" />
                    <ajaxToolkit:CalendarExtender runat="server" ID="calPaymentDate" TargetControlID="TextPaymentDate"
                        Format="dd/MM/yyyy " />
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <div style="width: 100%; overflow: auto; height: 36em">
                        <table id="rowsTable" width="100%" style="border-color: royalblue; border-width: thin;
                            border-style: solid;" cellspacing="0">
                            <thead>
                                <tr style="border-bottom: thin; border-bottom-color: royalblue; border-bottom-style: solid">
                                    <th>
                                        Components of this distribution
                                    </th>
                                    <th>
                                        Code
                                    </th>
                                    <th>
                                        Unit Amount<br>
                                        (%)
                                    </th>
                                    <th>
                                        Autax Credit<br>
                                        (%)
                                    </th>
                                    <th>
                                        Tax With Held<br>
                                        (%)
                                    </th>
                                    <th>
                                        Gross Distribution
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="6" class="distHeads">
                                        Australian Income
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Interest (subject to NR WHT)
                                    </td>
                                    <td>
                                        882
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount882" runat="server" onchange='SetPercentage("882");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit882" runat="server" onchange='SetPercentage("882");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld882" runat="server" onchange='SetPercentage("882");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution882" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Interest (not subject to NR WHT)
                                    </td>
                                    <td>
                                        871
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount871" runat="server" onchange='SetPercentage("882");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit871" runat="server" onchange='SetPercentage("882");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld871" runat="server" onchange='SetPercentage("882");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution871" runat="server" ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Dividends - Franked
                                    </td>
                                    <td>
                                        880
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount880" runat="server" onchange='SetPercentage("880");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit880" runat="server" onchange='SetPercentage("880");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld880" runat="server" onchange='SetPercentage("880");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution880" runat="server" onchange='SetPercentage("880");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Dividends - Unfranked
                                    </td>
                                    <td>
                                        881
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount881" runat="server" onchange='SetPercentage("881");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit881" runat="server" onchange='SetPercentage("881");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld881" runat="server" onchange='SetPercentage("881");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution881" runat="server" onchange='SetPercentage("881");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Other Income
                                    </td>
                                    <td>
                                        883
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount883" runat="server" onchange='SetPercentage("883");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit883" runat="server" onchange='SetPercentage("883");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld883" runat="server" onchange='SetPercentage("883");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution883" runat="server" onchange='SetPercentage("883");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Dividends Unfranked - CFI
                                    </td>
                                    <td>
                                        873
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount873" runat="server" onchange='SetPercentage("873");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit873" runat="server" onchange='SetPercentage("873");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld873" runat="server" onchange='SetPercentage("873");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution873" runat="server" onchange='SetPercentage("873");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Dividends Franked
                                    </td>
                                    <td>
                                        897
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount897" runat="server" onchange='SetPercentage("897");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit897" runat="server" onchange='SetPercentage("897");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld897" runat="server" onchange='SetPercentage("897");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution897" runat="server" onchange='SetPercentage("897");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" style="font-weight: bold; font-style: italic; border-top: thin; border-top-color: royalblue;
                                        border-top-style: solid">
                                        Non Primary Production Income
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="distHeads">
                                        Foreign Income
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Foreign Sourced Income
                                    </td>
                                    <td>
                                        872
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount872" runat="server" onchange='SetPercentage("872");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit872" runat="server" onchange='SetPercentage("872");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld872" runat="server" onchange='SetPercentage("872");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution872" runat="server" onchange='SetPercentage("872");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign Dividend
                                    </td>
                                    <td>
                                        884
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount884" runat="server" onchange='SetPercentage("884");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit884" runat="server" onchange='SetPercentage("884");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld884" runat="server" onchange='SetPercentage("884");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution884" runat="server" onchange='SetPercentage("884");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign Interest
                                    </td>
                                    <td>
                                        885
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount885" runat="server" onchange='SetPercentage("885");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit885" runat="server" onchange='SetPercentage("885");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld885" runat="server" onchange='SetPercentage("885");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution885" runat="server" onchange='SetPercentage("885");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign Other Income
                                    </td>
                                    <td>
                                        886
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount886" runat="server" onchange='SetPercentage("886");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit886" runat="server" onchange='SetPercentage("886");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld886" runat="server" onchange='SetPercentage("886");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution886" runat="server" onchange='SetPercentage("886");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr class="totalsRow">
                                    <td colspan="6">
                                        Assessable Foreign Income
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="distHeads">
                                        Capital Gains
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign Capital Gain - Indexed
                                    </td>
                                    <td>
                                        874
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount874" runat="server" onchange='SetPercentage("874");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit874" runat="server" onchange='SetPercentage("874");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld874" runat="server" onchange='SetPercentage("874");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution874" runat="server" onchange='SetPercentage("874");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign Capital Gain – Other
                                    </td>
                                    <td>
                                        875
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount875" runat="server" onchange='SetPercentage("875");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit875" runat="server" onchange='SetPercentage("875");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld875" runat="server" onchange='SetPercentage("875");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution875" runat="server" onchange='SetPercentage("875");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign Discount Gain – Direct Investment
                                    </td>
                                    <td>
                                        876
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount876" runat="server" onchange='SetPercentage("876");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit876" runat="server" onchange='SetPercentage("876");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld876" runat="server" onchange='SetPercentage("876");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution876" runat="server" onchange='SetPercentage("876");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign Discount Gain – Unit Trust
                                    </td>
                                    <td>
                                        877
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount877" runat="server" onchange='SetPercentage("877");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit877" runat="server" onchange='SetPercentage("877");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld877" runat="server" onchange='SetPercentage("877");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution877" runat="server" onchange='SetPercentage("877");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign CGT Concession Amount - Direct Investment
                                    </td>
                                    <td>
                                        878
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount878" runat="server" onchange='SetPercentage("878");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit878" runat="server" onchange='SetPercentage("878");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld878" runat="server" onchange='SetPercentage("878");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution878" runat="server" onchange='SetPercentage("878");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Foreign CGT Concession Amount - Unit Trust
                                    </td>
                                    <td>
                                        879
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount879" runat="server" onchange='SetPercentage("879");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit879" runat="server" onchange='SetPercentage("879");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld879" runat="server" onchange='SetPercentage("879");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution879" runat="server" onchange='SetPercentage("879");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        CGT Concession Amount
                                    </td>
                                    <td>
                                        894
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount894" runat="server" onchange='SetPercentage("894");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit894" runat="server" onchange='SetPercentage("894");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld894" runat="server" onchange='SetPercentage("894");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution894" runat="server" onchange='SetPercentage("894");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Capital Gain
                                    </td>
                                    <td>
                                        887
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount887" runat="server" onchange='SetPercentage("887");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit887" runat="server" onchange='SetPercentage("887");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld887" runat="server" onchange='SetPercentage("887");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution887" runat="server" onchange='SetPercentage("887");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="distHeads">
                                        Other Non-Assessable Amount
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Tax-deferred Amounts
                                    </td>
                                    <td>
                                        888
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount888" runat="server" onchange='SetPercentage("888");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit888" runat="server" onchange='SetPercentage("888");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld888" runat="server" onchange='SetPercentage("888");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution888" runat="server" onchange='SetPercentage("888");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Australian Tax Free Income
                                    </td>
                                    <td>
                                        889
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount889" runat="server" onchange='SetPercentage("889");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit889" runat="server" onchange='SetPercentage("889");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld889" runat="server" onchange='SetPercentage("889");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution889" runat="server" onchange='SetPercentage("889");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Other Income
                                    </td>
                                    <td>
                                        890
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount890" runat="server" onchange='SetPercentage("890");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit890" runat="server" onchange='SetPercentage("890");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld890" runat="server" onchange='SetPercentage("890");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution890" runat="server" onchange='SetPercentage("890");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Trans-Tasman dividends
                                    </td>
                                    <td>
                                        899
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_UnitAmount899" runat="server" onchange='SetPercentage("899");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_AutaxCredit899" runat="server" onchange='SetPercentage("899");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txt_TaxWithHeld899" runat="server" onchange='SetPercentage("899");'
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_GrossDistribution899" runat="server" onchange='SetPercentage("899");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2" style="font-weight: bold; font-style: italic; border-top: thin; border-top-color: royalblue;
                                        border-top-style: solid">
                                        Net Cash Distribution
                                    </td>
                                    <td style="border-top: thin; border-top-color: royalblue; border-top-style: solid">
                                        <asp:Label ID="lblUnitAmountTotal" runat="server" onchange='SetPercentage("899");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="border-top: thin; border-top-color: royalblue; border-top-style: solid">
                                        <asp:Label ID="lblAutaxCreditTotal" runat="server" onchange='SetPercentage("899");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="border-top: thin; border-top-color: royalblue; border-top-style: solid">
                                        <asp:Label ID="lblTaxWithHeldTotal" runat="server" onchange='SetPercentage("899");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                    <td style="border-top: thin; border-top-color: royalblue; border-top-style: solid">
                                        <asp:Label ID="lblGrossDistribution" runat="server" onchange='SetPercentage("899");'
                                            ClientIDMode="Static"></asp:Label>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="2">
                    <asp:Label ID="lblMessages" runat="server" Text="" ForeColor="#990000" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="right">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="4" align="right">
                    <asp:Button ID="btnUpdate" runat="server" Text="Save" OnClick="btnUpdate_OnClick" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_OnClick" />
                </td>
            </tr>
        </table>
    </div>
</div>
