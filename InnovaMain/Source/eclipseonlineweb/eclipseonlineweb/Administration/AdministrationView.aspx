﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="SetupMaster.master"
    AutoEventWireup="true" CodeBehind="AdministrationView.aspx.cs" Inherits="eclipseonlineweb.AdministrationView" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript">
        //Resetting menu index
        localStorage['MenuIndex'] = '0';
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/3232/down.png" OnClick="DownloadXLS"
                                AlternateText="Download Model List" ToolTip="Download Model List" ID="btnDownload" />
                            <asp:ImageButton runat="server" Visible="false" ImageUrl="~/images/3232/process.png"
                                OnClick="ReCalculationAllocation" AlternateText="Re Calculation" ToolTip="Re Calculation"
                                ID="btnReCalculationAllocation" />
                            <asp:ImageButton runat="server" ImageUrl="~/images/3232/process.png" OnClick="FixModel"
                                AlternateText="Fix Model" ToolTip="Fix Model" ID="btnFixModel" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblName" Text="Financial Models"></asp:Label>
                            <asp:Label Font-Bold="true" Visible="false" runat="server" ID="lblModelID"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <asp:Panel runat="server" ID="pnlMainGrid">
                <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
                    runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                    AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
                    OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                    AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                    OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
                    OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
                    EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
                    OnItemDataBound="PresentationGrid_ItemDataBound">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                        Name="Models" TableLayout="Fixed">
                        <CommandItemSettings AddNewRecordText="Add New Model" ShowExportToExcelButton="true"
                            ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                        <Columns>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridEditCommandColumn>
                            <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="ModelName" HeaderStyle-Width="20%"
                                ItemStyle-Width="15%" HeaderText="Model Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ModelName" UniqueName="ModelName">
                            </telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn HeaderText="Service Type" SortExpression="ServiceType"
                                UniqueName="ServiceTypeEditor" DataField="ServiceType" Visible="false" ColumnEditorID="GridDropDownColumnEditorModelServiceType" />
                            <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                                SortExpression="ProgramCode" HeaderText="Program Code" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ProgramCode" UniqueName="ProgramCode" ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="100px" ReadOnly="true" HeaderStyle-Width="15%"
                                ItemStyle-Width="15%" SortExpression="ServiceType" HeaderText="Service Type"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ServiceType" UniqueName="ServiceType">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" ReadOnly="true" HeaderStyle-Width="8%"
                                ItemStyle-Width="8%" SortExpression="HasErrors" HeaderText="Errors" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="HasErrors" UniqueName="HasErrors">
                            </telerik:GridBoundColumn>
                            <telerik:GridCheckBoxColumn FilterControlWidth="50px" HeaderStyle-Width="5%" AllowFiltering="false"
                                ItemStyle-Width="5%" SortExpression="IsPrivate" HeaderText="Private" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="IsPrivate" UniqueName="IsPrivate">
                            </telerik:GridCheckBoxColumn>
                            <telerik:GridButtonColumn CommandName="Chart" Text="Chart" ButtonType="ImageButton"
                                ItemStyle-Width="24px" ItemStyle-Height="24px" ImageUrl="~/images/pie_chart.png"
                                HeaderStyle-Width="6%">
                                <ItemStyle HorizontalAlign="Center" />
                            </telerik:GridButtonColumn>
                            <telerik:GridButtonColumn CommandName="Details" Text="Assets" ButtonType="LinkButton"
                                HeaderStyle-Width="8%" ItemStyle-Width="8%">
                            </telerik:GridButtonColumn>
                            <telerik:GridButtonColumn CommandName="UpdateClientModels" Text="Update Model at Clients"
                                ButtonType="LinkButton" HeaderStyle-Width="10%" ItemStyle-Width="10%" UniqueName="UpdateClientModel">
                            </telerik:GridButtonColumn>
                            <telerik:GridHyperLinkColumn HeaderStyle-Width="10%" ItemStyle-Width="10%" Text="View Clients" DataNavigateUrlFields="ProgramCode" DataNavigateUrlFormatString="./ViewModelClients.aspx?ProgramCode={0}" UniqueName="ViewCLients"/>
                            <telerik:GridButtonColumn ConfirmText="Delete this model?" ButtonType="ImageButton"
                                CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                <HeaderStyle Width="6%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                    <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                        <Excel Format="Html"></Excel>
                    </ExportSettings>
                </telerik:RadGrid>
                <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorModelServiceType"
                    runat="server" DropDownStyle-Width="200px" />
                <asp:Label ID="Label1" runat="server" EnableViewState="false" Visible="false"></asp:Label>
            </asp:Panel>
            <asp:Label ID="lblModelDetailID" runat="server" Visible="false"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
