﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;

namespace eclipseonlineweb
{
    public partial class AdministrationView : UMABasePage
    {
        protected override void GetBMCData()
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            ModelsDS modelsDS = new Oritax.TaxSimp.DataSets.ModelsDS();

            organization.GetData(modelsDS);
            this.PresentationData = modelsDS;

            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        public override void LoadPage()
        {
            base.LoadPage();

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
            {
                ((SetupMaster)Master).IsAdmin = "1";
            }
            //Only Admin User can Fix, Update Client Model and Add/Edit/Delete
            btnFixModel.Visible = objUser.Administrator;
            PresentationGrid.MasterTableView.CommandItemSettings.ShowAddNewRecordButton = objUser.Administrator;
            PresentationGrid.Columns.FindByUniqueNameSafe("UpdateClientModel").Visible = PresentationGrid.Columns.FindByUniqueNameSafe("DeleteColumn").Visible = PresentationGrid.Columns.FindByUniqueNameSafe("EditCommandColumn2").Visible = objUser.Administrator;

            if (objUser.Administrator || objUser.UserType == UserType.Innova)
                PresentationGrid.MasterTableView.GetColumn("HasErrors").Display = true;
            else
                PresentationGrid.MasterTableView.GetColumn("HasErrors").Display = false;
        }

        public override void PopulatePage(DataSet ds)
        {
            ScriptManager sm = ScriptManager.GetCurrent(Page);

            if (sm != null)
                sm.RegisterPostBackControl(this.btnDownload);
        }

        protected void ReCalculationAllocation(object sender, ImageClickEventArgs e)
        {
            ModelsDS modelsDS = new Oritax.TaxSimp.DataSets.ModelsDS();
            modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.ReCalculationAllocation;
            this.SaveOrganizanition(modelsDS);
        }

        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            if (pnlMainGrid.Visible)
            {
                IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

                ModelsDS modelsDS = new Oritax.TaxSimp.DataSets.ModelsDS();
                modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.ModelExport;
                organization.GetData(modelsDS);

                UMABroker.ReleaseBrokerManagedComponent(organization);

                DataSet excelDataset = new DataSet();
                excelDataset.Merge(modelsDS.Tables[ModelsDS.MODELLISTTABLEASSETPRODUCTS], true, MissingSchemaAction.Add);
                excelDataset.Tables[0].Columns.Remove("ID");
                excelDataset.Tables[0].Columns.Remove(ModelsDS.PROID);
                excelDataset.Tables[0].Columns.Remove(ModelsDS.ASSETID);
                ExcelHelper.ToExcel(excelDataset, "Models-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", this.Page.Response);
            }
            else
            {
                IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

                ModelsDS modelsDS = new Oritax.TaxSimp.DataSets.ModelsDS();

                modelsDS.GetDetails = true;
                modelsDS.DetailsGUID = new Guid(this.lblModelID.Text);

                organization.GetData(modelsDS);

                UMABroker.ReleaseBrokerManagedComponent(organization);

                DataSet excelDataset = new DataSet();
                excelDataset.Merge(modelsDS.Tables[ModelsDS.MODELDETAILSLISTPRO], true, MissingSchemaAction.Add);
                excelDataset.Merge(modelsDS.Tables[ModelsDS.MODELDETAILSLISTASSETS], true, MissingSchemaAction.Add);
                ExcelHelper.ToExcel(excelDataset, this.lblName.Text + "-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", this.Page.Response);
            }
        }

        protected void FixModel(object sender, ImageClickEventArgs e)
        {
            UMABroker.SaveOverride = true;

            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            ModelsDS modelsDS = new Oritax.TaxSimp.DataSets.ModelsDS();
            modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.BulkUpdate;
            organization.SetData(modelsDS);
            UMABroker.SetComplete();
            UMABroker.SetStart();
        }

        #region Telerik Code

        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e)
        {

        }

        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {

        }

        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;
                GridDropDownListColumnEditor editor = null;

                if ("Models".Equals(e.Item.OwnerTableView.Name))
                {
                    IDictionary<Enum, string> servicesValue = Enumeration.ToDictionary(typeof(ServiceTypes));
                    editor = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("ServiceTypeEditor"));

                    editor.DataSource = servicesValue;
                    editor.DataValueField = "Key";
                    editor.DataTextField = "Value";
                    editor.DataBind();
                    if (!(e.Item.DataItem is GridInsertionObject))
                    {
                        string value = ((System.Data.DataRowView)(e.Item.DataItem)).Row["ServiceType"].ToString();
                        if (value != string.Empty)
                        {
                            RadComboBoxItem item = editor.ComboBoxControl.FindItemByText(value);
                            if (item != null)
                                item.Selected = true;
                        }
                    }
                }
            }
            e.Canceled = true;
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DataView ModelListView = new DataView(this.PresentationData.Tables[ModelsDS.MODELLIST]);
            ModelListView.Sort = ModelsDS.MODELNAME + " ASC";
            this.PresentationGrid.DataSource = ModelListView.ToTable();
        }

        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {

            if (e.CommandName.ToLower() == "details")
            {
                string modelID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"].ToString();
                Response.Redirect("~/Administration/ModelDetailsView.aspx?ModelID=" + modelID);
            }

            if (e.CommandName.ToLower() == "chart")
            {
                string modelID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"].ToString();
                Response.Redirect("~/Administration/ModelChartView.aspx?ModelID=" + modelID);
            }

            if (e.CommandName.ToLower() == "update")
            {
                if ("Models".Equals(e.Item.OwnerTableView.Name))
                {
                    ModelsDS modelsDS = PopulateDatasetWithData(e, ModelDataSetOperationType.UpdateModel);
                    e.Item.Edit = false;
                    e.Canceled = true;
                    SaveAndRefresh(modelsDS);
                }
            }
            else if (e.CommandName.ToLower() == "delete")
            {
                ModelsDS modelsDS = new ModelsDS();
                modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.DeleteModel;
                modelsDS.ModelEntityID = (Guid)e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"];

                e.Item.Edit = false;
                e.Canceled = true;
                SaveAndRefresh(modelsDS);
            }

            else if (e.CommandName.ToLower() == "delete")
            {
                ModelsDS modelsDS = new ModelsDS();
                modelsDS.ModelDataSetOperationType = ModelDataSetOperationType.DeleteModel;
                modelsDS.ModelEntityID = (Guid)e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"];

                e.Item.Edit = false;
                e.Canceled = true;
                SaveAndRefresh(modelsDS);
            }
            else if (e.CommandName.ToLower() == "updateclientmodels")
            {
                var modelID = (Guid)e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"];
                var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                var model = org.Model.FirstOrDefault(mod => mod.ID == modelID);
                UMABroker.ReleaseBrokerManagedComponent(org);
                UpdateClientModelsDS modelsDS = new UpdateClientModelsDS
                                                    {
                                                        Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() },
                                                        Command = (int)WebCommands.UpdateAllClientDetails,
                                                        ModelDataSetOperationType = ModelDataSetOperationType.UpdateModel,
                                                        ModelEntityID = modelID,
                                                        ModelEntity = model,
                                                    };
                e.Item.Edit = false;
                e.Canceled = true;
                SaveOrganizanition(modelsDS);

            }



        }

        private void SaveAndRefresh(ModelsDS modelsDS)
        {
            SaveOrganizanition(modelsDS);
            this.GetBMCData();
            this.PresentationGrid.Rebind();
        }

        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e)
        {

        }

        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e)
        {

        }

        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("Models".Equals(e.Item.OwnerTableView.Name))
            {
                IOrganization iorg;
                ModelsDS modelsDS = PopulateDatasetWithData(e, ModelDataSetOperationType.NewModel);

                iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                iorg.SetData(modelsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Item.Edit = false;
                e.Canceled = true;
                this.PresentationGrid.Rebind();
            }
        }

        private ModelsDS PopulateDatasetWithData(GridCommandEventArgs e, ModelDataSetOperationType type)
        {
            GridEditFormItem editForm = (GridEditFormItem)e.Item;

            Hashtable values = new Hashtable();
            editForm.ExtractValues(values);

            TextBox modelName = (TextBox)editForm["ModelName"].Controls[0];
            RadComboBox combo = (RadComboBox)editForm["ServiceTypeEditor"].Controls[0];
            CheckBox chkIsPrivate = (CheckBox)editForm["IsPrivate"].Controls[0];


            ModelsDS modelsDS = new ModelsDS();
            modelsDS.ModelDataSetOperationType = type;
            DataRow row = modelsDS.Tables[ModelsDS.MODELTABLE].NewRow();
            row[ModelsDS.MODELNAME] = modelName.Text;

            if (string.IsNullOrWhiteSpace(combo.SelectedValue))
                row[ModelsDS.SERVICETYPE] = "";
            else
                row[ModelsDS.SERVICETYPE] = (ServiceTypes)Enum.Parse(typeof(ServiceTypes), combo.SelectedValue);

            TextBox programCode = (TextBox)editForm["ProgramCode"].Controls[0];
            row[ModelsDS.PROGRAMCODE] = programCode.Text;

            row[ModelsDS.ISPRIVATE] = chkIsPrivate.Checked;
            if (type == ModelDataSetOperationType.UpdateModel)
            {
                row[ModelsDS.ID] = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"];
            }

            modelsDS.Tables[ModelsDS.MODELTABLE].Rows.Add(row);
            return modelsDS;
        }

        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;
            if (item != null && item.IsInEditMode && item.ItemIndex != -1)
            {

            }
        }

        private void DisplayMessage(string text)
        {
            PresentationGrid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
        }

        #endregion
    }
}
