﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using System.Linq;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using C1.Web.Wijmo.Controls.C1ComboBox;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Security;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Telerik.Web.UI;
using Telerik.Charting;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using C1.C1Preview;
using C1.Web.Wijmo.Controls.C1Chart;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using eclipseonlineweb.Administration;


namespace eclipseonlineweb
{
    /// <summary>
    /// Keep commented lines intact - just in case
    /// </summary>
    public partial class InvestmentManagement : UMABasePage
    {
        public override void LoadPage()
        {
            this.cid = Request.QueryString["ID"].ToString();
            lblID.Text = this.cid;

            if (!this.Page.IsPostBack)
            {
                GetData();
                SetData();
                DataTable products = PresentationData.Tables[ProductsDS.PRODUCTSTABLE];

                var a = products.AsEnumerable().Where(r => r.Field<Guid>(InvestmentDS.ID) == new Guid(cid));
                if (a.AsDataView().Count > 0)
                {
                    DataRowView r = a.AsDataView()[0];
                    this.lblName.Text = r[ProductsDS.NAME].ToString() + " - " + r[ProductsDS.DESCRIPTION].ToString();
                }

            }
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                this.btnSave.Visible = false;
            }

            UMABroker.ReleaseBrokerManagedComponent(objUser);
        }
 
        private void GetData()
        {
            OrganizationCM cm = this.UMABroker.GetWellKnownCM(WellKnownCM.Organization) as OrganizationCM;
            InvestmentDS ds = new InvestmentDS();
            ds.InvestmentID = new Guid(this.lblID.Text);
            cm.GetData(ds);
            this.PresentationData = ds;
            if (!this.IsPostBack)
            {
                AllocationsTable = this.PresentationData.Tables[InvestmentDS.ALLOCATIONS]; 

            }
            ProductSecTable = this.PresentationData.Tables[PROSecuritiesDS.PROSECLISTTABLE];
            UMABroker.ReleaseBrokerManagedComponent(cm);
        }


        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

            BindManagersGrid();
        }

        private void BindManagersGrid()
        {
            //if (PresentationData == null)
            //   GetData();

            //DataTable tbAllocation = this.PresentationData.Tables[InvestmentDS.ALLOCATIONS];
             
            DataView View = new DataView(AllocationsTable);
            View.Sort = PROSecuritiesDS.NAME + " ASC";
            this.PresentationGrid.DataSource = View.ToTable();            

        }


        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {

            if (!e.Canceled)
            {
                SaveAlocations(e, DatasetCommandTypes.Add);
            }

        }

        private void SaveAlocations(GridCommandEventArgs e, DatasetCommandTypes cmdType)
        {
            //InvestmentDS ds = new InvestmentDS();
            //ds.CommandType = cmdType;
            DataTable table = AllocationsTable;
            DataRow changedRow = table.NewRow();

            if (cmdType == DatasetCommandTypes.Add)
            {
                FillDataRowFromGrid(e, changedRow);
                table.Rows.Add(changedRow);
            }
            else if (cmdType == DatasetCommandTypes.Delete)
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                Guid id = new Guid(gDataItem["cmbProductSecuritiesId"].Text);
                for (int i = table.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow r = table.Rows[i];
                    if ((Guid)r[InvestmentDS.ID] == id && r[InvestmentDS.PERCENTAGE] == gDataItem[InvestmentDS.PERCENTAGE])
                    {
                        table.Rows.RemoveAt(i);
                        break;
                    }
                }
            }
            else if (cmdType == DatasetCommandTypes.Update)
            {
                FillDataRowFromGrid(e, changedRow);
                bool found = false;
                for (int i = table.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow r = table.Rows[i];
                    if ((Guid)r[InvestmentDS.ID] == (Guid)changedRow[InvestmentDS.ID])
                    {
                        FillDataRowFromGrid(e, r);
                        found = true;
                        break;
                    }
                }
                //if (!found)
                //{
                //    for (int i = table.Rows.Count - 1; i >= 0; i--)
                //    {
                //        DataRow r = table.Rows[i];
                //        if (r[InvestmentDS.PERCENTAGE] == changedRow[InvestmentDS.PERCENTAGE])
                //        {
                //            FillRowData(e, r, cmdType);
                //            found = true;
                //            break;
                //        }
                //    }
                //}
                if (!found)
                    table.Rows.Add(changedRow);
            }
            e.Canceled = true;
            e.Item.Edit = false;
            PresentationGrid.DataSource = table;
            PresentationGrid.Rebind();
        }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                if (!e.Canceled)
                {
                    SaveAlocations(e, DatasetCommandTypes.Delete);
                }
            }
            else if (e.CommandName.ToLower() == "update")
            {
                if (!e.Canceled)
                {
                    SaveAlocations(e, DatasetCommandTypes.Update);
                }
            }
        }

        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;
                //IOrganization organization = this.UMABroker1.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;


                string selectedProdSec = DataBinder.Eval(editedItem.DataItem, "ID").ToString();
                GridDropDownListColumnEditor CEProductSecurities = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("cmbProductSecuritiesId"));

                if (ProductSecTable != null)
                {
                    DataView dv = new DataView(ProductSecTable);
                    dv.Sort = "Name ASC";
                    CEProductSecurities.DataSource = dv;
                    CEProductSecurities.DataValueField = "ID";
                    CEProductSecurities.DataTextField = "Name";

                    CEProductSecurities.DataBind();
                    if (!string.IsNullOrEmpty(selectedProdSec))
                        CEProductSecurities.SelectedValue = selectedProdSec;
                }

                editedItem["Name"].Parent.Visible = false;
           }
        }

        void cmbbProductSecurities_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox cmb = ((RadComboBox)sender);
            GridEditableItem editableItem = (GridEditableItem)cmb.NamingContainer;
            GridEditManager editMan = editableItem.EditManager;

            TextBox txtName = (TextBox)editableItem["Name"].Controls[0];
            txtName.Text = cmb.SelectedItem.Text;

            RadComboBox cmbbProductSecurities = (RadComboBox)editableItem["cmbProductSecuritiesId"].Controls[0];
        }


        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
            {
                GridEditFormItem EditForm = (GridEditFormItem)e.Item;


                RadComboBox cmbbProductSecurities = (RadComboBox)EditForm["cmbProductSecuritiesId"].Controls[0];
                cmbbProductSecurities.AutoPostBack = false;
                cmbbProductSecurities.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(cmbbProductSecurities_SelectedIndexChanged);
            }
        }

        private void FillDataRowFromGrid(GridCommandEventArgs e, DataRow row)
        {
            GridEditableItem item = (GridEditableItem)e.Item;

            if (item["PercentageAllocation"].Controls != null && item["PercentageAllocation"].Controls.Count > 0 )
                row[InvestmentDS.PERCENTAGE] = ((TextBox)item["PercentageAllocation"].Controls[0]).Text;
            else
                row[InvestmentDS.PERCENTAGE] = item["PercentageAllocation"].Text;

            if (((RadComboBox)(item["cmbProductSecuritiesId"].Controls[0])).SelectedItem != null)
            {
                row[InvestmentDS.ID] = ((RadComboBox)(item["cmbProductSecuritiesId"].Controls[0])).SelectedItem.Value;
                row[InvestmentDS.NAME] = ((RadComboBox)(item["cmbProductSecuritiesId"].Controls[0])).SelectedItem.Text;
            }

        }

        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e)
        {

        }
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e)
        {

        }
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e)
        {

        }
         /// <summary>
        /// Populate fields in the screen
        /// </summary>
        private void SetData()
        {
                BindBenchmarkCombo();
                BindManagersGrid();

                DataRow r = FindRow();
                this.txtInvestmentManager.Text = r[InvestmentDS.INVESTMENTMANAGER].ToString();
                this.txtPortfoloManager.Text = r[InvestmentDS.PORTFOLOMANAGER].ToString();
                this.txtAssetType.Text = r[InvestmentDS.ASSETTYPE].ToString();
                this.txtREGION.Text = r[InvestmentDS.REGION].ToString();
                this.txtOBJECTIVE.Text = r[InvestmentDS.OBJECTIVE].ToString();
                this.txtFINDSIZE.Text = r[InvestmentDS.FINDSIZE].ToString();
                this.txtMINWITHRAWAL.Text = r[InvestmentDS.MINWITHRAWAL].ToString();
                this.txtMINADDITION.Text = r[InvestmentDS.MINADDITION].ToString();
                this.txtENTRYFEE.Text = r[InvestmentDS.ENTRYFEE].ToString();
                this.txtEXITFEE.Text = r[InvestmentDS.EXITFEE].ToString();
                this.txtBUYSELLSPREAD.Text = r[InvestmentDS.BUYSELLSPREAD].ToString();
                this.txtMANAGEMENTFEE.Text = r[InvestmentDS.MANAGEMENTFEE].ToString();
                this.txtICR.Text = r[InvestmentDS.ICR].ToString();
                this.cmbBenchmark.SelectedValue = r[InvestmentDS.BENCHMARK].ToString();

                this.txtInvestTime.Text = r[InvestmentDS.INVESTMENTTIME].ToString();
                this.RadInceptionDate.DisplayText = r[InvestmentDS.INCEPTIONDATE].ToString();
                this.txtMinInvest.Text = r[InvestmentDS.MININVESTMENT].ToString();
                this.RadManagementOf.DisplayText = r[InvestmentDS.MANAGEMENTFEEOF].ToString();
                this.txtInfo.Text = r[InvestmentDS.INFO].ToString();

        }

        private void BindBenchmarkCombo()
        {
            DataTable secList = PresentationData.Tables[SecuritiesDS.SECLIST];
            foreach (DataRow row in secList.Rows)
            {
                var item = new RadComboBoxItem
                {
                    Text = String.Format("{0} - {1} ", row[SecuritiesDS.CODE], row[SecuritiesDS.DESCRIPTION]),
                    Value = row[SecuritiesDS.ID].ToString()
                };
                cmbBenchmark.Items.Add(item);
            }

            //add emptty item
            cmbBenchmark.Items.Insert(0, new RadComboBoxItem { Text = " ", Value = Guid.Empty.ToString() });

        }

        private DataRow FindRow()
        {
            InvestmentDS ds = this.PresentationData as InvestmentDS;
            DataTable table = PresentationData.Tables[InvestmentDS.INVESTMENTMANAGEMENT];
            Guid cidGuid = new Guid(cid);
            DataRow r;

            if (table.Rows.Count == 0)
            {
                r = table.NewRow();
                table.Rows.Add(r);
            }
            else
            {
                var a = table.AsEnumerable().Where(row => row.Field<Guid>(InvestmentDS.ID) == cidGuid);
                if (a.AsDataView().Count > 0)
                {
                    r = a.AsDataView()[0].Row;
                }
                else
                {
                    r = table.NewRow();
                    table.Rows.Add(r);
                }
            }
             r[InvestmentDS.ID] = cidGuid;
             return r;        
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            OrganizationCM cm = this.UMABroker.GetWellKnownCM(WellKnownCM.Organization) as OrganizationCM;

            InvestmentDS ds = new InvestmentDS();
            ds.CommandType = DatasetCommandTypes.Update;
            ds.InvestmentID = new Guid(this.lblID.Text);

            DataTable table = ds.Tables[InvestmentDS.INVESTMENTMANAGEMENT];
            DataRow r = table.NewRow();
            table.Rows.Add(r);

            r[InvestmentDS.ID] = ds.InvestmentID;
            r[InvestmentDS.INVESTMENTMANAGER] = this.txtInvestmentManager.Text;
            r[InvestmentDS.PORTFOLOMANAGER] = this.txtPortfoloManager.Text;
            r[InvestmentDS.ASSETTYPE] = this.txtAssetType.Text;
            r[InvestmentDS.REGION] = this.txtREGION.Text;
            r[InvestmentDS.OBJECTIVE] = this.txtOBJECTIVE.Text;
            r[InvestmentDS.FINDSIZE] = this.txtFINDSIZE.Text;
            r[InvestmentDS.MINWITHRAWAL] = this.txtMINWITHRAWAL.Value;
            r[InvestmentDS.MINADDITION] = this.txtMINADDITION.Value;
            r[InvestmentDS.ENTRYFEE] = this.txtENTRYFEE.Value;
            r[InvestmentDS.EXITFEE] = this.txtEXITFEE.Value;
            r[InvestmentDS.BUYSELLSPREAD] = this.txtBUYSELLSPREAD.Value;
            r[InvestmentDS.MANAGEMENTFEE] = this.txtMANAGEMENTFEE.Value;
            r[InvestmentDS.ICR] = this.txtICR.Value;
            r[InvestmentDS.BENCHMARK] = new Guid(this.cmbBenchmark.SelectedValue);

            r[InvestmentDS.INVESTMENTTIME] = this.txtInvestTime.Text;
            r[InvestmentDS.INCEPTIONDATE] = this.RadInceptionDate.DisplayText;
            r[InvestmentDS.MININVESTMENT] = this.txtMinInvest.Text;
            r[InvestmentDS.MANAGEMENTFEEOF] = this.RadManagementOf.DisplayText;
            r[InvestmentDS.INFO] = this.txtInfo.Text;

            ds.Merge(AllocationsTable);

            SaveOrganizanition(ds);
        }

        protected void btnBackToListing(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Administration/Products.aspx");
        }
        private void DisplayMessage(string text)
        {
            this.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
        }

        public DataTable AllocationsTable
        {
            get
            {
                if (this.ViewState["AllocationsTable"] == null)
                    this.ViewState.Add("AllocationsTable", new DataTable());
                return (DataTable)this.ViewState["AllocationsTable"];
            }
            set
            {
                this.ViewState["AllocationsTable"] = value;
            }
        }
        public DataTable ProductSecTable
        {
            get
            {
                if (this.ViewState["ProductSecTable"] == null)
                    this.ViewState.Add("ProductSecTable", new DataTable());
                return (DataTable)this.ViewState["ProductSecTable"];
            }
            set
            {
                this.ViewState["ProductSecTable"] = value;
            }
        }


    }		

}
