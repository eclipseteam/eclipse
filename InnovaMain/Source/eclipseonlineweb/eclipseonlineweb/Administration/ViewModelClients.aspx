﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="SetupMaster.master"
    AutoEventWireup="true" CodeBehind="ViewModelClients.aspx.cs" Inherits="eclipseonlineweb.ViewModelClients" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript">
        //Resetting menu index
        localStorage['MenuIndex'] = '0';
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/3232/down.png" OnClick="DownloadXLS"
                                AlternateText="Download Model Client List" ToolTip="DownloadModel Client List"
                                ID="btnDownload" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblName" Text="Model Clients"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <asp:Panel runat="server" ID="pnlMainGrid">
                <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                    Width="100%" PageSize="20" AllowSorting="True" AllowMultiRowSelection="False"
                    AllowPaging="True" GridLines="None" AllowFilteringByColumn="true" EnableViewState="true"
                    ShowFooter="false" OnNeedDataSource="PresentationGrid_NeedDataSource">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AllowFilteringByColumn="True" TableLayout="Fixed" Width="100%">
                        <Columns>
                            <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="ClientCID"
                                SortExpression="ClientID" UniqueName="ClientID" DataNavigateUrlFormatString="../ClientViews/ClientMainView.aspx?ins={0}"
                                HeaderText="Client ID" DataTextField="ClientID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                FilterControlWidth="100px" HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderButtonType="TextButton"
                                ShowFilterIcon="true">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridBoundColumn FilterControlWidth="200px" SortExpression="ClientName" HeaderStyle-Width="25%"
                                ItemStyle-Width="20%" HeaderText="ClientName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ClientName" UniqueName="ClientName"
                                ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="100px" HeaderStyle-Width="15%" ItemStyle-Width="10%"
                                SortExpression="ServiceType" HeaderText="Service Type" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ServiceType" UniqueName="ServiceType" ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="100px" HeaderStyle-Width="10%" ItemStyle-Width="8%"
                                SortExpression="ProgramCode" HeaderText="Program Code" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ProgramCode" UniqueName="ProgramCode" ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="100px" ReadOnly="true" HeaderStyle-Width="15%"
                                ItemStyle-Width="10%" SortExpression="ProgramName" HeaderText="ProgramName" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ProgramName" UniqueName="ProgramName">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                        <Excel Format="Html"></Excel>
                    </ExportSettings>
                </telerik:RadGrid>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
