﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;
using Telerik.Charting;
using System.Web.UI.HtmlControls;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1Gauge;

namespace eclipseonlineweb
{
    public partial class ModelChartView : UMABasePage
    {
        protected override void GetBMCData()
        {
            cid = Request.QueryString["ModelID"];

            IOrganization organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            ModelsDS modelsDS = new ModelsDS();
            modelsDS.GetDetails = true;
            modelsDS.DetailsGUID = new Guid(cid);

            organization.GetData(modelsDS);
            PresentationData = modelsDS;

            lblModelName.Text = modelsDS.Tables[ModelsDS.MODELTABLE].Rows[0][ModelsDS.PROGRAMCODE] + " - " + modelsDS.Tables[ModelsDS.MODELTABLE].Rows[0][ModelsDS.MODELNAME];

            if (!IsPostBack)
            {
                DataView view = new DataView(modelsDS.Tables[ModelsDS.MODELDETAILSLISTASSETS])
                                    {
                                        Sort = ModelsDS.ASSETDESCRIPTION + " ASC",
                                        RowFilter = "AssetNeutralPercentage" + " <> '0'"
                                    };

                //Adding New Row in case Sum is not equal to 100
                var table = view.ToTable();
                var sum = table.AsEnumerable().Sum(p => Double.Parse(p["AssetNeutralPercentage"].ToString()));
                if (sum < 100)
                {
                    var dr = table.NewRow();
                    dr["AssetNeutralPercentage"] = 100 - sum;
                    dr[ModelsDS.ASSETMODELNAME] = "None";
                    table.Rows.Add(dr);
                }

                RadChart.ChartTitle.TextBlock.Text = modelsDS.Tables[ModelsDS.MODELTABLE].Rows[0][ModelsDS.PROGRAMCODE] + " - " + modelsDS.Tables[ModelsDS.MODELTABLE].Rows[0][ModelsDS.MODELNAME];
                RadChart.DataSource = table;
                RadChart.DataBind();
            }
            
            
            var modelCollection = modelsDS.Tables[ModelsDS.MODELDETAILSLIST].AsEnumerable();
            var modelCollectionGroup = modelCollection.GroupBy(holdColl => holdColl["AssetName"]).OrderBy(holdCol => holdCol.Key);

            HtmlTable htmltable = new HtmlTable();
            HtmlTableRow row;
            HtmlTableCell cell;

            foreach (var group in modelCollectionGroup)
            {
                Double dynamic = 0;
                Double neutral = 0;
                Double min = 0;
                Double max = 0;

                foreach (DataRow dRow in group)
                {
                    dynamic += Convert.ToDouble(dRow[ModelsDS.PRODYNAMICPERCENTAGE].ToString());
                    neutral += Convert.ToDouble(dRow[ModelsDS.PROSHAREPERCENTAGE].ToString());
                    min += Convert.ToDouble(dRow[ModelsDS.PROMINALLOCATION].ToString());
                    max += Convert.ToDouble(dRow[ModelsDS.PROMAXALLOCATION].ToString());
                }

                C1.Web.Wijmo.Controls.C1Gauge.C1LinearGauge linearGauge = new C1.Web.Wijmo.Controls.C1Gauge.C1LinearGauge();
                linearGauge.Labels.LabelStyle.FontSize = "12pt";
                linearGauge.Labels.LabelStyle.FontWeight = "800";
                linearGauge.Labels.LabelStyle.Fill.Color = ColorTranslator.FromHtml("#1E395B");
                linearGauge.Height = 95;
                linearGauge.Width = new System.Web.UI.WebControls.Unit("70%");

                linearGauge.Pointer.Length = 0.5;
                linearGauge.Pointer.Shape = C1.Web.Wijmo.Controls.C1Gauge.Shape.Rect;
                linearGauge.Pointer.PointerStyle.Stroke = ColorTranslator.FromHtml("#1E395B");
                linearGauge.Pointer.PointerStyle.Fill.Color = ColorTranslator.FromHtml("#1E395B");

                linearGauge.TickMajor.Position = C1.Web.Wijmo.Controls.C1Gauge.Position.Inside;
                linearGauge.TickMajor.Offset = -11;
                linearGauge.TickMajor.Interval = 5;
                linearGauge.TickMajor.Factor = 12;
                linearGauge.TickMajor.TickStyle.Width = 2;
                linearGauge.TickMajor.TickStyle.Fill.Color = ColorTranslator.FromHtml("#1E395B");

                linearGauge.TickMinor.Position = C1.Web.Wijmo.Controls.C1Gauge.Position.Inside;
                linearGauge.TickMinor.Visible = true;
                linearGauge.TickMinor.Offset = -11;
                linearGauge.TickMinor.Interval = 1;
                linearGauge.TickMinor.Factor = 10;
                linearGauge.TickMinor.TickStyle.Width = 1;
                linearGauge.TickMinor.TickStyle.Fill.Color = ColorTranslator.FromHtml("#1E395B");

                linearGauge.Pointer.Shape = C1.Web.Wijmo.Controls.C1Gauge.Shape.Rect;
                linearGauge.Pointer.PointerStyle.Stroke = ColorTranslator.FromHtml("#1E395B");
                linearGauge.Pointer.PointerStyle.Fill.Color = ColorTranslator.FromHtml("#1E395B");

                linearGauge.Face.FaceStyle.Stroke = ColorTranslator.FromHtml("#7BA0CC");
                linearGauge.Face.FaceStyle.StrokeWidth = 4;
                linearGauge.Face.FaceStyle.Fill.LinearGradientAngle = 270;
                linearGauge.Face.FaceStyle.Fill.ColorBegin = ColorTranslator.FromHtml("#FFFFFF");
                linearGauge.Face.FaceStyle.Fill.ColorEnd = ColorTranslator.FromHtml("#D9E3F0");
                linearGauge.Face.FaceStyle.Fill.Type = C1.Web.Wijmo.Controls.C1Chart.ChartStyleFillType.LinearGradient;

                GaugelRange minMaxRange = new C1.Web.Wijmo.Controls.C1Gauge.GaugelRange();
                minMaxRange.StartValue = min;
                minMaxRange.EndValue = max;
                minMaxRange.StartDistance = 0.95;
                minMaxRange.EndDistance = 0.95;
                minMaxRange.StartWidth = 0.5;
                minMaxRange.EndWidth = 0.5;
                minMaxRange.RangeStyle.Fill.LinearGradientAngle = 90;
                minMaxRange.RangeStyle.Fill.Type = C1.Web.Wijmo.Controls.C1Chart.ChartStyleFillType.LinearGradient;
                minMaxRange.RangeStyle.Fill.ColorBegin = ColorTranslator.FromHtml("#3DA1D8");
                minMaxRange.RangeStyle.Fill.ColorEnd = ColorTranslator.FromHtml("#3A6CAC");

                GaugelRange neutralRange = new C1.Web.Wijmo.Controls.C1Gauge.GaugelRange();
                neutralRange.StartValue = neutral - 0.3;
                neutralRange.EndValue = neutral + 0.3;
                neutralRange.StartDistance = 0.70;
                neutralRange.EndDistance = 0.70;
                neutralRange.StartWidth = 0.5;
                neutralRange.EndWidth = 0.5;
                neutralRange.RangeStyle.Fill.LinearGradientAngle = 90;
                neutralRange.RangeStyle.Fill.Type = C1.Web.Wijmo.Controls.C1Chart.ChartStyleFillType.LinearGradient;
                neutralRange.RangeStyle.Fill.ColorBegin = Color.Gold;
                neutralRange.RangeStyle.Fill.ColorEnd = Color.Gold;

                linearGauge.Value = dynamic;

                linearGauge.Ranges.Add(minMaxRange);
                linearGauge.Ranges.Add(neutralRange);

                row = new HtmlTableRow();
                cell = new HtmlTableCell();
                cell.InnerText = group.Key.ToString();
                row.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.Controls.Add(linearGauge);
                row.Cells.Add(cell);
                htmltable.Rows.Add(row);
            }

            this.modelChartArea.Controls.Add(htmltable);

            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

       

        public override void LoadPage()
        {
            base.LoadPage();

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
            {
                ((SetupMaster)Master).IsAdmin = "1";
            }

            UMABroker.ReleaseBrokerManagedComponent(objUser);
        }

        public override void PopulatePage(DataSet ds)
        {
            DataView modelListView = new DataView(ds.Tables[ModelsDS.MODELLIST]);
            modelListView.Sort = ModelsDS.MODELNAME + " ASC";

        }

        protected void BtnBackToListing(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Administration/AdministrationView.aspx");
        }


        #region Telerik Code


        protected void RadChart1ItemDataBound(object sender, ChartItemDataBoundEventArgs e)
        {
            if (e.SeriesItem == null) return;

            DataRowView dtView = e.DataItem as DataRowView;
            if (dtView != null)
            {
                e.SeriesItem.Label.TextBlock.Text = string.Format("{1} - {0}%", dtView["ProNeutralPercentage"], dtView[ModelsDS.PROMODELNAME]);
            }
        }

        protected void RadChart1Click(object sender, ChartClickEventArgs args)
        {
            if (args.SeriesItem == null) return;

            string assetID = args.SeriesItem.Name;
            if (assetID == "None")
            {
                RadChart1.Visible = false;
                return;
            }


            DataView assetView = new DataView(PresentationData.Tables[ModelsDS.MODELDETAILSLISTASSETS]);
            assetView.RowFilter = ModelsDS.ASSETMODELNAME + "= '" + assetID + "'";
            assetID = assetView.ToTable().Rows[0][ModelsDS.ASSETID].ToString();

            DataView view = new DataView(PresentationData.Tables[ModelsDS.MODELDETAILSLISTPRO]);
            view.Sort = ModelsDS.PRODESCRIPTION + " ASC";
            view.RowFilter = ModelsDS.ASSETID + "= '" + assetID + "' and " + "ProNeutralPercentage" + " > 0";

            var table = view.ToTable();
            
            //Adding New Row in case Sum is not equal to 100
            var sum = table.AsEnumerable().Sum(p => Double.Parse(p["ProNeutralPercentage"].ToString()));
            if (sum < 100)
            {
                var dr = table.NewRow();
                dr["ProNeutralPercentage"] = 100 - sum;
                dr[ModelsDS.PROMODELNAME] = "Other Assets";
                table.Rows.Add(dr);
            }
            RadChart1.ChartTitle.TextBlock.Text = assetView.ToTable().Rows[0][ModelsDS.ASSETMODELNAME].ToString();

            RadChart1.DataSource = table;
            RadChart1.Visible = true;
            RadChart1.DataBind();
        }

        protected void RadChartItemDataBound(object sender, ChartItemDataBoundEventArgs e)
        {
            if (e.SeriesItem == null) return;

            DataRowView dtView = e.DataItem as DataRowView;
            if (dtView != null)
            {
                e.SeriesItem.Label.TextBlock.Text = string.Format("{1} - {0}%", dtView["AssetNeutralPercentage"], dtView[ModelsDS.ASSETMODELNAME]);
            }
        }

        #endregion
    }

}

