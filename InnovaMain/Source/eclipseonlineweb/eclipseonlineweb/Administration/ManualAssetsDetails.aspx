﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="SetupMaster.master"
    AutoEventWireup="true" CodeBehind="ManualAssetsDetails.aspx.cs" Inherits="eclipseonlineweb.ManualAssetsDetails" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Chart"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="breadcrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function hintContent() {
            return this.data.label + '<br/> ' + this.y + '';
        }
        function hintContentPie() {
            return this.data.toString() + " : " + Globalize.format(this.value / this.total, "p2");
        }

    </script>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                                ID="btnDownload" />
                            <asp:ImageButton runat="server" ImageUrl="~/images/window_previous.png" Visible="false"
                                OnClick="BtnBackToListing" ID="btnBack" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:breadcrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblManAssets" Text="Manual Assets"></asp:Label>
                            <asp:Label Font-Bold="true" Visible="false" runat="server" ID="lblManAssetsID"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <asp:Panel runat="server" ID="pnlGrid">
                <telerik:RadGrid OnNeedDataSource="PriceHistoryGridNeedDataSource" ID="PriceHistoryGrid"
                    runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                    AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnItemCommand="PriceHistoryGridItemCommand"
                    GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                    AllowAutomaticUpdates="True" OnInsertCommand="PriceHistoryGridInsertCommand"
                    EnableViewState="true" ShowFooter="true">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                        Name="PriceHistory" TableLayout="Fixed" EditMode="EditForms" DataKeyNames="ManualHistoryID">
                        <CommandItemSettings AddNewRecordText="Add New Security" ShowExportToExcelButton="true"
                            ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                        <Columns>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridEditCommandColumn>
                            <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="ManualHistoryID"
                                HeaderText="ID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ManualHistoryID"
                                UniqueName="ID" Visible="false" ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridDateTimeColumn UniqueName="calDate" PickerType="DatePicker" HeaderText="Date"
                                DataField="Date" DataFormatString="{0:dd/MM/yyyy}">
                                <ItemStyle Width="20%" />
                            </telerik:GridDateTimeColumn>
                            <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="Currency" ItemStyle-Width="10%"
                                HeaderText="Currency" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Currency" UniqueName="txtCurrency"
                                DefaultInsertValue="AUD">
                            </telerik:GridBoundColumn>
                            <telerik:GridNumericColumn SortExpression="UnitPrice" DataField="UnitPrice" DataFormatString="{0:N6}"
                                UniqueName="txtUnitPrice" HeaderText="Unit Price" DecimalDigits="6" />
                            <telerik:GridNumericColumn SortExpression="NavPrice" DataField="NavPrice" DataFormatString="{0:N6}"
                                UniqueName="txtNavPrice" HeaderText="Nav Price" DecimalDigits="6" />
                            <telerik:GridNumericColumn SortExpression="PurPrice" DataField="PurPrice" DataFormatString="{0:N6}"
                                UniqueName="txtPurPrice" HeaderText="Pur Price" DecimalDigits="6" />
                            <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="TotalUnits" ItemStyle-Width="20%"
                                HeaderText="Total Units" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TotalUnits" UniqueName="txtTotalUnits"
                                ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="Value" ItemStyle-Width="20%"
                                HeaderText="Value" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Value" UniqueName="txtValue"
                                ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn ConfirmText="Are you sure? You want to delete this record?"
                                ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                    <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                        <Excel Format="Html"></Excel>
                    </ExportSettings>
                </telerik:RadGrid></asp:Panel>
            <p>
                <asp:Panel runat="server" ID="pnlChart">
                    <c1:C1LineChart ID="C1WebChart1" EnableViewState="false" ViewStateMode="Disabled"
                        Visible="true" runat="server" Height="200px" Width="100%" ShowChartLabels="false">
                        <SeriesTransition Duration="2000" />
                        <Hint>
                            <Content Function="hintContent" />
                        </Hint>
                        <Animation Duration="2000" />
                        <SeriesList>
                            <c1:LineChartSeries Label="SampleData" LegendEntry="True">
                                <Markers Visible="True">
                                </Markers>
                                <Data>
                                    <X DoubleValues="1, 2, 3, 4, 5" />
                                    <Y DoubleValues="20, 22, 19, 24, 25" />
                                </Data>
                            </c1:LineChartSeries>
                        </SeriesList>
                        <SeriesStyles>
                            <c1:ChartStyle Stroke="#4B6C9E" StrokeWidth="4" Opacity="0.8">
                            </c1:ChartStyle>
                        </SeriesStyles>
                        <SeriesHoverStyles>
                            <c1:ChartStyle Stroke="#4B6C9E" StrokeWidth="6" Opacity="0.9">
                            </c1:ChartStyle>
                        </SeriesHoverStyles>
                        <Footer Compass="South" Visible="False">
                        </Footer>
                        <Legend Visible="false"></Legend>
                        <Axis>
                            <Y Compass="West" Visible="False">
                                <Labels TextAlign="Center">
                                </Labels>
                                <GridMajor Visible="True">
                                </GridMajor>
                            </Y>
                        </Axis>
                        <DataBindings>
                            <c1:C1ChartBinding XField="Date" XFieldType="DateTime" YField="UnitPrice" YFieldType="Number" />
                        </DataBindings>
                    </c1:C1LineChart>
                </asp:Panel>
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
