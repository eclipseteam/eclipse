﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;
using System.Linq;
namespace eclipseonlineweb
{
    public partial class SecurityDividendsReport : UMABasePage
    {
        private void GetRequiredData()
        {
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var securityDetailsDS = new SecurityDetailsDS
                                        {
                                            DetailsGUID = new Guid(((SecurityDetailsMaster)Master).SecurityID),
                                            RequiredDataType = SecurityDetailTypes.DividendPushDownReport
                                        };
            if (organization != null) organization.GetData(securityDetailsDS);
            PresentationData = securityDetailsDS;
            UMABroker.ReleaseBrokerManagedComponent(organization);

            var view = new DataView(securityDetailsDS.Tables[securityDetailsDS.SecDividendsReportTable.TB_Name]) { Sort = securityDetailsDS.SecDividendsReportTable.RECORDDATE + " DESC" };
            lblCode.Text = securityDetailsDS.ASXCode;
            ((SecurityDetailsMaster)Master).SecurityMenu = SecuirtyMenu.SetSecurityMenu(lblCode.Text);
            lblSecurityDesc.Text = securityDetailsDS.Description;
            PresentationGrid.DataSource = view.ToTable();
        }

        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            GetRequiredData();
            var ds = PresentationData as SecurityDetailsDS;
            var excelDataset = new DataSet();
            excelDataset.Merge(PresentationData.Tables[ds.SecDividendsReportTable.TB_Name], true, MissingSchemaAction.Add);
            excelDataset.Tables[0].Columns.Remove(ds.SecDividendsReportTable.ID);
            ExcelHelper.ToExcel(excelDataset, lblCode.Text + "-" + lblSecurityDesc.Text + "-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", Page.Response);

        }

        public void ClearDividindsReport(object sender, EventArgs eventArgs)
        {
            var securityDetailsDS = new SecurityDetailsDS
            {
                DetailsGUID = new Guid(((SecurityDetailsMaster)Master).SecurityID),
                RequiredDataType = SecurityDetailTypes.DividendPushDownReport,
                CommandType = DatasetCommandTypes.Delete
                
            };

            SaveOrganizanition(securityDetailsDS);
           
           PresentationGrid.Rebind();
        }

        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            return objUser.UserType == UserType.Innova || objUser.Name == "Administrator";
        }

        public override void LoadPage()
        {
            if (!Page.IsPostBack && Request.Params["ID"] != null)
            {
                ((SecurityDetailsMaster)Master).SecurityID = Request.Params["ID"];
                base.LoadPage();
            }
            PresentationGrid.Columns.FindByUniqueNameSafe("RecordDate").HeaderTooltip = Tooltip.ExDividendDate;
            PresentationGrid.Columns.FindByUniqueNameSafe("PaymentDate").HeaderTooltip = Tooltip.PaymentDate;
            PresentationGrid.Columns.FindByUniqueNameSafe("BalanceDate").HeaderTooltip = Tooltip.BalanceDate;
            PresentationGrid.Columns.FindByUniqueNameSafe("BooksCloseDate").HeaderTooltip = Tooltip.BooksCloseDate;
            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
                sm.RegisterPostBackControl(btnDownload);
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetRequiredData();
        }
    }
}
