﻿using System;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.Utilities;

// ReSharper disable CheckNamespace
namespace eclipseonlineweb
// ReSharper restore CheckNamespace
{
    public partial class SetupMaster : MasterPage
    {
        public string IsAdmin
        {
            set
            {
                hfIsAdmin.Value = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Master != null) ((HiddenField)Master.FindControl("hfMainMenuText")).Value = "INVESTMENT";

            UMABroker = new CMBroker(Context.User, DBConnection.Connection.ConnectionString);
            UMABroker.SetStart();

            var objUser = (DBUser)UMABroker.GetBMCInstance(Page.User.Identity.Name, "DBUser_1_1");
            var dbUserDetailsDs = new DBUserDetailsDS();
            objUser.GetData(dbUserDetailsDs);

            if (Page.User.Identity.Name.ToLower() != "administrator")
            {
                if (objUser.UserType == UserType.Advisor)
                {
                    liFinancialModelsBulkEdit.Visible = false;
                    liAssets.Visible = false;
                    liProducts.Visible = false;
                    liFinancialInstitutions.Visible = false;
                }
            }
        }

        private static ICMBroker UMABroker
        {
            get
            {
                var brokerSlot = Thread.GetNamedDataSlot("Broker");

                return (ICMBroker)Thread.GetData(brokerSlot);
            }
            set
            {
                var brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (value == null)
                    Thread.FreeNamedDataSlot("Broker");

                Thread.SetData(brokerSlot, value);
            }
        }
    }
}