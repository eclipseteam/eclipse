﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;

namespace eclipseonlineweb
{
    public partial class StatusFlag : UMABasePage
    {
        private ICMBroker umaBroker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected override void GetBMCData()
        {
            IOrganization organization =
                this.umaBroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            StatusDS statusDs = new StatusDS();
            organization.GetData(statusDs);
            this.PresentationData = statusDs;
            umaBroker.ReleaseBrokerManagedComponent(organization);
        }
        public override void LoadPage()
        {
            base.LoadPage();
            DBUser objUser =
                (DBUser)umaBroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
            {
                ((SetupMaster)Master).IsAdmin = "1";
            }
        }
        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
        }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "details")
            {
            }
            else if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                String _name = gDataItem["Name"].ToString();
                String _type = gDataItem["Type"].ToString();
                StatusDS DS = new StatusDS();
                DS.CommandType = DatasetCommandTypes.Delete;
                DataTable dt = DS.Tables[StatusDS.TABLENAME];
                DataRow dr = dt.NewRow();
                dr[StatusDS.NAME] = _name;
                dr[StatusDS.TYPE] = _type;
                dt.Rows.Add(dr);

                base.SaveOrganizanition(DS);
                GetBMCData();
                this.PresentationGrid.Rebind();
            }
            else if (e.CommandName.ToLower() == "update")
            {
                StatusDS DS = new StatusDS();
                DS.CommandType = DatasetCommandTypes.Delete;
                DataTable dt = DS.Tables[StatusDS.TABLENAME];
                DataRow row = dt.NewRow();
                row[StatusDS.NAME] = ((TextBox)(((GridEditableItem)(e.Item))["txtName"].Controls[0])).Text;
                row[StatusDS.TYPE] = ((TextBox)(((GridEditableItem)(e.Item))["txtType"].Controls[0])).Text;
                dt.Rows.Add(row);
                DS.CommandType = DatasetCommandTypes.Update;
                SaveOrganizanition(DS);
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.PresentationGrid.Rebind();
            }
        }
        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e)
        {

        }
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e)
        {

        }
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e)
        {

        }
        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("StatusFlag".Equals(e.Item.OwnerTableView.Name))
            {
                umaBroker.SaveOverride = true;
                IOrganization iorg = this.umaBroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                StatusDS DS = new StatusDS();
                DS.CommandType = DatasetCommandTypes.Add;
                DataRow row = DS.Tables[StatusDS.TABLENAME].NewRow();
                row[StatusDS.NAME] = ((TextBox)(((GridEditableItem)(e.Item))["txtName"].Controls[0])).Text;
                row[StatusDS.TYPE] = ((TextBox)(((GridEditableItem)(e.Item))["txtType"].Controls[0])).Text;
                DS.Tables[StatusDS.TABLENAME].Rows.Add(row);

                iorg.SetData(DS);
                umaBroker.SetComplete();
                umaBroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.PresentationGrid.Rebind();
            }
        }
        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
        }
        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
        }
        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetBMCData();
            DataView View = new DataView(this.PresentationData.Tables[StatusDS.TABLENAME]);
            View.Sort = StatusDS.NAME + " ASC";
            this.PresentationGrid.DataSource = View.ToTable();
        }
    }
}
