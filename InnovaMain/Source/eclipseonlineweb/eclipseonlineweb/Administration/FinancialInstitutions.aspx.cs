﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Telerik.Web.UI;

namespace eclipseonlineweb
{
    public partial class FinancialInstitutions : UMABasePage
    {
        protected override void Intialise()
        {
            base.Intialise();
            InstitutionControl1.Canceled += () => OVER.Visible = InstituteModal.Visible = false;
            InstitutionControl1.RetainPopup += () => OVER.Visible = InstituteModal.Visible = true;
            InstitutionControl1.ValidationFailed += () => HideShowInstituteModel(true);
            InstitutionControl1.SaveData += (ds, cmdType) =>
            {
                SaveInstitute(ds, cmdType);
                HideShowInstituteModel(false);

            };
        }

        public void SaveInstitute(InstitutesDS institutesDs, DatasetCommandTypes cmdType)
        {
            institutesDs.CommandType = cmdType;
            SaveOrganizanition(institutesDs);
            radGrid1.Rebind();
        }
        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            if (!btnBack.Visible)
            {
                ConfigureExport(radGrid1);
                radGrid1.MasterTableView.ExportToExcel();
            }
        }

        public void ConfigureExport(RadGrid r)
        {
            r.ExportSettings.ExportOnlyData = true;
            r.ExportSettings.IgnorePaging = true;
            r.ExportSettings.OpenInNewWindow = true;
        }

        protected void BtnBackToListing(object sender, ImageClickEventArgs e)
        {
            InstitutionGrid.Visible = true;
            btnBack.Visible = false;
            lblTDTransactionListing.Text = "TD Rates";
        }


        private void HideShowInstituteModel(bool show)
        {
            OVER.Visible = InstituteModal.Visible = show;
            //InstitutionControl1.ClearEntity();
        }
        protected void lnkbtnAddInstitute_Click(object sender, EventArgs e)
        {
            InstitutionControl1.SetEntity(null);
            HideShowInstituteModel(true);
        }
        protected void GetInstituteData(object sender, GridNeedDataSourceEventArgs e)
        {
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            //ToDo:Need to create new DS fro Institues info
            radGrid1.DataSource = organization.Institution;
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        protected void InstituteGridItemCommand(object sender, GridCommandEventArgs e)
        {
            var dataItem = (e.Item as GridDataItem);
            switch (e.CommandName.ToLower())
            {
                case "detail":
                    if (dataItem != null)
                    {
                        string rowID = dataItem["ID"].Text;



                        var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                        InstitutionEntity inst = null;
                        if (organization != null)
                        {
                            inst = organization.Institution.FirstOrDefault(ss => ss.ID == new Guid(rowID));
                            UMABroker.ReleaseBrokerManagedComponent(organization);
                        }
                        InstitutionControl1.LoadControl();
                        InstitutionControl1.SetEntity(inst);
                        HideShowInstituteModel(true);
                    }
                    break;
                case "delete":
                    string rowid = dataItem["ID"].Text;
                    var InstDS = new InstitutesDS();
                    var row = InstDS.InstitutionsTable.NewRow();
                    row[InstDS.InstitutionsTable.ID] = rowid;
                    InstDS.InstitutionsTable.Rows.Add(row);
                    SaveInstitute(InstDS, DatasetCommandTypes.Delete);
                    if (InstDS.ExtendedProperties.Contains("Message"))
                        ScriptManager.RegisterStartupScript(InstitutionGrid, InstitutionGrid.GetType(), "ShowAlert", "alert(' " + InstDS.ExtendedProperties["Message"] + "')", true);
                    break;
                case "contactdetail":
                    Response.Redirect("InstitutionsContactDetail.aspx?ID=" + dataItem["ID"].Text);
                    break;
            }
        }
    }

}