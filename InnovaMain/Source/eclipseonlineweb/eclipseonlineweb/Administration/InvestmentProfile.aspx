﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/NoMenu.Master" AutoEventWireup="true" CodeBehind="InvestmentProfile.aspx.cs" Inherits="eclipseonlineweb.InvestmentProfile" %>

<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" Assembly="C1.Web.Wijmo.Controls.4, Version=4.0.20121.56, Culture=neutral, PublicKeyToken=9b75583953471eea" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer" TagPrefix="C1ReportViewer" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<%--<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>--%>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
<%--                        <td width="20%">
                            <asp:ImageButton  runat="server" ImageUrl="~/images/window_previous.png" AlternateText="Back to Listing"
                                ToolTip="Back to Listing" OnClick="btnBackToListing" ID="btnBack" />
                        </td>--%>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:breadcrumb id="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="Label1" Text="FUND PROFILE REPORT:     "></asp:Label>
                            <asp:Label Font-Bold="true" Font-Italic ="true" Font-color="blue" runat="server" ID="lblName" Text="Fund name"></asp:Label>
                        </td>
                        <td align="right">                                    
                            <telerik:RadComboBox ID="cmbFunds" Label="Select Fund:" runat="server"  EnableViewState="true" LabelWidth="160px" Width="600px" 
                                   Font-Bold="true" OnSelectedIndexChanged="cmbFunds_SelectedIndexChanged" AutoPostBack="true">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <div id="MainView">
                 <asp:Panel runat="server" ID="Performance" Visible="true">
            <C1ReportViewer:C1ReportViewer CollapseToolsPanel="true" Cache-Enabled="false" Cache-ShareBetweenSessions="false"
                FileName="InMemoryBasicTable" runat="server" ID="C1ReportViewer1" Height="550px"
                Width="100%" Zoom="75%">
            </C1ReportViewer:C1ReportViewer>
                 </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
