﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;
using System.Collections.Generic;

namespace eclipseonlineweb
{
    public partial class SecurityDetails : UMABasePage
    {
        protected override void Intialise()
        {
            base.Intialise();
            PriceDetails.Saved += x =>
            {
                ModalPopupExtender1.Hide();
                PresentationGrid.Rebind();
            };
            PriceDetails.Canceled += () => ModalPopupExtender1.Hide();
            PriceDetails.ValidationFailed += () => ModalPopupExtender1.Show();
            PriceDetails.SaveData += SaveOrganizanition;
        }

        private void GetData()
        {
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var securityDetailsDS = new SecurityDetailsDS
                                                      {
                                                          DetailsGUID = new Guid(((SecurityDetailsMaster)Master).SecurityID),
                                                          RequiredDataType = SecurityDetailTypes.HistoricalPrices
                                                      };

            if (organization != null) organization.GetData(securityDetailsDS);
            PresentationData = securityDetailsDS;
            UMABroker.ReleaseBrokerManagedComponent(organization);
            BindGirdNChart(securityDetailsDS);
        }

        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            GetData();
            var excelDataset = new DataSet();
            var ds = (PresentationData as SecurityDetailsDS);
            var securityHis = new DataView(PresentationData.Tables[ds.PriceTable.TB_Name]) { Sort = "Date ASC" };
            excelDataset.Merge(securityHis.ToTable(), true, MissingSchemaAction.Add);
            excelDataset.Tables[0].Columns.Remove(ds.PriceTable.ID);
            excelDataset.Tables[0].Columns.Remove("SecID");

            List<string> capitals = new List<string>();
            capitals.Add("Security Code: " + lblCode.Text);
            capitals.Add("Security Name: " + lblSecurityDesc.Text);
            capitals.Add("Export Date: " + DateTime.Today.ToString("dd-MMM-yyyy"));
            capitals.Add("");   // leave one empty line before grid.

            ExcelHelper.ToExcel(excelDataset, lblCode.Text + "-" + lblSecurityDesc.Text + "-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", Page.Response, capitals);

        }

        public override void LoadPage()
        {
            if (!Page.IsPostBack && Request.Params["ID"] != null)
                ((SecurityDetailsMaster)Master).SecurityID = Request.Params["ID"];

            base.LoadPage();

            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");

            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                btaddNew.Visible = false;
                PresentationGrid.Columns.FindByUniqueNameSafe("DeleteColumn").Visible = false;
            }

            UMABroker.ReleaseBrokerManagedComponent(objUser);

            ScriptManager sm = ScriptManager.GetCurrent(Page);

            if (sm != null)
                sm.RegisterPostBackControl(btnDownload);
        }

        private void BindGirdNChart(SecurityDetailsDS data)
        {
            var view = new DataView(data.Tables[data.PriceTable.TB_Name]) { Sort = data.PriceTable.DATE + " DESC" };
            lblCode.Text = data.ASXCode;
            ((SecurityDetailsMaster)Master).SecurityMenu = SecuirtyMenu.SetSecurityMenu(lblCode.Text);
            lblSecurityDesc.Text = data.Description;
            PresentationGrid.DataSource = view.ToTable();
            C1WebChart1.Axis.X.AnnoFormatString = "MMM-yy";
            C1WebChart1.DataSource = view.ToTable();
            C1WebChart1.DataBind();
        }

        protected void BtnAddNewPriceClicked(object sender, ImageClickEventArgs e)
        {
            PriceDetails.ShowFundControls(lblCode.Text.ToLower().StartsWith("iv"));
            PriceDetails.ClearEntity();
            ModalPopupExtender1.Show();
        }

        private void SavePriceData(Guid id, Guid secID, DatasetCommandTypes commandType)
        {
            var securitiesDS = new SecurityDetailsDS
                                   {
                                       CommandType = commandType,
                                       RequiredDataType = SecurityDetailTypes.HistoricalPrices,
                                       DetailsGUID = secID
                                   };
            DataRow dr = securitiesDS.Tables[securitiesDS.PriceTable.TB_Name].NewRow();
            dr[securitiesDS.PriceTable.ID] = id;
            dr[securitiesDS.PriceTable.SECID] = secID;
            securitiesDS.Tables[securitiesDS.PriceTable.TB_Name].Rows.Add(dr);
            SaveOrganizanition(securitiesDS);
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            switch (e.CommandName.ToLower())
            {
                case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var id = new Guid(dataItem["ID"].Text);
                    var secID = new Guid(dataItem["SecID"].Text);
                    SavePriceData(id, secID, DatasetCommandTypes.Delete);
                    PresentationGrid.Rebind();
                    break;
            }
        }
    }
}
