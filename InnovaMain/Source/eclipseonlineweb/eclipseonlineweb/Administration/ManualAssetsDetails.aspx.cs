﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class ManualAssetsDetails : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            return objUser.UserType == UserType.Innova || objUser.Name == "Administrator";
        }

        public override void LoadPage()
        {
            this.cid = Request.QueryString["ins"].ToString();
            GetPriceHistory(cid, true); 
        }

        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var manualAssetsDS = new ManualAssetsDS { IsDetails = true, ItemID = new Guid(lblManAssetsID.Text) };

            if (organization != null)
            {
                organization.GetData(manualAssetsDS);
                UMABroker.ReleaseBrokerManagedComponent(organization);
            }

            var excelDataset = new DataSet();
            excelDataset.Merge(manualAssetsDS.Tables[ManualAssetsDS.MANPRICEHISTORY], true, MissingSchemaAction.Add);
            excelDataset.Tables[0].Columns.Remove("ManualHistoryID");
            ExcelHelper.ToExcel(excelDataset, lblManAssets.Text + "-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", Page.Response);
        }

        protected void BtnBackToListing(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("ManualAssets.aspx");
        }

        public override void PopulatePage(DataSet ds)
        {
            var sm = ScriptManager.GetCurrent(Page);

            if (sm != null)
                sm.RegisterPostBackControl(btnDownload);
        }

  
        #region Telerik

        #region PriceHistoryGrid

        protected void PriceHistoryGridItemCommand(object sender, GridCommandEventArgs e)
        {
            var dataItem = (e.Item as GridDataItem);
            var gridEditForm = (e.Item as GridEditFormItem); 

            switch (e.CommandName.ToLower())
            {
                case "delete":
                    if (dataItem != null)
                    {
                        var id = new Guid(dataItem["ID"].Text);
                        ManualAssetsDS manualAssetDs = new ManualAssetsDS { IsDetails = true, ItemID = new Guid(lblManAssetsID.Text), DetailsGUID = id, CommandType = DatasetCommandTypes.Delete };
                        SaveOrganizanition(manualAssetDs);
                        PriceHistoryGrid.Rebind();
                    }
                    break;
                case "update":
                    if (gridEditForm != null)
                    {
                        var id = new Guid(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ManualHistoryID"].ToString());
                        ManualAssetsDS manualAssetsDS = new ManualAssetsDS { IsDetails = true, ItemID = new Guid(lblManAssetsID.Text), DetailsGUID = id, CommandType = DatasetCommandTypes.Update };
                        var dt = manualAssetsDS.Tables[ManualAssetsDS.MANPRICEHISTORY];
                        var dr = dt.NewRow();
                        var editableItem = (GridEditableItem)e.Item;

                        var calDate = (RadDatePicker)editableItem["calDate"].Controls[0];

                        var txtCurrency = (TextBox)editableItem["txtCurrency"].Controls[0];
                        var txtUnitPrice = (RadNumericTextBox)editableItem["txtUnitPrice"].Controls[0];
                        var txtNavPrice = (RadNumericTextBox)editableItem["txtNavPrice"].Controls[0];
                        var txtPurPrice = (RadNumericTextBox)editableItem["txtPurPrice"].Controls[0];

                        dr[ManualAssetsDS.MANHISID] = manualAssetsDS.DetailsGUID;
                        if (calDate.SelectedDate != null)
                        {
                            dr[ManualAssetsDS.DATE] = calDate.SelectedDate.Value;
                        }
                        else
                        {
                            calDate.Focus();
                            DisplayMessage(PriceHistoryGrid, "<font color=\"red\">Please Select a Date*</font>");
                            e.Canceled = true;
                            return;
                        }
                        dr[ManualAssetsDS.CURRENCY] = txtCurrency.Text;
                        if (!Validate(txtUnitPrice))
                        {
                            txtUnitPrice.Focus();
                            e.Canceled = true;
                            return;
                        }


                        double unitPrice = 0;
                        double navPrice = 0;
                        double purPrice = 0;

                        double.TryParse(txtUnitPrice.Text, out unitPrice);
                        double.TryParse(txtNavPrice.Text, out navPrice);
                        double.TryParse(txtPurPrice.Text, out purPrice);

                        dr[ManualAssetsDS.UNITPRICE] = unitPrice;
                        dr[ManualAssetsDS.NAVPRICE] = navPrice;
                        dr[ManualAssetsDS.PURPRICE] = purPrice;

                        dt.Rows.Add(dr);

                        manualAssetsDS.Tables.Remove(ManualAssetsDS.MANPRICEHISTORY);
                        manualAssetsDS.Tables.Add(dt);
                        SaveOrganizanition(manualAssetsDS);
                        PriceHistoryGrid.Rebind();
                    }
                    break;
            }
        }

        protected void PriceHistoryGridInsertCommand(object source, GridCommandEventArgs e)
        {
            if (!"PriceHistory".Equals(e.Item.OwnerTableView.Name)) return;
            var manualAssetsDS = new ManualAssetsDS
                                     {
                                         IsDetails = true,
                                         ItemID = new Guid(lblManAssetsID.Text),
                                         DetailsGUID = Guid.NewGuid()
                                     };
            var dt = manualAssetsDS.Tables[ManualAssetsDS.MANPRICEHISTORY];
            var dr = dt.NewRow();
            var editableItem = (GridEditableItem)e.Item;

            var calDate = (RadDatePicker)editableItem["calDate"].Controls[0];

            var txtCurrency = (TextBox)editableItem["txtCurrency"].Controls[0];
            var txtUnitPrice = (RadNumericTextBox)editableItem["txtUnitPrice"].Controls[0];
            var txtNavPrice = (RadNumericTextBox)editableItem["txtNavPrice"].Controls[0];
            var txtPurPrice = (RadNumericTextBox)editableItem["txtPurPrice"].Controls[0];
            
            dr[ManualAssetsDS.MANHISID] = manualAssetsDS.DetailsGUID;
            if (calDate.SelectedDate != null)
            {
                dr[ManualAssetsDS.DATE] = calDate.SelectedDate.Value;
            }
            else
            {
                calDate.Focus();
                DisplayMessage(PriceHistoryGrid, "<font color=\"red\">Please Select a Date*</font>");
                e.Canceled = true;
                return;
            }
            dr[ManualAssetsDS.CURRENCY] = txtCurrency.Text;
            if (!Validate(txtUnitPrice))
            {
                txtUnitPrice.Focus();
                e.Canceled = true;
                return;
            }

         
            double unitPrice = 0;
            double navPrice = 0;
            double purPrice = 0;
           
            double.TryParse(txtUnitPrice.Text, out unitPrice);
            double.TryParse(txtNavPrice.Text, out navPrice);
            double.TryParse(txtPurPrice.Text, out purPrice);
 
            dr[ManualAssetsDS.UNITPRICE] = unitPrice;
            dr[ManualAssetsDS.NAVPRICE] = navPrice;
            dr[ManualAssetsDS.PURPRICE] = purPrice;

            dt.Rows.Add(dr);

            manualAssetsDS.Tables.Remove(ManualAssetsDS.MANPRICEHISTORY);
            manualAssetsDS.Tables.Add(dt);
            manualAssetsDS.CommandType = DatasetCommandTypes.Add;

            SaveOrganizanition(manualAssetsDS);

            e.Canceled = true;
            e.Item.Edit = false;

            PriceHistoryGrid.Rebind();
        }

        protected void PriceHistoryGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var view = new DataView(this.PresentationData.Tables[ManualAssetsDS.MANPRICEHISTORY]) { Sort = ManualAssetsDS.DATE + " DESC" };
            this.PriceHistoryGrid.DataSource = view.ToTable(); 
        }

        #endregion

        #endregion

        #region Fill Lists
        public string[] InvestmentTypes = {
                                              "Artwork",
                                              "Investment Property" ,
                                              "Unit Trust",
                                              "Residential Property",
                                              "Collectables ",
                                              "Other"
                                          };

        public string[] StatusList = { "Active", "Closed", "Frozen", "No Buy", "No Sell" };

        readonly Dictionary<string, string> _ratingList = new Dictionary<string, string>{
                                              {"Buy","buy"} ,
                                              {"Hold","hold"},
                                              {"Sell","sell"},
                                              {"Strong Buy","strongbuy"} };
        #endregion

        private void GetPriceHistory(string id, bool needsBinding)
        {
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var manualAssetsDS = new ManualAssetsDS { IsDetails = true, ItemID = new Guid(id) };

            if (organization != null)
            {
                organization.GetData(manualAssetsDS);
                this.PresentationData = manualAssetsDS; 
                UMABroker.ReleaseBrokerManagedComponent(organization);
            }
        }

        private void DisplayMessage(RadGrid grid, string text)
        {
            grid.Controls.Add(new LiteralControl(text));
        }


        public bool Validate(RadNumericTextBox textBox)
        {
            bool result = true;
            if (string.IsNullOrEmpty(textBox.Text))
            {
                result = false;
                if (textBox.ID == "RNTB_txtUnitPrice")
                    DisplayMessage(PriceHistoryGrid, "<font color=\"red\">Please Enter Unit Price*</font>");
            }
            return result;
        }
    }
}
