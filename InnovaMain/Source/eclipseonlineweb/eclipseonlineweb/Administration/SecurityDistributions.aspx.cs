﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class SecurityDistributions : UMABasePage
    {
        protected override void Intialise()
        {
            base.Intialise();
            DistributionDetails.Saved += x =>
                                                  {
                                                      ModalPopupExtender1.Hide();
                                                      PresentationGrid.Rebind();
                                                  };
            DistributionDetails.Canceled += () => ModalPopupExtender1.Hide();
            DistributionDetails.ValidationFailed += () => ModalPopupExtender1.Show();
            DistributionDetails.SaveData += SaveOrganizanition;
        }

        private void GetRequiredData()
        {
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var securityDetailsDS = new SecurityDetailsDS
                                        {
                                            DetailsGUID = new Guid(((SecurityDetailsMaster)Master).SecurityID),
                                            RequiredDataType = SecurityDetailTypes.Distiribution
                                        };
            if (organization != null) organization.GetData(securityDetailsDS);
            PresentationData = securityDetailsDS;
            UMABroker.ReleaseBrokerManagedComponent(organization);

            var view = new DataView(securityDetailsDS.Tables[securityDetailsDS.DistributionTable.TB_Name]) { Sort = securityDetailsDS.DistributionTable.RECORDDATE + " DESC" };
            lblCode.Text = securityDetailsDS.ASXCode;
            ((SecurityDetailsMaster)Master).SecurityMenu = SecuirtyMenu.SetSecurityMenu(lblCode.Text);
            lblSecurityDesc.Text = securityDetailsDS.Description;
            PresentationGrid.DataSource = view.ToTable();
        }

        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            GetRequiredData();
            var ds = PresentationData as SecurityDetailsDS;
            var excelDataset = new DataSet();
            excelDataset.Merge(PresentationData.Tables[ds.DistributionTable.TB_Name], true, MissingSchemaAction.Add);
            excelDataset.Tables[0].Columns.Remove(ds.DistributionTable.ID);
            ExcelHelper.ToExcel(excelDataset, lblCode.Text + "-" + lblSecurityDesc.Text + "-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", Page.Response);
        }


        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            return true;
        }

        public override void LoadPage()
        {
            if (!Page.IsPostBack && Request.Params["ID"] != null)
            {
                ((SecurityDetailsMaster)Master).SecurityID = Request.Params["ID"];
                base.LoadPage();
            }

            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
                sm.RegisterPostBackControl(btnDownload);
        }

        private void Delete(Guid id, Guid secID)
        {
            RunCommand(id, secID, DatasetCommandTypes.Delete);
        }

        private void RunCommand(Guid id, Guid secID, DatasetCommandTypes command)
        {
            var bankTransactionDetailsDS = new SecurityDistributionsDetailsDS { SecID = secID, ID = id, CommandType = command };
            SaveOrganizanition(bankTransactionDetailsDS);
        }

        private void Process(Guid id, Guid secID)
        {
            RunCommand(id, secID, DatasetCommandTypes.Process);
        }

        protected void AddNewClick(object sender, EventArgs e)
        {
            DistributionDetails.SetEntity(Guid.Empty, new Guid(((SecurityDetailsMaster)Master).SecurityID), lblCode.Text, lblSecurityDesc.Text);
            ModalPopupExtender1.Show();
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetRequiredData();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            var id = new Guid(dataItem["ID"].Text);
            var secID = new Guid(dataItem["SecID"].Text);
            switch (e.CommandName.ToLower())
            {
                case "details":
                    DistributionDetails.SetEntity(id, secID, lblCode.Text, lblSecurityDesc.Text);
                    ModalPopupExtender1.Show();
                    break;
                case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    Delete(id, secID);
                    PresentationGrid.Rebind();
                    break;
                case "process":
                    Process(id, secID);
                    PresentationGrid.Rebind();
                    break;

            }
        }

        protected void btnProcessAllDistributions_Click(object sender, EventArgs e)
        {
            ProcessDistributionsForAllSecurities();
        }
        private void ProcessDistributionsForAllSecurities()
        {
            SecurityDistributionsDetailsDS distributionsDetailsDs = new SecurityDistributionsDetailsDS();
            distributionsDetailsDs.SecID = new Guid(((SecurityDetailsMaster)Master).SecurityID);
            distributionsDetailsDs.CommandType = DatasetCommandTypes.ProcessAll;
            this.SaveOrganizanition(distributionsDetailsDs);
        }
    }
}
