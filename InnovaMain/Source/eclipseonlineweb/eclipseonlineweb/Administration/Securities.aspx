﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="SetupMaster.master"
    AutoEventWireup="true" CodeBehind="Securities.aspx.cs" Inherits="eclipseonlineweb.Securities" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <script src="../Scripts/GridScripts.js" type="text/javascript"></script>
    <script type="text/javascript">
        //Resetting menu index
        localStorage['MenuIndex'] = '2';

        function CheckAll(checkBox) {
            CheckAllCheckBoxes(checkBox, "<%:SecurityGrid.ClientID %>", "chkSMAApproved");
        }

        function CheckSingle(colId) {
            CheckSingleCheckBox(colId, "<%:SecurityGrid.ClientID %>", "chkSMAApproved");
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server">
                <fieldset>
                    <table width="100%">
                        <tr>
                            <td width="80%" valign="top">
                                <telerik:RadButton runat="server" ID="btnDownload" OnClick="btnDownload_OnClick"
                                    ToolTip="Download">
                                    <ContentTemplate>
                                        <img src="../images/download.png" alt="" class="btnImageWithText" />
                                        <span class="riLabel">Download</span>
                                    </ContentTemplate>
                                </telerik:RadButton>
                                &nbsp;
                                <telerik:RadButton runat="server" ID="btnProcessAllDividend" OnClick="btnProcessAllDividend_Click"
                                    ToolTip="Process All Dividends">
                                    <ContentTemplate>
                                        <img src="../images/distribution.png" alt="" class="btnImageWithText" />
                                        <span class="riLabel">Process Dividends</span>
                                    </ContentTemplate>
                                </telerik:RadButton>
                                &nbsp;
                                <telerik:RadButton runat="server" ID="btnProcessAllDistributions" OnClick="btnProcessAllDistributions_Click"
                                    ToolTip="Process All Distributions">
                                    <ContentTemplate>
                                        <img src="../images/distribution.png" alt="" class="btnImageWithText" />
                                        <span class="riLabel">Process Distributions</span>
                                    </ContentTemplate>
                                </telerik:RadButton>
                                &nbsp;
                                <telerik:RadButton runat="server" ID="btnRemoveInvalidDistributions" OnClick="btnRemoveInvalidDistributions_Click"
                                    ToolTip="Remove Zero Value Distributions From Client">
                                    <ContentTemplate>
                                        <img src="../images/database.png" alt="" class="btnImageWithText" />
                                        <span class="riLabel">Remove 0 Value Distributions</span>
                                    </ContentTemplate>
                                </telerik:RadButton>
                                &nbsp;
                                <telerik:RadButton ID="btnPaging" runat="server" ButtonType="StandardButton" Text="Open Bulk Edit Mode"
                                    OnClick="btnPaging_OnClick" Height="22px" ForeColor="gray">
                                </telerik:RadButton>
                                &nbsp;
                                <telerik:RadButton runat="server" ID="btnBulkUpdate" ToolTip="Bulk Updater" OnClick="btnBulkUpdate_OnClick"
                                    Visible="False">
                                    <ContentTemplate>
                                        <img src="../images/Save-Icon.png" alt="" class="btnImageWithText" />
                                        <span class="riLabel">Save</span>
                                    </ContentTemplate>
                                </telerik:RadButton>
                            </td>
                            <td width="20%" class="breadcrumbgap">
                                <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                                <br />
                                <asp:Label Font-Bold="true" runat="server" ID="lblSecurities" Text="Securities"></asp:Label>
                                <asp:HiddenField ID="hfSecID" runat="server" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </asp:Panel>
            <br />
            <telerik:RadGrid OnNeedDataSource="SecurityGrid_NeedDataSource" ID="SecurityGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="SecurityGrid_DetailTableDataBind"
                OnItemCommand="SecurityGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="SecurityGrid_ItemUpdated" OnItemDeleted="SecurityGrid_ItemDeleted"
                OnItemInserted="SecurityGrid_ItemInserted" OnInsertCommand="SecurityGrid_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="SecurityGrid_ItemCreated"
                OnItemDataBound="SecurityGrid_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="Security" TableLayout="Fixed" EditMode="PopUp">
                    <CommandItemSettings AddNewRecordText="Add New Security" ShowExportToExcelButton="true"
                        ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="ID" HeaderText="ID" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="ID" UniqueName="ID" Display="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridEditCommandColumn>
                        <telerik:GridDropDownColumn HeaderText="Investment Type" SortExpression="InvestmentType"
                            UniqueName="cmbTransactionType" DataField="InvestmentType" Visible="false" ColumnEditorID="GridDropDownColumnEditorInvestmentType" />
                        <telerik:GridDropDownColumn HeaderText="Security Code" SortExpression="Code" UniqueName="cmbCategoryCode"
                            DataField="Code" Visible="false" ColumnEditorID="GridDropDownColumnEditorCategoryCode" />
                        <telerik:GridBoundColumn FilterControlWidth="70px" SortExpression="Code" HeaderStyle-Width="10%"
                            ItemStyle-Width="10%" HeaderText="Security Code" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Code" UniqueName="Code">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="Market" HeaderStyle-Width="35%"
                            ItemStyle-Width="35%" HeaderText="Market" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" Visible="false" DataField="Market"
                            UniqueName="txtMarket">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn HeaderText="Asset" SortExpression="Asset" UniqueName="cmbAsset"
                            DataField="Asset" Visible="false" ColumnEditorID="GridDropDownColumnEditorAsset" />
                        <telerik:GridDropDownColumn HeaderText="Rating" SortExpression="Rating" UniqueName="cmbRating"
                            DataField="Rating" Visible="false" ColumnEditorID="GridDropDownColumnEditorRating" />
                        <telerik:GridDropDownColumn HeaderText="Status" SortExpression="Status" UniqueName="cmbStatus"
                            DataField="Status" Visible="false" ColumnEditorID="GridDropDownColumnEditorStatus" />
                        <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="35%" ItemStyle-Width="35%"
                            SortExpression="Description" HeaderText="Company Name" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Description" UniqueName="Description">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="IsInCode" HeaderStyle-Width="35%"
                            ItemStyle-Width="30%" HeaderText="ISIN Code" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" Visible="false" DataField="IsInCode"
                            UniqueName="txtISINCode">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="UnitsHeld" HeaderStyle-Width="32%"
                            ItemStyle-Width="32%" HeaderText="Units Held" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" Visible="false" DataField="UnitsHeld"
                            UniqueName="txtUnitHeld">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn HeaderText="Pricing Source" SortExpression="PricingSource"
                            UniqueName="cmbPricingSource" DataField="PricingSource" Visible="false" ColumnEditorID="GridDropDownColumnEditorPricingSource" />
                        <telerik:GridDropDownColumn HeaderText="Distribution Type" SortExpression="DistributionType"
                            UniqueName="cmbDistributionType" DataField="DistributionType" Visible="false"
                            ColumnEditorID="GridDropDownColumnEditorDistributionType" />
                        <telerik:GridDropDownColumn HeaderText="Distribution Frequency" SortExpression="DistributionFrequency"
                            UniqueName="cmbDistributionFrequency" DataField="DistributionFrequency" Visible="false"
                            ColumnEditorID="GridDropDownColumnEditorDistributionFrequency" />
                        <telerik:GridDateTimeColumn FilterControlWidth="95px" HeaderStyle-Width="12%" ItemStyle-Width="12%"
                            UniqueName="calDate" PickerType="DatePicker" HeaderText="Price Date" DataField="PriceDate"
                            DataFormatString="{0:dd/MM/yyyy}">
                            <ItemStyle Width="120px" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                            SortExpression="UnitPrice" HeaderText="Unit Price" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="false" HeaderButtonType="TextButton"
                            DataField="UnitPrice" UniqueName="UnitPrice" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn HeaderStyle-Width="6%" SortExpression="IsSMAApproved"
                            HeaderText="Super Approved" DataField="IsSMAApproved" UniqueName="IsSMAApproved"
                            AutoPostBackOnFilter="true" ShowFilterIcon="false" />
                        <telerik:GridNumericColumn HeaderStyle-Width="9%" FilterControlWidth="65px" NumericType="Percent"
                            SortExpression="SMAHoldingLimit" HeaderText="Super Holding Limit" DataField="SMAHoldingLimit"
                            UniqueName="SMAHoldingLimit" AutoPostBackOnFilter="true" DataFormatString="{0}%">
                        </telerik:GridNumericColumn>
                        <telerik:GridTemplateColumn HeaderStyle-Width="8%" UniqueName="ChkIsSMAApproved"
                            AllowFiltering="True" DataField="IsSMAApproved" HeaderText="Super Approved" Visible="False">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSMAApproved" runat="server" onclick="CheckSingle('ChkIsSMAApproved');"
                                    Checked='<%# Eval("IsSMAApproved") %>' />
                            </ItemTemplate>
                            <HeaderTemplate>
                                <asp:CheckBox ID="headerChkbox" Text="Super Approved" runat="server" onclick="CheckAll(this);"
                                    ToolTip="Select All" />
                            </HeaderTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderStyle-Width="10%" UniqueName="txtSMAHoldingLimit"
                            AllowFiltering="True" DataField="SMAHoldingLimit" HeaderText="Super Holding Limit"
                            FilterControlWidth="70px" Visible="False">
                            <ItemTemplate>
                                <telerik:RadNumericTextBox ID="txtSMAHoldingLimit" runat="server" Value='<%# Eval("SMAHoldingLimit") %>'
                                    Type="Percent" MinValue="0" MaxValue="100" Width="75px">
                                </telerik:RadNumericTextBox>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridButtonColumn ConfirmText="Delete the selected security?" ButtonType="ImageButton"
                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                        <telerik:GridButtonColumn CommandName="Details" Text="Details" ButtonType="LinkButton"
                            HeaderStyle-Width="10%" ItemStyle-Width="10%">
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="Html"></Excel>
                </ExportSettings>
            </telerik:RadGrid>
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorInvestmentType"
                runat="server" DropDownStyle-Width="250px" />
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorAsset" runat="server"
                DropDownStyle-Width="250px" />
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorRating" runat="server"
                DropDownStyle-Width="250px" />
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorStatus" runat="server"
                DropDownStyle-Width="250px" />
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorCategoryCode" runat="server"
                DropDownStyle-Width="250px" />
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorPricingSource"
                runat="server" DropDownStyle-Width="250px" />
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorDistributionType"
                runat="server" DropDownStyle-Width="250px" />
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorDistributionFrequency"
                runat="server" DropDownStyle-Width="250px" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDownload" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
