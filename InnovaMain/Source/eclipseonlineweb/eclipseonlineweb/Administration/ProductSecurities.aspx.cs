﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Data;
using System.Web.UI.WebControls;
using System.Linq;

namespace eclipseonlineweb
{
    public partial class ProductSecurities : UMABasePage
    {
        private DataView _proListView;
        bool _isExportMode;
        protected override void GetBMCData()
        {
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var proSecuritiesDS = new PROSecuritiesDS();
            if (organization != null) organization.GetData(proSecuritiesDS);
            PresentationData = proSecuritiesDS;
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        protected void btnBack_OnClick(object sender, EventArgs e)
        {
            pnlMainGrid.Visible = true;
            pnlDetails.Visible = false;
            btnBack.Visible = false;
            lblProSecurities.Text = "Product Securities";
        }

        public override void LoadPage()
        {
            base.LoadPage();
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");

            if (objUser.UserType != UserType.Innova || objUser.Name.ToLower() != "administrator")
            {
                PresentationDetailGrid.Columns.FindByUniqueNameSafe("BuyPrice").Visible = false;
                PresentationDetailGrid.Columns.FindByUniqueNameSafe("SellPrice").Visible = false;
            }

            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
            {
                var setupMaster = (SetupMaster)Master;
                if (setupMaster != null) setupMaster.IsAdmin = "1";
            }
        }

        public override void PopulatePage(DataSet ds)
        {
            _proListView = new DataView(ds.Tables[PROSecuritiesDS.PROSECLISTTABLE]) { Sort = PROSecuritiesDS.NAME + " ASC" };
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            PresentationGrid.DataSource = _proListView.ToTable();
        }

        protected void PresentationGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ("ProductSecurities".Equals(e.Item.OwnerTableView.Name))
            {
                if ((e.Item is GridEditFormItem) && (e.Item.IsInEditMode))
                {
                    GridEditFormItem editItem = (GridEditFormItem)e.Item;
                    GridEditManager editMan = editItem.EditManager;
                    TextBox txtbx = (TextBox)editItem["Description"].Controls[0];
                    txtbx.Width = Unit.Pixel(400);
                }
            }
            else if ("ProductSecuritiesDetails".Equals(e.Item.OwnerTableView.Name))
            {
                if ((e.Item is GridEditFormItem) && (e.Item.IsInEditMode))
                {
                    GridEditFormItem editItem = (GridEditFormItem)e.Item;
                    GridEditManager editMan = editItem.EditManager;
                    var ibmc = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

                    GridDropDownListColumnEditor ddlSec = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("DDLSecList"));
                    RadComboBox ddlSecRadCombo = ddlSec.ComboBoxControl;
                    ddlSecRadCombo.Filter = RadComboBoxFilter.Contains;
                    ddlSecRadCombo.DataSource = ibmc.Securities.OrderBy(secEnt => secEnt.CompanyName);
                    ddlSecRadCombo.DataValueField = "AsxCode";
                    ddlSecRadCombo.DataTextField = "CompanyName";
                    ddlSecRadCombo.DataBind();

                    GridTextBoxColumnEditor errorMessage = (GridTextBoxColumnEditor)(editMan.GetColumnEditor("ErrorMessage"));
                    errorMessage.TextBoxControl.ReadOnly = true;
                    errorMessage.TextBoxControl.Text = "";

                    string secTxt = String.Empty;

                    if (((Telerik.Web.UI.GridEditFormItem)(e.Item)).ParentItem != null)
                    {
                        secTxt = ((Telerik.Web.UI.GridEditFormItem)(e.Item)).ParentItem["Name"].Text;
                    }

                    if (secTxt != string.Empty)
                    {
                        ddlSec.ComboBoxControl.Items.FindItemByValue(secTxt).Selected = true;
                        ddlSec.ComboBoxControl.Enabled = false;
                    }
                }
            }
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if ("ProductSecurities".Equals(e.Item.OwnerTableView.Name))
            {
                switch (e.CommandName.ToLower())
                {
                    case "details":
                        {
                            var dataItem = (GridDataItem)e.Item;
                            e.Item.Edit = false;
                            e.Canceled = true;
                            pnlMainGrid.Visible = false;
                            pnlDetails.Visible = true;
                            string rowID = dataItem["ID"].Text;
                            lblProSecurities.Text = dataItem["Name"].Text + " - " + dataItem["Description"].Text;
                            lblProSecuritiesID.Text = rowID;
                            pnlMainGrid.Visible = false;
                            pnlDetails.Visible = true;
                            btnBack.Visible = true;
                            PresentationDetailGrid.Rebind();
                            break;
                        }
                    case "update":
                        {
                            var dataItem = (GridEditFormItem)e.Item;
                            string rowID = ((TextBox)dataItem["ID"].Controls[0]).Text;

                            IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                            ProductSecuritiesEntity productSecuritiesEntity = iorg.ProductSecurities.Where(proSec => proSec.ID.ToString() == rowID).FirstOrDefault();
                            if (productSecuritiesEntity != null)
                            {
                                UMABroker.SaveOverride = true;
                                productSecuritiesEntity.Name = ((TextBox)(((GridEditableItem)(e.Item))["Name"].Controls[0])).Text;
                                productSecuritiesEntity.Description = ((TextBox)(((GridEditableItem)(e.Item))["Description"].Controls[0])).Text;
                                iorg.CalculateToken(true);
                                UMABroker.SetComplete();
                                this.Response.Redirect(this.Request.Url.AbsoluteUri);
                            }
                            break;
                        }
                }
            }
            else if ("ProductSecuritiesDetails".Equals(e.Item.OwnerTableView.Name))
            {
                switch (e.CommandName.ToLower())
                {
                    case "update":
                        {
                            GridEditFormItem EditForm = (GridEditFormItem)e.Item;
                            RadComboBox comboSecLit = (RadComboBox)EditForm["DDLSecList"].Controls[0];
                            IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                            var productSecuritiesEntity = iorg.ProductSecurities.Where(prosec => prosec.ID.ToString() == lblProSecuritiesID.Text).FirstOrDefault();
                            if (productSecuritiesEntity != null)
                            {
                                if (comboSecLit.SelectedItem != null)
                                {
                                    UMABroker.SaveOverride = true;
                                    var securityEntity = iorg.Securities.Where(sec => sec.AsxCode == comboSecLit.SelectedItem.Value).FirstOrDefault();
                                    var productSecurityDetail = productSecuritiesEntity.Details.Where(detail => detail.SecurityCodeId == securityEntity.ID).FirstOrDefault();
                                    productSecurityDetail.BuyPrice = ((RadNumericTextBox)(((GridEditableItem)(e.Item))["BuyPrice"].Controls[0])).Value.Value;
                                    productSecurityDetail.SellPrice = ((RadNumericTextBox)(((GridEditableItem)(e.Item))["SellPrice"].Controls[0])).Value.Value;
                                    productSecurityDetail.Allocation = ((RadNumericTextBox)(((GridEditableItem)(e.Item))["Allocation"].Controls[0])).Value.Value;
                                    productSecurityDetail.IsDeactive = ((CheckBox)(((GridEditableItem)(e.Item))["IsDeactive"].Controls[0])).Checked;
                                    iorg.CalculateToken(true);
                                    UMABroker.SetComplete();
                                    UMABroker.SetStart();
                                    e.Canceled = true;
                                    e.Item.Edit = false;
                                    GetBMCData();
                                    this.PresentationDetailGrid.Rebind();
                                }
                            }
                            break;
                        }
                }
            }
            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName)
            {
                PresentationGrid.MasterTableView.GetColumn("EditCommandColumn2").Visible = false;
                PresentationGrid.MasterTableView.GetColumn("DeleteColumn2").Visible = false;
                PresentationGrid.MasterTableView.GetColumn("Name").HeaderStyle.Width = Unit.Pixel(240);
                PresentationGrid.MasterTableView.GetColumn("Description").HeaderStyle.Width = Unit.Pixel(300);
                _isExportMode = true;
                PresentationGrid.Rebind();
            }
        }

        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            GridEditFormInsertItem EditForm = (GridEditFormInsertItem)e.Item;

            if ("ProductSecurities".Equals(e.Item.OwnerTableView.Name))
            {
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                ProductSecuritiesEntity productSecuritiesEntity = new ProductSecuritiesEntity();
                productSecuritiesEntity.ID = Guid.NewGuid();
                productSecuritiesEntity.Name = ((TextBox)(((GridEditableItem)(e.Item))["Name"].Controls[0])).Text;
                productSecuritiesEntity.Description = ((TextBox)(((GridEditableItem)(e.Item))["Description"].Controls[0])).Text;
                iorg.ProductSecurities.Add(productSecuritiesEntity);
                iorg.CalculateToken(true);
                UMABroker.SetComplete();
                this.Response.Redirect(this.Request.Url.AbsoluteUri);
            }
            else if ("ProductSecuritiesDetails".Equals(e.Item.OwnerTableView.Name))
            {
                RadComboBox comboSecLit = (RadComboBox)EditForm["DDLSecList"].Controls[0];
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                var productSecuritiesEntity = iorg.ProductSecurities.Where(prosec => prosec.ID.ToString() == lblProSecuritiesID.Text).FirstOrDefault();
                if (productSecuritiesEntity != null)
                {
                    SecuritiesEntity securityEntity = null;

                    if (comboSecLit.SelectedItem != null && comboSecLit.SelectedItem.Value != string.Empty)
                        securityEntity = iorg.Securities.Where(sec => sec.AsxCode == comboSecLit.SelectedItem.Value).FirstOrDefault();

                    var existingProductSecurityDetail = productSecuritiesEntity.Details.Where(detail => detail.SecurityCodeId == securityEntity.ID).FirstOrDefault();

                    if (existingProductSecurityDetail == null)
                    {

                        UMABroker.SaveOverride = true;
                        ProductSecurityDetail productSecurityDetail = new ProductSecurityDetail();
                        if (securityEntity != null)
                        {
                            productSecurityDetail.MarketCode = "ASX";
                            productSecurityDetail.SecurityCodeId = securityEntity.ID;
                        }

                        productSecurityDetail.DetailId = productSecuritiesEntity.ID;
                        productSecurityDetail.BuyPrice = ((RadNumericTextBox)(((GridEditableItem)(e.Item))["BuyPrice"].Controls[0])).Value.Value;
                        productSecurityDetail.SellPrice = ((RadNumericTextBox)(((GridEditableItem)(e.Item))["SellPrice"].Controls[0])).Value.Value;
                        productSecurityDetail.Allocation = ((RadNumericTextBox)(((GridEditableItem)(e.Item))["Allocation"].Controls[0])).Value.Value;
                        productSecurityDetail.IsDeactive = ((CheckBox)(((GridEditableItem)(e.Item))["IsDeactive"].Controls[0])).Checked;
                        productSecurityDetail.Rating = "Dynamic";
                        productSecurityDetail.RatingOptions = "SellToWeighting";

                        productSecuritiesEntity.Details.Add(productSecurityDetail);
                        iorg.CalculateToken(true);
                        UMABroker.SetComplete();
                        UMABroker.SetStart();
                        e.Canceled = true;
                        e.Item.Edit = false;
                        GetBMCData();
                        this.PresentationDetailGrid.Rebind();
                    }
                    else
                    {
                        TextBox errorMessage = (TextBox)EditForm["ErrorMessage"].Controls[0];
                        errorMessage.Text = "Error: Security is already configured";
                    }
                }
            }
        }

        protected void PresentationDetailGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var proSecuritiesDS = new PROSecuritiesDS { GetDetails = true, DetailsGUID = new Guid(lblProSecuritiesID.Text) };
            if (organization != null) organization.GetData(proSecuritiesDS);
            var proListDetailView = new DataView(proSecuritiesDS.Tables[PROSecuritiesDS.PROSECDETAILTABLE]) { Sort = PROSecuritiesDS.MARKETCODE + " ASC" };
            UMABroker.ReleaseBrokerManagedComponent(organization);

            PresentationDetailGrid.DataSource = proListDetailView.ToTable();
        }

        protected void PresentationGrid_OnItemCreated(object sender, GridItemEventArgs e)
        {
            if (_isExportMode && e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                foreach (TableCell cell in item.Cells)
                {
                    if (cell.Controls.Count > 0)
                    {
                        for (int i = 0; i < cell.Controls.Count; i++)
                        {
                            if (cell.Controls[i] is LiteralControl)
                            {
                                (cell.Controls[i] as LiteralControl).Text = (cell.Controls[i] as LiteralControl).Text.Replace("<td>", "<td style=\"text-align:left\">");
                            }
                        }
                    }
                }
            }

            if (_isExportMode && e.Item is GridHeaderItem)
            {
                foreach (TableCell cell in e.Item.Cells)
                {
                    cell.Style["text-align"] = "left";
                }
            }
        }

        protected void PresentationDetailGrid_OnItemCreated(object sender, GridItemEventArgs e)
        {
            if (_isExportMode && e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                foreach (TableCell cell in item.Cells)
                {
                    if (cell.Controls.Count > 0)
                    {
                        for (int i = 0; i < cell.Controls.Count; i++)
                        {
                            if (cell.Controls[i] is LiteralControl)
                            {
                                (cell.Controls[i] as LiteralControl).Text = (cell.Controls[i] as LiteralControl).Text.Replace("<td>", "<td style=\"text-align:left\">");
                            }
                        }
                    }
                }
            }

            if (_isExportMode && e.Item is GridHeaderItem)
            {
                foreach (TableCell cell in e.Item.Cells)
                {
                    cell.Style["text-align"] = "left";
                }
            }
        }
        
        private void ConfigureExportPresentation()
        {
            PresentationGrid.ExportSettings.ExportOnlyData = true;
            PresentationGrid.ExportSettings.IgnorePaging = true;
            PresentationGrid.ExportSettings.OpenInNewWindow = true;
            PresentationGrid.MasterTableView.GetColumn("EditCommandColumn2").Visible = false;
            PresentationGrid.MasterTableView.GetColumn("DeleteColumn2").Visible = false;
            PresentationGrid.MasterTableView.GetColumn("Name").HeaderStyle.Width = Unit.Pixel(240);
            PresentationGrid.MasterTableView.GetColumn("Description").HeaderStyle.Width = Unit.Pixel(300);
        }

        private void ConfExportPresentationDetail()
        {
            PresentationDetailGrid.ExportSettings.ExportOnlyData = true;
            PresentationDetailGrid.ExportSettings.IgnorePaging = true;
            PresentationDetailGrid.ExportSettings.OpenInNewWindow = true;
            PresentationDetailGrid.MasterTableView.GetColumn("EditCommandColumn2").Visible = false;
        }

        protected void btnExcel_OnClick(object sender, EventArgs e)
        {
            _isExportMode = true;
            if (pnlMainGrid.Visible)
            {
                ConfigureExportPresentation();
                PresentationGrid.MasterTableView.ExportToExcel();
            }
            else
            {
                var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                var proSecuritiesDs = new PROSecuritiesDS
                {
                    GetDetails = true,
                    DetailsGUID = new Guid(lblProSecuritiesID.Text)
                };
                if (organization != null)
                {
                    organization.GetData(proSecuritiesDs);
                    UMABroker.ReleaseBrokerManagedComponent(organization);
                    var excelDataset = new DataSet();
                    excelDataset.Merge(proSecuritiesDs.Tables[PROSecuritiesDS.PROSECDETAILTABLE], true, MissingSchemaAction.Add);
                    ExcelHelper.ToExcel(excelDataset, lblProSecurities.Text + "-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", Page.Response);
                }
            }
        }

        protected void btnPDF_OnClick(object sender, EventArgs e)
        {
            _isExportMode = true;
            if (pnlMainGrid.Visible)
            {
                ConfigureExportPresentation();
                foreach (GridColumn col in PresentationGrid.MasterTableView.RenderColumns)
                {
                    col.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                    col.HeaderStyle.VerticalAlign = VerticalAlign.Middle;
                    col.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                    col.ItemStyle.VerticalAlign = VerticalAlign.Middle;
                }
                PresentationGrid.MasterTableView.ExportToPdf();
            }
            else
            {
                ConfExportPresentationDetail();
                PresentationDetailGrid.ExportSettings.Pdf.PageHeight = Unit.Parse("162mm");
                PresentationDetailGrid.ExportSettings.Pdf.PageWidth = Unit.Parse("600mm");
                foreach (GridColumn col in PresentationDetailGrid.MasterTableView.RenderColumns)
                {
                    col.HeaderStyle.Width = Unit.Pixel(160);
                    col.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                    col.HeaderStyle.VerticalAlign = VerticalAlign.Middle;
                    col.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                    col.ItemStyle.VerticalAlign = VerticalAlign.Middle;
                }
                PresentationDetailGrid.MasterTableView.ExportToPdf();
            }
        }
    }
}
