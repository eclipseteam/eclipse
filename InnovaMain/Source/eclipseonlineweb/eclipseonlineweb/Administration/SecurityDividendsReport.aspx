﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="SecurityDetailsMaster.master"
    AutoEventWireup="true" CodeBehind="SecurityDividendsReport.aspx.cs" Inherits="eclipseonlineweb.SecurityDividendsReport" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                                ID="btnDownload" />
                             <a href="Securities.aspx"><img src="../images/window_previous.png" border="0" style="border: 0 !important;" /></a>
                                              
                                <telerik:RadButton runat="server" ID="btnDeleteReportCollection" OnClick="ClearDividindsReport" OnClientClick="return confirm('Are you certain you want to delete whole report records?');"
                                    ToolTip=">Delete Report Records">
                                    <ContentTemplate>
                                        <img src="../images/cross.png" alt="" class="btnImageWithText" />
                                        <span class="riLabel">Delete Report Records</span>
                                    </ContentTemplate>
                                </telerik:RadButton>

                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblCode" Text=""></asp:Label>
                            <asp:Label Font-Bold="true" runat="server" ID="Label1" Text="-"></asp:Label>
                            <asp:Label Font-Bold="true" runat="server" ID="lblSecurityDesc" Text="Description"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="Banks" TableLayout="Fixed">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="ID" UniqueName="ID" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="SecID" ReadOnly="true" HeaderText="SecID"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="SecID" UniqueName="SecID" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" HeaderStyle-Width="7%" SortExpression="ClientIdentification"
                            ReadOnly="true" HeaderText="Client ID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ClientIdentification"
                            UniqueName="ClientIdentification">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80%" HeaderStyle-Width="14%" SortExpression="ClientName"
                            ReadOnly="true" HeaderText="Client Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ClientName" UniqueName="ClientName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80%" HeaderStyle-Width="19%" SortExpression="AccountName"
                            HeaderText="Account Name" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountName" UniqueName="AccountName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="AccountNumber"
                            ShowFilterIcon="true" HeaderText="Account Number" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" HeaderButtonType="TextButton" DataField="AccountNumber"
                            UniqueName="AccountNumber">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" HeaderStyle-Width="7%" ReadOnly="true"
                            SortExpression="RecordDate" HeaderText="Ex-Dividend Date" AutoPostBackOnFilter="true"
                            ShowFilterIcon="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                            DataField="RecordDate" UniqueName="RecordDate" DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" HeaderStyle-Width="7%" ReadOnly="true"
                            SortExpression="PaymentDate" HeaderText="Payment Date" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="PaymentDate" UniqueName="PaymentDate" DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" SortExpression="UnitsOnHand" ReadOnly="true"
                            HeaderText="Units On Hand" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UnitsOnHand" UniqueName="UnitsOnHand">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" SortExpression="CentsPerShare"
                            ReadOnly="true" HeaderText="Cents Per Share" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CentsPerShare"
                            UniqueName="CentsPerShare" DataFormatString="{0:N4}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" SortExpression="PaidDividend" ReadOnly="true"
                            HeaderText="Paid Dividend" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="PaidDividend"
                            UniqueName="PaidDividend" DataFormatString="{0:N4}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" HeaderStyle-Width="7%" SortExpression="BalanceDate"
                            ReadOnly="true" HeaderText="Balance Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BalanceDate" UniqueName="BalanceDate"
                            DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" SortExpression="DividendType" ReadOnly="true"
                            HeaderText="Dividend Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="DividendType"
                            UniqueName="DividendType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" SortExpression="Currency" ReadOnly="true"
                            HeaderText="Currency (Ccy)" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Currency" UniqueName="Currency">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70%" HeaderStyle-Width="7%" SortExpression="BooksCloseDate"
                            ReadOnly="true" HeaderText="Books Close Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BooksCloseDate"
                            UniqueName="BooksCloseDate" DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
