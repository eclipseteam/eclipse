﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using System.Linq;

namespace eclipseonlineweb.Administration
{
    public partial class AtCallRates : UMABasePage
    {
        private void GetData()
        {
            IBrokerManagedComponent iBMC = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            var ds = new OrderPadAtCallRatesDS { CommandType = DatasetCommandTypes.GetAll };
            iBMC.GetData(ds);
            UMABroker.ReleaseBrokerManagedComponent(iBMC);
            PresentationGrid.DataSource = ds.OrderPadAtCallRatesTable;
        }

        #region Telerik Code

        protected void PresentationGridItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
                if (!(Convert.ToBoolean(((DataRowView)(e.Item.DataItem)).Row["IsEditable"].ToString())))
                    e.Item.Edit = false;

            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var editedItem = (GridEditableItem)e.Item;
                GridEditManager editMan = editedItem.EditManager;

                var orgCM = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                var inst = orgCM.Institution;

                var brokerList = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("BrokerEditor"));
                var bankList = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("BankEditor"));

                //Broker
                brokerList.ComboBoxControl.Filter = RadComboBoxFilter.Contains;
                brokerList.DataSource = inst.Where(ss => ss.Name.ToLower() == "fiig" || ss.Name.ToLower() == "amm").ToList();
                brokerList.DataValueField = "ID";
                brokerList.DataTextField = "Name";
                brokerList.DataBind();


                GridEditableItem edititem = (GridEditableItem)e.Item;
                RadComboBox cmboBrokerName = (RadComboBox)edititem["BrokerEditor"].Controls[0];
                cmboBrokerName.Width = 200;


                TextBox cmboInstituteName = (TextBox)edititem["PriceInstituteName"].Controls[0];
                cmboInstituteName.Width = 200;

                TextBox txtAccountTier = (TextBox)edititem["AccountTier"].Controls[0];
                txtAccountTier.Width = 196;

                RadNumericTextBox txtMin = (RadNumericTextBox)edititem["Min"].Controls[0];
                txtMin.Width = 200;

                RadNumericTextBox txtMax = (RadNumericTextBox)edititem["Max"].Controls[0];
                txtMax.Width = 200;

                TextBox txtRate = (TextBox)e.Item.FindControl("Rate");
                txtRate.Width = 196;

                RadDatePicker dtpApplicableFrom = (RadDatePicker)edititem["ApplicableFrom"].Controls[0];
                dtpApplicableFrom.Width = 200;

                RadNumericTextBox txtHoneymoonperiod = (RadNumericTextBox)e.Item.FindControl("HoneymoonPeriod");
                txtHoneymoonperiod.Width = 200;

                bankList.ComboBoxControl.Filter = RadComboBoxFilter.Contains;
                inst.Sort((a, b) => a.Name.CompareTo(b.Name));
                bankList.DataSource = inst;
                bankList.DataValueField = "ID";
                bankList.DataTextField = "Name";
                bankList.DataBind();
                bankList.DropDownStyle.Width = 250;

                if (!(e.Item.DataItem is GridInsertionObject))
                {
                    string brokerID = ((DataRowView)(e.Item.DataItem)).Row["InstituteID"].ToString();
                    if (brokerID != string.Empty)
                    {
                        RadComboBoxItem item = brokerList.ComboBoxControl.FindItemByValue(brokerID);
                        if (item != null)
                            item.Selected = true;
                        brokerList.ComboBoxControl.Enabled = false;
                    }

                    string bankID = ((DataRowView)(e.Item.DataItem)).Row["PriceInstituteID"].ToString();
                    if (bankID != string.Empty)
                    {
                        RadComboBoxItem item = bankList.ComboBoxControl.FindItemByValue(bankID);
                        if (item != null)
                            item.Selected = true;
                    }

                    //var rate = (TextBox)editedItem["Rate"].Controls[1];
                    //rate.Text = ((DataRowView)(e.Item.DataItem)).Row["lblRate"].ToString();

                    var lstApplicability = (RadListBox)editedItem["Applicability"].FindControl("lstApplicability");
                    var applicability = ((DataRowView)(e.Item.DataItem)).Row["Applicability"].ToString().Split(',');
                    foreach (var item in applicability)
                    {
                        if (item.Trim().Length > 0)
                            lstApplicability.FindItemByText(item.Trim()).Checked = true;
                    }
                }
                else
                {
                    ((RadDatePicker)editedItem["ApplicableFrom"].Controls[0]).SelectedDate = DateTime.Now.Date;
                }

            }
            e.Canceled = true;
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void PresentationGridItemCommand(object sender, GridCommandEventArgs e)
        {
            OrderPadAtCallRatesDS ds;
            switch (e.CommandName.ToLower())
            {
                case "delete":
                    ds = PopulateDataSetWithData(e, DatasetCommandTypes.Delete);
                    e.Item.Edit = false;
                    e.Canceled = true;
                    SaveOrganizanition(ds);
                    PresentationGrid.Rebind();
                    break;
                case "update":
                    ds = PopulateDataSetWithData(e, DatasetCommandTypes.Update);
                    e.Item.Edit = false;
                    e.Canceled = true;
                    SaveOrganizanition(ds);
                    PresentationGrid.Rebind();
                    break;
            }
        }

        protected void PresentationGridInsertCommand(object source, GridCommandEventArgs e)
        {
            if (!"AtCallRates".Equals(e.Item.OwnerTableView.Name)) return;
            OrderPadAtCallRatesDS ds = PopulateDataSetWithData(e, DatasetCommandTypes.Add);
            SaveOrganizanition(ds);
            e.Item.Edit = false;
            e.Canceled = true;
            PresentationGrid.Rebind();
        }

        private OrderPadAtCallRatesDS PopulateDataSetWithData(GridCommandEventArgs e, DatasetCommandTypes command)
        {
            var ds = new OrderPadAtCallRatesDS();
            DataRow row = ds.OrderPadAtCallRatesTable.NewRow();
            ds.CommandType = command;

            if (command == DatasetCommandTypes.Delete)
            {
                var dataItem = (GridDataItem)e.Item;
                ds.MainInstituteId = new Guid(dataItem["InstituteID"].Text);
                row[ds.OrderPadAtCallRatesTable.ID] = new Guid(dataItem["ID"].Text);
            }
            else
            {
                var editForm = (GridEditFormItem)e.Item;
                if (command == DatasetCommandTypes.Update)
                {
                    row[ds.OrderPadAtCallRatesTable.ID] = ((TextBox)editForm["ID"].Controls[0]).Text;
                    row[ds.OrderPadAtCallRatesTable.TYPE] = ((TextBox)editForm["Type"].Controls[0]).Text;
                }
                else if (command == DatasetCommandTypes.Add)
                {
                    row[ds.OrderPadAtCallRatesTable.TYPE] = "Manual";
                }

                var comboBroker = (RadComboBox)editForm["BrokerEditor"].Controls[0];
                var comboBank = (RadComboBox)editForm["BankEditor"].Controls[0];
                var min = (RadNumericTextBox)editForm["Min"].Controls[0];
                var max = (RadNumericTextBox)editForm["Max"].Controls[0];
                var rate = (TextBox)editForm["Rate"].Controls[1];
                var applicableFrom = (RadDatePicker)editForm["ApplicableFrom"].Controls[0];
                var isEditable = (CheckBox)editForm["IsEditable"].Controls[0];
                var accountTier = (TextBox)editForm["AccountTier"].Controls[0];
                var lstApplicability = (RadListBox)editForm["Applicability"].FindControl("lstApplicability");
                var honeymoon = (RadNumericTextBox)e.Item.FindControl("HoneymoonPeriod");

                ds.MainInstituteId = new Guid(comboBroker.SelectedValue);
                row[ds.OrderPadAtCallRatesTable.INSTITUTEID] = new Guid(comboBank.SelectedValue);
                row[ds.OrderPadAtCallRatesTable.ACCOUNTTIER] = accountTier.Text.Trim();
                row[ds.OrderPadAtCallRatesTable.MIN] = string.IsNullOrEmpty(min.Text) ? "0" : min.Text;
                row[ds.OrderPadAtCallRatesTable.MAX] = string.IsNullOrEmpty(max.Text) ? "0" : max.Text;
                row[ds.OrderPadAtCallRatesTable.RATE] = rate.Text;// string.IsNullOrEmpty(rate.Text) || Convert.ToDecimal(rate.Text) == 0 ? "0" : (Convert.ToDecimal(rate.Text) / 100).ToString();
                row[ds.OrderPadAtCallRatesTable.APPLICABLEFROM] = applicableFrom.SelectedDate;
                row[ds.OrderPadAtCallRatesTable.ISEDITABLE] = isEditable.Checked;
                row[ds.OrderPadAtCallRatesTable.HONEYMOONPERIOD] = string.IsNullOrEmpty(honeymoon.Text) ? "0" : honeymoon.Text;

                var sb = new StringBuilder();
                IList<RadListBoxItem> collection = lstApplicability.CheckedItems;
                foreach (RadListBoxItem item in collection)
                {
                    sb.Append(item.Value + ", ");
                }
                string applicability = string.Empty;
                if (sb.Length > 0)
                {
                    applicability = sb.Remove(sb.ToString().Trim().Length - 1, 1).ToString();
                }

                row[ds.OrderPadAtCallRatesTable.APPLICABILITY] = applicability;
            }
            ds.OrderPadAtCallRatesTable.Rows.Add(row);
            return ds;
        }

        private void DisplayMessage(string text)
        {
            PresentationGrid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
        }

        #endregion
    }
}