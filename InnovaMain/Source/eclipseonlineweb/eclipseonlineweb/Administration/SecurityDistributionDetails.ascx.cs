﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;

namespace eclipseonlineweb.Administration
{
    public partial class SecurityDistributionDetails : System.Web.UI.UserControl
    {
        public Action<Guid> Saved
        {
            get;
            set;
        }
        public Action<DataSet> SaveData
        {
            get;
            set;
        }
        public Action Canceled { get; set; }
        public Action ValidationFailed { get; set; }
        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void ClearEntity()
        {
            lblMessages.Text = string.Empty;
            TextRecordDate.Text = string.Empty;
            TextPaymentDate.Text = string.Empty;
            calRecodDate.SelectedDate = null;
            calPaymentDate.SelectedDate = null;
            TextID.Value = Guid.Empty.ToString();
            TextSecID.Value = Guid.Empty.ToString();
            TxtFundCode.Text = string.Empty;
            txtFundName.Text = string.Empty;
            TextPaymentDate.Text = string.Empty;
            lblUnitAmountTotal.Text = string.Empty;
            lblAutaxCreditTotal.Text = string.Empty;
            lblTaxWithHeldTotal.Text = string.Empty;
            lblGrossDistribution.Text = string.Empty;
            foreach (var control in this.Controls)
            {
                if (control is TextBox && (control as TextBox).ID.StartsWith("txt_"))
                {
                    (control as TextBox).Text = string.Empty;
                }
                else
                    if (control is Label && (control as Label).ID.StartsWith("txt_"))
                    {
                        (control as Label).Text = string.Empty;
                    }
            }
        }

        public DistributionEntity GetEntity()
        {
            DistributionEntity entity = new DistributionEntity();
            entity.ID = new Guid(TextID.Value);

            DateTime date;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            if (TextRecordDate.Text.Trim() != string.Empty)
            {
                if (DateTime.TryParse(TextRecordDate.Text.Trim(), info, DateTimeStyles.None, out date))
                {
                    entity.RecordDate = date;
                }
            }
            else
            {
                entity.RecordDate = null;
            }

            if (TextPaymentDate.Text.Trim() != string.Empty)
            {

                if (DateTime.TryParse(TextPaymentDate.Text.Trim(), info, DateTimeStyles.None, out date))
                {
                    entity.PaymentDate = date;
                }
            }
            else
            {
                entity.PaymentDate = null;
            }
            SecurityDistributionsDetailsDS ds = GetDataSet(new Guid(TextID.Value), new Guid(TextSecID.Value));
            if (ds.Entity != null)
            {
                entity.ProcessFlag = ds.Entity.ProcessFlag;
                entity.Components = ds.Entity.Components;
            }

            if (entity.Components == null)
            {
                entity.Components = new List<DistributionComponent>();
            }
            bool addnew = false;
            bool hasValues = false;
            foreach (var componentKey in DistributionComponent.ComponentsTypes.Keys)
            {
                addnew = false;
                hasValues = false;
                var conponent = entity.Components.Where(ss => ss.ComponentType == componentKey).FirstOrDefault();
                if (conponent == null)
                {
                    conponent = new DistributionComponent() { ComponentType = componentKey };
                    addnew = true;
                }

                var txt_UnitAmount = this.FindControl("txt_UnitAmount" + componentKey);
                if (txt_UnitAmount != null)
                {
                    conponent.Unit_Amount = ((txt_UnitAmount as TextBox).Text != "") ? Convert.ToDouble((txt_UnitAmount as TextBox).Text) : (double?)null;
                    hasValues = true;
                }

                var txt_AutaxCredit = this.FindControl("txt_AutaxCredit" + componentKey);
                if (txt_AutaxCredit != null)
                {
                    conponent.Autax_Credit = ((txt_AutaxCredit as TextBox).Text != "") ? Convert.ToDouble((txt_AutaxCredit as TextBox).Text) : (double?)null;
                    hasValues = true;
                }
                var txt_TaxWithHeld = this.FindControl("txt_TaxWithHeld" + componentKey);
                if (txt_TaxWithHeld != null)
                {
                    conponent.Tax_WithHeld = ((txt_TaxWithHeld as TextBox).Text != "") ? Convert.ToDouble((txt_TaxWithHeld as TextBox).Text) : (double?)null;
                    hasValues = true;
                }
                if (addnew && hasValues)
                {

                    entity.Components.Add(conponent);
                }


            }

            return entity;
        }

        public void SetEntity(Guid ID, Guid SecID, string investmentcode, string description)
        {
            ClearEntity();
            TextSecID.Value = SecID.ToString();
            if (ID != Guid.Empty)
            {
                SecurityDistributionsDetailsDS ds = GetDataSet(ID, SecID);
                var Entity = ds.Entity;

                TextID.Value = ID.ToString();
                TextSecID.Value = SecID.ToString();


                TxtFundCode.Text = investmentcode;
                txtFundName.Text = description;

                if (Entity.RecordDate.HasValue)
                {
                    calRecodDate.SelectedDate = Entity.RecordDate.Value;
                    TextRecordDate.Text = Entity.RecordDate.Value.ToString("dd/MM/yyyy");
                }
                if (Entity.PaymentDate.HasValue)
                {
                    calPaymentDate.SelectedDate = Entity.PaymentDate.Value;
                    TextPaymentDate.Text = Entity.PaymentDate.Value.ToString("dd/MM/yyyy");
                }
                if (Entity.Components != null)
                {

                    decimal colTotal_txt_UnitAmount = 0;
                    decimal colTotal_txt_AutaxCredit = 0;
                    decimal colTotal_txt_TaxWithHeld = 0;
                    decimal colTotal_txt_GrossDistribution = 0;

                    foreach (var component in Entity.Components)
                    {
                        bool hasValue = false;
                        var unitamount = this.FindControl("txt_UnitAmount" + component.ComponentType);
                        if (unitamount != null)
                        {
                            if (component.Unit_Amount.HasValue)
                            {
                                (unitamount as TextBox).Text = component.Unit_Amount.Value.ToString();
                                colTotal_txt_UnitAmount += Convert.ToDecimal(component.Unit_Amount.Value);
                                hasValue = true;
                            }
                            else
                            {
                                (unitamount as TextBox).Text = "";
                            }
                        }

                        var txt_AutaxCredit = this.FindControl("txt_AutaxCredit" + component.ComponentType);
                        if (txt_AutaxCredit != null)
                        {
                            if (component.Autax_Credit.HasValue)
                            {
                                (txt_AutaxCredit as TextBox).Text = component.Autax_Credit.Value.ToString();
                                colTotal_txt_AutaxCredit += Convert.ToDecimal(component.Autax_Credit.Value);
                                hasValue = true;
                            }
                            else
                            {
                                (txt_AutaxCredit as TextBox).Text = "";
                            }
                        }
                        var txt_TaxWithHeld = this.FindControl("txt_TaxWithHeld" + component.ComponentType);
                        if (txt_TaxWithHeld != null)
                        {
                            if (component.Tax_WithHeld.HasValue)
                            {
                                (txt_TaxWithHeld as TextBox).Text = component.Tax_WithHeld.Value.ToString();
                                colTotal_txt_TaxWithHeld += Convert.ToDecimal(component.Tax_WithHeld.Value);
                                hasValue = true;
                            }
                            else
                            {
                                (txt_TaxWithHeld as TextBox).Text = "";
                            }
                        }

                        var txt_GrossDistribution = this.FindControl("txt_GrossDistribution" + component.ComponentType);
                        if (txt_GrossDistribution != null)
                        {
                            if (hasValue)
                            {
                                (txt_GrossDistribution as Label).Text = component.GrossDistribution.ToString();
                                colTotal_txt_GrossDistribution += Convert.ToDecimal(component.GrossDistribution);
                                hasValue = true;
                            }
                            else
                            {
                                (txt_GrossDistribution as Label).Text = "";
                            }
                        }
                    }

                    lblUnitAmountTotal.Text = colTotal_txt_UnitAmount.ToString();
                    lblAutaxCreditTotal.Text = colTotal_txt_AutaxCredit.ToString();
                    lblTaxWithHeldTotal.Text = colTotal_txt_TaxWithHeld.ToString();
                    lblGrossDistribution.Text = colTotal_txt_GrossDistribution.ToString();

                }
            }
            else
            {

                TxtFundCode.Text = investmentcode;
                txtFundName.Text = description;
            }            
        }

        private SecurityDistributionsDetailsDS GetDataSet(Guid id, Guid secId)
        {
            SecurityDistributionsDetailsDS bankTransactionDetailsDS = new SecurityDistributionsDetailsDS();
            bankTransactionDetailsDS.SecID = secId;
            bankTransactionDetailsDS.ID = id;
            bankTransactionDetailsDS.CommandType = DatasetCommandTypes.Get;

            if (Broker != null)
            {
                IOrganization organization = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                organization.GetData(bankTransactionDetailsDS);
                Broker.ReleaseBrokerManagedComponent(organization);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            return bankTransactionDetailsDS;

        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            if (Validate() == false)
            {
                if (ValidationFailed != null)
                    ValidationFailed();
                return;
            }

            var id = UpdateData();
            if (Saved != null)
            {
                Saved(id);
            }
        }

        public bool Validate()
        {
            bool result = true;
            bool tempResult = false;
            lblMessages.Text = "";
            StringBuilder Messages = new StringBuilder();

            DateTime date;
            tempResult = TextRecordDate.ValidateDate(true, "dd/MM/yyyy", out date);
            if (tempResult)
            {
                calRecodDate.SelectedDate = date;
            }
            else
            {
                calRecodDate.SelectedDate = null;
            }

            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Record Date*", Messages);

            tempResult = TextPaymentDate.ValidateDate(true, "dd/MM/yyyy", out date);
            if (tempResult)
            {
                calPaymentDate.SelectedDate = date;
            }
            else
            {
                calPaymentDate.SelectedDate = null;
            }
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Payment Date*", Messages);


            CalcSum(ref result, ref tempResult, Messages);

            lblMessages.Text = Messages.ToString();

            return result;
        }

        private void CalcSum(ref bool result, ref bool tempResult, StringBuilder Messages)
        {
            decimal total = 0;

            decimal colTotal_txt_UnitAmount = 0;
            decimal colTotal_txt_AutaxCredit = 0;
            decimal colTotal_txt_TaxWithHeld = 0;
            decimal colTotal_txt_GrossDistribution = 0;

            bool hasValue = false;
            foreach (var componentKey in DistributionComponent.ComponentsTypes.Keys)
            {
                total = 0;
                hasValue = false;
                var txt_UnitAmount = this.FindControl("txt_UnitAmount" + componentKey);
                if (txt_UnitAmount != null)
                {
                    tempResult = (txt_UnitAmount as TextBox).ValidateDecimalMinMax(false, 0, 100);
                    if ((txt_UnitAmount as TextBox).ValidateDecimal(true))
                    {
                        total += decimal.Parse((txt_UnitAmount as TextBox).Text.Trim());
                        colTotal_txt_UnitAmount += decimal.Parse((txt_UnitAmount as TextBox).Text.Trim());
                        hasValue = true;
                    }

                    result = tempResult == false ? false : result;
                    AddMessageIn(tempResult, "Invalid Unit Amount. -Code: " + componentKey, Messages);

                }

                var txt_AutaxCredit = this.FindControl("txt_AutaxCredit" + componentKey);
                if (txt_AutaxCredit != null)
                {
                    tempResult = (txt_AutaxCredit as TextBox).ValidateDecimalMinMax(false, 0, 100);
                    if ((txt_AutaxCredit as TextBox).ValidateDecimal(true))
                    {
                        total += decimal.Parse((txt_AutaxCredit as TextBox).Text.Trim());
                        colTotal_txt_AutaxCredit += decimal.Parse((txt_AutaxCredit as TextBox).Text.Trim());
                        hasValue = true;
                    }
                    result = tempResult == false ? false : result;
                    AddMessageIn(tempResult, "Invalid Autax Credit. Code- " + componentKey, Messages);
                }
                var txt_TaxWithHeld = this.FindControl("txt_TaxWithHeld" + componentKey);
                if (txt_TaxWithHeld != null)
                {
                    tempResult = (txt_TaxWithHeld as TextBox).ValidateDecimalMinMax(false, 0, 100);
                    if ((txt_TaxWithHeld as TextBox).ValidateDecimal(true))
                    {
                        total += decimal.Parse((txt_TaxWithHeld as TextBox).Text.Trim());
                        colTotal_txt_TaxWithHeld += decimal.Parse((txt_TaxWithHeld as TextBox).Text.Trim());
                        hasValue = true;
                    }
                    result = tempResult == false ? false : result;
                    AddMessageIn(tempResult, "Invalid Tax With Held. Code- " + componentKey, Messages);
                }
                var txt_GrossDistribution = this.FindControl("txt_GrossDistribution" + componentKey);
                if (total > 100 || total < 0)
                {
                    result = false;
                    if (txt_GrossDistribution != null)
                    {
                        (txt_GrossDistribution as Label).Text = total.ToString();
                        colTotal_txt_GrossDistribution += total;
                        (txt_GrossDistribution as Label).ForeColor = Color.Red;
                    }
                    AddMessageIn(false, "Invalid Gross Distribution value 0-100. Code- " + componentKey, Messages);
                }
                else
                {
                    if (txt_GrossDistribution != null && hasValue)
                    {
                        (txt_GrossDistribution as Label).Text = total.ToString();
                        colTotal_txt_GrossDistribution += total;
                        (txt_GrossDistribution as Label).ForeColor = Color.Black;
                    }
                }
            }

            lblUnitAmountTotal.Text = colTotal_txt_UnitAmount.ToString();
            lblAutaxCreditTotal.Text = colTotal_txt_AutaxCredit.ToString();
            lblTaxWithHeldTotal.Text = colTotal_txt_TaxWithHeld.ToString();
            lblGrossDistribution.Text = colTotal_txt_GrossDistribution.ToString();
        }

        private void AddMessageIn(bool result, string message, StringBuilder Messages)
        {
            if (!result)
            {
                Messages.Append(message + "<br/>");
            }
        }

        private Guid UpdateData()
        {
            SecurityDistributionsDetailsDS distributionsDetailsDs = new SecurityDistributionsDetailsDS();
            distributionsDetailsDs.SecID = new Guid(TextSecID.Value);
            var id = new Guid(TextID.Value);
            if (id == Guid.Empty)
            {
                distributionsDetailsDs.ID = Guid.NewGuid();
                distributionsDetailsDs.CommandType = DatasetCommandTypes.Add;
            }
            else
            {
                distributionsDetailsDs.ID = id;
                distributionsDetailsDs.CommandType = DatasetCommandTypes.Update;
            }

            if (Broker != null)
            {
                distributionsDetailsDs.Entity = this.GetEntity();
                distributionsDetailsDs.Entity.ID = distributionsDetailsDs.ID;
                if (SaveData != null)
                {
                    SaveData(distributionsDetailsDs);
                }

            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            TextID.Value = distributionsDetailsDs.Entity.ID.ToString();
            return distributionsDetailsDs.ID;
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (Canceled != null)
            {
                Canceled();
            }
        }


    }
}