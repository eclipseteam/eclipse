﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="SetupMaster.master"
    AutoEventWireup="true" CodeBehind="ManualAssets.aspx.cs" Inherits="eclipseonlineweb.ManualAssets" %>

<%@ Register TagPrefix="uc1" TagName="breadcrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function hintContent() {
            return this.data.label + '<br/> ' + this.y + '';
        }
        function hintContentPie() {
            return this.data.toString() + " : " + Globalize.format(this.value / this.total, "p2");
        }

    </script>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                                ID="btnDownload" />
                            <asp:ImageButton runat="server" ImageUrl="~/images/window_previous.png" Visible="false"
                                OnClick="BtnBackToListing" ID="btnBack" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:breadcrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblManAssets" Text="Manual Assets"></asp:Label>
                            <asp:Label Font-Bold="true" Visible="false" runat="server" ID="lblManAssetsID"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <asp:Panel runat="server" ID="pnlMainGrid" Visible="true">
                <telerik:RadGrid OnNeedDataSource="ManualAssetGridNeedDataSource" ID="ManualAssetGrid"
                    runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                    AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnItemCommand="ManualAssetGridItemCommand"
                    GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                    AllowAutomaticUpdates="True" OnInsertCommand="ManualAssetGridInsertCommand" EnableViewState="true"
                    ShowFooter="true" OnItemCreated="ManualAssetGridItemCreated" OnItemDataBound="ManualAssetGridItemDataBound">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                        Name="ManualAsset" TableLayout="Fixed" EditMode="EditForms">
                        <CommandItemSettings AddNewRecordText="Add New Security" ShowExportToExcelButton="true"
                            ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                        <Columns>
                            <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="ID" HeaderText="ID"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ID" UniqueName="ID" Display="false"
                                ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridEditCommandColumn>
                            <telerik:GridDropDownColumn HeaderText="Investment Type" SortExpression="InvestmentType"
                                UniqueName="cmbInvestmentType" DataField="InvestmentType" Visible="false" ColumnEditorID="GridDropDownColumnEditorInvestmentType" />
                            <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Code" HeaderStyle-Width="20%"
                                ItemStyle-Width="20%" HeaderText="Manual Asset Code" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="Code" UniqueName="txtCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="Market" HeaderStyle-Width="35%"
                                ItemStyle-Width="30%" HeaderText="Market" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" Visible="false" DataField="Market"
                                UniqueName="txtMarket">
                            </telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn HeaderText="Rating" SortExpression="Rating" UniqueName="cmbRating"
                                DataField="Rating" Visible="false" ColumnEditorID="GridDropDownColumnEditorRating" />
                            <telerik:GridDropDownColumn HeaderText="Status" SortExpression="Status" UniqueName="cmbStatus"
                                DataField="Status" Visible="false" ColumnEditorID="GridDropDownColumnEditorStatus" />
                            <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="35%" ItemStyle-Width="35%"
                                SortExpression="Description" HeaderText="Company Name" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="Description" UniqueName="txtDescription">
                            </telerik:GridBoundColumn>
                            <telerik:GridNumericColumn SortExpression="UnitPriceBaseCurrency" DataField="UnitPriceBaseCurrency"
                                UniqueName="txtUnitPriceBaseCurrency" HeaderText="Unit Price Base Currency" Visible="false"
                                ReadOnly="true" />
                            <telerik:GridCheckBoxColumn HeaderText="Is Unitised" UniqueName="chkIsUnitised" DataField="IsUnitised"
                                Visible="false" />
                            <telerik:GridNumericColumn SortExpression="UnitSize" DataField="UnitSize" UniqueName="txtUnitSize"
                                HeaderText="Unit Size" Visible="false" />
                            <telerik:GridNumericColumn SortExpression="MinSecurityBal" DataField="MinSecurityBal"
                                UniqueName="txtMinSecurityBal" HeaderText="Min Security Bal" Visible="false" />
                            <telerik:GridCheckBoxColumn HeaderText="Ignore" UniqueName="chkIgnoreMinSecurityBal"
                                DataField="IgnoreMinSecurityBal" Visible="false" />
                            <telerik:GridNumericColumn SortExpression="MinTradedLotSize" DataField="MinTradedLotSize"
                                UniqueName="txtMinTradedLotSize" HeaderText="Min Traded Lot Size" Visible="false" />
                            <telerik:GridCheckBoxColumn HeaderText="Ignore" UniqueName="chkIgnoreMinTradedLotSize"
                                DataField="IgnoreMinTradedLotSize" Visible="false" />
                            <telerik:GridDateTimeColumn UniqueName="calIssueDate" PickerType="DatePicker" HeaderText="Issue Date"
                                DataField="IssueDate" DataFormatString="{0:dd/MM/yyyy}" Visible="false">
                                <ItemStyle Width="120px" />
                            </telerik:GridDateTimeColumn>
                            <telerik:GridNumericColumn SortExpression="PriceAsAtDate" DataField="PriceAsAtDate"
                                UniqueName="txtPriceAsAtDate" HeaderText="Price As At Date" Visible="false" />
                            <telerik:GridCheckBoxColumn HeaderText="Preffered Investment" DataField="IsPrefferedInvestment"
                                Visible="false" UniqueName="chkIsPrefferedInvestment" />
                            <telerik:GridButtonColumn CommandName="Details" Text="Details" ButtonType="LinkButton"
                                HeaderStyle-Width="10%" ItemStyle-Width="10%">
                            </telerik:GridButtonColumn>
                            <telerik:GridButtonColumn ConfirmText="Are you sure? You want to delete this record?"
                                ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                    <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                        <Excel Format="Html"></Excel>
                    </ExportSettings>
                </telerik:RadGrid>
                <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorInvestmentType"
                    runat="server" />
                <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorRating" runat="server" />
                <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorStatus" runat="server" />
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlDetails" Visible="false">
                <div id="chart">
                    <telerik:RadHtmlChart ID="radChart1" runat="server" Height="200px">
                    </telerik:RadHtmlChart>
                </div>
                <telerik:RadGrid OnNeedDataSource="PriceHistoryGridNeedDataSource" ID="PriceHistoryGrid"
                    runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                    AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnItemCommand="PriceHistoryGridItemCommand"
                    GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                    AllowAutomaticUpdates="True" OnInsertCommand="PriceHistoryGridInsertCommand"
                    EnableViewState="true" ShowFooter="true">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                        Name="PriceHistory" TableLayout="Fixed" EditMode="EditForms" DataKeyNames="ManualHistoryID">
                        <CommandItemSettings AddNewRecordText="Add New Security" ShowExportToExcelButton="true"
                            ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                        <Columns>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridEditCommandColumn>
                            <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="ManualHistoryID"
                                HeaderText="ID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ManualHistoryID"
                                UniqueName="ID" Display="false" ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridDateTimeColumn UniqueName="calDate" PickerType="DatePicker" HeaderText="Date"
                                DataField="Date" DataFormatString="{0:dd/MM/yyyy}">
                                <ItemStyle Width="20%" />
                            </telerik:GridDateTimeColumn>
                            <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="Currency" ItemStyle-Width="10%"
                                HeaderText="Currency" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Currency" UniqueName="txtCurrency"
                                DefaultInsertValue="AUD">
                            </telerik:GridBoundColumn>
                            <telerik:GridNumericColumn SortExpression="UnitPrice" DataField="UnitPrice" DataFormatString="{0:N6}"
                                UniqueName="txtUnitPrice" HeaderText="Unit Price" DecimalDigits="6" />
                            <telerik:GridNumericColumn SortExpression="NavPrice" DataField="NavPrice" DataFormatString="{0:N6}"
                                UniqueName="txtNavPrice" HeaderText="Nav Price" DecimalDigits="6" />
                            <telerik:GridNumericColumn SortExpression="PurPrice" DataField="PurPrice" DataFormatString="{0:N6}"
                                UniqueName="txtPurPrice" HeaderText="Pur Price" DecimalDigits="6" />
                            <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="TotalUnits" ItemStyle-Width="20%"
                                HeaderText="Total Units" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TotalUnits" UniqueName="txtTotalUnits"
                                ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="Value" ItemStyle-Width="20%"
                                HeaderText="Value" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Value" UniqueName="txtValue"
                                ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn ConfirmText="Are you sure? You want to delete this record?"
                                ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                    <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                        <Excel Format="Html"></Excel>
                    </ExportSettings>
                </telerik:RadGrid>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
