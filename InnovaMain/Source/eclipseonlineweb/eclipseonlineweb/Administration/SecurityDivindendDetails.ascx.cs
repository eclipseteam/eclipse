﻿using System;
using System.Data;
using System.Globalization;
using System.Text;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;

namespace eclipseonlineweb.Administration
{
    public partial class SecurityDivindendDetails : System.Web.UI.UserControl
    {
        public Action<Guid> Saved
        {
            get;
            set;
        }
        public Action<DataSet> SaveData
        {
            get;
            set;
        }
        public Action Canceled { get; set; }
        public Action ValidationFailed { get; set; }
        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void ClearEntity()
        {
            lblMessages.Text = "";
            TextRecordDate.Text = "";
            TextPaymentDate.Text = "";
            calRecodDate.SelectedDate = null;
            calPaymentDate.SelectedDate = null;
            TextID.Value = Guid.Empty.ToString();
            TextSecID.Value = Guid.Empty.ToString();
            TextCentsPerShare.Text = string.Empty;
            cmbAdministrationSystem.Text = "";
            cmbAdministrationSystem.SelectedValue = null;
            cmbTransactionType.Text = "";
            cmbTransactionType.SelectedValue = null;
            TextPaymentDate.Text = string.Empty;
            TextFrankedAmount.Text = string.Empty;
            TextUnfrankedAmount.Text = string.Empty;
            TextTaxFree.Text = string.Empty;
            TextTaxDeferred.Text = string.Empty;
            TextReturnofCapital.Text = string.Empty;
            TextDomesticInterestAmount.Text = string.Empty;
            TextForeignInterestAmount.Text = string.Empty;
            TextDomesticOtherIncome.Text = string.Empty;
            TextForeignOtherIncome.Text = string.Empty;
            TextDomesticWithHoldingTax.Text = string.Empty;
            TextForeignWithHoldingTax.Text = string.Empty;
            TextDomesticImputationTaxCredits.Text = string.Empty;
            TextForeignImputationTaxCredits.Text = string.Empty;
            TextForeignInvestmentFunds.Text = string.Empty;
            TextDomesticIndexationCGT.Text = string.Empty;
            TextForeignIndexationCGT.Text = string.Empty;
            TextDomesticDiscountedCGT.Text = string.Empty;
            TextForeignDiscountedCGT.Text = string.Empty;
            TextDomesticOtherCGT.Text = string.Empty;
            TextForeignOtherCGT.Text = string.Empty;
            TextDomesticGrossAmount.Text = string.Empty;
            TextComment.Text = string.Empty;
            txtCurrency.Text = "AUD";
            cmbDividendType.Text = string.Empty;
            cmbDividendType.SelectedValue = null;
            txtBalanceDate.Text = string.Empty;
            txtBooksCloseDate.Text = string.Empty;
        }

        public DividendEntity GetEntity()
        {
            DividendEntity entity = new DividendEntity();
            entity.ID = new Guid(TextID.Value);
            entity.AdministrationSystem = cmbAdministrationSystem.Text;
            entity.TransactionType = cmbTransactionType.Text;
            DateTime date;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            if (TextRecordDate.Text.Trim() != string.Empty)
            {
                if (DateTime.TryParse(TextRecordDate.Text.Trim(), info, DateTimeStyles.None, out date))
                {
                    entity.RecordDate = date;
                }
            }
            else
            {
                entity.RecordDate = null;
            }
            if (TextRecordDate.Text.Trim() != string.Empty)
            {

                if (DateTime.TryParse(TextPaymentDate.Text.Trim(), info, DateTimeStyles.None, out date))
                {
                    entity.PaymentDate = date;
                }
            }
            else
            {
                entity.PaymentDate = null;
            }

            entity.CentsPerShare = (TextCentsPerShare.Text != "") ? Convert.ToDouble(TextCentsPerShare.Text) : 0;
            entity.FrankedAmount = (TextFrankedAmount.Text != "") ? Convert.ToDouble(TextFrankedAmount.Text) : 0;
            entity.UnfrankedAmount = (TextUnfrankedAmount.Text != "") ? Convert.ToDouble(TextUnfrankedAmount.Text) : 0;
            entity.TaxFree = (TextTaxFree.Text != "") ? Convert.ToDouble(TextTaxFree.Text) : 0;
            entity.TaxDeferred = (TextTaxDeferred.Text != "") ? Convert.ToDouble(TextTaxDeferred.Text) : 0;
            entity.ReturnOfCapital = (TextReturnofCapital.Text != "") ? Convert.ToDouble(TextReturnofCapital.Text) : 0;
            entity.DomesticInterestAmount = (TextDomesticInterestAmount.Text != "") ? Convert.ToDouble(TextDomesticInterestAmount.Text) : 0;
            entity.ForeignInterestAmount = (TextForeignInterestAmount.Text != "") ? Convert.ToDouble(TextForeignInterestAmount.Text) : 0;
            entity.DomesticOtherIncome = (TextDomesticOtherIncome.Text != "") ? Convert.ToDouble(TextDomesticOtherIncome.Text) : 0;
            entity.ForeignOtherIncome = (TextForeignOtherIncome.Text != "") ? Convert.ToDouble(TextForeignOtherIncome.Text) : 0;
            entity.DomesticWithHoldingTax = (TextDomesticWithHoldingTax.Text != "") ? Convert.ToDouble(TextDomesticWithHoldingTax.Text) : 0;
            entity.ForeignWithHoldingTax = (TextForeignWithHoldingTax.Text != "") ? Convert.ToDouble(TextForeignWithHoldingTax.Text) : 0;
            entity.DomesticImputationTaxCredits = (TextDomesticImputationTaxCredits.Text != "") ? Convert.ToDouble(TextDomesticImputationTaxCredits.Text) : 0;
            entity.ForeignImputationTaxCredits = (TextForeignImputationTaxCredits.Text != "") ? Convert.ToDouble(TextForeignImputationTaxCredits.Text) : 0;
            entity.ForeignInvestmentFunds = (TextForeignInvestmentFunds.Text != "") ? Convert.ToDouble(TextForeignInvestmentFunds.Text) : 0;
            entity.DomesticIndexationCGT = (TextDomesticIndexationCGT.Text != "") ? Convert.ToDouble(TextDomesticIndexationCGT.Text) : 0;
            entity.ForeignIndexationCGT = (TextForeignIndexationCGT.Text != "") ? Convert.ToDouble(TextForeignIndexationCGT.Text) : 0;
            entity.DomesticDiscountedCGT = (TextDomesticDiscountedCGT.Text != "") ? Convert.ToDouble(TextDomesticDiscountedCGT.Text) : 0;
            entity.ForeignDiscountedCGT = (TextForeignDiscountedCGT.Text != "") ? Convert.ToDouble(TextForeignDiscountedCGT.Text) : 0;
            entity.DomesticOtherCGT = (TextDomesticOtherCGT.Text != "") ? Convert.ToDouble(TextDomesticOtherCGT.Text) : 0;
            entity.ForeignOtherCGT = (TextForeignOtherCGT.Text != "") ? Convert.ToDouble(TextForeignOtherCGT.Text) : 0;
            entity.DomesticGrossAmount = (TextDomesticGrossAmount.Text != "") ? Convert.ToDouble(TextDomesticGrossAmount.Text) : 0;
            entity.Comment = TextComment.Text;

            if (txtBalanceDate.Text.Trim() != string.Empty)
            {
                if (DateTime.TryParse(txtBalanceDate.Text.Trim(), info, DateTimeStyles.None, out date))
                {
                    entity.BalanceDate = date;
                }
            }
            else
            {
                entity.BalanceDate = null;
            }

            if (txtBooksCloseDate.Text.Trim() != string.Empty)
            {
                if (DateTime.TryParse(txtBooksCloseDate.Text.Trim(), info, DateTimeStyles.None, out date))
                {
                    entity.BooksCloseDate = date;
                }
            }
            else
            {
                entity.BooksCloseDate = null;
            }
            entity.Dividendtype = (DividendType)Enum.Parse(typeof(DividendType),cmbDividendType.Text);
            entity.Currency = txtCurrency.Text;

            SecurityDividendDetailsDS ds = GetDataSet(new Guid(TextID.Value), new Guid(TextSecID.Value));
            if (ds.Entity != null)
                entity.ReportCollection = ds.Entity.ReportCollection;


            return entity;
        }
        
        public void SetEntity(Guid ID, Guid SecID)
        {
            ClearEntity();
            TextSecID.Value = SecID.ToString();
            if (ID != Guid.Empty)
            {
                SecurityDividendDetailsDS ds = GetDataSet(ID, SecID);
                var Entity = ds.Entity;

                TextID.Value = ID.ToString();
                TextSecID.Value = SecID.ToString();

                TextCentsPerShare.Text = Entity.CentsPerShare.ToString();
                cmbAdministrationSystem.Text = Entity.AdministrationSystem;
                cmbAdministrationSystem.SelectedValue = Entity.AdministrationSystem;
                cmbTransactionType.Text = Entity.TransactionType;
                cmbTransactionType.SelectedValue = Entity.TransactionType;
                if (Entity.RecordDate.HasValue)
                {
                    calRecodDate.SelectedDate = Entity.RecordDate.Value;
                    TextRecordDate.Text = Entity.RecordDate.Value.ToString("dd/MM/yyyy");
                }
                if (Entity.PaymentDate.HasValue)
                {
                    calPaymentDate.SelectedDate = Entity.PaymentDate.Value;
                    TextPaymentDate.Text = Entity.PaymentDate.Value.ToString("dd/MM/yyyy");
                }
                TextFrankedAmount.Text = Entity.FrankedAmount.ToString();
                TextUnfrankedAmount.Text = Entity.UnfrankedAmount.ToString();
                TextTaxFree.Text = Entity.TaxFree.ToString();
                TextTaxDeferred.Text = Entity.TaxDeferred.ToString();
                TextReturnofCapital.Text = Entity.ReturnOfCapital.ToString();
                TextDomesticInterestAmount.Text = Entity.DomesticInterestAmount.ToString();
                TextForeignInterestAmount.Text = Entity.ForeignInterestAmount.ToString();
                TextDomesticOtherIncome.Text = Entity.DomesticOtherIncome.ToString();
                TextForeignOtherIncome.Text = Entity.ForeignOtherIncome.ToString();
                TextDomesticWithHoldingTax.Text = Entity.DomesticWithHoldingTax.ToString();
                TextForeignWithHoldingTax.Text = Entity.ForeignWithHoldingTax.ToString();
                TextDomesticImputationTaxCredits.Text = Entity.DomesticImputationTaxCredits.ToString();
                TextForeignImputationTaxCredits.Text = Entity.ForeignImputationTaxCredits.ToString();
                TextForeignInvestmentFunds.Text = Entity.ForeignInvestmentFunds.ToString();
                TextDomesticIndexationCGT.Text = Entity.DomesticIndexationCGT.ToString();
                TextForeignIndexationCGT.Text = Entity.ForeignIndexationCGT.ToString();
                TextDomesticDiscountedCGT.Text = Entity.DomesticDiscountedCGT.ToString();
                TextForeignDiscountedCGT.Text = Entity.ForeignDiscountedCGT.ToString();
                TextDomesticOtherCGT.Text = Entity.DomesticOtherCGT.ToString();
                TextForeignOtherCGT.Text = Entity.ForeignOtherCGT.ToString();
                TextDomesticGrossAmount.Text = Entity.DomesticGrossAmount.ToString();
                TextComment.Text = Entity.Comment;

                if (Entity.BalanceDate.HasValue)
                {
                    calBanalceDate.SelectedDate = Entity.BalanceDate.Value;
                    txtBalanceDate.Text = Entity.BalanceDate.Value.ToString("dd/MM/yyyy");
                }

                if (Entity.BooksCloseDate.HasValue)
                {
                    calBooksCloseDate.SelectedDate = Entity.BooksCloseDate.Value;
                    txtBooksCloseDate.Text = Entity.BooksCloseDate.Value.ToString("dd/MM/yyyy");
                }
                txtCurrency.Text = Entity.Currency??string.Empty;
                cmbDividendType.Text = Entity.Dividendtype.ToString();
                cmbDividendType.SelectedValue = Entity.Dividendtype.ToString();
            }
        }

        private SecurityDividendDetailsDS GetDataSet(Guid id, Guid secId)
        {
            SecurityDividendDetailsDS bankTransactionDetailsDS = new SecurityDividendDetailsDS();
            bankTransactionDetailsDS.SecID = secId;
            bankTransactionDetailsDS.ID = id;
            bankTransactionDetailsDS.CommandType = DatasetCommandTypes.Get;
            if (Broker != null)
            {
                IOrganization organization = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                organization.GetData(bankTransactionDetailsDS);
                Broker.ReleaseBrokerManagedComponent(organization);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            return bankTransactionDetailsDS;
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            if (Validate() == false)
            {

                if (ValidationFailed != null)
                    ValidationFailed();

                return;
            }

            var id = UpdateData();

            if (Saved != null)
            {

                Saved(id);
            }
        }
        
        public bool Validate()
        {
            bool result = true;
            bool tempResult = false;
            lblMessages.Text = "";
            StringBuilder Messages = new StringBuilder();
            tempResult = cmbAdministrationSystem.ValidateCombo(true);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Administration System*", Messages);
            if (!tempResult)
            {
                cmbAdministrationSystem.SelectedValue = null;
            }
            else
            {
                cmbAdministrationSystem.SelectedValue = cmbAdministrationSystem.Text;
            }
            tempResult = cmbTransactionType.ValidateCombo(true);
            if (!tempResult)
            {
                cmbTransactionType.SelectedValue = null;
            }
            else
            {
                cmbTransactionType.SelectedValue = cmbTransactionType.Text;
            }

            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Transaction Type*", Messages);
            DateTime date;
            tempResult = TextRecordDate.ValidateDate(true, "dd/MM/yyyy", out date);
            if (tempResult)
            {
                calRecodDate.SelectedDate = date;
            }
            else
            {
                calRecodDate.SelectedDate = null;
            }

            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Record Date*", Messages);
            tempResult = TextPaymentDate.ValidateDate(true, "dd/MM/yyyy", out date);
            if (tempResult)
            {
                calPaymentDate.SelectedDate = date;
            }
            else
            {
                calPaymentDate.SelectedDate = null;
            }

            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Payment Date*", Messages);

            tempResult = TextFrankedAmount.ValidateDecimalMinMax(false,0,100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Franked Amount", Messages);

            tempResult = TextUnfrankedAmount.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid UnFranked Amount", Messages);

            tempResult = TextTaxFree.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Tax Free", Messages);

            tempResult = TextTaxDeferred.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Tax Deferred", Messages);

            tempResult = TextReturnofCapital.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Return on Capital", Messages);

            tempResult = TextDomesticInterestAmount.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Domestic Interes tAmount", Messages);

            tempResult = TextForeignInterestAmount.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Foreign Interest Amount", Messages);

            tempResult = TextDomesticOtherIncome.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Domestic Other Income", Messages);

            tempResult = TextForeignOtherIncome.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Foreign Other Income", Messages);

            tempResult = TextDomesticWithHoldingTax.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Domestic With Holding Tax", Messages);


            tempResult = TextForeignWithHoldingTax.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Foreign With Holding Tax", Messages);

            tempResult = TextDomesticImputationTaxCredits.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Domestic Imputation Tax Credits", Messages);


            tempResult = TextForeignImputationTaxCredits.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Foreign Imputation Tax Credits", Messages);


            tempResult = TextForeignInvestmentFunds.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Foreign Investment Funds", Messages);


            tempResult = TextDomesticIndexationCGT.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Domestic Indexation CGT", Messages);


            tempResult = TextForeignIndexationCGT.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Foreign Indexation CGT", Messages);


            tempResult = TextDomesticDiscountedCGT.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Domestic Discounted CGTt", Messages);


            tempResult = TextForeignDiscountedCGT.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Foreign Discounted CGT", Messages);

            tempResult = TextDomesticOtherCGT.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Domestic Other CGT", Messages);

            tempResult = TextForeignOtherCGT.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Foreign Other CGT", Messages);

            tempResult = TextDomesticGrossAmount.ValidateDecimalMinMax(false, 0, 100);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Domestic Gross Amount", Messages);

            tempResult = TextCentsPerShare.ValidateDecimal(true);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Cents Per Share", Messages);

            decimal Total = 0;

            if (result)
            {
                tempResult = ValidatePercentFields(out Total);
                result = tempResult == false ? false : result;
                AddMessageIn(tempResult, "Sum of all percentages is " + Total + ". It should be up to 100.", Messages);
            }


            tempResult = cmbDividendType.ValidateCombo(true);
            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Dividend Type*", Messages);
            if (!tempResult)
            {
                cmbDividendType.SelectedValue = null;
            }
            else
            {
                cmbDividendType.SelectedValue = cmbDividendType.Text;
            }

            tempResult = txtBalanceDate.ValidateDate(true, "dd/MM/yyyy", out date);
            if (tempResult)
            {
                calBanalceDate.SelectedDate = date;
            }
            else
            {
                calBanalceDate.SelectedDate = null;
            }

            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Balance Date*", Messages);

            tempResult = txtBooksCloseDate.ValidateDate(true, "dd/MM/yyyy", out date);
            if (tempResult)
            {
                calBooksCloseDate.SelectedDate = date;
            }
            else
            {
                calBooksCloseDate.SelectedDate = null;
            }

            result = tempResult == false ? false : result;
            AddMessageIn(tempResult, "Invalid Books Close Date*", Messages);

            lblMessages.Text = Messages.ToString();

            return result;
        }

        private void AddMessageIn(bool result, string message, StringBuilder Messages)
        {
            if (!result)
            {
                Messages.Append(message + "<br/>");
            }
        }

        public bool ValidatePercentFields(out decimal Total)
        {
            Total = (TextDomesticDiscountedCGT.Text == "" ? 0 : Convert.ToDecimal(TextDomesticDiscountedCGT.Text))
                + (TextDomesticGrossAmount.Text == "" ? 0 : Convert.ToDecimal(TextDomesticGrossAmount.Text))
                + (TextDomesticImputationTaxCredits.Text == "" ? 0 : Convert.ToDecimal(TextDomesticImputationTaxCredits.Text))
                + (TextDomesticIndexationCGT.Text == "" ? 0 : Convert.ToDecimal(TextDomesticIndexationCGT.Text))
                + (TextDomesticInterestAmount.Text == "" ? 0 : Convert.ToDecimal(TextDomesticInterestAmount.Text))
                + (TextDomesticOtherCGT.Text == "" ? 0 : Convert.ToDecimal(TextDomesticOtherCGT.Text))
                + (TextDomesticOtherIncome.Text == "" ? 0 : Convert.ToDecimal(TextDomesticOtherIncome.Text))
                + (TextDomesticWithHoldingTax.Text == "" ? 0 : Convert.ToDecimal(TextDomesticWithHoldingTax.Text))
                + (TextForeignDiscountedCGT.Text == "" ? 0 : Convert.ToDecimal(TextForeignDiscountedCGT.Text))
                + (TextForeignImputationTaxCredits.Text == "" ? 0 : Convert.ToDecimal(TextForeignImputationTaxCredits.Text))
                + (TextForeignIndexationCGT.Text == "" ? 0 : Convert.ToDecimal(TextForeignIndexationCGT.Text))
                + (TextForeignInterestAmount.Text == "" ? 0 : Convert.ToDecimal(TextForeignInterestAmount.Text))
                + (TextForeignInvestmentFunds.Text == "" ? 0 : Convert.ToDecimal(TextForeignInvestmentFunds.Text))
                + (TextForeignOtherCGT.Text == "" ? 0 : Convert.ToDecimal(TextForeignOtherCGT.Text))
                + (TextForeignOtherIncome.Text == "" ? 0 : Convert.ToDecimal(TextForeignOtherIncome.Text))
                + (TextForeignWithHoldingTax.Text == "" ? 0 : Convert.ToDecimal(TextForeignWithHoldingTax.Text))
                + (TextFrankedAmount.Text == "" ? 0 : Convert.ToDecimal(TextFrankedAmount.Text))
                + (TextReturnofCapital.Text == "" ? 0 : Convert.ToDecimal(TextReturnofCapital.Text))
                + (TextTaxDeferred.Text == "" ? 0 : Convert.ToDecimal(TextTaxDeferred.Text))
                + (TextTaxFree.Text == "" ? 0 : Convert.ToDecimal(TextTaxFree.Text))
                + (TextUnfrankedAmount.Text == "" ? 0 : Convert.ToDecimal(TextUnfrankedAmount.Text));

            return Total >= 0 && Total <= 100;
        }
        
        private Guid UpdateData()
        {
            SecurityDividendDetailsDS bankTransactionDetailsDS = new SecurityDividendDetailsDS();
            bankTransactionDetailsDS.SecID = new Guid(TextSecID.Value);


            var id = new Guid(TextID.Value);
            if (id == Guid.Empty)
            {
                bankTransactionDetailsDS.ID = Guid.NewGuid();
                bankTransactionDetailsDS.CommandType = DatasetCommandTypes.Add;
            }
            else
            {
                bankTransactionDetailsDS.ID = id;
                bankTransactionDetailsDS.CommandType = DatasetCommandTypes.Update;
            }

            if (Broker != null)
            {

                this.Broker.SaveOverride = true;
                bankTransactionDetailsDS.Entity = this.GetEntity();
                bankTransactionDetailsDS.Entity.ID = bankTransactionDetailsDS.ID;
                if (SaveData != null)
                {
                    SaveData(bankTransactionDetailsDS);
                }
            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            TextID.Value = bankTransactionDetailsDS.Entity.ID.ToString();

            return bankTransactionDetailsDS.ID;
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (Canceled != null)
            {
                Canceled();
            }
        }
        
    }
}