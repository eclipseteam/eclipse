﻿using System;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;

namespace eclipseonlineweb
{
    public partial class ViewErrorLog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DirectoryInfo info = new DirectoryInfo(Server.MapPath("ErrorLog//"));
            FileInfo[] files = info.GetFiles().OrderByDescending(p => p.CreationTime).ToArray();

            foreach (FileInfo file in files)
            {
                HyperLink link = new HyperLink();
                link.Text = file.Name;
                link.NavigateUrl = "ErrorLog/" + file.Name;
                Controls.Add(link);
                Controls.Add(new Literal() { Text = "<br/>" });
            }
        }
    }
}