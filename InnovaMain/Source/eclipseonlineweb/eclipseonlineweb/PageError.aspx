﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/NoMenu.Master" AutoEventWireup="true"
    CodeBehind="PageError.aspx.cs" Inherits="eclipseonlineweb.Account.PageError" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<h1>Application Error</h1>
<p style="height:600px"> Error has occured on the page or you are not a authorized user. e-Clipse Technology Team has been notified. Please <a href="Account/Login.aspx">CLICK</a> here to login again</p>
</asp:Content>
