﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.C1Menu;
using Oritax.TaxSimp.SM.Workflows;
using Oritax.TaxSimp.SM.Workflows.Data;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.Utilities;
using System.Threading;
using Telerik.Web.UI;
using System.Text;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.DataSets;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Web.UI.HtmlControls;


namespace eclipseonlineweb
{
    public partial class SiteMaster : MasterPage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (Utilities.IsTestApp())
                Img1.Visible = false;
            else
                ImgTest.Visible = false;

            hfIsTest.Value = Utilities.IsTestApp().ToString().ToLower();
            Page.Header.DataBind();
            
        }
        

        protected void GetAdviserInfo(object sender, EventArgs e)
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.Page.User.Identity.Name, "DBUser_1_1");
            DBUserDetailsDS dbUserDetailsDS = new DBUserDetailsDS();
            objUser.GetData(dbUserDetailsDS);

            if (objUser.UserType == UserType.Advisor)
            {
                StringBuilder strBuilder = new StringBuilder();
                strBuilder.Append("<table>");
                strBuilder.Append("<tr>");
                strBuilder.Append("<td>");
                strBuilder.Append("Adviser Name:");
                strBuilder.Append("</td>");
                strBuilder.Append("<td>");
                strBuilder.Append((dbUserDetailsDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0][DBUserDetailsDS.USER_FIRSTNAME_FIELD].ToString() + " " + dbUserDetailsDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0][DBUserDetailsDS.USER_LASTNAME_FIELD].ToString()).Replace("'", "\\'"));
                strBuilder.Append("</td>");
                strBuilder.Append("</tr>");
                strBuilder.Append("<tr>");
                strBuilder.Append("<td>");
                strBuilder.Append("Adviser Code:");
                strBuilder.Append("</td>");
                strBuilder.Append("<td>");
                strBuilder.Append(dbUserDetailsDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0][DBUserDetailsDS.ADVISERID].ToString());
                strBuilder.Append("</td>");
                strBuilder.Append("</tr>");

                strBuilder.Append("<tr>");
                strBuilder.Append("<td>");
                strBuilder.Append("IFA Name:");
                strBuilder.Append("</td>");
                strBuilder.Append("<td>");
                strBuilder.Append(dbUserDetailsDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0][DBUserDetailsDS.IFANAME].ToString().Replace("'", "\\'"));
                strBuilder.Append("</td>");
                strBuilder.Append("</tr>");

                strBuilder.Append("<tr>");
                strBuilder.Append("<td valign=\"top\">");
                strBuilder.Append("IFA HASH:");
                strBuilder.Append("</td>");
                strBuilder.Append("<td valign=\"top\">");
                string hash = Oritax.TaxSimp.Utilities.Encryption.EncryptData(dbUserDetailsDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0][DBUserDetailsDS.IFACID].ToString());
                hash = hash.Replace("/", "FWSLASH");
                hash = hash.Replace("\"", "BKSLASH");
                //strBuilder.Append(hash);
                //strBuilder.Append("<textarea");
                //strBuilder.Append("cols=\"100\" name=\"Text1\" rows=\"5\" style=\"width:200px; height:50px;\" >");
                strBuilder.Append("<TEXTAREA name=\"thetext\" rows=\"5\" cols=\"60\">");
                strBuilder.Append(hash);
                strBuilder.Append("</TEXTAREA>");
                strBuilder.Append("</td>");
                strBuilder.Append("</tr>");
                strBuilder.Append("</table>");
                strBuilder.Append("<br />");
                this.hashCode.Text = Oritax.TaxSimp.Utilities.Encryption.EncryptData(dbUserDetailsDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0][DBUserDetailsDS.IFACID].ToString());

                this.AdviserInfoPopup.RadAlert(strBuilder.ToString(), 600, 180, "Adviser Info", null);

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.UMABroker = new CMBroker(Context.User, DBConnection.Connection.ConnectionString);
            this.UMABroker.SetStart();

            if (NavigationMenu != null)
            {
                MenuItem adminmenu = this.NavigationMenu.FindItem("ADMINISTRATION");
                MenuItem workflow = this.NavigationMenu.FindItem("WORKFLOW");
                MenuItem setup = this.NavigationMenu.FindItem("INVESTMENT");
                DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.Page.User.Identity.Name, "DBUser_1_1");
                DBUserDetailsDS dbUserDetailsDS = new DBUserDetailsDS();
                objUser.GetData(dbUserDetailsDS);

                if (objUser.UserType == UserType.Advisor)
                {
                    StringBuilder strBuilder = new StringBuilder();
                    strBuilder.Append("<br />");
                    strBuilder.Append("Adviser Full Name: " + dbUserDetailsDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0][DBUserDetailsDS.USER_FIRSTNAME_FIELD].ToString() + " " + dbUserDetailsDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0][DBUserDetailsDS.USER_LASTNAME_FIELD].ToString());
                    strBuilder.Append("<br />");
                    strBuilder.Append("IFA Name: " + dbUserDetailsDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0][DBUserDetailsDS.IFANAME].ToString());
                    strBuilder.Append("<br />");
                    strBuilder.Append("Adviser Code: " + dbUserDetailsDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0][DBUserDetailsDS.ADVISERID].ToString());
                    strBuilder.Append("<br />");
                    strBuilder.Append("IFA HASH: " + Oritax.TaxSimp.Utilities.Encryption.EncryptData(dbUserDetailsDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0][DBUserDetailsDS.IFACID].ToString()));
                    strBuilder.Append("<br />");

                    ((LinkButton)this.HeadLoginView.FindControl("lblAdv")).Visible = true;
                    ((LinkButton)this.HeadLoginView.FindControl("lblAdv")).Text = " (" + dbUserDetailsDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0][DBUserDetailsDS.ADVISERID].ToString() + ")";
                }

                if (this.Page.User.Identity.Name.ToLower() != "administrator")
                {
                    if (objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA)
                    {
                        this.NavigationMenu.Items.Remove(adminmenu);
                    }

                    if (objUser.UserType == UserType.Client || objUser.UserType == UserType.Accountant)
                    {
                        this.NavigationMenu.Items.Remove(workflow);
                        this.NavigationMenu.Items.Remove(adminmenu);
                        this.NavigationMenu.Items.Remove(setup);
                    }
                }


                if (Utilities.IsTestApp() && (objUser.UserType == UserType.Advisor || this.Page.User.Identity.Name.ToLower() == "administrator" || objUser.UserType == UserType.Innova))
                {
                    MenuItem obp = new MenuItem();
                    obp.Text = "OBP";
                    obp.NavigateUrl = "~/OBP/Individuals.aspx";
                    if (!NavigationMenu.Items.Contains(obp))
                        NavigationMenu.Items.Add(obp);
                }
            }
        }

        private void Page_PreRender(object sender, EventArgs e)
        {
            ManageTopIcons();
        }

        public ICMBroker UMABroker
        {
            get
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (brokerSlot == null)
                    return null;

                return (ICMBroker)Thread.GetData(brokerSlot);
            }
            set
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (brokerSlot == null)
                    brokerSlot = Thread.AllocateNamedDataSlot("Broker");

                if (value == null)
                    Thread.FreeNamedDataSlot("Broker");

                Thread.SetData(brokerSlot, value);
            }
        }

        protected void MenuItemClick(object sender, C1MenuEventArgs e)
        {

        }

        #region Alerts & Nofications
        protected int GetAlertCount()
        {
            IBrokerManagedComponent bmc = UMABroker.CreateTransientComponentInstance(new Guid("15DE2644-C1A0-41D5-8293-2794EF31840D"));
            var alertDS = new MDAAlertDS { Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = ((UMABasePage)Page).GetCurrentUser() } };
            bmc.GetData(alertDS);
            DataTable mdaAlertTable = alertDS.Tables[MDAAlertDS.MDAALERTTABLE];

            //Filtering only Active Alerts.
            var alertCount = mdaAlertTable.AsEnumerable().Count(row => row.Field<string>(MDAAlertTable.STATUS).Equals(MDAAlertTable.AlretStatus.Active.ToString()));
            return alertCount;
        }
        protected int GetNotificationCount()
        {
            IBrokerManagedComponent bmc = UMABroker.CreateTransientComponentInstance(new Guid("15DE2644-C1A0-41D5-8293-2794EF31840D"));
            var workflowsDS = new WorkflowsDS();
            bmc.GetData(workflowsDS);
            DataTable table = workflowsDS.Tables[WorkflowsDS.NotificationsTable];
            var view = new DataView(table)
            {
                Sort = "CreatedDate DESC",
                RowFilter = string.Format("NotificationType>{0}", (int)NotificationType.MDA)
            };
            return view.ToTable().Rows.Count;
        }


        public void ManageTopIcons()
        {
            var alertsCount = GetAlertCount();
            var notificationCount = GetNotificationCount();

            var liAlerts = HeadLoginView.FindControl("liAlert") as HtmlGenericControl;
            var liNotification = HeadLoginView.FindControl("liNotification") as HtmlGenericControl;

            var lblAlertCount = HeadLoginView.FindControl("lblAlertCount") as Label;
            var lblNotificationCount = HeadLoginView.FindControl("lblNotificationCount") as Label;


            if (liAlerts != null)
            {
                //liAlerts.Visible = !(alertsCount <= 0);
                if (lblAlertCount != null)
                    lblAlertCount.Text = alertsCount.ToString();
            }
            if (liNotification != null)
            {
                //liNotification.Visible = !(notificationCount <= 0);
                if (lblNotificationCount != null)
                    lblNotificationCount.Text = notificationCount.ToString();
            }

            var user = ((UMABasePage)Page).GetCurrentUser();
            if (user.IsAdmin)
            {
                liAlerts.Visible = !(alertsCount <= 0);
                liNotification.Visible = !(notificationCount <= 0);
            }
            else
            {
                if ((UserType)user.UserType == UserType.Advisor)
                {
                    bool isAllowedAdvisor = (user.Email.ToLower().Contains("nac.com.au") || user.Email.ToLower().Contains("priorityplanners.com.au"));
                    //Alert Grid Visible
                    if (isAllowedAdvisor)
                    {
                        liAlerts.Visible = !(alertsCount <= 0);
                        liNotification.Visible = !(notificationCount <= 0);
                    }
                    else
                    {
                        liAlerts.Visible = false;
                        liNotification.Visible = false;
                    }
                }
            }
        }

        #endregion
    }
}
