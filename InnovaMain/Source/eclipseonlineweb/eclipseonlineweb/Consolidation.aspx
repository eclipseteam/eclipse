﻿<%@ Page Title="e-Clipse Online Portal" EnableViewState="true" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeBehind="Consolidation.aspx.cs" Inherits="eclipseonlineweb.Consolidation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Chart"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="breadcrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        function GoToPage(url) {
            window.location = url;
            return false;
        }
        localStorage['MenuIndex'] = '3';
    </script>
</asp:Content>
<asp:Content ID="SideMenu" ContentPlaceHolderID="SidebarMenu" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <ul class="menu">
                <li class="item1"><a href="#" onclick="javascript:GoToPage('ClientSearch.aspx');">ACCOUNTS
                    SEARCH </a></li>
                <li class="item2"><a href="#" onclick="javascript:GoToPage('AccountsFUM.aspx');"
                    >UMA ACCOUNTS - $FUM </a></li>
                <li class="item3" id="li1" runat="server"><a href="#" onclick="javascript:GoToPage('SuperAccountsFUM.aspx');">
                    E-CLIPSE SUPER - $FUM </a></li>

                <li class="item2"><a href="#" onclick="javascript:GoToPage('Consolidation.aspx');"
                    id="A1">CONSOLIDATION </a></li>
            </ul>
            <!--News End-->
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        function hintContentPie() {
            return Globalize.format(this.y, "N");
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                   
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:breadcrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Consolidation Entities"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" ShowFooter="true" AutoGenerateColumns="False"
                PageSize="15" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnInsertCommand="PresentationGrid_InsertCommand"
                GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                OnItemCommand="PresentationGrid_ItemCommand" AllowAutomaticUpdates="True" EnableViewState="true"
                OnNeedDataSource="PresentationGrid_OnNeedDataSource">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="ActiveOrders" TableLayout="Fixed">
                    <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add Consolidation Entity"></CommandItemSettings>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <FooterStyle HorizontalAlign="Right" />
                    <Columns>
                        <telerik:GridButtonColumn ButtonType="LinkButton" CommandName="Select" Text="Select"
                            UniqueName="DetailColumn">
                            <HeaderStyle Width="4%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyLinkButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn SortExpression="ENTITYCIID_FIELD" ReadOnly="true" HeaderText="ENTITYCIID_FIELD"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="ENTITYCIID_FIELD" UniqueName="ENTITYCIID_FIELD"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="ClientID" ReadOnly="true" HeaderText="e-Clipse ID" FilterControlWidth="75%"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="ClientID" UniqueName="ClientID">
                            <HeaderStyle Width="6%"></HeaderStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="ENTITYNAME_FIELD" ReadOnly="false" HeaderText="Account Name"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true" FilterControlWidth="75%"
                            HeaderButtonType="TextButton"  DataField="ENTITYNAME_FIELD" UniqueName="ENTITYNAME_FIELD">
                            <HeaderStyle Width="27%"></HeaderStyle>
                        </telerik:GridBoundColumn>
                           <telerik:GridButtonColumn 
                                ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                <HeaderStyle Width="3%" HorizontalAlign="Right"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                         </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    
</asp:Content>
