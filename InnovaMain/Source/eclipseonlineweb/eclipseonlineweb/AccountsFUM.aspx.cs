﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.CM.Organization.Data;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using C1.Web.Wijmo.Controls.C1Chart;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class AccountsFUM : UMABasePage
    {
        private DataView _entityView;
        
        protected void DownloadXLS(object sender, EventArgs e)
        {
            var excelDataset = new DataSet();
            PopulateData();
            _entityView = new DataView(PresentationData.Tables["Entities_Table"]) { RowFilter = "IsClient='true'", Sort = "ENTITYNAME_FIELD ASC" };
            DataTable entityTable = _entityView.ToTable();
            entityTable.Columns.Remove("ENTITYCLID_FIELD");
            entityTable.Columns.Remove("ENTITYCSID_FIELD");
            entityTable.Columns.Remove("ENTITYCIID_FIELD");
            entityTable.Columns.Remove("ENTITYCTID_FIELD");
            entityTable.Columns.Remove("ENTITYSCTYPE_FIELD");
            entityTable.Columns.Remove("ENTITYSCSTATUS_FIELD");
            entityTable.Columns.Remove("ENTITYSORT_FIELD");

            excelDataset.Merge(entityTable, true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, "Accounts-FUM-" + DateTime.Today.ToString("dd-MMM-yyyy") + "-" + comboBoxServiceType.SelectedItem.Text + ".xls", Page.Response);
        }
        
        protected void Filter(object sender, C1GridViewFilterEventArgs e)
        {
            e.Values[0] = ((string)e.Values[0]).Trim();
            _entityView = new DataView(PresentationData.Tables["Entities_Table"]) { RowFilter = "IsClient='true'", Sort = "ENTITYNAME_FIELD ASC" };
        }

        protected override void GetBMCData()
        {
            PopulateData();
        }

        protected void GenerateAccountsFUM(object sender, EventArgs e)
        {
            PopulateData();
        }

        private void PopulateData()
        {
            IBrokerManagedComponent iBMC = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            var ds = new OrganisationListingDS
                         {
                             AsOfDate = financialSummaryDate.SelectedDate.HasValue ? financialSummaryDate.SelectedDate.Value : DateTime.Now
                         };

            ds.OrganisationListingOperationType = OrganisationListingOperationType.ClientsWithFUM;
   
            if (comboBoxServiceType.SelectedItem.Value == "doitforme")
                ds.ServiceTypes = Oritax.TaxSimp.Data.ServiceTypes.DoItForMe;
            else if (comboBoxServiceType.SelectedItem.Value == "doitwithme")
                ds.ServiceTypes = Oritax.TaxSimp.Data.ServiceTypes.DoItWithMe;
            else if (comboBoxServiceType.SelectedItem.Value == "doityourself")
                ds.ServiceTypes = Oritax.TaxSimp.Data.ServiceTypes.DoItYourSelf;
            else if (comboBoxServiceType.SelectedItem.Value == "all")
                ds.ServiceTypes = Oritax.TaxSimp.Data.ServiceTypes.None;

            iBMC.GetData(ds);

            PresentationData = ds;
            this.financialSummaryDate.DbSelectedDate = ds.AsOfDate;
            UMABroker.ReleaseBrokerManagedComponent(iBMC);
            BindControls(ds);
        }

        private void BindControls(DataSet ds)
        {
            _entityView = new DataView(ds.Tables["Entities_Table"]) { RowFilter = "IsClient='true'", Sort = "ENTITYNAME_FIELD ASC" };
            DataTable entityTable = _entityView.ToTable();

            double holdingTotal = Convert.ToDouble(entityTable.Select().Sum(row => Convert.ToDecimal(row["Holding"])));
            double unsettled = Convert.ToDouble(entityTable.Select().Sum(row => Convert.ToDecimal(row["Unsettled"])));
            double total = Convert.ToDouble(entityTable.Select().Sum(row => Convert.ToDecimal(row["Total"])));

            lblFUMTOTAL.Text = holdingTotal.ToString("C");

            lblClientCorporationPrivate.Text = entityTable.Select().Where(row => row["ClientType"].ToString() == "Corporation Private").Sum(row => Convert.ToDecimal(row["Holding"])).ToString("C");
            lblClientCorporationPublic.Text = entityTable.Select().Where(row => row["ClientType"].ToString() == "Corporation Public").Sum(row => Convert.ToDecimal(row["Holding"])).ToString("C");
            lblClientEClipseSuper.Text = entityTable.Select().Where(row => row["ClientType"].ToString() == "e-Clipse Super").Sum(row => Convert.ToDecimal(row["Holding"])).ToString("C");
            lblClientIndividual.Text = entityTable.Select().Where(row => row["ClientType"].ToString() == "Client Individual").Sum(row => Convert.ToDecimal(row["Holding"])).ToString("C");
            lblClientOtherTrustsCorporate.Text = entityTable.Select().Where(row => row["ClientType"].ToString() == "Other Corporate Trustee").Sum(row => Convert.ToDecimal(row["Holding"])).ToString("C");
            lblClientOtherTrustsIndividual.Text = entityTable.Select().Where(row => row["ClientType"].ToString() == "Other Individual Trustee").Sum(row => Convert.ToDecimal(row["Holding"])).ToString("C");
            lblClientSMSFCorporateTrustee.Text = entityTable.Select().Where(row => row["ClientType"].ToString() == "Corporate SMSF Trustee").Sum(row => Convert.ToDecimal(row["Holding"])).ToString("C");
            lblClientSMSFIndividualTrustee.Text = entityTable.Select().Where(row => row["ClientType"].ToString() == "Individual SMSF Trustee").Sum(row => Convert.ToDecimal(row["Holding"])).ToString("C");
            lblClientSoleTrader.Text = entityTable.Select().Where(row => row["ClientType"].ToString() == "Sole Trader").Sum(row => Convert.ToDecimal(row["Holding"])).ToString("C");
            lblTotalAccounts.Text = entityTable.Rows.Count.ToString();

            BarChartSetteledUnsettled.SeriesList.Clear();

            var holdingSeries = new BarChartSeries { Label = "Settled Holding" };
            holdingSeries.Data.X.Add("Settled Holding");
            holdingSeries.Data.Y.Values.Add(new ChartYData(holdingTotal));

            var unsettledSeries = new BarChartSeries { Label = "Unsettled Holding" };
            unsettledSeries.Data.X.Add("Unsettled Holding");
            unsettledSeries.Data.Y.Values.Add(new ChartYData(unsettled));

            var totalSeries = new BarChartSeries { Label = "Total Holding" };
            totalSeries.Data.X.Add("Holdings");
            totalSeries.Data.Y.Values.Add(new ChartYData(total));

            BarChartSetteledUnsettled.SeriesList.Add(holdingSeries);
            BarChartSetteledUnsettled.SeriesList.Add(unsettledSeries);
            BarChartSetteledUnsettled.SeriesList.Add(totalSeries);

            var chartTable = new DataTable();
            chartTable.Columns.Add("Desc");
            chartTable.Columns.Add("Count", typeof(double));
            var entityGroups = ds.Tables["Entities_Table"].Select("IsClient='true'").GroupBy(ent => ent["ClientType"]);

            foreach (var entitytype in entityGroups)
            {
                DataRow chartRow = chartTable.NewRow();
                chartRow["Desc"] = entitytype.Key;
                chartRow["Count"] = entitytype.Count();
                chartTable.Rows.Add(chartRow);
            }

            var chartBinding = new C1ChartBinding
                                   {
                                       YField = "Count",
                                       YFieldType = ChartDataYFieldType.Number,
                                       XField = "Desc",
                                       XFieldType = ChartDataXFieldType.String
                                   };
            ClientsCount.DataBindings.Add(chartBinding);
            ClientsCount.DataSource = chartTable;
            ClientsCount.DataBind(); PresentationGrid.Rebind(); 
        }

        public override void LoadPage()
        {
            base.LoadPage();

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                this.pnlHoldingChart.Visible = false;

            ScriptManager sm = ScriptManager.GetCurrent(Page);
            sm.RegisterPostBackControl(BtnGenerateReport);
            sm.RegisterPostBackControl(BtnDownloadReport);
            sm.RegisterPostBackControl(PresentationGrid);
     }

        public override void PopulatePage(DataSet ds)
        {
            _entityView = new DataView(ds.Tables["Entities_Table"]) { RowFilter = "IsClient='true'", Sort = "ENTITYNAME_FIELD ASC" };
            DataTable entityTable = _entityView.ToTable();
            lblFUMTOTAL.Text = entityTable.Select().Sum(row => Convert.ToDecimal(row["Holding"])).ToString("C");
            lblTotalAccounts.Text = entityTable.Rows.Count.ToString();
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            PresentationGrid.DataSource = _entityView.ToTable();
        }

        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                string Ins = e.Item.Cells[3].Text;
                Response.Redirect("ClientViews/ClientMainView.aspx?INS=" + Ins + "&PageName=" + "AccountFUM" ); 
            }
        }

        protected void PresentationGrid_OnPreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                const string status = "Closed";
                PresentationGrid.MasterTableView.FilterExpression = string.Format("([Status] <> '{0}')", status);
                GridColumn column = PresentationGrid.MasterTableView.GetColumnSafe("Status");
                column.CurrentFilterFunction = GridKnownFunction.NotEqualTo;
                column.CurrentFilterValue = status;
                PresentationGrid.MasterTableView.Rebind();
            }
        }
    }
}
