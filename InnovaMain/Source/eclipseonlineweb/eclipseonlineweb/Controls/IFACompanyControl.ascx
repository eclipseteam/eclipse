﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IFACompanyControl.ascx.cs"
    EnableViewState="true" Inherits="eclipseonlineweb.Controls.IFACompanyControl" %>
<%@ Register Src="PhoneNumberControl.ascx" TagName="PhoneNumberControl" TagPrefix="uc1" %>
<%@ Register Src="TFN_InputControl.ascx" TagName="TFN_InputControl" TagPrefix="uc2" %>
<%@ Register Src="ABN_InputControl.ascx" TagName="ABN_InputControl" TagPrefix="uc3" %>
<%@ Register Src="ACN_InputControl.ascx" TagName="ACN_InputControl" TagPrefix="uc4" %>
<%@ Register Src="EmailControl.ascx" TagName="EmailControl" TagPrefix="uc5" %>
<script type="text/javascript">
    function SetTradingName(sender, args) {
        var TradingName = document.getElementById("<%:txtTradingName.ClientID %>").value;
        var chkthp = document.getElementById('<%: chkSameAsTrading.ClientID %>');
        var txtSgcPercentSalary = $find("<%:txtLegalName.ClientID %>");
        if (TradingName != "" && args.get_checked()) {
            txtSgcPercentSalary.set_value(TradingName);
            txtSgcPercentSalary.disable();
        }
        else {
            txtSgcPercentSalary.enable();
        }
    }
    function OnValueChangingTradingName(sender, args) {
        var TradingName = document.getElementById("<%:txtTradingName.ClientID %>").value;
        var txtSgcPercentSalary = $find("<%:txtLegalName.ClientID %>");
        var button = $find("<%: chkSameAsTrading.ClientID%>");
        if (button.get_checked() && TradingName != "") {
            txtSgcPercentSalary.set_value(TradingName);
            txtSgcPercentSalary.disable();
        }
        else {
            txtSgcPercentSalary.enable();
        }
    }
</script>
<fieldset style="width: 98%">
    <div style="text-align: left; float: none; border: 1px; background-color: White;
        height: 25px; position: relative; vertical-align: middle; margin: 0; padding: 0;">
        <telerik:RadButton ID="btnIfaCompany" Text="Add Client Addresses" ToolTip="Save Changes"
            ValidationGroup="VGIFACompanyControl" OnClick="btnSaveIfaCompany_Click" runat="server"
            Width="32px" Height="32px" BorderStyle="None">
            <image imageurl="~/images/Save-Icon.png"></image>
        </telerik:RadButton>
    </div>
</fieldset>
<br />
<fieldset>
    <asp:Panel runat="server" ID="pnlIFACompanyDetail">
        <table width="100%">
            <tr>
                <td>
                    <asp:HiddenField ID="hfCID" runat="server" />
                    <asp:HiddenField ID="hf_IfaType" runat="server" />
                    <span class="riLabel">Trading Name *</span>
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtTradingName" Width="197px">
                        <clientevents onblur="OnValueChangingTradingName" />
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RFVTradingName" ControlToValidate="txtTradingName"
                        ErrorMessage="*" ForeColor="Red" ValidationGroup="VGIFACompanyControl"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <span class="riLabel">Legal Name *</span>
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtLegalName" Width="196px">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtLegalName"
                        ErrorMessage="*" ForeColor="Red" ValidationGroup="VGIFACompanyControl"></asp:RequiredFieldValidator>
                    <telerik:RadButton ButtonType="ToggleButton" ToggleType="CheckBox" runat="server"
                        OnClientCheckedChanged="SetTradingName" AutoPostBack="false" ID="chkSameAsTrading"
                        Text="Same as Trading Name" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Account Designation</span>
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtAccountDesignation" Width="197px">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <span class="riLabel">SK Code</span>
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtSkCode">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Facsimile</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="ph_facsimle" runat="server" />
                </td>
                <td>
                    <span class="riLabel">Phone Number</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="ph_phoneNumber" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Website Address</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtWebSiteAddress" runat="server" Width="197px">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <span class="riLabel">ABN</span>
                </td>
                <td>
                    <uc3:ABN_InputControl ID="abn_ABN" runat="server" InvalidMessage="Invalid ABN (e.g. 53 004 085 616)" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">ACN</span>
                </td>
                <td>
                    <uc4:ACN_InputControl ID="acn_ACN" runat="server" InvalidMessage="Invalid ACN (e.g. 010 499 966)" />
                </td>
                <td>
                    <span class="riLabel">TFN</span>
                </td>
                <td>
                    <uc2:TFN_InputControl ID="tfn_TFN" runat="server" InvalidMessage="Invalid TFN (e.g. 123 456 782)" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Email Address</span>
                </td>
                <td>
                    <uc5:EmailControl ID="txtEmail" runat="server" />
                </td>
                <td>
                    <span class="riLabel"></span>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Date Last Audited</span>
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtLastAudited" runat="server" Width="199px" AutoPostBack="true"
                        DateInput-EmptyMessage="" MinDate="01/01/1000" MaxDate="01/01/3000">
                        <calendar runat="server">
                            <SpecialDays>
                                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-CssClass="rcToday">
                                </telerik:RadCalendarDay>
                            </SpecialDays>
                        </calendar>
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <span class="riLabel">Previous Practice Name</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtPreviousPractice" runat="server" Width="197px">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Turnover ($)</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtTurnOver" runat="server" Width="197px">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <span class="riLabel">FUM($)</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtFUM" runat="server" Width="197px">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center;">
                    <asp:Label runat="server" ID="txtMessage" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
</fieldset>
