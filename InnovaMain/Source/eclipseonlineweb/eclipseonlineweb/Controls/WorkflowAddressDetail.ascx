﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WorkflowAddressDetail.ascx.cs"
    Inherits="eclipseonlineweb.Controls.WorkflowAddressDetail" %>
<%@ Register Src="../Controls/AddressControl.ascx" TagName="AddressControl" TagPrefix="uc2" %>
<asp:Panel ID="pnlMain" runat="server">
    <fieldset>
        <table width="100%">
            <tr>
                <td>
                    <uc2:AddressControl ID="MailingAddressControl" Title="Account Mailing Address" runat="server" />
                    <br />
                    <uc2:AddressControl ID="DuplicateMailingAddressControl" Title="Address for Bankwest Statements (Duplicate Mailing Address )"
                        runat="server" />
                    <br />
                    <uc2:AddressControl ID="RegisteredAddressControl" Title="Registered Address of the company /Corporate Trustee as per AISIC register (PO Box, RMB or c/- is not sufficient)"
                        runat="server" />
                    <br />
                    <uc2:AddressControl ID="BusinessAddressControl" Title="Principal Place of the Business of the Trust/Super Fund/Company (Business Address)"
                        runat="server" />
                </td>
            </tr>
        </table>
    </fieldset>
</asp:Panel>
