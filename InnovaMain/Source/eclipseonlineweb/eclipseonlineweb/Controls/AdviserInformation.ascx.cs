﻿using System;
using System.Data;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;

namespace eclipseonlineweb.Controls
{
    public partial class AdviserInformation : UserControl
    {
        public Action<string, DataSet> SaveData
        {
            get;
            set;
        }
        public Action<Guid> Saved
        {
            get;
            set;
        }
        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            hfCID.Value = Request.Params["ins"];
            if (!IsPostBack)
            {
                LoadControls();
                if (!string.IsNullOrEmpty(hfCID.Value))
                {
                    Guid cid = Guid.Parse(hfCID.Value);
                    GetData(cid);
                }
            }
        }

        /// <summary>
        /// Binding Combo Boxes
        /// </summary>
        private void LoadControls()
        {
            //Binding Status combo box with organization Status
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            var status = new StatusDS { TypeFilter = "Organization" };
            org.GetData(status);
            cmbStatus.Items.Clear();
            cmbStatus.DataSource = status.Tables[StatusDS.TABLENAME];
            cmbStatus.DataTextField = StatusDS.NAME;
            cmbStatus.DataValueField = StatusDS.NAME;
            cmbStatus.DataBind();
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            if (ValidateData())
            {
                var adviserDs = new AdvisorDS();
                var cid = (string.IsNullOrEmpty(hfCID.Value)) ? Guid.Empty : new Guid(hfCID.Value);

                adviserDs.CommandType = cid == Guid.Empty ? DatasetCommandTypes.Add : DatasetCommandTypes.Update;

                adviserDs.AdvisorTable.Merge(GetEntity().AdvisorTable);
                if (cid == Guid.Empty)
                {
                    var unit = new OrganizationUnit
                    {
                        Name = txtFirstName.Text,
                        Type = ((int)OrganizationType.Adviser).ToString(),
                        CurrentUser = (Page as UMABasePage).GetCurrentUser()
                    };

                    adviserDs.Unit = unit;
                    adviserDs.Command = (int)WebCommands.AddNewOrganizationUnit;
                }

                if (SaveData != null)
                {
                    SaveData(cid.ToString(), adviserDs);
                    if (adviserDs.CommandType == DatasetCommandTypes.Add)
                    {
                        cid = adviserDs.Unit.Cid;
                        hfCID.Value = cid.ToString();
                        if (Saved != null)
                        {
                            Saved(cid);
                        }
                    }
                }

            }
        }

        /// <summary>
        /// Validate Data
        /// </summary>
        /// <returns>bool</returns>
        private bool ValidateData()
        {
            bool result = true;
            //TODO: apply validation as per requirement if any.
            return result;
        }

        /// <summary>
        /// Get Adviser Detail Information
        /// </summary>
        /// <param name="cid">Adviser CID</param>
        private void GetData(Guid cid)
        {
            IBrokerManagedComponent adviserData = Broker.GetBMCInstance(cid);
            var advisorDs = new AdvisorDS
            {
                CommandType = DatasetCommandTypes.GetChildren,
            };
            adviserData.GetData(advisorDs);
            Broker.ReleaseBrokerManagedComponent(adviserData);
            ClearEntity();
            SetEntity(advisorDs);
        }

        /// <summary>
        /// Get Adviser Detail Information in a Dataset for Saving.
        /// </summary>
        /// <returns>AdviserDS</returns>
        private AdvisorDS GetEntity()
        {
            var ds = new AdvisorDS();
            DataRow dr = ds.Tables[ds.AdvisorTable.TableName].NewRow();

            dr[ds.AdvisorTable.STATUS] = cmbStatus.SelectedValue;
            
            if (!string.IsNullOrEmpty(txtClientID.Text))
            {
                dr[ds.AdvisorTable.CLIENTID] = txtClientID.Text;
            }

            if (!string.IsNullOrEmpty(this.txtOtherID.Text))
            {
                dr[ds.AdvisorTable.OTHERID] = txtOtherID.Text;
            }
            
            dr[ds.AdvisorTable.TITLE] = cmbTitle.SelectedValue;
            dr[ds.AdvisorTable.PREFERREDNAME] = txtPreferredName.Text;
            dr[ds.AdvisorTable.FIRSTNAME] = txtFirstName.Text;
            dr[ds.AdvisorTable.SURNAME] = txtSurname.Text;
            dr[ds.AdvisorTable.EMAIL] = txtEmail.Text;
            dr[ds.AdvisorTable.GENDER] = cmbGender.SelectedValue;
            dr[ds.AdvisorTable.HOMEPHONECOUNTRYCODE] = HomePhoneNumber.CountryCode;
            dr[ds.AdvisorTable.HOMEPHONECITYCODE] = HomePhoneNumber.CityCode;
            dr[ds.AdvisorTable.HOMEPHONENUMBER] = HomePhoneNumber.Number;
            dr[ds.AdvisorTable.TFN] = TFNControl.GetEntity();
            dr[ds.AdvisorTable.MOBILEPHONECOUNTRYCODE] = MobilePhoneNumber.CountryCode;
            dr[ds.AdvisorTable.MOBILEPHONENUMBER] = MobilePhoneNumber.Number;
            dr[ds.AdvisorTable.FACSIMILECOUNTRYCODE] = Facsimile.CountryCode;
            dr[ds.AdvisorTable.FACSIMILECITYCODE] = Facsimile.CityCode;
            dr[ds.AdvisorTable.FACSIMILENUMBER] = Facsimile.Number;
            if (!dtDateAppointed.IsEmpty)
            {
                dr[ds.AdvisorTable.APPOINTEDDATE] = dtDateAppointed.SelectedDate;
            }
            dr[ds.AdvisorTable.WORKPHONECOUNTRYCODE] = WorkPhoneNumber.CountryCode;
            dr[ds.AdvisorTable.WORKPHONECITYCODE] = WorkPhoneNumber.CityCode;
            dr[ds.AdvisorTable.WORKPHONENUMBER] = WorkPhoneNumber.Number;
            dr[ds.AdvisorTable.AUTHORISEDREPNO] = txtAuthorisedRepNo.Text;
            dr[ds.AdvisorTable.REASONTERMINATED] = txtReasonTerminated.Text;
            dr[ds.AdvisorTable.OCCUPATION] = txtOccupation.Text;

            if (!dtDateTerminated.IsEmpty)
            {
                dr[ds.AdvisorTable.TERMINATEDDATE] = dtDateTerminated.SelectedDate;
            }
            dr[ds.AdvisorTable.BTWRAPADVISERCODE] = txtBTWRAPAdviserCode.Text;
            dr[ds.AdvisorTable.DESKTOPBROKERADVISERCODE] = txtDesktopBrokerAdviserCode.Text;
            dr[ds.AdvisorTable.AFSLICENSEENAME] = txtAFSLicenseeName.Text;
            dr[ds.AdvisorTable.AFSLICENCENUMBER] = txtAFSLicenseNumber.Text;
            dr[ds.AdvisorTable.BANKWESTADVISORCODE] = txtBankWestAdviserCode.Text;

            ds.Tables[ds.AdvisorTable.TableName].Rows.Add(dr);
            return ds;
        }

        /// <summary>
        /// Populate Adviser Information Details
        /// </summary>
        /// <param name="ds">AdviserDS</param>
        private void SetEntity(AdvisorDS ds)
        {
            if (ds.AdvisorTable.Rows.Count > 0)
            {
                cmbStatus.SelectedValue = ds.AdvisorTable.Rows[0][ds.AdvisorTable.STATUS].ToString();
                txtClientID.Text = ds.AdvisorTable.Rows[0][ds.AdvisorTable.CLIENTID].ToString();
                this.txtOtherID.Text = ds.AdvisorTable.Rows[0][ds.AdvisorTable.OTHERID].ToString();
                cmbTitle.SelectedValue = ds.AdvisorTable.Rows[0][ds.AdvisorTable.TITLE].ToString();
                txtPreferredName.Text = ds.AdvisorTable.Rows[0][ds.AdvisorTable.PREFERREDNAME].ToString();
                txtFirstName.Text = ds.AdvisorTable.Rows[0][ds.AdvisorTable.FIRSTNAME].ToString();
                txtSurname.Text = ds.AdvisorTable.Rows[0][ds.AdvisorTable.SURNAME].ToString();
                txtEmail.Text = ds.AdvisorTable.Rows[0][ds.AdvisorTable.EMAIL].ToString();
                cmbGender.SelectedValue = ds.AdvisorTable.Rows[0][ds.AdvisorTable.GENDER].ToString();
                HomePhoneNumber.CountryCode = ds.AdvisorTable.Rows[0][ds.AdvisorTable.HOMEPHONECOUNTRYCODE].ToString();
                HomePhoneNumber.CityCode = ds.AdvisorTable.Rows[0][ds.AdvisorTable.HOMEPHONECITYCODE].ToString();
                HomePhoneNumber.Number = ds.AdvisorTable.Rows[0][ds.AdvisorTable.HOMEPHONENUMBER].ToString();
                TFNControl.SetEntity(ds.AdvisorTable.Rows[0][ds.AdvisorTable.TFN].ToString());
                MobilePhoneNumber.CountryCode = ds.AdvisorTable.Rows[0][ds.AdvisorTable.MOBILEPHONECOUNTRYCODE].ToString();
                MobilePhoneNumber.Number = ds.AdvisorTable.Rows[0][ds.AdvisorTable.MOBILEPHONENUMBER].ToString();
                Facsimile.CountryCode = ds.AdvisorTable.Rows[0][ds.AdvisorTable.FACSIMILECOUNTRYCODE].ToString();
                Facsimile.CityCode = ds.AdvisorTable.Rows[0][ds.AdvisorTable.FACSIMILECITYCODE].ToString();
                Facsimile.Number = ds.AdvisorTable.Rows[0][ds.AdvisorTable.FACSIMILENUMBER].ToString();
                if (ds.AdvisorTable.Rows[0][ds.AdvisorTable.APPOINTEDDATE] != DBNull.Value || !string.IsNullOrEmpty(ds.AdvisorTable.Rows[0][ds.AdvisorTable.APPOINTEDDATE].ToString()))
                {
                    dtDateAppointed.SelectedDate = Convert.ToDateTime(ds.AdvisorTable.Rows[0][ds.AdvisorTable.APPOINTEDDATE].ToString());
                }
                WorkPhoneNumber.CountryCode = ds.AdvisorTable.Rows[0][ds.AdvisorTable.WORKPHONECOUNTRYCODE].ToString();
                WorkPhoneNumber.CityCode = ds.AdvisorTable.Rows[0][ds.AdvisorTable.WORKPHONECITYCODE].ToString();
                WorkPhoneNumber.Number = ds.AdvisorTable.Rows[0][ds.AdvisorTable.WORKPHONENUMBER].ToString();
                txtAuthorisedRepNo.Text = ds.AdvisorTable.Rows[0][ds.AdvisorTable.AUTHORISEDREPNO].ToString();
                txtReasonTerminated.Text = ds.AdvisorTable.Rows[0][ds.AdvisorTable.REASONTERMINATED].ToString();
                txtOccupation.Text = ds.AdvisorTable.Rows[0][ds.AdvisorTable.OCCUPATION].ToString();
                if (ds.AdvisorTable.Rows[0][ds.AdvisorTable.TERMINATEDDATE] != DBNull.Value || !string.IsNullOrEmpty(ds.AdvisorTable.Rows[0][ds.AdvisorTable.TERMINATEDDATE].ToString()))
                {
                    dtDateTerminated.SelectedDate = Convert.ToDateTime(ds.AdvisorTable.Rows[0][ds.AdvisorTable.TERMINATEDDATE].ToString());
                }
                txtBTWRAPAdviserCode.Text = ds.AdvisorTable.Rows[0][ds.AdvisorTable.BTWRAPADVISERCODE].ToString();
                txtDesktopBrokerAdviserCode.Text = ds.AdvisorTable.Rows[0][ds.AdvisorTable.DESKTOPBROKERADVISERCODE].ToString();
                txtAFSLicenseeName.Text = ds.AdvisorTable.Rows[0][ds.AdvisorTable.AFSLICENSEENAME].ToString();
                txtAFSLicenseNumber.Text = ds.AdvisorTable.Rows[0][ds.AdvisorTable.AFSLICENCENUMBER].ToString();
                txtBankWestAdviserCode.Text = ds.AdvisorTable.Rows[0][ds.AdvisorTable.BANKWESTADVISORCODE].ToString();
            }
        }

        /// <summary>
        /// Reset all controls
        /// </summary>
        private void ClearEntity()
        {
            cmbStatus.ClearSelection();
            txtClientID.Text = string.Empty;
            cmbTitle.ClearSelection();
            txtPreferredName.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtSurname.Text = string.Empty;
            txtEmail.Text = string.Empty;
            cmbGender.SelectedValue = string.Empty;
            HomePhoneNumber.ClearEntity();
            TFNControl.ClearEntity();
            MobilePhoneNumber.ClearEntity();
            Facsimile.ClearEntity();
            dtDateAppointed.Clear();
            WorkPhoneNumber.ClearEntity();
            txtAuthorisedRepNo.Text = string.Empty;
            txtReasonTerminated.Text = string.Empty;
            txtOccupation.Text = string.Empty;
            dtDateTerminated.Clear();
            txtBTWRAPAdviserCode.Text = string.Empty;
            txtDesktopBrokerAdviserCode.Text = string.Empty;
            txtAFSLicenseeName.Text = string.Empty;
            txtAFSLicenseNumber.Text = string.Empty;
            txtBankWestAdviserCode.Text = string.Empty;
        }
    }
}