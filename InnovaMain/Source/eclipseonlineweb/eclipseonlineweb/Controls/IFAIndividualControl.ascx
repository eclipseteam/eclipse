﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IFAIndividualControl.ascx.cs"
    EnableViewState="true" Inherits="eclipseonlineweb.Controls.IFAIndividualControl" %>
<%@ Register Src="PhoneNumberControl.ascx" TagName="PhoneNumberControl" TagPrefix="uc1" %>
<%@ Register Src="TFN_InputControl.ascx" TagName="TFN_InputControl" TagPrefix="uc2" %>
<%@ Register Src="ABN_InputControl.ascx" TagName="ABN_InputControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="uc5" TagName="EmailControl" Src="EmailControl.ascx" %>
<script type="text/javascript">
    function SetTradingName(sender, args) {
        var TradingName = document.getElementById("<%:txtTradingName.ClientID %>").value;
        var chkthp = document.getElementById('<%: chkSameAsTrading.ClientID %>');
        var txtSgcPercentSalary = $find("<%:txtLegalName.ClientID %>");
        if (TradingName != "" && args.get_checked()) {
            txtSgcPercentSalary.set_value(TradingName);
            txtSgcPercentSalary.disable();
        }
        else {
            txtSgcPercentSalary.enable();
        }
    }
    function OnValueChangingTradingName(sender, args) {
        var TradingName = document.getElementById("<%:txtTradingName.ClientID %>").value;
        var txtSgcPercentSalary = $find("<%:txtLegalName.ClientID %>");
        var button = $find("<%: chkSameAsTrading.ClientID%>");
        if (button.get_checked() && TradingName != "") {
            txtSgcPercentSalary.set_value(TradingName);
            txtSgcPercentSalary.disable();
        }
        else {
            txtSgcPercentSalary.enable();
        }
    }
</script>
<fieldset style="width: 98%">
    <div style="text-align: left; float: none; border: 1px; background-color: White;
        height: 25px; position: relative; vertical-align: middle; margin: 0; padding: 0;">
        <telerik:RadButton ID="btnSaveIfaIndivisual" Text="Add Client Addresses" ToolTip="Save Changes"
            OnClick="btnSaveIfaIndivisual_Click" runat="server" Width="32px" Height="32px"
            ValidationGroup="VGIFAIndividualControl" BorderStyle="None">
            <Image ImageUrl="~/images/Save-Icon.png"></Image>
        </telerik:RadButton>
    </div>
</fieldset>
<br />
<fieldset>
    <table id="TD_EditMode" runat="server" width="100%" style="background-color: White;">
        <tr>
            <td style="width: 43%;">
                <table>
                    <tr>
                        <td style="width: 45%;">
                            <span class="riLabel">Status Flag</span>
                            <asp:HiddenField ID="hfCID" runat="server" />
                            <asp:HiddenField ID="hfCLID" runat="server" />
                            <asp:HiddenField ID="hfCSID" runat="server" />
                            <asp:HiddenField ID="hf_IfaType" runat="server" />
                        </td>
                        <td style="text-align: left;">
                            &nbsp;
                            <telerik:RadComboBox ID="ddlStatusFlag" runat="server" Width="196px">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 58%;">
                <table>
                    <tr>
                        <td style="width: 40%; text-align: left;">
                            <span class="riLabel">Client ID</span>
                        </td>
                        <td>
                            <telerik:RadTextBox runat="server" ID="txtClientID" Width="196px">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</fieldset>
<fieldset>
    <asp:Panel runat="server" ID="pnlIFASoleTraderDetail">
        <table width="100%">
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Trading Name *</span>
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtTradingName" Width="196px">
                        <ClientEvents OnBlur="OnValueChangingTradingName" />
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RFVTradingName" ControlToValidate="txtTradingName"
                        ErrorMessage="*" ForeColor="Red" ValidationGroup="VGIFAIndividualControl"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <span class="riLabel">Legal Name *</span>
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtLegalName" Width="195px">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RFVLegalName" ControlToValidate="txtLegalName"
                        ErrorMessage="*" ForeColor="Red" ValidationGroup="VGIFAIndividualControl"></asp:RequiredFieldValidator>
                    <telerik:RadButton ToggleType="CheckBox" ButtonType="ToggleButton" runat="server"
                        AutoPostBack="false" OnClientCheckedChanged="SetTradingName" ID="chkSameAsTrading"
                        Text="Same as Trading Name" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Account Designation</span>
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtAccountDesignation" Width="196px">
                    </telerik:RadTextBox>
                </td>
                 <td>
                    <span class="riLabel">SK Code</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtSkCode" runat="server">
                    </telerik:RadTextBox>
                </td>
                
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Facsimile</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="FacsimleNo" runat="server" />
                </td>
                <td>
                    <span class="riLabel">Phone Number</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="PhNo" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Website Address</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtWebSite" runat="server" Width="196px">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <span class="riLabel">ABN *</span>
                </td>
                <td>
                    <uc3:ABN_InputControl ID="ABNNo" runat="server" InvalidMessage="Invalid ABN (e.g. 53 004 085 616)"
                        IsRequired="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">ACN</span>
                </td>
                <td>
                    <uc2:TFN_InputControl ID="ACNNo" runat="server" InvalidMessage="Invalid ACN (e.g. 010 499 966)" />
                </td>
                <td>
                    <span class="riLabel">Email Address</span>
                </td>
                <td>
                    <uc5:EmailControl ID="txtEmail" runat="server" />
                </td>
               
            </tr>
              
            <tr>
             <td>
                    <span class="riLabel">Date Last Audited</span>
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtLastAudited" runat="server" Width="200px" AutoPostBack="true"
                        DateInput-EmptyMessage="" MinDate="01/01/1000" MaxDate="01/01/3000">
                        <Calendar>
                            <SpecialDays>
                                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-CssClass="rcToday">
                                </telerik:RadCalendarDay>
                            </SpecialDays>
                        </Calendar>
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <span class="riLabel">Previous Practice Name</span>
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtPreviousPracticeName" Width="196px">
                    </telerik:RadTextBox>
                </td>
               
            </tr>
            <tr>
             <td>
                    <span class="riLabel">Turnover ($)</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtTurnOver" runat="server" Width="195px">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <span class="riLabel">FUM ($)</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtFum" runat="server" Width="196px">
                    </telerik:RadTextBox>
                </td>
                
            </tr>
            <tr>
                <td colspan="4" style="text-align: right;">
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
</fieldset>
