﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BankAccountMappingControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.BankAccountMappingControl" %>
<%@ Register Src="BankAccountControl.ascx" TagName="BankAccountControl" TagPrefix="uc1" %>
<%@ Register Src="BankAccountSearchControl.ascx" TagName="BankAccountSearchControl"
    TagPrefix="uc2" %>
<script type="text/javascript">
    function cancel() {
        window.close();
    }
</script>
<style type="text/css">
    .holder
    {
        width: 100%;
        display: block;
        z-index: 6;
    }
    .content
    {
        background: #fff;
        z-index: 7; /*  padding: 28px 26px 33px 25px;*/
    }
    .popup
    {
        border-radius: 7px;
        background: #6b6a63;
        margin: 30px auto 0;
        padding: 6px;
        position: absolute;
        width: 1000px;
        top: 20%;
        left: 50%;
        margin-left: -400px;
        margin-top: -40px;
        z-index: 6;
    }
    
    .popup1
    {
        border-radius: 7px;
        background: #6b6a63;
        margin: 30px auto 0;
        padding: 6px;
        position: absolute;
        width: 770px;
        top: 20%;
        left: 50%;
        margin-left: -400px;
        margin-top: -40px;
        z-index: 6;
    }
    .overlay
    {
        width: 100%;
        opacity: 0.65;
        height: 100%;
        left: 0; /*IE*/
        top: 0;
        text-align: center;
        z-index: 5;
        position: fixed;
        background-color: #444444;
    }
</style>
<table>
    <tr>
        <td>
            <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="true" OnNeedDataSource="PresentationGrid_OnNeedDataSource"
                OnItemCommand="PresentationGrid_OnItemCommand" OnItemDataBound="PresentationGrid_OnItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    ShowFooter="true" Name="Banks" TableLayout="Fixed">
                    <CommandItemTemplate>
                        <div style="padding: 5px 5px;">
                            <asp:LinkButton ID="btnAddNew" runat="server" OnClick="LnkbtnAddBankAccClick"><img style="border:0;vertical-align:middle;Width:22px;Height:22px;" alt="" src="../images/add-icon.png"/>Add New Bank Account</asp:LinkButton>&nbsp;&nbsp;
                            <asp:LinkButton ID="btnAddExisting" runat="server" OnClick="LnkbtnAddExistngBankClick"><img style="border:0;vertical-align:middle;Width:22px;Height:22px;" alt="" src="../images/add_existing.png"/>Add Existing Bank Account</asp:LinkButton>&nbsp;&nbsp;
                        </div>
                    </CommandItemTemplate>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="Cid" ReadOnly="true" HeaderText="Cid" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Cid" UniqueName="Cid" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="CLid" ReadOnly="true" HeaderText="CLid"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="CLid" UniqueName="CLid" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="CSid" ReadOnly="true" HeaderText="CSid"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="CSid" UniqueName="CSid" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="460px" HeaderStyle-Width="500px" SortExpression="AccountName"
                            ReadOnly="true" HeaderText="Account Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountName" UniqueName="AccountName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="120px" SortExpression="AccountType" HeaderText="Account Type"
                            ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountType" UniqueName="AccountType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="90px" FilterControlWidth="50px" ReadOnly="true"
                            SortExpression="BSBNo" ShowFilterIcon="true" HeaderText="BSB" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" HeaderButtonType="TextButton" DataField="BSBNo"
                            UniqueName="BSBNo">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="120px" ReadOnly="true" SortExpression="AccountNo"
                            HeaderText="Account Number" AutoPostBackOnFilter="true" ShowFilterIcon="true"
                            CurrentFilterFunction="Contains" HeaderButtonType="TextButton" DataField="AccountNo"
                            UniqueName="AccountNo">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="60px" ReadOnly="true" SortExpression="IsExternalAccount"
                            HeaderText="External" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="IsExternalAccount"
                            UniqueName="IsExternalAccount">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="110px" HeaderStyle-Width="150px" SortExpression="Institution"
                            ReadOnly="true" HeaderText="Institution" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Institution" UniqueName="Institution">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                            UniqueName="EditColumn">
                            <HeaderStyle Width="4%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                        <telerik:GridButtonColumn ConfirmText="Are you sure you want to remove the association?"
                            ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                            <HeaderStyle Width="4%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </td>
    </tr>
</table>
<div id="BankModal" runat="server" visible="false" class="holder">
    <div class="popup">
        <div class="content">
            <uc1:BankAccountControl ID="BankControl" runat="server" />
        </div>
    </div>
</div>
<div class="overlay" id="OVER" visible="False" runat="server">
</div>
<div id="BankModalExsiting" runat="server" visible="false" class="holder">
    <div class="popup1">
        <div class="content">
            <uc2:BankAccountSearchControl ID="BankAccountSearchControl" runat="server" />
        </div>
    </div>
</div>
<asp:HiddenField ID="hfIsAdviserOrClient" runat="server" />
