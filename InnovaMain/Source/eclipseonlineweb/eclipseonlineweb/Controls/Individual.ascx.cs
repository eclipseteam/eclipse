﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using C1.Web.Wijmo.Controls.C1ComboBox;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Telerik.Web.UI;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;
using System.Web.UI.HtmlControls;

namespace eclipseonlineweb.Controls
{
    public partial class Individual : System.Web.UI.UserControl
    {
        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }
        public Action Canceled { get; set; }
        public Action ValidationFailed { get; set; }

        public Action<Guid, Guid, Guid> Saved
        {
            get;
            set;
        }

        public Action<string, DataSet> SaveData
        {
            get;
            set;
        }

        public string BusinessTitle
        {
            get { return hfBussinessTitle.Value; }
            set { hfBussinessTitle.Value = value; }
        }

        public bool EnableTabs
        {
            get { return plnAttachment.Enabled; }
            set { plnIndividual.Enabled = plnIdentification.Enabled = plnAttachment.Enabled = value; }
        }

        private string URL { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.AttachmentControl.User = this.Broker.UserContext;
            this.AttachmentControl.CID = hfCID.Value;
            dtoBirth.MaxDate = DateTime.Now.Date;
        }



        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {

            var cid = UpdateData();
            if (Saved != null)
            {
                Saved(cid, new Guid(hfCLID.Value), new Guid(hfCSID.Value));
            }

            if (!string.IsNullOrEmpty(this.URL))
                Response.Redirect(this.URL);
        }

        private Guid UpdateData()
        {
            var individualDS = new IndividualDS();
            var cid = new Guid(hfCID.Value);
            if (cid == Guid.Empty)
            {
                individualDS.CommandType = DatasetCommandTypes.Add;
                if (Request.UrlReferrer.ToString().ToLower().Contains("obp"))
                {
                    Session["Search_Individual_By"] = txtgiventname.Text;
                    this.URL = "~/OBP/Individuals.aspx";
                }
            }
            else
            {
                individualDS.CommandType = DatasetCommandTypes.Update;
            }

            if (Broker != null)
            {
                individualDS.IndividualTable = GetEntity(individualDS).IndividualTable;

                if (hfCID.Value == Guid.Empty.ToString())
                {
                    var unit = new Oritax.TaxSimp.Data.OrganizationUnit
                    {
                        Name = txtgiventname.Text,
                        Type = ((int)OrganizationType.Individual).ToString(),
                        CurrentUser = (Page as UMABasePage).GetCurrentUser()
                    };

                    individualDS.Unit = unit;
                    individualDS.Command = (int)WebCommands.AddNewOrganizationUnit;
                }

                if (SaveData != null)
                {
                    SaveData(hfCID.Value, individualDS);
                    if (individualDS.CommandType == DatasetCommandTypes.Add)
                    {
                        cid = individualDS.Unit.Cid;
                        hfCLID.Value = individualDS.Unit.Clid.ToString();
                        hfCSID.Value = individualDS.Unit.Csid.ToString();
                        hfCID.Value = cid.ToString();
                    }
                }
            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            return cid;
        }

        private void LoadControls()
        {
            var org = this.Broker.GetWellKnownBMC(WellKnownCM.Organization);

            var status = new StatusDS();
            status.TypeFilter = "Individual";

            org.GetData(status);
            ddlcountry.Items.Clear();
            ddlcountry.DataSource = CountriesChoiceListISO.NameValuePairs;
            ddlcountry.DataTextField = "Name";
            ddlcountry.DataValueField = "Value";
            ddlcountry.DataBind();

            SetCountryState(ddlcountry, ComboState);

            ComboHomeCountry.Items.Clear();
            ComboHomeCountry.DataSource = CountriesChoiceListISO.NameValuePairs;
            ComboHomeCountry.DataTextField = "Name";
            ComboHomeCountry.DataValueField = "Value";
            ComboHomeCountry.DataBind();

            SetCountryState(ComboHomeCountry, ComboHomeState);
        }


        public IndividualDS GetEntity(IndividualDS individualDs)
        {
            DataTable dataTable = individualDs.Tables[individualDs.IndividualTable.TABLENAME];
            DataRow dataRow = dataTable.NewRow();
            dataRow[individualDs.IndividualTable.CID] = hfCID.Value;
            dataRow[individualDs.IndividualTable.CLID] = hfCLID.Value;
            dataRow[individualDs.IndividualTable.CSID] = hfCSID.Value;
            if (ddlpersonaltitle.SelectedItem != null)
                dataRow[individualDs.IndividualTable.PERSONALTITLE] = ddlpersonaltitle.SelectedItem.Value;
            else
            {
                dataRow[individualDs.IndividualTable.PERSONALTITLE] = string.Empty;
            }

            if (dllGender.SelectedItem != null)
                dataRow[individualDs.IndividualTable.GENDER] = dllGender.SelectedItem.Value;
            else
            {
                dataRow[individualDs.IndividualTable.GENDER] = string.Empty;
            }
            dataRow[individualDs.IndividualTable.NAME] = txtgiventname.Text;
            dataRow[individualDs.IndividualTable.SURNAME] = txtsurname.Text;
            dataRow[individualDs.IndividualTable.FULLNAME] = txtsurname.Text + " " + txtmidname.Text + " " + txtgiventname.Text;
            dataRow[individualDs.IndividualTable.MIDDLENAME] = txtmidname.Text;
            dataRow[individualDs.IndividualTable.EMAILADDRESS] = txtemailaddress.Text;

            dataRow[individualDs.IndividualTable.WORKCOUNTRYCODE] = txtworkphonenumber.CountryCode;
            dataRow[individualDs.IndividualTable.WORKCITYCODE] = txtworkphonenumber.CityCode;
            dataRow[individualDs.IndividualTable.WORKPHONENUMBER] = txtworkphonenumber.Number;
            dataRow[individualDs.IndividualTable.HOMECOUNTRYCODE] = txthomephonenumber.CountryCode;
            dataRow[individualDs.IndividualTable.HOMECITYCODE] = txthomephonenumber.CityCode;
            dataRow[individualDs.IndividualTable.HOMEPHONENUMBER] = txthomephonenumber.Number;
            dataRow[individualDs.IndividualTable.MOBILECOUNTRYCODE] = txtmobilephonenumber.CountryCode;
            dataRow[individualDs.IndividualTable.MOBILEPHONENUMBER] = txtmobilephonenumber.Number;
            dataRow[individualDs.IndividualTable.TFN] = txttfn.Text;
            dataRow[individualDs.IndividualTable.TIN] = txttin.Text;
            dataRow[individualDs.IndividualTable.FACSIMILECOUNTYCODE] = txtfacsimile.CountryCode;
            dataRow[individualDs.IndividualTable.FACSIMILECITYCODE] = txtfacsimile.CityCode;
            dataRow[individualDs.IndividualTable.FACSIMILE] = txtfacsimile.Number;
            //DateTime date;
            //if (DateTime.TryParse(dtoBirth.SelectedDate, out date))
            dataRow[individualDs.IndividualTable.DOB] = dtoBirth.SelectedDate;
            dataRow[individualDs.IndividualTable.PASSWORD] = txtpassword.Text;
            dataRow[individualDs.IndividualTable.OCCUPATION] = txtoccupation.Text;
            dataRow[individualDs.IndividualTable.EMPLOYER] = txtemployer.Text;
            dataRow[individualDs.IndividualTable.LICENSENUMBER] = txtlicensenumber.Text;
            dataRow[individualDs.IndividualTable.PASSPORTNUMBER] = txtpassportnumber.Text;
            dataRow[individualDs.IndividualTable.RESIDENTIALADDRESSLINE1] = txtaddressline1.Text;
            dataRow[individualDs.IndividualTable.RESIDENTIALADDRESSLINE2] = txtaddressline2.Text;
            dataRow[individualDs.IndividualTable.RESIDENTIALADDRESSSUBRUB] = txtsuburb.Text;
            if (ComboState.SelectedItem != null)
                dataRow[individualDs.IndividualTable.RESIDENTIALADDRESSSTATE] = ComboState.SelectedValue;
            else
            {
                dataRow[individualDs.IndividualTable.RESIDENTIALADDRESSSTATE] = ComboState.Text;
            }
            dataRow[individualDs.IndividualTable.RESIDENTIALADDRESSPOSTALCODE] = txtpostcode.Text;
            if (ddlcountry.SelectedItem != null)
                dataRow[individualDs.IndividualTable.RESIDENTIALADDRESSCOUNTRY] = ddlcountry.SelectedValue;
            else
            {
                dataRow[individualDs.IndividualTable.RESIDENTIALADDRESSCOUNTRY] = string.Empty;
            }
            dataRow[individualDs.IndividualTable.MAILINGADDRESSLINE1] = txtHomeAddress1.Text;
            dataRow[individualDs.IndividualTable.MAILINGADDRESSLINE2] = txtHomeAddress2.Text;
            dataRow[individualDs.IndividualTable.MAILINGADDRESSSUBRUB] = txtHomeSubrub.Text;
            if (ComboHomeState.SelectedItem != null)
                dataRow[individualDs.IndividualTable.MAILINGADDRESSSTATE] = ComboHomeState.SelectedValue;
            else
            {
                dataRow[individualDs.IndividualTable.MAILINGADDRESSSTATE] = ComboHomeState.Text;
            }
            dataRow[individualDs.IndividualTable.MAILINGADDRESSPOSTALCODE] = txtHomePostCode.Text;
            if (ComboHomeCountry.SelectedItem != null)
                dataRow[individualDs.IndividualTable.MAILINGADDRESSCOUNTRY] = ComboHomeCountry.SelectedValue;
            else
            {
                dataRow[individualDs.IndividualTable.MAILINGADDRESSCOUNTRY] = string.Empty;
            }
            dataRow[individualDs.IndividualTable.MAILINGADDRESSSAME] = chkSameAddress.Checked;
            dataRow[individualDs.IndividualTable.HASADDRESS] = HasAddress();
            dataTable.Rows.Add(dataRow);
            individualDs.Tables.Remove(individualDs.IndividualTable.TABLENAME);
            individualDs.Tables.Add(dataTable);
            return individualDs;
        }
        public bool HasAddress()
        {
            bool result = !(txtaddressline1.Text.Trim().Length == 0 && txtsuburb.Text.Trim().Length == 0 && txtpostcode.Text.Trim().Length == 0);
            return result;
        }
        public void SetEntity(Guid cid)
        {
            LoadControls();
            ClearEntity();
            hfCID.Value = cid.ToString();

            if (cid != Guid.Empty)
            {

                IndividualDS ds = GetDataSet(cid);
                DataRow dr = ds.Tables[ds.IndividualTable.TABLENAME].Rows[0];

                hfCID.Value = dr[ds.IndividualTable.CID].ToString();
                hfCLID.Value = dr[ds.IndividualTable.CLID].ToString();
                hfCSID.Value = dr[ds.IndividualTable.CSID].ToString();
                ddlpersonaltitle.SelectedValue = dr[ds.IndividualTable.PERSONALTITLE].ToString();
                dllGender.SelectedValue = dr[ds.IndividualTable.GENDER].ToString();
                txtgiventname.Text = dr[ds.IndividualTable.NAME].ToString();
                txtmidname.Text = dr[ds.IndividualTable.MIDDLENAME].ToString();
                txtsurname.Text = dr[ds.IndividualTable.SURNAME].ToString();
                txtemailaddress.Text = dr[ds.IndividualTable.EMAILADDRESS].ToString();
                if (dr[ds.IndividualTable.DOB] != DBNull.Value)
                {
                    DateTime dt = Convert.ToDateTime(dr[ds.IndividualTable.DOB]);
                    if (DateTime.Compare(dt, dtoBirth.MinDate) > 0)   //dt is later than MinDate
                        dtoBirth.SelectedDate = dt;
                }
                txthomephonenumber.CountryCode = dr[ds.IndividualTable.HOMECOUNTRYCODE].ToString();
                txthomephonenumber.CityCode = dr[ds.IndividualTable.HOMECITYCODE].ToString();
                txthomephonenumber.Number = dr[ds.IndividualTable.HOMEPHONENUMBER].ToString();
                txtworkphonenumber.CountryCode = dr[ds.IndividualTable.WORKCOUNTRYCODE].ToString();
                txtworkphonenumber.CityCode = dr[ds.IndividualTable.WORKCITYCODE].ToString();
                txtworkphonenumber.Number = dr[ds.IndividualTable.WORKPHONENUMBER].ToString();
                txtmobilephonenumber.CountryCode = dr[ds.IndividualTable.MOBILECOUNTRYCODE].ToString();
                txtmobilephonenumber.Number = dr[ds.IndividualTable.MOBILEPHONENUMBER].ToString();
                txtfacsimile.CountryCode = dr[ds.IndividualTable.FACSIMILECOUNTYCODE].ToString();
                txtfacsimile.CityCode = dr[ds.IndividualTable.FACSIMILECITYCODE].ToString();
                txtfacsimile.Number = dr[ds.IndividualTable.FACSIMILE].ToString();
                txtoccupation.Text = dr[ds.IndividualTable.OCCUPATION].ToString();
                txttfn.Text = dr[ds.IndividualTable.TFN].ToString();
                txttin.Text = dr[ds.IndividualTable.TIN].ToString();
                txtemployer.Text = dr[ds.IndividualTable.EMPLOYER].ToString();
                txtpassword.Text = dr[ds.IndividualTable.PASSWORD].ToString();
                txtlicensenumber.Text = dr[ds.IndividualTable.LICENSENUMBER].ToString();
                txtpassportnumber.Text = dr[ds.IndividualTable.PASSPORTNUMBER].ToString();
                txtaddressline1.Text = dr[ds.IndividualTable.RESIDENTIALADDRESSLINE1].ToString();
                txtaddressline2.Text = dr[ds.IndividualTable.RESIDENTIALADDRESSLINE2].ToString();
                txtsuburb.Text = dr[ds.IndividualTable.RESIDENTIALADDRESSSUBRUB].ToString();
                ddlcountry.SelectedValue = dr[ds.IndividualTable.RESIDENTIALADDRESSCOUNTRY].ToString();
                SetCountryState(ddlcountry, ComboState);
                ComboState.SelectedValue = dr[ds.IndividualTable.RESIDENTIALADDRESSSTATE].ToString();
                txtpostcode.Text = dr[ds.IndividualTable.RESIDENTIALADDRESSPOSTALCODE].ToString();

                txtHomeAddress1.Text = dr[ds.IndividualTable.MAILINGADDRESSLINE1].ToString();
                txtHomeAddress2.Text = dr[ds.IndividualTable.MAILINGADDRESSLINE2].ToString();
                txtHomeSubrub.Text = dr[ds.IndividualTable.MAILINGADDRESSSUBRUB].ToString();

                ComboHomeCountry.SelectedValue = dr[ds.IndividualTable.MAILINGADDRESSCOUNTRY].ToString();
                SetCountryState(ComboHomeCountry, ComboHomeState);
                ComboHomeState.SelectedValue = dr[ds.IndividualTable.MAILINGADDRESSSTATE].ToString();
                txtHomePostCode.Text = dr[ds.IndividualTable.MAILINGADDRESSPOSTALCODE].ToString();

                PresentationGrid.Rebind();

                if (this.hfCID != null && this.hfCID.Value != string.Empty)
                {
                    AttachmentControl.CID = this.hfCID.Value;
                    AttachmentControl.RebindAttachmentGrid();
                }
            }
        }

        private IndividualDS GetDataSet(Guid cid)
        {
            var individualDs = new IndividualDS { CommandType = DatasetCommandTypes.Get, Command = (int)WebCommands.GetOrganizationUnitDetails };
            individualDs.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { Cid = cid, CurrentUser = (Page as UMABasePage).GetCurrentUser(), Type = ((int)OrganizationType.Individual).ToString() };
            if (Broker != null)
            {
                IBrokerManagedComponent org = Broker.GetBMCInstance(cid);
                org.GetData(individualDs);
                Broker.ReleaseBrokerManagedComponent(org);
            }
            else
                throw new Exception("Broker Not Found");

            return individualDs;
        }

        public void ClearEntity()
        {
            hfCID.Value = Guid.Empty.ToString();
            hfCLID.Value = Guid.Empty.ToString();
            hfCSID.Value = Guid.Empty.ToString();
            hfBussinessTitle.Value = string.Empty;
            dllGender.SelectedIndex = 0;
            ddlpersonaltitle.SelectedIndex = 0;
            txtgiventname.Text = "";
            txtmidname.Text = "";
            txtsurname.Text = "";
            txtemailaddress.Text = "";
            dtoBirth.Clear();
            txthomephonenumber.ClearEntity();
            txtworkphonenumber.ClearEntity();
            txtmobilephonenumber.ClearEntity();
            txtfacsimile.ClearEntity();
            txtoccupation.Text = "";
            txttfn.Text = "";
            txttin.Text = "";
            txtemployer.Text = "";
            txtpassword.Text = "";
            txtlicensenumber.Text = "";
            txtpassportnumber.Text = "";
            txtaddressline1.Text = "";
            txtaddressline2.Text = "";
            txtsuburb.Text = "";
            ComboState.SelectedIndex = -1;
            txtpostcode.Text = "";
            ddlcountry.SelectedIndex = -1;
            txtHomeAddress1.Text = "";
            txtHomeAddress2.Text = "";
            txtHomeSubrub.Text = "";
            ComboHomeState.SelectedIndex = -1;
            txtHomePostCode.Text = "";
            ComboHomeCountry.SelectedIndex = -1;
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (Canceled != null)
            {
                Canceled();
            }
        }

        protected void ddlcountry_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            SetCountryState(ddlcountry, ComboState);
        }

        private void SetCountryState(RadComboBox country, RadComboBox state)
        {
            state.DataSource = new States((string)country.SelectedValue);
            state.DataTextField = "Name";
            state.DataValueField = "Code";
            state.DataBind();
            if (state.Items.Count > 0)
            {
                state.SelectedIndex = 0;
            }

        }

        protected void ComboHomeCountry_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            SetCountryState(ComboHomeCountry, ComboHomeState);
        }


        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var orgUnit = this.Broker.GetBMCInstance(new Guid(this.hfCID.Value)) as IOrganizationUnit;
            if (orgUnit != null)
            {
                var individualEntity = orgUnit.ClientEntity as IndividualEntity;
                if (individualEntity != null)
                    PresentationGrid.DataSource = individualEntity.DocumentTypeEntityList().Values;
            }
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                var gDataItem = e.Item as GridDataItem;
                if (gDataItem != null)
                {
                    string rowId = gDataItem.OwnerTableView.DataKeyValues[gDataItem.ItemIndex]["Type"].ToString();

                    var orgUnit = Broker.GetBMCInstance(new Guid(hfCID.Value)) as IOrganizationUnit;
                    var attachmentListDs = new AttachmentListDS();
                    if (orgUnit != null)
                    {
                        orgUnit.GetData(attachmentListDs);

                        var individualEntity = orgUnit.ClientEntity as IndividualEntity;
                        if (individualEntity != null)
                        {
                            DocumentTypeEntity selectedIdDocument = individualEntity.DocumentTypeEntityList().FirstOrDefault(c => c.Key == rowId).Value;
                            Broker.SaveOverride = true;

                            DataTable objAttachmentsTable = attachmentListDs.Tables[AttachmentListDS.ATTACHMENTTABLE];

                            if (objAttachmentsTable.Columns.Contains(AttachmentListDS.ATTACHMENTTYPENAME_FIELD))
                                objAttachmentsTable.Columns.Remove(AttachmentListDS.ATTACHMENTTYPENAME_FIELD);
                            objAttachmentsTable.AcceptChanges();

                            foreach (DataRow objRow in objAttachmentsTable.Rows)
                            {
                                if (new Guid((string)objRow[AttachmentListDS.ATTACHMENTID_FIELD]) == selectedIdDocument.AttachementID)
                                {
                                    objRow.Delete();
                                    this.Broker.LogEvent(EventType.UMAClientDoocumentDelete, orgUnit.CID, "The attachments of Client:" + orgUnit.ClientId + "-" + orgUnit.Name + "is Deleted");
                                }
                            }

                            selectedIdDocument.ExpiryDate = null;
                            selectedIdDocument.IsEnabled = false;
                            selectedIdDocument.IsPhotoShown = false;
                            selectedIdDocument.IsAddressMatch = false;
                            selectedIdDocument.IssueDate = null;
                            selectedIdDocument.IssuePlace = string.Empty;
                            selectedIdDocument.DocNumber = string.Empty;
                            selectedIdDocument.AttachementID = Guid.Empty;
                        }

                        orgUnit.SetData(attachmentListDs);
                        Broker.SaveOverride = true;
                        orgUnit.CalculateToken(true);
                        Broker.SetComplete();
                        Broker.SetStart();
                    }

                }

                e.Canceled = true;
                e.Item.Edit = false;
                PresentationGrid.Rebind();
                AttachmentControl.RebindAttachmentGrid();
            }

            if (e.CommandName.ToLower() == "update")
            {
                var gridEditableItem = e.Item as GridEditableItem;
                var orgUnit = this.Broker.GetBMCInstance(new Guid(hfCID.Value)) as IOrganizationUnit;
                if (orgUnit != null && gridEditableItem != null)
                {
                    string rowId = gridEditableItem.OwnerTableView.DataKeyValues[gridEditableItem.ItemIndex]["Type"].ToString();

                    var individualEntity = orgUnit.ClientEntity as IndividualEntity;
                    if (individualEntity != null)
                    {
                        DocumentTypeEntity selectedIdDocument = individualEntity.DocumentTypeEntityList().FirstOrDefault(c => c.Key == rowId).Value;
                        var attachmentListDs = new AttachmentListDS();
                        orgUnit.GetData(attachmentListDs);

                        Broker.SaveOverride = true;

                        DataTable objAttachmentsTable = attachmentListDs.Tables[AttachmentListDS.ATTACHMENTTABLE];
                        objAttachmentsTable.AcceptChanges();
                        var selectedRow = objAttachmentsTable.Select().FirstOrDefault(row => row[AttachmentListDS.ATTACHMENTDESCRIPTION_FIELD].ToString() == selectedIdDocument.Type);
                        if (selectedRow != null)
                        {
                            selectedRow.Delete();
                            orgUnit.SetData(attachmentListDs);

                        }
                        attachmentListDs = new AttachmentListDS();
                        orgUnit.GetData(attachmentListDs);
                        objAttachmentsTable = attachmentListDs.Tables[AttachmentListDS.ATTACHMENTTABLE];
                        objAttachmentsTable.AcceptChanges();

                        var radAsyncUpload = gridEditableItem["Upload"].FindControl("AsyncUpload1") as RadAsyncUpload;

                        if (radAsyncUpload != null)
                            foreach (UploadedFile uploadedFile in radAsyncUpload.UploadedFiles)
                            {
                                DataRow objDataRow = objAttachmentsTable.NewRow();
                                objDataRow[AttachmentListDS.ATTACHMENTID_FIELD] = Guid.NewGuid();
                                objDataRow[AttachmentListDS.ATTACHMENTDESCRIPTION_FIELD] = selectedIdDocument.Type;
                                objDataRow[AttachmentListDS.ATTACHMENTDATEIMPORTED_FIELD] = DateTime.Now;
                                objDataRow[AttachmentListDS.ATTACHMENTORIGINPATH_FIELD] = uploadedFile.FileName;
                                objDataRow[AttachmentListDS.ATTACHMENTIMPORTEDBY_FIELD] = this.Broker.UserContext.Identity.Name;
                                objDataRow[AttachmentListDS.ATTACHMENTFILE_FIELD] = uploadedFile.InputStream;
                                objDataRow[AttachmentListDS.ATTACHMENTTYPE_FIELD] = AttachmentType.ProofOfIdentity;
                                objDataRow[AttachmentListDS.ATTACHMENTLINKED_FIELD] = false;
                                objAttachmentsTable.Rows.Add(objDataRow);
                            }

                        orgUnit.SetData(attachmentListDs);
                        Broker.LogEvent(EventType.UMAClientDoocumentUpload, orgUnit.CID, "The attachments of Client: " + orgUnit.EclipseClientID + "-" + orgUnit.Name + " Added");

                        attachmentListDs = new AttachmentListDS();
                        orgUnit.GetData(attachmentListDs);
                        objAttachmentsTable = attachmentListDs.Tables[AttachmentListDS.ATTACHMENTTABLE];
                        selectedRow = objAttachmentsTable.Select().FirstOrDefault(row => row[AttachmentListDS.ATTACHMENTDESCRIPTION_FIELD].ToString() == selectedIdDocument.Type);

                        if (selectedRow != null)
                            selectedIdDocument.AttachementID = new Guid(selectedRow[AttachmentListDS.ATTACHMENTID_FIELD].ToString());

                        selectedIdDocument.DocNumber = ((TextBox)(gridEditableItem["DocNumber"].Controls[0])).Text;
                        selectedIdDocument.IsPhotoShown = ((CheckBox)(gridEditableItem["IsPhotoShown"].Controls[0])).Checked;
                        selectedIdDocument.IsAddressMatch = ((CheckBox)(gridEditableItem["IsAddressMatch"].Controls[0])).Checked;
                        selectedIdDocument.IsEnabled = ((CheckBox)(gridEditableItem["IsEnabledID"].Controls[0])).Checked;

                        DateTime? issueDate = ((RadDatePicker)(((GridEditableItem)(e.Item))["IssueDate"].Controls[0])).SelectedDate;
                        if (issueDate != null & issueDate.HasValue)
                            selectedIdDocument.IssueDate = issueDate.Value;

                        DateTime? expiryDate = ((RadDatePicker)(((GridEditableItem)(e.Item))["ExpiryDate"].Controls[0])).SelectedDate;
                        if (expiryDate != null & expiryDate.HasValue)
                            selectedIdDocument.ExpiryDate = expiryDate.Value;
                    }
                    Broker.SaveOverride = true;
                    orgUnit.CalculateToken(true);
                    Broker.SetComplete();
                    Broker.SetStart();
                }


                e.Canceled = true;
                e.Item.Edit = false;
                PresentationGrid.Rebind();
                AttachmentControl.RebindAttachmentGrid();
            }
        }

        protected void PresentationGrid_OnItemDataBound(object sender, GridItemEventArgs e)
        {
        }

        protected void chkSameAddress_OnChecked(object sender, EventArgs e)
        {
            if (chkSameAddress.Checked)
            {
                txtHomeAddress1.Text = txtaddressline1.Text;
                txtHomeAddress2.Text = txtaddressline2.Text;
                txtHomeSubrub.Text = txtsuburb.Text;
                if (ddlcountry.SelectedValue != null)
                    ComboHomeCountry.SelectedValue = ddlcountry.SelectedItem.Value;

                if (ComboState.FindItemByValue(ComboState.SelectedValue) != null)
                    ComboHomeState.SelectedValue = ComboState.SelectedValue;

                txtHomePostCode.Text = txtpostcode.Text;
                panlMailingAddress.Enabled = false;
            }
            else
                panlMailingAddress.Enabled = true;
        }


    }
}