﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DealerGroupCompanyControl.ascx.cs"
    EnableViewState="true" Inherits="eclipseonlineweb.Controls.DealerGroupCompanyControl" %>
<%@ Register Src="PhoneNumberControl.ascx" TagName="PhoneNumberControl" TagPrefix="uc1" %>
<%@ Register Src="TFN_InputControl.ascx" TagName="TFN_InputControl" TagPrefix="uc2" %>
<%@ Register Src="ABN_InputControl.ascx" TagName="ABN_InputControl" TagPrefix="uc3" %>
<%@ Register Src="ACN_InputControl.ascx" TagName="ACN_InputControl" TagPrefix="uc4" %>
<script type="text/javascript">
    function SetTradingName(sender, args) {
        var TradingName = document.getElementById("<%:txtTradingName.ClientID %>").value;
        var chkthp = document.getElementById('<%: chkSameAsTrading.ClientID %>');
        var txtSgcPercentSalary = $find("<%:txtLegalName.ClientID %>");
        if (TradingName != "" && args.get_checked()) {
            txtSgcPercentSalary.set_value(TradingName);
            txtSgcPercentSalary.disable();
        }
        else {
            txtSgcPercentSalary.enable();
        }
    }
    function OnValueChangingTradingName(sender, args) {
        var TradingName = document.getElementById("<%:txtTradingName.ClientID %>").value;
        var txtSgcPercentSalary = $find("<%:txtLegalName.ClientID %>");
        var button = $find("<%: chkSameAsTrading.ClientID%>");
        if (button.get_checked() && TradingName != "") {
            txtSgcPercentSalary.set_value(TradingName);
            txtSgcPercentSalary.disable();
        }
        else {
            txtSgcPercentSalary.enable();
        }
    }
</script>
<fieldset style="width: 98%">
    <div style="text-align: left; float: none; border: 1px; background-color: White;
        height: 25px; position: relative; vertical-align: middle; margin: 0; padding: 0;">
        <telerik:RadButton runat="server" ID="btnDealerGroupCompany" ToolTip="Save" OnClick="btnSaveDealerCompany_Click"
            ValidationGroup="VGDGC">
            <ContentTemplate>
                <img src="../images/Save-Icon.png" alt="" class="btnImageWithText" />
                <span class="riLabel">Save</span>
            </ContentTemplate>
        </telerik:RadButton>
        <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Visible="false"></asp:Label>
    </div>
</fieldset>
<br />
<fieldset>
    <asp:Panel runat="server" ID="pnlDealerGroupSoleTraderDetail">
        <table width="100%">
            <tr>
                <td>
                    <asp:HiddenField ID="hfCID" runat="server" />
                    <asp:HiddenField ID="hf_DealerGroupType" runat="server" />
                    <span class="riLabel">Name*</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtTradingName" runat="server" Width="195px">
                        <ClientEvents OnBlur="OnValueChangingTradingName" />
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="RFVTxtTradingName" runat="server" ErrorMessage="*"
                        ControlToValidate="txtTradingName" ForeColor="Red" ValidationGroup="VGDGC"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <span class="riLabel">Legal Name*</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtLegalName" runat="server" Width="195px">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="RFVLegalName" runat="server" ErrorMessage="*" ControlToValidate="txtLegalName"
                        ForeColor="Red" ValidationGroup="VGDGC"></asp:RequiredFieldValidator>
                    <telerik:RadButton ID="chkSameAsTrading" runat="server" Text="Same as Trading Name"
                        OnClientCheckedChanged="SetTradingName" AutoPostBack="false" ToggleType="CheckBox"
                        ButtonType="ToggleButton">
                    </telerik:RadButton>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Facsimile</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="ph_facsimle" runat="server" />
                </td>
                <td>
                    <span class="riLabel">Work Phone Number</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="ph_workPhone" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Advisory Firm Terminology</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtAdvisoryFirm" runat="server" Width="195px">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <span class="riLabel">Website Address</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtWebSiteAdd" runat="server" Width="195px">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">TFN</span>
                </td>
                <td>
                    <uc2:TFN_InputControl ID="TFN_InputControl1" runat="server" InvalidMessage="Invalid TFN (e.g. 123 456 782)" />
                </td>
                <td>
                    <span class="riLabel">ABN</span>
                </td>
                <td>
                    <uc3:ABN_InputControl ID="ABN_InputControl1" runat="server" InvalidMessage="Invalid ABN (e.g. 53 004 085 616)" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">AFSL</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtAFSL" runat="server" Width="195px">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <span class="riLabel">ACN</span>
                </td>
                <td>
                    <uc4:ACN_InputControl ID="ACN_InputControl1" runat="server" InvalidMessage="Invalid ACN (e.g. 010 499 966)" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">FUM($)</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtFUM" runat="server" Width="195px">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <span class="riLabel">Turnover ($)</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtTurnOver" runat="server" Width="195px">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center;">
                    <asp:Label runat="server" ID="txtMessage" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
</fieldset>
