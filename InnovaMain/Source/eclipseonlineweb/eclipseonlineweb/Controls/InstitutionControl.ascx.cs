﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;
using eclipseonlineweb.WebUtilities;

namespace eclipseonlineweb.Controls
{
    public partial class InstitutionControl : UserControl
    {
        public Action<InstitutesDS, DatasetCommandTypes> SaveData { get; set; }
        public Action Canceled { get; set; }
        public Action RetainPopup { get; set; }
        public Action ValidationFailed { get; set; }



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadControl();
            }
        }
        public void LoadControl()
        {
            if (!Page.IsPostBack)
            {
                ddlType.DataSource = InstituteCombos.Types;
                ddlType.DataTextField = "Key";
                ddlType.DataValueField = "Value";
                ddlType.DataBind();
                ddlLongTerm.DataSource = InstituteCombos.SPLongTermRatings;
                ddlLongTerm.DataTextField = "Key";
                ddlLongTerm.DataValueField = "Value";
                ddlLongTerm.DataBind();
                ddlShortTerm.DataSource = InstituteCombos.SPShortTermRatings;
                ddlShortTerm.DataTextField = "Key";
                ddlShortTerm.DataValueField = "Value";
                ddlShortTerm.DataBind();
                ddlCountry.DataSource = CountriesChoiceListISO.NameValuePairs;
                ddlCountry.DataTextField = "Name";
                ddlCountry.DataValueField = "Value";
                ddlCountry.DataBind();
            }
            //LoadStates();
        }

        private void LoadStates(string Countrycode)
        {
            ddlState.DataSource = new States(Countrycode);
            ddlState.DataTextField = "Name";
            ddlState.DataValueField = "Code";
            ddlState.DataBind();
            if (ddlState.Items.Count > 0)
            {
                ddlState.SelectedIndex = 0;
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlState.DataSource = new States((string)ddlCountry.SelectedValue);
            ddlState.DataTextField = "Name";
            ddlState.DataValueField = "Code";
            ddlState.DataBind();
            if (ddlState.Items.Count > 0)
            {
                ddlState.SelectedIndex = 0;
            }
            if (RetainPopup != null)
            {
                RetainPopup();
            }
        }
        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (Canceled != null)
            {
                Canceled();
            }
        }
        public void ClearEntity()
        {
            lblTitle.Text = "Add New Institute";
            txtID.Value = Guid.Empty.ToString();
            txtName.Text = "";
            txtDescription.Text = "";
            txtInsCode.Text = "";
            txtTradingName.Text = "";
            txtLegalName.Text = "";
            ddlType.SelectedValue = "0";
            ddlLongTerm.SelectedValue = "0";
            ddlShortTerm.SelectedValue = "0";
            ACN_Control.ClearEntity();
            ABN_Control.ClearEntity();
            TFN_Control.ClearEntity();
            PhoneControl.ClearEntity();
            FacsimileControl.ClearEntity();
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtPostalCode.Text = "";
            txtSubUrb.Text = "";
            ddlCountry.SelectedValue = "Australia";
            LoadStates(ddlCountry.SelectedValue);
            lblMessages.Text = "";
        }
        public void SetEntity(InstitutionEntity entity)
        {

            if (entity == null)
            {
                ClearEntity();
                return;
            }
            lblTitle.Text = "Update Institute";
            txtID.Value = entity.ID.ToString();
            txtName.Text = entity.Name;
            txtDescription.Text = entity.Description;
            ABN_Control.SetEntity(entity.ABN);
            ACN_Control.SetEntity(entity.ACN);
            FacsimileControl.Number = entity.Facsimile.PhoneNumber;
            FacsimileControl.CountryCode = entity.Facsimile.CountryCode;
            FacsimileControl.CityCode = entity.Facsimile.CityCode;
            PhoneControl.Number = entity.PhoneNumber.PhoneNumber;
            PhoneControl.CityCode = entity.PhoneNumber.CityCode;
            PhoneControl.CountryCode = entity.PhoneNumber.CountryCode;
            if (!string.IsNullOrEmpty(entity.SPCreditRatingLong.ToString().Trim()))
                ddlLongTerm.SelectedValue = entity.SPCreditRatingLong.ToString();
            if (!string.IsNullOrEmpty(entity.SPCreditRatingShort.ToString().Trim()))
                ddlShortTerm.SelectedValue = entity.SPCreditRatingShort.ToString();
            txtInsCode.Text = entity.TextInstitutionCode;
            TFN_Control.SetEntity(entity.TFN);
            txtLegalName.Text = entity.LegalName;
            txtTradingName.Text = entity.TradingName;
            chkSameAsTrading.Checked = entity.ChkLegalName;
            if (!string.IsNullOrEmpty(entity.Type.ToString().Trim()))
                ddlType.SelectedValue = entity.Type.ToString();
            txtAddress1.Text = entity.Addresses.BusinessAddress.Addressline1;
            txtAddress2.Text = entity.Addresses.BusinessAddress.Addressline2;
            txtSubUrb.Text = entity.Addresses.BusinessAddress.Suburb;
            txtPostalCode.Text = entity.Addresses.BusinessAddress.PostCode;
            if (!string.IsNullOrEmpty(entity.Addresses.BusinessAddress.Country.Trim()))
            {
                ddlCountry.SelectedValue = entity.Addresses.BusinessAddress.Country;
                LoadStates(ddlCountry.SelectedValue);
            }
            if (!string.IsNullOrEmpty(entity.Addresses.BusinessAddress.State.Trim()))
                ddlState.SelectedValue = entity.Addresses.BusinessAddress.State;

        }
        private InstitutesDS GetEntity()
        {
            var ds = new InstitutesDS();
            DataRow dr = ds.InstitutionsTable.NewRow();

            dr[ds.InstitutionsTable.ID] = Guid.Parse(txtID.Value);
            dr[ds.InstitutionsTable.NAME] = txtName.Text.Trim();
            dr[ds.InstitutionsTable.DESCRIPTION] = txtDescription.Text.Trim();
            dr[ds.InstitutionsTable.ABN] = ABN_Control.GetEntity();
            dr[ds.InstitutionsTable.ACN] = ACN_Control.GetEntity();
            dr[ds.InstitutionsTable.FACSIMILE] = FacsimileControl.Number;
            dr[ds.InstitutionsTable.FACSIMILECITYCODE] = FacsimileControl.CityCode;
            dr[ds.InstitutionsTable.FACSIMILECOUNTYCODE] = FacsimileControl.CountryCode;
            dr[ds.InstitutionsTable.PHONENUMBER] = PhoneControl.Number;
            dr[ds.InstitutionsTable.PHONENUMBERCITYCODE] = PhoneControl.CityCode;
            dr[ds.InstitutionsTable.PHONENUMBERCOUNTYCODE] = PhoneControl.CountryCode;
            dr[ds.InstitutionsTable.SPCREDITRATINGLONG] = Convert.ToInt32(ddlLongTerm.SelectedValue);
            dr[ds.InstitutionsTable.SPCREDITRATINGSHORT] = Convert.ToInt32(ddlShortTerm.SelectedValue);
            dr[ds.InstitutionsTable.INSTITUTIONCODE] = txtInsCode.Text;
            dr[ds.InstitutionsTable.TFN] = TFN_Control.GetEntity();
            dr[ds.InstitutionsTable.TRADINGNAME] = txtTradingName.Text;
            dr[ds.InstitutionsTable.LEGALNAME] = chkSameAsTrading.Checked ? txtTradingName.Text : txtLegalName.Text;
            dr[ds.InstitutionsTable.CHKLEGALNAME] = chkSameAsTrading.Checked;
            dr[ds.InstitutionsTable.TYPE] = Convert.ToInt32(ddlType.SelectedValue);
            dr[ds.InstitutionsTable.ADDRESSLINE1] = txtAddress1.Text;
            dr[ds.InstitutionsTable.ADDRESSLINE2] = txtAddress2.Text;
            dr[ds.InstitutionsTable.SUBURB] = txtSubUrb.Text;
            dr[ds.InstitutionsTable.STATE] = ddlState.SelectedValue;
            dr[ds.InstitutionsTable.COUNTRY] = ddlCountry.SelectedValue;
            dr[ds.InstitutionsTable.POSTCODE] = txtPostalCode.Text;
            ds.InstitutionsTable.Rows.Add(dr);
            return ds;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Validate() == false)
            {
                if (ValidationFailed != null)
                    ValidationFailed();
                return;
            }

            if (SaveData != null)
            {
                var instituteDS = GetEntity();

                if (txtID.Value != Guid.Empty.ToString())
                {
                    SaveData(instituteDS, DatasetCommandTypes.Update);
                }
                else
                {
                    SaveData(instituteDS, DatasetCommandTypes.Add);
                }
            }
        }
        public bool Validate()
        {
            var messages = new StringBuilder();
            bool tempResult = txtName.ValidateAlphaNumeric(1, 255);
            bool result = tempResult;
            AddMessageIn(tempResult, "Invalid Institute Name*", messages);
            tempResult = txtDescription.ValidateAlphaNumeric(0, 255);
            result = tempResult && result;
            AddMessageIn(tempResult, "Invalid Description Name*", messages);
            tempResult = txtInsCode.Text.Trim().Length == 0;
            if (!tempResult)
            {
                tempResult = ValidateInstitutionCode(txtInsCode.Text.Trim());
            }
            result = tempResult && result;
            AddMessageIn(tempResult, "Invalid Institute Code*", messages);
            if (!string.IsNullOrEmpty(ABN_Control.GetEntity()))
            {
                tempResult = ABN_Control.ValidateNumber(ABN_Control.GetEntity());
                AddMessageIn(tempResult, "Invalid ABN Number*", messages);
                result = tempResult && result;
            }

            if (!string.IsNullOrEmpty(TFN_Control.GetEntity()))
            {
                tempResult = TFN_Control.ValidateNumber(TFN_Control.GetEntity());
                AddMessageIn(tempResult, "Invalid TFN Number*", messages);
                result = tempResult && result;
            }

            if (!string.IsNullOrEmpty(ACN_Control.GetEntity()))
            {
                tempResult = ACN_Control.ValidateNumber(ACN_Control.GetEntity());
                AddMessageIn(tempResult, "Invalid ACN Name*", messages);
                result = tempResult && result;
            }

            lblMessages.Text = messages.ToString();
            return result;
        }
        private bool ValidateInstitutionCode(string Institutioncode)
        {
            bool isValid = true;

            if (string.IsNullOrEmpty(Institutioncode))
                isValid = false;

            if (Institutioncode.Length != 6)
                isValid = false;

            return isValid;
        }
        private void AddMessageIn(bool result, string message, StringBuilder messages)
        {
            if (!result)
            {
                messages.Append(message + "<br/>");
            }
        }
    }
}