﻿using System;
using System.Data;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;

namespace eclipseonlineweb.Controls
{
    public partial class IFAIndividualControl : System.Web.UI.UserControl
    {
        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        public Action<Guid> Saved
        {
            get;
            set;
        }

        public Action<Guid, DataSet> SaveData
        {
            get;
            set;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadControls();
            }
        }
        private void LoadControls()
        {
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            var status = new StatusDS { TypeFilter = "Organization" };
            org.GetData(status);
            ddlStatusFlag.Items.Clear();
            ddlStatusFlag.DataSource = status.Tables[StatusDS.TABLENAME];
            ddlStatusFlag.DataTextField = StatusDS.NAME;
            ddlStatusFlag.DataValueField = StatusDS.NAME;
            ddlStatusFlag.DataBind();
        }
        public IFADS GetEntity()
        {
            IFADS ifaDs = new IFADS();
            DataRow dr = ifaDs.Tables[ifaDs.ifaTable.TableName].NewRow();
            dr[ifaDs.ifaTable.TRADINGNAME] = txtTradingName.Text;
            dr[ifaDs.ifaTable.STATUS] = ddlStatusFlag.SelectedValue;
            dr[ifaDs.ifaTable.IFATYPE] = IFAEntityType.IFAIndividualControl;
            dr[ifaDs.ifaTable.NAME] = txtTradingName.Text;
            dr[ifaDs.ifaTable.EMAIL] = txtEmail.GetEntity();
            if (chkSameAsTrading.Checked)
            {
                dr[ifaDs.ifaTable.LEGALNAME] = txtTradingName.Text;
            }
            else
            {
                dr[ifaDs.ifaTable.LEGALNAME] = txtLegalName.Text;
            }
            dr[ifaDs.ifaTable.ACCOUNTDESIGNATION] = txtAccountDesignation.Text;
            dr[ifaDs.ifaTable.FACSIMILE] = FacsimleNo.Number;
            dr[ifaDs.ifaTable.FACSIMILECITYCODE] = FacsimleNo.CityCode;
            dr[ifaDs.ifaTable.FACSIMILECOUNTRYCODE] = FacsimleNo.CountryCode;
            dr[ifaDs.ifaTable.PHONENUBER] = PhNo.Number;
            dr[ifaDs.ifaTable.PHONENUMBERCOUNTRYCODE] = PhNo.CountryCode;
            dr[ifaDs.ifaTable.PHONENUMBERCITYCODE] = PhNo.CityCode;
            dr[ifaDs.ifaTable.SKCODE] = txtSkCode.Text;
            dr[ifaDs.ifaTable.WEBSITEADDRESS] = txtWebSite.Text;
            dr[ifaDs.ifaTable.ABN] = ABNNo.GetEntity();
            dr[ifaDs.ifaTable.ACN] = ACNNo.GetEntity();
            if (dtLastAudited.SelectedDate != null)
                dr[ifaDs.ifaTable.LASTAUDITEDDATE] = dtLastAudited.SelectedDate.Value.ToShortDateString();
            dr[ifaDs.ifaTable.PREVIOUSPRACTICE] = txtPreviousPracticeName.Text;
            dr[ifaDs.ifaTable.TURNOVER] = txtTurnOver.Text;
            dr[ifaDs.ifaTable.FUM] = txtFum.Text;
            ifaDs.ifaTable.Rows.Add(dr);
            return ifaDs;
        }
        public void SetEntity(Guid CID, IFAEntityType IFAType)
        {
            ClearEntity();
            hf_IfaType.Value = IFAType.ToString();
            if (CID != Guid.Empty)
            {
                IFADS ds = GetIFADetails(CID);
                hfCID.Value = CID.ToString();
                DataRow dr = ds.ifaTable.Rows[0];
                txtClientID.Text = dr[ds.ifaTable.CLIENTID].ToString();
                txtTradingName.Text = dr[ds.ifaTable.TRADINGNAME].ToString();
                txtLegalName.Text = dr[ds.ifaTable.LEGALNAME].ToString();
                txtEmail.SetEntity(dr[ds.ifaTable.EMAIL].ToString());
                if (txtTradingName.Text == txtLegalName.Text)
                {
                    chkSameAsTrading.Checked = true;
                    txtLegalName.Enabled = false;
                }
                else
                {
                    chkSameAsTrading.Checked = false;
                    txtLegalName.Enabled = true;
                }
                ddlStatusFlag.SelectedValue = dr[ds.ifaTable.STATUS].ToString();
                txtAccountDesignation.Text = dr[ds.ifaTable.ACCOUNTDESIGNATION].ToString();
                FacsimleNo.Number = (dr[ds.ifaTable.FACSIMILE].ToString());
                FacsimleNo.CountryCode = (dr[ds.ifaTable.FACSIMILECOUNTRYCODE].ToString());
                FacsimleNo.CityCode = (dr[ds.ifaTable.FACSIMILECITYCODE].ToString());
                PhNo.Number = (dr[ds.ifaTable.PHONENUBER].ToString());
                PhNo.CountryCode = (dr[ds.ifaTable.PHONENUMBERCOUNTRYCODE].ToString());
                PhNo.CityCode = (dr[ds.ifaTable.PHONENUMBERCITYCODE].ToString());
                txtSkCode.Text = dr[ds.ifaTable.SKCODE].ToString();
                txtWebSite.Text = dr[ds.ifaTable.WEBSITEADDRESS].ToString();
                ABNNo.SetEntity(dr[ds.ifaTable.ABN].ToString());
                ACNNo.SetEntity(dr[ds.ifaTable.ACN].ToString());
                if (!string.IsNullOrEmpty(dr[ds.ifaTable.LASTAUDITEDDATE].ToString()))
                    dtLastAudited.SelectedDate = Convert.ToDateTime(dr[ds.ifaTable.LASTAUDITEDDATE].ToString());
                txtPreviousPracticeName.Text = dr[ds.ifaTable.PREVIOUSPRACTICE].ToString();
                txtTurnOver.Text = dr[ds.ifaTable.TURNOVER].ToString();
                txtFum.Text = dr[ds.ifaTable.FUM].ToString();
            }
        }
        private IFADS GetIFADetails(Guid cid)
        {
            var ifaDs = new IFADS { CommandType = DatasetCommandTypes.Details };
            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
                clientData.GetData(ifaDs);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }
            return ifaDs;
        }
        public void ClearEntity()
        {
            hfCID.Value = Guid.Empty.ToString();
            hf_IfaType.Value = "";
            txtClientID.Text = "";
            txtTradingName.Text = "";
            txtLegalName.Text = "";
            txtAccountDesignation.Text = "";
            FacsimleNo.ClearEntity();
            PhNo.ClearEntity();
            txtSkCode.Text = "";
            txtWebSite.Text = "";
            ABNNo.ClearEntity();
            ACNNo.ClearEntity();
            dtLastAudited.Clear();
            dtLastAudited.DateInput.Clear();
            txtPreviousPracticeName.Text = "";
            txtTurnOver.Text = "";
            txtFum.Text = "";
            txtEmail.ClearEntity();
        }
        public void SetHiddenFields(string CID, string CLID, string CSID)
        {
            hfCID.Value = CID;
            hfCLID.Value = CLID;
            hfCSID.Value = CSID;
        }
        public bool Validate()
        {
            return Validate(true);
        }
        public bool Validate(bool ShowTabError)
        {
            bool result = true;


            result = ABNNo.Validate() & ACNNo.Validate() & FacsimleNo.Validate() & PhNo.Validate() & txtEmail.Validate();
           

            return result;
        }
       
        protected void btnSaveIfaIndivisual_Click(object sender, System.EventArgs e)
        {
            if (Validate())
            {
                IFADS ifaDS = new IFADS();
                var cid = (string.IsNullOrEmpty(hfCID.Value)) ? Guid.Empty : new Guid(hfCID.Value);
                if (cid == Guid.Empty)
                {
                    ifaDS.CommandType = DatasetCommandTypes.Add;
                }
                else
                {
                    ifaDS.CommandType = DatasetCommandTypes.Update;
                }
                if (Broker != null)
                {
                    ifaDS.ifaTable = GetEntity().ifaTable;
                    if (hfCID.Value == Guid.Empty.ToString())
                    {
                        var unit = new OrganizationUnit
                            {
                                OrganizationStatus = "Active",
                                Name = txtTradingName.Text,
                                Type = ((int) OrganizationType.IFA).ToString(),
                                CurrentUser = (Page as UMABasePage).GetCurrentUser()
                            };
                        ifaDS.Unit = unit;
                        ifaDS.Command = (int) WebCommands.AddNewOrganizationUnit;
                    }
                    if (SaveData != null)
                    {
                        SaveData(cid, ifaDS);
                        if (ifaDS.CommandType == DatasetCommandTypes.Add)
                        {
                            cid = ifaDS.Unit.Cid;
                            hfCLID.Value = ifaDS.Unit.Clid.ToString();
                            hfCSID.Value = ifaDS.Unit.Csid.ToString();
                            hfCID.Value = cid.ToString();
                        }
                    }
                    //Enable Disable Legal control
                    txtLegalName.Enabled = !chkSameAsTrading.Checked;
                }
                else
                {
                    throw new Exception("Broker Not Found");
                }
            }
        }
    }
}