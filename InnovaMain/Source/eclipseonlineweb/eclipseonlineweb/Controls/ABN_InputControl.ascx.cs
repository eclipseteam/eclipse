﻿using System;
using System.Text.RegularExpressions;

namespace eclipseonlineweb.Controls
{
    public partial class ABN_InputControl : AustralianNumbersControl
    {

        public ABN_InputControl()
        {
            //InitializeComponent();
        }

        public bool HasValue
        {
            get { return !string.IsNullOrEmpty(GetEntity()); }
        }

        public override void InitControl()
        {
            InvalidMessage = "Invalid ABN (e.g. 53 004 085 616)";
        }
        public override String GetEntity()
        {
            return txtVal1.Text.Trim() + txtVal2.Text.Trim() + txtVal3.Text.Trim() + txtVal4.Text.Trim();
        }
        public override void SetEntity(String value)
        {
            if (!string.IsNullOrEmpty(value) && !string.IsNullOrWhiteSpace(value))
            {
                //Removing whitespaces if any; as using numeric textboxes
                value = value.Replace(" ", "");

                #region Value1
                if (value.Length >= 2)
                    txtVal1.Text = value.Substring(0, 2);
                else
                {
                    if ((value.Length) > 0)
                        txtVal1.Text = value.Substring(0, value.Length);
                    else
                        txtVal1.Text = "";

                    txtVal2.Text = "";
                    txtVal3.Text = "";
                    txtVal4.Text = "";
                    return;
                }
                #endregion

                #region value2
                if (value.Length >= 5)
                    txtVal2.Text = value.Substring(2, 3);
                else
                {
                    if ((value.Length - 2) > 0)
                        txtVal2.Text = value.Substring(2, value.Length - 2);
                    else
                        txtVal2.Text = "";

                    txtVal3.Text = "";
                    txtVal4.Text = "";
                    return;
                }
                #endregion

                if (value.Length >= 8)
                    txtVal3.Text = value.Substring(5, 3);
                else
                {
                    if ((value.Length - 5) > 0)
                        txtVal3.Text = value.Substring(5, value.Length - 5);
                    else
                        txtVal3.Text = "";

                    txtVal4.Text = "";
                    return;
                }
                if (value.Length >= 11)
                    txtVal4.Text = value.Substring(8, 3);
                else
                {
                    if ((value.Length - 8) > 0)
                        txtVal4.Text = value.Substring(8, value.Length - 8);
                    else
                        txtVal4.Text = "";
                    return;
                }
            }
            else
            {
                txtVal1.Text = "";
                txtVal2.Text = "";
                txtVal3.Text = "";
                txtVal4.Text = "";
            }
        }
        public override void ClearEntity()
        {
            base.ClearEntity();
            Msg.Visible = false;
            txtVal1.Text = txtVal2.Text = txtVal3.Text = txtVal4.Text = string.Empty;
        }

        public bool Validate()
        {
            if (!base.IsValid)
            {
                Msg.Visible = true;
                Msg.InnerText = InvalidMessage;
                return false;
            }
            return true;
        }

        public override void ClearErrors()
        {
            base.ClearErrors();
            Msg.Visible = false;
        }

        public override bool ValidateNumber(string abn)
        {
            bool isValid = false;
            int[] weight = { 10, 1, 3, 5, 7, 9, 11, 13, 15, 17, 19 };
            int weightedSum = 0;
            //0. ABN must be 11 digits long   
            if (!string.IsNullOrEmpty(abn) && Regex.IsMatch(abn, @"^\d{11}$"))
            {


                //Rules: 1,2,3  ,4 
                for (int i = 0; i < weight.Length; i++)
                {
                    weightedSum += (int.Parse(abn[i].ToString()) - ((i == 0) ? 1 : 0)) * weight[i];
                }
                //Rules:5   
                isValid = ((weightedSum % 89) == 0);
            }

            return isValid;
        }



    }
}