﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountantCompanyControl.ascx.cs"
    EnableViewState="true" Inherits="eclipseonlineweb.Controls.AccountantCompanyControl" %>
<%@ Register Src="PhoneNumberControl.ascx" TagName="PhoneNumberControl" TagPrefix="uc1" %>
<%@ Register Src="TFN_InputControl.ascx" TagName="TFN_InputControl" TagPrefix="uc2" %>
<%@ Register Src="ABN_InputControl.ascx" TagName="ABN_InputControl" TagPrefix="uc3" %>
<%@ Register Src="ACN_InputControl.ascx" TagName="ACN_InputControl" TagPrefix="uc4" %>
<script type="text/javascript">
    function SetTradingName(sender, args) {
        var TradingName = document.getElementById("<%:txtTradingName.ClientID %>").value;
        var chkthp = document.getElementById('<%: chkSameAsTrading.ClientID %>');
        var txtSgcPercentSalary = $find("<%:txtLegalName.ClientID %>");
        if (TradingName != "" && args.get_checked()) {
            txtSgcPercentSalary.set_value(TradingName);
            txtSgcPercentSalary.disable();
        }
        else {
            txtSgcPercentSalary.enable();
        }
    }
    function OnValueChangingTradingName(sender, args) {
        var TradingName = document.getElementById("<%:txtTradingName.ClientID %>").value;
        var txtSgcPercentSalary = $find("<%:txtLegalName.ClientID %>");
        var button = $find("<%: chkSameAsTrading.ClientID%>");
        if (button.get_checked() && TradingName != "") {
            txtSgcPercentSalary.set_value(TradingName);
            txtSgcPercentSalary.disable();
        }
        else {
            txtSgcPercentSalary.enable();
        }
    }
</script>
<fieldset style="width: 98%">
    <div style="text-align: left; float: none; border: 1px; background-color: White;
        height: 25px; position: relative; vertical-align: middle; margin: 0; padding: 0;">
        <telerik:RadButton ID="btnAccountantCompany" Text="Add Client Addresses" ToolTip="Save Changes"
            ValidationGroup="VGAccountantCompanyControl" OnClick="btnSaveAccountantCompany_Click"
            runat="server" Width="32px" Height="32px" BorderStyle="None">
            <Image ImageUrl="~/images/Save-Icon.png"></Image>
        </telerik:RadButton>
    </div>
</fieldset>
<br />
<fieldset>
    <table id="TD_EditMode" runat="server" width="100%" style="background-color: White;">
        <tr>
            <td style="width: 43%;">
                <table>
                    <tr>
                        <td style="width: 45%;">
                            <span class="riLabel">Status Flag</span>
                            <asp:HiddenField ID="hfCID" runat="server" />
                            <asp:HiddenField ID="hfCLID" runat="server" />
                            <asp:HiddenField ID="hfCSID" runat="server" />
                            <asp:HiddenField ID="hf_AccountantType" runat="server" />
                        </td>
                        <td style="text-align: left;">
                            &nbsp;
                            <telerik:RadComboBox ID="ddlStatusFlag" runat="server" Width="196px">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 58%;">
                <table>
                    <tr>
                        <td style="width: 40%; text-align: left;">
                            <span class="riLabel">Client ID</span>
                        </td>
                        <td>
                            <telerik:RadTextBox runat="server" ID="txtClientID" Width="196px">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</fieldset>
<fieldset>
    <asp:Panel runat="server" ID="pnlAccountantCompanyDetail">
        <table width="100%">
            <tr>
                <td>
                    <span class="riLabel">Trading Name *</span>
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtTradingName" Width="197px">
                        <ClientEvents OnBlur="OnValueChangingTradingName" />
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RFVTradingName" ControlToValidate="txtTradingName"
                        ErrorMessage="*" ForeColor="Red" ValidationGroup="VGAccountantCompanyControl"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <span class="riLabel">Legal Name *</span>
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtLegalName" Width="196px">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtLegalName"
                        ErrorMessage="*" ForeColor="Red" ValidationGroup="VGAccountantCompanyControl"></asp:RequiredFieldValidator>
                    <telerik:RadButton ButtonType="ToggleButton" ToggleType="CheckBox" runat="server"
                        OnClientCheckedChanged="SetTradingName" AutoPostBack="false" ID="chkSameAsTrading"
                        Text="Same as Trading Name" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Account Designation</span>
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtAccountDesignation" Width="197px">
                    </telerik:RadTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Facsimile</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="ph_facsimle" runat="server" />
                </td>
                <td>
                    <span class="riLabel">Phone Number</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="ph_phoneNumber" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">TFN</span>
                </td>
                <td>
                    <uc2:TFN_InputControl ID="tfn_TFN" runat="server" InvalidMessage="Invalid TFN (e.g. 123 456 782)" />
                </td>
                <td>
                    <span class="riLabel">ABN </span>
                </td>
                <td>
                    <uc3:ABN_InputControl ID="abn_ABN" runat="server" InvalidMessage="Invalid ABN (e.g. 53 004 085 616)" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">ACN</span>
                </td>
                <td>
                    <uc4:ACN_InputControl ID="acn_ACN" runat="server" InvalidMessage="Invalid ACN (e.g. 010 499 966)" />
                </td>
                <td>
                    <span class="riLabel">Website Address</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtWebSiteAddress" runat="server" Width="197px">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center;">
                    <asp:Label runat="server" ID="txtMessage" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
</fieldset>
