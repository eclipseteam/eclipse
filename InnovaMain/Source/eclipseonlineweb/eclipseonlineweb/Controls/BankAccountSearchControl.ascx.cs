﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;

namespace eclipseonlineweb.Controls
{
    public partial class BankAccountSearchControl : UserControl
    {
        private DataView _summaryView1;
        private DataView _summaryView;
        private ICMBroker UMABroker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }
        public Action<BankAccountDS> Saved
        {
            get;
            set;
        }
        public Action ValidationFailed { get; set; }
        public Action Canceled { get; set; }

        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {


        }

        public void DisableExitingGrid()
        {
            DvAssBankAccountGrid.Visible = false;
        }

        private BankAccountDS GetDataSet(Guid cid)
        {
            var bankAccountDs = new BankAccountDS { CommandType = DatasetCommandTypes.Get };

            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
                clientData.GetData(bankAccountDs);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            return bankAccountDs;
        }
        
        public void SetEntity(Guid cid)
        {
            //ClearEntity();
            //hfCID.Value = cid.ToString();
            if (cid != Guid.Empty)
            {
                BankAccountDS ds = GetDataSet(cid);
                DataRow dr = ds.Tables[ds.BankAccountsTable.TABLENAME].Rows[0];
                _summaryView1 = dr.Table.DefaultView;

                //hfCID.Value = dr[0].ToString();
                //hfCLID.Value = dr[1].ToString();
                //hfCSID.Value = dr[2].ToString();
            }
        }

        private void LoadControls()
        {

            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            var allAccounts = new BankAccountDS
                                  {
                                      Command = (int)WebCommands.GetOrganizationUnitsByType,
                                      Unit = new OrganizationUnit
                                              {
                                                  CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                                                  Type = ((int)OrganizationType.BankAccount).ToString()
                                              }
                                  };
            org.GetData(allAccounts);
            var selectedClid = new Guid(selectedCLID.Value);
            var selectedCsid = new Guid(selectedCSID.Value);
            var rows = allAccounts.Tables[allAccounts.BankAccountsTable.TABLENAME].Select(string.Format("{0}='{1}' and {2}='{3}'", allAccounts.BankAccountsTable.CLID, selectedClid, allAccounts.BankAccountsTable.CSID, selectedCsid));
            if (rows.Length > 0)
            {
                rows[0][allAccounts.BankAccountsTable.ISSELECTED] = true;
            }
            _summaryView1 = new DataView(allAccounts.Tables[allAccounts.BankAccountsTable.TABLENAME]) { Sort = allAccounts.BankAccountsTable.ISSELECTED + " Desc" };
            if (!String.IsNullOrEmpty(hfFilter.Value) && hfFilter.Value.ToLower().StartsWith("term"))
            {
                _summaryView1.RowFilter = "AccountType = 'termdeposit'";
            }
            else
            {
                _summaryView1.RowFilter = "AccountType <> 'termdeposit'";
            }

            UMABroker.ReleaseBrokerManagedComponent(org);
        }
        
        public void SetEntity(Guid selectedClid, Guid selectedCsid, string linkedEntityType)
        {
            lblmessages.Text = "";
            selectedCLID.Value = selectedClid.ToString();
            selectedCSID.Value = selectedCsid.ToString();
            hfFilter.Value = linkedEntityType;
            LoadControls();

        }

        readonly Dictionary<Guid, Guid> _selectedByUser = new Dictionary<Guid, Guid>();
        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            if (Saved != null)
            {
                if (DvAssBankAccountGrid.Visible)
                {
                    int selectCount = 0;
                    int selectedRowIndex = -1;
                    Guid clid = Guid.Empty;
                    Guid csid = Guid.Empty;

                    foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
                    {
                        var checkbox = (CheckBox)dataItem["ChkSelect"].FindControl("CheckBox1");
                        if (checkbox.Checked)
                        {
                            clid = new Guid(dataItem["CLid"].Text);
                            csid = new Guid(dataItem["CSid"].Text);
                            _selectedByUser.Add(clid, csid);
                            selectCount++;
                            selectedRowIndex = dataItem.RowIndex;
                        }
                    }

                    if (selectedRowIndex == -1)
                    {
                        lblmessages.Text = "Please Select Account.";

                    }
                    else if (selectCount > 1)
                    {
                        lblmessages.Text = "Multiple Account selection is not allowed.";
                    }
                    else
                    {
                        var bankAccountDs = new BankAccountDS();
                        DataTable dataTable = bankAccountDs.Tables[bankAccountDs.BankAccountsTable.TABLENAME];
                        DataRow dataRow = dataTable.NewRow();
                        dataRow[bankAccountDs.BankAccountsTable.CLID] = clid;
                        dataRow[bankAccountDs.BankAccountsTable.CSID] = csid;
                        dataTable.Rows.Add(dataRow);
                        bankAccountDs.Tables.Remove(bankAccountDs.BankAccountsTable.TABLENAME);
                        bankAccountDs.Tables.Add(dataTable);
                        if (Saved != null)
                        {
                            Saved(bankAccountDs);
                        }
                        txtSearchAccName.Text = "";
                        lblmessages.Text = "";
                        PresentationGridAccount.Visible = false;
                    }

                    if (selectedRowIndex == -1 || selectCount > 1)
                    {
                        if (ValidationFailed != null)
                            ValidationFailed();
                    }
                }
                else
                {
                    var bankAccountDs = new BankAccountDS();
                    DataTable dataTable = bankAccountDs.Tables[bankAccountDs.BankAccountsTable.TABLENAME];
                    foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
                    {
                        var checkbox = (CheckBox)dataItem["ChkSelect"].FindControl("chkBankAccount");
                        if (checkbox.Checked)
                        {
                            DataRow dataRow = dataTable.NewRow();
                            dataRow[bankAccountDs.BankAccountsTable.CID] = new Guid(dataItem["Cid"].Text);
                            dataRow[bankAccountDs.BankAccountsTable.CLID] = new Guid(dataItem["CLid"].Text);
                            dataRow[bankAccountDs.BankAccountsTable.CSID] = new Guid(dataItem["CSid"].Text);
                            dataTable.Rows.Add(dataRow);
                            bankAccountDs.Tables.Remove(bankAccountDs.BankAccountsTable.TABLENAME);
                            bankAccountDs.Tables.Add(dataTable);
                        }
                    }

                    if (Saved != null)
                        Saved(bankAccountDs);

                    txtSearchAccName.Text = "";
                    lblmessages.Text = "";
                    PresentationGrid.Visible = false;
                }
            }

        }
        
        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (Canceled != null)
            {
                Canceled();
            }
        }
        
        protected void BtnSearchClick(object sender, EventArgs e)
        {
            VisibleExistingAccountGrid();
            if (ValidationFailed != null)
            {
                ValidationFailed();
            }
        }

        private void VisibleExistingAccountGrid()
        {
            if (!string.IsNullOrEmpty(txtSearchAccName.Text))
            {
                dvExistingAccGrid.Visible = true;
                PresentationGrid.Visible = true;
                LoadExistingBankAccounts();
            }
            Title.Text = "Available Bank Accounts";
        }

        private void LoadExistingBankAccounts()
        {
            Title.Text = "Available Bank Accounts";
            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            var allAccounts = new BankAccountDS
                                  {
                                      Command = (int)WebCommands.GetOrganizationUnitsByType,
                                      Unit = new OrganizationUnit
                                              {
                                                  Name = txtSearchAccName.Text,
                                                  CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                                                  Type = ((int)OrganizationType.BankAccount).ToString()
                                              }
                                  };
            org.GetData(allAccounts);
            _summaryView = new DataView(allAccounts.Tables[allAccounts.BankAccountsTable.TABLENAME]) { Sort = allAccounts.BankAccountsTable.ACCOUNTNAME + " DESC" };
            UMABroker.ReleaseBrokerManagedComponent(org);
        }
        
        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (_summaryView == null)
                LoadExistingBankAccounts();
            if (_summaryView != null) PresentationGrid.DataSource = _summaryView.ToTable();
        }

        protected void PresentationGridAccount_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (_summaryView1 == null)
                LoadControls();
            if (_summaryView1 != null) PresentationGrid.DataSource = _summaryView1.ToTable();
        }
    }
}