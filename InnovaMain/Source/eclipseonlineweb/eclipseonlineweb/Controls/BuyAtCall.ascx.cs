﻿using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;
using System.Web;
using System.IO;
using Aspose.Words;
using Aspose.Words.Tables;

namespace eclipseonlineweb.Controls
{
    public partial class BuyAtCall : UserControl
    {
        public string ClientCID
        {
            private get
            {
                return hfClientCID.Value;
            }
            set
            {
                hfClientCID.Value = value;
            }
        }

        private string ClientAccID
        {
            get
            {
                return hfClientID.Value;
            }
            set
            {
                hfClientID.Value = value;
            }
        }

        public bool IsAdmin
        {
            private get
            {
                return Convert.ToBoolean(hfIsAdmin.Value);
            }
            set
            {
                hfIsAdmin.Value = value.ToString();
            }
        }

        public bool IsAdviser
        {
            private get
            {
                return Convert.ToBoolean(hfIsAdviser.Value);
            }
            set
            {
                hfIsAdviser.Value = value.ToString();
            }
        }

        public bool IsAdminMenu
        {
            private get
            {
                if (string.IsNullOrEmpty(hfIsAdminMenu.Value))
                {
                    hfIsAdminMenu.Value = "false";
                }
                return Convert.ToBoolean(hfIsAdminMenu.Value);
            }
            set
            {
                hfIsAdminMenu.Value = value.ToString();
            }
        }

        private bool IsSMA
        {
            get
            {
                if (string.IsNullOrEmpty(hfIsSMA.Value))
                {
                    hfIsSMA.Value = "false";
                }
                return Convert.ToBoolean(hfIsSMA.Value);
            }
            set
            {
                hfIsSMA.Value = value.ToString();
            }
        }

        public Action<string, DataSet> SaveData { get; set; }

        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            searchAccountControl.SelectData += (clientCID, clientId, clientName) =>
            {
                ClientHeaderInfo1.SetEntity(new Guid(clientCID));
                ClientHeaderInfo1.Visible = true;
                ClientCID = clientCID;
                ClientAccID = clientId;
                SetClientManagementType();
                if (IsSMA)
                {
                    lblNotify.Visible = true;
                    lblNotify.Text = "This feature is not available for Super clients at the moment.";
                }
                else
                {
                    lblNotify.Visible = false;
                    LoadControls();
                    showDetails.Visible = true;
                    tdButtons.Visible = true;
                    ClearFields();
                }
            };
            searchAccountControl.ShowHideModal += (show) =>
            {
                if (show)
                {
                    popupSearch.Show();
                }
                else
                {
                    popupSearch.Hide();
                }
            };

            lblMsg.Visible = false;

            if (!IsPostBack)
            {
                if (IsAdminMenu)
                {
                    btnSearch.Visible = true;
                    showDetails.Visible = false;
                    ClientHeaderInfo1.Visible = false;
                    tdButtons.Visible = false;
                    searchAccountControl.IsAdminMenu = true;
                }
                else
                {
                    SetClientManagementType();
                    btnSearch.Visible = false;
                    showDetails.Visible = true;
                    ClientHeaderInfo1.Visible = true;
                    LoadControls();
                    tdButtons.Visible = true;
                    searchAccountControl.IsAdminMenu = false;
                }
            }
        }

        private HoldingRptDataSet GetData(Guid cid)
        {
            IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
            var ds = new HoldingRptDataSet();
            clientData.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(clientData);
            return ds;
        }

        private void LoadControls()
        {
            BindServiceType();
            BindBankAccounts(false);
        }

        private void BindServiceType()
        {
            //Getting Service type according to user type
            var ds = GetData(new Guid(ClientCID));
            OrderPadUtilities.FillServiceTypes(cmbServiceType, ds, IsAdmin, OrderAccountType.AtCall, IsAdviser, true);
        }

        private void BindBankAccounts(bool isTypeChanged)
        {
            //Getting cash bank account according to service type
            var ds = GetData(new Guid(ClientCID));

            if (!isTypeChanged)
            {
                //From Items
                string fromFilter = string.Format("{0}='{1}' and {2}='{3}' and {4}='cash'", ds.HoldingSummaryTable.LINKEDENTITYTYPE, OrganizationType.BankAccount, ds.HoldingSummaryTable.SERVICETYPE, cmbServiceType.SelectedValue, ds.HoldingSummaryTable.ASSETNAME);
                OrderPadUtilities.FillBankNAvailableFunds(cmbFromCashAccount, ds, fromFilter, IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA, Broker);
                if (cmbFromCashAccount.Items.Count > 0)
                {
                    cmbFromCashAccount.Items[0].Selected = true;
                    lblFromAvailableFunds.Text = Convert.ToDecimal(cmbFromCashAccount.SelectedItem.Attributes["AvailableFunds"]).ToString("C");
                }
                else
                {
                    lblFromAvailableFunds.Text = "$0.00";
                }
            }
            //To Items

            FillTransferToBankNAvailableFunds(new Guid(ClientCID));
            tdAF.Visible = tdAFV.Visible = trNarration.Visible = true;
        }

        private void FillTransferToBankNAvailableFunds(Guid cid)
        {
            bool isFromExternal = cmbFromCashAccount.SelectedItem.Attributes["IsExternal"] != null && Convert.ToBoolean(cmbFromCashAccount.SelectedItem.Attributes["IsExternal"]);
            IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
            var ds = new BankAccountDS();
            clientData.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(clientData);
            string filter = string.Format("{0} <> 'TERMDEPOSIT' and {1} = 'Active'", ds.BankAccountsTable.ACCOUNTTYPE, ds.BankAccountsTable.STATUS);
            DataRow[] cashAccounts = ds.BankAccountsTable.Select(filter);
            cmbToCashAccount.Items.Clear();
            lblToAvailableFunds.Text = String.Empty;
            foreach (var row in cashAccounts)
            {
                //Including ServiceType/External
                string serviceType = string.Empty;
                string checkServiceType = row[ds.BankAccountsTable.SERVICETYPES].ToString();
                bool isExternalAccount = ds.BankAccountsTable.ISEXTERNALACCOUNT != null && Convert.ToBoolean(row[ds.BankAccountsTable.ISEXTERNALACCOUNT]);
                if (isExternalAccount)
                {
                    if (isFromExternal)
                    {
                        continue;
                    }
                    serviceType = string.Format(" ({0})", "External");
                }
                else if (!string.IsNullOrEmpty(checkServiceType))
                {
                    serviceType = string.Format(" ({0})", row[ds.BankAccountsTable.SERVICETYPES]);
                }

                //Creating New Item
                var item = new RadComboBoxItem
                                   {
                                       Text = string.Format("{0} - {1} : {2}{3}", row[ds.BankAccountsTable.INSTITUTION],
                                                         row[ds.BankAccountsTable.ACCOUNTTYPE].ToString().ToUpper(),
                                                         row[ds.BankAccountsTable.ACCOUNTNO],
                                                        serviceType),
                                       Value = row[ds.BankAccountsTable.CID].ToString()
                                   };

                decimal holding;
                decimal unsettledBuy;
                decimal.TryParse(row[ds.BankAccountsTable.TRANSACTIONHOLDING].ToString(), out holding);
                decimal.TryParse(row[ds.BankAccountsTable.UNSETTLEDBUY].ToString(), out unsettledBuy);
                decimal availableFund = holding + unsettledBuy;

                item.Attributes.Add("AvailableFunds", availableFund.ToString());
                item.Attributes.Add("AccountNo", row[ds.BankAccountsTable.ACCOUNTNO].ToString());
                item.Attributes.Add("BSB", row[ds.BankAccountsTable.BSBNO].ToString());
                item.Attributes.Add("IsExternal", row[ds.BankAccountsTable.ISEXTERNALACCOUNT].ToString());
                cmbToCashAccount.Items.Add(item);
                item.DataBind();
                if (IsAdmin && isExternalAccount)
                {
                    var existingItem = cmbFromCashAccount.Items.FindItem(it => it.Value == item.Value);
                    if (existingItem == null)
                    {
                        //Creating New Item
                        var fromItem = new RadComboBoxItem
                        {
                            Text = item.Text,
                            Value = item.Value
                        };
                        fromItem.Attributes.Add("AvailableFunds", availableFund.ToString());
                        fromItem.Attributes.Add("AccountNo", row[ds.BankAccountsTable.ACCOUNTNO].ToString());
                        fromItem.Attributes.Add("BSB", row[ds.BankAccountsTable.BSBNO].ToString());
                        fromItem.Attributes.Add("IsExternal", row[ds.BankAccountsTable.ISEXTERNALACCOUNT].ToString());

                        cmbFromCashAccount.Items.Add(fromItem);
                        fromItem.DataBind();
                    }
                }
            }
            if (!isFromExternal)
            {
                //Fill At Call accounts
                GetAtCallAccounts(new Guid(ClientCID));
            }

            //Removing item from To Combo which is selected in From combo
            foreach (RadComboBoxItem item in cmbToCashAccount.Items)
            {
                if (cmbFromCashAccount.SelectedItem != null && item.Value.Contains(cmbFromCashAccount.SelectedItem.Value))
                {
                    item.Remove();
                    break;
                }
            }
            //Setting To Available Funds
            if (cmbToCashAccount.Items.Count > 0)
            {
                cmbToCashAccount.SelectedIndex = 0;
                lblToAvailableFunds.Text = Convert.ToDecimal(cmbToCashAccount.SelectedItem.Attributes["AvailableFunds"]).ToString("C");
            }
            else
            {
                lblToAvailableFunds.Text = "$0.00";
            }
        }

        protected void btnCutOff_OnClick(object sender, EventArgs e)
        {
            FileHelper.DownLoadFile(FileHelper.CutOffTimeFilePath, FileHelper.CutOffTimeFileName);
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            lblMsg.Visible = false;
            lblMsg.Text = "";

            //Validate funds
            if (!ValidateAmount())
            {
                lblMsg.Visible = true;
                return;
            }
            //Checking Existing Order CM ID
            Guid orderCMCID = OrderPadUtilities.IsOrderCmExists(Broker, (Page as UMABasePage).GetCurrentUser());

            var unit = new OrganizationUnit
            {
                Name = "Order " + DateTime.Now.ToString("dd MMM, yyyy hh:mm:ss"),
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = (Page as UMABasePage).GetCurrentUser()
            };
            var ds = new OrderPadDS
            {
                CommandType = DatasetCommandTypes.Add,
                Unit = unit,
                Command = (int)WebCommands.AddNewOrganizationUnit
            };

            //Add rows in order pad ds
            AddRows(ds);

            if (SaveData != null)
            {
                SaveData(orderCMCID.ToString(), ds);
            }

            lblMsg.Visible = true;
            lblMsg.Text = ds.ExtendedProperties["Message"].ToString();
            if (ds.ExtendedProperties["Result"].ToString() == OperationResults.Successfull.ToString() && cmbToCashAccount.SelectedItem.Attributes["BrokerID"] == null)
            {
                string path = CreateReceipt(ds);
                lblMsg.Text = string.Format("{0} Click <a href = '../SysAdministration/DownloadOrderReceipt.aspx?filename={1}'>here</a> to print its receipt.", lblMsg.Text, path);
            }
            BindBankAccounts(false);
            ClearFields();
        }

        private void ClearFields()
        {
            txtAmount.Text = string.Empty;
            txtFromNarration.Text = string.Empty;
            txtToNarration.Text = string.Empty;
        }

        private bool ValidateAmount()
        {
            decimal totalAmount = 0;

            if (!string.IsNullOrEmpty(txtAmount.Text.Trim()))
            {
                totalAmount = Convert.ToDecimal(txtAmount.Text.Trim());
            }
            if (totalAmount == 0)
            {
                lblMsg.Text = "Please Enter Amount";
                return false;
            }

            decimal availableFunds;
            decimal.TryParse(lblFromAvailableFunds.Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out availableFunds);
            if (totalAmount > availableFunds)
            {
                lblMsg.Text = "Insufficient Available Funds";
                return false;
            }

            if (cmbToCashAccount.Items.Count == 0)
            {
                lblMsg.Text = "Transfer To Cash Account not available";
                return false;
            }

            if (cmbFromCashAccount.SelectedValue == cmbToCashAccount.SelectedValue)
            {
                lblMsg.Text = "Transfer To Cash Account is same as Transfer From";
                return false;
            }
            return true;
        }

        private void AddRows(OrderPadDS ds)
        {
            //Order Row
            var orderId = AddOrderRow(ds);
            decimal amount = Convert.ToDecimal(txtAmount.Text.Trim());

            if (amount > 0)
            {
                //Order Items Row
                AddItemRow(ds, amount, orderId);
            }
        }

        private Guid AddOrderRow(OrderPadDS ds)
        {
            DataRow orderRow = ds.OrdersTable.NewRow();

            Guid orderId = Guid.NewGuid();
            orderRow[ds.OrdersTable.ID] = orderId;
            orderRow[ds.OrdersTable.CLIENTCID] = new Guid(ClientCID);
            orderRow[ds.OrdersTable.CLIENTID] = ClientHeaderInfo1.ClientId ?? ClientAccID;
            orderRow[ds.OrdersTable.ORDERACCOUNTTYPE] = OrderAccountType.AtCall;
            orderRow[ds.OrdersTable.ORDERTYPE] = OrderType.Manual;
            orderRow[ds.OrdersTable.STATUS] = OrderStatus.Active;
            orderRow[ds.OrdersTable.ACCOUNTCID] = new Guid(cmbFromCashAccount.SelectedValue);
            orderRow[ds.OrdersTable.ACCOUNTNUMBER] = cmbFromCashAccount.SelectedItem.Attributes["AccountNo"].Trim();
            orderRow[ds.OrdersTable.ACCOUNTBSB] = cmbFromCashAccount.SelectedItem.Attributes["BSB"].Trim();
            orderRow[ds.OrdersTable.ORDERITEMTYPE] = OrderItemType.Buy;
            orderRow[ds.OrdersTable.CLIENTMANAGEMENTTYPE] = IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA;

            bool isFromExternal = cmbFromCashAccount.SelectedItem.Attributes["IsExternal"] != null && Convert.ToBoolean(cmbFromCashAccount.SelectedItem.Attributes["IsExternal"]);
            orderRow[ds.OrdersTable.ORDERBANKACCOUNTTYPE] = isFromExternal ? OrderBankAccountType.External : OrderBankAccountType.Internal;

            ds.OrdersTable.Rows.Add(orderRow);
            return orderId;
        }

        private void AddItemRow(OrderPadDS ds, decimal amount, Guid orderId)
        {
            DataRow itemsRow = ds.AtCallTable.NewRow();

            itemsRow[ds.AtCallTable.ID] = Guid.NewGuid();
            itemsRow[ds.AtCallTable.ORDERID] = orderId;
            itemsRow[ds.AtCallTable.AMOUNT] = amount;
            if (cmbToCashAccount.SelectedItem.Attributes["BrokerID"] != null)
            {
                itemsRow[ds.AtCallTable.BANKID] = new Guid(cmbToCashAccount.SelectedItem.Attributes["BankID"].Trim());
                itemsRow[ds.AtCallTable.BROKERID] = new Guid(cmbToCashAccount.SelectedItem.Attributes["BrokerID"].Trim());
                itemsRow[ds.AtCallTable.PRODUCTID] = new Guid(cmbToCashAccount.SelectedItem.Attributes["ProductID"].Trim());
                itemsRow[ds.AtCallTable.ATCALLACCOUNTCID] = new Guid(cmbToCashAccount.SelectedItem.Attributes["AtCallAccountCID"].Trim());
                itemsRow[ds.AtCallTable.ACCOUNTTIER] = cmbToCashAccount.SelectedItem.Attributes["AccountTier"].Trim();
                itemsRow[ds.AtCallTable.ATCALLTYPE] = AtCallType.MoneyMovementBroker;
            }
            else
            {
                itemsRow[ds.AtCallTable.TRANSFERTO] = AtCallAccountType.Client;
                itemsRow[ds.AtCallTable.TRANSFERACCCID] = new Guid(cmbToCashAccount.SelectedValue);
                itemsRow[ds.AtCallTable.TRANSFERACCNUMBER] = cmbToCashAccount.SelectedItem.Attributes["AccountNo"].Trim();
                itemsRow[ds.AtCallTable.TRANSFERACCBSB] = cmbToCashAccount.SelectedItem.Attributes["BSB"].Trim();
                itemsRow[ds.AtCallTable.ISEXTERNALACCOUNT] = cmbToCashAccount.SelectedItem.Attributes["IsExternal"].Trim();
                itemsRow[ds.AtCallTable.ATCALLTYPE] = AtCallType.MoneyMovement;
                itemsRow[ds.AtCallTable.FROMNARRATION] = txtFromNarration.Text.Trim();
                itemsRow[ds.AtCallTable.TRANSFERNARRATION] = txtToNarration.Text.Trim();
            }

            ds.AtCallTable.Rows.Add(itemsRow);
        }

        protected void cmbFromCashAccount_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            lblFromAvailableFunds.Text = Convert.ToDecimal(cmbFromCashAccount.SelectedItem.Attributes["AvailableFunds"]).ToString("C");
            FillTransferToBankNAvailableFunds(new Guid(ClientCID));
        }

        protected void cmbToCashAccount_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            lblToAvailableFunds.Text = Convert.ToDecimal(cmbToCashAccount.SelectedItem.Attributes["AvailableFunds"]).ToString("C");

            if (cmbToCashAccount.SelectedItem.Attributes["BrokerID"] != null)
            {
                tdAF.Visible = tdAFV.Visible = trNarration.Visible = false;

            }
            else
            {
                tdAF.Visible = tdAFV.Visible = trNarration.Visible = true;
            }
        }

        protected void cmbServiceType_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindBankAccounts(false);
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            popupSearch.Show();
            showDetails.Visible = false;
            ClientHeaderInfo1.Visible = false;
            tdButtons.Visible = false;
            searchAccountControl.HideModal = false;
            searchAccountControl.FindControl("SearchBox").Focus();
        }

        private void GetAtCallAccounts(Guid cid)
        {
            IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
            var ds = new TDTransactionDS();
            clientData.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(clientData);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[TDTransactionDS.TDTRANTABLE] != null)
                {
                    const string filter = "TransactionType = 'At Call' and SystemTransactionType = 'Application' ";
                    DataRow[] cashAccounts = ds.Tables[TDTransactionDS.TDTRANTABLE].Select(filter);

                    string itemText = string.Empty;
                    foreach (var row in cashAccounts)
                    {
                        string newitemText = string.Format("{0} - {1} - {2}", row["BrokerName"], row["InstituteName"], row["TransactionType"]);
                        if (itemText == newitemText)
                            continue;

                        itemText = newitemText;
                        var item = new RadComboBoxItem
                        {
                            Text = itemText,
                            Value = row["ID"].ToString()
                        };
                        item.Attributes.Add("AvailableFunds", row["AmountTotal"].ToString());
                        item.Attributes.Add("BrokerID", row["BrokerID"].ToString());
                        item.Attributes.Add("BankID", row["InstitutionID"].ToString());
                        item.Attributes.Add("ProductID", row["Product"].ToString());
                        item.Attributes.Add("AtCallAccountCID", row["BankCid"].ToString());
                        item.Attributes.Add("AccountTier", row["AccountTier"].ToString());

                        cmbToCashAccount.Items.Add(item);
                        item.DataBind();
                    }
                }
            }
        }
        private string GetNewFilePath()
        {
            string fileName = string.Format("{0}_{1}_CashTransfer_{2}.PDF", ClientHeaderInfo1.ClientId ?? ClientAccID, lblMsg.Text.Split(' ')[1], DateTime.Now.ToShortDateString().Replace("/", "-"));
            string dirOrderReceipts = string.Format("{0}\\OrderReceipts", HttpContext.Current.Server.MapPath("~/App_Data"));
            if (!Directory.Exists(dirOrderReceipts))
                Directory.CreateDirectory(dirOrderReceipts);

            string filePath = string.Format("{0}\\{1}", dirOrderReceipts, fileName);
            return filePath;
        }

        private void AddCellToSheet(DocumentBuilder builderExl, string text, double width, Color backColor, bool isFontBold, int fontSize, int lineWidth, ParagraphAlignment paragraph, CellVerticalAlignment vAlign, TextOrientation orientation)
        {
            //Header Start
            builderExl.InsertCell();
            builderExl.CellFormat.PreferredWidth = PreferredWidth.FromPercent(width);
            builderExl.CellFormat.Shading.BackgroundPatternColor = backColor;
            builderExl.Font.Bold = isFontBold;
            builderExl.Font.Size = fontSize;
            builderExl.CellFormat.Borders.LineWidth = lineWidth;
            builderExl.CellFormat.VerticalAlignment = vAlign;
            builderExl.CellFormat.Orientation = orientation;
            builderExl.CurrentParagraph.ParagraphFormat.Alignment = paragraph;
            builderExl.Bold = isFontBold;
            builderExl.Write(text);

        }
        private string CreateReceipt(OrderPadDS ds)
        {
            DataRow clientSummaryRow = GetData(new Guid(ClientCID)).Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0];

            string path = string.Empty;
            string OutPath = string.Empty;
            string worksheetTitle = string.Empty;
            if (cmbToCashAccount.SelectedItem.Attributes["BrokerID"] != null)
            {
                worksheetTitle = "Sell AtCall";
                path = "~/Templates/OrderPad_SellAtCall_Template1.docx";
            }
            else
            {
                worksheetTitle = "Cash Transfer";
                path = Server.MapPath("~/Templates/OrderPad_CashTransfer_Template.docx");
            }
            Aspose.Words.Document document = null;
            document = new Aspose.Words.Document(path);

            //This is portion for the Header Section of the Document
            document.Range.Replace("[Eclipse_OrderNo]", string.Format("Order {0}", lblMsg.Text.Split(' ')[1]), false, false);
            document.Range.Replace("[Eclipse_Dat]", DateTime.Now.ToString("dd/MM/yyyy"), false, false);
            document.Range.Replace("[Eclipse_OrderBy]", ((Page as UMABasePage).GetCurrentUser()).CurrentUserName, false, false);
            document.Range.Replace("[Eclipse_InvestorName]", clientSummaryRow[HoldingRptDataSet.CLIENTNAME].ToString(), false, false);
            document.Range.Replace("[Eclipse_ClientID]", ClientHeaderInfo1.ClientId ?? ClientAccID, false, false);
            document.Range.Replace("[Eclipse_AccountTYpe]", clientSummaryRow[HoldingRptDataSet.CLIENTTYPE].ToString(), false, false);
            document.Range.Replace("[Eclipse_Adviser]", clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME].ToString(), false, false);
            document.Range.Replace("[Eclipse_ServiceType]", cmbServiceType.SelectedItem.Text.ToUpper(), false, false);
            document.Range.Replace("[A11]", clientSummaryRow[HoldingRptDataSet.CLIENTNAME].ToString(), false, false);
            string addressLine1And2 = string.Empty;
            if (clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2].ToString() == string.Empty)
                addressLine1And2 = clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1].ToString();
            else
                addressLine1And2 = clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1].ToString() + ", " + clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2].ToString();
            document.Range.Replace("[A12]", addressLine1And2, false, false);
            document.Range.Replace("[A13]", string.Format("{0} {1} {2}", clientSummaryRow[HoldingRptDataSet.SUBURB], clientSummaryRow[HoldingRptDataSet.STATE], clientSummaryRow[HoldingRptDataSet.POSTCODE]), false, false);
            document.Range.Replace("[A14]", clientSummaryRow[HoldingRptDataSet.COUNTRY].ToString(), false, false);

            int bodyFontSize = 9;
            if (document != null)
            {
                OutPath = GetNewFilePath();
                //Now here we try to Make Dynamic Table
                DocumentBuilder builderExl = new DocumentBuilder(document);
                //This is Point where to Start Generate Table
                builderExl.MoveToBookmark("EclipseCashTransfer", false, true);
                Table table = builderExl.StartTable();

                builderExl.PageSetup.LeftMargin = ConvertUtil.InchToPoint(1);
                builderExl.PageSetup.TopMargin = ConvertUtil.InchToPoint(1);
                builderExl.PageSetup.BottomMargin = ConvertUtil.InchToPoint(1);

                builderExl.StartTable();

                builderExl.InsertCell();
                builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                builderExl.CellFormat.Borders.LineWidth = 0;
                builderExl.EndRow();
                builderExl.EndTable();
                builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Left;
                builderExl.Writeln("");


                AddCellToSheet(builderExl, string.Empty, 20, Color.DarkGray, true, bodyFontSize, 1, ParagraphAlignment.Center, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                AddCellToSheet(builderExl, "FromAccount", 40, Color.DarkGray, true, bodyFontSize, 1, ParagraphAlignment.Center, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                AddCellToSheet(builderExl, "ToAccount", 40, Color.DarkGray, true, bodyFontSize, 1, ParagraphAlignment.Center, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                builderExl.EndRow();

                DataRow drOrderList = ds.Tables[ds.OrdersTable.TABLENAME].Rows[0];
                DataRow drCallList = ds.Tables[ds.AtCallTable.TABLENAME].Rows[0];
                DataRow rowTO = GetDataSet(new Guid(drOrderList[ds.OrdersTable.ACCOUNTCID].ToString())).Tables[0].Rows[0];
                DataRow rowTra =
                    GetDataSet(new Guid(drCallList[ds.AtCallTable.TRANSFERACCCID].ToString())).Tables[0].Rows[0];
                //Bank Name
                AddCellToSheet(builderExl, "BankName", 20, Color.White, true, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                //To
                AddCellToSheet(builderExl, rowTO["Institution"].ToString(), 40, Color.White, false, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                //From
                AddCellToSheet(builderExl, rowTra["Institution"].ToString(), 40, Color.White, false, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                builderExl.EndRow();
                //ACCOUNT Name
                AddCellToSheet(builderExl, "AccountName", 20, Color.White, true, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                //To
                AddCellToSheet(builderExl, rowTO["AccountName"].ToString(), 40, Color.White, false, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                //From
                AddCellToSheet(builderExl, rowTra["AccountName"].ToString(), 40, Color.White, false, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                builderExl.EndRow();
                //BSB
                AddCellToSheet(builderExl, "BSB", 20, Color.White, true, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                //To
                AddCellToSheet(builderExl, drOrderList[ds.OrdersTable.ACCOUNTBSB].ToString(), 40, Color.White, false, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                //From
                AddCellToSheet(builderExl, drCallList[ds.AtCallTable.TRANSFERACCBSB].ToString(), 40, Color.White, false, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                builderExl.EndRow();
                //AccountNumber
                AddCellToSheet(builderExl, "AccountNumber", 20, Color.White, true, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                //To
                AddCellToSheet(builderExl, drOrderList[ds.OrdersTable.ACCOUNTNUMBER].ToString(), 40, Color.White, false, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                //From
                AddCellToSheet(builderExl, drCallList[ds.AtCallTable.TRANSFERACCNUMBER].ToString(), 40, Color.White, false, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                builderExl.EndRow();
                switch (worksheetTitle)
                {
                    case "Sell AtCall":
                        //Broker Number
                        AddCellToSheet(builderExl, "Broker", 20, Color.White, true, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                        //To
                        AddCellToSheet(builderExl, drCallList[ds.AtCallTable.BROKERNAME].ToString(), 40, Color.White, false, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                        //From
                        AddCellToSheet(builderExl, drCallList[ds.AtCallTable.BROKERNAME].ToString(), 40, Color.White, false, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                        builderExl.EndRow();
                        break;
                    case "Cash Transfer":
                        break;
                }
                //Narration
                AddCellToSheet(builderExl, "Narration", 20, Color.White, true, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                //To
                AddCellToSheet(builderExl, drCallList[ds.AtCallTable.FROMNARRATION].ToString(), 40, Color.White, false, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                //From
                AddCellToSheet(builderExl, drCallList[ds.AtCallTable.TRANSFERNARRATION].ToString(), 40, Color.White, false, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                builderExl.EndRow();
                //Amount
                AddCellToSheet(builderExl, "Amount", 20, Color.White, true, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                //To
                AddCellToSheet(builderExl, string.Format("{0:C}", drCallList[ds.AtCallTable.AMOUNT]), 40, Color.White, false, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                //From
                AddCellToSheet(builderExl, string.Format("{0:C}", drCallList[ds.AtCallTable.AMOUNT]), 40, Color.White, false, bodyFontSize, 1, ParagraphAlignment.Left, CellVerticalAlignment.Center, TextOrientation.Horizontal);
                builderExl.EndRow();
                //table.AutoFit(AutoFitBehavior.FixedColumnWidths);
                table.PreferredWidth = PreferredWidth.FromPercent(100);
                document.Save(OutPath, SaveFormat.Pdf);
            }
            return FileHelper.GetFileName(OutPath);
        }

        private BankAccountDS GetDataSet(Guid cid)
        {
            var bankTransactionDetailsDS = new BankAccountDS { CommandType = DatasetCommandTypes.Get };
            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
                clientData.GetData(bankTransactionDetailsDS);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }
            return bankTransactionDetailsDS;
        }

        private void SetClientManagementType()
        {
            IsSMA = SMAUtilities.IsSMAClient(new Guid(ClientCID), Broker);
        }
    }
}