﻿using System;
using System.Data;
using System.Web.UI;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb.Controls
{
    public partial class AttachExistingConfirmation : UserControl
    {
        private string _cid = string.Empty;
        private string _accountType = string.Empty;
        private string _clientId = string.Empty;
        private string _filter = string.Empty;
        private string _entityId = string.Empty;

        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        public Action<string, DataSet> SaveData { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            _cid = Request.QueryString["ins"];
            _accountType = Request.QueryString["aType"];
            _clientId = Request.QueryString["clientId"];
            _filter = Request.QueryString["filter"];
            _entityId = Request.QueryString["entityId"];

            if (!IsPostBack)
            {
                OrderAccountType accountType;
                bool isValidOrderStatus = Enum.TryParse(_accountType, out accountType);
                if (isValidOrderStatus)
                {
                    switch (accountType)
                    {
                        case OrderAccountType.AtCall:
                        case OrderAccountType.TermDeposit:
                            TDPresentationGrid.Visible = true;
                            SSPresentationGrid.Visible = false;
                            ASXPresentationGrid.Visible = false;
                            BWPresentationGrid.Visible = false;
                            hfTableName.Value = "CashTransctions";
                            hfFilter.Value = accountType == OrderAccountType.AtCall ? GetAtCallFilter() : GetTDFilter();
                            break;
                        case OrderAccountType.StateStreet:
                            SSPresentationGrid.Visible = true;
                            ASXPresentationGrid.Visible = false;
                            BWPresentationGrid.Visible = false;
                            TDPresentationGrid.Visible = false;
                            hfTableName.Value = "MisTransactions";
                            break;
                        case OrderAccountType.ASX:
                            ASXPresentationGrid.Visible = true;
                            SSPresentationGrid.Visible = false;
                            BWPresentationGrid.Visible = false;
                            TDPresentationGrid.Visible = false;
                            hfTableName.Value = "ASXTransactions";
                            break;
                        case OrderAccountType.BankAccount:
                            BWPresentationGrid.Visible = true;
                            SSPresentationGrid.Visible = false;
                            ASXPresentationGrid.Visible = false;
                            TDPresentationGrid.Visible = false;
                            hfTableName.Value = "CashTransctions";
                            break;
                    }
                }
            }
        }

        private Guid CheckIfSettelledCmExists()
        {
            var ds = new SettledUnsetteledDS
            {
                Unit = new OrganizationUnit
                {
                    Name = _clientId,
                    CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                    Type = ((int)OrganizationType.SettledUnsettled).ToString(),
                },
                CommandType = DatasetCommandTypes.Check,
                Command = (int)WebCommands.GetOrganizationUnitsByType
            };

            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            org.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(org);

            return ds.Unit.Cid;
        }

        private void ExecuteCommand(GridCommandEventArgs e)
        {
            //DataRowView item = (DataRowView)e.Item.DataItem;
            switch (e.CommandName.ToLower())
            {
                case "attach":
                    AttachConfirmation(e);
                    ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "CloseAndRebind", "CloseAndRebind();", true);
                    break;
            }
        }

        private void AttachConfirmation(GridCommandEventArgs e)
        {
            Guid id = Guid.Parse(e.CommandArgument.ToString());

            Guid cid = CheckIfSettelledCmExists();

            if (cid != Guid.Empty)
            {
                var ds = new SettledUnsetteledTransactionDS
                            {
                                Unit = new OrganizationUnit
                                           {
                                               Name = _clientId,
                                               ClientId = _clientId,
                                               Type = ((int)OrganizationType.SettledUnsettled).ToString(),
                                               CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                                           },
                                CommandType = DatasetCommandTypes.Settle
                            };

                //Adding extended property to identify manual confirmation
                ds.ExtendedProperties.Add("IsManual", "true");

                GridDataItem dataItem = e.Item.OwnerTableView.Items[e.Item.ItemIndex];
                var dt = new DataTable { TableName = hfTableName.Value };
                foreach (GridColumn col in e.Item.OwnerTableView.Columns)
                {
                    var colString = new DataColumn(col.UniqueName);
                    dt.Columns.Add(colString);
                }
                var dc = new DataColumn("TransactionID", typeof(Guid)) { DefaultValue = id };
                dt.Columns.Add(dc);
                dc = new DataColumn("EntityID", typeof(Guid));
                dt.Columns.Add(dc);

                var entityIds = _entityId.Split(',');
                foreach (string entityId in entityIds)
                {
                    DataRow dr = dt.NewRow();
                    foreach (GridColumn col in e.Item.OwnerTableView.Columns) //loops through each column in RadGrid
                    {
                        dr[col.UniqueName] = dataItem[col.UniqueName].Text;
                    }
                    dr["EntityID"] = entityId;

                    dt.Rows.Add(dr);
                }
                ds.Tables.Add(dt);
                //Save data
                SaveData(cid.ToString(), ds);
            }
        }

        protected void SSPresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            ExecuteCommand(e);
        }

        protected void SSPresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetMISTransaction();
        }

        private void GetMISTransaction()
        {
            IBrokerManagedComponent clientData = Broker.GetBMCInstance(new Guid(_cid));
            var misDS = new MISTransactionDS();
            clientData.GetData(misDS);

            if (misDS.Tables.Count > 0)
            {
                var misView = new DataView(misDS.Tables[0]) { Sort = "TradeDate DESC", RowFilter = string.Format("Code ='{0}'", _filter) };
                SSPresentationGrid.DataSource = misView;
            }
            Broker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void ASXPresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            ExecuteCommand(e);
        }

        protected void ASXPresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetASXTransaction();
        }

        private void GetASXTransaction()
        {
            IBrokerManagedComponent clientData = Broker.GetBMCInstance(new Guid(_cid));
            var asxds = new ASXTransactionDS();
            clientData.GetData(asxds);

            if (asxds.Tables.Count > 0)
            {
                var asxView = new DataView(asxds.Tables[0]) { Sort = "TradeDate DESC", RowFilter = string.Format("InvestmentCode ='{0}'", _filter) };
                ASXPresentationGrid.DataSource = asxView;
            }
            Broker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void BWPresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            ExecuteCommand(e);
        }

        protected void BWPresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetBWTransaction();
        }

        private void GetBWTransaction()
        {
            IBrokerManagedComponent clientData = Broker.GetBMCInstance(new Guid(_cid));
            var ds = new BankTransactionDS();
            clientData.GetData(ds);

            if (ds.Tables["Cash Transactions"] != null)
            {
                var summaryView = new DataView(ds.Tables["Cash Transactions"]) { Sort = "TransactionDate DESC", RowFilter = string.Format("AccountNo like '%{0}'", _filter) };
                BWPresentationGrid.DataSource = summaryView;
            }
            Broker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void TDPresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            ExecuteCommand(e);
        }

        protected void TDPresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetTDTransaction();
        }

        private void GetTDTransaction()
        {
            IBrokerManagedComponent clientData = Broker.GetBMCInstance(new Guid(_cid));
            var tdds = new TDTransactionDS();
            clientData.GetData(tdds);

            if (tdds.Tables.Count > 0)
            {
                if (tdds.Tables[TDTransactionDS.TDTRANTABLE] != null)
                {
                    var tdView = new DataView(tdds.Tables[TDTransactionDS.TDTRANTABLE]) { Sort = "TransactionDate DESC", RowFilter = hfFilter.Value };
                    TDPresentationGrid.DataSource = tdView;
                }
            }
            Broker.ReleaseBrokerManagedComponent(clientData);
        }

        private string GetTDFilter()
        {
            return "SystemTransactionType= 'Application' and AdministrationSystem like '%TD' and ContractNote <> ''";
        }

        private string GetAtCallFilter()
        {
            return "SystemTransactionType= 'Application' and AdministrationSystem like '%At Call' and ContractNote <> ''";
        }
    }
}