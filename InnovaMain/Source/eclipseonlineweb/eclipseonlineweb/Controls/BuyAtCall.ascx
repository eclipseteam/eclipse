﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BuyAtCall.ascx.cs" Inherits="eclipseonlineweb.Controls.BuyAtCall" %>
<%@ Register TagPrefix="uc2" TagName="SearchNSelectClientAccount" Src="~/Controls/SearchNSelectClientAccount.ascx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<style>
    .rgCaption
    {
        color: Blue;
        font: bold 10pt Arial Narrow;
    }
</style>
<script language="javascript" type="text/javascript">

    function ShowModalPopup() {
        window.$find("ModalBehaviour").show();
    }

    function HideModalPopup() {
        window.$find("ModalBehaviour").hide();
    }
    
</script>
<asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
     <Triggers>
        <asp:PostBackTrigger ControlID="btnCutOff"  />
    </Triggers>
    <ContentTemplate>
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="20%" runat="server" id="tdButtons">
                          <telerik:RadButton runat="server" ID="btnCutOff" ToolTip="Cut Of Time" OnClick="btnCutOff_OnClick">
                            <ContentTemplate>
                                <img alt="" src="../images/cut-off-time_106.png" class="btnImageWithText" />
                                <span class="riLabel">Cut-Off Times</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:radbutton runat="server" id="btnSave" tooltip="Place Order" onclick="btnSave_OnClick">
                            <contenttemplate>
                                <img src="../images/cart_add.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Place Order</span>
                            </contenttemplate>
                        </telerik:radbutton>
                        
                    </td>
                    <td style="width: 80%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <telerik:radbutton runat="server" id="btnSearch" visible="False" text="Select Client Account"
                            onclick="btnSearch_OnClick">
                        </telerik:radbutton>
                        &nbsp;<uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <br />
        <asp:Label ID="lblNotify" runat="server" ForeColor="Blue" Visible="false"></asp:Label>
        <div id="MainView" style="background-color: white" runat="server">
            <table style="width: 100%; height: 80px" runat="server" id="showDetails" visible="false">
                <tr>
                    <td>
                        Service Type:
                    </td>
                    <td colspan="3">
                        <telerik:radcombobox id="cmbServiceType" autopostback="true" width="250px" runat="server"
                            onselectedindexchanged="cmbServiceType_OnSelectedIndexChanged">
                        </telerik:radcombobox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="font-weight: bold">
                        Transfer From:
                    </td>
                    <td style="font-weight: bold">
                        Transfer To:
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 180px">
                        Cash Account:
                    </td>
                    <td>
                        <telerik:radcombobox id="cmbFromCashAccount" autopostback="true" width="250px" runat="server"
                            onselectedindexchanged="cmbFromCashAccount_OnSelectedIndexChanged">
                        </telerik:radcombobox>
                    </td>
                    <td style="width: 180px">
                        Cash Account:
                    </td>
                    <td>
                        <telerik:radcombobox id="cmbToCashAccount" autopostback="true" width="250px" runat="server"
                            onselectedindexchanged="cmbToCashAccount_OnSelectedIndexChanged">
                        </telerik:radcombobox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Available Funds:
                    </td>
                    <td>
                        <asp:Label ID="lblFromAvailableFunds" runat="server" Text=""></asp:Label>
                    </td>
                    <td runat="server" id="tdAF">
                        Available Funds:
                    </td>
                    <td runat="server" id="tdAFV">
                        <asp:Label ID="lblToAvailableFunds" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Amount to Invest: $
                    </td>
                    <td colspan="3">
                        <telerik:radnumerictextbox id="txtAmount" runat="server">
                        </telerik:radnumerictextbox>
                    </td>
                </tr>
                <tr runat="server" id="trNarration">
                    <td>
                        Narration:
                    </td>
                    <td>
                        <telerik:radtextbox runat="server" id="txtFromNarration" width="250px">
                        </telerik:radtextbox>
                    </td>
                    <td>
                        Narration:
                    </td>
                    <td>
                        <telerik:radtextbox runat="server" id="txtToNarration" width="250px">
                        </telerik:radtextbox>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="hfClientCID" runat="server" />
        <asp:HiddenField ID="hfClientID" runat="server" />
        <asp:HiddenField ID="hfIsAdmin" runat="server" />
        <asp:HiddenField ID="hfIsAdviser" runat="server" />
        <asp:HiddenField ID="hfIsAdminMenu" runat="server" />
        <asp:HiddenField ID="hfIsSMA" runat="server" />
        <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
        <asp:ModalPopupExtender ID="popupSearch" runat="server" TargetControlID="btnShowPopup"
            PopupControlID="pnlpopup" PopupDragHandleControlID="PopupHeader" Drag="true"
            BackgroundCssClass="ModalPopupBG" BehaviorID="ModalBehaviour">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlpopup" runat="server" BackColor="White" Style="display: none">
            <div class="popup_Container">
                <div class="popup_Titlebar" id="PopupHeader">
                    <div class="TitlebarLeft">
                        Select Account
                    </div>
                    <div class="TitlebarRight" onclick="HideModalPopup();">
                    </div>
                </div>
                <div class="PopupBody">
                    <uc2:SearchNSelectClientAccount ID="searchAccountControl" runat="server" />
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
