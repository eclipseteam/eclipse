﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Attachments.ascx.cs"
    Inherits="eclipseonlineweb.Controls.Attachments" %>
<script type="text/javascript">
    function DisableSaveButton(sender, args) {
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';

        sender.set_autoPostBack(true);
    }
</script>
<br />
<asp:Panel ID="pnlUploadForm" runat="server">
    <fieldset>
        <legend>Document(s) Upload Form</legend>
        <table>
            <tr>
                <td style="font-size: smaller">
                    Select File(s):
                </td>
                <td>
                    <telerik:RadAsyncUpload MultipleFileSelection="Automatic" runat="server" ID="AsyncUpload" />
                </td>
            </tr>
            <tr>
                <td style="font-size: smaller">
                    File Description & Type
                </td>
                <td width="200px">
                    <telerik:RadTextBox runat="server" Text="" ID="description" Width="500px">
                    </telerik:RadTextBox>
                </td>
                <td width="200px">
                    <telerik:RadComboBox runat="server" ID="cmbDocuType" Filter="Contains" Width="250px">
                    </telerik:RadComboBox>
                </td>
                <td>
                    <telerik:RadButton runat="server" ID="btnUpload" Text="Upload" OnClick="Upload_Click">
                    </telerik:RadButton>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadProgressArea runat="server" ID="RadProgressArea2" />
                </td>
            </tr>
        </table>
    </fieldset>
</asp:Panel>
<br />
<asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:HiddenField ID="hfCID" runat="server" />
        <br />
        <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
            runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="50"
            AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
            OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
            AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
            OnItemDeleted="PresentationGrid_ItemDeleted" OnItemInserted="PresentationGrid_ItemInserted"
            OnInsertCommand="PresentationGrid_InsertCommand" EnableViewState="true" ShowFooter="true"
            OnItemCreated="PresentationGrid_ItemCreated" OnItemDataBound="PresentationGrid_ItemDataBound">
            <PagerStyle Mode="Slider" UseRouting="false" AlwaysVisible="true"></PagerStyle>
            <MasterTableView Width="100%" CommandItemDisplay="None" DataKeyNames="ID">
                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                <Columns>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton">
                        <HeaderStyle Width="5px"></HeaderStyle>
                    </telerik:GridEditCommandColumn>
                    <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ATTACHEDBMCID"
                        UniqueName="ATTACHEDBMCID" />
                    <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ID" UniqueName="ID" />
                    <telerik:GridTemplateColumn HeaderText="Description" ItemStyle-Width="250px" UniqueName="DESCRIPTION"
                        DataField="DESCRIPTION">
                        <ItemTemplate>
                            <asp:Label ID="lblDescription" runat="server" Text='<%# Server.HtmlEncode(TrimDescription(Eval("DESCRIPTION") as string)) %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadTextBox ID="txbDescription" Width="250px" runat="server" TextMode="SingleLine"
                                Text='<%# Eval("Description") %>'>
                            </telerik:RadTextBox>
                        </EditItemTemplate>
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                    </telerik:GridTemplateColumn>
                    <telerik:GridDropDownColumn Visible="false" HeaderText="Attachment Type" SortExpression="ATTACHMENTTYPENAMEDESC"
                        HeaderStyle-Width="15%" ItemStyle-Width="50%" UniqueName="ATTACHMENTTYPENAMEDESC"
                        DataField="DESCRIPTION" ListTextField="Value" ListValueField="Key" ColumnEditorID="GridDropDownColumnEditorProduct" />
                    <telerik:GridBoundColumn ReadOnly="true" HeaderText="Type" HtmlEncode="True" DataField="ATTACHMENTTYPENAME"
                        ItemStyle-Width="220px" UniqueName="ATTACHMENTTYPENAME" />
                    <telerik:GridBoundColumn ReadOnly="true" HeaderText="Date Imported" DataField="DATEIMPORTED"
                        ItemStyle-Width="120px" UniqueName="DATEIMPORTED" />
                    <telerik:GridBoundColumn ReadOnly="true" HeaderText="Imported By" DataField="IMPORTEDBY"
                        ItemStyle-Width="60px" UniqueName="IMPORTEDBY" />
                    <telerik:GridBoundColumn ReadOnly="true" HeaderText="Origin Path" DataField="ORIGINPATH"
                        ItemStyle-Width="200px" UniqueName="ORIGINPATH" />
                    <telerik:GridTemplateColumn UniqueName="FileDownLoadURLDownload" HeaderText="" ShowFilterIcon="false"
                        ItemStyle-Width="5px" ItemStyle-HorizontalAlign="Center" AllowFiltering="false"
                        HeaderStyle-Width="5px">
                        <ItemTemplate>
                            <a id="Download" runat="server">
                                <img src="../images/attach.png" /></a>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="FileIcon" ItemStyle-Width="20px" ItemStyle-HorizontalAlign="Center"
                        HeaderText="" ShowFilterIcon="false" AllowFiltering="false" HeaderStyle-Width="20px">
                        <ItemTemplate>
                            <img src="../images/FileTypeIcons/file.gif" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn ReadOnly="true" Display="False" HeaderText="File Download Link"
                        DataField="FileDownLoadURL" UniqueName="FileDownLoadURL" />
                    <telerik:GridButtonColumn Text="Delete" CommandName="Delete" ButtonType="ImageButton"
                        ConfirmText="Are you sure you want to delete this file?" ConfirmDialogType="Classic">
                        <HeaderStyle Width="5px"></HeaderStyle>
                    </telerik:GridButtonColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn ButtonType="ImageButton">
                    </EditColumn>
                </EditFormSettings>
                <PagerStyle AlwaysVisible="True"></PagerStyle>
            </MasterTableView>
        </telerik:RadGrid>
        <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorAttachmentTypeList"
            runat="server" EnableViewState="true" DropDownStyle-Width="400px" />
    </ContentTemplate>
</asp:UpdatePanel>
