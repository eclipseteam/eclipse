﻿using System;


namespace eclipseonlineweb.Controls
{
    public partial class TFN_InputControl : AustralianNumbersControl
    {
        public bool HasValue
        {

            get { return !string.IsNullOrEmpty(GetEntity()); }

        }
        public TFN_InputControl()
        {
            //InitializeComponent();
        }
        public override void InitControl()
        {
            InvalidMessage = "Invalid TFN (e.g. 123 456 782)";
        }
        public override String GetEntity()
        {
          
            return txtValue1.Text.Trim() + txtValue2.Text.Trim() + txtValue3.Text.Trim();
        }
        public override void SetEntity(String value)
        {
            //Removing whitespaces if any; as using numeric textboxes
            value = value.Replace(" ", "");
            
            if (!string.IsNullOrEmpty(value) && !string.IsNullOrWhiteSpace(value))
            {
                if (value.Length >= 3)
                    txtValue1.Text = value.Substring(0, 3);
                else
                {
                    if ((value.Length) > 0)
                        txtValue1.Text = value.Substring(0, value.Length);
                    else
                        txtValue1.Text = "";
                    txtValue2.Text = "";
                    txtValue3.Text = "";
                    return;
                }

                if (value.Length >= 6)
                    txtValue2.Text = value.Substring(3, 3);
                else
                {
                    if ((value.Length - 3) > 0)
                        txtValue2.Text = value.Substring(3, value.Length - 3);
                    else
                        txtValue2.Text = "";
                    txtValue3.Text = "";
                    return;
                }

                if (value.Length >= 9)
                    txtValue3.Text = value.Substring(6, 3);
                else
                {
                    if ((value.Length - 6) > 0)
                        txtValue3.Text = value.Substring(6, value.Length - 6);
                    else
                        txtValue3.Text = "";
                    return;
                }
            }
            else
            {
                txtValue1.Text = "";
                txtValue2.Text = "";
                txtValue3.Text = "";
            }
        }

        public override void ClearEntity()
        {
          
            base.ClearEntity();
            Msg.Visible = false;
            txtValue1.Text = txtValue2.Text = txtValue3.Text = string.Empty;
        }

        public bool Validate()
        {
            if (!base.IsValid)
            {
                Msg.Visible = true;
                Msg.InnerText = InvalidMessage;
                return false;
            }
            return true;

        }

        public override void ClearErrors()
        {
            base.ClearErrors();
            Msg.Visible = false;
        }

        public override bool ValidateNumber(string tfn)
        {
            // ATO TFN validation routine.
            int[] aiWeight8 = new int[] { 10, 7, 8, 4, 6, 3, 5, 1 };
            int[] aiWeight9 = new int[] { 10, 7, 8, 4, 6, 3, 5, 2, 1 };
            int iPtr = 0;
            int iSum = 0;
            string sTFN;
            // Messages

            long TFN;
            // Initializations.
            if (!long.TryParse(tfn, out TFN))
            {

                return false;
            }
            else
            {
                sTFN = ((TFN - 0) + "");
                int[] aiWeight;
                // Check TFN length
                if (sTFN.Length == 8)
                {
                    aiWeight = aiWeight8;
                }
                else if (sTFN.Length == 9)
                {
                    aiWeight = aiWeight9;
                }
                else
                {
                    // Error TFN length wrong

                    return false;
                }

                // Do Checksum
                for (iPtr = 0; iPtr <= sTFN.Length - 1; iPtr++)
                {
                    iSum += (Int32.Parse(sTFN.Substring(iPtr, 1)) * aiWeight[iPtr]);
                }

                // Validate TFN
                if ((iSum % 11) == 0)
                {
                    // Valid TFN.

                    return true;
                }
                else
                {
                    // Invalid TFN.

                    return false;
                }
            }
        }



    }
}