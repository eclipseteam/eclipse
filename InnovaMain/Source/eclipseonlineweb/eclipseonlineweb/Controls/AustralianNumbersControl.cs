﻿using System;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace eclipseonlineweb.Controls
{
    interface IAustralianNumbersControl 
    {
        string InvalidMessage { get; set; }                
        bool IsRequired { get; set; }
        void InitControl();
        bool ValidateNumber(string Value);

    }

    public abstract class AustralianNumbersControl: System.Web.UI.UserControl,
        IAustralianNumbersControl    
    {
        public string InvalidMessage { get; set; } 
       
        public bool IsRequired { get; set; }

        public virtual void InitControl()
        {

        }
        public virtual bool ValidateNumber(string Value) 
        {
            return true;
        }

        protected string Value;

        public virtual String GetEntity() 
        {
            return string.Empty;
        }
        public virtual void SetEntity(String value) 
        {

        }
        public virtual void ClearErrors()
        {
            InvalidMessage = string.Empty;
            
        }
        public virtual void ClearEntity()
        {
            ClearErrors();
            InvalidMessage = string.Empty;
            //Val1.Text = Val2.Text = Val3.Text = string.Empty;
        }
        public  bool IsValid 
        {
            get 
            {
                string numberValue = GetEntity();
                if (!IsRequired && numberValue.Length == 0)
                {


                    ClearErrors();

                    return true;
                }


                if (!ValidateNumber(numberValue))
                {                    
                    return false;
                }
                else
                {

                    ClearErrors();
                    return true;
                }
            }
        }
        protected  void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //ClearEntity();
                //InitControl();
                
            }
        }
    }
}