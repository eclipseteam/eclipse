﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecurityWorkFlow.ascx.cs"
    Inherits="eclipseonlineweb.Controls.SecurityWorkFlow" %>
<fieldset>
    <table>
        <tr>
            <td colspan="3">
                <span class="riLabelBold">Included Users</span>
            </td>
            <td colspan="2">
                <span class="riLabelBold">Available Users</span>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadComboBox ID="cmbIncluded" runat="server" Width="450px" IsEditable="True"
                    AllowCustomText="True" EnableTextSelection="True" CheckBoxes="true" Filter="StartsWith">
                </telerik:RadComboBox>
            </td>
            <td>
                <telerik:RadButton ID="btnRemove" runat="server" Text="Remove >>" OnClick="btnRemove_OnClick">
                </telerik:RadButton>
                <telerik:RadButton ID="Radbutton2" runat="server" Text="Remove All >>" OnClick="btnRemoveAll_OnClick">
                </telerik:RadButton>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <telerik:RadComboBox ID="cmbAvailable" runat="server" Width="450px" IsEditable="True"
                    AllowCustomText="True" EnableTextSelection="True" CheckBoxes="true" Filter="StartsWith">
                </telerik:RadComboBox>
            </td>
            <td>
                <telerik:RadButton ID="btnAdd" runat="server" Text="<< Add" OnClick="btnAdd_OnClick">
                </telerik:RadButton>
                <telerik:RadButton ID="Radbutton1" runat="server" Text="<< Add All" OnClick="btnAddAll_OnClick">
                </telerik:RadButton>
            </td>
        </tr>
    </table>
</fieldset>
