﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ABN_InputControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.ABN_InputControl" %>
<span id="ctrlABNBody">
    <telerik:RadMaskedTextBox Mask="##" PromptChar=" " ID="txtVal1" runat="server" MaskType="Numeric"
        Width="40px">
    </telerik:RadMaskedTextBox>
    <span>&nbsp;</span>
    <telerik:RadMaskedTextBox Mask="###" PromptChar=" " ID="txtVal2" runat="server" MaskType="Numeric"
        Width="40px">
    </telerik:RadMaskedTextBox>
    <span>&nbsp;</span>
    <telerik:RadMaskedTextBox Mask="###" PromptChar=" " ID="txtVal3" runat="server" Width="40px"
        MaskType="Numeric">
    </telerik:RadMaskedTextBox>
    <span>&nbsp;</span>
    <telerik:RadMaskedTextBox Mask="###" PromptChar=" " ID="txtVal4" runat="server" Width="40px"
        MaskType="Numeric">
    </telerik:RadMaskedTextBox>
    <span id="Msg" runat="server" visible="false" style="color: Red;"></span></span>
