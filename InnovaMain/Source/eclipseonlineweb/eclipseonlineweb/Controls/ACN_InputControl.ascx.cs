﻿using System;
using System.Text.RegularExpressions;

namespace eclipseonlineweb.Controls
{
    public partial class ACN_InputControl : AustralianNumbersControl
    {
        
         public ACN_InputControl()
        {
            //InitializeComponent();
           
             
        }

         public override void InitControl()
         {
             InvalidMessage = "Invalid ACN (e.g. 010 499 966)";
         }
        public override String GetEntity()
        {
            return txtVal1.Text.Trim() + txtVal2.Text.Trim() + txtVal3.Text.Trim();
        }
        public override void SetEntity(String value)
        {
            if (!string.IsNullOrEmpty(value) && !string.IsNullOrWhiteSpace(value))
            {
                //Removing whitespaces if any; as using numeric textboxes
                value = value.Replace(" ", "");

                if (value.Length >= 3)
                    txtVal1.Text = value.Substring(0, 3);
                else
                {
                    if ((value.Length) > 0)
                        txtVal1.Text = value.Substring(0, value.Length);
                    else
                        txtVal1.Text = "";
                    txtVal2.Text = "";
                    txtVal3.Text = "";
                    return;
                }

                if (value.Length >= 6)
                    txtVal2.Text = value.Substring(3, 3);
                else
                {
                    if ((value.Length - 3) > 0)
                        txtVal2.Text = value.Substring(3, value.Length - 3);
                    else
                        txtVal2.Text = "";

                    txtVal3.Text = "";
                    return;
                }

                if (value.Length >= 9)
                    txtVal3.Text = value.Substring(6, 3);
                else
                {
                    if ((value.Length - 6) > 0)
                        txtVal3.Text = value.Substring(6, value.Length - 6);
                    else
                        txtVal3.Text = "";

                    return;
                }
            }
            else
            {

                txtVal1.Text = "";
                txtVal2.Text = "";
                txtVal3.Text = "";

            }
        }

        public override void ClearEntity()
        {
            base.ClearEntity();      
            Msg.Visible = false;
            txtVal1.Text = txtVal2.Text = txtVal3.Text = string.Empty;
        }

        public bool Validate()
        {
            if (!base.IsValid)
            {
                Msg.Visible = true;
                Msg.InnerText = InvalidMessage;
                return false;
            }
            return true;
            
        }

        public override void ClearErrors()
        {
            base.ClearErrors();
            Msg.Visible = false;            
        }

        public override bool ValidateNumber(string acn)
        {
            int[] l_weight = { 8, 7, 6, 5, 4, 3, 2, 1 };
            int l_sum = 0;
            int l_remainder = 0;
            int l_calculatedCheckDigit = 0;

            // Ensure ACN is 9 digits long
            if (string.IsNullOrEmpty(acn) || acn.Length != 9 || !Regex.IsMatch(acn, @"^\d{9}$"))
            {
                return false;
            }

            try
            {
                // Sum the multiplication of all the digits and weights
                for (int i = 0; i < l_weight.Length; i++)
                {
                    l_sum += Convert.ToInt32(acn.Substring(i, 1)) * l_weight[i];
                }

                // Divide by 10 to obtain remainder
                l_remainder = l_sum % 10;

                // Complement the remainder to 10
                l_calculatedCheckDigit = (10 - l_remainder == 10) ? 0 : (10 - l_remainder);

                // Compare the calculated check digit with the actual check digit
                if (l_calculatedCheckDigit == Convert.ToInt32(acn.Substring(8, 1)))
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

       

    }
}