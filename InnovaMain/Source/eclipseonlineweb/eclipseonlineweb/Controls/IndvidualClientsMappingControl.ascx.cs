﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;

namespace eclipseonlineweb.Controls
{
    public partial class IndvidualClientsMappingControl : System.Web.UI.UserControl
    {
        private string _cid;

        public OrganizationType OrgType
        {
            get { return ClientSearchControl.OrgType; }
            set { ClientSearchControl.OrgType = value; }
        }

        

        protected ICMBroker UMABroker
        {
            get { return (Page as UMABasePage).UMABroker; }
        }

        public Action<string, DataSet> SaveUnit { get; set; }

     

        protected override void OnInit(EventArgs e)
        {
            ClientSearchControl.Canceled += () => ShowHideExisting(false);
            ClientSearchControl.ValidationFailed += () =>
            {

            };

            ClientSearchControl.Save += (ds) => SaveData(ds, DatasetCommandTypes.Add);
        }
        private void SaveData(MembershipDS ds, DatasetCommandTypes commandType)
        {
            var dstosend = TransformToClientDs(ds);
            dstosend.CommandType = commandType;
            if (SaveUnit != null)
                SaveUnit(_cid.ToString(), dstosend);

            ShowHideExisting(false);
            gd_ClientControl.Rebind();
        }

        private IndividualClientsMappingDS TransformToClientDs(MembershipDS ds)
        {
            var clientDs = new IndividualClientsMappingDS();
            foreach (DataRow dataRow in ds.membershipTable.Rows)
            {
                var row =clientDs.ClientMappingTable.NewRow();
                clientDs.ClientMappingTable.AddRow(Guid.Parse(_cid), Guid.Empty, Guid.Empty, dataRow[ds.membershipTable.CLIENTID].ToString(), (Guid) dataRow[ds.membershipTable.CID], (Guid) dataRow[ds.membershipTable.CLID], (Guid) dataRow[ds.membershipTable.CSID], dataRow[ds.membershipTable.NAME].ToString(), dataRow[ds.membershipTable.TYPE].ToString(),null);
            }
            return clientDs;
        }

        /// <summary>
        /// Get Client data
        /// </summary>
        private void GetData()
        {
            var cID = new Guid(_cid);
            var ds = new IndividualClientsMappingDS()
            {
                CommandType = DatasetCommandTypes.GetClients,
                Unit = new OrganizationUnit
                {
                    CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                }
            };


            var component = UMABroker.GetBMCInstance(cID);
            component.GetData(ds);
            UMABroker.ReleaseBrokerManagedComponent(component);
            var memberShipView = new DataView(ds.ClientMappingTable);
            memberShipView.Sort = ds.ClientMappingTable.NAME + " ASC";
            gd_ClientControl.DataSource = memberShipView.ToTable();
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            _cid = Request.Params["ins"];
        }

        protected void gd_ClientControl_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void gd_ClientControl_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                var gDataItem = e.Item as GridDataItem;
                if (gDataItem != null)
                {
                    var cid = new Guid(gDataItem["Cid"].Text);
                    var clid = new Guid(gDataItem["Clid"].Text);
                    var csid = new Guid(gDataItem["Csid"].Text);
                    DeleteClient(cid, clid, csid);
                }
            }
        }

        private void DeleteClient(Guid cid, Guid clid, Guid csid)
        {
            var clientDs = GetClientRow(cid, clid, csid,null);
            clientDs.CommandType = DatasetCommandTypes.Delete;
            if (SaveUnit != null)
                SaveUnit(_cid, clientDs);
            gd_ClientControl.Rebind();
        }
        private void SaveShare(Guid cid, Guid clid, Guid csid, string share)
        {
            _cid = Request.Params["ins"];
            var clientDs = GetClientRow(cid, clid, csid, share);
            clientDs.CommandType = DatasetCommandTypes.Update;
            if (SaveUnit != null)
                SaveUnit(_cid, clientDs);
            gd_ClientControl.Rebind();
        }
        private IndividualClientsMappingDS GetClientRow(Guid cid, Guid clid, Guid csid,string share)
        {
            var clientDs = new IndividualClientsMappingDS ();
            var row = clientDs.ClientMappingTable.NewRow();
            row[clientDs.ClientMappingTable.CID] = cid;
            row[clientDs.ClientMappingTable.CSID] = csid;
            row[clientDs.ClientMappingTable.CLID] = clid;
            row[clientDs.ClientMappingTable.INDICID] = _cid;
            if (!string.IsNullOrEmpty(share))
                row[clientDs.ClientMappingTable.INDISHARE] = share;

            clientDs.ClientMappingTable.Rows.Add(row);
            return clientDs;
        }


        public void ShowHideExisting(bool show)
        {

            OVER.Visible = clientSearchPopup.Visible = show;
        }

        protected void LnkbtnAddExistingClientClick(object sender, EventArgs e)
        {
            ShowHideExisting(true);
        }

        protected void txtShare_OnTextChanged(object sender, EventArgs e)
        {
            RadInputControl txt = sender as RadInputControl;
            if (txt != null)
            {
                SaveShare(Guid.Parse(txt.Attributes["CID"]), Guid.Parse(txt.Attributes["CLID"]), Guid.Parse(txt.Attributes["CSID"]), txt.Text);
            }
        }
       
        protected void PresentationGrid_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;

            var datarow = ((DataRowView)(dataItem.DataItem)).Row;
            var txt = ((RadTextBox)(dataItem["IndiShare"].Controls[1]));
            txt.Text = datarow["IndiShare"].ToString();
            txt.Attributes.Add("CID", dataItem["CID"].Text);
            txt.Attributes.Add("CLID", dataItem["CLID"].Text);
            txt.Attributes.Add("CSID", dataItem["CSID"].Text);
        }
    }
}