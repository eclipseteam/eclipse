﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CutOffTime.ascx.cs"
    Inherits="eclipseonlineweb.Controls.CutOffTime" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<style type="text/css">
    p.MsoNormal
    {
        margin-top: 0cm;
        margin-right: 0cm;
        margin-bottom: 10.0pt;
        margin-left: 0cm;
        line-height: 115%;
        font-size: 10.0pt;
        font-family: "Calibri" , "sans-serif";
    }
    .style1
    {
        border-collapse: collapse;
        font-size: 11.0pt;
        font-family: Calibri, sans-serif;
        border: 1.0pt solid black;
    }
</style>
<fieldset>
    <table width="100%">
        <tr>
            <td width="20%">
            </td>
            <td style="width: 80%;" class="breadcrumbgap">
                <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                <br />
                <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
            </td>
        </tr>
    </table>
</fieldset>
<br />
<fieldset>
    <div>
        &nbsp;
        <p class="MsoNormal" style="margin: 0cm 10.5pt 0.0001pt 0cm; background-color: white; background-position: initial initial; background-repeat: initial initial;"><span style="font-size: 25.5pt; font-family: Arial, sans-serif;">Order cut-off times<o:p></o:p></span></p>

<p class="MsoNormal" style="margin: 0cm 10.5pt 0.0001pt 0cm; background-color: white; background-position: initial initial; background-repeat: initial initial;"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#666666;mso-ansi-language:EN-AU;
mso-fareast-language:EN-AU"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="margin-bottom: 0.0001pt; background-color: white; background-position: initial initial; background-repeat: initial initial;"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#666666;mso-ansi-language:EN-AU;
mso-fareast-language:EN-AU">If you're looking to place an order you should be
aware of certain cut-off times.&nbsp; <o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom: 12pt; background-color: white; background-position: initial initial; background-repeat: initial initial;"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#666666;mso-ansi-language:EN-AU;
mso-fareast-language:EN-AU"><br>
Please note that it can take up to 7 business days for an order to complete and
funds to be transferred to another financial institution. Monies will be
displayed as unsettled until all financial transactions within the order are
complete.<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom: 0.0001pt; background-color: white; background-position: initial initial; background-repeat: initial initial;"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#666666;mso-ansi-language:EN-AU;
mso-fareast-language:EN-AU">If you need to cancel an order instruction, you
must do so prior&nbsp;to the cut-off time.<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom: 0.0001pt; background-color: white; background-position: initial initial; background-repeat: initial initial;"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#666666;mso-ansi-language:EN-AU;
mso-fareast-language:EN-AU"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="margin-bottom: 12pt; background-color: white; background-position: initial initial; background-repeat: initial initial;"><b><span style="font-size:14.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#666666;mso-ansi-language:EN-AU;mso-fareast-language:EN-AU">Cut-off times
for e-Clipse UMA clients<o:p></o:p></span></b></p>

<p class="MsoNormal" style="margin-bottom: 0.0001pt; background-color: white; background-position: initial initial; background-repeat: initial initial;"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#666666;mso-ansi-language:EN-AU;
mso-fareast-language:EN-AU">Orders placed before the cut-off-times will be processed
on the same business day. <o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom: 0.0001pt; background-color: white; background-position: initial initial; background-repeat: initial initial;"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#666666;mso-ansi-language:EN-AU;
mso-fareast-language:EN-AU"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="margin-bottom: 0.0001pt; background-color: white; background-position: initial initial; background-repeat: initial initial;"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#666666;mso-ansi-language:EN-AU;
mso-fareast-language:EN-AU">Orders made after the cut-off times or on a weekend
or public holiday will be processed the next business day.&nbsp;<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom: 0.0001pt; background-color: white; background-position: initial initial; background-repeat: initial initial;"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#666666;mso-ansi-language:EN-AU;
mso-fareast-language:EN-AU"><o:p>&nbsp;</o:p></span></p>

<table class="MsoTableLightList" border="1" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border:none;mso-border-alt:solid black 1.0pt;
 mso-border-themecolor:text1;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt">
 <tbody><tr>
  <td width="243" valign="top" style="width:182.6pt;border-top:solid black 1.0pt;
  mso-border-top-themecolor:text1;border-left:solid black 1.0pt;mso-border-left-themecolor:
  text1;border-bottom:none;border-right:none;background:black;mso-background-themecolor:
  text1;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><b><span lang="EN-US" style="color:white;mso-themecolor:
  background1">Order type<o:p></o:p></span></b></p>
  </td>
  <td width="208" valign="top" style="width:155.95pt;border-top:solid black 1.0pt;
  mso-border-top-themecolor:text1;border-left:none;border-bottom:none;
  border-right:solid black 1.0pt;mso-border-right-themecolor:text1;background:
  black;mso-background-themecolor:text1;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><b><span lang="EN-US" style="color:white;mso-themecolor:
  background1">Cut-off times (AEST)<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr>
  <td width="243" valign="top" style="width:182.6pt;border:solid black 1.0pt;
  mso-border-themecolor:text1;border-right:none;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><b><span lang="EN-US">TD &amp; At Call purchases</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="208" valign="top" style="width:155.95pt;border:solid black 1.0pt;
  mso-border-themecolor:text1;border-left:none;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span lang="EN-US">11 am<o:p></o:p></span></p>
  </td>
 </tr>
 <tr>
  <td width="243" valign="top" style="width:182.6pt;border:none;border-left:solid black 1.0pt;
  mso-border-left-themecolor:text1;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><b><span lang="EN-US">Cash transfers</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="208" valign="top" style="width:155.95pt;border:none;border-right:solid black 1.0pt;
  mso-border-right-themecolor:text1;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span lang="EN-US">12 pm<o:p></o:p></span></p>
  </td>
 </tr>
 <tr>
  <td width="243" valign="top" style="width:182.6pt;border:solid black 1.0pt;
  mso-border-themecolor:text1;border-right:none;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><b><span lang="EN-US">Managed funds buy and sell
  orders<o:p></o:p></span></b></p>
  </td>
  <td width="208" valign="top" style="width:155.95pt;border:solid black 1.0pt;
  mso-border-themecolor:text1;border-left:none;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span lang="EN-US">1 pm<o:p></o:p></span></p>
  </td>
 </tr>
 <tr>
  <td width="243" valign="top" style="width:182.6pt;border-top:none;border-left:
  solid black 1.0pt;mso-border-left-themecolor:text1;border-bottom:solid black 1.0pt;
  mso-border-bottom-themecolor:text1;border-right:none;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><b><span lang="EN-US">ASX buy and sell orders <o:p></o:p></span></b></p>
  </td>
  <td width="208" valign="top" style="width:155.95pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;
  border-right:solid black 1.0pt;mso-border-right-themecolor:text1;padding:
  0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span lang="EN-US">11 am<o:p></o:p></span></p>
  </td>
 </tr>
</tbody></table>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal" style="margin-bottom: 12pt; background-color: white; background-position: initial initial; background-repeat: initial initial;"><b><span style="font-size:14.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#666666;mso-ansi-language:EN-AU;mso-fareast-language:EN-AU">Cut-off times
for e-Clipse Super clients<o:p></o:p></span></b></p>

<p class="MsoNormal" style="margin-bottom: 0.0001pt; background-color: white; background-position: initial initial; background-repeat: initial initial;"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#666666;mso-ansi-language:EN-AU;
mso-fareast-language:EN-AU">Orders placed before the cut-off times will be
processed on the next trade day.<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom: 0.0001pt; background-color: white; background-position: initial initial; background-repeat: initial initial;"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#666666;mso-ansi-language:EN-AU;
mso-fareast-language:EN-AU"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="margin-bottom: 0.0001pt; background-color: white; background-position: initial initial; background-repeat: initial initial;"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#666666;mso-ansi-language:EN-AU;
mso-fareast-language:EN-AU">Orders placed after the cut-off times or on a
weekend or public holiday will be processed on the next scheduled trade day.<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom: 0.0001pt; background-color: white; background-position: initial initial; background-repeat: initial initial;"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#666666;mso-ansi-language:EN-AU;
mso-fareast-language:EN-AU"><o:p>&nbsp;</o:p></span></p>

<table class="MsoTableLightList" border="1" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border:none;mso-border-alt:solid black 1.0pt;
 mso-border-themecolor:text1;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt">
 <tbody><tr>
  <td width="243" valign="top" style="width:182.6pt;border-top:solid black 1.0pt;
  mso-border-top-themecolor:text1;border-left:solid black 1.0pt;mso-border-left-themecolor:
  text1;border-bottom:none;border-right:none;background:black;mso-background-themecolor:
  text1;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><b><span lang="EN-US" style="color:white;mso-themecolor:background1">Order type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <o:p></o:p></span></b></p>
  </td>
  <td width="208" valign="top" style="width:155.95pt;border-top:solid black 1.0pt;
  mso-border-top-themecolor:text1;border-left:none;border-bottom:none;
  border-right:solid black 1.0pt;mso-border-right-themecolor:text1;background:
  black;mso-background-themecolor:text1;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><b><span lang="EN-US" style="color:white;mso-themecolor:
  background1">Cut-off times (AEST) &amp; trading dates<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr>
  <td width="243" valign="top" style="width:182.6pt;border:solid black 1.0pt;
  mso-border-themecolor:text1;border-right:none;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><b><span lang="EN-US">TD purchases</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="208" valign="top" style="width:155.95pt;border:solid black 1.0pt;
  mso-border-themecolor:text1;border-left:none;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span lang="EN-US">6 pm every Thursday<o:p></o:p></span></p>
  </td>
 </tr>
 <tr>
  <td width="243" valign="top" style="width:182.6pt;border-top:none;border-left:
  solid black 1.0pt;mso-border-left-themecolor:text1;border-bottom:solid black 1.0pt;
  mso-border-bottom-themecolor:text1;border-right:none;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><b><span lang="EN-US">ASX buy and sell orders<o:p></o:p></span></b></p>
  </td>
  <td width="208" valign="top" style="width:155.95pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;
  border-right:solid black 1.0pt;mso-border-right-themecolor:text1;padding:
  0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span lang="EN-US">11 am&nbsp; every Tuesday and Thursday<o:p></o:p></span></p>
  </td>
 </tr>
</tbody></table>
    </div>
</fieldset>
