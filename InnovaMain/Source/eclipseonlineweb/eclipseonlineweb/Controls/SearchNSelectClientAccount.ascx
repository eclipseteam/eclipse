﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchNSelectClientAccount.ascx.cs"
    Inherits="eclipseonlineweb.Controls.SearchNSelectClientAccount" %>
<style>
    .filterMenu
    {
        z-index: 100002 !important;
    }
</style>
<asp:Panel runat="server" DefaultButton="btnSearch">
    <table style="width: 600px">
        <tr>
            <td>
                <asp:RadioButtonList ID="rbbtnSearchSelection" RepeatDirection="Horizontal" runat="server"
                    RepeatLayout="Flow">
                    <asp:ListItem Text="By Name" Value="SearchByName" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="By ID" Value="SearchByID"></asp:ListItem>
                </asp:RadioButtonList>
                <asp:TextBox ID="SearchBox" runat="server" Width="200px">
                </asp:TextBox>
                <telerik:RadButton runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_OnClick">
                </telerik:RadButton>
                <asp:HiddenField ID="hfIsAdminMenu" runat="server" />
                <asp:HiddenField ID="hfHideModal" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadGrid OnNeedDataSource="PresentationGrid_OnNeedDataSource" ID="PresentationGrid"
                    runat="server" ShowStatusBar="true" AutoGenerateColumns="false" PageSize="5"
                    AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" GridLines="None"
                    AllowAutomaticDeletes="True" AllowFilteringByColumn="true" EnableViewState="true"
                    ShowFooter="false" OnItemCommand="PresentationGrid_OnItemCommand" Visible="false">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <FilterMenu CssClass="filterMenu">
                    </FilterMenu>
                    <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                        Name="ActiveOrders" TableLayout="Fixed" Font-Size="8">
                        <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="ENTITYCIID_FIELD" ReadOnly="true" HeaderText="ENTITYCIID_FIELD"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ENTITYCIID_FIELD" UniqueName="ClientCID"
                                Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderStyle-Width="120px" SortExpression="ClientID" HeaderText="Account ID"
                                ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ClientID" UniqueName="ClientID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="250px" SortExpression="ENTITYNAME_FIELD"
                                HeaderText="Account Name" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ENTITYNAME_FIELD"
                                UniqueName="ClientName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderStyle-Width="120px" SortExpression="Total" ReadOnly="true"
                                HeaderText="Total Holding" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Total" UniqueName="Total"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderStyle-Width="50px" AllowFiltering="false" UniqueName="Actions"
                                HeaderText="Actions" ShowFilterIcon="false">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnSelect" runat="server" CommandName="attach" CommandArgument='<%# Eval("ClientID") %>'>Select</asp:LinkButton>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
</asp:Panel>
