﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderHistory.ascx.cs"
    Inherits="eclipseonlineweb.Controls.OrderHistory" %>
<%@ Import Namespace="Oritax.TaxSimp.Common" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<style type="text/css">
    .RadListBox .rlbSelected
    {
        background-color: transparent !important;
    }
    .RadListBox span.rlbText
    {
        color: black !important;
    }
    .rtWrapperContent
    {
        background-color: lightyellow !important;
        color: black !important;
    }
    .RadGrid_Metro .rgHeader, .RadGrid_Metro .rgHeader a, .RadGrid_Metro .rgRow a, .RadGrid_Metro .rgAltRow a, .RadGrid_Metro tr.rgEditRow a, .RadGrid_Metro .rgFooter a, .RadGrid_Metro .rgEditForm a, .RadGrid_Metro .rgFilterBox
    {
        font-size: 8pt !important;
    }
    .rbPrimaryIcon
    {
        left: 4px !important;
        top: 0 !important;
    }
</style>
<script src="../Scripts/GridScripts.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">

    function DeleteConfirm() {
        var isConfirmed = confirm('Are you sure you want to delete these orders?');
        return isConfirmed;
    }

    function ShowModalPopup() {
        window.$find("ModalBehaviour").show();
    }

    function HideModalPopup() {
        window.$find("ModalBehaviour").hide();
    }

    function getvalue() {
        var combo = window.$find('<%:cmbStatus.ClientID %>');
        var approveStatus = '<%:OrderStatus.Approved.ToString()%>';
        var item = combo.findItemByText(approveStatus);
        if (item) {
            var isConfirmed = confirm("The order has instructions issued. Changing its status manually will remove all instructions. Are you sure?");
            if (!isConfirmed) {
                HideModalPopup();
                return false;
            }
            else {
                return true;
            }
        }
        return true;
    }

    function onItemChecked(sender, e) {
        var item = e.get_item();
        var checked = item.get_checked();
        if (checked) {
            item.get_element().attributes["style"].value = "font-weight:Bold";
        }
        else {
            item.get_element().attributes["style"].value = "font-weight:Normal";
        }
    }

    function CheckAll(checkBox) {
        CheckAllCheckBoxes(checkBox, "<%:PresentationGrid.ClientID %>", "chkDelete");
    }

    function CheckSingle(colId) {
        CheckSingleCheckBox(colId, "<%:PresentationGrid.ClientID %>", "chkDelete");
    }

    function CheckSelected() {
        return CheckSelectedCheckBoxes("<%:PresentationGrid.ClientID %>", "chkDelete");
    }

    function CheckSeletedDelete(sender, args) {
        var isSelected = CheckSelected();
        var isConfirmed = false;
        if (!isSelected) {
            alert("Please select some orders first.");
        }
        else {
            isConfirmed = DeleteConfirm();
        }
        sender.set_autoPostBack(isConfirmed);
    }
</script>
<asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="30%">
                        <telerik:RadButton runat="server" ID="btnBulkDelete" ToolTip="Delete Selected Orders"
                            OnClick="btnBulkDelete_OnClick" OnClientClicked="CheckSeletedDelete" Visible="false">
                            <ContentTemplate>
                                <img src="../images/icondelete.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Delete</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        &nbsp;
                        <telerik:RadButton runat="server" ID="btnExcel" ToolTip="Download as Excel" OnClick="btnExcel_OnClick">
                            <ContentTemplate>
                                <img src="../images/export_excel.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Download</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        &nbsp;
                        <telerik:RadButton ID="btnUMA" runat="server" ToggleType="Radio" ButtonType="ToggleButton"
                            Text="UMA" Value="UMA" GroupName="ClientManagementType" Checked="true" OnClick="btnUMA_OnClick"
                            Visible="False">
                        </telerik:RadButton>
                        <telerik:RadButton ID="btnSMA" runat="server" ToggleType="Radio" Text="Super" Value="SMA"
                            GroupName="ClientManagementType" ButtonType="ToggleButton" OnClick="btnSMA_OnClick"
                            Visible="False">
                        </telerik:RadButton>
                    </td>
                    <td style="width: 30%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <br />
        <div id="MainView" style="background-color: white">
            <table style="width: 100%">
                <tr style="display: none">
                    <td style="width: 100px">
                        Order Status:
                    </td>
                    <td colspan="5">
                        <telerik:RadListBox ID="lstStatus" runat="server" CheckBoxes="true" Width="220px"
                            SelectionMode="Multiple" OnClientItemChecked="onItemChecked">
                        </telerik:RadListBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Date:
                    </td>
                    <td style="width: 50px">
                        From:
                    </td>
                    <td style="width: 200px">
                        <telerik:RadDatePicker ID="calFrom" runat="server">
                            <DateInput DateFormat="dd/MM/yyyy" runat="server">
                            </DateInput>
                        </telerik:RadDatePicker>
                    </td>
                    <td style="width: 50px">
                        To:
                    </td>
                    <td style="width: 200px">
                        <telerik:RadDatePicker ID="calTo" runat="server">
                            <DateInput DateFormat="dd/MM/yyyy" runat="server">
                            </DateInput>
                        </telerik:RadDatePicker>
                    </td>
                    <td>
                        <telerik:RadButton runat="server" ID="btnApplyFilter" Text="Apply Filter" OnClick="btnApplyFilter_OnClick">
                        </telerik:RadButton>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <telerik:RadGrid OnNeedDataSource="PresentationGridNeedDataSource" ID="PresentationGrid"
            runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
            AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGridDetailTableDataBind"
            OnItemCommand="PresentationGridItemCommand" GridLines="None" AllowAutomaticDeletes="True"
            AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
            EnableViewState="true" ShowFooter="false" OnItemDataBound="PresentationGridItemDataBound"
            EnableLinqExpressions="false">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                Name="ActiveOrders" TableLayout="Fixed" Font-Size="8">
                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                <DetailTables>
                    <telerik:GridTableView Name="SSOrderDetails" Width="100%" Font-Size="8">
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="false"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ID" UniqueName="ID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="OrderID" ReadOnly="true" HeaderText="OrderID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="OrderID" UniqueName="OrderID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ProductID" ReadOnly="true" HeaderText="ProductID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ProductID" UniqueName="ProductID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="80px" SortExpression="FundCode"
                                ReadOnly="true" HeaderText="Fund Code" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="FundCode" UniqueName="FundCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="220px" HeaderStyle-Width="260px" SortExpression="FundName"
                                ReadOnly="true" HeaderText="Fund Name" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="FundName" UniqueName="FundName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" SortExpression="Units"
                                ReadOnly="true" HeaderText="Units" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Units" UniqueName="Units"
                                DataFormatString="{0:N4}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Amount"
                                ReadOnly="true" HeaderText="Amount" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Amount" UniqueName="Amount"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="CreatedDate"
                                ReadOnly="true" HeaderText="CreatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedDate" UniqueName="CreatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="UpdatedDate"
                                ReadOnly="true" HeaderText="UpdatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UpdatedDate" UniqueName="UpdatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ValidationMsg" ReadOnly="true" HeaderText="Validation Msg"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ValidationMsg" UniqueName="ValidationMsg">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </telerik:GridTableView>
                    <telerik:GridTableView Name="ASXOrderDetails" Width="100%" Font-Size="8">
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="false"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ID" UniqueName="ID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="OrderID" ReadOnly="true" HeaderText="OrderID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="OrderID" UniqueName="OrderID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ProductID" ReadOnly="true" HeaderText="ProductID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ProductID" UniqueName="ProductID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" SortExpression="InvestmentCode"
                                ReadOnly="true" HeaderText="Investment Code" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InvestmentCode"
                                UniqueName="InvestmentCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="220px" HeaderStyle-Width="260px" SortExpression="InvestmentName"
                                ReadOnly="true" HeaderText="Investment Name" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InvestmentName"
                                UniqueName="InvestmentName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" SortExpression="Units"
                                ReadOnly="true" HeaderText="Units" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Units" UniqueName="Units"
                                DataFormatString="{0:N4}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Amount"
                                ReadOnly="true" HeaderText="Amount" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Amount" UniqueName="Amount"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="CreatedDate"
                                ReadOnly="true" HeaderText="CreatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedDate" UniqueName="CreatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="UpdatedDate"
                                ReadOnly="true" HeaderText="UpdatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UpdatedDate" UniqueName="UpdatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ValidationMsg" ReadOnly="true" HeaderText="Validation Msg"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ValidationMsg" UniqueName="ValidationMsg">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </telerik:GridTableView>
                    <telerik:GridTableView Name="TDOrderDetails" Width="100%" Font-Size="8" BackColor="White">
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="false"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ID" UniqueName="ID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="OrderID" ReadOnly="true" HeaderText="OrderID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="OrderID" UniqueName="OrderID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="BrokerID" ReadOnly="true" HeaderText="BrokerID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="BrokerID" UniqueName="BrokerID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="BrokerName" ReadOnly="true" HeaderText="Broker Name"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="BrokerName" UniqueName="BrokerName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="BankID" ReadOnly="true" HeaderText="BankID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="BankID" UniqueName="BankID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="BankName" ReadOnly="true" HeaderText="Bank Name"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="BankName" UniqueName="BankName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Min"
                                ReadOnly="true" HeaderText="Min" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Min" UniqueName="Min"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Max"
                                ReadOnly="true" HeaderText="Max" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Max" UniqueName="Max"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="80px" SortExpression="Rating"
                                ReadOnly="true" HeaderText="Rating" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Rating" UniqueName="Rating">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="35px" HeaderStyle-Width="75px" SortExpression="Duration"
                                ReadOnly="true" HeaderText="Duration" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Duration" UniqueName="Duration">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="80px" SortExpression="Percentage"
                                ReadOnly="true" HeaderText="Percentage" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Percentage" UniqueName="Percentage"
                                DataFormatString="{0:P}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="Units" ReadOnly="true" HeaderText="Units"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="Units" UniqueName="Units" Visible="false"
                                DataFormatString="{0:N4}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Amount"
                                ReadOnly="true" HeaderText="Amount" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Amount" UniqueName="Amount"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="CreatedDate"
                                ReadOnly="true" HeaderText="CreatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedDate" UniqueName="CreatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="UpdatedDate"
                                ReadOnly="true" HeaderText="UpdatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UpdatedDate" UniqueName="UpdatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ValidationMsg" ReadOnly="true" HeaderText="Validation Msg"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ValidationMsg" UniqueName="ValidationMsg">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </telerik:GridTableView>
                    <telerik:GridTableView Name="AtCallOrderDetails" Width="100%" Font-Size="8" AllowFilteringByColumn="False">
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="false"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ID" UniqueName="ID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="OrderID" ReadOnly="true" HeaderText="OrderID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="OrderID" UniqueName="OrderID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="80px" SortExpression="TransferTo"
                                ReadOnly="true" HeaderText="Transfer To" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransferTo" UniqueName="TransferTo">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="220px" HeaderStyle-Width="100px" SortExpression="TransferAccBSB"
                                ReadOnly="true" HeaderText="BSB" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransferAccBSB"
                                UniqueName="TransferAccBSB">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="220px" HeaderStyle-Width="120px" SortExpression="TransferAccNumber"
                                ReadOnly="true" HeaderText="Account No" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransferAccNumber"
                                UniqueName="TransferAccNumber">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="AtCallType" ReadOnly="true" HeaderText="AtCallType"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="AtCallType" UniqueName="AtCallType"
                                Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="BrokerID" ReadOnly="true" HeaderText="BrokerID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="BrokerID" UniqueName="BrokerID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="BrokerName" ReadOnly="true" HeaderText="Broker Name"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="BrokerName" UniqueName="BrokerName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="BankID" ReadOnly="true" HeaderText="BankID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="BankID" UniqueName="BankID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="BankName" ReadOnly="true" HeaderText="Bank Name"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="BankName" UniqueName="BankName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="AccountTier" HeaderText="Description" AutoPostBackOnFilter="false"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                ReadOnly="true" DataField="AccountTier" UniqueName="AccountTier">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Min"
                                ReadOnly="true" HeaderText="Min" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Min" UniqueName="Min"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Max"
                                ReadOnly="true" HeaderText="Max" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Max" UniqueName="Max"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="180px" SortExpression="Rate"
                                ReadOnly="true" HeaderText="Rate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Rate" UniqueName="Rate"
                                DataFormatString="{0:P}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Amount"
                                ReadOnly="true" HeaderText="Amount" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Amount" UniqueName="Amount"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="CreatedDate"
                                ReadOnly="true" HeaderText="CreatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedDate" UniqueName="CreatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="UpdatedDate"
                                ReadOnly="true" HeaderText="UpdatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UpdatedDate" UniqueName="UpdatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ValidationMsg" ReadOnly="true" HeaderText="Validation Msg"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ValidationMsg" UniqueName="ValidationMsg">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </telerik:GridTableView>
                </DetailTables>
                <Columns>
                    <telerik:GridTemplateColumn UniqueName="OrderID" HeaderText="Order ID" ShowFilterIcon="true"
                        FilterControlWidth="40px" HeaderStyle-Width="75px" ItemStyle-Width="75px" SortExpression="OrderID"
                        AllowFiltering="true" DataField="OrderID">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnOrderID" runat="server" Text='<%# Eval("OrderID") %>' OnClick="ShowHideDetailClick"
                                Font-Size="8"></asp:LinkButton>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="ID" UniqueName="ID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="ClientCID"
                        SortExpression="ClientID" UniqueName="ClientID" DataNavigateUrlFormatString="../ClientViews/ClientMainView.aspx?ins={0}"
                        HeaderText="Client ID" DataTextField="ClientID" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" FilterControlWidth="50px" HeaderStyle-Width="85px"
                        HeaderButtonType="TextButton" ShowFilterIcon="true" Visible="false">
                    </telerik:GridHyperLinkColumn>
                    <telerik:GridBoundColumn SortExpression="AccountCID" ReadOnly="true" HeaderText="AccountCID"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="AccountCID" UniqueName="AccountCID"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="AccountNumber" ReadOnly="true" HeaderText="AccountNumber"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="AccountNumber" UniqueName="AccountNumber"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="85px" SortExpression="OrderAccountType"
                        HeaderText="Account Type" ReadOnly="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="OrderAccountType"
                        UniqueName="OrderAccountType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="45px" HeaderStyle-Width="85px" ItemStyle-Width="85px"
                        ReadOnly="true" SortExpression="OrderType" ShowFilterIcon="true" HeaderText="Order Type"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                        DataField="OrderType" UniqueName="OrderType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="35px" HeaderStyle-Width="70px" ReadOnly="true"
                        SortExpression="OrderItemType" HeaderText="Item Type" AutoPostBackOnFilter="false"
                        ShowFilterIcon="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                        DataField="OrderItemType" UniqueName="OrderItemType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="35px" HeaderStyle-Width="70px" ReadOnly="true"
                        SortExpression="OrderPreferedBy" HeaderText="Requested By" AutoPostBackOnFilter="false"
                        ShowFilterIcon="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                        DataField="OrderPreferedBy" UniqueName="OrderPreferedBy">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn FilterControlWidth="45px" HeaderStyle-Width="80px" ReadOnly="true"
                        SortExpression="Status" HeaderText="Status" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Status" UniqueName="Status">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="hypLinkStatus" Text='<%# Bind("Status") %>'></asp:HyperLink>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" ReadOnly="true"
                        SortExpression="Value" HeaderText="Value" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Value" UniqueName="Value"
                        DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="85px" ReadOnly="true"
                        SortExpression="TradeDate" HeaderText="Trade Date" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="TradeDate" UniqueName="TradeDate" DataFormatString="{0:dd/MM/yyyy}"
                        DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="55px" HeaderStyle-Width="90px" SortExpression="CreatedBy"
                        ReadOnly="true" HeaderText="Created By" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedBy" UniqueName="CreatedBy">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="65px" HeaderStyle-Width="100px" ReadOnly="true"
                        SortExpression="CreatedDate" HeaderText="Create Date " AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="CreatedDate" UniqueName="CreatedDate" DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}"
                        DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="65px" HeaderStyle-Width="100px" ReadOnly="true"
                        SortExpression="UpdatedDate" HeaderText="Update Date" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="UpdatedDate" UniqueName="UpdatedDate" DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}"
                        DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="120px" ReadOnly="true" SortExpression="Comments"
                        HeaderText="Comments" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Comments" UniqueName="Comments">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="120px" SortExpression="ValidationMsg"
                        ReadOnly="true" HeaderText="Validation Msg" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ValidationMsg"
                        UniqueName="ValidationMsg" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderStyle-Width="50px" UniqueName="Actions" HeaderText="Actions"
                        AllowFiltering="false">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgUpdate" runat="server" ImageUrl="../images/update.png" AlternateText="Update Status"
                                ToolTip="Update Status Manually" CommandName="statusupdate" CommandArgument='<%# Eval("ID") %>'
                                Visible="false" />
                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="../images/delete.png" AlternateText="Delete"
                                ToolTip="Delete Order" OnClientClick="javascript:return confirm('Are you sure you want to delete this order?');"
                                CommandName="delete" CommandArgument='<%# Eval("ID") %>' />
                            <asp:ImageButton ID="imgIncomplete" runat="server" ImageUrl="../images/incomplete.png"
                                Visible="false" AlternateText="Incomplete" ToolTip="Incomplete Order" CommandName="incomplete"
                                CommandArgument='<%# Eval("ID") %>' OnClientClick="javascript:return confirm('Are you sure you want to incomplete this order?');" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="BulkDelete" AllowFiltering="false" HeaderStyle-Width="35px"
                        HeaderTooltip="Select Orders to Delete">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDelete" runat="server" onclick="CheckSingle('BulkDelete');"
                                ToolTip="Select Orders" />
                        </ItemTemplate>
                        <HeaderTemplate>
                            <asp:CheckBox ID="headerChkbox" runat="server" onclick="CheckAll(this);" ToolTip="Select All Orders" />
                        </HeaderTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <telerik:RadToolTipManager ID="RadToolTipManager1" runat="server" AutoTooltipify="true"
            Width="275px" Skin="Default" Animation="Fade">
        </telerik:RadToolTipManager>
        <asp:HiddenField ID="hfOrderID" runat="server" />
        <asp:Button ID="Button1" runat="server" Style="display: none" />
        <asp:ModalPopupExtender ID="popupStatus" runat="server" TargetControlID="Button1"
            PopupControlID="pnlStatus" PopupDragHandleControlID="PopupHeader" Drag="true"
            BackgroundCssClass="ModalPopupBG" BehaviorID="ModalBehaviour">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlStatus" runat="server" BackColor="White" Style="display: none">
            <div class="popup_Container">
                <div class="popup_Titlebar" id="Div1">
                    <div class="TitlebarLeft">
                        Update Status
                    </div>
                    <div class="TitlebarRight" onclick="HideModalPopup();">
                    </div>
                </div>
                <div class="PopupBody">
                    <table>
                        <tr>
                            <td>
                                Order Status:
                            </td>
                            <td>
                                <telerik:RadComboBox ID="cmbStatus" runat="server" ZIndex="10000000">
                                </telerik:RadComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <asp:Button ID="btnUpdate" runat="server" Text="Save" OnClick="btnUpdate_OnClick" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
        <asp:HiddenField ID="hfIsAdmin" runat="server" />
        <asp:HiddenField ID="hfIsAdminMenu" runat="server" />
        <asp:HiddenField ID="hfIsBulkDelete" runat="server" />
        <asp:HiddenField ID="hfIsFiltered" runat="server" />
        <asp:HiddenField ID="hfFilter" runat="server" />
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExcel" />
    </Triggers>
</asp:UpdatePanel>
