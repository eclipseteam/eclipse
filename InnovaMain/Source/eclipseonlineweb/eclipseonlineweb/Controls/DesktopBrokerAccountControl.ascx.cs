﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;
using eclipseonlineweb.ClientViews;

namespace eclipseonlineweb.Controls
{
    public partial class DesktopBrokerAccountControl : UserControl
    {
        private ICMBroker UMABroker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }
        public Action<Guid, Guid, Guid> Saved
        {
            get;
            set;
        }

        public Action<string, DataSet> SaveData
        {
            get;
            set;
        }
        
        public Guid selectedCLID { get; set; }
        public Guid selectedCSID { get; set; }

        public Action Canceled { get; set; }

        public Action ValidationFailed { get; set; }

        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadControls();
            }
        }
        private void LoadControls()
        {
            //bind combos here
          
          
            var org = this.Broker.GetWellKnownBMC(WellKnownCM.Organization);

            StatusDS status = new StatusDS();
            status.TypeFilter = "DesktopBrokerAccount";
            org.GetData(status);
            cmbStatus.Items.Clear();
            cmbStatus.DataSource = status.Tables[StatusDS.TABLENAME];
            cmbStatus.DataTextField = StatusDS.NAME;
            cmbStatus.DataValueField = StatusDS.NAME;
            cmbStatus.DataBind();
            //Broker.ReleaseBrokerManagedComponent(org);
        }
        
        public bool Validate()
        {
            bool result;
            StringBuilder messages = new StringBuilder();
            bool tempResult = txtAccountName.ValidateAlphaNumeric(true);
            result = tempResult;
            AddMessageIn(tempResult, "Invalid Account Name *", messages);

            tempResult = txtAccountNumber.ValidateAlphaNumeric(1, 25);
            result = tempResult && result;
            AddMessageIn(tempResult, "Invalid Account Number *", messages);

            tempResult = txtAccountDesignation.ValidateAlphaNumeric(false);
            result = tempResult && result;
            AddMessageIn(tempResult, "Invalid Account Designation", messages);

            tempResult = txtHINNumber.Validate();
            result = tempResult && result;
            AddMessageIn(tempResult, "Invalid HIN #", messages);

            lblMessages.Text = messages.ToString();

            return result;
        }

        private static void AddMessageIn(bool result, string message, StringBuilder messages)
        {
            if (!result)
            {
                messages.Append(message + "<br/>");
            }
        }

        public void SetEntity(Guid cid)
        {
            ClearEntity();
            LoadControls();
            hfCID.Value = cid.ToString();
            if (cid != Guid.Empty)
            {
                DesktopBrokerAccountDS ds = GetDataSet(cid);
                DataRow dr = ds.Tables[ds.DesktopBrokerAccountsTable.TABLENAME].Rows[0];
                cmbStatus.SelectedValue = dr[ds.DesktopBrokerAccountsTable.STATUS].ToString();
                txtAccountName.Text = dr[ds.DesktopBrokerAccountsTable.ACCOUNTNAME].ToString();
                txtAccountNumber.Text = dr[ds.DesktopBrokerAccountsTable.ACCOUNTNO].ToString();
                txtAccountDesignation.Text = dr[ds.DesktopBrokerAccountsTable.ACCOUNTDESIGNATION].ToString();
                txtHINNumber.SetEntity(dr[ds.DesktopBrokerAccountsTable.HINNUMBER].ToString());
                hfCID.Value = dr[ds.DesktopBrokerAccountsTable.CID].ToString();
                hfCLID.Value = dr[ds.DesktopBrokerAccountsTable.CLID].ToString();
                hfCSID.Value = dr[ds.DesktopBrokerAccountsTable.CSID].ToString();
            }
        }

        private DesktopBrokerAccountDS GetDataSet(Guid cid)
        {
            DesktopBrokerAccountDS desktopBrokerAccountDs = new DesktopBrokerAccountDS { CommandType = DatasetCommandTypes.Get, Command = (int)WebCommands.GetOrganizationUnitDetails };
            desktopBrokerAccountDs.Unit = new OrganizationUnit { Cid = cid, CurrentUser = (Page as UMABasePage).GetCurrentUser(), Type = ((int)OrganizationType.DesktopBrokerAccount).ToString() };
            if (Broker != null)
            {
                IBrokerManagedComponent org = Broker.GetWellKnownBMC(WellKnownCM.Organization);
                org.GetData(desktopBrokerAccountDs);
                Broker.ReleaseBrokerManagedComponent(org);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            return desktopBrokerAccountDs;
        }

        public DesktopBrokerAccountDS GetEntity(DesktopBrokerAccountDS desktopBrokerAccountDs)
        {
            DataTable dataTable = desktopBrokerAccountDs.Tables[desktopBrokerAccountDs.DesktopBrokerAccountsTable.TABLENAME];
            DataRow dataRow = dataTable.NewRow();


            dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.CID] = hfCID.Value;
            dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.CLID] = hfCLID.Value;
            dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.CSID] = hfCSID.Value;
            dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.ACCOUNTNAME] = txtAccountName.Text.Trim();
            dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.ACCOUNTNO] = txtAccountNumber.Text.Trim();
            dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.ACCOUNTDESIGNATION] = txtAccountDesignation.Text.Trim();
            dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.HINNUMBER] = txtHINNumber.GetEntity();
            dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.STATUS] = cmbStatus.SelectedValue;

            dataTable.Rows.Add(dataRow);
            desktopBrokerAccountDs.Tables.Remove(desktopBrokerAccountDs.DesktopBrokerAccountsTable.TABLENAME);
            desktopBrokerAccountDs.Tables.Add(dataTable);
            return desktopBrokerAccountDs;
        }

        public void ClearEntity()
        {
            if (cmbStatus.Items.Count > 0)
            {
                cmbStatus.SelectedIndex = 0;
            }
            txtAccountName.Text = string.Empty;
            txtAccountNumber.Text = string.Empty;
            txtAccountDesignation.Text = string.Empty;
            txtHINNumber.ClearEntity();
            hfCID.Value = Guid.Empty.ToString();
            hfCLID.Value = Guid.Empty.ToString();
            hfCSID.Value = Guid.Empty.ToString();
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
                if (Validate() == false)
                {
                    if (ValidationFailed != null)
                        ValidationFailed();
                    return;
                }
                var cid = UpdateData();
                if (Saved != null)
                {
                    Saved(cid, new Guid(hfCLID.Value), new Guid(hfCSID.Value));
                }
        }

        private Guid UpdateData()
        {
            DesktopBrokerAccountDS desktopBrokerAccountDs = new DesktopBrokerAccountDS();
            var cid = new Guid(hfCID.Value);
            string RepUnitCID = Request.QueryString["ins"];
            if (!string.IsNullOrWhiteSpace(RepUnitCID))
                desktopBrokerAccountDs.RepUnitCID = cid;
            if (cid == Guid.Empty)
            {
                desktopBrokerAccountDs.CommandType = DatasetCommandTypes.Add;
            }
            else
            {
                desktopBrokerAccountDs.CommandType = DatasetCommandTypes.Update;
            }

            if (Broker != null)
            {
                //desktopBrokerAccountDs.RepUnitID = ((DesktopBrokerAccount)Page).FindControl();
                desktopBrokerAccountDs.DesktopBrokerAccountsTable = GetEntity(desktopBrokerAccountDs).DesktopBrokerAccountsTable;

                if (hfCID.Value == Guid.Empty.ToString())
                {
                    var unit = new OrganizationUnit
                                   {
                                       Name = txtAccountName.Text,
                                       Type = ((int)OrganizationType.DesktopBrokerAccount).ToString(),
                                       CurrentUser = (Page as UMABasePage).GetCurrentUser()
                                   };

                    desktopBrokerAccountDs.Unit = unit;
                    desktopBrokerAccountDs.Command = (int)WebCommands.AddNewOrganizationUnit;
                }

                if (SaveData != null)
                {
                    SaveData(hfCID.Value, desktopBrokerAccountDs);
                    if (desktopBrokerAccountDs.CommandType == DatasetCommandTypes.Add)
                    {
                        cid = desktopBrokerAccountDs.Unit.Cid;
                        hfCLID.Value = desktopBrokerAccountDs.Unit.Clid.ToString();
                        hfCSID.Value = desktopBrokerAccountDs.Unit.Csid.ToString();
                        hfCID.Value = cid.ToString();
                    }
                }
            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            return cid;
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (Canceled != null)
            {
                Canceled();
            }
        }
    }
}