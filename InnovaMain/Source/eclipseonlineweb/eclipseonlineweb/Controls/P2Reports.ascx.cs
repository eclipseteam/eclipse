﻿using System;
using System.Data;
using System.Linq;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Utilities;
using Telerik.Web.UI;

namespace eclipseonlineweb.Controls
{
    public partial class P2Reports : System.Web.UI.UserControl
    {
        private DataSet PresentationData { get; set; }

        private DataSet P2ReportData { get; set; }

        public string ClientCID
        {
            private get { return hfClientCID.Value; }
            set { hfClientCID.Value = value; }
        }

        public bool IsAdmin
        {
            private get { return Convert.ToBoolean(hfIsAdmin.Value); }
            set { hfIsAdmin.Value = value.ToString(); }
        }

        public bool IsAdminMenu
        {
            private get
            {
                if (string.IsNullOrEmpty(hfIsAdminMenu.Value))
                {
                    hfIsAdminMenu.Value = "false";
                }
                return Convert.ToBoolean(hfIsAdminMenu.Value);
            }
            set { hfIsAdminMenu.Value = value.ToString(); }
        }

        private ICMBroker Broker
        {
            get { return (Page as UMABasePage).UMABroker; }
        }

        public Action<string, DataSet> SaveData { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            searchAccountControl.SelectData += (clientCID, clientId, clientName) =>
                {
                    lblErrorMessage.Text = string.Empty;
                    ClientHeaderInfo1.SetEntity(new Guid(clientCID));
                    ClientHeaderInfo1.Visible = true;
                    ClientCID = clientCID;
                    pnlUploadReport.Visible = true;
                    pnlReportGrid.Visible = true;
                    PresentationGrid.Rebind();
                };

            searchAccountControl.ShowHideModal += show =>
                {
                    if (show)
                    {
                        popupSearch.Show();
                    }
                    else
                    {
                        popupSearch.Hide();
                    }
                };

            if (!IsPostBack)
            {
                btnSearch.Visible = IsAdminMenu;
                searchAccountControl.IsAdminMenu = IsAdminMenu;
                PresentationGrid.MasterTableView.GetColumnSafe("DeleteButton").Visible = IsAdmin;
                txtYear.MinValue = 1980;
                txtYear.MaxValue = DateTime.Now.Year;
                txtYear.Value = DateTime.Now.Year;
                if (!IsAdminMenu)
                {
                    ClientHeaderInfo1.SetEntity(new Guid(ClientCID));
                    ClientHeaderInfo1.Visible = true;
                    pnlReportGrid.Visible = true;
                    PresentationGrid.Rebind();
                }
            }
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            popupSearch.Show();
            searchAccountControl.HideModal = false;
            searchAccountControl.FindControl("SearchBox").Focus();
        }

        protected void Upload_Click(object sender, EventArgs e)
        {
            var attachmentListDS = new AttachmentListDS();
            var objAttachmentsTable = attachmentListDS.Tables[AttachmentListDS.ATTACHMENTTABLE];
            objAttachmentsTable.AcceptChanges();

            var objUser = (DBUser)Broker.GetBMCInstance(Broker.UserContext.Identity.Name, "DBUser_1_1");
            var ds = new P2ReportsDS();

            IOrganizationUnit clientData = Broker.GetBMCInstance(new Guid(ClientCID)) as IOrganizationUnit;
            if (clientData != null)
            {
                AttachmentListDS attachmentDS = new AttachmentListDS();
                clientData.GetData(attachmentDS);
                PresentationData = attachmentDS;
                clientData.GetData(ds);
                Broker.ReleaseBrokerManagedComponent(clientData);
                P2ReportData = ds.Copy();

                ds.P2ReportDetailsTable.Clear();

                foreach (UploadedFile uploadedFile in AsyncUpload.UploadedFiles)
                {
                    if (IsFileExists(uploadedFile.FileName))
                    {
                        //Raise the Duplication Error Message
                        lblErrorMessage.Text = string.Format("{0} already exists!", uploadedFile.FileName);
                        break;
                    }

                    Guid attachementID = Guid.NewGuid();
                    DataRow objDataRow = objAttachmentsTable.NewRow();

                    objDataRow[AttachmentListDS.ATTACHMENTID_FIELD] = attachementID;
                    objDataRow[AttachmentListDS.ATTACHMENTDESCRIPTION_FIELD] = uploadedFile.FileName;
                    objDataRow[AttachmentListDS.ATTACHMENTDATEIMPORTED_FIELD] = DateTime.Now;
                    objDataRow[AttachmentListDS.ATTACHMENTORIGINPATH_FIELD] = uploadedFile.FileName;
                    objDataRow[AttachmentListDS.ATTACHMENTIMPORTEDBY_FIELD] = objUser.Name;
                    objDataRow[AttachmentListDS.ATTACHMENTFILE_FIELD] = uploadedFile.InputStream;
                    objDataRow[AttachmentListDS.ATTACHMENTTYPE_FIELD] = AttachmentType.P2Reports;
                    objDataRow[AttachmentListDS.ATTACHMENTLINKED_FIELD] = false;

                    objAttachmentsTable.Rows.Add(objDataRow);

                    //fill report DS
                    DataRow row = ds.P2ReportDetailsTable.NewRow();
                    row[ds.P2ReportDetailsTable.ATTACHEDBMCID] = attachementID;
                    row[ds.P2ReportDetailsTable.YEAR] = txtYear.Text;
                    row[ds.P2ReportDetailsTable.QUARTER] = rbQuarter.SelectedValue;
                    row[ds.P2ReportDetailsTable.P2TYPE] = rbType.SelectedValue;
                    ds.P2ReportDetailsTable.Rows.Add(row);
                }

                if (string.IsNullOrEmpty(lblErrorMessage.Text))
                {
                    SaveClientData(ClientCID, attachmentListDS, ds);
                    PresentationGrid.Rebind();
                }
            }
        }

        private void SaveClientData(string cid, DataSet ds, P2ReportsDS p2ReportsDs)
        {
            if (SaveData != null)
            {
                SaveData(cid, ds);
                SaveData(cid, p2ReportsDs);
            }
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(ClientCID))
                BindGridData();
        }

        protected void PresentationGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                if (gDataItem != null)
                {
                    IOrganizationUnit clientData = Broker.GetBMCInstance(new Guid(ClientCID)) as IOrganizationUnit;
                    if (clientData != null)
                    {
                        Guid rowID = new Guid(gDataItem.OwnerTableView.DataKeyValues[gDataItem.ItemIndex]["ID"].ToString());

                        AttachmentListDS attachmentListDS = new AttachmentListDS();
                        clientData.GetData(attachmentListDS);
                        PresentationData = attachmentListDS;

                        var p2ReportsDS = new P2ReportsDS();
                        clientData.GetData(p2ReportsDS);

                        DataTable objAttachmentsTable = PresentationData.Tables[AttachmentListDS.ATTACHMENTTABLE];

                        if (objAttachmentsTable.Columns.Contains(AttachmentListDS.ATTACHMENTTYPENAME_FIELD))
                            objAttachmentsTable.Columns.Remove(AttachmentListDS.ATTACHMENTTYPENAME_FIELD);

                        objAttachmentsTable.AcceptChanges();

                        foreach (DataRow objRow in objAttachmentsTable.Rows)
                        {
                            if (new Guid((string)objRow[AttachmentListDS.ATTACHMENTID_FIELD]) == rowID)
                            {
                                objRow.Delete();
                                Broker.LogEvent(EventType.UMAClientUpdated, clientData.CID, "The attachments of Client:" + clientData.ClientId + "-" + clientData.Name + "is Deleted");
                            }
                        }

                        p2ReportsDS.P2ReportDetailsTable.AcceptChanges();

                        foreach (DataRow objRow in p2ReportsDS.P2ReportDetailsTable.Rows)
                        {
                            if (new Guid((string)objRow[p2ReportsDS.P2ReportDetailsTable.ATTACHEDBMCID]) == rowID)
                            {
                                objRow.Delete();
                            }
                        }
                        Broker.ReleaseBrokerManagedComponent(clientData);
                        SaveClientData(ClientCID, PresentationData, p2ReportsDS);
                        e.Canceled = true;
                        e.Item.Edit = false;
                        PresentationGrid.Rebind();
                    }
                }
            }
        }

        private void BindGridData()
        {
            IOrganizationUnit clientData = Broker.GetBMCInstance(new Guid(ClientCID)) as IOrganizationUnit;
            AttachmentListDS attachmentListDS = new AttachmentListDS();
            if (clientData != null)
            {
                clientData.GetData(attachmentListDS);
                PresentationData = attachmentListDS;
                var ds = new P2ReportsDS();
                clientData.GetData(ds);
                Broker.ReleaseBrokerManagedComponent(clientData);
                
                if (PresentationData != null && PresentationData.Tables.Contains(AttachmentListDS.ATTACHMENTTABLE))
                {
                    var q = from tA in ds.P2ReportDetailsTable.AsEnumerable()
                            join tB in PresentationData.Tables[AttachmentListDS.ATTACHMENTTABLE].AsEnumerable()
                                on tA[ds.P2ReportDetailsTable.ATTACHEDBMCID].ToString() equals
                                tB[AttachmentListDS.ATTACHMENTID_FIELD].ToString()
                            select new
                                {
                                    ID = tB[AttachmentListDS.ATTACHMENTID_FIELD],
                                    BMCID = tB[AttachmentListDS.ATTACHEDBMCID_FIELD],
                                    Year = tA["Year"],
                                    Quarter = string.Format("Q{0}", tA["Quarter"]),
                                    P2Type = tA["P2Type"].ToString().ToUpper(),
                                    ORIGINPATH = tB["ORIGINPATH"]
                                };

                    PresentationGrid.DataSource = q.OrderByDescending(x => x.Year).ThenByDescending(x => x.Quarter).ToList();
                }
            }
        }

        private bool IsFileExists(string fileName)
        {
            var ds = P2ReportData as P2ReportsDS;
            if (ds != null)
            {
                var p2ReportDetailsTable = ds.P2ReportDetailsTable;

                var q = from tA in ds.Tables[0].AsEnumerable()
                        where p2ReportDetailsTable != null
                        join tB in PresentationData.Tables[AttachmentListDS.ATTACHMENTTABLE].AsEnumerable()
                            on tA[p2ReportDetailsTable.ATTACHEDBMCID].ToString() equals
                            tB[AttachmentListDS.ATTACHMENTID_FIELD].ToString()
                        where tB[AttachmentListDS.ATTACHMENTORIGINPATH_FIELD].ToString() == fileName &&
                              tA[p2ReportDetailsTable.YEAR].ToString() == txtYear.Text &&
                              tA[p2ReportDetailsTable.QUARTER].ToString() == rbQuarter.SelectedValue &&
                              tA[p2ReportDetailsTable.P2TYPE].ToString() == rbType.SelectedValue
                        select new { tA, tB };

                return (q.Any());
            }
            return false;
        }
    }
}