﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TFN_InputControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.TFN_InputControl" %>
<span id="ctrlABNBody">
    <telerik:RadMaskedTextBox runat="server" ID="txtValue1" Mask="###" MaskType="Numeric"
        Font-Names="Arial,FreeSans,Helvetica,Verdana,sans-serif" Width="50px" PromptChar=" " />
    <span>&nbsp;</span>
    <telerik:RadMaskedTextBox runat="server" ID="txtValue2" Mask="###" MaskType="Numeric"
        Font-Names="Arial,FreeSans,Helvetica,Verdana,sans-serif" Width="50px" PromptChar=" " />
    <span>&nbsp;</span>
    <telerik:RadMaskedTextBox runat="server" ID="txtValue3" Mask="###" MaskType="Numeric"
        Font-Names="Arial,FreeSans,Helvetica,Verdana,sans-serif" Width="50px" PromptChar=" " />
    <span id="Msg" runat="server" visible="false" style="color: Red;"></span></span>
