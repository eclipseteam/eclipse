﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace eclipseonlineweb.Controls
{
    public partial class AdviserDashboard : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void SetPanelVisibilty(bool isAlertsVisible)
        {
            //For Adviser which Email Contains @nac.com.au  @priorityplanners.com.au
            trAlerts.Visible = isAlertsVisible;
            trHolding.Visible = !isAlertsVisible;
            trNotification.Visible = isAlertsVisible;
            trBestRates.Visible = isAlertsVisible;
        }
      
        #region Special Dashlets
        protected void GridTDRates_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //GridTDRates.DataSource = new DataTable();
        }

        protected void GridAtCallRates_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            // GridAtCallRates.DataSource = new DataTable();
        }
        #endregion

    }
}