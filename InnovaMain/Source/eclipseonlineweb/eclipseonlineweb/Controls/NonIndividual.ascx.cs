﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;

namespace eclipseonlineweb.Controls
{
    public partial class NonIndividual : System.Web.UI.UserControl
    {
        private ICMBroker Broker
        {
            get
            {
                var umaBasePage = Page as UMABasePage;
                if (umaBasePage != null) return umaBasePage.UMABroker;
                return null;
            }
        }

        public Action Canceled { get; set; }

        public Action ValidationFailed { get; set; }

        public Action<Guid, Guid, Guid> Saved { get; set; }

        public Action<string, DataSet> SaveData { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                foreach (NonIndividualsType val in Enum.GetValues(typeof (NonIndividualsType)))
                {
                    rblType.Items.Add(new ListItem(val.ToString()));
                }
            }

            dtoCreation.MaxDate = DateTime.Now.Date;
        }

        public void SetEntity(Guid cid)
        {
            ClearControls();
            if (cid == Guid.Empty)
            {
                txtMailingAddress.SetEntity(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                            string.Empty);
                txtResidentialAddress.SetEntity(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                                string.Empty);

                hfCID.Value = Guid.Empty.ToString();
                hfCLID.Value = Guid.Empty.ToString();
                hfCSID.Value = Guid.Empty.ToString();
            }
            else
            {
                NonIndividualDS ds = GetDataSet(cid);
                DataRow dr = ds.Tables[ds.NonIndividualTable.TABLENAME].Rows[0];

                hfCID.Value = dr[ds.NonIndividualTable.CID].ToString();
                hfCLID.Value = dr[ds.NonIndividualTable.CLID].ToString();
                hfCSID.Value = dr[ds.NonIndividualTable.CSID].ToString();

                rblType.SelectedIndex = -1;
                rblType.Items.FindByText(dr[ds.NonIndividualTable.TYPE].ToString()).Selected = true;

                txtgiventname.Text = dr[ds.NonIndividualTable.NAME].ToString();
                txtemailaddress.Text = dr[ds.NonIndividualTable.EMAILADDRESS].ToString();
                dtoCreation.SelectedDate = Convert.ToDateTime(dr[ds.NonIndividualTable.DATEOFCREATION]);

                txtmobilephonenumber.CountryCode = dr[ds.NonIndividualTable.MOBILECOUNTRYCODE].ToString();
                txtmobilephonenumber.Number = dr[ds.NonIndividualTable.MOBILEPHONENUMBER].ToString();

                txtOfficePhone.CountryCode = dr[ds.NonIndividualTable.OFFICECOUNTRYCODE].ToString();
                txtOfficePhone.CityCode = dr[ds.NonIndividualTable.OFFICECITYCODE].ToString();
                txtOfficePhone.Number = dr[ds.NonIndividualTable.OFFICEPHONENUMBER].ToString();

                txtOfficePhone2.CountryCode = dr[ds.NonIndividualTable.OFFICECOUNTRYCODE2].ToString();
                txtOfficePhone2.CityCode = dr[ds.NonIndividualTable.OFFICECITYCODE2].ToString();
                txtOfficePhone2.Number = dr[ds.NonIndividualTable.OFFICEPHONENUMBER2].ToString();

                txtfacsimile.CountryCode = dr[ds.NonIndividualTable.FACSIMILECOUNTRYCODE].ToString();
                txtfacsimile.CityCode = dr[ds.NonIndividualTable.FACSIMILECITYCODE].ToString();
                txtfacsimile.Number = dr[ds.NonIndividualTable.FACSIMILEPHONENUMBER].ToString();

                TFN_InputControl1.SetEntity(dr[ds.NonIndividualTable.TFN].ToString());
                ABN_InputControl1.SetEntity(dr[ds.NonIndividualTable.ABN].ToString());

                txtDescription.Text = dr[ds.NonIndividualTable.DESCRIPTION].ToString();

                txtResidentialAddress.SetEntity(
                    dr[ds.NonIndividualTable.RESIDENTIALADDRESSLINE1].ToString(),
                    dr[ds.NonIndividualTable.RESIDENTIALADDRESSLINE2].ToString(),
                    dr[ds.NonIndividualTable.RESIDENTIALADDRESSSUBRUB].ToString(),
                    dr[ds.NonIndividualTable.RESIDENTIALADDRESSSTATE].ToString(),
                    dr[ds.NonIndividualTable.RESIDENTIALADDRESSPOSTALCODE].ToString(),
                    dr[ds.NonIndividualTable.RESIDENTIALADDRESSCOUNTRY].ToString()
                    );

                txtMailingAddress.SetEntity(
                    dr[ds.NonIndividualTable.MAILINGADDRESSLINE1].ToString(),
                    dr[ds.NonIndividualTable.MAILINGADDRESSLINE2].ToString(),
                    dr[ds.NonIndividualTable.MAILINGADDRESSSUBRUB].ToString(),
                    dr[ds.NonIndividualTable.MAILINGADDRESSSTATE].ToString(),
                    dr[ds.NonIndividualTable.MAILINGADDRESSPOSTALCODE].ToString(),
                    dr[ds.NonIndividualTable.MAILINGADDRESSCOUNTRY].ToString()
                    );

                chkSameAddress.Checked = Convert.ToBoolean(dr[ds.NonIndividualTable.MAILINGADDRESSSAME]);
            }

        }

        private void ClearControls()
        {
            rblType.SelectedIndex = 0;
            txtgiventname.Text = string.Empty;
            txtemailaddress.Text = string.Empty;
            dtoCreation.SelectedDate = DateTime.Now.Date;

            txtmobilephonenumber.CountryCode = string.Empty;
            txtmobilephonenumber.Number = string.Empty;
            txtmobilephonenumber.ClearEntity();

            txtOfficePhone.CountryCode = string.Empty;
            txtOfficePhone.Number = string.Empty;
            txtOfficePhone.ClearEntity();

            txtOfficePhone2.CountryCode = string.Empty;
            txtOfficePhone2.Number = string.Empty;
            txtOfficePhone2.ClearEntity();

            txtfacsimile.CountryCode = string.Empty;
            txtfacsimile.Number = string.Empty;
            txtfacsimile.ClearEntity();

            txtDescription.Text = string.Empty;
            TFN_InputControl1.ClearEntity();
            ABN_InputControl1.ClearEntity();

            txtResidentialAddress.ClearEntity();
            txtMailingAddress.ClearEntity();
            chkSameAddress.Checked = false;
        }

        private NonIndividualDS GetDataSet(Guid cid)
        {
            var umaBasePage = Page as UMABasePage;
            if (umaBasePage != null)
            {
                var nonIndividualDs = new NonIndividualDS
                    {
                        CommandType = DatasetCommandTypes.Get,
                        Command = (int) WebCommands.GetOrganizationUnitDetails,
                        Unit =
                            new OrganizationUnit
                                {
                                    Cid = cid,
                                    CurrentUser = umaBasePage.GetCurrentUser(),
                                    Type = ((int) OrganizationType.NonIndividual).ToString()
                                }
                    };

                if (Broker == null)
                    throw new Exception("Broker Not Found");
                IBrokerManagedComponent org = Broker.GetBMCInstance(cid);
                org.GetData(nonIndividualDs);
                Broker.ReleaseBrokerManagedComponent(org);

                return nonIndividualDs;
            }

            return null;
        }


        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            if (Validate())
            {

                var cid = UpdateData();
                if (Saved != null)
                {
                    Saved(cid, new Guid(hfCLID.Value), new Guid(hfCSID.Value));
                }
            }
            else
            {
                if (ValidationFailed != null)
                    ValidationFailed();           
            }
        }

        private bool Validate()
        {
            bool result = txtmobilephonenumber.Validate() & txtOfficePhone.Validate() & txtOfficePhone2.Validate() & txtfacsimile.Validate()
                          & TFN_InputControl1.Validate() & ABN_InputControl1.Validate();
            return result;
        }


        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (Canceled != null)
            {
                Canceled();
            }
        }

        private Guid UpdateData()
        {
            var nonIndividualDS = new NonIndividualDS();
            var cid = Guid.Empty;

            if (!string.IsNullOrEmpty(hfCID.Value))
                cid = new Guid(hfCID.Value);

            nonIndividualDS.CommandType = cid == Guid.Empty ? DatasetCommandTypes.Add : DatasetCommandTypes.Update;

            if (Broker != null)
            {
                nonIndividualDS.NonIndividualTable = GetEntity(nonIndividualDS).NonIndividualTable;

                if (hfCID.Value == Guid.Empty.ToString())
                {
                    var unit = new OrganizationUnit
                        {
                            Name = txtgiventname.Text,
                            Type = ((int) OrganizationType.NonIndividual).ToString(),
                            CurrentUser = ((UMABasePage) Page).GetCurrentUser()
                        };

                    nonIndividualDS.Unit = unit;
                    nonIndividualDS.Command = (int) WebCommands.AddNewOrganizationUnit;
                }

                if (SaveData != null)
                {
                    SaveData(hfCID.Value, nonIndividualDS);
                    if (nonIndividualDS.CommandType == DatasetCommandTypes.Add)
                    {
                        cid = nonIndividualDS.Unit.Cid;
                        hfCLID.Value = nonIndividualDS.Unit.Clid.ToString();
                        hfCSID.Value = nonIndividualDS.Unit.Csid.ToString();
                        hfCID.Value = cid.ToString();
                    }
                }
            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            return cid;
        }

        public NonIndividualDS GetEntity(NonIndividualDS nonIndividualDS)
        {
            DataTable dataTable = nonIndividualDS.Tables[nonIndividualDS.NonIndividualTable.TABLENAME];
            DataRow dataRow = dataTable.NewRow();

            dataRow[nonIndividualDS.NonIndividualTable.CID] = hfCID.Value;
            dataRow[nonIndividualDS.NonIndividualTable.CLID] = hfCLID.Value;
            dataRow[nonIndividualDS.NonIndividualTable.CSID] = hfCSID.Value;
            
            dataRow[nonIndividualDS.NonIndividualTable.TYPE] = rblType.SelectedItem.Text;

            dataRow[nonIndividualDS.NonIndividualTable.NAME] = txtgiventname.Text;
            dataRow[nonIndividualDS.NonIndividualTable.EMAILADDRESS] = txtemailaddress.Text;
            dataRow[nonIndividualDS.NonIndividualTable.DATEOFCREATION] = dtoCreation.SelectedDate;

            dataRow[nonIndividualDS.NonIndividualTable.MOBILECOUNTRYCODE] = txtmobilephonenumber.CountryCode;
            dataRow[nonIndividualDS.NonIndividualTable.MOBILEPHONENUMBER] = txtmobilephonenumber.Number;

            dataRow[nonIndividualDS.NonIndividualTable.OFFICECOUNTRYCODE] = txtOfficePhone.CountryCode;
            dataRow[nonIndividualDS.NonIndividualTable.OFFICECITYCODE] = txtOfficePhone.CityCode;
            dataRow[nonIndividualDS.NonIndividualTable.OFFICEPHONENUMBER] = txtOfficePhone.Number;

            dataRow[nonIndividualDS.NonIndividualTable.OFFICECOUNTRYCODE2] = txtOfficePhone2.CountryCode;
            dataRow[nonIndividualDS.NonIndividualTable.OFFICECITYCODE2] = txtOfficePhone2.CityCode;
            dataRow[nonIndividualDS.NonIndividualTable.OFFICEPHONENUMBER2] = txtOfficePhone2.Number;

            dataRow[nonIndividualDS.NonIndividualTable.FACSIMILECOUNTRYCODE] = txtfacsimile.CountryCode;
            dataRow[nonIndividualDS.NonIndividualTable.FACSIMILECITYCODE] = txtfacsimile.CityCode;
            dataRow[nonIndividualDS.NonIndividualTable.FACSIMILEPHONENUMBER] = txtfacsimile.Number;

            dataRow[nonIndividualDS.NonIndividualTable.TFN] = TFN_InputControl1.GetEntity();
            dataRow[nonIndividualDS.NonIndividualTable.ABN] = ABN_InputControl1.GetEntity();
            dataRow[nonIndividualDS.NonIndividualTable.DESCRIPTION] = txtDescription.Text;

            dataRow[nonIndividualDS.NonIndividualTable.RESIDENTIALADDRESSLINE1] = txtResidentialAddress.AddressLine1;
            dataRow[nonIndividualDS.NonIndividualTable.RESIDENTIALADDRESSLINE2] = txtResidentialAddress.AddressLine2;
            dataRow[nonIndividualDS.NonIndividualTable.RESIDENTIALADDRESSSUBRUB] = txtResidentialAddress.Subrub;
            dataRow[nonIndividualDS.NonIndividualTable.RESIDENTIALADDRESSPOSTALCODE] = txtResidentialAddress.Postcode;
            dataRow[nonIndividualDS.NonIndividualTable.RESIDENTIALADDRESSCOUNTRY] = txtResidentialAddress.Country;
            dataRow[nonIndividualDS.NonIndividualTable.RESIDENTIALADDRESSSTATE] = txtResidentialAddress.State;

            dataRow[nonIndividualDS.NonIndividualTable.MAILINGADDRESSLINE1] = txtMailingAddress.AddressLine1;
            dataRow[nonIndividualDS.NonIndividualTable.MAILINGADDRESSLINE2] = txtMailingAddress.AddressLine2;
            dataRow[nonIndividualDS.NonIndividualTable.MAILINGADDRESSSUBRUB] = txtMailingAddress.Subrub;
            dataRow[nonIndividualDS.NonIndividualTable.MAILINGADDRESSPOSTALCODE] = txtMailingAddress.Postcode;
            dataRow[nonIndividualDS.NonIndividualTable.MAILINGADDRESSCOUNTRY] = txtMailingAddress.Country;
            dataRow[nonIndividualDS.NonIndividualTable.MAILINGADDRESSSTATE] = txtMailingAddress.State;
            
            dataRow[nonIndividualDS.NonIndividualTable.MAILINGADDRESSSAME] = chkSameAddress.Checked;

            dataTable.Rows.Add(dataRow);

            nonIndividualDS.Tables.Remove(nonIndividualDS.NonIndividualTable.TABLENAME);
            nonIndividualDS.Tables.Add(dataTable);

            return nonIndividualDS;
        }

        protected void chkSameAddress_OnCheckedChanged(object sender, EventArgs e)
        {
            if (chkSameAddress != null && chkSameAddress.Checked)
                txtMailingAddress.CopyFromAddressControl(txtResidentialAddress);
        }
    }
}