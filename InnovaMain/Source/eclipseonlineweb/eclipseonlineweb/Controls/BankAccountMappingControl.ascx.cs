﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;

namespace eclipseonlineweb.Controls
{
    public partial class BankAccountMappingControl : System.Web.UI.UserControl
    {
        private string _cid;
        protected bool IsAdviserOrClient
        {
            get;
            set;
        }

        protected ICMBroker UMABroker
        {
            get { return (Page as UMABasePage).UMABroker; }

        }

        public Action<DataSet> SaveOrg { get; set; }
        public Action<string, DataSet> SaveUnit { get; set; }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            #region Bank Account control
            BankControl.Saved += (CID, CLID, CSID) =>
            {
                HideShowBankGrid(false);
                AttachBankAccount(CID, CLID, CSID, DatasetCommandTypes.Update);
                PresentationGrid.Rebind();
            };
            BankControl.Canceled += () => HideShowBankGrid(false);
            BankControl.ValidationFailed += () => HideShowBankGrid(true);
            BankControl.SaveData += (bankCid, ds) =>
            {
                if (bankCid == Guid.Empty)
                {
                    if (SaveOrg != null)
                    {
                        SaveOrg(ds);
                    }
                }
                else
                {
                    if (SaveUnit != null)
                        SaveUnit(bankCid.ToString(), ds);
                }
                HideShowBankGrid(false);

            };
            #endregion

            #region add existing events
            BankAccountSearchControl.Canceled += () => OVER.Visible = BankModalExsiting.Visible = false;
            BankAccountSearchControl.ValidationFailed += () =>
            {
                OVER.Visible = BankModalExsiting.Visible = true;
            };

            BankAccountSearchControl.Saved += bankAccountDS =>
            {
                OVER.Visible = BankModalExsiting.Visible = false;
                SaveBankData(bankAccountDS, DatasetCommandTypes.Update);
            };
            #endregion
        }
        private void SaveBankData(BankAccountDS desktopBrokerAccountDs, DatasetCommandTypes commandType)
        {
            desktopBrokerAccountDs.CommandType = commandType;
            if (SaveUnit != null)
                SaveUnit(_cid, desktopBrokerAccountDs);

            PresentationGrid.Rebind();
        }
        private DataSet AttachBankAccount(Guid CID, Guid CLID, Guid CSID, DatasetCommandTypes commandType)
        {
            var ds = new BankAccountDS();
            DataRow dr = ds.BankAccountsTable.NewRow();
            dr[ds.BankAccountsTable.CLID] = CLID;
            dr[ds.BankAccountsTable.CSID] = CSID;
            ds.Tables[ds.BankAccountsTable.TABLENAME].Rows.Add(dr);

            SaveBankData(ds, commandType);

            PresentationGrid.Rebind();
            return ds;

        }
        private void HideShowBankGrid(bool show)
        {
            OVER.Visible = BankModal.Visible = show;
        }

        private void ShowHideExisting(bool show)
        {
            OVER.Visible = BankModalExsiting.Visible = show;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            _cid = Request.QueryString["ins"];
            if (!IsPostBack)
            {
                var objUser = (DBUser)UMABroker.GetBMCInstance(Page.User.Identity.Name, "DBUser_1_1");
                if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
                {
                    IsAdviserOrClient = true;
                    BankControl.IsAdviserOrClient = true;
                    PresentationGrid.Columns.FindByUniqueNameSafe("Institution").Visible = false;
                }
                UMABroker.ReleaseBrokerManagedComponent(objUser);
            }
        }
        private void GetData()
        {
            _cid = Request.QueryString["ins"];
            if (!string.IsNullOrEmpty(_cid))
            {
                IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(_cid));
                var ds = new BankAccountDS();
                clientData.GetData(ds);

                UMABroker.ReleaseBrokerManagedComponent(clientData);
                var summaryView = new DataView(ds.Tables[ds.BankAccountsTable.TABLENAME]) { Sort = ds.BankAccountsTable.ACCOUNTNAME + " DESC" };
                PresentationGrid.DataSource = summaryView;
            }
        }

        protected void LnkbtnAddBankAccClick(object sender, EventArgs e)
        {
            Guid gCid = Guid.Empty;
            BankControl.SetEntity(gCid, false);
            HideShowBankGrid(true);
        }

        protected void LnkbtnAddExistngBankClick(object sender, EventArgs e)
        {
            BankAccountSearchControl.DisableExitingGrid();
            ShowHideExisting(true);
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var bankCid = new Guid(dataItem["Cid"].Text);
                    //BankControl.SetEntity(bankCid, false);
                    //HideShowBankGrid(true);
               
                    string url = "~/SysAdministration/AccountDetails.aspx?ins=" + bankCid + "&DelarGroup=" + _cid;

                    Response.Redirect(url);
                    break;
                case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var linkEntiyCID = new Guid(dataItem["Cid"].Text);
                    var linkEntiyClid = new Guid(dataItem["CLid"].Text);
                    var linkEntiyCsid = new Guid(dataItem["CSid"].Text);
                    DataSet ds = AttachBankAccount(linkEntiyCID, linkEntiyClid, linkEntiyCsid, DatasetCommandTypes.Delete);
                    PresentationGrid.Rebind();
                    ScriptManager.RegisterStartupScript(Page, GetType(), "MyScript", "alert('" + ds.ExtendedProperties["Message"] + "');", true);
                    break;
            }
        }
        public void RadGridVisibility (bool flage)
        {
            PresentationGrid.Visible = flage;
        }

        protected void PresentationGrid_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;

            if (IsAdviserOrClient && (!Convert.ToBoolean(dataItem["IsExternalAccount"].Text) || (Convert.ToBoolean(dataItem["IsExternalAccount"].Text) && dataItem["AccountType"].Text == "TERMDEPOSIT")))
            {
                var imgEdit = dataItem["EditColumn"].Controls[0] as ImageButton;
                var imgDelete = dataItem["DeleteColumn"].Controls[0] as ImageButton;
                if (imgEdit != null) imgEdit.Visible = false;
                if (imgDelete != null) imgDelete.Visible = false;
            }
        }


    }
}