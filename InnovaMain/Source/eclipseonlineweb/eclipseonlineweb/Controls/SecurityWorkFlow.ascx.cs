﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using Oritax.TaxSimp;
using System.Collections.Generic;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CM.Organization;
using System.Collections;
using Oritax.TaxSimp.CM.Organization.Data;
using System.Linq;

namespace eclipseonlineweb.Controls
{
    public partial class SecurityWorkFlow : UserControl
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindUserListData();
                
                AddUsersInIncludedList(inculdedUsers);
            }

            
        }
        private ICMBroker UMABroker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        private void BindUserListData()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance("Administrator", "DBUser_1_1");
            DBUserDetailsDS dBUserDetailsDS = new Oritax.TaxSimp.Security.DBUserDetailsDS();
            dBUserDetailsDS.UserList = true;
            objUser.GetData(dBUserDetailsDS);
            dBUserDetailsDS.Tables["UserDetail"].Columns.Add("UserTypeString");
            DataTable dt = dBUserDetailsDS.Tables["UserDetail"];
            BindUserInDropDown(dt);
        }
        private void BindUserInDropDown(DataTable dt)
        {
            cmbAvailable.Items.Clear();
            var dict = new Dictionary<string, string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dict.Add(dt.Rows[i]["CID"].ToString(), dt.Rows[i]["FirstName"].ToString() + " " + dt.Rows[i]["LastName"].ToString() + " (" + dt.Rows[i]["EmailAddress"].ToString() + "-" + System.Enum.Parse(typeof(UserType), dt.Rows[i]["UserType"].ToString()) + ")");
            }
            cmbAvailable.DataSource = dict.OrderBy(ss=>ss.Value);
            cmbAvailable.DataTextField = "Value";
            cmbAvailable.DataValueField = "Key";
            cmbAvailable.DataBind();

        }
        protected void btnAdd_OnClick(object sender, EventArgs e)
        {
            for (int i = cmbAvailable.Items.Count - 1; i >= 0; i--)
            {
                RadComboBoxItem item = cmbAvailable.Items[i];
                if (item.Selected || item.Checked)
                {
                    AddIncludedItem(item);
                }
            }

        }
        protected void btnAddAll_OnClick(object sender, EventArgs e)
        {
            //if (string.IsNullOrWhiteSpace(cmbAvailable.Text))
            //    return;
            string search = cmbAvailable.Text.ToLower();
            for (int i = cmbAvailable.Items.Count - 1; i >= 0; i--)
            {
                RadComboBoxItem item = cmbAvailable.Items[i];

                if (item.Text.ToLower().Contains(search))
                {
                    AddIncludedItem(item);
                }
            }
        }
        private void AddIncludedItem(RadComboBoxItem item)
        {
            item.Checked = false;
            string strText = item.Text;
            string strValue = item.Value;
            cmbAvailable.Items.Remove(item);
            cmbIncluded.Items.Add(new RadComboBoxItem(item.Text, item.Value));
        }
        protected void btnRemoveAll_OnClick(object sender, EventArgs e)
        {
            //if (string.IsNullOrWhiteSpace(cmbIncluded.Text))
            //    return;
            string search = cmbIncluded.Text.ToLower();
            for (int i = cmbIncluded.Items.Count - 1; i >= 0; i--)
            {
                RadComboBoxItem item = cmbIncluded.Items[i];
                if (item.Text.ToLower().Contains(search))
                {
                    RemoveIncludedItem(item);
                }
            }
        }
        protected void btnRemove_OnClick(object sender, EventArgs e)
        {
            for (int i = cmbIncluded.Items.Count - 1; i >= 0; i--)
            {
                RadComboBoxItem item = cmbIncluded.Items[i];
                if (item.Selected || item.Checked)
                {
                    RemoveIncludedItem(item);
                }
            }
        }
        private void RemoveIncludedItem(RadComboBoxItem item)
        {
            item.Checked = false;
            string strText = item.Text;
            string strValue = item.Value;
            cmbIncluded.Items.Remove(item);
            cmbAvailable.Items.Add(new RadComboBoxItem(item.Text, item.Value));
        }


        public List<Guid> inculdedUsers{get;set;}
      
        public List<Guid> InculdedUsers{
            get
            {
                if(inculdedUsers!=null)
                {
                return inculdedUsers;
                }
                else{
                List<Guid> users = new List<Guid>();
                for (int i = 0; i < cmbIncluded.Items.Count; i++)
                {
                    RadComboBoxItem item = cmbIncluded.Items[i];
                    users.Add(Guid.Parse(item.Value));
                }
                return users;
                }
            }

            set
            {
             
                    inculdedUsers=value;
                   AddUsersInIncludedList(value);
              
            }
        }

                private void AddUsersInIncludedList(List<Guid> value)
                {
                    if (value!=null&&cmbAvailable.Items.Count > 0)
                    foreach (Guid user in value)
                    {
                        cmbAvailable.SelectedValue = user.ToString();
                        if (cmbAvailable.SelectedItem != null)
                        {
                            AddIncludedItem(cmbAvailable.SelectedItem);
                        }
                    }
                }}
    }
