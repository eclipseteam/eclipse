﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminDashboard.ascx.cs"
    Inherits="eclipseonlineweb.Controls.AdminDashboard" %>
<%@ Register TagName="Alerts" TagPrefix="uc" Src="../JDash/Dashlets/Alerts.ascx" %>
<%@ Register TagName="Notifications" TagPrefix="uc" Src="~/JDash/Dashlets/Notifications.ascx" %>
<%@ Register TagName="BestRates" TagPrefix="uc" Src="../JDash/Dashlets/BestRates.ascx" %>
<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1Chart" Assembly="C1.Web.Wijmo.Controls.4, Version=4.0.20121.56, Culture=neutral, PublicKeyToken=9b75583953471eea" %>
<table width="100%">
    <tr>
        <td valign="top" style="width: 100%;">
            <div style="text-align: right; margin-right: 5px; margin-top: 5px;">
                <a href="#" onclick="ShowActionPanel();">
                    <%=GetAlertCount()%>
                    Active Alert(s)</a>&nbsp;|&nbsp; <a href="#" onclick="ShowNotificationPanel();">
                        <%=GetNotificationCount()%>
                        Active Notification(s)</a>
            </div>
            <div style="width: 100%;">
                <div id="dvAlerts">
                    <fieldset style="height: 300px">
                        <legend>ALERTS</legend>
                        <uc:Alerts runat="server" ID="Alerts2" />
                    </fieldset>
                </div>
                <div id="dvNotifications">
                    <fieldset style="height: 300px">
                        <legend>NOTIFICATIONS</legend>
                        <uc:Notifications runat="server" ID="Notifications" />
                    </fieldset>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td valign="top" style="width: 100%; display: none">
            <div>
                <fieldset>
                    <legend>Holdings (Dummy Data)</legend>
                    <div>
                        <table style="width: 100%; height: 275px">
                            <tr>
                                <td style="width: 50%">
                                    <telerik:RadHtmlChart runat="server" ID="RadHtmlChart1" Width="350px" Height="240px"
                                        Transitions="true" Skin="Metro">
                                        <Appearance>
                                            <FillStyle BackgroundColor="White"></FillStyle>
                                        </Appearance>
                                        <Legend>
                                            <Appearance BackgroundColor="White" Position="Left" Visible="true"></Appearance>
                                        </Legend>
                                        <PlotArea>
                                            <Appearance>
                                                <FillStyle BackgroundColor="White"></FillStyle>
                                            </Appearance>
                                            <Series>
                                                <telerik:PieSeries StartAngle="90">
                                                    <LabelsAppearance Position="Circle" DataFormatString="{0}">
                                                    </LabelsAppearance>
                                                    <TooltipsAppearance DataFormatString="{0}"></TooltipsAppearance>
                                                    <Items>
                                                        <telerik:SeriesItem BackgroundColor="#ff9900" Exploded="false" Name="Internet Explorer"
                                                            YValue="18.3"></telerik:SeriesItem>
                                                        <telerik:SeriesItem BackgroundColor="#cccccc" Exploded="false" Name="Firefox" YValue="35.8">
                                                        </telerik:SeriesItem>
                                                        <telerik:SeriesItem BackgroundColor="#999999" Exploded="false" Name="Chrome" YValue="38.3">
                                                        </telerik:SeriesItem>
                                                        <telerik:SeriesItem BackgroundColor="#666666" Exploded="false" Name="Safari" YValue="4.5">
                                                        </telerik:SeriesItem>
                                                        <telerik:SeriesItem BackgroundColor="#333333" Exploded="false" Name="Opera" YValue="2.3">
                                                        </telerik:SeriesItem>
                                                    </Items>
                                                </telerik:PieSeries>
                                            </Series>
                                        </PlotArea>
                                    </telerik:RadHtmlChart>
                                </td>
                                <td style="width: 50%">
                                    <c1:C1BarChart runat="server" Horizontal="false" ID="C1BarChart1" Height="240" Width="100%"
                                        Visible="false">
                                        <Legend Visible="true"></Legend>
                                        <Hint>
                                            <Content Function="hintContent" />
                                        </Hint>
                                    </c1:C1BarChart>
                                    <telerik:RadHtmlChart runat="server" ID="RadHtmlChart2" Transitions="true" Height="240px"
                                        Skin="Metro">
                                        <PlotArea>
                                            <Series>
                                                <telerik:ColumnSeries Name="Settled">
                                                    <TooltipsAppearance BackgroundColor="Orange" DataFormatString="{0}" />
                                                    <Items>
                                                        <telerik:SeriesItem YValue="15000" />
                                                    </Items>
                                                </telerik:ColumnSeries>
                                                <telerik:ColumnSeries Name="Un Settled">
                                                    <TooltipsAppearance BackgroundColor="Orange" DataFormatString="{0}" />
                                                    <Items>
                                                        <telerik:SeriesItem YValue="10000" />
                                                    </Items>
                                                </telerik:ColumnSeries>
                                                <telerik:ColumnSeries Name="Total">
                                                    <TooltipsAppearance BackgroundColor="Orange" DataFormatString="{0}" />
                                                    <Items>
                                                        <telerik:SeriesItem YValue="20000" />
                                                    </Items>
                                                </telerik:ColumnSeries>
                                            </Series>
                                            <XAxis AxisCrossingValue="0" MajorTickType="Outside" MinorTickType="Outside" Reversed="false"
                                                MajorTickSize="0" MinorTickSize="0">
                                                <Items>
                                                    <telerik:AxisItem LabelText="Holdings" />
                                                </Items>
                                                <LabelsAppearance DataFormatString="{0}" RotationAngle="0" />
                                                <MajorGridLines Color="#EFEFEF" Width="0" />
                                                <MinorGridLines Color="#F7F7F7" Width="0" />
                                            </XAxis>
                                            <YAxis AxisCrossingValue="0" MajorTickSize="0" MajorTickType="Outside" MinorTickSize="0"
                                                MinorTickType="Outside" MinValue="0" Reversed="false" Step="10000">
                                                <LabelsAppearance DataFormatString="{0}" RotationAngle="0" />
                                                <MajorGridLines Color="#EFEFEF" Width="0" />
                                                <MinorGridLines Color="#F7F7F7" Width="0" />
                                            </YAxis>
                                        </PlotArea>
                                        <Legend>
                                            <Appearance Position="Right" />
                                        </Legend>
                                    </telerik:RadHtmlChart>
                                </td>
                            </tr>
                        </table>
                    </div>
                </fieldset>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <table style="width: 100%; height: 300px; margin-top: 5px;">
                <tr>
                    <td valign="top" style="width: 75%; height: 275px">
                        <div>
                            <fieldset>
                                <legend>BEST RATES</legend>
                                <uc:BestRates runat="server" ID="BestRatesControl" />
                            </fieldset>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table style="width: 99%; height: 300px; margin-top: 5px;">
                <tr>
                    <td valign="top" style="width: 45%; height: 275px;">
                        <div>
                            <fieldset>
                                <legend>TECHNICAL DOCUMENTS</legend>
                                <div>
                                    Available Soon!
                                </div>
                            </fieldset>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<script type="text/javascript">
    var activeDiv = 1; //Managing div value for partial post back 1=Action, 2=Notification
    //Partial Postback handler
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
        if (activeDiv == 1) {
            $('#dvAlerts').show();
            $('#dvNotifications').hide();
        }
        if (activeDiv == 2) {
            $('#dvNotifications').show();
            $('#dvAlerts').hide();
        }
    });

    $(document).ready(function () {
        //Hide Nofication Panel at load to display one panel by default (Actions).
        $('#dvNotifications').hide();
        activeDiv = 1; //Managing div value for partial post back
    });

    function ShowActionPanel() {
        $('#dvAlerts').show();
        $('#dvNotifications').hide();
        activeDiv = 1; //Managing div value for partial post back
    }
    function ShowNotificationPanel() {
        $('#dvNotifications').show();
        $('#dvAlerts').hide();
        activeDiv = 2; //Managing div value for partial post back
    }
</script>
