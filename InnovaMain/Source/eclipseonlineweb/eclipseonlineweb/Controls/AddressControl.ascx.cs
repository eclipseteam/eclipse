﻿using System;
using System.Text;
using System.Web.UI;
using Oritax.TaxSimp.Extensions;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Utilities;
using Telerik.Web.UI;

namespace eclipseonlineweb.Controls
{
    public partial class AddressControl : UserControl
    {
        public Action<string> SameAddressAs
        {
            get;
            set;
        }

        public bool ShowFillAsCombo
        {
            get { return ddlSameAddress.Visible; }
            set { lblFillAS.Visible = btnClear.Visible = ddlSameAddress.Visible = value; }


        }
        public Action Clear { get; set; }
        public string Title
        {
            get { return lblTitle.Text; }
            set { lblTitle.Text = value; }
        }
        public string AddressLine1
        {
            get { return txtaddressline1.Text; }

        }
        public string AddressLine2
        {
            get { return txtaddressline2.Text; }

        }
        protected bool Mandatory
        {
            get { return bool.Parse(txtmandtory.Value); }
            set { txtmandtory.Value = value.ToString(); }
        }
        public string Subrub
        {
            get { return txtsuburb.Text; }

        }
        public string Postcode
        {
            get { return txtpostcode.Text; }

        }
        public string State
        {
            get
            {
                if (ComboState.SelectedItem != null) return ComboState.SelectedValue;
                return ComboState.Text;
            }
        }
        public string Country
        {
            get
            {
                if (ddlcountry.SelectedItem != null) return ddlcountry.SelectedValue;
                return ddlcountry.Text;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void ddlcountry_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            SetCountryState(ddlcountry, ComboState);
        }
        private void LoadControls()
        {
            ddlcountry.Items.Clear();
            ddlcountry.DataSource = CountriesChoiceListISO.NameValuePairs;
            ddlcountry.DataTextField = "Name";
            ddlcountry.DataValueField = "Value";
            ddlcountry.DataBind();
            var item = ddlcountry.Items.FindItemByText("Australia");
            if (item != null)
                item.Selected = true;

            if (ddlcountry.SelectedValue != null)
            {
                ComboState.Items.Clear();
                ComboState.DataSource = new States(ddlcountry.SelectedValue);
                ComboState.DataTextField = "Name";
                ComboState.DataValueField = "Code";
                ComboState.DataBind();
            }
        }
        public bool Validate()
        {
            lblMessages.Text = string.Empty;
            if (!Mandatory && txtaddressline1.Text.Trim().Length == 0 && txtsuburb.Text.Trim().Length == 0 && txtpostcode.Text.Trim().Length == 0)
            {
                //ClearErrors();
                return true;
            }

            var messages = new StringBuilder();

            bool tempResult = txtaddressline1.ValidateAlphaNumeric(1, 35);

            bool result = tempResult;
            AddMessageIn(tempResult, "Invalid Address Line 1 *", messages);

            tempResult = txtaddressline2.ValidateAlphaNumeric(0, 35);
            result = tempResult && result;
            AddMessageIn(tempResult, "Invalid Address Line 1", messages);

            tempResult = txtsuburb.ValidateAlphaNumeric(1, 20);
            result = tempResult && result;
            AddMessageIn(tempResult, "Invalid Suburb", messages);

            tempResult = txtpostcode.ValidateNumeric(1, 5);
            result = tempResult && result;
            AddMessageIn(tempResult, "Invalid Post Code", messages);

            //Setting Error messages for display
            lblMessages.Text = messages.ToString();
            return result;
        }
        private void AddMessageIn(bool result, string message, StringBuilder messages)
        {
            if (!result)
            {
                messages.Append(message + "<br/>");
            }
        }
        public void SetEntity(string addressLine1, string addressLine2, string suburb, string state, string postCode, string country)
        {
            ClearEntity();
            LoadControls();
            txtaddressline1.Text = addressLine1;
            txtaddressline2.Text = addressLine2;
            txtsuburb.Text = suburb;
            txtpostcode.Text = postCode;
            if (country == string.Empty)
            {
                var item = ddlcountry.Items.FindItemByText("Australia");
                if (item != null)
                    item.Selected = true;
            }
            else
                ddlcountry.SelectedValue = country;

            SetCountryState(ddlcountry, ComboState);
            ComboState.SelectedValue = AusStates.ReturnShortStateCodeFromLong(state);
        }
        public void SetEntity()
        {
            ClearEntity();
            LoadControls();
            var item = ddlcountry.Items.FindItemByText("Australia");
            if (item != null)
                item.Selected = true;
        }

        private void SetCountryState(RadComboBox country, RadComboBox state)
        {
            state.DataSource = new States(country.SelectedValue);
            state.DataTextField = "Name";
            state.DataValueField = "Code";
            state.DataBind();
            if (state.Items.Count > 0)
            {
                state.SelectedIndex = 0;
            }
        }

        public void ClearEntity()
        {
            txtaddressline1.Text = "";
            txtaddressline2.Text = "";
            txtsuburb.Text = "";
            ComboState.SelectedIndex = -1;
            txtpostcode.Text = "";
            ddlcountry.SelectedIndex = -1;
        }
        public void ResetEntity()
        {
            txtaddressline1.Text = "";
            txtaddressline2.Text = "";
            txtsuburb.Text = "";
            ComboState.SelectedIndex = -1;
            txtpostcode.Text = "";
            ddlSameAddress.SelectedIndex = -1;
        }
        protected void ddlSameAddress_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SameAddressAs != null)
            {
                SameAddressAs(ddlSameAddress.SelectedValue);
            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            ddlcountry.Items.FindItemByText("Australia").Selected = true;
            SetCountryState(ddlcountry, ComboState);
            ResetEntity();
        }
        public void CopyFromAddressControl(AddressControl addressControl)
        {
            SetEntity(addressControl.AddressLine1, addressControl.AddressLine2, addressControl.Subrub, addressControl.State, addressControl.Postcode, addressControl.Country);
        }
    }
}