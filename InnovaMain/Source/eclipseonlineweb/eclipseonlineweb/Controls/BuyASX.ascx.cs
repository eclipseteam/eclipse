﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using Aspose.Words.Tables;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;
using System.Web.UI.WebControls;
using Aspose.Words;
using System.Drawing;
using Table = Aspose.Words.Tables.Table;

namespace eclipseonlineweb.Controls
{
    public partial class BuyASX : UserControl
    {
        private DataView _securitiesView;

        public string ClientCID
        {
            private get
            {
                return hfClientCID.Value;
            }
            set
            {
                hfClientCID.Value = value;
            }
        }

        private string ClientAccID
        {
            get
            {
                return hfClientID.Value;
            }
            set
            {
                hfClientID.Value = value;
            }
        }

        public bool IsAdmin
        {
            private get
            {
                return Convert.ToBoolean(hfIsAdmin.Value);
            }
            set
            {
                hfIsAdmin.Value = value.ToString();
            }
        }

        public bool IsAdminMenu
        {
            private get
            {
                if (string.IsNullOrEmpty(hfIsAdminMenu.Value))
                {
                    hfIsAdminMenu.Value = "false";
                }
                return Convert.ToBoolean(hfIsAdminMenu.Value);
            }
            set
            {
                hfIsAdminMenu.Value = value.ToString();
            }
        }

        private bool IsSMA
        {
            get
            {
                if (string.IsNullOrEmpty(hfIsSMA.Value))
                {
                    hfIsSMA.Value = "false";
                }
                return Convert.ToBoolean(hfIsSMA.Value);
            }
            set
            {
                hfIsSMA.Value = value.ToString();
            }
        }

        public Action<string, DataSet> SaveData { get; set; }

        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            searchAccountControl.SelectData += (clientCID, clientId, clientName) =>
            {
                ClientHeaderInfo1.SetEntity(new Guid(clientCID));
                ClientHeaderInfo1.Visible = true;
                ClientCID = clientCID;
                ClientAccID = clientId;
                SetClientManagementType();
                LoadControls();
                showDetails.Visible = true;
                ViewState["SecurityTable"] = null;
                PresentationGrid.Rebind();
                PresentationGrid.Visible = true;
                tdButtons.Visible = true;
            };

            searchAccountControl.ShowHideModal += (show) =>
            {
                if (show)
                {
                    popupSearch.Show();
                }
                else
                {
                    popupSearch.Hide();
                }
            };

            lblMsg.Visible = false;

            if (!IsPostBack)
            {
                if (IsAdminMenu)
                {
                    btnSearch.Visible = true;
                    showDetails.Visible = false;
                    ClientHeaderInfo1.Visible = false;
                    PresentationGrid.Visible = false;
                    tdButtons.Visible = false;
                    searchAccountControl.IsAdminMenu = true;
                }
                else
                {
                    btnSearch.Visible = false;
                    showDetails.Visible = true;
                    SetClientManagementType();
                    LoadControls();
                    ClientHeaderInfo1.Visible = true;
                    PresentationGrid.Visible = true;
                    tdButtons.Visible = true;
                    searchAccountControl.IsAdminMenu = false;
                }
                hfIsAlert.Value = string.Empty;
                ViewState["SecurityTable"] = null;
            }
            divTitleNote.InnerText = Tooltip.OrderPadManualASXBuy;
        }

        private void GetASXListedSecurities()
        {
            var organization = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var ds = new SecuritiesDS();
            if (organization != null)
            {
                organization.GetData(ds);
                _securitiesView = ds.Tables[SecuritiesDS.SECLIST].DefaultView;
                //Filtering ASX Listed Securities
                string filter = string.Format("{0} = '{1}'", SecuritiesDS.MARKET, "ASX");
                if (IsSMA)
                {
                    filter += string.Format(" and {0} = {1}", SecuritiesDS.ISSMAAPPROVED, true);
                }
                _securitiesView.RowFilter = filter;
                _securitiesView.Sort = string.Format("{0} ASC", SecuritiesDS.CODE);
                Broker.ReleaseBrokerManagedComponent(organization);
            }
        }

        private HoldingRptDataSet GetData()
        {
            IBrokerManagedComponent clientData = Broker.GetBMCInstance(new Guid(ClientCID));
            var ds = new HoldingRptDataSet();
            clientData.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(clientData);
            return ds;
        }

        private void BindServiceType()
        {
            //Getting Service type according to user type
            var ds = GetData();
            OrderPadUtilities.FillServiceTypes(cmbServiceType, ds, IsAdmin, OrderAccountType.ASX);
        }

        private void BindBankAccounts()
        {
            //Getting cash bank account according to service type
            var ds = GetData();
            string filter = string.Format("{0}='{1}' and {2}='{3}' and {4}='cash'", ds.HoldingSummaryTable.LINKEDENTITYTYPE, OrganizationType.BankAccount, ds.HoldingSummaryTable.SERVICETYPE, cmbServiceType.SelectedValue, ds.HoldingSummaryTable.ASSETNAME);
            OrderPadUtilities.FillBankNAvailableFunds(cmbCashAccount, ds, filter, OrderAccountType.ASX, OrderItemType.Buy, cmbServiceType.SelectedValue, IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA, Broker);
            OrderPadUtilities.SetAvailableFunds(cmbCashAccount, lblAvailableFunds, lblCashBalance, lblMinCash, IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA);
        }

        private void LoadControls()
        {
            BindServiceType();
            BindBankAccounts();
            BindASXListedSecurities();
        }

        private void BindASXListedSecurities()
        {
            GetASXListedSecurities();
            //clearing items
            cmbSecurity.Items.Clear();
            cmbSecurity.Items.Insert(0, new RadComboBoxItem { Text = "--Select--", Value = "0" });
            ClearSecurtiyItems();
            foreach (DataRowView rowView in _securitiesView)
            {
                var item = new RadComboBoxItem
                {
                    Text = string.Format("{0}-{1}", rowView.Row[SecuritiesDS.CODE], rowView.Row[SecuritiesDS.DESCRIPTION]),
                    Value = rowView.Row[SecuritiesDS.ID].ToString()
                };

                item.Attributes.Add("InvestmentCode", rowView.Row[SecuritiesDS.CODE].ToString());
                item.Attributes.Add("InvestmentName", rowView.Row[SecuritiesDS.DESCRIPTION].ToString());
                item.Attributes.Add("Currency", rowView.Row[SecuritiesDS.CURRENCY].ToString());
                item.Attributes.Add("UnitPrice", rowView.Row[SecuritiesDS.UNITPRICE].ToString());
                item.Attributes.Add("SMAHoldingLimit", rowView.Row[SecuritiesDS.SMAHOLDINGLIMIT].ToString());
                cmbSecurity.Items.Add(item);
                item.DataBind();
            }
        }

        private void ClearSecurtiyItems()
        {
            cmbSecurity.SelectedIndex = 0;
            lblSMAAvailableHolding.Text =
            lblSMAHoldingLimit.Text =
            lblSMATotalHolding.Text =
            lblCurrencyCode.Text =
            lblUnitPrice.Text =
            txtAmount.Text = string.Empty;
        }

        protected void PresentationGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (ViewState["SecurityTable"] != null)
            {
                PresentationGrid.DataSource = ViewState["SecurityTable"];
            }
        }

        protected void cmbCashAccount_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            OrderPadUtilities.SetAvailableFunds(cmbCashAccount, lblAvailableFunds, lblCashBalance, lblMinCash, IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA);
        }

        protected void cmbServiceType_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindBankAccounts();
            PresentationGrid.Rebind();
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            popupSearch.Show();
            showDetails.Visible = false;
            ClientHeaderInfo1.Visible = false;
            PresentationGrid.Visible = false;
            tdButtons.Visible = false;
            searchAccountControl.HideModal = false;
            searchAccountControl.FindControl("SearchBox").Focus();
        }

        protected void btnCutOff_OnClick(object sender, EventArgs e)
        {
            FileHelper.DownLoadFile(FileHelper.CutOffTimeFilePath, FileHelper.CutOffTimeFileName);
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            lblMsg.Visible = false;
            lblMsg.Text = "";
            //Validate funds
            if (!ValidateAmount())
            {
                lblMsg.Visible = true;
                return;
            }
            //Checking Existing Order CM ID
            Guid orderCMCID = OrderPadUtilities.IsOrderCmExists(Broker, (Page as UMABasePage).GetCurrentUser());
            var unit = new OrganizationUnit
            {
                Name = "Order " + DateTime.Now.ToString("dd MMM, yyyy hh:mm:ss"),
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = (Page as UMABasePage).GetCurrentUser()
            };
            var ds = new OrderPadDS
            {
                CommandType = DatasetCommandTypes.Add,
                Unit = unit,
                Command = (int)WebCommands.AddNewOrganizationUnit
            };
            //Add rows in order pad ds
            AddRows(ds);
            if (SaveData != null)
            {
                SaveData(orderCMCID.ToString(), ds);
            }
            lblMsg.Visible = true;
            lblMsg.Text = ds.ExtendedProperties["Message"].ToString();
            if (ds.ExtendedProperties["Result"].ToString() == OperationResults.Successfull.ToString())
            {
                hfIsAlert.Value = string.Empty;
                ViewState["SecurityTable"] = null;
                BindBankAccounts();
                PresentationGrid.Rebind();
                string path = CreateReceipt(ds);
                lblMsg.Text = string.Format("{0} Click <a href = '../SysAdministration/DownloadOrderReceipt.aspx?filename={1}'>here</a> to print its receipt.", lblMsg.Text, path);
                if (ds.ExtendedProperties.Contains("SMAMessages"))
                {
                    var results = (Dictionary<long, string>)ds.ExtendedProperties["SMAMessages"];
                    if (results.Count > 0)
                    {
                        var messag = string.Empty;
                        foreach (KeyValuePair<long, string> result in results)
                        {
                            messag += string.Format("Order: {0},{2}{1}{2}", result.Key, result.Value, Environment.NewLine);
                        }
                        string smaErrorFile = GetNewFilePath("txt");
                        Utilities.SaveToFile(messag, smaErrorFile);

                        lblMsg.Text += string.Format("To view Super service errors click <a href = '../SysAdministration/DownloadOrderReceipt.aspx?filename={0}'>Here</a> ", FileHelper.GetFileName(smaErrorFile));
                        ShowAlert("ShowServiceErrors", "There were errors sending data to Super Service.");
                    }
                }
            }
        }

        private void AddCellToSheet(DocumentBuilder builderExl, string text, double width, Color backColor, bool isFontBold, int fontSize, int lineWidth, CellVerticalAlignment vAlign, TextOrientation orientation, ParagraphAlignment paragraph)
        {
            //Header Start
            builderExl.InsertCell();
            builderExl.CellFormat.PreferredWidth = PreferredWidth.FromPercent(width);
            builderExl.CellFormat.Shading.BackgroundPatternColor = backColor;
            builderExl.CellFormat.Borders.LineWidth = lineWidth;
            builderExl.Bold = isFontBold;
            builderExl.Font.Bold = isFontBold;
            builderExl.Font.Size = fontSize;
            builderExl.Font.Color = Color.Black;
            builderExl.CurrentParagraph.ParagraphFormat.Alignment = paragraph;
            builderExl.Write(text);
        }

        private string CreateReceipt(OrderPadDS ds)
        {
            double sum = 0;
            DataTable dts = ds.Tables[ds.ASXTable.TABLENAME];
            DataTable dt_ = dts.DefaultView.ToTable(false, "InvestmentCode", "InvestmentName", "Units", "Amount");
            DataRow clientSummaryRow = GetData().Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0];

            string OutPath = string.Empty;
            string path = Server.MapPath("~/Templates/OrderPad_BuyASX_Template2.docx");
            Aspose.Words.Document document = null;
            document = new Aspose.Words.Document(path);

            //This is portion for the Header Section of the Document
            document.Range.Replace("[Eclipse_OrderNo]", string.Format("Order {0}", lblMsg.Text.Split(' ')[1]), false, false);
            document.Range.Replace("[Eclipe_Date]", DateTime.Now.ToString("dd/MM/yyyy"), false, false);
            document.Range.Replace("[Eclipse_Order_By]", ((Page as UMABasePage).GetCurrentUser()).CurrentUserName, false, false);
            document.Range.Replace("[Eclipse_Investor_Name]", clientSummaryRow[HoldingRptDataSet.CLIENTNAME].ToString(), false, false);
            document.Range.Replace("[Eclipse_Client_ID]", ClientHeaderInfo1.ClientId ?? ClientAccID, false, false);
            document.Range.Replace("[Eclipse_Account_Type]", clientSummaryRow[HoldingRptDataSet.CLIENTTYPE].ToString(), false, false);
            document.Range.Replace("[Eclipse_Adviser]", clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME].ToString(), false, false);
            document.Range.Replace("[Eclipse_Service_Type]", cmbServiceType.SelectedItem.Text.ToUpper(), false, false);
            document.Range.Replace("[A11]", clientSummaryRow[HoldingRptDataSet.CLIENTNAME].ToString(), false, false);
            string addressLine1And2 = string.Empty;
            if (clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2].ToString() == string.Empty)
                addressLine1And2 = clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1].ToString();
            else
                addressLine1And2 = clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1].ToString() + ", " + clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2].ToString();
            document.Range.Replace("[A12]", addressLine1And2, false, false);
            document.Range.Replace("[A13]", string.Format("{0} {1} {2}", clientSummaryRow[HoldingRptDataSet.SUBURB], clientSummaryRow[HoldingRptDataSet.STATE], clientSummaryRow[HoldingRptDataSet.POSTCODE]), false, false);
            document.Range.Replace("[A14]", clientSummaryRow[HoldingRptDataSet.COUNTRY].ToString(), false, false);
            if (document != null)
            {
                OutPath = GetNewFilePath("PDF");
                //Now here we try to Make Dynamic Table
                DocumentBuilder builderExl = new DocumentBuilder(document);
                //This is Point where to Start Generate Table
                builderExl.MoveToBookmark("TestBookMarked", false, true);

                Table table = builderExl.StartTable();
                builderExl.PageSetup.LeftMargin = ConvertUtil.InchToPoint(1);
                builderExl.PageSetup.TopMargin = ConvertUtil.InchToPoint(1);
                builderExl.PageSetup.BottomMargin = ConvertUtil.InchToPoint(1);

                builderExl.StartTable();

                builderExl.InsertCell();
                builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                builderExl.CellFormat.Borders.LineWidth = 0;
                builderExl.EndRow();
                builderExl.EndTable();
                builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Left;
                builderExl.Writeln("");

                AddCellToSheet(builderExl, "Investment Code", 5, Color.DarkGray, true, 9, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                AddCellToSheet(builderExl, "Investment Name", 45, Color.DarkGray, true, 9, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                AddCellToSheet(builderExl, "Estimated Shares", 15, Color.DarkGray, true, 9, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, "Amount", 15, Color.DarkGray, true, 9, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                builderExl.EndRow();
                for (int i = 0; i < dt_.Rows.Count; i++)
                {
                    AddCellToSheet(builderExl, dt_.Rows[i]["InvestmentCode"].ToString(), 5, Color.White, false, 9, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                    AddCellToSheet(builderExl, dt_.Rows[i]["InvestmentName"].ToString(), 45, Color.White, false, 9, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                    AddCellToSheet(builderExl, dt_.Rows[i]["Units"].ToString(), 15, Color.White, false, 9, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                    AddCellToSheet(builderExl, string.Format("{0:C}", dt_.Rows[i]["Amount"]), 15, Color.White, false, 9, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                    builderExl.EndRow();
                    sum += Convert.ToDouble(dt_.Rows[i]["Amount"]);
                }
                //Here we add the Total Colunm
                AddCellToSheet(builderExl, "Total", 5, Color.White, true, 9, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                AddCellToSheet(builderExl, string.Empty, 45, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                AddCellToSheet(builderExl, string.Empty, 15, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, string.Format("{0:C}", sum), 15, Color.White, false, 9, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                builderExl.EndRow();
                table.PreferredWidth = PreferredWidth.FromPercent(70);
                // table.AutoFit(AutoFitBehavior.AutoFitToContents);
                document.Save(OutPath);

            }
            return FileHelper.GetFileName(OutPath);
        }

        private string GetNewFilePath(string ext)
        {
            string fileName = string.Format("{0}_{1}_BuyASX_{2}.{3}", ClientHeaderInfo1.ClientId ?? ClientAccID, lblMsg.Text.Split(' ')[1], DateTime.Now.ToShortDateString().Replace("/", "-"), ext);
            string dirOrderReceipts = string.Format("{0}\\OrderReceipts", HttpContext.Current.Server.MapPath("~/App_Data"));
            if (!Directory.Exists(dirOrderReceipts))
                Directory.CreateDirectory(dirOrderReceipts);
            string filePath = string.Format("{0}\\{1}", dirOrderReceipts, fileName);
            return filePath;
        }

        private bool ValidateAmount()
        {
            decimal totalAmount = 0;
            decimal totalUnits = 0;
            foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
            {
                decimal amount;
                decimal.TryParse(dataItem["OrderAmount"].Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out amount);
                if (amount > 0)
                {
                    totalAmount += amount;
                }
                decimal units;
                decimal.TryParse(dataItem["OrderUnits"].Text, out units);
                if (units > 0)
                {
                    totalUnits += units;
                }
            }
            if (totalAmount == 0 && totalUnits == 0)
            {
                lblMsg.Text = "Please add items to buy";
                return false;
            }

            decimal availableFunds;
            decimal.TryParse(lblAvailableFunds.Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out availableFunds);

            if (totalAmount > availableFunds)
            {
                lblMsg.Text = "Insufficient Available Funds";
                return false;
            }
            return true;
        }

        private void AddRows(OrderPadDS ds)
        {
            //Order Row
            var orderId = AddOrderRow(ds);

            foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
            {
                decimal amount;
                decimal.TryParse(dataItem["OrderAmount"].Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out amount);

                decimal units;
                decimal.TryParse(dataItem["OrderUnits"].Text, out units);

                if (amount > 0 && units > 0)
                {
                    //Order Items Row
                    AddItemRow(ds, units, amount, dataItem, orderId);
                }
            }
        }

        private Guid AddOrderRow(OrderPadDS ds)
        {
            DataRow orderRow = ds.OrdersTable.NewRow();

            Guid orderId = Guid.NewGuid();
            orderRow[ds.OrdersTable.ID] = orderId;
            orderRow[ds.OrdersTable.CLIENTCID] = new Guid(ClientCID);
            orderRow[ds.OrdersTable.CLIENTID] = ClientHeaderInfo1.ClientId ?? ClientAccID;
            orderRow[ds.OrdersTable.ORDERACCOUNTTYPE] = OrderAccountType.ASX;
            orderRow[ds.OrdersTable.ORDERTYPE] = OrderType.Manual;
            orderRow[ds.OrdersTable.STATUS] = OrderStatus.Active;
            orderRow[ds.OrdersTable.ACCOUNTCID] = new Guid(cmbCashAccount.SelectedValue);
            orderRow[ds.OrdersTable.ACCOUNTNUMBER] = cmbCashAccount.SelectedItem.Attributes["AccountNo"].Trim();
            orderRow[ds.OrdersTable.ACCOUNTBSB] = cmbCashAccount.SelectedItem.Attributes["BSB"].Trim();
            orderRow[ds.OrdersTable.ORDERITEMTYPE] = OrderItemType.Buy;
            orderRow[ds.OrdersTable.CLIENTMANAGEMENTTYPE] = IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA;

            ds.OrdersTable.Rows.Add(orderRow);
            return orderId;
        }

        private void AddItemRow(OrderPadDS ds, decimal units, decimal amount, GridDataItem dataItem, Guid orderId)
        {
            DataRow asxItemsRow = ds.ASXTable.NewRow();

            asxItemsRow[ds.ASXTable.ID] = Guid.NewGuid();
            asxItemsRow[ds.ASXTable.ORDERID] = orderId;
            asxItemsRow[ds.ASXTable.ASXUNITPRICE] = dataItem["UnitPrice"].Text;
            asxItemsRow[ds.ASXTable.UNITS] = units;
            asxItemsRow[ds.ASXTable.AMOUNT] = amount;
            asxItemsRow[ds.ASXTable.INVESTMENTCODE] = dataItem["Security"].Text;
            asxItemsRow[ds.ASXTable.INVESTMENTNAME] = dataItem["InvestmentName"].Text;
            ds.ASXTable.Rows.Add(asxItemsRow);
            GetProductId(ds);
        }

        private void GetProductId(OrderPadDS ds)
        {
            //Getting ASXCID and ASXAccountNumber and ProductID
            IBrokerManagedComponent clientData = Broker.GetBMCInstance(new Guid(ClientCID));
            if (ds.ExtendedProperties.Count >= 0 && ds.ExtendedProperties["ServiceType"] == null &&
                ds.ExtendedProperties["IsBuy"] == null)
            {
                ds.ExtendedProperties.Add("ServiceType", cmbServiceType.SelectedValue);
                ds.ExtendedProperties.Add("IsBuy", "true");
            }
            clientData.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void cmbSecurity_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (cmbSecurity.SelectedValue == "0")
            {
                ClearSecurtiyItems();
            }
            if (cmbSecurity.SelectedItem != null)
            {
                var unitPrice = cmbSecurity.SelectedItem.Attributes["UnitPrice"];
                lblCurrencyCode.Text = cmbSecurity.SelectedItem.Attributes["Currency"];
                lblUnitPrice.Text = unitPrice;
                cvMinAmount.ValueToCompare = unitPrice;
                cvAmount.Enabled = IsSMA;
                //Getting Available Security Holding Amount
                if (IsSMA)
                {
                    decimal secLimitPercent = Convert.ToDecimal(cmbSecurity.SelectedItem.Attributes["SMAHoldingLimit"]);
                    decimal availableFunds;
                    decimal.TryParse(lblAvailableFunds.Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out availableFunds);
                    var investmentCode = cmbSecurity.SelectedItem.Attributes["InvestmentCode"];
                    var holdingDs = GetData();
                    decimal secHoldingLimit, secCurrentHolding, secAvailableFund;
                    OrderPadUtilities.GetSecurityAvaiableFundsForSMA(holdingDs, investmentCode, secLimitPercent, Convert.ToDecimal(unitPrice), OrganizationType.DesktopBrokerAccount, out secHoldingLimit, out secCurrentHolding, out secAvailableFund);
                    lblSMAAvailableHolding.Text = secAvailableFund.ToString("C");
                    cvAmount.ValueToCompare = secAvailableFund.ToString();
                    lblSMAHoldingLimit.Text = secHoldingLimit.ToString("C");
                    lblSMATotalHolding.Text = secCurrentHolding.ToString("C");
                }
            }
        }

        protected void btnAdd_OnClick(object sender, EventArgs e)
        {
            CreateSecurityData();
        }

        private void CreateSecurityData()
        {
            DataTable dt;
            if (ViewState["SecurityTable"] == null)
            {
                dt = new DataTable("Security");
                dt.Columns.Add("SecurityId", typeof(string));
                dt.Columns.Add("Security", typeof(string));
                dt.Columns.Add("InvestmentName", typeof(string));
                dt.Columns.Add("Currency", typeof(string));
                dt.Columns.Add("UnitPrice", typeof(decimal));
                dt.Columns.Add("OrderUnits", typeof(decimal));
                dt.Columns.Add("OrderAmount", typeof(decimal));
                dt.Columns.Add("HoldingLimit", typeof(decimal));
                dt.Columns.Add("TotalHolding", typeof(decimal));
                dt.Columns.Add("AvailableFunds", typeof(decimal));
                dt.Columns.Add("ProductId", typeof(string));
                dt.Columns.Add("AsxAccNo", typeof(string));
                dt.Columns.Add("AsxAccCid", typeof(string));
            }
            else
            {
                dt = (DataTable)ViewState["SecurityTable"];
                var drs = dt.Select(string.Format("Security='{0}'", cmbSecurity.SelectedItem.Attributes["InvestmentCode"]));
                if (drs.Length > 0)
                {
                    lblMsg.Text = string.Format("Security '{0}' is already in the list", cmbSecurity.SelectedItem.Attributes["InvestmentCode"]);
                    lblMsg.Visible = true;
                    return;
                }
            }

            // Validating that unit price is not '0'
            if (lblUnitPrice.Text == "0")
            {
                lblMsg.Text = "Cannot add to list as the Unit Price is zero";
                lblMsg.Visible = true;
                return;
            }

            var dr = dt.NewRow();
            dr["SecurityId"] = cmbSecurity.SelectedValue;
            dr["Security"] = cmbSecurity.SelectedItem.Attributes["InvestmentCode"];
            dr["InvestmentName"] = cmbSecurity.SelectedItem.Attributes["InvestmentName"];
            dr["Currency"] = cmbSecurity.SelectedItem.Attributes["Currency"];
            dr["UnitPrice"] = cmbSecurity.SelectedItem.Attributes["UnitPrice"];
            decimal units = 0;
            if (txtAmount.Text.Length > 0)
            {
                decimal amount = Convert.ToDecimal(txtAmount.Text);
                decimal unitPrice = Convert.ToDecimal(lblUnitPrice.Text);
                // Dont need to check for devided by zero because it is already validated above
                units = decimal.Floor(amount / unitPrice);
            }
            dr["OrderUnits"] = units;
            dr["OrderAmount"] = txtAmount.Text;
            if (IsSMA)
            {
                decimal holdingLimit;
                decimal.TryParse(lblSMAHoldingLimit.Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out holdingLimit);
                decimal totalHolding;
                decimal.TryParse(lblSMATotalHolding.Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out totalHolding);
                decimal availableFunds;
                decimal.TryParse(lblSMAAvailableHolding.Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out availableFunds);
                dr["HoldingLimit"] = holdingLimit;
                dr["TotalHolding"] = totalHolding;
                dr["AvailableFunds"] = availableFunds;
            }
            //Getting ASXCID and ASXAccountNumber and ProductId
            var ds = new OrderPadDS();
            DataRow asxItemsRow = ds.ASXTable.NewRow();
            asxItemsRow[ds.ASXTable.INVESTMENTCODE] = cmbSecurity.SelectedItem.Attributes["InvestmentCode"];
            ds.ASXTable.Rows.Add(asxItemsRow);
            GetProductId(ds);

            dr["ProductId"] = ds.ASXTable.Rows[0][ds.ASXTable.PRODUCTID];
            dr["AsxAccNo"] = ds.ASXTable.Rows[0][ds.ASXTable.ASXACCOUNTNO];
            dr["AsxAccCid"] = ds.ASXTable.Rows[0][ds.ASXTable.ASXACCOUNTCID];
            if (string.IsNullOrWhiteSpace(hfIsAlert.Value))
            {
                if (string.IsNullOrEmpty(dr["ProductId"].ToString()))
                    hfIsAlert.Value = dr["Security"].ToString();
            }

            dt.Rows.Add(dr);
            ViewState["SecurityTable"] = dt;
            ClearSecurtiyItems();
            //rebind grid
            PresentationGrid.Rebind();
            PresentationGrid.Visible = true;
            PresentationGrid.Columns.FindByUniqueNameSafe("HoldingLimit").Visible =
            PresentationGrid.Columns.FindByUniqueNameSafe("TotalHolding").Visible =
            PresentationGrid.Columns.FindByUniqueNameSafe("AvailableFunds").Visible = IsSMA;
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            var dt = (DataTable)ViewState["SecurityTable"];

            switch (e.CommandName.ToLower())
            {
                case "update":
                    DataRow dr = dt.Rows[e.Item.ItemIndex];
                    var editForm = (GridEditFormItem)e.Item;

                    var txtUnitPrice = (TextBox)editForm["UnitPrice"].Controls[0];
                    var txtAmount = (RadNumericTextBox)editForm["OrderAmount"].Controls[0];

                    decimal amount = Convert.ToDecimal(txtAmount.Text);
                    decimal unitPrice = Convert.ToDecimal(txtUnitPrice.Text);
                    decimal units = unitPrice > 0 ? decimal.Floor(amount / unitPrice) : 0;

                    if (amount < unitPrice)
                    {
                        ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "showAmountAlert", "alert('Amount must be greater than equal to Unit Price.');", true);
                        break;
                    }

                    if (IsSMA)
                    {
                        decimal holdingLimit;
                        decimal.TryParse(dr["AvailableFunds"].ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out holdingLimit);

                        if (amount > holdingLimit)
                        {
                            ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "showHoldingLimit", "alert('Amount exceeds Holding Limit.');", true);
                            break;
                        }
                    }

                    dr["OrderUnits"] = units;
                    dr["OrderAmount"] = txtAmount.Text;

                    e.Item.Edit = false;
                    e.Canceled = true;
                    ViewState["SecurityTable"] = dt;
                    PresentationGrid.Rebind();
                    break;
                case "delete":
                    dt.Rows.RemoveAt(e.Item.ItemIndex);
                    ViewState["SecurityTable"] = dt;
                    PresentationGrid.Rebind();
                    break;
            }
        }

        public void ShowAlert(string key, string message)
        {

            ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), key, string.Format("alert('{0}');", message), true);
        }

        protected void PresentationGrid_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridEditableItem) && (e.Item.IsInEditMode))
            {
                var editForm = (GridEditableItem)e.Item;
                var update = (LinkButton)editForm.FindControl("UpdateButton");
                var txtAmount = (RadNumericTextBox)editForm["OrderAmount"].Controls[0];
                if (update != null)
                {
                    update.Attributes.Add("onclick", "javascript:return Validate('" + txtAmount.ClientID + "');");
                }
            }
        }

        private void SetClientManagementType()
        {
            IsSMA = SMAUtilities.IsSMAClient(new Guid(ClientCID), Broker);
            tdHoldingLimitLabel.Visible =
            tdHoldingLimitValue.Visible =
            tdTotalHoldingLabel.Visible =
            tdTotalHoldingValue.Visible =
            tdAvailableFundsLabel.Visible =
            tdAvailableFundsValue.Visible =
            tdCashBalanceHead.Visible =
            tdCashBalanceValue.Visible =
            tdMinCashHead.Visible =
            tdMinCashValue.Visible = IsSMA;
            tdFundsHead.Style["width"] = IsSMA ? "125px" : "100%";
        }
    }
}