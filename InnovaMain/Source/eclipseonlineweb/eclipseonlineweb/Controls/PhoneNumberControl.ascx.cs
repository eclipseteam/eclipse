﻿using Oritax.TaxSimp.Extensions;

namespace eclipseonlineweb.Controls
{
    public partial class PhoneNumberControl : System.Web.UI.UserControl
    {
        public bool Mandatory { get; set; }

        public string CountryCode
        {
            get { return txtCountryCode.Text; }
            set { txtCountryCode.Text = (value == null) ? string.Empty : value.Replace(" ", ""); }
        }
        public string CityCode
        {
            get { return txtCityCode.Text; }
            set
            {

                txtCityCode.Text = (value == null) ? string.Empty : value.Replace(" ", "");
            }
        }
        public string Number
        {
            get { return txtNumber.Text; }
            set { txtNumber.Text = (value == null) ? string.Empty : value.Replace(" ",""); }
        }
        public void ClearEntity()
        {
            CountryCode = string.Empty;
            CityCode = string.Empty;
            Number = string.Empty;
            Msg.Text = string.Empty;
        }
        public void ClearErrors ()
        {
            //txtCityCode.ClearControlError();
            //txtCountryCode.ClearControlError();
            //txtNumber.ClearControlError();
            Msg.Text = string.Empty;
        }
        public bool Validate()
        {
            if (!Mandatory && txtCountryCode.Text.Length == 0 && txtCityCode.Text.Length == 0 && txtNumber.Text.Length == 0)
            {
                ClearErrors();
                return true;
            }

            if (txtCountryCode.ValidateNumeric(1, 3) && txtCityCode.ValidateNumeric(1, 4) && txtNumber.ValidateNumeric(1, 20))
            {
                ClearErrors();
                return true;
            }
            Msg.Text="Invalid Number (e.g. 99 9999 99999)";
            return false;
        }

    }
}