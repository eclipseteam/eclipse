﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.Export;
using eclipseonlineweb.WebUtilities;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb.Controls
{
    public partial class PendingOrders : UserControl
    {
        private DataView _ordersView;
        private DataView _ssOrdersView;
        private DataView _asxOrdersView;
        private DataView _tdOrdersView;
        private DataView _atCallOrdersView;
        private bool IsFiltered
        {
            get
            {
                return !string.IsNullOrEmpty(hfIsFiltered.Value) && Convert.ToBoolean(hfIsFiltered.Value);
            }
            set
            {
                hfIsFiltered.Value = value.ToString();
            }
        }
        private string Filter
        {
            get { return hfFilter.Value; }
            set { hfFilter.Value = value; }
        }

        public Action<string, DataSet> SaveData { get; set; }

        public bool IsAdmin
        {
            private get
            {
                return Convert.ToBoolean(hfIsAdmin.Value);
            }
            set
            {
                hfIsAdmin.Value = value.ToString();
            }
        }

        public bool IsAdminMenu
        {
            private get
            {
                if (string.IsNullOrEmpty(hfIsAdminMenu.Value))
                {
                    hfIsAdminMenu.Value = "false";
                }
                return Convert.ToBoolean(hfIsAdminMenu.Value);
            }
            set
            {
                hfIsAdminMenu.Value = value.ToString();
            }
        }

        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        private string OrdersCacheKey = "Orders_";
        private OrderPadDS Orderds
        {
            get
            {
                OrderPadDS _temps;
                var key = OrdersCacheKey + Session.SessionID;
                if (Cache[key] != null)
                {
                    _temps = Cache[key] as OrderPadDS;
                }
                else
                {
                    _temps = new OrderPadDS();
                    Cache[key] = _temps;
                }

                return _temps;
            }
            set
            {
                string key = OrdersCacheKey + Session.SessionID;

                Cache[key] = value;
            }
        }
        private void GetOrders(bool loadData)
        {

            if (loadData)
                Orderds = GetOrderPadDs();

            _ordersView = Orderds.Tables[Orderds.OrdersTable.TableName].DefaultView;
            _ssOrdersView = Orderds.Tables[Orderds.StateStreetTable.TableName].DefaultView;
            _asxOrdersView = Orderds.Tables[Orderds.ASXTable.TableName].DefaultView;
            _tdOrdersView = Orderds.Tables[Orderds.TermDepositTable.TableName].DefaultView;
            _atCallOrdersView = Orderds.Tables[Orderds.AtCallTable.TableName].DefaultView;
            //_settledUnsettledView = ds.Tables[SettledUnsetteledList.TABLENAME].DefaultView;

            if (IsFiltered)
            {
                _ordersView.RowFilter = Filter;
            }
            //sort by order id/number desc 
            _ordersView.Sort = string.Format("{0} DESC", Orderds.OrdersTable.ORDERID);
            PresentationGrid.DataSource = _ordersView;
            PresentationGrid.Columns.FindByUniqueName("ClientID").Visible = IsAdminMenu;


            GridChildLoadMode loadmode = GridChildLoadMode.Client;
            if (PresentationGrid.PageSize > 30)
            {
                loadmode = GridChildLoadMode.ServerOnDemand;
            }

            PresentationGrid.MasterTableView.HierarchyLoadMode = loadmode;
            foreach (GridTableView gridTableView in PresentationGrid.MasterTableView.DetailTables)
            {
                gridTableView.HierarchyLoadMode = loadmode;
            }
        }

        private void GetData()
        {
            var orderUnit = new OrganizationUnit
            {
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = (Page as UMABasePage).GetCurrentUser()
            };

            if (!IsAdminMenu)
            {
                orderUnit.ClientId = ClientHeaderInfo1.ClientId;
            }

            //Orders Lists
            var ds = new OrderPadDS
            {
                Unit = orderUnit,
                CommandType = DatasetCommandTypes.Get,
                Command = (int)WebCommands.GetOrganizationUnitsByType,
            };
            if (Request.QueryString["orderId"] != null)
            {
                Guid orderId;
                Guid.TryParse(Request.QueryString["orderId"], out orderId);
                if (orderId != Guid.Empty)
                {
                    ds.OrderId = orderId;
                }
            }

            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            //Orders Lists
            org.GetData(ds);

            Broker.ReleaseBrokerManagedComponent(org);
            ((UMABasePage)Page).PresentationData = ds;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClientHeaderInfo1.Visible = !IsAdminMenu;
                btnSMA.Visible = btnUMA.Visible = IsAdminMenu;
                SetClientManagementType();
                LoadControls();
                btnExportOrders.Visible = btnExportAllOrders.Visible = IsAdminMenu & IsAdmin;
                dtTradeDate.SelectedDate = DateTime.Now;
                btnShowExportedOrders.Visible = IsAdminMenu & IsAdmin;
                btnBulkCancel.Visible = btnBulkApprove.Visible = btnExcel.Visible = IsAdmin;
                PresentationGrid.Columns.FindByUniqueNameSafe("BulkCancel").Visible = IsAdmin;
            }
        }

        private void BindStatusCombo(string itemStatus)
        {
            cmbStatus.Items.Clear();
            OrderStatus orderStatus;
            bool isValidOrderStatus = Enum.TryParse(itemStatus, out orderStatus);
            if (isValidOrderStatus)
            {
                //filling status combo for manual update
                var statuses = Enum.GetNames(typeof(OrderStatus));
                foreach (var status in statuses)
                {
                    if ((OrderStatus)Enum.Parse(typeof(OrderStatus), status) < orderStatus)
                    {
                        cmbStatus.Items.Add(new RadComboBoxItem(status));
                    }
                }
            }
        }

        private void LoadControls()
        {
            //Status List binding
            var orderStatus = Enum.GetNames(typeof(OrderStatus));
            foreach (var status in orderStatus)
            {
                if (status != OrderStatus.Cancelled.ToString())
                {
                    if (status != OrderStatus.Complete.ToString())
                    {
                        lstStatus.Items.Add(new RadListBoxItem(status));
                    }
                }
            }
            lstStatus.DataBind();

            foreach (RadListBoxItem item in lstStatus.Items)
            {
                item.Checked = true;
                item.Font.Bold = true;
            }

            ShowCheckedItems(lstStatus, true);
        }

        protected void PresentationGridDetailTableDataBind(object source, GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = e.DetailTableView.ParentItem;
            string orderID = dataItem["ID"].Text;

            if (dataItem.OwnerTableView.Columns.FindByUniqueNameSafe("OrderAccountType") != null)
            {
                OrderAccountType orderAccountType;
                bool isValidOrderAccountType = Enum.TryParse(dataItem["OrderAccountType"].Text, out orderAccountType);

                OrderItemType orderItemType;
                bool isValidOrderItemType = Enum.TryParse(dataItem["OrderItemType"].Text, out orderItemType);

                if (isValidOrderAccountType)
                {
                    string filter = "OrderId='" + orderID + "'";
                    switch (orderAccountType)
                    {
                        case OrderAccountType.TermDeposit:
                            switch (e.DetailTableView.Name)
                            {
                                case "TDOrderDetails":
                                    {
                                        e.DetailTableView.DataSource = _tdOrdersView;
                                        _tdOrdersView.RowFilter = filter;

                                        if (isValidOrderItemType)
                                        {
                                            switch (orderItemType)
                                            {
                                                case OrderItemType.Sell:
                                                    e.DetailTableView.GetColumn("Min").Visible = false;
                                                    e.DetailTableView.GetColumn("Max").Visible = false;
                                                    e.DetailTableView.GetColumn("Rating").Visible = false;
                                                    e.DetailTableView.GetColumn("Duration").Visible = false;
                                                    e.DetailTableView.GetColumn("Percentage").Visible = false;
                                                    break;
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    {
                                        e.DetailTableView.Visible = false;
                                    }
                                    break;
                            }
                            break;
                        case OrderAccountType.StateStreet:
                            switch (e.DetailTableView.Name)
                            {
                                case "SSOrderDetails":
                                    {
                                        e.DetailTableView.DataSource = _ssOrdersView;
                                        _ssOrdersView.RowFilter = filter;
                                        if (isValidOrderItemType)
                                        {
                                            switch (orderItemType)
                                            {
                                                case OrderItemType.Buy:
                                                    e.DetailTableView.GetColumn("Units").Visible = false;
                                                    break;
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    {
                                        e.DetailTableView.Visible = false;
                                    }
                                    break;
                            }
                            break;
                        case OrderAccountType.ASX:
                            switch (e.DetailTableView.Name)
                            {
                                case "ASXOrderDetails":
                                    {
                                        e.DetailTableView.DataSource = _asxOrdersView;
                                        _asxOrdersView.RowFilter = filter;
                                    }
                                    break;
                                default:
                                    {
                                        e.DetailTableView.Visible = false;
                                    }
                                    break;
                            }
                            break;
                        case OrderAccountType.AtCall:
                            switch (e.DetailTableView.Name)
                            {
                                case "AtCallOrderDetails":
                                    {
                                        e.DetailTableView.DataSource = _atCallOrdersView;
                                        _atCallOrdersView.RowFilter = filter;
                                    }
                                    break;
                                default:
                                    {
                                        e.DetailTableView.Visible = false;
                                    }
                                    break;
                            }
                            break;
                    }
                }
            }

            if (e.DetailTableView.Name == "")
            {

            }
        }

        protected void PresentationGridItemCommand(object sender, GridCommandEventArgs e)
        {
            Guid orderId;
            switch (e.CommandName.ToLower())
            {
                case "cancel":
                    hfIsBulkCancel.Value = false.ToString();
                    hfOrderID.Value = e.CommandArgument.ToString();
                    popupComments.Show();
                    rfComments.Enabled = true;
                    break;
                case "approve":
                    orderId = Guid.Parse(e.CommandArgument.ToString());
                    ExecuteCommand(DatasetCommandTypes.Approve, orderId);
                    break;
                case "statusupdate":
                    hfIsBulkUpdate.Value = false.ToString();
                    hfOrderID.Value = e.CommandArgument.ToString();
                    var item = (GridDataItem)e.Item;
                    var status = ((HyperLink)item.FindControl("hypLinkStatus")).Text;
                    BindStatusCombo(status);
                    popupStatus.Show();
                    break;
                case "complete":
                    orderId = Guid.Parse(e.CommandArgument.ToString());
                    ExecuteCommand(DatasetCommandTypes.Complete, orderId);
                    break;
            }
            //Collapse All Other Expanded Items
            if (e.CommandName == RadGrid.ExpandCollapseCommandName)
            {
                foreach (GridItem item in e.Item.OwnerTableView.Items)
                {
                    if (item.Expanded && item != e.Item)
                    {
                        item.Expanded = false;
                    }
                }
            }
        }

        protected void PresentationGridItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem && e.Item.OwnerTableView.Name == "AtCallOrderDetails")
            {
                var item = (GridDataItem)e.Item;
                if (item["AtCallType"].Text == AtCallType.MoneyMovement.ToString())
                {
                    item.OwnerTableView.GetColumn("BrokerName").Visible = false;
                    item.OwnerTableView.GetColumn("BankName").Visible = false;
                    item.OwnerTableView.GetColumn("Min").Visible = false;
                    item.OwnerTableView.GetColumn("Max").Visible = false;
                    item.OwnerTableView.GetColumn("Rate").Visible = false;
                    item.OwnerTableView.GetColumn("AccountTier").Visible = false;
                }
                else if (item["AtCallType"].Text == AtCallType.BestRates.ToString() || item["AtCallType"].Text == AtCallType.MoneyMovementBroker.ToString())
                {
                    item.OwnerTableView.GetColumn("TransferTo").Visible = false;
                    item.OwnerTableView.GetColumn("TransferAccBSB").Visible = false;
                    item.OwnerTableView.GetColumn("TransferAccNumber").Visible = false;
                    if (item["AtCallType"].Text == AtCallType.MoneyMovementBroker.ToString())
                    {
                        var headerItem = item.OwnerTableView.GetItems(GridItemType.Header)[0] as GridHeaderItem;
                        if (headerItem != null) headerItem["AccountTier"].Text = "Accounts";
                        item.OwnerTableView.GetColumn("Min").Visible = false;
                        item.OwnerTableView.GetColumn("Max").Visible = false;
                        item.OwnerTableView.GetColumn("Rate").Visible = false;
                    }
                }
            }

            if (!(e.Item is GridDataItem) || e.Item.OwnerTableView.Name != "ActiveOrders") return;

            var dataItem = (GridDataItem)e.Item;
            var orderId = dataItem["ID"].Text;
            var hyLinkStatus = (HyperLink)e.Item.FindControl("hypLinkStatus");
            var SMARequestlabel = dataItem["SMARequestID"].Controls[1] as Label;
            var hlnkSMATransactionIDRefresh = dataItem["SMARequestID"].Controls[3] as LinkButton;
            hlnkSMATransactionIDRefresh.Attributes.Add("OrderID", orderId);
            var SMACallStatustxt = dataItem["SMACallStatus"].Text;
            bool isSMA = btnSMA.Checked;

            var hypLinkClientId = dataItem["ClientID"].Controls[0] as HyperLink;

            if (hyLinkStatus.Text == OrderStatus.Submitted.ToString() && orderId != string.Empty)
            {
                if (IsAdminMenu)
                {
                    if (hypLinkClientId != null && hypLinkClientId.Text != string.Empty)
                    {
                        hyLinkStatus.NavigateUrl = string.Format("~/SysAdministration/SettledUnsettledTransactions.aspx?ClientId={0}&OrderId={1}&Page=1", hypLinkClientId.Text, orderId);
                    }
                }
                else
                {
                    if (Request.QueryString["ins"] != null)
                    {
                        hyLinkStatus.NavigateUrl = string.Format("~/ClientViews/SettledUnsettledTransactions.aspx?ins={0}&OrderId={1}&Page=1", Request.QueryString["ins"], orderId);
                    }
                }
            }
            else
            {
                hyLinkStatus.Font.Underline = false;
            }

            if (IsAdmin)
            {
                dataItem.OwnerTableView.GetColumn("SMACallStatus").Visible = dataItem.OwnerTableView.GetColumn("SMARequestID").Visible = isSMA;
                if (isSMA)
                {
                    var showlink = (SMACallStatustxt == SMACallStatus.Failed.ToString());
                    hlnkSMATransactionIDRefresh.Visible = showlink;
                    if (SMARequestlabel != null) SMARequestlabel.Visible = !showlink;
                }
            }
            else
            {
                dataItem.OwnerTableView.GetColumn("SMACallStatus").Visible = dataItem.OwnerTableView.GetColumn("SMARequestID").Visible = false;
            }

            OrderStatus orderStatus;
            bool isValidOrderStatus = Enum.TryParse(hyLinkStatus.Text, out orderStatus);
            OrderBulkStatus orderBulkStatus;
            if (!Enum.TryParse(dataItem["OrderBulkStatus"].Text, out orderBulkStatus))
            {
                orderBulkStatus = OrderBulkStatus.None;
            }

            if (isValidOrderStatus)
            {
                var imgCancel = dataItem.FindControl("imgCancel") as ImageButton;
                var chkCancel = dataItem.FindControl("chkCancel") as CheckBox;

                OrderPadUtilities.SetStatusToolTip(dataItem, orderStatus);
                if (orderStatus >= OrderStatus.Submitted || orderBulkStatus >= OrderBulkStatus.BulkTrade)
                {
                    if (imgCancel != null) imgCancel.Visible = false;
                    if (chkCancel != null) chkCancel.Visible = false;
                }
                else if (orderStatus == OrderStatus.ValidationFailed)
                {
                    dataItem.ForeColor = Color.Red;
                    if (string.IsNullOrEmpty(dataItem["ValidationMsg"].Text.Trim().Replace("&nbsp;", "")))
                    {
                        dataItem["ValidationMsg"].Text = "Drill down to see validation messages";
                    }
                }
                //checkbox for export should be visible
                if (orderStatus < OrderStatus.Submitted && orderBulkStatus == OrderBulkStatus.Complete)
                {
                    if (chkCancel != null) chkCancel.Visible = true;
                }

                if (IsAdmin)
                {
                    var orderAccountType = dataItem["OrderAccountType"].Text;
                    var orderType = dataItem["OrderType"].Text;

                    if (orderStatus == OrderStatus.Validated && !((orderAccountType == OrderAccountType.ASX.ToString() || (orderAccountType == OrderAccountType.TermDeposit.ToString() && btnSMA.Checked)) && orderType == OrderType.Manual.ToString()))
                    {
                        var imgApprove = dataItem.FindControl("imgApprove") as ImageButton;
                        if (imgApprove != null)
                        {
                            imgApprove.Visible = true;
                        }
                    }

                    if (orderStatus > OrderStatus.Active && orderBulkStatus < OrderBulkStatus.BulkTrade)
                    {
                        var imgUpdate = dataItem.FindControl("imgUpdate") as ImageButton;
                        if (imgUpdate != null) imgUpdate.Visible = true;
                    }

                    if (isSMA && orderStatus == OrderStatus.Submitted && orderAccountType == OrderAccountType.TermDeposit.ToString())
                    {
                        var imgComplete = dataItem.FindControl("imgComplete") as ImageButton;
                        if (imgComplete != null)
                        {
                            imgComplete.Visible = true;
                        }
                    }

                    //Allow Cancellation/Update of Order for SMA, MIS in Submitted Status
                    if (isSMA && orderAccountType == OrderAccountType.StateStreet.ToString() && orderStatus == OrderStatus.Submitted)
                    {
                        if (imgCancel != null) imgCancel.Visible = true;
                        if (chkCancel != null) chkCancel.Visible = true;
                    }
                    //Allow Cancellation/Update of Order for UMA, MIS/TD/At Call/ASX-FinSimplicity in Submitted Status
                    else if (!isSMA && orderStatus == OrderStatus.Submitted && (orderAccountType == OrderAccountType.StateStreet.ToString() || orderAccountType == OrderAccountType.TermDeposit.ToString() || orderAccountType == OrderAccountType.AtCall.ToString() || (orderAccountType == OrderAccountType.ASX.ToString() && orderType == OrderType.FinSimplicity.ToString())))
                    {
                        if (imgCancel != null) imgCancel.Visible = true;
                        if (chkCancel != null) chkCancel.Visible = true;
                    }
                }
            }
        }

        protected void PresentationGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetOrders(!e.IsFromDetailTable);
        }

        protected void btnApplyFilter_OnClick(object sender, EventArgs e)
        {
            ShowCheckedItems(lstStatus, true);
        }

        protected void btnApproved_OnClick(object sender, EventArgs e)
        {
            ApproveClientOrders();
        }

        private void ApproveClientOrders()
        {
            bool isApproved = false;
            foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
            {
                var status = ((HyperLink)dataItem.FindControl("hypLinkStatus")).Text;
                var chkCancel = dataItem.FindControl("chkCancel") as CheckBox;
                if (chkCancel != null && chkCancel.Visible && chkCancel.Checked && status == OrderStatus.Validated.ToString())
                {
                    Guid id = Guid.Parse(dataItem["ID"].Text);
                    ExecuteCommand(DatasetCommandTypes.Approve, id, false);
                    isApproved = true;
                }
            }
            // refresh grid
            PresentationGrid.Rebind();
            if (isApproved)
            {
                ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "showalertSuccess", "alert('Orders approved successfully.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "showalertFail", "alert('No orders could be approved.');", true);
            }
        }

        private void ShowCheckedItems(RadListBox listBox, bool isFilter)
        {
            foreach (RadListBoxItem item in lstStatus.Items)
            {
                if (!item.Checked)
                    item.Font.Bold = false;
            }
            var sb = new StringBuilder();
            IList<RadListBoxItem> collection = listBox.CheckedItems;
            foreach (RadListBoxItem item in collection)
            {
                sb.Append("'" + item.Value + "', ");
            }

            string filterStatus = "'Empty'";
            if (sb.Length > 0)
            {
                filterStatus = sb.Remove(sb.ToString().Trim().Length - 1, 1).ToString();
            }

            if (isFilter)
            {
                IsFiltered = true;
                string typeFilter = string.Format("ClientManagementType = '{0}'", btnSMA.Checked ? btnSMA.Value : btnUMA.Value);
                Filter = string.Format("{0} and Status in ({1})", typeFilter, filterStatus);
                if (IsPostBack)
                    PresentationGrid.Rebind();
            }
        }

        protected void btnValidate_OnClick(object sender, EventArgs e)
        {
            ValidateClientOrders();
        }

        private void ValidateClientOrders()
        {
            ExecuteCommand(DatasetCommandTypes.Validate);
        }

        private void ExecuteCommand(DatasetCommandTypes command, Guid id = default(Guid), bool refreshGrid = true)
        {
            if (Page.IsValid)
            {
                var unit = new OrganizationUnit
                {
                    Type = ((int)OrganizationType.Order).ToString(),
                    CurrentUser = (Page as UMABasePage).GetCurrentUser()
                };

                if (!IsAdminMenu)
                {
                    unit.ClientId = ClientHeaderInfo1.ClientId;
                }

                var ds = new OrderPadDS { CommandType = command, Unit = unit };

                if (command == DatasetCommandTypes.Validate)
                {
                    ds.ClientManagementType = btnSMA.Checked ? ClientManagementType.SMA : ClientManagementType.UMA;
                }

                if (id != Guid.Empty)
                {
                    DataRow dr = ds.OrdersTable.NewRow();
                    dr[ds.OrdersTable.ID] = id;
                    if (command == DatasetCommandTypes.Cancel)
                    {
                        dr[ds.OrdersTable.COMMENTS] = txtComments.Text.Trim();
                        //disable validator
                        rfComments.Enabled = false;
                    }
                    else if (command == DatasetCommandTypes.Update)
                    {
                        dr[ds.OrdersTable.STATUS] = cmbStatus.SelectedItem.Text;
                    }
                    ds.OrdersTable.Rows.Add(dr);
                }

                Guid orderCMCID = IsOrderCmExists();

                if (orderCMCID != Guid.Empty)
                {
                    if (SaveData != null)
                    {
                        SaveData(orderCMCID.ToString(), ds);
                        ShowSMAMessage(ds);
                    }
                }

                if (refreshGrid)
                {
                    // refresh grid
                    PresentationGrid.Rebind();
                }
            }
        }

        private Guid IsOrderCmExists()
        {
            var unit = new OrganizationUnit
            {
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = (Page as UMABasePage).GetCurrentUser()
            };

            var ds = new OrderPadDS
            {
                CommandType = DatasetCommandTypes.Check,
                Unit = unit,
                Command = (int)WebCommands.GetOrganizationUnitsByType
            };

            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            org.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(org);

            Guid orderCMCID = Guid.Empty;
            if (ds.OrdersTable.Rows.Count > 0)
            {
                //Getting ORDER CID 
                orderCMCID = new Guid(ds.OrdersTable.Rows[0][ds.OrdersTable.CID].ToString());
            }

            return orderCMCID;
        }

        protected void ShowHideDetailClick(object sender, EventArgs e)
        {
            var lbBtn = sender as LinkButton;
            if (lbBtn != null)
            {
                var item = (GridDataItem)(lbBtn).NamingContainer;
                item.Expanded = !item.Expanded;
            }
        }

        protected void btnSaveComments_OnClick(object sender, EventArgs e)
        {
            if (hfIsBulkCancel.Value == false.ToString())
            {
                Guid id = Guid.Parse(hfOrderID.Value);
                ExecuteCommand(DatasetCommandTypes.Cancel, id);
            }
            else if (hfIsBulkCancel.Value == true.ToString())
            {
                foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
                {
                    var chkCancel = dataItem.FindControl("chkCancel") as CheckBox;
                    if (chkCancel != null && chkCancel.Visible && chkCancel.Checked)
                    {
                        Guid id = Guid.Parse(dataItem["ID"].Text);
                        ExecuteCommand(DatasetCommandTypes.Cancel, id, false);
                    }
                }
                // refresh grid
                PresentationGrid.Rebind();
            }
            //clear comments
            txtComments.Text = String.Empty;
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            if (hfIsBulkUpdate.Value == false.ToString())
            {
                Guid id = Guid.Parse(hfOrderID.Value);
                ExecuteCommand(DatasetCommandTypes.Update, id);
            }
            else if (hfIsBulkUpdate.Value == true.ToString())
            {
                foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
                {
                    var chkCancel = dataItem.FindControl("chkCancel") as CheckBox;
                    if (chkCancel != null && chkCancel.Visible && chkCancel.Checked)
                    {
                        Guid id = Guid.Parse(dataItem["ID"].Text);
                        ExecuteCommand(DatasetCommandTypes.Update, id, false);
                    }
                }
                // refresh grid
                PresentationGrid.Rebind();
            }
        }

        private void ExportOrders(List<Guid> orderIDs)
        {
            var clientManagementType = btnSMA.Checked ? ClientManagementType.SMA : ClientManagementType.UMA;
            bool isExported = false;
            try
            {
                var expCommand = ExportFactory.GetExportCommand(Export.ExportType.InstructionsExport);
                if (expCommand != null)
                {
                    expCommand.Broker = Broker;
                    expCommand.User = (Page as UMABasePage).GetCurrentUser();
                    expCommand.ClientManagementType = clientManagementType;
                    if (expCommand is IHasOrdersFilter)
                    {
                        (expCommand as IHasOrdersFilter).OrdersContianingOnly = orderIDs;
                        (expCommand as IHasOrdersFilter).TradeDate = dtTradeDate.SelectedDate.ToString();
                    }

                    var data = expCommand.Export();
                    bool isEmpty = true;
                    foreach (DataTable dt in data.Tables)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            isEmpty = false;
                        }
                    }
                    if (!isEmpty)
                    {
                        if (clientManagementType == ClientManagementType.UMA)
                        {
                            byte[] bytes = expCommand.GetByteArray(data);
                            if (bytes.Length > 0)
                            {
                                string path = GetPath();
                                bool isExists = System.IO.Directory.Exists(path);
                                if (!isExists)
                                    System.IO.Directory.CreateDirectory(path);
                                string fileName = string.Format("{0}_{1}_{2}.{3}", Export.ExportType.InstructionsExport,
                                                                DateTime.Now.ToString("yyyy_MM_dd"),
                                                                DateTime.Now.ToString("hhmmssFFF"), expCommand.FileType);
                                path = path + fileName;
                                System.IO.File.WriteAllBytes(path, bytes);
                            }
                        }
                        //updating orders status
                        data.ExtendedProperties["TradeDate"] = dtTradeDate.SelectedDate;
                        OrderPadUtilities.UpdateExportStatus(data as ExportDS, Broker, (Page as UMABasePage).GetCurrentUser());

                        isExported = true;

                        string message = "Orders exported successfully.";
                        if (data.ExtendedProperties.Contains("SMAMessages"))
                        {
                            message += "But couldn't send to Super service.Please view exported files";
                        }
                        //
                        ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "ShowAlert",
                                                            string.Format("alert('{0}')", message), true);


                        PresentationGrid.Rebind();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "ShowAlert",
                                                            "alert('There is no approved orders to export.')", true);
                    }
                    ShowSMAMessage(data);
                }
            }
            catch (Exception ex)
            {
                if (!isExported)
                {
                    ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "ShowAlert",
                                                        "alert('Error exporting orders.')", true);
                }
                PresentationGrid.Rebind();
                Utilities.LogException(ex, Request, Context);
            }
        }

        private List<Guid> GetSelectedApprovedOrderIDs()
        {
            var orderIDs = new List<Guid>();

            foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
            {
                var status = ((HyperLink)dataItem.FindControl("hypLinkStatus")).Text;
                var chkCancel = dataItem.FindControl("chkCancel") as CheckBox;
                if (chkCancel != null && chkCancel.Visible && chkCancel.Checked && status == OrderStatus.Approved.ToString())
                {
                    Guid id = Guid.Parse(dataItem["ID"].Text);
                    orderIDs.Add(id);
                }
            }
            return orderIDs;
        }

        private List<Guid> GetAllApprovedOrderIDs()
        {
            var orderIDs = new List<Guid>();

            foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
            {
                var status = ((HyperLink)dataItem.FindControl("hypLinkStatus")).Text;
                if (status == OrderStatus.Approved.ToString())
                {
                    Guid id = Guid.Parse(dataItem["ID"].Text);
                    orderIDs.Add(id);
                }
            }
            return orderIDs;
        }

        protected void btnExportedOrdersFiles_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~\\SysAdministration\\DownloadExportFiles.aspx?type=" + Export.ExportType.InstructionsExport, false);
        }

        protected void btnBulkCancel_OnClick(object sender, EventArgs e)
        {
            hfIsBulkCancel.Value = true.ToString();
            popupComments.Show();
            rfComments.Enabled = true;
        }

        protected void btnExcel_OnClick(object sender, EventArgs e)
        {
            var ds = GetOrderPadDs();
            DataView dataView = ds.OrdersTable.DefaultView;
            if (IsFiltered)
            {
                dataView.RowFilter = Filter;
            }
            //sort by order id/number desc 
            dataView.Sort = string.Format("{0} DESC", ds.OrdersTable.ORDERID);

            var excelDataset = new DataSet();
            DataTable dt = dataView.ToTable();
            foreach (DataColumn col in ds.OrdersTable.Columns)
            {
                if (col.DataType == typeof(Guid))
                {
                    dt.Columns.Remove(col.ColumnName);
                }
            }
            dt.Columns.Remove(ds.OrdersTable.FILENAME);
            dt.Columns.Remove(ds.OrdersTable.ATTACHMENT);
            excelDataset.Merge(dt, true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, string.Format("ActiveOrders{0}.xls", DateTime.Now.ToFileTime()), Page.Response);
        }

        private OrderPadDS GetOrderPadDs()
        {
            if (!Page.IsPostBack || ((UMABasePage)Page).PresentationData == null)
            {
                GetData();
            }
            var ds = ((UMABasePage)Page).PresentationData as OrderPadDS;
            return ds;
        }

        protected void btnUMA_OnClick(object sender, EventArgs e)
        {
            ShowCheckedItems(lstStatus, true);
        }

        protected void btnSMA_OnClick(object sender, EventArgs e)
        {
            ShowCheckedItems(lstStatus, true);
        }

        private string GetPath()
        {
            string path = Server.MapPath("~\\" + ConfigurationManager.AppSettings["ExportInstructionsPath"]);
            if (!path.EndsWith("\\"))
                path += "\\";
            if (btnSMA.Checked)
            {
                path += "SMA\\";
            }
            return path;
        }

        private void SetClientManagementType()
        {
            if (!IsAdminMenu && Request.QueryString["ins"] != null)
            {
                var clientCid = Request.Params["ins"].Replace("?ins=", string.Empty);
                btnSMA.Checked = SMAUtilities.IsSMAClient(new Guid(clientCid), Broker);
            }

            if (IsAdminMenu)
            {
                if (Request.QueryString["type"] != null)
                {
                    var type = Request.QueryString["type"];
                    ClientManagementType clientType;
                    Enum.TryParse(type, true, out clientType);
                    if (clientType == ClientManagementType.SMA)
                    {
                        btnSMA.Checked = true;
                    }
                }
            }
        }

        protected void hlnkSMARequestIDRefresh_OnClick(object sender, EventArgs e)
        {
            var unit = new OrganizationUnit
            {
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = (Page as UMABasePage).GetCurrentUser()
            };

            if (!IsAdminMenu)
            {
                unit.ClientId = ClientHeaderInfo1.ClientId;
            }
            var link = sender as LinkButton;

            var ds = new OrderPadDS { CommandType = DatasetCommandTypes.SendOrderToSMA, Unit = unit, OrderId = Guid.Parse(link.Attributes["OrderID"]) };
            ds.ClientManagementType = btnSMA.Checked ? ClientManagementType.SMA : ClientManagementType.UMA;

            Guid orderCMCID = IsOrderCmExists();

            if (orderCMCID != Guid.Empty)
            {
                if (SaveData != null)
                {
                    SaveData(orderCMCID.ToString(), ds);
                    ShowSMAMessage(ds);
                    PresentationGrid.Rebind();
                }
            }

        }

        private void ShowSMAMessage(DataSet ds)
        {
            if (ds.ExtendedProperties.Contains("SMAMessages"))
                ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "ShowAlert", "alert('There was error in sending data to Super Trade service.Please check export Log file from exported files  ')", true);
        }

        protected void btnBulkUpdate_OnClick(object sender, EventArgs e)
        {
            hfIsBulkUpdate.Value = true.ToString();
            BindStatusCombo(OrderStatus.Submitted.ToString());
            popupStatus.Show();
        }

        protected void btnOK_OnClick(object sender, EventArgs e)
        {
            ExportOrders(hfIsExportAll.Value.ToLower() == "true"
                             ? GetAllApprovedOrderIDs()
                             : GetSelectedApprovedOrderIDs());
        }
    }
}