﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientFilterControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.ClientFilterControl" %>
<script type="text/javascript">
    function CheckAllChanged(sender, args)
    {
        ShowHideControls(sender.get_checked());
     };

    function ShowHideControls(ischecked) {
        var divpanle = $('#DivFilters');


        if (ischecked) {
            divpanle.hide();
        }
        else {
            divpanle.show();
        }
    };



   
</script>

<fieldset><legend>Filters</legend>
<table>
    <tr>
        <td >
            <span class="riLabel">All Clients:</span>
            <telerik:RadButton ID="chkAllClients" runat="server" AutoPostBack="false" 
                ToggleType="CheckBox" ButtonType="ToggleButton" Checked="true"  OnClientCheckedChanged="CheckAllChanged" OnClientLoad="CheckAllChanged">
            </telerik:RadButton>
        </td>
    </tr>
    </table>
    <div id="DivFilters">
    <table>
    <tr>
        <td>
            <telerik:RadButton ID="chkClientCount" runat="server" AutoPostBack="false" Value="1"
                 ToggleType="Radio" ButtonType="ToggleButton"
                Text="Number of Clients" Checked="true" GroupName="filters">
            </telerik:RadButton>
       &nbsp;&nbsp;&nbsp;
           
            <telerik:RadNumericTextBox runat="server" ID="txtClientCount" MinValue="5" ShowSpinButtons="True"
                EnableViewState="true" Value="5" >
                <NumberFormat GroupSeparator="" DecimalDigits="0" />
           </telerik:RadNumericTextBox>  <%--<span class="riLabel" id="LblClientCount" >No. of Clients</span>--%>
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadButton ID="chkClientFilter" runat="server" AutoPostBack="false" Value="2" 
                 ToggleType="Radio" ButtonType="ToggleButton"
                Text="Filter by Client IDs" Checked="False" GroupName="filters">
            </telerik:RadButton>
      &nbsp;&nbsp;&nbsp;
            
            <telerik:RadTextBox runat="server" ID="txtClientIDS" EnableViewState="true" >
            </telerik:RadTextBox>
            <span class="riLabel" id="lblclientFilter" >Example: IVCCCA35A,IVA517C7C </span>
        </td>
    </tr>
</table></div>
</fieldset>


