﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InstitutionControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.InstitutionControl" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="ABN_InputControl.ascx" TagName="ABN_InputControl" TagPrefix="uc1" %>
<%@ Register Src="ACN_InputControl.ascx" TagName="ACN_InputControl" TagPrefix="uc2" %>
<%@ Register Src="TFN_InputControl.ascx" TagName="TFN_InputControl" TagPrefix="uc3" %>
<%@ Register Src="PhoneNumberControl.ascx" TagName="PhoneNumberControl" TagPrefix="uc4" %>
<asp:Panel runat="server" ID="pnlTitle" Width="100%" BackColor="DarkBlue">
    <table width="100%">
        <tr>
            <td style="text-align: left; color: White;">
                <asp:Label runat="server" ID="lblTitle"></asp:Label>                
            </td>
            <td style="text-align: right;">
                <asp:ImageButton runat="server" ID="btnCancel" ImageUrl="~/images/cross_icon_normal.png"
                    OnClick="btnCancel_OnClick" />
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel runat="server" ID="pnlDetail" Width="100%">
<asp:HiddenField runat="server" ID="txtID" />
    <table width="100%">
        <tr>
            <td style="width:20%">
                &nbsp;Name
            </td>
            <td  style="width:80%" colspan="3">
                <asp:TextBox runat="server" ID="txtName" Width="95%"></asp:TextBox>
            </td>            
        </tr>
        <tr>
            <td>
                &nbsp;Description
            </td>
            <td  colspan="3">
                <asp:TextBox runat="server" ID="txtDescription" Width="95%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;Institution Code
            </td>
            <td  colspan="3">
                <asp:TextBox runat="server" ID="txtInsCode" Width="58%" MaxLength="6"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;Trading Name
            </td>
            <td  colspan="3">
                <asp:TextBox runat="server" ID="txtTradingName" Width="58%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;Legal Name
            </td>
            <td  colspan="3">
                <asp:TextBox runat="server" ID="txtLegalName" Width="30%"></asp:TextBox>
                <asp:CheckBox runat="server" Text="Same as Trading Name" ID="chkSameAsTrading" Checked="true" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;Type
            </td>
            <td  colspan="3">
                <asp:DropDownList runat="server" ID="ddlType" Width="60%" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                S & P Credit Rating
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;Long Term
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlLongTerm" Width="100%" />
            </td>
            <td  style="width:15%">
                &nbsp;Short Term
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlShortTerm" Width="95%" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;ACN
            </td>
            <td  colspan="3">
                <uc2:ACN_InputControl ID="ACN_Control" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;ABN
            </td>
            <td  colspan="3">
                <uc1:ABN_InputControl ID="ABN_Control" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;TFN
            </td>
            <td  colspan="3">
                <uc3:TFN_InputControl ID="TFN_Control" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;Telephone
            </td>
            <td  colspan="3">
                <uc4:PhoneNumberControl ID="PhoneControl" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;Facsimile
            </td>
            <td  colspan="3">
                <uc4:PhoneNumberControl ID="FacsimileControl" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;Address Line 1
            </td>
            <td  colspan="3">
                <asp:TextBox runat="server" ID="txtAddress1" Width="58%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;Address Line 2
            </td>
            <td  colspan="3">
                <asp:TextBox runat="server" ID="txtAddress2" Width="58%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;Suburb
            </td>
            <td  colspan="3">
                <asp:TextBox runat="server" ID="txtSubUrb" Width="58%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width:20%">
                &nbsp;Country
            </td>
            <td style="width:35%">
                <asp:DropDownList runat="server" ID="ddlCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                    AutoPostBack="true" EnableViewState="true" Width="100%"  />
            </td>
            <td colspan="2">
                &nbsp;State&nbsp;<asp:DropDownList runat="server" ID="ddlState" Width="80%"  />
            </td>
            
        </tr>
      
        <tr>
            <td>
                &nbsp;Post Code
            </td>
            <td colspan="3">
                <asp:TextBox runat="server" ID="txtPostalCode" Width="60%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label runat="server" ID="lblMessages" ForeColor="Red"></asp:Label>
            </td>
            <td style="text-align: right;" valign="top" colspan="3">
                <asp:Button runat="server" ID="btnSave" Text="Save" OnClick="btnSave_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>
