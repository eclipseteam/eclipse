﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttachExistingConfirmation.ascx.cs"
    Inherits="eclipseonlineweb.Controls.AttachExistingConfirmation" %>
<asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:HiddenField ID="hfTableName" runat="server" />
        <asp:HiddenField ID="hfFilter" runat="server" />
        <telerik:RadGrid OnNeedDataSource="SSPresentationGrid_OnNeedDataSource" ID="SSPresentationGrid"
            runat="server" ShowStatusBar="true" AutoGenerateColumns="false" PageSize="10"
            AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" GridLines="None"
            AllowAutomaticDeletes="True" AllowFilteringByColumn="true" EnableViewState="true"
            ShowFooter="false" OnItemCommand="SSPresentationGrid_OnItemCommand" Visible="false">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <GroupingSettings CaseSensitive="false"></GroupingSettings>
            <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                Name="ActiveOrders" TableLayout="Fixed" Font-Size="8">
                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                <Columns>
                    <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="ID" UniqueName="ID" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="MISCID" HeaderText="MISCID" ReadOnly="true"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="MISCID" UniqueName="MISCID" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="80px" SortExpression="Code"
                        HeaderText="Fund Code" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Code" UniqueName="FundCode">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="210px" HeaderStyle-Width="250px" SortExpression="Description"
                        ReadOnly="true" HeaderText="Fund Description" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Description" UniqueName="Description">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="65px" HeaderStyle-Width="105px" SortExpression="TradeDate"
                        ReadOnly="true" HeaderText="Trade Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TradeDate" UniqueName="TradeDate"
                        DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="75px" HeaderStyle-Width="110px" SortExpression="TransactionType"
                        ReadOnly="true" HeaderText="Transaction Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransactionType"
                        UniqueName="TransactionType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="105px" SortExpression="Shares"
                        ReadOnly="true" HeaderText="Shares" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Shares" UniqueName="Shares"
                        DataFormatString="{0:N4}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="105px" SortExpression="UnitPrice"
                        ReadOnly="true" HeaderText="Unit Price" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UnitPrice" UniqueName="UnitPrice"
                        DataFormatString="{0:N6}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="105px" SortExpression="Amount"
                        ReadOnly="true" HeaderText="Amount" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Amount" UniqueName="Amount"
                        DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderStyle-Width="50px" AllowFiltering="false" UniqueName="Actions"
                        HeaderText="Actions" ShowFilterIcon="false">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgAttachLink" runat="server" ImageUrl="../images/attach.png"
                                AlternateText="Attach Confirmation" ToolTip="Attach Confirmation" OnClientClick="javascript:return confirm('Are you sure you want to attach this confirmation?');"
                                CommandName="attach" CommandArgument='<%# Eval("ID") %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <telerik:RadGrid OnNeedDataSource="ASXPresentationGrid_OnNeedDataSource" ID="ASXPresentationGrid"
            runat="server" ShowStatusBar="true" AutoGenerateColumns="false" PageSize="10"
            AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" GridLines="None"
            AllowAutomaticDeletes="True" AllowFilteringByColumn="true" EnableViewState="true"
            ShowFooter="false" OnItemCommand="ASXPresentationGrid_OnItemCommand" Visible="false">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <GroupingSettings CaseSensitive="false"></GroupingSettings>
            <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                Name="ActiveOrders" TableLayout="Fixed" Font-Size="8">
                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                <Columns>
                    <telerik:GridBoundColumn SortExpression="TranstactionUniqueID" ReadOnly="true" HeaderText="TranstactionUniqueID"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="TranstactionUniqueID" UniqueName="TranstactionUniqueID"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="ASXCID" HeaderText="ASXCID" ReadOnly="true"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="ASXCID" UniqueName="ASXCID" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" SortExpression="InvestmentCode"
                        ReadOnly="true" HeaderText="Investment Code" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InvestmentCode"
                        UniqueName="InvestmentCode">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="65px" HeaderStyle-Width="105px" SortExpression="TransactionType"
                        ReadOnly="true" HeaderText="Transaction Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransactionType"
                        UniqueName="TransactionType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="AccountNo" ReadOnly="true" HeaderText="Account No"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="AccountNo" UniqueName="AccountNo">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="FPSInstructionID" ReadOnly="true" HeaderText="FPSInstructionID"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="FPSInstructionID" UniqueName="FPSInstructionID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="65px" HeaderStyle-Width="105px" SortExpression="TradeDate"
                        ReadOnly="true" HeaderText="Trade Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TradeDate" UniqueName="TradeDate"
                        DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="65px" HeaderStyle-Width="105px" SortExpression="SettlementDate"
                        ReadOnly="true" HeaderText="Settlement Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="SettlementDate"
                        UniqueName="SettlementDate" DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="105px" SortExpression="Units"
                        ReadOnly="true" HeaderText="Units" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Units" UniqueName="Units"
                        DataFormatString="{0:N4}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="105px" SortExpression="GrossValue"
                        ReadOnly="true" HeaderText="Gross Value" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="GrossValue" UniqueName="GrossValue"
                        DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="105px" SortExpression="Charges"
                        ReadOnly="true" HeaderText="Charges" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Charges" UniqueName="Charges"
                        DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="105px" SortExpression="BrokerageGST"
                        ReadOnly="true" HeaderText="GST" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BrokerageGST"
                        UniqueName="BrokerageGST" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="105px" SortExpression="BrokerageAmount"
                        ReadOnly="true" HeaderText="Brokerage" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BrokerageAmount"
                        UniqueName="BrokerageAmount" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="105px" SortExpression="NetValue"
                        ReadOnly="true" HeaderText="Net Value" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="NetValue" UniqueName="Amount"
                        DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="105px" SortExpression="Narrative"
                        ReadOnly="true" HeaderText="Contract Note" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Narrative" UniqueName="Narrative">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderStyle-Width="50px" AllowFiltering="false" UniqueName="Actions"
                        HeaderText="Actions" ShowFilterIcon="false">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgAttachLink" runat="server" ImageUrl="../images/attach.png"
                                AlternateText="Attach Confirmation" ToolTip="Attach Confirmation" OnClientClick="javascript:return confirm('Are you sure you want to attach this confirmation?');"
                                CommandName="attach" CommandArgument='<%# Eval("TranstactionUniqueID") %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <telerik:RadGrid OnNeedDataSource="BWPresentationGrid_OnNeedDataSource" ID="BWPresentationGrid"
            runat="server" ShowStatusBar="true" AutoGenerateColumns="false" PageSize="8"
            AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" GridLines="None"
            AllowAutomaticDeletes="True" AllowFilteringByColumn="true" EnableViewState="true"
            ShowFooter="false" OnItemCommand="BWPresentationGrid_OnItemCommand" Visible="false">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <GroupingSettings CaseSensitive="false"></GroupingSettings>
            <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                Name="ActiveOrders" TableLayout="Fixed" Font-Size="8">
                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                <Columns>
                    <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="ID" UniqueName="ID" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="BankCID" HeaderText="BankCID" ReadOnly="true"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="BankCID" UniqueName="BankCID" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="75px" HeaderStyle-Width="115px" SortExpression="ImportTransactionType"
                        ReadOnly="true" HeaderText="Import Transaction" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ImportTransactionType"
                        UniqueName="ImportTransactionType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="120px" SortExpression="SystemTransactionType"
                        ReadOnly="true" HeaderText="System Transaction" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="SystemTransactionType"
                        UniqueName="SystemTransactionType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="90px" SortExpression="Category"
                        ReadOnly="true" HeaderText="Category" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Category" UniqueName="Category">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="65px" HeaderStyle-Width="105px" SortExpression="BankTransactionDate"
                        ReadOnly="true" HeaderText="Transaction Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BankTransactionDate"
                        UniqueName="DateOfTransaction" DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="120px" SortExpression="AccountName"
                        ReadOnly="true" HeaderText="Account Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountName" UniqueName="BSBNAccountNumber"
                        Visible="False">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="120px" SortExpression="AccountNo"
                        ReadOnly="true" HeaderText="Account Number" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountNo" UniqueName="AccountNo">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="45px" HeaderStyle-Width="85px" SortExpression="AccountType"
                        ReadOnly="true" HeaderText="Account Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountType" UniqueName="AccountType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="105px" SortExpression="BankAmount"
                        ReadOnly="true" HeaderText="Amount" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BankAmount" UniqueName="BankAmount"
                        DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="105px" SortExpression="BankAdjustment"
                        ReadOnly="true" HeaderText="Adjustment" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BankAdjustment"
                        UniqueName="BankAdjustment" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="105px" SortExpression="AmountTotal"
                        ReadOnly="true" HeaderText="Total" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AmountTotal" UniqueName="Amount"
                        DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="120px" SortExpression="Comment"
                        ReadOnly="true" HeaderText="Comments" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Comment" UniqueName="Comment">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="90px" SortExpression="DividendStatus"
                        ReadOnly="true" HeaderText="Status" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="DividendStatus"
                        UniqueName="DividendStatus">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderStyle-Width="50px" AllowFiltering="false" UniqueName="Actions"
                        HeaderText="Actions" ShowFilterIcon="false">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgAttachLink" runat="server" ImageUrl="../images/attach.png"
                                AlternateText="Attach Confirmation" ToolTip="Attach Confirmation" OnClientClick="javascript:return confirm('Are you sure you want to attach this confirmation?');"
                                CommandName="attach" CommandArgument='<%# Eval("ID") %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <telerik:RadGrid OnNeedDataSource="TDPresentationGrid_OnNeedDataSource" ID="TDPresentationGrid"
            runat="server" ShowStatusBar="true" AutoGenerateColumns="false" PageSize="10"
            AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" GridLines="None"
            AllowAutomaticDeletes="True" AllowFilteringByColumn="true" EnableViewState="true"
            ShowFooter="false" OnItemCommand="TDPresentationGrid_OnItemCommand" Visible="false">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <GroupingSettings CaseSensitive="false"></GroupingSettings>
            <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                Name="ActiveOrders" TableLayout="Fixed" Font-Size="8">
                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                <Columns>
                    <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="ID" UniqueName="ID" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="BankCID" HeaderText="BankCID" ReadOnly="true"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="BankCID" UniqueName="BankCID" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="75px" HeaderStyle-Width="115px" SortExpression="ImportTransactionType"
                        ReadOnly="true" HeaderText="Import Transaction" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ImportTransactionType"
                        UniqueName="ImportTransactionType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="120px" SortExpression="SystemTransactionType"
                        ReadOnly="true" HeaderText="System Transaction" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="SystemTransactionType"
                        UniqueName="SystemTransactionType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="90px" SortExpression="Category"
                        ReadOnly="true" HeaderText="Category" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Category" UniqueName="Category">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="90px" SortExpression="AdministrationSystem"
                        ReadOnly="true" HeaderText="Admin Sys" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AdministrationSystem"
                        UniqueName="AdministrationSystem">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" SortExpression="InstituteName"
                        ReadOnly="true" HeaderText="Institute Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InstituteName"
                        UniqueName="InstituteName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="65px" HeaderStyle-Width="105px" SortExpression="BankTransactionDate"
                        ReadOnly="true" HeaderText="Transaction Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BankTransactionDate"
                        UniqueName="DateOfTransaction" DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="90px" SortExpression="ContractNote"
                        ReadOnly="true" HeaderText="Contract Note" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ContractNote"
                        UniqueName="ContractNote">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="65px" HeaderStyle-Width="105px" SortExpression="BankMaturityDate"
                        ReadOnly="true" HeaderText="Maturity Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BankMaturityDate"
                        UniqueName="BankMaturityDate" DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="80px" SortExpression="InterestRate"
                        ReadOnly="true" HeaderText="Interest Rate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InterestRate"
                        UniqueName="InterestRate">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="105px" SortExpression="BankAmount"
                        ReadOnly="true" HeaderText="Amount" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BankAmount" UniqueName="BankAmount"
                        DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="105px" SortExpression="BankAdjustment"
                        ReadOnly="true" HeaderText="Adjustment" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BankAdjustment"
                        UniqueName="BankAdjustment" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="105px" SortExpression="AmountTotal"
                        ReadOnly="true" HeaderText="Total" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AmountTotal" UniqueName="Amount"
                        DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderStyle-Width="50px" AllowFiltering="false" UniqueName="Actions"
                        HeaderText="Actions" ShowFilterIcon="false">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgAttachLink" runat="server" ImageUrl="../images/attach.png"
                                AlternateText="Attach Confirmation" ToolTip="Attach Confirmation" OnClientClick="javascript:return confirm('Are you sure you want to attach this confirmation?');"
                                CommandName="attach" CommandArgument='<%# Eval("ID") %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </ContentTemplate>
</asp:UpdatePanel>
