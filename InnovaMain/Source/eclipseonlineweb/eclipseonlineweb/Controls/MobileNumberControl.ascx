﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MobileNumberControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.MobileNumberControl" EnableViewState="true" %>
<script type="text/javascript">
    $(document).ready(function () {

        $(".autotab").keyup(function () {

            if ($(this).attr("maxlength") == $(this).val().length) {

                var index = $(".autotab").index(this);
                var item = $($(".autotab")[++index]);
                if (item.length > 0)
                    item.focus();
            }
        });
    });
</script>
<span id="ctrlPhoneNoBody"><span style="margin: 0 0 0 -15px; overflow: auto; position: absolute;">
    +&nbsp;</span>
    <telerik:RadMaskedTextBox runat="server" ID="TxtCountryCode" CssClass="autotab" Mask="###"
        MaskType="Numeric" Width="40px" PromptChar=" " />
    &nbsp;-&nbsp;
    <telerik:RadMaskedTextBox runat="server" ID="TxtBoxMobileNumber" CssClass="autotab"
        Mask="####################" MaskType="Numeric" Width="90px" PromptChar=" " />

     <asp:Label runat="server" ForeColor="Red" ID="Msg"></asp:Label>