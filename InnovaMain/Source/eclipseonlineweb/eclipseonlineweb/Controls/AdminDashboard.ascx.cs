﻿using System;
using System.Data;
using System.Linq;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.SM.Workflows;
using Oritax.TaxSimp.SM.Workflows.Data;

namespace eclipseonlineweb.Controls
{
    public partial class AdminDashboard : System.Web.UI.UserControl
    {
        private ICMBroker Broker
        {
            get { return ((UMABasePage)Page).UMABroker; }
        }
        #region Alerts & Nofications
        protected int GetAlertCount()
        {
            IBrokerManagedComponent bmc = Broker.CreateTransientComponentInstance(new Guid("15DE2644-C1A0-41D5-8293-2794EF31840D"));
            var alertDS = new MDAAlertDS { Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = ((UMABasePage)Page).GetCurrentUser() } };
            bmc.GetData(alertDS);
            DataTable mdaAlertTable = alertDS.Tables[MDAAlertDS.MDAALERTTABLE];

            //Filtering only Active Alerts.
            var rows = from r in mdaAlertTable.AsEnumerable()
                       where r.Field<string>(MDAAlertTable.STATUS) == MDAAlertTable.AlretStatus.Active.ToString()
                       select r;
            return rows.Count();
        }
        protected int GetNotificationCount()
        {
            IBrokerManagedComponent bmc = Broker.CreateTransientComponentInstance(new Guid("15DE2644-C1A0-41D5-8293-2794EF31840D"));
            var workflowsDS = new WorkflowsDS();
            bmc.GetData(workflowsDS);
            DataTable table = workflowsDS.Tables[WorkflowsDS.NotificationsTable];
            var view = new DataView(table)
                {
                    Sort = "CreatedDate DESC",
                    RowFilter = string.Format("NotificationType>{0}", (int)NotificationType.MDA)
                };
            return view.ToTable().Rows.Count;
        }
        #endregion
    }
}