﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Common;
using Telerik.Web.UI;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb.Controls
{
    public partial class ExistingBankAccountsControl : UserControl
    {
        
        private ICMBroker UMABroker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }
        public Action PageRefresh
        {
            get;
            set;
        }
        
        public Action CanceledPopup { get; set; }

        public Action ValidationFailed { get; set; }

        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }
        public Guid CLID { get; set; }
        public Guid CSID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        protected void gd_AvaliableBankAccount_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);

            IndividualDS idividualDS = new IndividualDS();
            idividualDS.Command = (int)WebCommands.GetOrganizationUnitsByType;
            idividualDS.Unit = new OrganizationUnit
            {
                CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                Type = ((int)OrganizationType.Individual).ToString()
            };
            org.GetData(idividualDS);
            gd_AvaliableBankAccount.DataSource = idividualDS.IndividualTable;
            Broker.ReleaseBrokerManagedComponent(org);

        }
        protected void gd_AvaliableBankAccount_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "edit")
            {

            }
        }
        protected void btnClose_OnClick(object sender, EventArgs e)
        {
            if (CanceledPopup != null)
            {
                gd_AvaliableBankAccount.DataSource = null;
                gd_AvaliableBankAccount.Rebind();
                CanceledPopup();
            }
        }
        public void VisibleAvailableIndividualsGrid()
        {
            if(!string.IsNullOrEmpty(txtSearchAccName.Text))
            {
                dvExistingAccGrid.Visible = true;
                //ContactAssociationGrid.Visible = true;
                LoadAvailableIndividualsForAssociation();
            }
            //Title.Text = "Available Contacts For Association";
        }
        private void LoadAvailableIndividualsForAssociation()
        {
            //Title.Text = "Availabe Contacts for association";
            //var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            //IndividualDS allIndividuals = new IndividualDS();
            //allIndividuals.Command = (int)WebCommands.GetOrganizationUnitsByType;
            //allIndividuals.Unit = new OrganizationUnit { Name = txtSearchAccName.Text, CurrentUser = (Page as UMABasePage).GetCurrentUser(), Type = ((int)OrganizationType.Individual).ToString() };
            //org.GetData(allIndividuals);

            //DataView summaryView = new DataView(allIndividuals.Tables[allIndividuals.IndividualTable.TABLENAME]);
            //summaryView.Sort = allIndividuals.IndividualTable.NAME + " ASC";
            //ContactAssociationGrid.DataSource = summaryView;
            //ContactAssociationGrid.DataBind();
            //UMABroker.ReleaseBrokerManagedComponent(org);
        }
        public void ClearEntity()
        {
            txtSearchAccName.Text = "";
            lblmessages.Text = "";
            //ContactAssociationGrid.Visible = false;
           
        }
        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            string CID = Request.QueryString["ins"];
            OrganizationUnitCM clientData = this.Broker.GetBMCInstance(new Guid(CID)) as OrganizationUnitCM;
            Guid clid = Guid.Empty;
            Guid csid = Guid.Empty;
            string businessTitle = string.Empty;
            this.UMABroker.SaveOverride = true;
            bool iSSelected = false;
            //foreach (C1GridViewRow dr in ContactAssociationGrid.Rows)
            //{
            //    CheckBox checkCell = (CheckBox)dr.Cells[3].FindControl("chkContactAssociation");
            //    if (checkCell.Checked)
            //    {
            //        clid = new Guid(dr.Cells[1].Text);
            //        csid = new Guid(dr.Cells[2].Text);
            //        DropDownList ddlBusinessTitle    = (DropDownList)dr.Cells[8].FindControl("DdlBusinessTitle");
            //        if (ddlBusinessTitle.SelectedIndex == 0)
            //        {
            //            lblmessages.Text = "Please select business title";
            //            return;
            //        }
            //        businessTitle = ddlBusinessTitle.SelectedItem.Text;
            //        if (clientData.SignatoriesApplicants.Where(associate => associate.Clid == clid &&
            //                                 associate.Csid == csid).FirstOrDefault() == null)
            //        {
            //            Oritax.TaxSimp.Common.IdentityCMDetail identityCMDetail = new Oritax.TaxSimp.Common.IdentityCMDetail();
            //            identityCMDetail.Clid = clid;
            //            identityCMDetail.Csid = csid;
            //            identityCMDetail.BusinessTitle.Title = businessTitle;
            //            clientData.SignatoriesApplicants.Add(identityCMDetail);
            //        }
            //        iSSelected = true;
            //    }
            //}
            if (!iSSelected)
            {
                lblmessages.Text = "Please select individual(s) for association";
                return;
            }
            clientData.CalculateToken(true);
            this.UMABroker.SetComplete();
            this.UMABroker.SetStart();
           
            if (PageRefresh != null)
            {
                PageRefresh();
            }
            ClearEntity();
            if (CanceledPopup != null)
            {
                CanceledPopup();
            }
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (CanceledPopup != null)
            {
                ClearEntity();
                CanceledPopup();
            }
        }

        protected void ContactAssociationGrid_OnRowCommand(object sender, C1GridViewCommandEventArgs e)
        {
        }
        //protected void ContactAssociationGrid_OnPageIndexChanging(object sender, C1GridViewPageEventArgs e)
        //{
        //    LoadAvailableIndividualsForAssociation();
        //    ContactAssociationGrid.PageIndex = e.NewPageIndex;
        //    ContactAssociationGrid.DataBind();
        //    if (ValidationFailed != null)
        //    {
        //        ValidationFailed();
        //    }
        //}
        protected void FilterContactAssociationGrid(object sender, C1GridViewFilterEventArgs e)
        {
            e.Values[0] = ((string)e.Values[0]).Trim();
            LoadAvailableIndividualsForAssociation();
            if (ValidationFailed != null)
            {
                ValidationFailed();
            }
        }

        protected void SortContactAssociationGrid(object sender, C1GridViewSortEventArgs e)
        {
            LoadAvailableIndividualsForAssociation();
            if (ValidationFailed != null)
            {
                ValidationFailed();
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            VisibleAvailableIndividualsGrid();
            if (ValidationFailed != null)
            {
                ValidationFailed();
            }
        }
    }
}