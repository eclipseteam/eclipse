﻿using System;
using System.Data;
using Oritax.TaxSimp.DataSets;

namespace eclipseonlineweb.Controls
{
    public partial class WorkflowAddressDetail : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            BusinessAddressControl.SameAddressAs += sameAs => SetAddressSameAs(BusinessAddressControl, sameAs);
            RegisteredAddressControl.SameAddressAs += sameAs => SetAddressSameAs(RegisteredAddressControl, sameAs);
            MailingAddressControl.SameAddressAs += sameAs => SetAddressSameAs(MailingAddressControl, sameAs);
            DuplicateMailingAddressControl.SameAddressAs += sameAs => SetAddressSameAs(DuplicateMailingAddressControl, sameAs);
        }

        private void SetAddressSameAs(AddressControl addressControl, string sameAs)
        {
            var copyControl = addressControl;
            switch ((AddressType)Enum.Parse(typeof(AddressType), sameAs))
            {
                case AddressType.BusinessAddress:
                    copyControl = BusinessAddressControl;
                    break;
                case AddressType.RegisteredAddress:
                    copyControl = RegisteredAddressControl;
                    break;
                case AddressType.MailingAddress:
                    copyControl = MailingAddressControl;
                    break;
                case AddressType.DuplicateMailingAddress:
                    copyControl = DuplicateMailingAddressControl;
                    break;
            }

            addressControl.CopyFromAddressControl(copyControl);
        }

        public void FillAddressControls(AddressDetailsDS ds)
        {
            foreach (DataRow dr in ds.ClientDetailsTable.Rows)
            {
                if ((AddressType)Enum.Parse(typeof(AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == AddressType.BusinessAddress)
                {
                    BusinessAddressControl.SetEntity(dr[ds.ClientDetailsTable.ADDRESSLINE1].ToString(), dr[ds.ClientDetailsTable.ADDRESSLINE2].ToString(), dr[ds.ClientDetailsTable.SUBURB].ToString(), dr[ds.ClientDetailsTable.STATE].ToString(), dr[ds.ClientDetailsTable.POSTCODE].ToString(), dr[ds.ClientDetailsTable.COUNTRY].ToString());
                }
                if ((AddressType)Enum.Parse(typeof(AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == AddressType.RegisteredAddress)
                {
                    RegisteredAddressControl.SetEntity(dr[ds.ClientDetailsTable.ADDRESSLINE1].ToString(), dr[ds.ClientDetailsTable.ADDRESSLINE2].ToString(), dr[ds.ClientDetailsTable.SUBURB].ToString(), dr[ds.ClientDetailsTable.STATE].ToString(), dr[ds.ClientDetailsTable.POSTCODE].ToString(), dr[ds.ClientDetailsTable.COUNTRY].ToString());
                }
                if ((AddressType)Enum.Parse(typeof(AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == AddressType.MailingAddress)
                {
                    MailingAddressControl.SetEntity(dr[ds.ClientDetailsTable.ADDRESSLINE1].ToString(), dr[ds.ClientDetailsTable.ADDRESSLINE2].ToString(), dr[ds.ClientDetailsTable.SUBURB].ToString(), dr[ds.ClientDetailsTable.STATE].ToString(), dr[ds.ClientDetailsTable.POSTCODE].ToString(), dr[ds.ClientDetailsTable.COUNTRY].ToString());
                }
                if ((AddressType)Enum.Parse(typeof(AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == AddressType.DuplicateMailingAddress)
                {
                    DuplicateMailingAddressControl.SetEntity(dr[ds.ClientDetailsTable.ADDRESSLINE1].ToString(), dr[ds.ClientDetailsTable.ADDRESSLINE2].ToString(), dr[ds.ClientDetailsTable.SUBURB].ToString(), dr[ds.ClientDetailsTable.STATE].ToString(), dr[ds.ClientDetailsTable.POSTCODE].ToString(), dr[ds.ClientDetailsTable.COUNTRY].ToString());
                }
            }
        }

        public AddressDetailsDS SetData()
        {
            var ds = new AddressDetailsDS();
            if (BusinessAddressControl.Validate())
            {
                DataRow drBusiness = ds.ClientDetailsTable.NewRow();
                drBusiness[ds.ClientDetailsTable.ADDRESSLINE1] = BusinessAddressControl.AddressLine1;
                drBusiness[ds.ClientDetailsTable.ADDRESSLINE2] = BusinessAddressControl.AddressLine2;
                drBusiness[ds.ClientDetailsTable.SUBURB] = BusinessAddressControl.Subrub;
                drBusiness[ds.ClientDetailsTable.STATE] = BusinessAddressControl.State;
                drBusiness[ds.ClientDetailsTable.COUNTRY] = BusinessAddressControl.Country;
                drBusiness[ds.ClientDetailsTable.POSTCODE] = BusinessAddressControl.Postcode;
                drBusiness[ds.ClientDetailsTable.ADDRESSTYPE] = AddressType.BusinessAddress;
                ds.ClientDetailsTable.Rows.Add(drBusiness);
            }
            if (RegisteredAddressControl.Validate())
            {
                DataRow drRegistered = ds.ClientDetailsTable.NewRow();
                drRegistered[ds.ClientDetailsTable.ADDRESSLINE1] = RegisteredAddressControl.AddressLine1;
                drRegistered[ds.ClientDetailsTable.ADDRESSLINE2] = RegisteredAddressControl.AddressLine2;
                drRegistered[ds.ClientDetailsTable.SUBURB] = RegisteredAddressControl.Subrub;
                drRegistered[ds.ClientDetailsTable.STATE] = RegisteredAddressControl.State;
                drRegistered[ds.ClientDetailsTable.COUNTRY] = RegisteredAddressControl.Country;
                drRegistered[ds.ClientDetailsTable.POSTCODE] = RegisteredAddressControl.Postcode;
                drRegistered[ds.ClientDetailsTable.ADDRESSTYPE] = AddressType.RegisteredAddress;
                ds.ClientDetailsTable.Rows.Add(drRegistered);
            }
            if (MailingAddressControl.Validate())
            {
                DataRow drMailing = ds.ClientDetailsTable.NewRow();
                drMailing[ds.ClientDetailsTable.ADDRESSLINE1] = MailingAddressControl.AddressLine1;
                drMailing[ds.ClientDetailsTable.ADDRESSLINE2] = MailingAddressControl.AddressLine2;
                drMailing[ds.ClientDetailsTable.SUBURB] = MailingAddressControl.Subrub;
                drMailing[ds.ClientDetailsTable.STATE] = MailingAddressControl.State;
                drMailing[ds.ClientDetailsTable.COUNTRY] = MailingAddressControl.Country;
                drMailing[ds.ClientDetailsTable.POSTCODE] = MailingAddressControl.Postcode;
                drMailing[ds.ClientDetailsTable.ADDRESSTYPE] = AddressType.MailingAddress;
                ds.ClientDetailsTable.Rows.Add(drMailing);
            }
            if (DuplicateMailingAddressControl.Validate())
            {
                DataRow drDuplicate = ds.ClientDetailsTable.NewRow();
                drDuplicate[ds.ClientDetailsTable.ADDRESSLINE1] = DuplicateMailingAddressControl.AddressLine1;
                drDuplicate[ds.ClientDetailsTable.ADDRESSLINE2] = DuplicateMailingAddressControl.AddressLine2;
                drDuplicate[ds.ClientDetailsTable.SUBURB] = DuplicateMailingAddressControl.Subrub;
                drDuplicate[ds.ClientDetailsTable.STATE] = DuplicateMailingAddressControl.State;
                drDuplicate[ds.ClientDetailsTable.COUNTRY] = DuplicateMailingAddressControl.Country;
                drDuplicate[ds.ClientDetailsTable.POSTCODE] = DuplicateMailingAddressControl.Postcode;
                drDuplicate[ds.ClientDetailsTable.ADDRESSTYPE] = AddressType.DuplicateMailingAddress;
                ds.ClientDetailsTable.Rows.Add(drDuplicate);
            }

            return ds;
        }
    }
}