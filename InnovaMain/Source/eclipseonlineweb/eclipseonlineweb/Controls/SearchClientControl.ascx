﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchClientControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.SearchClientControl" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<!--Search Start-->
<asp:Panel runat="server" DefaultButton="btnSearch">
    <asp:TextBox Width="350px" ID="txtSearch" runat="server" CssClass="searchInput"></asp:TextBox><asp:LinkButton
        ID="btnSearch" runat="server" OnClick="btnSearch_Click"><img runat="server" id="img1" class="searchImage" /></asp:LinkButton>
    <ajaxToolkit:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtSearch"
        WatermarkText=" Type in Client Name, e-Clipse ID or e-Clipse Super Member ID" />
</asp:Panel>
