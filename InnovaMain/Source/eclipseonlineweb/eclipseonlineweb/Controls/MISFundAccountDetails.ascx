﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MISFundAccountDetails.ascx.cs"
    Inherits="eclipseonlineweb.ClientViews.WebControls.MISFundAccountDetails" %>
<style type="text/css">
    .LabelText
    {
        width: 200px;
    }
    .ControlField
    {
        width: 300px;
    }
</style>
<div class="popup_Container">
    <div class="popup_Titlebar" id="PopupHeader">
        <div class="TitlebarLeft">
            <asp:Label ID="Title" runat="server" Text="Fund Account"></asp:Label>
        </div>
    </div>
    <div class="popup_Body">
        <asp:HiddenField ID="txtHidEditMode" runat="server" />
        <table>
            <tr>
                <td class="LabelText">
                    MIS Account
                </td>
                <td class="ControlField">
                    <telerik:RadComboBox runat="server" ID="cmbMISAcc" Width="250px" AutoPostBack="True" ZIndex="10000000"
                        OnSelectedIndexChanged="Mis_Changed">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td class="LabelText">
                    Fund Name
                </td>
                <td class="ControlField">
                    <telerik:RadComboBox runat="server" ID="cmbFundCode" Width="250px" ZIndex="10000000">
                    </telerik:RadComboBox>
                </td>
            </tr>
             <tr runat="server" ID="reInvetRow">
                <td class="LabelText">
                  Reinvestment Option
                </td>
                <td class="ControlField">
                    <telerik:RadComboBox runat="server" ID="cmbReinvetmentOption" Width="250px" ZIndex="10000000">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblMessages" runat="server" Text="" ForeColor="#990000" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <telerik:RadButton runat="server" ID="btnUpdate" Text="Save" OnClick="btnUpdate_Onclick">
                    </telerik:RadButton>
                    <telerik:RadButton runat="server" ID="btnCancel" Text="Cancel" OnClick="btnCancel_Onclick">
                    </telerik:RadButton>
                </td>
            </tr>
        </table>
    </div>
</div>
