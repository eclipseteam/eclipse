﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.C1ComboBox;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb.Controls
{
    public partial class BankAccountControl : System.Web.UI.UserControl
    {
        public Action<Guid, Guid, Guid> Saved
        {
            get;
            set;
        }
        public bool IsAdviserOrClient
        {
            private get
            {
                return !string.IsNullOrEmpty(hfIsAdviserOrClient.Value) && Convert.ToBoolean(hfIsAdviserOrClient.Value);
            }
            set
            {
                hfIsAdviserOrClient.Value = value.ToString();
            }
        }
        public Action<Guid, DataSet> SaveData
        {
            get;
            set;
        }
        public Action Canceled { get; set; }
        public Action ValidationFailed { get; set; }
        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadControls();
            }
        }

        private void LoadControls()
        {
            //bind combos here
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            var status = new StatusDS { TypeFilter = "Bank" };

            org.GetData(status);
            ComboStatus.Items.Clear();
            ComboStatus.DataSource = status.Tables[StatusDS.TABLENAME];
            ComboStatus.DataTextField = StatusDS.NAME;
            ComboStatus.DataValueField = StatusDS.NAME;
            ComboStatus.DataBind();

            ComboInstitute.Items.Clear();
            ComboInstitute.DataSource = org.Institution;
            ComboInstitute.DataTextField = "Name";
            ComboInstitute.DataValueField = "ID";
            ComboInstitute.DataBind();

            TermDepositBasicTable tble = GetTermDepositAccounts(org);
            ComboBroker.DataSource = tble;
            ComboBroker.DataTextField = "BrokerName";
            ComboBroker.DataValueField = "ID";
            ComboBroker.DataBind();

            Broker.ReleaseBrokerManagedComponent(org);

        }

        private TermDepositBasicTable GetTermDepositAccounts(IBrokerManagedComponent org)
        {
            var brokers = new TermDepositBrokerBasicDS
                              {
                                  Command = (int)WebCommands.GetOrganizationUnitsByType,
                                  Unit = new OrganizationUnit
                                             {
                                                 CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                                                 Type = ((int)OrganizationType.TermDepositAccount).ToString()
                                             }
                              };
            org.GetData(brokers);
            return brokers.TermDepositBasicTable;
        }
        private TermDepositBasicTable GetTermDepositAccounts()
        {
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            var tbl = GetTermDepositAccounts(org);
            Broker.ReleaseBrokerManagedComponent(org);
            return tbl;
        }

        private bool Validate()
        {
            var messages = new StringBuilder();
            bool tempResult = txttName.ValidateAlphaNumeric(true);
            bool result = tempResult;
            AddMessageIn(tempResult, "Invalid Account Name*", messages);
            tempResult = TextBSB1.ValidateAlphaNumeric(3, 3);
            result = tempResult && result;
            AddMessageIn(tempResult, "Invalid BSB*", messages);
            tempResult = TextBSB2.ValidateAlphaNumeric(3, 3);
            result = tempResult && result;
            AddMessageIn(tempResult, "Invalid BSB*", messages);
            tempResult = TextAccountNumber.ValidateAlphaNumeric(1, 25);
            result = tempResult && result;
            AddMessageIn(tempResult, "Invalid Account Number*", messages);
            tempResult = ComboAccoutType.ValidateCombo(true);
            result = tempResult && result;
            AddMessageIn(tempResult, "Invalid Accout Type*", messages);

            if (ComboAccoutType.SelectedValue.ToLower() == "termdeposit")
            {
                tempResult = ComboBroker.ValidateCombo(true);
                result = tempResult && result;
                AddMessageIn(tempResult, "Invalid Broker*", messages);
            }

            //Checking for existing Records
            //setting values
            var cid = Guid.Parse(TextCID.Value);
            var clid = Guid.Parse(TextClid.Value);
            var csid = Guid.Parse(TextCsid.Value);
            var bsb = TextBSB1.Text.Trim() + "-" + TextBSB2.Text.Trim();
            var accountNo = TextAccountNumber.Text.Trim();
            var isNew = cid == Guid.Empty;

            //calling function to check for existing record
            tempResult = !IsAlreadyPresent(isNew, bsb, accountNo, clid, csid);
            result = tempResult && result;
            AddMessageIn(tempResult, "BSB and Account Number already exist", messages);

            //Setting Error messages for display
            lblMessages.Text = messages.ToString();

            return result;
        }
        private void AddMessageIn(bool result, string message, StringBuilder messages)
        {
            if (!result)
            {
                messages.Append(message + "<br/>");
            }
        }

        public void SetEntity(Guid cid, bool fromAdmin)
        {
            LoadControls();
            ClearEntity();
            // lblBroker.Visible = ComboBroker.Visible = (accountType.ToLower() == "termdepositaccount");
            TextCID.Value = cid.ToString();

            if (cid != Guid.Empty)
            {
                BankAccountDS ds = GetDataSet(cid);
                DataRow dr = ds.Tables[ds.BankAccountsTable.TABLENAME].Rows[0];
                TextCsid.Value = dr[ds.BankAccountsTable.CSID].ToString();
                TextClid.Value = dr[ds.BankAccountsTable.CLID].ToString();
                txttName.Text = dr[ds.BankAccountsTable.ACCOUNTNAME].ToString();

                if (ComboAccoutType.FindItemByValue(dr[ds.BankAccountsTable.ACCOUNTTYPE].ToString()) != null)
                    ComboAccoutType.FindItemByValue(dr[ds.BankAccountsTable.ACCOUNTTYPE].ToString()).Selected = true;

                TextAccountNumber.Text = dr[ds.BankAccountsTable.ACCOUNTNO].ToString();
                ComboStatus.SelectedValue = dr[ds.BankAccountsTable.STATUS].ToString();
                ComboStatus.Text = dr[ds.BankAccountsTable.STATUS].ToString();
                if ((dr[ds.BankAccountsTable.ACCOUNTTYPE].ToString().ToLower() == "termdeposit"))
                {
                    SetBrokerVisibilty(true);
                    var table = GetTermDepositAccounts();
                    var iddr =
                        (table.AsEnumerable().FirstOrDefault(
                            ss =>
                           (Guid)ss[table.CLID] == (Guid)dr[ds.BankAccountsTable.BROKERID] &&
                             (Guid)ss[table.CSID] == (Guid)dr[ds.BankAccountsTable.BROKERCSID]));
                    if (iddr != null)
                    {

                        ComboBroker.Text = iddr[table.BROKERNAME].ToString();
                        ComboBroker.SelectedValue = iddr[table.ID].ToString();
                    }
                }
                else
                {
                    SetBrokerVisibilty(false);
                }

                if (dr[ds.BankAccountsTable.BSBNO] != DBNull.Value || dr[ds.BankAccountsTable.BSBNO].ToString().Trim() != "")
                {
                    string[] bsb = dr[ds.BankAccountsTable.BSBNO].ToString().Split('-');
                    TextBSB1.Text = bsb[0];
                    if (bsb.Length == 2)
                        TextBSB2.Text = bsb[1];
                }

                ComboInstitute.SelectedValue = dr[ds.BankAccountsTable.INSTITUTIONID].ToString();

                //Setting customer no
                txtCustomerNo.Text = dr[ds.BankAccountsTable.CUSTOMERNO].ToString();

                if (fromAdmin)
                {
                    lblCustomerNo.Visible = true;
                    txtCustomerNo.Visible = true;
                }

                chkExternal.Checked = (bool)dr[ds.BankAccountsTable.ISEXTERNALACCOUNT];
            }
            else
            {
                chkExternal.Checked = IsAdviserOrClient;
            }
            chkExternal.Enabled = !IsAdviserOrClient;

            if (IsAdviserOrClient)
            {
                ComboAccoutType.Items.Remove(ComboAccoutType.Items.FirstOrDefault(ss => ss.Value == "TERMDEPOSIT"));
            }
        }
        private BankAccountDS GetDataSet(Guid cid)
        {
            var bankTransactionDetailsDS = new BankAccountDS { CommandType = DatasetCommandTypes.Get };
            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
                clientData.GetData(bankTransactionDetailsDS);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }
            return bankTransactionDetailsDS;
        }

        private BankAccountDS GetEntity(BankAccountDS bankaccountds)
        {
            DataTable dataTable = bankaccountds.Tables[bankaccountds.BankAccountsTable.TABLENAME];
            DataRow dataRow = dataTable.NewRow();

            dataRow[bankaccountds.BankAccountsTable.CID] = TextCID.Value;
            dataRow[bankaccountds.BankAccountsTable.CLID] = TextClid.Value;
            dataRow[bankaccountds.BankAccountsTable.CSID] = TextCsid.Value;
            dataRow[bankaccountds.BankAccountsTable.ACCOUNTNAME] = txttName.Text.Trim();
            dataRow[bankaccountds.BankAccountsTable.ACCOUNTNO] = TextAccountNumber.Text.Trim();
            dataRow[bankaccountds.BankAccountsTable.ACCOUNTTYPE] = ComboAccoutType.SelectedValue;
            dataRow[bankaccountds.BankAccountsTable.INSTITUTIONID] = ComboInstitute.SelectedValue;
            dataRow[bankaccountds.BankAccountsTable.STATUS] = ComboStatus.SelectedValue;
            dataRow[bankaccountds.BankAccountsTable.BSBNO] = TextBSB1.Text + "-" + TextBSB2.Text;

            if (ComboAccoutType.SelectedValue.ToLower() == "termdeposit")
            {
                var table = GetTermDepositAccounts();
                if (!string.IsNullOrEmpty(ComboBroker.SelectedValue))
                {
                    var iddr =
                        table.AsEnumerable().FirstOrDefault(
                            ss => (Guid)ss[table.ID] == Guid.Parse(ComboBroker.SelectedValue));
                    if (iddr != null)
                    {
                        dataRow[bankaccountds.BankAccountsTable.BROKERID] = iddr[table.CLID].ToString();
                        dataRow[bankaccountds.BankAccountsTable.BROKERCSID] = iddr[table.CSID].ToString();
                        dataRow[bankaccountds.BankAccountsTable.BROKER] = iddr[table.BROKERNAME].ToString();
                    }
                }
            }

            dataRow[bankaccountds.BankAccountsTable.CUSTOMERNO] = txtCustomerNo.Text.Trim();
            dataRow[bankaccountds.BankAccountsTable.ISEXTERNALACCOUNT] = chkExternal.Checked;

            dataTable.Rows.Add(dataRow);
            bankaccountds.Tables.Remove(bankaccountds.BankAccountsTable.TABLENAME);
            bankaccountds.Tables.Add(dataTable);
            return bankaccountds;
        }

        private void ClearEntity()
        {
            TextCsid.Value = TextClid.Value = TextCID.Value = Guid.Empty.ToString();
            txttName.Text = string.Empty;
            if (ComboAccoutType.Items.Count > 0)
                ComboAccoutType.SelectedIndex = 0;
            if (ComboStatus.Items.Count > 0)
                ComboStatus.SelectedIndex = 0;
            SetBrokerVisibilty(false);
            TextAccountNumber.Text = string.Empty;
            TextBSB1.Text = string.Empty;
            TextBSB2.Text = string.Empty;
            if (ComboInstitute.Items.Count > 0)
            {
                ComboInstitute.SelectedIndex = 0;
            }
            lblMessages.Text = string.Empty;
            txtCustomerNo.Text = string.Empty;
            chkExternal.Checked = false;
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            if (Validate() == false)
            {
                if (ValidationFailed != null)
                    ValidationFailed();
                return;
            }

            var cid = UpdateData();


            if (Saved != null)
            {
                Saved(cid, new Guid(TextClid.Value), new Guid(TextCsid.Value));
            }

        }

        private Guid UpdateData()
        {
            var bankaccountDS = new BankAccountDS();
            // distributionsDetailsDs.SecID = new Guid(TextSecID.Value);
            var cid = Guid.Parse(TextCID.Value);
            if (cid == Guid.Empty)
            {
                //bankaccountDS.ID = Guid.NewGuid();
                bankaccountDS.CommandType = DatasetCommandTypes.Add;
                var unit = new OrganizationUnit
                {
                    Name = txttName.Text,
                    Type = ((int)OrganizationType.BankAccount).ToString(),
                    CurrentUser = (Page as UMABasePage).GetCurrentUser()
                };

                bankaccountDS.Unit = unit;
                bankaccountDS.Command = (int)WebCommands.AddNewOrganizationUnit;
            }
            else
            {
                //bankaccountDS.ID = id;
                bankaccountDS.CommandType = DatasetCommandTypes.Update;
            }
            if (Broker != null)
            {
                //bankaccountDS.Entity = this.GetEntity();
                //distributionsDetailsDs.Entity.ID = distributionsDetailsDs.ID;
                bankaccountDS.BankAccountsTable = GetEntity(bankaccountDS).BankAccountsTable;
                if (SaveData != null)
                {
                    SaveData(cid, bankaccountDS);
                    if (bankaccountDS.CommandType == DatasetCommandTypes.Add)
                    {
                        cid = bankaccountDS.Unit.Cid;
                        TextCID.Value = cid.ToString();
                        TextClid.Value = bankaccountDS.Unit.Clid.ToString();
                        TextCsid.Value = bankaccountDS.Unit.Csid.ToString();
                    }
                }
            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            //TextID.Value = distributionsDetailsDs.Entity.ID.ToString();
            return new Guid();
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (Canceled != null)
            {
                Canceled();
            }
        }
        //protected void ComboAccoutType_OnSelectedIndexChanged(object sender, C1ComboBoxEventArgs args)
        protected void ComboAccoutType_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            //ComboAccoutType.SelectedIndex = e.NewSelectedIndex;
            var istermdeposit = ComboAccoutType.SelectedValue.ToLower() == "termdeposit";
            SetBrokerVisibilty(istermdeposit);
        }

        private void SetBrokerVisibilty(bool istermdeposit)
        {

            lblBroker.Visible = istermdeposit;
            if (istermdeposit)
            {
                ComboBroker.Attributes["style"] = "display:block;";
                ComboBroker.SelectedIndex = -1;
                ComboBroker.Text = "";
            }
            else
            {
                ComboBroker.Attributes["style"] = "display:none;";
            }
        }

        private bool IsAlreadyPresent(bool isNew, string bsb, string accountNo, Guid clid, Guid csid)
        {
            if (Broker != null)
            {
                var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);
                var allAccounts = new BankAccountDS
                                      {
                                          Command = (int)WebCommands.GetOrganizationUnitsByType,
                                          Unit = new OrganizationUnit
                                                     {
                                                         CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                                                         Type = ((int)OrganizationType.BankAccount).ToString()
                                                     }
                                      };
                org.GetData(allAccounts);

                DataRow[] rows;

                if (isNew)
                {
                    rows =
                       allAccounts.Tables[allAccounts.BankAccountsTable.TABLENAME].Select(
                           string.Format("{0}='{1}' and {2}='{3}'",
                           allAccounts.BankAccountsTable.BSBNO,
                           bsb, allAccounts.BankAccountsTable.ACCOUNTNO, accountNo));
                }
                else
                {
                    rows =
                           allAccounts.Tables[allAccounts.BankAccountsTable.TABLENAME].Select(
                               string.Format("{0}<>'{1}' and {2}<>'{3}' and {4}='{5}' and {6}='{7}'",
                                             allAccounts.BankAccountsTable.CLID, clid.ToString(), allAccounts.BankAccountsTable.CSID,
                                             csid.ToString(), allAccounts.BankAccountsTable.BSBNO, bsb,
                                             allAccounts.BankAccountsTable.ACCOUNTNO, accountNo));
                }
                return rows.Length > 0;
            }
            return false;
        }
    }
}