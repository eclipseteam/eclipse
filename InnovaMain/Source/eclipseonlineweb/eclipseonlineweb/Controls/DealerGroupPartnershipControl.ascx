﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DealerGroupPartnershipControl.ascx.cs"
    EnableViewState="true" Inherits="eclipseonlineweb.Controls.DealerGroupPartnershipControl" %>
<%@ Register Src="PhoneNumberControl.ascx" TagName="PhoneNumberControl" TagPrefix="uc1" %>
<%@ Register Src="TFN_InputControl.ascx" TagName="TFN_InputControl" TagPrefix="uc2" %>
<%@ Register Src="ABN_InputControl.ascx" TagName="ABN_InputControl" TagPrefix="uc3" %>
<%@ Register Src="MobileNumberControl.ascx" TagName="MobileNumberControl" TagPrefix="uc1" %>
<script type="text/javascript">
    function SetTradingName(sender, args) {
        var TradingName = document.getElementById("<%:txtTradingName.ClientID %>").value;
        var chkthp = document.getElementById('<%: chkSameAsTrading.ClientID %>');
        var txtSgcPercentSalary = $find("<%:txtLegalName.ClientID %>");
        if (TradingName != "" && args.get_checked()) {
            txtSgcPercentSalary.set_value(TradingName);
            txtSgcPercentSalary.disable();
        }
        else {
            txtSgcPercentSalary.enable();
        }
    }
    function OnValueChangingTradingName(sender, args) {
        var TradingName = document.getElementById("<%:txtTradingName.ClientID %>").value;
        var txtSgcPercentSalary = $find("<%:txtLegalName.ClientID %>");
        var button = $find("<%: chkSameAsTrading.ClientID%>");
        if (button.get_checked() && TradingName != "") {
            txtSgcPercentSalary.set_value(TradingName);
            txtSgcPercentSalary.disable();
        }
        else {
            txtSgcPercentSalary.enable();
        }
    }
</script>
<fieldset style="width: 98%">
    <div style="text-align: left; float: none; border: 1px; background-color: White;
        height: 25px; position: relative; vertical-align: middle; margin: 0; padding: 0;">
        <telerik:RadButton ID="btnSaveDealerIndivisual" runat="server" Width="34px" Height="32px"
            ValidationGroup="VGPC" ToolTip="Save Changes" OnClick="btnSaveDealerIndivisual_Click">
            <Image ImageUrl="~/images/Save-Icon.png" />
        </telerik:RadButton>
        <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Visible="false"></asp:Label>
    </div>
</fieldset>
<table id="TD_EditMode" runat="server" width="100%">
    <tr>
        <td>
            <fieldset>
                <asp:Panel ID="pnlDealerGroupSoleTrader" runat="server">
                    <table width="100%">
                        <tr>
                            <td style="width: 7%;">
                                <span class="riLabel">Status Flag</span>
                                <asp:HiddenField ID="hfCID" runat="server" />
                                <asp:HiddenField ID="hf_DealerGroupType" runat="server" />
                            </td>
                            <td style="width: 7%;">
                                <telerik:RadComboBox ID="ddlStatusFlag" runat="server" Width="196px">
                                </telerik:RadComboBox>
                            </td>
                            <td style="width: 6.5%">
                                <span class="riLabel">Client ID</span>
                            </td>
                            <td style="width: 20%;">
                                <telerik:RadTextBox ID="txtClientID" runat="server" ReadOnly="true" Width="196px">
                                </telerik:RadTextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </fieldset>
        </td>
    </tr>
</table>
<fieldset>
    <asp:Panel ID="pnlDealerGroupSoleTraderDetail" runat="server">
        <table width="100%">
            <tr>
                <td>
                    <span class="riLabel">Title*</span>
                </td>
                <td>
                    <telerik:RadComboBox ID="ddlSoleTraderTitle" runat="server" Width="196px">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="Mr" Value="Mr" Selected="true" />
                            <telerik:RadComboBoxItem runat="server" Text="Ms" Value="Ms" />
                            <telerik:RadComboBoxItem runat="server" Text="Mrs" Value="Mrs" />
                            <telerik:RadComboBoxItem runat="server" Text="Miss" Value="Miss" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td>
                    <span class="riLabel">Preferred Name</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtPreferredName" runat="server" Width="196px">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Trading Name*</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtTradingName" runat="server" Width="196px">
                        <ClientEvents OnBlur="OnValueChangingTradingName" />
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="RfvTradingName" runat="server" ErrorMessage="*" ForeColor="Red"
                        ControlToValidate="txtTradingName" ValidationGroup="VGPC"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <span class="riLabel">Legal Name*</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtLegalName" runat="server" Width="196px">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="RfvtxtLegalName" runat="server" ErrorMessage="*"
                        ForeColor="Red" ControlToValidate="txtLegalName" ValidationGroup="VGPC"></asp:RequiredFieldValidator>
                    <telerik:RadButton ID="chkSameAsTrading" runat="server" Text="Same as Trading Name"
                        OnClientCheckedChanged="SetTradingName" AutoPostBack="false" ToggleType="CheckBox"
                        ButtonType="ToggleButton">
                    </telerik:RadButton>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Surname*</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtSoleTraderSurname" runat="server" Width="196px">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="RFVtxtSuraName" runat="server" ErrorMessage="*" ForeColor="Red"
                        ControlToValidate="txtSoleTraderSurname" ValidationGroup="VGPC"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <span class="riLabel">Email Address</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtSoleTraderEmail" runat="server" Width="196px">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Gender*</span>
                </td>
                <td>
                    <telerik:RadComboBox ID="ddlSoleTraderGender" runat="server" Width="196px">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="Male" Value="Male" Selected="true" />
                            <telerik:RadComboBoxItem runat="server" Text="Female" Value="Female" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td>
                    <span class="riLabel">Facsimile</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="FacsimleNo" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">ABN</span>
                </td>
                <td>
                    <uc3:ABN_InputControl ID="ABNNo" runat="server" InvalidMessage="Invalid ABN (e.g. 53 004 085 616)" />
                </td>
                <td>
                    <span class="riLabel">TFN</span>
                </td>
                <td>
                    <uc2:TFN_InputControl ID="TFNNo" runat="server" InvalidMessage="Invalid TFN (e.g. 123 456 782)" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Occupation</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtSoleTraderOccupation" runat="server" Width="196px">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <span class="riLabel">Work Phone Number</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="WorkPhNo" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Mobile Phone Number</span>
                </td>
                <td>
                    <uc1:MobileNumberControl ID="MobNo" runat="server" />
                </td>
                <td>
                    <span class="riLabel">Home Phone Number</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="HomePhNo" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center;">
                    <asp:Label runat="server" ID="txtMessage" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
</fieldset>
