﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb.Controls
{
    public partial class ExistingContactsControl : UserControl
    {
        public bool SingleSelect
        {
            get
            {
                if (!string.IsNullOrEmpty(hfSingleSelect.Value))
                {
                    return Convert.ToBoolean(hfSingleSelect.Value);
                }
                return false;
            }
            set { hfSingleSelect.Value = value.ToString(); }
        }
        private ICMBroker UMABroker { get { return (Page as UMABasePage).UMABroker; } }
        public Action PageRefresh { get; set; }
        public Action CanceledPopup { get; set; }
        public Action ValidationFailed { get; set; }
        public Action<InstitutionContactsDS> SaveData { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void GetContactData(object sender, GridNeedDataSourceEventArgs e)
        {
            if (txtSearchAccName.Text.Trim() != "")
            {
                dvExistingAccGrid.Visible = true;
                var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
                IndividualDS allIndividuals = new IndividualDS();
                allIndividuals.Command = (int)WebCommands.GetOrganizationUnitsByType;
                allIndividuals.Unit = new OrganizationUnit { Name = txtSearchAccName.Text, CurrentUser = (Page as UMABasePage).GetCurrentUser(), Type = ((int)OrganizationType.Individual).ToString() };
                org.GetData(allIndividuals);
                DataView summaryView = new DataView(allIndividuals.Tables[allIndividuals.IndividualTable.TABLENAME]);
                summaryView.Sort = allIndividuals.IndividualTable.NAME + " ASC";
                RGDContacts.DataSource = summaryView;
                UMABroker.ReleaseBrokerManagedComponent(org);
            }
            else
            {
                dvExistingAccGrid.Visible = false;
            }
        }
        public void ClearEntity()
        {
            txtSearchAccName.Text = "";
            lblmessages.Text = "";
            RGDContacts.Rebind();
        }
        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            if (SaveData != null)
            {
                InstitutionContactsDS institutionContactsDs = new InstitutionContactsDS();

                if (RGDContacts.Items.Count > 0)
                {
                    int selectCount = 0;
                    bool iSSelected = false;
                    foreach (GridDataItem item in RGDContacts.Items)//loops through each grid row
                    {
                        Guid clid = Guid.Empty;
                        Guid csid = Guid.Empty;
                        Guid cid;
                        Guid adviserClid = Guid.Empty;
                        Guid adviserCsid = Guid.Empty;
                        Guid adviserCid = Guid.Empty;
                        var checkCell = (item.FindControl("chkContactAssociation") as CheckBox);
                        if (checkCell.Checked)
                        {
                            selectCount++;
                            iSSelected = true;
                            clid = new Guid(item["Clid"].Text);
                            csid = new Guid(item["Csid"].Text);
                            cid = new Guid(item["Cid"].Text);
                            adviserClid = new Guid(item["AdviserClid"].Text);
                            adviserCsid = new Guid(item["AdviserCsid"].Text);
                            adviserCid = new Guid(item["AdviserCid"].Text);
                            DataRow datarow = institutionContactsDs.IndividualTable.NewRow();
                            datarow[institutionContactsDs.IndividualTable.CLID] = clid;
                            datarow[institutionContactsDs.IndividualTable.CSID] = csid;
                            datarow[institutionContactsDs.IndividualTable.CID] = cid;
                            datarow[institutionContactsDs.IndividualTable.ADVISERCLID] = adviserClid;
                            datarow[institutionContactsDs.IndividualTable.ADVISERCSID] = adviserCsid;
                            datarow[institutionContactsDs.IndividualTable.ADVISERCID] = adviserCid;
                            institutionContactsDs.IndividualTable.Rows.Add(datarow);
                        }
                    }
                    if (!iSSelected)
                    {
                        lblmessages.Text = "Please select contact(s) for association";
                        return;
                    }
                    if (SingleSelect && selectCount > 1)
                    {
                        lblmessages.Text = "Multiple selection is not allowed.";
                        return;
                    }
                    SaveData(institutionContactsDs);
                    if (institutionContactsDs.ExtendedProperties["isError"] != null && institutionContactsDs.ExtendedProperties["isError"].ToString() == "true")
                    {
                        return;
                    }
                }
                if (PageRefresh != null)
                {
                    PageRefresh();
                }
                ClearEntity();
                if (CanceledPopup != null)
                {
                    CanceledPopup();
                }
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            lblmessages.Text = string.Empty;
            RGDContacts.Rebind();
            if (ValidationFailed != null)
            {
                ValidationFailed();
            }
        }
    }
}