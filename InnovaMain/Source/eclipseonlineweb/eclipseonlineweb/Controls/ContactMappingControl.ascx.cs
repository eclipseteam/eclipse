﻿using System;
using System.Data;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;


namespace eclipseonlineweb.Controls
{
    public partial class ContactMappingControl : UserControl
    {
        private string _cid;
        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }
        private DataView BusinessTitleDataView { get; set; }

        public string IndividualsType
        {
            get { return HfIndividualsType.Value; }
            set { HfIndividualsType.Value = value; }
        }

        public bool SingleSelect
        {
            get
            {
                if (!string.IsNullOrEmpty(hfSingleSelect.Value))
                {
                    return Convert.ToBoolean(hfSingleSelect.Value);
                }
                return false;
            }
            set
            {
                hfSingleSelect.Value = value.ToString();
                ExistingContactControl.SingleSelect = value;
            }
        }
        public Action<DataSet> SaveOrg { get; set; }
        public Action<string, DataSet> SaveUnit { get; set; }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            #region add existing events

            ExistingContactControl.CanceledPopup += () => ShowHideExisting(false);
            ExistingContactControl.ValidationFailed += () => ShowHideExisting(true);
            ExistingContactControl.SaveData += contactDetail =>
                                                   {
                                                       bool isError = false;
                                                       switch ((IndividualsType)Enum.Parse(typeof(IndividualsType), IndividualsType))
                                                       {
                                                           case Oritax.TaxSimp.DataSets.IndividualsType.Individual:
                                                               foreach (DataRow row in contactDetail.IndividualTable.Rows)
                                                               {
                                                                   if (row[contactDetail.IndividualTable.ADVISERCID].ToString() != Guid.Empty.ToString() && _cid != row[contactDetail.IndividualTable.ADVISERCID].ToString())
                                                                   {
                                                                       var lblMsg = (Label)ExistingContactControl.FindControl("lblmessages");
                                                                       if (lblMsg != null)
                                                                           lblMsg.Text = "This Individual is already bonded to another advisor";
                                                                       isError = true;
                                                                       break;
                                                                   }
                                                               }

                                                               break;
                                                       }
                                                       if (isError)
                                                       {
                                                           contactDetail.ExtendedProperties["isError"] = "true";
                                                           return;
                                                       }
                                                       contactDetail.ExtendedProperties["isError"] = "false";
                                                       ConvertIndidivualData(contactDetail, DatasetCommandTypes.Update);
                                                       ShowHideExisting(false);
                                                   };
            #endregion

            IndividualControl.Canceled += () => HideShowIndividualGrid(false);
            IndividualControl.SaveData += (Cid, ds) =>
            {
                if (Cid == Guid.Empty.ToString())
                {
                    if (SaveOrg != null) SaveOrg(ds);
                }
                else
                {
                    if (SaveUnit != null) SaveUnit(Cid, ds);
                }
                HideShowIndividualGrid(false);
            };

            IndividualControl.ValidationFailed += () => HideShowIndividualGrid(true);

            IndividualControl.Saved += (CID, CLID, CSID) =>
            {
                HideShowIndividualGrid(false);
                SaveContacts(CID, CLID, CSID, IndividualControl.BusinessTitle, DatasetCommandTypes.Update);
            };
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            _cid = Request.QueryString["ins"];
        }

        private void ConvertIndidivualData(InstitutionContactsDS contactDetail, DatasetCommandTypes datasetCommandTypes)
        {
            var individuls = new IndividualDS();
            foreach (DataRow row in contactDetail.IndividualTable.Rows)
            {
                var newrow = individuls.IndividualTable.NewRow();
                newrow[individuls.IndividualTable.CLID] = row[contactDetail.IndividualTable.CLID];
                newrow[individuls.IndividualTable.CSID] = row[contactDetail.IndividualTable.CSID];
                newrow[individuls.IndividualTable.CID] = row[contactDetail.IndividualTable.CID];
                newrow[individuls.IndividualTable.ADVISERCLID] = row[contactDetail.IndividualTable.ADVISERCLID];
                newrow[individuls.IndividualTable.ADVISERCSID] = row[contactDetail.IndividualTable.ADVISERCSID];
                newrow[individuls.IndividualTable.ADVISERCID] = row[contactDetail.IndividualTable.ADVISERCID];
                individuls.IndividualTable.Rows.Add(newrow);
            }
            SaveIndividualData(individuls, datasetCommandTypes);
        }
        protected void LnkbtnAddNewIndividualClick(object sender, EventArgs e)
        {
            Guid gCid = Guid.Empty;
            IndividualControl.SetEntity(gCid);
            HideShowIndividualGrid(true);
        }
        protected void lnkbtnAddExistngMembers_Click(object sender, EventArgs e)
        {
            ShowHideExisting(true);
        }

        protected void btnClose_OnClick(object sender, EventArgs e)
        {
            ShowHideExisting(false);
        }
        private void ShowHideExisting(bool show)
        {
            OVER.Visible = MemershipPopup.Visible = show;
        }
        private void HideShowIndividualGrid(bool show)
        {
            OVER.Visible = IndividualModal.Visible = show;
        }

        private void SaveIndividualData(IndividualDS individualDs, DatasetCommandTypes commandType)
        {
            individualDs.CommandType = commandType;
            individualDs.IndividualsType = (IndividualsType)Enum.Parse(typeof(IndividualsType), IndividualsType);
            if (SaveUnit != null)
                SaveUnit(_cid, individualDs);

            PresentationGrid.Rebind();
        }

        private void SaveContacts(Guid cid, Guid clid, Guid csid, string bussinessTitle, DatasetCommandTypes commandType)
        {
            var ds = new IndividualDS();
            DataRow dr = ds.IndividualTable.NewRow();
            dr[ds.IndividualTable.CID] = cid;
            dr[ds.IndividualTable.CLID] = clid;
            dr[ds.IndividualTable.CSID] = csid;
            dr[ds.IndividualTable.BUSINESSTITLE] = bussinessTitle;
            ds.Tables[ds.IndividualTable.TABLENAME].Rows.Add(dr);
            SaveIndividualData(ds, commandType);
            PresentationGrid.Rebind();
        }

        private void GetData()
        {
            if (!Request.Params["ins"].IsEmptyOrNull())
            {
                _cid = Request.Params["ins"];

                IBrokerManagedComponent clientData = Broker.GetBMCInstance(new Guid(_cid));
                var ds = new IndividualDS { IndividualsType = (IndividualsType)Enum.Parse(typeof(IndividualsType), IndividualsType) };
                clientData.GetData(ds);
                Broker.ReleaseBrokerManagedComponent(clientData);
                var summaryView = new DataView(ds.Tables[ds.IndividualTable.TABLENAME]) { Sort = ds.IndividualTable.FULLNAME + " ASC" };
                PresentationGrid.DataSource = summaryView;
            }
        }

        protected void DdlBusinessTitle_OnSelectedIndexChanged(object sender, EventArgs e)
        {

            var ddl = (sender as RadComboBox);
            if (ddl != null && ddl.SelectedValue != null)
                SaveContacts(Guid.Parse(ddl.Attributes["CID"]), Guid.Parse(ddl.Attributes["CLID"]), Guid.Parse(ddl.Attributes["CSID"]), ddl.SelectedValue, DatasetCommandTypes.Update);
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            switch (e.CommandName.ToLower())
            {
                case "detail":
                    var sigCid = new Guid(dataItem["CID"].Text);
                    IndividualControlView.SetEntity(sigCid);
                    ViewPanel.Visible = true;
                    break;
                case "edit":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var sigCid1 = new Guid(dataItem["CID"].Text);
                    HideShowIndividualGrid(true);
                    IndividualControl.SetEntity(sigCid1);
                    IndividualControl.BusinessTitle = (((RadComboBox)(dataItem["BusinessTitle"].Controls[1]))).SelectedValue;
                    break;
                case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var linkEntiyCLID = new Guid(dataItem["CLID"].Text);
                    var linkEntiyCSID = new Guid(dataItem["CSID"].Text);
                    var linkEntiyCID = new Guid(dataItem["CID"].Text);
                    var businessTitle = (((RadComboBox)(dataItem["BusinessTitle"].Controls[1]))).SelectedValue;
                    SaveContacts(linkEntiyCID, linkEntiyCLID, linkEntiyCSID, businessTitle, DatasetCommandTypes.Delete);
                    break;
            }
        }

        protected void PresentationGrid_OnItemDataBound(object sender, GridItemEventArgs e)
        {
        }

        protected void BtnCloseView_Click(object sender, EventArgs e)
        {
            ViewPanel.Visible = false;
        }

        protected void PresentationGrid_OnPreRender(object sender, EventArgs e)
        {
            if (SingleSelect && PresentationGrid.MasterTableView.Items.Count > 0)
            {
                GridItem cmdItem = PresentationGrid.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                var btnAddNew = cmdItem.FindControl("btnAddNew") as LinkButton;
                var btnAddExisting = cmdItem.FindControl("btnAddExisting") as LinkButton;

                if (btnAddNew != null)
                    btnAddNew.Visible = false;
                if (btnAddExisting != null)
                    btnAddExisting.Visible = false;
            }
        }
    }
}