﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;

namespace eclipseonlineweb.Controls
{
    public partial class DesktopBrokerExistingAccountControl : UserControl
    {
        private ICMBroker UMABroker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }
        public Action<DesktopBrokerAccountDS> Saved
        {
            get;
            set;
        }



        public Action<string, DataSet> SaveData
        {
            get;
            set;
        }

        public bool SelectedGridView
        {
            get { return dvSelectedGrid.Visible; }

            set { dvSelectedGrid.Visible = value; }
        }
        public Action Canceled { get; set; }

        public Action ValidationFailed { get; set; }

        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        public void VisibleExistingAccountGrid()
        {
            if (!string.IsNullOrEmpty(txtSearchAccName.Text))
            {
                dvExistingAccGrid.Visible = true;
                gvDesktopBroker.Visible = true;
                LoadExistingDesktopBrokerAccounts();
            }

            Title.Text = "Availabe ASX Acocunts";
        }

        private void LoadExistingDesktopBrokerAccounts()
        {
            Title.Text = "Availabe ASX Acocunts";
            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            DesktopBrokerAccountDS allAccounts = new DesktopBrokerAccountDS();
            allAccounts.Command = (int)WebCommands.GetOrganizationUnitsByType;
            allAccounts.Unit = new OrganizationUnit { Name = txtSearchAccName.Text, CurrentUser = (Page as UMABasePage).GetCurrentUser(), Type = ((int)OrganizationType.DesktopBrokerAccount).ToString() };
            org.GetData(allAccounts);
            DataView summaryView = new DataView(allAccounts.Tables[allAccounts.DesktopBrokerAccountsTable.TABLENAME]);
            summaryView.Sort = allAccounts.DesktopBrokerAccountsTable.ACCOUNTNAME + " DESC";
            gvDesktopBroker.DataSource = summaryView;
            UMABroker.ReleaseBrokerManagedComponent(org);
        }

        public void SetEntity(Guid cid)
        {
            ClearEntity();
            hfCID.Value = cid.ToString();
            if (cid != Guid.Empty)
            {
                DesktopBrokerAccountDS ds = GetDataSet(cid);
                DataRow dr = ds.Tables[ds.DesktopBrokerAccountsTable.TABLENAME].Rows[0];
                DataView dv = dr.Table.DefaultView;
                gvExistingDesktopBroker.DataSource = dv;
                gvExistingDesktopBroker.DataBind();
                hfCID.Value = dr[0].ToString();
                hfCLID.Value = dr[1].ToString();
                hfCSID.Value = dr[2].ToString();
            }
        }

        public void SetEntity(Guid cid, Guid csid)
        {
            ClearEntity();
            hfCID.Value = cid.ToString();
            hfCSID.Value = csid.ToString();
            if (cid != Guid.Empty && csid != Guid.Empty)
            {
                DesktopBrokerAccountDS ds = GetDataSet(cid);
                DataRow dr = ds.Tables[ds.DesktopBrokerAccountsTable.TABLENAME].Rows[0];
                DataView dv = dr.Table.DefaultView;
                gvExistingDesktopBroker.DataSource = dv;
                gvExistingDesktopBroker.DataBind();
                hfCID.Value = dr[0].ToString();
                hfCLID.Value = dr[1].ToString();
                hfCSID.Value = dr[2].ToString();
            }
        }


        private DesktopBrokerAccountDS GetDataSet(Guid cid)
        {
            DesktopBrokerAccountDS desktopBrokerAccountDs = new DesktopBrokerAccountDS { CommandType = DatasetCommandTypes.Get };

            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
                clientData.GetData(desktopBrokerAccountDs);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            return desktopBrokerAccountDs;
        }

        public void ClearEntity()
        {
            hfCID.Value = Guid.Empty.ToString();
            hfCLID.Value = Guid.Empty.ToString();
            hfCSID.Value = Guid.Empty.ToString();
        }

        private void UpdateBroker()
        {
            if (dvDesktopExistingAccGrid.Visible)
            {
                //Check how many checkboxes r checked
                int selectCount = 0;
                int selectedRowIndex = -1;
                foreach (GridDataItem item in gvDesktopBroker.MasterTableView.Items)
                {
                    var chk = (CheckBox)item.FindControl("CheckBox");
                    if (chk.Checked)
                    {
                        selectCount++;
                        selectedRowIndex = item.RowIndex;
                        if (selectCount > 1)
                        {
                            lblmessages.Text = "Multiple Account selection is not allowed.";
                            gvDesktopBroker.Visible = true;
                            SelectedGridView = true;
                            if (ValidationFailed != null)
                            {
                                ValidationFailed();
                                return;
                            }
                        }
                    }
                }


                //If any check Box Not Checked
                if (selectedRowIndex == -1)
                {
                    lblmessages.Text = "Please Select Account.";
                    gvDesktopBroker.Visible = true;
                    SelectedGridView = true;
                    if (ValidationFailed != null)
                    {
                        ValidationFailed();
                    }
                    return;
                }


                foreach (GridDataItem item in gvDesktopBroker.MasterTableView.Items)
                {
                    var checkCell = (CheckBox)item.FindControl("CheckBox");
                    if (checkCell.Checked)
                    {
                        DesktopBrokerAccountDS desktopBrokerAccountDs = new DesktopBrokerAccountDS();
                        DataTable dataTable = desktopBrokerAccountDs.Tables[desktopBrokerAccountDs.DesktopBrokerAccountsTable.TABLENAME];
                        DataRow dataRow = dataTable.NewRow();
                        dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.CID] = new Guid(item["Cid"].Text);
                        dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.CLID] = new Guid(item["CLid"].Text);
                        dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.CSID] = new Guid(item["CSid"].Text);
                        dataTable.Rows.Add(dataRow);
                        desktopBrokerAccountDs.Tables.Remove(desktopBrokerAccountDs.DesktopBrokerAccountsTable.TABLENAME);
                        desktopBrokerAccountDs.Tables.Add(dataTable);
                        if (desktopBrokerAccountDs != null)
                        {
                            if (Saved != null)
                            {
                                Saved(desktopBrokerAccountDs);
                            }
                            txtSearchAccName.Text = "";
                            lblmessages.Text = "";
                            gvDesktopBroker.Visible = false;
                            SelectedGridView = false;
                        }
                    }
                }
            }
            else
            {
                var desktopBrokerAccountDs = new DesktopBrokerAccountDS();
                DataTable dataTable = desktopBrokerAccountDs.Tables[desktopBrokerAccountDs.DesktopBrokerAccountsTable.TABLENAME];
                foreach (GridDataItem item in gvDesktopBroker.MasterTableView.Items)
                {
                    var checkCell = (CheckBox)item.FindControl("CheckBox");
                    if (checkCell.Checked)
                    {
                        DataRow dataRow = dataTable.NewRow();
                        dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.CID] = new Guid(item["Cid"].Text); ;
                        dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.CLID] = new Guid(item["CLid"].Text);
                        dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.CSID] = new Guid(item["CSid"].Text);
                        dataTable.Rows.Add(dataRow);
                        desktopBrokerAccountDs.Tables.Remove(desktopBrokerAccountDs.DesktopBrokerAccountsTable.TABLENAME);
                        desktopBrokerAccountDs.Tables.Add(dataTable);
                    }
                }

                if (desktopBrokerAccountDs != null)
                {
                    if (Saved != null)
                        Saved(desktopBrokerAccountDs);

                    txtSearchAccName.Text = "";
                    lblmessages.Text = "";
                    gvDesktopBroker.Visible = false;
                    SelectedGridView = false;

                }
            }
        }

        protected void gvDesktopBroker_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            VisibleExistingAccountGrid();
        }

        protected void gvExistingDesktopBroker_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            SetEntity(new Guid(hfCID.Value));

        }

        protected void btnUpdate_Onclick(object sender, EventArgs e)
        {
            UpdateBroker();
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (Canceled != null)
            {
                Canceled();
                txtSearchAccName.Text = "";
                lblmessages.Text = "";
                SelectedGridView = false;
                gvDesktopBroker.Visible = false;
            }
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            VisibleExistingAccountGrid();
            if (ValidationFailed != null)
            {
                ValidationFailed();
            }
        }
    }
}