﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Words.Tables;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;
using System.Web;
using System.Linq;
using System.IO;
using Aspose.Words;

namespace eclipseonlineweb.Controls
{
    public partial class BuyTDCash : UserControl
    {
        private DataView _dataView;
        private DataView _detailDataView;
        private DataView _tdHoldingDataView;
        private OrderPadRatesDS _presentationData;

        public string ClientCID
        {
            private get
            {
                return hfClientCID.Value;
            }
            set
            {
                hfClientCID.Value = value;
            }
        }

        private string ClientAccID
        {
            get
            {
                return hfClientID.Value;
            }
            set
            {
                hfClientID.Value = value;
            }
        }

        public bool IsAdmin
        {
            private get
            {
                return Convert.ToBoolean(hfIsAdmin.Value);
            }
            set
            {
                hfIsAdmin.Value = value.ToString();
            }
        }

        public bool IsAdminMenu
        {
            private get
            {
                if (string.IsNullOrEmpty(hfIsAdminMenu.Value))
                {
                    hfIsAdminMenu.Value = "false";
                }
                return Convert.ToBoolean(hfIsAdminMenu.Value);
            }
            set
            {
                hfIsAdminMenu.Value = value.ToString();
            }
        }

        private bool IsSMA
        {
            get
            {
                if (string.IsNullOrEmpty(hfIsSMA.Value))
                {
                    hfIsSMA.Value = "false";
                }
                return Convert.ToBoolean(hfIsSMA.Value);
            }
            set
            {
                hfIsSMA.Value = value.ToString();
            }
        }

        private string[] TermList
        {
            get { return IsSMA ? _termListSMA : _termList; }
        }
        public Action<string, DataSet> SaveData { get; set; }

        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            searchAccountControl.SelectData += (clientCID, clientId, clientName) =>
            {
                ClientHeaderInfo1.SetEntity(new Guid(clientCID));
                ClientHeaderInfo1.Visible = true;
                ClientCID = clientCID;
                ClientAccID = clientId;
                SetClientManagementType();
                LoadControls();
                GetData();
                showDetails.Visible = true;
                PresentationGrid.Visible = true;
                PresentationGrid.Rebind();
                if (hidBackButton.Value != "1")
                {
                    PresentationGrid.Rebind();
                    hidBackButton.Value = "";
                }
                tdButtons.Visible = true;
                if (btnBack.Visible)
                {
                    lblMsg.Visible = false;
                    trMain.Style["Display"] = "";
                    btnBack.Visible = false;
                    btnSave.Visible = false;
                    imgItems.Visible = true;
                    PresentationDetailGrid.Visible = false;
                }
            };
            searchAccountControl.ShowHideModal += (show) =>
            {
                if (show)
                {
                    popupSearch.Show();
                }
                else
                {
                    popupSearch.Hide();
                }
            };


            lblMsg.Visible = false;

            if (!IsPostBack)
            {
                if (IsAdminMenu)
                {
                    btnSearch.Visible = true;
                    showDetails.Visible = false;
                    ClientHeaderInfo1.Visible = false;
                    PresentationGrid.Visible = false;
                    tdButtons.Visible = false;
                    searchAccountControl.IsAdminMenu = true;
                }
                else
                {
                    SetClientManagementType();
                    btnSearch.Visible = false;
                    showDetails.Visible = true;
                    ClientHeaderInfo1.Visible = true;
                    LoadControls();
                    PresentationGrid.Visible = true;
                    tdButtons.Visible = true;
                    searchAccountControl.IsAdminMenu = false;
                }
            }
            if (!IsAdminMenu || IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            IBrokerManagedComponent iBMC = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            var ds = new OrderPadRatesDS();
            ds.ClientManagementType = IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA;
            iBMC.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(iBMC);
            _dataView = new DataView(ds.Tables[OrderPadRatesDS.ORDERPADRATESTABLE])
                {
                    RowFilter = string.Format("{0}='{1}'", OrderPadRatesDS.CLIENTMANAGEMENTTYPE, IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA),
                    Sort = string.Format("{0} ASC, {1} DESC", OrderPadRatesDS.INSTITUTENAME, OrderPadRatesDS.DATE)
                };
            GetAccountProcessData();
        }

        private void GetMaxRates(DataView dv)
        {
            var ds = new OrderPadRatesDS();
            ds.Tables.Remove(OrderPadRatesDS.ORDERPADRATESTABLE);
            ds.Tables.Add(dv.ToTable(OrderPadRatesDS.ORDERPADRATESTABLE));

            IBrokerManagedComponent iBMC = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            ds.CommandType = DatasetCommandTypes.GetMax;
            iBMC.GetData(ds);
            //populating data to use during data bound
            _presentationData = ds;
            Broker.ReleaseBrokerManagedComponent(iBMC);
        }

        private void GetAccountProcessData()
        {
            if (!string.IsNullOrEmpty(ClientCID))
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(new Guid(ClientCID));
                var ds = new ClientAccountProcessDS();
                clientData.GetData(ds);
                Broker.ReleaseBrokerManagedComponent(clientData);
                _tdHoldingDataView = new DataView(ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE]) { RowFilter = string.Format("{0} = '{1}'", ClientAccountProcessDS.LINKEDENTITYTYPE, OrganizationType.TermDepositAccount) };
                _tdHoldingDataView = _tdHoldingDataView.ToTable(true, ClientAccountProcessDS.INSTITUTEID, ClientAccountProcessDS.TASKID, ClientAccountProcessDS.CID, ClientAccountProcessDS.SERVICETYPE, ClientAccountProcessDS.STATUS).DefaultView;
            }
        }

        private HoldingRptDataSet GetHoldingReportDS()
        {
            IBrokerManagedComponent clientData = Broker.GetBMCInstance(new Guid(ClientCID));
            var ds = new HoldingRptDataSet();
            clientData.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(clientData);
            return ds;
        }

        private void BindServiceType()
        {
            var ds = GetHoldingReportDS();
            OrderPadUtilities.FillServiceTypes(cmbServiceType, ds, IsAdmin, OrderAccountType.TermDeposit);
        }

        private void BindBankAccounts()
        {
            var ds = GetHoldingReportDS();

            //Getting cash bank account according to service type
            string filter = string.Format("{0}='{1}' and {2}='{3}' and {4}='cash'", ds.HoldingSummaryTable.LINKEDENTITYTYPE, OrganizationType.BankAccount, ds.HoldingSummaryTable.SERVICETYPE, cmbServiceType.SelectedValue, ds.HoldingSummaryTable.ASSETNAME);
            OrderPadUtilities.FillBankNAvailableFunds(cmbCashAccount, ds, filter, IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA, Broker);
            OrderPadUtilities.SetAvailableFunds(cmbCashAccount, lblAvailableFunds, lblCashBalance, lblMinCash, IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA);
        }

        private void LoadControls()
        {
            //TermList List 
            cmbMinTerm.DataSource = TermList;
            cmbMinTerm.DataBind();
            cmbMaxTerm.DataSource = TermList;
            cmbMaxTerm.DataBind();

            //Setting Default Min Term
            cmbMinTerm.SelectedIndex = 0;

            //Setting Default Max Term
            cmbMaxTerm.SelectedIndex = TermList.Length - 1;

            //Rating List
            cmbRating.DataSource = InstituteCombos.SPLongTermRatings;
            cmbRating.DataTextField = "Key";
            cmbRating.DataValueField = "Value";
            cmbRating.DataBind();

            //Setting Default Min Rating
            cmbRating.SelectedIndex = InstituteCombos.SPLongTermRatings.Count - 1;

            BindServiceType();
            BindBankAccounts();
        }

        #region Master

        protected void PresentationGridItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = e.Item as GridDataItem;
            if (dataItem["Rating"].Text.ToLower() == "unrated")
            {
                dataItem["Rating"].Text = string.Empty;
            }

            if (_tdHoldingDataView != null && _tdHoldingDataView.Count > 0)
            {
                foreach (DataRow dataRow in _tdHoldingDataView.ToTable().Rows)
                {
                    if (dataRow["InstituteID"].ToString() == dataItem["BrokerID"].Text && cmbServiceType.SelectedValue.ToLower() == dataRow["ServiceType"].ToString().ToLower())
                    {
                        dataItem["ProductID"].Text = dataRow["TaskID"].ToString();
                        dataItem["TDAccountCID"].Text = dataRow["CID"].ToString();
                        dataItem["TDAccountStatus"].Text = dataRow["Status"].ToString();

                        ShowHideTDCheckbox(dataItem, false);
                        break;
                    }
                    ShowHideTDCheckbox(dataItem, true);
                }
            }
            else
            {
                ShowHideTDCheckbox(dataItem, true);
            }

            var row = (DataRowView)e.Item.DataItem;

            if (row == null) return;

            foreach (GridColumn col in PresentationGrid.MasterTableView.Columns)
            {
                if (!_presentationData.Tables[OrderPadRatesDS.ORDERPADMAXRATESTABLE].Columns.Contains(col.SortExpression) || _presentationData.Tables[OrderPadRatesDS.ORDERPADMAXRATESTABLE].Rows.Count <= 0)
                    continue;

                if (!_presentationData.Tables[OrderPadRatesDS.ORDERPADMAXRATESTABLE].Rows[0][col.SortExpression].Equals(row[col.UniqueName]))
                    continue;

                switch (col.UniqueName)
                {
                    case "Days30":
                        ((GridDataItem)(e.Item))["30-D"].BackColor = Color.LightBlue;
                        break;
                    case "Days60":
                        ((GridDataItem)(e.Item))["60-D"].BackColor = Color.LightBlue;
                        break;
                    case "Days90":
                        ((GridDataItem)(e.Item))["90-D"].BackColor = Color.LightBlue;
                        break;
                    case "Days120":
                        ((GridDataItem)(e.Item))["120-D"].BackColor = Color.LightBlue;
                        break;
                    case "Days150":
                        ((GridDataItem)(e.Item))["150-D"].BackColor = Color.LightBlue;
                        break;
                    case "Days180":
                        ((GridDataItem)(e.Item))["180-D"].BackColor = Color.LightBlue;
                        break;
                    case "Days270":
                        ((GridDataItem)(e.Item))["270-D"].BackColor = Color.LightBlue;
                        break;
                    case "Years1":
                        ((GridDataItem)(e.Item))["Y-1"].BackColor = Color.LightBlue;
                        break;
                    case "Years2":
                        ((GridDataItem)(e.Item))["Y-2"].BackColor = Color.LightBlue;
                        break;
                    case "Years3":
                        ((GridDataItem)(e.Item))["Y-3"].BackColor = Color.LightBlue;
                        break;
                    case "Years4":
                        ((GridDataItem)(e.Item))["Y-4"].BackColor = Color.LightBlue;
                        break;
                    case "Years5":
                        ((GridDataItem)(e.Item))["Y-5"].BackColor = Color.LightBlue;
                        break;
                }
            }
        }

        private static void ShowHideTDCheckbox(GridDataItem dataItem, bool hide)
        {
            foreach (var control in dataItem.Controls)
            {
                if (control.GetType() == typeof(GridTableCell))
                {
                    var cell = (GridTableCell)control;
                    foreach (var ctrl in cell.Controls)
                    {
                        if (ctrl.GetType() == typeof(CheckBox))
                        {
                            if (hide)
                            {
                                dataItem["ProductID"].Text = string.Empty;
                                dataItem["TDAccountCID"].Text = string.Empty;
                                dataItem["TDAccountStatus"].Text = string.Empty;
                            }
                        }
                    }
                }
            }
        }

        protected void PresentationGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //Getting Max Rates for current view
            GetMaxRates(_dataView);
            PresentationGrid.DataSource = _dataView.ToTable();
        }

        #endregion

        #region Detail
        protected void PresentationDetailGridItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = (GridDataItem)e.Item;
                var txtAmountInvestment = (RadNumericTextBox)item.FindControl("txtAmount");
                if (hidMaxValue.Value != "")
                {
                    txtAmountInvestment.Text = hidMaxValue.Value;
                }
                
                if (IsSMA)
                {
                    var cmpValidator = (CompareValidator)item.FindControl("cmpValidateSMABalance");
                    cmpValidator.Enabled = true;
                }

            }
        }

        protected void PresentationDetailGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (_detailDataView != null) PresentationDetailGrid.DataSource = _detailDataView.ToTable();
        }
        #endregion

        protected void btnCutOff_OnClick(object sender, EventArgs e)
        {
            FileHelper.DownLoadFile(FileHelper.CutOffTimeFilePath, FileHelper.CutOffTimeFileName);
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;

            if (PresentationGrid.Visible)
            {
                hidBackButton.Value = "";
                hidChkCount.Value = "";
                hidMaxValue.Value = "";

                trMain.Style["Display"] = "none";
                PresentationGrid.Visible = false;
                PresentationDetailGrid.Visible = true;
                btnBack.Visible = true;
                btnSave.Visible = true;
                imgItems.Visible = false;

                CheckOrder();
            }
            else
            {
                PlaceOrder();
            }
        }

        protected void cmbCashAccount_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            OrderPadUtilities.SetAvailableFunds(cmbCashAccount, lblAvailableFunds, lblCashBalance, lblMinCash, IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA);
            PresentationGrid.Rebind();
        }

        protected void btnBack_OnClick(object sender, EventArgs e)
        {
            hidBackButton.Value = "1";
            lblMsg.Visible = false;
            trMain.Style["Display"] = "";
            PresentationGrid.Visible = true;
            PresentationDetailGrid.Visible = false;
            btnBack.Visible = false;
            btnSave.Visible = false;
            imgItems.Visible = true;
            if (hidBackButton.Value != "1")
            {
                PresentationGrid.Rebind();
            }
            hidBackButton.Value = "";
        }

        protected void btnFindTDCash_OnClick(object sender, EventArgs e)
        {
            string filter = string.Format("{0}<={1}", OrderPadRatesDS.PRICEINSTITUTELONGRATINGVALUE, cmbRating.SelectedValue);
            decimal amount = string.IsNullOrEmpty(txtAmounttoInvest.Text) ? 0 : Convert.ToDecimal(txtAmounttoInvest.Text);

            if (amount > 0)
            {
                filter += string.Format(" and {0}<={1}", OrderPadRatesDS.MIN, amount);
            }

            _dataView.RowFilter = filter;
            HideTermColumns();
            PresentationGrid.Rebind();
        }

        private void CheckValidationMaxValue()
        {
            hidChkCount.Value = "";
            int count = 0;
            foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
            {
                var chkDays30 = (dataItem.FindControl("chkDays30") as CheckBox);
                var chkDays60 = (dataItem.FindControl("chkDays60") as CheckBox);
                var chkDays90 = (dataItem.FindControl("chkDays90") as CheckBox);
                var chkDays120 = (dataItem.FindControl("chkDays120") as CheckBox);
                var chkDays150 = (dataItem.FindControl("chkDays150") as CheckBox);
                var chkDays180 = (dataItem.FindControl("chkDays180") as CheckBox);
                var chkDays270 = (dataItem.FindControl("chkDays270") as CheckBox);
                var chkYears1 = (dataItem.FindControl("chkYears1") as CheckBox);
                var chkYears2 = (dataItem.FindControl("chkYears2") as CheckBox);
                var chkYears3 = (dataItem.FindControl("chkYears3") as CheckBox);
                var chkYears4 = (dataItem.FindControl("chkYears4") as CheckBox);
                var chkYears5 = (dataItem.FindControl("chkYears5") as CheckBox);

                if (chkDays30.Checked)
                {
                    count = count + 1;
                }

                if (chkDays60.Checked)
                {
                    count = count + 1;
                }

                if (chkDays90.Checked)
                {
                    count = count + 1;
                }

                if (chkDays120.Checked)
                {
                    count = count + 1;
                }

                if (chkDays150.Checked)
                {
                    count = count + 1;
                }

                if (chkDays180.Checked)
                {
                    count = count + 1;
                }

                if (chkDays270.Checked)
                {
                    count = count + 1;
                }

                if (chkYears1.Checked)
                {
                    count = count + 1;
                }

                if (chkYears2.Checked)
                {
                    count = count + 1;
                }

                if (chkYears3.Checked)
                {
                    count = count + 1;
                }

                if (chkYears4.Checked)
                {
                    count = count + 1;
                }

                if (chkYears5.Checked)
                {
                    count = count + 1;
                }
            }
            if (count == 1 && txtAmounttoInvest.Text != "")
            {
                hidChkCount.Value = "1";
            }
            else
            {
                hidChkCount.Value = "";
            }
        }

        private void CheckOrder()
        {
            CheckValidationMaxValue();
            hfIsAlert.Value = "";
            hfTDAccStatus.Value = "";
            var ds = new OPTDCashDS();
            HoldingRptDataSet holdingDs = null;
            if (IsSMA)
            {
                holdingDs = GetHoldingReportDS();
            }
            foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
            {
                string duration = string.Empty, colName = string.Empty;

                var chkDays30 = (dataItem.FindControl("chkDays30") as CheckBox);
                var chkDays60 = (dataItem.FindControl("chkDays60") as CheckBox);
                var chkDays90 = (dataItem.FindControl("chkDays90") as CheckBox);
                var chkDays120 = (dataItem.FindControl("chkDays120") as CheckBox);
                var chkDays150 = (dataItem.FindControl("chkDays150") as CheckBox);
                var chkDays180 = (dataItem.FindControl("chkDays180") as CheckBox);
                var chkDays270 = (dataItem.FindControl("chkDays270") as CheckBox);
                var chkYears1 = (dataItem.FindControl("chkYears1") as CheckBox);
                var chkYears2 = (dataItem.FindControl("chkYears2") as CheckBox);
                var chkYears3 = (dataItem.FindControl("chkYears3") as CheckBox);
                var chkYears4 = (dataItem.FindControl("chkYears4") as CheckBox);
                var chkYears5 = (dataItem.FindControl("chkYears5") as CheckBox);

                if (chkDays30.Checked)
                {
                    duration = "30 Days";
                    colName = "Days30";
                    AddNewOrderItem(ds, duration, colName, dataItem, holdingDs);
                }

                if (chkDays60.Checked)
                {
                    duration = "60 Days";
                    colName = "Days60";
                    AddNewOrderItem(ds, duration, colName, dataItem, holdingDs);
                }

                if (chkDays90.Checked)
                {
                    duration = "90 Days";
                    colName = "Days90";
                    AddNewOrderItem(ds, duration, colName, dataItem, holdingDs);
                }

                if (chkDays120.Checked)
                {
                    duration = "120 Days";
                    colName = "Days120";
                    AddNewOrderItem(ds, duration, colName, dataItem, holdingDs);
                }

                if (chkDays150.Checked)
                {
                    duration = "150 Days";
                    colName = "Days150";
                    AddNewOrderItem(ds, duration, colName, dataItem, holdingDs);
                }

                if (chkDays180.Checked)
                {
                    duration = "180 Days";
                    colName = "Days180";
                    AddNewOrderItem(ds, duration, colName, dataItem, holdingDs);
                }

                if (chkDays270.Checked)
                {
                    duration = "270 Days";
                    colName = "Days270";
                    AddNewOrderItem(ds, duration, colName, dataItem, holdingDs);
                }

                if (chkYears1.Checked)
                {
                    duration = "1 Year";
                    colName = "Years1";
                    AddNewOrderItem(ds, duration, colName, dataItem, holdingDs);
                }

                if (chkYears2.Checked)
                {
                    duration = "2 Year";
                    colName = "Years2";
                    AddNewOrderItem(ds, duration, colName, dataItem, holdingDs);
                }

                if (chkYears3.Checked)
                {
                    duration = "3 Year";
                    colName = "Years3";
                    AddNewOrderItem(ds, duration, colName, dataItem, holdingDs);
                }

                if (chkYears4.Checked)
                {
                    duration = "4 Year";
                    colName = "Years4";
                    AddNewOrderItem(ds, duration, colName, dataItem, holdingDs);
                }

                if (chkYears5.Checked)
                {
                    duration = "5 Year";
                    colName = "Years5";
                    AddNewOrderItem(ds, duration, colName, dataItem, holdingDs);
                }

            }

            _detailDataView = new DataView(ds.Tables[ds.TDCashTable.TableName]) { Sort = ds.TDCashTable.BANKNAME + " ASC" };
            PresentationDetailGrid.Rebind();
            PresentationDetailGrid.Columns.FindByUniqueNameSafe("HoldingLimit").Visible =
            PresentationDetailGrid.Columns.FindByUniqueNameSafe("TotalHolding").Visible =
            PresentationDetailGrid.Columns.FindByUniqueNameSafe("AvailableFunds").Visible = IsSMA;
        }

        private void PlaceOrder()
        {
            lblMsg.Visible = false;
            lblMsg.Text = "";

            //Validate funds
            if (!ValidateAmount())
            {
                lblMsg.Visible = true;
                return;
            }

            //Checking Existing Order CM ID
            Guid orderCMCID = OrderPadUtilities.IsOrderCmExists(Broker, (Page as UMABasePage).GetCurrentUser());

            var unit = new OrganizationUnit
            {
                Name = "Order " + DateTime.Now.ToString("dd MMM, yyyy hh:mm:ss"),
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = (Page as UMABasePage).GetCurrentUser()
            };
            var ds = new OrderPadDS
            {
                CommandType = DatasetCommandTypes.Add,
                Unit = unit,
                Command = (int)WebCommands.AddNewOrganizationUnit
            };

            //Add rows in order pad ds
            AddRows(ds);

            if (SaveData != null)
            {
                SaveData(orderCMCID.ToString(), ds);
            }

            lblMsg.Visible = true;
            lblMsg.Text = ds.ExtendedProperties["Message"].ToString();

            if (ds.ExtendedProperties["Result"].ToString() == OperationResults.Successfull.ToString())
            {
                if (ds.ExtendedProperties.Contains("SMAMessages"))
                {
                    var results = (Dictionary<long, string>)ds.ExtendedProperties["SMAMessages"];
                    if (results.Count > 0)
                    {
                        var messag = string.Empty;
                        foreach (KeyValuePair<long, string> result in results)
                        {
                            messag += string.Format("Order: {0},{2}{1}{2}", result.Key, result.Value, Environment.NewLine);
                        }
                        string smaErrorFile = GetNewFilePath("txt");
                        Utilities.SaveToFile(messag, smaErrorFile);

                        lblMsg.Text += string.Format("To view Super service errors click <a href = '../SysAdministration/DownloadOrderReceipt.aspx?filename={0}'>Here</a> ", FileHelper.GetFileName(smaErrorFile));
                        ShowAlert("ShowServiceErrors", "There were errors sending data to Super Service.");
                    }
                }
                BindBankAccounts();
                CheckOrder();
                string path = CreateReceipt(ds);
                lblMsg.Text = string.Format("{0} Click <a href = '../SysAdministration/DownloadOrderReceipt.aspx?filename={1}'>here</a> to print its receipt.", lblMsg.Text, path);
            }
        }
        public void ShowAlert(string key, string message)
        {

            ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), key, string.Format("alert('{0}');", message), true);
        }
        private string GetNewFilePath(string ext)
        {
            string fileName = string.Format("{0}_{1}_BuyTD_{2}.{3}", ClientHeaderInfo1.ClientId ?? ClientAccID, lblMsg.Text.Split(' ')[1], DateTime.Now.ToShortDateString().Replace("/", "-"), ext);
            string dirOrderReceipts = string.Format("{0}\\OrderReceipts", HttpContext.Current.Server.MapPath("~/App_Data"));
            if (!Directory.Exists(dirOrderReceipts))
                Directory.CreateDirectory(dirOrderReceipts);
            string filePath = string.Format("{0}\\{1}", dirOrderReceipts, fileName);
            return filePath;
        }

        private void AddCellToSheet(DocumentBuilder builderExl, string text, double width, Color backColor,
                                  bool isFontBold, int fontSize, int lineWidth, CellVerticalAlignment vAlign,
                                  TextOrientation orientation, ParagraphAlignment paragraph)
        {
            builderExl.InsertCell();
            builderExl.CellFormat.PreferredWidth = PreferredWidth.FromPoints(width);
            builderExl.CellFormat.Shading.BackgroundPatternColor = backColor;
            builderExl.Font.Bold = isFontBold;
            builderExl.Font.Size = fontSize;
            builderExl.CellFormat.Borders.LineWidth = lineWidth;
            builderExl.CellFormat.VerticalAlignment = vAlign;
            builderExl.CellFormat.Orientation = orientation;
            builderExl.Font.Color = Color.Black;
            builderExl.CurrentParagraph.ParagraphFormat.Alignment = paragraph;

            builderExl.Bold = isFontBold;
            builderExl.Write(text);
        }
        private string CreateReceipt(OrderPadDS ds)
        {
            double sum = 0;
            DataRow clientSummaryRow = GetHoldingReportDS().Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0];
            string OutPath = string.Empty;
            string path = Server.MapPath("~/Templates/OrderPad_BuyTD_Template.docx");
            Aspose.Words.Document document = null;
            document = new Aspose.Words.Document(path);
            //This is portion for the Header Section of the Document
            document.Range.Replace("[Order_Date]", string.Format("Order {0}", lblMsg.Text.Split(' ')[1]), false, false);
            document.Range.Replace("[Eclipse_Date]", DateTime.Now.ToString("dd/MM/yyyy"), false, false);
            document.Range.Replace("[Eclipse_OrderBY]", ((Page as UMABasePage).GetCurrentUser()).CurrentUserName, false, false);
            document.Range.Replace("[Eclipse_InvestorName]", clientSummaryRow[HoldingRptDataSet.CLIENTNAME].ToString(), false, false);
            document.Range.Replace("[Eclipse_ClientID]", ClientHeaderInfo1.ClientId ?? ClientAccID, false, false);
            document.Range.Replace("[Eclipse_AccountType]", clientSummaryRow[HoldingRptDataSet.CLIENTTYPE].ToString(), false, false);
            document.Range.Replace("[Eclipse_Adviser]", clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME].ToString(), false, false);
            document.Range.Replace("[Eclipse_Service_Type]", cmbServiceType.SelectedItem.Text.ToUpper(), false, false);
            document.Range.Replace("[A11]", clientSummaryRow[HoldingRptDataSet.CLIENTNAME].ToString(), false, false);
            string addressLine1And2 = string.Empty;
            if (clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2].ToString() == string.Empty)
                addressLine1And2 = clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1].ToString();
            else
                addressLine1And2 = clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1].ToString() + ", " + clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2].ToString();
            document.Range.Replace("[A12]", addressLine1And2, false, false);
            document.Range.Replace("[A13]", string.Format("{0} {1} {2}", clientSummaryRow[HoldingRptDataSet.SUBURB], clientSummaryRow[HoldingRptDataSet.STATE], clientSummaryRow[HoldingRptDataSet.POSTCODE]), false, false);
            document.Range.Replace("[A14]", clientSummaryRow[HoldingRptDataSet.COUNTRY].ToString(), false, false);

            int bodyFontSize = 9;

            if (document != null)
            {
                OutPath = GetNewFilePath("PDF");
                //Now here we try to Make Dynamic Table
                DocumentBuilder builderExl = new DocumentBuilder(document);
                //This is Point where to Start Generate Table
                builderExl.MoveToBookmark("EclipseBuyTd", false, true);
                Aspose.Words.Tables.Table table = builderExl.StartTable();

                builderExl.PageSetup.LeftMargin = ConvertUtil.InchToPoint(0.5);
                builderExl.PageSetup.TopMargin = ConvertUtil.InchToPoint(0.5);
                builderExl.PageSetup.BottomMargin = ConvertUtil.InchToPoint(0.5);

                builderExl.StartTable();

                builderExl.InsertCell();
                builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                builderExl.CellFormat.Borders.LineWidth = 0;
                builderExl.EndRow();
                builderExl.EndTable();
                builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Left;
                builderExl.Writeln("");


                AddCellToSheet(builderExl, "BSB", 30, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, "Account No", 100, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, "Account Description", 140, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, "Amount", 52, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, "Term", 52, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, "Rate", 52, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, "Institution", 62, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, "Broker", 52, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                builderExl.EndRow();
                //Here we Make DataColunm
                DataTable dt_ = new DataTable();
                dt_ = ds.Tables[ds.TermDepositTable.TABLENAME].DefaultView.ToTable(false, "Amount", "Duration", "Percentage", "BankID", "BrokerID");
                DataRow drOrder = ds.Tables[ds.OrdersTable.TABLENAME].Rows[0];
                DataColumn dcBSB = new DataColumn("BSB");
                dcBSB.DefaultValue = drOrder[ds.OrdersTable.ACCOUNTBSB].ToString();
                dt_.Columns.Add(dcBSB);
                dt_.Columns["BSB"].SetOrdinal(0);

                DataColumn dcAccountNo = new DataColumn("AccountNo");
                dcAccountNo.DefaultValue = drOrder[ds.OrdersTable.ACCOUNTNUMBER].ToString();
                dt_.Columns.Add(dcAccountNo);
                dt_.Columns["AccountNo"].SetOrdinal(1);

                DataColumn dcAccountDescription = new DataColumn("AccountDescription");
                dcAccountDescription.DefaultValue = GetDataSet(new Guid(drOrder[ds.OrdersTable.ACCOUNTCID].ToString())).Tables[0].Rows[0]["AccountName"].ToString();
                dt_.Columns.Add(dcAccountDescription);
                dt_.Columns["AccountDescription"].SetOrdinal(2);

                DataColumn dcRate = new DataColumn("Rate");
                dcRate.DefaultValue = string.Empty;
                dt_.Columns.Add(dcRate);

                DataColumn dcInstitution = new DataColumn("Institution");
                dcInstitution.DefaultValue = string.Empty;
                dt_.Columns.Add(dcInstitution);

                DataColumn dcBroker = new DataColumn("Broker");
                dcBroker.DefaultValue = string.Empty;
                dt_.Columns.Add(dcBroker);
                SetTDData(dt_);

                for (int i = 0; i < dt_.Rows.Count; i++)
                {
                    AddCellToSheet(builderExl, dt_.Rows[i]["BSB"].ToString(), 30, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                    AddCellToSheet(builderExl, dt_.Rows[i]["AccountNo"].ToString(), 100, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                    AddCellToSheet(builderExl, dt_.Rows[i]["AccountDescription"].ToString(), 140, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                    AddCellToSheet(builderExl, string.Format("{0:C}", dt_.Rows[i]["Amount"]), 52, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                    AddCellToSheet(builderExl, dt_.Rows[i]["Duration"].ToString(), 52, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                    AddCellToSheet(builderExl, dt_.Rows[i]["Rate"].ToString(), 52, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                    AddCellToSheet(builderExl, dt_.Rows[i]["Institution"].ToString(), 62, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                    AddCellToSheet(builderExl, dt_.Rows[i]["Broker"].ToString(), 52, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);

                    builderExl.EndRow();
                    sum += Convert.ToDouble(dt_.Rows[i]["Amount"]);
                }
                //Here we add the Total Colunm
                AddCellToSheet(builderExl, "Total", 30, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, string.Empty, 100, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                AddCellToSheet(builderExl, string.Empty, 140, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                AddCellToSheet(builderExl, string.Format("{0:C}", sum), 52, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                AddCellToSheet(builderExl, string.Empty, 52, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                AddCellToSheet(builderExl, string.Empty, 52, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                AddCellToSheet(builderExl, string.Empty, 62, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                AddCellToSheet(builderExl, string.Empty, 52, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                builderExl.EndRow();
                //   builderExl.EndTable();
                if (table != null)
                {
                    table.PreferredWidth = PreferredWidth.FromPoints(552);
                    //   table.AutoFit(AutoFitBehavior.AutoFitToContents);
                }
                document.Save(OutPath, SaveFormat.Pdf);
            }
            return FileHelper.GetFileName(OutPath);
        }
        private BankAccountDS GetDataSet(Guid cid)
        {
            var bankTransactionDetailsDS = new BankAccountDS { CommandType = DatasetCommandTypes.Get };
            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
                clientData.GetData(bankTransactionDetailsDS);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }
            return bankTransactionDetailsDS;
        }

        private void SetTDData(DataTable dt)
        {
            Guid BankID = Guid.Empty;
            Guid BrokerID = Guid.Empty;

            foreach (DataRow row in dt.Rows)
            {
                BankID = new Guid(row["BankID"].ToString());
                BrokerID = new Guid(row["BrokerID"].ToString());

                // Get/Set the Bank and Broker by their IDs
                var inst = (Broker.GetWellKnownBMC(WellKnownCM.Organization) as Oritax.TaxSimp.CM.Organization.IOrganization).Institution;
                InstitutionEntity ie = inst.FirstOrDefault(ss => ss.ID == BankID);
                row["Institution"] = ie.Name;
                ie = inst.FirstOrDefault(ss => ss.ID == BrokerID);
                row["Broker"] = ie.Name;

                row["Rate"] = string.Format("{0}%", Convert.ToDecimal(row["Percentage"]) * 100);
            }

            dt.Columns.Remove(dt.Columns["Percentage"]);
            dt.Columns.Remove(dt.Columns["BankID"]);
            dt.Columns.Remove(dt.Columns["BrokerID"]);
        }

        private bool ValidateAmount()
        {
            decimal totalAmount = 0;
            foreach (GridDataItem dataItem in PresentationDetailGrid.MasterTableView.Items)
            {
                var txtAmount = (dataItem.FindControl("txtAmount") as RadNumericTextBox);

                if (txtAmount != null && (!string.IsNullOrEmpty(txtAmount.Text.Trim()) && Convert.ToDecimal(txtAmount.Text) > 0))
                {
                    totalAmount += Convert.ToDecimal(txtAmount.Text);
                }
            }

            if (totalAmount == 0)
            {
                lblMsg.Text = "Please Enter Amount";
                return false;
            }

            decimal availableFunds;
            decimal.TryParse(lblAvailableFunds.Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out availableFunds);
            if (totalAmount > availableFunds)
            {
                lblMsg.Text = "Insufficient Available Funds";
                return false;
            }

            return true;
        }

        private void AddRows(OrderPadDS ds)
        {
            //Order Row
            var orderId = AddOrderRow(ds);

            foreach (GridDataItem dataItem in PresentationDetailGrid.MasterTableView.Items)
            {
                var txtAmount = (dataItem.FindControl("txtAmount") as RadNumericTextBox);
                decimal amount = 0;
                if (txtAmount != null && !string.IsNullOrEmpty(txtAmount.Text))
                {
                    amount = Convert.ToDecimal(txtAmount.Text.Trim());
                }
                if (amount > 0)
                {
                    //Order Items Row
                    AddItemRow(ds, amount, dataItem, orderId);
                }
            }
        }

        private Guid AddOrderRow(OrderPadDS ds)
        {
            DataRow orderRow = ds.OrdersTable.NewRow();

            Guid orderId = Guid.NewGuid();
            orderRow[ds.OrdersTable.ID] = orderId;
            orderRow[ds.OrdersTable.CLIENTCID] = new Guid(ClientCID);
            orderRow[ds.OrdersTable.CLIENTID] = ClientHeaderInfo1.ClientId ?? ClientAccID;
            orderRow[ds.OrdersTable.ORDERACCOUNTTYPE] = OrderAccountType.TermDeposit;
            orderRow[ds.OrdersTable.ORDERTYPE] = OrderType.Manual;
            orderRow[ds.OrdersTable.STATUS] = OrderStatus.Active;
            orderRow[ds.OrdersTable.ACCOUNTCID] = new Guid(cmbCashAccount.SelectedValue);
            orderRow[ds.OrdersTable.ACCOUNTNUMBER] = cmbCashAccount.SelectedItem.Attributes["AccountNo"].Trim();
            orderRow[ds.OrdersTable.ACCOUNTBSB] = cmbCashAccount.SelectedItem.Attributes["BSB"].Trim();
            orderRow[ds.OrdersTable.ORDERITEMTYPE] = OrderItemType.Buy;
            orderRow[ds.OrdersTable.CLIENTMANAGEMENTTYPE] = IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA;

            ds.OrdersTable.Rows.Add(orderRow);
            return orderId;
        }

        private void AddItemRow(OrderPadDS ds, decimal amount, GridDataItem dataItem, Guid orderId)
        {

            DataRow itemsRow = ds.TermDepositTable.NewRow();

            itemsRow[ds.TermDepositTable.ID] = Guid.NewGuid();
            itemsRow[ds.TermDepositTable.ORDERID] = orderId;
            itemsRow[ds.TermDepositTable.AMOUNT] = amount;
            itemsRow[ds.TermDepositTable.BANKID] = new Guid(dataItem["BankID"].Text);
            itemsRow[ds.TermDepositTable.BROKERID] = new Guid(dataItem["BrokerID"].Text);
            itemsRow[ds.TermDepositTable.PRODUCTID] = new Guid(dataItem["ProductID"].Text);
            itemsRow[ds.TermDepositTable.TDACCOUNTCID] = new Guid(dataItem["TDAccountCID"].Text);

            decimal min;
            decimal.TryParse(dataItem["Min"].Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out min);
            decimal max;
            decimal.TryParse(dataItem["Max"].Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out max);

            itemsRow[ds.TermDepositTable.MIN] = min;
            itemsRow[ds.TermDepositTable.MAX] = max;
            itemsRow[ds.TermDepositTable.DURATION] = dataItem["Duration"].Text;
            itemsRow[ds.TermDepositTable.PERCENTAGE] = dataItem["Percentage"].Text;
            itemsRow[ds.TermDepositTable.RATING] = dataItem["Rating"].Text.Trim().Replace("&nbsp;", "").Length == 0 ? "Unrated" : dataItem["Rating"].Text;

            ds.TermDepositTable.Rows.Add(itemsRow);
        }

        private void HideTermColumns()
        {
            int minTerm = cmbMinTerm.SelectedIndex;
            int maxTerm = cmbMaxTerm.SelectedIndex;

            //Show/Hide terms column
            for (int i = 0; i < TermList.Length; i++)
            {
                string t = TermList[i];
                if (i >= minTerm && i <= maxTerm)
                {
                    PresentationGrid.MasterTableView.GetColumn(t).Display = true;
                }
                else
                {
                    PresentationGrid.MasterTableView.GetColumn(t).Display = false;
                }
            }
        }

        private void AddNewOrderItem(OPTDCashDS ds, string duration, string columnName, GridDataItem dataItem, HoldingRptDataSet holdingDs)
        {
            DataRow dr = ds.Tables[ds.TDCashTable.TableName].NewRow();

            var broker = dataItem["Broker"].Text;
            var bank = dataItem["Bank"].Text;
            var smaHoldingLimit = dataItem["SMAHoldingLimit"].Text;
            var productId = dataItem["ProductID"].Text;
            var tdAccCID = dataItem["TDAccountCID"].Text;

            dr[ds.TDCashTable.BROKERID] = new Guid(dataItem["BrokerID"].Text);
            dr[ds.TDCashTable.BANKID] = new Guid(dataItem["BankID"].Text);
            dr[ds.TDCashTable.BROKERNAME] = broker;
            dr[ds.TDCashTable.BANKNAME] = bank;
            dr[ds.TDCashTable.PRODUCTID] = string.IsNullOrEmpty(productId) ? Guid.Empty.ToString() : productId;
            dr[ds.TDCashTable.TDACCOUNTCID] = string.IsNullOrEmpty(tdAccCID) ? Guid.Empty.ToString() : tdAccCID;

            if (string.IsNullOrWhiteSpace(hfIsAlert.Value))
            {
                if (string.IsNullOrEmpty(productId))
                {
                    hfIsAlert.Value = broker;
                }
                else if (dataItem["TDAccountStatus"].Text.ToLower() != "active")
                {
                    hfIsAlert.Value = broker;
                    hfTDAccStatus.Value = dataItem["TDAccountStatus"].Text.ToLower();
                }
            }

            decimal min;
            decimal.TryParse(dataItem["Min"].Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out min);
            decimal max;
            decimal.TryParse(dataItem["Max"].Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out max);

            if (hidChkCount.Value == "1")
            {
                decimal txtValue = Convert.ToDecimal(txtAmounttoInvest.Text);
                if (txtValue >= max && max != 0)
                {
                    dr[ds.TDCashTable.AMOUNT] = max;
                    hidMaxValue.Value = max.ToString();
                }
                else
                {
                    dr[ds.TDCashTable.AMOUNT] = txtValue;
                    hidMaxValue.Value = txtValue.ToString();
                }
            }

            dr[ds.TDCashTable.MIN] = min;
            dr[ds.TDCashTable.MAX] = max;
            dr[ds.TDCashTable.PERCENTAGE] = Convert.ToDecimal(dataItem[columnName].Text);
            dr[ds.TDCashTable.DURATION] = duration;
            dr[ds.TDCashTable.RATING] = dataItem["Rating"].Text;

            if (IsSMA)
            {
                decimal tdHoldingLimit, tdCurrentHolding, tdAvailableFund;
                OrderPadUtilities.GetTDAvaiableFundsForSMA(holdingDs, broker, bank, Convert.ToDecimal(smaHoldingLimit), out tdHoldingLimit, out tdCurrentHolding, out tdAvailableFund);

                dr[ds.TDCashTable.HOLDINGLIMIT] = tdHoldingLimit;
                dr[ds.TDCashTable.TOTALHOLDING] = tdCurrentHolding;
                dr[ds.TDCashTable.AVAILABLEFUNDS] = tdAvailableFund;
            }
            ds.Tables[ds.TDCashTable.TableName].Rows.Add(dr);
        }

        #region Fill Lists

        private readonly string[] _termList = { "30-D", "60-D", "90-D", "120-D", "150-D", "180-D", "270-D", "Y-1", "Y-2", "Y-3", "Y-4", "Y-5" };
        private readonly string[] _termListSMA = { "30-D", "60-D", "90-D", "120-D", "150-D", "180-D", "270-D", "Y-1" };

        #endregion

        protected void cmbServiceType_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindBankAccounts();
            PresentationGrid.Rebind();
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            popupSearch.Show();
            showDetails.Visible = false;
            ClientHeaderInfo1.Visible = false;
            PresentationGrid.Visible = false;
            tdButtons.Visible = false;
            searchAccountControl.HideModal = false;
            searchAccountControl.FindControl("SearchBox").Focus();
        }

        protected void PresentationGrid_OnSortCommand(object sender, GridSortCommandEventArgs e)
        {
            if ("PriceInstituteLongRating".Equals(e.CommandArgument))
            {
                e.Item.OwnerTableView.AllowCustomSorting = true;
                switch (e.OldSortOrder)
                {
                    case GridSortOrder.None:
                        _dataView.Sort = string.Format("{0} DESC", OrderPadRatesDS.PRICEINSTITUTELONGRATINGVALUE);
                        e.Item.OwnerTableView.DataSource = _dataView;
                        e.Item.OwnerTableView.Rebind();
                        break;
                    case GridSortOrder.Ascending:
                        _dataView.Sort = string.Format("{0} ASC", OrderPadRatesDS.PRICEINSTITUTELONGRATINGVALUE);
                        e.Item.OwnerTableView.DataSource = _dataView;
                        e.Item.OwnerTableView.Rebind();
                        break;
                    case GridSortOrder.Descending:
                        e.Item.OwnerTableView.DataSource = _dataView;
                        e.Item.OwnerTableView.Rebind();
                        break;
                }
            }
            else
            {
                e.Item.OwnerTableView.AllowCustomSorting = false;
            }
        }

        private void SetClientManagementType()
        {
            IsSMA = SMAUtilities.IsSMAClient(new Guid(ClientCID), Broker);
            PresentationGrid.Columns.FindByUniqueNameSafe("Y-2").Visible =
            PresentationGrid.Columns.FindByUniqueNameSafe("Y-3").Visible =
            PresentationGrid.Columns.FindByUniqueNameSafe("Y-4").Visible =
            PresentationGrid.Columns.FindByUniqueNameSafe("Y-5").Visible = !IsSMA;

            tdCashBalanceHead.Visible =
            tdCashBalanceValue.Visible =
            tdMinCashHead.Visible =
            tdMinCashValue.Visible = IsSMA;
        }
    }
}