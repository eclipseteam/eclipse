﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BankAccountControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.BankAccountControl" %>
<script type="text/javascript">
    function DisableSaveButton(sender, args) {
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';

        sender.set_autoPostBack(true);
    }
</script>
<div class="popup_Container">
    <div class="popup_Titlebar" id="PopupHeader">
        <div class="TitlebarLeft">
            <span style="color: white;">Account Details</span>
        </div>
    </div>
    <div class="popup_Body">
        <table>
            <tr>
                <td colspan="4">
                    <asp:HiddenField ID="TextCID" runat="server" />
                    <asp:HiddenField ID="TextClid" runat="server" />
                    <asp:HiddenField ID="TextCsid" runat="server" />
                    <asp:HiddenField ID="hfIsAdviserOrClient" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Status</span>
                </td>
                <td style="width: 50%;">
                    <telerik:RadComboBox runat="server" ID="ComboStatus" Width="226px">
                    </telerik:RadComboBox>
                </td>
                <td style="display: block;">
                    <asp:Label ID="lblCustomerNo" runat="server" Text="Number" Visible="false"></asp:Label>
                </td>
                <td style="display: block;">
                    <telerik:RadTextBox ID="txtCustomerNo" runat="server" CssClass="Txt-Field" Width="200px"
                        Visible="false">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Account Name *</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txttName" runat="server" CssClass="Txt-Field" Width="227px"
                        ValidationGroup="RFVAddBankAccount">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="RFVAccountName" runat="server" ControlToValidate="txttName"
                        ErrorMessage="*" ForeColor="Red" ValidationGroup="RFVAddBankAccount"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <span class="riLabel">Account Type *</span>
                </td>
                <td>
                    <telerik:RadComboBox runat="server" ID="ComboAccoutType" Width="216px" AutoPostBack="true"
                        OnSelectedIndexChanged="ComboAccoutType_OnSelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="Saving" Value="SAVING" Selected="true" />
                            <telerik:RadComboBoxItem runat="server" Text="Cheque" Value="CHEQUE" />
                            <telerik:RadComboBoxItem runat="server" Text="CMA" Value="CMA" />
                            <telerik:RadComboBoxItem runat="server" Text="CMT" Value="CMT" />
                            <telerik:RadComboBoxItem runat="server" Text="MMA" Value="MMA" />
                            <telerik:RadComboBoxItem runat="server" Text="Term Deposit" Value="TERMDEPOSIT" />
                            <telerik:RadComboBoxItem runat="server" Text="Other" Value="OTHER" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">BSB *</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="TextBSB1" runat="server" CssClass="Txt-Field" Width="102px"
                        MaxLength="3" ValidationGroup="RFVAddBankAccount">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="RFVBSB1" runat="server" ControlToValidate="TextBSB1"
                        ErrorMessage="*" ForeColor="Red" ValidationGroup="RFVAddBankAccount"></asp:RequiredFieldValidator>
                    -&nbsp;&nbsp;
                    <telerik:RadTextBox CssClass="Txt-Field" runat="server" ID="TextBSB2" Width="101px"
                        MaxLength="3" ValidationGroup="RFVAddBankAccount">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="RFVBSB2" runat="server" ControlToValidate="TextBSB2"
                        ErrorMessage="*" ForeColor="Red" ValidationGroup="RFVAddBankAccount"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <span class="riLabel">Account Number *</span>
                </td>
                <td>
                    <telerik:RadTextBox CssClass="Txt-Field" runat="server" ID="TextAccountNumber" Width="215px"
                        ValidationGroup="RFVAddBankAccount">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="RFVTxtField" runat="server" ControlToValidate="TextAccountNumber"
                        ErrorMessage="*" ForeColor="Red" ValidationGroup="RFVAddBankAccount"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Institution</span>
                </td>
                <td>
                    <telerik:RadComboBox runat="server" ID="ComboInstitute" Width="226px">
                    </telerik:RadComboBox>
                </td>
                <td>
                    <asp:Label ID="lblBroker" runat="server" Text="Broker*"></asp:Label>
                </td>
                <td>
                    <telerik:RadComboBox runat="server" ID="ComboBroker" Width="210px">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="3">
                    <asp:Label ID="lblMessages" runat="server" Text="" ForeColor="#990000" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <telerik:RadButton ID="chkExternal" runat="server" Text="Is this an external account?"
                        AutoPostBack="False" ToggleType="CheckBox" ButtonType="ToggleButton">
                    </telerik:RadButton>
                </td>
                <td>
                </td>
                <td>
                    <telerik:RadButton ID="btnUpdate" runat="server" Text="Save" OnClick="btnUpdate_OnClick"
                        OnClientClicked="DisableSaveButton" ValidationGroup="RFVAddBankAccount" />
                    <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_OnClick" />
                </td>
            </tr>
        </table>
    </div>
</div>
