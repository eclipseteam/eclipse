﻿using System;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Calculation;
using Telerik.Web.UI;
using System.Security.Principal;
using Oritax.TaxSimp.Utilities;
using System.Web.UI.WebControls;

namespace eclipseonlineweb.Controls
{
    public partial class NotesControl : System.Web.UI.UserControl
    {
        public delegate void CustomHandler();
        public event CustomHandler FillPresentationData;

        public IPrincipal User { get; set; }
        public DataSet PresentationData
        {
            get;
            set;
        }

        public string CID
        {
            get;
            set;
        }

        public Action<Guid, Guid, Guid> Saved
        {
            get;
            set;
        }

        public Action<Guid, DataSet> SaveData
        {
            get;
            set;
        }
        public Action Canceled { get; set; }
        public Action ValidationFailed { get; set; }
        private ICMBroker UMABroker
        {
            get
            {
                return ((UMABasePage) Page).UMABroker;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            FillPresentationData();
            if (!IsPostBack)
            {
                lblCID.Text = CID;
                LoadPage();
            }
        }

        protected void GetBMCData()
        {
        }

        protected void RepDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                ((RadTextBox)e.Item.FindControl("txtCommentEdit")).ReadOnly = false;
                ((LinkButton)e.Item.FindControl("LinkEdit")).Visible = false;
                ((LinkButton)e.Item.FindControl("LinkDelete")).Visible = false;
                ((LinkButton)e.Item.FindControl("LinkUpdate")).Visible = true;
                ((LinkButton)e.Item.FindControl("Linkcancel")).Visible = true;
            }
            if (e.CommandName == "delete")
            {
                IOrganizationUnit clientData = UMABroker.GetBMCInstance(new Guid(CID)) as IOrganizationUnit;
                UMABroker.SaveOverride = true;
                Guid rowID = new Guid(e.CommandArgument.ToString());
                DataTable notesTable = PresentationData.Tables[NoteListDS.NOTES_TABLE];
                notesTable.AcceptChanges();

                foreach (DataRow objRow in notesTable.Rows)
                {
                    if ((Guid)objRow[NoteListDS.NOTES_PRIMARYKEY_FIELD] == rowID)
                    {
                        objRow.Delete();
                        UMABroker.LogEvent(EventType.UMAClientNotesDelete, clientData.CID, "The Notes of Client:" + clientData.ClientId + "-" + clientData.Name + "is Deleted");
                    }
                }
                clientData.SetData(PresentationData);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                LoadPage();
            }
            if (e.CommandName == "update")
            {
                IOrganizationUnit clientData = UMABroker.GetBMCInstance(new Guid(CID)) as IOrganizationUnit;
                UMABroker.SaveOverride = true;
                Guid rowID = new Guid(e.CommandArgument.ToString());
                DataTable notesTable = PresentationData.Tables[NoteListDS.NOTES_TABLE];
                notesTable.AcceptChanges();

                foreach (DataRow objRow in notesTable.Rows)
                {
                    if ((Guid)objRow[NoteListDS.NOTES_PRIMARYKEY_FIELD] == rowID)
                    {
                        objRow[NoteListDS.NOTES_DESCRIPTION_FIELD] = ((RadTextBox)e.Item.FindControl("txtCommentEdit")).Text;
                        objRow[NoteListDS.NOTES_LASTUSERUPDATED_FIELD] = User.Identity.Name;
                        objRow[NoteListDS.NOTES_LASTDATEUPDATED_FIELD] = DateTime.Now;
                        UMABroker.LogEvent(EventType.UMAClientNotesEdit, clientData.CID, "The Notes of Client:" + clientData.ClientId + "-" + clientData.Name + "is Updated");
                    }
                }

                clientData.SetData(PresentationData);
                UMABroker.SetComplete();
                UMABroker.SetStart();


                ((LinkButton)e.Item.FindControl("LinkEdit")).Visible = ((RadTextBox)e.Item.FindControl("txtCommentEdit")).ReadOnly = true;
                ((LinkButton)e.Item.FindControl("LinkUpdate")).Visible = false;
            }
            if (e.CommandName == "cancel")
            {
                ((LinkButton)e.Item.FindControl("LinkEdit")).Visible = ((RadTextBox)e.Item.FindControl("txtCommentEdit")).ReadOnly = ((LinkButton)e.Item.FindControl("LinkDelete")).Visible = true;
                ((LinkButton)e.Item.FindControl("LinkUpdate")).Visible = ((LinkButton)e.Item.FindControl("Linkcancel")).Visible = false;
                
            }
        }

        protected void RepDetails_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
        }

        public void LoadPage()
        {
            if (PresentationData.Tables.Contains(NoteListDS.NOTES_TABLE))
            {
                DataView dv = new DataView(PresentationData.Tables[NoteListDS.NOTES_TABLE]);
                dv.Sort = "NOTES_CREATEDDATE_FIELD ASC";
                RepDetails.DataSource = PresentationData.Tables[NoteListDS.NOTES_TABLE];
                RepDetails.DataBind();
            }
        }

        public void SaveClientData(Guid cid, DataSet ds)
        {
            UMABroker.SaveOverride = true;
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(cid);
            clientData.SetData(ds);
            UMABroker.SetComplete();
            UMABroker.SetStart();
        }


        protected void PresentationGrid_DetailTableDataBind(object source, GridDetailTableDataBindEventArgs e) { }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                IOrganizationUnit clientData = UMABroker.GetBMCInstance(new Guid(CID)) as IOrganizationUnit;
                UMABroker.SaveOverride = true;

                Guid rowID = new Guid(gDataItem.OwnerTableView.DataKeyValues[gDataItem.ItemIndex]["Key"].ToString());
                DataTable notesTable = PresentationData.Tables[NoteListDS.NOTES_TABLE];
                notesTable.AcceptChanges();

                foreach (DataRow objRow in notesTable.Rows)
                {
                    if ((Guid)objRow[NoteListDS.NOTES_PRIMARYKEY_FIELD] == rowID)
                    {
                        objRow.Delete();
                        UMABroker.LogEvent(EventType.UMAClientNotesDelete, clientData.CID, "The Notes of Client:" + clientData.ClientId + "-" + clientData.Name + "is Deleted");
                    }
                }
                clientData.SetData(PresentationData);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                Response.Redirect(Request.Url.AbsoluteUri);
            }

            if (e.CommandName.ToLower() == "update")
            {
                GridEditableItem gridEditableItem = e.Item as GridEditableItem;
                IOrganizationUnit clientData = UMABroker.GetBMCInstance(new Guid(CID)) as IOrganizationUnit;
                UMABroker.SaveOverride = true;

                Guid rowID = new Guid(gridEditableItem.OwnerTableView.DataKeyValues[gridEditableItem.ItemIndex]["Key"].ToString());
                DataTable notesTable = PresentationData.Tables[NoteListDS.NOTES_TABLE];
                notesTable.AcceptChanges();

                foreach (DataRow objRow in notesTable.Rows)
                {
                    if ((Guid)objRow[NoteListDS.NOTES_PRIMARYKEY_FIELD] == rowID)
                    {
                        objRow[NoteListDS.NOTES_DESCRIPTION_FIELD] = ((RadTextBox)(gridEditableItem["DESCRIPTION"].Controls[1])).Text;
                        objRow[NoteListDS.NOTES_LASTUSERUPDATED_FIELD] = User.Identity.Name;
                        objRow[NoteListDS.NOTES_LASTDATEUPDATED_FIELD] = DateTime.Now;

                        UMABroker.LogEvent(EventType.UMAClientNotesEdit, clientData.CID, "The Notes of Client:" + clientData.ClientId + "-" + clientData.Name + "is Updated");
                    }
                }
                clientData.SetData(PresentationData);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                Response.Redirect(Request.Url.AbsoluteUri);
            }
        }


        protected void PresentationGrid_ItemUpdated(object source, GridUpdatedEventArgs e)
        {
        }
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e)
        {


        }

        public static string WrappableText(string source)
        {
            string nwln = Environment.NewLine;
            return "<p>" +
            source.Replace(nwln + nwln, "</p><p>").Replace(nwln, "<br />") + "</p>";
        }

        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            IOrganizationUnit clientData = UMABroker.GetBMCInstance(new Guid(CID)) as IOrganizationUnit;

            DataRow objNewNoteRow = PresentationData.Tables[NoteListDS.NOTES_TABLE].NewRow();
            objNewNoteRow[NoteListDS.NOTES_PRIMARYKEY_FIELD] = Guid.NewGuid();
            objNewNoteRow[NoteListDS.NOTES_WORKPAPERID_FIELD] = Guid.Empty;
            objNewNoteRow[NoteListDS.NOTES_DESCRIPTION_FIELD] = ((RadTextBox)(((GridEditFormInsertItem)(e.Item))["DESCRIPTION"].Controls[1])).Text;
            objNewNoteRow[NoteListDS.NOTES_CREATEDBY_FIELD] = UMABroker.UserContext.Identity.Name;
            objNewNoteRow[NoteListDS.NOTES_CREATIONDATE_FIELD] = DateTime.Now;
            objNewNoteRow[NoteListDS.NOTES_LASTUSERUPDATED_FIELD] = UMABroker.UserContext.Identity.Name;
            objNewNoteRow[NoteListDS.NOTES_LASTDATEUPDATED_FIELD] = DateTime.Now;
            objNewNoteRow[NoteListDS.NOTES_ROLLFORWARD_FIELD] = false;
            PresentationData.Tables[NoteListDS.NOTES_TABLE].Rows.Add(objNewNoteRow);

            clientData.SetData(PresentationData);
            UMABroker.SetComplete();
            UMABroker.SetStart();
            e.Canceled = true;
            e.Item.Edit = false;
            Response.Redirect(Request.Url.AbsoluteUri);
        }

        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e) { }
        protected void PresentationGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                Label label = ((Telerik.Web.UI.GridDataItem)(e.Item))["DESCRIPTION"].Controls[1] as Label;
                label.Text = WrappableText(label.Text);
            }
        }
        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DataView View = new DataView(PresentationData.Tables[NoteListDS.NOTES_TABLE]);
            View.Sort = NoteListDS.NOTES_LASTDATEUPDATED_FIELD + " DESC";
        }

        protected string TrimDescription(string description)
        {
            if (!string.IsNullOrEmpty(description) && description.Length > 2000)
            {
                return string.Concat(description.Substring(0, 2000), "...");
            }
            return description;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            IOrganizationUnit clientData = UMABroker.GetBMCInstance(new Guid(lblCID.Text)) as IOrganizationUnit;
            NoteListDS noteListDS = new NoteListDS();
            clientData.GetData(noteListDS);

            UMABroker.SaveOverride = true;
            DataRow objNewNoteRow = noteListDS.Tables[NoteListDS.NOTES_TABLE].NewRow();
            objNewNoteRow[NoteListDS.NOTES_PRIMARYKEY_FIELD] = Guid.NewGuid();
            objNewNoteRow[NoteListDS.NOTES_WORKPAPERID_FIELD] = Guid.Empty;
            objNewNoteRow[NoteListDS.NOTES_DESCRIPTION_FIELD] = txtCommentBox.Text;
            objNewNoteRow[NoteListDS.NOTES_CREATEDBY_FIELD] = UMABroker.UserContext.Identity.Name;
            objNewNoteRow[NoteListDS.NOTES_CREATIONDATE_FIELD] = DateTime.Now;
            objNewNoteRow[NoteListDS.NOTES_LASTUSERUPDATED_FIELD] = UMABroker.UserContext.Identity.Name;
            objNewNoteRow[NoteListDS.NOTES_LASTDATEUPDATED_FIELD] = DateTime.Now;
            objNewNoteRow[NoteListDS.NOTES_ROLLFORWARD_FIELD] = false;
            noteListDS.Tables[NoteListDS.NOTES_TABLE].Rows.Add(objNewNoteRow);
            clientData.SetData(noteListDS);
            UMABroker.SetComplete();
            UMABroker.SetStart();
            //Refresh Records
            txtCommentBox.Text = string.Empty;
            FillPresentationData();
            LoadPage();

        }
    }
}