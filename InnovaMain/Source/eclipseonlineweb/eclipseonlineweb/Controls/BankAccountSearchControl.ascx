﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BankAccountSearchControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.BankAccountSearchControl" %>
<div class="popup_Container">
    <div class="popup_Titlebar" id="PopupHeader">
        <div class="TitlebarLeft">
            <asp:Label ID="Title" runat="server" Text="Availabe Bank Acocunts"></asp:Label>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="selectedCLID" />
    <asp:HiddenField runat="server" ID="selectedCSID" />
    <div class="popup_Body">
        <div runat="server" id="dvSearchtxt">
            <table>
                <tr>
                    <td style="width:25%">
                        <strong>Filter available bank accounts</strong>
                    </td>
                    <td>
                        <telerik:RadTextBox CssClass="Txt-Field" runat="server" ID="txtSearchAccName" Width="400" ValidationGroup="RFVfilter"></telerik:RadTextBox><asp:RequiredFieldValidator runat="server" ID="RFVTxtField" runat="server" ControlToValidate="txtSearchAccName"
                        ErrorMessage="*" ForeColor="Red" ValidationGroup="RFVfilter"></asp:RequiredFieldValidator>&nbsp;&nbsp;&nbsp;    <telerik:RadButton ID="RadButton1" runat="server" Text="Filter" OnClick="BtnSearchClick" ValidationGroup="RFVfilter" />
                        
                    </td>
                      
                </tr>
            </table>
        </div>
        <div runat="server" id="dvExistingAccGrid" style="width: 725px">
            <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                PageSize="4" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="Banks" TableLayout="Fixed">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="Cid" ReadOnly="true" HeaderText="Cid" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Cid" UniqueName="Cid" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="CLid" ReadOnly="true" HeaderText="CLid"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="CLid" UniqueName="CLid" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="CSid" ReadOnly="true" HeaderText="CSid"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="CSid" UniqueName="CSid" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn HeaderText="Select" UniqueName="ChkSelect" AllowFiltering="false">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkBankAccount" runat="server" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn SortExpression="AccountName" ReadOnly="true" HeaderText="Account Name"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="AccountName" UniqueName="AccountName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="AccountType" HeaderText="Account Type"
                            AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                            HeaderButtonType="TextButton" DataField="AccountType" UniqueName="AccountType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="BSBNo" HeaderText="BSB Number" ReadOnly="true"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="BSBNo" UniqueName="BSBNo">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="AccountNo" HeaderText="Account Number" ReadOnly="true"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="AccountNo" UniqueName="AccountNo">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Institution" HeaderText="Institution" ReadOnly="true"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="Institution" UniqueName="Institution">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </div>
        <div>
            <asp:Label runat="server" ID="Label1" Font-Size="Medium" Font-Bold="True" ForeColor="Red" />
            <br />
            <br />
        </div>
        <div runat="server" id="DvAssBankAccountGrid">
            <table style="width: 100%">
                <tr>
                    <td colspan="2">
                        <telerik:RadGrid ID="PresentationGridAccount" runat="server" ShowStatusBar="true"
                            AutoGenerateColumns="False" PageSize="4" AllowSorting="True" AllowMultiRowSelection="False"
                            AllowPaging="True" GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true"
                            AllowAutomaticInserts="True" AllowAutomaticUpdates="True" EnableViewState="true"
                            ShowFooter="false" OnNeedDataSource="PresentationGridAccount_OnNeedDataSource">
                            <PagerStyle Mode="NumericPages"></PagerStyle>
                            <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                                Name="Banks" TableLayout="Fixed">
                                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <Columns>
                                    <telerik:GridBoundColumn SortExpression="CLid" ReadOnly="true" HeaderText="CLid"
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                        HeaderButtonType="TextButton" DataField="CLid" UniqueName="CLid" Display="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn SortExpression="CSid" ReadOnly="true" HeaderText="CSid"
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                        HeaderButtonType="TextButton" DataField="CSid" UniqueName="CSid" Display="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Select" UniqueName="ChkSelect" AllowFiltering="false">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBox1" runat="server" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn SortExpression="AccountName" ReadOnly="true" HeaderText="Account Name"
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                        HeaderButtonType="TextButton" DataField="AccountName" UniqueName="AccountName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="AccountType" HeaderText="Account Type"
                                        AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                                        HeaderButtonType="TextButton" DataField="AccountType" UniqueName="AccountType">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn SortExpression="BSBNo" HeaderText="BSB" ReadOnly="true"
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                        HeaderButtonType="TextButton" DataField="BSBNo" UniqueName="BSBNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn SortExpression="AccountNo" HeaderText="Account Number" ReadOnly="true"
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                        HeaderButtonType="TextButton" DataField="AccountNo" UniqueName="AccountNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn SortExpression="Institution" HeaderText="Institution" ReadOnly="true"
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                        HeaderButtonType="TextButton" DataField="Institution" UniqueName="Institution">
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center" colspan="2">
                        <asp:Label runat="server" ID="lblmessages" Font-Size="Medium" Font-Bold="True" ForeColor="Red" /><asp:HiddenField
                            ID="hfFilter" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
        <div align="right">
            <telerik:RadButton  ID="btnUpdate" runat="server" Text="Save" OnClick="btnUpdate_OnClick" ValidationGroup="RFVfilter" />&nbsp;
            <telerik:RadButton  ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_OnClick"  />
        </div>
    </div>
</div>
