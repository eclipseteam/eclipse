﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactMappingControl.ascx.cs"
    EnableViewState="true" Inherits="eclipseonlineweb.Controls.ContactMappingControl" %>
<%@ Register TagPrefix="UC1" TagName="ExistingContacts" Src="~/Controls/ExistingContactsControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="IndividualControl" Src="~/Controls/Individual.ascx" %>
<style type="text/css">
    .holder
    {
        width: 100%;
        display: block;
        z-index: 6;
    }
    .content
    {
        background: #fff;
        z-index: 7; /*  padding: 28px 26px 33px 25px;*/
    }
    .popup
    {
        border-radius: 7px;
        background: #6b6a63;
        margin: 30px auto 0;
        padding: 6px;
        position: absolute;
        width: 900px;
        top: 20%;
        left: 50%;
        margin-left: -400px;
        margin-top: -40px;
        z-index: 6;
    }
    .overlay
    {
        width: 100%;
        opacity: 0.65;
        height: 100%;
        left: 0; /*IE*/
        top: 10px;
        text-align: center;
        z-index: 5;
        position: fixed;
        background-color: #444444;
    }
</style>
<asp:Panel ID="pnlIncludedContacts" runat="server" Width="100%">
    <asp:HiddenField runat="server" ID="HfIndividualsType" Value="Applicants" />
    <asp:HiddenField ID="hfSingleSelect" runat="server" />
    <table>
        <tr>
            <td>
                <telerik:radgrid id="PresentationGrid" runat="server" showstatusbar="true" autogeneratecolumns="False"
                    pagesize="20" allowsorting="True" allowmultirowselection="False" allowpaging="True"
                    gridlines="None" allowautomaticdeletes="false" allowfilteringbycolumn="true"
                    allowautomaticinserts="false" allowautomaticupdates="True" enableviewstate="true"
                    showfooter="true" onneeddatasource="PresentationGrid_OnNeedDataSource" onitemcommand="PresentationGrid_OnItemCommand"
                    onitemdatabound="PresentationGrid_OnItemDataBound" onprerender="PresentationGrid_OnPreRender">
                    <pagerstyle mode="NumericPages"></pagerstyle>
                    <mastertableview allowmulticolumnsorting="True" width="100%" commanditemdisplay="Top"
                        name="Banks" tablelayout="Fixed" showfooter="true">
                        <CommandItemTemplate>
                            <div style="padding: 5px 5px;">                              
                                <telerik:RadButton runat="server" ID="btnAddNew" OnClick="LnkbtnAddNewIndividualClick"
                                    ToolTip="Add New Individual">
                                    <ContentTemplate>
                                        <img src="../images/add-icon.png" alt="" class="btnImageWithText" />
                                        <span class="riLabel">Add New Individual</span>
                                    </ContentTemplate>
                                </telerik:RadButton>
                                &nbsp;&nbsp;                             
                                <telerik:RadButton runat="server" ID="btnAddExisting" OnClick="lnkbtnAddExistngMembers_Click"
                                    ToolTip="Add Existing Individual">
                                    <ContentTemplate>
                                        <img src="../images/add-icon.png" alt="" class="btnImageWithText" />
                                        <span class="riLabel">Add Existing Individual</span>
                                    </ContentTemplate>
                                </telerik:RadButton>
                                &nbsp;&nbsp;
                            </div>
                        </CommandItemTemplate>
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="CID" ReadOnly="true" HeaderText="CID" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="CID" UniqueName="CID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="CLID" ReadOnly="true" HeaderText="CLID"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="CLID" UniqueName="CLID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="CSID" ReadOnly="true" HeaderText="CSID"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="CSID" UniqueName="CSID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="Name" ReadOnly="true" HeaderText="Given Name"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="Name" UniqueName="Name">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" SortExpression="MiddleName" HeaderText="Middle Name"
                                AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                                HeaderButtonType="TextButton" DataField="MiddleName" UniqueName="MiddleName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="Surname" HeaderText="Family Name" ReadOnly="true"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="Surname" UniqueName="Surname">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="160px" HeaderStyle-Width="200px" SortExpression="EmailAddress"
                                ReadOnly="true" HeaderText="Email Address" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="EmailAddress"
                                UniqueName="EmailAddress">
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn ButtonType="LinkButton" CommandName="Detail" Text="Detail"
                                UniqueName="DetailColumn">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyLinkButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                            <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                                UniqueName="EditColumn">
                                <HeaderStyle Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                            <telerik:GridButtonColumn ConfirmText="Are you sure you want to remove the association?"
                                ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                <HeaderStyle Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                        </Columns>
                    </mastertableview>
                </telerik:radgrid>
            </td>
        </tr>
    </table>
    <asp:Panel ID="ViewPanel" runat="server" Visible="false">
        <div style="margin: 20px 0 -22px; position: relative; text-align: right; background-image: -moz-linear-gradient(center bottom , #3E3B6A 43%, #3E3B6A 10%, #5B5886 85%);
            height: 29px; display: inline-block; line-height: 25px; width: 100%;">
            <span style="color: white; float: left;">&nbsp;Individual Details</span>
            <telerik:radbutton id="BtnCloseView" runat="server" width="17px" height="18px" onclick="BtnCloseView_Click">
                <image imageurl="~/images/cross_icon_normal.png" />
            </telerik:radbutton>
        </div>
        <asp:Panel ID="Panel1" runat="server" Enabled="false">
            <div class="content">
                <uc:IndividualControl ID="IndividualControlView" runat="server" />
            </div>
        </asp:Panel>
    </asp:Panel>
    <div id="IndividualModal" runat="server" visible="false" class="holder">
        <div class="popup">
            <div class="content">
                <uc:IndividualControl ID="IndividualControl" runat="server" />
            </div>
        </div>
    </div>
    <div class="overlay" id="OVER" visible="False" runat="server">
    </div>
    <div id="MemershipPopup" runat="server" visible="false" class="holder">
        <div class="popup">
            <div class="popup_Titlebar" id="PopupHeader" style="width: 100%">
                <div class="TitlebarLeft" style="width: 100%">
                    <table width="98%">
                        <tr>
                            <td style="text-align: left;">
                                <span>Contact Associatons</span>
                            </td>
                            <td style="text-align: right;">
                                <telerik:radbutton id="btnClose" runat="server" width="16.5px" height="15px" onclick="btnClose_OnClick">
                                    <image imageurl="~/images/cross_icon_normal.png" />
                                </telerik:radbutton>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="content">
                <UC1:ExistingContacts ID="ExistingContactControl" runat="server" />
            </div>
        </div>
    </div>
</asp:Panel>
