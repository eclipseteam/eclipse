﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddressControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.AddressControl" %>
<fieldset style="width: 96%">
    <asp:HiddenField runat="server" ID="txtmandtory" Value="false" />
    <legend>
        <asp:Label ID="lblTitle" runat="server">
        </asp:Label>
    </legend>
    <table width="100%">
        <tr style="font-family: Calibri;">
            <td style="text-align: right">
            </td>
            <td colspan="3" style="text-align: right; font-family: Calibri;">
             <asp:Label runat="server" Text="Fill As" ID="lblFillAS"></asp:Label>    &nbsp;
                <telerik:RadComboBox runat="server" ID="ddlSameAddress" OnSelectedIndexChanged="ddlSameAddress_SelectedIndexChanged"
                    EnableViewState="true" AutoPostBack="true">
                    <Items>
                        <telerik:RadComboBoxItem Value="Select" Text="--Select--"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem Value="BusinessAddress" Text="Business Address"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem Value="ResidentialAddress" Text="Residential Address"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem Value="RegisteredAddress" Text="Registered Address"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem Value="MailingAddress" Text="Mailing Address"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem Value="DuplicateMailingAddress" Text="Duplicate Mailing Address">
                        </telerik:RadComboBoxItem>
                    </Items>
                </telerik:RadComboBox>
                &nbsp;
                <telerik:RadButton ID="btnClear" runat="server" Text="Reset" OnClick="btnClear_Click">
                </telerik:RadButton>
            </td>
        </tr>
        <tr style="font-family: Calibri;">
            <td style="width: 150px">
                <span class="riLabel">Address Line 1</span>
            </td>
            <td style="width: 250px">
                <telerik:RadTextBox ID="txtaddressline1" runat="server" Width="220px" MaxLength="35">
                </telerik:RadTextBox>
            </td>
            <td style="width: 150px">
                <span class="riLabel">Address Line 2</span>
            </td>
            <td>
                <telerik:RadTextBox ID="txtaddressline2" runat="server" Width="220px" MaxLength="35">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr style="font-family: Calibri;">
            <td>
                <span class="riLabel">Suburb *</span>
            </td>
            <td>
                <telerik:RadTextBox ID="txtsuburb" runat="server" Width="220px" MaxLength="20">
                </telerik:RadTextBox>
            </td>
            <td>
                <span class="riLabel">Post Code *</span>
            </td>
            <td>
                <telerik:RadTextBox ID="txtpostcode" runat="server" Width="220px" MaxLength="5">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr style="font-family: Calibri;">
            <td>
                <span class="riLabel">Country</span>
            </td>
            <td>
                <telerik:RadComboBox runat="server" ID="ddlcountry" Width="220px" AutoPostBack="true"
                    OnSelectedIndexChanged="ddlcountry_OnSelectedIndexChanged">
                </telerik:RadComboBox>
            </td>
            <td>
                <span class="riLabel">State *</span>
            </td>
            <td>
                <telerik:RadComboBox runat="server" ID="ComboState" Width="220px">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td colspan="3">
                <asp:Label ID="lblMessages" runat="server" ForeColor="#990000" Font-Bold="True">
                </asp:Label>
            </td>
        </tr>
    </table>
</fieldset>
