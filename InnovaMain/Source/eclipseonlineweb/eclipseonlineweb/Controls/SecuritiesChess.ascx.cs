﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.CM;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;

namespace eclipseonlineweb.Controls
{
    public partial class SecuritiesChess : UserControl
    {
        private string _cid = string.Empty;
        private ICMBroker Broker
        {
            get
            {
                return ((UMABasePage) Page).UMABroker;
            }
        }

        #region Chess

        public bool TransferListedSecuritiesToDesktopBroker
        {
            get
            {
                return btnChessTransYes.Checked;
            }
            set
            {
                if (value)
                {
                    btnChessTransYes.Checked = true;
                    btnChessTransNo.Checked = false;
                }
                else
                {
                    btnChessTransNo.Checked = true;
                    btnChessTransYes.Checked = false;
                }
            }
        }
        public string ChessDIYDIWM
        {
            get
            {
                if (btnServiceTypeDIY.Checked)
                    return btnServiceTypeDIY.Value;
                return btnServiceTypeDIWM.Checked ? btnServiceTypeDIWM.Value : string.Empty;
            }
            set
            {
                if (value != string.Empty)
                {
                    if (value == btnServiceTypeDIY.Value)
                    {
                        btnServiceTypeDIY.Checked = true;
                        btnServiceTypeDIWM.Checked = false;
                    }
                    else if (value == btnServiceTypeDIWM.Value)
                    {
                        btnServiceTypeDIWM.Checked = true;
                        btnServiceTypeDIY.Checked = false;
                    }
                }
            }
        }
        public string ChessRegisteredAccountName
        {
            get
            {
                return txtRegAccountName.Text;
            }
            set
            {
                txtRegAccountName.Text = value;
            }
        }
        public string ChessAccountDesignation
        {
            get
            {
                string ad1 = !string.IsNullOrWhiteSpace(txtad1.Text) ? txtad1.Text : " ";
                string ad2 = !string.IsNullOrWhiteSpace(txtad2.Text) ? txtad2.Text : " ";
                string ad3 = !string.IsNullOrWhiteSpace(txtad3.Text) ? txtad3.Text : " ";
                string ad4 = !string.IsNullOrWhiteSpace(txtad4.Text) ? txtad4.Text : " ";
                string ad5 = !string.IsNullOrWhiteSpace(txtad5.Text) ? txtad5.Text : " ";
                string ad6 = !string.IsNullOrWhiteSpace(txtad6.Text) ? txtad6.Text : " ";
                string ad7 = !string.IsNullOrWhiteSpace(txtad7.Text) ? txtad7.Text : " ";
                string ad8 = !string.IsNullOrWhiteSpace(txtad8.Text) ? txtad8.Text : " ";
                string ad9 = !string.IsNullOrWhiteSpace(txtad9.Text) ? txtad9.Text : " ";
                string ad10 = !string.IsNullOrWhiteSpace(txtad10.Text) ? txtad10.Text : " ";
                string ad11 = !string.IsNullOrWhiteSpace(txtad11.Text) ? txtad11.Text : " ";
                string ad12 = !string.IsNullOrWhiteSpace(txtad12.Text) ? txtad12.Text : " ";
                string ad13 = !string.IsNullOrWhiteSpace(txtad13.Text) ? txtad13.Text : " ";
                string ad14 = !string.IsNullOrWhiteSpace(txtad14.Text) ? txtad14.Text : " ";
                string ad15 = !string.IsNullOrWhiteSpace(txtad15.Text) ? txtad15.Text : " ";
                string ad16 = !string.IsNullOrWhiteSpace(txtad16.Text) ? txtad16.Text : " ";
                string ad17 = !string.IsNullOrWhiteSpace(txtad17.Text) ? txtad17.Text : " ";
                string ad18 = !string.IsNullOrWhiteSpace(txtad18.Text) ? txtad18.Text : " ";
                string ad19 = !string.IsNullOrWhiteSpace(txtad19.Text) ? txtad19.Text : " ";
                string ad20 = !string.IsNullOrWhiteSpace(txtad20.Text) ? txtad20.Text : " ";
                string ad21 = !string.IsNullOrWhiteSpace(txtad21.Text) ? txtad21.Text : " ";
                string ad22 = !string.IsNullOrWhiteSpace(txtad22.Text) ? txtad22.Text : " ";
                string ad23 = !string.IsNullOrWhiteSpace(txtad23.Text) ? txtad23.Text : " ";
                string ad24 = !string.IsNullOrWhiteSpace(txtad24.Text) ? txtad24.Text : " ";
                string ad25 = !string.IsNullOrWhiteSpace(txtad25.Text) ? txtad25.Text : " ";
                string ad26 = !string.IsNullOrWhiteSpace(txtad26.Text) ? txtad26.Text : " ";
                string ad27 = !string.IsNullOrWhiteSpace(txtad27.Text) ? txtad27.Text : " ";
                string ad28 = !string.IsNullOrWhiteSpace(txtad28.Text) ? txtad28.Text : " ";
                string ad29 = !string.IsNullOrWhiteSpace(txtad29.Text) ? txtad29.Text : " ";
                string ad30 = !string.IsNullOrWhiteSpace(txtad30.Text) ? txtad30.Text : " ";

                string designation = ad1 + ad2 + ad3 + ad4 + ad5 + ad6 + ad7 + ad8 + ad9 + ad10 + ad11 + ad12 + ad13 + ad14 + ad15 + ad16 + ad17 + ad18 + ad19 + ad20 + ad21 + ad22 + ad23 + ad24 + ad25 + ad26 + ad27 + ad28 + ad29 + ad30;
                return designation;
            }
            set
            {
                for (int i = 0; i < value.Length; i++)
                {
                    switch (i)
                    {
                        case 0:
                            txtad1.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 1:
                            txtad2.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 2:
                            txtad3.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 3:
                            txtad4.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 4:
                            txtad5.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 5:
                            txtad6.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 6:
                            txtad7.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 7:
                            txtad8.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 8:
                            txtad9.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 9:
                            txtad10.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 10:
                            txtad11.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 11:
                            txtad12.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 12:
                            txtad13.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 13:
                            txtad14.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 14:
                            txtad15.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 15:
                            txtad16.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 16:
                            txtad17.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 17:
                            txtad18.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 18:
                            txtad19.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 19:
                            txtad20.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 20:
                            txtad21.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 21:
                            txtad22.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 22:
                            txtad23.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 23:
                            txtad24.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 24:
                            txtad25.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 25:
                            txtad26.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 26:
                            txtad27.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 27:
                            txtad28.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 28:
                            txtad29.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 29:
                            txtad30.Text = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                    }

                }
            }
        }
        public string ChessAddressLine1
        {
            get
            {
                return txtAddressLine1Chess.Text;
            }
            set
            {
                txtAddressLine1Chess.Text = value;
            }
        }
        public string ChessAddressLine2
        {
            get
            {
                return txtAddressLine2Chess.Text;
            }
            set
            {
                txtAddressLine2Chess.Text = value;
            }
        }
        public string ChessSuburb
        {
            get
            {
                return txtSuburbChess.Text;
            }
            set
            {
                txtSuburbChess.Text = value;
            }
        }
        public string ChessState
        {
            get
            {
                return CmboStatesChess.SelectedItem != null ? CmboStatesChess.SelectedItem.Text : CmboStatesChess.Text;
            }
            set
            {
                CmboStatesChess.Text = value;
            }
        }
        public string ChessPostCode
        {
            get
            {
                return txtPostCodeChess.Text;
            }
            set
            {
                txtPostCodeChess.Text = value;
            }
        }
        public string ChessCountry
        {
            get
            {
                return CmboCountryChess.SelectedItem != null ? CmboCountryChess.SelectedItem.Text : CmboCountryChess.Text;
            }
            set
            {
                CmboCountryChess.Text = value;
            }
        }
        public bool IssuedSponsor
        {
            get
            {
                return chkIssueSponsorChess.Checked;
            }
            set
            {
                chkIssueSponsorChess.Checked = value;
            }
        }
        public bool ExactNameMatch
        {
            get
            {
                return chkExactNameAddressMatchAbove.Checked;
            }
            set
            {
                chkExactNameAddressMatchAbove.Checked = value;
            }
        }
        public string ExactNameMatchAmount
        {
            get
            {
                return txtHowManyHoldingCompanyChess.Text;
            }
            set
            {
                txtHowManyHoldingCompanyChess.Text = value;
            }
        }
        public bool ExactNameNoMatch
        {
            get
            {
                return chkExactNameAddressNOTMatchAbove.Checked;
            }
            set
            {
                chkExactNameAddressNOTMatchAbove.Checked = value;
            }
        }
        public string ExactNameNoMatchAmount
        {
            get
            {
                return txtHowManyHoldingCompanyChessNotMatch.Text;
            }
            set
            {
                txtHowManyHoldingCompanyChessNotMatch.Text = value;
            }
        }
        public bool BrokerSponsored
        {
            get
            {
                return chkBrokerSponsoredRegistration.Checked;
            }
            set
            {
                chkBrokerSponsoredRegistration.Checked = value;
            }
        }
        public string ExistingBroker1
        {
            get
            {
                return txtNameOfSponsorBroker1.Text;
            }
            set
            {
                txtNameOfSponsorBroker1.Text = value;
            }
        }
        public string ClientNo1
        {
            get
            {
                return txtBrokerID1.Text;
            }
            set
            {
                txtBrokerID1.Text = value;
            }
        }
        public string ClientHin1
        {
            get
            {
                return txtClientHin1.Text;
            }
            set
            {
                txtClientHin1.Text = value;
            }
        }
        public string ExistingBroker2
        {
            get
            {
                return txtNameOfSponsorBroker2.Text;
            }
            set
            {
                txtNameOfSponsorBroker2.Text = value;
            }
        }
        public string ClientNo2
        {
            get
            {
                return txtBrokerID2.Text;
            }
            set
            {
                txtBrokerID2.Text = value;
            }
        }
        public string ClientHin2
        {
            get
            {
                return txtClientHin2.Text;
            }
            set
            {
                txtClientHin2.Text = value;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            _cid = Request.QueryString["ins"];
            if (!Page.IsPostBack && !string.IsNullOrEmpty(_cid))
            {
                SetData();
            }
        }

        private void SetData()
        {

            var clientData = Broker.GetBMCInstance(new Guid(_cid)) as OrganizationUnitCM;
            if (clientData != null)
            {
                var entity = clientData.ClientEntity as IClientUMAData;
                if (entity != null)
                {
                    if (entity.ChessTransferofSecurity == null)
                    {
                        entity.ChessTransferofSecurity = new ClientChessTransferofSecurityEntity();
                    }
                    SetControlData(entity.ChessTransferofSecurity);
                }
            }
        }

        private void SetControlData(ClientChessTransferofSecurityEntity entity)
        {
            #region Chess

            TransferListedSecuritiesToDesktopBroker = entity.TransferListedSecuritiesToDesktopBroker;
            ChessDIYDIWM = entity.ChessDIYDIWM;
            ChessRegisteredAccountName = entity.ChessRegisteredAccountName;
            ChessAccountDesignation = entity.ChessAccountDesignation;
            ChessAddressLine1 = entity.ChessAddressLine1;
            ChessAddressLine2 = entity.ChessAddressLine2;
            ChessSuburb = entity.ChessSuburb;
            ChessState = entity.ChessState;
            ChessPostCode = entity.ChessPostCode;
            ChessCountry = entity.ChessCountry;
            IssuedSponsor = entity.IssuedSponsor;
            ExactNameMatch = entity.ExactNameMatch;
            ExactNameMatchAmount = entity.ExactNameMatchAmount;
            ExactNameNoMatch = entity.ExactNameNoMatch;
            ExactNameNoMatchAmount = entity.ExactNameNoMatchAmount;
            BrokerSponsored = entity.BrokerSponsored;
            ExistingBroker1 = entity.ExistingBroker1;
            ClientNo1 = entity.ClientNo1;
            ClientHin1 = entity.ClientHin1;
            ExistingBroker2 = entity.ExistingBroker2;
            ClientNo2 = entity.ClientNo2;
            ClientHin2 = entity.ClientHin2;

            #endregion
        }

        private void SetEntityValues(ClientChessTransferofSecurityEntity entity)
        {
            #region Chess

            entity.TransferListedSecuritiesToDesktopBroker = TransferListedSecuritiesToDesktopBroker;
            entity.ChessDIYDIWM = ChessDIYDIWM;
            entity.ChessRegisteredAccountName = ChessRegisteredAccountName;
            entity.ChessAccountDesignation = ChessAccountDesignation;
            entity.ChessAddressLine1 = ChessAddressLine1;
            entity.ChessAddressLine2 = ChessAddressLine2;
            entity.ChessSuburb = ChessSuburb;
            entity.ChessState = ChessState;
            entity.ChessPostCode = ChessPostCode;
            entity.ChessCountry = ChessCountry;
            entity.IssuedSponsor = IssuedSponsor;
            entity.ExactNameMatch = ExactNameMatch;
            entity.ExactNameMatchAmount = ExactNameMatchAmount;
            entity.ExactNameNoMatch = ExactNameNoMatch;
            entity.ExactNameNoMatchAmount = ExactNameNoMatchAmount;
            entity.BrokerSponsored = BrokerSponsored;
            entity.ExistingBroker1 = ExistingBroker1;
            entity.ClientNo1 = ClientNo1;
            entity.ClientHin1 = ClientHin1;
            entity.ExistingBroker2 = ExistingBroker2;
            entity.ClientNo2 = ClientNo2;
            entity.ClientHin2 = ClientHin2;

            #endregion
        }

        public void Save()
        {
            var clientData = Broker.GetBMCInstance(new Guid(_cid)) as OrganizationUnitCM;
            if (clientData != null)
            {
                clientData.UpdationDate = DateTime.Now;

                var entity = clientData.ClientEntity as IClientUMAData;
                if (entity != null) SetEntityValues(entity.ChessTransferofSecurity);

                Broker.SaveOverride = true;
                clientData.CalculateToken(true);
                Broker.SetComplete();
                Broker.SetStart();
            }
            SetData();
        }
    }
}