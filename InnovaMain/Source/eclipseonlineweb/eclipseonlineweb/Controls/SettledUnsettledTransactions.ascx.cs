﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;
using System.Diagnostics;

namespace eclipseonlineweb.Controls
{
    public partial class SettledUnsettledTransactions : UserControl
    {
        private DataView SettledUnsettledView { get; set; }
        private string Filter { get; set; }

        public bool IsAdmin
        {
            private get { return Convert.ToBoolean(hfIsAdmin.Value); }
            set { hfIsAdmin.Value = value.ToString(); }
        }

        public bool IsAdminMenu
        {
            private get
            {
                if (string.IsNullOrEmpty(hfIsAdminMenu.Value))
                {
                    hfIsAdminMenu.Value = "false";
                }
                return Convert.ToBoolean(hfIsAdminMenu.Value);
            }
            set { hfIsAdminMenu.Value = value.ToString(); }
        }

        private string ClientAccID
        {
            get { return hfClientID.Value; }
            set { hfClientID.Value = value; }
        }

        public Guid OrderId
        {
            private get
            {
                return string.IsNullOrEmpty(hfOrderId.Value) ? Guid.Empty : Guid.Parse(hfOrderId.Value);
            }
            set { hfOrderId.Value = value.ToString(); }
        }

        public Action<string, DataSet> SaveData { get; set; }

        private ICMBroker Broker
        {
            get { return (Page as UMABasePage).UMABroker; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnBack.Visible = OrderId != Guid.Empty;
                ClientHeaderInfo1.Visible = !IsAdminMenu;
                btnExcel.Visible = IsAdmin;
                LoadControls();
                //Default Filter
                SetStatusFilter();
                if (Request.QueryString["Page"] != null)
                {
                    string page = string.Empty;
                    switch (Request.QueryString["Page"])
                    {
                        case "1":
                            page = "Active Orders";
                            break;
                        case "2":
                            page = "Archived Orders";
                            break;
                    }
                    btnBack.ToolTip = string.Format("Back To {0}", page);
                }
            }
        }

        private void LoadControls()
        {
            //Status List binding
            var orderStatus = Enum.GetNames(typeof(OrderStatus));
            foreach (var status in orderStatus)
            {
                if (status == OrderStatus.Submitted.ToString() || status == OrderStatus.Complete.ToString())
                {
                    lstStatus.Items.Add(new RadListBoxItem(status));
                }
            }
            lstStatus.DataBind();


            foreach (RadListBoxItem item in lstStatus.Items)
            {
                item.Checked = true;
                item.Font.Bold = true;
            }
        }

        protected void PresentationGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            SetDatSource();
        }

        private void SetDatSource()
        {
            var ds = GetSettledUnsetteledDs();

            SettledUnsettledView = ds.Tables[SettledUnsetteledList.TABLENAME].DefaultView;
            FilterRecords();
            SettledUnsettledView.RowFilter = Filter;
            //sort by order id/number desc 
            SettledUnsettledView.Sort = string.Format("{0} DESC, {1} DESC, {2} ASC, {3} ASC, {4} ASC",
                                                      ds.SettledUnsetteledTable.ORDERID,
                                                      ds.SettledUnsetteledTable.ENTRYTYPE,
                                                      ds.SettledUnsetteledTable.ORDERITEMID,
                                                      ds.SettledUnsetteledTable.AMOUNT,
                                                      ds.SettledUnsetteledTable.TRANSACTIONTYPE);
            PresentationGrid.DataSource = SettledUnsettledView;
            PresentationGrid.Columns.FindByUniqueName("ClientID").Visible = IsAdminMenu;
        }

        private SettledUnsetteledDS GetSettledUnsetteledDs()
        {
            if (!Page.IsPostBack || ((UMABasePage)Page).PresentationData == null)
            {
                GetData();
            }
            SettledUnsetteledDS ds = ((UMABasePage)Page).PresentationData as SettledUnsetteledDS;
            return ds;
        }

        private void GetData()
        {
            var unit = new OrganizationUnit
                {
                    Type = ((int)OrganizationType.SettledUnsettled).ToString(),
                    CurrentUser = (Page as UMABasePage).GetCurrentUser()
                };

            if (!IsAdminMenu)
            {
                unit.ClientId = ClientHeaderInfo1.ClientId;
                unit.Name = ClientHeaderInfo1.ClientId;
            }
            else if (Request.QueryString["ClientId"] != null)
            {
                unit.ClientId = Request.QueryString["ClientId"];
                unit.Name = Request.QueryString["ClientId"];
            }
            //Orders Settled UnSettled Lists
            var ds = new SettledUnsetteledDS
                {
                    Unit = unit,
                    CommandType = DatasetCommandTypes.Get,
                    Command = (int)WebCommands.GetOrganizationUnitsByType
                };

            if (OrderId != Guid.Empty)
            {
                ds.OrderId = OrderId;
            }

            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            DateTime start = DateTime.Now;
            //Orders Settled UnSettled Lists
            org.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(org);
            Debug.WriteLine(String.Format("SettledUnsettledTransactions GetData SettledUnsetteledDS {0}", DateTime.Now - start));
            ((UMABasePage)Page).PresentationData = ds;
        }

        protected void btnApplyFilter_OnClick(object sender, EventArgs e)
        {
            RebindData();
        }

        private void SetStatusFilter()
        {
            foreach (RadListBoxItem item in lstStatus.Items)
            {
                item.Font.Bold = item.Checked;
            }

            var sb = new StringBuilder();
            IList<RadListBoxItem> collection = lstStatus.CheckedItems;
            foreach (RadListBoxItem item in collection)
            {
                sb.Append("'" + item.Value + "', ");
            }

            string statusFilter = sb.Length > 0 ? sb.Remove(sb.ToString().Trim().Length - 1, 1).ToString() : "''";

            hfStatusFilter.Value = statusFilter;
        }

        protected void PresentationGrid_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var item = e.Item as GridDataItem;
            if (IsAdmin)
            {
                var tansType = item["TransactionType"].Text;
                var accType = item["AccountType"].Text;
                var settledUnSettledType = item["Type"].Text;
                var clientManagementType = item["ClientManagementType"].Text;
                var entryType = item["EntryType"].Text;
                var orderBankAccountType = item["OrderBankAccountType"].Text;
                var orderType = item["OrderType"].Text;

                //hide delete manual confirmation: AtCall or TD
                if (tansType == SetlledunSettledTransactionType.Confirmation.ToString())
                {
                    item.ForeColor = Color.Green;
                    if (accType != OrderAccountType.AtCall.ToString() && accType != OrderAccountType.TermDeposit.ToString())
                    {
                        var imgDeleteLink = item.FindControl("imgDeleteLink") as ImageButton;
                        if (imgDeleteLink != null)
                        {
                            imgDeleteLink.Visible = true;
                        }
                    }
                }

                var imgAddLink = item.FindControl("imgAddLink") as ImageButton;
                if (imgAddLink != null)
                {
                    //hide add manual confirmation: OnMarket or Settled OR AtCall or TD
                    bool hideAddLink = settledUnSettledType == OrderSetlledUnsettledType.OnMarket.ToString() ||
                                       settledUnSettledType == OrderSetlledUnsettledType.Settled.ToString() ||
                                       accType == OrderAccountType.AtCall.ToString() ||
                                       accType == OrderAccountType.TermDeposit.ToString() ||
                                       (accType == OrderAccountType.BankAccount.ToString() && entryType == SetlledunSettledEntryType.SettlementTransactions.ToString() && orderBankAccountType == OrderBankAccountType.External.ToString()) ||
                                       (clientManagementType == ClientManagementType.SMA.ToString() && accType == OrderAccountType.BankAccount.ToString() && orderType == OrderAccountType.TermDeposit.ToString());
                    if (hideAddLink)
                    {
                        imgAddLink.Visible = false;
                    }
                    else
                    {
                        var investmentCode = item["InvestmentCode"].Text;
                        var accNo = item["AccountNumber"].Text;
                        var id = item["ID"].Text;
                        var clientCid = item["ClientCID"].Text;
                        var clientId = ((HyperLink)item["ClientID"].Controls[0]).Text;

                        imgAddLink.Visible = true;
                        string filter = string.Empty;

                        OrderAccountType accountType;
                        bool isValidOrderStatus = Enum.TryParse(accType, out accountType);
                        if (isValidOrderStatus)
                        {
                            switch (accountType)
                            {
                                case OrderAccountType.AtCall:
                                case OrderAccountType.TermDeposit:
                                    break;
                                case OrderAccountType.StateStreet:
                                case OrderAccountType.ASX:
                                    filter = investmentCode;
                                    break;
                                case OrderAccountType.BankAccount:
                                    filter = accNo;
                                    break;
                            }
                        }

                        imgAddLink.OnClientClick = string.Format("return ShowEditForm('{0}','{1}','{2}','{3}','{4}')", clientCid, accType, clientId, filter, id);
                    }
                }
            }
        }

        private void FilterRecords()
        {
            //Default Filter for Settled Unsettled
            Filter = string.Format("Type <> '{0}' and OrderStatus in ({1})", OrderSetlledUnsettledType.Initialized, hfStatusFilter.Value);

            string dateFilter = string.Empty;
            if (!string.IsNullOrEmpty(calFrom.SelectedDate.ToString()) && !string.IsNullOrEmpty(calTo.SelectedDate.ToString()))
            {
                dateFilter = string.Format(" and ((CreatedDate >= #{0}# and CreatedDate <= #{1}#) OR (UpdatedDate >= #{0}# and UpdatedDate <= #{1}#) OR (TradeDate >= #{0}# and TradeDate <= #{1}#))", Convert.ToDateTime(calFrom.SelectedDate).ToString("MM/dd/yyyy"), Convert.ToDateTime(calTo.SelectedDate).ToString("MM/dd/yyyy 23:59:59"));
            }
            else
            {
                if (!string.IsNullOrEmpty(calFrom.SelectedDate.ToString()))
                {
                    dateFilter += string.Format(" and (CreatedDate >= #{0}# OR UpdatedDate >= #{0}# OR TradeDate >= #{0}#)", Convert.ToDateTime(calFrom.SelectedDate).ToString("MM/dd/yyyy"));
                }

                if (!string.IsNullOrEmpty(calTo.SelectedDate.ToString()))
                {
                    dateFilter += string.Format(" and (CreatedDate <= #{0}# OR UpdatedDate <= #{0}# OR TradeDate <= #{0}#)", Convert.ToDateTime(calTo.SelectedDate).ToString("MM/dd/yyyy 23:59:59"));
                }
            }
            Filter += dateFilter;
            //if (IsPostBack)
            //    PresentationGrid.Rebind();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            var selectedCommand = DatasetCommandTypes.Get;
            bool isCommandFound = false;
            switch (e.CommandName.ToLower())
            {
                case "delete":
                    isCommandFound = true;
                    selectedCommand = DatasetCommandTypes.Delete;
                    break;
            }

            if (isCommandFound)
            {
                var dataItem = e.Item as GridDataItem;
                if (dataItem != null)
                {
                    Guid id = Guid.Parse(e.CommandArgument.ToString());
                    ClientAccID = ((HyperLink)dataItem["ClientID"].Controls[0]).Text;
                    ExecuteCommand(selectedCommand, id);
                }
            }
        }

        private void ExecuteCommand(DatasetCommandTypes command, Guid id = default(Guid))
        {
            var unit = new OrganizationUnit
                           {
                               ClientId = ClientAccID,
                               Type = ((int)OrganizationType.SettledUnsettled).ToString(),
                               CurrentUser = (Page as UMABasePage).GetCurrentUser()
                           };

            var ds = new SettledUnsetteledDS { CommandType = command, Unit = unit };

            if (id != Guid.Empty)
            {
                DataRow dr = ds.SettledUnsetteledTable.NewRow();
                dr[ds.SettledUnsetteledTable.ID] = id;
                ds.SettledUnsetteledTable.Rows.Add(dr);
            }

            Guid settledUnSettledCMCID = OrderPadUtilities.IsSettledUnSettledCmExists(Broker, (Page as UMABasePage).GetCurrentUser(), ClientAccID);

            if (settledUnSettledCMCID != Guid.Empty)
            {
                SaveData(settledUnSettledCMCID.ToString(), ds);
            }

            // refresh grid
            PresentationGrid.Rebind();
        }

        protected void cbMulti_OnCheckedChanged(object sender, EventArgs e)
        {
            var chk = (CheckBox)sender;
            var itm = (GridDataItem)chk.NamingContainer;
            int crrntindex = itm.ItemIndex;

            foreach (GridDataItem row in PresentationGrid.MasterTableView.Items)
            {
                var chk1 = (CheckBox)row.FindControl("cbMulti");
                if (chk1 != null)
                {
                    int index = row.ItemIndex;
                    if (crrntindex == index)
                    {
                        ArrayList selectedItems = ViewState["selectedItems"] == null ? new ArrayList() : (ArrayList)ViewState["selectedItems"];

                        if (chk1.Checked)
                        {
                            if (hfPopupClientCID.Value.Trim().Length == 0 || (hfPopupClientCID.Value == row["ClientCID"].Text && hfPopupFilter.Value == row["AccountNumber"].Text))
                            {
                                string id = row["ID"].Text;
                                selectedItems.Add(id);
                                ViewState["selectedItems"] = selectedItems;
                                hfPopupAccountType.Value = row["AccountType"].Text;
                                hfPopupClientCID.Value = row["ClientCID"].Text;
                                hfPopupClientID.Value = ((HyperLink)row["ClientID"].Controls[0]).Text;
                                hfPopupFilter.Value = row["AccountNumber"].Text;
                            }
                            else
                            {
                                chk1.Checked = false;
                                ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "AlertWind", "alert('Please select same client having same account number.');", true);
                            }
                        }
                        else
                        {
                            string id = row["ID"].Text;
                            selectedItems.Remove(id);
                            if (selectedItems.Count > 0)
                            {
                                ViewState["selectedItems"] = selectedItems;
                            }
                            else
                            {
                                ClearPopupValues();
                            }
                        }

                        if (ViewState["selectedItems"] != null)
                        {
                            var items = (ArrayList)ViewState["selectedItems"];
                            var ids = String.Join(",", items.ToArray());
                            string script = string.Format("function(sender, args){{ShowEditFormMultiple(sender,args,'{0}','{1}','{2}','{3}','{4}');}}", hfPopupClientCID.Value, hfPopupAccountType.Value, hfPopupClientID.Value, hfPopupFilter.Value, ids);
                            btnAttachMultiple.OnClientClicked = script;
                        }
                    }
                }
            }

        }

        protected void PresentationGrid_OnPreRender(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hfPopupClientCID.Value.Trim()))
            {
                ClearPopupValues();
            }

            if (ViewState["selectedItems"] != null)
            {
                var selectedItems = (ArrayList)ViewState["selectedItems"];
                for (int i = 0; i <= selectedItems.Count - 1; i++)
                {
                    string curItem = selectedItems[i].ToString();
                    foreach (GridItem item in PresentationGrid.MasterTableView.Items)
                    {
                        if (item is GridDataItem)
                        {
                            var dataItem = (GridDataItem)item;
                            if (curItem.Equals(dataItem["ID"].Text))
                            {
                                var chk1 = (CheckBox)dataItem.FindControl("cbMulti");
                                if (chk1 != null)
                                {
                                    chk1.Checked = true;
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void ClearPopupValues()
        {
            ViewState["selectedItems"] = null;
            hfPopupAccountType.Value = string.Empty;
            hfPopupClientCID.Value = string.Empty;
            hfPopupClientID.Value = string.Empty;
            hfPopupFilter.Value = string.Empty;
            btnAttachMultiple.OnClientClicked = "ShowEmtypAlert";
        }

        protected void btnExcel_OnClick(object sender, EventArgs e)
        {
            var ds = GetSettledUnsetteledDs();

            DataView dataView = ds.SettledUnsetteledTable.DefaultView;
            FilterRecords();
            dataView.RowFilter = Filter;
            //sort by order id/number desc 
            dataView.Sort = string.Format("{0} DESC, {1} DESC, {2} ASC, {3} ASC, {4} ASC",
                                                      ds.SettledUnsetteledTable.ORDERID,
                                                      ds.SettledUnsetteledTable.ENTRYTYPE,
                                                      ds.SettledUnsetteledTable.ORDERITEMID,
                                                      ds.SettledUnsetteledTable.AMOUNT,
                                                      ds.SettledUnsetteledTable.TRANSACTIONTYPE);
            var excelDataset = new DataSet();
            DataTable dt = dataView.ToTable();
            foreach (DataColumn col in ds.SettledUnsetteledTable.Columns)
            {
                if (col.DataType == typeof(Guid))
                {
                    dt.Columns.Remove(col.ColumnName);
                }
            }
            excelDataset.Merge(dt, true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, string.Format("OrderDetails{0}.xls", DateTime.Now.ToFileTime()), Page.Response);
        }

        private void RebindData()
        {
            SetStatusFilter();
            PresentationGrid.CurrentPageIndex = 0;
            PresentationGrid.Rebind();
        }
    }
}