﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SettledUnsettledTransactions.ascx.cs"
    Inherits="eclipseonlineweb.Controls.SettledUnsettledTransactions" %>
<%@ Import Namespace="Oritax.TaxSimp.Common" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<style type="text/css">
    .RadListBox .rlbSelected
    {
        background-color: transparent !important;
    }
    .RadListBox span.rlbText
    {
        color: black !important;
    }
    .RadGrid_Metro .rgHeader, .RadGrid_Metro .rgHeader a
    {
        font-size: 7.5pt !important;
    }
    .RadGrid_Metro .rgRow a, .RadGrid_Metro .rgAltRow a, .RadGrid_Metro tr.rgEditRow a, .RadGrid_Metro .rgFooter a, .RadGrid_Metro .rgEditForm a
    {
        font-size: 7.5pt !important;
    }
    .rbPrimaryIcon
    {
        left: 4px !important;
        top: 0 !important;
    }
</style>
<script type="text/javascript">
    function ShowEditForm(cid, accountType, clientId, filter, entityId) {
        window.radopen("../AttachExistingTransaction.aspx?ins=" + cid + "&aType=" + accountType + "&clientId=" + clientId + "&filter=" + filter + "&entityId=" + entityId, "UserListDialog");
        var hfpopup = document.getElementById("MainContent_MainContent_SettledUnsettledTransactionsControl_hfPopupClientCID");
        hfpopup.value = "";
        return false;
    }

    function ShowEditFormMultiple(sender, args, cid, accountType, clientId, filter, entityId) {
        window.radopen("../AttachExistingTransaction.aspx?ins=" + cid + "&aType=" + accountType + "&clientId=" + clientId + "&filter=" + filter + "&entityId=" + entityId, "UserListDialog");
        var hfpopup = document.getElementById("MainContent_MainContent_SettledUnsettledTransactionsControl_hfPopupClientCID");
        hfpopup.value = "";
        sender.set_autoPostBack(false);
    }

    function refreshGrid() {
        window.$find("<%: PresentationGrid.ClientID %>").get_masterTableView().rebind();
    }

    function onItemChecked(sender, e) {
        var item = e.get_item();
        var checked = item.get_checked();
        if (checked) {
            item.get_element().attributes["style"].value = "font-weight:Bold";
        }
        else {
            item.get_element().attributes["style"].value = "font-weight:Normal";
        }
    }

    function ShowEmtypAlert(sender, args) {
        alert("Please select an item first.");
        sender.set_autoPostBack(false);
    }

    function BackToPage(sender, args) {
        history.go(-1);
        sender.set_autoPostBack(true);
    }
    
</script>
<asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="30%">
                        <telerik:RadButton runat="server" ID="btnBack" ToolTip="Back" OnClientClicked="BackToPage"
                            Visible="false">
                            <ContentTemplate>
                                <img src="../images/window_previous.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Back</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ID="btnAttachMultiple" ToolTip="Attach multiple confirmations"
                            OnClientClicked="ShowEmtypAlert">
                            <ContentTemplate>
                                <img src="../images/multilink.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Attach Confirmations</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        &nbsp;
                        <telerik:RadButton runat="server" ID="btnExcel" ToolTip="Download as Excel" OnClick="btnExcel_OnClick">
                            <ContentTemplate>
                                <img src="../images/export_excel.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Download</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </td>
                    <td style="width: 30%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <br />
        <div id="MainView" style="background-color: white">
            <table style="width: 100%; display: none">
                <tr>
                    <td style="width: 100px">
                        Order Status:
                    </td>
                    <td style="width: 200px">
                        <telerik:RadListBox ID="lstStatus" runat="server" CheckBoxes="true" SelectionMode="Multiple"
                            OnClientItemChecked="onItemChecked">
                        </telerik:RadListBox>
                    </td>
                    <td style="width: 80px">
                        Date:
                    </td>
                    <td style="width: 50px">
                        From:
                    </td>
                    <td style="width: 150px">
                        <telerik:RadDatePicker ID="calFrom" runat="server" Width="110px">
                            <DateInput ID="DateInput1" runat="server" DateFormat="dd/MM/yyyy">
                            </DateInput>
                        </telerik:RadDatePicker>
                    </td>
                    <td style="width: 50px">
                        To:
                    </td>
                    <td style="width: 150px">
                        <telerik:RadDatePicker ID="calTo" runat="server" Width="110px">
                            <DateInput ID="DateInput2" runat="server" DateFormat="dd/MM/yyyy">
                            </DateInput>
                        </telerik:RadDatePicker>
                    </td>
                    <td>
                        <telerik:RadButton runat="server" ID="btnApplyFilter" Text="Apply Filter" OnClick="btnApplyFilter_OnClick">
                        </telerik:RadButton>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <telerik:RadGrid OnNeedDataSource="PresentationGridNeedDataSource" ID="PresentationGrid"
            runat="server" ShowStatusBar="true" AutoGenerateColumns="false" PageSize="20"
            AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" GridLines="None"
            AllowAutomaticDeletes="True" AllowFilteringByColumn="true" EnableViewState="true"
            ShowFooter="false" OnItemDataBound="PresentationGrid_OnItemDataBound" OnItemCommand="PresentationGrid_OnItemCommand"
            OnPreRender="PresentationGrid_OnPreRender">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                Name="ActiveOrders" TableLayout="Fixed" Font-Size="7.5">
                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                <GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldName="OrderNo" HeaderText="Order ID" />
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldName="OrderNo" SortOrder="Descending" />
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>
                <Columns>
                    <telerik:GridBoundColumn HeaderStyle-Width="170px" SortExpression="ID" ReadOnly="true"
                        HeaderText="ID" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ID" UniqueName="ID"
                        Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="ClientCID" HeaderText="ClientCID" ReadOnly="true"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="ClientCID" UniqueName="ClientCID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="ClientCID"
                        SortExpression="ClientID" UniqueName="ClientID" DataNavigateUrlFormatString="../ClientViews/ClientMainView.aspx?ins={0}"
                        HeaderText="Client ID" DataTextField="ClientID" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" FilterControlWidth="50px" HeaderStyle-Width="90px"
                        HeaderButtonType="TextButton" ShowFilterIcon="true" Visible="false">
                    </telerik:GridHyperLinkColumn>
                    <telerik:GridBoundColumn SortExpression="AccountCID" ReadOnly="true" HeaderText="AccountCID"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="AccountCID" UniqueName="AccountCID"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="70px" SortExpression="OrderID" ReadOnly="true"
                        HeaderText="OrderID" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="OrderID" UniqueName="OrderID"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="30px" HeaderStyle-Width="70px" SortExpression="OrderNo"
                        ReadOnly="true" HeaderText="Order ID" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="OrderNo" UniqueName="OrderNo"
                        GroupByExpression="OrderNo">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="OrderStatus" ReadOnly="true" HeaderText="OrderStatus"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="OrderStatus" UniqueName="OrderStatus"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="45px" HeaderStyle-Width="80px" SortExpression="AccountBSB"
                        ReadOnly="true" HeaderText="BSB" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountBSB" UniqueName="AccountBSB">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="85px" SortExpression="AccountNumber"
                        ReadOnly="true" HeaderText="Account No" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountNumber"
                        UniqueName="AccountNumber">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="ProductID" ReadOnly="true" HeaderText="ProductID"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="ProductID" UniqueName="ProductID" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="75px" SortExpression="InvestmentCode"
                        HeaderText="Investment Code" ReadOnly="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InvestmentCode"
                        UniqueName="InvestmentCode">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="30px" HeaderStyle-Width="65px" SortExpression="BrokerName"
                        HeaderText="Broker" ReadOnly="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BrokerName" UniqueName="BrokerName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="75px" SortExpression="BankName"
                        HeaderText="Bank" ReadOnly="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BankName" UniqueName="BankName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="95px" SortExpression="AccountType"
                        HeaderText="Account Type" ReadOnly="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountType" UniqueName="AccountType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="95px" SortExpression="OrderType"
                        HeaderText="Order Type" ReadOnly="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="OrderType" UniqueName="OrderType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="95px" SortExpression="OrderPreferedBy"
                        HeaderText="Prefered By" ReadOnly="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="OrderPreferedBy"
                        UniqueName="OrderPreferedBy">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="OrderItemID" ShowFilterIcon="true"
                        HeaderText="OrderItemID" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        HeaderButtonType="TextButton" DataField="OrderItemID" UniqueName="OrderItemID"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="75px" HeaderStyle-Width="110px" ReadOnly="true"
                        SortExpression="Units" HeaderText="Units" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Units" UniqueName="Units"
                        DataFormatString="{0:N4}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="75px" HeaderStyle-Width="110px" ReadOnly="true"
                        SortExpression="UnitPrice" HeaderText="Unit Price" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="UnitPrice" UniqueName="UnitPrice" DataFormatString="{0:N6}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="75px" HeaderStyle-Width="110px" ReadOnly="true"
                        SortExpression="Amount" HeaderText="Amount" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Amount" UniqueName="Amount"
                        DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="85px" ReadOnly="true"
                        SortExpression="Type" HeaderText="Type" AutoPostBackOnFilter="false" ShowFilterIcon="true"
                        CurrentFilterFunction="Contains" HeaderButtonType="TextButton" DataField="Type"
                        UniqueName="Type">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="55px" HeaderStyle-Width="90px" SortExpression="CreatedBy"
                        ReadOnly="true" HeaderText="Created By" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="OrderCreatedBy"
                        UniqueName="OrderCreatedBy">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="85px" HeaderStyle-Width="125px" ReadOnly="true"
                        SortExpression="CreatedDate" HeaderText="Create Date " AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="CreatedDate" UniqueName="CreatedDate" DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}"
                        DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="85px" HeaderStyle-Width="125px" ReadOnly="true"
                        SortExpression="UpdatedDate" HeaderText="Update Date" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="UpdatedDate" UniqueName="UpdatedDate" DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}"
                        DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="SettledAssociation" HeaderText="Settled Association"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="SettledAssociation" UniqueName="SettledAssociation"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="TransactionID" HeaderText="Transaction ID"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="TransactionID" UniqueName="TransactionID"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="35px" HeaderStyle-Width="70px" SortExpression="TradeType"
                        ReadOnly="true" HeaderText="Trade Type" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TradeType" UniqueName="TradeType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="95px" ReadOnly="true"
                        SortExpression="TradeDate" HeaderText="Trade Date" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="TradeDate" UniqueName="TradeDate" DataFormatString="{0:dd/MM/yyyy}"
                        DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="120px" SortExpression="EntryType" ReadOnly="true"
                        HeaderText="Entry Type" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="EntryType" UniqueName="EntryType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="65px" HeaderStyle-Width="100px" SortExpression="TransactionType"
                        ReadOnly="true" HeaderText="Transaction Type" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransactionType"
                        UniqueName="TransactionType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="65px" HeaderStyle-Width="100px" SortExpression="OrderBankAccountType"
                        ReadOnly="true" HeaderText="OrderBankAccountType" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="OrderBankAccountType" UniqueName="OrderBankAccountType" Display="False">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="65px" HeaderStyle-Width="100px" SortExpression="ClientManagementType"
                        ReadOnly="true" HeaderText="ClientManagementType" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="ClientManagementType" UniqueName="ClientManagementType" Display="False">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderStyle-Width="60px" UniqueName="Actions" HeaderText="Actions"
                        AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgAddLink" runat="server" ImageUrl="../images/link_add.png"
                                AlternateText="Add Confirmation" ToolTip="Add Confirmation" Visible="false" />
                            <asp:ImageButton ID="imgDeleteLink" runat="server" ImageUrl="../images/link_delete.png"
                                AlternateText="Delete Confirmation" ToolTip="Delete Confirmation" OnClientClick="javascript:return confirm('Are you sure you want to delete this confirmation?');"
                                CommandName="delete" CommandArgument='<%# Eval("ID") %>' Visible="false" />
                            <asp:CheckBox ID="cbMulti" runat="server" Visible='<%# Eval("OrderType").ToString()==OrderAccountType.ASX.ToString() && Eval("AccountType").ToString()==OrderAccountType.BankAccount.ToString() && Eval("Type").ToString()==OrderSetlledUnsettledType.Unsettled.ToString() %>'
                                OnCheckedChanged="cbMulti_OnCheckedChanged" AutoPostBack="true" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
            <Windows>
                <telerik:RadWindow ID="UserListDialog" runat="server" Title="Editing record" MinHeight="480px"
                    MinWidth="800px" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false"
                    VisibleStatusbar="false" Modal="true" AutoSize="true">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <asp:HiddenField ID="hfClientID" runat="server" />
        <asp:HiddenField ID="hfIsAdmin" runat="server" />
        <asp:HiddenField ID="hfIsAdminMenu" runat="server" />
        <asp:HiddenField ID="hfStatusFilter" runat="server" />
        <asp:HiddenField ID="hfPopupClientCID" runat="server" />
        <asp:HiddenField ID="hfPopupAccountType" runat="server" />
        <asp:HiddenField ID="hfPopupClientID" runat="server" />
        <asp:HiddenField ID="hfPopupFilter" runat="server" />
        <asp:HiddenField ID="hfOrderId" runat="server" />
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExcel" />
    </Triggers>
</asp:UpdatePanel>
