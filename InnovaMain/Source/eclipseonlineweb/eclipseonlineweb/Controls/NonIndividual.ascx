﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NonIndividual.ascx.cs"
    Inherits="eclipseonlineweb.Controls.NonIndividual" %>
<%@ Register TagPrefix="uc1" TagName="PhoneNumberControl" Src="~/Controls/PhoneNumberControl.ascx" %>
<%@ Register TagPrefix="uc2" TagName="MobileNumberControl" Src="~/Controls/MobileNumberControl.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ABN_InputControl" Src="~/Controls/ABN_InputControl.ascx" %>
<%@ Register TagPrefix="uc2" TagName="TFN_InputControl" Src="~/Controls/TFN_InputControl.ascx" %>
<%@ Register TagPrefix="uc2" TagName="AddressControl" Src="~/Controls/AddressControl.ascx" %>
<%@ Register TagPrefix="uc2" Namespace="eclipseonlineweb.Controls" Assembly="eclipseonlineweb" %>
<style type="text/css">
    .dropDownStyle
    {
        z-index: 100005;
    }
    
    .RadCalendar, .RadCalendarPopup, .RadCalendarPopupShadows
    {
        z-index: 100005;
    }
</style>
<asp:HiddenField ID="hfCID" runat="server" />
<asp:HiddenField ID="hfCLID" runat="server" />
<asp:HiddenField ID="hfCSID" runat="server" />
<asp:HiddenField ID="hfBussinessTitle" runat="server" />
<br />
<div>
    <fieldset style="width: 96%">
        <legend>Personal Information</legend>
        <table>
            <tr>
                <td style="width:150px">
                    <span class="riLabel">Type*</span>
                </td>
                <td>
                    <asp:RadioButtonList ID="rblType" runat="server" AutoPostBack="False" RepeatDirection="Horizontal" CssClass="riLabel">
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td style="width:150px">
                    <span class="riLabel">Name*</span>
                </td>
                <td style="width:250px">
                    <telerik:RadTextBox ID="txtgiventname" runat="server" CssClass="Txt-Field" ValidationGroup="VGIndividual"
                        Width="200px">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="RFVTxtGiveName" runat="server" ControlToValidate="txtgiventname"
                        ForeColor="Red" ErrorMessage="X" ValidationGroup="VGIndividual"></asp:RequiredFieldValidator>
                </td>
                <td style="width:150px">
                    <span class="riLabel">Email Address*</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtemailaddress" runat="server" Width="196px" CssClass="Txt-Field">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="RFVTxtEmail" runat="server" ControlToValidate="txtemailaddress"
                        ErrorMessage="X" ForeColor="Red" ValidationGroup="VGIndividual"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="*Invalid!"
                        ValidationExpression="^((?:(?:(?:[a-zA-Z0-9][\!\#\$\%\&\'\*\/\=\?\^\`\{\|\}\~\.\-\+_]?)*)[a-zA-Z0-9])+)\@((?:(?:(?:[a-zA-Z0-9][\!\#\$\%\&\'\*\/\=\?\^\`\{\|\}\~\.\-\+_]?){0,62})[a-zA-Z0-9])+)\.([a-zA-Z0-9]{2,6})$"
                        ControlToValidate="txtemailaddress" ForeColor="Red" Display="Dynamic" ValidationGroup="all">
                    </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Date of Creation*</span>
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtoCreation" runat="server" Width="200px" Calendar-RangeMinDate="1/01/1900 12:00:00 AM"
                        DateInput-MinDate="1/01/1900 12:00:00 AM" MinDate="1/01/1900 12:00:00 AM" ClientIDMode="AutoID">
                        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="RFVTxtdob" ControlToValidate="dtoCreation"
                        ErrorMessage="X" ForeColor="Red" ValidationGroup="VGIndividual"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <span class="riLabel">Mobile Phone &nbsp;&nbsp;&nbsp;</span>
                </td>
                <td>
                    <uc2:mobilenumbercontrol id="txtmobilephonenumber" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Office Phone &nbsp;&nbsp;&nbsp;</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="txtOfficePhone" runat="server" />
                </td>
                <td>
                    <span class="riLabel">Office Phone 2 &nbsp;&nbsp;&nbsp;</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="txtOfficePhone2" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Facsimile &nbsp;&nbsp;</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="txtfacsimile" runat="server" />
                </td>
                <td>
                    <span class="riLabel">Description</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtDescription" runat="server" CssClass="Txt-Field" Width="196px">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td style="width:150px">
                    <span class="riLabel">TFN</span>
                </td>
                <td style="width:400px">
                    <uc2:TFN_InputControl ID="TFN_InputControl1" runat="server" InvalidMessage="Invalid TFN (e.g. 123 456 782)" />
                </td>
            </tr>
            <tr>
                <td style="width:150px">
                    <span class="riLabel">ABN</span>
                </td>
                <td style="width:400px">
                    <uc2:ABN_InputControl ID="ABN_InputControl1" runat="server" InvalidMessage="Invalid ABN (e.g. 53 004 085 616)" />
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <fieldset style="width: 96%">
        <legend>Residential Address </legend>
        <uc2:addresscontrol runat="server" id="txtResidentialAddress" showfillascombo="False"
            title="Address" />
    </fieldset>
    <br />
    <div align="right">
        <telerik:RadButton ID="chkSameAddress" runat="server" Text="Mailing address same as above" OnCheckedChanged="chkSameAddress_OnCheckedChanged"
            ToggleType="CheckBox" ButtonType="ToggleButton">
        </telerik:RadButton>
        &nbsp;&nbsp;&nbsp;
    </div>
    <br />
    <fieldset style="width: 96%">
        <legend>Mailing Address </legend>
        <uc2:addresscontrol runat="server" id="txtMailingAddress" showfillascombo="False"
            title="Address" />
    </fieldset>
    <br />
    <div align="right">
        <telerik:RadButton ID="btnUpdate" runat="server" Text="Save" OnClick="btnUpdate_OnClick"
            ValidationGroup="VGIndividual">
        </telerik:RadButton>
        <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_OnClick">
        </telerik:RadButton>
        <br />
    </div>
</div>
