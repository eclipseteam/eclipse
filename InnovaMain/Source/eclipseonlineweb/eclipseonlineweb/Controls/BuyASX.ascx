﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BuyASX.ascx.cs" Inherits="eclipseonlineweb.Controls.BuyASX" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc2" TagName="SearchNSelectClientAccount" Src="~/Controls/SearchNSelectClientAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<style type="text/css">
    .rgCaption
    {
        color: blue;
        font: normal 9pt Arial;
        text-align: left !important;
    }
</style>
<script language="javascript" type="text/javascript">

    function ShowModalPopup() {
        window.$find("ModalBehaviour").show();
    }

    function HideModalPopup() {
        window.$find("ModalBehaviour").hide();
    }

    function ShowAlert(sender, args) {
        var isOK = true;
        var broker = document.getElementById("<%:hfIsAlert.ClientID %>").value;
        if (broker != "") {
            var x = confirm("This order may be delayed as we do not have a broker account opened yet.\nDo you want to place this order anyway?");
            if (x) {
                isOK = true;
            }
            else {
                isOK = false;
            }
        }
        sender.set_autoPostBack(isOK);
    }

    function CheckUnitsAmount(source, arguments) {
        var txtAmount = document.getElementById("<%:txtAmount.ClientID %>").value.trim();

        if (txtAmount == "$0.00" || txtAmount == "") {
            arguments.IsValid = false;
        }
        else {
            arguments.IsValid = true;
        }
    }

    function Validate(txtAmountID) {
        var txtAmount = document.getElementById(txtAmountID).value.trim();

        if (txtAmount == "0" || txtAmount == "") {
            alert('Please Enter Amount');
            return false;
        }
        else {
            return true;
        }
    } 
    
</script>
<asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
    <Triggers>
        <asp:PostBackTrigger ControlID="btnCutOff" />
    </Triggers>
    <ContentTemplate>
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="20%" runat="server" id="tdButtons">
                        <telerik:RadButton runat="server" ID="btnCutOff" ToolTip="Cut Of Time" OnClick="btnCutOff_OnClick">
                            <ContentTemplate>
                                <img alt="" src="../images/cut-off-time_106.png" class="btnImageWithText" />
                                <span class="riLabel">Cut-Off Times</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ID="btnSave" ToolTip="Place Order" OnClientClicked="ShowAlert"
                            OnClick="btnSave_OnClick">
                            <ContentTemplate>
                                <img src="../images/cart_add.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Place Order</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </td>
                    <td style="width: 80%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <telerik:RadButton ID="btnSearch" Visible="False" runat="server" Text="Select Client Account"
                            OnClick="btnSearch_OnClick">
                        </telerik:RadButton>
                        &nbsp;<uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <br />
        <div id="MainView" style="background-color: white">
            <div id="divTitleNote" style="color: blue; font-size: 8pt; margin-left:10px;" runat="server"></div>
            <table style="width: 100%; height: 50px" runat="server" id="showDetails" visible="false">
                <tr>
                    <td style="width: 120px">
                        Service Type:
                    </td>
                    <td style="width: 230px">
                        Cash Account:
                    </td>
                    <td style="width: 125px" runat="server" id="tdFundsHead">
                        Available Funds:
                    </td>
                    <td id="tdCashBalanceHead" runat="server" style="width: 160px">
                        = Current Cash Balance
                    </td>
                    <td id="tdMinCashHead" runat="server">
                        - Min Cash Holdings
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadComboBox ID="cmbServiceType" AutoPostBack="true" Width="110px" runat="server"
                            OnSelectedIndexChanged="cmbServiceType_OnSelectedIndexChanged">
                        </telerik:RadComboBox>
                    </td>
                    <td>
                        <telerik:RadComboBox ID="cmbCashAccount" AutoPostBack="true" Width="220px" runat="server"
                            OnSelectedIndexChanged="cmbCashAccount_OnSelectedIndexChanged">
                        </telerik:RadComboBox>
                    </td>
                    <td>
                        <asp:Label ID="lblAvailableFunds" runat="server" Text=""></asp:Label>
                    </td>
                    <td id="tdCashBalanceValue" runat="server">
                        <asp:Label ID="lblCashBalance" runat="server" Text=""></asp:Label>
                    </td>
                    <td id="tdMinCashValue" runat="server">
                        <asp:Label ID="lblMinCash" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <table width="100%">
                            <tr>
                                <td style="width: 350px">
                                    Security:
                                </td>
                                <td runat="server" id="tdHoldingLimitLabel" visible="False">
                                    Holding Limit:
                                </td>
                                <td runat="server" id="tdTotalHoldingLabel" visible="False">
                                    Current Holdings:
                                </td>
                                <td runat="server" id="tdAvailableFundsLabel" visible="False">
                                    Available:
                                </td>
                                <td>
                                    Currency Code:
                                </td>
                                <td>
                                    Latest Known Price:
                                </td>
                                <td colspan="2">
                                    Order Amount:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <telerik:RadComboBox ID="cmbSecurity" AutoPostBack="true" Width="340px" runat="server"
                                        Filter="Contains" IsEditable="True" AllowCustomText="True" EnableTextSelection="True"
                                        OnSelectedIndexChanged="cmbSecurity_OnSelectedIndexChanged">
                                    </telerik:RadComboBox>
                                </td>
                                <td runat="server" id="tdHoldingLimitValue" visible="False">
                                    <asp:Label ID="lblSMAHoldingLimit" runat="server" Text=""></asp:Label>
                                </td>
                                <td runat="server" id="tdTotalHoldingValue" visible="False">
                                    <asp:Label ID="lblSMATotalHolding" runat="server" Text=""></asp:Label>
                                </td>
                                <td runat="server" id="tdAvailableFundsValue" visible="False">
                                    <asp:Label ID="lblSMAAvailableHolding" runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCurrencyCode" runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblUnitPrice" runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ID="txtAmount" runat="server" Type="Currency">
                                    </telerik:RadNumericTextBox>
                                </td>
                                <td>
                                    <telerik:RadButton runat="server" ID="btnAdd" ValidationGroup="AddList" Text="Add To List"
                                        OnClick="btnAdd_OnClick">
                                    </telerik:RadButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Visible="false"></asp:Label><asp:CompareValidator
                            ID="validateSecurity" runat="server" ErrorMessage=" Please Select Security" ControlToValidate="cmbSecurity"
                            ValueToCompare="0" ValidationGroup="AddList" ForeColor="Red" Operator="GreaterThan"
                            Display="Dynamic"></asp:CompareValidator><asp:CustomValidator ID="validateUnitsAmount"
                                runat="server" ErrorMessage=" Please Enter Amount" ValidationGroup="AddList"
                                ForeColor="Red" ClientValidationFunction="CheckUnitsAmount" Display="Dynamic"></asp:CustomValidator>
                        <asp:CompareValidator ID="cvAmount" runat="server" ErrorMessage="Amount exceeds Available Limit"
                            ValidationGroup="AddList" ControlToValidate="txtAmount" Display="Dynamic" SetFocusOnError="true"
                            ForeColor="Red" Type="Double" Operator="LessThanEqual"></asp:CompareValidator>
                        <asp:CompareValidator ID="cvMinAmount" runat="server" ErrorMessage="Amount must be greater than equal to Unit Price"
                            ValidationGroup="AddList" ControlToValidate="txtAmount" Display="Dynamic" SetFocusOnError="true"
                            ForeColor="Red" Type="Double" Operator="GreaterThanEqual"></asp:CompareValidator>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <telerik:RadGrid OnNeedDataSource="PresentationGridNeedDataSource" ID="PresentationGrid"
            Visible="False" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
            PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
            GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="false"
            AllowAutomaticInserts="True" AllowAutomaticUpdates="True" EnableViewState="true"
            EditMode="EditForms" ShowFooter="True" OnItemDataBound="PresentationGrid_OnItemDataBound"
            OnItemCommand="PresentationGrid_OnItemCommand">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="None"
                Name="PendingOrders" TableLayout="Fixed">
                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                <Columns>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditColumn">
                        <HeaderStyle Width="25px"></HeaderStyle>
                        <ItemStyle CssClass="MyImageButton"></ItemStyle>
                    </telerik:GridEditCommandColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="ProductId" HeaderText="ProductId"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="ProductId" UniqueName="ProductId" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="AsxAccNo" HeaderText="AsxAccNo"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="AsxAccNo" UniqueName="AsxAccNo" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="AsxAccCid" HeaderText="AsxAccCid"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="AsxAccCid" UniqueName="AsxAccCid" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="SecurityId" HeaderText="SecurityId"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="SecurityId" UniqueName="SecurityId"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="80px" SortExpression="Security" ReadOnly="true"
                        HeaderText="Security" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Security" UniqueName="Security">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="280px" SortExpression="InvestmentName"
                        ReadOnly="true" HeaderText="Investment Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InvestmentName"
                        UniqueName="InvestmentName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="120px" SortExpression="HoldingLimit"
                        ReadOnly="true" HeaderText="Holding Limit" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="HoldingLimit"
                        UniqueName="HoldingLimit" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="120px" SortExpression="TotalHolding"
                        ReadOnly="true" HeaderText="Current Holdings" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TotalHolding"
                        UniqueName="TotalHolding" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="120px" SortExpression="AvailableFunds"
                        ReadOnly="true" HeaderText="Balance" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AvailableFunds"
                        UniqueName="AvailableFunds" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="100px" SortExpression="Currency" ReadOnly="true"
                        HeaderText="Currency Code" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Currency" UniqueName="Currency">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="120px" SortExpression="UnitPrice" ReadOnly="true"
                        HeaderText="Latest Known Price" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UnitPrice" UniqueName="UnitPrice"
                        DataFormatString="{0:N6}">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridNumericColumn HeaderStyle-Width="140px" SortExpression="OrderUnits"
                        ReadOnly="True" HeaderText="Suggested Order Units" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="OrderUnits" UniqueName="OrderUnits">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </telerik:GridNumericColumn>
                    <telerik:GridNumericColumn HeaderStyle-Width="160px" SortExpression="OrderAmount"
                        HeaderText="Suggested Order Amount" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="OrderAmount" UniqueName="OrderAmount"
                        Aggregate="Sum" DataFormatString="{0:C2}">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        <FooterStyle Font-Bold="True" HorizontalAlign="Right"></FooterStyle>
                    </telerik:GridNumericColumn>
                    <telerik:GridButtonColumn ConfirmText="Are you sure you want to delete it?" ButtonType="ImageButton"
                        CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                        <HeaderStyle Width="25px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <asp:HiddenField ID="hfIsAlert" runat="server" />
        <asp:HiddenField ID="hfClientCID" runat="server" />
        <asp:HiddenField ID="hfClientID" runat="server" />
        <asp:HiddenField ID="hfIsAdmin" runat="server" />
        <asp:HiddenField ID="hfIsAdminMenu" runat="server" />
        <asp:HiddenField ID="hfIsSMA" runat="server" />
        <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
        <asp:ModalPopupExtender ID="popupSearch" runat="server" TargetControlID="btnShowPopup"
            PopupControlID="pnlpopup" PopupDragHandleControlID="PopupHeader" Drag="true"
            BackgroundCssClass="ModalPopupBG" BehaviorID="ModalBehaviour">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlpopup" runat="server" BackColor="White" Style="display: none">
            <div class="popup_Container">
                <div class="popup_Titlebar" id="PopupHeader">
                    <div class="TitlebarLeft">
                        Select Account
                    </div>
                    <div class="TitlebarRight" onclick="HideModalPopup();">
                    </div>
                </div>
                <div class="PopupBody">
                    <uc2:SearchNSelectClientAccount ID="searchAccountControl" runat="server" />
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
