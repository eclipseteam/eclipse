﻿using System;
using System.Web.UI;

namespace eclipseonlineweb.Controls
{
    public partial class HINControl : UserControl
    {
        public bool Mandatory { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public bool Validate()
        {
            string HINCode = GetEntity();
            if (!Mandatory && HINCode.Length == 0)
            {
                return true;
            }
            return true;
        }


        public String GetEntity()
        {
            return TextAPIR1.Text.Trim() + TextAPIR2.Text.Trim() + TextAPIR3.Text.Trim() + TextAPIR4.Text.Trim() + TextAPIR5.Text.Trim() + TextAPIR6.Text.Trim() + TextAPIR7.Text.Trim() + TextAPIR8.Text.Trim() + TextAPIR9.Text.Trim() + TextAPIR10.Text.Trim() + TextAPIR11.Text.Trim();
        }

        public void SetEntity(String value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                #region Value1
                if (value.Length >= 1)
                    TextAPIR1.Text = Convert.ToString(value[0]);
                #endregion

                #region Value2
                if (value.Length >= 2)
                    TextAPIR2.Text = Convert.ToString(value[1]);
                #endregion

                #region Value3
                if (value.Length >= 3)
                    TextAPIR3.Text = Convert.ToString(value[2]);
                #endregion

                #region Value4
                if (value.Length >= 4)
                    TextAPIR4.Text = Convert.ToString(value[3]);
                #endregion

                #region Value5
                if (value.Length >= 5)
                    TextAPIR5.Text = Convert.ToString(value[4]);
                #endregion

                #region Value6
                if (value.Length >= 6)
                    TextAPIR6.Text = Convert.ToString(value[5]);
                #endregion

                #region Value7
                if (value.Length >= 7)
                    TextAPIR7.Text = Convert.ToString(value[6]);
                #endregion

                #region Value8
                if (value.Length >= 8)
                    TextAPIR8.Text = Convert.ToString(value[7]);
                #endregion

                #region Value9
                if (value.Length >= 9)
                    TextAPIR9.Text = Convert.ToString(value[8]);
                #endregion

                #region Value10
                if (value.Length >= 10)
                    TextAPIR10.Text = Convert.ToString(value[9]);
                #endregion

                #region Value11
                if (value.Length >= 11)
                    TextAPIR11.Text = Convert.ToString(value[10]);
                #endregion
            }
            else
            {
                TextAPIR1.Text = "X";
                TextAPIR2.Text = string.Empty;
                TextAPIR3.Text = string.Empty;
                TextAPIR4.Text = string.Empty;
                TextAPIR5.Text = string.Empty;
                TextAPIR6.Text = string.Empty;
                TextAPIR7.Text = string.Empty;
                TextAPIR8.Text = string.Empty;
                TextAPIR9.Text = string.Empty;
                TextAPIR10.Text = string.Empty;
                TextAPIR11.Text = string.Empty;
            }
        }

        public void ClearEntity()
        {
            TextAPIR1.Text = "X";
            TextAPIR2.Text = string.Empty;
            TextAPIR3.Text = string.Empty;
            TextAPIR4.Text = string.Empty;
            TextAPIR5.Text = string.Empty;
            TextAPIR6.Text = string.Empty;
            TextAPIR7.Text = string.Empty;
            TextAPIR8.Text = string.Empty;
            TextAPIR9.Text = string.Empty;
            TextAPIR10.Text = string.Empty;
            TextAPIR11.Text = string.Empty;
        }
        
    }
}