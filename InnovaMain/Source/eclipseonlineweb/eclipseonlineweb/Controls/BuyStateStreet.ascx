﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BuyStateStreet.ascx.cs"
    Inherits="eclipseonlineweb.Controls.BuyStateStreet" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="SearchNSelectClientAccount.ascx" TagName="SearchNSelectClientAccount"
    TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<style>
    .rgCaption
    {
        color: Blue;
        font: bold 10pt Arial Narrow;
    }
</style>
<script language="javascript" type="text/javascript">

    function ShowModalPopup() {
        window.$find("ModalBehaviour").show();
    }

    function HideModalPopup() {
        window.$find("ModalBehaviour").hide();
    }

    function ShowAlert() {
        var isOK = true;
        if (document.getElementById("<%:hfIsAlert.ClientID %>").value.toLowerCase() == "true") {
            var x = confirm("This order may be delayed as we do not have an Innova Investment Account opened yet.\nDo you want to place this order anyway?");
            if (x) {
                isOK = true;
                document.getElementById("<%:hfIsAlert.ClientID %>").value = "pass";
            }
            else {
                isOK = false;
                document.getElementById("<%:hfIsAlert.ClientID %>").value = "false";
            }
        }
        if (isOK) {
            window.__doPostBack("<%:btnSave.ClientID %>", '');
        }
        else {
            return false;
        }
        return true;
    }
</script>
<asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
    <Triggers>
        <asp:PostBackTrigger ControlID="btnCutOff" />
    </Triggers>
    <ContentTemplate>
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="20%" runat="server" id="tdButtons">
                        <telerik:RadButton runat="server" ID="btnCutOff" ToolTip="Cut Of Time" OnClick="btnCutOff_OnClick">
                            <ContentTemplate>
                                <img alt="" src="../images/cut-off-time_106.png" class="btnImageWithText" />
                                <span class="riLabel">Cut-Off Times</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ValidationGroup="btnSave" ID="btnSave" ToolTip="Place Order"
                            OnClick="btnSave_OnClick">
                            <ContentTemplate>
                                <img src="../images/cart_add.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Place Order</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </td>
                    <td style="width: 80%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <telerik:RadButton ID="btnSearch" runat="server" Visible="False" Text="Select Client Account"
                            OnClick="btnSearch_OnClick">
                        </telerik:RadButton>
                        &nbsp;<uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <br />
        <asp:Label ID="lblNotify" runat="server" ForeColor="Blue" Visible="false"></asp:Label>
        <asp:HiddenField ID="hfIsAlert" runat="server" />
        <div id="MainView" style="background-color: white">
            <span id="spanDIYService" runat="server" visible="false" style="padding-left: 2px;
                font: bold 10pt Arial Narrow; color: Blue">Managed Investments are not available
                in DIY service</span>
            <table style="width: 100%; height: 50px" runat="server" id="showDetails" visible="false">
                <tr>
                    <td style="width: 120px">
                        Service Type:
                    </td>
                    <td style="width: 230px">
                        Cash Account:
                    </td>
                    <td style="width: 125px" runat="server" id="tdFundsHead">
                        Available Funds:
                    </td>
                    <td id="tdCashBalanceHead" runat="server" style="width: 160px">
                        = Current Cash Balance
                    </td>
                    <td id="tdMinCashHead" runat="server">
                        - Min Cash Holdings
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadComboBox ID="cmbServiceType" AutoPostBack="true" Width="110px" runat="server"
                            OnSelectedIndexChanged="cmbServiceType_OnSelectedIndexChanged">
                        </telerik:RadComboBox>
                    </td>
                    <td>
                        <telerik:RadComboBox ID="cmbCashAccount" AutoPostBack="true" Width="220px" runat="server"
                            OnSelectedIndexChanged="cmbCashAccount_OnSelectedIndexChanged">
                        </telerik:RadComboBox>
                    </td>
                    <td>
                        <asp:Label ID="lblAvailableFunds" runat="server" Text=""></asp:Label>
                    </td>
                    <td id="tdCashBalanceValue" runat="server">
                        <asp:Label ID="lblCashBalance" runat="server" Text=""></asp:Label>
                    </td>
                    <td id="tdMinCashValue" runat="server">
                        <asp:Label ID="lblMinCash" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <telerik:RadGrid OnNeedDataSource="PresentationGridNeedDataSource" ID="PresentationGrid"
            Visible="False" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
            PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
            GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
            AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnItemDataBound="PresentationGridItemDataBound">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                Name="PendingOrders" TableLayout="Fixed" Caption="Please note that Innova Investments are only available for Wholesale and Sophisticated Investors.">
                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                <Columns>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="ProductID" HeaderText="Product ID"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="ProductID" UniqueName="ProductID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="140px" SortExpression="InvestmentCode"
                        ReadOnly="true" HeaderText="Fund Code" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InvestmentCode"
                        UniqueName="FundCode">
                    </telerik:GridBoundColumn>
                    <telerik:GridHyperLinkColumn FilterControlWidth="250px" HeaderStyle-Width="300px"
                        SortExpression="InvestmentName" HeaderText="Fund Name" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataTextField="InvestmentName" UniqueName="FundName">
                    </telerik:GridHyperLinkColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="HoldingLimit"
                        ReadOnly="true" HeaderText="Holding Limit" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="HoldingLimit"
                        UniqueName="HoldingLimit" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="TotalHolding"
                        ReadOnly="true" HeaderText="Current Holdings" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TotalHolding"
                        UniqueName="TotalHolding" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="AvailableFunds"
                        ReadOnly="true" HeaderText="Balance" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AvailableFunds"
                        UniqueName="AvailableFunds" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn UniqueName="Amount" HeaderText="Amount" AllowFiltering="False">
                        <ItemTemplate>
                            $&nbsp;<telerik:RadNumericTextBox ID="txtAmount" runat="server">
                            </telerik:RadNumericTextBox><asp:CompareValidator ID="cvAmount" runat="server" ErrorMessage="*Invalid Amount"
                                ControlToValidate="txtAmount" Display="Dynamic" SetFocusOnError="true" ForeColor="Red"
                                Type="Double" Operator="LessThanEqual" ValidationGroup="btnSave" Enabled="False">
                            </asp:CompareValidator>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <asp:HiddenField ID="hfClientCID" runat="server" />
        <asp:HiddenField ID="hfClientID" runat="server" />
        <asp:HiddenField ID="hfIsAdmin" runat="server" />
        <asp:HiddenField ID="hfIsAdminMenu" runat="server" />
        <asp:HiddenField ID="hfIsSMA" runat="server" />
        <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
        <asp:ModalPopupExtender ID="popupSearch" runat="server" TargetControlID="btnShowPopup"
            PopupControlID="pnlpopup" PopupDragHandleControlID="PopupHeader" Drag="true"
            BackgroundCssClass="ModalPopupBG" BehaviorID="ModalBehaviour">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlpopup" runat="server" BackColor="White" Style="display: none">
            <div class="popup_Container">
                <div class="popup_Titlebar" id="PopupHeader">
                    <div class="TitlebarLeft">
                        Select Account
                    </div>
                    <div class="TitlebarRight" onclick="HideModalPopup();">
                    </div>
                </div>
                <div class="PopupBody">
                    <uc2:SearchNSelectClientAccount ID="searchAccountControl" runat="server" />
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
