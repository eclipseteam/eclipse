﻿using Oritax.TaxSimp.Extensions;

namespace eclipseonlineweb.Controls
{
    public partial class MobileNumberControl : System.Web.UI.UserControl 
    {
        public bool Mandatory { get; set; }
        public string CountryCode
        {
            get { return TxtCountryCode.Text; }
            set { TxtCountryCode.Text = (value == null) ? string.Empty : value.Replace(" ", ""); }
        }
        public string Number
        {
            get { return TxtBoxMobileNumber.Text; }
            set { TxtBoxMobileNumber.Text = (value == null) ? string.Empty : value.Replace(" ", ""); }
        }
        public void ClearEntity()
        {
            CountryCode = string.Empty;
            Number = string.Empty;
            ClearErrors();
        }

       
        public void ClearErrors()
        {
            Msg.Text = string.Empty;
        }
        public bool Validate()
        {
            if (!Mandatory && TxtCountryCode.Text.Length == 0 && TxtBoxMobileNumber.Text.Length == 0)
            {
                ClearErrors();
                return true;
            }

            if (TxtCountryCode.ValidateNumeric(1, 3)  && TxtBoxMobileNumber.ValidateNumeric(1, 20))
            {
                ClearErrors();
                return true;
            }
            Msg.Text = "Invalid Number 99 99999";
            return false;
        }

    }
}