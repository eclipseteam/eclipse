﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="P2Reports.ascx.cs" Inherits="eclipseonlineweb.Controls.P2Reports" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc2" TagName="SearchNSelectClientAccount" Src="~/Controls/SearchNSelectClientAccount.ascx" %>
<script language="javascript" type="text/javascript">

    var isDuplicateFile = false;
    var uploadedFiles = 0;

    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(function (s, e) {
        uploadedFiles = 0;
    });

    function validateUpload(sender, args) {
        args.IsValid = uploadedFiles != 0;
    }

    function ShowModalPopup() {
        window.$find("ModalBehaviour").show();
    }

    function HideModalPopup() {
        window.$find("ModalBehaviour").hide();
    }

    function OnFileUploaded(sender, args) {
        uploadedFiles++;
        var numberOfFiles = sender._uploadedFiles.length;

        if (isDuplicateFile) {
            sender.deleteFileInputAt(numberOfFiles - 1);
            isDuplicateFile = false;
            sender.updateClientState();
            alert("You can't add the same file twice");
            return;
        }
    }

    function OnFileUploadRemoved(sender, args) {
        uploadedFiles--;
    }

    function OnFileSelected(sender, args) {
        for (var fileindex in sender._uploadedFiles) {
            if (sender._uploadedFiles[fileindex].fileInfo.FileName == args.get_fileName()) {
                isDuplicateFile = true;
            }
        }
    }
    


</script>
<style type="text/css">
    .RadUpload .ruFakeInput
    {
        display: none;
    }
</style>
<asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <asp:Panel ID="pnlSelectClient" runat="server">
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 80%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <telerik:RadButton runat="server" ID="btnSearch" Visible="False" OnClick="btnSearch_OnClick" Text="Select Client Account">
                            </telerik:RadButton>
                            &nbsp;<uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" Visible="False" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </asp:Panel>
        <asp:HiddenField ID="hfClientCID" runat="server" />
        <asp:HiddenField ID="hfClientID" runat="server" />
        <asp:HiddenField ID="hfIsAdmin" runat="server" />
        <asp:HiddenField ID="hfIsAdminMenu" runat="server" />
        <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
        <asp:ModalPopupExtender ID="popupSearch" runat="server" TargetControlID="btnShowPopup"
            PopupControlID="pnlpopup" PopupDragHandleControlID="PopupHeader" Drag="true"
            BackgroundCssClass="ModalPopupBG" BehaviorID="ModalBehaviour">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlpopup" runat="server" BackColor="White" Style="display: none">
            <div class="popup_Container">
                <div class="popup_Titlebar" id="PopupHeader">
                    <div class="TitlebarLeft">
                        Select Account
                    </div>
                    <div class="TitlebarRight" onclick="HideModalPopup();">
                    </div>
                </div>
                <div class="PopupBody">
                    <uc2:SearchNSelectClientAccount ID="searchAccountControl" runat="server" />
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlUploadReport" runat="server" Visible="False">
            <br />
            <fieldset>
                <legend>P2 Report(s) Upload Form</legend>
                <table>
                    <tr>
                        <td style="font-size: smaller">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Select File(s):
                        </td>
                        <td style="width: 20%;">
                            Type
                        </td>
                        <td style="font-size: smaller;">
                            &nbsp;
                        </td>
                        <td style="width: 20%; padding-left: 10px;">
                            Quarter
                        </td>
                        <td style="font-size: smaller">
                            &nbsp;
                        </td>
                        <td style="width: 20%;">
                            Year
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: smaller; vertical-align: top;">
                            <telerik:RadAsyncUpload ID="AsyncUpload" runat="server" MultipleFileSelection="Automatic"
                                OnClientFileSelected="OnFileSelected" OnClientFileUploaded="OnFileUploaded" OnClientFileUploadRemoved="OnFileUploadRemoved">
                                <Localization Select="Browse" />
                            </telerik:RadAsyncUpload>
                            <asp:CustomValidator runat="server" ID="CustomValidator" ClientValidationFunction="validateUpload"
                                ErrorMessage="Select a report first" Display="Dynamic" ValidationGroup="vgUploadPanel"
                                ForeColor="Red">
                            </asp:CustomValidator>
                        </td>
                        <td style="width: 20%; font-size: smaller;" valign="top">
                            <asp:RadioButtonList RepeatDirection="Vertical" runat="server" ID="rbType">
                                <asp:ListItem Text="INV0004" Value="INV0004" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="INV0014" Value="INV0014"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td style="font-size: smaller;">
                            &nbsp;
                        </td>
                        <td valign="top" style="width: 20%; padding-left: 10px; font-size: smaller;">
                            <asp:RadioButtonList RepeatDirection="Vertical" runat="server" ID="rbQuarter">
                                <asp:ListItem Text="Q1" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Q2" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Q3" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Q4" Value="4"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td style="font-size: smaller">
                            &nbsp;
                        </td>
                        <td valign="top" style="font-size: smaller;">
                            <telerik:RadNumericTextBox ID="txtYear" runat="server" ShowSpinButtons="True" Width="100px">
                                <NumberFormat DecimalDigits="0" GroupSeparator="" />
                            </telerik:RadNumericTextBox>
                            <br />
                            <asp:RequiredFieldValidator ID="rfvYear" runat="server" ControlToValidate="txtYear"
                                Display="Dynamic" ErrorMessage="No year selected" ValidationGroup="vgUploadPanel"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadButton runat="server" ID="RadButton1" Text="Upload" OnClick="Upload_Click"
                                ValidationGroup="vgUploadPanel">
                            </telerik:RadButton>
                            <asp:Label ID="lblErrorMessage" ForeColor="Red" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadProgressArea runat="server" ID="RadProgressArea2" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="pnlReportGrid" runat="server" Visible="False">
            <br />
            <fieldset>
                <legend>P2 Report(s)</legend>
                <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
                    runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="50"
                    AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true" GridLines="None"
                    AllowFilteringByColumn="true" EnableViewState="true" ShowFooter="true" OnItemCommand="PresentationGrid_ItemCommand">
                    <PagerStyle Mode="NumericPages" AlwaysVisible="true"></PagerStyle>
                    <MasterTableView Width="100%" DataKeyNames="ID,BMCID">
                        <Columns>
                            <telerik:GridTemplateColumn ReadOnly="true" HeaderText="File" UniqueName="ORIGINPATH"
                                ItemStyle-Width="60%">
                                <ItemTemplate>
                                    <a id="hlDowload" runat="server" href='<%# String.Format("../Reports/DownloadP2Reports.aspx?bmcid={0}", DataBinder.Eval(Container.DataItem,"BMCID"))%>'
                                        target="_blank">
                                        <%# DataBinder.Eval(Container.DataItem, "ORIGINPATH")%>
                                    </a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn ReadOnly="true" HeaderText="Type" DataField="P2Type" ItemStyle-Width="200px"
                                UniqueName="P2Type" />
                            <telerik:GridBoundColumn ReadOnly="true" HeaderText="QTR" DataField="Quarter" ItemStyle-Width="200px"
                                UniqueName="Quarter" />
                            <telerik:GridBoundColumn ReadOnly="true" HeaderText="Year" DataField="Year" ItemStyle-Width="200px"
                                UniqueName="Year" />
                            <telerik:GridButtonColumn UniqueName="DeleteButton" Text="Delete" CommandName="Delete"
                                ButtonType="ImageButton" ConfirmText="Are you sure you want to delete the report?">
                                <HeaderStyle Width="5px"></HeaderStyle>
                            </telerik:GridButtonColumn>
                        </Columns>
                        <PagerStyle AlwaysVisible="True"></PagerStyle>
                    </MasterTableView>
                </telerik:RadGrid>
            </fieldset>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
