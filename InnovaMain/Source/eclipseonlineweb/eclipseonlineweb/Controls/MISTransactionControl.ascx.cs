﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.C1ComboBox;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Commands;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb.Controls
{
    public partial class MISTransactionControl : System.Web.UI.UserControl
    {
        public Action<Guid, Guid, string> Saved
        {
            get;
            set;
        }

        public Action<Guid, DataSet> SaveData
        {
            get;
            set;
        }
        public Action Canceled { get; set; }
        public Action ValidationFailed { get; set; }
        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadControls();
            }
        }

        private void LoadControls()
        {
            //bind combos here
            var org = this.Broker.GetWellKnownBMC(WellKnownCM.Organization);

            MISBasicDS MIS = new MISBasicDS();
            MIS.Command = (int)WebCommands.GetOrganizationUnitsByType;
            MIS.Unit = new Oritax.TaxSimp.Data.OrganizationUnit
            {
                CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                Type = ((int)OrganizationType.ManagedInvestmentSchemesAccount).ToString()
            };
            org.GetData(MIS);
            cmbMISAcc.Items.Clear();
            cmbMISAcc.DataSource = MIS.MISBasicTable;
            cmbMISAcc.DataTextField = MIS.MISBasicTable.NAME;
            cmbMISAcc.DataValueField = MIS.MISBasicTable.CID;

            cmbMISAcc.DataBind();

            if (cmbMISAcc.Items.Count > 0)
            {

                cmbMISAcc.SelectedIndex = 0;
                FillFundCombo(new Guid(cmbMISAcc.SelectedValue.ToString()));

            }

            Broker.ReleaseBrokerManagedComponent(org);

        }
        private MISBasicDS GetMIsAccount(Guid MISCid)
        {

            MISBasicDS misBasicDs = new MISBasicDS { CommandType = DatasetCommandTypes.Get, Command = (int)WebCommands.GetOrganizationUnitDetails };
            misBasicDs.Unit = new OrganizationUnit { Cid = MISCid, CurrentUser = (Page as UMABasePage).GetCurrentUser(), Type = ((int)OrganizationType.ManagedInvestmentSchemesAccount).ToString() };
            if (Broker != null)
            {
                IBrokerManagedComponent org = Broker.GetWellKnownBMC(WellKnownCM.Organization);
                org.GetData(misBasicDs);
                Broker.ReleaseBrokerManagedComponent(org);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }


            return misBasicDs;

        }

        private ManagedInvestmentAccountDS GetFundDataSet(Guid MISCid)
        {
            ManagedInvestmentAccountDS misDS = new ManagedInvestmentAccountDS();
            misDS.FundID = Guid.Empty;
            if (Broker != null)
            {

                IBrokerManagedComponent clientData = this.Broker.GetBMCInstance(MISCid);

                misDS.CommandType = DatasetCommandTypes.Get;
                clientData.GetData(misDS);
                Broker.ReleaseBrokerManagedComponent(clientData);


            }
            else
            {
                throw new Exception("Broker Not Found");
            }



            return misDS;

        }


        private void FillFundCombo(Guid MISCid)
        {
            ManagedInvestmentAccountDS ds = GetFundDataSet(MISCid);
            FundAccountTable funds = ds.FundAccountsTable;
            cmbFundCode.Items.Clear();
            cmbFundCode.DataSource = funds;
            cmbFundCode.DataTextField = funds.FUNDCODE;
            cmbFundCode.DataValueField = funds.FUNDCODE;
            cmbFundCode.DataBind();
        }

        public void SetEntity(DataRow row, string clientID)
        {
            LoadControls();
            ClearEntity();

            if (row != null)
            {
                cmbMISAcc.SelectedValue = row["MISCID"].ToString();
                FillFundCombo(Guid.Parse(cmbMISAcc.SelectedValue));
                cmbFundCode.SelectedValue = row["CODE"].ToString();
                cmbFundCode.Text = row["CODE"].ToString();
                cmbMISAcc.Enabled = false;
                cmbFundCode.Enabled = false;


                transactionDate.Date = (DateTime)row["TradeDate"];
                
                foreach (var item in C1ComboBoxTrasactionType.Items)
                {
                    if(item.Text == row["TransactionType"].ToString())
                    {
                        item.Selected = true;
                    }
                    else
                    {
                        item.Selected = false;
                    }
                }
                lblTranID.Value = row["ID"].ToString();
                lblmisCid.Text = row["MISCID"].ToString();
                UnitPrice.Value = Convert.ToDouble(row["UnitPrice"]);
                TotalShares.Value = Convert.ToDouble(row["Shares"]);
                TotalAmount.Value = Convert.ToDouble(row["Amount"]);
                IsUpdate.Value = "true";
                //btnAdd.Visible = false;

            }
            else//add Clicked 
            {
                hfClientID.Value = clientID;
                IsUpdate.Value = "false";
                cmbMISAcc.Enabled = true;
                cmbFundCode.Enabled = true;
                transactionDate.Enabled = true;
                lblTranID.Value = (Guid.NewGuid()).ToString();
            }


        }

        protected void Mis_Changed(object sender, C1ComboBoxEventArgs e)
        {
            cmbMISAcc.SelectedIndex = e.NewSelectedIndex;
            FillFundCombo(Guid.Parse(cmbMISAcc.SelectedValue.ToString()));
            if (ValidationFailed != null)
                ValidationFailed();


        }
        protected void UpdateTransaction(object sender, EventArgs e)
        {
            if (Validate() == false)
            {
                if (ValidationFailed != null)
                    ValidationFailed();
                return;
            }

            var cid = UpdateData();


            if (Saved != null)
            {
                Saved(new Guid(cmbMISAcc.SelectedValue), new Guid(lblTranID.Value), cmbFundCode.SelectedValue);
            }

        }

        private Guid UpdateData()
        {
            var transactionDetailsDS = new MISTransactionDetailsDS();
            DataRow row = transactionDetailsDS.Tables[MISTransactionDetailsDS.MISTRANSDETAILSTABLE].NewRow();
            row[MISTransactionDetailsDS.ID] = lblTranID.Value;
            row[MISTransactionDetailsDS.SYSTRANTYPE] = C1ComboBoxTrasactionType.Items[C1ComboBoxTrasactionType.SelectedIndex].Text;
            row[MISTransactionDetailsDS.AMOUNT] = TotalAmount.Value;
            row[MISTransactionDetailsDS.UNITPRICE] = UnitPrice.Value;
            row[MISTransactionDetailsDS.SHARES] = TotalShares.Value;
            row[MISTransactionDetailsDS.TRADEDATE] = transactionDate.Date;
            row[MISTransactionDetailsDS.FUNDCODE] = cmbFundCode.SelectedValue;
            row[MISTransactionDetailsDS.CLIENTID] = hfClientID.Value;
            transactionDetailsDS.Tables[MISTransactionDetailsDS.MISTRANSDETAILSTABLE].Rows.Add(row);

            if (IsUpdate.Value == "true")
                transactionDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
            else
            {
                transactionDetailsDS.DataSetOperationType = DataSetOperationType.NewSingle;
            }


            if (SaveData != null)
            {
                SaveData(new Guid(cmbMISAcc.SelectedValue), transactionDetailsDS);
            }

            return Guid.Empty;
        }

        private bool Validate()
        {
            bool result;
            var messages = new StringBuilder();
            bool tempResult = cmbMISAcc.Items[cmbMISAcc.SelectedIndex].Text == cmbMISAcc.Text;
            result = tempResult;
            AddMessageIn(tempResult, "Select Existing MIS Account *", messages);

            tempResult = cmbFundCode.Items[cmbFundCode.SelectedIndex].Text == cmbFundCode.Text;
            result = tempResult && result;
            AddMessageIn(tempResult, "Select Existing Fund Code *", messages);

            tempResult = C1ComboBoxTrasactionType.Items[C1ComboBoxTrasactionType.SelectedIndex].Text == C1ComboBoxTrasactionType.Text;
            result = tempResult && result;
            AddMessageIn(tempResult, "Select Existing Transaction type", messages);


            lblMessages.Text = messages.ToString();


            return result;
        }

        private void AddMessageIn(bool result, string message, StringBuilder messages)
        {
            if (!result)
            {
                messages.Append(message + "<br/>");
            }
        }

        public void ClearEntity()
        {
            lblMessages.Text = string.Empty;
            C1ComboBoxTrasactionType.SelectedIndex = 0;
            C1ComboBoxTrasactionType.Text = C1ComboBoxTrasactionType.Items[C1ComboBoxTrasactionType.SelectedIndex].Text;
            lblTranID.Value = string.Empty;
            lblmisCid.Text = string.Empty;

            if (cmbMISAcc.Items.Count > 0 && cmbMISAcc.SelectedIndex != -1)
            {
                cmbMISAcc.Text = cmbMISAcc.Items[cmbMISAcc.SelectedIndex].Text;
            }

            if (cmbFundCode.Items.Count > 0)
            {
                cmbFundCode.SelectedIndex = 0;
                cmbFundCode.Text = cmbFundCode.Items[cmbFundCode.SelectedIndex].Text;
            }

            TotalAmount.Value = 0;
            UnitPrice.Value = 0;
            TotalShares.Value = 0;
            transactionDate.Date = DateTime.Now.Date;
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (Canceled != null)
            {
                Canceled();
            }
        }
    }
}