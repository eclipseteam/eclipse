﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IndvidualClientsMappingControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.IndvidualClientsMappingControl" %>
<%@ Register Src="../Controls/OrganizationMembership.ascx" TagName="MembershipControl"
    TagPrefix="uc3" %>
<script type="text/javascript">
    function cancel() {
        window.close();
    }
</script>
<style type="text/css">
    .holder
    {
        width: 100%;
        display: block;
        z-index: 6;
    }
    .content
    {
        background: #fff;
        z-index: 7; /*  padding: 28px 26px 33px 25px;*/
    }
    .popup
    {
        border-radius: 7px;
        background: #6b6a63;
        margin: 30px auto 0;
        padding: 6px;
        position: absolute;
        width: 1000px;
        top: 20%;
        left: 50%;
        margin-left: -400px;
        margin-top: -40px;
        z-index: 6;
    }
    
    .popup1
    {
        border-radius: 7px;
        background: #6b6a63;
        margin: 30px auto 0;
        padding: 6px;
        position: absolute;
        width: 770px;
        top: 20%;
        left: 50%;
        margin-left: -400px;
        margin-top: -40px;
        z-index: 6;
    }
    .overlay
    {
        width: 100%;
        opacity: 0.65;
        height: 100%;
        left: 0; /*IE*/
        top: 0;
        text-align: center;
        z-index: 5;
        position: fixed;
        background-color: #444444;
    }
</style>
<fieldset>
    <legend>Individual Clients</legend>
    <table style="width: 100%">
        <tr>
            <td>
                <telerik:RadGrid ID="gd_ClientControl" runat="server" AutoGenerateColumns="True"
                    PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true"
                    GridLines="None" AllowAutomaticInserts="false" AllowFilteringByColumn="true"
                    EnableViewState="true" ShowFooter="false" OnNeedDataSource="gd_ClientControl_OnNeedDataSource"
                    OnItemCommand="gd_ClientControl_ItemCommand"  OnItemDataBound="PresentationGrid_OnItemDataBound">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="Top" Width="100%">
                        <CommandItemTemplate>
                            <div style="padding: 5px 5px;">
                                <asp:LinkButton ID="btnExisting" runat="server" OnClick="LnkbtnAddExistingClientClick"><img style="border:0;vertical-align:middle;Width:22px;Height:22px;" alt="" src="../images/add_existing.png"/>Add Existing Client</asp:LinkButton>
                            </div>
                        </CommandItemTemplate>
                        <Columns>
                            <telerik:GridEditCommandColumn EditText="Edit" Visible="false" />
                            <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="Cid" HeaderStyle-Width="5%"
                                ItemStyle-Width="5%" HeaderText="CID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Cid" UniqueName="Cid"
                                Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="Clid" HeaderStyle-Width="5%"
                                ItemStyle-Width="5%" HeaderText="CLID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Clid" UniqueName="Clid"
                                Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="Csid" HeaderStyle-Width="5%"
                                ItemStyle-Width="5%" HeaderText="CSID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Csid" UniqueName="Csid"
                                Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridHyperLinkColumn SortExpression="ClientID" DataNavigateUrlFields="Cid"
                                UniqueName="ClientID" HeaderText="Client ID" DataTextField="ClientID" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" FilterControlWidth="120px" HeaderStyle-Width="5%"
                                HeaderButtonType="TextButton" DataNavigateUrlFormatString="~/ClientViews/ClientMainView.aspx?ins={0}"
                                ShowFilterIcon="true">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridBoundColumn FilterControlWidth="400px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                                SortExpression="Name" HeaderText="Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Name" UniqueName="Name">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="200px" HeaderStyle-Width="8%" ItemStyle-Width="8%"
                                SortExpression="Type" HeaderText="Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="false" DataField="Type" UniqueName="Type">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="IndiShare" HeaderText="Share" FilterControlWidth="200px"
                                DataField="IndiShare" SortExpression="IndiShare">
                                <ItemTemplate>
                                    <telerik:RadTextBox ID="txtShare" runat="server" OnTextChanged="txtShare_OnTextChanged"
                                        AutoPostBack="true">
                                    </telerik:RadTextBox>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridButtonColumn CommandName="Delete" Text="Remove" ButtonType="LinkButton"
                                ConfirmText="Are you sure you want to remove this Client?" HeaderStyle-Width="10%"
                                ItemStyle-Width="10%">
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
</fieldset>
<div class="overlay" id="OVER" visible="False" runat="server">
</div>
<div id="clientSearchPopup" runat="server" visible="false" class="holder">
    <div class="popup">
        <div class="content">
            <uc3:MembershipControl ID="ClientSearchControl" runat="server" OrgType="Adviser" />
        </div>
    </div>
</div>
<asp:HiddenField ID="hidOrgType" runat="server" />
