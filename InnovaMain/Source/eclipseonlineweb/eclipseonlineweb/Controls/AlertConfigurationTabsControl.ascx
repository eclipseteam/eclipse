﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AlertConfigurationTabsControl.ascx.cs" Inherits="eclipseonlineweb.Controls.AlertConfigurationTabsControl" %>

<%@ Register TagName="AdminAlertConfiguration" TagPrefix="uc" Src="../JDash/Dashlets/AdminAlertConfiguration.ascx" %>
<%@ Register TagName="AdviserAlertConfiguration" TagPrefix="uc" Src="../JDash/Dashlets/AdviserAlertConfiguration.ascx" %>
<%@ Register TagName="ClientAlertConfiguration" TagPrefix="uc" Src="../JDash/Dashlets/ClientAlertConfiguration.ascx" %>
<script type="text/javascript">
    var activeTab;
    var activeLi;    
    $(document).ready(function () {
        $(".tab_content").hide();
        $(".tab_content:first").show();
        $("ul.tabs li").click(function () {
            $("ul.tabs li").removeClass("active");
            $(this).addClass("active");
            $(".tab_content").hide();
            activeTab = $(this).attr("rel");
            activeLi = $(this).attr("id");
            $("#" + activeTab).fadeIn();
        });       
    });

    $(function () {
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {

            //Keep activate selected tab
            $("ul.tabs li").removeClass("active");
            $("#" + activeTab).show();
            $("#" + activeLi).addClass("active");

            //On li click after post back.
            $("ul.tabs li").click(function () {
                $("ul.tabs li").removeClass("active");
                $(this).addClass("active");
                $(".tab_content").hide();
                activeTab = $(this).attr("rel");
                activeLi = $(this).attr("id");
                $("#" + activeTab).fadeIn();
            });
        }
    });
</script>
<style type="text/css">
    /**********************************************    TABS CSS  **********************************************/
.formPanel{Width:100%;color: #D1D3D6;padding-top:5px;}
.formPanel fieldset{text-align: left;border: solid 1px #D1D3D6;margin: 0px;padding: 0px 0px 0px 5px;} 
.formPanel Table{padding:0px;margin:0px; }
.formPanel td{padding: 1px 3px 3px 0px;} 


.dvMainPanel .configTabs
{margin:3px;}

.configTabs ul.tabs
{margin: 0;padding: 0;float: left;list-style: none;height: 32px;color: #000000;
    border-left: 1px solid #E8E8E8;width: 100%;font-size: 9pt;font-family: Arial, Helvetica, sans-serif;}

.configTabs ul.tabs li
{float: left;margin: 0;cursor: pointer;padding: 0px 15px ;height: 31px;line-height: 31px;
    border: 1px solid #999999;border-bottom: #3D396B; font-weight: bold;background: #EEEEEE;overflow: hidden;
    position: relative;}

.configTabs ul.tabs li:hover
{background: #CCCCCC;}	

.configTabs ul.tabs li.active
{background: #25A0DA;border-bottom: 2px solid #25A0DA;color: #ffffff}

.configTabs .tab_container
{border-top: 2px solid #25A0DA;clear: both;float: left;width: 100%;background: #FFFFFF;color: #000000;font-size: 12px;}

.configTabs .tab_text {padding:5px;}

.configTabs .tab_content
{padding:0;font-size: 1.2em;display: none;}

.configTabs .tab_content .title
{padding:3px; background:#4d6b94; color:#fff; text-indent:5px;}

.adminTab { margin-left: 5px;height: 100%;padding-bottom: 5px;}
.adminTab .selectItem {text-align:center; padding:6px 0;}
.adminTab .selectItem label {font-size:12px; font-weight:bold; padding:0 10px;}
.adminTab .selectItem select {height:auto;}

.adviserTab {margin-left: 5px;height: 100%;padding-bottom: 5px;}
.adviserTab .searchItem {padding:10px; text-align:center;}
.adviserTab .searchItem label {font-weight:bold; padding:0 15px 0 0; font-size:11px;}
.adviserTab .searchItem span { font-weight:bold; text-align:center; padding: 0 25px; color:#ff5500;}
.adviserTab .searchItem input {border:1px solid #aacfe4; background:#fff; min-width:180px;}

.clientTab {margin-left: 5px;height: 100%;padding-bottom: 5px;}
.clientTab .searchItem {padding:10px; text-align:center;}
.clientTab .searchItem label {font-weight:bold; padding:0 15px 0 0; font-size:11px;}
.clientTab .searchItem span { font-weight:bold; text-align:center; padding: 0 25px; color:#ff5500;}
.clientTab .searchItem input {border:1px solid #aacfe4; background:#fff; min-width:180px;}

</style>
<div>   
    <asp:Panel ID="pnlConfigTabs" runat="server" CssClass="formPanel">
            <div class="configTabs">
                <ul runat="server" id="ulTabs" class="tabs">
                    <li id="liAdmin" rel="tab1" runat="server" class="active" >ADMIN</li>
                    <li id="liAdviser" rel="tab2" runat="server">ADVISER</li>
                    <li id="liClient" rel="tab3" runat="server">CLIENT</li>
                </ul>
                <div class="tab_container"> 
                    <div id="tab1" class="tab_content">
                        <div class="adminTab">
                           <div class="tab_text">Admin Alerts Configuration! You can set default settings for all users. These will apply to those users who do not have custom settings.</div>
                           <uc:AdminAlertConfiguration runat="server" ID="AdminAlertConfiguration1" />
                        </div>
                    </div>
                    <div id="tab2" class="tab_content">
                        <div class="adviserTab">
                            <div class="tab_text">Adviser Alerts Configuration! You can set default settings for all clients of an Adviser. These will apply to those client who do not have custom settings</div> 
                           <uc:AdviserAlertConfiguration runat="server" ID="AdviserAlertConfiguration1" />
                        </div>
                    </div>
                    <div id="tab3" class="tab_content">
                        <div class="clientTab">
                            <div class="tab_text">Client Alerts Configuration! You can customize Alert settings for a client.</div>
                            <uc:ClientAlertConfiguration runat="server" ID="ClientAlertConfiguration1" />
                        </div>
                    </div>
                </div>
            </div>
    </asp:Panel>
</div>