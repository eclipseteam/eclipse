﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NotesControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.NotesControl" %>
<script type="text/javascript">
    function DisableSaveButton(sender, args) {
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';

        sender.set_autoPostBack(true);
    }
</script>
<br />
<asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <fieldset>
            <legend>Notes & Comments</legend><span class="riLabel">
                <table>
                    <tr>
                        <td style="vertical-align: top">
                            <div>
                                <asp:Repeater ID="RepDetails" runat="server" OnItemDataBound="RepDetails_ItemDataBound"
                                    OnItemCommand="RepDetails_ItemCommand">
                                    <HeaderTemplate>
                                        <table style="border: 0px solid #3E3B6A; width: 100%; table-layout: fixed" cellpadding="0">
                                            <tr style="background-color: #3E3B6A; color: White">
                                                <td colspan="2">
                                                    <b>&nbsp;NOTES</b>
                                                </td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="word-wrap: break-word;" width="15%" colspan="2">
                                                <telerik:RadTextBox Width="100%" ID="txtCommentEdit" ReadOnly="true" runat="server"
                                                    TextMode="MultiLine" Text='<%#Eval("NOTES_DESCRIPTION_FIELD") %>' Resize="Both"
                                                    EmptyMessage="type here" Columns="20" Rows="5">
                                                </telerik:RadTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table style="background-color: #EBEFF0; border-top: 1px dotted #df5015; border-bottom: 1px solid #df5015;
                                                    width: 100%">
                                                    <tr>
                                                        <td>
                                                            Post By:
                                                            <asp:Label ID="lblUser" runat="server" Font-Bold="true" Text='<%#Eval("NOTES_CREATEDBY_FIELD") %>' />
                                                        </td>
                                                        <td>
                                                            Created Date:<asp:Label ID="lblDate" runat="server" Font-Bold="true" Text='<%#Eval("NOTES_CREATEDDATE_FIELD") %>' />
                                                        </td>
                                                        <td>
                                                            Updated By:
                                                            <asp:Label ID="Label1" runat="server" Font-Bold="true" Text='<%#Eval("NOTES_LASTUSERUPDATED_FIELD") %>' />
                                                        </td>
                                                        <td>
                                                            Update Date:<asp:Label ID="Label2" runat="server" Font-Bold="true" Text='<%#Eval("NOTES_LASTDATEUPDATED_FIELD") %>' />
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="LinkEdit" runat="server" CommandArgument='<%#Eval("Key") %>'
                                                                CommandName="edit">Edit</asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="LinkDelete" runat="server" CommandArgument='<%#Eval("Key") %>'
                                                                CommandName="delete">Del</asp:LinkButton>
                                                            <asp:LinkButton ID="LinkUpdate" runat="server" CommandArgument='<%#Eval("Key") %>'
                                                                CommandName="update" Visible="false">Update</asp:LinkButton>
                                                            <asp:LinkButton ID="Linkcancel" runat="server" CommandArgument='<%#Eval("Key") %>'
                                                                CommandName="cancel" Visible="false">Cancel</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </td>
                        <td style="vertical-align: top">
                            <div>
                                <table>
                                    <tr>
                                        <td>
                                            <telerik:RadTextBox Width="300px" ID="txtCommentBox" runat="server" TextMode="MultiLine"
                                                Resize="Both" EmptyMessage="type here" Columns="20" Rows="15">
                                            </telerik:RadTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <telerik:RadButton runat="server" ID="btnSubmitComment" Text="SUBMIT" OnClick="btnSubmit_Click">
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </span>
            <asp:Label runat="server" ID="lblCID" Visible="false"></asp:Label>
        </fieldset>
        <br />
    </ContentTemplate>
</asp:UpdatePanel>
