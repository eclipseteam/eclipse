﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ACN_InputControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.ACN_InputControl" %>
<span id="ctrlACNBody">
    <telerik:RadMaskedTextBox runat="server" ID="txtVal1" Mask="###" MaskType="Numeric"
        Width="50px" PromptChar=" ">
    </telerik:RadMaskedTextBox>
    <span>&nbsp;</span>
    <telerik:RadMaskedTextBox runat="server" ID="txtVal2" Mask="###" MaskType="Numeric"
        Width="50px" PromptChar=" " />
    <span>&nbsp;</span>
    <telerik:RadMaskedTextBox runat="server" ID="txtVal3" Mask="###" MaskType="Numeric"
        Width="50px" PromptChar=" " />
    <span id="Msg" runat="server" visible="false" style="color: Red;"></span></span>
