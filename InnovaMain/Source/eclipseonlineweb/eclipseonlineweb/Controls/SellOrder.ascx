﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SellOrder.ascx.cs" Inherits="eclipseonlineweb.Controls.SellOrder" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc2" TagName="SearchNSelectClientAccount" Src="~/Controls/SearchNSelectClientAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<style type="text/css">
    .rgCaption
    {
        color: blue;
        font: normal 8pt Arial;
        text-align: left !important;
    }
</style>
<script language="javascript" type="text/javascript">

    function ShowModalPopup() {
        window.$find("ModalBehaviour").show();
    }

    function HideModalPopup() {
        window.$find("ModalBehaviour").hide();
    }

    function SetHolding(txtUnitsID, txtAmountID, Unit, Amount, ddlOrderPreferedByID, chkBox) {
        var txtUnits = $find(txtUnitsID);
        var txtAmount = $find(txtAmountID);
        var ddlOrderPreferedBy = document.getElementById(ddlOrderPreferedByID);
        if (chkBox.checked) {
            switch (ddlOrderPreferedBy.value) {
                case 'Units':
                    txtUnits.set_value(Unit);
                    txtUnits._textBoxElement.readOnly = true;
                    break;
                case 'Amount':
                    txtAmount.set_value(Amount);
                    txtAmount._textBoxElement.readOnly = true;
                    break;
            }
        }
        else {
            txtUnits.set_value('');
            txtAmount.set_value('');
            switch (ddlOrderPreferedBy.value) {
                case 'Units':
                    txtUnits._textBoxElement.readOnly = false;
                    txtAmount._textBoxElement.readOnly = true;
                    break;
                case 'Amount':
                    txtUnits._textBoxElement.readOnly = true;
                    txtAmount._textBoxElement.readOnly = false;
                    break;
            }
        }
    }

    function SetHoldingForAtCall(txtAmountID, Amount, chkBox) {
        var txtAmount = $find(txtAmountID);

        if (chkBox.checked) {
            txtAmount.set_value(Amount);
        }
        else {
            txtAmount.set_value('');
        }
    }

    function ddlOrderPreferedBy_OnSelectedIndexChange(ddl, txtUnitID, txtAmountID, Unit, Amount, chkBoxID) {

        var txtUnit = $find(txtUnitID);
        var txtAmount = $find(txtAmountID);
        var chkBox = document.getElementById(chkBoxID);

        switch (ddl.value) {
            case 'Units':
                txtUnit._textBoxElement.readOnly = false || chkBox.checked;
                txtAmount._textBoxElement.readOnly = true;
                if (chkBox.checked)
                    txtUnit.set_value(Unit);
                txtAmount.set_value('');
                break;
            case 'Amount':
                txtUnit._textBoxElement.readOnly = true;
                txtAmount._textBoxElement.readOnly = false || chkBox.checked;
                txtUnit.set_value('');
                if (chkBox.checked)
                    txtAmount.set_value(Amount);
                break;
        }
    }
</script>
<asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
    <Triggers>
        <asp:PostBackTrigger ControlID="btnCutOff" />
    </Triggers>
    <ContentTemplate>
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="20%" runat="server" id="tdButtons">
                        <telerik:RadButton runat="server" ID="btnCutOff" ToolTip="Cut Of Time" OnClick="btnCutOff_OnClick">
                            <ContentTemplate>
                                <img alt="" src="../images/cut-off-time_106.png" class="btnImageWithText" />
                                <span class="riLabel">Cut-Off Times</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ID="btnSave" ToolTip="Place Order" OnClick="btnSave_OnClick">
                            <ContentTemplate>
                                <img src="../images/cart_add.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Place Order</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </td>
                    <td style="width: 80%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <telerik:RadButton ID="btnSearch" Visible="False" runat="server" Text="Select Client Account"
                            OnClick="btnSearch_OnClick">
                        </telerik:RadButton>
                        &nbsp;
                        <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <br />
        <div id="MainView" style="background-color: white">
            <table style="width: 100%; height: 50px" runat="server" id="showDetails" visible="false">
                <tr>
                    <td style="width: 120px">
                        Service Type:
                    </td>
                    <td style="width: 230px">
                        Cash Account:
                    </td>
                    <td style="width: 125px">
                        Available Funds:
                    </td>
                    <td id="tdCashBalanceHead" runat="server" style="width: 160px">
                        = Current Cash Balance
                    </td>
                    <td id="tdMinCashHead" runat="server" style="width: 150px">
                        - Min Cash Holdings
                    </td>
                    <td>
                        Order Account Type:
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadComboBox ID="cmbServiceType" AutoPostBack="true" Width="110px" runat="server"
                            OnSelectedIndexChanged="cmbServiceType_OnSelectedIndexChanged">
                        </telerik:RadComboBox>
                    </td>
                    <td>
                        <telerik:RadComboBox ID="cmbCashAccount" AutoPostBack="true" Width="220px" runat="server"
                            OnSelectedIndexChanged="cmbCashAccount_OnSelectedIndexChanged">
                        </telerik:RadComboBox>
                    </td>
                    <td>
                        <asp:Label ID="lblAvailableFunds" runat="server" Text=""></asp:Label>
                    </td>
                    <td id="tdCashBalanceValue" runat="server">
                        <asp:Label ID="lblCashBalance" runat="server" Text=""></asp:Label>
                    </td>
                    <td id="tdMinCashValue" runat="server">
                        <asp:Label ID="lblMinCash" runat="server" Text=""></asp:Label>
                    </td>
                    <td>
                        <telerik:RadComboBox ID="cmbOrderAccountType" runat="server" OnSelectedIndexChanged="cmbOrderAccountType_OnSelectedIndexChanged"
                            AutoPostBack="true">
                        </telerik:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <div id="divMsg" runat="server" style="color: Red" visible="false">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <telerik:RadGrid OnNeedDataSource="PresentationGridNeedDataSource" ID="PresentationGrid"
            runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
            AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="false" GridLines="None"
            AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
            AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="true" OnItemDataBound="PresentationGrid_OnItemDataBound">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                Name="PendingOrders" TableLayout="Fixed">
                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                <Columns>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="ProductID" HeaderText="ProductID"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="ProductID" UniqueName="ProductID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" ItemStyle-Width="110px"
                        ReadOnly="true" SortExpression="InvestmentCode" HeaderText="Investment Code"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="InvestmentCode" UniqueName="InvestmentCode">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="210px" HeaderStyle-Width="240px" ItemStyle-Width="230px"
                        ReadOnly="true" SortExpression="InvestmentName" HeaderText="Investment Name"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="InvestmentName" UniqueName="InvestmentName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ColumnGroupName="Current Investment" SortExpression="UnsettledSell"
                        ReadOnly="true" HeaderText="UnsettledSell" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UnsettledSell"
                        UniqueName="UnsettledSell" DataFormatString="{0:N4}" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" ItemStyle-Width="100px"
                        ColumnGroupName="Current Investment" SortExpression="Holding" ReadOnly="true"
                        HeaderText="Units" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Holding" UniqueName="Units"
                        DataFormatString="{0:N4}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" ItemStyle-Width="100px"
                        ColumnGroupName="Current Investment" SortExpression="UnitPrice" ReadOnly="true"
                        HeaderText="Unit Price" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UnitPrice" UniqueName="UnitPrice"
                        DataFormatString="{0:N6}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="100px" ItemStyle-Width="100px" ColumnGroupName="Current Investment"
                        ReadOnly="true" SortExpression="CurrentValue" HeaderText="Value" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" DataFormatString="{0:C}" AllowFiltering="false"
                        HeaderButtonType="TextButton" DataField="CurrentValue" UniqueName="CurrentValue"
                        FooterAggregateFormatString="{0:C}" Aggregate="Sum">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Requested By" ShowFilterIcon="false" AllowFiltering="false"
                        HeaderStyle-Width="90px">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlOrderPreferedBy" runat="server" AutoPostBack="false">
                                <asp:ListItem Text="Units" Value="Units"></asp:ListItem>
                                <asp:ListItem Text="Amount" Value="Amount"></asp:ListItem>
                            </asp:DropDownList>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Sell Whole Holding" ShowFilterIcon="false"
                        AllowFiltering="false" HeaderStyle-Width="70px">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSetHolding" runat="server" AutoPostBack="false"></asp:CheckBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="SellUnits" HeaderText="Sell Units (Preferred)"
                        HeaderStyle-Width="130px" ItemStyle-Width="130px" AllowFiltering="False" ColumnGroupName="Instruction to sell">
                        <ItemTemplate>
                            <telerik:RadNumericTextBox ID="txtUnits" runat="server" Width="110px">
                            </telerik:RadNumericTextBox><asp:CompareValidator ID="cvUnits" runat="server" ErrorMessage="*Invalid Units"
                                ControlToValidate="txtUnits" ValueToCompare='<%# Eval("Holding") %>' Display="Dynamic"
                                SetFocusOnError="true" ForeColor="Red" Operator="LessThanEqual"></asp:CompareValidator>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="SellAmount" HeaderText="Sell Amount" AllowFiltering="False"
                        ColumnGroupName="Instruction to sell" HeaderStyle-Width="150px" ItemStyle-Width="150px">
                        <ItemTemplate>
                            $&nbsp;<telerik:RadNumericTextBox ID="txtAmount" runat="server" Width="120px">
                            </telerik:RadNumericTextBox><asp:CompareValidator ID="cvAmount" runat="server" ErrorMessage="*Invalid Amount"
                                ControlToValidate="txtAmount" Display="Dynamic" SetFocusOnError="true" ForeColor="Red"
                                Type="Double" Operator="LessThanEqual">
                            </asp:CompareValidator>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <telerik:RadGrid OnNeedDataSource="TDPresentationGridNeedDataSource" ID="TDPresentationGrid"
            Visible="false" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
            PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="false"
            GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
            AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="true">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                Name="PendingOrders" TableLayout="Fixed">
                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                <Columns>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="ContractNote" HeaderText="ContractNote"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="ContractNote" UniqueName="ContractNote"
                        Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="ProductID" HeaderText="ProductID"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="ProductID" UniqueName="ProductID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="TDAccountCID" HeaderText="TDAccountCID"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="TDAccountCID" UniqueName="TDAccountCID"
                        Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="TDID" HeaderText="TDID"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="TDID" UniqueName="TDID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="BrokerId" HeaderText="BrokerId"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="BrokerId" UniqueName="BrokerId" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="InstituteId" HeaderText="InstituteId"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="InstituteId" UniqueName="InstituteId"
                        Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="100px" HeaderStyle-Width="150px" ItemStyle-Width="150px"
                        ReadOnly="true" SortExpression="BROKER" HeaderText="Broker" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="BROKER" UniqueName="InvestmentCode">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="200px" HeaderStyle-Width="250px" ItemStyle-Width="250px"
                        ReadOnly="true" SortExpression="InsName" HeaderText="Bank" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="InsName" UniqueName="InvestmentName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="150px" ItemStyle-Width="150px" ColumnGroupName="Current Investment"
                        ReadOnly="true" SortExpression="Total" HeaderText="Value" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" DataFormatString="{0:C}" AllowFiltering="false"
                        HeaderButtonType="TextButton" DataField="Total" UniqueName="CurrentValue" FooterAggregateFormatString="{0:C}"
                        Aggregate="Sum">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn UniqueName="SellAmount" HeaderText="Sell" AllowFiltering="False"
                        ColumnGroupName="Instruction to sell">
                        <ItemTemplate>
                            <asp:CheckBox ID="cbSelected" runat="server" Enabled='<%# Convert.ToDecimal(Eval("Total"))>0%>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <telerik:RadGrid ID="SellAtCallGrid" OnNeedDataSource="SellAtCallGrid_OnNeedDataSource"
            runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
            AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="false" GridLines="None"
            AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
            AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="true" OnItemDataBound="SellAtCallGrid_OnItemDataBound"
            Visible="false">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                Name="AtCallOrders" TableLayout="Fixed">
                <Columns>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="ProductID" HeaderText="ProductID"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="ProductID" UniqueName="ProductID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="BrokerId" HeaderText="BrokerId"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="BrokerId" UniqueName="BrokerId" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="InstituteId" HeaderText="InstituteId"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="InstituteId" UniqueName="InstituteId"
                        Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" ItemStyle-Width="110px"
                        ReadOnly="true" SortExpression="PRODUCTNAME" HeaderText="Broker" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="PRODUCTNAME" UniqueName="Broker">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="130px" ItemStyle-Width="130px"
                        ReadOnly="true" SortExpression="DESCRIPTION" HeaderText="Bank" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="DESCRIPTION" UniqueName="InvestmentName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="100px" ItemStyle-Width="100px"
                        ReadOnly="true" SortExpression="Total" HeaderText="Amount" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="Total" UniqueName="Amount">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Sell Whole Holding" ShowFilterIcon="false"
                        AllowFiltering="false" HeaderStyle-Width="70px">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSetHolding" runat="server" AutoPostBack="false"></asp:CheckBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="SellAmount" HeaderText="Sell Amount" AllowFiltering="False"
                        ColumnGroupName="Instruction to sell" HeaderStyle-Width="170px" ItemStyle-Width="170px">
                        <ItemTemplate>
                            $&nbsp;<telerik:RadNumericTextBox ID="txtAmount" runat="server" Width="150px">
                            </telerik:RadNumericTextBox><asp:CompareValidator ID="cvAmount" runat="server" ErrorMessage="*Invalid Amount"
                                ControlToValidate="txtAmount" Display="Dynamic" SetFocusOnError="true" ForeColor="Red"
                                Type="Double" Operator="LessThanEqual" ValueToCompare='<%# Eval("Total")%>'>
                            </asp:CompareValidator>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <asp:HiddenField ID="hfClientCID" runat="server" />
        <asp:HiddenField ID="hfClientID" runat="server" />
        <asp:HiddenField ID="hfIsAdmin" runat="server" />
        <asp:HiddenField ID="hfIsAdminMenu" runat="server" />
        <asp:HiddenField ID="hfIsSMA" runat="server" />
        <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
        <asp:ModalPopupExtender ID="popupSearch" runat="server" TargetControlID="btnShowPopup"
            PopupControlID="pnlpopup" PopupDragHandleControlID="PopupHeader" Drag="true"
            BackgroundCssClass="ModalPopupBG" BehaviorID="ModalBehaviour">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlpopup" runat="server" BackColor="White" Style="display: none">
            <div class="popup_Container">
                <div class="popup_Titlebar" id="PopupHeader">
                    <div class="TitlebarLeft">
                        Select Account
                    </div>
                    <div class="TitlebarRight" onclick="HideModalPopup();">
                    </div>
                </div>
                <div class="PopupBody">
                    <uc2:SearchNSelectClientAccount ID="searchAccountControl" runat="server" />
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
