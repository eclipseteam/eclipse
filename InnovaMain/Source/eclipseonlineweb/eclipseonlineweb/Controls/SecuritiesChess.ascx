﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecuritiesChess.ascx.cs"
    Inherits="eclipseonlineweb.Controls.SecuritiesChess" %>
<script>
    $(document).ready(function () {
        $(".autotab").keyup(function () {
            if ($(this).attr("maxlength") == $(this).val().length) {
                var index = $(".autotab").index(this);
                var item = $($(".autotab")[++index]);
                if (item.length > 0)
                    item.focus();
            }
        });
    });
  
</script>
<div class="MainView" style="padding-top: 5px;">
    <fieldset>
        <legend>CHESS Transfer Of Securities</legend>
        <p>
            Do you want to transfer any existing listed securities to the new Desktop Broker
            account to be established in your name?
            <br />
            <telerik:RadButton ID="btnChessTransYes" runat="server" ToggleType="Radio" ButtonType="ToggleButton"
                Text="Yes" Value="Yes" GroupName="ChessTransferYesNo">
            </telerik:RadButton>
            <telerik:RadButton ID="btnChessTransNo" runat="server" ToggleType="Radio" Text="No"
                Value="No" GroupName="ChessTransferYesNo" ButtonType="ToggleButton">
            </telerik:RadButton>
        </p>
        <p>
            If Yes to which service (ie. DIY, DIWM) :
            <br />
            <telerik:RadButton ID="btnServiceTypeDIY" runat="server" ToggleType="Radio" ButtonType="ToggleButton"
                Text="DIY" Value="DIY" GroupName="ChessServiceTypeDIYDIWM">
            </telerik:RadButton>
            <telerik:RadButton ID="btnServiceTypeDIWM" runat="server" ToggleType="Radio" Text="DIWM"
                Value="DIWM" GroupName="ChessServiceTypeDIYDIWM" ButtonType="ToggleButton">
            </telerik:RadButton>
        </p>
    </fieldset>
    <br />
    <fieldset>
        <legend>CHESS Registration Details</legend>
        <p>
            Please note that if you are transferring securities, The details below should be
            EXACTLY the same as on your latest holding statement.</p>
        <table width="100%">
            <tr>
                <td width="20%">
                    Registered Account Name:
                </td>
                <td colspan="3">
                    <telerik:RadTextBox runat="server" ID="txtRegAccountName" Width="90%">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td width="20%">
                    Account Designation (Designation must not be more than 25 characters):
                </td>
                <td colspan="3">
                    <telerik:RadTextBox runat="server" ID="txtad1" Width="20px" MaxLength="1" Height="20px"
                        Text="<" ReadOnly="True">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad2" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad3" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad4" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad5" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad6" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad7" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad8" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad9" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad10" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad11" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad12" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad13" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad14" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad15" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad16" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad17" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad18" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad19" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad20" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad21" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad22" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad23" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad24" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad25" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad26" Width="20px" MaxLength="1" Height="20px"
                        CssClass="autotab">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad27" Width="20px" MaxLength="1" Height="20px"
                        Text="A" ReadOnly="True">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad28" Width="20px" MaxLength="1" Height="20px"
                        Text="/" ReadOnly="True">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad29" Width="20px" MaxLength="1" Height="20px"
                        Text="C" ReadOnly="True">
                    </telerik:RadTextBox>
                    <telerik:RadTextBox runat="server" ID="txtad30" Width="20px" MaxLength="1" Height="20px"
                        Text=">" ReadOnly="True">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td width="20%">
                    Address Line 1:
                </td>
                <td colspan="3">
                    <telerik:RadTextBox runat="server" ID="txtAddressLine1Chess" Width="90%">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td width="20%">
                    Address Line 2:
                </td>
                <td colspan="3">
                    <telerik:RadTextBox runat="server" ID="txtAddressLine2Chess" Width="90%">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td width="20%">
                    Suburb:
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtSuburbChess" Width="62%">
                    </telerik:RadTextBox>
                </td>
                <td>
                    State:
                </td>
                <td>
                    <telerik:RadComboBox runat="server" ID="CmboStatesChess" Width="250px">
                        <Items>
                            <telerik:RadComboBoxItem Value="" Text="" />
                            <telerik:RadComboBoxItem Value="ACT" Text="ACT" />
                            <telerik:RadComboBoxItem Value="NSW" Text="NSW" />
                            <telerik:RadComboBoxItem Value="NT" Text="NT" />
                            <telerik:RadComboBoxItem Value="QLD" Text="QLD" />
                            <telerik:RadComboBoxItem Value="SA" Text="SA" />
                            <telerik:RadComboBoxItem Value="TAS" Text="TAS" />
                            <telerik:RadComboBoxItem Value="VIC" Text="VIC" />
                            <telerik:RadComboBoxItem Value="WA" Text="WA" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    Postcode:
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtPostCodeChess" Width="62%">
                    </telerik:RadTextBox>
                </td>
                <td>
                    Country:
                </td>
                <td>
                    <telerik:RadComboBox runat="server" ID="CmboCountryChess" Width="250px">
                        <Items>
                            <telerik:RadComboBoxItem Value="AF" Text="Afghanistan" />
                            <telerik:RadComboBoxItem Value="AL" Text="Albania" />
                            <telerik:RadComboBoxItem Value="DZ" Text="Algeria" />
                            <telerik:RadComboBoxItem Value="AS" Text="American Samoa" />
                            <telerik:RadComboBoxItem Value="AD" Text="Andorra" />
                            <telerik:RadComboBoxItem Value="AO" Text="Angola" />
                            <telerik:RadComboBoxItem Value="AI" Text="Anguilla" />
                            <telerik:RadComboBoxItem Value="AQ" Text="Antarctica" />
                            <telerik:RadComboBoxItem Value="AG" Text="Antigua And Barbuda" />
                            <telerik:RadComboBoxItem Value="AR" Text="Argentina" />
                            <telerik:RadComboBoxItem Value="AM" Text="Armenia" />
                            <telerik:RadComboBoxItem Value="AW" Text="Aruba" />
                            <telerik:RadComboBoxItem Value="AU" Text="Australia" Selected="True" />
                            <telerik:RadComboBoxItem Value="AT" Text="Austria" />
                            <telerik:RadComboBoxItem Value="AZ" Text="Azerbaijan" />
                            <telerik:RadComboBoxItem Value="BS" Text="Bahamas" />
                            <telerik:RadComboBoxItem Value="BH" Text="Bahrain" />
                            <telerik:RadComboBoxItem Value="BD" Text="Bangladesh" />
                            <telerik:RadComboBoxItem Value="BB" Text="Barbados" />
                            <telerik:RadComboBoxItem Value="BY" Text="Belarus" />
                            <telerik:RadComboBoxItem Value="BE" Text="Belgium" />
                            <telerik:RadComboBoxItem Value="BZ" Text="Belize" />
                            <telerik:RadComboBoxItem Value="BJ" Text="Benin" />
                            <telerik:RadComboBoxItem Value="BM" Text="Bermuda" />
                            <telerik:RadComboBoxItem Value="BT" Text="Bhutan" />
                            <telerik:RadComboBoxItem Value="BO" Text="Bolivia" />
                            <telerik:RadComboBoxItem Value="BA" Text="Bosnia And Herzegowina" />
                            <telerik:RadComboBoxItem Value="BW" Text="Botswana" />
                            <telerik:RadComboBoxItem Value="BV" Text="Bouvet Island" />
                            <telerik:RadComboBoxItem Value="BR" Text="Brazil" />
                            <telerik:RadComboBoxItem Value="IO" Text="British Indian Ocean Territory" />
                            <telerik:RadComboBoxItem Value="BN" Text="Brunei Darussalam" />
                            <telerik:RadComboBoxItem Value="BG" Text="Bulgaria" />
                            <telerik:RadComboBoxItem Value="BF" Text="Burkina Faso" />
                            <telerik:RadComboBoxItem Value="BI" Text="Burundi" />
                            <telerik:RadComboBoxItem Value="KH" Text="Cambodia" />
                            <telerik:RadComboBoxItem Value="CM" Text="Cameroon" />
                            <telerik:RadComboBoxItem Value="CA" Text="Canada" />
                            <telerik:RadComboBoxItem Value="CV" Text="Cape Verde" />
                            <telerik:RadComboBoxItem Value="KY" Text="Cayman Islands" />
                            <telerik:RadComboBoxItem Value="CF" Text="Central African Republic" />
                            <telerik:RadComboBoxItem Value="TD" Text="Chad" />
                            <telerik:RadComboBoxItem Value="CL" Text="Chile" />
                            <telerik:RadComboBoxItem Value="CN" Text="China" />
                            <telerik:RadComboBoxItem Value="CX" Text="Christmas Island" />
                            <telerik:RadComboBoxItem Value="CC" Text="Cocos (Keeling) Islands" />
                            <telerik:RadComboBoxItem Value="CO" Text="Colombia" />
                            <telerik:RadComboBoxItem Value="KM" Text="Comoros" />
                            <telerik:RadComboBoxItem Value="CG" Text="Congo" />
                            <telerik:RadComboBoxItem Value="CK" Text="Cook Islands" />
                            <telerik:RadComboBoxItem Value="CR" Text="Costa Rica" />
                            <telerik:RadComboBoxItem Value="CI" Text="Cote D'Ivoire" />
                            <telerik:RadComboBoxItem Value="HR" Text="Croatia (Local Name: Hrvatska)" />
                            <telerik:RadComboBoxItem Value="CU" Text="Cuba" />
                            <telerik:RadComboBoxItem Value="CY" Text="Cyprus" />
                            <telerik:RadComboBoxItem Value="CZ" Text="Czech Republic" />
                            <telerik:RadComboBoxItem Value="DK" Text="Denmark" />
                            <telerik:RadComboBoxItem Value="DJ" Text="Djibouti" />
                            <telerik:RadComboBoxItem Value="DM" Text="Dominica" />
                            <telerik:RadComboBoxItem Value="DO" Text="Dominican Republic" />
                            <telerik:RadComboBoxItem Value="TP" Text="East Timor" />
                            <telerik:RadComboBoxItem Value="EC" Text="Ecuador" />
                            <telerik:RadComboBoxItem Value="EG" Text="Egypt" />
                            <telerik:RadComboBoxItem Value="SV" Text="El Salvador" />
                            <telerik:RadComboBoxItem Value="GQ" Text="Equatorial Guinea" />
                            <telerik:RadComboBoxItem Value="ER" Text="Eritrea" />
                            <telerik:RadComboBoxItem Value="EE" Text="Estonia" />
                            <telerik:RadComboBoxItem Value="ET" Text="Ethiopia" />
                            <telerik:RadComboBoxItem Value="FK" Text="Falkland Islands (Malvinas)" />
                            <telerik:RadComboBoxItem Value="FO" Text="Faroe Islands" />
                            <telerik:RadComboBoxItem Value="FJ" Text="Fiji" />
                            <telerik:RadComboBoxItem Value="FI" Text="Finland" />
                            <telerik:RadComboBoxItem Value="FR" Text="France" />
                            <telerik:RadComboBoxItem Value="GF" Text="French Guiana" />
                            <telerik:RadComboBoxItem Value="PF" Text="French Polynesia" />
                            <telerik:RadComboBoxItem Value="TF" Text="French Southern Territories" />
                            <telerik:RadComboBoxItem Value="GA" Text="Gabon" />
                            <telerik:RadComboBoxItem Value="GM" Text="Gambia" />
                            <telerik:RadComboBoxItem Value="GE" Text="Georgia" />
                            <telerik:RadComboBoxItem Value="DE" Text="Germany" />
                            <telerik:RadComboBoxItem Value="GH" Text="Ghana" />
                            <telerik:RadComboBoxItem Value="GI" Text="Gibraltar" />
                            <telerik:RadComboBoxItem Value="GR" Text="Greece" />
                            <telerik:RadComboBoxItem Value="GL" Text="Greenland" />
                            <telerik:RadComboBoxItem Value="GD" Text="Grenada" />
                            <telerik:RadComboBoxItem Value="GP" Text="Guadeloupe" />
                            <telerik:RadComboBoxItem Value="GU" Text="Guam" />
                            <telerik:RadComboBoxItem Value="GT" Text="Guatemala" />
                            <telerik:RadComboBoxItem Value="GN" Text="Guinea" />
                            <telerik:RadComboBoxItem Value="GW" Text="Guinea-Bissau" />
                            <telerik:RadComboBoxItem Value="GY" Text="Guyana" />
                            <telerik:RadComboBoxItem Value="HT" Text="Haiti" />
                            <telerik:RadComboBoxItem Value="HM" Text="Heard And Mc Donald Islands" />
                            <telerik:RadComboBoxItem Value="VA" Text="Holy See (Vatican City State)" />
                            <telerik:RadComboBoxItem Value="HN" Text="Honduras" />
                            <telerik:RadComboBoxItem Value="HK" Text="Hong Kong" />
                            <telerik:RadComboBoxItem Value="HU" Text="Hungary" />
                            <telerik:RadComboBoxItem Value="IS" Text="Icel And" />
                            <telerik:RadComboBoxItem Value="IN" Text="India" />
                            <telerik:RadComboBoxItem Value="ID" Text="Indonesia" />
                            <telerik:RadComboBoxItem Value="IR" Text="Iran (Islamic Republic Of)" />
                            <telerik:RadComboBoxItem Value="IQ" Text="Iraq" />
                            <telerik:RadComboBoxItem Value="IE" Text="Ireland" />
                            <telerik:RadComboBoxItem Value="IL" Text="Israel" />
                            <telerik:RadComboBoxItem Value="IT" Text="Italy" />
                            <telerik:RadComboBoxItem Value="JM" Text="Jamaica" />
                            <telerik:RadComboBoxItem Value="JP" Text="Japan" />
                            <telerik:RadComboBoxItem Value="JO" Text="Jordan" />
                            <telerik:RadComboBoxItem Value="KZ" Text="Kazakhstan" />
                            <telerik:RadComboBoxItem Value="KE" Text="Kenya" />
                            <telerik:RadComboBoxItem Value="KI" Text="Kiribati" />
                            <telerik:RadComboBoxItem Value="KP" Text="Korea, Dem People'S Republic" />
                            <telerik:RadComboBoxItem Value="KR" Text="Korea, Republic Of" />
                            <telerik:RadComboBoxItem Value="KW" Text="Kuwait" />
                            <telerik:RadComboBoxItem Value="KG" Text="Kyrgyzstan" />
                            <telerik:RadComboBoxItem Value="LA" Text="Lao People'S Dem Republic" />
                            <telerik:RadComboBoxItem Value="LV" Text="Latvia" />
                            <telerik:RadComboBoxItem Value="LB" Text="Lebanon" />
                            <telerik:RadComboBoxItem Value="LS" Text="Lesotho" />
                            <telerik:RadComboBoxItem Value="LR" Text="Liberia" />
                            <telerik:RadComboBoxItem Value="LY" Text="Libyan Arab Jamahiriya" />
                            <telerik:RadComboBoxItem Value="LI" Text="Liechtenstein" />
                            <telerik:RadComboBoxItem Value="LT" Text="Lithuania" />
                            <telerik:RadComboBoxItem Value="LU" Text="Luxembourg" />
                            <telerik:RadComboBoxItem Value="MO" Text="Macau" />
                            <telerik:RadComboBoxItem Value="MK" Text="Macedonia" />
                            <telerik:RadComboBoxItem Value="MG" Text="Madagascar" />
                            <telerik:RadComboBoxItem Value="MW" Text="Malawi" />
                            <telerik:RadComboBoxItem Value="MY" Text="Malaysia" />
                            <telerik:RadComboBoxItem Value="MV" Text="Maldives" />
                            <telerik:RadComboBoxItem Value="ML" Text="Mali" />
                            <telerik:RadComboBoxItem Value="MT" Text="Malta" />
                            <telerik:RadComboBoxItem Value="MH" Text="Marshall Islands" />
                            <telerik:RadComboBoxItem Value="MQ" Text="Martinique" />
                            <telerik:RadComboBoxItem Value="MR" Text="Mauritania" />
                            <telerik:RadComboBoxItem Value="MU" Text="Mauritius" />
                            <telerik:RadComboBoxItem Value="YT" Text="Mayotte" />
                            <telerik:RadComboBoxItem Value="MX" Text="Mexico" />
                            <telerik:RadComboBoxItem Value="FM" Text="Micronesia, Federated States" />
                            <telerik:RadComboBoxItem Value="MD" Text="Moldova, Republic Of" />
                            <telerik:RadComboBoxItem Value="MC" Text="Monaco" />
                            <telerik:RadComboBoxItem Value="MN" Text="Mongolia" />
                            <telerik:RadComboBoxItem Value="MS" Text="Montserrat" />
                            <telerik:RadComboBoxItem Value="MA" Text="Morocco" />
                            <telerik:RadComboBoxItem Value="MZ" Text="Mozambique" />
                            <telerik:RadComboBoxItem Value="MM" Text="Myanmar" />
                            <telerik:RadComboBoxItem Value="NA" Text="Namibia" />
                            <telerik:RadComboBoxItem Value="NR" Text="Nauru" />
                            <telerik:RadComboBoxItem Value="NP" Text="Nepal" />
                            <telerik:RadComboBoxItem Value="NL" Text="Netherlands" />
                            <telerik:RadComboBoxItem Value="AN" Text="Netherlands Ant Illes" />
                            <telerik:RadComboBoxItem Value="NC" Text="New Caledonia" />
                            <telerik:RadComboBoxItem Value="NZ" Text="New Zealand" />
                            <telerik:RadComboBoxItem Value="NI" Text="Nicaragua" />
                            <telerik:RadComboBoxItem Value="NE" Text="Niger" />
                            <telerik:RadComboBoxItem Value="NG" Text="Nigeria" />
                            <telerik:RadComboBoxItem Value="NU" Text="Niue" />
                            <telerik:RadComboBoxItem Value="NF" Text="Norfolk Island" />
                            <telerik:RadComboBoxItem Value="MP" Text="Northern Mariana Islands" />
                            <telerik:RadComboBoxItem Value="NO" Text="Norway" />
                            <telerik:RadComboBoxItem Value="OM" Text="Oman" />
                            <telerik:RadComboBoxItem Value="PK" Text="Pakistan" />
                            <telerik:RadComboBoxItem Value="PW" Text="Palau" />
                            <telerik:RadComboBoxItem Value="PA" Text="Panama" />
                            <telerik:RadComboBoxItem Value="PG" Text="Papua New Guinea" />
                            <telerik:RadComboBoxItem Value="PY" Text="Paraguay" />
                            <telerik:RadComboBoxItem Value="PE" Text="Peru" />
                            <telerik:RadComboBoxItem Value="PH" Text="Philippines" />
                            <telerik:RadComboBoxItem Value="PN" Text="Pitcairn" />
                            <telerik:RadComboBoxItem Value="PL" Text="Poland" />
                            <telerik:RadComboBoxItem Value="PT" Text="Portugal" />
                            <telerik:RadComboBoxItem Value="PR" Text="Puerto Rico" />
                            <telerik:RadComboBoxItem Value="QA" Text="Qatar" />
                            <telerik:RadComboBoxItem Value="RE" Text="Reunion" />
                            <telerik:RadComboBoxItem Value="RO" Text="Romania" />
                            <telerik:RadComboBoxItem Value="RU" Text="Russian Federation" />
                            <telerik:RadComboBoxItem Value="RW" Text="Rwanda" />
                            <telerik:RadComboBoxItem Value="KN" Text="Saint K Itts And Nevis" />
                            <telerik:RadComboBoxItem Value="LC" Text="Saint Lucia" />
                            <telerik:RadComboBoxItem Value="VC" Text="Saint Vincent, The Grenadines" />
                            <telerik:RadComboBoxItem Value="WS" Text="Samoa" />
                            <telerik:RadComboBoxItem Value="SM" Text="San Marino" />
                            <telerik:RadComboBoxItem Value="ST" Text="Sao Tome And Principe" />
                            <telerik:RadComboBoxItem Value="SA" Text="Saudi Arabia" />
                            <telerik:RadComboBoxItem Value="SN" Text="Senegal" />
                            <telerik:RadComboBoxItem Value="SC" Text="Seychelles" />
                            <telerik:RadComboBoxItem Value="SL" Text="Sierra Leone" />
                            <telerik:RadComboBoxItem Value="SG" Text="Singapore" />
                            <telerik:RadComboBoxItem Value="SK" Text="Slovakia (Slovak Republic)" />
                            <telerik:RadComboBoxItem Value="SI" Text="Slovenia" />
                            <telerik:RadComboBoxItem Value="SB" Text="Solomon Islands" />
                            <telerik:RadComboBoxItem Value="SO" Text="Somalia" />
                            <telerik:RadComboBoxItem Value="ZA" Text="South Africa" />
                            <telerik:RadComboBoxItem Value="GS" Text="South Georgia , S Sandwich Is." />
                            <telerik:RadComboBoxItem Value="ES" Text="Spain" />
                            <telerik:RadComboBoxItem Value="LK" Text="Sri Lanka" />
                            <telerik:RadComboBoxItem Value="SH" Text="St. Helena" />
                            <telerik:RadComboBoxItem Value="PM" Text="St. Pierre And Miquelon" />
                            <telerik:RadComboBoxItem Value="SD" Text="Sudan" />
                            <telerik:RadComboBoxItem Value="SR" Text="Suriname" />
                            <telerik:RadComboBoxItem Value="SJ" Text="Svalbard, Jan Mayen Islands" />
                            <telerik:RadComboBoxItem Value="SZ" Text="Sw Aziland" />
                            <telerik:RadComboBoxItem Value="SE" Text="Sweden" />
                            <telerik:RadComboBoxItem Value="CH" Text="Switzerland" />
                            <telerik:RadComboBoxItem Value="SY" Text="Syrian Arab Republic" />
                            <telerik:RadComboBoxItem Value="TW" Text="Taiwan" />
                            <telerik:RadComboBoxItem Value="TJ" Text="Tajikistan" />
                            <telerik:RadComboBoxItem Value="TZ" Text="Tanzania, United Republic Of" />
                            <telerik:RadComboBoxItem Value="TH" Text="Thailand" />
                            <telerik:RadComboBoxItem Value="TG" Text="Togo" />
                            <telerik:RadComboBoxItem Value="TK" Text="Tokelau" />
                            <telerik:RadComboBoxItem Value="TO" Text="Tonga" />
                            <telerik:RadComboBoxItem Value="TT" Text="Trinidad And Tobago" />
                            <telerik:RadComboBoxItem Value="TN" Text="Tunisia" />
                            <telerik:RadComboBoxItem Value="TR" Text="Turkey" />
                            <telerik:RadComboBoxItem Value="TM" Text="Turkmenistan" />
                            <telerik:RadComboBoxItem Value="TC" Text="Turks And Caicos Islands" />
                            <telerik:RadComboBoxItem Value="TV" Text="Tuvalu" />
                            <telerik:RadComboBoxItem Value="UG" Text="Uganda" />
                            <telerik:RadComboBoxItem Value="UA" Text="Ukraine" />
                            <telerik:RadComboBoxItem Value="AE" Text="United Arab Emirates" />
                            <telerik:RadComboBoxItem Value="GB" Text="United Kingdom" />
                            <telerik:RadComboBoxItem Value="US" Text="United States" />
                            <telerik:RadComboBoxItem Value="UM" Text="United States Minor Is." />
                            <telerik:RadComboBoxItem Value="UY" Text="Uruguay" />
                            <telerik:RadComboBoxItem Value="UZ" Text="Uzbekistan" />
                            <telerik:RadComboBoxItem Value="VU" Text="Vanuatu" />
                            <telerik:RadComboBoxItem Value="VE" Text="Venezuela" />
                            <telerik:RadComboBoxItem Value="VN" Text="Viet Nam" />
                            <telerik:RadComboBoxItem Value="VG" Text="Virgin Islands (British)" />
                            <telerik:RadComboBoxItem Value="VI" Text="Virgin Islands (U.S.)" />
                            <telerik:RadComboBoxItem Value="WF" Text="Wallis And Futuna Islands" />
                            <telerik:RadComboBoxItem Value="EH" Text="Western Sahara" />
                            <telerik:RadComboBoxItem Value="YE" Text="Yemen" />
                            <telerik:RadComboBoxItem Value="YU" Text="Yugoslavia" />
                            <telerik:RadComboBoxItem Value="ZR" Text="Zaire" />
                            <telerik:RadComboBoxItem Value="ZM" Text="Zambia" />
                            <telerik:RadComboBoxItem Value="ZW" Text="Zimbabwe" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <b>Please note: This address will receive all CHESS and registry correspondence (HIN
                        confirmation, TFN confirmation, Bank A/C confirmation, Dividend statements, Annual
                        reports, Corporate Action Notices)</b>
                </td>
            </tr>
        </table>
        <br />
    </fieldset>
    <br />
    <fieldset>
        <legend>CHESS Sponsorship Transfer</legend>
        <p>
            Are the securities to be transferred</p>
        <ul>
            <li>
                <telerik:RadButton ID="chkIssueSponsorChess" runat="server" Text="Issuer Sponsored - please attach copies of your Issuer Sponsored Holding Statements"
                    AutoPostBack="false" ToggleType="CheckBox" ButtonType="ToggleButton">
                </telerik:RadButton>
            </li>
            <ul>
                <li>
                    <telerik:RadButton ID="chkExactNameAddressMatchAbove" runat="server" Text="Exact Name/Address match as above"
                        AutoPostBack="false" ToggleType="CheckBox" ButtonType="ToggleButton">
                    </telerik:RadButton>
                    <br />
                    <br />
                    How many different company holdings
                    <telerik:RadTextBox runat="server" ID="txtHowManyHoldingCompanyChess">
                    </telerik:RadTextBox>
                    <br />
                    <br />
                    i. Use an Issuer Sponsored Holdings to CHESS Sponsorship Conversion Form for all
                    matching holdings<br />
                    <br />
                </li>
                <li>
                    <telerik:RadButton ID="chkExactNameAddressNOTMatchAbove" runat="server" Text="Name/Address does not match the CHESS Registration Details above"
                        AutoPostBack="false" ToggleType="CheckBox" ButtonType="ToggleButton">
                    </telerik:RadButton>
                    <br />
                    <br />
                    How many different company holdings need to be changed?
                    <telerik:RadTextBox runat="server" ID="txtHowManyHoldingCompanyChessNotMatch">
                    </telerik:RadTextBox>
                    <br />
                    <br />
                    i. Use Change of client details form<br />
                    <br />
                    ii. Use an Issuer Sponsored Holdings to CHESS Sponsorship Conversion Form for all
                    un-matched holdings </li>
            </ul>
        </ul>
        <ul>
            <li>
                <telerik:RadButton ID="chkBrokerSponsoredRegistration" runat="server" Text="Broker Sponsored - please attach copies of your CHESS Registration"
                    AutoPostBack="false" ToggleType="CheckBox" ButtonType="ToggleButton">
                </telerik:RadButton>
            </li>
            <ul>
                <li>i. Use the Broker to Broker Transfer Request, for each different Broker</li>
                <li>ii. If names/addresses are not matched, use a Client Change of Details Form</li>
                <li>iii. If there is a change of Legal or Beneficial ownership, use an Off Market Transfer
                    Form for each holding</li>
            </ul>
        </ul>
        <br />
        <table>
            <tr>
                <td>
                    Name of existing Sponsoring Broker:
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtNameOfSponsorBroker1" Width="200px" />
                </td>
            </tr>
            <tr>
                <td>
                    Existing Broker PID:
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtBrokerID1" Width="200px">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Client HIN:
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtClientHin1" Width="200px">
                    </telerik:RadTextBox>
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td>
                    Name of existing Sponsoring Broker:
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtNameOfSponsorBroker2" Width="200px">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Existing Broker PID:
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtBrokerID2" Width="200px">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Client HIN:
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtClientHin2" Width="200px">
                    </telerik:RadTextBox>
                </td>
            </tr>
        </table>
    </fieldset>
</div>
