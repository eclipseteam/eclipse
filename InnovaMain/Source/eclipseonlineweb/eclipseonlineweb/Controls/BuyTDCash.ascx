﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BuyTDCash.ascx.cs" Inherits="eclipseonlineweb.Controls.BuyTDCash" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc2" TagName="SearchNSelectClientAccount" Src="~/Controls/SearchNSelectClientAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<style>
    .rgCaption
    {
        color: Blue;
        font: bold 10pt Arial Narrow;
    }
</style>
<script language="javascript" type="text/javascript">

    function ShowModalPopup() {
        window.$find("ModalBehaviour").show();
    }

    function HideModalPopup() {
        window.$find("ModalBehaviour").hide();
    }

    function ShowAlert(sender, args) {
        var isOK = true;
        var broker = document.getElementById("<%:hfIsAlert.ClientID %>").value;
        var tdAccStatus = document.getElementById("<%:hfTDAccStatus.ClientID %>").value;
        if (broker != "" && tdAccStatus == "") {
            var msg = "This order may be delayed as we do not have an account with " + broker + " opened yet.\nDo you want to place this order anyway?\nPlease contact e-Clipse Head Office if assistance is required.";
            var x = confirm(msg);
            if (x) {
                isOK = true;
            }
            else {
                isOK = false;
            }
        }
        else if (tdAccStatus != "active" && tdAccStatus != "") {
            var msg = "This order may be delayed as we do not have an account with " + broker + " activated yet.\nDo you want to place this order anyway?\nPlease contact e-Clipse Head Office if assistance is required.";
            var x = confirm(msg);
            if (x) {
                isOK = true;
            }
            else {
                isOK = false;
            }
        }
        sender.set_autoPostBack(isOK);
    }
</script>
<asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
    <Triggers>
        <asp:PostBackTrigger ControlID="btnCutOff" />
    </Triggers>
    <ContentTemplate>
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="28%" runat="server" id="tdButtons">
                        <telerik:RadButton runat="server" ID="btnBack" OnClick="btnBack_OnClick" Visible="false"
                            ToolTip="Back to TD order">
                            <ContentTemplate>
                                <img src="../images/window_previous.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Back</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ID="imgItems" ToolTip="Items to order" OnClick="btnSave_OnClick">
                            <ContentTemplate>
                                <img src="../images/book_next.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Next</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ID="btnCutOff" ToolTip="Cut Of Time" OnClick="btnCutOff_OnClick">
                            <ContentTemplate>
                                <img alt="" src="../images/cut-off-time_106.png" class="btnImageWithText" />
                                <span class="riLabel">Cut-Off Times</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ID="btnSave" ToolTip="Place Order" OnClick="btnSave_OnClick"
                            OnClientClicked="ShowAlert" Visible="false">
                            <ContentTemplate>
                                <img src="../images/cart_add.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Place Order</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </td>
                    <td style="width: 80%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <telerik:RadButton runat="server" ID="btnSearch" Visible="False" OnClick="btnSearch_OnClick"
                            Text="Select Client Account">
                        </telerik:RadButton>
                        &nbsp;<uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <br />
        <asp:HiddenField ID="hfIsAlert" runat="server" />
        <asp:HiddenField ID="hfTDAccStatus" runat="server" />
        <div id="MainView" style="background-color: white" runat="server">
            <table style="width: 100%; height: 50px" runat="server" id="showDetails" visible="false">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    Service Type:
                                </td>
                                <td>
                                    Cash Account:
                                </td>
                                <td>
                                    Available Funds:
                                </td>
                                <td id="tdCashBalanceHead" runat="server">
                                    = Current Cash Balance
                                </td>
                                <td id="tdMinCashHead" runat="server">
                                    - Min Cash Holdings
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <telerik:RadComboBox ID="cmbServiceType" AutoPostBack="true" Width="110px" runat="server"
                                        OnSelectedIndexChanged="cmbServiceType_OnSelectedIndexChanged">
                                    </telerik:RadComboBox>
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="cmbCashAccount" AutoPostBack="true" Width="220px" runat="server"
                                        OnSelectedIndexChanged="cmbCashAccount_OnSelectedIndexChanged">
                                    </telerik:RadComboBox>
                                </td>
                                <td>
                                    <asp:Label ID="lblAvailableFunds" runat="server" Text=""></asp:Label>
                                </td>
                                <td id="tdCashBalanceValue" runat="server">
                                    <asp:Label ID="lblCashBalance" runat="server" Text=""></asp:Label>
                                </td>
                                <td id="tdMinCashValue" runat="server">
                                    <asp:Label ID="lblMinCash" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td id="trMain" runat="server">
                        <table>
                            <tr>
                                <td>
                                    Min Rating:
                                </td>
                                <td>
                                    Min Term:
                                </td>
                                <td>
                                    Max Term:
                                </td>
                                <td>
                                    Amount to Invest: $
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <telerik:RadComboBox ID="cmbRating" runat="server" Width="80">
                                    </telerik:RadComboBox>
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="cmbMinTerm" runat="server" Width="80">
                                    </telerik:RadComboBox>
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="cmbMaxTerm" runat="server" Width="80">
                                    </telerik:RadComboBox>
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ID="txtAmounttoInvest" runat="server">
                                    </telerik:RadNumericTextBox>&nbsp;&nbsp;
                                    <telerik:RadButton runat="server" ID="btnFindTDCash" Text="Filter Term Deposits"
                                        OnClick="btnFindTDCash_OnClick">
                                    </telerik:RadButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <telerik:RadGrid OnNeedDataSource="PresentationGridNeedDataSource" ID="PresentationGrid"
            runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
            AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="False" GridLines="None"
            AllowAutomaticDeletes="True" AllowFilteringByColumn="False" AllowAutomaticInserts="True"
            AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnItemDataBound="PresentationGridItemDataBound"
            OnSortCommand="PresentationGrid_OnSortCommand">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowMultiColumnSorting="False" Width="100%" CommandItemDisplay="Top"
                Name="TDCash" TableLayout="Fixed" Font-Size="8" Caption="The highlighted fields show the best rate available for each term">
                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                <Columns>
                    <telerik:GridBoundColumn ReadOnly="true" UniqueName="TDAccountStatus" Display="False">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" UniqueName="ProductID" Display="False">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" UniqueName="TDAccountCID" Display="False">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="ID" HeaderText="ID" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="ID" UniqueName="ID" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="InstituteID" HeaderText="InstituteID"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="InstituteID" UniqueName="BrokerID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="InstituteName" HeaderText="Broker"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="InstituteName" UniqueName="Broker">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="PriceInstituteID" ReadOnly="true" HeaderText="PriceInstituteID"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="PriceInstituteID" UniqueName="BankID"
                        Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="10%" ItemStyle-Width="10%" ReadOnly="true"
                        SortExpression="PriceInstituteName" HeaderText="Bank" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="PriceInstituteName" UniqueName="Bank">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="7%" ItemStyle-Width="7%" SortExpression="Min"
                        ReadOnly="true" HeaderText="Min" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Min" UniqueName="Min"
                        Visible="true" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="7%" ItemStyle-Width="7%" SortExpression="Max"
                        ReadOnly="true" HeaderText="Max" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Max" UniqueName="Max"
                        Visible="true" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="PriceInstituteLongRatingValue" ReadOnly="true"
                        HeaderText="R" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="PriceInstituteLongRatingValue" UniqueName="RatingValue"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="40px" ItemStyle-Width="40px" SortExpression="PriceInstituteLongRating"
                        ReadOnly="true" HeaderText="R" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="PriceInstituteLongRating"
                        UniqueName="Rating">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="Days30" HeaderText="Days30"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Days30" UniqueName="Days30" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn AutoPostBackOnFilter="true" HeaderText="30-D" AllowFiltering="False"
                        UniqueName="30-D">
                        <ItemStyle Wrap="False"></ItemStyle>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDays30" runat="server" Text='<%# Eval("Days30", "{0:P}") %>'
                                Visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("Days30"))) %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="Days60" HeaderText="Days60"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Days60" UniqueName="Days60" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn AutoPostBackOnFilter="true" HeaderText="60-D" AllowFiltering="False"
                        UniqueName="60-D">
                        <ItemStyle Wrap="False"></ItemStyle>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDays60" runat="server" Text='<%# Eval("Days60", "{0:P}") %>'
                                Visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("Days60"))) %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="Days90" HeaderText="Days90"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Days90" UniqueName="Days90" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn AutoPostBackOnFilter="true" HeaderText="90-D" AllowFiltering="False"
                        UniqueName="90-D">
                        <ItemStyle Wrap="False"></ItemStyle>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDays90" runat="server" Text='<%# Eval("Days90", "{0:P}") %>'
                                Visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("Days90"))) %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="Days120" HeaderText="Days120"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Days120" UniqueName="Days120" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn AutoPostBackOnFilter="true" HeaderText="120-D" AllowFiltering="False"
                        UniqueName="120-D">
                        <ItemStyle Wrap="False"></ItemStyle>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDays120" runat="server" Text='<%# Eval("Days120", "{0:P}") %>'
                                Visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("Days120"))) %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="Days150" HeaderText="Days150"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Days150" UniqueName="Days150" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn AutoPostBackOnFilter="true" HeaderText="150-D" AllowFiltering="False"
                        UniqueName="150-D">
                        <ItemStyle Wrap="False"></ItemStyle>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDays150" runat="server" Text='<%# Eval("Days150", "{0:P}") %>'
                                Visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("Days150"))) %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="Days180" HeaderText="Days180"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Days180" UniqueName="Days180" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn AutoPostBackOnFilter="true" HeaderText="180-D" AllowFiltering="False"
                        UniqueName="180-D">
                        <ItemStyle Wrap="False"></ItemStyle>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDays180" runat="server" Text='<%# Eval("Days180", "{0:P}") %>'
                                Visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("Days180"))) %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="Days270" HeaderText="Days270"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Days270" UniqueName="Days270" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn AutoPostBackOnFilter="true" HeaderText="270-D" AllowFiltering="False"
                        UniqueName="270-D">
                        <ItemStyle Wrap="False"></ItemStyle>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDays270" runat="server" Text='<%# Eval("Days270", "{0:P}") %>'
                                Visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("Days270"))) %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="Years1" HeaderText="Years1"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Years1" UniqueName="Years1" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn AutoPostBackOnFilter="true" HeaderText="Y-1" AllowFiltering="False"
                        UniqueName="Y-1">
                        <ItemStyle Wrap="False"></ItemStyle>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkYears1" runat="server" Text='<%# Eval("Years1", "{0:P}") %>'
                                Visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("Years1"))) %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="Years2" HeaderText="Years2"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Years2" UniqueName="Years2" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn AutoPostBackOnFilter="true" HeaderText="Y-2" AllowFiltering="False"
                        UniqueName="Y-2">
                        <ItemStyle Wrap="False"></ItemStyle>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkYears2" runat="server" Text='<%# Eval("Years2", "{0:P}") %>'
                                Visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("Years2"))) %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="Years3" HeaderText="Years3"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Years3" UniqueName="Years3" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn AutoPostBackOnFilter="true" HeaderText="Y-3" AllowFiltering="False"
                        UniqueName="Y-3">
                        <ItemStyle Wrap="False"></ItemStyle>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkYears3" runat="server" Text='<%# Eval("Years3", "{0:P}") %>'
                                Visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("Years3"))) %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="Years4" HeaderText="Years4"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Years4" UniqueName="Years4" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn AutoPostBackOnFilter="true" HeaderText="Y-4" AllowFiltering="False"
                        UniqueName="Y-4">
                        <ItemStyle Wrap="False"></ItemStyle>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkYears4" runat="server" Text='<%# Eval("Years4", "{0:P}") %>'
                                Visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("Years4"))) %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="Years5" HeaderText="Years5"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Years5" UniqueName="Years5" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn AutoPostBackOnFilter="true" HeaderText="Y-5" AllowFiltering="False"
                        UniqueName="Y-5">
                        <ItemStyle Wrap="False"></ItemStyle>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkYears5" runat="server" Text='<%# Eval("Years5", "{0:P}") %>'
                                Visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("Years5"))) %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="SMAHoldingLimit" HeaderText="SMAHoldingLimit"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="SMAHoldingLimit" UniqueName="SMAHoldingLimit"
                        Display="false">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <telerik:RadGrid OnNeedDataSource="PresentationDetailGridNeedDataSource" ID="PresentationDetailGrid"
            runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
            OnItemDataBound="PresentationDetailGridItemDataBound" Visible="False" AllowSorting="True"
            AllowMultiRowSelection="False" AllowPaging="False" GridLines="None" AllowAutomaticDeletes="True"
            AllowFilteringByColumn="False" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
            EnableViewState="true" ShowFooter="false">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                Name="PendingOrders" TableLayout="Fixed">
                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                <Columns>
                    <telerik:GridBoundColumn ReadOnly="true" DataField="ProductID" UniqueName="ProductID"
                        Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" DataField="TDAccountCID" UniqueName="TDAccountCID"
                        Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="BrokerID" HeaderText="BrokerID"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="BrokerID" UniqueName="BrokerID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="BankID" ReadOnly="true" HeaderText="BankID"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="BankID" UniqueName="BankID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="Rating" ReadOnly="true" HeaderText="Rating"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Rating" UniqueName="Rating" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="100px" ReadOnly="true" SortExpression="BrokerName"
                        HeaderText="Broker" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BrokerName" UniqueName="BrokerName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="200px" ReadOnly="true" SortExpression="BankName"
                        HeaderText="Bank" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BankName" UniqueName="BankName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="120px" SortExpression="HoldingLimit"
                        ReadOnly="true" HeaderText="Holding Limit" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="HoldingLimit"
                        UniqueName="HoldingLimit" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="120px" SortExpression="TotalHolding"
                        ReadOnly="true" HeaderText="Current Holdings" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TotalHolding"
                        UniqueName="TotalHolding" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="120px" SortExpression="AvailableFunds"
                        ReadOnly="true" HeaderText="Balance" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AvailableFunds"
                        UniqueName="AvailableFunds" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="Percentage" ReadOnly="true" HeaderText="Percentage"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Percentage" UniqueName="Percentage"
                        Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="100px" SortExpression="Percentage" ReadOnly="true"
                        HeaderText="Rate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Percentage" UniqueName="Rate"
                        DataFormatString="{0:P}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="100px" SortExpression="Duration" ReadOnly="true"
                        HeaderText="Duration" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Duration" UniqueName="Duration">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="150px" SortExpression="Min" ReadOnly="true"
                        HeaderText="Min" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Min" UniqueName="Min"
                        DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="150px" SortExpression="Max" ReadOnly="true"
                        HeaderText="Max" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Max" UniqueName="Max"
                        DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderStyle-Width="200px" HeaderText="Amount" AllowFiltering="False">
                        <ItemTemplate>
                            $&nbsp;<telerik:RadNumericTextBox ID="txtAmount" runat="server">
                            </telerik:RadNumericTextBox><asp:RangeValidator ID="RangeValidator1" runat="server"
                                ErrorMessage="*Invalid Amount" ControlToValidate="txtAmount" MinimumValue='<%# Eval("Min") %>'
                                MaximumValue='<%# Convert.ToDecimal(Eval("Max"))==0?decimal.MaxValue: Eval("Max")%>'
                                Type="Currency" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"></asp:RangeValidator>
                            <asp:CompareValidator runat="server" ID="cmpValidateSMABalance" ValueToCompare='<%# Math.Round(Convert.ToDecimal(Eval("AvailableFunds")is DBNull?0:Eval("AvailableFunds")),2) %>'
                                ControlToValidate="txtAmount" Enabled="False" Display="Dynamic" SetFocusOnError="True"
                                ForeColor="Red" Type="Double" ErrorMessage="*Amount exceeds holding balance" Operator="LessThanEqual"></asp:CompareValidator>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <asp:HiddenField ID="hfClientCID" runat="server" />
        <asp:HiddenField ID="hfClientID" runat="server" />
        <asp:HiddenField ID="hfIsAdmin" runat="server" />
        <asp:HiddenField ID="hfIsAdminMenu" runat="server" />
        <asp:HiddenField ID="hidBackButton" runat="server" />
        <asp:HiddenField ID="hidChkCount" runat="server" />
        <asp:HiddenField ID="hidMaxValue" runat="server" />
        <asp:HiddenField ID="hfIsSMA" runat="server" />
        <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
        <asp:ModalPopupExtender ID="popupSearch" runat="server" TargetControlID="btnShowPopup"
            PopupControlID="pnlpopup" PopupDragHandleControlID="PopupHeader" Drag="true"
            BackgroundCssClass="ModalPopupBG" BehaviorID="ModalBehaviour">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlpopup" runat="server" BackColor="White" Style="display: none">
            <div class="popup_Container">
                <div class="popup_Titlebar" id="PopupHeader">
                    <div class="TitlebarLeft">
                        Select Account
                    </div>
                    <div class="TitlebarRight" onclick="HideModalPopup();">
                    </div>
                </div>
                <div class="PopupBody">
                    <uc2:SearchNSelectClientAccount ID="searchAccountControl" runat="server" />
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
