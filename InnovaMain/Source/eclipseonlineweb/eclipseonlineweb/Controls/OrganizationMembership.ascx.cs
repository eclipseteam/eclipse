﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using Oritax.TaxSimp.CM.Organization.Data;

namespace eclipseonlineweb.Controls
{
    public partial class OrganizationMembership : System.Web.UI.UserControl
    {
        private ICMBroker UMABroker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }
        public Action<DataSet> SaveUnit { get; set; }
        public Action ValidationFailed { get; set; }
        public Action Canceled { get; set; }
        public Action<MembershipDS> Save { get; set; }
        public new string ClientID { get; set; }
        public Guid Clid { get; set; }
        public Guid Csid { get; set; }
        public OrganizationType OrgType { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetLabelCaption();
            }
            if (OrgType == OrganizationType.Accountant)
            {
                btnCancel.Visible = false;
            }
        }
       
        private void EnablePenal()
        {
            pnlGrid.Visible = true;
            pnlUpdate.Visible = true;
        }
        private void SetLabelCaption()
        {
            switch (OrgType)
            {
                case OrganizationType.DealerGroup:
                    lblSearch.Text = "Enter name of IFA";
                    break;
                case OrganizationType.IFA:
                    lblSearch.Text = "Enter name of Adviser";
                    break;
                case OrganizationType.Adviser:
                    lblSearch.Text = "Enter name of Client";
                    break;
            }
            btnSave.Visible = false;
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (Canceled != null)
            {
                Canceled();
            }
        }

        public void ResetControls()
        {
            txtSearch.Text = string.Empty;
            gd_Membership.DataSource = null;
            gd_Membership.DataBind();
            
           
        }

        protected void gd_Membership_InsertCommand(object source, GridCommandEventArgs e)
        { }

        protected void gd_Membership_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            EnablePenal();
            Search(bool.Parse(hf_searchPattren.Value));
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hf_searchPattren.Value = false.ToString();
            gd_Membership.Rebind();
            pnlGrid.Visible = true;
            if (ValidationFailed != null)
            {
                ValidationFailed();
            }
        }

        private void LoadMembership(OrganizationType type, bool isShowAll, WebCommands cmd)
        {
            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            DataTable entityTable = new DataTable();

            if (type == OrganizationType.ConsoleClient)
            {
                OrganisationListingDS organisationListingDS = new OrganisationListingDS();
                organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.ClientsSearch;
                organisationListingDS.ClientNameSearchFilter = this.txtSearch.Text;
                org.GetData(organisationListingDS);

                var entityView = new DataView(organisationListingDS.Tables[OrganisationListingDS.TableName]) { RowFilter = "IsClient='true'", Sort = "ENTITYNAME_FIELD ASC" };
                entityTable = entityView.ToTable();
            }

            if (type == OrganizationType.IFA)
            {
                OrganisationListingDS organisationListingDS = new OrganisationListingDS();
                organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.IFA;
                org.GetData(organisationListingDS);
                var entityView = new DataView(organisationListingDS.Tables[OrganisationListingDS.TableName]) { RowFilter = "ENTITYNAME_FIELD LIKE '%" + this.txtSearch.Text + "%'", Sort = "ENTITYNAME_FIELD ASC" };
                entityTable = entityView.ToTable();
            }

            if (type == OrganizationType.Adviser)
            {
                OrganisationListingDS organisationListingDS = new OrganisationListingDS();
                organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.AdvisersBasicList;
                org.GetData(organisationListingDS);
                var entityView = new DataView(organisationListingDS.Tables[OrganisationListingDS.ADVISERLISTINGTABLE]) { RowFilter = "ENTITYNAME_FIELD LIKE '%" + this.txtSearch.Text + "%'", Sort = "ENTITYNAME_FIELD ASC" };
                entityTable = entityView.ToTable();
            }

            gd_Membership.DataSource = entityTable;
            pnlGrid.Visible = true;
            if (entityTable.Rows.Count > 0)
                btnSave.Visible = true;
            else
                btnSave.Visible = false;

            UMABroker.ReleaseBrokerManagedComponent(org);
        }

        private void Search(bool showall)
        {

            hf_searchPattren.Value = showall.ToString();
            switch (OrgType)
            {
                case OrganizationType.DealerGroup:
                    LoadMembership(OrganizationType.IFA, showall, WebCommands.GetOrganizationUnitsByType);
                    break;
                case OrganizationType.IFA:
                    LoadMembership(OrganizationType.Adviser, showall, WebCommands.GetOrganizationUnitsByType);
                    break;
                case OrganizationType.Adviser:
                case OrganizationType.Accountant:
                    LoadMembership(OrganizationType.ConsoleClient, showall, WebCommands.GetAllClientDetails);
                    break;
            }

        }

        protected void gd_Membership_OnPageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            if (ValidationFailed != null)
                ValidationFailed();

            EnablePenal();
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            MembershipDS membershipDs = new MembershipDS();
            membershipDs.CommandType = DatasetCommandTypes.Add;
            foreach (GridDataItem item in gd_Membership.Items)
            {
                var chkMemeber = (item.FindControl("chkmember") as CheckBox);
                if (chkMemeber.Checked)
                {
                    DataRow dr = membershipDs.membershipTable.NewRow();
                    dr[membershipDs.membershipTable.CID] = new Guid(item.Cells[2].Text);
                    dr[membershipDs.membershipTable.CLID] = new Guid(item.Cells[3].Text);
                    dr[membershipDs.membershipTable.CSID] = new Guid(item.Cells[4].Text);
                    dr[membershipDs.membershipTable.NAME] = item.Cells[7].Text;
                    membershipDs.membershipTable.Rows.Add(dr);
                }
            }
            if (Save != null)
                Save(membershipDs);
            gd_Membership.DataSource = null;
            gd_Membership.Rebind();
        }

        protected void gd_Membership_OnSortCommand(object sender, GridSortCommandEventArgs e)
        {
            if (ValidationFailed != null)
                ValidationFailed();

            EnablePenal();
        }
        
    }
}