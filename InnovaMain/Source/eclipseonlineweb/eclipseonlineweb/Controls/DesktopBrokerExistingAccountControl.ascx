﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DesktopBrokerExistingAccountControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.DesktopBrokerExistingAccountControl" %>
<div class="popup_Container">
    <div class="popup_Titlebar" id="PopupHeader">
        <div class="TitlebarLeft">
            <asp:Label ID="Title" runat="server" Text="ASX Broker Account Details"></asp:Label>
            <asp:HiddenField ID="hfCID" runat="server" />
            <asp:HiddenField ID="hfCLID" runat="server" />
            <asp:HiddenField ID="hfCSID" runat="server" />
        </div>
    </div>
    <div class="popup_Body">
        <div runat="server" id="dvSearchtxt">
            <table>
                <tr>
                    <td>
                        <strong>Search existing Account Name</strong>
                    </td>
                    <td>
                        <telerik:RadTextBox runat="server" Width="488px" ID="txtSearchAccName">
                        </telerik:RadTextBox>
                    </td>
                    <td>
                        <telerik:RadButton runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_OnClick">
                        </telerik:RadButton>
                    </td>
                </tr>
            </table>
        </div>
        <div runat="server" id="dvExistingAccGrid" style="width: 725px">
            <telerik:RadGrid ID="gvDesktopBroker" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                PageSize="4" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                AllowFilteringByColumn="True" GridLines="None" EnableViewState="true" ShowFooter="false"
                OnNeedDataSource="gvDesktopBroker_OnNeedDataSource">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" Name="IDDocuments" TableLayout="Fixed">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <Columns>
                        <telerik:GridBoundColumn DataField="Cid" UniqueName="Cid" Display="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CLid" UniqueName="CLid" Display="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CSid" UniqueName="CSid" Display="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn UniqueName="Select" HeaderStyle-Width="45px" HeaderText="Select"
                            AllowFiltering="False">
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBox" runat="server" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn SortExpression="AccountName" HeaderText="Account Name" CurrentFilterFunction="Contains"
                            AllowFiltering="True" ShowFilterIcon="True" DataField="AccountName" UniqueName="AccountName"
                            FilterControlWidth="60%" HeaderStyle-Width="55%" />
                        <telerik:GridBoundColumn SortExpression="AccountNo" HeaderText="Account No" CurrentFilterFunction="Contains"
                            FilterControlWidth="60%" AllowFiltering="True" ShowFilterIcon="True" DataField="AccountNo"
                            UniqueName="AccountNo" HeaderStyle-Width="13%" />
                        <telerik:GridBoundColumn SortExpression="AccountDesignation" HeaderText="Account Designation"
                            AllowFiltering="True" CurrentFilterFunction="Contains" ShowFilterIcon="True"
                            DataField="AccountDesignation" UniqueName="AccountDesignation" HeaderStyle-Width="20%" />
                        <telerik:GridBoundColumn SortExpression="HINNumber" HeaderText="HIN No" CurrentFilterFunction="Contains"
                            AllowFiltering="True" ShowFilterIcon="True" DataField="HINNumber" UniqueName="HINNumber"
                            FilterControlWidth="45%" HeaderStyle-Width="12%" />
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </div>
        <div>
            <asp:Label runat="server" ID="lblmessages" Font-Size="Medium" Font-Bold="True" ForeColor="Red" />
            <br />
            <br />
        </div>
        <div runat="server" id="dvSelectedGrid" visible="false">
            <div>
                <strong>Included ASX Account</strong>
            </div>
            <div id="dvDesktopExistingAccGrid" style="width: 725px;" runat="server">
                <telerik:RadGrid ID="gvExistingDesktopBroker" runat="server" ShowStatusBar="true"
                    AutoGenerateColumns="False" PageSize="4" AllowSorting="True" AllowMultiRowSelection="False"
                    AllowPaging="True" AllowFilteringByColumn="True" GridLines="None" EnableViewState="true"
                    ShowFooter="false" OnNeedDataSource="gvExistingDesktopBroker_OnNeedDataSource">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AllowMultiColumnSorting="True" Width="100%" Name="IDDocuments" TableLayout="Fixed">
                        <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridBoundColumn DataField="Cid" UniqueName="Cid" Display="False">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CLid" UniqueName="CLid" Display="False">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CSid" UniqueName="CSid" Display="False">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="AccountName" HeaderText="Account Name" CurrentFilterFunction="Contains"
                                AllowFiltering="True" ShowFilterIcon="True" DataField="AccountName" UniqueName="AccountName"
                                FilterControlWidth="60%" HeaderStyle-Width="55%" />
                            <telerik:GridBoundColumn SortExpression="AccountNo" HeaderText="Account No" CurrentFilterFunction="Contains"
                                AllowFiltering="True" ShowFilterIcon="True" DataField="AccountNo" UniqueName="AccountNo"
                                HeaderStyle-Width="13%" />
                            <telerik:GridBoundColumn SortExpression="AccountDesignation" HeaderText="Account Designation"
                                AllowFiltering="True" CurrentFilterFunction="Contains" ShowFilterIcon="True"
                                DataField="AccountDesignation" UniqueName="AccountDesignation" HeaderStyle-Width="20%" />
                            <telerik:GridBoundColumn SortExpression="HINNumber" HeaderText="HIN No" CurrentFilterFunction="Contains"
                                AllowFiltering="True" ShowFilterIcon="True" DataField="HINNumber" UniqueName="HINNumber"
                                FilterControlWidth="45%" HeaderStyle-Width="12%" />
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </div>
        </div>
        <br />
        <div align="right" style="width: 725px">
            <telerik:RadButton runat="server" ID="btnUpdate" Text="Save" OnClick="btnUpdate_Onclick">
            </telerik:RadButton>
            <telerik:RadButton runat="server" ID="btnCancel" Text="Cancel" OnClick="btnCancel_OnClick">
            </telerik:RadButton>
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            

        </div>
    </div>
</div>
