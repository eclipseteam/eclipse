﻿using System.Data;
using Oritax.TaxSimp.CM.Group;
using System;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;

namespace eclipseonlineweb.Controls
{
    public partial class IFATrustControl : System.Web.UI.UserControl
    {
        private ICMBroker Broker
        {

            get { return (Page as UMABasePage).UMABroker; }
        }

        public Action<Guid> Saved { get; set; }

        public Action<Guid, DataSet> SaveData { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                LoadControls();
            }
        }

        private void LoadControls()
        {
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            var status = new StatusDS { TypeFilter = "Organization" };
            org.GetData(status);
            ddlStatusFlag.Items.Clear();
            ddlStatusFlag.DataSource = status.Tables[StatusDS.TABLENAME];
            ddlStatusFlag.DataTextField = StatusDS.NAME;
            ddlStatusFlag.DataValueField = StatusDS.NAME;
            ddlStatusFlag.DataBind();
        }

        public IFADS GetEntity()
        {

            IFADS ifaDs = new IFADS();
            DataRow dr = ifaDs.Tables[ifaDs.ifaTable.TableName].NewRow();
            dr[ifaDs.ifaTable.TRADINGNAME] = txtTradingName.Text;
            dr[ifaDs.ifaTable.NAME] = txtTradingName.Text;
            dr[ifaDs.ifaTable.EMAIL] = txtEmail.GetEntity();
            dr[ifaDs.ifaTable.STATUS] = ddlStatusFlag.SelectedValue;
            dr[ifaDs.ifaTable.IFATYPE] = hf_IfaType.Value;
            if (chkSameAsTrading.Checked)
            {
                dr[ifaDs.ifaTable.LEGALNAME] = txtTradingName.Text;
            }
            else
            {
                dr[ifaDs.ifaTable.LEGALNAME] = txtLegalName.Text;
            }
            dr[ifaDs.ifaTable.ACCOUNTDESIGNATION] = txtAccountDesignation.Text;
            dr[ifaDs.ifaTable.FACSIMILE] = FacsimileNo.Number;
            dr[ifaDs.ifaTable.FACSIMILECOUNTRYCODE] = FacsimileNo.CountryCode;
            dr[ifaDs.ifaTable.FACSIMILECITYCODE] = FacsimileNo.CityCode;
            dr[ifaDs.ifaTable.PHONENUBER] = ph_Phone.Number;
            dr[ifaDs.ifaTable.PHONENUMBERCOUNTRYCODE] = ph_Phone.CountryCode;
            dr[ifaDs.ifaTable.PHONENUMBERCITYCODE] = ph_Phone.CityCode;
            dr[ifaDs.ifaTable.SKCODE] = txtSkCode.Text;
            dr[ifaDs.ifaTable.WEBSITEADDRESS] = txtWebsiteAddress.Text;
            dr[ifaDs.ifaTable.ABN] = abn_ABN.GetEntity();
            dr[ifaDs.ifaTable.ACN] = acn_ACN.GetEntity();
            if (dtLastAudited.SelectedDate != null)
                dr[ifaDs.ifaTable.LASTAUDITEDDATE] = dtLastAudited.SelectedDate.Value.ToShortDateString();
            dr[ifaDs.ifaTable.PREVIOUSPRACTICE] = txtPreviousPractice.Text;
            dr[ifaDs.ifaTable.FUM] = txtFum.Text;
            dr[ifaDs.ifaTable.TURNOVER] = txtTurnOver.Text;
            ifaDs.Tables[ifaDs.ifaTable.TableName].Rows.Add(dr);
            return ifaDs;
        }

        public void SetEntity(Guid CID, IFAEntityType IfaType)
        {
            ClearEntity();
            hf_IfaType.Value = IfaType.ToString();
            if (CID != Guid.Empty)
            {
                IFADS ds = GetIfaDetails(CID);
                hfCID.Value = CID.ToString();
                DataRow dr = ds.Tables[ds.ifaTable.TableName].Rows[0];
                txtClientID.Text = dr[ds.ifaTable.CLIENTID].ToString();
                txtTradingName.Text = dr[ds.ifaTable.TRADINGNAME].ToString();
                txtLegalName.Text = dr[ds.ifaTable.LEGALNAME].ToString();
                txtEmail.SetEntity(dr[ds.ifaTable.EMAIL].ToString());
                if (txtTradingName.Text == txtLegalName.Text)
                {
                    chkSameAsTrading.Checked = true;
                    txtLegalName.Enabled = false;
                }
                else
                {
                    chkSameAsTrading.Checked = false;
                    txtLegalName.Enabled = true;
                }
                ddlStatusFlag.SelectedValue = dr[ds.ifaTable.STATUS].ToString();
                txtAccountDesignation.Text = dr[ds.ifaTable.ACCOUNTDESIGNATION].ToString();
                FacsimileNo.Number = dr[ds.ifaTable.FACSIMILE].ToString();
                FacsimileNo.CountryCode = dr[ds.ifaTable.FACSIMILECOUNTRYCODE].ToString();
                FacsimileNo.CityCode = dr[ds.ifaTable.FACSIMILECITYCODE].ToString();
                ph_Phone.Number = dr[ds.ifaTable.PHONENUBER].ToString();
                ph_Phone.CountryCode = dr[ds.ifaTable.PHONENUMBERCOUNTRYCODE].ToString();
                ph_Phone.CityCode = dr[ds.ifaTable.PHONENUMBERCITYCODE].ToString();
                txtSkCode.Text = dr[ds.ifaTable.SKCODE].ToString();
                txtWebsiteAddress.Text = dr[ds.ifaTable.WEBSITEADDRESS].ToString();
                abn_ABN.SetEntity(dr[ds.ifaTable.ABN].ToString());
                acn_ACN.SetEntity(dr[ds.ifaTable.ACN].ToString());
                if (!string.IsNullOrEmpty(dr[ds.ifaTable.LASTAUDITEDDATE].ToString()))
                    dtLastAudited.SelectedDate = Convert.ToDateTime(dr[ds.ifaTable.LASTAUDITEDDATE].ToString());
                txtPreviousPractice.Text = dr[ds.ifaTable.PREVIOUSPRACTICE].ToString();
                txtFum.Text = dr[ds.ifaTable.FUM].ToString();
                txtTurnOver.Text = dr[ds.ifaTable.TURNOVER].ToString();
            }
        }

        private IFADS GetIfaDetails(Guid cid)
        {
            var ifaDs = new IFADS { CommandType = DatasetCommandTypes.Details };
            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
                clientData.GetData(ifaDs);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }
            return ifaDs;
        }

        public void ClearEntity()
        {
            hfCID.Value = Guid.Empty.ToString();
            hf_IfaType.Value = "";
            txtClientID.Text = "";
            txtTradingName.Text = "";
            txtLegalName.Text = "";
            txtAccountDesignation.Text = "";
            FacsimileNo.ClearEntity();
            ph_Phone.ClearEntity();
            txtSkCode.Text = "";
            txtWebsiteAddress.Text = "";
            abn_ABN.ClearEntity();
            acn_ACN.ClearEntity();
            dtLastAudited.Clear();
            dtLastAudited.DateInput.Clear();
            txtPreviousPractice.Text = "";
            txtFum.Text = "";
            txtTurnOver.Text = "";
            txtEmail.ClearEntity();
        }

        public bool Validate()
        {
            return Validate(true);
        }

        public bool Validate(bool ShowTabError)
        {
            bool result = true;

            result = abn_ABN.Validate() & acn_ACN.Validate() & FacsimileNo.Validate() & ph_Phone.Validate() & txtEmail.Validate();
            return result;
        }
        
        protected void btnSaveIfaTrust_Click(object sender, System.EventArgs e)
        {
            if (Validate())
            {
                IFADS ifaDs = new IFADS();
                var cid = (string.IsNullOrEmpty(hfCID.Value)) ? Guid.Empty : new Guid(hfCID.Value);

                if (cid == Guid.Empty)
                {
                    ifaDs.CommandType = DatasetCommandTypes.Add;
                }
                else
                {
                    ifaDs.CommandType = DatasetCommandTypes.Update;
                }
                if (Broker != null)
                {
                    ifaDs.ifaTable.Merge(GetEntity().ifaTable);
                    if (cid == Guid.Empty)
                    {
                        var unit = new OrganizationUnit
                            {
                                OrganizationStatus = "Active",
                                Name = txtTradingName.Text,
                                Type = ((int)OrganizationType.IFA).ToString(),
                                CurrentUser = (Page as UMABasePage).GetCurrentUser()
                            };
                        ifaDs.Unit = unit;
                        ifaDs.Command = (int)WebCommands.AddNewOrganizationUnit;
                    }
                    if (SaveData != null)
                    {
                        SaveData(cid, ifaDs);
                        if (ifaDs.CommandType == DatasetCommandTypes.Add)
                        {
                            cid = ifaDs.Unit.Cid;

                            hfCID.Value = cid.ToString();
                            if (Saved != null)
                                Saved(cid);
                        }
                        //Enable Disable Legal control
                        txtLegalName.Enabled = !chkSameAsTrading.Checked;
                    }
                    else
                    {
                        throw new Exception("Broker Not Found");
                    }
                }
            }
        }
    }
}