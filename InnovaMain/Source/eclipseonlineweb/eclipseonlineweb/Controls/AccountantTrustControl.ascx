﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountantTrustControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.AccountantTrustControl" %>
<%@ Register Src="PhoneNumberControl.ascx" TagName="PhoneNumberControl" TagPrefix="uc1" %>
<%@ Register Src="ABN_InputControl.ascx" TagName="ABN_InputControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="uc2" TagName="ACN_InputControl" Src="ACN_InputControl.ascx" %>
<script type="text/javascript">
    function SetTradingName(sender, args) {
        var TradingName = document.getElementById("<%:txtTradingName.ClientID %>").value;
        var chkthp = document.getElementById('<%: chkSameAsTrading.ClientID %>');
        var txtSgcPercentSalary = $find("<%:txtLegalName.ClientID %>");
        if (TradingName != "" && args.get_checked()) {
            txtSgcPercentSalary.set_value(TradingName);            
            txtSgcPercentSalary.disable();
        }
        else {
            txtSgcPercentSalary.enable();
        }
    }
    function OnValueChangingTradingName(sender, args) {
        var TradingName = document.getElementById("<%:txtTradingName.ClientID %>").value;
        var txtSgcPercentSalary = $find("<%:txtLegalName.ClientID %>");
        var button = $find("<%: chkSameAsTrading.ClientID%>");
        if (button.get_checked() && TradingName != "") {
            txtSgcPercentSalary.set_value(TradingName);
            txtSgcPercentSalary.disable();
        }
        else {
            txtSgcPercentSalary.enable();
        }
    }
</script>
<fieldset style="width: 98%">
    <div style="text-align: left; float: none; border: 1px; background-color: White;
        height: 25px; position: relative; vertical-align: middle; margin: 0; padding: 0;">
        <telerik:RadButton ID="btnSaveAccountantTrust" Text="Add Client Addresses" ToolTip="Save Changes"
            ValidationGroup="VGAccountantTrustControl" OnClick="btnSaveAccountantTrust_Click"
            runat="server" Width="32px" Height="32px" BorderStyle="None">
            <Image ImageUrl="~/images/Save-Icon.png"></Image>
        </telerik:RadButton>
    </div>
</fieldset>
<br />
<table id="Table1" runat="server" width="100%" style="background-color: White;">
    <tr>
        <td style="width: 42%;">
            <table>
                <tr>
                    <td style="width: 46%;">
                        &nbsp; <span class="riLabel">Status Flag</span>
                        <asp:HiddenField ID="hfCID" runat="server" />
                        <asp:HiddenField ID="hf_AccountantType" runat="server" />
                    </td>
                    <td style="text-align: left;">
                        &nbsp;&nbsp;
                        <telerik:RadComboBox ID="ddlStatusFlag" runat="server" Width="196px">
                        </telerik:RadComboBox>
                    </td>
                </tr>
            </table>
        </td>
        <td style="width: 80%;">
            <table>
                <tr>
                    <td style="width: 41%; text-align: left;">
                        &nbsp; <span class="riLabel">Client ID</span>
                    </td>
                    <td align="left">
                        <telerik:RadTextBox runat="server" ID="txtClientID" ReadOnly="true" Width="200px">
                        </telerik:RadTextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<fieldset>
    <asp:Panel runat="server" ID="pnlAccountantTrustDetail">
        <table width="100%">
            <tr>
                <td>
                    <span class="riLabel">Trading Name *</span>
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtTradingName" Width="196px">
                        <ClientEvents OnBlur="OnValueChangingTradingName" />
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RFVTxtTradingName" ControlToValidate="txtTradingName"
                        ErrorMessage="*" ForeColor="Red" ValidationGroup="VGAccountantTrustControl"> </asp:RequiredFieldValidator>
                </td>
                <td>
                    <span class="riLabel">Legal Name *</span>
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtLegalName" Width="200px">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RFVLegalName" ControlToValidate="txtLegalName"
                        ErrorMessage="*" ForeColor="Red" ValidationGroup="VGAccountantTrustControl"> </asp:RequiredFieldValidator>
                    <telerik:RadButton ToggleType="CheckBox" ButtonType="ToggleButton" runat="server"
                        OnClientCheckedChanged="SetTradingName" ID="chkSameAsTrading" Text="Same as Trading Name"
                        AutoPostBack="False" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Account Designation</span>
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtAccountDesignation" Width="196px">
                    </telerik:RadTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Facsimile</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="FacsimileNo" runat="server" />
                </td>
                <td>
                    <span class="riLabel">Phone Number</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="ph_Phone" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Website Address</span>
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="txtWebsiteAddress" Width="196px">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <span class="riLabel">ABN *</span>
                </td>
                <td>
                    <uc3:ABN_InputControl ID="abn_ABN" runat="server" InvalidMessage="Invalid ABN (e.g. 53 004 085 616)"
                        IsRequired="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">ACN</span>
                </td>
                <td>
                    <uc2:ACN_InputControl ID="acn_ACN" runat="server" InvalidMessage="Invalid ACN (e.g. 010 499 966)" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center;">
                    <asp:Label runat="server" ID="txtMessage" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
</fieldset>
