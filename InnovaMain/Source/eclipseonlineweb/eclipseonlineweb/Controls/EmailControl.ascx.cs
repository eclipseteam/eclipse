﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.Extensions;
namespace eclipseonlineweb.Controls
{
    public partial class EmailControl : UserControl
    {
        public bool Mandatory { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public bool Validate()
        {
            string email = GetEntity();
            bool result = false;
            if (!Mandatory && email.Length == 0)
            {
                result= true;
            }
            else
            {
                result = txtEmail.ValidateEmail(Mandatory);
            }
          
            Msg.Visible = !result;
           

            return result;
        }


        public string GetEntity()
        {
            return txtEmail.Text.Trim();
        }

        public void SetEntity(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {

                txtEmail.Text = value;

            }

        }

        public void ClearEntity()
        {
            txtEmail.Text = string.Empty;
            Msg.Visible = false;

        }

    }
}