﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BuyAtCallRates.ascx.cs"
    Inherits="eclipseonlineweb.Controls.BuyAtCallRates" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc2" TagName="SearchNSelectClientAccount" Src="~/Controls/SearchNSelectClientAccount.ascx" %>
<script language="javascript" type="text/javascript">

    function ShowModalPopup() {
        window.$find("ModalBehaviour").show();
    }

    function HideModalPopup() {
        window.$find("ModalBehaviour").hide();
    }

    function ShowAlert(sender, args) {
        var isOK = true;
        var broker = document.getElementById("<%:hfIsAlert.ClientID %>").value;
        var atCallAccStatus = document.getElementById("<%:hfAtCallAccStatus.ClientID %>").value;
        if (broker != "" && atCallAccStatus == "") {
            var msg = "This order may be delayed as we do not have an account with " + broker + " opened yet.\nDo you want to place this order anyway?\nPlease contact e-Clipse Head Office if assistance is required.";
            var x = confirm(msg);
            if (x) {
                isOK = true;
            }
            else {
                isOK = false;
            }
        }
        else if (atCallAccStatus != "active" && atCallAccStatus != "") {
            var msg = "This order may be delayed as we do not have an account with " + broker + " activated yet.\nDo you want to place this order anyway?\nPlease contact e-Clipse Head Office if assistance is required.";
            var x = confirm(msg);
            if (x) {
                isOK = true;
            }
            else {
                isOK = false;
            }
        }
        sender.set_autoPostBack(isOK);
    }
</script>
<style type="text/css">
    .rgCaption
    {
        color: blue;
        font: normal 9pt Arial;
        text-align: center !important;
    }
    .ToolTipContent
    {
        background: yellow;
        background-color: yellow !important;
        color: Black !important;
        font-size: 0.8em !important;
    }
</style>
<asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
    <Triggers>
        <asp:PostBackTrigger ControlID="btnCutOff" />
    </Triggers>
    <ContentTemplate>
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="26%" runat="server" id="tdButtons">
                        <telerik:RadButton runat="server" ID="btnBack" ToolTip="Back to TD order" OnClick="btnBack_OnClick"
                            Visible="false">
                            <ContentTemplate>
                                <img src="../images/window_previous.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Back</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ID="imgItems" ToolTip="Enter amount(s)" OnClick="imgItems_OnClick">
                            <ContentTemplate>
                                <img src="../images/book_next.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Next</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ID="btnCutOff" ToolTip="Cut Of Time" OnClick="btnCutOff_OnClick">
                            <ContentTemplate>
                                <img alt="" src="../images/cut-off-time_106.png" class="btnImageWithText" />
                                <span class="riLabel">Cut-Off Times</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ID="btnSave" ToolTip="Place Order" OnClick="btnSave_OnClick"
                            OnClientClicked="ShowAlert" Visible="false">
                            <ContentTemplate>
                                <img src="../images/cart_add.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Place Order</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </td>
                    <td style="width: 74%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <telerik:RadButton runat="server" ID="btnSearch" Visible="False" Text="Select Client Account"
                            OnClick="btnSearch_OnClick">
                        </telerik:RadButton>
                        &nbsp;<uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <br />
        <asp:HiddenField ID="hfClientType" runat="server" />
        <asp:HiddenField ID="hfIsAlert" runat="server" />
        <asp:HiddenField ID="hfIds" runat="server" />
        <asp:HiddenField ID="hfAtCallAccStatus" runat="server" />
        <asp:Label ID="lblNotify" runat="server" ForeColor="Blue" Visible="false"></asp:Label>
        <div id="MainView" style="background-color: white" runat="server">
            <table style="width: 100%; height: 50px" runat="server" id="showDetails" visible="false">
                <tr>
                    <td style="width: 120px">
                        Service Type:
                    </td>
                    <td style="width: 230px">
                        Cash Account:
                    </td>
                    <td>
                        Available Funds:
                    </td>
                    <td id="tdCashBalanceHead" runat="server">
                        = Current Cash Balance
                    </td>
                    <td id="tdMinCashHead" runat="server">
                        - Min Cash Holdings
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadComboBox ID="cmbServiceType" AutoPostBack="true" Width="110px" runat="server"
                            OnSelectedIndexChanged="cmbServiceType_OnSelectedIndexChanged">
                        </telerik:RadComboBox>
                    </td>
                    <td>
                        <telerik:RadComboBox ID="cmbFromCashAccount" AutoPostBack="true" Width="220px" runat="server"
                            OnSelectedIndexChanged="cmbFromCashAccount_OnSelectedIndexChanged">
                        </telerik:RadComboBox>
                    </td>
                    <td>
                        <asp:Label ID="lblFromAvailableFunds" runat="server" Text=""></asp:Label>
                    </td>
                    <td id="tdCashBalanceValue" runat="server">
                        <asp:Label ID="lblCashBalance" runat="server" Text=""></asp:Label>
                    </td>
                    <td id="tdMinCashValue" runat="server">
                        <asp:Label ID="lblMinCash" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <telerik:RadGrid OnNeedDataSource="PresentationGrid_OnNeedDataSource" ID="PresentationGrid"
            runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
            AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="false" GridLines="None"
            AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
            AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" Visible="false"
            OnItemDataBound="PresentationGrid_OnItemDataBound">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                Name="AtCallRates" TableLayout="Fixed">
                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                <Columns>
                    <telerik:GridBoundColumn ReadOnly="true" UniqueName="AtCallAccStatus" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" UniqueName="ProductID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" UniqueName="AtCallAccountCID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderStyle-Width="30px" UniqueName="AlertMsg" ReadOnly="True"
                        AllowFiltering="false">
                        <ItemTemplate>
                            <asp:Image runat="server" ID="ImgAlertImage" ImageUrl="../images/announcements.png" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderStyle-Width="30px" UniqueName="chkItems" ReadOnly="true"
                        AllowFiltering="false">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkItem" runat="server" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn SortExpression="ID" HeaderText="ID" ReadOnly="true" Display="false"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="ID" UniqueName="ID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="InstituteID" HeaderText="InstituteID" ReadOnly="true"
                        Display="false" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InstituteID" UniqueName="InstituteID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="70px" FilterControlWidth="35px" SortExpression="InstituteName"
                        HeaderText="Broker" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" ReadOnly="true" DataField="InstituteName"
                        UniqueName="InstituteName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="PriceInstituteID" HeaderText="PriceInstituteID"
                        ReadOnly="true" Display="false" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="PriceInstituteID"
                        UniqueName="PriceInstituteID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="PriceInstituteName" HeaderText="Bank" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        ReadOnly="true" DataField="PriceInstituteName" UniqueName="PriceInstituteName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="AccountTier" HeaderText="Description" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        ReadOnly="true" DataField="AccountTier" UniqueName="AccountTier">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="Min" HeaderText="Min" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="Min" UniqueName="Min" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="Max" HeaderText="Max" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="Max" UniqueName="Max" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="180px" SortExpression="Rate"
                        HeaderText="Rate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Rate" UniqueName="Rate">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="MaximumBrokerage" HeaderText="MaximumBrokerage"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="MaximumBrokerage" UniqueName="MaximumBrokerage"
                        ReadOnly="true" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="HoneymoonPeriod" HeaderText="HoneymoonPeriod"
                        Display="False" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                        DataField="HoneymoonPeriod" UniqueName="HoneymoonPeriod">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="Type" HeaderText="Type"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Type" UniqueName="Type" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="Applicability" HeaderText="Applicability"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Applicability" UniqueName="Applicability">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="ApplicableFrom" HeaderText="Applicable From"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="ApplicableFrom" DataFormatString="{0:dd/MM/yyyy}"
                        UniqueName="ApplicableFrom">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <telerik:RadGrid OnNeedDataSource="PresentationDetailGrid_OnNeedDataSource" ID="PresentationDetailGrid"
            runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
            AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="false" GridLines="None"
            AllowAutomaticDeletes="True" AllowFilteringByColumn="false" AllowAutomaticInserts="True"
            AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" Visible="false"
            OnItemDataBound="PresentationDetailGrid_OnItemDataBound">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                Name="AtCallRates" TableLayout="Fixed">
                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                <Columns>
                    <telerik:GridBoundColumn ReadOnly="true" UniqueName="AtCallAccStatus" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" UniqueName="ProductID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" UniqueName="AtCallAccountCID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="ID" HeaderText="ID" ReadOnly="true" Display="false"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="ID" UniqueName="ID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="InstituteID" HeaderText="InstituteID" ReadOnly="true"
                        Display="false" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InstituteID" UniqueName="InstituteID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="70px" FilterControlWidth="35px" SortExpression="InstituteName"
                        HeaderText="Broker" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" ReadOnly="true" DataField="InstituteName"
                        UniqueName="InstituteName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="PriceInstituteID" HeaderText="PriceInstituteID"
                        ReadOnly="true" Display="false" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="PriceInstituteID"
                        UniqueName="PriceInstituteID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="PriceInstituteName" HeaderText="Bank" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        ReadOnly="true" DataField="PriceInstituteName" UniqueName="PriceInstituteName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="AccountTier" HeaderText="Description" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        ReadOnly="true" DataField="AccountTier" UniqueName="AccountTier">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="Min" HeaderText="Min" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="Min" UniqueName="Min" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="Max" HeaderText="Max" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="Max" UniqueName="Max" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="180px" SortExpression="Rate" HeaderText="Rate"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Rate" UniqueName="Rate" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="180px" SortExpression="Rate" HeaderText="Rate"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Rate" UniqueName="RateDisplay" DataFormatString="{0:P}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="MaximumBrokerage" HeaderText="MaximumBrokerage"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="MaximumBrokerage" UniqueName="MaximumBrokerage"
                        ReadOnly="true" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="Type" HeaderText="Type"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Type" UniqueName="Type" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn ReadOnly="true" SortExpression="Applicability" HeaderText="Applicability"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="Applicability" UniqueName="Applicability">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="80px" SortExpression="ApplicableFrom"
                        HeaderText="Applicable From" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ApplicableFrom"
                        DataFormatString="{0:dd/MM/yyyy}" UniqueName="ApplicableFrom">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Amount" AllowFiltering="False">
                        <ItemTemplate>
                            $&nbsp;<telerik:RadNumericTextBox ID="txtAmount" runat="server" Width="120px">
                            </telerik:RadNumericTextBox><asp:RangeValidator ID="RangeValidator1" runat="server"
                                ErrorMessage="Invalid Amount" ControlToValidate="txtAmount" MinimumValue='<%# Eval("Min") %>'
                                MaximumValue='<%# Convert.ToDecimal(Eval("Max"))==0?decimal.MaxValue: Eval("Max")%>'
                                Type="Currency" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"></asp:RangeValidator>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <telerik:RadToolTipManager ID="RadToolTipManager1" runat="server" AutoTooltipify="True"
            CssClass="ToolTipContent" Position="BottomRight" Skin="Default" Animation="Fade">
        </telerik:RadToolTipManager>
        <asp:HiddenField ID="hfClientCID" runat="server" />
        <asp:HiddenField ID="hfClientID" runat="server" />
        <asp:HiddenField ID="hfIsAdmin" runat="server" />
        <asp:HiddenField ID="hfIsAdminMenu" runat="server" />
        <asp:HiddenField ID="hfIsSMA" runat="server" />
        <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
        <asp:ModalPopupExtender ID="popupSearch" runat="server" TargetControlID="btnShowPopup"
            PopupControlID="pnlpopup" PopupDragHandleControlID="PopupHeader" Drag="true"
            BackgroundCssClass="ModalPopupBG" BehaviorID="ModalBehaviour">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlpopup" runat="server" BackColor="White" Style="display: none">
            <div class="popup_Container">
                <div class="popup_Titlebar" id="PopupHeader">
                    <div class="TitlebarLeft">
                        Select Account
                    </div>
                    <div class="TitlebarRight" onclick="HideModalPopup();">
                    </div>
                </div>
                <div class="PopupBody">
                    <uc2:SearchNSelectClientAccount ID="searchAccountControl" runat="server" />
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
