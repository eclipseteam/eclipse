﻿using System;

namespace eclipseonlineweb.Controls
{
    public partial class SearchClientControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            img1.Src = Page.ResolveUrl("~/images/bgMainSearch1.png");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string url = string.Format(Page.ResolveClientUrl("~/clientsearch.aspx") + "?mode={0}&name={1}", "both", txtSearch.Text.Trim());
            Response.Redirect(url);
        }
    }
}