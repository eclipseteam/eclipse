﻿using System;
using System.Data;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;

namespace eclipseonlineweb.Controls
{
    public partial class AccountantIndividualControl : System.Web.UI.UserControl
    {
        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        public Action<Guid> Saved
        {
            get;
            set;
        }

        public Action<Guid, DataSet> SaveData
        {
            get;
            set;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadControls();
            }
        }
        private void LoadControls()
        {
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            var status = new StatusDS { TypeFilter = "Organization" };
            org.GetData(status);
            ddlStatusFlag.Items.Clear();
            ddlStatusFlag.DataSource = status.Tables[StatusDS.TABLENAME];
            ddlStatusFlag.DataTextField = StatusDS.NAME;
            ddlStatusFlag.DataValueField = StatusDS.NAME;
            ddlStatusFlag.DataBind();
        }
        public AccountantDS GetEntity()
        {
            AccountantDS accountantDs = new AccountantDS();
            DataRow dr = accountantDs.Tables[accountantDs.accountantTable.TableName].NewRow();
            dr[accountantDs.accountantTable.CLIENTID] = txtClientID.Text;
            dr[accountantDs.accountantTable.NAME] = txtTradingName.Text;
            dr[accountantDs.accountantTable.TRADINGNAME] = txtTradingName.Text;
            if (chkSameAsTrading.Checked)
            {
                dr[accountantDs.accountantTable.LEGALNAME] = txtTradingName.Text;
            }
            else
            {
                dr[accountantDs.accountantTable.LEGALNAME] = txtLegalName.Text;
            }
            dr[accountantDs.accountantTable.ACCOUNTANTTYPE] = AccountantEntityType.AccountantIndividualControl;
            dr[accountantDs.accountantTable.STATUS] = ddlStatusFlag.SelectedValue;
            dr[accountantDs.accountantTable.ACCOUNTDESIGNATION] = txtAccountDesignation.Text;
            dr[accountantDs.accountantTable.FACSIMILE] = FacsimleNo.Number;
            dr[accountantDs.accountantTable.FACSIMILECITYCODE] = FacsimleNo.CityCode;
            dr[accountantDs.accountantTable.FACSIMILECOUNTRYCODE] = FacsimleNo.CountryCode;
            dr[accountantDs.accountantTable.PHONENUBER] = PhNo.Number;
            dr[accountantDs.accountantTable.PHONENUMBERCOUNTRYCODE] = PhNo.CountryCode;
            dr[accountantDs.accountantTable.PHONENUMBERCITYCODE] = PhNo.CityCode;
            dr[accountantDs.accountantTable.WEBSITEADDRESS] = txtWebSite.Text;
            dr[accountantDs.accountantTable.ABN] = ABNNo.GetEntity();
            dr[accountantDs.accountantTable.ACN] = ACNNo.GetEntity();
            accountantDs.accountantTable.Rows.Add(dr);
            return accountantDs;
        }
        public void SetEntity(Guid CID, AccountantEntityType accountantType)
        {
            ClearEntity();
            hf_AccountantType.Value = accountantType.ToString();
            if (CID != Guid.Empty)
            {
                AccountantDS ds = GetAccountantDetails(CID);
                hfCID.Value = CID.ToString();
                DataRow dr = ds.accountantTable.Rows[0];
                txtClientID.Text = dr[ds.accountantTable.CLIENTID].ToString();
                txtTradingName.Text = dr[ds.accountantTable.TRADINGNAME].ToString();
                txtLegalName.Text = dr[ds.accountantTable.LEGALNAME].ToString();
                if (txtTradingName.Text == txtLegalName.Text)
                {
                    chkSameAsTrading.Checked = true;
                    txtLegalName.Enabled = false;
                }
                else
                {
                    chkSameAsTrading.Checked = false;
                    txtLegalName.Enabled = true;
                }
                ddlStatusFlag.SelectedValue = dr[ds.accountantTable.STATUS].ToString();
                txtAccountDesignation.Text = dr[ds.accountantTable.ACCOUNTDESIGNATION].ToString();
                FacsimleNo.Number = (dr[ds.accountantTable.FACSIMILE].ToString());
                FacsimleNo.CountryCode = (dr[ds.accountantTable.FACSIMILECOUNTRYCODE].ToString());
                FacsimleNo.CityCode = (dr[ds.accountantTable.FACSIMILECITYCODE].ToString());
                PhNo.Number = (dr[ds.accountantTable.PHONENUBER].ToString());
                PhNo.CountryCode = (dr[ds.accountantTable.PHONENUMBERCOUNTRYCODE].ToString());
                PhNo.CityCode = (dr[ds.accountantTable.PHONENUMBERCITYCODE].ToString());
                txtWebSite.Text = dr[ds.accountantTable.WEBSITEADDRESS].ToString();
                ABNNo.SetEntity(dr[ds.accountantTable.ABN].ToString());
                ACNNo.SetEntity(dr[ds.accountantTable.ACN].ToString());
            }
        }
        private AccountantDS GetAccountantDetails(Guid cid)
        {
            var accountantDs =
                new AccountantDS { CommandType = DatasetCommandTypes.Details };
            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
                clientData.GetData(accountantDs);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }
            return accountantDs;
        }
        public void ClearEntity()
        {
            hfCID.Value = Guid.Empty.ToString();
            hf_AccountantType.Value = "";
            txtClientID.Text = "";
            txtTradingName.Text = "";
            txtLegalName.Text = "";
            txtAccountDesignation.Text = "";
            FacsimleNo.ClearEntity();
            PhNo.ClearEntity();
            txtWebSite.Text = "";
            ABNNo.ClearEntity();
            ACNNo.ClearEntity();
        }
        public void SetHiddenFields(string CID, string CLID, string CSID)
        {
            hfCID.Value = CID;
            hfCLID.Value = CLID;
            hfCSID.Value = CSID;
        }
        public bool Validate()
        {
            return Validate(true);
        }
        public bool Validate(bool ShowTabError)
        {
            bool result = true;
            result = result & ABNNo.Validate() & ACNNo.Validate() & PhNo.Validate() & FacsimleNo.Validate() ;
            return result;
        }
      
        protected void btnSaveAccountantIndivisual_Click(object sender, System.EventArgs e)
        {
            if (Validate())
            {
            AccountantDS accountantDs = new AccountantDS();
            var cid = (string.IsNullOrEmpty(hfCID.Value)) ? Guid.Empty : new Guid(hfCID.Value);
            if (cid == Guid.Empty)
            {
                accountantDs.CommandType = DatasetCommandTypes.Add;
            }
            else
            {
                accountantDs.CommandType = DatasetCommandTypes.Update;
            }
            if (Broker != null)
            {
                accountantDs.accountantTable.Merge(GetEntity().accountantTable);
                if (hfCID.Value == Guid.Empty.ToString())
                {
                    var unit = new OrganizationUnit
                                   {
                                       OrganizationStatus = "Active",
                                       Name = txtTradingName.Text,
                                       Type = ((int)OrganizationType.Accountant).ToString(),
                                       CurrentUser = (Page as UMABasePage).GetCurrentUser()
                                   };

                    accountantDs.Unit = unit;
                    accountantDs.Command = (int)WebCommands.AddNewOrganizationUnit;
                }
                if (SaveData != null)
                {
                   
                        SaveData(cid, accountantDs);
                        if (accountantDs.CommandType == DatasetCommandTypes.Add)
                        {
                            cid = accountantDs.Unit.Cid;
                            hfCLID.Value = accountantDs.Unit.Clid.ToString();
                            hfCSID.Value = accountantDs.Unit.Csid.ToString();
                            hfCID.Value = cid.ToString();

                          
                            if (Saved != null)
                            {
                                Saved(cid);
                            }
                        }
                }
                //Enable Disable Legal control
                txtLegalName.Enabled = !chkSameAsTrading.Checked;
            }

            else
            {
                throw new Exception("Broker Not Found");
            }
            }
            else
            {
                return;//for now : skip saving
            }
        }
    }
}