﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountProcessBankAccountMappingControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.AccountProcessBankAccountMappingControl" %>
<div class="popup_Container">
    <div class="popup_Titlebar" id="PopupHeader">
        <div class="TitlebarLeft">
            <asp:Label ID="Title" runat="server" Text="Availabe Bank Acocunts"></asp:Label>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="selectedCLID" />
    <asp:HiddenField runat="server" ID="selectedCSID" />
    <div class="popup_Body">
        <div runat="server" id="dvSearchtxt">
            <table>
                <tr>
                    <td>
                        <strong>Search existing Account Name</strong>
                    </td>
                    <td>
                        <telerik:RadTextBox runat="server" ID="txtSearchAccName" Width="520px">
                        </telerik:RadTextBox>
                    </td>
                    <td>
                        <telerik:RadButton runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_OnClick">
                        </telerik:RadButton>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <table width="100%" id="includedAccount" runat="server" border="1">
            <thead>
                <tr>
                    <td>
                        Account Name
                    </td>
                    <td>
                        Account Type
                    </td>
                    <td>
                        BSB No
                    </td>
                    <td>
                        Account No
                    </td>
                    <td>
                        Institution
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <asp:Label ID="AccountName" Width="300" runat="server">                             
                        </asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="AccountType" Width="70" runat="server">
                               
                        </asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="BSBNo" Width="70" runat="server">
                        </asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="AccountNo" Width="70" runat="server">
                               
                        </asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="Institution" Width="120" runat="server">
                               
                        </asp:Label>
                    </td>
                </tr>
            </tbody>
        </table>
        <table style="width: 100%">
            <tr>
                <td colspan="2">
                    <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                        PageSize="4" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                        GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                        AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource"
                        OnItemDataBound="PresentationGrid_OnItemDataBound">
                        <PagerStyle Mode="NumericPages"></PagerStyle>
                        <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                            Name="Banks" TableLayout="Fixed">
                            <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <Columns>
                                <telerik:GridBoundColumn SortExpression="CLid" ReadOnly="true" HeaderText="CLid"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="CLid" UniqueName="CLid" Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="CSid" ReadOnly="true" HeaderText="CSid"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="CSid" UniqueName="CSid" Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderText="Select" UniqueName="ChkSelect" AllowFiltering="false"
                                    HeaderStyle-Width="45px">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBox1" runat="server" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn SortExpression="AccountName" ReadOnly="true" HeaderText="Account Name"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    FilterControlWidth="60%" HeaderStyle-Width="45%" HeaderButtonType="TextButton"
                                    DataField="AccountName" UniqueName="AccountName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn ReadOnly="true" SortExpression="AccountType" HeaderText="Account Type"
                                    FilterControlWidth="55%" AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                                    HeaderStyle-Width="13%" HeaderButtonType="TextButton" DataField="AccountType"
                                    UniqueName="AccountType">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="BSBNo" HeaderText="BSB" ReadOnly="true"
                                    HeaderStyle-Width="10%" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" FilterControlWidth="55%" HeaderButtonType="TextButton"
                                    DataField="BSBNo" UniqueName="BSBNo">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="AccountNo" HeaderText="Account No" ReadOnly="true"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderStyle-Width="14%" HeaderButtonType="TextButton" DataField="AccountNo" UniqueName="AccountNo">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="Institution" HeaderText="Institution" ReadOnly="true"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    FilterControlWidth="60%" HeaderButtonType="TextButton" DataField="Institution"
                                    UniqueName="Institution">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </td>
            </tr>
            <tr>
                <td style="text-align: center" colspan="2">
                    <asp:Label runat="server" ID="lblmessages" Font-Size="Medium" Font-Bold="True" ForeColor="Red" /><asp:HiddenField
                        ID="hfFilter" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td style="text-align: right">
                    <telerik:RadButton runat="server" ID="btnUpdate" Text="Save" OnClick="btnUpdate_Onclick">
                    </telerik:RadButton>
                    <telerik:RadButton runat="server" ID="btnCancel" Text="Cancel" OnClick="btnCancel_Onclick">
                    </telerik:RadButton>
                </td>
            </tr>
        </table>
    </div>
</div>
