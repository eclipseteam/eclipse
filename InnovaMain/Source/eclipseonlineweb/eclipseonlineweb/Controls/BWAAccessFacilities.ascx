﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BWAAccessFacilities.ascx.cs"
    Inherits="eclipseonlineweb.Controls.BWAAccessFacilities" %>
<asp:HiddenField ID="hfType" runat="server" />
<div class="MainView" style="padding-top: 5px;">
    <fieldset>
        <legend>
            <asp:Label ID="lbl1" runat="server"></asp:Label>
            - BWA CMA OPTIONS </legend>
        <p>
            The following selections will apply to my/our
            <asp:Label ID="lbl2" runat="server"></asp:Label>
            BWA CMA</p>
        <h2>
            <asp:Label ID="lbl3" runat="server"></asp:Label>
            BWA Access Facilities
        </h2>
        <p>
            Please tick ( √ ) the Access Facilities required:</p>
        1.
        <telerik:RadButton ID="chkPhoneAccess" runat="server" Text="Phone Access" AutoPostBack="false"
            ToggleType="CheckBox" ButtonType="ToggleButton">
        </telerik:RadButton>
        <br />
        2.
        <telerik:RadButton ID="chkOnLineAccess" runat="server" Text="Online Access" AutoPostBack="false"
            ToggleType="CheckBox" ButtonType="ToggleButton">
        </telerik:RadButton>
        <br />
        3.
        <telerik:RadButton ID="chkDebitCard" runat="server" Text="Debit Card" AutoPostBack="false"
            ToggleType="CheckBox" ButtonType="ToggleButton">
        </telerik:RadButton>
        <br />
        4.
        <telerik:RadButton ID="chkChequeBook" runat="server" Text="Cheque Book (25 per book)"
            AutoPostBack="false" ToggleType="CheckBox" ButtonType="ToggleButton">
        </telerik:RadButton>
        <br />
        5.
        <telerik:RadButton ID="chkDepositBook" runat="server" Text="Deposit Book" AutoPostBack="false"
            ToggleType="CheckBox" ButtonType="ToggleButton">
        </telerik:RadButton>
        <br />
        <br />
        <h2>
            <asp:Label ID="lbl4" runat="server"></asp:Label>
            BWA Manner Of Operation
        </h2>
        <p>
            Please select how you wish to operate your CMA by ticking ( √ ) one of the following:</p>
        <telerik:RadButton ID="btnAnyOneSign" runat="server" ToggleType="Radio" ButtonType="ToggleButton"
            Text="Any one of us to sign" Value="Any one of us to sign" GroupName="MannerOperationOptionsDIY"
            AutoPostBack="False">
        </telerik:RadButton>
        <telerik:RadButton ID="btnAnyTwoSign" runat="server" ToggleType="Radio" Text="Any two of us to sign"
            Value="Any two of us to sign" GroupName="MannerOperationOptionsDIY" ButtonType="ToggleButton"
            OnCheckedChanged="btnAnyTwoSign_OnCheckedChanged">
        </telerik:RadButton>
        <telerik:RadButton ID="btnAllSign" runat="server" ToggleType="Radio" Text="All of us to sign"
            Value="All of us to sign" GroupName="MannerOperationOptionsDIY" ButtonType="ToggleButton"
            OnCheckedChanged="btnAllSign_OnCheckedChanged">
        </telerik:RadButton>
        <p>
            <p>
                Note:<br />
                <i>1. Where you do not elect a manner of operation, BWA will default to 'All of us to
                    sign'</i><br />
                <i>2. Phone Access, Online Access and a Debit Card cannot be selected unless the manner
                    of operation is 'Any one of us to sign'. </i>
            </p>
            <br />
    </fieldset>
    <br />
</div>
