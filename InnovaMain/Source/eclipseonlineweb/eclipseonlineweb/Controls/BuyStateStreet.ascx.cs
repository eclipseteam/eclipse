﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Words;
using Aspose.Words.Tables;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;
using System.Web;
using System.IO;
using Table = Aspose.Words.Tables.Table;

namespace eclipseonlineweb.Controls
{
    public partial class BuyStateStreet : UserControl
    {
        public string ClientCID
        {
            private get
            {
                return hfClientCID.Value;
            }
            set
            {
                hfClientCID.Value = value;
            }
        }

        private string ClientAccID
        {
            get
            {
                return hfClientID.Value;
            }
            set
            {
                hfClientID.Value = value;
            }
        }

        public bool IsAdmin
        {
            private get
            {
                return Convert.ToBoolean(hfIsAdmin.Value);
            }
            set
            {
                hfIsAdmin.Value = value.ToString();
            }
        }

        public bool IsAdminMenu
        {
            private get
            {
                if (string.IsNullOrEmpty(hfIsAdminMenu.Value))
                {
                    hfIsAdminMenu.Value = "false";
                }
                return Convert.ToBoolean(hfIsAdminMenu.Value);
            }
            set
            {
                hfIsAdminMenu.Value = value.ToString();
            }
        }

        private bool IsSMA
        {
            get
            {
                if (string.IsNullOrEmpty(hfIsSMA.Value))
                {
                    hfIsSMA.Value = "false";
                }
                return Convert.ToBoolean(hfIsSMA.Value);
            }
            set
            {
                hfIsSMA.Value = value.ToString();
            }
        }

        public Action<string, DataSet> SaveData { get; set; }

        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            searchAccountControl.SelectData += (clientCID, clientId, clientName) =>
            {
                ClientHeaderInfo1.SetEntity(new Guid(clientCID));
                ClientHeaderInfo1.Visible = true;
                ClientCID = clientCID;
                ClientAccID = clientId;
                SetClientManagementType();
                if (IsSMA && !Utilities.IsTestApp())
                {
                    lblNotify.Visible = true;
                    lblNotify.Text = "This feature is not available for Super clients at the moment.";
                }
                else
                {
                    LoadControls();
                    showDetails.Visible = true;
                    spanDIYService.Visible = showDetails.Visible;
                    PresentationGrid.Rebind();
                    PresentationGrid.Visible = true;
                    tdButtons.Visible = true;
                }
            };

            searchAccountControl.ShowHideModal += (show) =>
            {
                if (show)
                {
                    popupSearch.Show();
                }
                else
                {
                    popupSearch.Hide();
                }
            };

            lblMsg.Visible = false;

            if (!IsPostBack)
            {
                if (IsAdminMenu)
                {
                    btnSearch.Visible = true;
                    showDetails.Visible = false;
                    spanDIYService.Visible = showDetails.Visible;
                    ClientHeaderInfo1.Visible = false;
                    PresentationGrid.Visible = false;
                    tdButtons.Visible = false;
                    searchAccountControl.IsAdminMenu = true;
                }
                else
                {
                    SetClientManagementType();
                    btnSearch.Visible = false;
                    showDetails.Visible = true;
                    spanDIYService.Visible = showDetails.Visible;
                    LoadControls();
                    ClientHeaderInfo1.Visible = true;
                    PresentationGrid.Visible = true;
                    tdButtons.Visible = true;
                    searchAccountControl.IsAdminMenu = false;
                }
            }
            else if (hfIsAlert.Value.ToLower() == "pass")
            {
                if (!IsAdminMenu && Request.QueryString["ins"] != null)
                {
                    var clientCid = new Guid(Request.Params["ins"]);
                    ClientHeaderInfo1.SetEntity(clientCid);
                }
                btnSave_OnClick(null, null);
            }
        }

        private HoldingRptDataSet GetData()
        {
            IBrokerManagedComponent clientData = Broker.GetBMCInstance(new Guid(ClientCID));
            var ds = new HoldingRptDataSet();
            clientData.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(clientData);
            return ds;
        }

        private void BindServiceType()
        {
            //Getting Service type according to user type
            var ds = GetData();
            OrderPadUtilities.FillServiceTypes(cmbServiceType, ds, IsAdmin, OrderAccountType.StateStreet);
        }

        private void BindBankAccounts()
        {
            //Getting cash bank account according to service type
            var ds = GetData();
            string filter = string.Format("{0}='{1}' and {2}='{3}' and {4}='cash'", ds.HoldingSummaryTable.LINKEDENTITYTYPE, OrganizationType.BankAccount, ds.HoldingSummaryTable.SERVICETYPE, cmbServiceType.SelectedValue, ds.HoldingSummaryTable.ASSETNAME);
            OrderPadUtilities.FillBankNAvailableFunds(cmbCashAccount, ds, filter, OrderAccountType.StateStreet, OrderItemType.Buy, cmbServiceType.SelectedValue, IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA, Broker);
            OrderPadUtilities.SetAvailableFunds(cmbCashAccount, lblAvailableFunds, lblCashBalance, lblMinCash, IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA);
        }

        private void LoadControls()
        {
            BindServiceType();
            BindBankAccounts();
        }

        protected void PresentationGridItemDataBound(object sender, GridItemEventArgs e)
        {
            var item = e.Item as GridDataItem;
            if (item != null)
            {
                switch (item["FundCode"].Text)
                {
                    case "IV001":
                    case "IV008":
                    case "IV011":
                        GetPrductDetails(
                            "https://www.smartsheet.com/b/download/att/Sh43KBRbWLjGk3HuR8dYljPR2Ey5TmhdMSV8GzayzcE",
                            item);
                        break;
                    case "IV002":
                    case "IV009":
                    case "IV012":
                        GetPrductDetails(
                            "https://www.smartsheet.com/b/download/att/9zlRPBbf1ZLt0wgsGE8tiiRk5JubhxCUCxJzWhlFRPE",
                            item);
                        break;
                    case "IV003":
                    case "IV010":
                    case "IV013":
                        GetPrductDetails(
                            "https://www.smartsheet.com/b/download/att/v4ROc2AEikwUOvBVpUekB-Z28hCU_9Jd9JQv1n7q8QY",
                            item);
                        break;

                    case "IV004":
                    case "IV014":
                        GetPrductDetails(
                            "https://www.smartsheet.com/b/download/att/749oLm5g_r9bwSMPztptDy-U4UVenOizkJDEWsCf05w",
                            item);
                        break;

                    case "IV005":
                    case "IV015":
                        GetPrductDetails(
                            "https://www.smartsheet.com/b/download/att/7PPuDOFwdrP8o5uGn60qyxHCmwTUbLqxf9vn_Ptqmsk",
                            item);
                        break;

                    case "IV006":
                    case "IV016":
                    case "IV018":
                        GetPrductDetails(
                            "https://www.smartsheet.com/b/download/att/RYoaLrhKUCUOJ7Tvs2HkfVT0k7N-SgBQXaNUUSf_Gwg",
                            item);
                        break;

                    case "IV007":
                    case "IV017":
                        GetPrductDetails(
                            "https://www.smartsheet.com/b/download/att/50dQd9qsPHhbPUt3Nd4Pm22qiporIm8qIiB-zJHvjAs",
                            item);
                        break;
                }

                var cvAmount = item.FindControl("cvAmount") as CompareValidator;
                if (cvAmount != null)
                {
                    if (IsSMA)
                    {
                        decimal value;
                        decimal.TryParse(item["AvailableFunds"].Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out value);
                        cvAmount.ValueToCompare = Math.Round(value, 2).ToString();
                        cvAmount.Enabled = true;
                    }
                    else
                    {
                        cvAmount.Enabled = false;
                    }
                }
            }
        }

        private void GetPrductDetails(string url, GridDataItem item)
        {
            var link = (HyperLink)item["FundName"].Controls[0];
            link.NavigateUrl = url;
        }

        protected void PresentationGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var ds = GetData();
            string filter = string.Format("{0}='{1}'", ds.ProductBreakDownTable.LINKEDENTITYTYPE, OrganizationType.ManagedInvestmentSchemesAccount);
            if (IsSMA)
            {
                filter += string.Format(" and {0} = '{1}'", ds.ProductBreakDownTable.ISSMAAPPROVED, true);
            }
            else
            {
                GetMissingMIS(ds);
            }
            var dt = new DataView(ds.ProductBreakDownTable)
            {
                RowFilter = filter,
                Sort = string.Format("{0} ASC", ds.ProductBreakDownTable.INVESMENTCODE)
            }.ToTable();

            if (IsSMA)
            {
                dt.Columns.Add("HoldingLimit", typeof(decimal));
                dt.Columns.Add("TotalHolding", typeof(decimal));
                dt.Columns.Add("AvailableFunds", typeof(decimal));

                foreach (DataRow row in dt.Rows)
                {
                    var fundCode = row[ds.ProductBreakDownTable.INVESMENTCODE].ToString();
                    var unitPrice = (decimal)row[ds.ProductBreakDownTable.UNITPRICE];
                    decimal secLimitPercent = Convert.ToDecimal(row[ds.ProductBreakDownTable.SMAHOLDINGLIMIT]);
                    decimal secHoldingLimit, secCurrentHolding, secAvailableFund;
                    OrderPadUtilities.GetSecurityAvaiableFundsForSMA(ds, fundCode, secLimitPercent, unitPrice, OrganizationType.ManagedInvestmentSchemesAccount, out secHoldingLimit, out secCurrentHolding, out secAvailableFund);
                    row["HoldingLimit"] = secHoldingLimit;
                    row["TotalHolding"] = secCurrentHolding;
                    row["AvailableFunds"] = secAvailableFund;
                }
            }

            PresentationGrid.DataSource = dt;
        }

        private void GetMissingMIS(HoldingRptDataSet ds)
        {
            hfIsAlert.Value = "false";
            //Get All MIS
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            var misDs = new ManagedInvestmentAccountDS
            {
                Command = (int)WebCommands.GetOrganizationUnitsByType,
                Unit = new OrganizationUnit
                {
                    CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                    Type = ((int)OrganizationType.ManagedInvestmentSchemesAccount).ToString()
                }
            };
            org.GetData(misDs);
            Broker.ReleaseBrokerManagedComponent(org);

            //Adding Missing MIS
            foreach (DataRow row in misDs.FundAccountsTable.Rows)
            {
                string filterExp = string.Format("{0}='{1}'", ds.ProductBreakDownTable.INVESMENTCODE, row[misDs.FundAccountsTable.FUNDCODE]);
                var isFound = ds.ProductBreakDownTable.Select(filterExp).Length > 0;
                if (!isFound)
                {
                    var dr = ds.ProductBreakDownTable.NewRow();
                    dr[ds.ProductBreakDownTable.PRODUCTID] = Guid.Empty;
                    dr[ds.ProductBreakDownTable.LINKEDENTITYTYPE] = OrganizationType.ManagedInvestmentSchemesAccount.ToString();
                    dr[ds.ProductBreakDownTable.INVESMENTCODE] = row[misDs.FundAccountsTable.FUNDCODE];
                    dr[ds.ProductBreakDownTable.INVESMENTNAME] = row[misDs.FundAccountsTable.FUNDDESCRIPTION];
                    ds.ProductBreakDownTable.Rows.Add(dr);
                }
            }

        }

        protected void btnCutOff_OnClick(object sender, EventArgs e)
        {
            FileHelper.DownLoadFile(FileHelper.CutOffTimeFilePath, FileHelper.CutOffTimeFileName);
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            if (hfIsAlert.Value.ToLower() != "pass")
            {
                lblMsg.Visible = false;
                lblMsg.Text = "";

                //Validate funds
                if (!ValidateAmount())
                {
                    lblMsg.Visible = true;
                    return;
                }
            }

            if (hfIsAlert.Value.ToLower() == "true")
            {
                ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "showConfirmAlert", "ShowAlert()", true);
            }
            else
            {
                hfIsAlert.Value = false.ToString();
                //Checking Existing Order CM ID
                Guid orderCMCID = OrderPadUtilities.IsOrderCmExists(Broker, (Page as UMABasePage).GetCurrentUser());

                var unit = new OrganizationUnit
                               {
                                   Name = "Order " + DateTime.Now.ToString("dd MMM, yyyy hh:mm:ss"),
                                   Type = ((int)OrganizationType.Order).ToString(),
                                   CurrentUser = (Page as UMABasePage).GetCurrentUser()
                               };
                var ds = new OrderPadDS
                             {
                                 CommandType = DatasetCommandTypes.Add,
                                 Unit = unit,
                                 Command = (int)WebCommands.AddNewOrganizationUnit
                             };

                //Add rows in order pad ds
                AddRows(ds);

                if (SaveData != null)
                {
                    SaveData(orderCMCID.ToString(), ds);
                }

                lblMsg.Visible = true;
                lblMsg.Text = ds.ExtendedProperties["Message"].ToString();

                if (ds.ExtendedProperties["Result"].ToString() == OperationResults.Successfull.ToString())
                {

                    if (ds.ExtendedProperties.Contains("SMAMessages"))
                    {
                        var results = (Dictionary<long, string>)ds.ExtendedProperties["SMAMessages"];
                        if (results.Count > 0)
                        {
                            var messag = string.Empty;
                            foreach (KeyValuePair<long, string> result in results)
                            {
                                messag += string.Format("Order: {0},{2}{1}{2}", result.Key, result.Value, Environment.NewLine);
                            }
                            string smaErrorFile = GetNewFilePath("txt");
                            Utilities.SaveToFile(messag, smaErrorFile);

                            lblMsg.Text += string.Format("To view Super service errors click <a href = '../SysAdministration/DownloadOrderReceipt.aspx?filename={0}'>Here</a> ", FileHelper.GetFileName(smaErrorFile));
                            ShowAlert("ShowServiceErrors", "There were errors sending data to Super Service.");
                        }
                    }


                    BindBankAccounts();
                    PresentationGrid.Rebind();
                    string path = CreateReceipt(ds);
                    lblMsg.Text = string.Format("{0} Click <a href = '../SysAdministration/DownloadOrderReceipt.aspx?filename={1}'>here</a> to print its receipt.", lblMsg.Text, path);

                }
            }
        }
        public void ShowAlert(string key, string message)
        {
            ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), key, string.Format("alert('{0}');", message), true);
        }
        private string GetNewFilePath(string ext)
        {
            string fileName = string.Format("{0}_{1}_BuyMIS_{2}.{3}", ClientHeaderInfo1.ClientId ?? ClientAccID, lblMsg.Text.Split(' ')[1], DateTime.Now.ToShortDateString().Replace("/", "-"), ext);
            string dirOrderReceipts = string.Format("{0}\\OrderReceipts", HttpContext.Current.Server.MapPath("~/App_Data"));
            if (!Directory.Exists(dirOrderReceipts))
                Directory.CreateDirectory(dirOrderReceipts);
            string filePath = string.Format("{0}\\{1}", dirOrderReceipts, fileName);
            return filePath;
        }


        private void AddCellToSheet(DocumentBuilder builderExl, string text, double width, Color backColor,
                                    bool isFontBold, int fontSize, int lineWidth, CellVerticalAlignment vAlign,
                                    TextOrientation orientation, ParagraphAlignment paragraph)
        {
            builderExl.InsertCell();
            builderExl.CellFormat.PreferredWidth = PreferredWidth.FromPercent(width);
            builderExl.CellFormat.Shading.BackgroundPatternColor = backColor;
            builderExl.Font.Bold = isFontBold;
            builderExl.Font.Size = fontSize;
            builderExl.Font.Color = Color.Black;
            builderExl.CellFormat.Borders.LineWidth = lineWidth;
            builderExl.CellFormat.VerticalAlignment = vAlign;
            builderExl.CellFormat.Orientation = orientation;
            builderExl.CurrentParagraph.ParagraphFormat.Alignment = paragraph;
            builderExl.Bold = isFontBold;
            builderExl.Write(text);
        }

        private string CreateReceipt(OrderPadDS ds)
        {
            double sum = 0;
            DataTable dt = ds.Tables[ds.StateStreetTable.TABLENAME];
            DataTable dt_ = dt.DefaultView.ToTable(false, "FundCode", "FundName", "Amount");
            DataRow clientSummaryRow = GetData().Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0];
            string OutPath = string.Empty;
            string path = Server.MapPath("~/Templates/OrderPad_BuyMIS_Template.docx");
            Document document = null;
            document = new Document(path);
            //This is portion for the Header Section of the Document
            document.Range.Replace("[Order_Date]", string.Format("Order {0}", lblMsg.Text.Split(' ')[1]), false, false);
            document.Range.Replace("[Elipse_Or]", DateTime.Now.ToString("dd/MM/yyyy"), false, false);
            document.Range.Replace("[Eclips_OrderBy]", ((Page as UMABasePage).GetCurrentUser()).CurrentUserName, false, false);
            document.Range.Replace("[Eclipse_InvestorName]", clientSummaryRow[HoldingRptDataSet.CLIENTNAME].ToString(), false, false);
            document.Range.Replace("[Eclipse_ClientID]", ClientHeaderInfo1.ClientId ?? ClientAccID, false, false);
            document.Range.Replace("[Eclipse_AccountType]", clientSummaryRow[HoldingRptDataSet.CLIENTTYPE].ToString(), false, false);
            document.Range.Replace("[Eclipse_Adviser]", clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME].ToString(), false, false);
            document.Range.Replace("[Eclipse_ServicesType]", cmbServiceType.SelectedItem.Text.ToUpper(), false, false);
            document.Range.Replace("[A11]", clientSummaryRow[HoldingRptDataSet.CLIENTNAME].ToString(), false, false);
            string addressLine1And2 = string.Empty;
            if (clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2].ToString() == string.Empty)
                addressLine1And2 = clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1].ToString();
            else
                addressLine1And2 = clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1].ToString() + ", " +
                                   clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2];
            document.Range.Replace("[A12]", addressLine1And2, false, false);
            document.Range.Replace("[A13]", string.Format("{0} {1} {2}", clientSummaryRow[HoldingRptDataSet.SUBURB], clientSummaryRow[HoldingRptDataSet.STATE], clientSummaryRow[HoldingRptDataSet.POSTCODE]), false, false);
            document.Range.Replace("[A14]", clientSummaryRow[HoldingRptDataSet.COUNTRY].ToString(), false, false);
            //EclipseMisTable
            int bodyFontSize = 9;
            if (document != null)
            {
                OutPath = GetNewFilePath("PDF");
                //Now here we try to Make Dynamic Table
                DocumentBuilder builderExl = new DocumentBuilder(document);
                //This is Point where to Start Generate Table
                builderExl.MoveToBookmark("EclipseMisTable", false, true);
                Table table = builderExl.StartTable();

                builderExl.PageSetup.LeftMargin = ConvertUtil.InchToPoint(1);
                builderExl.PageSetup.TopMargin = ConvertUtil.InchToPoint(1);
                builderExl.PageSetup.BottomMargin = ConvertUtil.InchToPoint(1);

                builderExl.StartTable();

                builderExl.InsertCell();
                builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                builderExl.CellFormat.Borders.LineWidth = 0;
                builderExl.EndRow();
                builderExl.EndTable();
                builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Left;
                builderExl.Writeln("");

                AddCellToSheet(builderExl, "FundCode", 20, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, "FundName", 60, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, "Amount", 20, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                builderExl.EndRow();

                for (int i = 0; i < dt_.Rows.Count; i++)
                {
                    AddCellToSheet(builderExl, dt_.Rows[i]["FundCode"].ToString(), 20, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                    AddCellToSheet(builderExl, dt_.Rows[i]["FundName"].ToString(), 60, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                    AddCellToSheet(builderExl, string.Format("{0:C}", dt_.Rows[i]["Amount"]), 20, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                    builderExl.EndRow();
                    sum += Convert.ToDouble(dt_.Rows[i]["Amount"]);
                }
                //Here we add the Total Colunm
                AddCellToSheet(builderExl, "Total", 20, Color.White, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                AddCellToSheet(builderExl, string.Empty, 60, Color.White, true, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, string.Format("{0:C}", sum), 20, Color.White, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                builderExl.EndRow();
                //table.AutoFit(AutoFitBehavior.FixedColumnWidths);
                table.PreferredWidth = PreferredWidth.FromPercent(70);
                document.Save(OutPath, SaveFormat.Pdf);
            }
            return FileHelper.GetFileName(OutPath);
        }

        private bool ValidateAmount()
        {
            decimal totalAmount = 0;
            foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
            {
                var txtAmount = (dataItem.FindControl("txtAmount") as RadNumericTextBox);

                if (txtAmount != null && (!string.IsNullOrEmpty(txtAmount.Text.Trim()) && Convert.ToDecimal(txtAmount.Text) > 0))
                {
                    totalAmount += Convert.ToDecimal(txtAmount.Text);
                    if (hfIsAlert.Value.ToLower() != "true" && dataItem["ProductID"].Text == Guid.Empty.ToString())
                        hfIsAlert.Value = true.ToString();
                }
            }
            if (totalAmount == 0)
            {
                lblMsg.Text = "Please Enter Amount";
                return false;
            }

            decimal availableFunds;
            decimal.TryParse(lblAvailableFunds.Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out availableFunds);
            if (totalAmount > availableFunds)
            {
                lblMsg.Text = "Insufficient Available Funds";
                return false;
            }
            return true;
        }

        private void AddRows(OrderPadDS ds)
        {
            //Order Row
            var orderId = AddOrderRow(ds);

            foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
            {
                var txtAmount = (dataItem.FindControl("txtAmount") as RadNumericTextBox);
                decimal amount = 0;
                if (txtAmount != null && !string.IsNullOrEmpty(txtAmount.Text))
                {
                    amount = Convert.ToDecimal(txtAmount.Text.Trim());
                }
                if (amount > 0)
                {
                    //Order Items Row
                    AddItemRow(ds, amount, dataItem, orderId);
                }
            }
        }

        private Guid AddOrderRow(OrderPadDS ds)
        {
            DataRow orderRow = ds.OrdersTable.NewRow();

            Guid orderId = Guid.NewGuid();
            orderRow[ds.OrdersTable.ID] = orderId;
            orderRow[ds.OrdersTable.CLIENTCID] = new Guid(ClientCID);
            orderRow[ds.OrdersTable.CLIENTID] = ClientHeaderInfo1.ClientId ?? ClientAccID;
            orderRow[ds.OrdersTable.ORDERACCOUNTTYPE] = OrderAccountType.StateStreet;
            orderRow[ds.OrdersTable.ORDERTYPE] = OrderType.Manual;
            orderRow[ds.OrdersTable.STATUS] = OrderStatus.Active;
            orderRow[ds.OrdersTable.ACCOUNTCID] = new Guid(cmbCashAccount.SelectedValue);
            orderRow[ds.OrdersTable.ACCOUNTNUMBER] = cmbCashAccount.SelectedItem.Attributes["AccountNo"].Trim();
            orderRow[ds.OrdersTable.ACCOUNTBSB] = cmbCashAccount.SelectedItem.Attributes["BSB"].Trim();
            orderRow[ds.OrdersTable.ORDERITEMTYPE] = OrderItemType.Buy;
            orderRow[ds.OrdersTable.CLIENTMANAGEMENTTYPE] = IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA;

            ds.OrdersTable.Rows.Add(orderRow);
            return orderId;
        }

        private void AddItemRow(OrderPadDS ds, decimal amount, GridDataItem dataItem, Guid orderId)
        {
            DataRow itemsRow = ds.StateStreetTable.NewRow();

            itemsRow[ds.StateStreetTable.ID] = Guid.NewGuid();
            itemsRow[ds.StateStreetTable.ORDERID] = orderId;
            itemsRow[ds.StateStreetTable.AMOUNT] = amount;
            itemsRow[ds.StateStreetTable.PRODUCTID] = new Guid(dataItem["ProductID"].Text);
            itemsRow[ds.StateStreetTable.FUNDCODE] = dataItem["FundCode"].Text;
            itemsRow[ds.StateStreetTable.FUNDNAME] = ((HyperLink)dataItem["FundName"].Controls[0]).Text;

            ds.StateStreetTable.Rows.Add(itemsRow);
        }

        protected void cmbCashAccount_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            OrderPadUtilities.SetAvailableFunds(cmbCashAccount, lblAvailableFunds, lblCashBalance, lblMinCash, IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA);
        }

        protected void cmbServiceType_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindBankAccounts();
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            popupSearch.Show();
            showDetails.Visible = false;
            spanDIYService.Visible = showDetails.Visible;
            ClientHeaderInfo1.Visible = false;
            PresentationGrid.Visible = false;
            tdButtons.Visible = false;
            searchAccountControl.HideModal = false;
            searchAccountControl.FindControl("SearchBox").Focus();
        }

        private void SetClientManagementType()
        {
            IsSMA = SMAUtilities.IsSMAClient(new Guid(ClientCID), Broker);
            PresentationGrid.Columns.FindByUniqueNameSafe("HoldingLimit").Visible =
            PresentationGrid.Columns.FindByUniqueNameSafe("TotalHolding").Visible =
            PresentationGrid.Columns.FindByUniqueNameSafe("AvailableFunds").Visible =
            tdCashBalanceHead.Visible =
            tdCashBalanceValue.Visible =
            tdMinCashHead.Visible =
            tdMinCashValue.Visible = IsSMA;
            tdFundsHead.Style["width"] = IsSMA ? "125px" : "100%";
        }
    }
}