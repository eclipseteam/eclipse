﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;

namespace eclipseonlineweb.Controls
{
    public partial class ClientMappingControl : System.Web.UI.UserControl
    {
        private string _cid;

        public OrganizationType OrgType
        {
            get { return ClientSearchControl.OrgType; }
            set { ClientSearchControl.OrgType = value; }
        }

        public Action Canceled { get; set; }

        protected ICMBroker UMABroker
        {
            get { return (Page as UMABasePage).UMABroker; }
        }

        public Action<MembershipDS> Saved
        {
            get;
            set;
        }

        public Action<DataSet> SaveOrg { get; set; }

        public Action<string, DataSet> SaveUnit { get; set; }

     

        protected override void OnInit(EventArgs e)
        {
            ClientSearchControl.Canceled += () => ShowHideExisting(false);
            ClientSearchControl.ValidationFailed += () =>
            {

            };

            ClientSearchControl.Save += (ds) => SaveBankData(ds, DatasetCommandTypes.Add);
        }
        private void SaveBankData(MembershipDS ds, DatasetCommandTypes commandType)
        {
            var dstosend = TransformToClientDs(ds);
            dstosend.CommandType = commandType;
            if (SaveUnit != null)
                SaveUnit(_cid.ToString(), dstosend);

            ShowHideExisting(false);
            gd_ClientControl.Rebind();
        }

        private ClientMappingControlDS TransformToClientDs(MembershipDS ds)
        {
            var clientDs = new ClientMappingControlDS();
            foreach (DataRow dataRow in ds.membershipTable.Rows)
            {
                clientDs.ClientMappingTable.ImportRow(dataRow);
            }
            return clientDs;
        }

        /// <summary>
        /// Get Client data
        /// </summary>
        private void GetData()
        {
            var cID = new Guid(_cid);
            var ds = new ClientMappingControlDS()
            {
                CommandType = DatasetCommandTypes.GetClients,
                Unit = new OrganizationUnit
                {
                    CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                }
            };


            var component = UMABroker.GetBMCInstance(cID);
            component.GetData(ds);
            UMABroker.ReleaseBrokerManagedComponent(component);
            DataView memberShipView = new DataView(ds.ClientMappingTable);
            memberShipView.Sort = ds.ClientMappingTable.NAME + " ASC";
            gd_ClientControl.DataSource = memberShipView.ToTable();
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            _cid = Request.Params["ins"];
        }

        protected void gd_ClientControl_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void gd_ClientControl_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                var gDataItem = e.Item as GridDataItem;
                if (gDataItem != null)
                {
                    var cid = new Guid(gDataItem["Cid"].Text);
                    var clid = new Guid(gDataItem["Clid"].Text);
                    var csid = new Guid(gDataItem["Csid"].Text);
                    DeleteClient(cid, clid, csid);
                }
            }
        }

        private void DeleteClient(Guid cid, Guid clid, Guid csid)
        {

            var clientDs = new ClientMappingControlDS();
            clientDs.CommandType = DatasetCommandTypes.Delete;
            var row = clientDs.ClientMappingTable.NewRow();
            row[clientDs.ClientMappingTable.CID] = cid;
            row[clientDs.ClientMappingTable.CSID] = csid;
            row[clientDs.ClientMappingTable.CLID] = clid;


            clientDs.ClientMappingTable.Rows.Add(row);

            if (SaveUnit != null)
                SaveUnit(_cid, clientDs);
            gd_ClientControl.Rebind();
        }

        protected void gd_ClientControl_OnItemDataBound(object sender, GridItemEventArgs e)
        { }


        protected void LnkbtnAddBankAccClick(object sender, EventArgs e)
        {
            ShowHideExisting(true);
        }

        public void ShowHideExisting(bool show)
        {

            OVER.Visible = clientSearchPopup.Visible = show;
        }
    }
}