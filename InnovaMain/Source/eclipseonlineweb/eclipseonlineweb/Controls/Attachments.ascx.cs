﻿using System;
using System.Data;
using System.Linq;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Calculation;
using System.Collections.Generic;
using Oritax.TaxSimp;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Security.Principal;
using Oritax.TaxSimp.Utilities;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace eclipseonlineweb.Controls
{
    public partial class Attachments : System.Web.UI.UserControl
    {
        public IPrincipal User { get; set; }
        public DataSet PresentationData
        {
            get;
            set;
        }

        public void RebindAttachmentGrid()
        {
            GetBMCData();
            LoadPage();
            PresentationGrid.Rebind();
        }

        public string CID
        {
            get { return hfCID.Value; }
            set { hfCID.Value = value; }
        }

        public Action<Guid, Guid, Guid> Saved
        {
            get;
            set;
        }

        public Action<Guid, DataSet> SaveData
        {
            get;
            set;
        }
        public Action Canceled { get; set; }
        public Action ValidationFailed { get; set; }
        private ICMBroker Broker
        {
            get
            {
                return ((UMABasePage) Page).UMABroker;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadPage();
            }
        }

        protected void GetBMCData()
        {
            if (!string.IsNullOrEmpty(CID))
            {
                var clientData = Broker.GetBMCInstance(new Guid(CID)) as IOrganizationUnit;
                var attachmentListDs = new AttachmentListDS();
                if (clientData != null)
                {
                    clientData.GetData(attachmentListDs);
                    Broker.ReleaseBrokerManagedComponent(clientData);
                }
                PresentationData = attachmentListDs;

            }
        }

        public enum AttachmentStateEnum
        {
            List = 0, Import = 1
        }

        public enum AttachmentCommandEventTypeEnum { None = 0, Delete = 1, Copy = 2, Modify = 3, Add = 4, Cancel = 5, Sort = 6 };

        public void LoadPage()
        {
            var objUser = (DBUser)Broker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");

            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA))
            {
                PresentationGrid.Columns[PresentationGrid.Columns.Count - 1].Visible = false;
                PresentationGrid.Columns[0].Visible = false;
            }

            IDictionary<int, string> attachmentValue = Enumeration.ToIntDictionary(typeof(AttachmentType));
            cmbDocuType.DataSource = attachmentValue;
            cmbDocuType.DataValueField = "Key";
            cmbDocuType.DataTextField = "Value";
            cmbDocuType.DataBind();
            RadComboBoxItem radComboBoxItem = cmbDocuType.Items.FirstOrDefault(item => item.Text == "Other");
            if (radComboBoxItem != null)
                radComboBoxItem.Selected = true;
        }

        protected void PresentationGrid_DetailTableDataBind(object source, GridDetailTableDataBindEventArgs e) { }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                var gDataItem = e.Item as GridDataItem;
                var clientData = Broker.GetBMCInstance(new Guid(CID)) as IOrganizationUnit;
                Broker.SaveOverride = true;

                if (PresentationData == null)
                {
                    var attachmentListDS = new AttachmentListDS();
                    if (clientData != null) clientData.GetData(attachmentListDS);
                    PresentationData = attachmentListDS;
                }

                if (gDataItem != null)
                {
                    var rowId = new Guid(gDataItem.OwnerTableView.DataKeyValues[gDataItem.ItemIndex]["ID"].ToString());
                    DataTable objAttachmentsTable = PresentationData.Tables[AttachmentListDS.ATTACHMENTTABLE];

                    if (objAttachmentsTable.Columns.Contains(AttachmentListDS.ATTACHMENTTYPENAME_FIELD))
                        objAttachmentsTable.Columns.Remove(AttachmentListDS.ATTACHMENTTYPENAME_FIELD);

                    objAttachmentsTable.AcceptChanges();
                    foreach (DataRow objRow in objAttachmentsTable.Rows)
                    {
                        if (new Guid((string)objRow[AttachmentListDS.ATTACHMENTID_FIELD]) == rowId)
                        {
                            objRow.Delete();
                            if (clientData != null)
                                Broker.LogEvent(EventType.UMAClientDoocumentDelete, clientData.CID, "The attachments of Client:" + clientData.ClientId + "-" + clientData.Name + "is Deleted");
                        }
                    }
                }
                if (clientData != null) clientData.SetData(PresentationData);
                Broker.SetComplete();
                Broker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                PresentationGrid.Rebind();
            }

            if (e.CommandName.ToLower() == "update")
            {

                if (PresentationData == null)
                    GetBMCData();

                var gridEditableItem = e.Item as GridEditableItem;
                var clientData = Broker.GetBMCInstance(new Guid(CID)) as IOrganizationUnit;
                Broker.SaveOverride = true;

                if (gridEditableItem != null)
                {
                    var rowId = new Guid(gridEditableItem.OwnerTableView.DataKeyValues[gridEditableItem.ItemIndex]["ID"].ToString());
                    if (PresentationData != null)
                    {
                        DataTable objAttachmentsTable = PresentationData.Tables[AttachmentListDS.ATTACHMENTTABLE];

                        if (objAttachmentsTable.Columns.Contains(AttachmentListDS.ATTACHMENTTYPENAME_FIELD))
                            objAttachmentsTable.Columns.Remove(AttachmentListDS.ATTACHMENTTYPENAME_FIELD);

                        objAttachmentsTable.AcceptChanges();
                        foreach (DataRow objRow in objAttachmentsTable.Rows)
                        {
                            if (new Guid((string)objRow[AttachmentListDS.ATTACHMENTID_FIELD]) == rowId)
                            {
                                string productId = ((RadComboBox)gridEditableItem["ATTACHMENTTYPENAMEDESC"].Controls[0]).SelectedValue;
                                objRow[AttachmentListDS.ATTACHMENTDESCRIPTION_FIELD] = ((RadTextBox)(gridEditableItem["DESCRIPTION"].Controls[1])).Text;
                                objRow[AttachmentListDS.ATTACHMENTTYPE_FIELD] = int.Parse(productId);
                                if (clientData != null)
                                    Broker.LogEvent(EventType.UMAClientDoocumentEdit, clientData.CID, "The attachments of Client:" + clientData.ClientId + "-" + clientData.Name + "is Updated");
                            }
                        }
                    }
                }
                if (clientData != null) clientData.SetData(PresentationData);
                Broker.SetComplete();
                Broker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                Response.Redirect(Request.Url.AbsoluteUri);
            }

        }
        
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e) { }
        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("Attachments".Equals(e.Item.OwnerTableView.Name))
            {
            }
        }
        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                var pagerItem = (GridPagerItem)e.Item;
                var pageSizeComboBox = (RadComboBox)pagerItem.FindControl("PageSizeComboBox");
                pageSizeComboBox.Visible = false;
                var changePageSizelbl = (Label)pagerItem.FindControl("ChangePageSizeLabel");
                changePageSizelbl.Visible = false;
            }
        }
        protected void PresentationGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {

                var editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;
                var editor = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("ATTACHMENTTYPENAMEDESC"));

                IDictionary<int, string> attachmentValue = Enumeration.ToIntDictionary(typeof(AttachmentType));
                editor.ComboBoxControl.Filter = RadComboBoxFilter.Contains;
                editor.ComboBoxControl.Width = 400;
                editor.DataSource = attachmentValue;
                editor.DataValueField = "Key";
                editor.DataTextField = "Value";
                editor.DataBind();

                if (!(e.Item.DataItem is GridInsertionObject))
                {
                    string id = ((DataRowView)(e.Item.DataItem)).Row[AttachmentListDS.ATTACHMENTTYPE_FIELD].ToString();
                    if (id != string.Empty)
                        editor.ComboBoxControl.FindItemByValue(id).Selected = true;
                }
            }
            if (e.Item is GridDataItem)
            {

                string downloadFileURL = "~/" + ((GridDataItem)(e.Item))["FileDownLoadURL"].Text;
                var linkButton = ((GridDataItem)(e.Item))["FileDownLoadURLDownload"].Controls[1] as HtmlAnchor;
                if (linkButton != null) linkButton.Attributes.Add("href", downloadFileURL);

                string[] fileNameExtensionSplit = ((GridDataItem)(e.Item))["ORIGINPATH"].Text.Split('.');
                string fileExtendion = fileNameExtensionSplit[fileNameExtensionSplit.Length - 1].ToLower();
                string fileIcon;
                switch (fileExtendion)
                {
                    case "exe":
                        {
                            fileIcon = "\r\n<img src=\"../images/FileTypeIcons/exe-file.gif\" />\r\n";
                            break;
                        }
                    case "xls":
                    case "xlsx":
                    case "csv":
                        {
                            fileIcon = "\r\n<img src=\"../images/FileTypeIcons/excel-file.gif\" />\r\n";
                            break;
                        }
                    case "pdf":
                        {
                            fileIcon = "\r\n<img src=\"../images/FileTypeIcons/pdf.gif\" />\r\n";
                            break;
                        }
                    case "doc":
                    case "rtf":
                    case "docx":
                        {
                            fileIcon = "\r\n<img src=\"../images/FileTypeIcons/ms-word.gif\" />\r\n";
                            break;
                        }
                    case "rar":
                    case "zip":
                        {
                            fileIcon = "\r\n<img src=\"../images/FileTypeIcons/zip-file.gif\" />\r\n";
                            break;
                        }
                    case "jpg":
                    case "jpeg":
                    case "png":
                    case "gif":
                        {
                            fileIcon = "\r\n<img src=\"../images/FileTypeIcons/photoshop-file.gif\" />\r\n";
                            break;
                        }
                    default:
                        {
                            fileIcon = "\r\n<img src=\"../images/FileTypeIcons/file.gif\" />\r\n";
                            break;
                        }

                }

                ((GridDataItem)(e.Item))["FileIcon"].Text = fileIcon;
            }
        }
        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (PresentationData == null)
                GetBMCData();
            if (PresentationData != null && PresentationData.Tables.Contains(AttachmentListDS.ATTACHMENTTABLE))
            {
                var view = new DataView(PresentationData.Tables[AttachmentListDS.ATTACHMENTTABLE])
                    {
                        Sort = AttachmentListDS.ATTACHMENTDATEIMPORTED_FIELD + " DESC"
                    };
                PresentationGrid.DataSource = view.ToTable();
            }
        }

        protected void Upload_Click(object sender, EventArgs e)
        {
            if (PresentationData == null)
                GetBMCData();

            if (PresentationData != null)
            {
                DataTable objAttachmentsTable = PresentationData.Tables[AttachmentListDS.ATTACHMENTTABLE];
                objAttachmentsTable.AcceptChanges();

                var clientData = Broker.GetBMCInstance(new Guid(CID)) as IOrganizationUnit;

                var objUser = (DBUser)Broker.GetBMCInstance(Broker.UserContext.Identity.Name, "DBUser_1_1");

                foreach (UploadedFile uploadedFile in AsyncUpload.UploadedFiles)
                {
                    DataRow objDataRow = objAttachmentsTable.NewRow();
                    objDataRow[AttachmentListDS.ATTACHMENTID_FIELD] = Guid.NewGuid();
                    if (description.Text != string.Empty)
                        objDataRow[AttachmentListDS.ATTACHMENTDESCRIPTION_FIELD] = description.Text;
                    else
                        objDataRow[AttachmentListDS.ATTACHMENTDESCRIPTION_FIELD] = uploadedFile.FileName;

                    objDataRow[AttachmentListDS.ATTACHMENTDATEIMPORTED_FIELD] = DateTime.Now;
                    objDataRow[AttachmentListDS.ATTACHMENTORIGINPATH_FIELD] = uploadedFile.FileName;
                    objDataRow[AttachmentListDS.ATTACHMENTIMPORTEDBY_FIELD] = objUser.Name;
                    objDataRow[AttachmentListDS.ATTACHMENTFILE_FIELD] = uploadedFile.InputStream;
                    objDataRow[AttachmentListDS.ATTACHMENTTYPE_FIELD] = int.Parse(cmbDocuType.SelectedValue);
                    objDataRow[AttachmentListDS.ATTACHMENTLINKED_FIELD] = false;
                    objAttachmentsTable.Rows.Add(objDataRow);
                }

                SaveClientData(new Guid(CID), PresentationData);
                Response.Redirect(Request.Url.AbsoluteUri);
                if (clientData != null)
                    Broker.LogEvent(EventType.UMAClientDoocumentUpload, clientData.CID, "The attachments of Client: " + clientData.EclipseClientID + "-" + clientData.Name + " Added");
            }
        }

        protected string TrimDescription(string desc)
        {
            if (!string.IsNullOrEmpty(desc) && desc.Length > 200)
            {
                return string.Concat(desc.Substring(0, 200), "...");
            }
            return desc;
        }

        public void SaveClientData(Guid cid, DataSet ds)
        {
            Broker.SaveOverride = true;
            IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
            clientData.SetData(ds);
            Broker.SetComplete();
            Broker.SetStart();
        }
    }
}