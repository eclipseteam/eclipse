﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Words;
using Aspose.Words.Tables;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;
using System.Web;
using System.IO;
using Table = Aspose.Words.Tables.Table;
using Oritax.TaxSimp.Utilities;

namespace eclipseonlineweb.Controls
{
    public partial class SellOrder : UserControl
    {
        public string ClientCID
        {
            private get
            {
                return hfClientCID.Value;
            }
            set
            {
                hfClientCID.Value = value;
            }
        }

        private string ClientAccID
        {
            get
            {
                return hfClientID.Value;
            }
            set
            {
                hfClientID.Value = value;
            }
        }

        public bool IsAdmin
        {
            private get
            {
                return Convert.ToBoolean(hfIsAdmin.Value);
            }
            set
            {
                hfIsAdmin.Value = value.ToString();
            }
        }

        public bool IsAdminMenu
        {
            private get
            {
                if (string.IsNullOrEmpty(hfIsAdminMenu.Value))
                {
                    hfIsAdminMenu.Value = "false";
                }
                return Convert.ToBoolean(hfIsAdminMenu.Value);
            }
            set
            {
                hfIsAdminMenu.Value = value.ToString();
            }
        }

        private bool IsSMA
        {
            get
            {
                if (string.IsNullOrEmpty(hfIsSMA.Value))
                {
                    hfIsSMA.Value = "false";
                }
                return Convert.ToBoolean(hfIsSMA.Value);
            }
            set
            {
                hfIsSMA.Value = value.ToString();
            }
        }

        private string OrderNumber { get; set; }

        private SerializableDictionary<string, byte[]> output { get; set; }

        public Action<string, DataSet> SaveData { get; set; }

        private ICMBroker Broker
        {
            get
            {
                var umaBasePage = Page as UMABasePage;
                return umaBasePage != null ? umaBasePage.UMABroker : null;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            searchAccountControl.SelectData += (clientCID, clientId, clientName) =>
            {
                ClientHeaderInfo1.SetEntity(new Guid(clientCID));
                ClientHeaderInfo1.Visible = true;
                ClientCID = clientCID;
                ClientAccID = clientId;
                SetClientManagementType();
                LoadControls();
                showDetails.Visible = true;
                PresentationGrid.Rebind();
                PresentationGrid.Visible = true;
                tdButtons.Visible = true;
            };

            searchAccountControl.ShowHideModal += show =>
            {
                if (show)
                {
                    popupSearch.Show();
                }
                else
                {
                    popupSearch.Hide();
                }
            };

            if (!IsPostBack)
            {
                if (IsAdminMenu)
                {
                    btnSearch.Visible = true;
                    showDetails.Visible = false;
                    ClientHeaderInfo1.Visible = false;
                    PresentationGrid.Visible = false;
                    tdButtons.Visible = false;
                    searchAccountControl.IsAdminMenu = true;
                }
                else
                {
                    SetClientManagementType();
                    btnSearch.Visible = false;
                    showDetails.Visible = true;
                    LoadControls();
                    ClientHeaderInfo1.Visible = true;
                    PresentationGrid.Visible = true;
                    tdButtons.Visible = true;
                    searchAccountControl.IsAdminMenu = false;
                }
            }
        }

        private void LoadControls()
        {
            BindServiceType();
            BindBankAccounts();
            BindOrderAccountType();
        }

        private void BindOrderAccountType()
        {
            cmbOrderAccountType.Items.Clear();
            
            if (IsSMA || Utilities.IsTestApp()) 
                cmbOrderAccountType.Items.Add(new RadComboBoxItem("ASX", "3"));
            
            if (!IsSMA)
            {
                cmbOrderAccountType.Items.Add(new RadComboBoxItem("State Street", "2"));
                cmbOrderAccountType.Items.Add(new RadComboBoxItem("At Call", "5"));
            }
            else if (IsSMA && Utilities.IsTestApp())
            {
                cmbOrderAccountType.Items.Add(new RadComboBoxItem("State Street", "2"));
            }
            cmbOrderAccountType.DataBind();
            cmbOrderAccountType.SelectedIndex = 0;
        }

        private HoldingRptDataSet GetData()
        {
            IBrokerManagedComponent clientData = Broker.GetBMCInstance(new Guid(ClientCID));
            var ds = new HoldingRptDataSet();
            clientData.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(clientData);
            return ds;
        }

        private void BindServiceType()
        {
            var ds = GetData();
            //sending At Call as we need to populate all service types
            OrderPadUtilities.FillServiceTypes(cmbServiceType, ds, IsAdmin, OrderAccountType.AtCall);
        }

        protected void PresentationGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var ds = GetData();
            string filter = string.Format("{0} > 0 and {1}='{2}'", ds.ProductBreakDownTable.HOLDING, ds.ProductBreakDownTable.SERVICETYPE, cmbServiceType.SelectedValue);
            var type = (OrderAccountType)Convert.ToInt32(cmbOrderAccountType.SelectedValue);
            switch (type)
            {
                case OrderAccountType.StateStreet:
                    filter += string.Format(" and {0}='{1}'", ds.ProductBreakDownTable.LINKEDENTITYTYPE, OrganizationType.ManagedInvestmentSchemesAccount);
                    break;
                case OrderAccountType.ASX:
                    filter += string.Format(" and {0}='{1}'", ds.ProductBreakDownTable.LINKEDENTITYTYPE, OrganizationType.DesktopBrokerAccount);
                    break;
            }

            PresentationGrid.DataSource = new DataView(ds.ProductBreakDownTable)
            {
                RowFilter = filter,
                Sort = string.Format("{0} ASC", ds.ProductBreakDownTable.INVESMENTCODE)
            };
            //for ASX
            PresentationGrid.MasterTableView.Caption = cmbOrderAccountType.SelectedValue == "3" || cmbOrderAccountType.SelectedValue == "2" ? Tooltip.OrderPadManualASX : string.Empty;
        }
        protected void btnCutOff_OnClick(object sender, EventArgs e)
        {
            FileHelper.DownLoadFile(FileHelper.CutOffTimeFilePath, FileHelper.CutOffTimeFileName);
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            //Checking Existing Order CM ID
            var umaBasePage = Page as UMABasePage;
            bool isUnit = false;
            bool isAmount = false;

            if (umaBasePage != null)
            {
                var orderCMCID = OrderPadUtilities.IsOrderCmExists(Broker, umaBasePage.GetCurrentUser());
                var unit = new OrganizationUnit
                    {
                        Name = "Order " + DateTime.Now.ToString("dd MMM, yyyy hh:mm:ss"),
                        Type = ((int)OrganizationType.Order).ToString(CultureInfo.InvariantCulture),
                        CurrentUser = umaBasePage.GetCurrentUser()
                    };
                var ds = new OrderPadDS
                    {
                        CommandType = DatasetCommandTypes.Add,
                        Unit = unit,
                        Command = (int)WebCommands.AddNewOrganizationUnit
                    };

                var orderAccountType = (OrderAccountType)Convert.ToInt32(cmbOrderAccountType.SelectedValue);
                switch (orderAccountType)
                {
                    case OrderAccountType.StateStreet:
                    case OrderAccountType.ASX:
                        GridDataItemCollection items = PresentationGrid.MasterTableView.Items;
                        foreach (GridDataItem dataItem in items)
                        {
                            if (isUnit && isAmount) break;
                            var ddlOrderPreferedBy = (dataItem.FindControl("ddlOrderPreferedBy") as DropDownList);
                            OrderPreferedBy opb = (OrderPreferedBy)Enum.Parse(typeof(OrderPreferedBy), ddlOrderPreferedBy.SelectedValue);
                            switch (opb)
                            {
                                case OrderPreferedBy.Amount:
                                    var txtAmount = (dataItem.FindControl("txtAmount") as RadNumericTextBox);
                                    if (txtAmount != null && !string.IsNullOrEmpty(txtAmount.Text))
                                        isAmount = true;
                                    break;
                                case OrderPreferedBy.Units:
                                    var txtUnits = (dataItem.FindControl("txtUnits") as RadNumericTextBox);
                                    if (txtUnits != null && !string.IsNullOrEmpty(txtUnits.Text))
                                        isUnit = true;
                                    break;
                            }
                        }

                        if (isAmount)
                            AddRows(ds, orderAccountType, OrderPreferedBy.Amount);
                        if (isUnit)
                            AddRows(ds, orderAccountType, OrderPreferedBy.Units);
                        break;
                    default:
                        isAmount = true;
                        AddRows(ds, orderAccountType, OrderPreferedBy.Amount);
                        break;
                }

                if (SaveData != null)
                {
                    SaveData(orderCMCID.ToString(), ds);
                }

                divMsg.Visible = true;
                divMsg.InnerHtml = ds.ExtendedProperties["Message"].ToString().Replace("\r\n", "<br/>");

                if (ds.ExtendedProperties["Result"].ToString() == OperationResults.Successfull.ToString())
                {
                    string path = string.Empty;

                    if (isAmount)
                    {
                        DataRow dr = ds.Tables[ds.OrdersTable.TABLENAME].Select(string.Format("OrderPreferedBy='{0}'", OrderPreferedBy.Amount))[0];
                        OrderNumber = divMsg.InnerText.Split(' ')[1];
                        path = CreateReceipt(ds, orderAccountType, dr[ds.OrdersTable.ID].ToString(), OrderNumber, OrderPreferedBy.Amount);
                    }

                    if (isUnit)
                    {
                        DataRow dr = ds.Tables[ds.OrdersTable.TABLENAME].Select(string.Format("OrderPreferedBy='{0}'", OrderPreferedBy.Units))[0];
                        if (isAmount)
                            OrderNumber = divMsg.InnerText.Split('>')[1].Split(' ')[1];
                        else
                            OrderNumber = divMsg.InnerText.Split(' ')[1];

                        if (!string.IsNullOrEmpty(path))
                        {
                            path = path.Insert(path.IndexOf('_') + 1, string.Format("{0}_", OrderNumber));
                            CreateReceipt(ds, orderAccountType, dr[ds.OrdersTable.ID].ToString(), OrderNumber, OrderPreferedBy.Units);
                        }
                        else
                            path = CreateReceipt(ds, orderAccountType, dr[ds.OrdersTable.ID].ToString(), OrderNumber, OrderPreferedBy.Units);

                    }

                    byte[] bytes = null;
                    if (isUnit && isAmount)
                    {
                        bytes = Zipper.CreateZip(output);
                        path = path.Replace(".PDF", ".zip");
                    }
                    else
                        bytes = output[path];

                    System.IO.File.WriteAllBytes(string.Format("{0}\\OrderReceipts\\{1}", HttpContext.Current.Server.MapPath("~/App_Data"), path), bytes);

                    divMsg.InnerHtml = string.Format("{0} Click <a href = '../SysAdministration/DownloadOrderReceipt.aspx?filename={1}'>here</a> to print its receipt.", divMsg.InnerHtml, path);



                    if (ds.ExtendedProperties.Contains("SMAMessages"))
                    {
                        var results = (Dictionary<long, string>)ds.ExtendedProperties["SMAMessages"];
                        if (results.Count > 0)
                        {
                            var messag = string.Empty;
                            foreach (KeyValuePair<long, string> result in results)
                            {
                                messag += string.Format("Order: {0},{2}{1}{2}", result.Key, result.Value, Environment.NewLine);
                            }
                            string smaErrorFile = GetNewFilePath(orderAccountType, "txt");
                            Utilities.SaveToFile(messag, smaErrorFile);

                            divMsg.InnerHtml += string.Format("To view Super service errors click <a href = '../SysAdministration/DownloadOrderReceipt.aspx?filename={0}'>Here</a> ", FileHelper.GetFileName(smaErrorFile));
                            ShowAlert("ShowServiceErrors", "There were errors sending data to Super Service.");
                        }
                    }


                    BindBankAccounts();
                    switch (orderAccountType)
                    {
                        case OrderAccountType.TermDeposit:
                            TDPresentationGrid.Rebind();
                            break;
                        case OrderAccountType.StateStreet:
                        case OrderAccountType.ASX:
                            PresentationGrid.Rebind();
                            break;
                        case OrderAccountType.AtCall:
                            SellAtCallGrid.Rebind();
                            break;
                    }
                }
            }
        }

        public void ShowAlert(string key, string message)
        {

            ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), key, string.Format("alert('{0}');", message), true);
        }

        private void AddRows(OrderPadDS ds, OrderAccountType orderAccountType, OrderPreferedBy orderPreferedBy)
        {

            //Order Row
            var orderId = AddOrderRow(ds, orderAccountType, orderPreferedBy);

            GridDataItemCollection items = null;
            switch (orderAccountType)
            {
                case OrderAccountType.TermDeposit:
                    items = TDPresentationGrid.MasterTableView.Items;
                    break;
                case OrderAccountType.StateStreet:
                case OrderAccountType.ASX:
                    items = PresentationGrid.MasterTableView.Items;
                    break;
                case OrderAccountType.AtCall:
                    items = SellAtCallGrid.MasterTableView.Items;
                    break;
            }

            if (items != null)
                foreach (GridDataItem dataItem in items)
                {
                    if (orderAccountType == OrderAccountType.ASX || orderAccountType == OrderAccountType.StateStreet)
                    {
                        var ddlOrderPreferedBy = (dataItem.FindControl("ddlOrderPreferedBy") as DropDownList);
                        OrderPreferedBy opb = (OrderPreferedBy)Enum.Parse(typeof(OrderPreferedBy), ddlOrderPreferedBy.SelectedValue);

                        if (opb != orderPreferedBy)
                        { continue; }
                    }

                    decimal amount = 0;
                    decimal units = 0;
                    var txtAmount = (dataItem.FindControl("txtAmount") as RadNumericTextBox);
                    var txtUnits = (dataItem.FindControl("txtUnits") as RadNumericTextBox);

                    if (orderAccountType == OrderAccountType.AtCall)
                    {
                        if (txtAmount != null && !string.IsNullOrEmpty(txtAmount.Text))
                        {
                            amount = Convert.ToDecimal(txtAmount.Text.Trim());
                            units = 0;
                        }
                    }
                    else
                    {
                        decimal unitPrice = Convert.ToDecimal(dataItem["UnitPrice"].Text);
                        if (txtUnits != null && !string.IsNullOrEmpty(txtUnits.Text))
                        {
                            units = Convert.ToDecimal(txtUnits.Text.Trim());
                            amount = units * unitPrice;
                        }
                        else if (txtAmount != null && !string.IsNullOrEmpty(txtAmount.Text))
                        {
                            amount = Convert.ToDecimal(txtAmount.Text.Trim());
                            units = amount / unitPrice;
                            if (orderAccountType == OrderAccountType.ASX)
                            {
                                units = decimal.Floor(units);
                            }
                        }
                    }

                    if (units > 0 || amount > 0)
                    {
                        //Order Items Row
                        AddItemRow(ds, amount, dataItem, orderId, orderAccountType, units);
                    }
                }
        }

        private Guid AddOrderRow(OrderPadDS ds, OrderAccountType orderAccountType, OrderPreferedBy orderPreferedBy)
        {
            DataRow orderRow = ds.OrdersTable.NewRow();

            Guid orderId = Guid.NewGuid();
            orderRow[ds.OrdersTable.ID] = orderId;
            orderRow[ds.OrdersTable.CLIENTCID] = new Guid(ClientCID);
            orderRow[ds.OrdersTable.CLIENTID] = ClientHeaderInfo1.ClientId ?? ClientAccID;
            orderRow[ds.OrdersTable.ORDERACCOUNTTYPE] = orderAccountType.ToString();
            orderRow[ds.OrdersTable.ORDERTYPE] = OrderType.Manual;
            orderRow[ds.OrdersTable.STATUS] = OrderStatus.Active;
            orderRow[ds.OrdersTable.ORDERITEMTYPE] = OrderItemType.Sell;
            if (cmbCashAccount.Items.Count > 0)
            {
                orderRow[ds.OrdersTable.ACCOUNTCID] = new Guid(cmbCashAccount.SelectedValue);
                orderRow[ds.OrdersTable.ACCOUNTNUMBER] = cmbCashAccount.SelectedItem.Attributes["AccountNo"].Trim();
                orderRow[ds.OrdersTable.ACCOUNTBSB] = cmbCashAccount.SelectedItem.Attributes["BSB"].Trim();
            }
            orderRow[ds.OrdersTable.CLIENTMANAGEMENTTYPE] = IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA;
            orderRow[ds.OrdersTable.ORDERPREFEREDBY] = orderPreferedBy;

            ds.OrdersTable.Rows.Add(orderRow);
            return orderId;
        }

        private void AddItemRow(OrderPadDS ds, decimal amount, GridDataItem dataItem, Guid orderId, OrderAccountType orderAccountType, decimal units)
        {
            switch (orderAccountType)
            {
                case OrderAccountType.TermDeposit:
                    DataRow termDepositItemsRow = ds.TermDepositTable.NewRow();

                    termDepositItemsRow[ds.TermDepositTable.ID] = Guid.NewGuid();
                    termDepositItemsRow[ds.TermDepositTable.ORDERID] = orderId;
                    termDepositItemsRow[ds.TermDepositTable.AMOUNT] = amount;
                    termDepositItemsRow[ds.TermDepositTable.BROKERID] = new Guid(dataItem["BrokerId"].Text);
                    termDepositItemsRow[ds.TermDepositTable.BANKID] = new Guid(dataItem["InstituteId"].Text);
                    termDepositItemsRow[ds.TermDepositTable.PRODUCTID] = new Guid(dataItem["ProductID"].Text);
                    termDepositItemsRow[ds.TermDepositTable.TDACCOUNTCID] = new Guid(dataItem["TDAccountCID"].Text);
                    termDepositItemsRow[ds.TermDepositTable.CONTRACTNOTE] = new Guid(dataItem["ContractNote"].Text);

                    ds.TermDepositTable.Rows.Add(termDepositItemsRow);
                    break;
                case OrderAccountType.StateStreet:
                    DataRow stateStreetItemsRow = ds.StateStreetTable.NewRow();

                    stateStreetItemsRow[ds.StateStreetTable.ID] = Guid.NewGuid();
                    stateStreetItemsRow[ds.StateStreetTable.ORDERID] = orderId;
                    stateStreetItemsRow[ds.StateStreetTable.PRODUCTID] = new Guid(dataItem["ProductID"].Text);
                    stateStreetItemsRow[ds.StateStreetTable.FUNDCODE] = dataItem["InvestmentCode"].Text;
                    stateStreetItemsRow[ds.StateStreetTable.FUNDNAME] = dataItem["InvestmentName"].Text;
                    stateStreetItemsRow[ds.StateStreetTable.UNITS] = units;
                    stateStreetItemsRow[ds.StateStreetTable.AMOUNT] = amount;
                    ds.StateStreetTable.Rows.Add(stateStreetItemsRow);
                    break;
                case OrderAccountType.ASX:
                    DataRow asxItemsRow = ds.ASXTable.NewRow();

                    asxItemsRow[ds.ASXTable.ID] = Guid.NewGuid();
                    asxItemsRow[ds.ASXTable.ORDERID] = orderId;
                    asxItemsRow[ds.ASXTable.ASXUNITPRICE] = dataItem["UnitPrice"].Text;
                    asxItemsRow[ds.ASXTable.UNITS] = units;
                    asxItemsRow[ds.ASXTable.AMOUNT] = amount;
                    asxItemsRow[ds.ASXTable.INVESTMENTCODE] = dataItem["InvestmentCode"].Text;
                    asxItemsRow[ds.ASXTable.INVESTMENTNAME] = dataItem["InvestmentName"].Text;
                    asxItemsRow[ds.ASXTable.PRODUCTID] = new Guid(dataItem["ProductID"].Text);

                    ds.ASXTable.Rows.Add(asxItemsRow);

                    //Getting ASXCID and ASXAccount Number 
                    IBrokerManagedComponent clientData = Broker.GetBMCInstance(new Guid(ClientCID));
                    if (ds.ExtendedProperties.Count >= 0 && ds.ExtendedProperties["ServiceType"] == null)
                        ds.ExtendedProperties.Add("ServiceType", cmbServiceType.SelectedValue);
                    clientData.GetData(ds);
                    Broker.ReleaseBrokerManagedComponent(clientData);
                    break;
                case OrderAccountType.AtCall:
                    DataRow drAtCall = ds.AtCallTable.NewRow();

                    drAtCall[ds.AtCallTable.ID] = Guid.NewGuid();
                    drAtCall[ds.AtCallTable.ORDERID] = orderId;
                    drAtCall[ds.AtCallTable.AMOUNT] = amount;
                    drAtCall[ds.AtCallTable.PRODUCTID] = new Guid(dataItem["ProductId"].Text);
                    drAtCall[ds.AtCallTable.BROKERID] = new Guid(dataItem["BrokerId"].Text);
                    drAtCall[ds.AtCallTable.BANKID] = new Guid(dataItem["InstituteId"].Text);
                    drAtCall[ds.AtCallTable.ATCALLTYPE] = AtCallType.MoneyMovementBroker;
                    ds.AtCallTable.Rows.Add(drAtCall);

                    break;
            }
        }

        protected void cmbOrderAccountType_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            var type = (OrderAccountType)Convert.ToInt32(cmbOrderAccountType.SelectedValue);
            switch (type)
            {
                case OrderAccountType.TermDeposit:
                    PresentationGrid.Visible = false;
                    TDPresentationGrid.Visible = true;
                    SellAtCallGrid.Visible = false;
                    TDPresentationGrid.Rebind();
                    break;
                case OrderAccountType.StateStreet:
                case OrderAccountType.ASX:
                    TDPresentationGrid.Visible = false;
                    PresentationGrid.Visible = true;
                    SellAtCallGrid.Visible = false;
                    PresentationGrid.Rebind();
                    break;
                case OrderAccountType.AtCall:
                    PresentationGrid.Visible = false;
                    TDPresentationGrid.Visible = false;
                    SellAtCallGrid.Visible = true;
                    SellAtCallGrid.Rebind();
                    break;
            }
            divMsg.Visible = false;
            divMsg.InnerHtml = string.Empty;
        }

        protected void TDPresentationGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetTDAtCallData("TD");
        }

        protected void SellAtCallGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetTDAtCallData("At Call");
        }

        protected void SellAtCallGrid_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var item = e.Item as GridDataItem;
            var txtAmount = item.FindControl("txtAmount") as RadNumericTextBox;
            decimal amount = Convert.ToDecimal(item["Amount"].Text);
            var chkSetHolding = item.FindControl("chkSetHolding") as CheckBox;
            chkSetHolding.Attributes.Add("onclick", string.Format("javascript:SetHoldingForAtCall('{0}','{1}',this);", txtAmount.ClientID, amount));
        }

        private void GetTDAtCallData(string type)
        {
            var ds = GetData();
            string filter = string.Format("{0} And {1}>0", ds.HoldingSummaryTable.PRODUCTNAME + " LIKE '%" + type + "%'", ds.HoldingSummaryTable.TOTAL);
            DataView dv = ds.HoldingSummaryTable.DefaultView;
            dv.RowFilter = filter;
            dv.Sort = string.Format("{0} ASC", ds.HoldingSummaryTable.PRODUCTNAME);
            switch (type)
            {
                case "TD":
                    TDPresentationGrid.DataSource = dv;
                    break;
                case "At Call":
                    SellAtCallGrid.DataSource = dv;
                    break;
            }
        }

        protected void PresentationGrid_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var item = e.Item as GridDataItem;
            var cvUnits = item.FindControl("cvUnits") as CompareValidator;
            var cvAmount = item.FindControl("cvAmount") as CompareValidator;
            var txtUnits = item.FindControl("txtUnits") as RadNumericTextBox;
            var txtAmount = item.FindControl("txtAmount") as RadNumericTextBox;
            txtAmount.ReadOnly = true;

            if (cvUnits == null || txtUnits == null) return;

            var type = (OrderAccountType)Convert.ToInt32(cmbOrderAccountType.SelectedValue);
            switch (type)
            {
                case OrderAccountType.StateStreet:
                    txtUnits.NumberFormat.DecimalDigits = 4;
                    cvUnits.Type = ValidationDataType.Double;
                    SetSellUnitsNPrice(item, cvAmount);
                    break;
                case OrderAccountType.ASX:
                    txtUnits.NumberFormat.DecimalDigits = 0;
                    cvUnits.Type = ValidationDataType.Double;
                    SetSellUnitsNPrice(item, cvAmount);
                    break;
            }

            var chkSetHolding = item.FindControl("chkSetHolding") as CheckBox;
            var ddlOrderPreferedBy = item.FindControl("ddlOrderPreferedBy") as DropDownList;


            chkSetHolding.Attributes.Add("onclick", string.Format("javascript:SetHolding('{0}','{1}','{2}','{3}','{4}',this);", txtUnits.ClientID, txtAmount.ClientID, item["Units"].Text, item["CurrentValue"].Text.Remove(0, 1), ddlOrderPreferedBy.ClientID));
            ddlOrderPreferedBy.Attributes.Add("onchange", string.Format("javascript:return ddlOrderPreferedBy_OnSelectedIndexChange(this,'{0}','{1}','{2}','{3}','{4}');", txtUnits.ClientID, txtAmount.ClientID, item["Units"].Text, item["CurrentValue"].Text.Remove(0, 1), chkSetHolding.ClientID));

        }

        private void SetSellUnitsNPrice(GridDataItem item, CompareValidator cvAmount)
        {
            //Units
            decimal unsettledSellUnits = Convert.ToDecimal(item["UnsettledSell"].Text);
            decimal sellUnits = Convert.ToDecimal(item["Units"].Text);
            sellUnits = sellUnits + unsettledSellUnits;
            item["Units"].Text = sellUnits.ToString(CultureInfo.InvariantCulture);

            //TotalVaue
            decimal unitPrice = Convert.ToDecimal(item["UnitPrice"].Text);
            decimal totalValue = sellUnits * unitPrice;
            item["CurrentValue"].Text = totalValue.ToString("C");
            cvAmount.ValueToCompare = Math.Round(totalValue, 2).ToString();
        }

        protected void cmbServiceType_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindBankAccounts();

            switch (Convert.ToInt32(cmbOrderAccountType.SelectedValue))
            {
                case 1:
                    TDPresentationGrid.Rebind();
                    break;
                case 5:
                    SellAtCallGrid.Rebind();
                    break;
                default:
                    PresentationGrid.Rebind();
                    break;
            }
        }

        protected void cmbCashAccount_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            OrderPadUtilities.SetAvailableFunds(cmbCashAccount, lblAvailableFunds, lblCashBalance, lblMinCash, IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA);
        }

        private void BindBankAccounts()
        {
            var ds = GetData();
            //Getting cash bank account according to service type
            string filter = string.Format("{0}='{1}' and {2}='{3}' and {4}='cash'", ds.HoldingSummaryTable.LINKEDENTITYTYPE, OrganizationType.BankAccount, ds.HoldingSummaryTable.SERVICETYPE, cmbServiceType.SelectedValue, ds.HoldingSummaryTable.ASSETNAME);
            OrderPadUtilities.FillBankNAvailableFunds(cmbCashAccount, ds, filter, IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA, Broker);
            OrderPadUtilities.SetAvailableFunds(cmbCashAccount, lblAvailableFunds, lblCashBalance, lblMinCash, IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA);
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            popupSearch.Show();
            showDetails.Visible = false;
            ClientHeaderInfo1.Visible = false;
            PresentationGrid.Visible = false;
            tdButtons.Visible = false;
            searchAccountControl.HideModal = false;
            searchAccountControl.FindControl("SearchBox").Focus();
        }
        private BankAccountDS GetDataSet(Guid cid)
        {
            var bankTransactionDetailsDS = new BankAccountDS { CommandType = DatasetCommandTypes.Get };
            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
                clientData.GetData(bankTransactionDetailsDS);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }
            return bankTransactionDetailsDS;
        }
        private string GetNewFilePath(OrderAccountType orderAccountType, string ext)
        {
            string fileName = string.Empty;
            switch (orderAccountType)
            {
                case OrderAccountType.StateStreet:
                    fileName = string.Format("{0}_{1}_SellMIS_{2}.{3}", ClientHeaderInfo1.ClientId ?? ClientAccID, OrderNumber, DateTime.Now.ToShortDateString().Replace("/", "-"), ext);
                    break;
                case OrderAccountType.ASX:
                    fileName = string.Format("{0}_{1}_SellASX_{2}.{3}", ClientHeaderInfo1.ClientId ?? ClientAccID, OrderNumber, DateTime.Now.ToShortDateString().Replace("/", "-"), ext);
                    break;
                case OrderAccountType.AtCall:
                    fileName = string.Format("{0}_{1}_ATCALL_{2}.{3}", ClientHeaderInfo1.ClientId ?? ClientAccID, OrderNumber, DateTime.Now.ToShortDateString().Replace("/", "-"), ext);
                    break;

            }
            string dirOrderReceipts = string.Format("{0}\\OrderReceipts", HttpContext.Current.Server.MapPath("~/App_Data"));
            if (!Directory.Exists(dirOrderReceipts))
            {
                Directory.CreateDirectory(dirOrderReceipts);
            }
            string filePath = string.Format("{0}\\{1}", dirOrderReceipts, fileName);
            return filePath;
        }

        private void SetTDData(DataTable dt)
        {
            Guid BankID = Guid.Empty;
            Guid BrokerID = Guid.Empty;

            foreach (DataRow row in dt.Rows)
            {
                BankID = new Guid(row["BankID"].ToString());
                BrokerID = new Guid(row["BrokerID"].ToString());

                // Get/Set the Bank and Broker by their IDs
                var inst = (Broker.GetWellKnownBMC(WellKnownCM.Organization) as Oritax.TaxSimp.CM.Organization.IOrganization).Institution;
                InstitutionEntity ie = inst.FirstOrDefault(ss => ss.ID == BankID);
                row["ADI"] = ie.Name;
                ie = inst.FirstOrDefault(ss => ss.ID == BrokerID);
                row["Broker"] = ie.Name;
            }
        }

        private string CreateReceipt(OrderPadDS ds, OrderAccountType orderAccountType, string OrderID, string OrderNumber, OrderPreferedBy orderPreferedBy)
        {
            int bodyFontSize = 9;
            double sum = 0;
            DataTable dt_ = new DataTable();
            DataRow clientSummaryRow = GetData().Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0];
            string OutPath = string.Empty;
            string path = string.Empty;
            Document document = null;
            //This is portion for the Header Section of the Document
            switch (orderAccountType)
            {
                case OrderAccountType.StateStreet:
                    ds.Tables[ds.StateStreetTable.TABLENAME].DefaultView.RowFilter = string.Format("OrderID='{0}'", OrderID);
                    dt_ = ds.Tables[ds.StateStreetTable.TABLENAME].DefaultView.ToTable(false, "FundCode", "FundName", "Units", "Amount");
                    path = Server.MapPath("~/Templates/OrderPad_SellMIS_Template.docx");
                    document = new Document(path);
                    document.Range.Replace("[OrderNo]", string.Format("Order {0}", OrderNumber), false, false);
                    document.Range.Replace("[Eclipse_Advisrer]", clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME].ToString(), false, false);
                    document.Range.Replace("[Eclipse_ServicesTyep]", cmbServiceType.SelectedItem.Text.ToUpper(), false, false);
                    break;
                case OrderAccountType.ASX:
                    ds.Tables[ds.ASXTable.TABLENAME].DefaultView.RowFilter = string.Format("OrderID='{0}'", OrderID);
                    dt_ = ds.Tables[ds.ASXTable.TABLENAME].DefaultView.ToTable(false, "InvestmentCode", "InvestmentName", "Units", "Amount");
                    path = Server.MapPath("~/Templates/OrderPad_SellASX_Template.docx");
                    document = new Document(path);
                    document.Range.Replace("[OrderNo]", string.Format("Order {0}", OrderNumber), false, false);
                    document.Range.Replace("[Eclipse_Advisrer]", clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME].ToString(), false, false);
                    document.Range.Replace("[Eclipse_ServicesTyep]", cmbServiceType.SelectedItem.Text.ToUpper(), false, false);
                    break;
                case OrderAccountType.AtCall:
                    dt_ = ds.Tables[ds.AtCallTable.TABLENAME];
                    path = Server.MapPath("~/Templates/OrderPad_SellAtCall_Template1.docx");
                    document = new Document(path);
                    document.Range.Replace("[Eclipse_OrderNo]", string.Format("Order {0}", OrderNumber), false, false);
                    document.Range.Replace("[Eclipse_Adviser]", clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME].ToString(), false, false);
                    document.Range.Replace("[Eclipse_ServiceType]", cmbServiceType.SelectedItem.Text.ToUpper(), false, false);
                    break;
            }
            document.Range.Replace("[Eclipse_Dat]", DateTime.Now.ToString("dd/MM/yyyy"), false, false);
            document.Range.Replace("[Eclipse_OrderBy]", ((Page as UMABasePage).GetCurrentUser()).CurrentUserName, false, false);
            document.Range.Replace("[Eclipse_InvestorName]", clientSummaryRow[HoldingRptDataSet.CLIENTNAME].ToString(), false, false);
            document.Range.Replace("[Eclipse_ClientID]", ClientHeaderInfo1.ClientId ?? ClientAccID, false, false);
            document.Range.Replace("[Eclipse_AccountType]", clientSummaryRow[HoldingRptDataSet.CLIENTTYPE].ToString(), false, false);


            document.Range.Replace("[A11]", clientSummaryRow[HoldingRptDataSet.CLIENTNAME].ToString(), false, false);
            string addressLine1And2 = string.Empty;
            if (clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2].ToString() == string.Empty)
                addressLine1And2 = clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1].ToString();
            else
                addressLine1And2 = clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1].ToString() + ", " + clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2].ToString();
            document.Range.Replace("[A12]", addressLine1And2, false, false);
            document.Range.Replace("[A13]", string.Format("{0} {1} {2}", clientSummaryRow[HoldingRptDataSet.SUBURB], clientSummaryRow[HoldingRptDataSet.STATE], clientSummaryRow[HoldingRptDataSet.POSTCODE]), false, false);
            document.Range.Replace("[A14]", clientSummaryRow[HoldingRptDataSet.COUNTRY].ToString(), false, false);
            if (document != null)
            {
                OutPath = GetNewFilePath(orderAccountType, "PDF");
                //Now here we try to Make Dynamic Table
                DocumentBuilder builderExl = new DocumentBuilder(document);
                //This is Point where to Start Generate Table
                switch (orderAccountType)
                {
                    case OrderAccountType.StateStreet:
                        builderExl.MoveToBookmark("EclipseOrderPadSellAsxMis", false, true);
                        break;
                    case OrderAccountType.ASX:
                        builderExl.MoveToBookmark("EclipseOrderPadSellAsxMis", false, true);
                        break;
                    case OrderAccountType.AtCall:
                        builderExl.MoveToBookmark("EclipseCashTransfer", false, true);
                        break;
                }
                Table table = builderExl.StartTable();
                builderExl.PageSetup.LeftMargin = ConvertUtil.InchToPoint(1);
                builderExl.PageSetup.TopMargin = ConvertUtil.InchToPoint(1);
                builderExl.PageSetup.BottomMargin = ConvertUtil.InchToPoint(1);

                builderExl.StartTable();

                builderExl.InsertCell();
                builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                builderExl.CellFormat.Borders.LineWidth = 0;
                builderExl.EndRow();
                builderExl.EndTable();
                builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Left;
                builderExl.Writeln("");
                switch (orderAccountType)
                {
                    case OrderAccountType.StateStreet: //OrderPad_SellMIS_Template.docx
                        //Header Start
                        AddCellToSheet(builderExl, "FundCode", 20, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                        AddCellToSheet(builderExl, "FundName", 40, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                        AddCellToSheet(builderExl, "Units", 20, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                        AddCellToSheet(builderExl, (orderPreferedBy == OrderPreferedBy.Amount ? "Amount" : "Estimated Amount"), 20, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                        builderExl.EndRow();
                        //Header End
                        for (int i = 0; i < dt_.Rows.Count; i++)
                        {
                            //Insert Data Start
                            AddCellToSheet(builderExl, dt_.Rows[i]["FundCode"].ToString(), 20, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                            AddCellToSheet(builderExl, dt_.Rows[i]["FundName"].ToString(), 40, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                            AddCellToSheet(builderExl, (orderPreferedBy == OrderPreferedBy.Units ? dt_.Rows[i]["Units"].ToString() : ""), 20, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                            AddCellToSheet(builderExl, string.Format("{0:C}", dt_.Rows[i]["Amount"]), 20, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                            builderExl.EndRow(); sum += Convert.ToDouble(dt_.Rows[i]["Amount"]);
                            //End Data Insert
                        }
                        //Here we add the Total Colunm
                        AddCellToSheet(builderExl, "Total", 20, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                        AddCellToSheet(builderExl, string.Empty, 40, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                        AddCellToSheet(builderExl, string.Empty, 20, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                        AddCellToSheet(builderExl, string.Format("{0:C}", sum), 20, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                        builderExl.EndRow();
                        break;
                    case OrderAccountType.ASX: //OrderPad_SellASX_Template.docx
                        //Header Start
                        AddCellToSheet(builderExl, "InvestmentCode", 20, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                        AddCellToSheet(builderExl, "InvestmentName", 40, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                        AddCellToSheet(builderExl, "Units", 20, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                        AddCellToSheet(builderExl, (orderPreferedBy == OrderPreferedBy.Amount ? "Amount" : "Estimated Amount"), 20, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                        builderExl.EndRow();
                        //Header End
                        for (int i = 0; i < dt_.Rows.Count; i++)
                        {
                            AddCellToSheet(builderExl, dt_.Rows[i]["InvestmentCode"].ToString(), 20, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                            AddCellToSheet(builderExl, dt_.Rows[i]["InvestmentName"].ToString(), 40, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                            AddCellToSheet(builderExl, (orderPreferedBy == OrderPreferedBy.Units ? dt_.Rows[i]["Units"].ToString() : ""), 20, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                            AddCellToSheet(builderExl, string.Format("{0:C}", dt_.Rows[i]["Amount"]), 20, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                            builderExl.EndRow();
                            sum += Convert.ToDouble(dt_.Rows[i]["Amount"]);
                        }
                        //Here we add the Total Colunm
                        AddCellToSheet(builderExl, "Total", 20, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                        AddCellToSheet(builderExl, string.Empty, 40, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                        AddCellToSheet(builderExl, string.Empty, 20, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                        AddCellToSheet(builderExl, string.Format("{0:C}", sum), 20, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                        builderExl.EndRow();
                        break;
                    case OrderAccountType.AtCall: //OrderPad_SellAtCall_Template1.docx
                        DataTable dtAtCall = new DataTable();
                        dtAtCall = ds.Tables[ds.AtCallTable.TABLENAME].DefaultView.ToTable(false, "Amount", "BankID",
                                                                                           "BrokerID");
                        DataRow drOrder = ds.Tables[ds.OrdersTable.TABLENAME].Rows[0];

                        DataColumn dcBSB = new DataColumn("BSB");
                        dcBSB.DefaultValue = drOrder[ds.OrdersTable.ACCOUNTBSB].ToString();
                        dt_.Columns.Add(dcBSB);
                        dt_.Columns["BSB"].SetOrdinal(0);
                        DataColumn dcAccountNo = new DataColumn("AccountNo");
                        dcAccountNo.DefaultValue = drOrder[ds.OrdersTable.ACCOUNTNUMBER].ToString();
                        dt_.Columns.Add(dcAccountNo);
                        dt_.Columns["AccountNo"].SetOrdinal(1);
                        DataColumn dcAccountName = new DataColumn("AccountName");
                        dcAccountName.DefaultValue = GetDataSet(new Guid(drOrder[ds.OrdersTable.ACCOUNTCID].ToString())).Tables[0].Rows[0]["AccountName"].ToString();
                        dt_.Columns.Add(dcAccountName);
                        dt_.Columns["AccountName"].SetOrdinal(2);
                        DataColumn dcADI = new DataColumn("ADI");
                        dcADI.DefaultValue = string.Empty;
                        dt_.Columns.Add(dcADI);
                        DataColumn dcBroker = new DataColumn("Broker");
                        dcBroker.DefaultValue = string.Empty;
                        dt_.Columns.Add(dcBroker);
                        int count = 0;
                        SetTDData(dt_);
                        for (int i = 0; i < dt_.Rows.Count; i++)
                        {
                            count = count + 1;
                            string[] toAtCallInfo = cmbCashAccount.SelectedItem.Text.Split('-');
                            //Merage
                            AddCellToSheet(builderExl, string.Empty, 12, Color.White, true, 7, 1,
                                           CellVerticalAlignment.Center, TextOrientation.Horizontal,
                                           ParagraphAlignment.Left);
                            //To
                            AddCellToSheet(builderExl, string.Format("{0} : {1}", "Order No", count), 28, Color.DimGray, true, bodyFontSize, 1, CellVerticalAlignment.Center,
                                           TextOrientation.Horizontal, ParagraphAlignment.Center);
                            //From
                            AddCellToSheet(builderExl, string.Empty, 60, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center,
                                           TextOrientation.Horizontal, ParagraphAlignment.Left);
                            builderExl.EndRow();

                            //Heading Start
                            AddCellToSheet(builderExl, string.Empty, 12, Color.DarkGray, true, 7, 1,
                                           CellVerticalAlignment.Center, TextOrientation.Horizontal,
                                           ParagraphAlignment.Center);
                            AddCellToSheet(builderExl, "FromAccount", 28, Color.DarkGray, true, 7, 1,
                                           CellVerticalAlignment.Center, TextOrientation.Horizontal,
                                           ParagraphAlignment.Center);
                            AddCellToSheet(builderExl, "ToAccount", 60, Color.DarkGray, true, 7, 1,
                                           CellVerticalAlignment.Center, TextOrientation.Horizontal,
                                           ParagraphAlignment.Center);
                            builderExl.EndRow();
                            //Heading End
                            //Bank Name
                            AddCellToSheet(builderExl, "BankName", 12, Color.White, true, 7, 1,
                                           CellVerticalAlignment.Center, TextOrientation.Horizontal,
                                           ParagraphAlignment.Left);
                            //To
                            AddCellToSheet(builderExl, dt_.Rows[i]["ADI"].ToString(), 28, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center,
                                           TextOrientation.Horizontal, ParagraphAlignment.Left);
                            //From
                            AddCellToSheet(builderExl, toAtCallInfo[0], 60, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center,
                                           TextOrientation.Horizontal, ParagraphAlignment.Left);
                            builderExl.EndRow();
                            //ACCOUNT Name
                            AddCellToSheet(builderExl, "AccountName", 12, Color.White, true, 7, 1,
                                           CellVerticalAlignment.Center, TextOrientation.Horizontal,
                                           ParagraphAlignment.Left);
                            //To
                            AddCellToSheet(builderExl, string.Empty, 28, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center,
                                           TextOrientation.Horizontal, ParagraphAlignment.Left);
                            //From
                            AddCellToSheet(builderExl, dt_.Rows[i]["AccountName"].ToString(), 60, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center,
                                           TextOrientation.Horizontal, ParagraphAlignment.Left);
                            builderExl.EndRow();
                            //BSB
                            AddCellToSheet(builderExl, "BSB", 12, Color.White, true, bodyFontSize, 1, CellVerticalAlignment.Center,
                                           TextOrientation.Horizontal, ParagraphAlignment.Left);
                            //To
                            AddCellToSheet(builderExl, dt_.Rows[i]["BSB"].ToString(), 28, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center,
                                           TextOrientation.Horizontal, ParagraphAlignment.Left);
                            //From
                            AddCellToSheet(builderExl, cmbCashAccount.SelectedItem.Attributes["BSB"], 60, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center,
                                           TextOrientation.Horizontal, ParagraphAlignment.Left);
                            builderExl.EndRow();
                            //AccountNumber
                            AddCellToSheet(builderExl, "AccountNumber", 12, Color.White, true, 7, 1,
                                           CellVerticalAlignment.Center, TextOrientation.Horizontal,
                                           ParagraphAlignment.Left);
                            //To
                            AddCellToSheet(builderExl, dt_.Rows[i]["AccountNo"].ToString(), 28, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center,
                                           TextOrientation.Horizontal, ParagraphAlignment.Left);
                            //From
                            AddCellToSheet(builderExl, cmbCashAccount.SelectedItem.Attributes["AccountNo"], 60, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center,
                                           TextOrientation.Horizontal, ParagraphAlignment.Left);
                            builderExl.EndRow();
                            //Broker Number
                            AddCellToSheet(builderExl, "Broker", 12, Color.White, true, 7, 1,
                                           CellVerticalAlignment.Center, TextOrientation.Horizontal,
                                           ParagraphAlignment.Left);
                            //To
                            AddCellToSheet(builderExl, dt_.Rows[i]["Broker"].ToString(), 28, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center,
                                           TextOrientation.Horizontal, ParagraphAlignment.Left);
                            //From
                            AddCellToSheet(builderExl, string.Empty, 60, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center,
                                           TextOrientation.Horizontal, ParagraphAlignment.Left);
                            builderExl.EndRow();

                            //Narration
                            AddCellToSheet(builderExl, "Narration", 12, Color.White, true, 7, 1,
                                           CellVerticalAlignment.Center, TextOrientation.Horizontal,
                                           ParagraphAlignment.Left);
                            //To
                            AddCellToSheet(builderExl, string.Empty, 28, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center,
                                           TextOrientation.Horizontal, ParagraphAlignment.Left);
                            //From
                            AddCellToSheet(builderExl, string.Empty, 60, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center,
                                           TextOrientation.Horizontal, ParagraphAlignment.Left);
                            builderExl.EndRow();
                            //Amount
                            AddCellToSheet(builderExl, "Amount", 12, Color.White, true, 7, 1,
                                           CellVerticalAlignment.Center, TextOrientation.Horizontal,
                                           ParagraphAlignment.Left);
                            //To
                            AddCellToSheet(builderExl, string.Format("{0:C}", dt_.Rows[i]["Amount"]), 28, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center,
                                           TextOrientation.Horizontal, ParagraphAlignment.Left);
                            //From
                            AddCellToSheet(builderExl, string.Format("{0:C}", dt_.Rows[i]["Amount"]), 60, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center,
                                           TextOrientation.Horizontal, ParagraphAlignment.Left);
                            builderExl.EndRow();
                        }
                        break;
                }
                // table.AutoFit(AutoFitBehavior.FixedColumnWidths);
                table.PreferredWidth = PreferredWidth.FromPercent(100);
                MemoryStream outStream = new MemoryStream();
                document.Save(outStream, SaveFormat.Pdf);
                byte[] docBytes = outStream.ToArray();
                if (output == null)
                    output = new SerializableDictionary<string, byte[]>();

                output.Add(FileHelper.GetFileName(OutPath), docBytes);
            }
            return FileHelper.GetFileName(OutPath);
        }

        private void AddCellToSheet(DocumentBuilder builderExl, string text, double width, Color backColor, bool isFontBold, int fontSize, int lineWidth, CellVerticalAlignment vAlign, TextOrientation orientation, ParagraphAlignment paragraph)
        {
            //Header Start
            builderExl.InsertCell();
            builderExl.CellFormat.HorizontalMerge = CellMerge.None;
            builderExl.CellFormat.PreferredWidth = PreferredWidth.FromPercent(width);
            builderExl.CellFormat.Shading.BackgroundPatternColor = backColor;
            builderExl.Font.Bold = isFontBold;
            builderExl.Font.Size = fontSize;
            builderExl.CellFormat.Borders.LineWidth = lineWidth;
            builderExl.CellFormat.VerticalAlignment = vAlign;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.CurrentParagraph.ParagraphFormat.Alignment = paragraph;
            builderExl.Write(text);
        }
        
        private void SetClientManagementType()
        {
            IsSMA = SMAUtilities.IsSMAClient(new Guid(ClientCID), Broker);

            tdCashBalanceHead.Visible =
            tdCashBalanceValue.Visible =
            tdMinCashHead.Visible =
            tdMinCashValue.Visible = IsSMA;
        }
    }
}