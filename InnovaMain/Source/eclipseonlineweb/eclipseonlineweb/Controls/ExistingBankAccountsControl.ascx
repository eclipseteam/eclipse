﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExistingBankAccountsControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.ExistingBankAccountsControl" %>
<div class="popup_Container">
    
    <div class="popup_Body">
        <div runat="server" id="dvSearchtxt">
            <table>
                <tr>
                    <td style="width:25%">
                        <strong>Filter available bank accounts</strong>
                    </td>
                    <td>
                        <telerik:RadTextBox CssClass="Txt-Field" runat="server" ID="txtSearchAccName" Width="400"></telerik:RadTextBox>&nbsp;&nbsp;&nbsp;<telerik:RadButton ID="btnSearch" runat="server" Text="Filter" OnClick="btnSearch_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div runat="server" id="dvExistingAccGrid" style="width: 550px">
        <telerik:RadGrid ID="gd_AvaliableBankAccount" runat="server" AutoGenerateColumns="True" PageSize="10" 
        AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true" GridLines="Horizontal" AllowAutomaticInserts="false" 
        AllowFilteringByColumn="true" EnableViewState="true" ShowFooter="True" OnNeedDataSource="gd_AvaliableBankAccount_OnNeedDataSource"   OnItemCommand="gd_AvaliableBankAccount_ItemCommand">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="Top">
            <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false"
                ShowExportToWordButton="false" ShowExportToPdfButton="false"></CommandItemSettings>
            <Columns>  
              <telerik:GridTemplateColumn HeaderStyle-Width="10px" AllowFiltering="false" ShowFilterIcon="false">
                <ItemTemplate>
                    <asp:CheckBox runat="server" ID="chkContact"/>
                </ItemTemplate>
              </telerik:GridTemplateColumn>           
              <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                    SortExpression="AccountName" HeaderText="Account Name" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="AccountName" UniqueName="AccountName" ReadOnly="true">
                </telerik:GridBoundColumn>               
                <telerik:GridBoundColumn FilterControlWidth="75px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                    SortExpression="AccountType" HeaderText="Account Type" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="AccountType" UniqueName="AccountType">
                </telerik:GridBoundColumn>                
                 <telerik:GridBoundColumn FilterControlWidth="75px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                    SortExpression="BSB" HeaderText="BSB" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="BSB" UniqueName="BSB">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="100px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                    SortExpression="AccountNumber" HeaderText="Account Number" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="AccountNumber" UniqueName="AccountNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                    SortExpression="Institution" HeaderText="Institution" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="Institution" UniqueName="Institution">
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
        </div>
        <div>
            <asp:Label runat="server" ID="lblmessages" Font-Size="Medium" Font-Bold="True" ForeColor="Red" />
            <br />            
        </div>    
        <div align="right" style="width: 800px">   
            <telerik:RadButton ID="btnUpdate" runat="server" Text="Save" OnClick="btnUpdate_OnClick" />            
        </div>
    </div>
</div>
