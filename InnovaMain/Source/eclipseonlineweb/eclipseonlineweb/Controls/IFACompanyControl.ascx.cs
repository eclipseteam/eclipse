﻿using Oritax.TaxSimp.CM.Group;
using System.Data;
using Oritax.TaxSimp.DataSets;
using System;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Commands;


namespace eclipseonlineweb.Controls
{
    public partial class IFACompanyControl : System.Web.UI.UserControl
    {

        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        public Action<Guid> Saved
        {
            get;
            set;
        }

        public Action<Guid, DataSet> SaveData
        {
            get;
            set;
        }
        public IFADS GetEntity()
        {
            IFADS ifaDs = new IFADS();
            DataRow dr = ifaDs.Tables[ifaDs.ifaTable.TableName].NewRow();
            dr[ifaDs.ifaTable.TRADINGNAME] = txtTradingName.Text;
            dr[ifaDs.ifaTable.STATUS] = "Active";
            dr[ifaDs.ifaTable.IFATYPE] = hf_IfaType.Value;
            dr[ifaDs.ifaTable.EMAIL] = txtEmail.GetEntity();
            dr[ifaDs.ifaTable.NAME] = txtTradingName.Text;
            if (chkSameAsTrading.Checked)
            {
                dr[ifaDs.ifaTable.LEGALNAME] = txtTradingName.Text;
            }
            else
            {
                dr[ifaDs.ifaTable.LEGALNAME] = txtLegalName.Text;
            }
            dr[ifaDs.ifaTable.ACCOUNTDESIGNATION] = txtAccountDesignation.Text;
            dr[ifaDs.ifaTable.FACSIMILE] = ph_facsimle.Number;
            dr[ifaDs.ifaTable.FACSIMILECOUNTRYCODE] = ph_facsimle.CountryCode;
            dr[ifaDs.ifaTable.FACSIMILECITYCODE] = ph_facsimle.CityCode;
            dr[ifaDs.ifaTable.WEBSITEADDRESS] = txtWebSiteAddress.Text;
            dr[ifaDs.ifaTable.ACN] = acn_ACN.GetEntity();
            if (dtLastAudited.SelectedDate != null)
                dr[ifaDs.ifaTable.LASTAUDITEDDATE] = dtLastAudited.SelectedDate.Value.ToShortDateString();
            dr[ifaDs.ifaTable.TURNOVER] = txtTurnOver.Text;
            dr[ifaDs.ifaTable.PHONENUBER] = ph_phoneNumber.Number;
            dr[ifaDs.ifaTable.PHONENUMBERCOUNTRYCODE] = ph_phoneNumber.CountryCode;
            dr[ifaDs.ifaTable.PHONENUMBERCITYCODE] = ph_phoneNumber.CityCode;
            dr[ifaDs.ifaTable.SKCODE] = txtSkCode.Text;
            dr[ifaDs.ifaTable.ABN] = abn_ABN.GetEntity();
            dr[ifaDs.ifaTable.TFN] = tfn_TFN.GetEntity();
            dr[ifaDs.ifaTable.PREVIOUSPRACTICE] = txtPreviousPractice.Text;
            dr[ifaDs.ifaTable.FUM] = txtFUM.Text;
            ifaDs.Tables[ifaDs.ifaTable.TableName].Rows.Add(dr);
            return ifaDs;
        }
        public void SetEntity(Guid CID, IFAEntityType IfaType)
        {
            ClearEntity();
            hf_IfaType.Value = IfaType.ToString();
            if (CID != Guid.Empty)
            {
                IFADS ds = GetIfaDetails(CID);
                hfCID.Value = CID.ToString();
                DataRow dr = ds.Tables[ds.ifaTable.TableName].Rows[0];

                txtTradingName.Text = dr[ds.ifaTable.TRADINGNAME].ToString();
                txtLegalName.Text = dr[ds.ifaTable.LEGALNAME].ToString();
                txtEmail.SetEntity(dr[ds.ifaTable.EMAIL].ToString());
                if (txtTradingName.Text == txtLegalName.Text)
                {
                    chkSameAsTrading.Checked = true;
                    txtLegalName.Enabled = false;
                }
                else
                {
                    chkSameAsTrading.Checked = false;
                    txtLegalName.Enabled = true;
                }
                txtAccountDesignation.Text = dr[ds.ifaTable.ACCOUNTDESIGNATION].ToString();
                ph_facsimle.CountryCode = dr[ds.ifaTable.FACSIMILECOUNTRYCODE].ToString();
                ph_facsimle.CityCode = dr[ds.ifaTable.FACSIMILECITYCODE].ToString();
                ph_facsimle.Number = dr[ds.ifaTable.FACSIMILE].ToString();
                txtWebSiteAddress.Text = dr[ds.ifaTable.WEBSITEADDRESS].ToString();
                acn_ACN.SetEntity(dr[ds.ifaTable.ACN].ToString());
                if (!string.IsNullOrEmpty(dr[ds.ifaTable.LASTAUDITEDDATE].ToString()))
                    dtLastAudited.SelectedDate = Convert.ToDateTime(dr[ds.ifaTable.LASTAUDITEDDATE].ToString());
                txtTurnOver.Text = dr[ds.ifaTable.TURNOVER].ToString();
                ph_phoneNumber.CountryCode = dr[ds.ifaTable.PHONENUMBERCOUNTRYCODE].ToString();
                ph_phoneNumber.CityCode = dr[ds.ifaTable.PHONENUMBERCITYCODE].ToString();
                ph_phoneNumber.Number = dr[ds.ifaTable.PHONENUBER].ToString();
                txtSkCode.Text = dr[ds.ifaTable.SKCODE].ToString();
                abn_ABN.SetEntity(dr[ds.ifaTable.ABN].ToString());
                tfn_TFN.SetEntity(dr[ds.ifaTable.TFN].ToString());
                txtPreviousPractice.Text = dr[ds.ifaTable.PREVIOUSPRACTICE].ToString();
                txtFUM.Text = dr[ds.ifaTable.FUM].ToString();
            }
        }
        private IFADS GetIfaDetails(Guid cid)
        {
            var ifaDs = new IFADS { CommandType = DatasetCommandTypes.Details };
            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
                clientData.GetData(ifaDs);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }
            return ifaDs;
        }
        public void ClearEntity()
        {
            hfCID.Value = Guid.Empty.ToString();
            hf_IfaType.Value = "";
            txtTradingName.Text = "";
            txtLegalName.Text = "";
            txtAccountDesignation.Text = "";
            ph_facsimle.ClearEntity();
            ph_phoneNumber.ClearEntity();
            txtSkCode.Text = "";
            txtWebSiteAddress.Text = "";
            acn_ACN.ClearEntity();
            dtLastAudited.Clear();
            dtLastAudited.DateInput.Clear();
            txtTurnOver.Text = "";
            abn_ABN.ClearEntity();
            tfn_TFN.ClearEntity();
            txtPreviousPractice.Text = "";
            txtFUM.Text = "";
            txtEmail.ClearEntity();
        }
        public bool Validate()
        {
            return Validate(true);
        }
        public bool Validate(bool ShowTabError)
        {
            bool result = true;
            string message = string.Empty;
            if (!abn_ABN.HasValue && !tfn_TFN.HasValue)
            {
                message += "Please provide ABN or TFN <br/>";
                result = false;
            }

           

            txtMessage.Text = message;

            result = result & abn_ABN.Validate() & tfn_TFN.Validate() & ph_facsimle.Validate() & ph_phoneNumber.Validate() & acn_ACN.Validate() & txtEmail.Validate();
            return result;
        }
        
        protected void btnSaveIfaCompany_Click(object sender, System.EventArgs e)
        {
            if (Validate())
            {
                IFADS ifaDs = new IFADS();
                var cid = (string.IsNullOrEmpty(hfCID.Value)) ? Guid.Empty : new Guid(hfCID.Value);
                if (cid == Guid.Empty)
                {
                    ifaDs.CommandType = DatasetCommandTypes.Add;
                }
                else
                {
                    ifaDs.CommandType = DatasetCommandTypes.Update;
                }
                if (Broker != null)
                {
                    ifaDs.ifaTable.Merge(GetEntity().ifaTable);
                    if (cid == Guid.Empty)
                    {
                        var unit = new OrganizationUnit
                            {
                                OrganizationStatus = "Active",
                                Name = txtTradingName.Text,
                                Type = ((int) OrganizationType.IFA).ToString(),
                                CurrentUser = (Page as UMABasePage).GetCurrentUser()
                            };

                        ifaDs.Unit = unit;
                        ifaDs.Command = (int) WebCommands.AddNewOrganizationUnit;
                    }

                    if (SaveData != null)
                    {
                        
                            SaveData(cid, ifaDs);
                            if (ifaDs.CommandType == DatasetCommandTypes.Add)
                            {
                                cid = ifaDs.Unit.Cid;
                                hfCID.Value = cid.ToString();

                                if (Saved != null)
                                    Saved(cid);
                            }
                    }
                    //Enable Disable Legal control
                    txtLegalName.Enabled = !chkSameAsTrading.Checked;
                }
                else
                {
                    throw new Exception("Broker Not Found");
                }
            }
        }
    }
}