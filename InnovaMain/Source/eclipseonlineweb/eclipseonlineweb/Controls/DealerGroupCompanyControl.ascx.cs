﻿using Oritax.TaxSimp.CM.Group;
using System.Data;
using Oritax.TaxSimp.DataSets;
using System;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Commands;
using Telerik.Web.UI;
using System.Web;
using System.Web.UI;
using CorporateEntity = Oritax.TaxSimp.Common.CorporateEntity;

namespace eclipseonlineweb.Controls
{
    public partial class DealerGroupCompanyControl : System.Web.UI.UserControl
    {
        public Action ValidationFailed { get; set; }
        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }
        public Action<Guid> Saved
        {
            get;
            set;
        }
        public Action<Guid, DataSet> SaveData
        {
            get;
            set;
        }
        public DealerGroupDS GetEntity()
        {
            DealerGroupDS dealerGroupDs = new DealerGroupDS();
            DataRow dr = dealerGroupDs.Tables[dealerGroupDs.DealerGroupTable.TableName].NewRow();
            dr[dealerGroupDs.DealerGroupTable.TRADINGNAME] = txtTradingName.Text;
            dr[dealerGroupDs.DealerGroupTable.STATUS] = "Active";
            dr[dealerGroupDs.DealerGroupTable.DEALERGROUPTYPE] = hf_DealerGroupType.Value;

            dr[dealerGroupDs.DealerGroupTable.NAME] = txtTradingName.Text;
            if (chkSameAsTrading.Checked)
            {
                dr[dealerGroupDs.DealerGroupTable.LEGALNAME] = txtTradingName.Text;
            }
            else
            {
                dr[dealerGroupDs.DealerGroupTable.LEGALNAME] = txtLegalName.Text;
            }
            dr[dealerGroupDs.DealerGroupTable.FACSIMLENUMBER] = ph_facsimle.Number;
            dr[dealerGroupDs.DealerGroupTable.FACSIMLECOUTNRYCODE] = ph_facsimle.CountryCode;
            dr[dealerGroupDs.DealerGroupTable.FACSIMLECITYCODE] = ph_facsimle.CityCode;
            dr[dealerGroupDs.DealerGroupTable.WORKPHONENUMBER] = ph_workPhone.Number;
            dr[dealerGroupDs.DealerGroupTable.WORKPHONECOUTNRYCODE] = ph_workPhone.CountryCode;
            dr[dealerGroupDs.DealerGroupTable.WORKPHONECITYCODE] = ph_workPhone.CityCode;
            dr[dealerGroupDs.DealerGroupTable.ADVISORYFIRMTERMINOLOGY] = txtAdvisoryFirm.Text;
            dr[dealerGroupDs.DealerGroupTable.WEBSITEADDRESS] = txtWebSiteAdd.Text;
            dr[dealerGroupDs.DealerGroupTable.TFN] = TFN_InputControl1.GetEntity();
            dr[dealerGroupDs.DealerGroupTable.ABN] = ABN_InputControl1.GetEntity();
            dr[dealerGroupDs.DealerGroupTable.AFSLNO] = txtAFSL.Text;
            dr[dealerGroupDs.DealerGroupTable.ACN] = ACN_InputControl1.GetEntity();
            dr[dealerGroupDs.DealerGroupTable.FUM] = txtFUM.Text;
            dr[dealerGroupDs.DealerGroupTable.TURNOVER] = txtTurnOver.Text;
            dealerGroupDs.Tables[dealerGroupDs.DealerGroupTable.TableName].Rows.Add(dr);
            return dealerGroupDs;
        }
        public void SetEntity(Guid CID, DealerGroupEntityType DealerGroupType)
        {

            ClearEntity();
            hf_DealerGroupType.Value = DealerGroupType.ToString();
            if (CID != Guid.Empty)
            {
                DealerGroupDS ds = GetDealorGroupDetails(CID);
                hfCID.Value = CID.ToString();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[ds.DealerGroupTable.TableName].Rows[0];
                    txtTradingName.Text = dr[ds.DealerGroupTable.TRADINGNAME].ToString();
                    txtLegalName.Text = dr[ds.DealerGroupTable.LEGALNAME].ToString();
                    if (txtTradingName.Text == txtLegalName.Text)
                    {
                        chkSameAsTrading.Checked = true;
                        txtLegalName.Enabled = false;
                    }
                    else
                    {
                        chkSameAsTrading.Checked = false;
                        txtLegalName.Enabled = true;
                    }
                    ph_facsimle.CountryCode = dr[ds.DealerGroupTable.FACSIMLECOUTNRYCODE].ToString();
                    ph_facsimle.CityCode = dr[ds.DealerGroupTable.FACSIMLECITYCODE].ToString();
                    ph_facsimle.Number = dr[ds.DealerGroupTable.FACSIMLENUMBER].ToString();
                    ph_workPhone.CountryCode = dr[ds.DealerGroupTable.WORKPHONECOUTNRYCODE].ToString();
                    ph_workPhone.CityCode = dr[ds.DealerGroupTable.WORKPHONECITYCODE].ToString();
                    ph_workPhone.Number = dr[ds.DealerGroupTable.WORKPHONENUMBER].ToString();
                    txtAdvisoryFirm.Text = dr[ds.DealerGroupTable.ADVISORYFIRMTERMINOLOGY].ToString();
                    txtWebSiteAdd.Text = dr[ds.DealerGroupTable.WEBSITEADDRESS].ToString();
                    TFN_InputControl1.SetEntity(dr[ds.DealerGroupTable.TFN].ToString());
                    ABN_InputControl1.SetEntity(dr[ds.DealerGroupTable.ABN].ToString());
                    txtAFSL.Text = dr[ds.DealerGroupTable.AFSLNO].ToString();
                    ACN_InputControl1.SetEntity(dr[ds.DealerGroupTable.ACN].ToString());
                    txtFUM.Text = dr[ds.DealerGroupTable.FUM].ToString();
                    txtTurnOver.Text = dr[ds.DealerGroupTable.TURNOVER].ToString();
                }
            }
        }
        private DealerGroupDS GetDealorGroupDetails(Guid cid)
        {
            var dealerGroupDs = new DealerGroupDS { CommandType = DatasetCommandTypes.Details };
            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
                clientData.GetData(dealerGroupDs);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }
            return dealerGroupDs;
        }
        public void ClearEntity()
        {
            hfCID.Value = Guid.Empty.ToString();
            hf_DealerGroupType.Value = "";
            txtTradingName.Text = "";
            txtLegalName.Text = "";
            txtAdvisoryFirm.Text = "";
            txtAFSL.Text = "";
            txtWebSiteAdd.Text = "";
            txtFUM.Text = "";
            txtTurnOver.Text = "";
            ACN_InputControl1.ClearEntity();
            ph_workPhone.ClearEntity();
            ph_facsimle.ClearEntity();
            ABN_InputControl1.ClearEntity();
            TFN_InputControl1.ClearEntity();
        }
        
        public bool Validate()
        {
            return Validate(true);
        }
        public bool Validate(bool ShowTabError)
        {
            bool result = true;

            if (!ABN_InputControl1.HasValue && !TFN_InputControl1.HasValue)
            {
                txtMessage.Text = "Please provide ABN or TFN";
                result = false;
            }
            else
            {
                txtMessage.Text = "";
            }
            result = result & ABN_InputControl1.Validate() & TFN_InputControl1.Validate() & ACN_InputControl1.Validate() & ph_workPhone.Validate() & ph_facsimle.Validate();

            return result;
        }   
        
        protected void btnSaveDealerCompany_Click(object sender, System.EventArgs e)
        {
            if (Validate())
            {
                DealerGroupDS dealerGroupDs = new DealerGroupDS();
                var cid = (string.IsNullOrEmpty(hfCID.Value)) ? Guid.Empty : new Guid(hfCID.Value);

                if (cid == Guid.Empty)
                {
                    dealerGroupDs.CommandType = DatasetCommandTypes.Add;
                }
                else
                {
                    dealerGroupDs.CommandType = DatasetCommandTypes.Update;
                }
                if (Broker != null)
                {
                    dealerGroupDs.DealerGroupTable.Merge(GetEntity().DealerGroupTable);
                    if (cid == Guid.Empty)
                    {
                        var unit = new OrganizationUnit
                            {
                                OrganizationStatus = "Active",
                                Name = txtTradingName.Text,
                                Type = ((int)OrganizationType.DealerGroup).ToString(),
                                CurrentUser = (Page as UMABasePage).GetCurrentUser()
                            };

                        dealerGroupDs.Unit = unit;
                        dealerGroupDs.Command = (int)WebCommands.AddNewOrganizationUnit;
                    }
                    if (SaveData != null)
                    {
                        SaveData(cid, dealerGroupDs);
                        if (dealerGroupDs.CommandType == DatasetCommandTypes.Add)
                        {
                            cid = dealerGroupDs.Unit.Cid;
                            hfCID.Value = cid.ToString();
                            if (Saved != null)
                                Saved(cid);
                        }

                        lblMsg.Text = "Saved successfully.";
                        lblMsg.Visible = true;
                    }
                    //Enable Disable Legal control
                    txtLegalName.Enabled = !chkSameAsTrading.Checked;
                }
                else
                {
                    throw new Exception("Broker Not Found");
                }
            }
        }
    }
}