﻿using Oritax.TaxSimp.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace eclipseonlineweb.Controls
{
    public partial class AlertConfigurationTabsControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {               
            var user = (Page as UMABasePage).GetCurrentUser();
            if (user.UserType==(int)UserType.Advisor)
            {
                liAdmin.Visible = false;
                //Applying active css class.
                liAdviser.Attributes.Add("class", "active");
            }
            if (user.UserType == (int)UserType.Client)
            {
                liAdmin.Visible = false;
                liAdviser.Visible = false;
                //Applying active css class.
                liClient.Attributes.Add("class", "active");
            }
        }
    }
}