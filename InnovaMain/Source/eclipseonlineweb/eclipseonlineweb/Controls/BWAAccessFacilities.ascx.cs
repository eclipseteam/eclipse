﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.CM;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using ServiceType = Oritax.TaxSimp.CM.Organization.HelperClass.ServiceType;

namespace eclipseonlineweb.Controls
{
    public partial class BWAAccessFacilities : UserControl
    {
        private string _cid = string.Empty;
        private ICMBroker Broker
        {
            get
            {
                return ((UMABasePage)Page).UMABroker;
            }
        }

        public ServiceType BWAAccessFacilityType
        {
            get
            {
                ServiceType sType;
                Enum.TryParse(hfType.Value, true, out sType);
                return sType;
            }
            set
            {
                hfType.Value = value.ToString();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _cid = Request.QueryString["ins"];
            if (!Page.IsPostBack && !string.IsNullOrEmpty(_cid))
            {
                SetLitralText();
                SetData();
            }
        }

        private void SetLitralText()
        {
            string type = string.Empty;
            switch (BWAAccessFacilityType)
            {
                case ServiceType.DoItYourSelf:
                    type = "DIY";
                    break;
                case ServiceType.DoItWithMe:
                    type = "DIWM";
                    break;
                case ServiceType.DoItForMe:
                    type = "DIFM";
                    break;
            }

            lbl1.Text = type;
            lbl2.Text = type;
            lbl3.Text = type;
            lbl4.Text = type;
        }

        private void SetData()
        {
            var clientData = Broker.GetBMCInstance(new Guid(_cid)) as OrganizationUnitCM;
            if (clientData != null)
            {
                var entity = clientData.ClientEntity as IClientUMAData;
                if (entity != null)
                {
                    SetData(entity);
                }
            }
        }

        private void SetData(IClientUMAData entity)
        {
            switch (BWAAccessFacilityType)
            {
                case ServiceType.DoItYourSelf:
                    if (entity.Servicetype.DoItYourSelf.AccessFacilities == null)
                    {
                        entity.Servicetype.DoItYourSelf.AccessFacilities = new AccessFacilities();
                    }
                    if (entity.Servicetype.DoItYourSelf.OperationManner == null)
                    {
                        entity.Servicetype.DoItYourSelf.OperationManner = new OperationManner();
                    }
                    SetControlData(entity.Servicetype.DoItYourSelf.AccessFacilities);
                    SetControlData(entity.Servicetype.DoItYourSelf.OperationManner);
                    break;
                case ServiceType.DoItWithMe:
                    if (entity.Servicetype.DoItWithMe.AccessFacilities == null)
                    {
                        entity.Servicetype.DoItWithMe.AccessFacilities = new AccessFacilities();
                    }
                    if (entity.Servicetype.DoItWithMe.OperationManner == null)
                    {
                        entity.Servicetype.DoItWithMe.OperationManner = new OperationManner();
                    }
                    SetControlData(entity.Servicetype.DoItWithMe.AccessFacilities);
                    SetControlData(entity.Servicetype.DoItWithMe.OperationManner);
                    break;
                case ServiceType.DoItForMe:
                    if (entity.Servicetype.DoItForMe.AccessFacilities == null)
                    {
                        entity.Servicetype.DoItForMe.AccessFacilities = new AccessFacilities();
                    }
                    if (entity.Servicetype.DoItForMe.OperationManner == null)
                    {
                        entity.Servicetype.DoItForMe.OperationManner = new OperationManner();
                    }
                    SetControlData(entity.Servicetype.DoItForMe.AccessFacilities);
                    SetControlData(entity.Servicetype.DoItForMe.OperationManner);
                    break;
            }
        }

        private void SetControlData(AccessFacilities accessFacilities)
        {
            chkChequeBook.Checked = accessFacilities.CHEQUEBOOK;
            chkDebitCard.Checked = accessFacilities.DEBITCARD;
            chkDepositBook.Checked = accessFacilities.DEPOSITBOOK;
            chkOnLineAccess.Checked = accessFacilities.ONLINEACCESS;
            chkPhoneAccess.Checked = accessFacilities.PHONEACCESS;
        }

        private void SetControlData(OperationManner operationManner)
        {
            btnAllSign.Checked = operationManner.ALL_OF_US_TO_SIGN;
            btnAnyOneSign.Checked = operationManner.ANY_ONE_OF_US_TO_SIGN;
            btnAnyTwoSign.Checked = operationManner.ANY_TWO_OF_US_TO_SIGN;
        }
        
        public void Save()
        {
            if (!btnAnyOneSign.Checked && !btnAnyTwoSign.Checked && !btnAllSign.Checked)
            {
                btnAllSign.Checked = true;
            }
            if (btnAllSign.Checked || btnAnyTwoSign.Checked)
            {
                chkPhoneAccess.Checked = false;
                chkDebitCard.Checked = false;
                chkOnLineAccess.Checked = false;
            }

            var clientData = Broker.GetBMCInstance(new Guid(_cid)) as OrganizationUnitCM;
            if (clientData != null)
            {
                clientData.UpdationDate = DateTime.Now;

                var entity = clientData.ClientEntity as IClientUMAData;
                if (entity != null)
                {
                    switch (BWAAccessFacilityType)
                    {
                        case ServiceType.DoItYourSelf:
                            SetEntityValues(entity.Servicetype.DoItYourSelf.AccessFacilities);
                            SetEntityValues(entity.Servicetype.DoItYourSelf.OperationManner);
                            break;
                        case ServiceType.DoItWithMe:
                            SetEntityValues(entity.Servicetype.DoItWithMe.AccessFacilities);
                            SetEntityValues(entity.Servicetype.DoItWithMe.OperationManner);
                            break;
                        case ServiceType.DoItForMe:
                            SetEntityValues(entity.Servicetype.DoItForMe.AccessFacilities);
                            SetEntityValues(entity.Servicetype.DoItForMe.OperationManner);
                            break;
                    }
                }

                Broker.SaveOverride = true;
                clientData.CalculateToken(true);
                Broker.SetComplete();
                Broker.SetStart();
            }
            SetData();
        }

        private void SetEntityValues(OperationManner operationManner)
        {
            operationManner.ALL_OF_US_TO_SIGN = btnAllSign.Checked;
            operationManner.ANY_ONE_OF_US_TO_SIGN = btnAnyOneSign.Checked;
            operationManner.ANY_TWO_OF_US_TO_SIGN = btnAnyTwoSign.Checked;
        }

        private void SetEntityValues(AccessFacilities accessFacilities)
        {
            accessFacilities.CHEQUEBOOK = chkChequeBook.Checked;
            accessFacilities.DEBITCARD = chkDebitCard.Checked;
            accessFacilities.DEPOSITBOOK = chkDepositBook.Checked;
            accessFacilities.ONLINEACCESS = chkOnLineAccess.Checked;
            accessFacilities.PHONEACCESS = chkPhoneAccess.Checked;
        }

        protected void btnAllSign_OnCheckedChanged(object sender, EventArgs e)
        {
            if(btnAllSign.Checked)
            {
                chkPhoneAccess.Checked = false;
                chkDebitCard.Checked = false;
                chkOnLineAccess.Checked = false;
            }
        }

        protected void btnAnyTwoSign_OnCheckedChanged(object sender, EventArgs e)
        {
            if (btnAnyTwoSign.Checked)
            {
                chkPhoneAccess.Checked = false;
                chkDebitCard.Checked = false;
                chkOnLineAccess.Checked = false;
            }
            
        }
    }
}