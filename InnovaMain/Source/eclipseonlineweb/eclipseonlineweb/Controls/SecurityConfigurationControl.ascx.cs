﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Telerik.Web.UI;
using Aspose.Cells.Drawing;

namespace eclipseonlineweb.Controls
{
    public partial class SecurityConfigurationControl : UserControl
    {
        private string _cid = string.Empty;
        private DataSet _dataSet;

        protected void Page_Load(object sender, EventArgs e)
        {
            _cid = Request.QueryString["ins"];
            if (_cid != null)
            {
                SecuritiesClient();
            }
        }

        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        private void SecuritiesClient()
        {
            if (!String.IsNullOrEmpty(_cid))
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(new Guid(_cid));
                var ds = new PartyDS();
                clientData.GetData(ds);
                ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].AcceptChanges();
                if (!IsPostBack)
                    BindConfigurations(ds);
                _dataSet = ds;
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
        }

          private void BindConfigurations(PartyDS ds)
        {
            cmbIncluded.Items.Clear();
            cmbAvailable.Items.Clear();
            DataView objDataView = ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].DefaultView;
            objDataView.Sort = PartyDS.PARTYNAME_FIELD;

            DataTable dtIncluded = objDataView.ToTable();

            DataView availView = ds.Tables[PartyDS.AVAILABLEPARTY_TABLE].DefaultView;
            availView.Sort = PartyDS.PARTYNAME_FIELD;
            DataTable dtAvailable = availView.ToTable();

            foreach (DataRow accessRow in dtAvailable.Rows)
            {
                if (accessRow[PartyDS.PARTYNAME_FIELD].ToString().IndexOf("Administrator") == -1)
                {
                    var item = new RadComboBoxItem(accessRow[PartyDS.PARTYNAME_FIELD].ToString(), accessRow[PartyDS.PARTYCID_FIELD].ToString());

                    DataRow[] rows = dtIncluded.Select(PartyDS.PARTYCID_FIELD + "='" + item.Value + "'");
                    if (rows.Count() > 0)
                        cmbIncluded.Items.Add(item);
                    else
                        cmbAvailable.Items.Add(item);
                }
            }
            cmbIncluded.Filter = RadComboBoxFilter.Contains;
            cmbAvailable.Filter = RadComboBoxFilter.Contains;
        }

        protected void btnAdd_OnClick(object sender, EventArgs e)
        {
            for (int i= cmbAvailable.Items.Count-1; i >=0; i-- )
            {
                RadComboBoxItem item = cmbAvailable.Items[i];
                if (item.Selected || item.Checked)
                {
                    AddIncludedItem(item);
                }
            }
            Complete();
        }

        protected void btnAddAll_OnClick(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(cmbAvailable.Text))
                return;
            string search = cmbAvailable.Text.ToLower();
            for (int i = cmbAvailable.Items.Count - 1; i >= 0; i--)
            {
                RadComboBoxItem item = cmbAvailable.Items[i];
                
                if (item.Text.ToLower().Contains(search))
                {
                    AddIncludedItem(item);
                }
            }
            Complete();
        }

        private void AddIncludedItem(RadComboBoxItem item)
        {
            item.Checked = false;
            cmbAvailable.Items.Remove(item);
            cmbIncluded.Items.Add(item);
            DataRow partyRow = _dataSet.Tables[PartyDS.INCLUDEDPARTY_TABLE].NewRow();
            partyRow[PartyDS.PARTYNAME_FIELD] = item.Text;
            partyRow[PartyDS.PARTYCID_FIELD] = item.Value;
            _dataSet.Tables[PartyDS.INCLUDEDPARTY_TABLE].Rows.Add(partyRow);
        }
 
        protected void btnRemove_OnClick(object sender, EventArgs e)
        {

            //var item = cmbIncluded.SelectedItem;
            for (int i = cmbIncluded.Items.Count - 1; i >= 0; i--)
            {
                RadComboBoxItem item = cmbIncluded.Items[i];
                if (item.Selected || item.Checked)
                {
                    RemoveIncludedItem(item);
                }
            }

            Complete();

        }
        protected void btnRemoveAll_OnClick(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(cmbIncluded.Text))
                return;
            string search = cmbIncluded.Text.ToLower();
            for (int i = cmbIncluded.Items.Count - 1; i >= 0; i--)
            {
                RadComboBoxItem item = cmbIncluded.Items[i];
                if (item.Text.ToLower().Contains(search))
                {
                    RemoveIncludedItem(item);
                }
            }

            Complete();

        }
        private void RemoveIncludedItem(RadComboBoxItem item)
        {
            item.Checked = false;
            cmbIncluded.Items.Remove(item);
            cmbAvailable.Items.Add(item);
            DataRow[] drs = _dataSet.Tables[PartyDS.INCLUDEDPARTY_TABLE].Select(string.Format("{0}='{1}'", PartyDS.PARTYCID_FIELD, item.Value));
            if (drs.Length > 0)
            {
                foreach (DataRow dataRow in drs)
                {
                    if (dataRow.RowState != DataRowState.Deleted)
                    {
                        dataRow.Delete();
                    }
                }
            }
        }

        private void Complete()
        {
            Broker.SaveOverride = true;
            cmbIncluded.Text = string.Empty;
            cmbAvailable.Text = string.Empty;
            cmbAvailable.SelectedValue = "";
            cmbIncluded.SelectedValue = "";
            cmbAvailable.ClearSelection();
            cmbIncluded.ClearSelection();

            IBrokerManagedComponent clientData = Broker.GetBMCInstance(new Guid(_cid));
            clientData.SetData(_dataSet);
            Broker.ReleaseBrokerManagedComponent(clientData);
            Broker.SetComplete();
            BindConfigurations((PartyDS)_dataSet);
        }

        private bool IsValidUser(Guid userID)
        {
            DataTable availablePartyTable = _dataSet.Tables[PartyDS.AVAILABLEPARTY_TABLE];
            DataRow[] partyRows = availablePartyTable.Select(PartyDS.PARTYCID_FIELD + "='" + userID.ToString() + "'", "", DataViewRowState.CurrentRows);
            return partyRows.Length > 0;
        }
    }
}