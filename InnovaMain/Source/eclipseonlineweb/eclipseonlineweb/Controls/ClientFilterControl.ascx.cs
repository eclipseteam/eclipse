﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eclipseonlineweb.Controls
{
    public partial class ClientFilterControl : System.Web.UI.UserControl
    {
        public bool AllClients { 
            get { return chkAllClients.Checked;}
        }


        public int ClientCount
        {
            get
            {
                if ((!chkAllClients.Checked)&&chkClientCount.Checked && txtClientCount.Value.HasValue)
                return   (int) txtClientCount.Value.Value;
                
                return 0;
            }
        }


        public string ClientIDs
        {
            get
            {
                if ((!chkAllClients.Checked) && chkClientFilter.Checked)
                    return txtClientIDS.Text;

                return string.Empty;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}