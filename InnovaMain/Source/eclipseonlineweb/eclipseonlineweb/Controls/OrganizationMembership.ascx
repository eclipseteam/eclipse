﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrganizationMembership.ascx.cs"
    Inherits="eclipseonlineweb.Controls.OrganizationMembership" %>
<asp:Panel ID="pnlTitle" runat="server" Width="100%" BackColor="DarkBlue">
    <table width="100%">
        <tr>
            <td style="text-align: left; color: White;">
                Search for available members
            </td>
            <td style="text-align: right;">
                <telerik:RadButton ID="btnCancel" runat="server" Width="14px" Height="14px" OnClick="btnCancel_OnClick">
                    <Image ImageUrl="~/images/cross_icon_normal.png"></Image>
                </telerik:RadButton>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pnlSearch" runat="server" Width="100%" DefaultButton="btnSearch">  
    <table width="100%">
        <tr>
            <td style="width: 20%;">
                <asp:Label runat="server" ID="lblSearch">Enter name</asp:Label>
            </td>
            <td style="width: 35%; text-align: left">
                <telerik:RadTextBox ID="txtSearch" runat="server" Width="95%" ValidationGroup="VGMembership">
                </telerik:RadTextBox>
                <asp:RequiredFieldValidator ID="RFVTxtSearch" runat="server" ControlToValidate="txtSearch"
                    ErrorMessage="*" ForeColor="Red" ValidationGroup="VGMembership"></asp:RequiredFieldValidator>
            </td>
            <td style="width: 10%; text-align: left;">
                <telerik:RadButton ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click"
                    ValidationGroup="VGMembership">
                </telerik:RadButton>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pnlGrid" runat="server" Width="100%">
    <table width="100%">
        <tr>
            <td>
                <asp:HiddenField runat="server" ID="hf_searchPattren" Value="false" />
                <telerik:RadGrid ID="gd_Membership" runat="server" AutoGenerateColumns="True" PageSize="10"
                    ViewStateMode="Enabled" OnNeedDataSource="gd_Membership_OnNeedDataSource" AllowSorting="True"
                    AllowMultiRowSelection="False" AllowPaging="true" GridLines="Horizontal" AllowAutomaticInserts="false"
                    OnInsertCommand="gd_Membership_InsertCommand" AllowFilteringByColumn="true" EnableViewState="true"
                    ShowFooter="True" OnPageIndexChanged="gd_Membership_OnPageIndexChanged" OnSortCommand="gd_Membership_OnSortCommand">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="Top" Name="Memberships">
                        <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false"
                            ShowRefreshButton="false" ShowExportToWordButton="false" ShowExportToPdfButton="false">
                        </CommandItemSettings>
                        <Columns>
                            <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="ENTITYCIID_FIELD"
                                HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderText="CID" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ENTITYCIID_FIELD" UniqueName="Cid" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="ENTITYCLID_FIELD"
                                HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderText="CLID" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ENTITYCLID_FIELD" UniqueName="ENTITYCLID_FIELD" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="ENTITYCSID_FIELD"
                                HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderText="CSID" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ENTITYCSID_FIELD" UniqueName="ENTITYCSID_FIELD" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="member" AllowFiltering="False" ColumnGroupName="members"
                                HeaderStyle-Width="10px" ItemStyle-Width="10px">
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkmember" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="ClientID" HeaderStyle-Width="5%"
                                ItemStyle-Width="5%" HeaderText="Client ID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ClientID" UniqueName="ClientID"
                                Visible="false">
                                <HeaderStyle Width="200px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ENTITYNAME_FIELD" ReadOnly="true" HeaderText="Account Name"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ENTITYNAME_FIELD" UniqueName="ENTITYNAME_FIELD">
                                <HeaderStyle Width="400px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="AccountType" ReadOnly="true" HeaderText="Account Type"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="AccountType" UniqueName="AccountType">
                                <HeaderStyle Width="300px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="Status" ReadOnly="true" HeaderText="Status"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="Status" UniqueName="Status">
                                <HeaderStyle Width="6%"></HeaderStyle>
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pnlUpdate" runat="server" Width="100%">
    <table width="100%">
        <tr>
            <td style="text-align: right;">
                <telerik:RadButton ID="btnSave" runat="server" Text="Save" OnClick="btnSave_OnClick"
                    Width="56px">
                </telerik:RadButton>
                &nbsp;
                <telerik:RadButton ID="btnCancels" runat="server" Text="Cancel" OnClick="btnCancel_OnClick"
                    Width="56px">
                </telerik:RadButton>
            </td>
        </tr>
    </table>
</asp:Panel>
