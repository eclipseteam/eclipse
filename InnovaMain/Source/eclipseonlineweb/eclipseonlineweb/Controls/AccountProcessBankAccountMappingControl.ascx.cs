﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;

namespace eclipseonlineweb.Controls
{
    public partial class AccountProcessBankAccountMappingControl : System.Web.UI.UserControl
    {
        private DataView _summaryView;
        readonly Dictionary<Guid, Guid> _selectedByUser = new Dictionary<Guid, Guid>();
        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }
        public Action<Guid, Guid> Saved
        {
            get;
            set;
        }
        public Action ValidationFailed { get; set; }
        public Action Canceled { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void LoadControls()
        {
            if (string.IsNullOrEmpty(txtSearchAccName.Text))
            {
                return;
            }
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            var allAccounts = new BankAccountDS
            {
                Command = (int)WebCommands.GetOrganizationUnitsByType,
                Unit = new OrganizationUnit
                {
                    Name = txtSearchAccName.Text,
                    CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                    Type = ((int)OrganizationType.BankAccount).ToString()
                }
            };
            org.GetData(allAccounts);
            var selectedClid = new Guid(selectedCLID.Value);
            var selectedCsid = new Guid(selectedCSID.Value);
            var rows = allAccounts.Tables[allAccounts.BankAccountsTable.TABLENAME].Select(string.Format("{0}='{1}' and {2}='{3}'", allAccounts.BankAccountsTable.CLID, selectedClid, allAccounts.BankAccountsTable.CSID, selectedCsid));
            if (rows.Length > 0)
            {
                rows[0][allAccounts.BankAccountsTable.ISSELECTED] = true;
            }

            _summaryView = new DataView(allAccounts.Tables[allAccounts.BankAccountsTable.TABLENAME]) { Sort = allAccounts.BankAccountsTable.ISSELECTED + " Desc" };
            if (!String.IsNullOrEmpty(hfFilter.Value) && hfFilter.Value.ToLower().StartsWith("term"))
            {
                _summaryView.RowFilter = "AccountType = 'TERMDEPOSIT' and IsExternalAccount = false";
            }
            else
            {
                _summaryView.RowFilter = "AccountType <> 'TERMDEPOSIT' and IsExternalAccount = false";
            }

            Broker.ReleaseBrokerManagedComponent(org);

            PresentationGrid.Rebind();
        }

        public void SetEntity(Guid cid, Guid selectedClid, Guid selectedCsid, string linkedEntityType)
        {
            ClearEntity();
            lblmessages.Text = "";
            selectedCLID.Value = selectedClid.ToString();
            selectedCSID.Value = selectedCsid.ToString();
            hfFilter.Value = linkedEntityType;
            // LoadControls();
            if (cid != Guid.Empty)
            {
                includedAccount.Visible = true;
                SetIncluded(cid);
            }
        }

        private void ClearEntity()
        {
            includedAccount.Visible = false;
            txtSearchAccName.Text = "";
            _summaryView = new BankAccountDS().BankAccountsTable.DefaultView;
            PresentationGrid.Rebind();
            AccountName.Text = "";
            AccountType.Text = "";
            BSBNo.Text = "";
            AccountNo.Text = "";
            Institution.Text = "";
        }

        private void SetValues(string acconuntName, string accounttyep, string bsb, string accnumber, string institute)
        {
            AccountName.Text = acconuntName;
            AccountType.Text = accounttyep;
            BSBNo.Text = bsb;
            AccountNo.Text = accnumber;
            Institution.Text = institute;
        }

        private void SetIncluded(Guid cid)
        {
            BankAccountDS ds = GetDataSet(cid);
            DataRow dr = ds.Tables[ds.BankAccountsTable.TABLENAME].Rows[0];
            SetValues(dr[ds.BankAccountsTable.ACCOUNTNAME].ToString(), dr[ds.BankAccountsTable.ACCOUNTTYPE].ToString(), dr[ds.BankAccountsTable.BSBNO].ToString(), dr[ds.BankAccountsTable.ACCOUNTNO].ToString(), dr[ds.BankAccountsTable.INSTITUTION].ToString());
        }

        private BankAccountDS GetDataSet(Guid cid)
        {
            var brokerAccountDs = new BankAccountDS { CommandType = DatasetCommandTypes.Get };

            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
                clientData.GetData(brokerAccountDs);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            return brokerAccountDs;
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (_summaryView == null)
                LoadControls();
            if (_summaryView != null) PresentationGrid.DataSource = _summaryView.ToTable();
        }

        protected void PresentationGrid_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = e.Item as GridDataItem;

            var clid = new Guid(dataItem["CLid"].Text);
            var csid = new Guid(dataItem["CSid"].Text);
            var selectedClid = new Guid(selectedCLID.Value);
            var selectedCsid = new Guid(selectedCSID.Value);
            if (selectedClid != Guid.Empty && selectedCsid != Guid.Empty && clid == selectedClid && csid == selectedCsid)
            {
                var checkbox = (CheckBox)dataItem["ChkSelect"].FindControl("CheckBox1");
                checkbox.Checked = true;
            }
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            PresentationGrid.Rebind();
            if (ValidationFailed != null)
            {
                ValidationFailed();
            }
        }

        protected void btnUpdate_Onclick(object sender, EventArgs e)
        {
            if (Saved != null)
            {
                int selectCount = 0;
                int selectedRowIndex = -1;
                Guid clid = Guid.Empty;
                Guid csid = Guid.Empty;

                foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
                {
                    var checkbox = (CheckBox)dataItem["ChkSelect"].FindControl("CheckBox1");
                    if (checkbox.Checked)
                    {
                        clid = new Guid(dataItem["CLid"].Text);
                        csid = new Guid(dataItem["CSid"].Text);
                        _selectedByUser.Add(clid, csid);
                        selectCount++;
                        selectedRowIndex = dataItem.RowIndex;
                    }
                }
                if (selectedRowIndex == -1)
                {
                    lblmessages.Text = "Please Select Account.";

                }
                else if (selectCount > 1)
                {
                    lblmessages.Text = "Multiple Account selection is not allowed.";
                }
                else
                {
                    Saved(clid, csid);
                }

                if (selectedRowIndex == -1 || selectCount > 1)
                {
                    if (ValidationFailed != null)
                        ValidationFailed();
                }
            }
        }

        protected void btnCancel_Onclick(object sender, EventArgs e)
        {
            if (Canceled != null)
            {
                Canceled();
            }
        }
    }
}