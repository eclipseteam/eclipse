﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrganizationUnitMappingControl.ascx.cs"
    EnableViewState="true" Inherits="eclipseonlineweb.Controls.OrganizationUnitMappingControl" %>
<%@ Register TagPrefix="UC1" TagName="ExistingOrgUnitsControl" Src="~/Controls/ExistingOrgUnitsControl.ascx" %>
<%@ Register Src="NonIndividual.ascx" TagName="NonIndividual" TagPrefix="uc2" %>
<style type="text/css">
    .holder
    {
        width: 100%;
        display: block;
        z-index: 6;
    }
    .content
    {
        background: #fff;
        z-index: 7; /*  padding: 28px 26px 33px 25px;*/
    }
    .popup
    {
        border-radius: 7px;
        background: #6b6a63;
        margin: 30px auto 0;
        padding: 6px;
        position: absolute;
        width: 900px;
        top: 20%;
        left: 50%;
        margin-left: -400px;
        margin-top: -40px;
        z-index: 6;
    }
    .overlay
    {
        width: 100%;
        opacity: 0.65;
        height: 100%;
        left: 0; /*IE*/
        top: 10px;
        text-align: center;
        z-index: 5;
        position: fixed;
        background-color: #444444;
    }
</style>
<asp:Panel runat="server" ID="pnlIncludedUnits" Width="100%">
    <asp:HiddenField runat="server" ID="HfUnitPropertyName" Value="ShareHolders" />
    <table>
        <tr>
            <td>
                <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                    PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                    GridLines="None" AllowAutomaticDeletes="false" AllowFilteringByColumn="true"
                    AllowAutomaticInserts="false" AllowAutomaticUpdates="True" EnableViewState="true"
                    ShowFooter="true" OnNeedDataSource="PresentationGrid_OnNeedDataSource" OnItemCommand="PresentationGrid_OnItemCommand">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                        Name="Banks" TableLayout="Fixed" ShowFooter="true">
                        <CommandItemTemplate>
                            <div style="padding: 5px 5px;">
                                <telerik:RadButton runat="server" ID="btnAddExisting" OnClick="lnkbtnAddExistngMembers_Click"
                                    ToolTip="Add Existing Share Holder">
                                    <ContentTemplate>
                                        <img src="../images/add_existing.png" alt="" class="btnImageWithText" />
                                        <span class="riLabel">Add Existing Share Holder</span>
                                    </ContentTemplate>
                                </telerik:RadButton>
                                &nbsp;&nbsp;
                                <telerik:RadButton runat="server" ID="btnAddNonIndividualShareHolder" OnClick="lnkbtnAddNonIndividualShareHolder_Click"
                                    ToolTip="Add New Non-Individual Share Holder">
                                    <ContentTemplate>
                                        <img src="../images/add-icon.png" alt="" class="btnImageWithText" />
                                        <span class="riLabel">Add New Non-Individual Share Holder</span>
                                    </ContentTemplate>
                                </telerik:RadButton>
                            </div>
                        </CommandItemTemplate>
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="CID" ReadOnly="true" HeaderText="CID" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="CID" UniqueName="CID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="CLID" ReadOnly="true" HeaderText="CLID"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="CLID" UniqueName="CLID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="CSID" ReadOnly="true" HeaderText="CSID"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="CSID" UniqueName="CSID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="Name" ReadOnly="true" HeaderText="Name"
                                FilterControlWidth="180px" HeaderStyle-Width="60%" ItemStyle-Width="60%" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="Name" UniqueName="Name">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" SortExpression="Type" HeaderText="Type"
                                FilterControlWidth="80px" HeaderStyle-Width="20%" ItemStyle-Width="20%" AutoPostBackOnFilter="true"
                                ShowFilterIcon="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                                DataField="Type" UniqueName="Type">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="Status" HeaderText="Status" ReadOnly="true"
                                FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="Status" UniqueName="Status">
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn ConfirmText="Are you sure you want to remove the association?"
                                FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%" ButtonType="ImageButton"
                                CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                <HeaderStyle Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
    <div class="overlay" id="OVER" visible="False" runat="server">
    </div>
    <div id="MemershipPopup" runat="server" visible="false" class="holder">
        <div class="popup">
            <div class="popup_Titlebar" id="PopupHeader" style="width: 100%">
                <div class="TitlebarLeft" style="width: 100%">
                    <table width="98%">
                        <tr>
                            <td style="text-align: left;">
                                <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
                            </td>
                            <td style="text-align: right;">
                                <asp:ImageButton runat="server" ID="btnClose" ImageUrl="~/images/cross_icon_normal.png"
                                    OnClick="btnClose_OnClick" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="content">
                <UC1:ExistingOrgUnitsControl ID="ExistingOrgUnitsControl" runat="server" />
            </div>
        </div>
    </div>
    <div id="NonIndividualModal" runat="server" visible="false" class="holder">
        <div class="popup">
            <div class="content">
                <uc2:NonIndividual ID="NonIndividualControl" runat="server" />
            </div>
        </div>
    </div>
</asp:Panel>
