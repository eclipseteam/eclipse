﻿using System;
using System.Data;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;
using System.Web;
using System.Linq;
using System.IO;
using System.Drawing;
using Aspose.Words;
using Aspose.Words.Tables;
using Image = System.Web.UI.WebControls.Image;
using Table = Aspose.Words.Tables.Table;

namespace eclipseonlineweb.Controls
{
    public partial class BuyAtCallRates : UserControl
    {
        private DataView _tdHoldingDataView;

        public string ClientCID
        {
            private get
            {
                return hfClientCID.Value;
            }
            set
            {
                hfClientCID.Value = value;
            }
        }

        private string ClientAccID
        {
            get
            {
                return hfClientID.Value;
            }
            set
            {
                hfClientID.Value = value;
            }
        }

        public bool IsAdmin
        {
            private get
            {
                return Convert.ToBoolean(hfIsAdmin.Value);
            }
            set
            {
                hfIsAdmin.Value = value.ToString();
            }
        }

        public bool IsAdminMenu
        {
            private get
            {
                if (string.IsNullOrEmpty(hfIsAdminMenu.Value))
                {
                    hfIsAdminMenu.Value = "false";
                }
                return Convert.ToBoolean(hfIsAdminMenu.Value);
            }
            set
            {
                hfIsAdminMenu.Value = value.ToString();
            }
        }

        private bool IsSMA
        {
            get
            {
                if (string.IsNullOrEmpty(hfIsSMA.Value))
                {
                    hfIsSMA.Value = "false";
                }
                return Convert.ToBoolean(hfIsSMA.Value);
            }
            set
            {
                hfIsSMA.Value = value.ToString();
            }
        }

        public Action<string, DataSet> SaveData { get; set; }

        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            PresentationGrid.MasterTableView.Caption = Tooltip.OrderPadBuyAtCall;
            searchAccountControl.SelectData += (clientCID, clientId, clientName) =>
            {
                ClientHeaderInfo1.SetEntity(new Guid(clientCID));
                ClientHeaderInfo1.Visible = true;
                ClientCID = clientCID;
                ClientAccID = clientId;
                SetClientManagementType();
                if (IsSMA)
                {
                    lblNotify.Visible = true;
                    lblNotify.Text = "This feature is not available for Super clients at the moment.";
                }
                else
                {
                    lblNotify.Visible = false;
                    LoadControls();
                    showDetails.Visible = true;
                    tdButtons.Visible = true;

                    PresentationGrid.Visible = true;
                    PresentationGrid.Rebind();
                    if (btnBack.Visible)
                    {
                        lblMsg.Visible = false;
                        btnBack.Visible = false;
                        btnSave.Visible = false;
                        imgItems.Visible = true;
                        PresentationDetailGrid.Visible = false;
                    }
                }

            };
            searchAccountControl.ShowHideModal += (show) =>
            {
                if (show)
                {
                    popupSearch.Show();
                }
                else
                {
                    popupSearch.Hide();
                }
            };


            lblMsg.Visible = false;

            if (!IsPostBack)
            {
                if (IsAdminMenu)
                {
                    btnSearch.Visible = true;
                    showDetails.Visible = false;
                    ClientHeaderInfo1.Visible = false;
                    tdButtons.Visible = false;
                    searchAccountControl.IsAdminMenu = true;
                    PresentationGrid.Visible = false;
                }
                else
                {
                    SetClientManagementType();
                    btnSearch.Visible = false;
                    showDetails.Visible = true;
                    ClientHeaderInfo1.Visible = true;
                    LoadControls();
                    tdButtons.Visible = true;
                    searchAccountControl.IsAdminMenu = false;
                    PresentationGrid.Visible = true;
                }
            }
        }

        private HoldingRptDataSet GetData(Guid cid)
        {
            IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
            var ds = new HoldingRptDataSet();
            clientData.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(clientData);
            if (ds.Tables[BaseClientDetails.CLIENTSUMMARYTABLE].Rows.Count > 0)
            {
                hfClientType.Value = ds.Tables[BaseClientDetails.CLIENTSUMMARYTABLE].Rows[0][BaseClientDetails.CLIENTAPPLICABILITYTYPE].ToString();
            }
            return ds;
        }

        private void LoadControls()
        {
            BindServiceType();
            BindBankAccounts();
        }

        private void BindServiceType()
        {
            //Getting Service type according to user type
            var ds = GetData(new Guid(ClientCID));
            OrderPadUtilities.FillServiceTypes(cmbServiceType, ds, IsAdmin, OrderAccountType.AtCall);
        }

        private void BindBankAccounts()
        {
            //Getting cash bank account according to service type
            var ds = GetData(new Guid(ClientCID));

            //From Items
            string fromFilter = string.Format("{0}='{1}' and {2}='{3}' and {4}='cash'", ds.HoldingSummaryTable.LINKEDENTITYTYPE, OrganizationType.BankAccount, ds.HoldingSummaryTable.SERVICETYPE, cmbServiceType.SelectedValue, ds.HoldingSummaryTable.ASSETNAME);
            OrderPadUtilities.FillBankNAvailableFunds(cmbFromCashAccount, ds, fromFilter, IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA, Broker);
            OrderPadUtilities.SetAvailableFunds(cmbFromCashAccount, lblFromAvailableFunds, lblCashBalance, lblMinCash, IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA);
        }

        protected void btnCutOff_OnClick(object sender, EventArgs e)
        {
            FileHelper.DownLoadFile(FileHelper.CutOffTimeFilePath, FileHelper.CutOffTimeFileName);
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            lblMsg.Visible = false;
            lblMsg.Text = "";

            //Validate funds
            if (!ValidateAmount())
            {
                lblMsg.Visible = true;
                return;
            }

            //Checking Existing Order CM ID
            Guid orderCMCID = OrderPadUtilities.IsOrderCmExists(Broker, (Page as UMABasePage).GetCurrentUser());

            var unit = new OrganizationUnit
            {
                Name = "Order " + DateTime.Now.ToString("dd MMM, yyyy hh:mm:ss"),
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = (Page as UMABasePage).GetCurrentUser()
            };
            var ds = new OrderPadDS
            {
                CommandType = DatasetCommandTypes.Add,
                Unit = unit,
                Command = (int)WebCommands.AddNewOrganizationUnit
            };

            //Add rows in order pad ds
            AddRows(ds);

            if (SaveData != null)
            {
                SaveData(orderCMCID.ToString(), ds);
            }

            lblMsg.Visible = true;
            lblMsg.Text = ds.ExtendedProperties["Message"].ToString();

            if (ds.ExtendedProperties["Result"].ToString() == OperationResults.Successfull.ToString())
            {
                BindBankAccounts();
                PresentationDetailGrid.Rebind();
                string path = CreateReceipt(ds);
                lblMsg.Text = string.Format("{0} Click <a href = '../SysAdministration/DownloadOrderReceipt.aspx?filename={1}'>here</a> to print its receipt.", lblMsg.Text, path);
            }
        }
        private string GetNewFilePath()
        {
            string fileName = string.Format("{0}_{1}_BuyAtCall_{2}.PDF", ClientHeaderInfo1.ClientId ?? ClientAccID, lblMsg.Text.Split(' ')[1], DateTime.Now.ToShortDateString().Replace("/", "-"));
            string dirOrderReceipts = string.Format("{0}\\OrderReceipts", HttpContext.Current.Server.MapPath("~/App_Data"));
            if (!Directory.Exists(dirOrderReceipts))
                Directory.CreateDirectory(dirOrderReceipts);
            string filePath = string.Format("{0}\\{1}", dirOrderReceipts, fileName);
            return filePath;
        }


        private void AddCellToSheet(DocumentBuilder builderExl, string text, double width, Color backColor,
                                    bool isFontBold, int fontSize, int lineWidth, CellVerticalAlignment vAlign,
                                    TextOrientation orientation, ParagraphAlignment paragraph)
        {
            builderExl.InsertCell();
            builderExl.CellFormat.PreferredWidth = PreferredWidth.FromPoints(width);
            builderExl.CellFormat.VerticalAlignment = vAlign;
            builderExl.CellFormat.Shading.BackgroundPatternColor = backColor;
            builderExl.CellFormat.Orientation = orientation;
            builderExl.Bold = isFontBold;
            builderExl.Font.Color = Color.Black;
            builderExl.Font.Size = fontSize;
            builderExl.CellFormat.Borders.LineWidth = lineWidth;
            builderExl.CurrentParagraph.ParagraphFormat.Alignment = paragraph;
            builderExl.Write(text);
        }

        private string CreateReceipt(OrderPadDS ds)
        {
            int bodyFontSize = 9;
            double sum = 0;
            DataRow clientSummaryRow = GetData(new Guid(ClientCID)).Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0];
            string OutPath = string.Empty;
            string path = Server.MapPath("~/Templates/OrderPad_BuyAtCall_Template2.docx");
            Aspose.Words.Document document = null;
            document = new Aspose.Words.Document(path);
            //This is portion for the Header Section of the Document
            document.Range.Replace("[Eclipse_OrderNo]", string.Format("Order {0}", lblMsg.Text.Split(' ')[1]), false, false);
            document.Range.Replace("[Eclipse_OrderBy]", ((Page as UMABasePage).GetCurrentUser()).CurrentUserName, false, false);
            document.Range.Replace("[Eclipse_OrderDate]", DateTime.Now.ToString("dd/MM/yyyy"), false, false);
            document.Range.Replace("[Eclipse_InvestorName]", clientSummaryRow[HoldingRptDataSet.CLIENTNAME].ToString(), false, false);
            document.Range.Replace("[Eclipse_ClientID]", ClientHeaderInfo1.ClientId ?? ClientAccID, false, false);
            document.Range.Replace("[Eclipse_AccountType]", clientSummaryRow[HoldingRptDataSet.CLIENTTYPE].ToString(), false, false);
            document.Range.Replace("[Eclipse_Adviser]", clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME].ToString(), false, false);
            document.Range.Replace("[Eclipse_ServiceTye]", cmbServiceType.SelectedItem.Text.ToUpper(), false, false);
            document.Range.Replace("[A11]", clientSummaryRow[HoldingRptDataSet.CLIENTNAME].ToString(), false, false);
            string addressLine1And2 = string.Empty;
            if (clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2].ToString() == string.Empty)
                addressLine1And2 = clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1].ToString();
            else
                addressLine1And2 = clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1].ToString() + ", " + clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2].ToString();
            document.Range.Replace("[A12]", addressLine1And2, false, false);
            document.Range.Replace("[A13]", string.Format("{0} {1} {2}", clientSummaryRow[HoldingRptDataSet.SUBURB], clientSummaryRow[HoldingRptDataSet.STATE], clientSummaryRow[HoldingRptDataSet.POSTCODE]), false, false);
            document.Range.Replace("[A14]", clientSummaryRow[HoldingRptDataSet.COUNTRY].ToString(), false, false);
            if (document != null)
            {
                OutPath = GetNewFilePath();
                //Now here we try to Make Dynamic Table
                DocumentBuilder builderExl = new DocumentBuilder(document);
                //This is Point where to Start Generate Table
                builderExl.MoveToBookmark("EclipseByAtCall", false, true);
                Table table = builderExl.StartTable();

                builderExl.PageSetup.LeftMargin = ConvertUtil.InchToPoint(0.5);
                builderExl.PageSetup.TopMargin = ConvertUtil.InchToPoint(0.5);
                builderExl.PageSetup.BottomMargin = ConvertUtil.InchToPoint(0.5);

                builderExl.StartTable();

                builderExl.InsertCell();
                builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                builderExl.CellFormat.Borders.LineWidth = 0;
                builderExl.EndRow();
                builderExl.EndTable();
                builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Left;
                builderExl.Writeln("");

                AddCellToSheet(builderExl, "BSB", 50, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, "Account No", 80, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, "Account Name", 140, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, "Amount", 52, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                //AddCellToSheet(builderExl, "Account", 52, Color.DarkGray, true, bodyFontSize,  1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, "Rate", 104, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, "ADI", 62, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, "Broker", 52, Color.DarkGray, true, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                builderExl.EndRow();
                //Here we Make DataColunm
                DataTable dt_ = new DataTable();

                dt_ = ds.Tables[ds.AtCallTable.TABLENAME].DefaultView.ToTable(false, "Amount", "Rate", "BankID", "BrokerID");
                DataRow drOrder = ds.Tables[ds.OrdersTable.TABLENAME].Rows[0];
                DataColumn dcBSB = new DataColumn("BSB");
                dcBSB.DefaultValue = drOrder[ds.OrdersTable.ACCOUNTBSB].ToString();
                dt_.Columns.Add(dcBSB);
                dt_.Columns["BSB"].SetOrdinal(0);
                DataColumn dcAccountNo = new DataColumn("AccountNo");
                dcAccountNo.DefaultValue = drOrder[ds.OrdersTable.ACCOUNTNUMBER].ToString();
                dt_.Columns.Add(dcAccountNo);
                dt_.Columns["AccountNo"].SetOrdinal(1);
                DataColumn dcAccountName = new DataColumn("AccountName");
                dcAccountName.DefaultValue = GetDataSet(new Guid(drOrder[ds.OrdersTable.ACCOUNTCID].ToString())).Tables[0].Rows[0]["AccountName"].ToString();
                dt_.Columns.Add(dcAccountName);
                dt_.Columns["AccountName"].SetOrdinal(2);
                //DataColumn dcAccount = new DataColumn("Account");
                //dcAccount.DefaultValue = string.Empty;
                //dt_.Columns.Add(dcAccount);
                //dt_.Columns["Account"].SetOrdinal(4);
                DataColumn dcRate = new DataColumn("Rate_");
                dcRate.DefaultValue = string.Empty;
                dt_.Columns.Add(dcRate);
                DataColumn dcADI = new DataColumn("ADI");
                dcADI.DefaultValue = string.Empty;
                dt_.Columns.Add(dcADI);
                DataColumn dcBroker = new DataColumn("Broker");
                dcBroker.DefaultValue = string.Empty;
                dt_.Columns.Add(dcBroker);
                SetTDData(dt_);
                for (int i = 0; i < dt_.Rows.Count; i++)
                {
                    string _Rate;
                    if (dt_.Rows[i]["Rate_"].ToString() == "&nbsp;")
                        _Rate = string.Empty;
                    else
                        _Rate = dt_.Rows[i]["Rate_"].ToString();

                    AddCellToSheet(builderExl, dt_.Rows[i]["BSB"].ToString(), 50, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                    AddCellToSheet(builderExl, dt_.Rows[i]["AccountNo"].ToString(), 80, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                    AddCellToSheet(builderExl, dt_.Rows[i]["AccountName"].ToString(), 140, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                    AddCellToSheet(builderExl, string.Format("{0:C}", dt_.Rows[i]["Amount"]), 52, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                    //AddCellToSheet(builderExl, dt_.Rows[i]["Account"].ToString(), 52, Color.White, false, bodyFontSize,  1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                    AddCellToSheet(builderExl, _Rate, 104, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                    AddCellToSheet(builderExl, dt_.Rows[i]["ADI"].ToString(), 62, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                    AddCellToSheet(builderExl, dt_.Rows[i]["Broker"].ToString(), 52, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                    builderExl.EndRow();
                    sum += Convert.ToDouble(dt_.Rows[i]["Amount"]);
                }
                //Here we add the Total Colunm
                AddCellToSheet(builderExl, "Total", 50, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Left);
                AddCellToSheet(builderExl, string.Empty, 80, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, string.Empty, 140, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, string.Format("{0:C}", sum), 52, Color.White, false, bodyFontSize, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Right);
                //AddCellToSheet(builderExl, string.Empty, 52, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, string.Empty, 104, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, string.Empty, 62, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                AddCellToSheet(builderExl, string.Empty, 52, Color.White, false, 0, 1, CellVerticalAlignment.Center, TextOrientation.Horizontal, ParagraphAlignment.Center);
                builderExl.EndRow();
                if (table != null)
                {
                    //table.AutoFit(AutoFitBehavior.FixedColumnWidths);
                    table.PreferredWidth = PreferredWidth.FromPoints(570);
                }
                document.Save(OutPath, SaveFormat.Pdf);
            }
            return FileHelper.GetFileName(OutPath);
        }
        private BankAccountDS GetDataSet(Guid cid)
        {
            var bankTransactionDetailsDS = new BankAccountDS { CommandType = DatasetCommandTypes.Get };
            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
                clientData.GetData(bankTransactionDetailsDS);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }
            return bankTransactionDetailsDS;
        }

        private void SetTDData(DataTable dt)
        {
            Guid BankID = Guid.Empty;
            Guid BrokerID = Guid.Empty;

            foreach (DataRow row in dt.Rows)
            {
                BankID = new Guid(row["BankID"].ToString());
                BrokerID = new Guid(row["BrokerID"].ToString());

                // Get/Set the Bank and Broker by their IDs
                var inst = (Broker.GetWellKnownBMC(WellKnownCM.Organization) as Oritax.TaxSimp.CM.Organization.IOrganization).Institution;
                InstitutionEntity ie = inst.FirstOrDefault(ss => ss.ID == BankID);
                row["ADI"] = ie.Name;
                ie = inst.FirstOrDefault(ss => ss.ID == BrokerID);
                row["Broker"] = ie.Name;

                row["Rate_"] = row["Rate"].ToString();
            }

            dt.Columns.Remove(dt.Columns["Rate"]);
            dt.Columns.Remove(dt.Columns["BankID"]);
            dt.Columns.Remove(dt.Columns["BrokerID"]);
        }

        private bool ValidateAmount()
        {
            decimal totalAmount = 0;
            foreach (GridDataItem dataItem in PresentationDetailGrid.MasterTableView.Items)
            {
                var txtAmount = (dataItem.FindControl("txtAmount") as RadNumericTextBox);

                if (txtAmount != null && (!string.IsNullOrEmpty(txtAmount.Text.Trim()) && Convert.ToDecimal(txtAmount.Text) > 0))
                {
                    totalAmount += Convert.ToDecimal(txtAmount.Text);
                }
            }

            if (totalAmount == 0)
            {
                lblMsg.Text = "Please Enter Amount";
                return false;
            }

            decimal availableFunds;
            decimal.TryParse(lblFromAvailableFunds.Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out availableFunds);
            if (totalAmount > availableFunds)
            {
                lblMsg.Text = "Insufficient Available Funds";
                return false;
            }
            return true;
        }

        private void AddRows(OrderPadDS ds)
        {
            //Order Row
            var orderId = AddOrderRow(ds);

            foreach (GridDataItem dataItem in PresentationDetailGrid.MasterTableView.Items)
            {
                var txtAmount = (dataItem.FindControl("txtAmount") as RadNumericTextBox);
                decimal amount = 0;
                if (txtAmount != null && !string.IsNullOrEmpty(txtAmount.Text))
                {
                    amount = Convert.ToDecimal(txtAmount.Text.Trim());
                }
                if (amount > 0)
                {
                    //Order Items Row
                    AddItemRow(ds, amount, dataItem, orderId);
                }
            }
        }

        private Guid AddOrderRow(OrderPadDS ds)
        {
            DataRow orderRow = ds.OrdersTable.NewRow();

            Guid orderId = Guid.NewGuid();
            orderRow[ds.OrdersTable.ID] = orderId;
            orderRow[ds.OrdersTable.CLIENTCID] = new Guid(ClientCID);
            orderRow[ds.OrdersTable.CLIENTID] = ClientHeaderInfo1.ClientId ?? ClientAccID;
            orderRow[ds.OrdersTable.ORDERACCOUNTTYPE] = OrderAccountType.AtCall;
            orderRow[ds.OrdersTable.ORDERTYPE] = OrderType.Manual;
            orderRow[ds.OrdersTable.STATUS] = OrderStatus.Active;
            orderRow[ds.OrdersTable.ACCOUNTCID] = new Guid(cmbFromCashAccount.SelectedValue);
            orderRow[ds.OrdersTable.ACCOUNTNUMBER] = cmbFromCashAccount.SelectedItem.Attributes["AccountNo"].Trim();
            orderRow[ds.OrdersTable.ACCOUNTBSB] = cmbFromCashAccount.SelectedItem.Attributes["BSB"].Trim();
            orderRow[ds.OrdersTable.ORDERITEMTYPE] = OrderItemType.Buy;
            orderRow[ds.OrdersTable.CLIENTMANAGEMENTTYPE] = IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA;

            ds.OrdersTable.Rows.Add(orderRow);
            return orderId;
        }

        private void AddItemRow(OrderPadDS ds, decimal amount, GridDataItem dataItem, Guid orderId)
        {
            DataRow itemsRow = ds.AtCallTable.NewRow();

            itemsRow[ds.AtCallTable.ID] = Guid.NewGuid();
            itemsRow[ds.AtCallTable.ORDERID] = orderId;
            itemsRow[ds.AtCallTable.AMOUNT] = amount;
            itemsRow[ds.AtCallTable.BANKID] = new Guid(dataItem["PriceInstituteID"].Text);
            itemsRow[ds.AtCallTable.BROKERID] = new Guid(dataItem["InstituteID"].Text);
            itemsRow[ds.AtCallTable.PRODUCTID] = string.IsNullOrEmpty(dataItem["ProductID"].Text) ? Guid.Empty.ToString() : dataItem["ProductID"].Text;
            itemsRow[ds.AtCallTable.ATCALLACCOUNTCID] = string.IsNullOrEmpty(dataItem["AtCallAccountCID"].Text) ? Guid.Empty.ToString() : dataItem["AtCallAccountCID"].Text;
            itemsRow[ds.AtCallTable.ACCOUNTTIER] = dataItem["AccountTier"].Text;
            itemsRow[ds.AtCallTable.ATCALLTYPE] = AtCallType.BestRates;

            decimal min;
            decimal.TryParse(dataItem["Min"].Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out min);
            decimal max;
            decimal.TryParse(dataItem["Max"].Text, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out max);

            itemsRow[ds.AtCallTable.MIN] = min;
            itemsRow[ds.AtCallTable.MAX] = max;
            itemsRow[ds.AtCallTable.RATE] = dataItem["Rate"].Text;

            ds.AtCallTable.Rows.Add(itemsRow);
        }

        protected void cmbFromCashAccount_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            OrderPadUtilities.SetAvailableFunds(cmbFromCashAccount, lblFromAvailableFunds, lblCashBalance, lblMinCash, IsSMA ? ClientManagementType.SMA : ClientManagementType.UMA);
        }

        protected void cmbServiceType_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            BindBankAccounts();
            PresentationGrid.Rebind();
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            popupSearch.Show();
            showDetails.Visible = false;
            ClientHeaderInfo1.Visible = false;
            tdButtons.Visible = false;
            searchAccountControl.HideModal = false;
            searchAccountControl.FindControl("SearchBox").Focus();
            PresentationGrid.Visible = false;
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        private void GetData()
        {
            IBrokerManagedComponent iBMC = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            var ds = new OrderPadAtCallRatesDS { CommandType = DatasetCommandTypes.Get };
            iBMC.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(iBMC);
            GetAccountProcessData();
            var dataView = ds.OrderPadAtCallRatesTable.DefaultView;
            dataView.RowFilter = string.Format("{0} like '%{1}%'", ds.OrderPadAtCallRatesTable.APPLICABILITY, hfClientType.Value);
            PresentationGrid.DataSource = dataView.ToTable();
        }

        private void GetAccountProcessData()
        {
            if (!string.IsNullOrEmpty(ClientCID))
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(new Guid(ClientCID));
                var ds = new ClientAccountProcessDS();
                clientData.GetData(ds);
                Broker.ReleaseBrokerManagedComponent(clientData);
                _tdHoldingDataView = new DataView(ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE]) { RowFilter = string.Format("{0} = '{1}'", ClientAccountProcessDS.LINKEDENTITYTYPE, OrganizationType.TermDepositAccount) };
                _tdHoldingDataView = _tdHoldingDataView.ToTable(true, ClientAccountProcessDS.INSTITUTEID, ClientAccountProcessDS.TASKID, ClientAccountProcessDS.CID, ClientAccountProcessDS.SERVICETYPE, ClientAccountProcessDS.STATUS).DefaultView;
            }
        }

        protected void PresentationDetailGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetDetailData(hfIds.Value);
        }

        private void GetDetailData(string ids)
        {
            IBrokerManagedComponent iBMC = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            var ds = new OrderPadAtCallRatesDS { CommandType = DatasetCommandTypes.Get };
            iBMC.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(iBMC);
            var dataView = ds.OrderPadAtCallRatesTable.DefaultView;
            dataView.RowFilter = string.Format("{0} in ({1})", ds.OrderPadAtCallRatesTable.ID, ids);
            PresentationDetailGrid.DataSource = dataView.ToTable();
            GetAccountProcessData();
        }

        protected void btnBack_OnClick(object sender, EventArgs e)
        {
            lblMsg.Visible = false;
            PresentationGrid.Visible = true;
            PresentationDetailGrid.Visible = false;
            btnBack.Visible = false;
            btnSave.Visible = false;
            imgItems.Visible = true;
            PresentationGrid.Rebind();
        }

        protected void imgItems_OnClick(object sender, EventArgs e)
        {
            hfIsAlert.Value = "";
            hfAtCallAccStatus.Value = "";
            string ids = string.Empty;
            foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
            {
                var chkItem = (dataItem.FindControl("chkItem") as CheckBox);

                if (chkItem != null && chkItem.Checked)
                {
                    ids += string.Format("CONVERT('{0}','System.Guid'),", new Guid(dataItem["ID"].Text));
                    if (string.IsNullOrWhiteSpace(hfIsAlert.Value))
                    {
                        if (string.IsNullOrEmpty(dataItem["ProductID"].Text))
                        {
                            hfIsAlert.Value = dataItem["InstituteName"].Text;
                        }
                        else if (dataItem["AtCallAccStatus"].Text.ToLower() != "active")
                        {
                            hfIsAlert.Value = dataItem["InstituteName"].Text;
                            hfAtCallAccStatus.Value = dataItem["AtCallAccStatus"].Text.ToLower();
                        }
                    }
                }
            }
            if (ids.Length > 0)
            {
                PresentationGrid.Visible = false;
                PresentationDetailGrid.Visible = true;
                btnBack.Visible = true;
                btnSave.Visible = true;
                imgItems.Visible = false;
                hfIds.Value = ids.Remove(ids.LastIndexOf(","));
                PresentationDetailGrid.MasterTableView.Caption = Tooltip.OrderPadBuyAtCall;
                PresentationDetailGrid.Rebind();
            }
        }

        protected void PresentationGrid_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            FillProductIDAtCallAccCID(e, true);
        }

        private void FillProductIDAtCallAccCID(GridItemEventArgs e, bool checkHMP = false)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = e.Item as GridDataItem;
            if (_tdHoldingDataView != null && _tdHoldingDataView.Count > 0)
            {
                foreach (DataRow dataRow in _tdHoldingDataView.ToTable().Rows)
                {
                    if (dataRow["InstituteID"].ToString() == dataItem["InstituteID"].Text &&
                        cmbServiceType.SelectedValue.ToLower() == dataRow["ServiceType"].ToString().ToLower())
                    {
                        dataItem["ProductID"].Text = dataRow["TaskID"].ToString();
                        dataItem["AtCallAccountCID"].Text = dataRow["CID"].ToString();
                        dataItem["AtCallAccStatus"].Text = dataRow["Status"].ToString();
                        break;
                    }
                    dataItem["ProductID"].Text = string.Empty;
                    dataItem["AtCallAccountCID"].Text = string.Empty;
                    dataItem["AtCallAccStatus"].Text = string.Empty;
                }
            }
            else
            {
                dataItem["ProductID"].Text = string.Empty;
                dataItem["AtCallAccountCID"].Text = string.Empty;
                dataItem["AtCallAccStatus"].Text = string.Empty;
            }
            if (checkHMP)
            {
                //Here we decide to show and hide the alert
                var chk = (CheckBox)e.Item.FindControl("chkItem");
                var imgAlert = (Image)e.Item.FindControl("ImgAlertImage");
                string honeymoonPeriod = dataItem["HoneymoonPeriod"].Text;
                string tooltip = Tooltip.BuyAtCallAccount(honeymoonPeriod, true);
                if (honeymoonPeriod == "0")
                {
                    imgAlert.Visible = false;
                    chk.ToolTip = string.Empty;
                }
                else
                {
                    imgAlert.Visible = true;
                    imgAlert.ToolTip = tooltip;
                    chk.ToolTip = tooltip;
                }
            }
        }

        protected void PresentationDetailGrid_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            FillProductIDAtCallAccCID(e);
        }

        private void SetClientManagementType()
        {
            IsSMA = SMAUtilities.IsSMAClient(new Guid(ClientCID), Broker);
            tdCashBalanceHead.Visible =
            tdCashBalanceValue.Visible =
            tdMinCashHead.Visible =
            tdMinCashValue.Visible = IsSMA;
        }
    }
}