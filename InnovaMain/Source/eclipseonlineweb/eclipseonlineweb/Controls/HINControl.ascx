﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HINControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.HINControl" %>
<script type="text/javascript">
    $(document).ready(function () {
        $(".autotab").keyup(function () {
            if ($(this).attr("maxlength") == $(this).val().length) {
                var index = $(".autotab").index(this);
                var item = $($(".autotab")[++index]);
                if (item.length > 0)
                    item.focus();
            }
        });
    });
</script>
<div class="popup_Container">
    <div class="PopupBody">
        <table>
            <tr>
                <td>
                    <asp:TextBox runat="server" Width="18" ID="TextAPIR1" MaxLength="1" CssClass="autotab"
                        Text="X" ReadOnly="True" />
                    <asp:Label ID="Label1" runat="server" Text=" " />
                    <asp:TextBox runat="server" Width="18" ID="TextAPIR2" MaxLength="1" CssClass="autotab" />
                    <asp:Label ID="Label2" runat="server" Text=" " />
                    <asp:TextBox runat="server" Width="18" ID="TextAPIR3" MaxLength="1" CssClass="autotab" />
                    <asp:Label ID="Label3" runat="server" Text=" " />
                    <asp:TextBox runat="server" Width="18" ID="TextAPIR4" MaxLength="1" CssClass="autotab" />
                    <asp:Label ID="Label4" runat="server" Text=" " />
                    <asp:TextBox runat="server" Width="18" ID="TextAPIR5" MaxLength="1" CssClass="autotab" />
                    <asp:Label ID="Label5" runat="server" Text=" " />
                    <asp:TextBox runat="server" Width="18" ID="TextAPIR6" MaxLength="1" CssClass="autotab" />
                    <asp:Label ID="Label6" runat="server" Text=" " />
                    <asp:TextBox runat="server" Width="18" ID="TextAPIR7" MaxLength="1" CssClass="autotab" />
                    <asp:Label ID="Label7" runat="server" Text=" " />
                    <asp:TextBox runat="server" Width="18" ID="TextAPIR8" MaxLength="1" CssClass="autotab" />
                    <asp:Label ID="Label8" runat="server" Text=" " />
                    <asp:TextBox runat="server" Width="18" ID="TextAPIR9" MaxLength="1" CssClass="autotab" />
                    <asp:Label ID="Label9" runat="server" Text=" " />
                    <asp:TextBox runat="server" Width="18" ID="TextAPIR10" MaxLength="1" CssClass="autotab" />
                    <asp:Label ID="Label10" runat="server" Text=" " />
                    <asp:TextBox runat="server" Width="18" ID="TextAPIR11" MaxLength="1" CssClass="autotab" />
                </td>
            </tr>
        </table>
    </div>
</div>
