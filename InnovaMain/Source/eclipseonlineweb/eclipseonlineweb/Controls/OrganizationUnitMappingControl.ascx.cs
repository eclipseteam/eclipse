﻿using System;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;
using Telerik.Web.UI;

namespace eclipseonlineweb.Controls
{
    public partial class OrganizationUnitMappingControl : System.Web.UI.UserControl
    {
        private string _cid;
        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        public string TitleText
        {
            get { return lblTitle.Text; }
            set { lblTitle.Text = value; }
        }

        public string UnitPropertyName {
            get { return HfUnitPropertyName.Value; }
            set { HfUnitPropertyName.Value = value; }
        }
       
        public string Filter
        {
            get { return ExistingOrgUnitsControl.Filter; }
            set { ExistingOrgUnitsControl.Filter = value; }
        }
        public Action<DataSet> SaveOrg { get; set; }
        public Action<string, DataSet> SaveUnit { get; set; }
        public Action<string, DataSet> SaveNonIndividual { get; set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            #region add existing events

            ExistingOrgUnitsControl.CanceledPopup += () => ShowHideExisting(false);
            ExistingOrgUnitsControl.ValidationFailed += () => ShowHideExisting(true);
            ExistingOrgUnitsControl.SaveData += (instanceCmds) =>
                {
                    SaveUnitData(instanceCmds, DatasetCommandTypes.Update);
                    ShowHideExisting(false);
                };


            NonIndividualControl.Saved += (CID, CLID, CSID) => HideShowNonIndividualPopup(false);
            NonIndividualControl.Canceled += () => HideShowNonIndividualPopup(false);
            NonIndividualControl.ValidationFailed += () => HideShowNonIndividualPopup(true);
            NonIndividualControl.SaveData += (Cid, ds) =>
                {
                    if (Cid == Guid.Empty.ToString())
                    {
                        SaveOrg(ds);
                    }
                    HideShowNonIndividualPopup(false);
                };

            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _cid = Request.QueryString["ins"];
        }

        
       
        protected void lnkbtnAddExistngMembers_Click(object sender, EventArgs e)
        {
            ShowHideExisting(true);
        }

        protected void lnkbtnAddNonIndividualShareHolder_Click(object sender, EventArgs e)
        {
            Guid gCid = Guid.Empty;
            NonIndividualControl.SetEntity(gCid);
            HideShowNonIndividualPopup(true);
        }

        protected void btnClose_OnClick(object sender, EventArgs e)
        {
            ShowHideExisting(false);
        }
        private void ShowHideExisting(bool show)
        {
            OVER.Visible = MemershipPopup.Visible = show;
        }

        private void HideShowNonIndividualPopup(bool show)
        {
            OVER.Visible = NonIndividualModal.Visible = show;
        }


        private void SaveUnitData(InstanceCMDS individualDs, DatasetCommandTypes commandType)
        {
            individualDs.CommandType = commandType;
            individualDs.UnitPropertyType = (UnitPropertyType)Enum.Parse(typeof(UnitPropertyType), this.UnitPropertyName);
            if (SaveUnit != null)
                SaveUnit(_cid, individualDs);

            PresentationGrid.Rebind();
        }

        private void SaveUnits(Guid cid, Guid clid, Guid csid, DatasetCommandTypes commandType)
        {
            var ds = new InstanceCMDS()
                         {
                            UnitPropertyType =(UnitPropertyType) Enum.Parse(typeof (UnitPropertyType), this.UnitPropertyName)
                         };
            DataRow dr = ds.InstanceCMDetailsTable.NewRow();
            dr[ds.InstanceCMDetailsTable.CID] = cid;
            dr[ds.InstanceCMDetailsTable.CLID] = clid;
            dr[ds.InstanceCMDetailsTable.CSID] = csid;

            ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows.Add(dr);
            SaveUnitData(ds, commandType);
            PresentationGrid.Rebind();
          
        }
       
        private void GetData()
        {
            if (!Request.Params["ins"].IsEmptyOrNull())
            {
                _cid = Request.Params["ins"];
                
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(new Guid(_cid));
                var ds = new InstanceCMDS() { CommandType = DatasetCommandTypes.GetChildren,UnitPropertyType = (UnitPropertyType)Enum.Parse(typeof(UnitPropertyType), this.UnitPropertyName) };
                clientData.GetData(ds);
                Broker.ReleaseBrokerManagedComponent(clientData);
                var summaryView = new DataView(ds.InstanceCMDetailsTable) { Sort = ds.InstanceCMDetailsTable.NAME + " ASC" };
                
                PresentationGrid.DataSource = summaryView;
            }
        }
       
      

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            switch (e.CommandName.ToLower())
            {
               case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var linkEntiyCLID = new Guid(dataItem["CLID"].Text);
                    var linkEntiyCSID = new Guid(dataItem["CSID"].Text);
                    var linkEntiyCID = new Guid(dataItem["CID"].Text);
                    
                    SaveUnits(linkEntiyCID,linkEntiyCLID, linkEntiyCSID, DatasetCommandTypes.Delete);
                    break;
            }
        }

        

    }
}