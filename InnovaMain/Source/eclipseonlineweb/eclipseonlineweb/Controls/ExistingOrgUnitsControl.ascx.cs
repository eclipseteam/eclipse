﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb.Controls
{
    public partial class ExistingOrgUnitsControl : UserControl
    {

        private ICMBroker UMABroker{get{return (Page as UMABasePage).UMABroker;}}
        public Action PageRefresh{get;set;}
        public Action CanceledPopup { get; set; }
        public Action ValidationFailed { get; set; }
        public Action<InstanceCMDS> SaveData { get; set; }
       
        public string Filter
        {
            get { return hfSearchFilter.Value; }
            set { hfSearchFilter.Value = value; }
        }
      


        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void GetUnitsData(object sender, GridNeedDataSourceEventArgs e)
        {
            if (txtSearchAccName.Text.Trim() != "")
            {
                dvExistingAccGrid.Visible = true;
                var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
                InstanceCMDS instanceCmds = new InstanceCMDS();
                instanceCmds.Command = (int)WebCommands.GetOrganizationAllUnitDetails;
                instanceCmds.Unit = new OrganizationUnit { Name = txtSearchAccName.Text, CurrentUser = (Page as UMABasePage).GetCurrentUser() };
                org.GetData(instanceCmds);
                DataView summaryView = new DataView(instanceCmds.InstanceCMDetailsTable);
                summaryView.Sort = instanceCmds.InstanceCMDetailsTable.NAME + " ASC";
                summaryView.RowFilter = string.Format("{0}<>'{1}' and {0}<>'{2}'", instanceCmds.InstanceCMDetailsTable.TYPE, "Orders", "SettledUnsettled") + (string.IsNullOrEmpty(Filter) ? string.Empty : " and (" + Filter+")");// do not include orders and settled unsettled objects
                PresentationGird.DataSource = summaryView;
                UMABroker.ReleaseBrokerManagedComponent(org);
            }
            else
            {
                dvExistingAccGrid.Visible = false;
            }
        }
        public void ClearEntity()
        {
            txtSearchAccName.Text = "";
            lblmessages.Text = "";
            PresentationGird.Rebind();
        }
        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            if (SaveData != null)
            {
                InstanceCMDS institutionContactsDs = new InstanceCMDS();

                if (PresentationGird.Items.Count > 0)
                {
                    bool iSSelected = false;
                    foreach (GridDataItem item in PresentationGird.Items)//loops through each grid row
                    {
                        Guid clid = Guid.Empty;
                        Guid csid = Guid.Empty;
                        Guid cid;
                        var checkCell = (item.FindControl("chkSelected") as CheckBox);
                        if (checkCell.Checked)
                        {
                            iSSelected = true;
                            clid = new Guid(item["Clid"].Text);
                            csid = new Guid(item["Csid"].Text);
                            cid = new Guid(item["Cid"].Text);
                            DataRow datarow = institutionContactsDs.InstanceCMDetailsTable.NewRow();
                            datarow[institutionContactsDs.InstanceCMDetailsTable.CLID] = clid;
                            datarow[institutionContactsDs.InstanceCMDetailsTable.CSID] = csid;
                            datarow[institutionContactsDs.InstanceCMDetailsTable.CID] = cid;
                            datarow[institutionContactsDs.InstanceCMDetailsTable.TYPE] = item["Type"].Text;
                            institutionContactsDs.InstanceCMDetailsTable.Rows.Add(datarow);  
                        }
                    }
                    if (!iSSelected)
                    {
                        lblmessages.Text = "Please select record for association";
                        return;
                    }
                    SaveData(institutionContactsDs);
                }
                if (PageRefresh != null)
                {
                    PageRefresh();
                }
                ClearEntity();
                if (CanceledPopup != null)
                {
                    CanceledPopup();
                }
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            PresentationGird.Rebind();
            if (ValidationFailed != null)
            {
                ValidationFailed();
            }
        }
    }
}