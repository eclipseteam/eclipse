﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdviserDashboard.ascx.cs" Inherits="eclipseonlineweb.Controls.AdviserDashboard" %>

<%@ Register tagName="Alerts" tagPrefix="uc" src="../JDash/Dashlets/Alerts.ascx" %>
<%@ Register tagName="Notifications" tagPrefix="uc" src="~/JDash/Dashlets/Notifications.ascx" %>
<%@ Register TagName="BestRates" TagPrefix="uc" Src="../JDash/Dashlets/BestRates.ascx" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1Chart" Assembly="C1.Web.Wijmo.Controls.4, Version=4.0.20121.56, Culture=neutral, PublicKeyToken=9b75583953471eea" %>

<table width="100%">
    <tr id="trAlerts" runat="server">
        <td valign="top" style="width:100%;">                        
            <div style="width:100%;margin-top:5px;">
                <div id="dvAlerts">
                    <fieldset style="height: 300px">
                        <legend >ALERTS</legend>                        
                            <uc:Alerts runat="server" ID="Alerts2" />  
                    </fieldset>
                </div>                               
            </div>            
        </td>
    </tr>    
    <tr id="trNotification" runat="server">
        <td valign="top" style="width:100%;">
            <div style="width:100%;margin-top:5px;">
                <div id="dvNotifications">
                    <fieldset style="height: 300px">
                    <legend >NOTIFICATIONS</legend>                        
                        <uc:Notifications runat="server" ID="Notifications" />
                    </fieldset>
                </div>
            </div>
        </td>
    </tr>    
     <tr id="trHolding" runat="server">
        <td valign="top" style="width: 100%;">
            <div>
                <fieldset>
                    <legend>Holdings (Dummy Data)</legend>
                    <div>
                        <table style="width: 100%; height: 275px">
                            <tr>
                                <td style="width: 50%">
                                    <telerik:RadHtmlChart runat="server" ID="PieChartHolding" Width="350px" Height="240px"
                                        Transitions="true" Skin="Metro">
                                        <Appearance>
                                            <FillStyle BackgroundColor="White"></FillStyle>
                                        </Appearance>
                                        <Legend>
                                            <Appearance BackgroundColor="White" Position="Left" Visible="true"></Appearance>
                                        </Legend>
                                        <PlotArea>
                                            <Appearance>
                                                <FillStyle BackgroundColor="White"></FillStyle>
                                            </Appearance>
                                            <Series>
                                                <telerik:PieSeries StartAngle="90">
                                                    <LabelsAppearance Position="Circle" DataFormatString="{0}">
                                                    </LabelsAppearance>
                                                    <TooltipsAppearance DataFormatString="{0}"></TooltipsAppearance>
                                                    <Items>
                                                        <telerik:SeriesItem BackgroundColor="#ff9900" Exploded="false" Name="Internet Explorer"
                                                            YValue="18.3"></telerik:SeriesItem>
                                                        <telerik:SeriesItem BackgroundColor="#cccccc" Exploded="false" Name="Firefox" YValue="35.8">
                                                        </telerik:SeriesItem>
                                                        <telerik:SeriesItem BackgroundColor="#999999" Exploded="false" Name="Chrome" YValue="38.3">
                                                        </telerik:SeriesItem>
                                                        <telerik:SeriesItem BackgroundColor="#666666" Exploded="false" Name="Safari" YValue="4.5">
                                                        </telerik:SeriesItem>
                                                        <telerik:SeriesItem BackgroundColor="#333333" Exploded="false" Name="Opera" YValue="2.3">
                                                        </telerik:SeriesItem>
                                                    </Items>
                                                </telerik:PieSeries>
                                            </Series>
                                        </PlotArea>
                                    </telerik:RadHtmlChart>
                                </td>
                                <td style="width: 50%">
                                    <c1:C1BarChart runat="server" Horizontal="false" ID="BarChartSetteledUnsettled" Height="240"
                                        Width="100%" Visible="false">
                                        <Legend Visible="true"></Legend>
                                        <Hint>
                                            <Content Function="hintContent" />
                                        </Hint>
                                    </c1:C1BarChart>
                                    <telerik:RadHtmlChart runat="server" ID="ColumnChart1" Transitions="true" Height="240px"
                                        Skin="Metro">
                                        <PlotArea>
                                            <Series>
                                                <telerik:ColumnSeries Name="Settled">
                                                    <TooltipsAppearance BackgroundColor="Orange" DataFormatString="{0}" />
                                                    <Items>
                                                        <telerik:SeriesItem YValue="15000" />
                                                    </Items>
                                                </telerik:ColumnSeries>
                                                <telerik:ColumnSeries Name="Un Settled">
                                                    <TooltipsAppearance BackgroundColor="Orange" DataFormatString="{0}" />
                                                    <Items>
                                                        <telerik:SeriesItem YValue="10000" />
                                                    </Items>
                                                </telerik:ColumnSeries>
                                                <telerik:ColumnSeries Name="Total">
                                                    <TooltipsAppearance BackgroundColor="Orange" DataFormatString="{0}" />
                                                    <Items>
                                                        <telerik:SeriesItem YValue="20000" />
                                                    </Items>
                                                </telerik:ColumnSeries>
                                            </Series>
                                            <XAxis AxisCrossingValue="0" MajorTickType="Outside" MinorTickType="Outside" Reversed="false"
                                                MajorTickSize="0" MinorTickSize="0">
                                                <Items>
                                                    <telerik:AxisItem LabelText="Holdings" />
                                                </Items>
                                                <LabelsAppearance DataFormatString="{0}" RotationAngle="0" />
                                                <MajorGridLines Color="#EFEFEF" Width="0" />
                                                <MinorGridLines Color="#F7F7F7" Width="0" />
                                            </XAxis>
                                            <YAxis AxisCrossingValue="0" MajorTickSize="0" MajorTickType="Outside" MinorTickSize="0"
                                                MinorTickType="Outside" MinValue="0" Reversed="false" Step="10000">
                                                <LabelsAppearance DataFormatString="{0}" RotationAngle="0" />
                                                <MajorGridLines Color="#EFEFEF" Width="0" />
                                                <MinorGridLines Color="#F7F7F7" Width="0" />
                                            </YAxis>
                                        </PlotArea>
                                        <Legend>
                                            <Appearance Position="Right" />
                                        </Legend>
                                    </telerik:RadHtmlChart>
                                </td>
                            </tr>
                        </table>
                    </div>
                </fieldset>
            </div>
        </td>
    </tr>
    <tr id="trBestRates" runat="server">
        <td>
            <table style="width: 100%; height: 300px; margin-top: 5px;">
                <tr>
                    <td valign="top" style="width: 75%; height: 275px">
                        <div>
                            <fieldset>
                                <legend>BEST RATES</legend>
                                <uc:BestRates runat="server" ID="BestRatesControl" />
                            </fieldset>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>    
</table>