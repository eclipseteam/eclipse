﻿using System;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;

namespace eclipseonlineweb.Controls
{
    public partial class ClientHeaderInfo : System.Web.UI.UserControl
    {
        private string cid;
        public string ClientId { get; set; }
        public string ClientName { get; set; }

        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["ins"] != null)
            {
                cid = Request.Params["ins"].Replace("?ins=", string.Empty);
                SetEntity(new Guid(cid));
            }
        }

        public void SetEntity(Guid cid)
        {
            if (cid != Guid.Empty)
            {
                ClientDetailsDS ds = GetDataSet(cid);
                DataTable dt = ds.Tables[ds.ClientDetailsTable.TABLENAME];
                if (dt.Rows.Count > 0)
                {
                    ClientId = dt.Rows[0][ds.ClientDetailsTable.CLIENTID].ToString();
                    ClientName = dt.Rows[0][ds.ClientDetailsTable.CLIENTNAME].ToString();
                    lblClientID.Text = ClientId;
                    lblClientName.Text = ClientName;
                }
            }
        }

        private ClientDetailsDS GetDataSet(Guid cid)
        {
            ClientDetailsDS clientDetailsDs = new ClientDetailsDS();
            clientDetailsDs.CommandType = DatasetCommandTypes.Get;

            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
                clientData.GetData(clientDetailsDs);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }

            return clientDetailsDs;
        }

        public Guid GetClientCLID()
        {
            Guid clientCLID = new Guid();

            if (Broker != null)
            {
                clientCLID = Broker.CIIDToCLID(new Guid(this.cid));
            }

            return clientCLID;
        }

    }
}