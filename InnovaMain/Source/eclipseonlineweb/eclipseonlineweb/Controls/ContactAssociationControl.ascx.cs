﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;
using Telerik.Web.UI;
using Oritax.TaxSimp.CM.Organization;
using OTD = Oritax.TaxSimp.DataSets;

namespace eclipseonlineweb.Controls
{
    public partial class ContactAssociationControl : UserControl
    {

        private ICMBroker UMABroker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        public Action PageRefresh
        {
            get;
            set;
        }

        public Action CanceledPopup { get; set; }

        public Action ValidationFailed { get; set; }

        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        public Guid CLID { get; set; }

        public Guid CSID { get; set; }

       

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private void LoadAvailableIndividualsForAssociation()
        {
           
            Title.Text = "Availabe Contacts for association";
             gvContantAssociation.DataSource=null;
            if (!String.IsNullOrEmpty(txtSearchAccName.Text))
            {
                var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
                var allIndividuals = new IndividualDS();
                allIndividuals.Command = (int) WebCommands.GetOrganizationUnitsByType;
                allIndividuals.Unit = new OrganizationUnit
                    {
                        Name = txtSearchAccName.Text,
                        CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                        Type = ((int) OrganizationType.Individual).ToString()
                    };
                org.GetData(allIndividuals);

                var summaryView = new DataView(allIndividuals.Tables[allIndividuals.IndividualTable.TABLENAME]);
                summaryView.Sort = allIndividuals.IndividualTable.NAME + " ASC";
                gvContantAssociation.DataSource = summaryView;
                UMABroker.ReleaseBrokerManagedComponent(org);
            }
          
        }

        public void ClearEntity()
        {
            txtSearchAccName.Text = "";
            lblmessages.Text = "";
            gvContantAssociation.Rebind();
            hfCID.Value = Guid.Empty.ToString();
            hfCLID.Value = Guid.Empty.ToString();
            hfCSID.Value = Guid.Empty.ToString();
        }

        private void SaveData()
        {
            string CID = Request.QueryString["ins"];
            var clientData = this.Broker.GetBMCInstance(new Guid(CID)) as OrganizationUnitCM;
            var clid = Guid.Empty;
            var csid = Guid.Empty;
            
            UMABroker.SaveOverride = true;
            bool iSSelected = false;
            foreach (GridDataItem item in gvContantAssociation.Items)
            {
                var checkCell = (CheckBox)item.FindControl("chkContactAssociation");
                if (checkCell.Checked)
                {
                    clid = new Guid(item["CLid"].Text);
                    csid = new Guid(item["CSid"].Text);
                    
                    if (clientData.SignatoriesApplicants.FirstOrDefault(associate => associate.Clid == clid && associate.Csid == csid) == null)
                    {
                        var identityCmDetail = new Oritax.TaxSimp.Common.IdentityCMDetail {Clid = clid, Csid = csid};
                        clientData.SignatoriesApplicants.Add(identityCmDetail);
                    }
                    iSSelected = true;
                }
            }
            if (!iSSelected)
            {
                lblmessages.Text = "Please select individual(s) for association";
                return;
            }
            clientData.CalculateToken(true);
            this.UMABroker.SetComplete();
            this.UMABroker.SetStart();

            if (PageRefresh != null)
            {
                PageRefresh();
            }
            ClearEntity();
            if (CanceledPopup != null)
            {
                CanceledPopup();
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            SaveData();
        }

       
        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (CanceledPopup != null)
            {
                ClearEntity();
                CanceledPopup();
            }
        }

        protected void btnSearchClient_OnClick(object sender, EventArgs e)
        {
            gvContantAssociation.Rebind();
            if (ValidationFailed != null)
            {
                ValidationFailed();
            }

        }

       

        protected void gvContantAssociation_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
           LoadAvailableIndividualsForAssociation();
        }

       

      
    }
}