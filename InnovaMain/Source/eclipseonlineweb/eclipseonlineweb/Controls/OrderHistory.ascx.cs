﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb.Controls
{
    public partial class OrderHistory : UserControl
    {
        private DataView _ordersView;
        private DataView _ssOrdersView;
        private DataView _asxOrdersView;
        private DataView _tdOrdersView;
        private DataView _atCallOrdersView;
        private bool IsFiltered
        {
            get
            {
                return !string.IsNullOrEmpty(hfIsFiltered.Value) && Convert.ToBoolean(hfIsFiltered.Value);
            }
            set
            {
                hfIsFiltered.Value = value.ToString();
            }
        }
        private string Filter
        {
            get { return hfFilter.Value; }
            set { hfFilter.Value = value; }
        }

        public Action<string, DataSet> SaveData { get; set; }

        public bool IsAdmin
        {
            private get
            {
                return Convert.ToBoolean(hfIsAdmin.Value);
            }
            set
            {
                hfIsAdmin.Value = value.ToString();
            }
        }

        public bool IsAdminMenu
        {
            private get
            {
                if (string.IsNullOrEmpty(hfIsAdminMenu.Value))
                {
                    hfIsAdminMenu.Value = "false";
                }
                return Convert.ToBoolean(hfIsAdminMenu.Value);
            }
            set
            {
                hfIsAdminMenu.Value = value.ToString();
            }
        }

        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClientHeaderInfo1.Visible = !IsAdminMenu;
                btnSMA.Visible = btnUMA.Visible = IsAdminMenu;
                SetClientManagementType();
                LoadControls();
                btnBulkDelete.Visible = btnExcel.Visible = IsAdmin;
                PresentationGrid.Columns.FindByUniqueNameSafe("BulkDelete").Visible = IsAdmin;
            }
        }

        private void LoadControls()
        {
            //Status List binding
            var orderStatus = Enum.GetNames(typeof(OrderStatus));
            foreach (var status in orderStatus)
            {
                if (status == OrderStatus.Cancelled.ToString() || status == OrderStatus.Complete.ToString())
                {
                    lstStatus.Items.Add(new RadListBoxItem(status));
                }
            }
            lstStatus.DataBind();


            foreach (RadListBoxItem item in lstStatus.Items)
            {
                item.Checked = true;
                item.Font.Bold = true;
            }

            ShowCheckedItems(lstStatus, true);
        }

        private void BindStatusCombo(string itemStatus)
        {
            cmbStatus.Items.Clear();
            OrderStatus orderStatus;
            bool isValidOrderStatus = Enum.TryParse(itemStatus, out orderStatus);
            if (isValidOrderStatus)
            {
                //filling status combo for manual update
                if (orderStatus == OrderStatus.Cancelled)
                {
                    cmbStatus.Items.Add(new RadComboBoxItem(OrderStatus.Active.ToString()));
                }
                else if (orderStatus == OrderStatus.Complete)
                {
                    cmbStatus.Items.Add(new RadComboBoxItem(OrderStatus.Submitted.ToString()));
                }
            }
        }

        protected void PresentationGridDetailTableDataBind(object source, GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = e.DetailTableView.ParentItem;
            string orderID = dataItem["ID"].Text;
            if (dataItem.OwnerTableView.Columns.FindByUniqueNameSafe("OrderAccountType") != null)
            {
                OrderAccountType orderAccountType;
                bool isValidOrderAccountType = Enum.TryParse(dataItem["OrderAccountType"].Text, out orderAccountType);

                OrderItemType orderItemType;
                bool isValidOrderItemType = Enum.TryParse(dataItem["OrderItemType"].Text, out orderItemType);

                if (isValidOrderAccountType)
                {
                    string filter = "OrderId='" + orderID + "'";
                    switch (orderAccountType)
                    {
                        case OrderAccountType.TermDeposit:
                            switch (e.DetailTableView.Name)
                            {
                                case "TDOrderDetails":
                                    {
                                        e.DetailTableView.DataSource = _tdOrdersView;
                                        _tdOrdersView.RowFilter = filter;

                                        if (isValidOrderItemType)
                                        {
                                            switch (orderItemType)
                                            {
                                                case OrderItemType.Sell:
                                                    e.DetailTableView.GetColumn("Min").Visible = false;
                                                    e.DetailTableView.GetColumn("Max").Visible = false;
                                                    e.DetailTableView.GetColumn("Rating").Visible = false;
                                                    e.DetailTableView.GetColumn("Duration").Visible = false;
                                                    e.DetailTableView.GetColumn("Percentage").Visible = false;
                                                    break;
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    {
                                        e.DetailTableView.Visible = false;
                                    }
                                    break;
                            }
                            break;
                        case OrderAccountType.StateStreet:
                            switch (e.DetailTableView.Name)
                            {
                                case "SSOrderDetails":
                                    {
                                        e.DetailTableView.DataSource = _ssOrdersView;
                                        _ssOrdersView.RowFilter = filter;
                                        if (isValidOrderItemType)
                                        {
                                            switch (orderItemType)
                                            {
                                                case OrderItemType.Buy:
                                                    e.DetailTableView.GetColumn("Units").Visible = false;
                                                    break;
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    {
                                        e.DetailTableView.Visible = false;
                                    }
                                    break;
                            }
                            break;
                        case OrderAccountType.ASX:
                            switch (e.DetailTableView.Name)
                            {
                                case "ASXOrderDetails":
                                    {
                                        e.DetailTableView.DataSource = _asxOrdersView;
                                        _asxOrdersView.RowFilter = filter;
                                    }
                                    break;
                                default:
                                    {
                                        e.DetailTableView.Visible = false;
                                    }
                                    break;
                            }
                            break;
                        case OrderAccountType.AtCall:
                            switch (e.DetailTableView.Name)
                            {
                                case "AtCallOrderDetails":
                                    {
                                        e.DetailTableView.DataSource = _atCallOrdersView;
                                        _atCallOrdersView.RowFilter = filter;
                                    }
                                    break;
                                default:
                                    {
                                        e.DetailTableView.Visible = false;
                                    }
                                    break;
                            }
                            break;
                    }
                }
            }
        }

        protected void PresentationGridItemCommand(object sender, GridCommandEventArgs e)
        {
            Guid orderId;
            switch (e.CommandName.ToLower())
            {
                case "delete":
                    orderId = Guid.Parse(e.CommandArgument.ToString());
                    ExecuteCommand(DatasetCommandTypes.Delete, orderId);
                    break;
                case "statusupdate":
                    hfOrderID.Value = e.CommandArgument.ToString();
                    var item = (GridDataItem)e.Item;
                    var status = ((HyperLink)item.FindControl("hypLinkStatus")).Text;
                    BindStatusCombo(status);
                    popupStatus.Show();
                    // refresh grid
                    PresentationGrid.Rebind();
                    break;
                case "incomplete":
                    orderId = Guid.Parse(e.CommandArgument.ToString());
                    ExecuteCommand(DatasetCommandTypes.Incomplete, orderId);
                    break;
            }

            //Collapse All Other Expanded Items
            if (e.CommandName == RadGrid.ExpandCollapseCommandName)
            {
                foreach (GridItem item in e.Item.OwnerTableView.Items)
                {
                    if (item.Expanded && item != e.Item)
                    {
                        item.Expanded = false;
                    }
                }
            }
        }

        protected void PresentationGridItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem && e.Item.OwnerTableView.Name == "AtCallOrderDetails")
            {
                var item = (GridDataItem)e.Item;
                if (item["AtCallType"].Text == AtCallType.MoneyMovement.ToString())
                {
                    item.OwnerTableView.GetColumn("BrokerName").Visible = false;
                    item.OwnerTableView.GetColumn("BankName").Visible = false;
                    item.OwnerTableView.GetColumn("Min").Visible = false;
                    item.OwnerTableView.GetColumn("Max").Visible = false;
                    item.OwnerTableView.GetColumn("Rate").Visible = false;
                    item.OwnerTableView.GetColumn("AccountTier").Visible = false;
                }
                else if (item["AtCallType"].Text == AtCallType.BestRates.ToString() || item["AtCallType"].Text == AtCallType.MoneyMovementBroker.ToString())
                {
                    item.OwnerTableView.GetColumn("TransferTo").Visible = false;
                    item.OwnerTableView.GetColumn("TransferAccBSB").Visible = false;
                    item.OwnerTableView.GetColumn("TransferAccNumber").Visible = false;
                    if (item["AtCallType"].Text == AtCallType.MoneyMovementBroker.ToString())
                    {
                        var headerItem = item.OwnerTableView.GetItems(GridItemType.Header)[0] as GridHeaderItem;
                        if (headerItem != null) headerItem["AccountTier"].Text = "Accounts";
                        item.OwnerTableView.GetColumn("Min").Visible = false;
                        item.OwnerTableView.GetColumn("Max").Visible = false;
                        item.OwnerTableView.GetColumn("Rate").Visible = false;
                    }
                }
            }

            if (!(e.Item is GridDataItem) || e.Item.OwnerTableView.Name != "ActiveOrders") return;
            var dataItem = (GridDataItem)e.Item;
            var orderId = dataItem["ID"].Text;
            var hypLinkClientId = dataItem["ClientID"].Controls[0] as HyperLink;
            var hyLinkStatus = (HyperLink)e.Item.FindControl("hypLinkStatus");
            bool isSMA = btnSMA.Checked;
            if (hyLinkStatus.Text == OrderStatus.Complete.ToString() && orderId != string.Empty)
            {
                if (IsAdminMenu)
                {
                    if (hypLinkClientId != null && hypLinkClientId.Text != string.Empty)
                    {
                        hyLinkStatus.NavigateUrl = string.Format("~/SysAdministration/SettledUnsettledTransactions.aspx?ClientId={0}&OrderId={1}&Page=2", hypLinkClientId.Text, orderId);
                    }
                }
                else
                {
                    if (Request.QueryString["ins"] != null)
                    {
                        hyLinkStatus.NavigateUrl = string.Format("~/ClientViews/SettledUnsettledTransactions.aspx?ins={0}&OrderId={1}&Page=2", Request.QueryString["ins"], orderId);
                    }
                }
            }
            else
            {
                hyLinkStatus.Font.Underline = false;
            }
            OrderStatus orderStatus;
            bool isValidOrderStatus = Enum.TryParse(hyLinkStatus.Text, out orderStatus);
            if (isValidOrderStatus)
            {
                OrderPadUtilities.SetStatusToolTip(dataItem, orderStatus);
                if (orderStatus != OrderStatus.Cancelled)
                {
                    var imgDelete = dataItem.FindControl("imgDelete") as ImageButton;
                    if (imgDelete != null) imgDelete.Visible = false;
                    var chkDelete = dataItem.FindControl("chkDelete") as CheckBox;
                    if (chkDelete != null) chkDelete.Visible = false;
                }

                if (IsAdmin)
                {
                    if (orderStatus == OrderStatus.Cancelled)
                    {
                        var imgUpdate = dataItem.FindControl("imgUpdate") as ImageButton;
                        if (imgUpdate != null) imgUpdate.Visible = true;
                    }

                    if (isSMA && orderStatus == OrderStatus.Complete && dataItem["OrderAccountType"].Text == OrderAccountType.TermDeposit.ToString())
                    {
                        var imgIncomplete = dataItem.FindControl("imgIncomplete") as ImageButton;
                        if (imgIncomplete != null)
                        {
                            imgIncomplete.Visible = true;
                        }
                    }
                }
            }
        }

        protected void PresentationGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetOrders();
        }

        private void GetOrders()
        {
            var ds = GetOrderPadDs();

            _ordersView = ds.Tables[ds.OrdersTable.TableName].DefaultView;
            _ssOrdersView = ds.Tables[ds.StateStreetTable.TableName].DefaultView;
            _asxOrdersView = ds.Tables[ds.ASXTable.TableName].DefaultView;
            _tdOrdersView = ds.Tables[ds.TermDepositTable.TableName].DefaultView;
            _atCallOrdersView = ds.Tables[ds.AtCallTable.TableName].DefaultView;

            if (IsFiltered)
            {
                _ordersView.RowFilter = Filter;
            }

            //sort by order id/number desc 
            _ordersView.Sort = string.Format("{0} DESC", ds.OrdersTable.ORDERID);
            PresentationGrid.DataSource = _ordersView;
            PresentationGrid.Columns.FindByUniqueName("ClientID").Visible = IsAdminMenu;
        }

        private void GetData()
        {
            var orderUnit = new OrganizationUnit
                {
                    Type = ((int)OrganizationType.Order).ToString(),
                    CurrentUser = (Page as UMABasePage).GetCurrentUser()
                };

            if (!IsAdminMenu)
            {
                orderUnit.ClientId = ClientHeaderInfo1.ClientId;
            }
            //Orders Lists
            var ds = new OrderPadDS
                {
                    Unit = orderUnit,
                    CommandType = DatasetCommandTypes.Get,
                    Command = (int)WebCommands.GetOrganizationUnitsByType
                };
            if (Request.QueryString["orderId"] != null)
            {
                Guid orderId;
                Guid.TryParse(Request.QueryString["orderId"], out orderId);
                if (orderId != Guid.Empty)
                {
                    ds.OrderId = orderId;
                }
            }

            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            //Orders Lists
            org.GetData(ds);

            Broker.ReleaseBrokerManagedComponent(org);

            ((UMABasePage)Page).PresentationData = ds;
        }

        protected void btnApplyFilter_OnClick(object sender, EventArgs e)
        {
            ShowCheckedItems(lstStatus, true);
        }

        private void ShowCheckedItems(RadListBox listBox, bool isFilter)
        {
            foreach (RadListBoxItem item in lstStatus.Items)
            {
                if (!item.Checked)
                    item.Font.Bold = false;
            }
            var sb = new StringBuilder();
            IList<RadListBoxItem> collection = listBox.CheckedItems;
            foreach (RadListBoxItem item in collection)
            {
                sb.Append("'" + item.Value + "', ");
            }

            string filterStatus = sb.Remove(sb.ToString().Trim().Length - 1, 1).ToString();

            if (isFilter)
            {
                FilterRecords(filterStatus);
            }
        }

        private void FilterRecords(string filterStatus)
        {
            string typeFilter = string.Format("ClientManagementType = '{0}'", btnSMA.Checked ? btnSMA.Value : btnUMA.Value);
            string statusFilter = string.Format("Status in ({0})", filterStatus);
            string dateFilter = "";
            if (!string.IsNullOrEmpty(calFrom.SelectedDate.ToString()) && !string.IsNullOrEmpty(calTo.SelectedDate.ToString()))
            {
                dateFilter = string.Format(" and ((CreatedDate >= #{0}# and CreatedDate <= #{1}#) OR (UpdatedDate >= #{0}# and UpdatedDate <= #{1}#))",
                                               Convert.ToDateTime(calFrom.SelectedDate).ToString("MM/dd/yyyy"), Convert.ToDateTime(calTo.SelectedDate).ToString("MM/dd/yyyy 23:59:59"));
            }
            else
            {
                if (!string.IsNullOrEmpty(calFrom.SelectedDate.ToString()))
                {
                    dateFilter = string.Format(" and (CreatedDate >= #{0}# OR UpdatedDate >= #{0}#)",
                                               Convert.ToDateTime(calFrom.SelectedDate).ToString("MM/dd/yyyy"));
                }

                if (!string.IsNullOrEmpty(calTo.SelectedDate.ToString()))
                {
                    dateFilter += string.Format(" and (CreatedDate <= #{0}# OR UpdatedDate <= #{0}#)",
                                                Convert.ToDateTime(calTo.SelectedDate).ToString("MM/dd/yyyy 23:59:59"));
                }
            }
            IsFiltered = true;

            Filter = string.Format("{0} and {1} {2}", typeFilter, statusFilter, dateFilter);
            if (IsPostBack)
                PresentationGrid.Rebind();
        }

        private void ExecuteCommand(DatasetCommandTypes command, Guid id = default(Guid), bool refreshGrid = true)
        {
            var unit = new OrganizationUnit
            {
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = (Page as UMABasePage).GetCurrentUser()
            };

            if (!IsAdminMenu)
            {
                unit.ClientId = ClientHeaderInfo1.ClientId;
            }

            var ds = new OrderPadDS { CommandType = command, Unit = unit };

            if (id != Guid.Empty)
            {
                DataRow dr = ds.OrdersTable.NewRow();
                dr[ds.OrdersTable.ID] = id;
                if (command == DatasetCommandTypes.Update)
                {
                    dr[ds.OrdersTable.STATUS] = cmbStatus.SelectedItem.Text;
                }
                ds.OrdersTable.Rows.Add(dr);
            }

            Guid orderCMCID = IsOrderCmExists();

            if (orderCMCID != Guid.Empty)
            {
                if (SaveData != null)
                {
                    SaveData(orderCMCID.ToString(), ds);
                }
            }

            if (refreshGrid)
            {
                // refresh grid
                PresentationGrid.Rebind();
            }
        }

        private Guid IsOrderCmExists()
        {
            var unit = new OrganizationUnit
            {
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = (Page as UMABasePage).GetCurrentUser()
            };

            var ds = new OrderPadDS
            {
                CommandType = DatasetCommandTypes.Check,
                Unit = unit,
                Command = (int)WebCommands.GetOrganizationUnitsByType
            };

            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            org.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(org);

            Guid orderCMCID = Guid.Empty;
            if (ds.OrdersTable.Rows.Count > 0)
            {
                //Getting ORDER CID 
                orderCMCID = new Guid(ds.OrdersTable.Rows[0][ds.OrdersTable.CID].ToString());
            }

            return orderCMCID;
        }

        protected void ShowHideDetailClick(object sender, EventArgs e)
        {
            var lbBtn = sender as LinkButton;
            if (lbBtn != null)
            {
                var item = (GridDataItem)(lbBtn).NamingContainer;
                item.Expanded = !item.Expanded;
            }
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            Guid id = Guid.Parse(hfOrderID.Value);
            ExecuteCommand(DatasetCommandTypes.Update, id);
        }
        
        protected void btnBulkDelete_OnClick(object sender, EventArgs e)
        {
            bool isSelected = false;
            foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
            {
                var chkCancel = dataItem.FindControl("chkDelete") as CheckBox;
                if (chkCancel != null && chkCancel.Visible && chkCancel.Checked)
                {
                    isSelected = true;
                    Guid id = Guid.Parse(dataItem["ID"].Text);
                    ExecuteCommand(DatasetCommandTypes.Delete, id, false);
                }
            }
            if (isSelected)
            {
                // refresh grid
                PresentationGrid.Rebind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "showalert", "alert('Please select some orders first.');", true);
            }
        }

        protected void ToggleRowSelection(object sender, EventArgs e)
        {
            var checkBox = sender as CheckBox;
            if (checkBox != null)
            {
                var item = checkBox.NamingContainer as GridItem;
                if (item != null)
                    item.Selected = checkBox.Checked;
            }
            bool checkHeader = true;
            foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
            {
                var chkDelete = dataItem.FindControl("chkDelete") as CheckBox;
                if (chkDelete != null && !chkDelete.Checked)
                {
                    checkHeader = false;
                    break;
                }
            }
            var headerItem = PresentationGrid.MasterTableView.GetItems(GridItemType.Header)[0] as GridHeaderItem;
            if (headerItem != null)
            {
                var headerChkbox = headerItem.FindControl("headerChkbox") as CheckBox;
                if (headerChkbox != null)
                    headerChkbox.Checked = checkHeader;
            }
        }

        protected void ToggleSelectedState(object sender, EventArgs e)
        {
            var headerCheckBox = (sender as CheckBox);
            if (headerCheckBox != null)
            {
                foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
                {
                    var chkDelete = dataItem.FindControl("chkDelete") as CheckBox;
                    if (chkDelete != null && chkDelete.Visible)
                    {
                        chkDelete.Checked = headerCheckBox.Checked;
                        dataItem.Selected = headerCheckBox.Checked;
                    }
                }
            }
        }

        protected void btnExcel_OnClick(object sender, EventArgs e)
        {
            var ds = GetOrderPadDs();
            DataView dataView = ds.OrdersTable.DefaultView;
            if (IsFiltered)
            {
                dataView.RowFilter = Filter;
            }
            //sort by order id/number desc 
            dataView.Sort = string.Format("{0} DESC", ds.OrdersTable.ORDERID);
            var excelDataset = new DataSet();
            DataTable dt = dataView.ToTable();
            foreach (DataColumn col in ds.OrdersTable.Columns)
            {
                if (col.DataType == typeof(Guid))
                {
                    dt.Columns.Remove(col.ColumnName);
                }
            }
            dt.Columns.Remove(ds.OrdersTable.FILENAME);
            dt.Columns.Remove(ds.OrdersTable.ATTACHMENT);
            excelDataset.Merge(dt, true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, string.Format("ArchivedOrders{0}.xls", DateTime.Now.ToFileTime()), Page.Response);
        }

        private OrderPadDS GetOrderPadDs()
        {
            if (!Page.IsPostBack || ((UMABasePage)Page).PresentationData == null)
            {
                GetData();
            }

            var ds = ((UMABasePage)Page).PresentationData as OrderPadDS;
            return ds;
        }

        protected void btnUMA_OnClick(object sender, EventArgs e)
        {
            ShowCheckedItems(lstStatus, true);
        }

        protected void btnSMA_OnClick(object sender, EventArgs e)
        {
            ShowCheckedItems(lstStatus, true);
        }

        private void SetClientManagementType()
        {
            if (!IsAdminMenu && Request.QueryString["ins"] != null)
            {
                var clientCid = Request.Params["ins"].Replace("?ins=", string.Empty);
                btnSMA.Checked = SMAUtilities.IsSMAClient(new Guid(clientCid), Broker);
            }

            if (IsAdminMenu)
            {
                if (Request.QueryString["type"] != null)
                {
                    var type = Request.QueryString["type"];
                    ClientManagementType clientType;
                    Enum.TryParse(type, true, out clientType);
                    if (clientType == ClientManagementType.SMA)
                    {
                        btnSMA.Checked = true;
                    }
                }
            }
        }
    }
}