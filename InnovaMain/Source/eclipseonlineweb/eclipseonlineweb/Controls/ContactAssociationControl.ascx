﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactAssociationControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.ContactAssociationControl" %>
<div class="popup_Container">
    <div class="popup_Titlebar" id="PopupHeader">
        <div class="TitlebarLeft">
            <asp:Label ID="Title" runat="server" Text="Contact Associatons"></asp:Label>
            <asp:HiddenField ID="hfCID" runat="server" />
            <asp:HiddenField ID="hfCLID" runat="server" />
            <asp:HiddenField ID="hfCSID" runat="server" />
        </div>
    </div>
    <div class="popup_Body">
        <div runat="server" id="dvSearchtxt">
            <table>
                <tr>
                    <td>
                        <strong>Filter available contacts</strong>
                    </td>
                    <td>
                        <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtSearchAccName" Width="400"></asp:TextBox>
                    </td>
                    <td>
                        <telerik:RadButton runat="server" ID="btnSearch" Text="Filter" OnClick="btnSearchClient_OnClick">
                        </telerik:RadButton>
                    </td>
                </tr>
            </table>
        </div>
        <div runat="server" id="dvExistingAccGrid" style="width: 850px">
            <telerik:RadGrid ID="gvContantAssociation" runat="server" AutoGenerateColumns="False"
                 OnNeedDataSource="gvContantAssociation_OnNeedDataSource"
                PageSize="10" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                GridLines="None" EnableViewState="True" ShowFooter="True">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="Cid" ReadOnly="true" HeaderText="Cid" HeaderStyle-Width="85px"
                            Display="False" HeaderButtonType="TextButton" DataField="Cid" UniqueName="Cid">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="CLid" ReadOnly="true" HeaderText="CLid"
                            Display="False" HeaderStyle-Width="200px" HeaderButtonType="TextButton" DataField="CLid"
                            UniqueName="CLid">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="CSid" ReadOnly="true" HeaderText="CSid"
                            Display="False" HeaderButtonType="TextButton" DataField="CSid" UniqueName="CSid">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn HeaderText="Select" AllowFiltering="False" HeaderStyle-Width="30px">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkContactAssociation" runat="server" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridNumericColumn SortExpression="Name" ReadOnly="true" HeaderText="Given Name"
                            HeaderButtonType="TextButton" DataField="Name" UniqueName="Name">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn ReadOnly="true" SortExpression="MiddleName" HeaderText="Middle Name"
                            ShowFilterIcon="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                            DataField="MiddleName" UniqueName="MiddleName">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn SortExpression="SurName" HeaderText="Family Name"
                            ReadOnly="true" HeaderButtonType="TextButton" DataField="SurName" UniqueName="SurName">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn SortExpression="EmailAddress" ReadOnly="true" HeaderText="Email Address"
                            DataField="EmailAddress" UniqueName="EmailAddress">
                        </telerik:GridNumericColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </div>
        <div>
            <asp:Label runat="server" ID="lblmessages" Font-Size="Medium" Font-Bold="True" ForeColor="Red" />
            <br />
            <br />
        </div>
        <div align="right" style="width: 850px">
            <telerik:RadButton ID="btnUpdate" runat="server" Text="Save" ToolTip="Save" OnClick="btnSave_OnClick">
            </telerik:RadButton>
            <telerik:RadButton runat="server" Text="Cancel" ToolTip="Cancel" ID="btnCancel" OnClick="btnCancel_OnClick">
            </telerik:RadButton>
        </div>
    </div>
</div>
