﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MISTransactionControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.MISTransactionControl" %>
<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1Input" Assembly="C1.Web.Wijmo.Controls.4, Version=4.0.20121.56, Culture=neutral, PublicKeyToken=9b75583953471eea" %>
<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" Assembly="C1.Web.Wijmo.Controls.4, Version=4.0.20121.56, Culture=neutral, PublicKeyToken=9b75583953471eea" %>
<table>
    <tr>
    </tr>
    <tr>
        <td>
        </td>
        <td colspan="2">
            <asp:Label runat="server" ForeColor="Red" ID="lblMessages" ></asp:Label>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td style="width: 200px">
            MIS Account
        </td>
        <td>
            <c1:C1ComboBox ID="cmbMISAcc" runat="server" Width="250px" AutoPostBack="true" OnSelectedIndexChanged="Mis_Changed">
            </c1:C1ComboBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td style="width: 200px">
            Fund Name
        </td>
        <td>
            <c1:C1ComboBox ID="cmbFundCode" runat="server" Width="250px">
            </c1:C1ComboBox>
            <asp:HiddenField runat="server" ID="lblTranID"></asp:HiddenField>
            <asp:HiddenField runat="server" ID="lblFundCid"></asp:HiddenField>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            Transaction Type:
        </td>
        <td>
            <c1:C1ComboBox ID="C1ComboBoxTrasactionType" runat="server" Width="410px" CssClass=""
                ViewStateMode="Enabled" Height="100px" EnableTheming="false" TriggerPosition="Right"
                ShowTrigger="true" ShowingAnimation-Animated-Disabled="true">
                <Items>
                    <c1:C1ComboBoxItem Value="VA" Text="Reversal - Redemption / Withdrawal / Fee / Tax" />
                    <c1:C1ComboBoxItem Value="AE" Text="Rights Issue" />
                    <c1:C1ComboBoxItem Value="RA" Text="Sale" />
                    <c1:C1ComboBoxItem Value="ST" Text="Surcharge Tax" />
                    <c1:C1ComboBoxItem Value="AS" Text="Switch In" />
                    <c1:C1ComboBoxItem Value="RS" Text="Switch Out" />
                    <c1:C1ComboBoxItem Value="RX" Text="Tax" />
                    <c1:C1ComboBoxItem Value="AR" Text="Tax Rebate" />
                    <c1:C1ComboBoxItem Value="AT" Text="Transfer In" />
                    <c1:C1ComboBoxItem Value="RT" Text="Transfer Out" />
                    <c1:C1ComboBoxItem Value="RW" Text="Withdrawal" />
                    <c1:C1ComboBoxItem Value="RJ" Text="Adjustment Up" />
                    <c1:C1ComboBoxItem Value="AJ" Text="Adjustment Down" />
                    <c1:C1ComboBoxItem Value="AP" Text="Application" />
                    <c1:C1ComboBoxItem Value="AM" Text="Bonus Issue" />
                    <c1:C1ComboBoxItem Value="AN" Text="Purchase" />
                    <c1:C1ComboBoxItem Value="RU" Text="Commutation" />
                    <c1:C1ComboBoxItem Value="AC" Text="Contribution" />
                    <c1:C1ComboBoxItem Value="RL" Text="Contribution Tax" />
                    <c1:C1ComboBoxItem Value="AD" Text="Deposit" />
                    <c1:C1ComboBoxItem Value="EX" Text="Expense" />
                    <c1:C1ComboBoxItem Value="IN" Text="Opening (Initial) Balance" />
                    <c1:C1ComboBoxItem Value="LM" Text="Instalment" />
                    <c1:C1ComboBoxItem Value="RH" Text="Insurance Premium" />
                    <c1:C1ComboBoxItem Value="AL" Text="Lodgement" />
                    <c1:C1ComboBoxItem Value="RK" Text="Lump Sum Tax" />
                    <c1:C1ComboBoxItem Value="MA" Text="Manager Fee" />
                    <c1:C1ComboBoxItem Value="RY" Text="PAYG" />
                    <c1:C1ComboBoxItem Value="RP" Text="Pension Payment" />
                    <c1:C1ComboBoxItem Value="RI" Text="Pension Payment – Income Stream" />
                    <c1:C1ComboBoxItem Value="PR" Text="Purchase Reinvestment" />
                    <c1:C1ComboBoxItem Value="RR" Text="Redemption" />
                    <c1:C1ComboBoxItem Value="VB" Text="Reversal - Buy / Purchase / Deposit" />
                </Items>
            </c1:C1ComboBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            Total Shares:
        </td>
        <td>
            <c1:C1InputNumeric Width="410px" ID="TotalShares" DecimalPlaces="4" runat="server"
                ShowSpinner="true" Value="50">
            </c1:C1InputNumeric>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            Unit Price:
        </td>
        <td>
            <c1:C1InputCurrency DecimalPlaces="4" Width="410px" ID="UnitPrice" runat="server"
                ShowSpinner="true" Value="50">
            </c1:C1InputCurrency>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            Total Amount:
        </td>
        <td>
            <c1:C1InputCurrency Width="410px" DisableUserInput="true" ID="TotalAmount" ShowSpinner="false"
                runat="server" Value="50">
            </c1:C1InputCurrency>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            Transaction Date:
        </td>
        <td>
            <c1:C1InputDate runat="server" ID="transactionDate" Width="410px" Height="10px" DateFormat="dd/MM/yyyy">
            </c1:C1InputDate>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
        </td>
        <td align="right">
            <asp:Button ID="btnUpdate" CommandName="Update" runat="server" Text="Update" OnClick="UpdateTransaction" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_OnClick" />
            <asp:Label ID="lblmisCid" runat="server" Visible="false" />
            <asp:HiddenField ID="IsUpdate" runat="server" />
            <asp:HiddenField ID="hfClientID" runat="server" />
        </td>
        <td>
        </td>
    </tr>
</table>
