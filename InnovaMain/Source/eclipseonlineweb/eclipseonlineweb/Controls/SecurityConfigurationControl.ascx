﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecurityConfigurationControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.SecurityConfigurationControl" %>
<fieldset>
    <table>
        <tr>
            <td colspan="3">
                <span class="riLabelBold">Included Users</span>
            </td>
            <td colspan="2">
                <span class="riLabelBold">Available Users</span>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:radcombobox id="cmbIncluded" runat="server" width="450px"  IsEditable="True"  AllowCustomText= "True"  EnableTextSelection ="True" CheckBoxes="true"   >
                </telerik:radcombobox>
            </td>
            <td>
                <telerik:radbutton id="btnRemove" runat="server" text="Remove >>" onclick="btnRemove_OnClick">
                </telerik:radbutton>
                <telerik:radbutton id="Radbutton2" runat="server" text="Remove All >>" onclick="btnRemoveAll_OnClick">
                </telerik:radbutton>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <telerik:radcombobox id="cmbAvailable" runat="server" width="450px" IsEditable="True" AllowCustomText= "True"  EnableTextSelection ="True" CheckBoxes="true"  >

                </telerik:radcombobox>
            </td>
            <td>
                <telerik:radbutton id="btnAdd" runat="server" text="<< Add" onclick="btnAdd_OnClick">
                </telerik:radbutton>
                <telerik:radbutton id="Radbutton1" runat="server" text="<< Add All" onclick="btnAddAll_OnClick">
                </telerik:radbutton>
            </td>
        </tr>
    </table>

   
</fieldset>
