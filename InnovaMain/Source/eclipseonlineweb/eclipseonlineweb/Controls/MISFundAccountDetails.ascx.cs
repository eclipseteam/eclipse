﻿using System;
using System.Text;
using C1.Web.Wijmo.Controls.C1ComboBox;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Commands;
using Telerik.Web.UI;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb.ClientViews.WebControls
{
    public partial class MISFundAccountDetails : System.Web.UI.UserControl
    {

        public bool  ShowReinvestRow
        {
            get { return this.reInvetRow.Visible; }
            set { this.reInvetRow.Visible = value; }
        }

        public Action<Guid, Guid, Guid,ReinvestmentOption?> Saved
        {
            get;
            set;
        }
        public Action Canceled { get; set; }
        public Action ValidationFailed { get; set; }
        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                LoadControls();
            }
        }

        private void LoadControls()
        {
            //bind combos here
            var org = this.Broker.GetWellKnownBMC(WellKnownCM.Organization);

            MISBasicDS MIS = new MISBasicDS();
            MIS.Command = (int)WebCommands.GetOrganizationUnitsByType;
            MIS.Unit = new Oritax.TaxSimp.Data.OrganizationUnit
            {
                CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                Type = ((int)OrganizationType.ManagedInvestmentSchemesAccount).ToString()
            };
            org.GetData(MIS);
            cmbMISAcc.Items.Clear();
            cmbMISAcc.DataSource = MIS.MISBasicTable;
            cmbMISAcc.DataTextField = MIS.MISBasicTable.NAME;
            cmbMISAcc.DataValueField = MIS.MISBasicTable.CID;
            cmbMISAcc.DataBind();

            if (cmbMISAcc.Items.Count > 0)
            {

                cmbMISAcc.SelectedIndex = 0;
                FillFundCombo(new Guid(cmbMISAcc.SelectedValue.ToString()));

            }
            BindReinvestmentOptions();
            Broker.ReleaseBrokerManagedComponent(org);

        }

        private void BindReinvestmentOptions()
        {
            var reinvetmentOptions = WebUtilities.Utilities.GetEnumList<ReinvestmentOption>();
            cmbReinvetmentOption.DataSource = reinvetmentOptions;
            cmbReinvetmentOption.DataTextField = "Key";
            cmbReinvetmentOption.DataValueField = "Value";
            cmbReinvetmentOption.DataBind();
        }

        public void ClearEntity()
        {
            lblMessages.Text = "";
            cmbMISAcc.Text = "";
            cmbFundCode.Text = "";
            if (cmbMISAcc.Items.Count > 0)
            {
                cmbMISAcc.SelectedIndex = 0;
                FillFundCombo(Guid.Parse(cmbMISAcc.SelectedValue));
                if (cmbFundCode.Items.Count > 0)
                    cmbFundCode.SelectedIndex = 0;
            }

            if (cmbReinvetmentOption.Items.Count > 0)
                cmbReinvetmentOption.SelectedIndex = 0;
        }

        public void SetEntity(Guid MISCid, Guid FundID,ReinvestmentOption? reinvestOption=null, bool IsAccountProcess = false)
        {
            LoadControls();
            ClearEntity();


            if (MISCid != Guid.Empty && FundID != Guid.Empty)//Edit Clicked
            {
                cmbMISAcc.SelectedValue = MISCid.ToString();
                FillFundCombo(Guid.Parse(cmbMISAcc.SelectedValue));
                cmbFundCode.SelectedValue = FundID.ToString();
                cmbMISAcc.Enabled = false;
                cmbFundCode.Enabled = false;
               
               // btnUpdate.Enabled = IsAccountProcess;
                if(reinvestOption.HasValue)
                cmbReinvetmentOption.SelectedValue = reinvestOption.ToString();
            }
            else//add Clicked 
            {
                cmbMISAcc.Enabled = true;
                cmbFundCode.Enabled = true;
                btnUpdate.Enabled = true;

            }


        }

        private MISBasicDS GetMIsAccount(Guid MISCid)
        {

            MISBasicDS misBasicDs = new MISBasicDS { CommandType = DatasetCommandTypes.Get, Command = (int)WebCommands.GetOrganizationUnitDetails };
            misBasicDs.Unit = new OrganizationUnit { Cid = MISCid, CurrentUser = (Page as UMABasePage).GetCurrentUser(), Type = ((int)OrganizationType.ManagedInvestmentSchemesAccount).ToString() };
            if (Broker != null)
            {
                IBrokerManagedComponent org = Broker.GetWellKnownBMC(WellKnownCM.Organization);
                org.GetData(misBasicDs);
                Broker.ReleaseBrokerManagedComponent(org);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }


            return misBasicDs;

        }

        private void FillFundCombo(Guid MISCid)
        {
            ManagedInvestmentAccountDS ds = GetFundDataSet(MISCid);
            FundAccountTable funds = ds.FundAccountsTable;
            cmbFundCode.Items.Clear();
            cmbFundCode.DataSource = funds;
            cmbFundCode.DataTextField = funds.FUNDCODE;
            cmbFundCode.DataValueField = funds.FUNDID;
            cmbFundCode.DataBind();
        }

        private ManagedInvestmentAccountDS GetFundDataSet(Guid MISCid)
        {
            ManagedInvestmentAccountDS misDS = new ManagedInvestmentAccountDS();
            misDS.FundID = Guid.Empty;
            if (Broker != null)
            {

                IBrokerManagedComponent clientData = this.Broker.GetBMCInstance(MISCid);

                misDS.CommandType = DatasetCommandTypes.Get;
                clientData.GetData(misDS);
                Broker.ReleaseBrokerManagedComponent(clientData);


            }
            else
            {
                throw new Exception("Broker Not Found");
            }



            return misDS;

        }



        public bool Validate()
        {
            bool result = true;

            lblMessages.Text = "";
            StringBuilder Messages = new StringBuilder();

            if (string.IsNullOrEmpty(cmbMISAcc.SelectedValue))
            {
                result = false;
                AddMessageIn(result, "Please Select MIS Account*", Messages);
            }
            if (string.IsNullOrEmpty(cmbFundCode.SelectedValue))
            {
                result = false;
                AddMessageIn(result, "Please Select Fund Code*", Messages);
            }
            lblMessages.Text = Messages.ToString();
            return result;
        }

        private void AddMessageIn(bool result, string message, StringBuilder Messages)
        {
            if (!result)
            {
                Messages.Append(message + "<br/>");
            }
        }

        protected void btnUpdate_Onclick(object sender, EventArgs e)
        {
            if (Validate() == false)
            {
                if (ValidationFailed != null)
                    ValidationFailed();
                return;
            }

            if (Saved != null)
            {
                MISBasicDS mis = GetMIsAccount(new Guid(cmbMISAcc.SelectedValue));
                Guid Clid = Guid.Parse(mis.MISBasicTable.Rows[0][mis.MISBasicTable.CLID].ToString());
                Guid csid = Guid.Parse(mis.MISBasicTable.Rows[0][mis.MISBasicTable.CSID].ToString());

                var rein=(ShowReinvestRow?(ReinvestmentOption?)Enum.Parse(typeof(ReinvestmentOption),cmbReinvetmentOption.SelectedValue):(ReinvestmentOption?)null);
                Saved(Clid, csid, new Guid(cmbFundCode.SelectedValue), rein);
            }
        }

        protected void btnCancel_Onclick(object sender, EventArgs e)
        {
            if (Canceled != null)
            {
                Canceled();
            }
        }

        protected void Mis_Changed(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            FillFundCombo(Guid.Parse(cmbMISAcc.SelectedValue));
            if (ValidationFailed != null)
                ValidationFailed();
        }
    }
}