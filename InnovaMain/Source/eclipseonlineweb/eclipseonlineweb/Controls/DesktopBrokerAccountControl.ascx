﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DesktopBrokerAccountControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.DesktopBrokerAccountControl" %>
<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" Assembly="C1.Web.Wijmo.Controls.4, Version=4.0.20121.56, Culture=neutral, PublicKeyToken=9b75583953471eea" %>
<%@ Register Src="HINControl.ascx" TagName="HINControl" TagPrefix="uc1" %>
<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1GridView" Assembly="C1.Web.Wijmo.Controls.4, Version=4.0.20121.56, Culture=neutral, PublicKeyToken=9b75583953471eea" %>
<div class="popup_Container">
    <div class="popup_Titlebar" id="PopupHeader">
        <div class="TitlebarLeft">
            <asp:Label ID="Title" runat="server" Text="ASX Broker Account Details"></asp:Label>
            <asp:HiddenField ID="hfCID" runat="server" />
            <asp:HiddenField ID="hfCLID" runat="server" />
            <asp:HiddenField ID="hfCSID" runat="server" />
        </div>
    </div> 
    <div class="popup_Body">               
        <table runat="server" id="tblAsx">
            <colgroup>
            </colgroup>
            <tr>
                <td>
                    Status
                </td>
                <td>
                    <c1:C1ComboBox ID="cmbStatus" runat="server" Width="150" ViewStateMode="Enabled">
                    </c1:C1ComboBox>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Account Name *
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtAccountName" Width="400"></asp:TextBox>
                </td>
                <td>
                    Account Number *
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtAccountNumber" Width="400"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Account Designation
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtAccountDesignation" Width="400"></asp:TextBox>
                </td>
                <td>
                    HIN #
                </td>
                <td>
                    <uc1:HINControl ID="txtHINNumber" runat="server" />
                </td>
            </tr>
              <tr>
                <td>
                    Other ID (e.g. e-Clipse Super Full ID)
                </td>
                <td>
                    <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtOtherID" Width="400"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="3">
                    <asp:Label ID="lblMessages" runat="server" Text="" ForeColor="#990000" Font-Bold="True"></asp:Label>
                </td>
            </tr>
        </table>
        <div align="right">
            <asp:Button ID="btnUpdate" runat="server" Text="Save" OnClick="btnUpdate_OnClick" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_OnClick" />
        </div>
    </div>
</div>
