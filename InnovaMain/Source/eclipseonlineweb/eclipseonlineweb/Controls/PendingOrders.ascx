﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PendingOrders.ascx.cs"
    Inherits="eclipseonlineweb.Controls.PendingOrders" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Import Namespace="Oritax.TaxSimp.Common" %>
<style type="text/css">
    .RadListBox .rlbSelected
    {
        background-color: transparent !important;
    }
    .RadListBox span.rlbText
    {
        color: black !important;
    }
    .rtWrapperContent
    {
        background-color: lightyellow !important;
        color: black !important;
    }
    .RadGrid_Metro .rgHeader, .RadGrid_Metro .rgHeader a, .RadGrid_Metro .rgRow a, .RadGrid_Metro .rgAltRow a, .RadGrid_Metro tr.rgEditRow a, .RadGrid_Metro .rgFooter a, .RadGrid_Metro .rgEditForm a, .RadGrid_Metro .rgFilterBox
    {
        font-size: 8pt !important;
    }
    .rbPrimaryIcon
    {
        left: 4px !important;
        top: 0 !important;
    }
</style>
<script src="../Scripts/GridScripts.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
    function ShowModalPopup() {
        window.$find("ModalBehaviour").show();
    }

    function HideModalPopup() {
        window.$find("ModalBehaviour").hide();
        window.$find("ModalBehaviourUpdateStatus").hide();
        window.$find("ModalBehaviourTradeDate").hide();

        if (typeof window.Page_Validators != 'undefined') {
            for (var i = 0; i <= window.Page_Validators.length; i++) {
                if (window.Page_Validators[i] != null) {
                    window.ValidatorEnable(window.Page_Validators[i], false);
                }
            }
        };
    }

    function getvalue() {
        var combo = window.$find('<%:cmbStatus.ClientID %>');
        var approveStatus = '<%:OrderStatus.Approved.ToString()%>';
        var item = combo.findItemByText(approveStatus);
        var msg = "The order(s) may have instructions issued. Changing its status manually will remove all instructions. Are you sure?";
        if (item) {
            var isConfirmed = confirm(msg);
            if (!isConfirmed) {
                HideModalPopup();
                return false;
            }
            else {
                return true;
            }
        }
        return true;
    }

    function onItemChecked(sender, e) {
        var item = e.get_item();
        var checked = item.get_checked();
        if (checked) {
            item.get_element().attributes["style"].value = "font-weight:Bold";
        }
        else {
            item.get_element().attributes["style"].value = "font-weight:Normal";
        }
    }

    function CheckAll(checkBox) {
        CheckAllCheckBoxes(checkBox, "<%:PresentationGrid.ClientID %>", "chkCancel");
    }

    function CheckSingle(colId) {
        CheckSingleCheckBox(colId, "<%:PresentationGrid.ClientID %>", "chkCancel");
    }

    function CheckSelected() {
        return CheckSelectedCheckBoxes("<%:PresentationGrid.ClientID %>", "chkCancel");
    }

    function CheckSeletedApprove(sender, args) {
        var isSelected = CheckSelected();
        if (!isSelected) {
            alert("Please select some orders first.");
        }
        else {
            alert("Orders in Validated state will be approved only.");
        }
        sender.set_autoPostBack(isSelected);
    }

    function CheckForApprovedOrders(sender, args) {
        var isSelected = CheckSelected();
        if (!isSelected) {
            alert("Please select some orders first.");
        }
        else {
            var hfIsExportAll = document.getElementById('<%:hfIsExportAll.ClientID %>');
            hfIsExportAll.value = "false";
            $find('ModalBehaviourTradeDate').show();
        }
        sender.set_autoPostBack(false);
    }

    function CheckForAllApprovedOrders(sender, args) {
        var hfIsExportAll1 = document.getElementById('<%:hfIsExportAll.ClientID %>');
        hfIsExportAll1.value = "true";
        $find('ModalBehaviourTradeDate').show();
        sender.set_autoPostBack(false);
    }

    function CheckSelectedCancel(sender, args) {
        var isSelected = CheckSelected();
        var isConfirmed = false;
        if (!isSelected) {
            alert("Please select some orders first.");
        }
        else {
            isConfirmed = CancelConfirm();
        }
        sender.set_autoPostBack(isConfirmed);
    }

    function CancelConfirm() {
        var isConfirmed = confirm('Are you sure you want to cancel these orders?');
        return isConfirmed;
    }

    function CheckSeletedUpdate(sender, args) {
        var isSelected = CheckSelected();
        if (!isSelected) {
            alert("Please select some orders first.");
        }
        else {
            alert("Through manual status change, order status can be moved backward only.");
        }
        sender.set_autoPostBack(isSelected);
    }
</script>
<asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="75%">
                        <telerik:RadButton runat="server" ID="btnValidate" ToolTip="Validate All Orders"
                            OnClick="btnValidate_OnClick">
                            <ContentTemplate>
                                <img src="../images/ValidateIcon.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Validate</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ID="btnBulkUpdate" ToolTip="Update Selected Orders Status Manually"
                            OnClick="btnBulkUpdate_OnClick" OnClientClicked="CheckSeletedUpdate">
                            <ContentTemplate>
                                <img src="../images/update.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Update Status</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ID="btnBulkCancel" ToolTip="Cancel Selected Orders"
                            OnClick="btnBulkCancel_OnClick" OnClientClicked="CheckSelectedCancel">
                            <ContentTemplate>
                                <img src="../images/CrossIcon.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Cancel</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ID="btnBulkApprove" ToolTip="Approve Selected Orders"
                            OnClick="btnApproved_OnClick" OnClientClicked="CheckSeletedApprove" Visible="false">
                            <ContentTemplate>
                                <img src="../images/ApproveIcon.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Approve</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ID="btnExportOrders" ToolTip="Export Approved Orders"
                            OnClientClicked="CheckForApprovedOrders" Visible="false">
                            <ContentTemplate>
                                <img src="../images/download.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Export Orders</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ID="btnExportAllOrders" ToolTip="Export All Approved Orders"
                            OnClientClicked="CheckForAllApprovedOrders" Visible="false">
                            <ContentTemplate>
                                <img src="../images/download.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Export All</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ID="btnShowExportedOrders" Text="Show Exported Orders Files"
                            Height="22px" OnClick="btnExportedOrdersFiles_OnClick" Visible="false">
                        </telerik:RadButton>
                        <telerik:RadButton runat="server" ID="btnExcel" ToolTip="Download as Excel" OnClick="btnExcel_OnClick">
                            <ContentTemplate>
                                <img src="../images/export_excel.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Download</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                        <telerik:RadButton ID="btnUMA" runat="server" ToggleType="Radio" ButtonType="ToggleButton"
                            Text="UMA" Value="UMA" GroupName="ClientManagementType" Checked="true" OnClick="btnUMA_OnClick"
                            Visible="False">
                        </telerik:RadButton>
                        <telerik:RadButton ID="btnSMA" runat="server" ToggleType="Radio" Text="Super" Value="SMA"
                            GroupName="ClientManagementType" ButtonType="ToggleButton" OnClick="btnSMA_OnClick"
                            Visible="False">
                        </telerik:RadButton>
                    </td>
                    <td style="width: 25%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <br />
        <div id="MainView" style="background-color: white; display: none">
            <table style="width: 100%; height: 50px">
                <tr>
                    <td style="width: 100px">
                        Order Status:
                    </td>
                    <td style="width: 500px">
                        <telerik:RadListBox ID="lstStatus" runat="server" CheckBoxes="true" SelectionMode="Multiple"
                            OnClientItemChecked="onItemChecked">
                        </telerik:RadListBox>
                    </td>
                    <td align="left">
                        <asp:Button ID="btnApplyFilter" runat="server" Text="Apply Filter" OnClick="btnApplyFilter_OnClick" />
                    </td>
                </tr>
            </table>
        </div>
        <telerik:RadGrid OnNeedDataSource="PresentationGridNeedDataSource" ID="PresentationGrid"
            runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
            AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true" OnDetailTableDataBind="PresentationGridDetailTableDataBind"
            OnItemCommand="PresentationGridItemCommand" GridLines="None" AllowAutomaticDeletes="True"
            AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
            EnableViewState="true" ShowFooter="false" OnItemDataBound="PresentationGridItemDataBound"
            EnableLinqExpressions="false">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                Name="ActiveOrders" TableLayout="Fixed" Font-Size="8">
                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                <PagerStyle Mode="NextPrevNumericAndAdvanced" AlwaysVisible="True"></PagerStyle>
                <DetailTables>
                    <telerik:GridTableView Name="SSOrderDetails" Width="100%" Font-Size="8" AllowFilteringByColumn="False">
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="false"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ID" UniqueName="ID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="OrderID" ReadOnly="true" HeaderText="OrderID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="OrderID" UniqueName="OrderID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ProductID" ReadOnly="true" HeaderText="ProductID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ProductID" UniqueName="ProductID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="80px" SortExpression="FundCode"
                                ReadOnly="true" HeaderText="Fund Code" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="FundCode" UniqueName="FundCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="220px" HeaderStyle-Width="260px" SortExpression="FundName"
                                ReadOnly="true" HeaderText="Fund Name" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="FundName" UniqueName="FundName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" SortExpression="Units"
                                ReadOnly="true" HeaderText="Units" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Units" UniqueName="Units"
                                DataFormatString="{0:N4}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Amount"
                                ReadOnly="true" HeaderText="Amount" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Amount" UniqueName="Amount"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="CreatedDate"
                                ReadOnly="true" HeaderText="CreatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedDate" UniqueName="CreatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="UpdatedDate"
                                ReadOnly="true" HeaderText="UpdatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UpdatedDate" UniqueName="UpdatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ValidationMsg" ReadOnly="true" HeaderText="Validation Msg"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ValidationMsg" UniqueName="ValidationMsg">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </telerik:GridTableView>
                    <telerik:GridTableView Name="ASXOrderDetails" Width="100%" Font-Size="8" AllowFilteringByColumn="False">
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="false"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ID" UniqueName="ID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="OrderID" ReadOnly="true" HeaderText="OrderID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="OrderID" UniqueName="OrderID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ProductID" ReadOnly="true" HeaderText="ProductID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ProductID" UniqueName="ProductID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" SortExpression="InvestmentCode"
                                ReadOnly="true" HeaderText="Investment Code" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InvestmentCode"
                                UniqueName="InvestmentCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="220px" HeaderStyle-Width="260px" SortExpression="InvestmentName"
                                ReadOnly="true" HeaderText="Investment Name" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InvestmentName"
                                UniqueName="InvestmentName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" SortExpression="Units"
                                ReadOnly="true" HeaderText="Units" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Units" UniqueName="Units"
                                DataFormatString="{0:N4}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="ASXUnitPrice"
                                ReadOnly="true" HeaderText="Unit Price" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ASXUnitPrice"
                                UniqueName="ASXUnitPrice" DataFormatString="{0:N6}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Amount"
                                ReadOnly="true" HeaderText="Amount" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Amount" UniqueName="Amount"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="CreatedDate"
                                ReadOnly="true" HeaderText="CreatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedDate" UniqueName="CreatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="UpdatedDate"
                                ReadOnly="true" HeaderText="UpdatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UpdatedDate" UniqueName="UpdatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ValidationMsg" ReadOnly="true" HeaderText="Validation Msg"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ValidationMsg" UniqueName="ValidationMsg">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </telerik:GridTableView>
                    <telerik:GridTableView Name="TDOrderDetails" Width="100%" Font-Size="8" BackColor="White"
                        AllowFilteringByColumn="False">
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="false"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ID" UniqueName="ID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="OrderID" ReadOnly="true" HeaderText="OrderID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="OrderID" UniqueName="OrderID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="BrokerID" ReadOnly="true" HeaderText="BrokerID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="BrokerID" UniqueName="BrokerID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="BrokerName" ReadOnly="true" HeaderText="Broker Name"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="BrokerName" UniqueName="BrokerName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="BankID" ReadOnly="true" HeaderText="BankID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="BankID" UniqueName="BankID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="BankName" ReadOnly="true" HeaderText="Bank Name"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="BankName" UniqueName="BankName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Min"
                                ReadOnly="true" HeaderText="Min" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Min" UniqueName="Min"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Max"
                                ReadOnly="true" HeaderText="Max" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Max" UniqueName="Max"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="80px" SortExpression="Rating"
                                ReadOnly="true" HeaderText="Rating" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Rating" UniqueName="Rating">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="35px" HeaderStyle-Width="75px" SortExpression="Duration"
                                ReadOnly="true" HeaderText="Duration" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Duration" UniqueName="Duration">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="80px" SortExpression="Percentage"
                                ReadOnly="true" HeaderText="Percentage" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Percentage" UniqueName="Percentage"
                                DataFormatString="{0:P}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="Units" ReadOnly="true" HeaderText="Units"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="Units" UniqueName="Units" Visible="false"
                                DataFormatString="{0:N4}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Amount"
                                ReadOnly="true" HeaderText="Amount" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Amount" UniqueName="Amount"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="CreatedDate"
                                ReadOnly="true" HeaderText="CreatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedDate" UniqueName="CreatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="UpdatedDate"
                                ReadOnly="true" HeaderText="UpdatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UpdatedDate" UniqueName="UpdatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ValidationMsg" ReadOnly="true" HeaderText="Validation Msg"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ValidationMsg" UniqueName="ValidationMsg">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </telerik:GridTableView>
                    <telerik:GridTableView Name="AtCallOrderDetails" Width="100%" Font-Size="8" AllowFilteringByColumn="False">
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="false"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ID" UniqueName="ID" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="OrderID" ReadOnly="true" HeaderText="OrderID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="OrderID" UniqueName="OrderID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="80px" SortExpression="TransferTo"
                                ReadOnly="true" HeaderText="Transfer To" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransferTo" UniqueName="TransferTo">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="220px" HeaderStyle-Width="100px" SortExpression="TransferAccBSB"
                                ReadOnly="true" HeaderText="BSB" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransferAccBSB"
                                UniqueName="TransferAccBSB">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="220px" HeaderStyle-Width="120px" SortExpression="TransferAccNumber"
                                ReadOnly="true" HeaderText="Account No" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransferAccNumber"
                                UniqueName="TransferAccNumber">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="AtCallType" ReadOnly="true" HeaderText="AtCallType"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="AtCallType" UniqueName="AtCallType"
                                Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="BrokerID" ReadOnly="true" HeaderText="BrokerID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="BrokerID" UniqueName="BrokerID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="BrokerName" ReadOnly="true" HeaderText="Broker Name"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="BrokerName" UniqueName="BrokerName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="BankID" ReadOnly="true" HeaderText="BankID"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="BankID" UniqueName="BankID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="BankName" ReadOnly="true" HeaderText="Bank Name"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="BankName" UniqueName="BankName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="AccountTier" HeaderText="Description" AutoPostBackOnFilter="false"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                ReadOnly="true" DataField="AccountTier" UniqueName="AccountTier">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Min"
                                ReadOnly="true" HeaderText="Min" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Min" UniqueName="Min"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Max"
                                ReadOnly="true" HeaderText="Max" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Max" UniqueName="Max"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="180px" SortExpression="Rate"
                                ReadOnly="true" HeaderText="Rate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Rate" UniqueName="Rate"
                                DataFormatString="{0:P}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Amount"
                                ReadOnly="true" HeaderText="Amount" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Amount" UniqueName="Amount"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="CreatedDate"
                                ReadOnly="true" HeaderText="CreatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedDate" UniqueName="CreatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="UpdatedDate"
                                ReadOnly="true" HeaderText="UpdatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UpdatedDate" UniqueName="UpdatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ValidationMsg" ReadOnly="true" HeaderText="Validation Msg"
                                AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ValidationMsg" UniqueName="ValidationMsg">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </telerik:GridTableView>
                </DetailTables>
                <Columns>
                    <%-- <telerik:GridTemplateColumn UniqueName="OrderID" HeaderText="Order ID" ShowFilterIcon="true"
                        FilterControlWidth="40px" HeaderStyle-Width="75px" ItemStyle-Width="75px" SortExpression="OrderID"
                        AllowFiltering="true" DataField="OrderID">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnOrderID" runat="server" Text='<%# Eval("OrderID") %>' OnClick="ShowHideDetailClick" 
                                Font-Size="8"></asp:LinkButton>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>--%>
                    <%-- because we are using client side and Server side Techniques so we need to remove link (removing after discussion with Farooqe).Samee--%>
                    <telerik:GridBoundColumn FilterControlWidth="45px" HeaderStyle-Width="75px" ItemStyle-Width="75px"
                        ReadOnly="true" SortExpression="OrderID" ShowFilterIcon="true" HeaderText="Order ID"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                        DataField="OrderID" UniqueName="OrderID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="ID" UniqueName="ID" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="ClientCID"
                        SortExpression="ClientID" UniqueName="ClientID" DataNavigateUrlFormatString="../ClientViews/ClientMainView.aspx?ins={0}"
                        HeaderText="Client ID" DataTextField="ClientID" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" FilterControlWidth="50px" HeaderStyle-Width="85px"
                        HeaderButtonType="TextButton" ShowFilterIcon="true" Visible="false">
                    </telerik:GridHyperLinkColumn>
                    <telerik:GridBoundColumn SortExpression="AccountCID" ReadOnly="true" HeaderText="AccountCID"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="AccountCID" UniqueName="AccountCID"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="200px" SortExpression="AccountNumber"
                        ReadOnly="true" HeaderText="AccountNumber" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountNumber"
                        UniqueName="AccountNumber" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="85px" SortExpression="OrderAccountType"
                        HeaderText="Account Type" ReadOnly="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="OrderAccountType"
                        UniqueName="OrderAccountType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="45px" HeaderStyle-Width="85px" ItemStyle-Width="85px"
                        ReadOnly="true" SortExpression="OrderType" ShowFilterIcon="true" HeaderText="Order Type"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                        DataField="OrderType" UniqueName="OrderType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="35px" HeaderStyle-Width="70px" ReadOnly="true"
                        SortExpression="OrderItemType" HeaderText="Item Type" AutoPostBackOnFilter="false"
                        ShowFilterIcon="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                        DataField="OrderItemType" UniqueName="OrderItemType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="45px" HeaderStyle-Width="85px" ReadOnly="true"
                        SortExpression="OrderPreferedBy" HeaderText="Requested By" AutoPostBackOnFilter="false"
                        ShowFilterIcon="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                        DataField="OrderPreferedBy" UniqueName="OrderPreferedBy">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn FilterControlWidth="60px" HeaderStyle-Width="95px" ReadOnly="true"
                        SortExpression="SMARequestID" HeaderText="Super Request ID" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="SMARequestID" UniqueName="SMARequestID" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblRequestID" runat="server" Text='<%# Bind("SMARequestID") %>'></asp:Label>
                            <asp:LinkButton runat="server" ID="hlnkSMARequestIDRefresh" Text="Send Order to Super"
                                ForeColor="Red" Visible="false" OnClick="hlnkSMARequestIDRefresh_OnClick"></asp:LinkButton>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn FilterControlWidth="35px" HeaderStyle-Width="70px" ReadOnly="true"
                        SortExpression="SMACallStatus" HeaderText="Super Call Status" AutoPostBackOnFilter="false"
                        ShowFilterIcon="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                        DataField="SMACallStatus" UniqueName="SMACallStatus">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn FilterControlWidth="60px" HeaderStyle-Width="95px" ReadOnly="true"
                        SortExpression="Status" HeaderText="Status" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Status" UniqueName="Status">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="hypLinkStatus" Text='<%# Bind("Status") %>'></asp:HyperLink>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="75px" SortExpression="OrderBulkStatus"
                        ReadOnly="true" HeaderText="Bulk Status" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="OrderBulkStatus"
                        UniqueName="OrderBulkStatus">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" ReadOnly="true"
                        SortExpression="Value" HeaderText="Value" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Value" UniqueName="Value"
                        DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="85px" ReadOnly="true"
                        SortExpression="TradeDate" HeaderText="Trade Date" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="TradeDate" UniqueName="TradeDate" DataFormatString="{0:dd/MM/yyyy}"
                        DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="90px" SortExpression="CreatedBy"
                        ReadOnly="true" HeaderText="Created By" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedBy" UniqueName="CreatedBy">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="65px" HeaderStyle-Width="100px" ReadOnly="true"
                        SortExpression="CreatedDate" HeaderText="Create Date " AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="CreatedDate" UniqueName="CreatedDate" DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}"
                        DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="65px" HeaderStyle-Width="100px" ReadOnly="true"
                        SortExpression="UpdatedDate" HeaderText="Update Date" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="UpdatedDate" UniqueName="UpdatedDate" DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}"
                        DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="120px" SortExpression="ValidationMsg"
                        ReadOnly="true" HeaderText="Validation Msg" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ValidationMsg"
                        UniqueName="ValidationMsg">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="UpdatedBy" ReadOnly="true" HeaderText="UpdatedBy"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="UpdatedBy" UniqueName="UpdatedBy" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="UpdatedByCID" ReadOnly="true" HeaderText="UpdatedByCID"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="UpdatedByCID" UniqueName="UpdatedByCID"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderStyle-Width="82px" UniqueName="Actions" HeaderText="Actions"
                        AllowFiltering="false">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgApprove" runat="server" ImageUrl="../images/ApproveIcon.png"
                                Visible="false" AlternateText="Approve" ToolTip="Approve Order" CommandName="approve"
                                CommandArgument='<%# Eval("ID") %>' OnClientClick="javascript:return confirm('Are you sure you want to approve this order?');" />
                            <asp:ImageButton ID="imgUpdate" runat="server" ImageUrl="../images/update.png" Visible="false"
                                Width="19px" Height="19px" AlternateText="Update Status" ToolTip="Update Status Manually"
                                CommandName="statusupdate" CommandArgument='<%# Eval("ID") %>' />
                            <asp:ImageButton ID="imgCancel" runat="server" ImageUrl="../images/CrossIcon.png"
                                AlternateText="Cancel" Width="19px" Height="19px" ToolTip="Cancel Order" OnClientClick="javascript:return confirm('Are you sure you want to cancel this order?');"
                                CommandName="Cancel" CommandArgument='<%# Eval("ID") %>' />
                            <asp:ImageButton ID="imgComplete" runat="server" ImageUrl="../images/complete.png"
                                Visible="false" AlternateText="Complete" ToolTip="Complete Order" CommandName="complete"
                                CommandArgument='<%# Eval("ID") %>' OnClientClick="javascript:return confirm('Are you sure you want to complete this order?');" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="BulkCancel" AllowFiltering="false" HeaderStyle-Width="35px">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkCancel" runat="server" onclick="CheckSingle('BulkCancel');"
                                ToolTip="Select Orders" />
                        </ItemTemplate>
                        <HeaderTemplate>
                            <asp:CheckBox ID="headerChkbox" runat="server" onclick="CheckAll(this);" ToolTip="Select All Orders" />
                        </HeaderTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <telerik:RadToolTipManager runat="server" AutoTooltipify="true" Width="275px" Skin="Default"
            Animation="Fade">
        </telerik:RadToolTipManager>
        <asp:HiddenField ID="hfOrderID" runat="server" />
        <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
        <asp:ModalPopupExtender ID="popupComments" runat="server" TargetControlID="btnShowPopup"
            PopupControlID="pnlpopup" PopupDragHandleControlID="PopupHeader" Drag="true"
            BackgroundCssClass="ModalPopupBG" BehaviorID="ModalBehaviour">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlpopup" runat="server" BackColor="White" Style="display: none">
            <div class="popup_Container">
                <div class="popup_Titlebar" id="PopupHeader">
                    <div class="TitlebarLeft">
                        To cancel this order, please type the reason it is being cancelled
                    </div>
                    <div class="TitlebarRight" onclick="HideModalPopup();">
                    </div>
                </div>
                <div class="PopupBody">
                    <table>
                        <tr>
                            <td colspan="2">
                                <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Rows="4" Columns="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RequiredFieldValidator ID="rfComments" runat="server" ErrorMessage="Enter Comments"
                                    ControlToValidate="txtComments" Enabled="false" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td align="right">
                                <asp:Button ID="btnSaveComments" runat="server" Text="Save" OnClick="btnSaveComments_OnClick" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
        <asp:Button ID="Button1" runat="server" Style="display: none" />
        <asp:ModalPopupExtender ID="popupStatus" runat="server" TargetControlID="Button1"
            PopupControlID="pnlStatus" PopupDragHandleControlID="PopupHeader" Drag="true"
            BackgroundCssClass="ModalPopupBG" BehaviorID="ModalBehaviourUpdateStatus">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlStatus" runat="server" BackColor="White" Style="display: none">
            <div class="popup_Container">
                <div class="popup_Titlebar" id="Div1">
                    <div class="TitlebarLeft">
                        Update Status
                    </div>
                    <div class="TitlebarRight" onclick="HideModalPopup();">
                    </div>
                </div>
                <div class="PopupBody">
                    <table>
                        <tr>
                            <td>
                                Order Status:
                            </td>
                            <td>
                                <telerik:RadComboBox ID="cmbStatus" runat="server" ZIndex="10000000">
                                </telerik:RadComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <asp:Button ID="btnUpdate" runat="server" Text="Save" OnClientClick="javascript:return getvalue();"
                                    OnClick="btnUpdate_OnClick" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
        <asp:Button ID="Button2" runat="server" Style="display: none" />
        <asp:ModalPopupExtender ID="popupTradeDate" runat="server" TargetControlID="Button2"
            PopupControlID="pnlTradeDate" PopupDragHandleControlID="PopupHeader1" Drag="true"
            BackgroundCssClass="ModalPopupBG" BehaviorID="ModalBehaviourTradeDate">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlTradeDate" runat="server" BackColor="White" Style="display: none">
            <div class="popup_Container">
                <div class="popup_Titlebar" id="Div2">
                    <div class="TitlebarLeft">
                        Export Orders
                    </div>
                    <div class="TitlebarRight" onclick="HideModalPopup();">
                    </div>
                </div>
                <div class="PopupBody">
                    <table>
                        <tr>
                            <td>
                                Trade Date:
                            </td>
                            <td>
                                <telerik:RadDatePicker ID="dtTradeDate" runat="server" Calendar-RangeMinDate="1/01/2000 12:00:00 AM"
                                    DateInput-MinDate="1/01/2000 12:00:00 AM" MinDate="1/01/1900 12:00:00 AM" ClientIDMode="AutoID"
                                    ZIndex="10000000">
                                    <DateInput DateFormat="dd/MM/yyyy" ReadOnly="true" DisplayDateFormat="dd/MM/yyyy">
                                    </DateInput>
                                </telerik:RadDatePicker>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <asp:Button ID="btnOK" runat="server" Text="Export" OnClick="btnOK_OnClick" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
        <asp:HiddenField ID="hfIsAdmin" runat="server" />
        <asp:HiddenField ID="hfIsAdminMenu" runat="server" />
        <asp:HiddenField ID="hfIsBulkCancel" runat="server" />
        <asp:HiddenField ID="hfIsFiltered" runat="server" />
        <asp:HiddenField ID="hfFilter" runat="server" />
        <asp:HiddenField ID="hfIsBulkUpdate" runat="server" />
        <asp:HiddenField ID="hfIsExportAll" runat="server" />
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        </telerik:RadWindowManager>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExcel" />
    </Triggers>
</asp:UpdatePanel>
