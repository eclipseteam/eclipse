﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.CalculationInterface;
using Telerik.Web.UI;

namespace eclipseonlineweb.Controls
{
    public partial class SearchNSelectClientAccount : UserControl
    {
        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        public Action<string, string, string> SelectData { get; set; }

        public Action<bool> ShowHideModal { get; set; }

        public bool HideModal
        {
            private get
            {
                if (string.IsNullOrEmpty(hfHideModal.Value))
                {
                    hfHideModal.Value = "false";
                }
                return Convert.ToBoolean(hfHideModal.Value);
            }
            set
            {
                hfHideModal.Value = value.ToString();
            }
        }
        public bool IsAdminMenu
        {
            private get
            {
                if (string.IsNullOrEmpty(hfIsAdminMenu.Value))
                {
                    hfIsAdminMenu.Value = "false";
                }
                return Convert.ToBoolean(hfIsAdminMenu.Value);
            }
            set
            {
                hfIsAdminMenu.Value = value.ToString();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (ShowHideModal != null && IsAdminMenu && !HideModal)
                {
                    ShowHideModal(true);
                }
            }
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            SearchClient();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "attach":
                    GridDataItem dataItem = e.Item.OwnerTableView.Items[e.Item.ItemIndex];
                    string clientCID = dataItem["ClientCID"].Text;
                    string clientId = dataItem["ClientID"].Text;
                    string clientName = dataItem["ClientName"].Text;
                    if (SelectData != null)
                    {
                        SelectData(clientCID, clientId, clientName);
                    }
                    if (ShowHideModal != null)
                    {
                        HideModal = true;
                        ShowHideModal(false);
                    }
                    break;
            }
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            PresentationGrid.Rebind();
            PresentationGrid.Visible = true;
        }

        private void SearchClient()
        {
            IBrokerManagedComponent iBMC = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            var ds = new OrganisationListingDS
                         {
                             OrganisationListingOperationType = OrganisationListingOperationType.ClientsSearch,
                             ClientNameSearchFilter = SearchBox.Text
                         };

            switch (rbbtnSearchSelection.SelectedItem.Value)
            {
                case "SearchByName":
                    ds.OrganisationFilterType = OrganisationFilterType.ClientByName;
                    break;
                case "SearchByID":
                    ds.OrganisationFilterType = OrganisationFilterType.ClientByID;
                    break;
            }
            iBMC.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(iBMC);

            var entityView = new DataView(ds.Tables["Entities_Table"]) { RowFilter = "IsClient='true'", Sort = "ENTITYNAME_FIELD ASC" };
            PresentationGrid.DataSource = entityView.ToTable();
        }
    }
}