﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdviserInformation.ascx.cs"
    Inherits="eclipseonlineweb.Controls.AdviserInformation" %>
<%@ Register TagPrefix="uc1" TagName="PhoneNumberControl" Src="~/Controls/PhoneNumberControl.ascx" %>
<%@ Register TagPrefix="uc2" TagName="TFN_InputControl" Src="~/Controls/TFN_InputControl.ascx" %>
<%@ Register TagPrefix="uc3" TagName="MobileNumberControl" Src="~/Controls/MobileNumberControl.ascx" %>
<fieldset>
    <asp:Panel runat="server" ID="pnlAdviserInformationDetail">
        <table width="100%">
            <tr>
                <td>
                    <span class="riLabel">Client ID :</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtClientID" runat="server" ReadOnly="true">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Other ID :</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtOtherID" runat="server" ReadOnly="false">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <span class="riLabel">Status Flag :</span>
                </td>
                <td>
                    <telerik:RadComboBox ID="cmbStatus" runat="server">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Title * :</span>
                </td>
                <td>
                    <telerik:RadComboBox ID="cmbTitle" runat="server">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="Mr" Value="Mr" Selected="true" />
                            <telerik:RadComboBoxItem runat="server" Text="Ms" Value="Ms" />
                            <telerik:RadComboBoxItem runat="server" Text="Mrs" Value="Mrs" />
                            <telerik:RadComboBoxItem runat="server" Text="Miss" Value="Miss" />
                            <telerik:RadComboBoxItem runat="server" Text="Dr" Value="Dr" />
                            <telerik:RadComboBoxItem runat="server" Text="Rev" Value="Rev" />
                            <telerik:RadComboBoxItem runat="server" Text="Other" Value="Other" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td>
                    <span class="riLabel">Preferred Name :</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtPreferredName" runat="server">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">First Name * :</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtFirstName" runat="server">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="reqFirstName" runat="server" ErrorMessage="Enter First Name"
                        ControlToValidate="txtFirstName" ForeColor="Red" Display="Dynamic" ValidationGroup="AdviserInfo"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <span class="riLabel">Surname * :</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtSurname" runat="server">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="reqSurname" runat="server" ErrorMessage="Enter Surname"
                        ControlToValidate="txtSurname" ForeColor="Red" Display="Dynamic" ValidationGroup="AdviserInfo"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Email Address * :</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtEmail" runat="server">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="reqEmail" runat="server" ErrorMessage="Enter Email Address"
                        ControlToValidate="txtEmail" ForeColor="Red" Display="Dynamic" ValidationGroup="AdviserInfo"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <span class="riLabel">Gender * :</span>
                </td>
                <td>
                    <telerik:RadComboBox ID="cmbGender" runat="server">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="Male" Value="Male" Selected="true" />
                            <telerik:RadComboBoxItem runat="server" Text="Female" Value="Female" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Home Phone Number :</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="HomePhoneNumber" runat="server" />
                </td>
                <td>
                    <span class="riLabel">TFN :</span>
                </td>
                <td>
                    <uc2:TFN_InputControl ID="TFNControl" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Mobile Phone Number :</span>
                </td>
                <td>
                    <uc3:MobileNumberControl ID="MobilePhoneNumber" runat="server" />
                </td>
                <td>
                    <span class="riLabel">Facsimile :</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="Facsimile" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Date Appointed :</span>
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtDateAppointed" runat="server">
                        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <span class="riLabel">Work Phone Number :</span>
                </td>
                <td>
                    <uc1:PhoneNumberControl ID="WorkPhoneNumber" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Authorised Rep No :</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtAuthorisedRepNo" runat="server">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <span class="riLabel">Reason Terminated :</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtReasonTerminated" runat="server">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Occupation :</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtOccupation" runat="server">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <span class="riLabel">Date Terminated :</span>
                </td>
                <td>
                    <telerik:RadDatePicker ID="dtDateTerminated" runat="server">
                        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">BT WRAP Adviser Code :</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtBTWRAPAdviserCode" runat="server">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <span class="riLabel">Desktop Broker Adviser Code :</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtDesktopBrokerAdviserCode" runat="server">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">AFS Licensee Name :</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtAFSLicenseeName" runat="server">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <span class="riLabel">AFS License Number :</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtAFSLicenseNumber" runat="server">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="riLabel">Bank West Adviser Code :</span>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtBankWestAdviserCode" runat="server">
                    </telerik:RadTextBox>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: right;">
                    <telerik:RadButton ID="btnSave" runat="server" Text="Save" OnClick="btnSave_OnClick"
                        ValidationGroup="AdviserInfo">
                    </telerik:RadButton>
                    <asp:HiddenField ID="hfCID" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</fieldset>
