﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Individual.ascx.cs"
    Inherits="eclipseonlineweb.Controls.Individual" %>
<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" Assembly="C1.Web.Wijmo.Controls.4, Version=4.0.20121.56, Culture=neutral, PublicKeyToken=9b75583953471eea" %>
<%@ Register TagPrefix="uc1" TagName="PhoneNumberControl" Src="~/Controls/PhoneNumberControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MobileNumberControl" Src="~/Controls/MobileNumberControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Attachments" Src="~/Controls/Attachments.ascx" %>
<style type="text/css">
    .dropDownStyle
    {
        z-index: 100005;
    }
    
    .RadCalendar, .RadCalendarPopup, .RadCalendarPopupShadows
    {
        z-index: 100005;
    }
</style>
<asp:HiddenField ID="hfCID" runat="server" />
<asp:HiddenField ID="hfCLID" runat="server" />
<asp:HiddenField ID="hfCSID" runat="server" />
<asp:HiddenField ID="hfBussinessTitle" runat="server" />
<br />
<telerik:RadTabStrip Skin="MetroTouch" ID="IndividualTab" runat="server" MultiPageID="IndividualTabMultiPage"
    Align="Left" AutoPostBack="false">
    <Tabs>
        <telerik:RadTab PageViewID="IndividualDetails" Text="Individual Detail" Selected="true"
            ToolTip="Main individual details" />
        <telerik:RadTab PageViewID="IndentificationDetails" Text="Identification" ToolTip="Identification" />
        <telerik:RadTab PageViewID="Attachments" Text="Attachments" ToolTip="Identification" />
    </Tabs>
</telerik:RadTabStrip>
<telerik:RadMultiPage ID="IndividualTabMultiPage" runat="server" Width="100%">
    <telerik:RadPageView ID="IndividualDetails" runat="server" BackColor="White" Selected="true">
        <asp:Panel runat="server" ID="plnIndividual" Enabled="True">
            <br />
            <fieldset style="width: 96%">
                <legend>Personal Information</legend>
                <table>
                    <tr>
                        <td>
                            <span class="riLabel">Personal Title</span>
                        </td>
                        <td>
                            <telerik:RadComboBox runat="server" ID="ddlpersonaltitle" Width="200PX" class="dropDownStyle">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="Mr" Value="Mr" Selected="true" />
                                    <telerik:RadComboBoxItem runat="server" Text="Ms" Value="Ms" />
                                    <telerik:RadComboBoxItem runat="server" Text="Mrs" Value="Mrs" />
                                    <telerik:RadComboBoxItem runat="server" Text="Miss" Value="Miss" />
                                    <telerik:RadComboBoxItem runat="server" Text="Dr" Value="Dr" />
                                    <telerik:RadComboBoxItem runat="server" Text="Rev" Value="Rev" />
                                    <telerik:RadComboBoxItem runat="server" Text="Other" Value="Other" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <span class="riLabel">Gender</span>
                        </td>
                        <td>
                            <telerik:RadComboBox runat="server" ID="dllGender" Width="196px" class="dropDownStyle">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="Male" Value="Male" Selected="true" />
                                    <telerik:RadComboBoxItem runat="server" Text="Female" Value="Female" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="riLabel">Given Name*</span>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtgiventname" runat="server" CssClass="Txt-Field" ValidationGroup="VGIndividual"
                                Width="200px">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RFVTxtGiveName" runat="server" ControlToValidate="txtgiventname"
                                ForeColor="Red" ErrorMessage="X" ValidationGroup="VGIndividual"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <span class="riLabel">Middle Name</span>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtmidname" runat="server" CssClass="Txt-Field" Width="196px">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="riLabel">SurName*</span>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtsurname" runat="server" CssClass="Txt-Field" Width="200px">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RFVTxtSurName" runat="server" ControlToValidate="txtsurname"
                                ErrorMessage="X" ForeColor="Red" ValidationGroup="VGIndividual"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <span class="riLabel">Email Address*</span>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtemailaddress" runat="server" Width="196px" CssClass="Txt-Field">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RFVTxtEmail" runat="server" ControlToValidate="txtemailaddress"
                                ErrorMessage="X" ForeColor="Red" ValidationGroup="VGIndividual"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="riLabel">Date of Birth*</span>
                        </td>
                        <td>
                            <telerik:RadDatePicker ID="dtoBirth" runat="server" Width="200px" Calendar-RangeMinDate="1/01/1900 12:00:00 AM"
                                DateInput-MinDate="1/01/1900 12:00:00 AM" MinDate="1/01/1900 12:00:00 AM" ClientIDMode="AutoID">
                                <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                                </DateInput>
                            </telerik:RadDatePicker>
                            <asp:RequiredFieldValidator runat="server" ID="RFVTxtdob" ControlToValidate="dtoBirth"
                                ErrorMessage="X" ForeColor="Red" ValidationGroup="VGIndividual"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <span class="riLabel">Home Phone Number &nbsp;&nbsp;&nbsp;</span>
                        </td>
                        <td>
                            <uc1:PhoneNumberControl ID="txthomephonenumber" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="riLabel">Work Phone Number &nbsp;&nbsp;</span>
                        </td>
                        <td>
                            <uc1:PhoneNumberControl ID="txtworkphonenumber" runat="server" />
                        </td>
                        <td>
                            <span class="riLabel">Mobile Phone Number &nbsp;&nbsp;&nbsp;</span>
                        </td>
                        <td>
                            <uc1:MobileNumberControl ID="txtmobilephonenumber" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="riLabel">Facsimile &nbsp;&nbsp;</span>
                        </td>
                        <td>
                            <uc1:PhoneNumberControl ID="txtfacsimile" runat="server" />
                        </td>
                        <td>
                            <span class="riLabel">Occupation</span>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtoccupation" runat="server" CssClass="Txt-Field" Width="196px">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="riLabel">TFN</span>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txttfn" runat="server" CssClass="Txt-Field" Width="200px">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <span class="riLabel">TIN</span>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txttin" runat="server" CssClass="Txt-Field" Width="196px">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="riLabel">Employer</span>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtemployer" runat="server" CssClass="Txt-Field" Width="200px">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <span class="riLabel">Password</span>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtpassword" runat="server" CssClass="Txt-Field" Width="196px">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="riLabel">License Number</span>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtlicensenumber" runat="server" CssClass="Txt-Field" Width="200px">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <span class="riLabel">Passport Number</span>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtpassportnumber" runat="server" CssClass="Txt-Field" Width="196px">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <fieldset style="width: 96%">
                <legend>Residential Address </legend>
                <table>
                    <tr>
                        <td style="width: 150px">
                            <span class="riLabel">Address Line 1</span>
                        </td>
                        <td style="width: 250px">
                            <telerik:RadTextBox ID="txtaddressline1" runat="server" CssClass="Txt-Field" Width="220px">
                            </telerik:RadTextBox>
                        </td>
                        <td style="width: 150px">
                            <span class="riLabel">Address Line 2</span>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtaddressline2" runat="server" CssClass="Txt-Field" Width="220px">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="riLabel">Suburb *</span>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtsuburb" runat="server" CssClass="Txt-Field" Width="220px">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <span class="riLabel">State *</span>
                        </td>
                        <td>
                            <telerik:RadComboBox runat="server" ID="ComboState" Width="220px" class="dropDownStyle">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="riLabel">Post Code</span>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtpostcode" runat="server" CssClass="Txt-Field" Width="220px">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <span class="riLabel">Country</span>
                        </td>
                        <td>
                            <telerik:RadComboBox runat="server" ID="ddlcountry" Width="220px" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlcountry_OnSelectedIndexChanged" class="dropDownStyle">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <div align="right">
                <telerik:RadButton ID="chkSameAddress" runat="server" Text="Mailing address same as above"
                    OnCheckedChanged="chkSameAddress_OnChecked" ToggleType="CheckBox" ButtonType="ToggleButton">
                </telerik:RadButton>
                &nbsp;&nbsp;&nbsp;
            </div>
            <br />
            <fieldset style="width: 96%">
                <legend>Mailing Address </legend>
                <asp:Panel runat="server" ID="panlMailingAddress">
                <table>
                    <tr>
                        <td style="width: 150px">
                            <span class="riLabel">Address Line 1</span>
                        </td>
                        <td style="width: 250px">
                            <telerik:RadTextBox ID="txtHomeAddress1" runat="server" CssClass="Txt-Field" Width="220px">
                            </telerik:RadTextBox>
                        </td>
                        <td style="width: 150px">
                            <span class="riLabel">Address Line 2</span>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtHomeAddress2" runat="server" CssClass="Txt-Field" Width="220px">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="riLabel">Suburb *</span>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtHomeSubrub" runat="server" CssClass="Txt-Field" Width="220px">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <span class="riLabel">State *</span>
                        </td>
                        <td>
                            <telerik:RadComboBox runat="server" ID="ComboHomeState" Width="220" class="dropDownStyle">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="riLabel">Post Code</span>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtHomePostCode" runat="server" CssClass="Txt-Field" Width="220px">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <span class="riLabel">Country</span>
                        </td>
                        <td>
                            <telerik:RadComboBox runat="server" ID="ComboHomeCountry" Width="220px" AutoPostBack="true"
                                OnSelectedIndexChanged="ComboHomeCountry_OnSelectedIndexChanged" class="dropDownStyle">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                </table></asp:Panel>
            </fieldset>
            <br />
            <div align="right">
                <telerik:RadButton ID="btnUpdate" runat="server" Text="Save" OnClick="btnUpdate_OnClick"
                    ValidationGroup="VGIndividual">
                </telerik:RadButton>
                <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_OnClick">
                </telerik:RadButton>
                <br />
            </div>
        </asp:Panel>
    </telerik:RadPageView>
    <telerik:RadPageView ID="IndentificationDetails" runat="server" BackColor="White">
        <asp:Panel runat="server" ID="plnIdentification" Enabled="True">
            <br />
            <fieldset style="width: 96%">
                <legend>Identification Details</legend>
                <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                    PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="false"
                    AllowFilteringByColumn="false" GridLines="None" AllowAutomaticDeletes="True"
                    AllowAutomaticInserts="True" AllowAutomaticUpdates="True" EnableViewState="true"
                    ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource" OnItemCommand="PresentationGrid_OnItemCommand"
                    OnItemDataBound="PresentationGrid_OnItemDataBound">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                        Name="IDDocuments" TableLayout="Fixed" DataKeyNames="Type">
                        <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                                UniqueName="EditColumn">
                                <HeaderStyle Width="2%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                            <telerik:GridBoundColumn SortExpression="Type" ReadOnly="true" HeaderText="Type"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="Type" UniqueName="Type" HeaderStyle-Width="30%">
                            </telerik:GridBoundColumn>
                            <telerik:GridCheckBoxColumn SortExpression="IsEnabled" ReadOnly="false" HeaderText="Is Enabled?"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="IsEnabled" UniqueName="IsEnabledID"
                                HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center">
                            </telerik:GridCheckBoxColumn>
                            <telerik:GridBoundColumn SortExpression="DocNumber" ReadOnly="false" HeaderText="Doc Number"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="DocNumber" UniqueName="DocNumber">
                            </telerik:GridBoundColumn>
                            <telerik:GridDateTimeColumn UniqueName="IssueDate" PickerType="DatePicker" HeaderText="Issue Date"
                                DataField="IssueDate" DataFormatString="{0:dd/MM/yyyy}" ReadOnly="false">
                            </telerik:GridDateTimeColumn>
                            <telerik:GridDateTimeColumn UniqueName="ExpiryDate" PickerType="DatePicker" HeaderText="Expiry Date"
                                DataField="ExpiryDate" DataFormatString="{0:dd/MM/yyyy}" ReadOnly="false">
                            </telerik:GridDateTimeColumn>
                            <telerik:GridCheckBoxColumn SortExpression="IsPhotoShown" ReadOnly="false" HeaderText="Is Photo Shown?"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="IsPhotoShown" UniqueName="IsPhotoShown"
                                HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center">
                            </telerik:GridCheckBoxColumn>
                            <telerik:GridCheckBoxColumn SortExpression="IsAddressMatch" ReadOnly="false" HeaderText="Is Address Match?"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="IsAddressMatch" UniqueName="IsAddressMatch"
                                HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center">
                            </telerik:GridCheckBoxColumn>
                            <telerik:GridTemplateColumn DataField="FileDownLoadURL" Display="false" HeaderText="Attachment"
                                UniqueName="Upload">
                                <EditItemTemplate>
                                    <telerik:RadAsyncUpload runat="server" ID="AsyncUpload1" AllowedFileExtensions="csv,txt,xls,xlsx,doc,docx,pdf,ppt,pptx,jpg,jpeg,png,gif"
                                        MaxFileSize="1048576">
                                    </telerik:RadAsyncUpload>
                                </EditItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridButtonColumn ConfirmText="Are you sure you want to remove the ID details from the Individual?"
                                ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                <HeaderStyle Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <br />
                <div align="right">
                    <telerik:RadButton ID="RadButton2" runat="server" Text="Cancel" OnClick="btnCancel_OnClick">
                    </telerik:RadButton>
                    <br />
                </div>
            </fieldset>
        </asp:Panel>
    </telerik:RadPageView>
    <telerik:RadPageView ID="Attachments" runat="server" BackColor="White">
        <asp:Panel runat="server" ID="plnAttachment" Enabled="True">
            <br />
            <uc1:Attachments ID="AttachmentControl" runat="server" />
        </asp:Panel>
    </telerik:RadPageView>
</telerik:RadMultiPage>
