﻿using System.Data;
using Oritax.TaxSimp.CM.Group;
using System;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;

namespace eclipseonlineweb.Controls
{
    public partial class DealerGroupTrustControl : System.Web.UI.UserControl
    {
        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }
        public Action<Guid> Saved
        {
            get;
            set;
        }
        public Action<Guid, DataSet> SaveData
        {
            get;
            set;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadControls();
            }
        }
        private void LoadControls()
        {
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            var status = new StatusDS { TypeFilter = "Organization" };
            org.GetData(status);
            ddlStatusFlag.Items.Clear();
            ddlStatusFlag.DataSource = status.Tables[StatusDS.TABLENAME];
            ddlStatusFlag.DataTextField = StatusDS.NAME;
            ddlStatusFlag.DataValueField = StatusDS.NAME;
            ddlStatusFlag.DataBind();
        }
        public DealerGroupDS GetEntity()
        {

            DealerGroupDS dealerGroupDs = new DealerGroupDS();
            DataRow dr = dealerGroupDs.Tables[dealerGroupDs.DealerGroupTable.TableName].NewRow();
            dr[dealerGroupDs.DealerGroupTable.TRADINGNAME] = txtTradingName.Text;
            dr[dealerGroupDs.DealerGroupTable.NAME] = txtTradingName.Text;

            dr[dealerGroupDs.DealerGroupTable.STATUS] = ddlStatusFlag.SelectedValue;
            dr[dealerGroupDs.DealerGroupTable.DEALERGROUPTYPE] = hf_DealerGroupType.Value;
            if (chkSameAsTrading.Checked)
            {
                dr[dealerGroupDs.DealerGroupTable.LEGALNAME] = txtTradingName.Text;
            }
            else
            {
                dr[dealerGroupDs.DealerGroupTable.LEGALNAME] = txtLegalName.Text;
            }
            dr[dealerGroupDs.DealerGroupTable.TITLE] = ddlSoleTraderTitle.SelectedValue;
            dr[dealerGroupDs.DealerGroupTable.PREFERREDNAME] = txtPreferredName.Text;
            dr[dealerGroupDs.DealerGroupTable.SURNAME] = txtSoleTraderSurname.Text;
            dr[dealerGroupDs.DealerGroupTable.EMAIL] = txtSoleTraderEmail.Text;
            dr[dealerGroupDs.DealerGroupTable.GENDER] = ddlSoleTraderGender.SelectedValue;
            dr[dealerGroupDs.DealerGroupTable.FACSIMLENUMBER] = FacsimleNo.Number;
            dr[dealerGroupDs.DealerGroupTable.FACSIMLECOUTNRYCODE] = FacsimleNo.CountryCode;
            dr[dealerGroupDs.DealerGroupTable.FACSIMLECITYCODE] = FacsimleNo.CityCode;
            dr[dealerGroupDs.DealerGroupTable.ABN] = ABNNo.GetEntity();
            dr[dealerGroupDs.DealerGroupTable.TFN] = TFNNo.GetEntity();
            dr[dealerGroupDs.DealerGroupTable.OCCUPATION] = txtSoleTraderOccupation.Text;
            dr[dealerGroupDs.DealerGroupTable.WORKPHONENUMBER] = WorkPhNo.Number;
            dr[dealerGroupDs.DealerGroupTable.WORKPHONECOUTNRYCODE] = WorkPhNo.CountryCode;
            dr[dealerGroupDs.DealerGroupTable.WORKPHONECITYCODE] = WorkPhNo.CityCode;
            dr[dealerGroupDs.DealerGroupTable.HOMEPHONENUMBER] = HomePhNo.Number;
            dr[dealerGroupDs.DealerGroupTable.HOMEPHONECOUTNRYCODE] = HomePhNo.CountryCode;
            dr[dealerGroupDs.DealerGroupTable.HOMEPHONECITYCODE] = HomePhNo.CityCode;
            dr[dealerGroupDs.DealerGroupTable.MOBILEPHONENUMBER] = MobNo.Number;
            dr[dealerGroupDs.DealerGroupTable.MOBILEPHONECOUTNRYCODE] = MobNo.CountryCode;

            dealerGroupDs.Tables[dealerGroupDs.DealerGroupTable.TableName].Rows.Add(dr);
            return dealerGroupDs;
        }
        public void SetEntity(Guid CID, DealerGroupEntityType DealerGroupType)
        {
            ClearEntity();
            hf_DealerGroupType.Value = DealerGroupType.ToString();
            if (CID != Guid.Empty)
            {
                DealerGroupDS ds = GetDealorGroupDetails(CID);
                hfCID.Value = CID.ToString();
                DataRow dr = ds.Tables[ds.DealerGroupTable.TableName].Rows[0];
                txtClientID.Text = dr[ds.DealerGroupTable.CLIENTID].ToString();
                txtTradingName.Text = dr[ds.DealerGroupTable.TRADINGNAME].ToString();
                txtLegalName.Text = dr[ds.DealerGroupTable.LEGALNAME].ToString();
                if (txtTradingName.Text == txtLegalName.Text)
                {
                    chkSameAsTrading.Checked = true;
                    txtLegalName.Enabled = false;
                }
                else
                {
                    chkSameAsTrading.Checked = false;
                    txtLegalName.Enabled = true;
                }
                ddlStatusFlag.SelectedValue = dr[ds.DealerGroupTable.STATUS].ToString();
                ddlSoleTraderTitle.SelectedValue = dr[ds.DealerGroupTable.TITLE].ToString();
                txtPreferredName.Text = dr[ds.DealerGroupTable.PREFERREDNAME].ToString();
                txtSoleTraderSurname.Text = dr[ds.DealerGroupTable.SURNAME].ToString();
                txtSoleTraderEmail.Text = dr[ds.DealerGroupTable.EMAIL].ToString();
                ddlSoleTraderGender.SelectedValue = dr[ds.DealerGroupTable.GENDER].ToString();
                FacsimleNo.Number = dr[ds.DealerGroupTable.FACSIMLENUMBER].ToString();
                FacsimleNo.CountryCode = dr[ds.DealerGroupTable.FACSIMLECOUTNRYCODE].ToString();
                FacsimleNo.CityCode = dr[ds.DealerGroupTable.FACSIMLECITYCODE].ToString();
                ABNNo.SetEntity(dr[ds.DealerGroupTable.ABN].ToString());
                TFNNo.SetEntity(dr[ds.DealerGroupTable.TFN].ToString());
                txtSoleTraderOccupation.Text = dr[ds.DealerGroupTable.OCCUPATION].ToString();
                WorkPhNo.Number = dr[ds.DealerGroupTable.WORKPHONENUMBER].ToString();
                WorkPhNo.CountryCode = dr[ds.DealerGroupTable.WORKPHONECOUTNRYCODE].ToString();
                WorkPhNo.CityCode = dr[ds.DealerGroupTable.WORKPHONECITYCODE].ToString();
                HomePhNo.Number = dr[ds.DealerGroupTable.HOMEPHONENUMBER].ToString();
                HomePhNo.CountryCode = dr[ds.DealerGroupTable.HOMEPHONECOUTNRYCODE].ToString();
                HomePhNo.CityCode = dr[ds.DealerGroupTable.HOMEPHONECITYCODE].ToString();
                MobNo.Number = dr[ds.DealerGroupTable.MOBILEPHONENUMBER].ToString();
                MobNo.CountryCode = dr[ds.DealerGroupTable.MOBILEPHONECOUTNRYCODE].ToString();
            }
        }
        private DealerGroupDS GetDealorGroupDetails(Guid cid)
        {
            var dealerGroupDs = new DealerGroupDS { CommandType = DatasetCommandTypes.Details };
            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
                clientData.GetData(dealerGroupDs);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }
            return dealerGroupDs;
        }
        public void ClearEntity()
        {
            txtTradingName.Text = "";
            txtLegalName.Text = "";
            txtPreferredName.Text = "";
            txtSoleTraderEmail.Text = "";
            txtSoleTraderOccupation.Text = "";
            txtSoleTraderSurname.Text = "";
            HomePhNo.ClearEntity();
            WorkPhNo.ClearEntity();
            FacsimleNo.ClearEntity();
            MobNo.ClearEntity();
            ABNNo.ClearEntity();
            TFNNo.ClearEntity();
        }
        public bool Validate()
        {
            return Validate(true);
        }
        public bool Validate(bool ShowTabError)
        {
            bool result = true;

            if (!ABNNo.HasValue && !TFNNo.HasValue)
            {
                txtMessage.Text = "Please provide ABN or TFN";
                result = false;
            }
            else
            {

                txtMessage.Text = "";
            }

            result = result & ABNNo.Validate() & TFNNo.Validate() & HomePhNo.Validate() &
                     WorkPhNo.Validate() &
                     FacsimleNo.Validate() &
                     MobNo.Validate();
            return result;
        }
       
        protected void btnSaveDealerIndivisual_Click(object sender, System.EventArgs e)
        {
            if (Validate())
            {
                DealerGroupDS dealerGroupDs = new DealerGroupDS();
                var cid = (string.IsNullOrEmpty(hfCID.Value)) ? Guid.Empty : new Guid(hfCID.Value);
                if (cid == Guid.Empty)
                {
                    dealerGroupDs.CommandType = DatasetCommandTypes.Add;
                }
                else
                {
                    dealerGroupDs.CommandType = DatasetCommandTypes.Update;
                }
                if (Broker != null)
                {
                    dealerGroupDs.DealerGroupTable.Merge(GetEntity().DealerGroupTable);
                    if (cid == Guid.Empty)
                    {
                        var unit = new OrganizationUnit
                            {
                                OrganizationStatus = "Active",
                                Name = txtTradingName.Text,
                                Type = ((int)OrganizationType.DealerGroup).ToString(),
                                CurrentUser = (Page as UMABasePage).GetCurrentUser()
                            };

                        dealerGroupDs.Unit = unit;
                        dealerGroupDs.Command = (int)WebCommands.AddNewOrganizationUnit;
                    }
                    if (SaveData != null)
                    {
                        SaveData(cid, dealerGroupDs);
                        if (dealerGroupDs.CommandType == DatasetCommandTypes.Add)
                        {
                            cid = dealerGroupDs.Unit.Cid;
                            hfCID.Value = cid.ToString();
                            if (Saved != null)
                                Saved(cid);
                        }
                        lblMsg.Text = "Saved successfully.";
                        lblMsg.Visible = true;
                    }
                    //Enable Disable Legal control
                    txtLegalName.Enabled = !chkSameAsTrading.Checked;
                }
                else
                {
                    throw new Exception("Broker Not Found");
                }
            }
        }
    }
}