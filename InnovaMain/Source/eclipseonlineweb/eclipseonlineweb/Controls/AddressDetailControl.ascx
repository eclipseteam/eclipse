﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddressDetailControl.ascx.cs"
    Inherits="eclipseonlineweb.Controls.AddressDetailControl" %>
<%@ Register Src="../Controls/AddressControl.ascx" TagName="AddressControl" TagPrefix="uc2" %>
<asp:Panel ID="pnlMain" runat="server">
    <fieldset>
        <table width="100%">
            <tr>
                <td>
                    <uc2:AddressControl ID="BusinessAddressControl" Title="Business Address" runat="server" />
                    <br />
                    <uc2:AddressControl ID="ResidentialAddressControl" Title="Residential Address" runat="server" />
                    <br />
                    <uc2:AddressControl ID="RegisteredAddressControl" Title="Registered Address" runat="server" />
                    <br />
                    <uc2:AddressControl ID="MailingAddressControl" Title="Mailing Address" runat="server" />
                    <br />
                    <uc2:AddressControl ID="DuplicateMailingAddressControl" Title="Duplicate Mailing Address"
                        runat="server" />
                </td>
            </tr>
        </table>
    </fieldset>
</asp:Panel>
