﻿using Oritax.TaxSimp.CM.Group;
using System.Data;
using Oritax.TaxSimp.DataSets;
using System;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Commands;


namespace eclipseonlineweb.Controls
{
    public partial class AccountantCompanyControl : System.Web.UI.UserControl
    {
        private ICMBroker Broker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        public Action<Guid> Saved
        {
            get;
            set;
        }

        public Action<Guid, DataSet> SaveData
        {
            get;
            set;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadControls();
            }
        }
        private void LoadControls()
        {
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            var status = new StatusDS { TypeFilter = "Organization" };
            org.GetData(status);
            ddlStatusFlag.Items.Clear();
            ddlStatusFlag.DataSource = status.Tables[StatusDS.TABLENAME];
            ddlStatusFlag.DataTextField = StatusDS.NAME;
            ddlStatusFlag.DataValueField = StatusDS.NAME;
            ddlStatusFlag.DataBind();
        }
        public AccountantDS GetEntity()
        {
            AccountantDS accountantDs = new AccountantDS();
            DataRow dr = accountantDs.Tables[accountantDs.accountantTable.TableName].NewRow();
            dr[accountantDs.accountantTable.STATUS] = ddlStatusFlag.SelectedValue;
            dr[accountantDs.accountantTable.TRADINGNAME] = txtTradingName.Text;
            dr[accountantDs.accountantTable.ACCOUNTANTTYPE] = hf_AccountantType.Value;
            dr[accountantDs.accountantTable.NAME] = txtTradingName.Text;
            if (chkSameAsTrading.Checked)
            {
                dr[accountantDs.accountantTable.LEGALNAME] = txtTradingName.Text;
            }
            else
            {
                dr[accountantDs.accountantTable.LEGALNAME] = txtLegalName.Text;
            }
            dr[accountantDs.accountantTable.ACCOUNTDESIGNATION] = txtAccountDesignation.Text;
            dr[accountantDs.accountantTable.FACSIMILE] = ph_facsimle.Number;
            dr[accountantDs.accountantTable.FACSIMILECOUNTRYCODE] = ph_facsimle.CountryCode;
            dr[accountantDs.accountantTable.FACSIMILECITYCODE] = ph_facsimle.CityCode;
            dr[accountantDs.accountantTable.PHONENUBER] = ph_phoneNumber.Number;
            dr[accountantDs.accountantTable.PHONENUMBERCOUNTRYCODE] = ph_phoneNumber.CountryCode;
            dr[accountantDs.accountantTable.PHONENUMBERCITYCODE] = ph_phoneNumber.CityCode;
            dr[accountantDs.accountantTable.WEBSITEADDRESS] = txtWebSiteAddress.Text;
            dr[accountantDs.accountantTable.ABN] = abn_ABN.GetEntity();
            dr[accountantDs.accountantTable.ACN] = acn_ACN.GetEntity();
            dr[accountantDs.accountantTable.TFN] = tfn_TFN.GetEntity();
            accountantDs.Tables[accountantDs.accountantTable.TableName].Rows.Add(dr);
            return accountantDs;
        }
        public void SetEntity(Guid CID, AccountantEntityType accountantType)
        {
            ClearEntity();
            hf_AccountantType.Value = accountantType.ToString();
            if (CID != Guid.Empty)
            {
                AccountantDS ds = GetAccountantDetails(CID);
                hfCID.Value = CID.ToString();
                DataRow dr = ds.Tables[ds.accountantTable.TableName].Rows[0];
                txtClientID.Text = dr[ds.accountantTable.CLIENTID].ToString();
                txtTradingName.Text = dr[ds.accountantTable.TRADINGNAME].ToString();
                txtLegalName.Text = dr[ds.accountantTable.LEGALNAME].ToString();
                if (txtTradingName.Text == txtLegalName.Text)
                {
                    chkSameAsTrading.Checked = true;
                    txtLegalName.Enabled = false;
                }
                else
                {
                    chkSameAsTrading.Checked = false;
                    txtLegalName.Enabled = true;
                }
                txtAccountDesignation.Text = dr[ds.accountantTable.ACCOUNTDESIGNATION].ToString();
                ph_facsimle.CountryCode = dr[ds.accountantTable.FACSIMILECOUNTRYCODE].ToString();
                ph_facsimle.CityCode = dr[ds.accountantTable.FACSIMILECITYCODE].ToString();
                ph_facsimle.Number = dr[ds.accountantTable.FACSIMILE].ToString();
                ph_phoneNumber.CountryCode = dr[ds.accountantTable.PHONENUMBERCOUNTRYCODE].ToString();
                ph_phoneNumber.CityCode = dr[ds.accountantTable.PHONENUMBERCITYCODE].ToString();
                ph_phoneNumber.Number = dr[ds.accountantTable.PHONENUBER].ToString();
                txtWebSiteAddress.Text = dr[ds.accountantTable.WEBSITEADDRESS].ToString();
                abn_ABN.SetEntity(dr[ds.accountantTable.ABN].ToString());
                acn_ACN.SetEntity(dr[ds.accountantTable.ACN].ToString());
                tfn_TFN.SetEntity(dr[ds.accountantTable.TFN].ToString());
            }
        }
        private AccountantDS GetAccountantDetails(Guid cid)
        {
            var accountantDs = new AccountantDS { CommandType = DatasetCommandTypes.Details };
            if (Broker != null)
            {
                IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
                clientData.GetData(accountantDs);
                Broker.ReleaseBrokerManagedComponent(clientData);
            }
            else
            {
                throw new Exception("Broker Not Found");
            }
            return accountantDs;
        }
        public void ClearEntity()
        {
            hfCID.Value = Guid.Empty.ToString();
            hf_AccountantType.Value = "";
            txtTradingName.Text = "";
            txtLegalName.Text = "";
            txtAccountDesignation.Text = "";
            ph_facsimle.ClearEntity();
            ph_phoneNumber.ClearEntity();
            txtWebSiteAddress.Text = "";
            abn_ABN.ClearEntity();
            acn_ACN.ClearEntity();
            tfn_TFN.ClearEntity();
        }
        public bool Validate()
        {
            return Validate(true);
        }
        public bool Validate(bool ShowTabError)
        {
            bool result = true;

            if (!abn_ABN.HasValue && !tfn_TFN.HasValue)
            {
                txtMessage.Text = "Please provide ABN or TFN";
                result = false;
            }
            else
            {
                txtMessage.Text = "";
            }
            result = result & abn_ABN.Validate() & tfn_TFN.Validate() & acn_ACN.Validate() & ph_phoneNumber.Validate() & ph_facsimle.Validate();

            return result;
        }
       
        protected void btnSaveAccountantCompany_Click(object sender, System.EventArgs e)
        {
            if (Validate())
            {
            AccountantDS accountantDs = new AccountantDS();
            var cid = (string.IsNullOrEmpty(hfCID.Value)) ? Guid.Empty : new Guid(hfCID.Value);
            if (cid == Guid.Empty)
            {
                accountantDs.CommandType = DatasetCommandTypes.Add;
            }
            else
            {
                accountantDs.CommandType = DatasetCommandTypes.Update;
            }
            if (Broker != null)
            {
                accountantDs.accountantTable.Merge(GetEntity().accountantTable);
                if (cid == Guid.Empty)
                {
                    var unit = new OrganizationUnit
                                   {
                                       OrganizationStatus = "Active",
                                       Name = txtTradingName.Text,
                                       Type = ((int)OrganizationType.Accountant).ToString(),
                                       CurrentUser = (Page as UMABasePage).GetCurrentUser()
                                   };

                    accountantDs.Unit = unit;
                    accountantDs.Command = (int)WebCommands.AddNewOrganizationUnit;
                }
                if (SaveData != null)
                {
                        SaveData(cid, accountantDs);
                        if (accountantDs.CommandType == DatasetCommandTypes.Add)
                        {
                            cid = accountantDs.Unit.Cid;
                            hfCID.Value = cid.ToString();

                            if (Saved != null)
                                Saved(cid);
                        }
                }
                //Enable Disable Legal control
                txtLegalName.Enabled = !chkSameAsTrading.Checked;
            }
            else
            {
                throw new Exception("Broker Not Found");
            }
            }
           
        }
    }
}