﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.CM.Organization.Data;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class _Default : UMABasePage
    {
        private bool flag = false;

        protected override void Intialise()
        {
            base.Intialise();
            this.ClientList.RowCommand += new C1GridViewCommandEventHandler(ClientList_RowCommand);
        }

        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            DataSet excelDataset = new DataSet();
            DataView entityView = new DataView(this.PresentationData.Tables["Entities_Table"]);
            entityView.Sort = "ENTITYNAME_FIELD ASC";
            DataTable entityTable = entityView.ToTable();
            entityTable.Columns.Remove("ENTITYCLID_FIELD");
            entityTable.Columns.Remove("ENTITYCSID_FIELD");
            entityTable.Columns.Remove("ENTITYCIID_FIELD");
            entityTable.Columns.Remove("ENTITYCTID_FIELD");
            entityTable.Columns.Remove("ENTITYSCTYPE_FIELD");
            entityTable.Columns.Remove("ENTITYSCSTATUS_FIELD");
            entityTable.Columns.Remove("ENTITYSORT_FIELD");

            excelDataset.Merge(entityTable, true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, "Accounts-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", this.Page.Response);
        }

        protected void SortMainList(object sender, C1GridViewSortEventArgs e)
        {
            IBrokerManagedComponent iBMC = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            OrganisationListingDS ds = new OrganisationListingDS();
            ds.OrganisationListingOperationType = OrganisationListingOperationType.Clients;
            iBMC.GetData(ds);
            this.PresentationData = ds;

            DataView entityView = new DataView(ds.Tables["Entities_Table"]);
            entityView.Sort = "ENTITYNAME_FIELD ASC";
            DataTable entityTable = entityView.ToTable();

            this.ClientList.DataSource = entityView.ToTable();
            this.ClientList.DataBind();

            this.UMABroker.ReleaseBrokerManagedComponent(iBMC);
        }

        protected void Filter(object sender, C1GridViewFilterEventArgs e)
        {
            e.Values[0] = ((string)e.Values[0]).Trim();
            DataView entityView = new DataView(this.PresentationData.Tables["Entities_Table"]);
            entityView.RowFilter = "IsClient='true'";
            entityView.Sort = "ENTITYNAME_FIELD ASC";
            this.ClientList.DataSource = entityView.ToTable();
            ClientList.DataBind();
        }

        protected override void GetBMCData()
        {
            IBrokerManagedComponent iBMC = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            OrganisationListingDS ds = new OrganisationListingDS();

            ds.OrganisationListingOperationType = OrganisationListingOperationType.ClientsSearch;
            iBMC.GetData(ds);
            this.PresentationData = ds;

            DataView entityView = new DataView(ds.Tables["Entities_Table"]);
            entityView.RowFilter = "IsClient='true'";
            entityView.Sort = "ENTITYNAME_FIELD ASC";
            DataTable entityTable = entityView.ToTable();

            this.ClientList.DataSource = entityView.ToTable();
            this.ClientList.DataBind();

            this.UMABroker.ReleaseBrokerManagedComponent(iBMC);
        }

        public override void LoadPage()
        {
            if (IsTestApp())
            {
                Response.Redirect("Dashboard.aspx");
            }
            else
            {
                DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");

                if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
                    Response.Redirect("AccountsFUM.aspx");
                else
                    Response.Redirect("ClientSearch.aspx");

                UMABroker.ReleaseBrokerManagedComponent(objUser);
            }
        }

        private void AllClientsClick()
        {
            DataView entityView = new DataView(PresentationData.Tables["Entities_Table"]);
            entityView.RowFilter = "IsClient='true'";
            entityView.Sort = "ENTITYNAME_FIELD ASC";
            DataTable entityTable = entityView.ToTable();
            this.ClientList.DataSource = entityView.ToTable();
            this.ClientList.DataBind();
        }

        private void ClientSearchClick()
        {
            Response.Redirect("ClientSearch.aspx");
        }

        private void AllClientsFUMClick()
        {
            Response.Redirect("AccountsFUM.aspx");
        }

        protected void NavigationMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            if (e.Item.Text == "ALL ACCOUNTS")
                AllClientsClick();
            else if (e.Item.Text == "UMA ACCOUNTS - $FUM")
                AllClientsFUMClick();
            else if (e.Item.Text == "ACCOUNTS SEARCH")
                ClientSearchClick();
        }

        public override void PopulatePage(DataSet ds)
        {
            if (!this.IsPostBack)
            {
                DataView entityView = new DataView(ds.Tables["Entities_Table"]);
                entityView.RowFilter = "IsClient='true'";
                entityView.Sort = "ENTITYNAME_FIELD ASC";
                DataTable entityTable = entityView.ToTable();
                this.ClientList.DataSource = entityView.ToTable();
                this.ClientList.DataBind();
            }
        }

        protected void ClientList_PageIndexChanging(object sender, C1GridViewPageEventArgs e)
        {
            ClientList.PageIndex = e.NewPageIndex;
            ClientList.DataBind();
        }

        protected void ClientList_RowCommand(object sender, C1GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "select")
            {
                this.RequestInstanceID = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[0].Text;
                Response.Redirect("ClientViews/ClientMainView.aspx?ins=" + this.RequestInstanceID);
            }
        }

        protected void C1GridView1_SelectedIndexChanging(object sender, C1.Web.Wijmo.Controls.C1GridView.C1GridViewSelectEventArgs e)
        {
        }

        protected void ClientList_DataBound(object sender, EventArgs e)
        {
            if (flag)
            {
                this.ClientList.SelectedIndex = 0;
            }
        }
    }
}
