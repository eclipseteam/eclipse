﻿using System.Data;
using Oritax.TaxSimp.CalculationInterface;
using Telerik.Web.UI;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class Dashboard : UMABasePage
    {
        public override void LoadPage()
        {
            base.LoadPage();
            SetDashBoardVisiblity();
        }

        private void SetDashBoardVisiblity()
        {
            var user = GetCurrentUser();
            divAdmin.Visible = user.IsAdmin;
            //Temporarily remarked the adviser dashboard on client request.
            divAdviser.Visible = (UserType)user.UserType == UserType.Advisor;
            if ((UserType)user.UserType == UserType.Advisor)
            {
                bool isAllowedAdvisor = (user.Email.ToLower().Contains("nac.com.au") || user.Email.ToLower().Contains("priorityplanners.com.au"));
                //Alert Grid Visible
                dashAdviser.SetPanelVisibilty(isAllowedAdvisor);
            }
        }
    }
}