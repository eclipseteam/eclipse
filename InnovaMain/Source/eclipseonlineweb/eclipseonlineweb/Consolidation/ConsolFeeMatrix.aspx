﻿<%@ Page Title="e-Clipse Online Portal - Configure Fees" Language="C#" MasterPageFile="ConsolViewMaster.master"
    AutoEventWireup="true" CodeBehind="ConsolFeeMatrix.aspx.cs" Inherits="eclipseonlineweb.ConsolFeeDistribution" %>

<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function GoToPage(url) {
            window.location = url + '?ins=<%:cid %>';
            return false;
        }
    </script>
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="15%">
                       <telerik:RadComboBox runat="server" Width="320px" Filter="Contains"  ID="cmbFeeList"></telerik:RadComboBox>
                    </td>
                     <td width="15%">
                        <telerik:RadButton ID="btnLoadFee" Width="220px" runat="server" Text="LOAD FEE RUN"
                            OnClick="btnFeeLoad_Click">
                        </telerik:RadButton>
                    </td>

                    <td width="15%">
                        <telerik:RadButton ID="btnFeeRun" Width="220px" runat="server" Text="RUN CONSOL FEE"
                            OnClick="btnFeeRun_Click">
                        </telerik:RadButton>
                    </td>
                    <td width="1%">
                    </td>
                    <td style="width: 30%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid" 
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
                OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
                OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
                OnItemDataBound="PresentationGrid_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="CLIENTCID" ShowGroupFooter="true"  AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="ConsolCalculatedFees" TableLayout="Fixed" EditMode="EditForms">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <GroupByExpressions>
                        <telerik:GridGroupByExpression>
                            <SelectFields>
                                <telerik:GridGroupByField FieldName="CLIENTNAME" HeaderText="Client Name" HeaderValueSeparator=" : "></telerik:GridGroupByField>
                            </SelectFields>
                            <GroupByFields>
                                <telerik:GridGroupByField FieldName="CLIENTNAME" SortOrder="Ascending"></telerik:GridGroupByField>
                            </GroupByFields>
                        </telerik:GridGroupByExpression>
                    </GroupByExpressions>
                   
                    <Columns>
                       <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="FEETYPE" ReadOnly="true" FooterText="Total" FooterStyle-Font-Bold="true"
                            HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderText="Fee Type" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="FEETYPE" UniqueName="FEETYPE">
                        </telerik:GridBoundColumn>
                      
                        <telerik:GridBoundColumn FilterControlWidth="100px" FooterAggregateFormatString="{0:C}"
                            Aggregate="Sum" ReadOnly="true" DefaultInsertValue="2000" SortExpression="FEETOTAL"
                            HeaderStyle-Width="20%" ItemStyle-Width="15%" HeaderText="Caculated Fee" AutoPostBackOnFilter="true"  FooterStyle-Font-Bold="true"
                            CurrentFilterFunction="Contains" DataFormatString="{0:c}" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="FEETOTAL" UniqueName="FEETOTAL">
                        </telerik:GridBoundColumn>
                        
                        <telerik:GridBoundColumn FilterControlWidth="100px" FooterAggregateFormatString="{0:C}"
                            Aggregate="Sum" ReadOnly="true" DefaultInsertValue="2000" SortExpression="AUTOADJUST"
                            HeaderStyle-Width="20%" ItemStyle-Width="15%" HeaderText="Auto Adjustment" AutoPostBackOnFilter="true"  FooterStyle-Font-Bold="true"
                            CurrentFilterFunction="Contains" DataFormatString="{0:c}" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="AUTOADJUST" UniqueName="AUTOADJUST">
                        </telerik:GridBoundColumn>
                        
                        <telerik:GridBoundColumn FilterControlWidth="100px" FooterAggregateFormatString="{0:C}"
                            Aggregate="Sum" ReadOnly="true" DefaultInsertValue="2000" SortExpression="ADJUST"
                            HeaderStyle-Width="20%" ItemStyle-Width="15%" HeaderText="Manual Adjustment" AutoPostBackOnFilter="true"  FooterStyle-Font-Bold="true"
                            CurrentFilterFunction="Contains" DataFormatString="{0:c}" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="ADJUST" UniqueName="ADJUST">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterControlWidth="100px" FooterAggregateFormatString="{0:C}"
                            Aggregate="Sum" ReadOnly="true" DefaultInsertValue="2000" SortExpression="FEETOTALADJUST"
                            HeaderStyle-Width="20%" ItemStyle-Width="15%" HeaderText="Total Fee" AutoPostBackOnFilter="true"  FooterStyle-Font-Bold="true"
                            CurrentFilterFunction="Contains" DataFormatString="{0:c}" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="FEETOTALADJUST" UniqueName="FEETOTALADJUST">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
