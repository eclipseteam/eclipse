﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.Utilities;
using System.Threading;

namespace eclipseonlineweb
{
    public partial class ConsolViewMaster : MasterPage
    {
        protected string Cid = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            Cid = Request.Params["ins"];

            if (Master != null) ((HiddenField)Master.FindControl("hfMainMenuText")).Value = "HOME";

            UMABroker = new CMBroker(Context.User, DBConnection.Connection.ConnectionString);
            UMABroker.SetStart();


            var objUser = (DBUser)UMABroker.GetBMCInstance(Page.User.Identity.Name, "DBUser_1_1");
        }

        public bool IsAdminUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(Page.User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType == UserType.Innova || objUser.Name == "Administrator")
                return true;
            else
                return false;
        }
        private ICMBroker UMABroker
        {
            get
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                return (ICMBroker)Thread.GetData(brokerSlot);
            }
            set
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (value == null)
                    Thread.FreeNamedDataSlot("Broker");

                Thread.SetData(brokerSlot, value);
            }
        }
    }
}