﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/Consolidation/ConsolViewMaster.master"
    AutoEventWireup="true" CodeBehind="ConsolMembers.aspx.cs" Inherits="eclipseonlineweb.ConsolMembers" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Chart"
    TagPrefix="wijmo" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript">
        //Resetting menu index
        localStorage['MenuIndex'] = '0';
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function hintContent() {
            return this.data.label + '<br/> ' + this.y + '';
        }
        function hintContentPie() {
            return this.data.toString() + " : " + window.Globalize.format(this.value / this.total, "p2");
        }

    </script>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlToolBar">
                <fieldset>
                    <table width="100%">
                        <tr>
                            <td style="width: 5%">
                                <td style="width: 85%;" class="breadcrumbgap">
                                    <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                                    <br />
                                    <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                                </td>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </asp:Panel>
          <table>
        <tr>
            <td colspan="3">
                <span class="riLabelBold">Included Entities</span>
            </td>
            <td colspan="2">
                <span class="riLabelBold">Available Entities</span>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:radcombobox id="cmbIncluded" runat="server" width="450px"  IsEditable="True"  AllowCustomText= "True"  EnableTextSelection ="True" CheckBoxes="true"   >
                </telerik:radcombobox>
            </td>
            <td>
                <telerik:radbutton id="btnRemove" runat="server" text="Remove >>" onclick="button_RemoveMember_Click">
                </telerik:radbutton>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <telerik:radcombobox id="cmbAvailable" runat="server" width="450px" IsEditable="True" AllowCustomText= "True"  EnableTextSelection ="True" CheckBoxes="true"  >

                </telerik:radcombobox>
            </td>
            <td>
                <telerik:radbutton id="btnAdd" runat="server" text="<< Add" onclick="button_AddMember_Click">
                </telerik:radbutton>
            </td>
        </tr>
    </table>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
