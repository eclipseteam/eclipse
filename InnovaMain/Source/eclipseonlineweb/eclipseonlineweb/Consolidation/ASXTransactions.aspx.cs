﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.Common;
using System.Collections;

namespace eclipseonlineweb
{
    public partial class ASXTransactionsGroup : UMABasePage
    {
        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];

            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
                sm.RegisterPostBackControl(btnReport);

            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");

            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                ASXTransGrid.Columns.FindByUniqueNameSafe("DeleteColumn").Visible = false;
                ASXTransGrid.Columns.FindByUniqueNameSafe("EditColumn").Visible = false;
                btnImportASXHistoricalCostbase.Visible = false;
                ASXTransGrid.MasterTableView.CommandItemSettings.ShowAddNewRecordButton = false;
                this.PresentationGrid.MasterTableView.CommandItemSettings.ShowAddNewRecordButton = false;
            }

            UMABroker.ReleaseBrokerManagedComponent(objUser);
            if (!IsPostBack)
                ASXPnl.Visible = true;
        }

        protected void BtnImportASXHistorical(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/SysAdministration/ImportASXHistoricalCostbase.aspx?ins=" + Request.QueryString["ins"].ToString());
        }

        protected void BackToSummary(object sender, EventArgs args)
        {
            ASXPnl.Visible = true;
            pnlASXAdjustments.Visible = false;
        }

        protected void BtnReportClick(object sender, EventArgs e)
        {
            BulkTransactionsOutput(this.cid, this.ClientHeaderInfo1.ClientName);
        }

        private void GetData(bool isCommand = false)
        {
            ASXPnl.Visible = true;

            cid = Request.QueryString["ins"];
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(cid));

            var asxds = new ASXTransactionDS();
            clientData.GetData(asxds);
            PresentationData = asxds;

            if (asxds.Tables.Count > 0)
            {
                var asxView = new DataView(asxds.Tables[0]) { Sort = "TradeDate DESC" };
                if (!isCommand)
                    ASXTransGrid.DataSource = asxView;
            }

            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        #region Adjustment Grid

        protected void PresentationGridItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;

                if ("ASXDetails".Equals(e.Item.OwnerTableView.Name))
                {
                    var orgCM = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                    var organisationListingDS = new OrganisationListingDS
                                                    {
                                                        OrganisationListingOperationType = OrganisationListingOperationType.AdvisersBasicList
                                                    };
                    if (orgCM != null) orgCM.GetData(organisationListingDS);
                    var tranType = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("TransactionTypeEditor"));

                    tranType.ComboBoxControl.Filter = RadComboBoxFilter.Contains;
                    tranType.DataSource = HoldingTransactions.TransactionTypes;
                    tranType.DataValueField = "Type";
                    tranType.DataTextField = "TransactionDescription";
                    tranType.DataBind();
                    tranType.ComboBoxControl.Text = String.Empty;
                    tranType.SelectedText = String.Empty;

                    if (!(e.Item.DataItem is GridInsertionObject))
                    {
                        var value = (string)((DataRowView)(e.Item.DataItem)).Row["TransactionType"];
                        if (value != string.Empty)
                        {
                            RadComboBoxItem item = tranType.ComboBoxControl.FindItemByText(value);
                            if (item != null)
                                item.Selected = true;
                        }
                    }
                }
            }



            e.Canceled = true;
        }

        protected void PresentationGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (lblASCCID.Text != string.Empty && lblTranIDPatent.Text != string.Empty)
            {
                IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(lblASCCID.Text));
                var asxAdjustmentTransactionsDS = new ASXAdjustmentTransactionsDS { ParentTransactionID = new Guid(lblTranIDPatent.Text) };
                clientData.GetData(asxAdjustmentTransactionsDS);

                PresentationGrid.DataSource = asxAdjustmentTransactionsDS.Tables[ASXAdjustmentTransactionsDS.ASXADJTABLE];
            }
        }

        protected void PresentationGridItemCommand(object sender, GridCommandEventArgs e)
        {
            ASXPnl.Visible = false;
            pnlASXAdjustments.Visible = true;

            if (e.CommandName.ToLower() == "update")
            {
                if ("ASXDetails".Equals(e.Item.OwnerTableView.Name))
                {
                    PopulateDatasetWithDataAndUpdate(e);
                }
            }
            else if (e.CommandName.ToLower() == "delete")
            {
                PopulateDatasetWithDataAndUpdate(e);
            }
        }

        protected void PresentationGridInsertCommand(object source, GridCommandEventArgs e)
        {
            ASXPnl.Visible = false;
            pnlASXAdjustments.Visible = true;

            if ("ASXDetails".Equals(e.Item.OwnerTableView.Name))
            {
                PopulateDatasetWithDataAndSave(e);
            }
        }

        private void PopulateDatasetWithDataAndSave(GridCommandEventArgs e)
        {
            var editForm = (GridEditFormItem)e.Item;

            var unitPrice = (TextBox)editForm["UnitPrice"].Controls[0];
            var units = (TextBox)editForm["Units"].Controls[0];
            var brokerageAmount = (TextBox)editForm["BrokerageAmount"].Controls[0];
            var transactionTypeEditor = (RadComboBox)editForm["TransactionTypeEditor"].Controls[0];

            IBrokerManagedComponent ibmc = UMABroker.GetBMCInstance(new Guid(lblASCCID.Text));
            var asxAdjustmentTransactionsDS = new ASXAdjustmentTransactionsDS { ParentTransactionID = new Guid(lblTranIDPatent.Text) };
            ibmc.GetData(asxAdjustmentTransactionsDS);

            DataRow row = asxAdjustmentTransactionsDS.Tables[ASXAdjustmentTransactionsDS.ASXADJTABLE].NewRow();
            row[ASXAdjustmentTransactionsDS.TRANID] = Guid.NewGuid();
            UpdateRow(e, unitPrice, units, brokerageAmount, transactionTypeEditor, asxAdjustmentTransactionsDS, row);
            asxAdjustmentTransactionsDS.Tables[ASXAdjustmentTransactionsDS.ASXADJTABLE].Rows.Add(row);
            asxAdjustmentTransactionsDS.DataSetOperationType = DataSetOperationType.UpdateBulk;
            SaveData(lblASCCID.Text, asxAdjustmentTransactionsDS);

            e.Item.Edit = false;
            e.Canceled = true;
            PresentationGrid.Rebind();
        }

        private void PopulateDatasetWithDataAndUpdate(GridCommandEventArgs e)
        {
            IBrokerManagedComponent ibmc = UMABroker.GetBMCInstance(new Guid(lblASCCID.Text));
            var asxAdjustmentTransactionsDS = new ASXAdjustmentTransactionsDS { ParentTransactionID = new Guid(lblTranIDPatent.Text) };
            ibmc.GetData(asxAdjustmentTransactionsDS);

            if (e.CommandName.ToLower() == "delete" || e.CommandName.ToLower() == "update")
            {
                DataRow row;
                if (e.CommandName.ToLower() == "delete")
                {
                    var dataForm = (GridDataItem)e.Item;
                    string transactionIDString = dataForm["TransactionID"].Text;
                    row = asxAdjustmentTransactionsDS.Tables[ASXAdjustmentTransactionsDS.ASXADJTABLE].Select().Where(drow => drow[ASXAdjustmentTransactionsDS.TRANID].ToString() == transactionIDString).FirstOrDefault();
                    if (row != null)
                        asxAdjustmentTransactionsDS.Tables[ASXAdjustmentTransactionsDS.ASXADJTABLE].Rows.Remove(row);
                }
                else
                {
                    var editForm = (GridEditFormItem)e.Item;
                    var transactionID = (TextBox)editForm["TransactionID"].Controls[0];
                    row = asxAdjustmentTransactionsDS.Tables[ASXAdjustmentTransactionsDS.ASXADJTABLE].Select().Where(drow => drow[ASXAdjustmentTransactionsDS.TRANID].ToString() == transactionID.Text).FirstOrDefault();
                    if (row != null)
                    {
                        var unitPrice = (TextBox)editForm["UnitPrice"].Controls[0];
                        var units = (TextBox)editForm["Units"].Controls[0];
                        var brokerageAmount = (TextBox)editForm["BrokerageAmount"].Controls[0];
                        var transactionTypeEditor = (RadComboBox)editForm["TransactionTypeEditor"].Controls[0];

                        UpdateRow(e, unitPrice, units, brokerageAmount, transactionTypeEditor, asxAdjustmentTransactionsDS, row);
                    }
                }
                asxAdjustmentTransactionsDS.DataSetOperationType = DataSetOperationType.UpdateBulk;
                SaveData(lblASCCID.Text, asxAdjustmentTransactionsDS);

                e.Item.Edit = false;
                e.Canceled = true;
                PresentationGrid.Rebind();
            }
        }

        private static void UpdateRow(GridCommandEventArgs e, TextBox unitPrice, TextBox units, TextBox brokerageAmount, RadComboBox transactionTypeEditor, ASXAdjustmentTransactionsDS asxAdjustmentTransactionsDS, DataRow row)
        {
            row[ASXAdjustmentTransactionsDS.PARENTTRANID] = asxAdjustmentTransactionsDS.ParentTransactionID;
            row[ASXAdjustmentTransactionsDS.INVESTCODE] = String.Empty;
            if (transactionTypeEditor.SelectedItem != null)
            {
                row[ASXAdjustmentTransactionsDS.TYPE] = transactionTypeEditor.SelectedItem.Value;
                row[ASXAdjustmentTransactionsDS.TRANSACTIONTYPE] = transactionTypeEditor.SelectedItem.Text;
            }
            else
            {
                row[ASXAdjustmentTransactionsDS.TYPE] = String.Empty;
                row[ASXAdjustmentTransactionsDS.TRANSACTIONTYPE] = String.Empty;
            }

            if (((RadDatePicker)(((GridEditableItem)(e.Item))["TradeDate"].Controls[0])).SelectedDate.Value != null)
                row[ASXAdjustmentTransactionsDS.TRADEDATE] = ((RadDatePicker)(((GridEditableItem)(e.Item))["TradeDate"].Controls[0])).SelectedDate.Value;
            else
                row[ASXAdjustmentTransactionsDS.TRADEDATE] = DateTime.Now;

            row[ASXAdjustmentTransactionsDS.UNITS] = units.Text == string.Empty ? 0 : Convert.ToDecimal(units.Text);
            row[ASXAdjustmentTransactionsDS.BROKERAGEAMT] = brokerageAmount.Text == string.Empty ? 0 : Convert.ToDecimal(brokerageAmount.Text);
            row[ASXAdjustmentTransactionsDS.UNITPRICE] = unitPrice.Text == string.Empty ? 0 : Convert.ToDecimal(unitPrice.Text);
        }

        protected void PresentationGridItemCreated(object sender, GridItemEventArgs e)
        {
            var item = e.Item as GridEditableItem;
            if (item != null && item.IsInEditMode && item.ItemIndex != -1)
            {

            }
        }

        private void DisplayMessage(string text)
        {
            PresentationGrid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
        }

        #endregion

        #region ASX Grid

        protected void ASXTransGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void ASXTransGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "delete":
                    {
                        var dataItem = (GridDataItem)e.Item;
                        e.Item.Edit = false;
                        e.Canceled = true;
                        var manualTransactionDetailsDS = new SecTransactionDetailsDS { DataSetOperationType = DataSetOperationType.DeletSingle };
                        DataRow row = manualTransactionDetailsDS.Tables[SecTransactionDetailsDS.SECTRANSDETAILSTABLE].NewRow();
                        row[SecTransactionDetailsDS.ID] = dataItem["TranstactionUniqueID"].Text;

                        manualTransactionDetailsDS.Tables[SecTransactionDetailsDS.SECTRANSDETAILSTABLE].Rows.Add(row);

                        SaveData(dataItem["ASXCID"].Text, manualTransactionDetailsDS);
                        ASXTransGrid.Rebind();
                        break;
                    }
                case "update":
                    {
                        GridEditFormItem EditForm = (GridEditFormItem)e.Item;

                        RadComboBox comboAccList = (RadComboBox)EditForm["DDLAccList"].Controls[0];
                        RadComboBox comboTranType = (RadComboBox)EditForm["DDLTranType"].Controls[0];
                        RadComboBox comboSecLit = (RadComboBox)EditForm["DDLSecList"].Controls[0];

                        if (comboAccList.SelectedItem != null)
                            lblAsxID.Text = comboAccList.SelectedItem.Value;

                        if (lblAsxID.Text != ((TextBox)(((GridEditableItem)(e.Item))["ASXCID"].Controls[0])).Text)
                        {
                            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(((TextBox)(((GridEditableItem)(e.Item))["ASXCID"].Controls[0])).Text));
                            var asxAdjustmentTransactionsDS = new ASXAdjustmentTransactionsDS { ParentTransactionID = new Guid(((TextBox)(((GridEditableItem)(e.Item))["TranstactionUniqueID"].Controls[0])).Text) };
                            clientData.GetData(asxAdjustmentTransactionsDS);

                            var manualTransactionDetailsDS = new SecTransactionDetailsDS { DataSetOperationType = DataSetOperationType.DeletSingle };
                            DataRow row = manualTransactionDetailsDS.Tables[SecTransactionDetailsDS.SECTRANSDETAILSTABLE].NewRow();
                            row[SecTransactionDetailsDS.ID] = ((TextBox)(((GridEditableItem)(e.Item))["TranstactionUniqueID"].Controls[0])).Text;
                            manualTransactionDetailsDS.Tables[SecTransactionDetailsDS.SECTRANSDETAILSTABLE].Rows.Add(row);
                            SaveData(((TextBox)(((GridEditableItem)(e.Item))["ASXCID"].Controls[0])).Text, manualTransactionDetailsDS);

                            Guid parentID = AddUpdateTransactions(e, comboTranType, comboSecLit, DataSetOperationType.NewSingle, lblAsxID.Text);
                            asxAdjustmentTransactionsDS.DataSetOperationType = DataSetOperationType.UpdateBulk;
                            asxAdjustmentTransactionsDS.ParentTransactionID = parentID;
                            SaveData(lblAsxID.Text, asxAdjustmentTransactionsDS);
                        }
                        else
                            AddUpdateTransactions(e, comboTranType, comboSecLit, DataSetOperationType.UpdateSingle, lblAsxID.Text);

                        ASXTransGrid.Rebind();
                        e.Canceled = true;
                        e.Item.Edit = false;
                        this.GetData(true);
                        this.ASXTransGrid.Rebind();
                        break;
                    }
                case "asxadjustment":
                    {
                        var dataItem = (GridDataItem)e.Item;

                        GetData(true);
                        string id = e.CommandArgument.ToString();
                        lblTranIDPatent.Text = id;
                        lblASCCID.Text = dataItem["ASXCID"].Text;
                        ASXPnl.Visible = false;
                        pnlASXAdjustments.Visible = true;
                        var view = new DataView(PresentationData.Tables["DesktopBroker Transactions"]) { RowFilter = "TranstactionUniqueID='" + id + "'" };
                        ASXGridParent.DataSource = view.ToTable();
                        ASXGridParent.DataBind();

                        IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(lblASCCID.Text));
                        var asxAdjustmentTransactionsDataSet = new ASXAdjustmentTransactionsDS { ParentTransactionID = new Guid(id) };
                        clientData.GetData(asxAdjustmentTransactionsDataSet);

                        PresentationGrid.DataSource = asxAdjustmentTransactionsDataSet.Tables[ASXAdjustmentTransactionsDS.ASXADJTABLE];
                        PresentationGrid.DataBind();
                        break;
                    }
            }
        }

        private Guid AddUpdateTransactions(GridCommandEventArgs e, RadComboBox comboTranType, RadComboBox comboSecLit, DataSetOperationType dataSetOperationType, string asxCID)
        {
            var asxAdjustmentTransactionsDS = new SecTransactionDetailsDS();
            DataRow row = asxAdjustmentTransactionsDS.Tables[SecTransactionDetailsDS.SECTRANSDETAILSTABLE].NewRow();

            asxAdjustmentTransactionsDS.DataSetOperationType = dataSetOperationType;

            if (comboSecLit.SelectedItem != null)
                row[SecTransactionDetailsDS.INVESTCODE] = comboSecLit.SelectedItem.Value;

            row[SecTransactionDetailsDS.ID] = ((TextBox)(((GridEditableItem)(e.Item))["TranstactionUniqueID"].Controls[0])).Text;

            if (((RadDatePicker)(((GridEditableItem)(e.Item))["TradeDate"].Controls[0])).SelectedDate.Value != null)
                row[SecTransactionDetailsDS.TRANDATE] = ((RadDatePicker)(((GridEditableItem)(e.Item))["TradeDate"].Controls[0])).SelectedDate.Value;

            if (((RadDatePicker)(((GridEditableItem)(e.Item))["SettlementDate"].Controls[0])).SelectedDate.Value != null)
                row[SecTransactionDetailsDS.SETTLEDATE] = ((RadDatePicker)(((GridEditableItem)(e.Item))["SettlementDate"].Controls[0])).SelectedDate.Value;

            row[SecTransactionDetailsDS.NARRATIVE] = ((TextBox)(((GridEditableItem)(e.Item))["Narrative"].Controls[0])).Text;

            row[SecTransactionDetailsDS.UNITSONHAND] = ((RadNumericTextBox)(((GridEditableItem)(e.Item))["Units"].Controls[0])).Text;
            row[SecTransactionDetailsDS.BROKERAGE] = ((RadNumericTextBox)(((GridEditableItem)(e.Item))["BrokerageAmount"].Controls[0])).Text;
            row[SecTransactionDetailsDS.TRANTYPE] = comboTranType.Text;
            row[SecTransactionDetailsDS.UNITSPRICE] = ((RadNumericTextBox)(((GridEditableItem)(e.Item))["UnitPrice"].Controls[0])).Text; ;

            asxAdjustmentTransactionsDS.Tables[SecTransactionDetailsDS.SECTRANSDETAILSTABLE].Rows.Add(row);
            SaveData(asxCID, asxAdjustmentTransactionsDS);

            return asxAdjustmentTransactionsDS.ParentID;
        }

        private void SetEditingData(GridEditManager editMan, ref GridDropDownListColumnEditor ddlTranType, ref GridDropDownListColumnEditor ddlAccountList, ref GridDropDownListColumnEditor ddlSec)
        {
            var ibmc = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            ddlSec = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("DDLSecList"));
            RadComboBox ddlSecRadCombo = ddlSec.ComboBoxControl;
            ddlSecRadCombo.Filter = RadComboBoxFilter.Contains;
            ddlSecRadCombo.DataSource = ibmc.Securities.OrderBy(secEnt => secEnt.CompanyName);
            ddlSecRadCombo.DataValueField = "AsxCode";
            ddlSecRadCombo.DataTextField = "CompanyName";
            ddlSecRadCombo.DataBind();

            RadComboBoxItem ddlSecRadComboDefaultItem = new RadComboBoxItem("", "");
            ddlSecRadComboDefaultItem.Selected = true;
            ddlSecRadCombo.Items.Add(ddlSecRadComboDefaultItem);

            UMABroker.ReleaseBrokerManagedComponent(ibmc);

            ddlAccountList = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("DDLAccList"));
            RadComboBox ddlAccountListRadCombo = ddlAccountList.ComboBoxControl;
            ddlAccountListRadCombo.Filter = RadComboBoxFilter.Contains;
            ddlAccountListRadCombo.DataSource = PresentationData.Tables["Accounts"];
            ddlAccountListRadCombo.DataValueField = "ID";
            ddlAccountListRadCombo.DataTextField = "AccountNo";
            ddlAccountListRadCombo.DataBind();

            RadComboBoxItem ddlAccountListRadComboDefaultItem = new RadComboBoxItem("", "");
            ddlAccountListRadComboDefaultItem.Selected = true;
            ddlAccountListRadCombo.Items.Add(ddlAccountListRadComboDefaultItem);
            
            ddlTranType = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("DDLTranType"));
            RadComboBox ddlTranTypeRadCombo = ddlTranType.ComboBoxControl;
            ddlTranTypeRadCombo.Filter = RadComboBoxFilter.Contains;
            RadComboBoxItem ddlTranTypeRadComboDefaultItem = new RadComboBoxItem("", "");
            ddlTranTypeRadComboDefaultItem.Selected = true;
            ddlTranTypeRadCombo.Items.Add(ddlTranTypeRadComboDefaultItem);
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Adjustment Down", "AJ"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Adjustment Up", "RJ"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Application", "AP"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Bonus Issue", "AM"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Buy / Purchase", "AN"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Commutation", "RU"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Contribution", "AC"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Contribution Tax", "RL"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Deposit", "AD"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Expense", "EX"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Instalment", "LM"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Insurance Premium", "RH"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Lodgement", "AL"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Lump Sum Tax", "RK"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Manager Fee", "MA"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Opening (Initial) Balance", "IN"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("PAYG", "RY"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Pension Payment", "PR"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Pension Payment – Income Stream", "RR"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Purchase Reinvestment", "VB"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Reversal - Redemption / Withdrawal / Fee / Tax", "VA"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Rights Issue", "AE"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Sale", "RA"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Surcharge Tax", "ST"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Switch In", "AS"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Switch Out", "RS"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Tax", "RX"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Tax Rebate", "AR"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Transfer In", "AT"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Transfer Out", "RT"));
            ddlTranTypeRadCombo.Items.Add(new RadComboBoxItem("Withdrawal", "RW"));
        }

        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) { }

        protected void ASXTransGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            GridEditFormInsertItem EditForm = (GridEditFormInsertItem)e.Item;

            Hashtable values = new Hashtable();
            EditForm.ExtractValues(values);

            RadComboBox comboAccList = (RadComboBox)EditForm["DDLAccList"].Controls[0];
            RadComboBox comboTranType = (RadComboBox)EditForm["DDLTranType"].Controls[0];
            RadComboBox comboSecLit = (RadComboBox)EditForm["DDLSecList"].Controls[0];

            if (comboAccList.SelectedItem != null)
                lblAsxID.Text = comboAccList.SelectedItem.Value;

            var asxAdjustmentTransactionsDS = new SecTransactionDetailsDS();
            DataRow row = asxAdjustmentTransactionsDS.Tables[SecTransactionDetailsDS.SECTRANSDETAILSTABLE].NewRow();

            asxAdjustmentTransactionsDS.DataSetOperationType = DataSetOperationType.NewSingle;

            if (comboSecLit.SelectedItem != null)
                row[SecTransactionDetailsDS.INVESTCODE] = comboSecLit.SelectedItem.Value;

            row[SecTransactionDetailsDS.ID] = Guid.NewGuid().ToString();

            if (((RadDatePicker)(((GridEditableItem)(e.Item))["TradeDate"].Controls[0])).SelectedDate.Value != null)
                row[SecTransactionDetailsDS.TRANDATE] = ((RadDatePicker)(((GridEditableItem)(e.Item))["TradeDate"].Controls[0])).SelectedDate.Value;

            if (((RadDatePicker)(((GridEditableItem)(e.Item))["SettlementDate"].Controls[0])).SelectedDate.Value != null)
                row[SecTransactionDetailsDS.SETTLEDATE] = ((RadDatePicker)(((GridEditableItem)(e.Item))["SettlementDate"].Controls[0])).SelectedDate.Value;

            row[SecTransactionDetailsDS.NARRATIVE] = ((TextBox)(((GridEditableItem)(e.Item))["Narrative"].Controls[0])).Text; 
            row[SecTransactionDetailsDS.ACCOUNTNO] = comboAccList.SelectedItem.Text;
            row[SecTransactionDetailsDS.UNITSONHAND] = ((RadNumericTextBox)(((GridEditableItem)(e.Item))["Units"].Controls[0])).Text;
            row[SecTransactionDetailsDS.BROKERAGE] = ((RadNumericTextBox)(((GridEditableItem)(e.Item))["BrokerageAmount"].Controls[0])).Text;
            row[SecTransactionDetailsDS.TRANTYPE] = comboTranType.Text;
            row[SecTransactionDetailsDS.UNITSPRICE] = ((RadNumericTextBox)(((GridEditableItem)(e.Item))["UnitPrice"].Controls[0])).Text; ;

            asxAdjustmentTransactionsDS.Tables[SecTransactionDetailsDS.SECTRANSDETAILSTABLE].Rows.Add(row);
            if (comboSecLit.SelectedItem != null && comboTranType != null && comboAccList.SelectedItem != null)
                SaveData(lblAsxID.Text, asxAdjustmentTransactionsDS);


            ASXTransGrid.Rebind();

            e.Canceled = true;
            e.Item.Edit = false;
            this.GetData(true);
            this.ASXTransGrid.Rebind();
        }

        protected void ASXTransGrid_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;
                GridDropDownListColumnEditor ddlTranType = null;
                GridDropDownListColumnEditor ddlAccountList = null;
                GridDropDownListColumnEditor ddlSec = null;


                SetEditingData(editMan, ref ddlTranType, ref ddlAccountList, ref ddlSec);

                string tranTxt = String.Empty;
                string accountTxt = String.Empty;
                string secTxt = String.Empty;
                string accountCID = String.Empty;

                if (((Telerik.Web.UI.GridEditFormItem)(e.Item)).ParentItem != null)
                {
                    tranTxt = ((Telerik.Web.UI.GridEditFormItem)(e.Item)).ParentItem["TransactionType"].Text;
                    accountTxt = ((Telerik.Web.UI.GridEditFormItem)(e.Item)).ParentItem["AccountNoText"].Text;
                    secTxt = ((Telerik.Web.UI.GridEditFormItem)(e.Item)).ParentItem["InvestmentCode"].Text;
                }

                if (tranTxt != string.Empty)
                    ddlTranType.ComboBoxControl.Items.FindItemByText(tranTxt).Selected = true;

                if (accountTxt != string.Empty)
                    ddlAccountList.ComboBoxControl.Items.FindItemByText(accountTxt).Selected = true;

                if (secTxt != string.Empty)
                    ddlSec.ComboBoxControl.Items.FindItemByValue(secTxt).Selected = true;

             }

            else if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                string transactionType = item["TransactionType"].Text;

                if (transactionType == "Transfer In" || transactionType == "Transfer Out")
                {
                    var linkDesc = (LinkButton)item["AccountNo"].FindControl("linkDesc");
                    linkDesc.Visible = true;
                    item["AccountNo"].FindControl("lblDesc").Visible = false;
                }

                var divstatus = item["DividendStatus"];

                if (divstatus.Text == TransectionStatus.Matched.ToString())
                {
                    var divID = item["DividendID"].Text;
                    var contents = CreateDividendTable(divID);
                    divstatus.ToolTip = contents;
                }

            }
        }

        private string CreateDividendTable(string divID)
        {
            DataRow[] drs = PresentationData.Tables["Income Transactions"].Select(string.Format("ID='{0}'", divID));
            string contents = string.Empty;

            if (drs.Length > 0)
            {
                string recordDate = string.Empty;
                string paymentDate = string.Empty;
                string balanceDate = string.Empty;
                string booksCloseDate = string.Empty;

                string unitsOnHand = string.Empty;
                string centsPerShare = string.Empty;
                string investmentAmount = string.Empty;
                string paidDividend = string.Empty; 
                string interestPaid = string.Empty;
                string interestRate = string.Empty; 
                string frankingCredits = string.Empty;
                string frankedAmount = string.Empty; 
                string unfrankedAmount = string.Empty; 
                string totalFrankingCredits = string.Empty;

                if (drs[0]["RecordDate"] != DBNull.Value)
                {
                    recordDate = ((DateTime)drs[0]["RecordDate"]).ToString("dd/MM/yyyy");
                }

                if (drs[0]["PaymentDate"] != DBNull.Value)
                {
                    paymentDate = ((DateTime)drs[0]["PaymentDate"]).ToString("dd/MM/yyyy");
                }

                if (drs[0]["BalanceDate"] != DBNull.Value)
                {
                    balanceDate = ((DateTime)drs[0]["BalanceDate"]).ToString("dd/MM/yyyy");
                }

                if (drs[0]["BooksCloseDate"] != DBNull.Value)
                {
                    booksCloseDate = ((DateTime)drs[0]["BooksCloseDate"]).ToString("dd/MM/yyyy");
                }

                if (drs[0]["UnitsOnHand"] != DBNull.Value)
                {
                    unitsOnHand = decimal.Round((decimal)drs[0]["UnitsOnHand"], 4).ToString();
                }
                if (drs[0]["CentsPerShare"] != DBNull.Value)
                {
                    centsPerShare = decimal.Round((decimal)drs[0]["CentsPerShare"], 4).ToString();
                }
                if (drs[0]["InvestmentAmount"] != DBNull.Value)
                {
                    investmentAmount = decimal.Round((decimal)drs[0]["InvestmentAmount"], 6).ToString();
                }

                if (drs[0]["PaidDividend"] != DBNull.Value)
                {
                    paidDividend = decimal.Round((decimal)drs[0]["PaidDividend"], 6).ToString();
                }


                if (drs[0]["InterestPaid"] != DBNull.Value)
                {
                    interestPaid = decimal.Round((decimal)drs[0]["InterestPaid"], 6).ToString();
                }

                if (drs[0]["InterestRate"] != DBNull.Value)
                {
                    interestRate = decimal.Round((decimal)drs[0]["InterestRate"], 6).ToString();
                }

                if (drs[0]["FrankingCredits"] != DBNull.Value)
                {

                    frankingCredits = Math.Round((double)drs[0]["FrankingCredits"], 3).ToString();
                }


                if (drs[0]["FrankedAmount"] != DBNull.Value)
                {
                    frankedAmount = Math.Round((double)drs[0]["FrankedAmount"], 3).ToString();
                }

                if (drs[0]["UnfrankedAmount"] != DBNull.Value)
                {
                    unfrankedAmount = Math.Round((double)drs[0]["UnfrankedAmount"], 3).ToString();
                }

                if (drs[0]["TotalFrankingCredits"] != DBNull.Value)
                {
                    totalFrankingCredits = Math.Round((double)drs[0]["TotalFrankingCredits"], 3).ToString();
                }
                contents = "<table>" +
                                 "<tr><td><u>Investment Code:</u></td><td>" + drs[0]["InvestmentCode"] + "</td></tr>" +
                                 "<tr><td><u>Product Type:</u></td><td>" + drs[0]["ProductType"] + "</td></tr>" +
                                 "<tr><td><u>Transaction Type:</u></td><td>" + drs[0]["TransactionType"] + "</td></tr>" +
                                 "<tr><td><u>Record Date:</u></td><td>" + recordDate + "</td></tr>" +
                                 "<tr><td><u>Payment Date:</u></td><td>" + paymentDate + "</td></tr>" +
                                 "<tr><td><u>Units On Hand:</u></td><td>" + unitsOnHand + "</td></tr>" +
                                 "<tr><td><u>Cents Per Share:</u></td><td>" + centsPerShare + "</td></tr>" +
                                 "<tr><td><u>Investment Amount:</u></td><td>" + investmentAmount + "</td></tr>" +
                                 "<tr><td><u>Paid Dividend:</u></td><td>" + paidDividend + "</td></tr>" +
                                 "<tr><td><u>Interest Paid:</u></td><td>" + interestPaid + "</td></tr>" +
                                 "<tr><td><u>Interest Rate:</u></td><td>" + interestRate + "</td></tr>" +
                                 "<tr><td><u>Franking Credits:</u></td><td>" + frankingCredits + "</td></tr>" +
                                  "<tr><td><u>Franked Amount:</u></td><td>" + frankedAmount + "</td></tr>" +
                                 "<tr><td><u>UnfrankedAmount:</u></td><td>" + unfrankedAmount + "</td></tr>" +
                                 "<tr><td><u>Total Franking Credits:</u></td><td>" + totalFrankingCredits + "</td></tr>" +

                                 "<tr><td><u>Balance Date:</u></td><td>" + balanceDate + "</td></tr>" +
                                 "<tr><td><u>Books Close Date:</u></td><td>" + booksCloseDate + "</td></tr>" +
                                 "<tr><td><u>Dividend Type:</u></td><td>" + drs[0]["DividendType"] + "</td></tr>" +
                                 "<tr><td><u>Currency:</u></td><td>" + drs[0]["Currency"] + "</td></tr>" +
                                 "<tr><td><u>Status:</u></td><td>" + drs[0]["Status"] + "</td></tr>" +
                                 "</table>";
            }
            return contents;
        }

        #endregion

    }
}
