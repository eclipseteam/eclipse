﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.Administration;
using eclipseonlineweb.ClientViews.WebControls;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Security;
using System.Collections;
using System.Collections.Generic;
using Oritax.TaxSimp;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Common.UMAFee;
using System.Web.UI.WebControls;

namespace eclipseonlineweb
{
    public partial class BankTransactionsGroup : UMABasePage
    {
        protected void UpdateTransaction(object sender, EventArgs e)
        {
            var bankTransactionDetailsDS = new BankTransactionDetailsDS { DataSetOperationType = DataSetOperationType.UpdateSingle };
            DataRow row = bankTransactionDetailsDS.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].NewRow();
            row[BankTransactionDetailsDS.ID] = lblTranID.Text;
            row[BankTransactionDetailsDS.IMPORTTRANTYPE] = C1ComboBoxImport.Text;
            row[BankTransactionDetailsDS.CATTRANTYPE] = C1ComboBoxCat.Text;
            row[BankTransactionDetailsDS.SYSTRANTYPE] = C1ComboBoxSystem.Text;
            row[BankTransactionDetailsDS.AMOUNT] = Amount.Value;
            row[BankTransactionDetailsDS.ADJUSTMENT] = Adjustment.Value;
            row[BankTransactionDetailsDS.COMMENT] = textComments.Text;
            if (transactionDate.Date.HasValue)
                row[BankTransactionDetailsDS.TRANDATE] = transactionDate.Date;
            bankTransactionDetailsDS.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows.Add(row);

            SaveData(lblBankCID.Text, bankTransactionDetailsDS);
            PresentationGrid.Rebind();
        }

        protected void AddTransaction(object sender, EventArgs e)
        {
            var bankTransactionDetailsDS = new BankTransactionDetailsDS { DataSetOperationType = DataSetOperationType.NewSingle };
            DataRow row = bankTransactionDetailsDS.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].NewRow();
            row[BankTransactionDetailsDS.ID] = new Guid(C1ComboBoxBankAccountList.SelectedValue);
            row[BankTransactionDetailsDS.IMPORTTRANTYPE] = C1ComboBoxImport.Text;
            row[BankTransactionDetailsDS.CATTRANTYPE] = C1ComboBoxCat.Text;
            row[BankTransactionDetailsDS.SYSTRANTYPE] = C1ComboBoxSystem.Text;
            row[BankTransactionDetailsDS.AMOUNT] = Amount.Value;
            row[BankTransactionDetailsDS.ADJUSTMENT] = Adjustment.Value;
            row[BankTransactionDetailsDS.COMMENT] = textComments.Text;

            bankTransactionDetailsDS.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows.Add(row);

            SaveData(C1ComboBoxBankAccountList.SelectedValue, bankTransactionDetailsDS);
            PresentationGrid.Rebind();
        }

        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];
            ScriptManager sm = ScriptManager.GetCurrent(Page);
            
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
            {
                btnAddtran.Visible = false;
                PresentationGrid.Columns.FindByUniqueNameSafe("DeleteColumn").Visible = false;
                PresentationGrid.Columns.FindByUniqueNameSafe("EditColumn").Visible = false;
                PresentationGrid.Columns.FindByUniqueNameSafe("DividendStatus").Visible = false;
                PresentationGrid.MasterTableView.CommandItemSettings.ShowAddNewRecordButton = false; 
            }

            if (sm != null)
            {
                sm.RegisterPostBackControl(btnReport);
            }

            UMABroker.ReleaseBrokerManagedComponent(objUser);
        }

        private void GetData(bool isCommand = false)
        {
            cid = Request.QueryString["ins"];
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(cid));
            var ds = new BankTransactionDS();
            clientData.GetData(ds);
            PresentationData = ds;

            if (ds.Tables["Cash Transactions"] != null)
            {
                var summaryView = new DataView(ds.Tables["Cash Transactions"]) { Sort = "TransactionDate DESC" };
                if (!isCommand)
                    PresentationGrid.DataSource = summaryView;
            }
            if (PresentationData.Tables[BankTransactionDS.BANKACCOUNTLIST] != null)
            {
                C1ComboBoxBankAccountList.DataSource = PresentationData.Tables[BankTransactionDS.BANKACCOUNTLIST];
                C1ComboBoxBankAccountList.DataBind();
            }
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void BtnReportClick(object sender, EventArgs e)
        {
            BulkTransactionsOutput(this.cid, this.ClientHeaderInfo1.ClientName);
        }

        protected void BtnAddTran(object sender, EventArgs e)
        {
            C1ComboBoxBankAccountList.Visible = true;
            btnAdd.Visible = true;

            btnUpdate.Visible = false;
            lblBankDetails.Visible = false;
            transactionDate.Date = DateTime.Today;
            C1ComboBoxImport.SelectedValue = string.Empty;
            C1ComboBoxImport.Text = string.Empty;
            C1ComboBoxCat.SelectedValue = string.Empty;
            C1ComboBoxCat.Text = string.Empty;
            C1ComboBoxSystem.SelectedValue = string.Empty;
            C1ComboBoxSystem.Text = string.Empty;
            lblTranID.Text = string.Empty;
            lblBankCID.Text = Guid.NewGuid().ToString();
            textComments.Text = string.Empty;
            lblBankDetails.Text = string.Empty;
            Amount.Value = 0;
            TotalAmount.Value = 0;
            Adjustment.Value = 0;

            ModalPopupExtenderEditTran.Show();
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;
                GridDropDownListColumnEditor ddlImportTranType = null;
                GridDropDownListColumnEditor ddlSysTranType = null;
                GridDropDownListColumnEditor ddlCatTranType = null;
                GridDropDownListColumnEditor ddlAccountListType = null;

                if ("Banks".Equals(e.Item.OwnerTableView.Name))
                {
                    SetEditingData(editMan, ref ddlImportTranType, ref ddlSysTranType, ref ddlCatTranType, ref ddlAccountListType);

                    string impTranType = String.Empty;
                    string sysTranType = String.Empty;
                    string catTranType = String.Empty;
                    string accountCID = String.Empty;

                    var pItem = ((GridEditFormItem)(e.Item)).ParentItem;
                    if (pItem != null)
                    {
                        impTranType = pItem["ImportTransactionType"].Text == "&nbsp;" ? "" : pItem["ImportTransactionType"].Text;
                        sysTranType = pItem["SystemTransactionType"].Text == "&nbsp;" ? "" : pItem["SystemTransactionType"].Text;
                        catTranType = pItem["Category"].Text == "&nbsp;" ? "" : pItem["Category"].Text;
                        accountCID = pItem["BankCID"].Text == "&nbsp;" ? "" : pItem["BankCID"].Text;
                    }

                    if (impTranType != string.Empty)
                        ddlImportTranType.ComboBoxControl.Items.FindItemByText(impTranType).Selected = true;

                    if (sysTranType != string.Empty)
                        ddlSysTranType.ComboBoxControl.Items.FindItemByText(sysTranType).Selected = true;

                    if (catTranType != string.Empty)
                        ddlCatTranType.ComboBoxControl.Items.FindItemByText(catTranType).Selected = true;

                    if (accountCID != string.Empty)
                    {
                        ddlAccountListType.ComboBoxControl.Enabled = false;
                        ddlAccountListType.ComboBoxControl.Items.FindItemByValue(accountCID).Selected = true;
                    }
                }
            }
            else if (e.Item is GridDataItem)
            {
                var item = (GridDataItem)e.Item;
                var divstatus = item["DividendStatus"];

                if (divstatus.Text == TransectionStatus.Matched.ToString())
                {
                    var divID = item["DividendID"].Text;
                    var contents = CreateDividendTable(divID);
                    divstatus.ToolTip = contents;
                }
            }
        }

        private string CreateDividendTable(string divID)
        {
            DataRow[] drs = PresentationData.Tables["Income Transactions"].Select(string.Format("ID='{0}'", divID));
            string contents = string.Empty;

            if (drs.Length > 0)
            {
                string recordDate = string.Empty;
                string paymentDate = string.Empty;
                string balanceDate = string.Empty;
                string booksCloseDate = string.Empty;

                string unitsOnHand = string.Empty;
                string centsPerShare = string.Empty;
                string investmentAmount = string.Empty;
                string paidDividend = string.Empty;
                string interestPaid = string.Empty;
                string interestRate = string.Empty;
                string frankingCredits = string.Empty;
                string frankedAmount = string.Empty;
                string unfrankedAmount = string.Empty;
                string totalFrankingCredits = string.Empty;

                if (drs[0]["RecordDate"] != DBNull.Value)
                {
                    recordDate = ((DateTime)drs[0]["RecordDate"]).ToString("dd/MM/yyyy");
                }

                if (drs[0]["PaymentDate"] != DBNull.Value)
                {
                    paymentDate = ((DateTime)drs[0]["PaymentDate"]).ToString("dd/MM/yyyy");
                }

                if (drs[0]["BalanceDate"] != DBNull.Value)
                {
                    balanceDate = ((DateTime)drs[0]["BalanceDate"]).ToString("dd/MM/yyyy");
                }

                if (drs[0]["BooksCloseDate"] != DBNull.Value)
                {
                    booksCloseDate = ((DateTime)drs[0]["BooksCloseDate"]).ToString("dd/MM/yyyy");
                }

                if (drs[0]["UnitsOnHand"] != DBNull.Value)
                {
                    unitsOnHand = decimal.Round((decimal)drs[0]["UnitsOnHand"], 4).ToString();
                }
                if (drs[0]["CentsPerShare"] != DBNull.Value)
                {
                    centsPerShare = decimal.Round((decimal)drs[0]["CentsPerShare"], 4).ToString();
                }
                if (drs[0]["InvestmentAmount"] != DBNull.Value)
                {
                    investmentAmount = decimal.Round((decimal)drs[0]["InvestmentAmount"], 6).ToString();
                }

                if (drs[0]["PaidDividend"] != DBNull.Value)
                {
                    paidDividend = decimal.Round((decimal)drs[0]["PaidDividend"], 6).ToString();
                }


                if (drs[0]["InterestPaid"] != DBNull.Value)
                {
                    interestPaid = decimal.Round((decimal)drs[0]["InterestPaid"], 6).ToString();
                }

                if (drs[0]["InterestRate"] != DBNull.Value)
                {
                    interestRate = decimal.Round((decimal)drs[0]["InterestRate"], 6).ToString();
                }

                if (drs[0]["FrankingCredits"] != DBNull.Value)
                {

                    frankingCredits = Math.Round((double)drs[0]["FrankingCredits"], 3).ToString();
                }


                if (drs[0]["FrankedAmount"] != DBNull.Value)
                {
                    frankedAmount = Math.Round((double)drs[0]["FrankedAmount"], 3).ToString();
                }

                if (drs[0]["UnfrankedAmount"] != DBNull.Value)
                {
                    unfrankedAmount = Math.Round((double)drs[0]["UnfrankedAmount"], 3).ToString();
                }

                if (drs[0]["TotalFrankingCredits"] != DBNull.Value)
                {
                    totalFrankingCredits = Math.Round((double)drs[0]["TotalFrankingCredits"], 3).ToString();
                }
                contents = "<table>" +
                                 "<tr><td><u>Investment Code:</u></td><td>" + drs[0]["InvestmentCode"] + "</td></tr>" +
                                 "<tr><td><u>Product Type:</u></td><td>" + drs[0]["ProductType"] + "</td></tr>" +
                                 "<tr><td><u>Transaction Type:</u></td><td>" + drs[0]["TransactionType"] + "</td></tr>" +
                                 "<tr><td><u>Record Date:</u></td><td>" + recordDate + "</td></tr>" +
                                 "<tr><td><u>Payment Date:</u></td><td>" + paymentDate + "</td></tr>" +
                                 "<tr><td><u>Units On Hand:</u></td><td>" + unitsOnHand + "</td></tr>" +
                                 "<tr><td><u>Cents Per Share:</u></td><td>" + centsPerShare + "</td></tr>" +
                                 "<tr><td><u>Investment Amount:</u></td><td>" + investmentAmount + "</td></tr>" +
                                 "<tr><td><u>Paid Dividend:</u></td><td>" + paidDividend + "</td></tr>" +
                                 "<tr><td><u>Interest Paid:</u></td><td>" + interestPaid + "</td></tr>" +
                                 "<tr><td><u>Interest Rate:</u></td><td>" + interestRate + "</td></tr>" +
                                 "<tr><td><u>Franking Credits:</u></td><td>" + frankingCredits + "</td></tr>" +
                                  "<tr><td><u>Franked Amount:</u></td><td>" + frankedAmount + "</td></tr>" +
                                 "<tr><td><u>UnfrankedAmount:</u></td><td>" + unfrankedAmount + "</td></tr>" +
                                 "<tr><td><u>Total Franking Credits:</u></td><td>" + totalFrankingCredits + "</td></tr>" +

                                 "<tr><td><u>Balance Date:</u></td><td>" + balanceDate + "</td></tr>" +
                                 "<tr><td><u>Books Close Date:</u></td><td>" + booksCloseDate + "</td></tr>" +
                                 "<tr><td><u>Dividend Type:</u></td><td>" + drs[0]["DividendType"] + "</td></tr>" +
                                 "<tr><td><u>Currency:</u></td><td>" + drs[0]["Currency"] + "</td></tr>" +
                                 "<tr><td><u>Status:</u></td><td>" + drs[0]["Status"] + "</td></tr>" +
                                 "</table>";
            }
            return contents;
        }

        private void SetEditingData(GridEditManager editMan, ref GridDropDownListColumnEditor ddlImportTranType, ref GridDropDownListColumnEditor ddlSysTranType, ref GridDropDownListColumnEditor ddlCatTranType, ref GridDropDownListColumnEditor ddlAccountListType)
        {
            ddlAccountListType = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("DDLAccountList"));
            RadComboBox ddlAccountListTypeRadCombo = ddlAccountListType.ComboBoxControl;
            ddlAccountListTypeRadCombo.Filter = RadComboBoxFilter.Contains;

            if (PresentationData.Tables[BankTransactionDS.BANKACCOUNTLIST] != null)
            {
                ddlAccountListType.DataTextField = BankTransactionDS.BANKACCOUNTNO;
                ddlAccountListType.DataValueField = BankTransactionDS.BANKCID;
                ddlAccountListType.DataSource = PresentationData.Tables[BankTransactionDS.BANKACCOUNTLIST];
                ddlAccountListType.DataBind();
                RadComboBoxItem ddlAccountListTypeRadComboDefaultItem = new RadComboBoxItem("", "");
                ddlAccountListTypeRadComboDefaultItem.Selected = true;
                ddlAccountListTypeRadCombo.Items.Add(ddlAccountListTypeRadComboDefaultItem);
            }

            ddlCatTranType = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("DDLCategoryTranType"));
            RadComboBox ddlCatTranTypeRadCombo = ddlCatTranType.ComboBoxControl;
            ddlCatTranTypeRadCombo.Filter = RadComboBoxFilter.Contains;
            RadComboBoxItem ddlCatTranTypeRadComboDefaultItem = new RadComboBoxItem("", "");
            ddlCatTranTypeRadComboDefaultItem.Selected = true;
            ddlCatTranTypeRadCombo.Items.Add(ddlCatTranTypeRadComboDefaultItem);
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Benefit Payment", "Benefit Payment"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Contribution", "Contribution"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Expense", "Expense"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Income", "Income"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("Investment", "Investment"));
            ddlCatTranTypeRadCombo.Items.Add(new RadComboBoxItem("TAX", "TAX"));

            ddlImportTranType = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("DDLImportTranType"));
            RadComboBox ddlImportTranTypeRadCombo = ddlImportTranType.ComboBoxControl;
            ddlImportTranTypeRadCombo.Filter = RadComboBoxFilter.Contains;
            RadComboBoxItem ddlImportTranTypeRadComboDefaultItem = new RadComboBoxItem("", "");
            ddlImportTranTypeRadComboDefaultItem.Selected = true;
            ddlImportTranTypeRadCombo.Items.Add(ddlImportTranTypeRadComboDefaultItem);
            ddlImportTranTypeRadCombo.Items.Add(new RadComboBoxItem("Deposit", "Deposit"));
            ddlImportTranTypeRadCombo.Items.Add(new RadComboBoxItem("Withdrawal", "Withdrawal"));

            ddlSysTranType = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("DDLSystemTranType"));
            RadComboBox ddlSysTranTypeCombo = ddlSysTranType.ComboBoxControl;
            ddlSysTranTypeCombo.Filter = RadComboBoxFilter.Contains;
            RadComboBoxItem ddlSysTranTypeComboDefaultItem = new RadComboBoxItem("", "");
            ddlSysTranTypeComboDefaultItem.Selected = true;
            ddlSysTranTypeCombo.Items.Add(ddlSysTranTypeComboDefaultItem);
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Accounting Expense", "Accounting Expense"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Administration Fee", "Administration Fee"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Advisory Fee", "Advisory Fee"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Application", "Application"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Business Activity Statement", "Business Activity Statement"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Commission Rebate", "Commission Rebate"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Distribution", "Distribution"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Dividend", "Dividend"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Employer Additional", "Employer Additional"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Employer SG", "Employer SG"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Income Tax", "Income Tax"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Instalment Activity Statement", "Instalment Activity Statement"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Insurance Premium", "Insurance Premium"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Interest", "Interest"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Internal Cash Movement", "Internal Cash Movement"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Investment Fee", "Investment Fee"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Legal Expense", "Legal Expense"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("PAYG", "PAYG"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Pension Payment", "Pension Payment"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Personal", "Personal"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Property", "Property"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Redemption", "Redemption"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Regulatory Fee", "Regulatory Fee"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Rental Income", "Rental Income"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Salary Sacrifice", "Salary Sacrifice"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Spouse", "Spouse"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Tax Refund", "Tax Refund"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Transfer In", "Transfer In"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Transfer Out", "Transfer Out"));
            ddlSysTranTypeCombo.Items.Add(new RadComboBoxItem("Withdrawal", "Withdrawal"));
        }

        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) { }
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e) { }
        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e) { }

        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("Banks".Equals(e.Item.OwnerTableView.Name))
            {
                GridEditFormInsertItem EditForm = (GridEditFormInsertItem)e.Item;

                Hashtable values = new Hashtable();
                EditForm.ExtractValues(values);

                RadComboBox comboSystem = (RadComboBox)EditForm["DDLSystemTranType"].Controls[0];
                RadComboBox comboImport = (RadComboBox)EditForm["DDLImportTranType"].Controls[0];
                RadComboBox comboTrans = (RadComboBox)EditForm["DDLCategoryTranType"].Controls[0];
                RadComboBox comboAccount = (RadComboBox)EditForm["DDLAccountList"].Controls[0];

                var bankTransactionDetailsDS = new BankTransactionDetailsDS { DataSetOperationType = DataSetOperationType.NewSingle };
                DataRow row = bankTransactionDetailsDS.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].NewRow();
                row[BankTransactionDetailsDS.ID] = new Guid(comboAccount.SelectedValue);
                row[BankTransactionDetailsDS.IMPORTTRANTYPE] = comboImport.SelectedValue;
                row[BankTransactionDetailsDS.CATTRANTYPE] = comboTrans.SelectedValue;
                row[BankTransactionDetailsDS.SYSTRANTYPE] = comboSystem.SelectedValue;
                row[BankTransactionDetailsDS.AMOUNT] = ((TextBox)(((GridEditableItem)(e.Item))["BankAmount"].Controls[0])).Text;
                row[BankTransactionDetailsDS.ADJUSTMENT] = ((TextBox)(((GridEditableItem)(e.Item))["BankAdjustment"].Controls[0])).Text;
                row[BankTransactionDetailsDS.COMMENT] = ((TextBox)(((GridEditableItem)(e.Item))["Comment"].Controls[0])).Text;

                if (((RadDatePicker)(((GridEditableItem)(e.Item))["BankTransactionDate"].Controls[0])).SelectedDate.Value != null)
                    row[BankTransactionDetailsDS.TRANDATE] = ((RadDatePicker)(((GridEditableItem)(e.Item))["BankTransactionDate"].Controls[0])).SelectedDate.Value;

                bankTransactionDetailsDS.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows.Add(row);

                SaveData(comboAccount.SelectedValue, bankTransactionDetailsDS);
                e.Canceled = true;
                e.Item.Edit = false;
                this.GetData(true);
                this.PresentationGrid.Rebind();
            }
        }
        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {

            switch (e.CommandName.ToLower())
            {
                case "edit":
                    {
                        var dataItem = (GridDataItem)e.Item;

                        this.GetData(true);
                        GridDataItem EditForm = (GridDataItem)e.Item;
                        string rowID = dataItem["ID"].Text;
                        DataRow selectedRow = PresentationData.Tables["Cash Transactions"].Select("ID='" + rowID + "'").FirstOrDefault();
                        if (selectedRow != null)
                        {
                            lblTranID.Text = selectedRow["ID"].ToString();
                            this.lblBankCID.Text = selectedRow["BankCID"].ToString();
                        }
                        break;
                    }
                case "update":
                    {
                        GridEditFormItem EditForm = (GridEditFormItem)e.Item;
                        RadComboBox comboSystem = (RadComboBox)EditForm["DDLSystemTranType"].Controls[0];
                        RadComboBox comboImport = (RadComboBox)EditForm["DDLImportTranType"].Controls[0];
                        RadComboBox comboTrans = (RadComboBox)EditForm["DDLCategoryTranType"].Controls[0];
                        RadComboBox comboAccount = (RadComboBox)EditForm["DDLAccountList"].Controls[0];

                        var bankTransactionDetailsDS = new BankTransactionDetailsDS { DataSetOperationType = DataSetOperationType.UpdateSingle };
                        DataRow row = bankTransactionDetailsDS.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].NewRow();
                        row[BankTransactionDetailsDS.ID] = lblTranID.Text;
                        row[BankTransactionDetailsDS.IMPORTTRANTYPE] = comboImport.Text;
                        row[BankTransactionDetailsDS.CATTRANTYPE] = comboTrans.Text;
                        row[BankTransactionDetailsDS.SYSTRANTYPE] = comboSystem.Text;
                        row[BankTransactionDetailsDS.AMOUNT] = ((TextBox)(((GridEditableItem)(e.Item))["BankAmount"].Controls[0])).Text;
                        row[BankTransactionDetailsDS.ADJUSTMENT] = ((TextBox)(((GridEditableItem)(e.Item))["BankAdjustment"].Controls[0])).Text;
                        row[BankTransactionDetailsDS.COMMENT] = ((TextBox)(((GridEditableItem)(e.Item))["Comment"].Controls[0])).Text;
                        if (((RadDatePicker)(((GridEditableItem)(e.Item))["BankTransactionDate"].Controls[0])).SelectedDate.Value != null)
                            row[BankTransactionDetailsDS.TRANDATE] = ((RadDatePicker)(((GridEditableItem)(e.Item))["BankTransactionDate"].Controls[0])).SelectedDate.Value;
                        bankTransactionDetailsDS.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows.Add(row);
                        SaveData(lblBankCID.Text, bankTransactionDetailsDS);
                        e.Canceled = true;
                        e.Item.Edit = false;
                        this.GetData(true);
                        PresentationGrid.Rebind();
                        break;
                    }
                case "delete":
                    var dataItemDelete = (GridDataItem)e.Item;
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var ds = new BankTransactionDetailsDS
                                 {
                                     DataSetOperationType = DataSetOperationType.DeletSingle,
                                     BankTransactionID = dataItemDelete["ID"].Text
                                 };
                    SaveData(dataItemDelete["BankCID"].Text, ds);
                    PresentationGrid.Rebind();
                    break;
            }
        }
    }
}
