﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.CM.OrganizationUnit;
using System.Globalization;

namespace eclipseonlineweb
{
    public partial class ConsolFeeMatrix : UMABasePage
    {
        protected override void GetBMCData()
        {
            this.cid = Request.QueryString["ins"].ToString();
            IOrganizationUnit clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            ConsolidationFeeDistributionDS consolidationFeeDistributionDS = new ConsolidationFeeDistributionDS();
            this.SaveData(cid, consolidationFeeDistributionDS);
            clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            clientData.GetData(consolidationFeeDistributionDS);
            this.PresentationData = consolidationFeeDistributionDS;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }


        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
        }

        public override void LoadPage()
        {
            this.cid = Request.QueryString["ins"].ToString();
            base.LoadPage();
        }

        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e){ }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "update":
                    {
                        GridEditFormItem dataItem = (GridEditFormItem)e.Item;
                        Guid rowID = (Guid)dataItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["CLIENTCID"];
                        string feeDis = ((TextBox)(((GridEditableItem)(e.Item))["DISTRIBUTEDVALUE"].Controls[0])).Text;

                        ConsolidationFeeDistributionDS consolidationFeeDistributionDS = new Oritax.TaxSimp.Common.Data.ConsolidationFeeDistributionDS();
                        consolidationFeeDistributionDS.OrgCID = rowID;
                        consolidationFeeDistributionDS.DistributionPercentage = decimal.Parse(feeDis);
                        consolidationFeeDistributionDS.DataSetOperationType = DataSetOperationType.UpdateSingle;

                        this.cid = Request.QueryString["ins"].ToString();
                        IOrganizationUnit clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
                        clientData.SetData(consolidationFeeDistributionDS);
                        e.Canceled = true;
                        e.Item.Edit = false;
                        this.GetBMCData();
                        this.PresentationGrid.Rebind();
                        break;
                    }
            }
        }

        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e){}
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e){}
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e){}
        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
        }

        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e){}

        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DataView view = new DataView(this.PresentationData.Tables[ConsolidationFeeDistributionDS.CONSOLIDATIONFEEDISTRIBUTIONTABLE]);
            view.Sort = ConsolidationFeeDistributionDS.CLIENTNAME + " ASC";
            this.PresentationGrid.DataSource = view.ToTable(); 
        }

        protected void btnFeeRun_Click(object sender, EventArgs e)
        {
           
        }

        protected void btnFeeRunWithTransaction_Click(object sender, EventArgs e)
        {
            
        }
    }
}
