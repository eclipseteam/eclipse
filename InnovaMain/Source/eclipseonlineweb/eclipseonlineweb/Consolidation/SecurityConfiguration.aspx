﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/Consolidation/ConsolViewMaster.master"
    AutoEventWireup="true" CodeBehind="SecurityConfiguration.aspx.cs" Inherits="eclipseonlineweb.SecurityConfigurationGroup" %>

<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register Src="../Controls/SecurityConfigurationControl.ascx" TagName="SecurityConfigurationControl"
    TagPrefix="uc2" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td style="width: 5%">
                    </td>
                    <td style="width: 5%">
                    </td>
                    <td style="width: 5%">
                    </td>
                    <td style="width: 5%">
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td style="width: 85%;" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <table id="MainView">
        <tr>
            <td>
                <uc2:SecurityConfigurationControl ID="SecurityConfigurationControl1" runat="server" />
            </td>
        </tr>
    </table>

    
</asp:Content>
