﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using System.Collections;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.OrganizationUnit;
using System.Collections.Generic;
using Telerik.Web.UI;
using Oritax.TaxSimp.CalculationInterface;

// ReSharper disable CheckNamespace
namespace eclipseonlineweb
// ReSharper restore CheckNamespace
{
    public partial class ConsolMembers : UMABasePage
    {

        protected void button_AddMember_Click(object sender, System.EventArgs e)
        {
            foreach (RadComboBoxItem objItem in this.cmbAvailable.Items.ToList())
            {
                if (objItem.Checked &&
                    !this.cmbIncluded.Items.Contains(objItem))
                {
                    objItem.Checked = false; 
                    this.cmbIncluded.Items.Add(objItem);
                }
            }

            cid = Request.QueryString["ins"];
            IOrganizationUnit clientData = UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            GroupMembersDS objGroupMembersDS = new GroupMembersDS();
            objGroupMembersDS.GroupMemberOperations = GroupMemberOperations.AllClients;
            objGroupMembersDS.Username = this.Context.User.Identity.Name;
            clientData.GetData(objGroupMembersDS);

            this.ProcessIncludedMembers(this.cmbIncluded, objGroupMembersDS);
        }

        protected void button_RemoveMember_Click(object sender, System.EventArgs e)
        {
            IEnumerator objIncludedEntitiesEn = this.cmbIncluded.Items.GetEnumerator();

            while (objIncludedEntitiesEn.MoveNext())
            {
                RadComboBoxItem objItem = (RadComboBoxItem)objIncludedEntitiesEn.Current;

                if (objItem.Checked)
                {
                    this.cmbIncluded.Items.Remove(objItem);
                    objIncludedEntitiesEn = this.cmbIncluded.Items.GetEnumerator();
                }
            }

            cid = Request.QueryString["ins"];
            IOrganizationUnit clientData = UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            GroupMembersDS objGroupMembersDS = new GroupMembersDS();
            objGroupMembersDS.GroupMemberOperations = GroupMemberOperations.AllClients;
            objGroupMembersDS.Username = this.Context.User.Identity.Name;
            clientData.GetData(objGroupMembersDS);

            this.ProcessIncludedMembers(this.cmbIncluded, objGroupMembersDS);
        }

        private void ProcessIncludedMembers(RadComboBox includedMembersList, DataSet objGroupMembersDS)
        {

            DataTable includedMembersTable =  objGroupMembersDS.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE]; 
            List<Guid> includedMemberIDs = new List<Guid>(includedMembersList.Items.Count);

            foreach (RadComboBoxItem includedMemberItem in includedMembersList.Items)
            {
                Guid includedMemberID = new Guid(includedMemberItem.Value);
                includedMemberIDs.Add(includedMemberID);
            }

            //Reset Consol Fee
            for (int i = 0; i < includedMembersTable.Rows.Count; i++)
            {
                Guid entityCLID = new Guid((string)includedMembersTable.Rows[i][GroupMembersDS.ENTITYCLID_FIELD]);

                if (!includedMemberIDs.Contains(entityCLID))
                {
                    Guid entityCIID = new Guid((string)includedMembersTable.Rows[i][GroupMembersDS.ENTITYCIID_FIELD]);
                    IOrganizationUnit orgUnit = UMABroker.GetBMCInstance(entityCIID) as IOrganizationUnit;
                    foreach(var feeTran in orgUnit.FeeTransactions)
                    {
                        foreach(var holdingEnt in feeTran.FeeHoldingEntityList)
                        {
                            holdingEnt.CalculatedRatio = 0;
                            holdingEnt.ConsolHolding = 0;
                        }
                    }

                    SaveData(orgUnit as IBrokerManagedComponent);
                }
            }

            for (int i = 0; i < includedMembersTable.Rows.Count; i++)
            {
                Guid entityCLID = new Guid((string)includedMembersTable.Rows[i][GroupMembersDS.ENTITYCLID_FIELD]);

                if (!includedMemberIDs.Contains(entityCLID))
                    includedMembersTable.Rows[i].Delete();
            }

            foreach (Guid entityCLID in includedMemberIDs)
            {
                DataRow includedMemberRow = includedMembersTable.Rows.Find(entityCLID);

                if (includedMemberRow == null)
                {
                    includedMemberRow = includedMembersTable.NewRow();
                    includedMemberRow[GroupMembersDS.ENTITYNAME_FIELD] = includedMembersList.Items.FindItemByValue(entityCLID.ToString());
                    includedMemberRow[GroupMembersDS.ENTITYCLID_FIELD] = entityCLID;
                    includedMemberRow[GroupMembersDS.ADJUSTMENTENTITY_FIELD] = bool.FalseString;
                    includedMembersTable.Rows.Add(includedMemberRow);
                }
            }

            this.SaveData(this.cid, objGroupMembersDS );
            this.LoadPage();
        }

        protected override void GetBMCData()
        {
            cid = Request.QueryString["ins"];
            IOrganizationUnit clientData = UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            GroupMembersDS objGroupMembersDS = new GroupMembersDS();
            objGroupMembersDS.GroupMemberOperations = GroupMemberOperations.AllClients;
            objGroupMembersDS.Username = this.Context.User.Identity.Name;
            clientData.GetData(objGroupMembersDS);
            this.PresentationData = objGroupMembersDS;
            PopulateWorkpaper(objGroupMembersDS, clientData.CLID);
        }

        public override void LoadPage()
        {
            if(!this.IsPostBack)
               this.GetBMCData(); 
        }

        public void PopulateWorkpaper(DataSet objData, Guid groupCLID)
        {
            // get the information for available entities, 
            // sort it alphabetically and bind it to the listbox
            DataTable objAvailableEntitiesTable = ((GroupMembersDS)objData).Tables[GroupMembersDS.AVAILABLEENTITIES_TABLE];
            DataView objAvailableEntitiesView = new DataView(objAvailableEntitiesTable);

            // remove myself from the list of entities my excluding my clid from the list
            objAvailableEntitiesView.RowFilter = "NOT (" + GroupMembersDS.ENTITYCLID_FIELD + " = '" + groupCLID.ToString() + "')";
            objAvailableEntitiesView.Sort = GroupMembersDS.ENTITYNAME_FIELD + " ASC";
            this.cmbAvailable.DataSource = objAvailableEntitiesView;
            this.cmbAvailable.DataValueField = GroupMembersDS.ENTITYCLID_FIELD;
            this.cmbAvailable.DataTextField = GroupMembersDS.ENTITYNAME_FIELD;
            this.cmbAvailable.DataBind();

            // get the information for included entities,
            // sort it alphabetically and bind it to the listbox
            DataTable objIncludedEntitiesTable = ((GroupMembersDS)objData).Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE];
            DataView objIncludedEntitiesView = new DataView(objIncludedEntitiesTable);
            objIncludedEntitiesView.RowFilter = GroupMembersDS.ADJUSTMENTENTITY_FIELD + " <> '" + bool.TrueString + "'";
            objIncludedEntitiesView.Sort = GroupMembersDS.ENTITYNAME_FIELD + " ASC";
            this.cmbIncluded.DataSource = objIncludedEntitiesView;
            this.cmbIncluded.DataValueField = GroupMembersDS.ENTITYCLID_FIELD;
            this.cmbIncluded.DataTextField = GroupMembersDS.ENTITYNAME_FIELD;
            this.cmbIncluded.DataBind();
       }
    }
}
