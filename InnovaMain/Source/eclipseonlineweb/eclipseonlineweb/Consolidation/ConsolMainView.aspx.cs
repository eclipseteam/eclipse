﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
namespace eclipseonlineweb
{
    public partial class ConsolMainView : UMABasePage
    {
        private void BindBreakDownGridTD(string tdId)
        {
            var ds = PresentationData as HoldingRptDataSet;
            if (ds != null)
            {
                var tdsummaryView = new DataView(ds.TDBreakDownTable)
                    {
                        RowFilter = ds.TDBreakDownTable.TDID + "='" + tdId + "'",
                        Sort = string.Format("{0} Asc,{1} Asc", ds.TDBreakDownTable.TRANSDATE, ds.TDBreakDownTable.TRANSTYPE)
                    };
                var tdfilter = AddCumulativeAmount(tdsummaryView.ToTable(), ds.TDBreakDownTable.HOLDING).DefaultView;
                tdfilter.Sort = string.Format("{0} DESC,{1} DESC", ds.TDBreakDownTable.TRANSDATE, ds.TDBreakDownTable.TRANSTYPE);
                TDBreakDown.DataSource = tdfilter;
                TDBreakDown.DataBind();
            }
        }

        /// <summary>
        /// Calculate and Add Cumulative Amount Balance Column in datatable
        /// </summary>
        /// <param name="dataTable">Datatable to use</param>
        /// <param name="holdingCol">Values Column</param>
        /// <returns>Datatable</returns>
        private DataTable AddCumulativeAmount(DataTable dataTable, string holdingCol)
        {
            const string colName = "CumulativeAmount";
            if (!dataTable.Columns.Contains(colName))
                dataTable.Columns.Add(colName, typeof(decimal));

            decimal cumulativeAmount = 0;
            foreach (DataRow dr in dataTable.Rows)
            {
                decimal holding;
                decimal.TryParse(dr[holdingCol].ToString().Trim(), out holding);
                cumulativeAmount = cumulativeAmount + holding;
                dr[colName] = cumulativeAmount;
            }
            return dataTable;
        }

        private void BindBreakDownGrid(string productID, string serviceType)
        {
            var ds = PresentationData as HoldingRptDataSet;
            if (ds != null)
            {
                var prosummaryView = new DataView(ds.ProductBreakDownTable)
                    {
                        RowFilter =
                            ds.ProductBreakDownTable.PRODUCTID + "='" + productID + "' and " +
                            ds.ProductBreakDownTable.SERVICETYPE + "='" + serviceType + "'",
                        Sort = ds.ProductBreakDownTable.INVESMENTCODE + " ASC"
                    };
                DataTable sortedFilteredTable = prosummaryView.ToTable();
                BreakdownGrid.DataSource = sortedFilteredTable;
                BreakdownGrid.DataBind();
            }
        }

        protected void FinancialSummary_RowCommand(object sender, C1GridViewCommandEventArgs e)
        {
            if (e.CommandName != "filter" & e.CommandName != "sort")
            {
                string[] args = e.CommandArgument.ToString().Split('_');

                string assetName = args[0];
                string tdID = args[1];
                string productID = args[2];
                string description = args[3];
                string serviceType = args[4];
                if ((assetName == "FI" && tdID != string.Empty) || assetName == "TDs" || description.Contains("Term Deposit Account"))
                {
                    if (tdID != string.Empty)
                    {
                        BindBreakDownGridTD(tdID);
                        BreakdownGrid.Visible = false;
                        lnkBackToSummary.Visible = true;
                        FinancialSummary.Visible = false;
                        TDBreakDown.Visible = true;
                    }
                }
                else
                {
                    if (productID != string.Empty)
                    {
                        BindBreakDownGrid(productID, serviceType);
                        lnkBackToSummary.Visible = true;
                        BreakdownGrid.Visible = true;
                        FinancialSummary.Visible = false;
                        TDBreakDown.Visible = false;
                    }
                }
            }
        }

        /// <summary>
        /// Seting User Type and Is admin hidden fileds
        /// </summary>
        private void SetUserTypes()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            hidUserType.Value = objUser.UserType.ToString();
            hidIsAdmin.Value = objUser.Administrator.ToString();
        }

        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];
            var clientData = UMABroker.GetBMCInstance(new Guid(cid));
            var ds = new HoldingRptDataSet();
            SetUserTypes();
            if (C1FinancialSummaryDate.Date.HasValue)
                ds.ValuationDate = C1FinancialSummaryDate.Date.Value;
            clientData.GetData(ds);
            FilterRecords(ds);
            PresentationData = ds;
            PopulatePage(PresentationData);
            BindFinancialSummary(ds);
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            var sm = ScriptManager.GetCurrent(Page);
            string pageName = Request.QueryString["PageName"];
        }

        /// <summary>
        /// Filter records with 0 Holding and 0 Total, excluding desktop broker and bank accounts with account type CMA.
        /// </summary>
        /// <param name="ds">HoldingRptDataSet</param>
        private void FilterRecords(HoldingRptDataSet ds)
        {
            //Filter Record for Ticket No. 893
            if (ds.Tables.Count != 0)
            {
                ds.Tables["HoldingSummary"].Columns.Add("RemoveRow", typeof(Boolean));
                foreach (DataRow objDr in ds.Tables["HoldingSummary"].Rows)
                {
                    if (objDr["LinkedEntityType"].ToString() != OrganizationType.DesktopBrokerAccount.ToString() &&
                        !(objDr["LinkedEntityType"].ToString() == OrganizationType.BankAccount.ToString() &&
                          objDr["ProductName"].ToString().ToLower().Contains("cma")))
                    {
                        if (Decimal.Parse(objDr["Holding"].ToString()) == 0 &&
                            Decimal.Parse(objDr["Total"].ToString()) == 0)
                        {
                            objDr["RemoveRow"] = true;
                        }
                    }
                }

                DataRow[] drRowsToDelete = ds.Tables["HoldingSummary"].Select("RemoveRow='true'");
                foreach (DataRow dr in drRowsToDelete)
                {
                    ds.Tables["HoldingSummary"].Rows.Remove(dr);
                }
            }
        }

        private void BindFinancialSummary(HoldingRptDataSet ds)
        {
            var summaryView = new DataView(ds.HoldingSummaryTable)
                {
                    Sort =
                        string.Format("{0}, {1} ASC", ds.HoldingSummaryTable.SERVICETYPE, ds.HoldingSummaryTable.ASSETNAME)
                };
            FinancialSummary.DataSource = summaryView.ToTable();
            FinancialSummary.DataBind();
        }

        protected void BackToSummary(object sender, EventArgs args)
        {
            BindFinancialSummary(PresentationData as HoldingRptDataSet);
            FinancialSummary.Visible = true;
            BreakdownGrid.Visible = false;
            lnkBackToSummary.Visible = false;
            TDBreakDown.Visible = false;
        }
        protected void btnBack_Click(object sender, EventArgs args)
        {
            string PageName = Request.QueryString["PageName"];
            if (PageName == "GroupsFUM")
                Response.Redirect("~/GroupsFUM.aspx");
            else if (PageName == "AccountFumCompliance")
                Response.Redirect("~/SysAdministration/AccountsFUMCompliance.aspx");
            else if (PageName == "AccountFUM")
                Response.Redirect("~/AccountsFUM.aspx");
        }
        /// <summary>
        /// Get the Service Type
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string ReturnServicesType(string str)
        {
            string[] strArray = str.Split(':');
            if (strArray.Length > 0)
                return strArray[0];
            return string.Empty;
        }

        protected void FinancialSummary_RowDataBound(object sender, C1GridViewRowEventArgs e)
        {
            if (e.Row.DataItem != null)
            {
                //Do It For Me: 0111 - DIWM Bank, Broker, Fixed Interest & Innova
                string modelName = ((DataRowView)(e.Row.DataItem)).Row["MODELNAMECONSOL"].ToString();
                string serviceType = ReturnServicesType(modelName);
                if ((hidIsAdmin.Value.ToLower() == "true" || hidUserType.Value == UserType.Innova.ToString()) && !string.IsNullOrEmpty(serviceType))
                {
                    //if user is Admin
                    e.Row.Cells[2].Text = modelName;
                }
                else
                {
                    if (serviceType == "Do It For Me")
                    {
                        e.Row.Cells[2].Text = modelName;
                    }
                    else
                    {
                        //if the selected User are Not Admin
                        e.Row.Cells[2].Text = serviceType;
                    }
                }

                var assetName = ((DataRowView)(e.Row.DataItem)).Row["AssetName"].ToString();
                var description = ((DataRowView)(e.Row.DataItem)).Row["Description"].ToString();
                var tdID = ((DataRowView)(e.Row.DataItem)).Row["TDID"].ToString();
                var productID = ((DataRowView)(e.Row.DataItem)).Row["ProductID"].ToString();

                if (assetName == "TDs" || description.Contains("Term Deposit Account"))
                {
                    if (tdID != string.Empty)
                    {
                        var linkDesc = (LinkButton)e.Row.FindControl("linkDesc");
                        linkDesc.Visible = true;
                        e.Row.FindControl("lblDesc").Visible = false;
                    }
                }
                else
                {
                    if (productID != string.Empty)
                    {
                        var linkDesc = (LinkButton)e.Row.FindControl("linkDesc");
                        linkDesc.Visible = true;
                        e.Row.FindControl("lblDesc").Visible = false;
                    }
                }
            }
        }

        private void MainView()
        {
            BindFinancialSummary(PresentationData as HoldingRptDataSet);
            pnlFinancialSummary.Visible = true;
            pnlToolBar.Visible = true;
        }

        protected void GenerateFinancialSummary(object sender, EventArgs e)
        {
            var clientData = UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"]));

            if (C1FinancialSummaryDate.Date != null)
            {
                var ds = new HoldingRptDataSet { ValuationDate = C1FinancialSummaryDate.Date.Value };
                clientData.GetData(ds);
                PresentationData = ds;

                BindFinancialSummary(ds);
            }
            pnlFinancialSummary.Visible = true;
            pnlToolBar.Visible = true;

            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void Filter(object sender, C1GridViewFilterEventArgs e)
        {
            e.Values[0] = ((string)e.Values[0]).Trim();

            var ds = PresentationData as HoldingRptDataSet;
            if (ds != null)
            {
                var summaryView = new DataView(ds.HoldingSummaryTable)
                    {
                        Sort =
                            string.Format("{0}, {1} ASC", ds.HoldingSummaryTable.SERVICETYPE, ds.HoldingSummaryTable.ASSETNAME)
                    };

                FinancialSummary.DataSource = summaryView;
                FinancialSummary.DataBind();
            }
        }

        protected void FinancialSummarySorting(object sender, C1GridViewSortEventArgs e)
        {
            MainView();
        }

        #region Events

        protected void MainView_Click(object sender, EventArgs e)
        {
            MainView();
        }

        protected void Refresh_Click(object sender, EventArgs e)
        {
            cid = Request.QueryString["ins"];
            var clientData = UMABroker.GetBMCInstance(new Guid(cid));
            UMABroker.ReleaseBrokerManagedComponent(clientData);

            clientData = UMABroker.GetBMCInstance(new Guid(cid));

            var ds = new HoldingRptDataSet();

            if (C1FinancialSummaryDate.Date.HasValue)
                ds.ValuationDate = C1FinancialSummaryDate.Date.Value;

            clientData.GetData(ds);
            PresentationData = ds;


            PopulatePage(PresentationData);
            BindFinancialSummary(ds);

            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }
        #endregion

    }
}
