﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.CM.OrganizationUnit;
using System.Globalization;
using Oritax.TaxSimp.CM.Organization.Data;

namespace eclipseonlineweb
{
    public partial class ConsolFeeDistribution : UMABasePage
    {
        protected override void  GetBMCData()
        {
            if (!this.Page.IsPostBack)
            {
                IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                OrganisationListingDS organisationListingDS = new OrganisationListingDS();
                organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.FeeRunsShort;
                organization.GetData(organisationListingDS);

                this.cmbFeeList.DataSource = organisationListingDS.Tables[FeeRunListDS.FEERUNSTABLE];
                this.cmbFeeList.DataTextField = "ENTITYNAME_FIELD";
                this.cmbFeeList.DataValueField = "ENTITYCIID_FIELD";
                this.cmbFeeList.DataBind();
            }
        }


        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
        }

        public override void LoadPage()
        {
            this.cid = Request.QueryString["ins"].ToString();
            base.LoadPage();
        }

        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e){ }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
        }

        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e){}
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e){}
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e){}
        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
        }

        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e){}

        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (this.PresentationData != null && this.PresentationData.Tables.Contains(ConsolidationFeeDS.CONSOLIDATIONFEETABLE))
            {
                DataView dataView = new DataView(this.PresentationData.Tables[ConsolidationFeeDS.CONSOLIDATIONFEETABLE]);
                dataView.Sort = ConsolidationFeeDS.CLIENTNAME + " ASC";
                PresentationGrid.DataSource = dataView.ToTable();
            }
        }
        protected void btnFeeRun_Click(object sender, EventArgs e)
        {
            this.cid = Request.QueryString["ins"].ToString();
            IOrganizationUnit clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            ConsolidationFeeDS consolidationFeeDS = new ConsolidationFeeDS();
            consolidationFeeDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
            string id = string.Empty;

            if (this.cmbFeeList.SelectedValue != null)
                id = this.cmbFeeList.SelectedValue;

            if (id != string.Empty)
            {
                consolidationFeeDS.FeeRunID = new Guid(id);
                this.SaveData(clientData.CID.ToString(), consolidationFeeDS);
                LoadFee();
                this.PresentationGrid.Rebind();
            }
        }

        protected void btnFeeLoad_Click(object sender, EventArgs e)
        {
            LoadFee();
        }

        private void LoadFee()
        {
            this.cid = Request.QueryString["ins"].ToString();
            IOrganizationUnit clientData = this.UMABroker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            ConsolidationFeeDS consolidationFeeDS = new ConsolidationFeeDS();

            string id = string.Empty;

            if (this.cmbFeeList.SelectedValue != null)
                id = this.cmbFeeList.SelectedValue;

            if (id != string.Empty)
            {
                consolidationFeeDS.FeeRunID = new Guid(id);
                clientData.GetData(consolidationFeeDS);
                this.PresentationData = consolidationFeeDS;
                this.PresentationGrid.Rebind();
            }
        }
    }
}
