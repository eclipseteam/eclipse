﻿function CheckAllCheckBoxes(headerCheckBox, gridId, itemCheckBoxId) {
    var grid = $find(gridId);
    var masterTable = grid.get_masterTableView();
    for (var i = 0; i < masterTable.get_dataItems().length; i++) {
        var gridItemElement = masterTable.get_dataItems()[i].findElement(itemCheckBoxId);
        if (gridItemElement != null)
            gridItemElement.checked = headerCheckBox.checked;
    }
}

function CheckSingleCheckBox(colId, gridId, itemCheckBoxId) {
    var grid = $find(gridId);
    var masterTable = grid.get_masterTableView();
    var selectAll = true;
    for (var i = 0; i < masterTable.get_dataItems().length; i++) {
        var gridItemElement = masterTable.get_dataItems()[i].findElement(itemCheckBoxId);
        if (gridItemElement != null && !gridItemElement.checked)
            selectAll = false;
    }
    var chkHeader = $telerik.$(masterTable.getColumnByUniqueName(colId).get_element().getElementsByTagName('input'));
    if (chkHeader != null && chkHeader.get(0)!=null)
        chkHeader.get(0).checked = selectAll;
}

function CheckSelectedCheckBoxes(gridId, itemCheckBoxId) {
    var grid = $find(gridId);
    var masterTable = grid.get_masterTableView();
    var isSelected = false;
    for (var i = 0; i < masterTable.get_dataItems().length; i++) {
        var gridItemElement = masterTable.get_dataItems()[i].findElement(itemCheckBoxId);
        if (gridItemElement != null && gridItemElement.checked) {
            isSelected = true;
            break;
        }
    }
    return isSelected;
}
