function onCallbackSuccess(result) {

}

$(document).ready(function () {


	var cnt = $('#content').children('div').length - 1;


	if ($(".wijmo-stylesheet-wijmo_theme").attr("href").indexOf("WebResource") != -1) {
		$('#themes').val("http://cdn.wijmo.com/themes/rocket/jquery-wijmo.css");
	} else {
		$('#themes').val($(".wijmo-stylesheet-wijmo_theme").attr("href"));
	}

	$('#themes').bind("change", function () {
		$(".wijmo-stylesheet-wijmo_theme").attr("href", $(this).val());
		executeCallback("theme=" + $(this).val());
	}).wijdropdown();


	$('.code-button').button({ icons: { primary: 'ui-icon-carat-2-e-w'} });
	//    $('#arrow-up').button({ text: false, icons: { primary: 'ui-icon-carat-1-n'} });
	//    $('#arrow-down').button({ text: false, icons: { primary: 'ui-icon-carat-1-s'} });
	$('#flyout').button({ text: false, icons: { primary: 'ui-icon-triangle-2-e-w'} });
	$('#close-code').button({ text: false, icons: { primary: 'ui-icon-close'} });
	//    $('#arrows').buttonset();
	$('#switcher').buttonset();

	$('.listitem').hover(
        function () {
        	$(this).find('a:first').stop().animate({ paddingLeft: '20px', paddingTop: '12px', fontSize: '16px' }, { duration: 250, easing: "easeOutCirc" }).addClass('listitem-hover');
        },
        function () {
        	$(this).find('a:first').stop().animate({ paddingLeft: '8px', paddingTop: '8px', fontSize: '10px' }, { duration: 350, easing: "easeOutCirc" }).removeClass('listitem-hover');
        }
    );
	$('.listitem-inner').hover(
        function () {
        	$(this).find('a').stop().animate({ paddingLeft: '20px', paddingTop: '12px', fontSize: '16px' }, { duration: 250, easing: "easeOutCirc" }).addClass('listitem-hover');
        },
        function () {
        	$(this).find('a').stop().animate({ paddingLeft: '8px', paddingTop: '8px', fontSize: '10px' }, { duration: 350, easing: "easeOutCirc" }).removeClass('listitem-hover');
        }
    );


	if (jQuery.browser.msie && jQuery.browser.version == "6.0") {

	} else {
		$('.listitem').hover(
        function () {
        	$(this).siblings().stop().animate({ opacity: '0.25' });
        },
        function () {
        	$(this).siblings().stop().animate({ opacity: '1' });
        }
    );
	}



	$('#arrow-up').click(function () {

		return false;
	});

	$('#arrow-down').click(function () {

		return false;
	});


	$(".left-nav>li:has(ul)").hoverIntent(
        function () {
        	$(">ul", this).fadeIn();
        },
        function () {
        	$(">ul", this).fadeOut('fast');
        }
    );


     $('.parent-item').hoverIntent(
        function () {
            $(this).find('ul').fadeIn();
        },
        function () {
            $(this).find('ul').fadeOut('fast');
        }
    );


	$('#view-code').wijdialog({ autoOpen: false, modal: true, resizable: false, draggable: false, width: 960 });
	$('#view-code').parent().css('overflow', 'visible');
	$('#view-code').siblings(".ui-dialog-titlebar").hide();

	$('.code-button').click(function () { $('#view-code').wijdialog('open'); return false; });
	$('#close-code').click(function () {
		$('#view-code').wijdialog('close');
		return false;
	});

	$('#side-menu-list').wijmenu({ orientation: 'vertical', showAnimation: { animated: "slide", option: { direction: "right" }, duration: 350, easing: "easeOutCirc"} });

	function updateDemoSource(source) {
		if ($('#demo-source').length == 0) {
			$('<div id="demo-source"><div class="source-block"><pre><code></code></pre></div></div>').appendTo('#tabs-1');

		}
		var cleanedSource = source
				.replace('themes/Wijmo/jquery.ui.all.css', 'theme/jquery.ui.all.css')
				.replace(/\s*\x3Clink.*demos\x2Ecss.*\x3E\s*/, '\r\n\t')
				.replace(/\x2E\x2E\x2F\x2E\x2E\x2F/g, '');

		$('#demo-source code').empty().text(cleanedSource);

	}

	function getDemoSource(conttroller, action) {
		var data = { controller: conttroller, action: action };
		$.getJSON("/home/GetSource", data, function (data) {
			updateDemoSource(data.source);
		});
	}
});

