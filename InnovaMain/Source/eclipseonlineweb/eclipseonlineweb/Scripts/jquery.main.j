$(function(){
	
    //original field values
    var field_values = {
            //id        :  value
            'username'  : 'username',
            'password'  : 'password',
            'cpassword' : 'password',
            'firstname'  : 'first name',
            'lastname'  : 'last name',
            'email'  : 'email address'
    };
	
	var $lefty = $('#formcontainer');
	var oldStep = 'btn0';

    //inputfocus
    $('input#username').inputfocus({ value: field_values['username'] });
    $('input#password').inputfocus({ value: field_values['password'] });
    $('input#cpassword').inputfocus({ value: field_values['cpassword'] }); 
    $('input#lastname').inputfocus({ value: field_values['lastname'] });
    $('input#firstname').inputfocus({ value: field_values['firstname'] });
    $('input#email').inputfocus({ value: field_values['email'] }); 

    //reset progress bar
    $('#progress').css('width','0');
    $('#progress_text').html('0% Complete');

    //first_step
    $('form').submit(function(){ return false; });
	
    $('#submit_first').click(function(){
        //remove classes
        $('#first_step input').removeClass('error').removeClass('valid');
		$('#btn2').removeClass('current');
        //ckeck if inputs aren't empty
        var fields = $('#first_step input[type=text]');
		var chk_act = $('#first_step input[type=check]').serializeArray();
		var chk_trs = $('#first_step input[type=check]').serializeArray();
		
        var error = 0;
		var chk_trs_error = 0;
//        fields.each(function(){
//            var value = $(this).val();
//			
//            if($(this).attr('id') != 'txt_act_others'){
//    			if( value.length<4 || value==field_values[$(this).attr('id')] ) {
//    				$(this).addClass('error');
//                    //$(this).effect("shake", { times:3 }, 50);
//                    error++;
//                }else {
//                    $(this).addClass('valid');
//                }
//			}
//        });    
//		
//		if (chk_act.length == 0)
//	    {
//		   alert('Please specify the type of account');
//		   error++;
//	    }
//	    else
//	    {
//			//alert(chk_act.length + " items selected");
//			
//			if($('#chk_act_smsf').is(':checked')){ 
//			   
//				var chk_act_smsf = $('#first_step input[name=chk_act_smsf]').serializeArray();
//				
//				if (chk_act_smsf.length == 0)
//        	    {
//        		   alert('Please specify the type of trustee');
//        		   error++;
//        	    }
//			}
//			
//			if($('#chk_act_trust').is(':checked')){ 
//			   
//				var chk_act_trust = $('#first_step input[name=chk_act_trust]').serializeArray();
//				
//				if (chk_act_trust.length == 0)
//        	    {
//        		   alert('Please specify the type of trustee');
//        		   error++;
//        	    }
//			}
//			//check if Other box is ticked
//			if($('#chk_act_others').is(':checked')){
//				if(!$('#txt_act_others').val()){
//					alert('Please provide details');
//					error++;
//				}
//			} 
//	    }
//		
//		
//		var chk_inv = $('#first_step input[name=chk_inv]').serializeArray();
//		
//		if (chk_inv.length == 0)
//        {
//           alert('Please specify the Investor Status');
//           error++;
//        }
       
        if(!error) {
                //update progress bar
                $('#progress_text').html('33% Complete');
                $('#progress').css('width','113px');
  
				//$lefty.animate({marginLeft: parseInt($lefty.css('margin-left'),10) == 0 ? '-890' : 0});

				var newX = parseInt($lefty.css('margin-left')) + (-890);
				$lefty.animate({marginLeft: newX});
				getStep($(this).attr('id'))
			
        } else return false;
    });


    $('#submit_second').click(function(){
        //remove classes
        $('#second_step input').removeClass('error').removeClass('valid');

        //var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;  
        var fields = $('#second_step input[type=text]');
        var error = 0;
        
		fields.each(function(){
            var value = $(this).val();
			
			if($(this).attr('id')!='others'){
              if(value.length < 1 || value==field_values[$(this).attr('id')] ) {
  			      $(this).addClass('error');
                  //$(this).effect("shake", { times:3 }, 50);
                  error++;
              } else {
                  $(this).addClass('valid');
              }
			}
		});

        if(!error) {
                //update progress bar
                $('#progress_text').html('66% Complete');
                $('#progress').css('width','226px');
                
                var newX = parseInt($lefty.css('margin-left')) + (-890);
				$lefty.animate({marginLeft: newX});
				getStep($(this).attr('id'))
				
        } else return false;

    });


    $('#submit_third').click(function(){
		var fields = $('#third_step input[type=text]');
        var error = 0;
		var err = 0;
		
		if($('#chk_diy').is(':checked')){
		   if(!$('#txt_diy').val()){
		       alert('Please specify Estimated Amount to be Invested');
			   error++;
		   }else if($('#txt_diy').val() == '$ Estimated Amount to be Invested'){
		   		alert('Please specify Estimated Amount to be Invested');
		   }
		   
		  /* var chk_diy_type = $('#third_step input[name=chk_diy_type]').serializeArray();
		
    	   if (chk_diy_type.length == 0){
               alert('Please select Service');
               error++;
           }*/
        }else{
			  err++;
		}
		
		if($('#chk_dwm').is(':checked')){
		   if(!$('#txt_dwm').val()){
		       alert('Please specify Estimated Amount to be Invested');
			   error++;
		   }else if($('#txt_dwm').val() == '$ Estimated Amount to be Invested'){
		   		alert('Please specify Estimated Amount to be Invested');
		   }
		   var chk_dwm_type = $('#third_step input[name=chk_dwm_type]').serializeArray();
		
    	  /* if (chk_dwm_type.length == 0){
               alert('Please select Service');
               error++;
           }*/
        }else{
			  err++;
		}
		
		if($('#chk_dfm').is(':checked')){
		   if(!$('#txt_dfm').val()){
		       alert('Please specify Estimated Amount to be Invested');
			   error++;
		   }else if($('#txt_dfm').val() == '$ Estimated Amount to be Invested'){
		   		alert('Please specify Estimated Amount to be Invested');
				 error++;
		   }
		}else{
			  err++;
		}
		
		 
		 fields.each(function(){
            var value = $(this).val();
			
			if($(this).attr('id')!='txt_dfm' || $(this).attr('id')!='txt_dwm' || $(this).attr('id')!='txt_diy'){
              if( value==field_values[$(this).attr('id')] ) {
  			   
                  $(this).addClass('error');
                  //$(this).effect("shake", { times:3 }, 50);
                  error++;
              } else {
                  $(this).addClass('valid');
              }
			}
		});

		if(!error){
		
		   if(err < 3){
                var newX = parseInt($lefty.css('margin-left')) + (-890);
    			$lefty.animate({marginLeft: newX});
    			getStep($(this).attr('id'));
		   }else{
		   		 alert('Indicate the UMA account and services you would like established');
		   }
		} else return false;
		
		
    });
    
    $('#submit_fourth').click(function(){
		
		var fields = $('#fourth_step input[type=text]');
        var error = 0;
		
		fields.each(function(){
            var value = $(this).val();
			
			if(value.length < 1 || value==field_values[$(this).attr('id')] ) {
  			      $(this).addClass('error');
                  //$(this).effect("shake", { times:3 }, 50);
                  error++;
            }else {
                  $(this).addClass('valid');
            }
		});	
											 
		if(!error){
        	 var newX = parseInt($lefty.css('margin-left')) + (-890);
    		 $lefty.animate({marginLeft: newX});
    		 getStep($(this).attr('id'));
		}
    });
    
	$('#chk_acd_registered').live('click',function(){
		
	    if($(this).is(':checked')){
			$('#txt_acd_address2').val($('#txt_acd_address').val());
			$('#txt_acd_suburb2').val($('#txt_acd_suburb').val());
			$('#txt_acd_state2').val($('#txt_acd_state').val());
			$('#txt_acd_postcode2').val($('#txt_acd_postcode').val());
			$('#txt_acd_country2').val($('#txt_acd_country').val());
		}else{
			$('#txt_acd_address2').val('');
			$('#txt_acd_suburb2').val('');
			$('#txt_acd_state2').val('');
			$('#txt_acd_postcode2').val('');
			$('#txt_acd_country2').val('');
		}

	});
	
    $('#submit_fifth').click(function(){
		
		var fields = $('#fifth_step input[type=text]');
        var error = 0;
		
		var sigtitle = $('#fifth_step input[name=sigtitle]').serializeArray();

		if(sigtitle.length == 0){
			 if($('#sigtitle_other').val() == ''){
			 	alert('Please select Title');
				error++;
			 }
		}
		
		fields.each(function(){
            var value = $(this).val();
			
			if(value.length < 1 || value==field_values[$(this).attr('id')] ) {
			    if( ($(this).attr('id') != 'sigtitle_other') && ($(this).attr('id') != 'txt_sig_maincountry') ){
				  $(this).addClass('error');
                  //$(this).effect("shake", { times:3 }, 50);
                  error++;
				}
				
            }else {
                  $(this).removeClass('error');
            }
		});
		
		
		if(!error){ 
    		var newX = parseInt($lefty.css('margin-left')) + (-890);
    		$lefty.animate({marginLeft: newX});
    		getStep($(this).attr('id'))
		}
    });
    
    $('#submit_sixth').click(function(){
   	 	var fields = $('#sixth_step input[type=text]');
        var error = 0;
		var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		
		/*fields.each(function(){
            var value = $(this).val();
			
			if(value.length < 1 || value==field_values[$(this).attr('id')]  || ( $(this).attr('id')=='txt_con_email' && !emailPattern.test(value) ) ) {
  			   $(this).addClass('error');
               error++;
            }else {
               $(this).removeClass('error');
            }
		});*/
		if(!error){
    		var newX = parseInt($lefty.css('margin-left')) + (-890);
    		$lefty.animate({marginLeft: newX});
    		getStep($(this).attr('id'));
		}
    });
    
    $('#submit_seventh').click(function(){
		var chk_access = $('#seventh_step input[name=chk_access]').serializeArray();
		var chk_cma = $('#seventh_step input[name=chk_cma]').serializeArray();
		var error = 0;
		
//		if(chk_access.length == 0){
//			alert('Please select Access Facilities required');
//			error++;
//		}
//		if(chk_cma.length == 0){
//			alert('Please select how you wish to operate your CMA');
//			error++;
//		}
		
		if(!error){ 
			var newX = parseInt($lefty.css('margin-left')) + (-890);
			$lefty.animate({marginLeft: newX});
			getStep($(this).attr('id'))
		}
    });
    
    $('#submit_eight').click(function(){
	
		var fields = $('#eight_step input[type=text]');
        var error = 0;
		
		fields.each(function(){
            var value = $(this).val();
			
			if(value.length < 1 || value==field_values[$(this).attr('id')]  ) {
  			   $(this).addClass('error');
               error++;
            }else {
               $(this).removeClass('error');
            }
		});
		
		if(!error){
    		var newX = parseInt($lefty.css('margin-left')) + (-890);
    		$lefty.animate({marginLeft: newX});
    		getStep($(this).attr('id'))
		}
		
    });
    $('#submit_ninth').click(function(){
   	 	var fields = $('#ninth_step input[type=text]');
		var chk_docs = $('#ninth_step input[name=chk_docs]').serializeArray();
        var error = 0;
		
//		fields.each(function(){
//            var value = $(this).val();
//			
//			if(value.length < 1 || value==field_values[$(this).attr('id')]  ) {
//  			   $(this).addClass('error');
//               error++;
//            }else {
//               $(this).removeClass('error');
//            }
//		});
//		
//		if(chk_docs.length == 0){
//			alert('Please select document');
//			error++;
//		}
		
		if(!error){
    		var newX = parseInt($lefty.css('margin-left')) + (-890);
    		$lefty.animate({marginLeft: newX});
    		getStep($(this).attr('id'))
		}
    });
	
	 $('#submit_tenth').click(function(){
   	 	var fields = $('#tenth_step input[type=text]');
        var error = 0;
		
		fields.each(function(){
            var value = $(this).val();
			
			if($(this).attr('id') != 'txt_declr_sig_app1' && $(this).attr('id') != 'txt_declr_sig_app2' ){
    			if(value.length < 1 || value==field_values[$(this).attr('id')] ) {
      			   $(this).addClass('error');
                   error++;
                }else {
                   $(this).removeClass('error');
                }
			}
		});
		
		if(!error){
    		var newX = parseInt($lefty.css('margin-left')) + (-890);
    		$lefty.animate({marginLeft: newX});
    		getStep($(this).attr('id'))
		}
    });
	
	$('#submit_eleventh').click(function(){
   	 	$(this).addClass('valid');
   	 	var newX = parseInt($lefty.css('margin-left')) + (-890);
		$lefty.animate({marginLeft: newX});
		getStep($(this).attr('id'))
    });
    
    $('.mover').click(function(){
		$('#btn2').removeClass('current');
		var newStep = $(this).attr('id');
		var tempX = $(this).attr('id').replace('btn','');
		var xx = parseInt(tempX) * 890;
		var newX = xx - 890;
		
		$lefty.animate({marginLeft: -newX});
		$(this).addClass('current');
		$('#'+oldStep).removeClass('current');
		oldStep = newStep;
		
	});
	
	function getStep(id){
		
		switch(id){
			case 'resume_final': 
				newStep = 'btn1';
				break;
			case 'submit_first': 
				newStep = 'btn3';
				break;
			case 'submit_second': 
				newStep = 'btn4';
				break;
			case 'submit_third': 
				newStep = 'btn5';
				break;
			case 'submit_fourth': 
				newStep = 'btn6';
				break;
			case 'submit_fifth': 
				newStep = 'btn7';
				break;
			case 'submit_sixth': 
				newStep = 'btn8';
				break;
			case 'submit_seventh': 
				newStep = 'btn9';
				break;
			case 'submit_eight': 
				newStep = 'btn10';
				break;
			case 'submit_ninth': 
				newStep = 'btn11';
				break;
			case 'submit_tenth': 
				newStep = 'btn12';
				break;
			case 'submit_eleventh': 
				newStep = 'btn13';
				break;
			case 'submit_twelveth':
				 newStep = 'btn14';
				break;
			
			
			default:
				newStep = 'btn0';
		}
		
		//alert(newStep+'=='+oldStep)
		$('#'+ newStep).addClass('current');
		$('#'+ oldStep).removeClass('current');
		oldStep = newStep;
		
	}
	
    
    $('#submit_final').click(function(){
		
		$('#smethod').val('submit');
		document.forms["myform"].submit();

    });
	
	$('.save').click(function(){
		
		$('#smethod').val('save');
		document.forms["myform"].submit();
		

    });
	
	$('#resume_final').click(function(){
		$('#smethod').val('resume');
		//$('form').submit(function(){ return true; });
		document.forms["myform"].submit();
		
    });
	
	
	
	$('#programme').live('change',function(){
		$('#txt_programcode').val($(this).val());
	});
	
	
	var id;
	/*** slide 4 add platform ****/
	$('.ddl_platform_name').live('click',function(){
		id = $(this).attr('id').replace('ddl_platform_name','');
		$('#choices'+id).fadeIn('fast');
	});
		
	$('.closebox').live('click',function(){
		var c = $(this).attr('id').replace('closebox','');
		$('#choices'+c).fadeOut('fast');	
	});
		
	$('.ddl_platform').live('click',function(){
			
		if($(this).is(':checked')){
			var t = $('#ddl_platform_name'+id).val();
				
			var e = $(this).attr('title')+',';
			if(t !=''){
				var u = t + e ;
				$('#ddl_platform_name'+id).val(u);
			}else{
				$('#ddl_platform_name'+id).val(e);
			}
		}else{
			var temp = $(this).attr('title')+',';
			var txt = $('#ddl_platform_name'+id).val().replace(temp,'');
			$('#ddl_platform_name'+id).val(txt);
		}
	});
		
	$('#platform').live('click',function(){
		if($(this).is(':checked')){
			$('#addplatform').css('visibility','visible');
		}else{
			$('#addplatform').css('visibility','hidden');	
		}
	});
	$('#addplatform').live('click',function(){
		 var num     = parseInt($('#numform').val());
		 
		 
         var newNum  = new Number(num + 1);
		 
		 var newForm = '<table style="border:1px #d1d1d1 dashed">'+
                       '     <tr>'+
                       '       <td width="20%"><b>Name of the platform:</b></td>'+
                       '        <td>'+
                       '           <input type="text" name="ddl_platform_nametxt'+ newNum +'" readonly="readonly" id="ddl_platform_name'+ newNum +'" class="indentRight indentTop indentBottom ddl_platform_name" value="" /><div id="choices'+ newNum +'" style="position:absolute; width:172px; display:none; height:100px; background:#fff; border:1px #d1d1d1 solid;  margin-top:-5px"><a id="closebox'+ newNum +'" class="closebox" style="position:absolute; width:20px; height:15px; display:block; right:2px; top:2px; cursor:pointer">[ X ]</a>'+
                       '                 <input type="checkbox" class="ddl_platform" name="ddl_platform_name1_'+ newNum +'" value="1" title="e-Clipse Super" /> e-Clipse Super<br />'
                       +'                 <input type="checkbox" class="ddl_platform" name="ddl_platform_name2_'+ newNum +'" value="1" title="BT Wrap" /> BT Wrap<br />'+
                       '                 <input type="checkbox" class="ddl_platform" name="ddl_platform_name3_'+ newNum +'" value="1" title="Macquarie CMA" /> Macquarie CMA<br />'+
                       '                 <input type="checkbox" class="ddl_platform" name="ddl_platform_name4_'+ newNum +'" value="1" title="AMP" /> AMP<br />'+
                       '                 <input type="checkbox" visible="false" class="ddl_platform" name="ddl_platform_name5_'+ newNum +'" value="1" title="Margin Lender" /> Margin Lender'+
                       '             </div>'+
                       '             </td>'+
                       '         </tr>'+
                       '             <tr>'+
                       '                 <td width="152"><b>Investor No. / Account No.:</b></td>'+
                       '                 <td><input type="text" name="txt_investornumber1_'+ newNum +'" id="txt_investornumber1_'+ newNum +'" class="floatLeft indentRight indentTop indentBottom" value="" /></td>'+
                       '             </tr>'+
                       '             <tr>'+
                       '                 <td><b>Adviser No. / Code:</b></td>'+
                       '                 <td><input type="text" name="txt_advisernumber1_'+ newNum +'" id="txt_advisernumber1_'+ newNum +'" class="floatLeft indentRight indentTop indentBottom" value="" /></td>'+
                       '             </tr>'+
                       '             <tr><td><div class="spacer-5"></div></td></tr>'+
                       '     </table>';
 		$('#linkedAccounts').append(newForm);
		$('#numform').val(newNum);
		
         if (newNum == 5)
             $('#btnAdd').attr('disabled','disabled');
	});
	
	$('.deleter').live('click',function(){
		var tid = $(this).attr('id').replace('del','');
		$('#tab'+tid).remove();	
	});
	$('.sigdeleter').live('click',function(){
		var tid = $(this).attr('id').replace('delsig','');
		$('#sigtab'+tid).remove();	
	});
	
	/********* SIGFORM **********/
	$('#sigform').live('click',function(){
		if($(this).is(':checked')){
			$('#addsigform').css('visibility','visible');
		}else{
			$('#addsigform').css('visibility','hidden');	
		}
	});
	
	$('#addsigform').live('click',function(){
		 var num     = parseInt($('#numsigform').val());
		 var newNum  = new Number(num + 1);
		 
		 var newForm = '<table width="100%" cellspacing="5" style="border:1px #d1d1d1 dashed"><tr><td colspan="4"><label>ACCOUNT SIGNATORY '+newNum+'</label></td></tr>'+
						'<tr>'+
						'	<td colspan="4">'+
						'		Mr <input type="radio" name="sigtitle'+newNum+'" id="sigtitle_mr'+newNum+'" value="Mr" />'+
						'		Mrs <input type="radio" name="sigtitle'+newNum+'" id="sigtitle_mrs'+newNum+'" value="Mrs" />'+
						'		Ms <input type="radio" name="sigtitle'+newNum+'" id="sigtitle_ms'+newNum+'" value="Ms" />'+
						'		Miss <input type="radio" name="sigtitle'+newNum+'" id="sigtitle_miss'+newNum+'" value="Miss" />'+
                        '       Other <input type="text" name="sigtitle_other'+newNum+'" id="sigtitle_other'+newNum+'" class="indentRight indentBottom" style="margin-left: 5px; width:125px" value="" />'+
						'	</td>'+
						'</tr>'+
                        '<tr>'+
                        '	<td colspan="4">'+
                        '    	<strong>Title if Corporate Trustee:</strong> <div style="height:5px"></div>'+
                        '        Director <input type="radio" name="corp_sigtitle'+newNum+'" id="sigtitle_dir'+newNum+'" value="Director" />'+
                        '        Secretary <input type="radio" name="corp_sigtitle'+newNum+'" id="sigtitle_sec'+newNum+'" value="Secretary" />'+
						'		Other <input type="text" name="corp_sigtitle_other'+newNum+'" id="sigtitle_other'+newNum+'" class="indentRight indentBottom" style="margin-left: 5px; width:155px" value="" />'+
                        '   	</td>'+
                        '</tr>'+
						'<tr>'+
						'	<td style="width:20%"><b>Date of Birth:</b></td>'+
						'	<td style="width:35%;"><input type="text" name="txt_sig_dob'+newNum+'" id="txt_sig_dob'+newNum+'" class="datepicker indentRight " value="" /></td>'+
						'</tr>'+
						'<tr>'+
						'	<td style="width:100px;"><b>Family Name:</b></td><td><input type="text" name="txt_familyname'+newNum+'" id="txt_familyname'+newNum+'" class="Width250 indentRight  "  value="" /></td>'+
						'	<td><b>Given Name/s:</b></td><td><input type="text" name="txt_givenname'+newNum+'" id="txt_givenname'+newNum+'" class="Width250  indentRight  " value="" /></td>'+
						'</tr>'+
						'<tr>'+
						'	<td colspan="4">'+
						'      <label for="title">Residential Address (mandatory, a PO Box, RMB or c/ - is not sufficient):</label>'+
                        '        <input type="checkbox" name="chk_res_sameasbilling'+newNum+'" id="samebilling'+newNum+'" /> <label for="title">Please tick ( &radic; ) if mailing address is same. </label>'+
						'	</td>'+
						'</tr>'+
                        '<tr>'+
						'	<td><b>Address:</b></td><td colspan="3"><input type="text" name="txt_sig_address'+newNum+'" id="txt_sig_address'+newNum+'" style="width:95%" class=" indentRight  " value="" /></td>'+
						'</tr>'+
						'<tr>'+
						'	<td><b>Suburb:</b></td><td><input type="text" name="txt_sig_suburb'+newNum+'" id="txt_sig_suburb'+newNum+'" class="Width250  indentRight  " value="" /></td>'+
						'	<td><b>State:</b></td><td><input type="text" name="txt_sig_state'+newNum+'" id="txt_sig_state'+newNum+'" class="Width250  indentRight  " value="" /></td>'+
						'</tr>'+
						'<tr>'+
						'	<td><b>Postcode:</b></td><td><input type="text" name="txt_sig_postcode'+newNum+'" id="txt_sig_postcode'+newNum+'" class="Width250  indentRight  " value="" /></td>'+
						'	<td><b>Country:</b></td><td><input type="text" name="txt_sig_country'+newNum+'" id="txt_sig_country'+newNum+'" class="Width250  indentRight  " value="" /></td>'+
						'</tr>'+
						'<tr>'+
						'	<td><b>Occupation:</b></td><td><input type="text" name="txt_sig_occupation'+newNum+'" id="txt_sig_occupation'+newNum+'" class="Width250  indentRight  " value="" /></td>'+
						' 	<td><b>Employer:</b></td><td ><input type="text" name="txt_sig_employer'+newNum+'" id="txt_sig_employer'+newNum+'" class="Width250  indentRight  " value="" /></td>'+
						'</tr>'+
						'<tr>'+
						'<td style="padding:0" colspan="4">'+
						'	<table>'+
						'		<tr>'+
						'			<td>Main country of residence, if not Australia:</td><td><input type="text" name="txt_sig_maincountry'+newNum+'" id="txt_sig_maincountry'+newNum+'" class="indentRight  " value="" /></td>'+
						'		</tr>'+
						'	</table>'+
						'</td>'+
						'</tr>'+
						'<tr>'+
						'	<td><b>Contact Ph:</b></td><td><input type="text" name="txt_sig_phone'+newNum+'" id="txt_sig_phone'+newNum+'" class=" indentRight  " value="" /></td>'+
						'	<td><b>Fax:</b></td><td><input type="text" name="txt_sig_fax'+newNum+'" id="txt_sig_fax'+newNum+'" class=" indentRight  " value="" /></td>'+
						'</tr>'+
						'<tr>'+
						'	<td><b>Alternate Ph:</b></td><td><input type="text" name="txt_sig_alternatephone'+newNum+'" id="txt_sig_alternatephone'+newNum+'" class=" indentRight  " value="" /></td>'+
						'	<td><b>Email:</b></td><td><input type="text" name="txt_sig_email'+newNum+'" id="txt_sig_email'+newNum+'" class="Width250 indentRight  " value="" /></td>'+
						'</tr>'+
						'<tr>'+
						'	<td><b>ID Type</b></td><td colspan="3"><input type="checkbox" name="chk_sig_driverslicense'+newNum+'" id="txt_sig_driverslicense'+newNum+'" value="1" /> Drivers License'+
						'		<input type="checkbox" name="chk_sig_passport'+newNum+'" id="chk_sig_passport'+newNum+'" value="1" /> Passport</td>'+
						'</tr>'+
						'<tr>'+
						'	<td><b>Document Issuer:</b></td><td style="width:30%" ><input type="text" name="txt_sig_documentissuer'+newNum+'" id="txt_sig_documentissuer'+newNum+'" class="Width250  indentRight  " value="" /></td>'+
						'</tr>'+
						'<tr>'+
						'	<td><b>Issue Date:</b></td><td><input type="text" name="txt_sig_issuedate'+newNum+'" id="txt_sig_issuedate'+newNum+'" class="datepicker indentRight" value="" /> </td>'+
						'	<td><b>Expiry Date:</b></td><td><input type="text" name="txt_sig_expiracydate'+newNum+'" id="txt_sig_expiracydate'+newNum+'" class="datepicker indentRight  " value="" /></td>'+
						'</tr>'+
						'<tr>'+
						'	<td style="width:150px"><b>Document Number:</b></td><td><input type="text" name="txt_sig_documentnumber'+newNum+'" id="txt_sig_documentnumber'+newNum+'" class=" Width250 indentRight  " value="" /></td>'+
                        '    <td colspan="2"><label>Certified copy attached</label><input type="checkbox" name="cca'+newNum+'" id="cca'+newNum+'" value="1" /></td>'+
						'</tr>'+
                        '</table>';
		 
		 $('#sigformHolder').append(newForm);
 		 $('#numsigform').val(newNum);
	});
	
	$('#brokerform').live('click',function(){
		if($(this).is(':checked')){
			$('#addbroker').css('visibility','visible');
		}else{
			$('#addbroker').css('visibility','hidden');	
		}
	});
	
	$('#addbroker').live('click',function(){
		 var num     = parseInt($('#numbrokerform').val());
		 var newNum  = new Number(num + 1);
		 
		 var newForm = '<table style="border:1px #d1d1d1 dashed">'+
                       '         <tr>'+
                       '             <td colspan="2">Name of existing Sponsoring Broker: <input type="text" name="txt_chs_brokername'+newNum+'" id="txt_chs_brokername'+newNum+'" class="" value="" style="margin-left:10px; width:200px" /></td>'+
                       '         </tr>'+
                       '         <tr><td><div class="spacer-5"></div></td></tr>'+
                       '         <tr>'+
                       '             <td>Existing Broker PID: <input type="text" name="txt_chs_pid'+newNum+'" id="txt_chs_pid'+newNum+'" class=" " value=""  style="margin-left:10px; width:90px"  /></td>'+
                       '             <td>Client HIN: <input type="text" name="txt_chs_hin'+newNum+'" id="txt_chs_hin'+newNum+'" class=" " value=""  style="margin-left:10px; width:85px"  /></td>'+
                       '         </tr>'+
					   '         <tr><td><div class="spacer-5"></div></td></tr>'+
                       '</table>';
		
		$('#brokerHolder').append(newForm);
 		$('#numbrokerform').val(newNum);
	});
	
	/**** POPUPS ****/
	//$('#popup3').css('display','block');
	
	$('a.popupwin').click(function(){
		var pid = $(this).attr('id').replace('p','');
		
		$('#popup'+pid).css('display','block').fadeIn('fast');
		
	});
	
	
	$('.popcloser').live('click', function(){
		var pid = $(this).attr('id').replace('cp','');	
		$('#popup'+pid).fadeOut('fast');
	});
	
	
	$('#addexclu').live('click',function(){
		 var nu     = parseInt($('#excluNum').val());
		
		 var newN  = new Number(nu + 1);
		 if(newN % 2){
			cls = 'even'; 
		 }else{
			cls = 'odd';  
		 }
		 var newForm = '<tr class="'+cls+'">'+
                       ' <td style="width:163px; height:25px">'+
					   '    <input type="text" name="cac'+newN+'" style="width:163px; padding:0; margin:0" id="cac'+newN+'" value="" />'+
					   ' </td>'+
					   ' <td style="width:220px; height:25px">'+
					   ' 	<input type="text" name="cn'+newN+'" style="width:220px; padding:0; margin:0" id="cn'+newN+'" value="" />'+
					   ' </td>'+
					   ' <td width="30%">'+
					   '	<select name="et'+newN+'" id="et'+newN+'">'+
					   ' 		<option value="">Don\'t Sell</option>'+
					   '		<option value="">Don\'t Buy</option>'+
					   '		<option value="">Don\'t Buy or Sell</option>'+
					   '	</select>'+
					   ' </td>'+
					   '</tr>';
		 
		 $('#exclusions').append(newForm);
 		 $('#excluNum').val(newN);
	});
});