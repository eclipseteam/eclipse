﻿<%@ Page Title="e-Clipse Online Portal" EnableViewState="true" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="MISHoldingVsTransactions.aspx.cs" Inherits="eclipseonlineweb.MISHoldingVsTransactions" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                                ID="btnDownload" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="MIS Holdings vs. Transactions"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <c1:C1GridView ShowFilter="true" OnFiltering="Filter" RowStyle-Font-Size="Smaller"
                AllowSorting="true" OnSorting="SortMainList" FilterStyle-Font-Size="Smaller"
                HeaderStyle-Font-Size="Smaller" EnableTheming="true" Width="100%" ID="MISAccountList"
                CallbackSettings-Action="None" OnPageIndexChanging="MISAccountList_PageIndexChanging"
                PagerSettings-Mode="Numeric" runat="server" AutogenerateColumns="False" AllowPaging="true"
                PageSize="15" ClientSelectionMode="None" CallbackSettings-Mode="Partial" OnSelectedIndexChanging="MISAccountList_SelectedIndexChanging">
                <SelectedRowStyle />
                <Columns>
                    <c1:C1BoundField DataField="MISCID" SortExpression="MISCID" HeaderText="MISCID" Visible="false">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1BoundField>
                    <c1:C1HyperLinkField DataTextField="ClientID" SortExpression="ClientID" HeaderText="Client ID"
                        DataNavigateUrlFields="ClientCID" DataNavigateUrlFormatString="../ClientViews/ClientMainView.aspx?ins={0}">
                        <ItemStyle HorizontalAlign="Left" />
                    </c1:C1HyperLinkField>
                    <c1:C1BoundField DataField="ClientName" SortExpression="ClientName" HeaderText="Account Name">
                        <ItemStyle HorizontalAlign="Left" />
                    </c1:C1BoundField>
                    <c1:C1BoundField DataField="AccountType" SortExpression="AccountType" HeaderText="Account Type">
                        <ItemStyle HorizontalAlign="Left" />
                    </c1:C1BoundField>
                    
                    <c1:C1BoundField DataField="Code" SortExpression="Code" HeaderText="Fund Code">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1BoundField>
                    <c1:C1BoundField DataField="Description" SortExpression="Description" HeaderText="Fund Description">
                        <ItemStyle HorizontalAlign="Left" />
                    </c1:C1BoundField>
                  
                    <c1:C1BoundField DataField="Amount" SortExpression="Amount" DataFormatString="C"
                        HeaderText="Transactions ($)">
                        <ItemStyle HorizontalAlign="Right" />
                    </c1:C1BoundField>
                    <c1:C1BoundField DataField="Shares" SortExpression="Shares" DataFormatString="N4"
                        HeaderText="Transactions (Units)">
                        <ItemStyle HorizontalAlign="Right" />
                    </c1:C1BoundField>
                     <c1:C1BoundField DataField="Holding" SortExpression="Holding" DataFormatString="N4"
                        HeaderText="Holdings  (Units)">
                        <ItemStyle HorizontalAlign="Right" />
                    </c1:C1BoundField>
                     <c1:C1BoundField DataField="Diff" SortExpression="Diff" DataFormatString="N4"
                        HeaderText="Difference  (Units)">
                        <ItemStyle HorizontalAlign="Right" />
                    </c1:C1BoundField>
                  
                    <c1:C1BoundField DataField="Error" SortExpression="Error" HeaderText="Error">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1BoundField>
                </Columns>
            </c1:C1GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
