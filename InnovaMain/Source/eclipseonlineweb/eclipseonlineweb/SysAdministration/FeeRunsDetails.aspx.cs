﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.CM.Organization.Data;
using CarlosAg.ExcelXmlWriter;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using Oritax.TaxSimp.CM.OrganizationUnit;

namespace eclipseonlineweb
{
    public partial class FeeRunsDetails : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void GetBMCData()
        {
            this.cid = Request.QueryString["ID"].ToString();
            Guid ID = new Guid(cid);
            IBrokerManagedComponent bmc =  UMABroker.GetBMCInstance(ID) as IBrokerManagedComponent;
            FeeRunTransactionsDetailsDS feeRunTransactionsDetailsDS = new Oritax.TaxSimp.Common.Data.FeeRunTransactionsDetailsDS();
            bmc.GetData(feeRunTransactionsDetailsDS);
            this.PresentationData = feeRunTransactionsDetailsDS;
            this.lblFeeRunDetails.Text = feeRunTransactionsDetailsDS.FeeRunDetails;

            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            
            if (!Page.IsPostBack)
            {
                this.C1ComboBoxFeeTemplates.DataSource = organization.FeeEnttyList.OrderBy(feetemp => feetemp.Description);
                C1ComboBoxFeeTemplates.DataTextField = "Description";
                C1ComboBoxFeeTemplates.DataValueField = "ID";
                C1ComboBoxFeeTemplates.DataBind();
                this.btnUseLocalFee.Checked = feeRunTransactionsDetailsDS.UseLocalFee;
            }

            UMABroker.ReleaseBrokerManagedComponent(organization);
            UMABroker.ReleaseBrokerManagedComponent(bmc);
        }


        protected void DownloadXLS(object sender, EventArgs e)
        {
            FeeRunTransactionsDetailsByFeeBreakDownDS feeBreakDown = new FeeRunTransactionsDetailsByFeeBreakDownDS();
            Guid ID = new Guid(cid);
            IBrokerManagedComponent bmc = UMABroker.GetBMCInstance(ID) as IBrokerManagedComponent;
            bmc.GetData(feeBreakDown);

            DataSet excelDataset = new DataSet();
            DataView entityView = new DataView(feeBreakDown.Tables[FeeRunTransactionsDetailsByFeeBreakDownDS.FEERUNSDETAILSTRANSACTIONTABLE]);
            entityView.Sort = FeeRunTransactionsDetailsByFeeBreakDownDS.CLIENTNAME +" ASC";
            DataTable entityTable = entityView.ToTable();
            entityTable.Columns.Remove(FeeRunTransactionsDetailsByFeeBreakDownDS.ID);
            entityTable.Columns.Remove(FeeRunTransactionsDetailsByFeeBreakDownDS.CLIENTCID);
            entityTable.Columns.Remove(FeeRunTransactionsDetailsByFeeBreakDownDS.ADVISERCID);
            entityTable.Columns.Remove(FeeRunTransactionsDetailsByFeeBreakDownDS.FEERUNID);
            entityTable.Columns.Remove(FeeRunTransactionsDetailsByFeeBreakDownDS.CLENTTYPE);
            entityTable.Columns.Remove(FeeRunTransactionsDetailsByFeeBreakDownDS.MONTHINT);
            entityTable.Columns.Remove(FeeRunTransactionsDetailsByFeeBreakDownDS.TRANSACTIONDATE);
 
            excelDataset.Merge(entityTable, true, MissingSchemaAction.Add);
            ExcelHelper.ToOpenExcel(excelDataset, "FeeBreakDown-FUM-" + DateTime.Today.ToString("dd-MMM-yyyy") + "-" + ".xlsx", this.Page.Response);
        }

        protected void DownloadXLSHoldings(object sender, EventArgs e)
        {
            FeeRunTransactionsDetailsByHoldingsDS feeBreakDown = new FeeRunTransactionsDetailsByHoldingsDS();
            Guid ID = new Guid(cid);
            IBrokerManagedComponent bmc = UMABroker.GetBMCInstance(ID) as IBrokerManagedComponent;
            bmc.GetData(feeBreakDown);

            DataSet excelDataset = new DataSet();
            DataView entityView = new DataView(feeBreakDown.Tables[FeeRunTransactionsDetailsByHoldingsDS.FEEHOLDINGTABLE]);
            entityView.Sort = FeeRunTransactionsDetailsByHoldingsDS.CLIENTNAME + " ASC";
            DataTable entityTable = entityView.ToTable();
            entityTable.Columns.Remove(FeeRunTransactionsDetailsByHoldingsDS.ID);
            entityTable.Columns.Remove(FeeRunTransactionsDetailsByHoldingsDS.CLIENTCID);
            entityTable.Columns.Remove(FeeRunTransactionsDetailsByHoldingsDS.ADVISERCID);
            entityTable.Columns.Remove(FeeRunTransactionsDetailsByHoldingsDS.FEERUNID);
            entityTable.Columns.Remove(FeeRunTransactionsDetailsByHoldingsDS.CLENTTYPE);
            entityTable.Columns.Remove(FeeRunTransactionsDetailsByHoldingsDS.MONTHINT);
            entityTable.Columns.Remove(FeeRunTransactionsDetailsByHoldingsDS.PRODUCTID);
            
            excelDataset.Merge(entityTable, true, MissingSchemaAction.Add);
            ExcelHelper.ToOpenExcel(excelDataset, "FeeHoldings-" + DateTime.Today.ToString("dd-MMM-yyyy") + "-" + ".xlsx", this.Page.Response);
        }

        protected void RadioCheckChanged_Clicked(Object sender, EventArgs e)
        {
            UMABroker.SaveOverride = true;

            this.cid = Request.QueryString["ID"].ToString();
            GroupFeeTransactionDetailsDS feeTransactionDetailsDS = new GroupFeeTransactionDetailsDS();
            feeTransactionDetailsDS.FeeOperationType = FeeOperationType.UseLocalFee;
            feeTransactionDetailsDS.UseLocalFee = this.btnUseLocalFee.Checked;
            this.SaveData(cid, feeTransactionDetailsDS);
        }

        public override void LoadPage()
        {
            base.LoadPage();

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
                ((SetupMaster)Master).IsAdmin = "1";
            //Here we get the page name
            string PageName = Request.QueryString["PageName"];
            btnBack.Visible = !string.IsNullOrEmpty(PageName);
        }
        protected void btnBack_Click(object sender, EventArgs args)
        {
            Response.Redirect("~/SysAdministration/FeeRuns.aspx");
        }
        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e){ }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "details")
            {
                //GridDataItem gDataItem = e.Item as GridDataItem;
                //string ins = gDataItem["CLIENTCID"].Text;                
                //string ID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"].ToString();
                string[] IdIns = e.CommandArgument.ToString().Split(',');
                string PrePageId = Request.QueryString["ID"];
                string PrePageIns=Request.QueryString["INS"];
                Response.Redirect("~/SysAdministration/FeeTransactionDetailsGlobal.aspx?ID=" + IdIns[0] + "&INS=" + IdIns[1] + "&PageName=" + "FeeRunDetail" + "&PrePageID=" + PrePageId + "&PrePageINS=" + PrePageIns);
            }
            else if (e.CommandName.ToLower() == "delete")
            {
                UMABroker.SaveOverride = true; 

                GridDataItem gDataItem = e.Item as GridDataItem;
                Guid ID = new Guid(gDataItem["FEERUNID"].Text);
                string ClientID = gDataItem["CLIENTCID"].Text;
                GroupFeeTransactionDetailsDS feeTransactionDetailsDS = new GroupFeeTransactionDetailsDS();
                feeTransactionDetailsDS.DataSetOperationType = DataSetOperationType.DeletSingle;
                feeTransactionDetailsDS.FeeTransactionID = ID;
                feeTransactionDetailsDS.ClientID = new Guid(ClientID);
                this.SaveData(ID.ToString(), feeTransactionDetailsDS); 
                GetBMCData();
                this.PresentationGrid.Rebind();
            }
            else if (e.CommandName.ToLower() == "update")
            {
                GridEditFormItem EditForm = (GridEditFormItem)e.Item;
                GridDataItem gDataItem = e.Item as GridDataItem;

                UMABroker.SaveOverride = true;
               
                RadComboBox combo = (RadComboBox)EditForm["AdviserListingCombo"].Controls[0];
                Guid feeTranID = (Guid)EditForm.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"];

                DataRow[] selectedRow = this.PresentationData.Tables[FeeRunTransactionsDetailsDS.FEERUNSDETAILSTRANSACTIONTABLE].Select("ID ='" + feeTranID.ToString() + "'");
                Guid ID = new Guid(selectedRow[0]["CLIENTCID"].ToString());
                Guid feeRunID = new Guid(selectedRow[0]["FEERUNID"].ToString());

                Guid adviserID = new Guid(combo.SelectedValue);
                OrganizationUnitCM iorgunit = this.UMABroker.GetBMCInstance(ID) as OrganizationUnitCM;
                OrganizationUnitCM adviserUnit = this.UMABroker.GetBMCInstance(adviserID) as OrganizationUnitCM;
                iorgunit.ConfiguredParent = new IdentityCM(adviserUnit.Clid.ToString(), adviserUnit.Csid.ToString());
                iorgunit.ConfiguredParent.Cid = adviserUnit.CID;
                FeeTransactionDetailsDS feeTransactionDetailsDS = new Oritax.TaxSimp.Common.Data.FeeTransactionDetailsDS();
                IBrokerManagedComponent feeRun = this.UMABroker.GetBMCInstance(feeRunID);
                feeTransactionDetailsDS.AdviserCID = adviserUnit.CID; 
                feeTransactionDetailsDS.AdviserName = adviserUnit.FullName(); 
                feeTransactionDetailsDS.AdviserID = adviserUnit.ClientId;
                feeTransactionDetailsDS.FeeTransactionID = feeTranID; 
                feeTransactionDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                feeRun.SetData(feeTransactionDetailsDS); 
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.PresentationGrid.Rebind();
            }
        }

        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e){}
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e){}
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e){}
        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("FeeRuns".Equals(e.Item.OwnerTableView.Name))
            {
                GridEditFormInsertItem EditForm = (GridEditFormInsertItem)e.Item;

                UMABroker.SaveOverride = true; 

                //string description = ((TextBox)(((GridEditableItem)(e.Item))["DESCRIPTION"].Controls[0])).Text;
                string shortname = ((TextBox)(((GridEditableItem)(e.Item))["SHORTNAME"].Controls[0])).Text;
                int year = int.Parse(((TextBox)(((GridEditableItem)(e.Item))["YEAR"].Controls[0])).Text);
                RadComboBox combo = (RadComboBox)EditForm["MONTHCOMBO"].Controls[0];
                int month = int.Parse(combo.SelectedItem.Value); 

                ILogicalModule created = null;
                ILogicalModule organization = null;

                IOrganization org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                created = this.UMABroker.CreateLogicalCM(new Guid("AE8DCE8C-0343-461C-8152-7FA4F4E22427"), shortname.ToUpper() + " - " + DateTime.Today.ToLongDateString(), Guid.Empty, null);

                organization = this.UMABroker.GetLogicalCM(org.CLID);
                organization.AddChildLogicalCM(created);
                IBrokerManagedComponent iBMC = (IBrokerManagedComponent)created[created.CurrentScenario];

                FeeRunDetailsDS feeRunDetailsDS = new Oritax.TaxSimp.Common.Data.FeeRunDetailsDS(); 
                DataTable feeDetailsTable =   feeRunDetailsDS.Tables[FeeRunDetailsDS.FEERUNSDETAILSTABLE]; 

                DataRow feeDetailRow = feeDetailsTable.NewRow(); 
                //feeDetailRow[FeeRunDetailsDS.DESCRIPTION] = description;
                feeDetailRow[FeeRunDetailsDS.SHORTNAME] = shortname;
                feeDetailRow[FeeRunDetailsDS.YEAR] = year;
                feeDetailRow[FeeRunDetailsDS.MONTHINT] = month;
                feeDetailRow[FeeRunDetailsDS.RUNTYPEENUM] = FeeRunType.Global;
                feeDetailsTable.Rows.Add(feeDetailRow);

                iBMC.SetData(feeRunDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.PresentationGrid.Rebind();
            }
            
        }

        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e){}

        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;
                GridDropDownListColumnEditor editor = null;
                IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                OrganisationListingDS OrganisationListingDS = new OrganisationListingDS();
                OrganisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.AdvisersBasicList;
                organization.GetData(OrganisationListingDS); 

                if ("FeeRuns".Equals(e.Item.OwnerTableView.Name))
                {
                    IDictionary<Enum, string> servicesValue = Enumeration.ToDictionary(typeof(ServiceTypes));
                    editor = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("AdviserListingCombo"));

                    DataView view = new DataView(OrganisationListingDS.Tables[OrganisationListingDS.ADVISERLISTINGTABLE]);
                    view.Sort = "ENTITYNAME_FIELD ASC";
                    DataTable adviserListingTable = view.ToTable();

                    editor.DataSource = adviserListingTable;
                    editor.DataTextField = "ENTITYNAME_FIELD";
                    editor.DataValueField = "ENTITYCIID_FIELD";
                    editor.DataBind();
                }

                UMABroker.ReleaseBrokerManagedComponent(organization);
            }
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DataView View = new DataView(this.PresentationData.Tables[FeeRunTransactionsDetailsDS.FEERUNSDETAILSTRANSACTIONTABLE]);
            View.Sort = FeeRunTransactionsDetailsDS.FEERUNDATE + " DESC";
            this.PresentationGrid.DataSource = View.ToTable();
        }

        MemoryStream WriteToStream(HSSFWorkbook hssfworkbook)
        {
            //Write the stream data of workbook to the root directory
            MemoryStream file = new MemoryStream();
            hssfworkbook.Write(file);
            return file;
        }

        #region Telerik Grid Products Config

        protected void GridConfiguredFees_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e) { }
        protected void GridConfiguredFees_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                Guid rowID = new Guid(gDataItem["ID"].Text);
                UMABroker.SaveOverride = true;
                IBrokerManagedComponent ibmc = this.UMABroker.GetBMCInstance(new Guid(cid)) as IBrokerManagedComponent;
                FeeRunTransactionsDetailsDS feeRunTransactionsDetailsDS = new FeeRunTransactionsDetailsDS();
                feeRunTransactionsDetailsDS.FeeRunTransactionsOperation = FeeRunTransactionsOperation.DeleteFeeTemplate;
                feeRunTransactionsDetailsDS.FeeTemplateID = rowID;
                ibmc.SetData(feeRunTransactionsDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.GridFeeTemplate.Rebind();
            }
        }
        protected void GridConfiguredFees_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) { }
        protected void GridConfiguredFees_ItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void GridConfiguredFees_ItemInserted(object source, GridInsertedEventArgs e) { }
        protected void GridConfiguredFees_InsertCommand(object source, GridCommandEventArgs e) { }
        protected void GridConfiguredFees_ItemCreated(object sender, GridItemEventArgs e) { }
        protected void GridConfiguredFees_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e) { }
        protected void GridConfiguredFees_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DataTable templateTable = this.PresentationData.Tables[FeeRunTransactionsDetailsDS.TEMPLATELISTTABLE];
            DataView templateTableView = new DataView(templateTable);
            templateTableView.Sort = FeeRunTransactionsDetailsDS.DESCRIPTION + " ASC";
            this.GridFeeTemplate.DataSource = templateTableView.ToTable();
        }

        protected void btnAddFeeTemplate_Click(object sender, EventArgs e)
        {
            UMABroker.SaveOverride = true;
            IBrokerManagedComponent ibmc = this.UMABroker.GetBMCInstance(new Guid(cid)) as IBrokerManagedComponent;
            FeeRunTransactionsDetailsDS feeRunTransactionsDetailsDS = new FeeRunTransactionsDetailsDS();
            feeRunTransactionsDetailsDS.FeeRunTransactionsOperation = FeeRunTransactionsOperation.AddFeeTemplate;
            DataTable templateListTable = feeRunTransactionsDetailsDS.Tables[FeeRunTransactionsDetailsDS.TEMPLATELISTTABLE];

            foreach (var item in C1ComboBoxFeeTemplates.CheckedItems)
            {
                DataRow row = templateListTable.NewRow();
                row[FeeRunTransactionsDetailsDS.ID] = new Guid(item.Value);
                row[FeeRunTransactionsDetailsDS.DESCRIPTION] = item.Text;

                templateListTable.Rows.Add(row);
            }

            ibmc.SetData(feeRunTransactionsDetailsDS);

            UMABroker.SetComplete();
            UMABroker.SetStart();
            GetBMCData();
            this.GridFeeTemplate.Rebind();
        }
        #endregion 

        protected void rbExportBankwestOutput_Click(object sender, EventArgs e)
        {
            GroupFeeBankwestExportDS groupFeeBankwestExportDS = new GroupFeeBankwestExportDS();
            Guid ID = new Guid(Request.QueryString["ID"].ToString());
            IBrokerManagedComponent bmc = UMABroker.GetBMCInstance(ID) as IBrokerManagedComponent;
            bmc.GetData(groupFeeBankwestExportDS);

            FileStream fs =
            new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\BWA CMA Fee Debit Template.xls", FileMode.Open, FileAccess.Read);


            HSSFWorkbook hssfworkbook = new HSSFWorkbook(fs,true);
            fs.Close();
            hssfworkbook.ForceFormulaRecalculation = true;

            HSSFSheet sheet = ((HSSFSheet)hssfworkbook.GetSheetAt(0));

            IRow dateRow = sheet.GetRow(10);
            dateRow.Cells[1].SetCellValue(DateTime.Now);

            IRow narrationRow = sheet.GetRow(15);
            narrationRow.Cells[2].SetCellValue(groupFeeBankwestExportDS.Narration);

            DataView view = new DataView(groupFeeBankwestExportDS.Tables[GroupFeeBankwestExportDS.GROUPFEEEXPORTBANKWESTABLE]);
            view.RowFilter = GroupFeeBankwestExportDS.AMOUNT + " <> '0'";
            view.Sort = GroupFeeBankwestExportDS.ACCOUNTNAME + " ASC";
            DataTable table = view.ToTable(); 

            decimal totalFee =  groupFeeBankwestExportDS.Tables[GroupFeeBankwestExportDS.GROUPFEEEXPORTBANKWESTABLE].Select().Sum(row => (decimal)row[GroupFeeBankwestExportDS.AMOUNT]);

            int count = 20;
            //Insert Entries
            foreach (DataRow row in table.Rows)
            {
                double amount = Convert.ToDouble(row[GroupFeeBankwestExportDS.AMOUNT]);
                if (amount != 0)
                {
                    IRow newUmaFeeRow = sheet.GetRow(count);

                    ICell cell1 = newUmaFeeRow.Cells[0];
                    cell1.SetCellValue((String)row[GroupFeeBankwestExportDS.ACCOUNTNAME]);

                    ICell cell2 = newUmaFeeRow.Cells[1];
                    cell2.SetCellValue((String)row[GroupFeeBankwestExportDS.BSB]);

                    ICell cell3 = newUmaFeeRow.Cells[2];
                    cell3.SetCellValue((String)row[GroupFeeBankwestExportDS.ACCOUNTNO]);

                    ICell cell4 = newUmaFeeRow.Cells[3];
                    cell4.SetCellValue(Math.Round(amount,2));


                    ICell cell5 = newUmaFeeRow.Cells[4];
                    cell5.SetCellValue(groupFeeBankwestExportDS.Narration);
                    count++;
                }
                
            }

            string filename = groupFeeBankwestExportDS.Narration.ToUpper() +" - ["+DateTime.Now.ToString("dd/MM/yyyy").ToUpper()+"]";
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
            Response.Clear();

            Response.BinaryWrite(WriteToStream(hssfworkbook).GetBuffer());
            Response.Flush();
            Response.End();
        }
    }
}
