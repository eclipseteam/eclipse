﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    EnableEventValidation="true" AutoEventWireup="true" CodeBehind="DealerGroupCompanyDetail.aspx.cs"
    Inherits="eclipseonlineweb.SysAdministration.DealerGroupCompanyDetail" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register Src="../Controls/AddressDetailControl.ascx" TagName="AddressDetailControl"
    TagPrefix="uc7" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="../Controls/ContactMappingControl.ascx" TagName="ContactMappingControl"
    TagPrefix="uc5" %>
<%@ Register Src="../Controls/OrganizationUnitMappingControl.ascx" TagName="OrganizationUnitMappingControl"
    TagPrefix="uc5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Controls/DealerGroupCompanyControl.ascx" TagName="DealerGroupCompanyControl"
    TagPrefix="uc2" %>
<%@ Register Src="../Controls/BankAccountMappingControl.ascx" TagName="BankAccountMappingControl"
    TagPrefix="uc3" %>
<%@ Register TagPrefix="uc2" TagName="SecurityConfigurationControl" Src="~/Controls/SecurityConfigurationControl.ascx" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                        </td>
                        <td width="90%" align="right">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <span style="font-weight: bold;">Dealer Group</span>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadTabStrip ID="RadTabStrip1" runat="server" Skin="Vista" MultiPageID="RadMultiPage1"
                SelectedIndex="0">
                <Tabs>
                    <telerik:RadTab Text="Dealer Group Company" runat="server" Selected="True" Value="DealerGroupCompanyHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Address" runat="server" Visible="true" Value="AddressHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Accounts" runat="server" Visible="true" Value="AccountsHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Directors" runat="server" Visible="true" Value="DirectorsHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Signatories" runat="server" Visible="true" Value="SignatoriesHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Share Holders" runat="server" Visible="true" Value="ShareHoldersHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Security" runat="server" Visible="true" Value="SecurityHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Documents" runat="server" Visible="true" Value="DocumentsHeader">
                    </telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage runat="server" ID="RadMultiPage1" SelectedIndex="0">
                <telerik:RadPageView runat="server" ID="ApBasic">
                    <uc2:DealerGroupCompanyControl ID="DealerGroupCompanyControl1" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="ApAddress" runat="server">
                    <fieldset style="width: 98%">
                        <div style="text-align: left; float: none; border: 1px; background-color: White;
                            height: 25px; position: relative; vertical-align: middle; margin: 0; padding: 0;">
                            <telerik:RadButton ID="btnSave" runat="server" Width="34px" Height="32px" ToolTip="Save Changes"
                                OnClick="btnSave_Click">
                                <Image ImageUrl="~/images/Save-Icon.png" />
                            </telerik:RadButton>
                        </div>
                    </fieldset>
                    <br />
                    <uc7:AddressDetailControl ID="AddressDetailControl" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="ApBankAccounts" runat="server">
                    <uc3:BankAccountMappingControl ID="BankAccounts" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="ApDirectors" runat="server">
                    <uc5:ContactMappingControl ID="Directors" runat="server" IndividualsType="Directors" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="ApSignatories" runat="server">
                    <uc5:ContactMappingControl ID="Signatories" runat="server" IndividualsType="Signatories" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="ApShareHolders" runat="server">
                    <uc5:OrganizationUnitMappingControl ID="ShareHolders" runat="server" UnitPropertyName="ShareHolders"
                        TitleText="Share Holders" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="ApSecurity" runat="server">
                    <uc2:SecurityConfigurationControl ID="SecurityConfiguration" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="ApDocuments" runat="server">
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
