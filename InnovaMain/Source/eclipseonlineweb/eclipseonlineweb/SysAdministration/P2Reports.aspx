﻿<%@ Page Language="C#"  MasterPageFile="AdminMaster.master" AutoEventWireup="true" CodeBehind="P2Reports.aspx.cs" 
Inherits="eclipseonlineweb.SysAdministration.P2Reports" %>

<%@ Register TagPrefix="uc1" TagName="P2Reports" Src="~/Controls/P2Reports.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:P2Reports ID="P2ReportsControl" runat="server" />
</asp:Content>
