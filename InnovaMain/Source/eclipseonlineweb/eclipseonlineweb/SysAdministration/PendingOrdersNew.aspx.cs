﻿using System;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb.SysAdministration
{
    public partial class PendingOrdersNew : UMABasePage
    {
        protected override void Intialise()
        {

        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        public override void LoadPage()
        {
            if (!IsPostBack)
            {

            }
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetAllOrders();
        }

        private void GetData(string orderID = null)
        {
            var orderUnit = new OrganizationUnit
            {
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = (Page as UMABasePage).GetCurrentUser()
            };



            //Orders Lists
            var ds = new OrderPadDS
            {
                Unit = orderUnit,
                CommandType = DatasetCommandTypes.Get,
                Command = (int)WebCommands.GetOrganizationUnitsByType,
            };
            if (!string.IsNullOrEmpty(orderID))
            {
                Guid orderId;
                Guid.TryParse(orderID, out orderId);
                if (orderId != Guid.Empty)
                {
                    ds.OrderId = orderId;
                }
            }

            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            //Orders Lists
            org.GetData(ds);

            UMABroker.ReleaseBrokerManagedComponent(org);
            ((UMABasePage)Page).PresentationData = ds;
        }
        private OrderPadDS GetOrderPadDs(string orderID = null)
        {
            if (!Page.IsPostBack || ((UMABasePage)Page).PresentationData == null)
            {
                GetData(orderID);
            }
            var ds = ((UMABasePage)Page).PresentationData as OrderPadDS;
            return ds;
        }
        private void GetAllOrders()
        {


            var Orderds = GetOrderPadDs();

            var _ordersView = Orderds.Tables[Orderds.OrdersTable.TableName].DefaultView;
            var _ssOrdersView = Orderds.Tables[Orderds.StateStreetTable.TableName].DefaultView;
            var _asxOrdersView = Orderds.Tables[Orderds.ASXTable.TableName].DefaultView;
            var _tdOrdersView = Orderds.Tables[Orderds.TermDepositTable.TableName].DefaultView;
            var _atCallOrdersView = Orderds.Tables[Orderds.AtCallTable.TableName].DefaultView;
            //_settledUnsettledView = ds.Tables[SettledUnsetteledList.TABLENAME].DefaultView;
            //sort by order id/number desc 
            _ordersView.RowFilter = GetFilter();
            _ordersView.Sort = string.Format("{0} DESC", Orderds.OrdersTable.ORDERID);
            PresentationGrid.DataSource = _ordersView;
            OrderDetails.Visible = false;
        }

        protected void btnSMA_OnClick(object sender, EventArgs e)
        {

            PresentationGrid.Rebind();
        }

        private string GetFilter()
        {
            PresentationGrid.MasterTableView.GetColumn("SMARequestID").Visible = PresentationGrid.MasterTableView.GetColumn("SMACallStatus").Visible = btnSMA.Checked;
            string typeFilter = string.Format("ClientManagementType = '{0}'", btnSMA.Checked ? btnSMA.Value : btnUMA.Value);
            return string.Format("{0} and Status not in ('{1}','{2}')", typeFilter, OrderStatus.Cancelled, OrderStatus.Complete);
        }

        protected void PresentationGrid_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var id = PresentationGrid.SelectedValue.ToString();
            SetOrderDetails(id);
        }


        private void SetOrderDetails(string ID)
        {
            var Orderds = GetOrderPadDs(ID);

            var _ssOrdersView = Orderds.Tables[Orderds.StateStreetTable.TableName].DefaultView;
            var _asxOrdersView = Orderds.Tables[Orderds.ASXTable.TableName].DefaultView;
            var _tdOrdersView = Orderds.Tables[Orderds.TermDepositTable.TableName].DefaultView;
            var _atCallOrdersView = Orderds.Tables[Orderds.AtCallTable.TableName].DefaultView;




            OrderDetails.Visible = true;

            SetControlValues(Orderds.OrdersTable);


            bool showStateStreet = _ssOrdersView.Count > 0;

            bool showASx = _asxOrdersView.Count > 0;
            bool showTD = _tdOrdersView.Count > 0;
            bool showAtCall = _atCallOrdersView.Count > 0;

            SSOrderDetails.DataSource = _ssOrdersView;
            SSOrderDetails.Rebind();
            SSOrderDetails.Visible = showStateStreet;

            ASXOrderDetails.DataSource = _asxOrdersView;
            ASXOrderDetails.Rebind();
            ASXOrderDetails.Visible = showASx;

            TDOrderDetails.DataSource = _tdOrdersView;
            TDOrderDetails.Rebind();
            TDOrderDetails.Visible = showTD;

            AtCallOrderDetails.DataSource = _atCallOrdersView;
            AtCallOrderDetails.Rebind();
            AtCallOrderDetails.Visible = showAtCall;
            
        }

        private void SetControlValues(OrderList ordersTable)
        {
            var row = ordersTable.Rows[0];

            TxtOrderID.Text = row[ordersTable.ORDERID].ToString();
            TxtStatus.Text = row[ordersTable.STATUS].ToString();
            TxtOrderType.Text = row[ordersTable.ORDERTYPE].ToString();

            TxtClientID.Text = row[ordersTable.CLIENTID].ToString();
            TxtItemType.Text = row[ordersTable.ORDERITEMTYPE].ToString();
            TxtRequestBy.Text = row[ordersTable.ORDERPREFEREDBY].ToString();


            TxtOrderAccountType.Text = row[ordersTable.ORDERACCOUNTTYPE].ToString();
            TxtValue.Text = row[ordersTable.VALUE].ToString();
            TxtBulkStatus.Text = row[ordersTable.ORDERBULKSTATUS].ToString();


            TxtTradeDate.Text = row[ordersTable.TRADEDATE].ToString();
            TxtCreatedDate.Text = row[ordersTable.CREATEDDATE].ToString();
            TxtUpdatedDate.Text = row[ordersTable.UPDATEDDATE].ToString();

            TxtCreatedBy.Text = row[ordersTable.CREATEDBY].ToString();
            if (btnSMA.Checked)
            {
                TxtSMARequestID.Text = row[ordersTable.SMAREQUESTID].ToString();
                TxtSMACallStatus.Text = row[ordersTable.SMACALLSTATUS].ToString();
            }

            TxtSMARequestID.Visible =TxtSMACallStatus.Visible = btnSMA.Checked;

        }
    }
}