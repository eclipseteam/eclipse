﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb.SysAdministration
{
    public partial class Advisor : UMABasePage
    {
        public override void LoadPage()
        {

        }

        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            return true;
        }

        protected void gd_Advisor_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);

            var advisorDs = new AdvisorDS
                                {
                                    CommandType = DatasetCommandTypes.Get,
                                    Command = (int)WebCommands.GetOrganizationUnitsByType,
                                    Unit = new OrganizationUnit
                                               {
                                                   CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                                                   Type = ((int)OrganizationType.Adviser).ToString(),
                                               }
                                };
            org.GetData(advisorDs);
            gd_Advisor.DataSource = advisorDs;
            UMABroker.ReleaseBrokerManagedComponent(org);
        }

        protected void GdAdvisorItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            var cID = new Guid(dataItem["Cid"].Text);
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    Response.Redirect("AdviserDetails.aspx?ins=" + cID);
                    break;
                case "membership":
                    string clientID = dataItem["ClientID"].Text;
                    string name = dataItem["Name"].Text;
                    Response.Redirect("MembershipDetail.aspx?CID=" + cID.ToString() + "&ClientID=" + clientID + "&orgtype=" + (int)OrganizationType.Adviser + "&Name=" + name);
                    break;
                case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var clid = new Guid(dataItem["Clid"].Text);
                    var csid = new Guid(dataItem["Csid"].Text);
                    DeleteAdviser(clid, csid);
                    break;
            }
        }

        /// <summary>
        /// Delete Adviser
        /// </summary>
        /// <param name="clid"></param>
        /// <param name="csid"></param>
        private void DeleteAdviser(Guid clid, Guid csid)
        {
            var ds = new AdvisorDS
                         {
                             CommandType = DatasetCommandTypes.Delete,
                             Unit = new OrganizationUnit
                                        {
                                            Clid = clid,
                                            Csid = csid,
                                            CurrentUser = GetCurrentUser(),
                                            Type = ((int)OrganizationType.Adviser).ToString(),
                                        },
                             Command = (int)WebCommands.DeleteOrganizationUnit
                         };

            SaveOrganizanition(ds);
            gd_Advisor.Rebind();
        }

        protected void btnAddAdvisor_Click(object sender, EventArgs e)
        {
            Response.Redirect("AdviserDetails.aspx", false);
        }




    }
}