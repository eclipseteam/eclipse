﻿using System;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CM.Group;

namespace eclipseonlineweb.SysAdministration
{
    public partial class OrgAccountant : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }
        public override void LoadPage()
        {
        }
        protected void gd_Accountant_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);

            AccountantDS accountantDs = new AccountantDS { CommandType = DatasetCommandTypes.Get, Command = (int)WebCommands.GetOrganizationUnitsByType };
            accountantDs.Unit = new OrganizationUnit
            {
                CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                Type = ((int)OrganizationType.Accountant).ToString(),

            };
            org.GetData(accountantDs);
            gd_Accountant.DataSource = accountantDs;
            UMABroker.ReleaseBrokerManagedComponent(org);
        }
        protected void gd_Accountant_InsertCommand(object source, GridCommandEventArgs e)
        {
        }
        protected void gd_Accountant_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;
            GridDataItem gDataItem = e.Item as GridDataItem;
            Guid CID = new Guid(gDataItem["Cid"].Text);
            string ClientID = gDataItem["ClientID"].Text;
            string name = gDataItem["TradingName"].Text;
            AccountantEntityType accountantEntityType = (AccountantEntityType)Enum.Parse(typeof(AccountantEntityType), gDataItem["AccountantType"].Text);

            switch (e.CommandName.ToLower())
            {
                case "edit":
                    switch (accountantEntityType)
                    {
                        case AccountantEntityType.AccountantIndividualControl:
                            Response.Redirect("AccountantSoleTraderDetail.aspx?ins=" + CID  +"&orgtype=" + (int)OrganizationType.Accountant);
                            break;
                        case AccountantEntityType.AccountantCompanyControl:
                            Response.Redirect("AccountantCompanyDetail.aspx?ins=" + CID);
                            break;
                        case AccountantEntityType.AccountantPartnershipControl:
                            Response.Redirect("AccountantPartnershipDetail.aspx?ins=" + CID);
                            break;
                        case AccountantEntityType.AccountantTrustControl:
                            Response.Redirect("AccountantTrustDetail.aspx?ins=" + CID);
                            break;
                    }
                    break;
                case "membership":
                    {
                        Response.Redirect("MembershipDetail.aspx?CID=" + CID.ToString() + "&ClientID=" + ClientID + "&orgtype=" + (int)OrganizationType.Accountant + "&Name=" + name);
                    }
                    break;
                case "update":
                    break;
                case "delete":
                    {
                        e.Item.Edit = false;
                        e.Canceled = true;
                        var clid = new Guid(gDataItem["Clid"].Text);
                        var csid = new Guid(gDataItem["Csid"].Text);
                        DeleteAccountant(clid, csid);
                    }
                    break;
            }
        }

        /// <summary>
        /// Delete Accountant
        /// </summary>
        /// <param name="clid"></param>
        /// <param name="csid"></param>
        private void DeleteAccountant(Guid clid, Guid csid)
        {
            var ds = new AccountantDS
            {
                CommandType = DatasetCommandTypes.Delete,
                Unit = new OrganizationUnit
                {
                    Clid = clid,
                    Csid = csid,
                    CurrentUser = GetCurrentUser(),
                    Type = ((int)OrganizationType.Accountant).ToString(),
                },
                Command = (int)WebCommands.DeleteOrganizationUnit
            };

            SaveOrganizanition(ds);
            gd_Accountant.Rebind();
        }

        protected void lnkbtnAddAccountant_Click(object sender, EventArgs e)
        {
            HideShowOrganizationType(true);

        }
        private void HideShowOrganizationType(bool show)
        {
            OVER.Visible = OrganizationTypePopup.Visible = show;
        }
        protected void btnNext_OnClick(object sender, EventArgs e)
        {
            if (rdoSoleTrader.Checked)
            {
                Response.Redirect("AccountantSoleTraderDetail.aspx");
            }
            else if (rdoCompany.Checked)
            {
                Response.Redirect("AccountantCompanyDetail.aspx");
            }
            else if (rdoPartnership.Checked)
            {
                Response.Redirect("AccountantPartnershipDetail.aspx");
            }
            else
            {
                Response.Redirect("AccountantTrustDetail.aspx");
            }
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            HideShowOrganizationType(false);
        }
    }
}