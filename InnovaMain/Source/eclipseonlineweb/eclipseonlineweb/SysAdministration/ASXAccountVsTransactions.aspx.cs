﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.Commands;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.DataSets;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class ASXAccountVsTransactions : UMABasePage
    {
        private DataView _entityView;

        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            GetData();
            var excelDataset = new DataSet();
            DataTable entityTable = _entityView.ToTable();
            
            excelDataset.Merge(entityTable, true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, "ASX Accounts-ALL HOLDING VS TRANS-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", Page.Response);
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        private void GetData()
        {
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var accountsHoldingsDs = new ClientsASXAccountsHoldingsDS
            {
                Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() },
                Command = (int)WebCommands.GetAllClientDetails,
            };
            if (organization != null)
            {
                organization.GetData(accountsHoldingsDs);
                UMABroker.ReleaseBrokerManagedComponent(organization);
            }
            _entityView = new DataView(accountsHoldingsDs.ASXHoldingTable);
            _entityView.Sort = accountsHoldingsDs.ASXHoldingTable.DIFFERENCE + " DESC";
            PresentationGrid.DataSource = _entityView.ToTable();
        }

        public override void LoadPage()
        {
            base.LoadPage();
            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
                sm.RegisterPostBackControl(btnDownload);
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void PresentationGrid_OnItemCreated(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridFilteringItem)) return;

            var filterItem = (GridFilteringItem)e.Item;
            foreach (GridColumn col in PresentationGrid.MasterTableView.Columns)
            {
                var colUniqueName = col.UniqueName;
                var txtbox = (TextBox)filterItem[colUniqueName].Controls[0];
                txtbox.Attributes.Add("onkeydown", "doFilter(this,event,'" + colUniqueName + "')");
            }
        }
    }
}
