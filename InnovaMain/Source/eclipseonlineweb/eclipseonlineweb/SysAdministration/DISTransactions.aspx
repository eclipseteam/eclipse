﻿<%@ Page Title="e-Clipse Online Portal" EnableViewState="true" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="DISTransactions.aspx.cs" Inherits="eclipseonlineweb.DISTransactions" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <telerik:RadButton runat="server" ID="btnDownload" OnClick="btnExport_Onclick">
                                <Image ImageUrl="~/images/download.png"></Image>
                            </telerik:RadButton>
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="Label1" Text="Distributions"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid ID="gvDistributionTranscation" runat="server" AutoGenerateColumns="False"
                PageSize="40" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true"
                GridLines="Horizontal" AllowAutomaticUpdates="true" AllowFilteringByColumn="true"
                OnNeedDataSource="gvDistribution_OnNeedDataSource" EnableViewState="true" ShowFooter="True"
                OnItemCommand="gvDistributionTranscation_OnitemCommand">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowFilteringByColumn="True" TableLayout="Fixed" Width="100%">
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" Display="False"
                            HeaderButtonType="TextButton" DataField="ID" UniqueName="ID">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="ClientCID" ReadOnly="true" HeaderText="ClientCID"
                            Display="False" HeaderButtonType="TextButton" DataField="ClientCID" UniqueName="ClientCID">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="ClientID" ReadOnly="true" HeaderText="ClientID"
                            HeaderStyle-Width="8%" ItemStyle-Width="8%"  HeaderButtonType="TextButton" FilterControlWidth="50px"
                            DataField="ClientID" UniqueName="ClientID">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="ClientName" ReadOnly="true" HeaderText="Client Name"
                            HeaderStyle-Width="19%" ItemStyle-Width="19%" HeaderButtonType="TextButton" DataField="ClientName"
                            UniqueName="ClientName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="“FundCode" ReadOnly="true" HeaderText="Fund Code"
                            HeaderStyle-Width="8%" ItemStyle-Width="8%"  HeaderButtonType="TextButton" FilterControlWidth="50px"
                            DataField="FundCode" UniqueName="FundCode">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="“FundName" ReadOnly="true" HeaderText="Fund Description"
                            HeaderStyle-Width="19%" ItemStyle-Width="19%" HeaderButtonType="TextButton" DataField="FundName"
                            UniqueName="FundName">
                        </telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn Visible="true" UniqueName="RecordDate" PickerType="DatePicker"
                            HeaderStyle-Width="12%" ItemStyle-Width="12%" HeaderText="Record Date" DataField="RecordDate"
                            DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridDateTimeColumn>
                        <telerik:GridDateTimeColumn Visible="true" UniqueName="PaymentDate" PickerType="DatePicker"
                            HeaderStyle-Width="12%" ItemStyle-Width="12%" HeaderText="Payment Date" DataField="PaymentDate"
                            DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridDateTimeColumn>
                        <telerik:GridBoundColumn SortExpression="RecodDate_Shares" ReadOnly="true" HeaderText="Shares"
                            HeaderStyle-Width="10%" ItemStyle-Width="10%"  DataFormatString="{0:f4}"
                            HeaderButtonType="TextButton" DataField="RecodDate_Shares" UniqueName="RecodDate_Shares">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridNumericColumn SortExpression="NetCashDistribution" HeaderText="Distribution Amount" 
                            HeaderStyle-Width="8%" ItemStyle-Width="8%"  HeaderButtonType="TextButton" FilterControlWidth="50px"
                            UniqueName="NetCashDistribution" DataField="NetCashDistribution" 
                            DataType="System.Decimal" DataFormatString="{0:C2}" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </telerik:GridNumericColumn>
                        <telerik:GridBoundColumn SortExpression="Status" ReadOnly="true" HeaderText="Status" 
                            HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderButtonType="TextButton" FilterControlWidth="50px"
                            DataField="Status" UniqueName="Status">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn CommandName="Delete" Text="Delete" ButtonType="LinkButton"
                            ConfirmText="Are you sure you want to remove?" HeaderStyle-Width="6%" ItemStyle-Width="6%">
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
