﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="SettledUnsettledTransactions.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.SettledUnsettledTransactions" %>

<%@ Register Src="../Controls/SettledUnsettledTransactions.ascx" TagName="SettledUnsettledTransactions"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:SettledUnsettledTransactions ID="SettledUnsettledTransactionsControl" runat="server" />
</asp:Content>
