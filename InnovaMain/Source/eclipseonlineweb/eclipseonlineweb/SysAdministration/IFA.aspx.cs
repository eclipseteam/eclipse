﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CM.Group;

namespace eclipseonlineweb.SysAdministration
{
    public partial class IFA : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }
        public override void LoadPage()
        {
        }
        protected void gd_IFA_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);

            IFADS ifaDs = new IFADS { CommandType = DatasetCommandTypes.Get, Command = (int)WebCommands.GetOrganizationUnitsByType };
            ifaDs.Unit = new OrganizationUnit
            {
                CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                Type = ((int)OrganizationType.IFA).ToString(),

            };
            org.GetData(ifaDs);
            gd_IFA.DataSource = ifaDs;
            UMABroker.ReleaseBrokerManagedComponent(org);
        }
        protected void gd_IFA_InsertCommand(object source, GridCommandEventArgs e)
        {
        }
        protected void gd_IFA_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;
            GridDataItem gDataItem = e.Item as GridDataItem;
            Guid CID = new Guid(gDataItem["Cid"].Text);
            string ClientID = gDataItem["ClientID"].Text;
            string name = gDataItem["TradingName"].Text;
            IFAEntityType ifaEntityType = (IFAEntityType)Enum.Parse(typeof(IFAEntityType), gDataItem["IFAType"].Text);

            switch (e.CommandName.ToLower())
            {
                case "edit":
                    switch (ifaEntityType)
                    {
                        case IFAEntityType.IFAIndividualControl:
                            Response.Redirect("IFASoleTraderDetail.aspx?ins=" + CID);
                            break;
                        case IFAEntityType.IFACompanyControl:
                            Response.Redirect("IFACompanyDetail.aspx?ins=" + CID);
                            break;
                        case IFAEntityType.IFAPartnershipControl:
                            Response.Redirect("IFAPartnershipDetail.aspx?ins=" + CID);
                            break;
                        case IFAEntityType.IFATrustControl:
                            Response.Redirect("IFATrustDetail.aspx?ins=" + CID);
                            break;
                    }
                    break;
                case "membership":
                    {
                        Response.Redirect("MembershipDetail.aspx?CID=" + CID.ToString() + "&ClientID=" + ClientID + "&orgtype=" + (int)OrganizationType.IFA + "&Name=" + name);
                    }
                    break;
                case "update":
                    break;
                case "delete":
                    {
                        e.Item.Edit = false;
                        e.Canceled = true;
                        var clid = new Guid(gDataItem["Clid"].Text);
                        var csid = new Guid(gDataItem["Csid"].Text);
                        DeleteIfa(clid, csid);
                    }
                    break;
            }
        }

        /// <summary>
        /// Delete IFA
        /// </summary>
        /// <param name="clid"></param>
        /// <param name="csid"></param>
        private void DeleteIfa(Guid clid, Guid csid)
        {
            var ds = new IFADS
            {
                CommandType = DatasetCommandTypes.Delete,
                Unit = new OrganizationUnit
                {
                    Clid = clid,
                    Csid = csid,
                    CurrentUser = GetCurrentUser(),
                    Type = ((int)OrganizationType.IFA).ToString(),
                },
                Command = (int)WebCommands.DeleteOrganizationUnit
            };

            SaveOrganizanition(ds);
            gd_IFA.Rebind();
        }

        protected void lnkbtnAddIFA_Click(object sender, EventArgs e)
        {
            HideShowOrganizationType(true);

        }
        private void HideShowOrganizationType(bool show)
        {
            OVER.Visible = OrganizationTypePopup.Visible = show;
        }
        protected void btnNext_OnClick(object sender, EventArgs e)
        {
            if (rdoSoleTrader.Checked)
            {
                Response.Redirect("IFASoleTraderDetail.aspx");
            }
            else if (rdoCompany.Checked)
            {
                Response.Redirect("IFACompanyDetail.aspx");
            }
            else if (rdoPartnership.Checked)
            {
                Response.Redirect("IFAPartnershipDetail.aspx");
            }
            else
            {
                Response.Redirect("IFATrustDetail.aspx");
            }
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            HideShowOrganizationType(false);
        }
    }
}