﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    EnableEventValidation="true" AutoEventWireup="true" CodeBehind="AccountantTrustDetail.aspx.cs"
    Inherits="eclipseonlineweb.SysAdministration.AccountantTrustDetail" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register Src="../Controls/AddressDetailControl.ascx" TagName="AddressDetailControl"
    TagPrefix="uc1" %>
<%@ Register Src="../Controls/BankAccountMappingControl.ascx" TagName="BankAccountMappingControl"
    TagPrefix="uc4" %>
<%@ Register Src="../Controls/AccountantTrustControl.ascx" TagName="AccountantTrustControl"
    TagPrefix="uc2" %>
<%@ Register TagPrefix="uc2" TagName="SecurityConfigurationControl" Src="~/Controls/SecurityConfigurationControl.ascx" %>

   <%@ Register Src="../Controls/ClientMappingControl.ascx" TagName="ClientMapping"
    TagPrefix="uc8" %>
    

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                        </td>
                        <td width="90%" align="right">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblAccountant" Text="Accountant"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadTabStrip ID="RadTabStrip1" runat="server" Skin="Vista" MultiPageID="RadMultiPage1"
                SelectedIndex="0">
                <Tabs>
                    <telerik:RadTab Text="Accountant Trust" runat="server" Selected="True" Value="AccountantTrustHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Addresses" runat="server" Visible="true" Value="AddressHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Corporate" runat="server" Visible="true" Value="CorporateHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Bank Accounts" runat="server" Visible="true" Value="BankAccountsHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Clients" runat="server" Visible="true" Value="ClientHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Security" runat="server" Visible="true" Value="SecurityHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Documents" runat="server" Visible="true" Value="DocumentsHeader">
                    </telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage runat="server" ID="RadMultiPage1" SelectedIndex="0">
                <telerik:RadPageView runat="server" ID="rpvAccountantTrust">
                    <uc2:AccountantTrustControl ID="AccountantTrustControl1" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvAddress" runat="server">
                    <fieldset style="width: 98%">
                        <div style="text-align: left; float: none; border: 1px; background-color: White;
                            height: 25px; position: relative; vertical-align: middle; margin: 0; padding: 0;">
                            <telerik:RadButton ID="btnSave" Text="Add Client Addresses" ToolTip="Save Changes"
                                OnClick="btnSave_Click" runat="server" Width="32px" Height="32px" BorderStyle="None">
                                <Image ImageUrl="~/images/Save-Icon.png"></Image>
                            </telerik:RadButton>
                        </div>
                    </fieldset>
                    <br />
                    <uc1:AddressDetailControl ID="AddressDetailControl" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvCorporate" runat="server">
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvBankAccounts" runat="server">
                    <uc4:BankAccountMappingControl ID="BankAccountMappingControl" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvClient" runat="server">
                   <uc8:ClientMapping ID="ClientMappingControl" runat="server"  />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvSecurity" runat="server">
                    <uc2:SecurityConfigurationControl ID="SecurityConfiguration" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvDocuments" runat="server">
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
