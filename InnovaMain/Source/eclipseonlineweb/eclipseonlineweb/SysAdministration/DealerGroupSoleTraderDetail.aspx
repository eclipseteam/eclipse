﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    EnableEventValidation="true" AutoEventWireup="true" CodeBehind="DealerGroupSoleTraderDetail.aspx.cs"
    Inherits="eclipseonlineweb.SysAdministration.DealerGroupDetail" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register Src="../Controls/AddressDetailControl.ascx" TagName="AddressDetailControl"
    TagPrefix="uc7" %>
<%@ Register Src="../Controls/DealerGroupIndividualControl.ascx" TagName="DealerGroupIndividualControl"
    TagPrefix="uc2" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="../Controls/ContactMappingControl.ascx" TagName="ContactMappingControl"
    TagPrefix="uc5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Controls/BankAccountMappingControl.ascx" TagName="BankAccountMappingControl"
    TagPrefix="uc3" %>
<%@ Register Src="../Controls/AddressDetailControl.ascx" TagName="AddressControl"
    TagPrefix="uc7" %>
<%@ Register TagPrefix="uc2" TagName="SecurityConfigurationControl" Src="~/Controls/SecurityConfigurationControl.ascx" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                        </td>
                        <td width="90%" align="right">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <span class="riLabel">Group</span>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:radtabstrip id="RadTabStrip1" runat="server" skin="Vista" multipageid="RadMultiPage1"
                selectedindex="0">
                <tabs>
                    <telerik:RadTab Text="Dealer Group Sole Trader" runat="server" Selected="True">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Address" runat="server" Visible="true" Value="AddressHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Bank Account" runat="server" Visible="true" Value="BankAccounHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Sole Trader" runat="server" Visible="true" Value="SoleTraderHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Security" runat="server" Visible="true" Value="SecurityHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Documents" runat="server" Visible="true" Value="DocumentsHeader">
                    </telerik:RadTab>
                </tabs>
            </telerik:radtabstrip>
            <telerik:radmultipage runat="server" id="RadMultiPage1" selectedindex="0">
                <telerik:radpageview runat="server" id="ApBasic">
                    <uc2:DealerGroupIndividualControl ID="DealerIndivisualControl" runat="server" />
                </telerik:radpageview>
                <telerik:radpageview id="ApAddress" runat="server">
                    <fieldset style="width: 98%">
                        <div style="text-align: left; float: none; border: 1px; background-color: White;
                            height: 25px; position: relative; vertical-align: middle; margin: 0; padding: 0;">
                            <telerik:radbutton id="btnSave" runat="server" width="34px" height="32px" tooltip="Save Changes"
                                onclick="BtnSaveClick">
                                <image imageurl="~/images/Save-Icon.png" />
                            </telerik:radbutton>
                        </div>
                    </fieldset>
                    <br />
                    <uc7:AddressDetailControl ID="AddressDetailControl" runat="server" />
                </telerik:radpageview>
                <telerik:radpageview id="ApBankAccounts" runat="server">
                    <uc3:BankAccountMappingControl ID="BankAccountMappingControl" runat="server" />
                </telerik:radpageview>
                <telerik:radpageview id="ApSol" runat="server">
                    <uc5:ContactMappingControl ID="ContactMappingControl1" runat="server" IndividualsType="Applicants" />
                </telerik:radpageview>
                <telerik:radpageview id="ApSecurity" runat="server">
                    <uc2:SecurityConfigurationControl ID="SecurityConfiguration" runat="server" />
                </telerik:radpageview>
                <telerik:radpageview id="ApDocuments" runat="server">
                </telerik:radpageview>
            </telerik:radmultipage>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
