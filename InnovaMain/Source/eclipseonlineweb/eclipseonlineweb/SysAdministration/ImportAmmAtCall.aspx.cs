﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebControls;
using Oritax.TaxSimp.Security;
using eclipseonlineweb.WebUtilities;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;
using System.IO;
using System.Web.UI.WebControls;

namespace eclipseonlineweb.SysAdministration
{
    public partial class ImportAmmAtCall : UMABasePage// System.Web.UI.Page
    {
        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            return true;
        }

        public override void LoadPage()
        {
            if (!Page.IsPostBack)
                ImportMessageControl.Clear();
        }

        protected override void Intialise()
        {
            ImportMessageControl.GridImportMessages.ColumnCreated += GridImportMessages_ColumnCreated;
            ImportMessageControl.GirdInvestmentCodeError.ColumnCreated += GridImportErrorMessages_ColumnCreated;
            ImportMessageControl.GridImportMessages.ItemDataBound += GridImportMessages_ItemDataBound;

            if (!IsPostBack)
            {
                ImportMessageControl.Clear();
            }

            ImportMessageControl.Completed += text =>
            {
                switch (text.ToLower())
                {
                    case "next":
                        SendData();
                        break;

                    default:
                        ClearMessageControls();
                        break;
                }
            };
        }

        void GridImportMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            SetDataGridColumns(e);
        }

        void GridImportErrorMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            SetDataGridColumns(e);
        }

        void GridImportMessages_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item).ItemIndex != -1)
            {
                DataRow dr = ((DataRowView)((e.Item).DataItem)).Row;
                if (dr["HasErrors"].ToString() != string.Empty && bool.Parse(dr["HasErrors"].ToString()))
                {
                    e.Item.ForeColor = Color.Red;
                }
            }

        }

        private void SendData()
        {
            var distributionDictory = Server.MapPath("~/App_Data/Import/");
            if (!Directory.Exists(distributionDictory))
            {
                Directory.CreateDirectory(distributionDictory);
            }
            string filename = distributionDictory + ImportMessageControl.GetFileID() + ".xml";
            var ds = new ImportProcessDS();
            ds.ReadXml(filename);
            ds.Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportAtCallClientTransactionImportFile;
            ds.FileName = filename;
            SaveOrganizanition(ds);
            SetCompletionGird(ds);

        }

        private void SetCompletionGird(ImportProcessDS ds)
        {
            ImportMessageControl.Clear();
            if (ds.Tables["AtCallAmm"].Columns.Contains("InstituteID") &&
               ds.Tables["AtCallAmm"].Columns.Contains("ProductID") &&
               ds.Tables["AtCallAmm"].Columns.Contains("BrokerName") &&
               ds.Tables["AtCallAmm"].Columns.Contains("ServiceType"))
            {
                ds.Tables["AtCallAmm"].Columns.Remove("InstituteID");
                ds.Tables["AtCallAmm"].Columns.Remove("ProductID");
                ds.Tables["AtCallAmm"].Columns.Remove("BrokerName");
                ds.Tables["AtCallAmm"].Columns.Remove("ServiceType");
            }
            using (DataView dv = new DataView(ds.Tables["AtCallAmm"]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }

            using (DataView dv = new DataView(ds.Tables["AtCallAmm"]))
            {
                dv.RowFilter = "IsMissingItem='true' OR HasErrors='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");

                foreach (DataRow row in dv.ToTable().Rows)
                {
                    var dr = missingItems.NewRow();
                    dr["MissingItem"] = row["AccountNumber"];
                    missingItems.Rows.Add(dr);
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();

                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingAccountsIgnored;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }

            int successfulRows = ds.Tables["AtCallAmm"].Select("HasErrors='false'").Length;
            ImportMessageControl.SetButtons(ButtonTypes.FINISH);
            ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sImportedSuccessfully, successfulRows, ds.Tables["AtCallAmm"].Rows.Count - successfulRows), ImportMessageType.Sucess);

        }

        private void ClearMessageControls()
        {
            ImportMessageControl.Clear();
            ImportMessageControl.TextMessage.Text = "";
        }

        private static void SetDataGridColumns(GridColumnCreatedEventArgs e)
        {
            string colName = e.Column.UniqueName.ToLower();
            switch (colName)
            {
                case "accountnumber":
                    e.Column.HeaderText = "AccountNumber";
                    break;
                case "dealid":
                    e.Column.HeaderText = "DealID";
                    break;
                case "tdtran":
                    e.Column.HeaderText = "TDTRAN";
                    break;
                case "contractid":
                    e.Column.HeaderText = "ContractID";
                    break;
                case "transactiondate":
                    e.Column.HeaderText = "TransactionDate";
                    break;
                case "transactionamount":
                    e.Column.HeaderText = "TransactionAmount";
                    break;
                case "transactionrate":
                    e.Column.HeaderText = "TransactionRate";
                    break;
                case "transactiontype":
                    e.Column.HeaderText = "TransactionType";
                    break;
                case "message":
                    e.Column.HeaderText = "Message";
                    break;
                case "haserrors":
                    e.Column.HeaderText = "Has Errors";
                    e.Column.Visible = false;
                    break;
                case "ismissingitem":
                    e.Column.HeaderText = "Is Missing Item";
                    e.Column.Visible = false;
                    break;
                case "missingitem":
                    e.Column.HeaderText = "Missing Account Numbers";
                    break;
            }
        }



        private void SetValidationGird(ImportProcessDS ds)
        {
            using (DataView dv = new DataView(ds.Tables["AtCallAmm"]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }
            using (DataView dv = new DataView(ds.Tables["AtCallAmm"]))
            {
                dv.RowFilter = "IsMissingItem='true' OR HasErrors='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");

                foreach (DataRow row in dv.ToTable().Rows)
                {
                    var dr = missingItems.NewRow();
                    dr["MissingItem"] = row["AccountNumber"];
                    missingItems.Rows.Add(dr);
                }
                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();

                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingAccountsIgnored;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }
            int successfulRows = ds.Tables["AtCallAmm"].Select("HasErrors='false'").Length;
            if (ds.Tables["AtCallAmm"].Rows.Count == 0 || successfulRows == 0)
            {
                ImportMessageControl.SetButtons(ButtonTypes.CLOSE);
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sErrorsCannotContinue, successfulRows, ds.Tables["AtCallAmm"].Rows.Count - successfulRows), ImportMessageType.Error);
                ImportMessageControl.TextMessage.Text = "";
            }
            else
            {
                ImportMessageControl.SetButtons(ButtonTypes.NEXTCANCEL);
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sValidationCompleted, successfulRows, ds.Tables["AtCallAmm"].Rows.Count - successfulRows), ImportMessageType.Sucess);
            }
        }

        protected void btnImportTran_Click(object sender, EventArgs e)
        {
            if (fileuploadExcel.PostedFile != null)
            {
                // Get a reference to PostedFile object
                HttpPostedFile myFile = fileuploadExcel.PostedFile;
                // Get size of uploaded file
                int nFileLen = myFile.ContentLength;
                // make sure the size of the file is > 0
                if (nFileLen > 0)
                {
                    // Allocate a buffer for reading of the file
                    string fileName = Path.GetFileName(myFile.FileName);
                    var date = DateTime.Now;
                    var dictory = Server.MapPath("~/App_Data/Import/");
                    if (!Directory.Exists(dictory))
                    {
                        Directory.CreateDirectory(dictory);
                    }
                    string fileLocation = dictory + date.ToString("ddMMyyhhmmsstt") + fileName;
                    byte[] myData = new byte[nFileLen];
                    // Read uploaded file from the Stream
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    // Write data into a file
                    Utilities.WriteToFile(fileLocation, ref myData);
                    string csv = string.Empty;
                    using (StreamReader reader = new StreamReader(fileLocation, true))
                    {
                        csv = reader.ReadToEnd().Replace("\"", string.Empty);
                        reader.Close();
                    }
                    string cashTransactions = ConversorTransactionCsv.FilterRow(csv, "TDTRAN");
                    DataTable transactions = ConversorTransactionCsv.CreateAtCallAmmClientTrans(cashTransactions);

                    var ds = new ImportProcessDS();
                    if (transactions != null)
                        ds.Tables.Add(transactions);

                    ds.Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() };
                    ds.Command = (int)WebCommands.ValidateAtCallClientTransactionImportFile;
                    ds.FileName = fileName;
                    SaveOrganizanition(ds);
                    SetValidationGird(ds);
                    PresentationData = ds;
                    string filename = ImportMessageControl.RefreshFileID() + ".xml";
                    ds.WriteXml(dictory + filename, XmlWriteMode.WriteSchema);
                }
            }
        }

    }
}