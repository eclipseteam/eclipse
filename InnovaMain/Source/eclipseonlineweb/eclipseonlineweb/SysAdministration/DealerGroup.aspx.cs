﻿using System;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using Oritax.TaxSimp.Security;
using eclipseonlineweb.WebUtilities;

namespace eclipseonlineweb.SysAdministration
{
    public partial class DealerGroup : UMABasePage
    {
        public override void LoadPage()
        {

        }

        protected void gd_DealerGroup_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            DealerGroupDS dealerGroupDs = new DealerGroupDS { CommandType = DatasetCommandTypes.Get, Command = (int)WebCommands.GetOrganizationUnitsByType };
            dealerGroupDs.Unit = new OrganizationUnit
            {
                CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                Type = ((int)OrganizationType.DealerGroup).ToString(),

            };
            org.GetData(dealerGroupDs);
            gd_DealerGroup.DataSource = dealerGroupDs;
            UMABroker.ReleaseBrokerManagedComponent(org);
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected void gd_DealerGroup_InsertCommand(object source, GridCommandEventArgs e)
        { }
        private void HideShowOrganizationType(bool show)
        {
            OVER.Visible = OrganizationTypePopup.Visible = show;
        }

        protected void btnNext_OnClick(object sender, EventArgs e)
        {
            if (rdoDealerSoleTrader.Checked)
            {
                Response.Redirect("DealerGroupSoleTraderDetail.aspx");
            }
            else if (rdoDealerCompany.Checked)
            {
                Response.Redirect("DealerGroupCompanyDetail.aspx");
            }
            else if (rdoDealerPartnership.Checked)
            {
                Response.Redirect("DealerGroupPartnershipDetail.aspx");
            }
            else
            {
                Response.Redirect("DealerGroupTrustDetail.aspx");
            }
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            HideShowOrganizationType(false);
        }

        protected void gd_DealerGroup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            GridDataItem gDataItem = e.Item as GridDataItem;
            Guid CID = new Guid(gDataItem["Cid"].Text);
            string ClientID = gDataItem["ClientID"].Text;
            string name = gDataItem["TradingName"].Text;
            DealerGroupEntityType dealerGroupEntityType = (DealerGroupEntityType)Enum.Parse(typeof(DealerGroupEntityType), gDataItem["DealerGroupType"].Text);
            switch (e.CommandName.ToLower())
            {
                case "membership":
                    Response.Redirect("MembershipDetail.aspx?CID=" + CID.ToString() + "&ClientID=" + ClientID + "&orgtype=" + (int)OrganizationType.DealerGroup + "&Name=" + name);
                    break;
                case "edit":
                    switch (dealerGroupEntityType)
                    {
                        case DealerGroupEntityType.DealerGroupIndividualControl:
                            Response.Redirect("DealerGroupSoleTraderDetail.aspx?ins=" + CID);
                            break;
                        case DealerGroupEntityType.DealerGroupCompanyControl:
                            Response.Redirect("DealerGroupCompanyDetail.aspx?ins=" + CID);
                            break;
                        case DealerGroupEntityType.DealerGroupPartnershipControl:
                            Response.Redirect("DealerGroupPartnershipDetail.aspx?ins=" + CID);
                            break;
                        case DealerGroupEntityType.DealerGroupTrustControl:
                            Response.Redirect("DealerGroupTrustDetail.aspx?ins=" + CID);
                            break;
                    }
                    break;
                case "delete":
                    var clid = new Guid(gDataItem["Clid"].Text);
                    var csid = new Guid(gDataItem["Csid"].Text);
                    if (IsContainIFA(CID))
                    {
                        const string script =
                            "alert(\"This dealer group is associated with one or more IFA's. Please remove that association first then try again!\");";
                        ClientScript.RegisterClientScriptBlock(GetType(), "showalert", script, true);
                    }
                    else
                    {
                        DeleteDelarGroup(clid, csid);
                        string msg = name + " dealer group is deleted";
                        ClientScript.RegisterClientScriptBlock(GetType(), "showalertConfirm", "alert('" + msg + "');",
                                                               true);
                    }
                    break;
            }
        }

        /// <summary>
        /// Check delar group contains IFA's
        /// </summary>
        /// <param name="cId"></param>
        /// <returns></returns>
        private bool IsContainIFA(Guid cId)
        {
            var ds = new MembershipDS
                         {
                             CommandType = DatasetCommandTypes.GetChildren,
                             Unit = new OrganizationUnit
                                        {
                                            CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                                        }
                         };
            var component = UMABroker.GetBMCInstance(cId);
            component.GetData(ds);
            UMABroker.ReleaseBrokerManagedComponent(component);
            return ds.Tables[0].Rows.Count > 0;
        }

        /// <summary>
        /// Delete Dealer Group
        /// </summary>
        /// <param name="clid"></param>
        /// <param name="csid"></param>
        private void DeleteDelarGroup(Guid clid, Guid csid)
        {
            BrokerUtility.DeleteOrgUnit(clid, csid, GetCurrentUser(), OrganizationType.DealerGroup, SaveOrganizanition);
            gd_DealerGroup.Rebind();
        }

        protected void btnAddDealerGroup_Click(object sender, EventArgs e)
        {
            HideShowOrganizationType(true);
        }
    }

}