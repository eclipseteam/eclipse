﻿<%@ Page Title="" Language="C#" MasterPageFile="AdminMaster.master" AutoEventWireup="true"
    CodeBehind="ImportAtCallTransactionsFromFIIG.aspx.cs" Inherits="eclipseonlineweb.ImportAtCallTransactionsFromFIIG"
    EnableViewState="false" %>

<%@ Register TagPrefix="uc" TagName="Details" Src="WebControls/ImportMessageControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Import At Call Transactions From FIIG"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <table width="100%">
                <tr valign="top">
                    <td>
                        <fieldset>
                            <legend>Select At Call Transactions From FIIG File</legend>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblFile" runat="server" Font-Bold="True" Text="File:"></asp:Label>
                                        <asp:FileUpload Height="23px" ID="filMyFile" runat="server" />
                                        <asp:Button ID="cmdSend" runat="server" Width="92px" Text="Upload" OnClick="BtnUploadClick" />
                                        <asp:RegularExpressionValidator ID="rexp" runat="server" ControlToValidate="filMyFile"
                                            ErrorMessage="File type should be Excel (.xls)" ValidationExpression="(.*\.([Xx][Ll][Ss][Xx])$)"
                                            ForeColor="#FF3300"></asp:RegularExpressionValidator>
                                        <br />
                                        Select sheet:
                                        <telerik:RadComboBox ID="ddlSheetName" runat="server" EnableViewState="true" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlSheetName_SelectedIndexChanged">
                                        </telerik:RadComboBox>
                                    </td>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    Institution
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="cmbInstitution" runat="server">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    Transaction date
                                                </td>
                                                <td>
                                                    <telerik:RadDatePicker runat="server" ID="dtPicker" Width="160px">
                                                        <Calendar ID="cal1" runat="server" EnableKeyboardNavigation="true">
                                                        </Calendar>
                                                    </telerik:RadDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    System Transaction type
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="cmbSystemTransType" runat="server" EnableViewState="true">
                                                        <Items>
                                                            <telerik:RadComboBoxItem Text="Accounting Expense" Value="" />
                                                            <telerik:RadComboBoxItem Text="Administration Fee" Value="" />
                                                            <telerik:RadComboBoxItem Text="Advisory Fee" Value="" />
                                                            <telerik:RadComboBoxItem Text="Application" Value="" />
                                                            <telerik:RadComboBoxItem Text="Business Activity Statement" Value="" />
                                                            <telerik:RadComboBoxItem Text="Commission Rebate" Value="" />
                                                            <telerik:RadComboBoxItem Text="Distribution" Value="" />
                                                            <telerik:RadComboBoxItem Text="Dividend" Value="" />
                                                            <telerik:RadComboBoxItem Text="Employer Additional" Value="" />
                                                            <telerik:RadComboBoxItem Text="Employer SG" Value="" />
                                                            <telerik:RadComboBoxItem Text="Income Tax" Value="" />
                                                            <telerik:RadComboBoxItem Text="Instalment Activity Statement" Value="" />
                                                            <telerik:RadComboBoxItem Text="Insurance Premium" Value="" />
                                                            <telerik:RadComboBoxItem Text="Interest" Value="" Selected="true" />
                                                            <telerik:RadComboBoxItem Text="Internal Cash Movement" Value="" />
                                                            <telerik:RadComboBoxItem Text="Internal Cash Movement" Value="" />
                                                            <telerik:RadComboBoxItem Text="Investment Fee" Value="" />
                                                            <telerik:RadComboBoxItem Text="Legal Expense" Value="" />
                                                            <telerik:RadComboBoxItem Text="PAYG" Value="" />
                                                            <telerik:RadComboBoxItem Text="Pension Payment" Value="" />
                                                            <telerik:RadComboBoxItem Text="Personal" Value="" />
                                                            <telerik:RadComboBoxItem Text="Property" Value="" />
                                                            <telerik:RadComboBoxItem Text="Redemption" Value="" />
                                                            <telerik:RadComboBoxItem Text="Regulatory Fee" Value="" />
                                                            <telerik:RadComboBoxItem Text="Rental Income" Value="" />
                                                            <telerik:RadComboBoxItem Text="Salary Sacrifice" Value="" />
                                                            <telerik:RadComboBoxItem Text="Spouse" Value="" />
                                                            <telerik:RadComboBoxItem Text="Tax Refund" Value="" />
                                                            <telerik:RadComboBoxItem Text="Transfer In" Value="" />
                                                            <telerik:RadComboBoxItem Text="Transfer Out" Value="" />
                                                            <telerik:RadComboBoxItem Text="Withdrawal" Value="" />
                                                        </Items>
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    Import Transaction type
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="cmbImportTransactionType" runat="server">
                                                        <Items>
                                                            <telerik:RadComboBoxItem Text="Deposit" Value="Deposit" Selected="true" />
                                                            <telerik:RadComboBoxItem Text="Withdrawal" Value="Withdrawal" />
                                                        </Items>
                                                    </telerik:RadComboBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Broker
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="cmbBroker" runat="server">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td colspan="2">
                                                    <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <asp:HiddenField runat="server" ID="hf_FileName" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Panel runat="server" ID="PnlResults">
                            <uc:Details ID="ImportMessageControl" runat="server" />
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="cmdSend" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
