﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="OrgAssetSummaryReport.aspx.cs" Inherits="eclipseonlineweb.OrgAssetSummaryReport" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer"
    TagPrefix="C1ReportViewer" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <c1:C1InputDate DateFormat="dd/MMM/yyyy" ID="c1ValutionDate" runat="server" ShowTrigger="true">
                            </c1:C1InputDate>
                        </td>
                        <td style="width: 5%">
                            <asp:ImageButton OnClick="GenerateAssetSummaryReport" runat="server" ID="ImageButton1"
                                ImageUrl="~/images/database.png" />
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <C1ReportViewer:C1ReportViewer CollapseToolsPanel="true" Cache-Enabled="false" Cache-ShareBetweenSessions="false"
                FileName="C1ReportViewerAssetSummary" runat="server" ID="C1ReportViewerAssetClass"
                Zoom="75%" Height="550px" Width="100%">
            </C1ReportViewer:C1ReportViewer>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
