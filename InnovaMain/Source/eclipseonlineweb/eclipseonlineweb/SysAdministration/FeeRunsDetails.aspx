﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="FeeRunsDetails.aspx.cs" Inherits="eclipseonlineweb.FeeRunsDetails" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnBack" ToolTip="Back" AlternateText="back" runat="server"
                            ImageUrl="~/images/window_previous.png" OnClick="btnBack_Click" />
                    </td>
                    <td width="4%">
                        <telerik:RadButton Height="40px" ID="rbExportBankwestOutput" runat="server" Width="125px"
                            Text="EXPORT BANKWEST" OnClick="rbExportBankwestOutput_Click">
                        </telerik:RadButton>
                    </td>
                    <td>
                        <telerik:RadButton ID="btnExportFeeBreakDown" Text="EXPORT FEE DETAILS" runat="server"
                            Height="40px" Width="125px" OnClick="DownloadXLS">
                        </telerik:RadButton>
                    </td>
                    <td>
                        <telerik:RadButton ID="btnExportFeeHoldings" Text="EXPORT HOLDINGS" runat="server"
                            Height="40px" Width="125px" OnClick="DownloadXLSHoldings">
                        </telerik:RadButton>
                    </td>
                    <td width="100%" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <asp:Label Font-Bold="true" runat="server" ID="lblFeeRunDetails" Text="Fee Run Details"></asp:Label>
                        <asp:HiddenField ID="hfSecID" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="300"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
                OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
                OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
                OnItemDataBound="PresentationGrid_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="FeeRuns" TableLayout="Fixed" EditMode="EditForms">
                    <CommandItemSettings ShowAddNewRecordButton="false" ShowExportToExcelButton="true"
                        ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                            <HeaderStyle Width="2%"></HeaderStyle>
                            <ItemStyle CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridEditCommandColumn>
                        <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="FEERUNID" UniqueName="FEERUNID" />
                        <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ID" UniqueName="ID" />
                        <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="CLIENTCID" UniqueName="CLIENTCID" />
                        <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                            SortExpression="CLIENTID" HeaderText="Client ID" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="CLIENTID" UniqueName="CLIENTID" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                            SortExpression="CLIENTNAME" HeaderText="Client Name" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="CLIENTNAME" UniqueName="CLIENTNAME" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                            SortExpression="ADVISERNAME" HeaderText="Adviser Name" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="ADVISERNAME" UniqueName="ADVISERNAME" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn HeaderText="Adviser Name" SortExpression="AdviserName"
                            UniqueName="AdviserListingCombo" DataField="AdviserName" Visible="false" ColumnEditorID="GridDropDownColumnEditorListOfAdvisers" />
                        <telerik:GridBoundColumn FilterControlWidth="80px" Aggregate="Sum" HeaderStyle-Width="10%"
                            ItemStyle-Width="10%" SortExpression="TOTALFEES" HeaderText="Total Fees" AutoPostBackOnFilter="true"
                            DataFormatString="{0:c}" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="TOTALFEES" UniqueName="TOTALFEES" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn FilterControlWidth="0px" UniqueName="FeeTemplateDetails"
                            ShowFilterIcon="false">
                            <HeaderStyle Width="7%" />
                            <ItemStyle Width="7%" />
                            <ItemTemplate>
                                <asp:LinkButton ID="Definition" runat="server" Text="Details" CommandName="Details"
                                    CommandArgument='<%# Eval("ID") +"," + Eval("CLIENTCID")%>'></asp:LinkButton>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridButtonColumn ConfirmText="Delete these details record?" ButtonType="ImageButton"
                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="Html"></Excel>
                </ExportSettings>
            </telerik:RadGrid>
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorListOfAdvisers"
                runat="server" DropDownStyle-Width="200px">
            </telerik:GridDropDownListColumnEditor>
            <br />
            <asp:Panel runat="server" ID="pnlSelectProducts">
                <h3>
                    Fee Templates List</h3>
                <table>
                    <tr>
                        <td>
                            <asp:CheckBox Text="Use Local Fee Template" ID="btnUseLocalFee" runat="server" OnCheckedChanged="RadioCheckChanged_Clicked"
                                AutoPostBack="true" />
                            <br />
                            <br />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Select Fee Template</b>
                        </td>
                        <td>
                            <telerik:RadComboBox CheckBoxes="true" EnableCheckAllItemsCheckBox="true" Width="400px"
                                ID="C1ComboBoxFeeTemplates" Filter="Contains" runat="server" Skin="Metro">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <telerik:RadButton ID="btnAddFeeTemplate" runat="server" Height="20px" Width="50px"
                                Text="ADD" OnClick="btnAddFeeTemplate_Click">
                            </telerik:RadButton>
                        </td>
                    </tr>
                </table>
                <br />
                <telerik:RadGrid OnNeedDataSource="GridConfiguredFees_NeedDataSource" ID="GridFeeTemplate"
                    runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                    AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="GridConfiguredFees_DetailTableDataBind"
                    OnItemCommand="GridConfiguredFees_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                    AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                    OnItemUpdated="GridConfiguredFees_ItemUpdated" OnItemDeleted="GridConfiguredFees_ItemDeleted"
                    OnItemInserted="GridConfiguredFees_ItemInserted" OnInsertCommand="GridConfiguredFees_InsertCommand"
                    EnableViewState="true" ShowFooter="true" OnItemCreated="GridConfiguredFees_ItemCreated"
                    OnItemDataBound="GridConfiguredFees_ItemDataBound">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                        Name="Products" TableLayout="Fixed" EditMode="EditForms">
                        <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                        <Columns>
                            <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ID" UniqueName="ID" />
                            <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="DESCRIPTION"
                                HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderText="Description" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="DESCRIPTION" UniqueName="DESCRIPTION">
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn ConfirmText="Delete these details record?" ButtonType="ImageButton"
                                CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                    <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                        <Excel Format="Html"></Excel>
                    </ExportSettings>
                </telerik:RadGrid>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
