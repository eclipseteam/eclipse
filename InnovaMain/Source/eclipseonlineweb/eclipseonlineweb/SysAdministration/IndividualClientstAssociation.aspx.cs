﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using Oritax.TaxSimp.Common;
using System.Collections.Generic;
using Oritax.TaxSimp.CM.Organization;
using eclipseonlineweb.Controls;
using OTD = Oritax.TaxSimp.DataSets;

namespace eclipseonlineweb
{
    public partial class IndividualClientstAssociation : UMABasePage
    {
        private string _cid = string.Empty;
        private ICMBroker Broker
        {
            get
            {
                var umaBasePage = Page as UMABasePage;
                return umaBasePage != null ? umaBasePage.UMABroker : null;
            }
        }
        private DataView BusinessTitleDataView { get; set; }


        protected override void Intialise()
        {
            base.Intialise();
            IndividualClientMappingControl.SaveUnit += (ins, clientds) =>
            {
                SaveData(ins, clientds);
            };

        }
        public override void LoadPage()
        {
            _cid = Request.QueryString["ins"];

        }

       

    }
}
