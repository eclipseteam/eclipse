﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class OrgAssetSummaryReport : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        private List<AssetEntity> assets = null;

        public override void LoadPage()
        {
            OrganisationListingDS organisationListingDS = new OrganisationListingDS(); 
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.AssetSummaryOrg; 
            IOrganization orgData = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            if (this.c1ValutionDate.Date.HasValue)
                organisationListingDS.AsOfDate = c1ValutionDate.Date.Value;
            
            orgData.GetData(organisationListingDS);
            this.assets = orgData.Assets;
            this.PresentationData = organisationListingDS;

            if (!this.IsPostBack)
                this.PopulatePage(this.PresentationData);

            ScriptManager sm = ScriptManager.GetCurrent(Page);

            this.UMABroker.ReleaseBrokerManagedComponent(orgData);
            
        }

        public override void PopulatePage(DataSet ds)
        {
            base.PopulatePage(ds);
            ClientAssetsReportClick();
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);

        }

        public C1PrintDocument MakeDocAssetSummary()
        {
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            //DataRow clientSummaryRow = this.PresentationData.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0];
            AddHeaderToReport(doc, ((OrganisationListingDS)this.PresentationData).DateInfo(), "ASSETS & PRODUCTS FUM$ REPORT");
            DataTable holdingTable = PresentationData.Tables["HoldingSummary"];
            holdingTable.Columns.Remove("AccountNo");
            DataView holdingSummaryTableView = new DataView(PresentationData.Tables["HoldingSummary"]);
            holdingSummaryTableView.Sort = "AssetName, ProductName ASC";

            var assetGroups = holdingSummaryTableView.ToTable().Select().GroupBy(row => row["AssetName"]);

            RenderTable assetSumTable = new RenderTable();

            RenderTable assetSummaryTotalTable = new RenderTable();
            assetSummaryTotalTable.Style.FontSize = 8;

            assetSummaryTotalTable.Rows[0].Height = .2;
            assetSummaryTotalTable.Rows[0].Style.FontBold = true;
            assetSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            assetSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            assetSummaryTotalTable.Rows[0].Style.FontBold = true;
            assetSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            assetSummaryTotalTable.Cells[0, 0].Text = "Asset Code";
            assetSummaryTotalTable.Cells[0, 1].Text = "Description";
            assetSummaryTotalTable.Cells[0, 2].Text = "Allocation (%)";
            assetSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
            assetSummaryTotalTable.Cells[0, 3].Text = "Holding ($)";
            assetSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

            assetSummaryTotalTable.Cols[0].Width = 4;
            assetSummaryTotalTable.Cols[1].Width = 8;
            assetSummaryTotalTable.Cols[2].Width = 4;
            assetSummaryTotalTable.Cols[3].Width = 4;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Assets Summary\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            double grandTotal = 0;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(assetSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            foreach (var assetGroup in assetGroups)
            {
                if (assetGroup.Count() > 0)
                {
                    foreach (DataRow row in assetGroup)
                        grandTotal += Convert.ToDouble(row["Holding"]);
                }
            }


            foreach (var assetGroup in assetGroups)
            {
                if (assetGroup.Count() > 0)
                {
                    int index = 1;
                    string assetClass = string.Empty;
                    //Deploy header
                    RenderTable assetTable = new RenderTable();

                    double totalHolding = 0;

                    assetTable.Style.FontSize = 8;

                    assetTable.Rows[0].Height = .2;
                    assetTable.Rows[0].Style.FontBold = true;
                    assetTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                    assetTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                    assetTable.Rows[0].Style.FontBold = true;
                    assetTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                    assetTable.Cells[0, 0].Text = "Product Name";
                    assetTable.Cells[0, 1].Text = "Description";
                    assetTable.Cells[0, 2].Text = "Allocation (%)";
                    assetTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                    assetTable.Cells[0, 3].Text = "Holding ($)";
                    assetTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    assetTable.Cols[0].Width = 2.5;
                    assetTable.Cols[1].Width = 4.3;
                    assetTable.Cols[2].Width = 1.5;
                    assetTable.Cols[3].Width = 1.5;

                    foreach (DataRow row in assetGroup)
                    {
                        assetClass = row["AssetName"].ToString();
                        totalHolding += Convert.ToDouble(row["Holding"]);
                    }

                    var productGroups = assetGroup.GroupBy(proRow => proRow["ProductName"]);
                    foreach (var productGroup in productGroups)
                    {
                        assetTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                        assetTable.Rows[index].Height = .3;
                        assetTable.Cells[index, 0].Text = productGroup.Key.ToString();
                        assetTable.Cells[index, 1].Text = productGroup.FirstOrDefault()["Description"].ToString();
                        double productsHolding = 0;

                        foreach (DataRow proRow in productGroup)
                        {
                            double productHolding = Convert.ToDouble(proRow["Holding"]);
                            productsHolding += productHolding;
                        }

                        if (double.IsNaN(productsHolding / totalHolding))
                            assetTable.Cells[index, 2].Text = "-";
                        else
                            assetTable.Cells[index, 2].Text = (productsHolding / totalHolding).ToString("P2");

                        assetTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                        assetTable.Cells[index, 3].Text = productsHolding.ToString("C");
                        assetTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                        index++;
                    }
                    assetTable.Rows[index].Height = .2;
                    assetTable.Rows[index].Style.FontBold = true;
                    assetTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                    assetTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                    assetTable.Rows[index].Style.FontBold = true;
                    assetTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                    assetTable.Cells[index, 0].Text = "TOTAL";

                    assetTable.Cells[index, 3].Text = totalHolding.ToString("C");
                    assetTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    RenderText gridHeader = new RenderText();

                    string assetName = string.Empty;

                    if (assetClass == "ManualAsset")
                        assetName = "Manual Assets\n\n";
                    else
                    {
                        var assetNameObj = this.assets.Where(asset => asset.Name == assetClass).FirstOrDefault();
                        if (assetNameObj != null)
                            assetName = this.assets.Where(asset => asset.Name == assetClass).FirstOrDefault().Description + "\n\n";
                        else
                            assetName = assetClass + "\n\n";
                    }
                    gridHeader.Text = assetName;
                    gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                    gridHeader.Style.FontSize = 11;
                    gridHeader.Style.FontBold = true;

                    RenderText lineBreak = new RenderText();
                    lineBreak.Text = "\n\n\n";

                    assetSummaryTotalTable.Cells[totalIndex, 0].Text = assetClass;
                    assetSummaryTotalTable.Cells[totalIndex, 1].Text = assetName;

                    if (double.IsNaN(totalHolding / grandTotal))
                        assetSummaryTotalTable.Cells[totalIndex, 2].Text = "-";
                    else
                        assetSummaryTotalTable.Cells[totalIndex, 2].Text = (totalHolding / grandTotal).ToString("P2");

                    assetSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                    assetSummaryTotalTable.Cells[totalIndex, 3].Text = totalHolding.ToString("C");
                    assetSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    doc.Body.Children.Add(gridHeader);
                    doc.Body.Children.Add(assetTable);
                    doc.Body.Children.Add(lineBreak);

                    totalIndex++;
                }
            }

            assetSummaryTotalTable.Rows[totalIndex].Height = .2;
            assetSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            assetSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            assetSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            assetSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            assetSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            assetSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            assetSummaryTotalTable.Cells[totalIndex, 1].Text = "";
            assetSummaryTotalTable.Cells[totalIndex, 3].Text = grandTotal.ToString("C");
            assetSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
             doc.PageLayout.PageSettings.Landscape = false;
            return doc;
        }

        public void SetAssetsSummaryGrid(C1PrintDocument doc, IOrderedEnumerable<DataRow> rows, string serviceName)
        {

            int index = 1;

            if (rows.Count() > 0)
            {
                RenderTable capitalTable = new RenderTable();

                double OpeningBalance = 0;
                double TransferInOut = 0;
                double Income = 0;
                double AppicationRedemption = 0;
                double TaxInOut = 0;
                double Expense = 0;
                double InternalCashMovement = 0;
                double ClosingBalance = 0;
                double ChangeInInvestmentValue = 0;

                capitalTable.Style.FontSize = 8;

                capitalTable.Rows[0].Height = .2;
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.FontSize = 7;
                capitalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[0, 0].Text = "Month";
                capitalTable.Cells[0, 1].Text = "Opening Balance";
                capitalTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 2].Text = "Transfer In/Out";
                capitalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 3].Text = "Income";
                capitalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 4].Text = "Invesments";
                capitalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 5].Text = "Tax & Expenses";
                capitalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 6].Text = "Internal Cash Mvt";
                capitalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 7].Text = "Closing Balance";
                capitalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 8].Text = "Change in Investment";
                capitalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cols[0].Width = .5;
                capitalTable.Cols[1].Width = 1.2;
                capitalTable.Cols[2].Width = 1.2;
                capitalTable.Cols[3].Width = 1.2;
                capitalTable.Cols[4].Width = 1.2;
                capitalTable.Cols[5].Width = 1.2;
                capitalTable.Cols[6].Width = 1.2;
                capitalTable.Cols[7].Width = 1.2;
                capitalTable.Cols[8].Width = 1.5;

                foreach (DataRow row in rows)
                {

                    capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    capitalTable.Rows[index].Height = .3;
                    capitalTable.Cells[index, 0].Text = ((DateTime)row[CapitalReportDS.MONTH]).ToString("MMM-yy").ToUpper();

                    OpeningBalance += Convert.ToDouble(row[CapitalReportDS.OPENINGBAL]);
                    TransferInOut += Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]);
                    Income += Convert.ToDouble(row[CapitalReportDS.INCOME]);
                    AppicationRedemption += Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION]);
                    TaxInOut += Convert.ToDouble(row[CapitalReportDS.TAXINOUT]);
                    Expense += Convert.ToDouble(row[CapitalReportDS.EXPENSE]);
                    InternalCashMovement += Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]);
                    ClosingBalance += Convert.ToDouble(row[CapitalReportDS.CLOSINGBALANCE]);
                    ChangeInInvestmentValue += Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]);

                    capitalTable.Cells[index, 1].Text = Convert.ToDouble(row[CapitalReportDS.OPENINGBAL]).ToString("C");
                    capitalTable.Cells[index, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 2].Text = Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]).ToString("C");
                    capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 3].Text = Convert.ToDouble(row[CapitalReportDS.INCOME]).ToString("C");
                    capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 4].Text = Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION]).ToString("C");
                    capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 5].Text = Convert.ToDouble(row[CapitalReportDS.EXPENSE]).ToString("C");
                    capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 6].Text = Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]).ToString("C");
                    capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 7].Text = Convert.ToDouble(row[CapitalReportDS.CLOSINGBALANCE]).ToString("C");
                    capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 8].Text = Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]).ToString("C");
                    capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;


                    index++;
                }


                capitalTable.Rows[index].Height = .2;
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[index, 0].Text = "TOTAL";

                capitalTable.Cells[index, 2].Text = TransferInOut.ToString("C");
                capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 3].Text = Income.ToString("C");
                capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 4].Text = AppicationRedemption.ToString("C");
                capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 5].Text = Expense.ToString("C");
                capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 6].Text = InternalCashMovement.ToString("C");
                capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(capitalTable);
                doc.Body.Children.Add(lineBreak);
            }

        }

        protected void GenerateAssetSummaryReport(object sender, EventArgs e)
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            this.assets = organization.Assets;
            OrganisationListingDS organisationListingDS = new OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.AssetSummaryOrg;
            if (this.c1ValutionDate.Date.HasValue)
                organisationListingDS.AsOfDate = c1ValutionDate.Date.Value;

            organization.GetData(organisationListingDS);
            this.assets = organization.Assets;
            this.PresentationData = organisationListingDS;
            
            this.PopulatePage(this.PresentationData);
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        protected void ClientAssetsReportClick()
        {
            string reportName = "Report_" + Guid.NewGuid().ToString();
            C1ReportViewer.RegisterDocument(reportName, MakeDocAssetSummary);
            this.C1ReportViewerAssetClass.FileName = reportName;
            C1ReportViewerAssetClass.ReportName = reportName;
        }
    }
}
