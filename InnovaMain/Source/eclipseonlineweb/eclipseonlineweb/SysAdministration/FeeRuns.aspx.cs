﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.CM.Organization.Data;

namespace eclipseonlineweb
{
    public partial class FeeRuns : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void GetBMCData()
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            OrganisationListingDS organisationListingDS = new OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.FeeRuns;

            organization.GetData(organisationListingDS);
            this.PresentationData = organisationListingDS;

            UMABroker.ReleaseBrokerManagedComponent(organization);
        }


        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
        }

        public override void LoadPage()
        {
            base.LoadPage();

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
                ((SetupMaster)Master).IsAdmin = "1";
        }

        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e) { }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "details")
            {
                string[] IdIns = e.CommandArgument.ToString().Split(',');
                //string ID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ENTITYCIID_FIELD"].ToString();
                Response.Redirect("~/SysAdministration/FeeRunsDetails.aspx?ID=" + IdIns[0].ToString() + "&INS=" + IdIns[1].ToString() +"&PageName=" + "FeeRun");
            }
            else if (e.CommandName.ToLower() == "run")
            {
                UMABroker.SaveOverride = true;

                string ID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ENTITYCIID_FIELD"].ToString();
                IBrokerManagedComponent bmc = this.UMABroker.GetBMCInstance(new Guid(ID));
                ExecuteFeeRunDS executeFeeRunDS = new ExecuteFeeRunDS();
                bmc.SetData(executeFeeRunDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                GetBMCData();
                this.PresentationGrid.Rebind();
            }
            else if (e.CommandName.ToLower() == "delete")
            {
                UMABroker.SaveOverride = true;
                GridDataItem gDataItem = e.Item as GridDataItem;
                Guid ID = new Guid(gDataItem["ENTITYCIID_FIELD"].Text);
                GroupFeeRunDS groupFeeRunDS = new Oritax.TaxSimp.Common.Data.GroupFeeRunDS();
                groupFeeRunDS.DataSetOperationType = DataSetOperationType.DeletSingle;
                groupFeeRunDS.FeeRunID = ID;
                SaveData(ID.ToString(), groupFeeRunDS);

                UMABroker.DeleteCMInstance(ID);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                GetBMCData();
                this.PresentationGrid.Rebind();

            }

        }

        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) { }
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e) { }
        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("FeeRuns".Equals(e.Item.OwnerTableView.Name))
            {
                GridEditFormInsertItem EditForm = (GridEditFormInsertItem)e.Item;

                UMABroker.SaveOverride = true;

                //string description = ((TextBox)(((GridEditableItem)(e.Item))["DESCRIPTION"].Controls[0])).Text;
                string shortname = ((TextBox)(((GridEditableItem)(e.Item))["SHORTNAME"].Controls[0])).Text;
                int year = int.Parse(((TextBox)(((GridEditableItem)(e.Item))["YEAR"].Controls[0])).Text);
                RadComboBox combo = (RadComboBox)EditForm["MONTHCOMBO"].Controls[0];
                int month = int.Parse(combo.SelectedItem.Value);

                ILogicalModule created = null;
                ILogicalModule organization = null;

                IOrganization org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                created = this.UMABroker.CreateLogicalCM(new Guid("AE8DCE8C-0343-461C-8152-7FA4F4E22427"), shortname.ToUpper() + " - " + DateTime.Today.ToLongDateString(), Guid.Empty, null);

                organization = this.UMABroker.GetLogicalCM(org.CLID);
                organization.AddChildLogicalCM(created);
                IBrokerManagedComponent iBMC = (IBrokerManagedComponent)created[created.CurrentScenario];

                FeeRunDetailsDS feeRunDetailsDS = new Oritax.TaxSimp.Common.Data.FeeRunDetailsDS();
                DataTable feeDetailsTable = feeRunDetailsDS.Tables[FeeRunDetailsDS.FEERUNSDETAILSTABLE];

                DataRow feeDetailRow = feeDetailsTable.NewRow();
                //feeDetailRow[FeeRunDetailsDS.DESCRIPTION] = description;
                feeDetailRow[FeeRunDetailsDS.SHORTNAME] = shortname;
                feeDetailRow[FeeRunDetailsDS.YEAR] = year;
                feeDetailRow[FeeRunDetailsDS.MONTHINT] = month;
                feeDetailRow[FeeRunDetailsDS.RUNTYPEENUM] = FeeRunType.Global;
                feeDetailsTable.Rows.Add(feeDetailRow);

                iBMC.SetData(feeRunDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.PresentationGrid.Rebind();
            }
        }

        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e) { }

        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;
                GridDropDownListColumnEditor editor = null;
                IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

                if ("FeeRuns".Equals(e.Item.OwnerTableView.Name))
                {
                    IDictionary<Enum, string> servicesValue = Enumeration.ToDictionary(typeof(ServiceTypes));
                    editor = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("MONTHCOMBO"));

                    Dictionary<string, int> months = new Dictionary<string, int>();
                    months.Add("January", 1);
                    months.Add("February", 2);
                    months.Add("March", 3);
                    months.Add("April", 4);
                    months.Add("May", 5);
                    months.Add("June", 6);
                    months.Add("July", 7);
                    months.Add("August", 8);
                    months.Add("September", 9);
                    months.Add("October", 10);
                    months.Add("November", 11);
                    months.Add("December", 12);

                    editor.DataSource = months;
                    editor.DataTextField = "Key";
                    editor.DataValueField = "Value";
                    editor.DataBind();
                }

                UMABroker.ReleaseBrokerManagedComponent(organization);
            }
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DataView View = new DataView(this.PresentationData.Tables[FeeRunListDS.FEERUNSTABLE]);
            View.Sort = FeeRunListDS.FEERUNDATE + " DESC";
            this.PresentationGrid.DataSource = View.ToTable();
        }
    }
}
