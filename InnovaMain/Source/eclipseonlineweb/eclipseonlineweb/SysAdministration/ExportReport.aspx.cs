﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using eclipseonlineweb.Export;
using Oritax.TaxSimp.Data;
using System.Diagnostics;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class ExportReport : UMABasePage
    {
        ExportType _type;
        public override void LoadPage()
        {
            ScriptManager sm = ScriptManager.GetCurrent(Page);

            if (sm != null)
                sm.RegisterPostBackControl(this.btnExport);

            if (!IsPostBack)
            {
                if (Enum.TryParse(Request.Params["Type"], out _type))
                {
                    switch (_type)
                    {
                        case ExportType.AdviserDetailsCSVExport:
                            lblDesc.Text = "Export Adviser Details Data";
                            Panel1.GroupingText = "Export Adviser Details";
                            btnExport.Text = "Export Adviser Details";
                            hyp_sampleFile.Visible = false;
                            break;
                        case ExportType.BankWestAccountOpeningCSVExport:
                            lblDesc.Text = "Export BankWest Account Opening Data";
                            Panel1.GroupingText = "Export BankWest Account Opening";
                            btnExport.Text = "Export BankWest Account Opening";
                            hyp_sampleFile.Visible = false;
                            break;
                        case ExportType.DesktopBrokerCSVExport:
                            lblDesc.Text = "Export Desktop Broker Information";
                            Panel1.GroupingText = "Export Desktop Broker Information";
                            btnExport.Text = "Export Desktop Broker Information";
                            hyp_sampleFile.Visible = false;
                            break;
                        case ExportType.FinSimplicityInvestmentCSVExport:
                            lblDesc.Text = "Export Financial Simplicity Investment Data";
                            Panel1.GroupingText = "Export Financial Simplicity Investment";
                            btnExport.Text = "Export Financial Simplicity Investment";
                            hyp_sampleFile.Visible = false;
                            break;
                        case ExportType.StateStreetCSVExport:
                            lblDesc.Text = "Export State Street Information";
                            Panel1.GroupingText = "Export State Street Information";
                            btnExport.Text = "Export State Street Information";
                            hyp_sampleFile.Visible = false;
                            break;
                        case ExportType.StateStreetCSVExportRecon:
                            lblDesc.Text = "Export State Street Information Bank Reconcile";
                            Panel1.GroupingText = "Export State Street Bank Reconcile Information";
                            btnExport.Text = "Export State Street Bank Reconcile Information";
                            hyp_sampleFile.Visible = false;
                            break;
                        case ExportType.ExportClientDetails:
                            lblDesc.Text = "Export Client Details";
                            Panel1.GroupingText = "Export Client Details";
                            btnExport.Text = "Export Client Details";
                            hyp_sampleFile.Visible = false;
                            break;


                        case ExportType.InstructionsExport:
                            lblDesc.Text = "Export Order Pad Instructions";
                            Panel1.GroupingText = "Export Order Pad Instructions";
                            btnExport.Text = "Export Order Pad Instructions";
                            if (IsTestApp())
                            {
                                hyp_sampleFile.Visible = true;
                                hyp_sampleFile.NavigateUrl = "~/Templates/ExportSampleFiles/InstructionsExport.zip";
                            }
                            break;
                    }
                }
            }
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected void Export(object sender, EventArgs e)
        {
            try
            {
                if (Enum.TryParse(Request.Params["Type"], out _type))
                {
                    var expCommand = ExportFactory.GetExportCommand(_type);
                    if (expCommand != null)
                    {
                        expCommand.Broker = UMABroker;
                        expCommand.User = GetCurrentUser();
                        var Data = expCommand.Export();
                        bool IsEmpty = true;
                        foreach (DataTable dt in Data.Tables)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                IsEmpty = false;  
                            }
                        }
                        if (!IsEmpty)
                        {
                            byte[] bytes = expCommand.GetByteArray(Data);
                            if (_type == ExportType.BankWestAccountOpeningCSVExport)
                            {
                                Response.Clear();
                                Response.ClearContent();
                                Response.ClearHeaders();
                                HttpContext.Current.Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
                                Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
                                Response.AppendHeader("Content-Type", "application/zip");
                                Response.AppendHeader("Content-disposition", "attachment; filename=BankWest Multi Account Opening Utility["+DateTime.Now.ToString("dd-MM-yyyy")+"].zip");
                                Response.BinaryWrite(bytes);
                                Response.Flush();
                                Response.End();
                            }
                            else
                            {
                                Session.Remove("ExportFileBytes");
                                Session["ExportFileBytes"] = bytes;
                                string script = string.Format("window.open('ExportReportPopup.aspx?fileName={0}&filetype={1}',null,'height=200,width=400,status=yes,toolbar=no,menubar=no,location=no');", _type, expCommand.FileType);
                                ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "OpenNewWindow", script, true);
                            }
                        }
                        else if (_type == ExportType.InstructionsExport)
                        {
                            lblMessage.Text = "There is no approved orders to export.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strScript = "<script>";
                strScript = strScript + "alert('" + ex.Message + "');";
                strScript = strScript + "</script>";
                ClientScript.RegisterStartupScript(GetType(), "ClientScript", strScript);
            }
        }

    }
}