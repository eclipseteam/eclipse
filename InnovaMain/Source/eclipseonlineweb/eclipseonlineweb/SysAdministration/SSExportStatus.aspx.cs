﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb
{
    public partial class SSExportStatus : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        #region Telerik

        #region ExportStatusGrid
        
        protected void ExportStatusGridItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            if ((e.Item).ItemIndex != -1)
            {
                DataRow dr = ((DataRowView)((e.Item).DataItem)).Row;
                (e.Item.FindControl("CheckBox1") as CheckBox).Checked = bool.Parse(dr["SSExport"].ToString());

            }
        }

        protected void ExportStatusGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var exportStatusDS = new ExportStatusDS
            {
                Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() },
                Command = (int)WebCommands.GetAllClientDetails,
                ExportMode = ExportMode.StateStreet,
            };
            if (organization != null)
            {
                organization.GetData(exportStatusDS);
                UMABroker.ReleaseBrokerManagedComponent(organization);
            }
            PresentationData = exportStatusDS;
            var exportStatusListView = new DataView(exportStatusDS.Tables[exportStatusDS.ExportStatusTable.TableName]) { Sort = exportStatusDS.ExportStatusTable.TRADINGNAME + " ASC" };
            ExportStatusGrid.DataSource = exportStatusListView.ToTable();

        }

        protected void ToggleRowSelection(object sender, EventArgs e)
        {
            ((sender as CheckBox).NamingContainer as GridItem).Selected = (sender as CheckBox).Checked;
            bool checkHeader = true;
            foreach (GridDataItem dataItem in ExportStatusGrid.MasterTableView.Items)
            {
                if (!(dataItem.FindControl("CheckBox1") as CheckBox).Checked)
                {
                    checkHeader = false;
                    break;
                }
            }
            GridHeaderItem headerItem = ExportStatusGrid.MasterTableView.GetItems(GridItemType.Header)[0] as GridHeaderItem;
            (headerItem.FindControl("headerChkbox") as CheckBox).Checked = checkHeader;
        }

        protected void ToggleSelectedState(object sender, EventArgs e)
        {
            CheckBox headerCheckBox = (sender as CheckBox);
            foreach (GridDataItem dataItem in ExportStatusGrid.MasterTableView.Items)
            {
                (dataItem.FindControl("CheckBox1") as CheckBox).Checked = headerCheckBox.Checked;
                dataItem.Selected = headerCheckBox.Checked;
            }
        }
        #endregion

        #endregion
        
        protected void btnSave_OnClick(object o, ImageClickEventArgs e)
        {
            ExportStatusDS exportStatusDS = new ExportStatusDS
            {
                Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() },
                Command = (int)WebCommands.UpdateAllClientDetails,
                ExportMode = ExportMode.StateStreet,
            };
            foreach (GridDataItem dataItem in ExportStatusGrid.MasterTableView.Items)
            {
                SetExportStatusDS(dataItem, exportStatusDS);
            }

            SaveOrganizanition(exportStatusDS);
        }

        private void SetExportStatusDS(GridDataItem dataItem, ExportStatusDS ds)
        {
            CheckBox cb = (dataItem.FindControl("CheckBox1") as CheckBox);

            DataRow dataRow = ds.ExportStatusTable.NewRow();

            dataRow[ds.ExportStatusTable.CID] = dataItem[ds.ExportStatusTable.CID].Text;
            dataRow[ds.ExportStatusTable.CLID] = dataItem[ds.ExportStatusTable.CLID].Text;
            dataRow[ds.ExportStatusTable.CSID] = dataItem[ds.ExportStatusTable.CSID].Text;
            dataRow[ds.ExportStatusTable.SSEXPORT] = cb.Checked;

            ds.ExportStatusTable.Rows.Add(dataRow);
        }


    }
}
