﻿using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.AmmWs;
using eclipseonlineweb.WebControls;
using Oritax.TaxSimp.Security;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb.SysAdministration
{
    public partial class ImportTdRates : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            return true;
        }

        protected override void Intialise()
        {
            ImportMessageControl.GridImportMessages.ColumnCreated += GridImportMessages_ColumnCreated;
            ImportMessageControl.GirdInvestmentCodeError.ColumnCreated += GridImportErrorMessages_ColumnCreated;
            ImportMessageControl.GridImportMessages.ItemDataBound += GridImportMessages_ItemDataBound;

            if (!IsPostBack)
            {
                ImportMessageControl.Clear();
            }

            ImportMessageControl.Completed += text =>
                {
                    switch (text.ToLower())
                    {
                        case "next":
                            SendData();
                            break;

                        default:
                            ClearMessageControls();
                            break;
                    }
                };
        }

        public override void LoadPage()
        {
            if (!IsPostBack)
            {
                LoadControls();
            }

            ImportMessageControl.ShowHideSampleFileLink("javascript: return false;");
        }
        private void LoadControls()
        {
            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            if (org != null)
            {
                var inst = org.Institution;
                ddlInstitute.Items.Clear();
                ddlInstitute.DataSource =
                    inst.Where(ss => ss.Name.ToLower() == "fiig" || ss.Name.ToLower() == "amm").ToList();
                ddlInstitute.DataValueField = "Id";
                ddlInstitute.DataTextField = "Name";
                ddlInstitute.DataBind();

                ddlInstitute.Enabled = false;
                ddlInstitute.SelectedIndex = ddlInstitute.Items.FindItemByText("amm", true).Index;
            }
            UMABroker.ReleaseBrokerManagedComponent(org);
        }

        void GridImportMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            SetDataGridColumns(e);
        }

        void GridImportErrorMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            SetDataGridColumns(e);
        }

        private void SetValidationGird(ImportProcessDS ds)
        {
            //ImportMessageControl.Clear();
            using (var dv = new DataView(ds.Tables[0]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }
            using (var dv = new DataView(ds.Tables[0]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                var missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    if (missingItems.Select(string.Format("MissingItem='{0}'", row["InvestmentCode"])).Length == 0)
                    {
                        DataRow dr = missingItems.NewRow();
                        dr["MissingItem"] = row["InvestmentCode"];
                        missingItems.Rows.Add(dr);
                    }
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();

                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingInvestmentCodeAdded;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }

            int successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
            if (ds.Tables[0].Rows.Count == 0 || successfulRows == 0)
            {
                ImportMessageControl.SetButtons(ButtonTypes.CLOSE);
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sErrorsCannotContinue, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Error);
                ImportMessageControl.TextMessage.Text = "";
            }
            else
            {
                ImportMessageControl.SetButtons(ButtonTypes.NEXTCANCEL);
                //sucess
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sValidationCompleted, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
            }
        }

        private void SetCompletionGird(ImportProcessDS ds)
        {
            ImportMessageControl.Clear();
            using (var dv = new DataView(ds.Tables[0]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }

            using (var dv = new DataView(ds.Tables[0]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                var missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    if (missingItems.Select(string.Format("MissingItem='{0}'", row["InvestmentCode"])).Length == 0)
                    {
                        var dr = missingItems.NewRow();
                        dr["MissingItem"] = row["InvestmentCode"];
                        missingItems.Rows.Add(dr);
                    }
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();

                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingInvestmentCodeAdded;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }

            var successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
            ImportMessageControl.SetButtons(ButtonTypes.FINISH);
            ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sImportedSuccessfully, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
        }

        private void SendData()
        {
            var dt = GetTdRates(Guid.Parse(ddlInstitute.SelectedValue));
            var ds = new ImportProcessDS();
            ds.Tables.Add(dt.Copy());

            ds.Unit = new OrganizationUnit
            {
                CurrentUser = GetCurrentUser()
            };
            ds.Command = (int)WebCommands.ImportTDRates;

            var objUser = (DBUser)UMABroker.GetBMCInstance(Page.User.Identity.Name, "DBUser_1_1");
            var dbUserDetailsDs = new DBUserDetailsDS();
            objUser.GetData(dbUserDetailsDs);

            SaveOrganizanition(ds);
            SetCompletionGird(ds);
            ImportMessageControl.SetImportSuccessFileMsg("Data imported successfully.",
                "javascript:return false;");
        }

        private void ClearMessageControls()
        {
            ImportMessageControl.Clear();
            ImportMessageControl.TextMessage.Text = "";
        }

        private static void SetDataGridColumns(GridColumnCreatedEventArgs e)
        {
            string colName = e.Column.UniqueName.ToLower();
            switch (colName)
            {
                case "message":
                    e.Column.HeaderText = "Message";
                    break;
                case "haserrors":
                    e.Column.HeaderText = "Has Errors";
                    e.Column.Visible = false;
                    break;
                case "ismissingitem":
                    e.Column.HeaderText = "Is Missing Item";
                    e.Column.Visible = false;
                    break;
                case "missingitem":
                    e.Column.HeaderText = "Missing Investment Code";
                    break;
                case "id":
                case "instituteid":
                    e.Column.Visible = false;
                    break;
            }
        }

        private DataTable GetTdRates(Guid instituteId)
        {
            var dt = new DataTable();
            dt.Columns.Add(new DataColumn("RateID", typeof(int)));
            dt.Columns.Add(new DataColumn("Name", typeof(String)));
            dt.Columns.Add(new DataColumn("Type", typeof(String)));
            dt.Columns.Add(new DataColumn("ID", typeof(Guid)));
            dt.Columns.Add(new DataColumn("InstituteID", typeof(Guid)));
            dt.Columns.Add(new DataColumn("Min", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Max", typeof(decimal)));
            dt.Columns.Add(new DataColumn("MaximumBrokeage", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Days30", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Days60", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Days90", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Days120", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Days150", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Days180", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Days270", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Years1", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Years2", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Years3", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Years4", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Years5", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Status", typeof(String)));
            dt.Columns.Add(new DataColumn("Date", typeof(DateTime)));
            dt.Columns.Add(new DataColumn("Ongoing", typeof(decimal)));
            var objTdws = new TDWS();
            var objProviderRates =
                objTdws.GetAllRates(ConfigurationManager.AppSettings["AmmTestApiKey"]);

            var orgCm = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            if (orgCm != null)
            {
                var inst = orgCm.Institution;

                foreach (ProviderRate rate in objProviderRates.ProviderRate)
                {
                    if (rate.providerProductType == "Term Deposits")
                    {
                        var dr = dt.NewRow();
                        dr["RateID"] = rate.providerRateID;
                        dr["Name"] = rate.providerName;
                        dr["Type"] = rate.providerType;
                        dr["RateID"] = rate.providerRateID;
                        dr["ID"] = Guid.NewGuid();
                        dr["InstituteID"] = instituteId;

                        if (!string.IsNullOrEmpty(rate.providerRateLowerLimit))
                            dr["Min"] = Decimal.Parse(rate.providerRateLowerLimit);

                        if (!string.IsNullOrEmpty(rate.providerRateUpperLimit))
                            dr["Max"] = Decimal.Parse(rate.providerRateUpperLimit);

                        if (!string.IsNullOrEmpty(rate.providerRateBrokerage))
                            dr["MaximumBrokeage"] = Decimal.Parse(rate.providerRateBrokerage);

                        if (!string.IsNullOrEmpty(rate.provider1mRate))
                            dr["Days30"] = Decimal.Parse(rate.provider1mRate);

                        if (!string.IsNullOrEmpty(rate.provider2mRate))
                            dr["Days60"] = Decimal.Parse(rate.provider2mRate);

                        if (!string.IsNullOrEmpty(rate.provider3mRate))
                            dr["Days90"] = Decimal.Parse(rate.provider3mRate);

                        if (!string.IsNullOrEmpty(rate.provider4mRate))
                            dr["Days120"] = Decimal.Parse(rate.provider4mRate);

                        if (!string.IsNullOrEmpty(rate.provider5mRate))
                            dr["Days150"] = Decimal.Parse(rate.provider5mRate);

                        if (!string.IsNullOrEmpty(rate.provider6mRate))
                            dr["Days180"] = Decimal.Parse(rate.provider6mRate);

                        if (!string.IsNullOrEmpty(rate.provider9mRate))
                            dr["Days270"] = Decimal.Parse(rate.provider9mRate);

                        if (!string.IsNullOrEmpty(rate.provider1yRate))
                            dr["Years1"] = Decimal.Parse(rate.provider1yRate);

                        if (!string.IsNullOrEmpty(rate.provider2yRate))
                            dr["Years2"] = Decimal.Parse(rate.provider2yRate);

                        if (!string.IsNullOrEmpty(rate.provider3yRate))
                            dr["Years3"] = Decimal.Parse(rate.provider3yRate);

                        if (!string.IsNullOrEmpty(rate.provider4yRate))
                            dr["Years4"] = Decimal.Parse(rate.provider4yRate);

                        if (!string.IsNullOrEmpty(rate.provider5yRate))
                            dr["Years5"] = Decimal.Parse(rate.provider5yRate);
                        
                        dr["Status"] = "Uploaded";
                        dr["Date"] = DateTime.Now;
                        dr["Ongoing"] = 0;
                        dt.Rows.Add(dr);
                    }
                }
            }
            return dt;
        }

        protected void btnImport_OnClick(object sender, EventArgs e)
        {
            var dt = GetTdRates(Guid.Parse(ddlInstitute.SelectedValue));
            WriteXml(dt);
            var ds = new ImportProcessDS();
            ds.Tables.Add(dt.Copy());
            ds.Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ValidateTDRates;
            SaveOrganizanition(ds);
            SetValidationGird(ds);
            PresentationData = ds;
        }

        private void WriteXml(DataTable dt)
        {
            const string fileName = "AMM_TDRates.xml";
            var date = DateTime.Now;
            var distributionDictory = Server.MapPath("~/App_Data/Import/");
            if (!Directory.Exists(distributionDictory))
                Directory.CreateDirectory(distributionDictory);
            DataSet dsXml = new DataSet();
            dsXml.Tables.Add(dt);
            string fileContent = dsXml.GetXml();
            byte[] myData = new byte[fileContent.Length * sizeof(char)];
            System.Buffer.BlockCopy(fileContent.ToCharArray(), 0, myData, 0, myData.Length);
            string fileLocation =
                distributionDictory + date.ToString("ddMMyyhhmmsstt") + fileName;
            WebUtilities.Utilities.WriteToFile(fileLocation, ref myData);
        }

        void GridImportMessages_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item).ItemIndex == -1) return;

            var dr = ((DataRowView)((e.Item).DataItem)).Row;
            if (bool.Parse(dr["HasErrors"].ToString()))
            {
                e.Item.ForeColor = Color.Red;
            }
        }
    }
}