﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="NonIndividualList.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.NonIndividualList" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register Src="~/Controls/NonIndividual.ascx" TagName="NonIndividual" TagPrefix="uc2" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <style>
        .holder
        {
            width: 100%;
            display: block;
            z-index: 6;
        }
        .content
        {
            background: #fff;
            z-index: 7; /*  padding: 28px 26px 33px 25px;*/
        }
        .popup
        {
            border-radius: 7px;
            background: #6b6a63;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 1000px;
            top: 10%;
            left: 40%;
            margin-left: -300px;
            margin-top: -40px;
            z-index: 6;
        }
        
        .popup1
        {
            border-radius: 7px;
            background: #6b6a63;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 1000px;
            top: 20%;
            left: 50%;
            margin-left: -400px;
            margin-top: -40px;
            z-index: 6;
        }
        .overlay
        {
            width: 100%;
            opacity: 0.65;
            height: 100%;
            left: 0; /*IE*/
            top: 0;
            text-align: center;
            z-index: 5;
            position: fixed;
            background-color: #444444;
        }
        
        .wijmo-wijgrid .wijmo-wijgrid-groupheaderrow td
        {
            background-color: #DAE6F4;
            color: #000000;
            font-size: smaller;
            font-weight: bold;
            vertical-align: middle;
        }
        .wijmo-wijgrid .wijmo-wijgrid-footerrow td
        {
            font-weight: bolder;
            text-align: right;
            font-size: smaller;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <telerik:RadButton ID="btnAddNonIndividual" runat="server" ToolTip="Create a Non-Individual"
                                OnClick="btnAddNonIndividual_Click">
                                <ContentTemplate>
                                    <img src="../images/add-icon.png" alt="" class="btnImageWithText" />
                                </ContentTemplate>
                            </telerik:RadButton>
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Non-Individuals"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <asp:Panel runat="server" ID="pnlMainGrid" Visible="true">
                <telerik:RadGrid ID="PresentationGird" runat="server" AutoGenerateColumns="False"
                    PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true"
                    GridLines="Horizontal" AllowAutomaticUpdates="true" AllowFilteringByColumn="true"
                    OnNeedDataSource="PresentationGird_OnNeedDataSource" OnItemCommand="PresentationGird_OnItemCommand"
                    EnableViewState="true" ShowFooter="True">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AutoGenerateColumns="false">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Cid" Display="false" HeaderButtonType="TextButton"
                                DataField="Cid" UniqueName="Cid">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="CLid" Display="false" HeaderButtonType="TextButton"
                                DataField="CLID" UniqueName="CLid">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="CSid" Display="False" HeaderButtonType="TextButton"
                                DataField="CSID" UniqueName="CSid">
                            </telerik:GridBoundColumn>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                                <HeaderStyle Width="2%"></HeaderStyle>
                                <ItemStyle CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridEditCommandColumn>                            
                            <telerik:GridBoundColumn FilterControlWidth="120px" HeaderStyle-Width="7%" ItemStyle-Width="200px"
                                SortExpression="Name" HeaderText="Name" AutoPostBackOnFilter="False" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Name" UniqueName="Name">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="8%" ItemStyle-Width="120px"
                                SortExpression="EmailAddress" HeaderText="Email" AutoPostBackOnFilter="False"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="EmailAddress" UniqueName="EmailAddress">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="100px" HeaderStyle-Width="4%"
                                SortExpression="Type" HeaderText="Type" AutoPostBackOnFilter="False"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="Type" UniqueName="Type">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </telerik:GridBoundColumn>
                             <telerik:GridDateTimeColumn FilterControlWidth="100px" HeaderStyle-Width="4%"
                                SortExpression="DateOfCreation" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Date Of Creation" AutoPostBackOnFilter="False"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="DateOfCreation" UniqueName="DateOfCreation">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </telerik:GridDateTimeColumn>
                            <telerik:GridBoundColumn FilterControlWidth="250px"
                                SortExpression="Description" HeaderText="Description" AutoPostBackOnFilter="False"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="Description" UniqueName="Description">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn CommandName="Delete" Text="Delete" ButtonType="ImageButton"
                                ConfirmText="Are you sure you want to delete the selected record?">
                                <HeaderStyle Width="2%"></HeaderStyle>
                                <ItemStyle Width="2%" HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                    </telerik:RadGrid>
            </asp:Panel>
            <div id="NonIndividualModal" runat="server" visible="false" class="holder">
                <div class="popup">
                    <div class="content">
                        <uc2:NonIndividual ID="NonIndividualControl" runat="server" />
                    </div>
                </div>
            </div>
            <div class="overlay" id="OVER" visible="False" runat="server">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
