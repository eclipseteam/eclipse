﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class OrderPadUtil : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void Intialise()
        {
            base.Intialise();
           
        }

        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
        }

        protected void SortMainList(object sender, C1GridViewSortEventArgs e)
        {
          
        }

        protected void Filter(object sender, C1GridViewFilterEventArgs e)
        {
            e.Values[0] = ((string)e.Values[0]).Trim();

            var ds=this.PresentationData as InstanceCMDS;
            DataView entityView = new DataView(ds.InstanceCMDetailsTable);
          
            this.InstanceListGrid.DataSource = entityView.ToTable();
            InstanceListGrid.DataBind();
        }

        protected override void GetBMCData()
        {
          
        }

        private void ShowCms(OrganizationType Type)
        {

            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            InstanceCMDS MIS = new InstanceCMDS();
            MIS.Command = (int)Oritax.TaxSimp.Commands.WebCommands.GetOrganizationUnitsByType;
            MIS.Unit = new Oritax.TaxSimp.Data.OrganizationUnit
            {
                CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                Type = ((int)Type).ToString()

            };
            org.GetData(MIS);
            this.UMABroker.ReleaseBrokerManagedComponent(org);

            this.PresentationData = MIS;
            InstanceListGrid.DataSource = MIS;
            InstanceListGrid.DataBind();
        }


      

        public override void LoadPage()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");

            if (objUser.Name!="Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
                Response.Redirect("AccountsFUM.aspx");
            
            UMABroker.ReleaseBrokerManagedComponent(objUser);
        }

      

        public override void PopulatePage(DataSet ds)
        {
            
        }

        protected void ClientList_PageIndexChanging(object sender, C1GridViewPageEventArgs e)
        {
            InstanceListGrid.PageIndex = e.NewPageIndex;
            InstanceListGrid.DataBind();
        }

    

        protected void C1GridView1_SelectedIndexChanging(object sender, C1.Web.Wijmo.Controls.C1GridView.C1GridViewSelectEventArgs e)
        {
        }

        protected void ClientList_DataBound(object sender, EventArgs e)
        {
        }

        protected void OnClick(object sender, EventArgs e)
        {
            ShowCms(OrganizationType.Order);
        }

        protected void Unsetlled_OnClick(object sender, EventArgs e)
        {
            ShowCms(OrganizationType.SettledUnsettled);
        }

        protected void OBPCM_OnClick(object sender, EventArgs e)
        {
            
        }
    }
}
