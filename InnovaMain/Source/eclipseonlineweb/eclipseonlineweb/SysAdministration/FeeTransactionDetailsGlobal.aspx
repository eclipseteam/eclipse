﻿<%@ Page Title="e-Clipse Online Portal - Configure Fees" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="FeeTransactionDetailsGlobal.aspx.cs" Inherits="eclipseonlineweb.FeeTransactionDetailsGlobal" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function GoToPage(url) {
            window.location = url + '?ins=<%:cid %>';
            return false;
        }
    </script>
    <fieldset>
        <table width="100%">
            <tr>
                <td width="10%">
                    <asp:ImageButton ID="btnBack" ToolTip="Back" AlternateText="back" runat="server"
                        ImageUrl="~/images/window_previous.png" OnClick="btnBack_Click" />
                </td>
                <td width="10%">
                </td>
                <td style="width: 30%;" class="breadcrumbgap">
                    <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                    <br />
                    <asp:Label runat="server" ID="lblDetails"></asp:Label>
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
          <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
                OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
                OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
                OnItemDataBound="PresentationGrid_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="CalculatedFees" TableLayout="Fixed" EditMode="EditForms" ShowGroupFooter="true">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                 
                    <GroupByExpressions>
                        <telerik:GridGroupByExpression>
                            <SelectFields>
                                <telerik:GridGroupByField FieldName="FEETYPEDESC" HeaderText="Fee Type" HeaderValueSeparator=" : "></telerik:GridGroupByField>
                            </SelectFields>
                            <GroupByFields>
                                <telerik:GridGroupByField FieldName="FEETYPEDESC" SortOrder="Ascending"></telerik:GridGroupByField>
                            </GroupByFields>
                        </telerik:GridGroupByExpression>
                    </GroupByExpressions>
                    <Columns>
                        <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ID" UniqueName="ID" />
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="FEESUBTYPE" ReadOnly="true"
                            HeaderStyle-Width="14%" ItemStyle-Width="20%" HeaderText="Fee Sub Type" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="FEESUBTYPE" UniqueName="FEESUBTYPE">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="MONTH" HeaderStyle-Width="5%"
                            ItemStyle-Width="5%" HeaderText="Month" ReadOnly="true" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="MONTH" UniqueName="MONTH">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" ReadOnly="true" DefaultInsertValue="2000"
                            SortExpression="YEAR" HeaderStyle-Width="5%" ItemStyle-Width="15%" HeaderText="Year"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="YEAR" UniqueName="YEAR">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" FooterAggregateFormatString="{0:C}"
                            Aggregate="Sum" ReadOnly="true" DefaultInsertValue="2000" SortExpression="CALCULATEDFEES"
                            HeaderStyle-Width="19%" ItemStyle-Width="15%" HeaderText="Calculated Fee" AutoPostBackOnFilter="true"
                            HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"
                            CurrentFilterFunction="Contains" DataFormatString="{0:c}" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="CALCULATEDFEES" UniqueName="CALCULATEDFEES">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" FooterAggregateFormatString="{0:C}"
                            Aggregate="Sum" ReadOnly="true" DefaultInsertValue="2000" SortExpression="AUTOADJUST"
                            HeaderStyle-Width="19%" ItemStyle-Width="15%" HeaderText="Automatic Adjustment"
                            AutoPostBackOnFilter="true" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                            FooterStyle-HorizontalAlign="Right" CurrentFilterFunction="Contains" DataFormatString="{0:c}"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AUTOADJUST" UniqueName="AUTOADJUST">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" FooterAggregateFormatString="{0:C}"
                            Aggregate="Sum" ReadOnly="true" DefaultInsertValue="2000" SortExpression="ADJUST"
                            HeaderStyle-Width="19%" ItemStyle-Width="15%" HeaderText="Manual Adjustment" AutoPostBackOnFilter="true"
                            HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"
                            CurrentFilterFunction="Contains" DataFormatString="{0:c}" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="ADJUST" UniqueName="ADJUST">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" FooterAggregateFormatString="{0:C}"
                            Aggregate="Sum" ReadOnly="true" DefaultInsertValue="2000" SortExpression="FEETOTALADJUST"
                            HeaderStyle-Width="19%" ItemStyle-Width="15%" HeaderText="Total Fee" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" DataFormatString="{0:c}" ShowFilterIcon="true"
                            HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"
                            HeaderButtonType="TextButton" DataField="FEETOTALADJUST" UniqueName="FEETOTALADJUST">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
