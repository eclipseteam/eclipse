﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    EnableEventValidation="true" AutoEventWireup="true" CodeBehind="IFAPartnershipDetail.aspx.cs"
    Inherits="eclipseonlineweb.SysAdministration.IFAPartnershipDetail" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register Src="../Controls/AddressDetailControl.ascx" TagName="AddressDetailControl"
    TagPrefix="uc7" %>
<%@ Register Src="../Controls/BankAccountMappingControl.ascx" TagName="BankAccountMappingControl"
    TagPrefix="uc3" %>
<%@ Register Src="../Controls/IFAPartnershipControl.ascx" TagName="IFAPartnershipControl"
    TagPrefix="uc2" %>
<%@ Register TagPrefix="uc2" TagName="SecurityConfigurationControl" Src="~/Controls/SecurityConfigurationControl.ascx" %>
<%@ Register TagPrefix="uc5" TagName="OrganizationUnitMappingControl" Src="~/Controls/OrganizationUnitMappingControl.ascx" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                        </td>
                        <td width="90%" align="right">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblIfa" Text="IFA"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadTabStrip ID="RadTabStrip1" runat="server" Skin="Vista" MultiPageID="RadMultiPage1"
                SelectedIndex="0">
                <Tabs>
                    <telerik:RadTab Text="IFA Partnership" runat="server" Selected="True" Value="DealerGroupPartnershipHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Address" runat="server" Visible="true" Value="AddressHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Partnerships" runat="server" Visible="true" Value="PartnershipsHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Bank Accounts" runat="server" Visible="true" Value="BankAccountsHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Security" runat="server" Visible="true" Value="SecurityHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Documents" runat="server" Visible="true" Value="DocumentsHeader">
                    </telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage runat="server" ID="RadMultiPage1" SelectedIndex="0">
                <telerik:RadPageView runat="server" ID="APIfaPartnership">
                    <uc2:IFAPartnershipControl ID="IFAPartnershipControl1" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="ApAddress" runat="server">
                    <fieldset style="width: 98%">
                        <div style="text-align: left; float: none; border: 1px; background-color: White;
                            height: 25px; position: relative; vertical-align: middle; margin: 0; padding: 0;">
                            <telerik:RadButton ID="btnSave" Text="Add Client Addresses" ToolTip="Save Changes"
                                OnClick="btnSave_Click" runat="server" Width="32px" Height="32px" BorderStyle="None">
                                <Image ImageUrl="~/images/Save-Icon.png"></Image>
                            </telerik:RadButton>
                        </div>
                    </fieldset>
                    <br />
                    <uc7:AddressDetailControl ID="AddressDetailControl" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="ApPartnerships" runat="server">
                <uc5:OrganizationUnitMappingControl ID="Partnerships" runat="server" UnitPropertyName="Partnerships" TitleText="Partnerships"/>
                </telerik:RadPageView>
                <telerik:RadPageView ID="ApBankAccounts" runat="server">
                    <uc3:BankAccountMappingControl ID="BankAccountMappingControl" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="ApSecurity" runat="server">
                    <uc2:SecurityConfigurationControl ID="SecurityConfiguration" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="APDocuments" runat="server">
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
