﻿using System;
using System.IO;
using System.Web.UI;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Organization.Data;
using System.Xml.Linq;
using System.Data;
using System.Xml;
using Oritax.TaxSimp.CM.Organization;
using System.Collections.Generic;

namespace eclipseonlineweb
{
    public partial class ExportFINSIMPInvestors : UMABasePage
    {
      
        public override void LoadPage()
        {
            this.Form.SubmitDisabledControls = true;
            base.LoadPage();
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected void Export(object sender, EventArgs e)
        {
            var nodes = new List<XNode>();
            IOrganization Organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization; 
            OrganisationListingDS organisationListingDS = new OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.ClientsWithFUMDIFM;
            Organization.GetData(organisationListingDS);
            DataView entityView = new DataView(organisationListingDS.Tables["Entities_Table"]);
            entityView.Sort = "ENTITYNAME_FIELD ASC";
            entityView.RowFilter = "IsClient='true' AND IsDIFM='true'";
            DataTable entityTable = entityView.ToTable();
            entityTable.Columns.Remove("ENTITYCLID_FIELD");
            entityTable.Columns.Remove("ENTITYCSID_FIELD");
            entityTable.Columns.Remove("ENTITYCIID_FIELD");
            entityTable.Columns.Remove("ENTITYCTID_FIELD");
            entityTable.Columns.Remove("ENTITYSCTYPE_FIELD");
            entityTable.Columns.Remove("ENTITYSCSTATUS_FIELD");
            entityTable.Columns.Remove("ENTITYSORT_FIELD");
            entityTable.Columns["ENTITYNAME_FIELD"].ColumnName = "ClientName";

            XElement root = new XElement("e-Clipse_Online_Services");
            entityTable.TableName = "Client";
            DataSet clientsDS = new DataSet("Clients");
            clientsDS.Tables.Add(entityTable);

            using (XmlWriter w = root.CreateWriter()) { clientsDS.WriteXml(w); }
            string xml = root.ToString();

            if (xml.Contains("&"))
                xml = xml.Replace("&", "&amp;");

            string attachment = "attachment; filename=Investors "+DateTime.Now.ToString("ddMMyyyy")+".xml";
            Response.ClearContent();
            Response.ContentType = "application/xml";
            Response.AddHeader("content-disposition", attachment);
            Response.Write(xml);
            Response.End(); 
        }
    }
}
