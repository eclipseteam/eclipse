﻿<%@ Page Title="" Language="C#" MasterPageFile="AdminMaster.master" AutoEventWireup="true"
    CodeBehind="ImportSecuritiesPriceList.aspx.cs" Inherits="eclipseonlineweb.ImportSecuritiesPriceList"
    EnableViewState="false" %>

<%@ Register TagPrefix="uc" TagName="Details" Src="WebControls/ImportMessageControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .padl
        {
            padding-left: 90px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblSelectType" runat="server" Font-Bold="True" Text="Select Type:"></asp:Label>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="cmbSelectType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cmbSelectType_SelectedIndexChanged"
                                Width="140px">
                                <Items>
                                    <telerik:RadComboBoxItem Value="ASXPrice" Text="ASX Price List" />
                                    <telerik:RadComboBoxItem Value="StateStreetPrice" Text="State Street Price List" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <asp:Label ID="lblSelectDate" runat="server" Font-Bold="True" Text="Select Date:"></asp:Label>
                        </td>
                        <td>
                            <telerik:RadDatePicker ID="dtPicker" runat="server" Culture="en-AU" Width="100px">
                                <DateInput ID="dtPrice" DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy" runat="server" />
                            </telerik:RadDatePicker>
                        </td>
                        <td>
                            <asp:Label ID="lblCurrency" runat="server" Font-Bold="True" Text="Currency:"></asp:Label>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtCurrency" runat="server" Text="AUD" Width="50px">
                            </telerik:RadTextBox>
                        </td>
                        <td style="width: 37%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Import Securities Price List"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:Label ID="lblMessage" CssClass="padl" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <table width="100%">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Select Security Price File</legend>
                            <asp:Label ID="lblFile" runat="server" Font-Bold="True" Text="File:"></asp:Label>
                            <asp:FileUpload Height="23px" ID="filMyFile" runat="server" />
                            <asp:Button ID="cmdSend" runat="server" Width="92px" Text="Upload" OnClick="BtnUploadClick" />
                            <asp:RegularExpressionValidator ID="rexp" runat="server" ControlToValidate="filMyFile"
                                ErrorMessage="File type should be csv (.csv)" ValidationExpression="(.*\.([Cc][Ss][Vv])$)"
                                ForeColor="#FF3300"></asp:RegularExpressionValidator>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel runat="server" ID="PnlResults">
                            <uc:Details ID="ImportMessageControl" runat="server" />
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        Import Price from EOD Data Services:
                        <telerik:RadButton runat="server" ID="btnImportPriceHistoryFromEOD" OnClick="btnImportPriceHistoryFromEODClick"
                            Text="Import Price History From EOD">
                        </telerik:RadButton>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="cmdSend" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
