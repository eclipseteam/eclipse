﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class DISTransactions : UMABasePage
    {
        
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

       

        protected void btnExport_Onclick(object sender, EventArgs e)
        {
            DataSet excelDataset = new DataSet();
            DataView entityView = new DataView(this.PresentationData.Tables[ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE]);
            DataTable entityTable = entityView.ToTable();

            excelDataset.Merge(entityTable, true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, "DIS-ALL TRANS-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", this.Page.Response);
        }

        private void GetData()
        {
            IBrokerManagedComponent orgCM = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            OrganisationListingDS ds = new OrganisationListingDS();
            ds.OrganisationListingOperationType = OrganisationListingOperationType.ClientDistributions;
            orgCM.GetData(ds);
            this.PresentationData = ds;
            UMABroker.ReleaseBrokerManagedComponent(orgCM);
        }

       

        private void PopulateData()
        {
            GetData();
            BindControls(this.PresentationData);
        }

        protected void gvDistribution_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            PopulateData();
        }

        private void BindControls(DataSet ds)
        {
            DataView entityView = new DataView(ds.Tables[ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE]);
            DataTable entityTable = entityView.ToTable();
            gvDistributionTranscation.DataSource = entityTable;
        }

        public override void LoadPage()
        {
            base.LoadPage();

            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
                sm.RegisterPostBackControl(this.btnDownload);
        }

        public override void PopulatePage(DataSet ds)
        {
            if (!this.IsPostBack)
            {  
            }
        }

        protected void gvDistributionTranscation_OnitemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;
            var gDataItem = e.Item as GridDataItem;
            switch (e.CommandName.ToLower())
            {
                case "delete":
                    {
                        string rowID = gDataItem["ID"].Text;
                        string clientID = gDataItem["ClientCID"].Text;
                        DataRow selectedRow = this.PresentationData.Tables[ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE].Select("ID='" + rowID + "'").FirstOrDefault();

                        ClientDistributionsDS ds = new ClientDistributionsDS { CommandType = DatasetCommandTypes.Delete };
                        DataTable tab = new DataTable(ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE);
                        tab.Columns.Add(new DataColumn("ID", typeof(Guid)));
                        ds.Tables.Add(tab);
                        DataRow dr = ds.Tables[ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE].NewRow();
                        dr["ID"] = rowID;
                        ds.Tables[ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE].Rows.Add(dr);

                        this.SaveData(clientID, ds);

                        PopulateData();
                    }
                    break;
            }
        }






    }
}
