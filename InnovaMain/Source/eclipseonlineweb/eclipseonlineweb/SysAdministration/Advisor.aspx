﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="Advisor.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.Advisor" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td style="width: 30%">
                        <telerik:RadButton ID="btnAddAdvisor" runat="server" ToolTip="Add New Adviser" OnClick="btnAddAdvisor_Click">
                            <ContentTemplate>
                                <img src="../images/Add-button-.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">New Adviser</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </td>
                    <td width="90%" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <asp:Label Font-Bold="true" runat="server" ID="lblAdvisor" Text="Advisers"></asp:Label>
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <telerik:RadGrid ID="gd_Advisor" runat="server" AutoGenerateColumns="False" PageSize="20"
        AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true" GridLines="None"
        AllowAutomaticInserts="false" AllowFilteringByColumn="true" EnableViewState="true"
        ShowFooter="false" OnNeedDataSource="gd_Advisor_OnNeedDataSource" OnItemCommand="GdAdvisorItemCommand">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="Top" Width="100%">
            <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false"
                ShowExportToWordButton="false" ShowExportToPdfButton="false"></CommandItemSettings>
            <Columns>
                <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="Cid" HeaderStyle-Width="5%"
                    ItemStyle-Width="5%" HeaderText="CID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    HeaderButtonType="TextButton" DataField="Cid" UniqueName="Cid" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="Clid" HeaderStyle-Width="5%"
                    ItemStyle-Width="5%" HeaderText="CLID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    HeaderButtonType="TextButton" DataField="Clid" UniqueName="Clid" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="Csid" HeaderStyle-Width="5%"
                    ItemStyle-Width="5%" HeaderText="CSID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    HeaderButtonType="TextButton" DataField="Csid" UniqueName="Csid" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="ClientID" HeaderStyle-Width="5%"
                    ItemStyle-Width="5%" HeaderText="Client ID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    HeaderButtonType="TextButton" DataField="ClientID" UniqueName="ClientID">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="400px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                    SortExpression="Name" HeaderText="Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    HeaderButtonType="TextButton" DataField="Name" UniqueName="Name">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="200px" HeaderStyle-Width="8%" ItemStyle-Width="8%"
                    SortExpression="Type" HeaderText="Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    DataField="Type" UniqueName="Type">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="100px" HeaderStyle-Width="5%" ItemStyle-Width="5%"
                    SortExpression="Status" HeaderText="Status" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    HeaderButtonType="TextButton" DataField="Status" UniqueName="Status">
                </telerik:GridBoundColumn>
                <telerik:GridButtonColumn CommandName="Membership" Text="Membership" ButtonType="LinkButton"
                    HeaderStyle-Width="10%" ItemStyle-Width="10%">
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                    UniqueName="EditColumn">
                    <HeaderStyle Width="4%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn Visible="false" ConfirmText="Are you sure you want to delete this adviser?"
                    ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                    <HeaderStyle Width="4%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                </telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
</asp:Content>
