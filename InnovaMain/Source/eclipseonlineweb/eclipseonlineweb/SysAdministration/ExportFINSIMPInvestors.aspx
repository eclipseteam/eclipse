﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="ExportFINSIMPInvestors.aspx.cs" Inherits="eclipseonlineweb.ExportFINSIMPInvestors" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function hintContent() {
            return this.data.label + '<br/> ' + this.y + '';
        }
        function hintContentPie() {
            return this.data.toString() + " : " + Globalize.format(this.value / this.total, "p2");
        }

    </script>
    <asp:Panel ID="Panel2" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="100%" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Export FinSimplicity Investors Data"></asp:Label>
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <asp:Panel runat="server" ID="Panel1" Visible="true" GroupingText="Export Fin Simp Investors">
        <asp:Button UseSubmitBehavior="false" runat="server" ID="btnExport" OnClick="Export"
            Text="Export Fin Simp Investors" />
    </asp:Panel>
</asp:Content>
