﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="PendingOrders.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.PendingOrders" %>

<%@ Register Src="../Controls/PendingOrders.ascx" TagName="PendingOrders" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:PendingOrders ID="PendingOrdersControl" runat="server" />
</asp:Content>
