﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.Commands;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.DataSets;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CM.Organization.Data;

namespace eclipseonlineweb
{
    public partial class BankAccountVsTransactions : UMABasePage
    {
        private DataView _entityView;

        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            GetData();
            var excelDataset = new DataSet();
            DataTable entityTable = _entityView.ToTable();
            entityTable.Columns.Remove("ENTITYCLID_FIELD");
            entityTable.Columns.Remove("ENTITYCSID_FIELD");
            entityTable.Columns.Remove("ENTITYCIID_FIELD");
            entityTable.Columns.Remove("ENTITYCTID_FIELD");
            entityTable.Columns.Remove("ENTITYSCTYPE_FIELD");
            entityTable.Columns.Remove("ENTITYSCSTATUS_FIELD");
            entityTable.Columns.Remove("ENTITYSORT_FIELD");
            entityTable.Columns.Remove("ENTITYECLIPSEID_FIELD");
            entityTable.Columns.Remove("STATUS_FIELD");
            entityTable.Columns.Remove("ENTITYTYPENAME_FIELD");
            entityTable.Columns.Remove("InstitutionID");
            entityTable.Columns.Remove("ClientID");
            entityTable.Columns.Remove("ClientCID");
            entityTable.Columns.Remove("ClientName");
            entityTable.Columns["ENTITYNAME_FIELD"].ColumnName = "AccountName";

            excelDataset.Merge(entityTable, true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, "BankAccount-ALL HOLDING VS TRANS-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", Page.Response);
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        private void GetData()
        {
            IBrokerManagedComponent iBMC = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            OrganisationListingDS ds = new OrganisationListingDS();
            ds.OrganisationListingOperationType = OrganisationListingOperationType.BankAccounts;
            iBMC.GetData(ds);
            PresentationData = ds;
            UMABroker.ReleaseBrokerManagedComponent(iBMC);

            _entityView = new DataView(ds.Tables[BankDetailsDS.BANKDETAILSTABLELIST]);
            _entityView.Sort = "ENTITYNAME_FIELD ASC";
            PresentationGrid.DataSource = _entityView.ToTable();
        }

        public override void LoadPage()
        {
            base.LoadPage();
            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
                sm.RegisterPostBackControl(btnDownload);
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        protected void PresentationGrid_OnItemCreated(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridFilteringItem)) return;

            var filterItem = (GridFilteringItem)e.Item;
            foreach (GridColumn col in PresentationGrid.MasterTableView.Columns)
            {
                var colUniqueName = col.UniqueName;
                var txtbox = (TextBox)filterItem[colUniqueName].Controls[0];
                txtbox.Attributes.Add("onkeydown", "doFilter(this,event,'" + colUniqueName + "')");
            }
        }
    }
}
