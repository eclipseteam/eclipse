﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    EnableEventValidation="true" AutoEventWireup="true" CodeBehind="AccountantCompanyDetail.aspx.cs"
    Inherits="eclipseonlineweb.SysAdministration.AccountantCompanyDetail" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register Src="../Controls/AddressDetailControl.ascx" TagName="AddressControl"
    TagPrefix="uc7" %>
<%@ Register Src="../Controls/ContactMappingControl.ascx" TagName="ContactMappingControl"
    TagPrefix="uc5" %>
<%@ Register Src="../Controls/AccountantCompanyControl.ascx" TagName="AccountantCompanyControl"
    TagPrefix="uc2" %>
<%@ Register Src="../Controls/BankAccountMappingControl.ascx" TagName="BankAccountMappingControl"
    TagPrefix="uc3" %>
    
<%@ Register TagPrefix="uc2" TagName="SecurityConfigurationControl" Src="~/Controls/SecurityConfigurationControl.ascx" %>
<%@ Register TagPrefix="uc5" TagName="OrganizationUnitMappingControl" Src="~/Controls/OrganizationUnitMappingControl.ascx" %>

<%@ Register Src="../Controls/ClientMappingControl.ascx" TagName="ClientMapping"
    TagPrefix="uc8" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                        <telerik:RadButton runat="server" ID="btnPushAccessToClient" OnClick="btnPushAccessToClient_Click" Text="Push Down User Access"></telerik:RadButton>
                        </td>
                        <td width="90%" align="right">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblAccountant" Text="Accountant"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadTabStrip ID="RadTabStrip1" runat="server" Skin="Vista" MultiPageID="RadMultiPage1"
                SelectedIndex="0">
                <Tabs>
                    <telerik:RadTab Text="Accountant Company" runat="server" Selected="True" Value="AccountantCompanyHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Address" runat="server" Visible="true" Value="AddressHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Directors" runat="server" Visible="true" Value="DirectorHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Signatories" runat="server" Visible="true" Value="SignatoryHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Share Holders" runat="server" Visible="true" Value="ShareHolderHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Bank Account" runat="server" Visible="true" Value="BankAccounHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Clients" runat="server" Visible="true" Value="ClientHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Security" runat="server" Visible="true" Value="SecurityHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Documents" runat="server" Visible="true" Value="DocumentsHeader">
                    </telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage runat="server" ID="RadMultiPage1" SelectedIndex="0">
                <telerik:RadPageView runat="server" ID="RadPageView1">
                    <uc2:AccountantCompanyControl ID="AccountantCompanyControl1" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvAddress" runat="server">
                    <fieldset style="width: 98%">
                        <div style="text-align: left; float: none; border: 1px; background-color: White;
                            height: 25px; position: relative; vertical-align: middle; margin: 0; padding: 0;">
                            <telerik:RadButton ID="btnSave" Text="Add Client Addresses" ToolTip="Save Changes"
                                OnClick="btnSave_Click" runat="server" Width="32px" Height="32px" BorderStyle="None">
                                <Image ImageUrl="~/images/Save-Icon.png"></Image>
                            </telerik:RadButton>
                        </div>
                    </fieldset>
                    <br />
                    <uc7:AddressControl ID="AddressControl" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvDirectors" runat="server">
                    <uc5:ContactMappingControl ID="DirectorControl" runat="server" IndividualsType="Directors" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvSignatories" runat="server">
                    <uc5:ContactMappingControl ID="SignatoryControl" runat="server" IndividualsType="Signatories" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvShareHolders" runat="server">
                    <uc5:OrganizationUnitMappingControl ID="ShareHolders" runat="server" UnitPropertyName="ShareHolders"
                        TitleText="Share Holders" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvBankAccounts" runat="server">
                    <uc3:BankAccountMappingControl ID="BankAccountControl" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvClient" runat="server">
                   <uc8:ClientMapping ID="ClientMappingControl" runat="server"  />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvSecurity" runat="server">
                    <uc2:SecurityConfigurationControl ID="SecurityControl" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvDocuments" runat="server">
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
