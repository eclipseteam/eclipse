﻿<%@ Page Title="e-Clipse Online Portal" EnableViewState="true" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="BankAccounts.aspx.cs" Inherits="eclipseonlineweb.BankAccounts" %>

<%@ Register TagPrefix="uc1" TagName="bankaccountcontrol" Src="~/Controls/BankAccountControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .holder
        {
            width: 100%;
            display: block;
            z-index: 6;
        }
        .content
        {
            background: #fff;
            z-index: 7; /*  padding: 28px 26px 33px 25px;*/
        }
        .popup
        {
            border-radius: 7px;
            background: #6b6a63;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 1000px;
            top: 20%;
            left: 50%;
            margin-left: -400px;
            margin-top: -40px;
            z-index: 6;
        }
        
        .popup1
        {
            border-radius: 7px;
            background: #6b6a63;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 770px;
            top: 20%;
            left: 50%;
            margin-left: -400px;
            margin-top: -40px;
            z-index: 6;
        }
        .overlay
        {
            width: 100%;
            opacity: 0.65;
            height: 100%;
            left: 0; /*IE*/
            top: 0;
            text-align: center;
            z-index: 5;
            position: fixed;
            background-color: #444444;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="20%">
                        <telerik:RadButton runat="server" ID="btnDownload" ToolTip="Download" OnClick="btnDownload_Onclick"
                            Width="100PX">
                            <ContentTemplate>
                                <img src="../images/download.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Download</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </td>
                    <td width="100%" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Bank Accounts"></asp:Label>
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <telerik:RadGrid ID="gvBankAccount" runat="server" AutoGenerateColumns="True" PageSize="20"
        OnNeedDataSource="gvBankAccount_OnNeedDataSource" AllowSorting="True" AllowMultiRowSelection="False"
        AllowPaging="true" GridLines="Horizontal" AllowAutomaticUpdates="true" AllowFilteringByColumn="true"
        EnableViewState="true" ShowFooter="True" OnItemCommand="gvBankAccount_OnItemCommand">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="Top" EditMode="EditForms">
            <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false"
                ShowExportToWordButton="false" ShowExportToPdfButton="false"></CommandItemSettings>
            <Columns>
                <telerik:GridBoundColumn SortExpression="ENTITYCIID_FIELD" HeaderText="ENTITYCIID_FIELD"
                    Display="false" HeaderButtonType="TextButton" DataField="ENTITYCIID_FIELD" UniqueName="ENTITYCIID_FIELD">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="InstitutionID" HeaderText="InstitutionID"
                    Visible="false" HeaderButtonType="TextButton" DataField="InstitutionID" UniqueName="InstitutionID">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="50px" SortExpression="BSB" HeaderStyle-Width="8%"
                    HeaderText="BSB" AutoPostBackOnFilter="False" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BSB" UniqueName="BSB">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="9%" SortExpression="CustomerNO"
                    HeaderText="Cust Number" AutoPostBackOnFilter="False" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CustomerNO" UniqueName="CustomerNO">
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="10%" SortExpression="AccountNumber"
                    HeaderText="Account Number" AutoPostBackOnFilter="False" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountNumber"
                    UniqueName="AccountNumber">
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="260px" HeaderStyle-Width="50%"  SortExpression="Name"
                    HeaderText="Account Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Name" UniqueName="Name">
                </telerik:GridBoundColumn>
                <telerik:GridCheckBoxColumn FilterControlWidth="50px" ReadOnly="true" HeaderStyle-Width="5%"
                    SortExpression="IsExternalAccount" HeaderText="External" AutoPostBackOnFilter="False"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="IsExternalAccount" UniqueName="IsExternalAccount">
                </telerik:GridCheckBoxColumn>
                <telerik:GridBoundColumn FilterControlWidth="105px" ReadOnly="true" HeaderStyle-Width="10%"
                    SortExpression="InstitutionName" HeaderText="Institution" AutoPostBackOnFilter="False"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="InstitutionName" UniqueName="InstitutionName">
                </telerik:GridBoundColumn>
                <telerik:GridNumericColumn SortExpression="Holding" HeaderText="Holding ($)" DataType="System.Decimal"
                    FilterControlWidth="90px" DataFormatString="{0:C2}" Aggregate="Sum" HeaderButtonType="TextButton"
                    UniqueName="Holding" DataField="Holding" HeaderStyle-Width="120px" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true">
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                </telerik:GridNumericColumn>
                <telerik:GridButtonColumn CommandName="EditRecords" Text="Edit" ButtonType="LinkButton"
                    HeaderTooltip="Edit" HeaderStyle-Width="4%" ItemStyle-Width="4%">
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn CommandName="Delete" Text="Delete" ButtonType="LinkButton"
                    ConfirmText="Are you sure you want to remove this Bank Account?" HeaderStyle-Width="7%"
                    ItemStyle-Width="7%">
                </telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
        <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
            <Excel Format="Html"></Excel>
        </ExportSettings>
    </telerik:RadGrid>
    <div id="BankModal" runat="server" visible="false" class="holder">
        <div class="popup">
            <div class="content">
                <uc1:bankaccountcontrol ID="BankControl" runat="server" />
            </div>
        </div>
    </div>
    <div class="overlay" id="OVER" visible="False" runat="server">
    </div>
</asp:Content>
