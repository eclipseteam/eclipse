﻿<%@ Page Title="" Language="C#" MasterPageFile="AdminMaster.master" AutoEventWireup="true"
    CodeBehind="ImportMacquarieDataFeed.aspx.cs" Inherits="eclipseonlineweb.ImportMacquarieDataFeed"
    EnableViewState="false" %>

<%@ Register TagPrefix="uc" TagName="Details" Src="WebControls/ImportMessageControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                       <td style="width: 5%">
                            <span class="riLabel">End Date:</span><telerik:RadDatePicker ID="InputEndDate" Width="120px"
                                runat="server">
                            </telerik:RadDatePicker>
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Import Macquarie Bank CMA Data Feed"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <table width="100%">
                <tr>
                    <td>
                        <fieldset>
                        <asp:HiddenField runat="server" ID="txtIsFile"/>
                            <legend>Import Macquarie Bank CMA Data Feed</legend>
                            <asp:Button ID="btnImport" runat="server" Text="Import from Service" OnClick="btnImport_OnClick" />
                            &nbsp;&nbsp;&nbsp;<b>OR</b>&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblFile" runat="server" Text="Upload File:"></asp:Label>
                            <asp:FileUpload Height="23px" ID="filMyFile" runat="server" />
                            <asp:Button ID="cmdSend" runat="server" Width="92px" Text="Upload" OnClick="BtnUploadFileClick" />
                            <asp:RegularExpressionValidator ID="rexp" runat="server" ControlToValidate="filMyFile"
                                ErrorMessage="File type should be CSV (.csv)" ValidationExpression="(.*\.([Cc][Ss][Vv])$)"
                                ForeColor="#FF3300"></asp:RegularExpressionValidator>
                        </fieldset>
                    </td>
                </tr>
          
           <tr> <td><fieldset>
                <legend>Result</legend>
                <table width="100%">
                    <tr>
                        <td>
                            Code:
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblMessageCode"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Summary:
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblMessageSummary"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Details:
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblMessageDetails"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset></td></tr>
              <tr>
                    <td>
                        <asp:Panel runat="server" ID="PnlResults">
                            <uc:Details ID="ImportMessageControl" runat="server" />
                        </asp:Panel>
                    </td>
                </tr>
             </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger runat="server" ControlID="cmdSend" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
