﻿using System;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using Oritax.TaxSimp.CM.OrganizationUnit;

namespace eclipseonlineweb.SysAdministration
{
    public partial class AccountantSoleTraderDetail : UMABasePage
    {
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cid))
            {
                var ds = AddressControl.SetData();
                SaveData(cid, ds);
                AddressControl.FillAddressControls(ds);
            }
        }

        protected override void Intialise()
        {
            base.Intialise();
            accountantIndividualControl1.SaveData += (cid, ds) =>
            {
                if (cid == Guid.Empty)
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(cid.ToString(), ds);
                }
            };
            accountantIndividualControl1.Saved += (cID) => Response.Redirect(Request.Url.AbsoluteUri + "?ins=" + cID);
            BankAccountControl.SaveOrg += SaveOrganizanition;
            BankAccountControl.SaveUnit += SaveData;
            SoleTraderControl.SaveOrg += SaveOrganizanition;
            SoleTraderControl.SaveUnit += SaveData;
            ClientMappingClient.SaveOrg += SaveOrganizanition;
            ClientMappingClient.SaveUnit += SaveData;
            ClientMappingClient.OrgType = OrganizationType.Accountant;
            
        }

        private void GetData()
        {
            var clientData = UMABroker.GetBMCInstance(new Guid(cid)) as OrganizationUnitCM;
            if (clientData != null)
            {
                var ds = new AddressDetailsDS();
                clientData.GetData(ds);
                PresentationData = ds;
                UMABroker.ReleaseBrokerManagedComponent(clientData);
                AddressControl.FillAddressControls(ds);
            }
        }

        public override void LoadPage()
        {
            cid = string.IsNullOrEmpty(Request.Params["ins"]) ? string.Empty : Request.QueryString["ins"];
            if (!IsPostBack)
            {
                accountantIndividualControl1.SetEntity(
                    string.IsNullOrEmpty(Request.Params["ins"]) ? Guid.Empty : new Guid(Request.QueryString["ins"]),
                    Oritax.TaxSimp.CM.Group.AccountantEntityType.AccountantIndividualControl);
                if (string.IsNullOrEmpty(cid))
                {
                    SetVisibility(false);
                }
                else
                {
                    SetVisibility(true);
                    GetData();
                }
            }
        }
        private void SetVisibility(bool visible)
        {
            rpvAddress.Visible = rpvSoleTrader.Visible = rpvBankAccount.Visible = rpvClient.Visible = rpvSecurity.Visible = rpvDocuments.Visible == visible;

            RadTab tabAddressHeader = RadTabStrip1.FindTabByValue("AddressHeader");
            tabAddressHeader.Visible = visible;
            RadTab tabSoleTraderHeader = RadTabStrip1.FindTabByValue("SoleTraderHeader");
            tabSoleTraderHeader.Visible = visible;
            RadTab tabBankAccountHeader = RadTabStrip1.FindTabByValue("BankAccountHeader");
            tabBankAccountHeader.Visible = visible;
            RadTab tabClientHeader = RadTabStrip1.FindTabByValue("ClientHeader");
            tabClientHeader.Visible = visible;
            RadTab tabSecHeader = RadTabStrip1.FindTabByValue("SecurityHeader");
            tabSecHeader.Visible = visible;
            RadTab tabDocumentsHeader = RadTabStrip1.FindTabByValue("DocumentsHeader");
            tabDocumentsHeader.Visible = visible;
        }
    }
}