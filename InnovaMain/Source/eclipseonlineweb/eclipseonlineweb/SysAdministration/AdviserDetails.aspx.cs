﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Telerik.Web.UI;

namespace eclipseonlineweb.SysAdministration
{
    public partial class AdviserDetails : UMABasePage
    {
        protected override void Intialise()
        {
            base.Intialise();
            AdviserInfo.SaveData += (cID, ds) =>
            {
                if (cID == Guid.Empty.ToString())
                {
                    SaveOrganizanition(ds);

                }
                else
                {
                    SaveData(cID, ds);
                }
                if (ds.ExtendedProperties["Message"] != null)
                    ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "AlertMsgAdd", "alert('" + ds.ExtendedProperties["Message"] + "');", true);
            };
            AdviserInfo.Saved += cID => Response.Redirect(Request.Url.AbsoluteUri + "?ins=" + cID);

            BankAccountMappingControl.SaveOrg += SaveOrganizanition;
            BankAccountMappingControl.SaveUnit += SaveData;
            IndividualMappingControl.SaveOrg += SaveOrganizanition;
            IndividualMappingControl.SaveUnit += SaveData;
        }
        private void GetData()
        {
            if (cid != null)
            {
                var clientData = UMABroker.GetBMCInstance(new Guid(cid)) as OrganizationUnitCM;
                if (clientData != null)
                {
                    var ds = new AddressDetailsDS();
                    clientData.GetData(ds);
                    PresentationData = ds;
                    UMABroker.ReleaseBrokerManagedComponent(clientData);
                    AddressDetailControl.FillAddressControls(ds);
                    HideTab(true);
                }
            }
            else
            {
                HideTab(false);
            }

        }
        protected void BtnSaveClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cid))
            {
                AddressDetailsDS ds = AddressDetailControl.SetData();
                SaveData(cid, ds);
                AddressDetailControl.FillAddressControls(ds);
            }
        }
        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];
            base.LoadPage();
            if (!IsPostBack)
            {
                GetData();
                if (!string.IsNullOrEmpty(Request.Params["ins"]))
                {
                    foreach (var tab in RadTabStrip1.GetAllTabs())
                    {
                        if (!tab.Visible)
                            tab.Visible = true;
                    }
                }
            }
        }
        /// <summary>
        /// This function is used to hide the tab which is not required
        /// </summary>
        /// <param name="flage"></param>
        private void HideTab (bool flage)
        {
            RadTab tabAddress = RadTabStrip1.FindTabByValue("TabAddress");
            tabAddress.Visible = flage;
            RadTab tabBankAccounts = RadTabStrip1.FindTabByValue("TabBankAccounts");
            tabBankAccounts.Visible = flage;
            RadTab tabSecurity = RadTabStrip1.FindTabByValue("TabSecurity");
            tabSecurity.Visible = flage;
        }

    }
}