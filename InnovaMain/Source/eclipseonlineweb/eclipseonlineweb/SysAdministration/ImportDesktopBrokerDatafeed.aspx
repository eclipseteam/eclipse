﻿<%@ Page Title="" Language="C#" MasterPageFile="AdminMaster.master" AutoEventWireup="true"
    CodeBehind="ImportDesktopBrokerDataFeed.aspx.cs" Inherits="eclipseonlineweb.ImportDesktopBrokerDataFeed"
    EnableViewState="false" %>

<%@ Register TagPrefix="uc" TagName="Details" Src="WebControls/ImportMessageControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Import Desktop Broker Data Feed"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br/>
            <table width="100%">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Import Desktop Broker Data Feed</legend>
                            <asp:Button ID="btnImport" runat="server" Text="Import from Service (Inception)" OnClick="btnImport_OnClick" />&nbsp;&nbsp;&nbsp;<b>OR</b>&nbsp;&nbsp;&nbsp;
                             <asp:Button ID="Button1" runat="server" Text="Import from Service (Last 5 Days)" OnClick="btnImportFiveDays_OnClick" />&nbsp;&nbsp;&nbsp;<b>OR</b>&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblFile" runat="server" Text="Upload File:"></asp:Label>
                            <asp:FileUpload Height="23px" ID="filMyFile" runat="server" />
                            <asp:Button ID="cmdSend" runat="server" Width="92px" Text="Upload" OnClick="BtnUploadClick" />
                            <asp:RegularExpressionValidator ID="rexp" runat="server" ControlToValidate="filMyFile"
                                ErrorMessage="File type should be Xml (.xml)" ValidationExpression="(.*\.([Xx][Mm][Ll])$)"
                                ForeColor="#FF3300"></asp:RegularExpressionValidator>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel runat="server" ID="PnlResults">
                            <uc:Details ID="ImportMessageControl" runat="server" />
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger runat="server" ControlID="cmdSend" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
