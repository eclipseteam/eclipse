﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="SystemConfigurations.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.SystemConfigurations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>System Configurations</legend>
        <fieldset>
            <legend>System Settings</legend>
            <telerik:RadButton ID="RadButton3" runat="server" Text="Save" OnClick="BtnSaveOrganizationalSettings"
                ToolTip="SaveSettings">
            </telerik:RadButton>
            <br />
            <br />
            <telerik:RadButton ID="chkSendToSMA" runat="server" Text="Use service for Super Client order sending"
                AutoPostBack="false" ToggleType="CheckBox" ButtonType="ToggleButton">
            </telerik:RadButton>
            <br />
            <telerik:RadButton ID="chkReadFromSMA" runat="server" Text="Use service for Super Client data refresh"
                AutoPostBack="false" ToggleType="CheckBox" ButtonType="ToggleButton">
            </telerik:RadButton>
        </fieldset>
        <br />
        <fieldset>
            <legend>OBP</legend>
            <telerik:RadButton ID="BtnCreateOBP" runat="server" Text="Create OBP Singlton" OnClick="BtnCreateObpObject"
                ToolTip="Create OBP Singleton Object in the system">
            </telerik:RadButton>
            <br />
            <telerik:RadButton ID="RadButton1" runat="server" Text="Create Security Singlton"
                OnClick="BtnCreateSecuritiyObject" ToolTip="Create Security Singleton Object in the system">
            </telerik:RadButton>
            <br />
            <telerik:RadButton ID="RadButton2" runat="server" Text="Create License Singlton"
                OnClick="BtnCreateLiecenceObject" ToolTip="Create License Singleton Object in the system">
            </telerik:RadButton>
        </fieldset>
        <br />
        <fieldset>
            <legend>CM Until</legend><a href="OrderPadUtil.aspx">Go To Utility Page </a>
            <br />
            <a href="../ViewErrorLog.aspx">View Error Log </a>
        </fieldset>
    </fieldset>
    <fieldset>
        <h1>
            <asp:Label runat="server" ID="lblMessage"></asp:Label></h1>
    </fieldset>
</asp:Content>
