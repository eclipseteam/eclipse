﻿using System;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;

namespace eclipseonlineweb.SysAdministration
{
    public partial class AccountantPartnershipDetail : UMABasePage
    {
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cid))
            {
                AddressDetailsDS ds = AddressDetailControl.SetData();
                SaveData(cid, ds);
                AddressDetailControl.FillAddressControls(ds);
            }
        }

        protected override void Intialise()
        {
            base.Intialise();
            AccountantPartnershipControl1.SaveData += (Cid, ds) =>
            {
                if (Cid == Guid.Empty)
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(Cid.ToString(), ds);
                }
            };
            AccountantPartnershipControl1.Saved += (cID) => Response.Redirect(Request.Url.AbsoluteUri + "?ins=" + cID);
            BankAccountMappingControl.SaveOrg += SaveOrganizanition;
            BankAccountMappingControl.SaveUnit += SaveData;
            Partnerships.SaveOrg += SaveOrganizanition;
            Partnerships.SaveUnit += SaveData;
        }

        private void GetData()
        {
            var clientData = UMABroker.GetBMCInstance(new Guid(cid)) as OrganizationUnitCM;
            if (clientData != null)
            {
                var ds = new AddressDetailsDS();
                clientData.GetData(ds);
                PresentationData = ds;
                UMABroker.ReleaseBrokerManagedComponent(clientData);
                AddressDetailControl.FillAddressControls(ds);
            }
        }
        public override void LoadPage()
        {
            cid = string.IsNullOrEmpty(Request.Params["ins"]) ? string.Empty : Request.QueryString["ins"];
            if (!IsPostBack)
            {
                AccountantPartnershipControl1.SetEntity(
                    string.IsNullOrEmpty(Request.Params["ins"]) ? Guid.Empty : new Guid(Request.QueryString["ins"]),
                    Oritax.TaxSimp.CM.Group.AccountantEntityType.AccountantPartnershipControl);
                if (string.IsNullOrEmpty(cid))
                {
                    SetVisibility(false);
                }
                else
                {
                    SetVisibility(true);
                    GetData();
                }
            }
        }

        private void SetVisibility(bool visible)
        {
            ApAddress.Visible = ApBankAccounts.Visible = ApPartnerships.Visible = ApSecurity.Visible = APDocuments.Visible == visible;
            RadTab tabAddressHeader = RadTabStrip1.FindTabByValue("AddressHeader");
            tabAddressHeader.Visible = visible;
            RadTab tabPartnershipsHeader = RadTabStrip1.FindTabByValue("PartnershipsHeader");
            tabPartnershipsHeader.Visible = visible;
            RadTab tabBankAccountsHeader = RadTabStrip1.FindTabByValue("BankAccountsHeader");
            tabBankAccountsHeader.Visible = visible;
            RadTab tabSecHeader = RadTabStrip1.FindTabByValue("SecurityHeader");
            tabSecHeader.Visible = visible;
            RadTab tabDocumentsHeader = RadTabStrip1.FindTabByValue("DocumentsHeader");
            tabDocumentsHeader.Visible = visible;
        }
    }
}