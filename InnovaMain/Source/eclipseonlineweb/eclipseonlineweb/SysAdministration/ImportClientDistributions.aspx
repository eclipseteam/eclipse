﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="ImportClientDistributions.aspx.cs" Inherits="eclipseonlineweb.ImportClientDistributions" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function hintContent() {
            return this.data.label + '<br/> ' + this.y + '';
        }
        function hintContentPie() {
            return this.data.toString() + " : " + Globalize.format(this.value / this.total, "p2");
        }

    </script>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                                ID="btnDownload" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Import Client Distribution"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <asp:Panel runat="server" ID="Panel1" Visible="true" GroupingText="Import Client Distribution File">
                <asp:FileUpload Height="23px" ID="FileUpload1" runat="server" />&nbsp;<asp:Button
                    ID="btnUpload" runat="server" Text="Upload" Width="92px" OnClick="btnUpload_Click" />
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlMainGrid" Visible="true">
                <c1:C1GridView ShowFilter="true" AllowColMoving="true" AllowColSizing="true" RowStyle-Font-Size="Smaller"
                    AllowSorting="true" FilterStyle-Font-Size="Smaller" HeaderStyle-Font-Size="Smaller"
                    EnableTheming="true" OnFiltering="Filter" OnSorting="SecListSorting" Width="100%"
                    ID="SecList" CallbackSettings-Action="None" OnPageIndexChanging="Sec_PageIndexChanging"
                    PagerSettings-Mode="Numeric" runat="server" AutogenerateColumns="true" AllowPaging="false"
                    ClientSelectionMode="None" CallbackSettings-Mode="Full" ScrollMode="Both">
                    <SelectedRowStyle />
                    <Columns>
                    </Columns>
                </c1:C1GridView>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
