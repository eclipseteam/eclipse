﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.Common.UMAFee;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb
{
    public partial class UnlinkedAccounts : UMABasePage
    {

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void GetBMCData()
        {

            UnlinkedAccountsDS unlinkedAccounts = new UnlinkedAccountsDS { CommandType = DatasetCommandTypes.Get, Command = (int)WebCommands.GetUnlinkedAccount };
            unlinkedAccounts.Unit = new OrganizationUnit { CurrentUser = (Page as UMABasePage).GetCurrentUser(), Type = ((int)OrganizationType.DesktopBrokerAccount).ToString() };
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            organization.GetData(unlinkedAccounts);
            UMABroker.ReleaseBrokerManagedComponent(organization);
            this.PresentationData = unlinkedAccounts;
        }


        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
        }

        public override void LoadPage()
        {
            base.LoadPage();

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
                ((SetupMaster)Master).IsAdmin = "1";
        }

        protected void BankAccounts_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var unlinkedAccountsDS = PresentationData as UnlinkedAccountsDS;
            if (unlinkedAccountsDS != null)
                BankAccounts.DataSource = unlinkedAccountsDS.bankAccounts;
        }

        protected void ASXAccounts_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            ASXAccounts.DataSource = ((UnlinkedAccountsDS)PresentationData).desktopBrokerAccounts;
        }


        protected void MISAccounts_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            MISAccounts.DataSource = ((UnlinkedAccountsDS)PresentationData).fundAccountTable;
        }

        protected void AccountGrid_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            var gird = (RadGrid)sender;
            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName ||
                e.CommandName == Telerik.Web.UI.RadGrid.ExportToWordCommandName ||
                e.CommandName == Telerik.Web.UI.RadGrid.ExportToCsvCommandName)
            {
                ConfigureExport(gird, "");
            }


            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;

           

            switch (e.CommandName.ToLower())
            {
                case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    if (((WebControl)sender).ID.Contains("Bank") || ((WebControl)sender).ID.Contains("ASX"))// allow only bank account and ASX account deletation
                    {
                        var linkEntiyCID = new Guid(dataItem["CID"].Text);
                        DeleteInstance(linkEntiyCID);
                        gird.Rebind();
                    }
                    break;
            }

        }

        private void ConfigureExport(RadGrid grid, string text)
        {
            grid.ExportSettings.ExportOnlyData = true;
            grid.ExportSettings.IgnorePaging = true;
            grid.ExportSettings.OpenInNewWindow = true;
            grid.ExportSettings.FileName = text;
        }
        protected void ButtonExcel_Click(object sender, System.EventArgs e)
        {
            RadGrid grid;
            string text = "";
            if (((WebControl)sender).ID.Contains("Bank"))
            {
                grid = BankAccounts;
                text = "Unlinked Bank Accounts";
            }
            else if (((WebControl)sender).ID.Contains("ASX"))
            {
                grid = ASXAccounts;
                text = "Unlinked ASX Accounts";
            }
            else
            {
                grid = MISAccounts;
                text = "Unlinked MIS Accounts";
            }
            ConfigureExport(grid, text);
            grid.MasterTableView.ExportToExcel();
        }

        protected void ButtonWord_Click(object sender, System.EventArgs e)
        {

            RadGrid grid;
            string text = "";
            if (((Button)sender).ID.Contains("Bank"))
                grid = BankAccounts;
            else if (((Button)sender).ID.Contains("ASX"))
                grid = ASXAccounts;
            else
                grid = MISAccounts;
            ConfigureExport(grid, text);
            grid.MasterTableView.ExportToWord();
        }


    }
}
