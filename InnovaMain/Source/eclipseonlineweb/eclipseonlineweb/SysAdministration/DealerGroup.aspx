﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="DealerGroup.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.DealerGroup" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .holder
        {
            width: 100%;
            display: block;
            z-index: 6;
        }
        .content
        {
            background: #fff;
            z-index: 7; /*  padding: 28px 26px 33px 25px;*/
        }
        .popup
        {
            border-radius: 7px;
            background: white;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 185px;
            top: 20%;
            left: 50%;
            margin-left: -400px;
            margin-top: -40px;
            z-index: 6;
        }
        
        .overlay
        {
            width: 100%;
            opacity: 0.65;
            height: 100%;
            left: 0; /*IE*/
            top: 0;
            text-align: center;
            z-index: 5;
            position: fixed;
            background-color: #444444;
        }
    </style>
    <fieldset>
        <table width="100%">
            <tr>
                <td width="30%">
                    <telerik:RadButton runat="server" ID="btnAddDealerGroup" ToolTip="Add New Dealer Group"
                        OnClick="btnAddDealerGroup_Click">
                        <ContentTemplate>
                            <img src="../images/Add-button-.png" alt="" class="btnImageWithText" />
                            <span class="riLabel">New Dealer Group</span>
                        </ContentTemplate>
                    </telerik:RadButton>
                </td>
                <td width="70%" align="right">
                    <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                    <br />
                    <span class="riLabel">Dealer Group</span>
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <telerik:RadGrid ID="gd_DealerGroup" runat="server" AutoGenerateColumns="True" PageSize="20"
        AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true" GridLines="Horizontal"
        AllowAutomaticInserts="false" OnInsertCommand="gd_DealerGroup_InsertCommand"
        AllowAutomaticDeletes="True" AllowFilteringByColumn="true" EnableViewState="true"
        ShowFooter="True" OnNeedDataSource="gd_DealerGroup_OnNeedDataSource" OnItemCommand="gd_DealerGroup_ItemCommand">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="Top">
            <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false"
                AddNewRecordText="Add New Dealer Group" ShowExportToWordButton="false" ShowExportToPdfButton="false">
            </CommandItemSettings>
            <Columns>
                <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="Cid" HeaderStyle-Width="5%"
                    ItemStyle-Width="5%" HeaderText="CID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Cid" UniqueName="Cid"
                    Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="Clid" HeaderStyle-Width="5%"
                    ItemStyle-Width="5%" HeaderText="CLID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Clid" UniqueName="Clid"
                    Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="Csid" HeaderStyle-Width="5%"
                    ItemStyle-Width="5%" HeaderText="CSID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Csid" UniqueName="Csid"
                    Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="DealerGroupType"
                    HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderText="DealerGroupType" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="DealerGroupType" UniqueName="DealerGroupType" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="ClientID" HeaderStyle-Width="5%"
                    ItemStyle-Width="5%" HeaderText="Client ID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ClientID" UniqueName="ClientID">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="400px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                    SortExpression="TradingName" HeaderText="Trading Name" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="TradingName" UniqueName="TradingName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="200px" HeaderStyle-Width="8%" ItemStyle-Width="8%"
                    SortExpression="Type" HeaderText="Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="false" DataField="Type" UniqueName="Type">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="100px" HeaderStyle-Width="5%" ItemStyle-Width="5%"
                    SortExpression="Status" HeaderText="Status" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Status" UniqueName="Status">
                </telerik:GridBoundColumn>
                <telerik:GridButtonColumn CommandName="Membership" Text="Membership" ButtonType="LinkButton"
                    HeaderStyle-Width="10%" ItemStyle-Width="10%">
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn CommandName="Edit" Text="Detail" ButtonType="LinkButton"
                    HeaderStyle-Width="10%" ItemStyle-Width="10%">
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn ConfirmText=" Do you really want to delete this dealer group from the system. You would not be able to undo this? Yes/No"
                    ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                    <HeaderStyle Width="3%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                </telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <div id="OrganizationTypePopup" runat="server" visible="false" class="holder">
        <div class="popup">
            <table>
                <tr>
                    <td>
                        <asp:Panel ID="ViewPanel" runat="server" BackColor="DarkBlue">
                            <table width="100%">
                                <tr>
                                    <td style="text-align: center; color: White">
                                        Select Dealer Group
                                    </td>
                                    <td style="text-align: right;">
                                        <telerik:RadButton ID="btnCanl" runat="server" OnClick="btnCancel_OnClick" Width="14px"
                                            Height="15px">
                                            <Image ImageUrl="~/images/cross_icon_normal.png"></Image>
                                        </telerik:RadButton>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <telerik:RadButton runat="server" ID="rdoDealerSoleTrader" ToggleType="Radio" ButtonType="ToggleButton"
                            GroupName="Dealer" Text="Dealer Group - Sole Trader" Checked="true">
                        </telerik:RadButton>
                        <br />
                        <telerik:RadButton runat="server" ID="rdoDealerCompany" ToggleType="Radio" ButtonType="ToggleButton"
                            GroupName="Dealer" Text="Dealer Group - Company">
                        </telerik:RadButton>
                        <br />
                        <telerik:RadButton runat="server" ID="rdoDealerPartnership" ToggleType="Radio" ButtonType="ToggleButton"
                            GroupName="Dealer" Text="Dealer Group - Partnership">
                        </telerik:RadButton>
                        <br />
                        <telerik:RadButton runat="server" ID="rdoDealerTrust" ToggleType="Radio" ButtonType="ToggleButton"
                            GroupName="Dealer" Text="Dealer Group - Trust">
                        </telerik:RadButton>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        <telerik:RadButton ID="btnNext" runat="server" Text="Next" OnClick="btnNext_OnClick">
                        </telerik:RadButton>
                        <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_OnClick">
                        </telerik:RadButton>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="overlay" id="OVER" visible="False" runat="server">
    </div>
</asp:Content>
