﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="OrderHistory.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.OrderHistory" %>

<%@ Register Src="../Controls/OrderHistory.ascx" TagName="OrderHistory" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:OrderHistory ID="OrderHistoryControl" runat="server" />
</asp:Content>
