﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using eclipseonlineweb.WebControls;
using System.Drawing;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM;

namespace eclipseonlineweb
{
    public partial class ImportSecuritiesPriceList : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void Intialise()
        {
            ImportMessageControl.GridImportMessages.ColumnCreated += GridImportMessages_ColumnCreated;
            ImportMessageControl.GirdInvestmentCodeError.ColumnCreated += GridImportErrorMessages_ColumnCreated;
            ImportMessageControl.GridImportMessages.ItemDataBound += new GridItemEventHandler(GridImportMessages_ItemDataBound);
            if (!IsPostBack)
            {
                ImportMessageControl.Clear();
            }

            ImportMessageControl.Completed += text =>
                                                  {
                                                      switch (text.ToLower())
                                                      {
                                                          case "next":
                                                              SendData();
                                                              break;
                                                          default:
                                                              ClearMessageControls();
                                                              break;
                                                      }
                                                  };

           
        }

        void GridImportMessages_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item).ItemIndex != -1)
            {
                DataRow dr = ((DataRowView)((e.Item).DataItem)).Row;
                if (bool.Parse(dr["HasErrors"].ToString()))
                {
                    e.Item.ForeColor = Color.Red;
                }
            }

        }
        void GridImportMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {

           
            SetDataGridColumns(e);
        }

        void GridImportErrorMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
           
            SetDataGridColumns(e);
        }

        public void btnImportPriceHistoryFromEODClick(object sender, EventArgs e)
        {
            ImportProcessDS ds = new ImportProcessDS();
            ds.Tables.Clear();

            DataTable dt = new DataTable("ResultTable");
            dt.Columns.Add("ASX Code");
            dt.Columns.Add("Company Name");
            dt.Columns.Add("Status");
            ds.Tables.Add(dt);

            ds.MessageNumber = "ASX History 01";
            ds.MessageSummary = "ASX History Load";
            ds.MessageDetail = "Import 3 years history for securities from EOD Data Services";


            IOrganization orgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            EODWebserviceUtilities eodWebserviceUtilities = new EODWebserviceUtilities();
            eodWebserviceUtilities.Login();
            foreach (var securityEntity in orgCM.Securities)
                eodWebserviceUtilities.GetPriceHistory(ds, "ASX", securityEntity.AsxCode, "", securityEntity);
            this.SaveData(orgCM as IBrokerManagedComponent); 
        }   

        public void BtnUploadClick(object sender, EventArgs e)
        {
            lblMessage.Text = "";
            if (cmbSelectType.SelectedValue == "ASXPrice")
            {
                bool haserrors = false;
                if (dtPicker.SelectedDate == null)
                {
                    lblMessage.Text = "Please select date";
                    haserrors = true;
                }                
                if (txtCurrency.Text == "")
                {
                    lblMessage.Text += "<br>Please enter currency";
                    haserrors = true;
                }                
                if (haserrors)
                    return;

            }

            try
            {
                if (filMyFile.PostedFile != null)
                {
                    // Get a reference to PostedFile object
                    HttpPostedFile myFile = filMyFile.PostedFile;

                    // Get size of uploaded file
                    int nFileLen = myFile.ContentLength;

                    // make sure the size of the file is > 0
                    if (nFileLen > 0)
                    {
                        // Allocate a buffer for reading of the file
                        string fileName = Path.GetFileName(myFile.FileName);
                        var date = DateTime.Now;
                        var DistinationDirectory = Server.MapPath("~/App_Data/Import/");
                        if (!Directory.Exists(DistinationDirectory))
                        {
                            Directory.CreateDirectory(DistinationDirectory);
                        }

                        string fileLocation = DistinationDirectory + date.ToString("ddMMyyhhmmsstt") + fileName;
                        byte[] myData = new byte[nFileLen];

                        // Read uploaded file from the Stream
                        myFile.InputStream.Read(myData, 0, nFileLen);

                        // Write data into a file
                        Utilities.WriteToFile(fileLocation, ref myData);
                        string csv = string.Empty;
                        using (StreamReader reader = new StreamReader(fileLocation, true))
                        {
                            csv = reader.ReadToEnd();
                            reader.Close();
                        }
                        string ASXPrice = ConversorTransactionCsv.SelectRows(csv, "");
                        DataTable dt = new DataTable();
                        ImportProcessDS ds = new ImportProcessDS();
                        
                        switch (cmbSelectType.SelectedValue)
                        {
                            case "ASXPrice":
                                dt = ConversorTransactionCsv.CreateASXPriceList(ASXPrice, dtPicker.SelectedDate.Value.ToString("dd/MM/yyyy"), txtCurrency.Text.Trim());
                                ds.Command = (int)WebCommands.ValidateASXPriceImportFile;
                                break;
                            case "StateStreetPrice":
                                dt = ConversorTransactionCsv.CreateStateStreetPriceList(ASXPrice);
                                ds.Command = (int)WebCommands.ValidateStateStreetPriceImportFile;
                                break;
                        }
                        ds.Tables.Add(dt);
                        ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
                        ds.FileName = fileName;
                        SaveOrganizanition(ds);
                        SetValidationGird(ds);
                        PresentationData = ds;
                        string filename = ImportMessageControl.RefreshFileID() + ".xml";
                        ds.WriteXml(DistinationDirectory + filename, XmlWriteMode.WriteSchema);
                    }
                }
                VisibleControl();
            }
            catch 
            {
                lblMessage.Text = ImportMessages.sFileTypeMismatch;
            }
            // Check to see if file was uploaded
           
        }

        private void SetValidationGird(ImportProcessDS ds)
        {
            //ImportMessageControl.Clear();
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                DataRow dr;
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    dr = missingItems.NewRow();
                    if (cmbSelectType.SelectedValue == "ASXPrice")
                    dr["MissingItem"] = row["Code"];
                    else
                        dr["MissingItem"] = row["FundCode"];
                    missingItems.Rows.Add(dr);
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    if (cmbSelectType.SelectedValue == "ASXPrice")
                        ImportMessageControl.TextMessage.Text = ImportMessages.sMissingCodeAdded;
                    else
                        ImportMessageControl.TextMessage.Text = ImportMessages.sMissingCodeIgnored;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }

            int successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
            if (ds.Tables[0].Rows.Count == 0 || successfulRows == 0)
            {
                ImportMessageControl.SetButtons(ButtonTypes.CLOSE);
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sErrorsCannotContinue, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Error);
                ImportMessageControl.TextMessage.Text = "";
            }
            else
            {
                ImportMessageControl.SetButtons(ButtonTypes.NEXTCANCEL);
                //sucess
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sValidationCompleted, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
            }
        }

        private void SetCompletionGird(ImportProcessDS ds)
        {
            ImportMessageControl.Clear();
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }

            using (DataView dv = new DataView(ds.Tables[0]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                DataRow dr;
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    dr = missingItems.NewRow();
                    if (cmbSelectType.SelectedValue == "ASXPrice")
                        dr["MissingItem"] = row["Code"];
                    else
                        dr["MissingItem"] = row["FundCode"];
                    missingItems.Rows.Add(dr);
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    if (cmbSelectType.SelectedValue == "ASXPrice")
                        ImportMessageControl.TextMessage.Text = ImportMessages.sMissingInvestmentCodeAdded;
                    else
                        ImportMessageControl.TextMessage.Text = ImportMessages.sMissingInvestmentCodeIgnored;

                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }

            int successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
            ImportMessageControl.SetButtons(ButtonTypes.FINISH);
            ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sImportedSuccessfully, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
        }

        private void SendData()
        {
            var distributionDictory = Server.MapPath("~/App_Data/Import/");
            if (!Directory.Exists(distributionDictory))
            {
                Directory.CreateDirectory(distributionDictory);
            }
            string filename = distributionDictory + ImportMessageControl.GetFileID() + ".xml";
            ImportProcessDS ds = new ImportProcessDS();
            ds.ReadXml(filename);
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            switch (cmbSelectType.SelectedValue)
            {
                case "ASXPrice":
                    ds.Command = (int)WebCommands.ImportASXPriceImportFile;
                    break;
                case "StateStreetPrice":
                    ds.Command = (int)WebCommands.ImportStateStreetImportFile;
                    break;
            }            
            ds.FileName = filename;
            SaveOrganizanition(ds);
            SetCompletionGird(ds);
        }

        public void ClearMessageControls()
        {
            ImportMessageControl.Clear();
            ImportMessageControl.TextMessage.Text = "";
        }

        private  void SetDataGridColumns(GridColumnCreatedEventArgs e)
        {
            string colName = e.Column.UniqueName.ToLower();
            if (cmbSelectType.SelectedValue == "ASXPrice")
            {
                switch (colName)
                {
                    case "code":
                        e.Column.HeaderText = "ASX Code";
                        break;
                    case "companycame":
                        e.Column.HeaderText = "Company Name";
                        e.Column.FilterControlWidth = Unit.Pixel(50);
                        break;
                    case "last":
                        e.Column.HeaderText = "Last";
                        e.Column.FilterControlWidth = Unit.Pixel(50);
                        break;
                    case "add_sub":
                        e.Column.HeaderText = "Add/Sub";
                        e.Column.FilterControlWidth = Unit.Pixel(170);
                        break;
                    case "percent":
                        e.Column.HeaderText = "Percent";
                        break;
                    case "open":
                        e.Column.HeaderText = "Open";
                        break;

                    case "high":
                        e.Column.HeaderText = "High";
                        break;
                    case "low":
                        e.Column.HeaderText = "Low";
                        break;
                    case "trades":
                        e.Column.HeaderText = "Trades";
                        break;

                    case "volume":
                        e.Column.HeaderText = "Volume";
                        break;
                    case "buyprice":
                        e.Column.HeaderText = "Buy Price";
                        break;
                    case "sellprice":
                        e.Column.HeaderText = "Sell Price";
                        break;

                    case "rating":
                        e.Column.HeaderText = "Rating";
                        break;
                    case "pricedate":
                        e.Column.HeaderText = "Price Date";
                        break;
                    case "currency":
                        e.Column.HeaderText = "Currency";
                        break;                 
                    case "message":
                        e.Column.HeaderText = "Message";
                        break;
                    case "haserrors":
                        e.Column.HeaderText = "Has Errors";
                        e.Column.Visible = false;
                        break;
                    case "ismissingitem":
                        e.Column.HeaderText = "Is Missing Item";
                        e.Column.Visible = false;
                        break;
                    case "missingitem":
                        e.Column.HeaderText = "Missing Investment Code(s)";
                        break;
                }  
            }
            else
            {
                switch (colName)
                {
                    case "fundcode":
                        e.Column.HeaderText = "Fund Code";
                        break;
                    case "fundname":
                        e.Column.HeaderText = "Company Name";
                        e.Column.FilterControlWidth = Unit.Pixel(50);
                        break;
                    case "date":
                        e.Column.HeaderText = "Date";
                        e.Column.FilterControlWidth = Unit.Pixel(50);
                        break;
                    case "price(nav)":
                        e.Column.HeaderText = "Price(NAV)";
                        e.Column.FilterControlWidth = Unit.Pixel(170);
                        break;
                    case "price(pur)":
                        e.Column.HeaderText = "Price(PUR)";
                        break;
                    case "price(rdm)":
                        e.Column.HeaderText = "Price(RDM)";
                        break;

                    case "currency":
                        e.Column.HeaderText = "Currency";
                        break;
                    case "reinvestmentprice":
                        e.Column.HeaderText = "Reinvestment Price";
                        break;
                    case "pricedate":
                        e.Column.HeaderText = "Price Date";
                        break;
                    case "message":
                        e.Column.HeaderText = "Message";
                        break;
                    case "haserrors":
                        e.Column.HeaderText = "Has Errors";
                        e.Column.Visible = false;
                        break;
                    case "ismissingitem":
                        e.Column.HeaderText = "Is Missing Item";
                        e.Column.Visible = false;
                        break;
                    case "missingitem":
                        e.Column.HeaderText = "Missing Fund Code(s)";
                        break;
                } 
            }
            
        }

        protected void cmbSelectType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            VisibleControl();

        }

        private void VisibleControl()
        {
            if (cmbSelectType.SelectedValue == "ASXPrice")
            {

                lblCurrency.Visible = true;
                lblSelectDate.Visible = true;
                dtPicker.Visible = true;
                txtCurrency.Visible = true;

            }
            else
            {
                lblCurrency.Visible = false;
                lblSelectDate.Visible = false;
                dtPicker.Visible = false;
                txtCurrency.Visible = false;
            }
        }
    }
}