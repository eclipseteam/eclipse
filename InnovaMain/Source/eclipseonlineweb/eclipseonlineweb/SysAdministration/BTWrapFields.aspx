﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="BTWrapFields.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.BTWrapFields" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <table width="100%">
            <tr>
                <td width="100%" class="breadcrumbgap">
                    <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                    <br />
                    <asp:Label Font-Bold="true" runat="server" ID="lblAdministrationUsersList" Text="CLIENT BT WRAP REPORT"></asp:Label>
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <fieldset>
        <asp:Panel runat="server" ID="pnlMainGrid" Visible="true">
            <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" GridLines="None"
                AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="CID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="None"
                    Name="Users" TableLayout="Fixed">
                    <CommandItemSettings AddNewRecordText="Add New User" ShowExportToExcelButton="true"
                        ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                    <Columns>
                        <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="CID"
                            SortExpression="ClientID" UniqueName="ClientID" DataNavigateUrlFormatString="../ClientViews/ClientMainView.aspx?ins={0}"
                            HeaderText="Client ID" DataTextField="ClientID" AutoPostBackOnFilter="false"
                            CurrentFilterFunction="Contains" FilterControlWidth="80px" HeaderStyle-Width="90px"
                            HeaderButtonType="TextButton" ShowFilterIcon="true">
                        </telerik:GridHyperLinkColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90%" SortExpression="ClientName" HeaderStyle-Width="20%"
                            ItemStyle-Width="20%" HeaderText="Client Name" ReadOnly="true" Visible="true"
                            AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="ClientName" UniqueName="ClientName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="85%" SortExpression="AccountNumber"
                            HeaderStyle-Width="12%" ItemStyle-Width="12%" HeaderText="Account Number" AutoPostBackOnFilter="false"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="AccountNumber" UniqueName="AccountNumber">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90%" ReadOnly="false" HeaderStyle-Width="25%"
                            ItemStyle-Width="25%" SortExpression="AccountDescription" HeaderText="Account Description"
                            AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" Visible="true"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountDescription"
                            UniqueName="AccountDescription">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90%" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                            SortExpression="BusinessGroupID" HeaderText="Business Group ID" AutoPostBackOnFilter="false"
                            CurrentFilterFunction="Contains" Visible="false" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="BusinessGroupID" UniqueName="BusinessGroupID" ReadOnly="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="85%" ReadOnly="false" HeaderStyle-Width="8%"
                            ItemStyle-Width="8%" SortExpression="BusinessGroupName" HeaderText="Business Group"
                            AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" Visible="true"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BusinessGroupName"
                            UniqueName="BusinessGroupName">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="Html"></Excel>
                </ExportSettings>
            </telerik:RadGrid>
        </asp:Panel>
    </fieldset>
</asp:Content>
