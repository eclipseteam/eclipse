﻿using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Telerik.Web.UI;
using Oritax.TaxSimp.DataSets;
using System.Data;
using Oritax.TaxSimp.Data;

namespace eclipseonlineweb.SysAdministration
{
    public partial class BTWrapFields : UMABasePage
    {
        private void GetData()
        {
            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);

            var ds = new BTWrapDS
                {
                    Command = (int) WebCommands.GetAllClientDetails,
                    Unit = new OrganizationUnit
                        {
                            CurrentUser = GetCurrentUser()
                        }
                };
            org.GetData(ds);


            var dv = new DataView(ds.Tables[ds.btwrapTable.TableName]);

            const string strFilter = " AccountNumber <> '' or AccountDescription <> '' or BusinessGroupName <> ''";
            
            dv.RowFilter = strFilter;
            DataTable dt = dv.ToTable();
            
            PresentationGrid.DataSource = dt;
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //Call function to get data
            GetData();
        }
    }
}