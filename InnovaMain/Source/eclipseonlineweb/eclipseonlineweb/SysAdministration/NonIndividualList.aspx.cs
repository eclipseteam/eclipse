﻿using System;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;

namespace eclipseonlineweb.SysAdministration
{
    public partial class NonIndividualList : UMABasePage
    {
        private ICMBroker Broker
        {
            get
            {
                var umaBasePage = Page as UMABasePage;
                if (umaBasePage != null) return umaBasePage.UMABroker;
                return null;
            }
        }

        protected override void Intialise()
        {
            base.Intialise();
            NonIndividualControl.Saved += (CID, CLID, CSID) =>
                {
                    HideShowNonIndividualPopup(false);
                };
            NonIndividualControl.Canceled += () => HideShowNonIndividualPopup(false);
            NonIndividualControl.ValidationFailed += () => HideShowNonIndividualPopup(true);
            NonIndividualControl.SaveData += (Cid, ds) =>
                {
                    if (Cid == Guid.Empty.ToString())
                    {
                        SaveOrganizanition(ds);
                        PresentationGird.Rebind();
                    }
                    else
                    {
                        SaveData(Cid, ds);
                        PresentationGird.Rebind();
                    }
                    HideShowNonIndividualPopup(false);
                };
        }

        private void GetNonIndividualData()
        {
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);

            var nonIndividualDS = new NonIndividualDS();
            nonIndividualDS.Command = (int)WebCommands.GetOrganizationUnitsByType;
            nonIndividualDS.Unit = new OrganizationUnit
                {
                    CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                    Type = ((int)OrganizationType.NonIndividual).ToString()
                };
            org.GetData(nonIndividualDS);
            PresentationData = nonIndividualDS;
            Broker.ReleaseBrokerManagedComponent(org);
        }

        protected void btnAddNonIndividual_Click(object sender, EventArgs e)
        {
            Guid gCid = Guid.Empty;
            NonIndividualControl.SetEntity(gCid);
            HideShowNonIndividualPopup(true);
        }

        private void HideShowNonIndividualPopup(bool show)
        {
            OVER.Visible = NonIndividualModal.Visible = show;
        }

        protected void PresentationGird_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetNonIndividualData();
            PresentationGird.DataSource = (PresentationData as NonIndividualDS).NonIndividualTable;
        }

        protected void PresentationGird_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;
            var gvDataItem = e.Item as GridDataItem;
            var linkEntiyCid = new Guid(gvDataItem["Cid"].Text);

            if (e.CommandName.ToLower() == "edit")
            {
                e.Item.Edit = false;
                e.Canceled = true;
                NonIndividualControl.SetEntity(linkEntiyCid);
                HideShowNonIndividualPopup(true);
            }
            else if (e.CommandName.ToLower() == "delete")
            {
                DeleteInstance(linkEntiyCid);
                PresentationGird.Rebind();
            }
        }
    }
}