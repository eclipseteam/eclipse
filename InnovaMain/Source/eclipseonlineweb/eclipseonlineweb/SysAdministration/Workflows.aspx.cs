﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.SM.Workflows;
using Oritax.TaxSimp.SM.Workflows.Data;

namespace eclipseonlineweb
{
    public partial class Workflows : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void GetBMCData()
        {
            IBrokerManagedComponent bmc = this.UMABroker.CreateTransientComponentInstance(new Guid("15DE2644-C1A0-41D5-8293-2794EF31840D"));
            WorkflowsDS workflowsDS = new WorkflowsDS();
            bmc.GetData(workflowsDS);
            this.PresentationData = workflowsDS;
        }

        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
        }

        public override void LoadPage()
        {
            base.LoadPage();

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
                ((SetupMaster)Master).IsAdmin = "1";



            clientFilter.Visible = IsTestApp();

        }

        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e) { }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.ToLower() == "details")
                {
                    string[] IdIns = e.CommandArgument.ToString().Split(',');
                    Response.Redirect("~/SysAdministration/Notifications.aspx?ID=" + IdIns[0].ToString() + "&INS=" + IdIns[1].ToString() + "&PageName=" + "Notification");
                }
                else if (e.CommandName.ToLower() == "run")
                {
                    UMABroker.SaveOverride = true;
                    string id = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["WorkflowID"].ToString();
                    IBrokerManagedComponent bmc = this.UMABroker.CreateTransientComponentInstance(new Guid("15DE2644-C1A0-41D5-8293-2794EF31840D"));
                    Guid wfId = new Guid(id);
                    ExecuteWorkFlowDS executeWorkFlowDS = new ExecuteWorkFlowDS();
                    GridDataItem gDataItem = e.Item as GridDataItem;

                    executeWorkFlowDS.WorkflowType = (WorkflowTypeEnum)Enum.Parse(typeof(WorkflowTypeEnum), gDataItem["WorkflowType"].Text);
                    executeWorkFlowDS.WorkflowID = wfId;
                    if( IsTestApp())
                    {
                        executeWorkFlowDS.ClientCount = clientFilter.ClientCount;
                        executeWorkFlowDS.ClientIDs = clientFilter.ClientIDs;
                    }

                    bmc.SetData(executeWorkFlowDS);
                    UMABroker.SetComplete();
                    UMABroker.SetStart();
                    GetBMCData();
                    this.PresentationGrid.Rebind();
                }
                else if (e.CommandName.ToLower() == "delete")
                {
                    UMABroker.SaveOverride = true;
                    GridDataItem gDataItem = e.Item as GridDataItem;
                    Guid ID = new Guid(gDataItem["WorkflowID"].Text);
                    WorkflowsDS workflowDS = new WorkflowsDS();
                    workflowDS.DataSetOperationType = DataSetOperationType.DeletSingle;
                    workflowDS.WorkflowID = ID;
                    SaveDataTransientComponent(new Guid("15DE2644-C1A0-41D5-8293-2794EF31840D"), workflowDS);
                    GetBMCData();
                    this.PresentationGrid.Rebind();
                }
            }
            catch (Exception ex)
            {
                string strScript = ex.ToString().Replace("'", "");
                
                if (ex.InnerException != null)
                {
                    strScript = ex.InnerException.ToString().Replace("'", "");

                    if (ex.InnerException.ToString().Contains("dbo.Action"))
                    {
                        strScript = "WARNING!! MDA Action table does not exist in the database.";
                    }
                    
                    ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "showalert",
                                                            string.Format("alert('{0}');", strScript), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "showalert",
                                                        string.Format("alert('{0}');", strScript), true);
                }
            }
            
        }

        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) { }
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e) { }
        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            GridEditFormInsertItem EditForm = (GridEditFormInsertItem)e.Item;
            UMABroker.SaveOverride = true;

            string workflowDesc = ((TextBox)(((GridEditableItem)(e.Item))["WorkflowDesc"].Controls[0])).Text;
            string workflowName = ((TextBox)(((GridEditableItem)(e.Item))["WorkflowName"].Controls[0])).Text;
            RadComboBox combo = (RadComboBox)EditForm["WorkflowTypeCombo"].Controls[0];
            int worflowType = int.Parse(combo.SelectedItem.Value);

            WorkflowsDS workflowsDS = new WorkflowsDS();
            workflowsDS.DataSetOperationType = DataSetOperationType.NewSingle;
            workflowsDS.WorkflowToAdd = new Workflow();
            workflowsDS.WorkflowToAdd.WorkflowID = Guid.NewGuid();
            workflowsDS.WorkflowToAdd.CreatedBy = this.UMABroker.UserContext.Identity.Name;
            workflowsDS.WorkflowToAdd.WorkflowDesc = workflowDesc;
            workflowsDS.WorkflowToAdd.CreatedDate = DateTime.Now;
            workflowsDS.WorkflowToAdd.WorkflowType = worflowType;
            workflowsDS.WorkflowToAdd.WorkflowName = Enumeration.GetDescription((WorkflowTypeEnum)worflowType);
            workflowsDS.WorkflowToAdd.Comments = Enumeration.GetDescription((WorkflowTypeEnum)worflowType);
            SaveDataTransientComponent(new Guid("15DE2644-C1A0-41D5-8293-2794EF31840D"), workflowsDS);
            e.Canceled = true;
            e.Item.Edit = false;
            GetBMCData();
            this.PresentationGrid.Rebind();
        }

        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e) { }
        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;
                GridDropDownListColumnEditor editor = null;

                IDictionary<string, int> WorkflowTypeEnumList = Enumeration.ToDictionaryForBinding(typeof(WorkflowTypeEnum));
                editor = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("WorkflowTypeCombo"));
                editor.DataSource = WorkflowTypeEnumList;
                editor.DataTextField = "Key";
                editor.DataValueField = "Value";
                editor.DataBind();
            }
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DataView View = new DataView(this.PresentationData.Tables[WorkflowsDS.WorkflowsTable]);
            View.Sort = "CreatedDate DESC";
            this.PresentationGrid.DataSource = View.ToTable();
        }
    }
}
