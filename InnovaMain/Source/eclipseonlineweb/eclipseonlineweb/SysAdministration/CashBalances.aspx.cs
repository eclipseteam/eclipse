﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;

namespace eclipseonlineweb.SysAdministration
{
    public partial class CashBalances : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            return objUser.UserType == UserType.Innova || objUser.Name == "Administrator";
        }

        private void GetCashBalancesData()
        {
            IBrokerManagedComponent iBMC = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            var ds = new CashBalanceDS { CommandType = DatasetCommandTypes.Get };
            iBMC.GetData(ds);
            UMABroker.ReleaseBrokerManagedComponent(iBMC);
            CashBalancesGrid.DataSource = ds.CashBalanceTable;
        }

        protected void CashBalancesGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetCashBalancesData();
        }

        protected void CashBalancesGrid_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var editedItem = (GridEditableItem)e.Item;
                GridEditManager editMan = editedItem.EditManager;

                var superAccountTypeList = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("SuperAccountTypeEditor"));
                superAccountTypeList.ComboBoxControl.Filter = RadComboBoxFilter.Contains;
                superAccountTypeList.DataSource = Enum.GetNames(typeof(SuperAccountType));
                superAccountTypeList.DataBind();

                var edititem = (GridEditableItem)e.Item;
                var cmboSuperAccountType = (RadComboBox)edititem["SuperAccountTypeEditor"].Controls[0];
                cmboSuperAccountType.Width = 200;

                var txtPercentage = (RadNumericTextBox)edititem["Percentage"].Controls[0];
                txtPercentage.Width = 200;
                txtPercentage.MinValue = 0;
                txtPercentage.Value = txtPercentage.Value * 100;

                var txtMinValue = (RadNumericTextBox)edititem["MinValue"].Controls[0];
                txtMinValue.Width = 200;
                txtMinValue.MinValue = 0;


                var txtMaxValue = (RadNumericTextBox)edititem["MaxValue"].Controls[0];
                txtMaxValue.Width = 200;
                txtMaxValue.MinValue = 0;

                if (!(e.Item.DataItem is GridInsertionObject))
                {
                    string superAccountType = ((DataRowView)(e.Item.DataItem)).Row["SuperAccountType"].ToString();
                    if (superAccountType != string.Empty)
                    {
                        RadComboBoxItem item = superAccountTypeList.ComboBoxControl.FindItemByValue(superAccountType);
                        if (item != null)
                            item.Selected = true;
                        superAccountTypeList.ComboBoxControl.Enabled = false;
                    }
                }
            }
            e.Canceled = true;
        }

        protected void CashBalancesGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "update":
                    CashBalanceDS ds = PopulateDataSetWithData(e, DatasetCommandTypes.Update);
                    e.Item.Edit = false;
                    e.Canceled = true;
                    SaveOrganizanition(ds);
                    CashBalancesGrid.Rebind();
                    break;
            }
        }

        private CashBalanceDS PopulateDataSetWithData(GridCommandEventArgs e, DatasetCommandTypes command)
        {
            var ds = new CashBalanceDS();
            DataRow row = ds.CashBalanceTable.NewRow();
            ds.CommandType = command;

            var editForm = (GridEditFormItem)e.Item;
            if (command == DatasetCommandTypes.Update)
            {
                row[ds.CashBalanceTable.ID] = ((TextBox)editForm["ID"].Controls[0]).Text;
            }

            var cmbSuperAccountType = (RadComboBox)editForm["SuperAccountTypeEditor"].Controls[0];
            var percentage = (RadNumericTextBox)editForm["Percentage"].Controls[0];
            var minValue = (RadNumericTextBox)editForm["MinValue"].Controls[0];
            var maxValue = (RadNumericTextBox)editForm["MaxValue"].Controls[0];

            row[ds.CashBalanceTable.SUPERACCOUNTTYPE] = cmbSuperAccountType.SelectedItem.Text;
            row[ds.CashBalanceTable.PERCENTAGE] = string.IsNullOrEmpty(percentage.Text) || Convert.ToDecimal(percentage.Text) == 0 ? "0" : (Convert.ToDecimal(percentage.Text) / 100).ToString();
            row[ds.CashBalanceTable.MINVALUE] = string.IsNullOrEmpty(minValue.Text) ? "0" : minValue.Text;
            row[ds.CashBalanceTable.MAXVALUE] = string.IsNullOrEmpty(maxValue.Text) ? "0" : maxValue.Text;

            ds.CashBalanceTable.Rows.Add(row);
            return ds;
        }
    }
}