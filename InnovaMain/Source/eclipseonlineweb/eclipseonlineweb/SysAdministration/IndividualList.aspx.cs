﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Telerik.Web.UI;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class IndividualList : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void Intialise()
        {
            base.Intialise();
            IndividualControl.Saved += (CID, CLID, CSID) =>
            {
                HideShowIndividualPopup(false);
                gvIndividualList.Rebind();
            };
            IndividualControl.Canceled += () =>
            {
                HideShowIndividualPopup(false);
            };
            IndividualControl.ValidationFailed += () =>
            {
                HideShowIndividualPopup(true);
            };
            IndividualControl.SaveData += (Cid, ds) =>
            {
                if (Cid == Guid.Empty.ToString())
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(Cid, ds);
                }
                HideShowIndividualPopup(false);
            };

        }

        private ICMBroker Broker
        {

            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }


        public override void LoadPage()
        {
            base.LoadPage();

        }

        private void GetIndividualData()
        {
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);

            IndividualDS idividualDS = new IndividualDS();
            idividualDS.Command = (int)WebCommands.GetOrganizationUnitsByType;
            idividualDS.Unit = new OrganizationUnit
            {
                CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                Type = ((int)OrganizationType.Individual).ToString()
            };
            org.GetData(idividualDS);
            PresentationData = idividualDS;
            Broker.ReleaseBrokerManagedComponent(org);
        }

        private void HideShowIndividualPopup(bool show)
        {
            OVER.Visible = IndividualModal.Visible = show;
        }

        protected void btnAddIndividual_Click(object sender, EventArgs e)
        {
            Guid gCid = Guid.Empty;
            IndividualControl.SetEntity(gCid);
            HideShowIndividualPopup(true);
        }

        // This Patch is Required to Run because Individual ID of Old records were changing after IIS Reset
        protected void BtnRunClientID_Click(object sender, EventArgs e)
        {
            var idividualDS = new IndividualDS();
            idividualDS.Command = (int)WebCommands.SetOrganizationUnitClientIDsByType;
            idividualDS.Unit = new OrganizationUnit
            {
                CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                Type = ((int)OrganizationType.Individual).ToString()
            };
            SaveOrganizanition(idividualDS);
        }

        protected void btnSetIndividualShares_Click(object sender, EventArgs e)
        {
            var idividualDs = new ClientIndividualShareDS();
            idividualDs.Command = (int)WebCommands.UpdateAllClientDetails;
            idividualDs.Unit = new OrganizationUnit
            {
                CurrentUser = (Page as UMABasePage).GetCurrentUser(),

            };
            SaveOrganizanition(idividualDs);
        }

        protected void gvIndividualBList_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetIndividualData();
            gvIndividualList.DataSource = PresentationData;
        }

        protected void gvIndividualList_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;
            var gvDataItem = e.Item as GridDataItem;
            var linkEntiyCid = new Guid(gvDataItem["Cid"].Text);

            if (e.CommandName.ToLower() == "editrecords")
            {
                IndividualControl.SetEntity(linkEntiyCid);
                HideShowIndividualPopup(true);
            }
            else if (e.CommandName.ToLower() == "delete")
            {
                DeleteInstance(linkEntiyCid);
                gvIndividualList.Rebind();
            }

        }
    }
}
