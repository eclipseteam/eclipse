﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    EnableEventValidation="true" AutoEventWireup="true" CodeBehind="IFASoleTraderDetail.aspx.cs"
    Inherits="eclipseonlineweb.SysAdministration.IFASoleTraderDetail" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register Src="../Controls/IFAIndividualControl.ascx" TagName="IFAIndividualControl"
    TagPrefix="uc2" %>
<%@ Register Src="../Controls/ContactMappingControl.ascx" TagName="ContactMappingControl"
    TagPrefix="uc5" %>
<%@ Register Src="../Controls/BankAccountMappingControl.ascx" TagName="BankAccountMappingControl"
    TagPrefix="uc3" %>
<%@ Register Src="../Controls/AddressDetailControl.ascx" TagName="AddressControl"
    TagPrefix="uc7" %>
<%@ Register TagPrefix="uc2" TagName="SecurityConfigurationControl" Src="~/Controls/SecurityConfigurationControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                        </td>
                        <td width="90%" align="right">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblIFA" Text="IFA"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadTabStrip ID="RadTabStrip1" runat="server" Skin="Vista" MultiPageID="RadMultiPage1"
                SelectedIndex="0">
                <Tabs>
                    <telerik:RadTab Text="IFA Sole Trader" runat="server" Selected="True" Value="IFASoleTraderHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Address" runat="server" Visible="true" Value="AddressHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Sole Trader" runat="server" Visible="true" Value="SoleTraderHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Bank Account" runat="server" Visible="true" Value="BankAccounHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Security" runat="server" Visible="true" Value="SecurityHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Documents" runat="server" Visible="true" Value="DocumentsHeader">
                    </telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage runat="server" ID="RadMultiPage1" SelectedIndex="0">
                <telerik:RadPageView runat="server" ID="rpvIndividualControl">
                    <uc2:IFAIndividualControl ID="ifaIndividualControl1" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvAddress" runat="server">
                    <fieldset style="width: 98%">
                        <div style="text-align: left; float: none; border: 1px; background-color: White;
                            height: 25px; position: relative; vertical-align: middle; margin: 0; padding: 0;">
                            <telerik:RadButton ID="ImageButton1" Text="Add Client Addresses" ToolTip="Save Changes"
                                OnClick="btnSave_Click" runat="server" Width="32px" Height="32px" BorderStyle="None">
                                <Image ImageUrl="~/images/Save-Icon.png"></Image>
                            </telerik:RadButton>
                        </div>
                    </fieldset>
                    <br />
                    <uc7:AddressControl ID="AddressControl" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvSoleTrader" runat="server">
                    <uc5:ContactMappingControl ID="SoleTraderControl" runat="server"  IndividualsType="Applicants"  />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvBankAccount" runat="server">
                    <uc3:BankAccountMappingControl ID="BankAccountControl" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvSecurity" runat="server">
                    <uc2:SecurityConfigurationControl ID="SecurityControl" runat="server" />
                </telerik:RadPageView>
                <telerik:RadPageView ID="rpvDocuments" runat="server">
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
