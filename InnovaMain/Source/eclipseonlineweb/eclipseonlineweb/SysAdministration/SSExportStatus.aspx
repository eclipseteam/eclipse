﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="SSExportStatus.aspx.cs" Inherits="eclipseonlineweb.SSExportStatus" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/Save-Icon.png" OnClick="btnSave_OnClick"
                                ToolTip="Save" AlternateText="Save" ID="btnSave" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblExportStatus" Text="State Street Export Status"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <asp:Panel runat="server" ID="pnlMainGrid" Visible="true">
                <telerik:RadGrid OnNeedDataSource="ExportStatusGridNeedDataSource" ID="ExportStatusGrid"
                    runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                    AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="False" GridLines="None"
                    AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                    AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnItemDataBound="ExportStatusGridItemDataBound">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="None"
                        Name="ExportStatus" TableLayout="Fixed">
                        <Columns>
                            <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Cid" HeaderText="Cid"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="Cid" UniqueName="Cid" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Clid" HeaderText="Clid"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="Clid" UniqueName="Clid" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Csid" HeaderText="Csid"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="Csid" UniqueName="Csid" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="Cid"
                                SortExpression="ClientID" UniqueName="ClientID" DataNavigateUrlFormatString="../ClientViews/ClientMainView.aspx?ins={0}"
                                HeaderText="Client ID" DataTextField="ClientID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                FilterControlWidth="80px" HeaderButtonType="TextButton" ShowFilterIcon="true"
                                HeaderStyle-Width="12%" ItemStyle-Width="12%">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridBoundColumn FilterControlWidth="180px" SortExpression="TradingName"
                                HeaderStyle-Width="35%" ItemStyle-Width="35%" HeaderText="Trading Name" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="TradingName" UniqueName="TradingName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="Type" HeaderStyle-Width="20%"
                                ItemStyle-Width="20%" HeaderText="Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Type" UniqueName="Type">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Status" HeaderStyle-Width="15%"
                                ItemStyle-Width="15%" HeaderText="Status" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Status" UniqueName="Status">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="CheckBoxTemplateColumn" AutoPostBackOnFilter="true"
                                DataField="SSExport">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" 
                                        AutoPostBack="false" />
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="headerChkbox" Text="Export Status" runat="server" OnCheckedChanged="ToggleSelectedState"
                                        AutoPostBack="True" />
                                </HeaderTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
