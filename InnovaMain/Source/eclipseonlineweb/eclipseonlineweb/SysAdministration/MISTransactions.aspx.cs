﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class MISTransactions : UMABasePage
    {
        private bool flag = false;

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void Intialise()
        {
            base.Intialise();
            this.MISAccountList.RowCommand += new C1GridViewCommandEventHandler(MISAccountList_RowCommand);
            MISAccountList.RowEditing += new C1GridViewEditEventHandler(MISAccountList_RowEditing);
            MISAccountList.RowDeleting +=new C1GridViewDeleteEventHandler(MISAccountList_RowDeleting);
        }

        protected void MISAccountList_RowEditing(object sender, C1GridViewEditEventArgs e)
        { 
        
        }

        protected void MISAccountList_RowDeleting(object sender, C1GridViewDeleteEventArgs e)
        {

        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            if (e.Item.Text == "PLATFORM USERS")
                Response.Redirect(@"PlatformUsersList.aspx");
            else if (e.Item.Text == "BANK ACCOUNTS")
                Response.Redirect(@"BankAccounts.aspx");
        }


        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            DataSet excelDataset = new DataSet();
            DataView entityView = new DataView(this.PresentationData.Tables[MISTransactionDS.MISTRANTABLE]);
            DataTable entityTable = entityView.ToTable();
            
            excelDataset.Merge(entityTable, true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, "MIS-ALL TRANS-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", this.Page.Response);
        }

        private void GetData()
        {
            IBrokerManagedComponent clientData = UMABroker.GetCMImplementation(new Guid("7986b8ee-4509-44a5-ac8c-7ce59b59592d"), new Guid("b7ae4bb1-2143-4e56-b9ac-10cf24f7eae5"));
            MISTransactionDS ds = new MISTransactionDS();
            clientData.GetData(ds);
            this.PresentationData = ds; 
        }

        protected void SortMainList(object sender, C1GridViewSortEventArgs e)
        {
            PopulateData();
        }

        protected void Filter(object sender, C1GridViewFilterEventArgs e)
        {
            e.Values[0] = ((string)e.Values[0]).Trim();
            DataView entityView = new DataView(this.PresentationData.Tables[MISTransactionDS.MISTRANTABLE]);
            this.MISAccountList.DataSource = entityView.ToTable();
            MISAccountList.DataBind();
        }

        protected override void GetBMCData()
        {
            PopulateData();
        }

        private void PopulateData()
        {
            GetData();
            BindControls(this.PresentationData);
        }

        private void BindControls(DataSet ds)
        {
            DataView entityView = new DataView(ds.Tables[MISTransactionDS.MISTRANTABLE]);
            DataTable entityTable = entityView.ToTable();

            this.MISAccountList.DataSource = entityView.ToTable();
            this.MISAccountList.DataBind();
        }

        public override void LoadPage()
        {
            base.LoadPage();

            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
                sm.RegisterPostBackControl(this.btnDownload);
        }

        protected void NavigationMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            if (e.Item.Text == "PLATFORM USERS")
                Response.Redirect(@"PlatformUsersList.aspx");
            else if (e.Item.Text == "BANK ACCOUNTS")
                Response.Redirect(@"BankAccounts.aspx");
            else if (e.Item.Text == "MIS TRANSACTONS")
                Response.Redirect(@"MISTransactions.aspx");
        }

        public override void PopulatePage(DataSet ds)
        {
            if (!this.IsPostBack)
            {
                DataView entityView = new DataView(ds.Tables[MISTransactionDS.MISTRANTABLE]);
                DataTable entityTable = entityView.ToTable();
             }
        }

        protected void MISAccountList_PageIndexChanging(object sender, C1GridViewPageEventArgs e)
        {
            MISAccountList.PageIndex = e.NewPageIndex;
            MISAccountList.DataBind();
        }

        protected void MISAccountList_RowCommand(object sender, C1GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                string rowID = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[0].Text;
                string misrowID = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[1].Text;
                DataRow selectedRow = this.PresentationData.Tables[0].Select("ID='" + rowID + "'").FirstOrDefault();

                MISTransactionDetailsDS transactionDetailsDS = new MISTransactionDetailsDS();
                DataRow row = transactionDetailsDS.Tables[MISTransactionDetailsDS.MISTRANSDETAILSTABLE].NewRow();
                row[MISTransactionDetailsDS.ID] = rowID;
                row[MISTransactionDetailsDS.FUNDCODE] = selectedRow["Code"];
         
                transactionDetailsDS.DataSetOperationType = DataSetOperationType.DeletSingle;

                transactionDetailsDS.Tables[MISTransactionDetailsDS.MISTRANSDETAILSTABLE].Rows.Add(row);
                this.SaveData(misrowID, transactionDetailsDS);

                PopulateData();
            }
        }

        protected void MISAccountList_SelectedIndexChanging(object sender, C1.Web.Wijmo.Controls.C1GridView.C1GridViewSelectEventArgs e)
        {
        }

        protected void MISAccountList_DataBound(object sender, EventArgs e)
        {
            if (flag)
            {
                this.MISAccountList.SelectedIndex = 0;
            }
        }
    }
}
