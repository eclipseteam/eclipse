﻿using System;
using Telerik.Web.UI;
using Oritax.TaxSimp.DataSets;
using System.Data;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.CalculationInterface;
using eclipseonlineweb.WebUtilities;


namespace eclipseonlineweb.SysAdministration
{
    public partial class MissingDividendsReport : UMABasePage
    {

        private void GetData()
        {
            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            
            var ds = new BankTransactionDS()
            {
                Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = (Page as UMABasePage).GetCurrentUser(), },
                Command = (int)WebCommands.GetAllClientDetails,
                AddEmptyRow = false,
            };

            if (dtStartDate.SelectedDate.HasValue)
                ds.StartDate = dtStartDate.SelectedDate.Value;

            if (dtEndDate.SelectedDate.HasValue)
                ds.EndDate = dtEndDate.SelectedDate.Value;

            org.GetData(ds);
            PresentationData = ds;

            UMABroker.ReleaseBrokerManagedComponent(org);

        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (IsPostBack)
            {
                if (PresentationData == null)
                    GetData();

                if (PresentationData != null && PresentationData.Tables["Cash Transactions"] != null)
                {
                    var summaryView = new DataView(PresentationData.Tables["Cash Transactions"]) { Sort = "TransactionDate DESC" };
                    summaryView.RowFilter = "SystemTransactionType='Dividend' and DividendStatus<>'Matched'";
                    PresentationGrid.DataSource = summaryView;
                    btnExportToExcel.Visible = (summaryView.Count > 0);
                }
            }
        }
        protected void DividendGird_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (IsPostBack)
            {
                if (PresentationData == null)
                    GetData();

                if (PresentationData != null && PresentationData.Tables["ClientAllDividends"] != null)
                {
                    var summaryView = new DataView(PresentationData.Tables["ClientAllDividends"]) { Sort = "PaymentDate DESC" };
                    summaryView.RowFilter = "Status<>'Matched'";
                    DividendGird.DataSource = summaryView;

                }
            }
        }

        protected void btnRunReport_Click(object sender, EventArgs e)
        {
            PresentationData = null;
            PresentationGrid.Rebind();
            DividendGird.Rebind();
        }

        protected void btnExportToExcel_Click(object sender, System.EventArgs e)
        {
            if (PresentationData == null)
                GetData();

            var excelDataset = new DataSet();

            if (PresentationData != null && PresentationData.Tables["Cash Transactions"] != null)
            {
                var summaryView = new DataView(PresentationData.Tables["Cash Transactions"]) { Sort = "TransactionDate DESC" };
                summaryView.RowFilter = "SystemTransactionType='Dividend' and DividendStatus<>'Matched'";

                DataTable dt = summaryView.ToTable(true, CapitalReportDS.CLIENTID, CapitalReportDS.IMPTRANTYPE, CapitalReportDS.SYSTRANTYPE,
                    CapitalReportDS.CATEGORY, CapitalReportDS.BANKTRANSDATE, CapitalReportDS.ACCOUNTNO, CapitalReportDS.ACCOUNTTYPE, CapitalReportDS.BANKAMOUNT, CapitalReportDS.BANKADJUSTMENT, CapitalReportDS.AMOUNTTOTAL, CapitalReportDS.COMMENT);

                dt.TableName = "Bank Transactions";
                excelDataset.Merge(dt, true, MissingSchemaAction.Add);
            }

            if (PresentationData != null && PresentationData.Tables["ClientAllDividends"] != null)
            {
                var summaryView = new DataView(PresentationData.Tables["ClientAllDividends"]) { Sort = "PaymentDate DESC" };
                summaryView.RowFilter = "Status<>'Matched'";

                DataTable dt = summaryView.ToTable(true, "ClientID", "InvestmentCode", "TransactionType", "BalanceDate",
                "DividendType", "RecordDate", "BooksCloseDate", "PaymentDate", "UnitsOnHand",
                "CentsPerShare", "PaidDividend", "FrankingCredits",
                "TotalFrankingCredits", "Currency", "Status");

                dt.TableName = "Dividends Transactions";
                excelDataset.Merge(dt, true, MissingSchemaAction.Add);
            }

            ExcelHelper.ToExcel(excelDataset, string.Format("MissingDividend_{0}.xls", DateTime.Now.ToFileTime()), Page.Response);
        }
    }
}