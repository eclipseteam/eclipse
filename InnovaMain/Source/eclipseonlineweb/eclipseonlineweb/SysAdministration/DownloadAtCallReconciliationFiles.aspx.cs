﻿using System;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Configuration;
using Oritax.TaxSimp.Utilities;
using Telerik.Web.UI;

namespace eclipseonlineweb
{
    public partial class DownloadAtCallReconciliationFiles : UMABasePage
    {

        public override void LoadPage()
        {

        }

        private DataTable get_files()
        {
            string pattern = string.Empty;
            pattern = "*.*";
            string path = Server.MapPath("~\\" + ConfigurationManager.AppSettings["SaveAtCallReconciliation"]);
            bool IsExists = System.IO.Directory.Exists(path);
            if (!IsExists)
                System.IO.Directory.CreateDirectory(path);
            var directory = new DirectoryInfo(path);
            var files = directory.GetFiles(pattern);
            DataTable dt = CreateTableForExportInstructionsFiles();
            foreach (var fileInfo in files)
            {
                DataRow dr = dt.NewRow();
                dr["FileName"] = fileInfo.Name;
                dr["CreateDate"] = fileInfo.CreationTime;
                dt.Rows.Add(dr);
            }

            return dt;
        }
        protected void gd_Download_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        private void GetData()
        {
            DataView dv = get_files().DefaultView;
            dv.Sort = "CreateDate desc";
            gd_Download.DataSource = dv.ToTable();
        }

        private DataTable CreateTableForExportInstructionsFiles()
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("FileName", typeof(string));
            DataColumn dc1 = new DataColumn("CreateDate", typeof(DateTime));
            dt.Columns.Add(dc);
            dt.Columns.Add(dc1);
            return dt;
        }
        protected void gd_DownloadGridItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "download":
                    var item = (GridDataItem)e.Item;
                    string DownloadfileName = item["FileName"].Text;
                    DownloadFiles(DownloadfileName);
                    break;
                case "delete":
                    var item1 = (GridDataItem)e.Item;
                    string DeletedfileName = item1["FileName"].Text;
                    DeleteSelectedFile(DeletedfileName);
                    break;
            }
        }

        private void DeleteSelectedFile(string fileName)
        {
            string path = Server.MapPath("~\\" + ConfigurationManager.AppSettings["SaveAtCallReconciliation"]);
            path = path + fileName;
            File.Delete(path);
            GetData();
        }

        private void DownloadFiles(string fileName)
        {

            string path = Server.MapPath("~\\" + ConfigurationManager.AppSettings["SaveAtCallReconciliation"]);
            string strURL = path + fileName;
            byte[] bytes = File.ReadAllBytes(strURL);
            Session["ExportFileBytes"] = bytes;
            string script = string.Format("window.open('ExportReportPopup.aspx?fileName={0}&filetype={1}',null,'height=200,width=400,status=yes,toolbar=no,menubar=no,location=no');", fileName, "");
            ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "OpenNewWindow", script, true);


        }
    }
}