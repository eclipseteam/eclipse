﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using eclipseonlineweb.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class ImportBankAccountOpenings : UMABasePage
    {

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType == UserType.Innova || objUser.Name == "Administrator")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void Intialise()
        {
            ImportMessageControl.GridImportMessages.ColumnCreated += GridImportMessages_ColumnCreated;
            ImportMessageControl.GirdInvestmentCodeError.ColumnCreated += GridImportErrorMessages_ColumnCreated;
            ImportMessageControl.GridImportMessages.ItemDataBound += GridImportMessages_ItemDataBound;
            if (!IsPostBack)
            {
                ImportMessageControl.Clear();
            }

            ImportMessageControl.Completed += text =>
                                                  {
                                                      switch (text.ToLower())
                                                      {
                                                          case "next":
                                                              SendData();
                                                              break;
                                                          default:
                                                              ClearMessageControls();
                                                              break;
                                                      }
                                                  };

           
        }

        void GridImportMessages_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if((e.Item).ItemIndex!=-1)
            {
              DataRow dr= ((DataRowView) ((e.Item).DataItem)).Row;
                if(bool.Parse(dr["HasErrors"].ToString()))
                {
                    e.Item.ForeColor = Color.Red;
                }
            }

        }

       

       

        void GridImportMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {

            SetDataGridColumns(e);
        }

        void GridImportErrorMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            SetDataGridColumns(e);
        }

        public void BtnUploadClick(object sender, EventArgs e)
        {
            // Check to see if file was uploaded
            if (filMyFile.PostedFile != null)
            {
                // Get a reference to PostedFile object
                HttpPostedFile myFile = filMyFile.PostedFile;

                // Get size of uploaded file
                int nFileLen = myFile.ContentLength;

                // make sure the size of the file is > 0
                if (nFileLen > 0)
                {
                    // Allocate a buffer for reading of the file
                    string fileName = Path.GetFileName(myFile.FileName);
                    var date = DateTime.Now;
                    var distributionDictory = Server.MapPath("~/App_Data/Import/");
                    if (!Directory.Exists(distributionDictory))
                    {
                        Directory.CreateDirectory(distributionDictory);
                    }

                    string fileLocation = distributionDictory + date.ToString("ddMMyyhhmmsstt") + fileName;
                    byte[] myData = new byte[nFileLen];

                    // Read uploaded file from the Stream
                    myFile.InputStream.Read(myData, 0, nFileLen);

                    // Write data into a file
                    
                    Utilities.WriteToFile(fileLocation, ref myData);

                    SpreadsheetGear.IWorkbook workbook = SpreadsheetGear.Factory.GetWorkbook(fileLocation);

                    SpreadsheetGear.IRange range = workbook.Worksheets["Application Forms"].Cells;

                    DataTable dataTable = new DataTable();
                    dataTable.Columns.Add("AccountName");
                    dataTable.Columns.Add("BSB");
                    dataTable.Columns.Add("AccountNo");


                    bool isEmpty = false;
                    int count = 10;
                    while (!isEmpty)
                    {
                        if(range.Cells["AX" + count.ToString()].Value == null)
                            isEmpty = true;
                        else
                        {
                            string accountNo = range.Cells["AX" + count.ToString()].Value.ToString();
                       
                            DataRow row = dataTable.NewRow();
                            string accountName = string.Empty;

                            if(range.Cells["K" + count.ToString()].Value != null)
                                accountName = range.Cells["K" + count.ToString()].Value.ToString();
                            else
                                accountName = range.Cells["BJ" + count.ToString()].Value.ToString() + " " + range.Cells["BK" + count.ToString()].Value.ToString() + " " + range.Cells["G" + count.ToString()].Value.ToString();

                            string BSB1 = range.Cells["AZ" + count.ToString()].Value.ToString();
                            string BSB2 = range.Cells["BA" + count.ToString()].Value.ToString();

                            row["AccountName"] = accountName;
                            row["BSB"] = BSB1 + "-"+BSB2;
                            row["AccountNo"] = accountNo;
                            dataTable.Rows.Add(row);
                            count++; 
                        }
                    }


                    ImportProcessDS ds = new ImportProcessDS();
                    ds.Tables.Add(dataTable);
                    ds.Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() };
                    ds.Command = (int)WebCommands.ValidateAccountOpenningImportFile;
                    ds.FileName = fileName;
                    SaveOrganizanition(ds);
                    SetValidationGird(ds);

                    UMABroker.SaveOverride = true;

                    IBrokerManagedComponent iBMC = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
                    OrganisationListingDS organisationListingDS = new OrganisationListingDS();
                    organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.SetBankTransactions;
                    iBMC.GetData(organisationListingDS);
                    UMABroker.SetComplete();
                    UMABroker.SetStart(); 

                    PresentationData = ds;
                    string filename = ImportMessageControl.RefreshFileID() + ".xml";
                    ds.WriteXml(distributionDictory + filename,XmlWriteMode.WriteSchema);
                }
            }
        }

        private void SetValidationGird(ImportProcessDS ds)
        {
            //ImportMessageControl.Clear();
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                DataRow dr;
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    dr = missingItems.NewRow();
                    dr["MissingItem"] = row["AccountName"];
                    missingItems.Rows.Add(dr);
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();

                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingClientAccountsIgnored;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }
            int successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
            if (ds.Tables[0].Rows.Count == 0 || successfulRows == 0)
            {
                ImportMessageControl.SetButtons(ButtonTypes.CLOSE);
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sErrorsCannotContinue, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Error);
                ImportMessageControl.TextMessage.Text = "";
            }
            else
            {
                ImportMessageControl.SetButtons(ButtonTypes.NEXTCANCEL);
                //sucess
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sValidationCompleted ,successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
            }
        }

        private void SetCompletionGird(ImportProcessDS ds)
        {
            ImportMessageControl.Clear();
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }

            using (DataView dv = new DataView(ds.Tables[0]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                DataRow dr;
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    dr = missingItems.NewRow();
                    dr["MissingItem"] = row["ApplicationID"];
                    missingItems.Rows.Add(dr);
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();

                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingClientAccountsIgnored;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }
            int successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
            ImportMessageControl.SetButtons(ButtonTypes.FINISH);
            ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sImportedSuccessfully,successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
        }

        private void SendData()
        {
            var distributionDictory = Server.MapPath("~/App_Data/Import/");
            if (!Directory.Exists(distributionDictory))
            {
                Directory.CreateDirectory(distributionDictory);
            }

            string filename = distributionDictory + ImportMessageControl.GetFileID() + ".xml";
            ImportProcessDS ds = new ImportProcessDS();

            ds.ReadXml(filename);
            ds.Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportAccountOpenningImportFile;
            ds.FileName = filename;
            SaveOrganizanition(ds);
            SetCompletionGird(ds);
        }

        public void ClearMessageControls()
        {
            ImportMessageControl.Clear();
            ImportMessageControl.TextMessage.Text = "";
        }

        private static void SetDataGridColumns(GridColumnCreatedEventArgs e)
        {
            string colName = e.Column.UniqueName.ToLower();
            switch (colName)
            {
                case "applicationid":
                    e.Column.HeaderText = "Client ID";
                    break;
                case "bsb":
                    e.Column.HeaderText = "BSB";
                    e.Column.FilterControlWidth = Unit.Pixel(50);
                    break;
                case "bch":
                    e.Column.HeaderText = "BCH";
                    e.Column.FilterControlWidth = Unit.Pixel(50);
                    break;
                case "account number":
                    e.Column.HeaderText = "Account Number";
                    e.Column.FilterControlWidth = Unit.Pixel(170);
                    break;
                case "name":
                    e.Column.HeaderText = "Account Name";
                    break;
                case "trustee":
                    e.Column.HeaderText = "Trustee";
                    break;
                case "message":
                    e.Column.HeaderText = "Message";
                    break;
                case "haserrors":
                    e.Column.HeaderText = "Has Errors";
                    e.Column.Visible = false;
                    break;
                case "ismissingitem":
                    e.Column.HeaderText = "Is Missing Item";
                    e.Column.Visible = false;
                    break;
                case "missingitem":
                    e.Column.HeaderText = "Missing Client Accounts";
                    break;
            }
        }
    }
}