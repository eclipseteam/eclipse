﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="BusinessTitle.aspx.cs" Inherits="eclipseonlineweb.BusinessTitle" %>

<%@ Register TagPrefix="uc1" TagName="breadcrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="4%">
                        &nbsp;
                    </td>
                    <td width="100%" class="breadcrumbgap">
                        <uc1:breadcrumb ID="BreadCrumb1" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <asp:Button ID="btnDefaultBTitles" runat="server" Visible="false" Text="Default Business Titles" onclick="btnDefaultBTitles_Click" />
    <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
        runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
        AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnItemCommand="PresentationGrid_ItemCommand"
        GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
        AllowAutomaticUpdates="True" OnInsertCommand="PresentationGrid_InsertCommand"
        EnableViewState="true" ShowFooter="true">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
            Name="BusinessTitle" TableLayout="Fixed" EditMode="PopUp">
            <CommandItemSettings AddNewRecordText="Add New Business Title" ShowExportToExcelButton="true"
                ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
            <Columns>
                <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Title" HeaderStyle-Width="20%"
                    ItemStyle-Width="20%" HeaderText="Business Title" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="Title" UniqueName="txtTitle">
                </telerik:GridBoundColumn>
                <telerik:GridButtonColumn ConfirmText="Are you sure you want to delete this Title?"
                    ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                    <HeaderStyle Width="5%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                </telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
        <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
            <Excel Format="Html"></Excel>
        </ExportSettings>
    </telerik:RadGrid>
</asp:Content>
