﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="AdminMaster.master"
    CodeBehind="ClientsFeeAssociation.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.ClientsFeeAssociation" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="4%">
                    </td>
                    <td width="100%" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumbForPage" runat="server" />
                        <br />
                        <asp:Label Font-Bold="true" runat="server" ID="lblClientsFeeAssociation" Text="Clients & Fees Associations"></asp:Label>
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
    <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
        runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
        AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="false" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
        OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
        AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
        OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
        OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
        EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
        ShowGroupPanel="True" OnItemDataBound="PresentationGrid_ItemDataBound">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView DataKeyNames="ClientCID" AllowMultiColumnSorting="True" Width="100%"
            CommandItemDisplay="Top" Name="ClientsFeeAssociation" TableLayout="Fixed" EditMode="EditForms">
            <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false"
                ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
            <GroupByExpressions>
                <telerik:GridGroupByExpression>
                    <SelectFields>
                        <telerik:GridGroupByField FieldName="ClientName" HeaderValueSeparator=" : "></telerik:GridGroupByField>
                    </SelectFields>
                    <GroupByFields>
                        <telerik:GridGroupByField FieldName="ClientName" SortOrder="Ascending"></telerik:GridGroupByField>
                    </GroupByFields>
                </telerik:GridGroupByExpression>
            </GroupByExpressions>
            <Columns>
                <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="15%" ItemStyle-Width="35%"
                    SortExpression="ADVISERNAME" HeaderText="Adviser Name" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="ADVISERNAME" UniqueName="ADVISERNAME">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="20%" ItemStyle-Width="35%"
                    SortExpression="ClientName" HeaderText="Client Name" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="ClientName" UniqueName="ClientName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="15%" ItemStyle-Width="35%"
                    SortExpression="Description" HeaderText="Description" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="Description" UniqueName="Description">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="15%" ItemStyle-Width="35%"
                    SortExpression="Comments" HeaderText="Comments" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Comments" UniqueName="Comments">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="ServiceType"
                    ReadOnly="true" HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderText="Service Type"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="ServiceType" UniqueName="ServiceType">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="FeeType" ReadOnly="true"
                    HeaderStyle-Width="15%" ItemStyle-Width="15%" HeaderText="Fee Type" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="FeeType" UniqueName="FeeType">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn UniqueName="selec" HeaderText="" ShowFilterIcon="false"
                    AllowFiltering="false" HeaderStyle-Width="70px">
                    <ItemTemplate>
                        <asp:LinkButton ID="Definition" runat="server" Text="Configure" CommandArgument='<%# Eval("ClientCID")%>'
                            CommandName="Configure"></asp:LinkButton>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
        </MasterTableView>
        <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
            <Excel Format="Html"></Excel>
        </ExportSettings>
    </telerik:RadGrid>
</asp:Content>
