﻿using System;
using System.Web.UI;
using System.Data;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;

namespace eclipseonlineweb
{
    public partial class AtCallHoldingVSTransactions : UMABasePage
    {
        //private bool flag = false;

        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            return true;
        }

        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            var excelDataset = new DataSet();
            var entityView = new DataView(PresentationData.Tables["AtCallHoldings"]);
            DataTable entityTable = entityView.ToTable();
            
            excelDataset.Merge(entityTable, true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, "AT CALL HOLDING VS TRANS-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", Page.Response);
        }

        

        private void GetData()
        {
            var holdingFileName = Server.MapPath("~/App_Data/Import/") + Request.Params["ID"] + ".xml";
            var ds = new ImportProcessDS();
            ds.ReadXml(holdingFileName);            
            PresentationData = ds;

            DataTable dtAtCallHoldings = ds.Tables["AtCallHoldings"];
            DataTable datatable = dtAtCallHoldings.Copy();
            if (datatable.Columns.Contains("ClientCID"))
            {
                datatable.Columns.Remove("ClientCID"); 
            }
            if (datatable.Columns.Contains("CID"))
            {
                datatable.Columns.Remove("CID");
            }
            if (datatable.Columns.Contains("CLID"))
            {
                datatable.Columns.Remove("CLID");
            }
            if (datatable.Columns.Contains("CSID"))
            {
                datatable.Columns.Remove("CSID");
            }
            //if (Session["AtCallVsTransactions"] == null)
            //{
            //    Session["AtCallVsTransactions"] = ds;
            //    //// Open the template workbook, which contains number formats and 
            //    //// formulas, and get an IRange from a defined name
            //    //string path = "~/Templates/AtCallHoldings.xls";
            //    //SpreadsheetGear.IWorkbook workbook = SpreadsheetGear.Factory.GetWorkbook(HttpContext.Current.Server.MapPath(path));
            //    //SpreadsheetGear.IRange range = workbook.Names["AtCallHoldingVsTransaction"].RefersToRange;
            //    //// Insert the DataTable into the template worksheet range. The InsertCells
            //    //// flag will cause the formatted range to be adjusted for the inserted data.
            //    //range.CopyFromDataTable(datatable, SpreadsheetGear.Data.SetDataFlags.NoColumnHeaders);
            //    //// Save to disk. 
            //    //string DiskPath = Server.MapPath("~\\" + ConfigurationManager.AppSettings["SaveAtCallReconciliation"]);
            //    //bool IsExists = System.IO.Directory.Exists(DiskPath);
            //    //if (!IsExists)
            //    //    System.IO.Directory.CreateDirectory(DiskPath);
            //    //DiskPath = DiskPath + "AtCallHoldingsVsTransactions_" + DateTime.Now.ToString("yyyy_MM_dd") + "_" + DateTime.Now.ToString("hhmmssFFF") + ".xls";
            //    //workbook.SaveAs(DiskPath, FileFormat.Excel8);
            //}
        }
        protected void btnShowFiles_Click(object sender, EventArgs e)
        {
            Response.Redirect("DownloadAtCallReconciliationFiles.aspx");
        }
        //protected void SortMainList(object sender, C1GridViewSortEventArgs e)
        //{
        //    PopulateData();
        //}        

        protected void gd_AtCall_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            
            //if (Session["AtCallVsTransactions"] != null)
            //{
            //    ImportProcessDS impDS = new ImportProcessDS();
            //    impDS = (ImportProcessDS)Session["AtCallVsTransactions"];
            //    DataView entityView = new DataView(impDS.Tables["AtCallHoldings"]);
            //    gd_AtCall.DataSource = entityView.ToTable();
            //}
            //else
            //{
                GetData();
                var entityView = new DataView(PresentationData.Tables["AtCallHoldings"]);
                gd_AtCall.DataSource = entityView.ToTable();
            //}
            //gd_AtCall.DataBind();
        }

        //protected override void GetBMCData()
        //{
        //    PopulateData();
        //}

        //private void PopulateData()
        //{
        //    GetData();
        //    BindControls(PresentationData);
        //}

        //private void BindControls(DataSet ds)
        //{
        //    DataView entityView = new DataView(ds.Tables["AtCallHoldings"]);
        //    DataTable entityTable = entityView.ToTable();

        //    gd_AtCall.DataSource = entityView.ToTable();
        //    //gd_AtCall.DataBind();
        //}

        public override void LoadPage()
        {
            base.LoadPage();

            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
                sm.RegisterPostBackControl(btnDownload);
        }        

       
    }
}
