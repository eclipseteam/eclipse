﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using eclipseonlineweb.Export;
using eclipseonlineweb.WebUtilities;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb.SysAdministration
{
    public partial class SubmitToDesktopBroker1 : UMABasePage
    {
        private DataView _bulkOrdersView;
        private DataView _bulkOrderItemsView;
        private DataView _bulkOrderItemDetailsView;

        private const string FIN_SIMPLICITY = "Fin Simplicity";
        private const string SMA = "SMA";

        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            return true;
        }

        // called from page_load event in UMABasePage
        public override void PopulatePage(DataSet ds)
        {
            this.cbFrom.Items.Clear();
            RadComboBoxItem item1 = new RadComboBoxItem(FIN_SIMPLICITY, FIN_SIMPLICITY);
            this.cbFrom.Items.Add(item1);

            RadComboBoxItem item2 = new RadComboBoxItem(SMA, SMA);
            this.cbFrom.Items.Add(item2);

            this.cbFrom.SelectedIndex = 1;
        }

        private void GetOrders()
        {
            var orderUnit = new OrganizationUnit
            {
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = (Page as UMABasePage).GetCurrentUser()
            };

            //BulkOrders Lists
            var ds = new BulkOrderDS
            {
                Unit = orderUnit,
                CommandType = DatasetCommandTypes.Get,
                Command = (int)WebCommands.GetOrganizationUnitsByType
            };

            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            if (org != null)
            {
                org.GetData(ds);
                UMABroker.ReleaseBrokerManagedComponent(org);
            }

            _bulkOrdersView = ds.Tables[ds.BulkOrdersTable.TableName].DefaultView;
            _bulkOrderItemsView = ds.Tables[ds.BulkOrderItemsTable.TableName].DefaultView;
            _bulkOrderItemDetailsView = ds.Tables[ds.BulkOrderItemDetailsList.TableName].DefaultView;

            PresentationGrid.DataSource = _bulkOrdersView;
        }

        protected void PresentationGridDetailTableDataBind(object source, GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = e.DetailTableView.ParentItem;
            var id = dataItem["ID"].Text;

            var isTD = false;
            if (dataItem.OwnerTableView.Columns.FindByUniqueNameSafe("OrderAccountType") != null)
            {
                OrderAccountType orderAccountType;
                bool isValidOrderAccountType = Enum.TryParse(dataItem["OrderAccountType"].Text, out orderAccountType);
                if (isValidOrderAccountType)
                {
                    isTD = orderAccountType == OrderAccountType.TermDeposit;
                }
            }

            if (dataItem.OwnerTableView.Columns.FindByUniqueNameSafe("IsExported") != null)
            {
                string filter = string.Format("BulkOrderID = '{0}'", id);
                switch (e.DetailTableView.Name)
                {
                    case "BulkOrderItems":
                        {
                            e.DetailTableView.DataSource = _bulkOrderItemsView;
                            _bulkOrderItemsView.RowFilter = filter;
                            //TD
                            e.DetailTableView.Columns.FindByUniqueNameSafe("BrokerName").Visible = isTD;
                            e.DetailTableView.Columns.FindByUniqueNameSafe("InstituteName").Visible = isTD;
                            e.DetailTableView.Columns.FindByUniqueNameSafe("Term").Visible = isTD;
                            e.DetailTableView.Columns.FindByUniqueNameSafe("Rate").Visible = isTD;
                            e.DetailTableView.Columns.FindByUniqueNameSafe("Amount").Visible = isTD;
                            //ASX
                            e.DetailTableView.Columns.FindByUniqueNameSafe("Code").Visible = !isTD;
                            e.DetailTableView.Columns.FindByUniqueNameSafe("ExpectedUnitPrice").Visible = !isTD;
                            e.DetailTableView.Columns.FindByUniqueNameSafe("ExpectedUnits").Visible = !isTD;
                            e.DetailTableView.Columns.FindByUniqueNameSafe("ExpectedAmount").Visible = !isTD;
                            e.DetailTableView.Columns.FindByUniqueNameSafe("ActualUnitPrice").Visible = !isTD;
                            e.DetailTableView.Columns.FindByUniqueNameSafe("ActualUnits").Visible = !isTD;
                            e.DetailTableView.Columns.FindByUniqueNameSafe("ActualAmount").Visible = !isTD;
                        }
                        break;
                }
            }
            else
            {
                var ids = GetOrderIds(dataItem);
                var filter = string.Format("BulkOrderItemID = '{0}' and OrderItemID IN ({1})", id, ids);
                isTD = !string.IsNullOrEmpty(dataItem["BrokerName"].Text.Replace("&nbsp;", ""));
                bool isApportioned = Convert.ToBoolean(dataItem["IsApportioned"].Text);
                switch (e.DetailTableView.Name)
                {
                    case "BulkOrderItemDetails":
                        e.DetailTableView.DataSource = _bulkOrderItemDetailsView;
                        _bulkOrderItemDetailsView.RowFilter = filter;
                        //TD
                        e.DetailTableView.Columns.FindByUniqueNameSafe("BrokerName").Visible = isTD;
                        e.DetailTableView.Columns.FindByUniqueNameSafe("InstituteName").Visible = isTD;
                        e.DetailTableView.Columns.FindByUniqueNameSafe("Term").Visible = isTD;
                        e.DetailTableView.Columns.FindByUniqueNameSafe("Rate").Visible = isTD;
                        //ASX
                        e.DetailTableView.Columns.FindByUniqueNameSafe("Code").Visible = !isTD;
                        e.DetailTableView.Columns.FindByUniqueNameSafe("Currency").Visible = !isTD;
                        e.DetailTableView.Columns.FindByUniqueNameSafe("UnitPrice").Visible = !isTD;
                        e.DetailTableView.Columns.FindByUniqueNameSafe("Units").Visible = !isTD;
                        e.DetailTableView.Columns.FindByUniqueNameSafe("ActualUnitPrice").Visible = !isTD && isApportioned;
                        e.DetailTableView.Columns.FindByUniqueNameSafe("ActualUnits").Visible = !isTD && isApportioned;
                        e.DetailTableView.Columns.FindByUniqueNameSafe("ActualAmount").Visible = !isTD && isApportioned;
                        e.DetailTableView.Columns.FindByUniqueNameSafe("Amount").HeaderText = !isTD ? "Suggested Amount" : "Amount";
                        break;
                }
            }
        }

        private static string GetOrderIds(GridDataItem dataItem)
        {
            var orderItemIds = dataItem["OrderItemIDs"].Text;
            var ids = string.Empty;

            if (orderItemIds.Length > 0)
            {
                orderItemIds = orderItemIds.Remove(orderItemIds.Length - 1);
                var itemIds = orderItemIds.Split(',');
                foreach (var itemId in itemIds)
                {
                    ids += string.Format("CONVERT('{0}','System.Guid'),", new Guid(itemId));
                }
            }
            return ids;
        }

        protected void PresentationGridItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "complete":
                    if (Page.IsValid)
                    {
                        Guid bulkOrderItemID = Guid.Parse(e.CommandArgument.ToString());
                        var item = (GridDataItem)e.Item;
                        var isExported = Convert.ToBoolean(item.OwnerTableView.ParentItem["IsExported"].Text);
                        if (isExported)
                        {
                            OrderAccountType orderAccountType;
                            bool isValidOrderAccountType = Enum.TryParse(item.OwnerTableView.ParentItem["OrderAccountType"].Text, out orderAccountType);
                            if (isValidOrderAccountType)
                            {
                                var ds = CreateBulkOrderDS(item, bulkOrderItemID, orderAccountType);
                                var index = e.Item.OwnerTableView.ParentItem.ItemIndex;
                                ExecuteCommand(ds);
                                PresentationGrid.MasterTableView.Items[index].Expanded = true;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "OpenAlert", "alert('Please First Export Bulk Orders')", true);
                            PresentationGrid.Rebind();
                        }
                    }
                    break;
            }

            //Collapse All Other Expanded Items
            if (e.CommandName == RadGrid.ExpandCollapseCommandName)
            {
                foreach (GridItem item in e.Item.OwnerTableView.Items)
                {
                    if (item.Expanded && item != e.Item)
                    {
                        item.Expanded = false;
                    }
                }
            }
        }

        private BulkOrderDS CreateBulkOrderDS(GridDataItem item, Guid bulkOrderItemID, OrderAccountType orderAccountType)
        {
            var bulkOrderId = item["BulkOrderID"].Text;
            var txtNote = item["ContractNote"].Controls[1] as RadTextBox;

            string contractNote = string.Empty;
            decimal units = 0;
            decimal unitPrice = 0;
            decimal brokerage = 0;
            decimal charges = 0;
            decimal tax = 0;
            decimal grossValue = 0;
            decimal amount = 0;

            if (txtNote != null) contractNote = txtNote.Text;

            if (orderAccountType == OrderAccountType.ASX)
            {
                var txtUnits = item["ActualUnits"].Controls[1] as RadNumericTextBox;
                var txtUnitPrice = item["ActualUnitPrice"].Controls[1] as RadNumericTextBox;
                var txtBrokerage = item["Brokerage"].Controls[1] as RadNumericTextBox;
                var txtCharges = item["Charges"].Controls[1] as RadNumericTextBox;
                var txtTax = item["Tax"].Controls[1] as RadNumericTextBox;
                var txtGrossValue = item["GrossValue"].Controls[1] as RadNumericTextBox;

                if (txtUnits != null) units = Convert.ToDecimal(txtUnits.Text);
                if (txtUnitPrice != null) unitPrice = Convert.ToDecimal(txtUnitPrice.Text);
                if (txtBrokerage != null && !string.IsNullOrEmpty(txtBrokerage.Text)) brokerage = Convert.ToDecimal(txtBrokerage.Text);
                if (txtCharges != null && !string.IsNullOrEmpty(txtCharges.Text)) charges = Convert.ToDecimal(txtCharges.Text);
                if (txtTax != null && !string.IsNullOrEmpty(txtTax.Text)) tax = Convert.ToDecimal(txtTax.Text);
                if (txtGrossValue != null && !string.IsNullOrEmpty(txtGrossValue.Text)) grossValue = Convert.ToDecimal(txtGrossValue.Text);

                amount = units * unitPrice;
            }

            var unit = new OrganizationUnit
                           {
                               Type = ((int)OrganizationType.Order).ToString(),
                               CurrentUser = (Page as UMABasePage).GetCurrentUser()
                           };

            var ds = new BulkOrderDS { CommandType = DatasetCommandTypes.Update, Unit = unit };

            DataRow dr = ds.BulkOrderItemsTable.NewRow();

            dr[ds.BulkOrderItemsTable.ID] = bulkOrderItemID;
            dr[ds.BulkOrderItemsTable.BULKORDERID] = bulkOrderId;
            dr[ds.BulkOrderItemsTable.CONTRACTNOTE] = contractNote;
            if (orderAccountType == OrderAccountType.ASX)
            {
                dr[ds.BulkOrderItemsTable.ACTUALAMOUNT] = amount;
                dr[ds.BulkOrderItemsTable.ACTUALUNITPRICE] = unitPrice;
                dr[ds.BulkOrderItemsTable.ACTUALUNITS] = units;
                dr[ds.BulkOrderItemsTable.BROKERAGE] = brokerage;
                dr[ds.BulkOrderItemsTable.CHARGES] = charges;
                dr[ds.BulkOrderItemsTable.TAX] = tax;
                dr[ds.BulkOrderItemsTable.GROSSVALUE] = grossValue;
            }
            ds.BulkOrderItemsTable.Rows.Add(dr);
            return ds;
        }

        private void ExecuteCommand(BulkOrderDS ds)
        {
            Guid orderCMCID = IsOrderCmExists();
            if (orderCMCID != Guid.Empty)
            {
                SaveData(orderCMCID.ToString(), ds);
            }
            //refresh grid
            PresentationGrid.Rebind();
        }

        protected void PresentationGridItemDataBound(object sender, GridItemEventArgs e)
        {
        }

        protected void PresentationGridNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetOrders();
        }

        protected void btnExportOrders_OnClick(object sender, EventArgs e)
        {
        }

        protected void btnShowExportedOrders_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("DownloadExportFiles.aspx?type=" + Export.ExportType.BulkInstructionsExport, false);
        }

        private Guid IsOrderCmExists()
        {
            var unit = new OrganizationUnit
            {
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = (Page as UMABasePage).GetCurrentUser()
            };

            var ds = new OrderPadDS
            {
                CommandType = DatasetCommandTypes.Check,
                Unit = unit,
                Command = (int)WebCommands.GetOrganizationUnitsByType
            };

            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            if (org != null)
            {
                org.GetData(ds);
                UMABroker.ReleaseBrokerManagedComponent(org);
            }

            var orderCMCID = Guid.Empty;
            if (ds.OrdersTable.Rows.Count > 0)
            {
                //Getting ORDER CID 
                orderCMCID = new Guid(ds.OrdersTable.Rows[0][ds.OrdersTable.CID].ToString());
            }

            return orderCMCID;
        }

        protected void PresentationGrid_OnItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem && e.Item.OwnerTableView.Name == "BulkOrderItems")
            {
                var item = (GridDataItem)e.Item;
                var btnAdjust = (LinkButton)item.FindControl("btnAdjust");
                if (btnAdjust != null)
                {
                    string type = e.Item.OwnerTableView.ParentItem["OrderAccountType"].Text;
                    int index = item.ItemIndex;
                    btnAdjust.Attributes.Add("onclick", string.Format("return change('{0}','{1}');", index, type));
                }
            }
        }

        protected void btnUMA_OnClick(object sender, EventArgs e)
        {
            PresentationGrid.Rebind();
        }

        protected void btnSMA_OnClick(object sender, EventArgs e)
        {
            PresentationGrid.Rebind();
        }

        private string GetPath()
        {
            return string.Empty;
        }

        private List<Guid> GetBulkOrderIDs()
        {
            var orderIDs = new List<Guid>();

            foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
            {
                var isExported = Convert.ToBoolean(dataItem["IsExported"].Text);
                var chkSelect = dataItem.FindControl("chkSelect") as CheckBox;
                if (chkSelect != null && chkSelect.Visible && chkSelect.Checked && !isExported)
                {
                    Guid id = Guid.Parse(dataItem["ID"].Text);
                    orderIDs.Add(id);
                }
            }

            return orderIDs;
        }

        protected void btnSubmitBulkOrders_OnClick(object sender, EventArgs e)
        {
            // get selected bulk order ID for UMA
            var BulkOrdersID = GetSelectedBulkOrdersIDsForUMA();

            // error message should be poped up from JavaScript
            if (BulkOrdersID.Count == 0)
                return;

            var unit = new OrganizationUnit
            {
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = (Page as UMABasePage).GetCurrentUser()
            };

            var ds = new BulkOrderDS { CommandType = DatasetCommandTypes.SubmitToDesktopBroker, Unit = unit, BulkOrderIDs = BulkOrdersID };

            // Get OrdersCM CID. OrdersCM is unique.
            Guid orderCMCID = IsOrderCmExists();

            if (orderCMCID != Guid.Empty)
            {
                var orderCM = UMABroker.GetBMCInstance(orderCMCID);
                orderCM.SetData(ds);
                UMABroker.SetComplete();
                UMABroker.SetStart();

                // refresh grid
                PresentationGrid.Rebind();
            }
        }

        private List<Guid> GetSelectedBulkOrdersIDsForUMA()
        {
            var bulkorders = new List<Guid>();

            foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
            {
                var status = dataItem["Status"].Text;
                var checkBox = dataItem.FindControl("chkSelect") as CheckBox;
                if (checkBox != null && checkBox.Visible && checkBox.Checked)
                {
                    Guid id = Guid.Parse(dataItem["ID"].Text);
                    bulkorders.Add(id);
                }
            }

            return bulkorders;
        }

        protected void btnBulkExportSMA_Click(object sender, EventArgs e)
        {
            var unit = new OrganizationUnit
            {
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = (Page as UMABasePage).GetCurrentUser()
            };
            var BulkOrdersID = GetSelectedBulkOrdersIDsForSMA();
            if (BulkOrdersID.Count > 0)
            {
                var ds = new BulkOrderDS {CommandType = DatasetCommandTypes.SendOrderToSMA, Unit = unit, BulkOrderIDs = BulkOrdersID};


                Guid orderCMCID = IsOrderCmExists();

                if (orderCMCID != Guid.Empty)
                {
                    SaveData(orderCMCID.ToString(), ds);
                    ShowSMAMessage(ds);
                    PresentationGrid.Rebind();
                }
            }

        }
      
        private void ShowSMAMessage(DataSet ds)
        {
            if (ds.ExtendedProperties.Contains("SMAMessages"))
                ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "ShowAlert", "alert('There was error in sending data to Super Trade service.Please check export Log file from exported files  ')", true);
        }

        private List<Guid> GetSelectedBulkOrdersIDsForSMA()
        {
            var bulkorders = new List<Guid>();

            foreach (GridDataItem dataItem in PresentationGrid.MasterTableView.Items)
            {
                var status = dataItem["Status"].Text;
                var checkBox = dataItem.FindControl("chkSelect1") as CheckBox;
                if (checkBox != null && checkBox.Visible && checkBox.Checked && status == BulkOrderStatus.Complete.ToString())
                {
                    Guid id = Guid.Parse(dataItem["ID"].Text);
                    bulkorders.Add(id);
                }
            }

            return bulkorders;
        }
    }
}