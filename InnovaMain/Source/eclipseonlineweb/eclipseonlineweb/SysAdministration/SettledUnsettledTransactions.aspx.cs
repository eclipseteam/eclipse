﻿using System;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb.SysAdministration
{
    public partial class SettledUnsettledTransactions : UMABasePage
    {
        protected override void Intialise()
        {
            SettledUnsettledTransactionsControl.SaveData += SaveData;
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        public override void LoadPage()
        {
            if (!IsPostBack)
            {
                cid = Request.QueryString["ins"];
                var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
                SettledUnsettledTransactionsControl.IsAdmin = objUser.Administrator;
                SettledUnsettledTransactionsControl.IsAdminMenu = true;
                if (Request.QueryString["OrderId"] != null)
                {
                    SettledUnsettledTransactionsControl.OrderId = Guid.Parse(Request.QueryString["OrderId"]);
                }
            }
        }
    }
}