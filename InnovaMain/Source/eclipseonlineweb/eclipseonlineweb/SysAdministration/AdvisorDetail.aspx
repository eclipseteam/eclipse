﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="AdvisorDetail.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.AdvisorDetail" %>

<%@ Register src="../Controls/AddressControl.ascx" tagname="AddressControl" tagprefix="uc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<%@ Register src="../Controls/ContactMappingControlExtension.ascx" tagname="ContactMappingControlExtension" tagprefix="uc3" %>


<%@ Register src="../Controls/ContactMappingControl.ascx" tagname="ContactMappingControl" tagprefix="uc5" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="../Controls/BankAccountMappingControl.ascx" tagname="BankAccountMappingControl" tagprefix="uc2" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">   
   
    
   
 <ajaxToolkit:Accordion ID="MyAccordion" runat="server" SelectedIndex="0" FadeTransitions="true"
   FramesPerSecond="40" TransitionDuration="250" AutoSize="None" HeaderCssClass="accordionHeader"
    ContentCssClass="accordionContent">
    <Panes>
        <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                <Header>
                    <asp:Label runat="server" ID="lblDealerGroupTitle">Dealer Group Sole Trader</asp:Label>                                                        
                </Header>
                <Content> 
                    <fieldset style="background-color: darkblue; color:White;">
                        <table width="100%">
                            <tr>
                                <td>
                                    Advisor - Sole Trader
                                </td>
                            </tr>
                        </table>
                    </fieldset>  
                    <table width="100%">                           
                            <tr>
                                <td style="text-align:right;">
                                    
                                </td>                                
                            </tr>
                    </table>                                            
                </Content>
            </ajaxToolkit:AccordionPane>
        <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server">
                <Header>
                    <asp:Label runat="server" ID="lblTitle">Business Address</asp:Label>                                  
                </Header>
                <Content>
                    <fieldset style="background-color: darkblue; color:White;">
                        <table width="100%">
                            <tr>
                                <td>
                                    Business Address
                                </td>
                            </tr>
                        </table>
                    </fieldset>    
                    <uc1:AddressControl ID="AddressControl1" runat="server" /> 
                </Content>
            </ajaxToolkit:AccordionPane>
        <ajaxToolkit:AccordionPane ID="AccordionPane3" runat="server">
                <Header>
                    <asp:Label runat="server" ID="lblSoleTrader">Sole Trader</asp:Label>                          
                </Header>
                <Content>
                <fieldset style="background-color: darkblue; color:White;">
                        <table width="100%">
                            <tr>
                                <td>
                                    Sole Trader
                                </td>
                            </tr>
                        </table>
                </fieldset>   
                    <table width="100%">
                        <tr>
                            <td style="text-align:right;">
                                 <uc5:ContactMappingControl ID="ContactMappingControl1" runat="server" />
                            </td>                                
                        </tr>
                    </table>                           
                </Content>
        </ajaxToolkit:AccordionPane>
        <ajaxToolkit:AccordionPane ID="AccordionPane4" runat="server">
                <Header>
                    <asp:Label runat="server" ID="lblBankAccount">Bank Account</asp:Label>  
                </Header>
                <Content>
                <fieldset style="background-color: darkblue; color:White;">
                        <table width="100%">
                            <tr>
                                <td>
                                    Bank Account
                                </td>
                            </tr>
                        </table>
                </fieldset>   
                    <table width="100%">                            
                            <tr>
                                <td style="text-align:right;">
                                    <uc2:BankAccountMappingControl ID="BankAccountSearchControl" runat="server" />
                                </td>                                
                            </tr>
                        </table>                        
                </Content>
        </ajaxToolkit:AccordionPane>
    </Panes>
 </ajaxToolkit:Accordion>
</asp:Content>


