﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eclipseonlineweb
{
    public class ImportMessages
    {
        public const string sMissingStockCodeAdded = "Missing Stock code(s) have been added.";
        public const string sSelectProductSecurity = "Please select Product Security";
        public const string sMissingFundCodeIgnored = "Missing Fund Code(s) will be ignored automatically.";
        public const string sMissingCodeAdded = "Missing code(s) will be added automatically.";
        public const string sMissingCodeIgnored = "Missing code(s) will be Ignored automatically.";
        public const string sMissingInvestmentCodeAdded = "Missing Investment code(s) will be added automatically.";
        public const string sMissingInvestmentCodeIgnored = "Missing Investment code(s) will be ignored automatically.";
        public const string sMissingProviderAdded = "Missing Providers (institutes) will be added automatically.";
        public const string sMissingClientAccountsIgnored = "Missing Client account(s) have been ignored automatically.";
        public const string sMissingAccountsIgnored = "Missing Account number(s) have been ignored automatically.";
        public const string sErrorsCannotContinue = "There are errors in data. Import wizard could not continue! (Successful:{0}, Errors:{1}) ";
        public const string sValidationCompleted = "Validation completed. Press Next to continue. (Successful:{0}, Errors:{1})";
        public const string sImportedSuccessfully = "Imported successfully. Press Finish to complete. (Successful:{0}, Errors:{1}) ";
        public const string sImportFinish = "Import process completed. Press finish to exit. (Successful:{0}, Errors:{1}) ";
        public const string sFileTypeMismatch = "Selected file is not according to the specified type.";


    }
}