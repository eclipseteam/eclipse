﻿using System;
using System.Collections.Generic;
using System.Data;
using Oritax.TaxSimp.Data;

namespace eclipseonlineweb.Export
{
    public interface IExportable
    {
        DataSet Export();
        byte[] GetByteArray(DataSet data);
        string FileType { get; set; }
        Oritax.TaxSimp.CalculationInterface.ICMBroker Broker { get; set; }
        UserEntity User { get; set; }
        ClientManagementType ClientManagementType { get; set; }
    }
    public enum ExportType
    {
        BankWestAccountOpeningCSVExport = 1,
        StateStreetCSVExport = 2,
        AdviserDetailsCSVExport = 3,
        DesktopBrokerCSVExport = 4,
        FinSimplicityHoldingCSVExport = 5,
        FinSimplicityInvestmentCSVExport = 6,
        InstructionsExport = 7,
        BulkInstructionsExport = 8,
        StateStreetCSVExportRecon = 9,
        ExportClientDetails = 10,
    }
    public class ExportFactory
    {
        public static Dictionary<ExportType, Type> ExportCommands = new Dictionary<ExportType, Type>()
                                                       {
                                                        {ExportType.BankWestAccountOpeningCSVExport, typeof(BankWestAccountOpeningCSVExport)},  
                                                        {ExportType.StateStreetCSVExport, typeof(StateStreetCSVExport)},
                                                        {ExportType.StateStreetCSVExportRecon, typeof(StateStreetCSVExportRecon)},
                                                        {ExportType.AdviserDetailsCSVExport, typeof(AdvisorCSVExport)},
                                                        {ExportType.DesktopBrokerCSVExport, typeof(DesktopBrokerCSVExport)},
                                                        {ExportType.FinSimplicityInvestmentCSVExport, typeof(FinancialSimplicityInvestmentCSVExport)},
                                                        {ExportType.InstructionsExport, typeof(OrderPadInstructionsExport)},
                                                        {ExportType.BulkInstructionsExport, typeof(OrderPadInstructionsExport)},
                                                        {ExportType.ExportClientDetails, typeof(ClientDetailsCSVExport)},
                                                       };

        public static IExportable GetExportCommand(ExportType exportType)
        {
            if (!ExportCommands.ContainsKey(exportType)) return null;
            Type value = ExportCommands[exportType];
            return Activator.CreateInstance(value) as IExportable;
        }
    }
}
