﻿using System;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Commands;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Utilities;
using Aspose.Cells;
using System.IO;
using Aspose.Words;
using System.Web;

namespace eclipseonlineweb.Export
{
    public class ClientDetailsCSVExport : IExportable
    {

        #region IExportable Members

        public DataSet Export()
        {
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);

            var accountopeingsDs = new ExportAllClientDetailsDS
                {
                    Command = (int) WebCommands.GetAllClientDetails, Unit = new OrganizationUnit
                        {
                            CurrentUser = User,
                        }
                };
            org.GetData(accountopeingsDs);
            return accountopeingsDs;
        }

        #endregion

        #region IExportable Members


        public string FileType
        {
            get;
            set;
        }

        public ICMBroker Broker
        {
            get;
            set;
        }

        #endregion

        #region IExportable Members


        public UserEntity User{get;set;}
        public ClientManagementType ClientManagementType { get; set; }

        #endregion

        #region IExportable Members

        public byte[] GetByteArray(DataSet data)
        {
            FileType = "zip";
            string baseDirectory = HttpContext.Current.Request.PhysicalApplicationPath;
            string bankwestTemplateFullFileName = string.Empty;
            bankwestTemplateFullFileName = baseDirectory + @"Templates\ClietnDetails.xlsm";

            var destFolder = baseDirectory + @"tempReports\";
            if (!Directory.Exists(destFolder))
            {
                Directory.CreateDirectory(destFolder);
            }
            Workbook book = null;
            string destFileName = destFolder + Guid.NewGuid().ToString() + ".xlsm";
            
            File.Copy(bankwestTemplateFullFileName,
                destFileName, true);

            book = new Workbook(destFileName);
            Set_tblAccountOpeningBankWestASPOSE(data, book);
            Set_tblAccountOpeningBankWestASPOSEID(data, book);
            var ds = data as ExportAllClientDetailsDS;
            if(ds!=null)
            {
                Set_tblClientDetailsASPOSEID(ds.ClientDetailsTable, book);
                Set_tblSignatoryDetailsASPOSEID(ds.ClientSignatoryDetailsTable, book);
                Set_tblModelDetailsASPOSEID(ds.ClientModelTable, book);
            }
            book.Save(destFileName);
            var output = new SerializableDictionary<string, byte[]> {{string.Format("ClientsDetails_" + DateTime.Now.ToString("dd-MM-yy") + "_.xlsm", DateTime.Now.ToString("yyyyMMdd")), File.ReadAllBytes(destFileName)}};
            return Zipper.CreateZip(output);
        }

        private static void Set_tblAccountOpeningBankWestASPOSE(DataSet ds, Workbook book)
        {
            DataTable dt = ds.Tables["tblAccountOpeningBankWest"];
            int noofaccounts = dt.Rows.Count;
            int startRowForInsert = 10;
            int startRowForData = 10;
            int rowCount = 0;
            var accountOpeningBankWestCSVColumns = new AccountOpeningBankWestCSVColumns();

            Worksheet applicationFormWorksheet = book.Worksheets["Application Forms"];
            Cells cells = applicationFormWorksheet.Cells;

            while (noofaccounts != 0)
            {
                cells.CopyRow(cells, startRowForInsert, startRowForInsert + 1);
                noofaccounts--;
                startRowForInsert++;
            }

            noofaccounts = dt.Rows.Count;
            applicationFormWorksheet =  book.Worksheets["Application Forms"];
            cells = applicationFormWorksheet.Cells;

            while (noofaccounts != 0)
            {

                DataRow row = dt.Rows[rowCount];

                cells["A" + startRowForData.ToString()].Value = "CMT & CMA";

                cells["B" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_ApplicationID].ToString();
                //C, D, E, F are calculated
                //Based on client type 
                cells["G" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_OpenAccountApplicationApplicantType].ToString();
                //H is calculated 
                cells["I" + startRowForData.ToString()].Value = "CMT";
                //if Other Corporate Trustee or Corporate Trustee
                cells["J" + startRowForData.ToString()].Value = "FALSE";
                //Legal Entity name only for non individual objects
                cells["K" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TrusteesCompanyDetailEntityName1]; // Legal Entity Name
                cells["L" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TrusteesCompanyDetailEntityName2]; // Legal Entity Name
                cells["M" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TrusteesCompanyDetailACNABN].ToString();
                cells["N" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TrusteesCompanyDetailABN].ToString();
                cells["O" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TrusteesCompanyDetailACNARBN].ToString();
                cells["P" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailGivenName] + " " + row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailSurname];
                cells["Q" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailEmail].ToString();
                cells["R" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TrusteesCompanyDetailContactPhPrefix].ToString();
                cells["S" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TrusteesCompanyDetailContactPh].ToString();
                cells["T" + startRowForData.ToString()].Value = "Australia";
                cells["U" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TrusteesCompanyDetailTrustType].ToString();
                cells["V" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailTFN].ToString();
                cells["W" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailNRC].ToString();
                cells["X" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailExemptionCategory].ToString();
                cells["Y" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailTIN].ToString();
                cells["Z" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorpTrusteeDetailEntityName1].ToString();


                cells["AA" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorpTrusteeDetailEntityName2].ToString();
                cells["AB" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorpTrusteeDetailACNABN].ToString();
                cells["AC" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorpTrusteeDetailABN].ToString();
                cells["AD" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorpTrusteeDetailACNARBN].ToString();
                cells["AE" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorpTrusteeDetailCountryOfEstablishment].ToString();
                cells["AG" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_OpenAccountApplicationMannerOfOperation].ToString();
                cells["AH" + startRowForData.ToString()].Value = String.Empty;
                cells["AI" + startRowForData.ToString()].Value = "General";
                cells["AU" + startRowForData.ToString()].Value = "FORTNUM PRIVATE WEALTH PTY LTD";
                cells["AV" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_OpenAccountApplicationAdviserName].ToString();
                cells["AW" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_OpenAccountApplicationAdviserCode].ToString();
                cells["AX" + startRowForData.ToString()].Value = String.Empty;
                cells["AY" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_OpenAccountApplicationCustomerAccountNumber].ToString();
                cells["AZ" + startRowForData.ToString()].Value = String.Empty;
                cells["BA" + startRowForData.ToString()].Value = String.Empty;
                cells["BB" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_OpenAccountApplicationClientAccountDesignation].ToString(); ;
                cells["BC" + startRowForData.ToString()].Value = "CMT1345";
                cells["BE" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailOccupation].ToString();
                cells["BF" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailEmployer].ToString();
                cells["BG" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailCountryOfResidence].ToString();
                cells["BH" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailTitle].ToString();
                cells["BI" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailTitleOther].ToString();
                cells["BJ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailGivenName].ToString();
                cells["BK" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailSurname].ToString();
                cells["BL" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhoneWk].ToString();
                cells["BM" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhoneHm].ToString();
                cells["BN" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhoneMb].ToString();
                cells["BO" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailDOBDay].ToString();
                cells["BP" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailDOBMonth].ToString();
                cells["BQ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailDOBYear].ToString();
                cells["BR" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailEmail].ToString();
                cells["BS" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailTelephonePassword].ToString();
                cells["BT" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressAddress1].ToString();
                cells["BU" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressAddress2].ToString();
                cells["BV" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressAddress3].ToString();
                cells["BW" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressSuburb].ToString();
                cells["BX" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressState].ToString();
                cells["BY" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressPostcode].ToString();
                cells["BZ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressCountry].ToString();

                cells["CA" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressCareOf].ToString();
                cells["CB" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhoneHmPrefix].ToString();
                cells["CC" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhonewkPrefix].ToString();

                cells["CD" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailTFN_2].ToString();
                cells["CE" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailNRC_2].ToString();
                cells["CF" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailExemptionCategory_2].ToString();
                cells["CG" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailTIN_2].ToString();
                //SIG2
                cells["CH" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailOccupation_2].ToString();
                cells["CI" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailEmployer_2].ToString();
                cells["CJ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailCountryOfResidence_2].ToString();
                cells["CK" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailTitle_2].ToString();
                cells["CL" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailTitleOther_2].ToString();
                cells["CM" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailGivenName_2].ToString();
                cells["CN" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailSurname_2].ToString();
                cells["CO" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhoneWk_2].ToString();
                cells["CP" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhoneHm_2].ToString();
                cells["CQ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhoneMb_2].ToString();
                cells["CR" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailDOBDay_2].ToString();
                cells["CS" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailDOBMonth_2].ToString();
                cells["CT" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailDOBYear_2].ToString();
                cells["CU" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailEmail_2].ToString();
                cells["CV" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailTelephonePassword_2].ToString();
                cells["CW" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressAddress1_2].ToString();
                cells["CX" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressAddress2_2].ToString();
                cells["CY" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressAddress3_2].ToString();
                cells["CZ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressSuburb_2].ToString();
                cells["DA" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressState_2].ToString();
                cells["DB" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressPostcode_2].ToString();
                cells["DC" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressCountry_2].ToString();

                cells["DD" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressCareOf_2].ToString();
                cells["DE" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhoneHmPrefix_2].ToString();
                cells["DF" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhonewkPrefix_2].ToString();

                cells["DG" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailTFN_3].ToString();
                cells["DH" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailNRC_3].ToString();
                cells["DI" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailExemptionCategory_3].ToString();
                cells["DJ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailTIN_3].ToString();
                //SIG3
                cells["DK" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailOccupation_3].ToString();
                cells["DL" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailEmployer_3].ToString();
                cells["DM" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailCountryOfResidence_3].ToString();
                cells["DN" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailTitle_3].ToString();
                cells["DO" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailTitleOther_3].ToString();
                cells["DP" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailGivenName_3].ToString();
                cells["DQ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailSurname_3].ToString();
                cells["DR" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhoneWk_3].ToString();
                cells["DS" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhoneHm_3].ToString();
                cells["DT" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhoneMb_3].ToString();
                cells["DU" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailDOBDay_3].ToString();
                cells["DV" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailDOBMonth_3].ToString();
                cells["DW" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailDOBYear_3].ToString();
                cells["DX" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailEmail_3].ToString();
                cells["DY" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailTelephonePassword_3].ToString();
                cells["DZ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressAddress1_3].ToString();
                cells["EA" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressAddress2_3].ToString();
                cells["EB" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressAddress3_3].ToString();
                cells["EC" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressSuburb_3].ToString();
                cells["ED" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressState_3].ToString();
                cells["EE" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressPostcode_3].ToString();
                cells["EF" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressCountry_3].ToString();

                cells["EG" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressCareOf_3].ToString();
                cells["EH" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhoneHmPrefix_3].ToString();
                cells["EI" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhonewkPrefix_3].ToString();

                cells["EJ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailTFN_4].ToString();
                cells["EK" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailNRC_4].ToString();
                cells["EL" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailExemptionCategory_4].ToString();
                cells["EM" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailTIN_4].ToString();
                //SIG4
                cells["EN" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailOccupation_4].ToString();
                cells["EO" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailEmployer_4].ToString();
                cells["EP" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailCountryOfResidence_4].ToString();
                cells["EQ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailTitle_4].ToString();
                cells["ER" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailTitleOther_4].ToString();
                cells["ES" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailGivenName_4].ToString();
                cells["ET" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailSurname_4].ToString();
                cells["EU" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhoneWk_4].ToString();
                cells["EV" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhoneHm_4].ToString();
                cells["EW" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhoneMb_4].ToString();
                cells["EX" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailDOBDay_4].ToString();
                cells["EY" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailDOBMonth_4].ToString();
                cells["EZ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailDOBYear_4].ToString();
                cells["FA" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailEmail_4].ToString();
                cells["FB" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailTelephonePassword_4].ToString();
                cells["FC" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressAddress1_4].ToString();
                cells["FD" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressAddress2_4].ToString();
                cells["FE" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressAddress3_4].ToString();
                cells["FF" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressSuburb_4].ToString();
                cells["FG" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressState_4].ToString();
                cells["FH" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressPostcode_4].ToString();
                cells["FI" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressCountry_4].ToString();

                cells["FJ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_AddressCareOf_4].ToString();
                cells["FK" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhoneHmPrefix_4].ToString();
                cells["FL" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_SignatoryPersonalDetailPhonewkPrefix_4].ToString();

                cells["FM" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailTFN_5].ToString();
                cells["FN" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailNRC_5].ToString();
                cells["FO" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailExemptionCategory_5].ToString();
                cells["FP" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_TFNDetailTIN_5].ToString();

                //Rest of details Correspondence Addresses

                cells["FQ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressAddress1].ToString();
                cells["FR" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressAddress2].ToString();
                cells["FS" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressAddress3].ToString();
                cells["FT" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressSuburb].ToString();
                cells["FU" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressState].ToString();
                cells["FV" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressPostcode].ToString();
                cells["FW" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressCountry].ToString();
                cells["FX" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressCareOf].ToString();

                cells["FY" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressAddress1_2].ToString();
                cells["FZ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressAddress2_2].ToString();
                cells["GA" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressAddress3_2].ToString();
                cells["GB" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressSuburb_2].ToString();
                cells["GC" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressState_2].ToString();
                cells["GD" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressPostcode_2].ToString();
                cells["GE" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressCountry_2].ToString();
                cells["GF" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressCareOf_2].ToString();

                cells["GG" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressAddress1_3].ToString();
                cells["GH" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressAddress2_3].ToString();
                cells["GI" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressAddress3_3].ToString();
                cells["GJ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressSuburb_3].ToString();
                cells["GK" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressState_3].ToString();
                cells["GL" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressPostcode_3].ToString();
                cells["GM" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressCountry_3].ToString();
                cells["GN" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_CorrespondenceAddressCareOf_3].ToString();
                cells["HE" + startRowForData.ToString()].Value = "e-Clipse Online";
                cells["HF" + startRowForData.ToString()].Value = "IO";
                cells["HI" + startRowForData.ToString()].Value = "VisiPlan";
                cells["HJ" + startRowForData.ToString()].Value = "VP";
                cells["IA" + startRowForData.ToString()].Value = "BWACMA";
                rowCount++;
                startRowForData++;
                noofaccounts--;
            }

            noofaccounts = dt.Rows.Count;
            applicationFormWorksheet =  book.Worksheets["Application Forms"];
            applicationFormWorksheet.Cells.Rows.RemoveAt(startRowForInsert);
            applicationFormWorksheet.Cells.Rows.RemoveAt(startRowForInsert - 1);
        }
        private static void Set_tblAccountOpeningBankWestASPOSEID(DataSet ds, Workbook book)
        {
            DataTable dt = ds.Tables["tblAccountOpeningBankWestID"];
            int noofaccounts = dt.Rows.Count;
            int startRowForInsert = 8;
            int startRowForData = 8;
            int rowCount = 0;
            AccountOpeningBankWestIDCSVColumns accountOpeningBankWestCSVColumns = new AccountOpeningBankWestIDCSVColumns();

            Worksheet applicationFormWorksheet = book.Worksheets["CIP"];
            Cells cells = applicationFormWorksheet.Cells;

            while (noofaccounts != 0)
            {
                cells.CopyRow(cells, startRowForInsert, startRowForInsert + 1);
                noofaccounts--;
                startRowForInsert++;
            }

            noofaccounts = dt.Rows.Count;
            applicationFormWorksheet =  book.Worksheets["CIP"];
            cells = applicationFormWorksheet.Cells;

            while (noofaccounts != 0)
            {

                DataRow row = dt.Rows[rowCount];

                cells["A" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_ApplicationID].ToString(); 
                cells["F" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationTitle].ToString();
                if(row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationTitle].ToString() == "Other")
                    cells["G" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationTitleOther].ToString();
                
                cells["H" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationSurname].ToString();
                cells["I" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationGivenName].ToString();
                cells["J" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationDOBDay];
                cells["K" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationDOBMonth].ToString();
                cells["L" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationDOBYear].ToString();
                cells["N" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationPartnerCode].ToString();

                cells["R" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationOccupation].ToString();
                cells["S" + startRowForData.ToString()].Value = "Australia";
                cells["T" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationResidentialAddress].ToString();
                cells["U" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_ResidentialAddressSuburb].ToString();
                cells["V" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_ResidentialAddressState].ToString();
                cells["W" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_ResidentialAddressPostcode].ToString();
                cells["X" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationMailingAddress].ToString();
                cells["Y" + startRowForData.ToString()].Value = String.Empty;//row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationSurname].ToString();
                cells["Z" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_MailingAddressSuburb].ToString();
                cells["AA" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_MailingAddressState].ToString();
                cells["AB" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_MailingAddressPostcode].ToString();
    
                cells["AC" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationHomePh].ToString();
                cells["AD" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationBusPh].ToString();
                cells["AE" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationMobilePh].ToString();
               
                cells["AJ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentTypeOfDoc].ToString();
                cells["AK" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentFullName].ToString();
                cells["AL" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentDOBDay].ToString();
                cells["AM" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentDOBMonth].ToString();
                cells["AN" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentDOBYear].ToString();
                cells["AO" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentDocNumber].ToString();
                cells["AP" + startRowForData.ToString()].Value = "Yes";//row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationSurname].ToString();
                cells["AQ" + startRowForData.ToString()].Value = "Yes";//row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationSurname].ToString();
                cells["AR" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentDateOfIssueDay].ToString();
                cells["AS" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentDateOfIssueMonth].ToString();
                cells["AT" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentDateOfIssueYear].ToString();
               
                cells["AU" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentExpDateDay].ToString();
                cells["Av" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentExpDateMonth].ToString();
                cells["AW" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentExpDateYear].ToString();
                cells["AX" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentIssuePlace].ToString();

                cells["BN" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentTypeOfDoc_2].ToString();
                cells["BO" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentFullName_2].ToString();
                cells["BP" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentDOBDay_2].ToString();
                cells["BQ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentDOBMonth_2].ToString();
                cells["BR" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentDOBYear_2].ToString();
                cells["BS" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentDocNumber_2].ToString();
                cells["BT" + startRowForData.ToString()].Value = "Yes"; //row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationSurname].ToString();
                cells["BU" + startRowForData.ToString()].Value = "Yes"; //row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationSurname].ToString();
                cells["BV" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentDateOfIssueDay_2].ToString();
                cells["BW" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentDateOfIssueMonth_2].ToString();
                cells["BX" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentDateOfIssueYear_2].ToString();
                cells["BY" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentExpDateDay_2].ToString();
                cells["BZ" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentExpDateMonth_2].ToString();
                
                cells["CA" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentExpDateYear_2].ToString();
                cells["CB" + startRowForData.ToString()].Value = row[accountOpeningBankWestCSVColumns.Col_IDDocumentIssuePlace_2].ToString();
                cells["CC" + startRowForData.ToString()].Value = "Australia";// row[accountOpeningBankWestCSVColumns.Col_HundredPointApplicationSurname].ToString();
                rowCount++;
                startRowForData++;
                noofaccounts--;
            }

            noofaccounts = dt.Rows.Count;
            applicationFormWorksheet =  book.Worksheets["CIP"];
            applicationFormWorksheet.Cells.Rows.RemoveAt(startRowForInsert);
            applicationFormWorksheet.Cells.Rows.RemoveAt(startRowForInsert - 1);
        }

        private static void Set_tblClientDetailsASPOSEID(ClientDetailsTable dt, Workbook book)
        {
            int startRowForData = 7;
            Worksheet applicationFormWorksheet = book.Worksheets["Clients Details"];
            Cells cells = applicationFormWorksheet.Cells;

            foreach (DataRow row in dt.Rows)
            {
                cells["A" + startRowForData.ToString()].Value = row[dt.ClientCode].ToString();
                cells["B" + startRowForData.ToString()].Value = row[dt.ClientName].ToString();
                cells["C" + startRowForData.ToString()].Value = row[dt.TFN].ToString();
                cells["D" + startRowForData.ToString()].Value = row[dt.ABN].ToString();
                cells["E" + startRowForData.ToString()].Value = row[dt.Adviser].ToString();
                cells["F" + startRowForData.ToString()].Value = row[dt.InvestorStatus].ToString();
                cells["G" + startRowForData.ToString()].Value = row[dt.LimitedPowerofAttorney].ToString();

                cells["H" + startRowForData.ToString()].Value = row[dt.BAddressLine1].ToString();
                cells["I" + startRowForData.ToString()].Value = row[dt.BAddressLine2].ToString();
                cells["J" + startRowForData.ToString()].Value = row[dt.BSubRub].ToString();
                cells["K" + startRowForData.ToString()].Value = row[dt.BPostCode].ToString();
                cells["L" + startRowForData.ToString()].Value = row[dt.BState].ToString();
                cells["M" + startRowForData.ToString()].Value = row[dt.BCountry].ToString();
               
                cells["N" + startRowForData.ToString()].Value = row[dt.RAddressLine1].ToString();
                cells["O" + startRowForData.ToString()].Value = row[dt.RAddressLine2].ToString();
                cells["P" + startRowForData.ToString()].Value = row[dt.RSubRub].ToString();
                cells["Q" + startRowForData.ToString()].Value = row[dt.RPostCode].ToString();
                cells["R" + startRowForData.ToString()].Value = row[dt.RState].ToString();
                cells["S" + startRowForData.ToString()].Value = row[dt.RCountry].ToString();

                cells["T" + startRowForData.ToString()].Value = row[dt.RIAddressLine1].ToString();
                cells["U" + startRowForData.ToString()].Value = row[dt.RIAddressLine2].ToString();
                cells["V" + startRowForData.ToString()].Value = row[dt.RISubRub].ToString();
                cells["W" + startRowForData.ToString()].Value = row[dt.RIPostCode].ToString();
                cells["X" + startRowForData.ToString()].Value = row[dt.RIState].ToString();
                cells["Y" + startRowForData.ToString()].Value = row[dt.RICountry].ToString();

                cells["Z" + startRowForData.ToString()].Value = row[dt.MAddressLine1].ToString();
                cells["AA" + startRowForData.ToString()].Value = row[dt.MAddressLine2].ToString();
                cells["AB" + startRowForData.ToString()].Value = row[dt.MSubRub].ToString();
                cells["AC" + startRowForData.ToString()].Value = row[dt.MPostCode].ToString();
                cells["AD" + startRowForData.ToString()].Value = row[dt.MState].ToString();
                cells["AE" + startRowForData.ToString()].Value = row[dt.MCountry].ToString();


                cells["AF" + startRowForData.ToString()].Value = row[dt.DMAddressLine1].ToString();
                cells["AG" + startRowForData.ToString()].Value = row[dt.DMAddressLine2].ToString();
                cells["AH" + startRowForData.ToString()].Value = row[dt.DMSubRub].ToString();
                cells["AI" + startRowForData.ToString()].Value = row[dt.DMPostCode].ToString();
                cells["AJ" + startRowForData.ToString()].Value = row[dt.DMState].ToString();
                cells["AK" + startRowForData.ToString()].Value = row[dt.DMCountry].ToString();

                cells["AL" + startRowForData.ToString()].Value = row[dt.CRAddressLine1].ToString();
                cells["AM" + startRowForData.ToString()].Value = row[dt.CRAddressLine2].ToString();
                cells["AN" + startRowForData.ToString()].Value = row[dt.CRSubRub].ToString();
                cells["AN" + startRowForData.ToString()].Value = row[dt.CRPostCode].ToString();
                cells["AP" + startRowForData.ToString()].Value = row[dt.CRState].ToString();
                cells["AQ" + startRowForData.ToString()].Value = row[dt.CRCountry].ToString();

                startRowForData++;
            }

            
        }


        private static void Set_tblSignatoryDetailsASPOSEID(ClientSignatoryDetailsTable dt, Workbook book)
        {
            int startRowForData = 7;
            Worksheet applicationFormWorksheet = book.Worksheets["Client Signatories"];
            Cells cells = applicationFormWorksheet.Cells;
            foreach (DataRow row in dt.Rows)
            {
                cells["A" + startRowForData.ToString()].Value = row[dt.ClientCode].ToString();
                cells["B" + startRowForData.ToString()].Value = row[dt.SignatoryName].ToString();
                cells["C" + startRowForData.ToString()].Value = row[dt.TFN].ToString();
                cells["D" + startRowForData.ToString()].Value = row[dt.Email].ToString();
                cells["E" + startRowForData.ToString()].Value = row[dt.Phone].ToString();
                cells["F" + startRowForData.ToString()].Value = row[dt.WorkPhone].ToString();
                cells["G" + startRowForData.ToString()].Value = row[dt.MobilePhone].ToString();

                cells["H" + startRowForData.ToString()].Value = row[dt.RAddressLine1].ToString();
                cells["I" + startRowForData.ToString()].Value = row[dt.RAddressLine2].ToString();
                cells["J" + startRowForData.ToString()].Value = row[dt.RSubRub].ToString();
                cells["K" + startRowForData.ToString()].Value = row[dt.RPostCode].ToString();
                cells["L" + startRowForData.ToString()].Value = row[dt.RState].ToString();
                cells["M" + startRowForData.ToString()].Value = row[dt.RCountry].ToString();

                cells["N" + startRowForData.ToString()].Value = row[dt.MAddressLine1].ToString();
                cells["O" + startRowForData.ToString()].Value = row[dt.MAddressLine2].ToString();
                cells["P" + startRowForData.ToString()].Value = row[dt.MSubRub].ToString();
                cells["Q" + startRowForData.ToString()].Value = row[dt.MPostCode].ToString();
                cells["R" + startRowForData.ToString()].Value = row[dt.MState].ToString();
                cells["S" + startRowForData.ToString()].Value = row[dt.MCountry].ToString();
               
                startRowForData++;
               
            }

            
        }
        private static void Set_tblModelDetailsASPOSEID(ClientModelTable dt, Workbook book)
        {
            
            int startRowForData = 7;
            Worksheet applicationFormWorksheet = book.Worksheets["Client Models"];
            Cells cells = applicationFormWorksheet.Cells;

            foreach (DataRow row in dt.Rows)
            {
                cells["A" + startRowForData.ToString()].Value = row[dt.ClientCode].ToString();
                cells["B" + startRowForData.ToString()].Value = row[dt.ModelName].ToString();
                cells["C" + startRowForData.ToString()].Value = row[dt.ModelCode].ToString();
                startRowForData++;
            }

        }
        #endregion
    }
}