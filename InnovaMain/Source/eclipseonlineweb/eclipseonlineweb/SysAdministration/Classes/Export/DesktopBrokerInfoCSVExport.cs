﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Commands;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Utilities;


namespace eclipseonlineweb.Export
{
    public class DesktopBrokerCSVExport : IExportable
    {

        #region IExportable Members

        public DataSet Export()
        {
           
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);

            ExportDS accountopeingsDs = new ExportDS();
            accountopeingsDs.Command = (int)WebCommands.ExportDesktopBrokerInfo;
            accountopeingsDs.Unit = new Oritax.TaxSimp.Data.OrganizationUnit
            {
                CurrentUser = User,
                
            };
            org.GetData(accountopeingsDs);
            
            Broker.ReleaseBrokerManagedComponent(org);

            return accountopeingsDs;
        }

        #endregion

        #region IExportable Members


        public string FileType
        {get;set;}


        public Oritax.TaxSimp.CalculationInterface.ICMBroker Broker
        {
            get;
            set;
        }

        #endregion

        #region IExportable Members


        public Oritax.TaxSimp.Data.UserEntity User{get;set;}
        public ClientManagementType ClientManagementType { get; set; }

        #endregion

        #region IExportable Members


  
        #region Excel Writing Code
  
        public byte[] GetByteArray(DataSet Data)
        {
            FileType = "zip";
            string data=Data.Tables[0].Rows[0][0].ToString();
            byte[] bytes = System.Convert.FromBase64String(data);
            return bytes;
        }

      
        #endregion
        #endregion
    }
}