﻿using System;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Commands;
using eclipseonlineweb.WebUtilities;
using System.Text;

namespace eclipseonlineweb.Export
{
    public class FinancialSimplicityInvestmentCSVExport : IExportable
    {

        #region IExportable Members
        string TableName = "FinInvestment";
        public System.Data.DataSet Export()
        {
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);

            ExportDS accountopeingsDs = new ExportDS();
            accountopeingsDs.Command = (int)WebCommands.ExportFinSimplicityInvestment;
            accountopeingsDs.Unit = new Oritax.TaxSimp.Data.OrganizationUnit
            {
                CurrentUser = User,
                
            };
            org.GetData(accountopeingsDs);
            
            Broker.ReleaseBrokerManagedComponent(org);

            return accountopeingsDs;
        }

        public byte[] GetByteArray(System.Data.DataSet data)
        {         
            FileType = "csv";
            string sbFinSimplicityInvestment = string.Empty;
            sbFinSimplicityInvestment = Utilities.Datatable_to_csv(data.Tables[TableName], "FinancialSimplicityInvestment_" + DateTime.Now.ToString("dd-MM-yy") + "_.csv");
            byte[] bytes = Encoding.Unicode.GetBytes(sbFinSimplicityInvestment);
            return bytes;        
        }

        public string FileType
        {get;set;}
        

        public ICMBroker Broker
        {
            get;
            set;
        }

        public Oritax.TaxSimp.Data.UserEntity User{get;set;}
        public ClientManagementType ClientManagementType { get; set; }

        #endregion
    }
}