﻿using System;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Commands;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Data;
using System.Text;

namespace eclipseonlineweb.Export
{
    public class StateStreetCSVExport : IExportable
    {
        #region IExportable Members

        public System.Data.DataSet Export()
        {
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);

            ExportDS accountopeingsDs = new ExportDS();
            accountopeingsDs.Command = (int)WebCommands.ExportStateStreet;
            accountopeingsDs.Unit = new OrganizationUnit
            {
                CurrentUser = User,
                
            };
            org.GetData(accountopeingsDs);
            
              return accountopeingsDs;
        }

        public byte[] GetByteArray(System.Data.DataSet data)
        {         
            FileType = "csv";
            string sbStateStreet = string.Empty;            
            AOIDCSVColumns obj = new AOIDCSVColumns();
            sbStateStreet = Utilities.Datatable_to_csv(data.Tables[obj.Table.TableName], "NewAccount" + DateTime.Now.ToString("yyyyMMdd") + ".csv");

            byte[] bytes = Encoding.Unicode.GetBytes(sbStateStreet);
            return bytes;        
        }

        public string FileType
        {get;set;}
        

        public ICMBroker Broker
        {
            get;
            set;
        }

        public UserEntity User{get;set;}
        public ClientManagementType ClientManagementType { get; set; }

        #endregion
    }

    public class StateStreetCSVExportRecon : IExportable
    {
        #region IExportable Members

        public System.Data.DataSet Export()
        {
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);

            ExportDS accountopeingsDs = new ExportDS();
            accountopeingsDs.Command = (int)WebCommands.ExportStateStreetRecon;
            accountopeingsDs.Unit = new OrganizationUnit
            {
                CurrentUser = User,

            };
            org.GetData(accountopeingsDs);

            Broker.ReleaseBrokerManagedComponent(org);

            return accountopeingsDs;
        }

        public byte[] GetByteArray(System.Data.DataSet data)
        {
            FileType = "csv";
            string sbStateStreet = string.Empty;
            AOIDCSVColumnsRecon obj = new AOIDCSVColumnsRecon();
            sbStateStreet = Utilities.Datatable_to_csv(data.Tables[obj.Table.TableName], "NewAccount" + DateTime.Now.ToString("yyyyMMdd") + ".csv");

            byte[] bytes = Encoding.Unicode.GetBytes(sbStateStreet);
            return bytes;
        }

        public string FileType
        { get; set; }


        public ICMBroker Broker
        {
            get;
            set;
        }

        public UserEntity User { get; set; }
        public ClientManagementType ClientManagementType { get; set; }

        #endregion
    }
}