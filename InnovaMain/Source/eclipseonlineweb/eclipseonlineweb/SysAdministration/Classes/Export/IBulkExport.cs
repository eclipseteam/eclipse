﻿using System.Data;

namespace eclipseonlineweb.Export
{
    public interface IBulkExport
    {
        DataSet ExportBulk();
    }
}
