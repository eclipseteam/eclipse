﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Web;
using Aspose.Words;
using Aspose.Words.Tables;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Utilities;
using SpreadsheetGear.Data;
using eclipseonlineweb.WebUtilities;
using System.Drawing;
using Oritax.TaxSimp.Data;
using SpreadsheetGear;
using Document = Aspose.Words.Document;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;

namespace eclipseonlineweb.Export
{
    public interface IHasOrdersFilter
    {
        System.Collections.Generic.List<Guid> OrdersContianingOnly { get; set; }
        string TradeDate { get; set; }
    }
    public class OrderPadInstructionsExport : IExportable, IBulkExport, IHasOrdersFilter
    {

        #region IExportable Members
        string TDFiiGBulkTableName = "TDFiiGBulkExport";
        string TDAMMBulkTableName = "TDAMMBulkExport";
        string ASXBulkTableName = "ASXBulkExport";
        string ASXTableName = "ASXExport";
        string StateStreetTableName = "StateStreetExport";
        string BankWestTableNameForStateStreet = "BankWestExportForStateStreet";
        string BankWestTableNameForP2 = "BankWestExportForP2";
        string BankWestTable = "BankWestExport";
        string MacquarieTableNameForStateStreet = "MacquarieExportForStateStreet";
        string MacquarieTableNameForP2 = "MacquarieExportForP2";
        string MacquarieTable = "MacquarieExport";
        string MacquarietAtCallTable = "MacquarieAtCallExport";

        string batchNoForSS = string.Empty;


        public DataSet Export()
        {
            ExportDS exportInstructionDS = new ExportDS();

            if (OrdersContianingOnly != null && OrdersContianingOnly.Count > 0)
            {
                exportInstructionDS.ExtendedProperties["OrdersFilter"] = OrdersContianingOnly;
            }

            if (!string.IsNullOrEmpty(TradeDate))
            {
                exportInstructionDS.ExtendedProperties["TradeDate"] = TradeDate;
            }

            Guid orderCID = IsOrderCmExists();
            if (orderCID != Guid.Empty)
            {
                exportInstructionDS.Unit = new OrganizationUnit
                {
                    CurrentUser = User,
                };
                exportInstructionDS.ClientManagementType = ClientManagementType;
                Broker.SaveOverride = true;
                var orderCM = Broker.GetBMCInstance(orderCID);
                orderCM.SetData(exportInstructionDS);
                Broker.SetComplete();
                Broker.SetStart();
            }
            return exportInstructionDS;
        }

        public DataSet ExportBulk()
        {
            var exportInstructionDS = new ExportDS();

            if (OrdersContianingOnly != null && OrdersContianingOnly.Count > 0)
            {
                exportInstructionDS.ExtendedProperties["BulkOrdersFilter"] = OrdersContianingOnly;
            }
            var orderCID = IsOrderCmExists();
            if (orderCID != Guid.Empty)
            {
                exportInstructionDS.Unit = new OrganizationUnit
                {
                    CurrentUser = User,
                };
                exportInstructionDS.ExtendedProperties.Add("IsBulk", true);
                exportInstructionDS.ClientManagementType = ClientManagementType;
                Broker.SaveOverride = true;
                var orderCM = Broker.GetBMCInstance(orderCID);
                orderCM.SetData(exportInstructionDS);
                Broker.SetComplete();
                Broker.SetStart();
            }
            return exportInstructionDS;
        }
        private Guid IsOrderCmExists()
        {
            var unit = new OrganizationUnit
            {
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = User
            };

            var ds = new OrderPadDS
            {
                CommandType = DatasetCommandTypes.Check,
                Unit = unit,
                Command = (int)WebCommands.GetOrganizationUnitsByType
            };

            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            org.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(org);

            Guid orderCMCID = Guid.Empty;
            if (ds.OrdersTable.Rows.Count > 0)
            {
                //Getting ORDER CID 
                orderCMCID = new Guid(ds.OrdersTable.Rows[0][ds.OrdersTable.CID].ToString());
            }

            return orderCMCID;
        }
        public byte[] GetByteArray(DataSet data)
        {
            FileType = "zip";
            string sbStateStreet = string.Empty;
            string sbStateStreetForP2 = string.Empty;
            string sbDesktop = string.Empty;
            //batchNoForSS = "0" + data.ExtendedProperties["BatchNo"];
            if (data.ExtendedProperties["SequanceNumber"] != null)
            {
                batchNoForSS = data.ExtendedProperties["SequanceNumber"].ToString();
            }
            IWorkbook bankWestWorkBookForSS = Factory.GetWorkbook();
            IWorkbook bankWestWorkBookForP2 = Factory.GetWorkbook();
            IWorkbook bankWestWorkBook = Factory.GetWorkbook();

            SerializableDictionary<string, byte[]> output = new SerializableDictionary<string, byte[]>();
            if (data.Tables[StateStreetTableName].Rows.Count > 0)
            {
                sbStateStreet = Utilities.Datatable_to_csv(data.Tables[StateStreetTableName], "StateStreet-PendingTransactions" + DateTime.Now.ToString("yyyyMMdd") + "-" + batchNoForSS + ".csv");
                output.Add(string.Format("StateStreet-PendingTransactions{0}-{1}.csv", DateTime.Now.ToString("yyyyMMdd"), batchNoForSS), Utilities.StrToByteArray(sbStateStreet));
            }
            if (data.Tables["StateStreetForP2Export"].Rows.Count > 0)
            {
                sbStateStreetForP2 = Utilities.Datatable_to_csv(data.Tables["StateStreetForP2Export"], "StateStreet-PendingTransactions" + DateTime.Now.ToString("yyyyMMdd") + "-" + batchNoForSS + ".csv");
                output.Add(string.Format("StateStreet-PendingTransactions{0}-{1}-EPPA.csv", DateTime.Now.ToString("yyyyMMdd"), batchNoForSS), Utilities.StrToByteArray(sbStateStreetForP2));
            }
            if (data.Tables[ASXTableName].Rows.Count > 0)
            {
                sbDesktop = Utilities.Datatable_to_csv(data.Tables[ASXTableName], "ASX_BulkTradeExport_" + DateTime.Now.ToString("yyyyMMdd") + ".csv");
                output.Add(string.Format("ASX_BulkTradeExport_{0}.csv", DateTime.Now.ToString("yyyyMMdd")), Utilities.StrToByteArray(sbDesktop));
            }
            if (data.Tables[BankWestTableNameForStateStreet].Rows.Count > 0)
            {
                //output.Add(string.Format("Bankwest-FundTransfer_Rebalance_" + DateTime.Now.ToString("ddMMyyyy") + "_SS.xls", DateTime.Now.ToString("yyyyMMdd")), FormatBankWestExcelFile(bankWestWorkBookForSS, data.Tables[BankWestTableNameForStateStreet]).SaveToMemory(FileFormat.Excel8));
                //output.Add(string.Format("Bankwest-FundTransfer_Rebalance_" + DateTime.Now.ToString("ddMMyyyy") + "_SS.xls", DateTime.Now.ToString("yyyyMMdd")), CreateBankWestInstructions(data.Tables[BankWestTableNameForStateStreet]).SaveToMemory(FileFormat.Excel8));
                output.Add(string.Format("BWA-FundTransfer_Rebalance_{0}_INAP.xls", DateTime.Now.ToString("yyyyMMdd")), CreateBankWestInstructions(data.Tables[BankWestTableNameForStateStreet]).SaveToMemory(FileFormat.Excel8));
            }
            if (data.Tables[BankWestTableNameForP2].Rows.Count > 0)
            {
                //output.Add(string.Format("Bankwest-FundTransfer_Rebalance_" + DateTime.Now.ToString("ddMMyyyy") + "_P2.xls", DateTime.Now.ToString("yyyyMMdd")), FormatBankWestExcelFile(bankWestWorkBookForP2, data.Tables[BankWestTableNameForP2]).SaveToMemory(FileFormat.Excel8));
                //output.Add(string.Format("Bankwest-FundTransfer_Rebalance_" + DateTime.Now.ToString("ddMMyyyy") + "_P2.xls", DateTime.Now.ToString("yyyyMMdd")), CreateBankWestInstructions(data.Tables[BankWestTableNameForP2]).SaveToMemory(FileFormat.Excel8));
                output.Add(string.Format("BWA-FundTransfer_Rebalance_{0}_EPPA.xls", DateTime.Now.ToString("yyyyMMdd")), CreateBankWestInstructions(data.Tables[BankWestTableNameForP2]).SaveToMemory(FileFormat.Excel8));
            }
            if (data.Tables[BankWestTable].Rows.Count > 0)
            {
                //output.Add(string.Format("Bankwest-FundTransfer_Rebalance_" + DateTime.Now.ToString("ddMMyyyy") + ".xls", DateTime.Now.ToString("yyyyMMdd")), FormatBankWestExcelFile(bankWestWorkBook, data.Tables[BankWestTable]).SaveToMemory(FileFormat.Excel8));
                output.Add(string.Format("Bankwest-FundTransfer_Rebalance_{0}.xls", DateTime.Now.ToString("yyyyMMdd")), CreateBankWestInstructions(data.Tables[BankWestTable]).SaveToMemory(FileFormat.Excel8));
            }
            if (data.Tables[MacquarieTableNameForStateStreet].Rows.Count > 0)
            {
                output.Add(string.Format("MAC-FundTransfer_Rebalance_{0}_INAP.csv", DateTime.Now.ToString("yyyyMMdd")), CreateMacquarieInstructions(data.Tables[MacquarieTableNameForStateStreet]).SaveToMemory(FileFormat.CSV));
            }
            if (data.Tables[MacquarieTableNameForP2].Rows.Count > 0)
            {
                output.Add(string.Format("MAC-FundTransfer_Rebalance_{0}_EPPA.csv", DateTime.Now.ToString("yyyyMMdd")), CreateMacquarieInstructions(data.Tables[MacquarieTableNameForP2]).SaveToMemory(FileFormat.CSV));
            }
            if (data.Tables[MacquarieTable].Rows.Count > 0)
            {
                output.Add(string.Format("MAC-FundTransfer_Rebalance_{0}.csv", DateTime.Now.ToString("yyyyMMdd")), CreateMacquarieInstructions(data.Tables[MacquarieTable]).SaveToMemory(FileFormat.CSV));
            }
            if (data.Tables["TDFiiGExport"].Rows.Count > 0)
            {
                MailMessage mailmsg = CreateEmailForTDInstructions(data.Tables["TDFiiGExport"], "FiiG");
                if (mailmsg != null)
                {
                    MemoryStream mrp = new MemoryStream();
                    MailMessageExt.Save(mailmsg, mrp);
                    int bytesRead = 0;
                    byte[] buffer = new byte[8096];
                    while ((bytesRead = mrp.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        mrp.Write(buffer, 0, bytesRead);
                    }
                    string file = "TDInstructions to FiiG_" + DateTime.Now.ToString("yyyyMMdd") + ".eml";
                    output.Add(file, mrp.ToArray());
                }
            }
            if (data.Tables["TDAMMExport"].Rows.Count > 0)
            {
                MailMessage mailmsg = CreateEmailForTDInstructions(data.Tables["TDAMMExport"], "AMM");
                if (mailmsg != null)
                {
                    MemoryStream mrp = new MemoryStream();
                    MailMessageExt.Save(mailmsg, mrp);
                    int bytesRead = 0;
                    byte[] buffer = new byte[8096];
                    while ((bytesRead = mrp.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        mrp.Write(buffer, 0, bytesRead);
                    }
                    string file = "TDInstructions to AMM_" + DateTime.Now.ToString("yyyyMMdd") + ".eml";
                    output.Add(file, mrp.ToArray());
                }
            }
            if (data.Tables["TDOtherExport"].Rows.Count > 0)
            {
                DataTable dt = data.Tables["TDOtherExport"].DefaultView.ToTable(true, "Institution");
                foreach (DataRow dr in dt.Rows)
                {
                    DataRow[] drs = data.Tables["TDOtherExport"].Select("Institution=" + "'" + dr["Institution"] + "'");
                    MailMessage mailmsg = CreateEmailForTDInstructions(drs, "Other");
                    if (mailmsg != null)
                    {
                        MemoryStream mrp = new MemoryStream();
                        MailMessageExt.Save(mailmsg, mrp);
                        int bytesRead = 0;
                        byte[] buffer = new byte[8096];
                        while ((bytesRead = mrp.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            mrp.Write(buffer, 0, bytesRead);
                        }
                        string file = "TDInstructions to " + dr["Institution"] + "_" + DateTime.Now.ToString("yyyyMMdd") + ".eml";
                        output.Add(file, mrp.ToArray());
                    }
                }
            }
            if (data.Tables["AtCallExport"].Rows.Count > 0)
            {

                foreach (DataRow dr in data.Tables["AtCallExport"].Rows)
                {
                    if (dr["OrderBankAccountType"].ToString() == OrderBankAccountType.External.ToString())
                    {
                        continue;
                    }
                    Document doc = new Document();
                    doc = SendAtCallInstructions(dr);
                    string fileName = string.Format("BWA-Instruction_Transfer of Funds {0}_{1}", DateTime.Now.ToString("yyyyMMdd"), dr["Client"]);

                    string ext = ".doc";

                    MemoryStream ms = new MemoryStream();
                    doc.Save(ms, SaveFormat.Docx);
                    string key = fileName + ext;
                    int count = 1;
                    while (output.ContainsKey(key))
                    {
                        key = fileName + "_" + (count++) + ext;
                    }

                    output.Add(key, ms.ToArray());
                }
            }
            //Creating instruction for Mac AtCall Mony movement
            if (data.Tables[MacquarietAtCallTable].Rows.Count > 0)
            {
                var macDt = data.Tables[MacquarietAtCallTable];
                DataRow[] foundRows = macDt.Select(string.Format("OrderBankAccountType = '{0}'", OrderBankAccountType.External));
                foreach (var row in foundRows)
                {
                    row.Delete();
                }
                output.Add(string.Format("MAC-Instruction_Transfer of Funds {0}.csv", DateTime.Now.ToString("yyyyMMdd")), CreateMacquarieInstructions(macDt).SaveToMemory(FileFormat.CSV));

            }
            if (data.Tables["AtCallFiiGExport"].Rows.Count > 0)
            {
                //For Buy
                var dv = data.Tables["AtCallFiiGExport"].DefaultView;
                dv.RowFilter = string.Format("OrderItemType = '{0}'", Oritax.TaxSimp.Common.OrderItemType.Buy);
                var dt = dv.ToTable();
                if (dt.Rows.Count > 0)
                {
                    CreateAtCallInstructions(output, dt, "FiiG", Oritax.TaxSimp.Common.OrderItemType.Buy);
                }
                //For Sell 
                dv = data.Tables["AtCallFiiGExport"].DefaultView;
                dv.RowFilter = string.Format("OrderItemType = '{0}'", Oritax.TaxSimp.Common.OrderItemType.Sell);
                dt = dv.ToTable();
                if (dt.Rows.Count > 0)
                {
                    CreateAtCallInstructions(output, dt, "FiiG", Oritax.TaxSimp.Common.OrderItemType.Sell);
                }
            }
            if (data.Tables["AtCallAMMExport"].Rows.Count > 0)
            {
                //For Buy
                var dv = data.Tables["AtCallAMMExport"].DefaultView;
                dv.RowFilter = string.Format("OrderItemType = '{0}'", Oritax.TaxSimp.Common.OrderItemType.Buy);
                var dt = dv.ToTable();
                if (dt.Rows.Count > 0)
                {
                    CreateAtCallInstructions(output, dt, "AMM", Oritax.TaxSimp.Common.OrderItemType.Buy);
                }
                //For Sell 
                dv = data.Tables["AtCallAMMExport"].DefaultView;
                dv.RowFilter = string.Format("OrderItemType = '{0}'", Oritax.TaxSimp.Common.OrderItemType.Sell);
                dt = dv.ToTable();
                if (dt.Rows.Count > 0)
                {
                    CreateAtCallInstructions(output, dt, "AMM", Oritax.TaxSimp.Common.OrderItemType.Sell);
                }
            }
            if (data.Tables[ASXBulkTableName].Rows.Count > 0)
            {
                sbDesktop = Utilities.Datatable_to_csv(data.Tables[ASXBulkTableName], "ASX_BulkOrdersExport_" + DateTime.Now.ToString("yyyyMMdd") + ".csv");
                output.Add(string.Format("ASX_BulkOrdersExport_" + DateTime.Now.ToString("yyyyMMdd") + ".csv"), Utilities.StrToByteArray(sbDesktop));
            }
            if (data.Tables[TDFiiGBulkTableName].Rows.Count > 0)
            {
                MailMessage mailmsg = CreateEmailForBulkTDInstructions(data.Tables[TDFiiGBulkTableName], "FiiG");
                if (mailmsg != null)
                {
                    MemoryStream mrp = new MemoryStream();
                    MailMessageExt.Save(mailmsg, mrp);
                    int bytesRead = 0;
                    byte[] buffer = new byte[8096];
                    while ((bytesRead = mrp.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        mrp.Write(buffer, 0, bytesRead);
                    }
                    string file = "Bulk_TDInstructions to FiiG_" + DateTime.Now.ToString("yyyyMMdd") + ".eml";
                    output.Add(file, mrp.ToArray());
                }
            }
            if (data.Tables[TDAMMBulkTableName].Rows.Count > 0)
            {
                MailMessage mailmsg = CreateEmailForBulkTDInstructions(data.Tables[TDAMMBulkTableName], "AMM");
                if (mailmsg != null)
                {
                    MemoryStream mrp = new MemoryStream();
                    MailMessageExt.Save(mailmsg, mrp);
                    int bytesRead = 0;
                    byte[] buffer = new byte[8096];
                    while ((bytesRead = mrp.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        mrp.Write(buffer, 0, bytesRead);
                    }
                    string file = "Bulk_TDInstructions to AMM_" + DateTime.Now.ToString("yyyyMMdd") + ".eml";
                    output.Add(file, mrp.ToArray());
                }
            }

            var bytes = new byte[] { };
            if (output.Count > 0)
            {
                bytes = Zipper.CreateZip(output);
            }
            return bytes;
        }

        private void CreateAtCallInstructions(SerializableDictionary<string, byte[]> output, DataTable dt, string broker, Oritax.TaxSimp.Common.OrderItemType type)
        {
            MailMessage mailmsg = CreateEmailForATCallInstructions(dt, broker);
            if (mailmsg != null)
            {
                MemoryStream mrp = new MemoryStream();
                MailMessageExt.Save(mailmsg, mrp);
                int bytesRead = 0;
                byte[] buffer = new byte[8096];
                while ((bytesRead = mrp.Read(buffer, 0, buffer.Length)) > 0)
                {
                    mrp.Write(buffer, 0, bytesRead);
                }
                string file = string.Format("AtCallInstructions {0} to {1}_{2}.eml", type, broker, DateTime.Now.ToString("yyyyMMdd"));
                output.Add(file, mrp.ToArray());
            }
        }

        public IWorkbook CreateBankWestInstructions(DataTable dt)
        {
            for (int i = 1; i <= 5; i++)
            {
                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);
            }
            int row = 20;
            int currentColumn = 0;
            string path = "~/Templates/BWA_CMA_Template.xls";
            IWorkbook workbook = Factory.GetWorkbook(HttpContext.Current.Server.MapPath(path));
            IWorksheet worksheet = workbook.Worksheets["Sheet1"];
            IRange cells = workbook.Worksheets["Sheet1"].Cells;
            //cells["B11"].NumberFormat = "dd/MM/yyyy";
            cells["B11"].Value = DateTime.Now;
            cells["C13"].Value = "913-001";
            cells["C14"].Value = "812096";
            if (dt.TableName == BankWestTableNameForP2)
            {
                cells["C15"].Value = "State Street - EPPA";
            }
            else
            {
                cells["C15"].Value = "State Street - INAP";
            }
            cells["C16"].Value = "e-Clipse UMA Transfers";
            IRange range = worksheet.Cells[row, currentColumn];
            SpreadsheetGear.Data.SetDataFlags Flag;
            Flag = SpreadsheetGear.Data.SetDataFlags.NoColumnHeaders;
            range.CopyFromDataTable(dt, Flag);
            IRange Cell = cells["D17"];
            Cell.Formula = "=SUM(D21:D" + (20 + dt.Rows.Count + 1).ToString() + ")";
            cells["A21:E" + (20 + dt.Rows.Count).ToString()].Borders.Color = Color.Black;
            IRange totalCellLabel = cells["C" + (21 + dt.Rows.Count).ToString()];
            totalCellLabel.Value = "TOTAL";
            totalCellLabel.Font.Name = "Arial";
            totalCellLabel.Font.Size = 10;
            totalCellLabel.Font.Bold = true;
            totalCellLabel.Borders.Color = Color.Black;
            totalCellLabel.Interior.Color = Color.FromArgb(255, 192, 192, 192);
            cells[21 + dt.Rows.Count + ":499"].Rows.Delete();
            return workbook;
        }

        private IWorkbook CreateMacquarieInstructions(DataTable dt)
        {
            //removing Extra column 
            if (dt.Columns.IndexOf("OrderBankAccountType") > -1)
            {
                dt.Columns.Remove("OrderBankAccountType");
            }
            for (int i = 1; i <= 1; i++)
            {
                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);
            }
            const int row = 14;
            const int currentColumn = 0;
            const string path = "~/Templates/MAC_CMA_Template.csv";
            IWorkbook workbook = Factory.GetWorkbook(HttpContext.Current.Server.MapPath(path));
            IWorksheet worksheet = workbook.Worksheets["Sheet1"];
            IRange cells = workbook.Worksheets["Sheet1"].Cells;
            cells["B2"].Value = DateTime.Now.ToString("dd/MM/yyyy");
            cells["B2"].HorizontalAlignment = HAlign.Right;
            IRange range = worksheet.Cells[row, currentColumn];
            const SetDataFlags flag = SetDataFlags.NoColumnHeaders;
            range.CopyFromDataTable(dt, flag);
            cells[16 + dt.Rows.Count + ":3000"].Rows.Delete();
            return workbook;
        }

        public string FileType
        { get; set; }

        private Document SendAtCallInstructions(DataRow dr)
        {
            Document doc = new Document();
            const string TitlePic = "~/images/AtCallPDFTitle.png";
            const string BretSingtaure = "~/images/BretSignature.png";
            const string InnaSingtaure = "~/images/AnnaSignature.png";
            DocumentBuilder builderExl = new DocumentBuilder(doc);
            builderExl.MoveToBookmark("ECLIPSE_PDF", false, true);
            builderExl.PageSetup.LeftMargin = 40;
            builderExl.PageSetup.RightMargin = 40;
            builderExl.PageSetup.TopMargin = 40;
            builderExl.PageSetup.BottomMargin = 40;
            builderExl.StartTable();
            builderExl.InsertCell();
            builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Center;
            builderExl.InsertImage(File.ReadAllBytes(HttpContext.Current.Server.MapPath(TitlePic)));
            builderExl.CellFormat.Borders.LineWidth = 0;
            builderExl.EndRow();
            builderExl.EndTable();
            builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            builderExl.Writeln("");
            builderExl.Writeln(DateTime.Now.ToString("dd/MM/yyyy"));
            builderExl.Writeln("");
            builderExl.Writeln("Client Services");
            builderExl.Writeln("BWA Managed Investments");
            builderExl.Writeln("GPO Box 2515");
            builderExl.Writeln("Perth  WA  6001");
            builderExl.Writeln("");
            builderExl.Writeln("Dear Client Services");
            builderExl.Writeln("");
            builderExl.StartTable();
            builderExl.InsertCell();
            builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Center;
            builderExl.Bold = true;
            builderExl.Writeln(dr["Client"].ToString());
            builderExl.Bold = true;
            builderExl.EndRow();
            builderExl.EndTable();

            builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            builderExl.Bold = false;
            builderExl.Writeln("Please arrange for " + dr["Amount"] + " to be transferred between the following accounts:");
            builderExl.Writeln("");
            builderExl.StartTable();

            builderExl.InsertCell();
            builderExl.CellFormat.Borders.LineWidth = 1;
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.SteelBlue;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.Bold = false;
            builderExl.Write("");

            builderExl.InsertCell();
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.SteelBlue;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.Bold = false;
            builderExl.Font.Color = Color.White;
            builderExl.Write("From account");

            builderExl.InsertCell();
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.SteelBlue;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.Bold = false;
            builderExl.Font.Color = Color.White;
            builderExl.Write("To account");
            builderExl.EndRow();

            builderExl.InsertCell();
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.SteelBlue;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.Bold = false;
            builderExl.Font.Color = Color.White;
            builderExl.Write("Bank Name");

            builderExl.InsertCell();
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.White;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.Bold = false;
            builderExl.Font.Color = Color.Black;
            builderExl.Write(dr["Bank From"].ToString());

            builderExl.InsertCell();
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.White;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.Bold = false;
            builderExl.Font.Color = Color.Black;
            builderExl.Write(dr["Bank To"].ToString());
            builderExl.EndRow();

            builderExl.InsertCell();
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.SteelBlue;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.Bold = false;
            builderExl.Font.Color = Color.White;
            builderExl.Write("Account Name");

            builderExl.InsertCell();
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.White;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.Bold = false;
            builderExl.Font.Color = Color.Black;
            builderExl.Write(dr["Account Name From"].ToString());

            builderExl.InsertCell();
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.White;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.Bold = false;
            builderExl.Font.Color = Color.Black;
            builderExl.Write(dr["Account Name To"].ToString());
            builderExl.EndRow();

            builderExl.InsertCell();
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.SteelBlue;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.Bold = false;
            builderExl.Font.Color = Color.White;
            builderExl.Write("BSB");

            builderExl.InsertCell();
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.White;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.Bold = false;
            builderExl.Font.Color = Color.Black;
            builderExl.Write(dr["BSB From"].ToString());

            builderExl.InsertCell();
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.White;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.Bold = false;
            builderExl.Font.Color = Color.Black;
            builderExl.Write(dr["BSB To"].ToString());
            builderExl.EndRow();

            builderExl.InsertCell();
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.SteelBlue;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.CellFormat.WrapText = false;
            builderExl.Bold = false;
            builderExl.Font.Color = Color.White;
            builderExl.Write("Account number");

            builderExl.InsertCell();
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.White;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.Bold = false;
            builderExl.Font.Color = Color.Black;
            builderExl.Write(dr["Account No From"].ToString());

            builderExl.InsertCell();
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.White;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.Bold = false;
            builderExl.Font.Color = Color.Black;
            builderExl.Write(dr["Account No To"].ToString());
            builderExl.EndRow();

            builderExl.InsertCell();
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.SteelBlue;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.Bold = false;
            builderExl.Font.Color = Color.White;
            builderExl.Write("Narration");

            builderExl.InsertCell();
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.White;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.Bold = false;
            builderExl.Font.Color = Color.Black;
            builderExl.Write(dr["Narration From"].ToString());

            builderExl.InsertCell();
            builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.White;
            builderExl.CellFormat.LeftPadding = 2;
            builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
            builderExl.Bold = false;
            builderExl.Font.Color = Color.Black;
            builderExl.Write(dr["Narration To"].ToString());
            builderExl.EndRow();
            builderExl.EndTable();

            builderExl.Writeln("");
            builderExl.Writeln("Please confirm once the transfer has been processed.");
            builderExl.Writeln("");
            builderExl.Writeln("If you have any questions, please do not hesitate to contact either one of us on 02 9346 4660.");
            builderExl.Writeln("");
            builderExl.Writeln("Kind regards");
            builderExl.Writeln("");

            builderExl.StartTable();

            builderExl.InsertCell();
            builderExl.CellFormat.Borders.LineWidth = 0;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.White;
            builderExl.CellFormat.Borders.Color = Color.White;
            builderExl.InsertImage(File.ReadAllBytes(HttpContext.Current.Server.MapPath(BretSingtaure)));

            builderExl.InsertCell();
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.White;
            builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Right;
            builderExl.InsertImage(File.ReadAllBytes(HttpContext.Current.Server.MapPath(InnaSingtaure)));
            builderExl.EndRow();

            builderExl.InsertCell();
            builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.White;
            builderExl.Writeln("");
            builderExl.Writeln("Brett Westbrook");

            builderExl.InsertCell();
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.White;
            builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Center;
            builderExl.Writeln("");
            builderExl.Writeln("Anna Uremovic");
            builderExl.EndRow();
            builderExl.EndTable();

            builderExl.StartTable();
            builderExl.InsertCell();
            builderExl.ParagraphFormat.Alignment = ParagraphAlignment.Center;
            builderExl.CellFormat.Shading.BackgroundPatternColor = Color.White;
            builderExl.CellFormat.Borders.LineWidth = 0;
            builderExl.CellFormat.Borders.Color = Color.White;
            builderExl.Bold = true;
            builderExl.Font.Size = 10;
            builderExl.Writeln("FORTNUM PRIVATE WEALTH PTY LTD");
            builderExl.Bold = false;
            builderExl.Font.Size = 8;
            builderExl.Writeln("ABN 54 139 889 535 ");
            builderExl.Writeln("3/36 Bydown Street, Neutral Bay NSW 2089 |  P.O. Box 1899 Neutral Bay NSW 2089 ");
            builderExl.Writeln("T +61 02 9904 2792 | F +61 02 9953 5668  ");
            builderExl.EndRow();
            builderExl.EndTable();
            return doc;
        }

        private MailMessage CreateEmailForATCallInstructions(DataTable dt, string AtCallInst)
        {
            StringBuilder strBody = new StringBuilder();
            strBody.AppendLine("<html>");
            strBody.AppendLine("<head>");
            strBody.AppendLine("</head>");
            strBody.AppendLine("<body>");
            strBody.AppendLine("<p>Dear Client Services,</p>");

            string oit = dt.Rows[0]["OrderItemType"].ToString();
            switch (oit.ToLower())
            {
                case "buy":
                    CreateInstructionTableForBuyAtCall(dt, strBody);
                    break;
                case "sell":
                    CreateInstructionTableForSellAtCall(dt, strBody);
                    break;
            }


            strBody.AppendLine("</table></p>");
            strBody.AppendLine("<p>If you have any questions, please do not hesitate to contact Anna Uremovic or Brett Westbrook on 02 9346 4660.</p>");
            strBody.AppendLine("<p>Kind regards</p>");
            strBody.AppendLine("<p>Anna Uremovic<br/>Head of Operations<br/><span style=\"font-family: 'Times New Roman', Times, serif; font-size: 16px; font-weight: bold\">e-Clipse Online</span><br/>3/36 Bydown Street<br/>PO Box 1899<br/>NEUTRAL BAY NSW 2089<br/><br/>Tel: (02) 9346-4686<br/>Direct: (02) 9346-4660<br/>Mobile: 0416 075 017<br/>Email: auremovic@e-clipse.com.au<br/>www.e-clipse.com.au</p>");
            strBody.AppendLine("</body>");
            strBody.AppendLine("</html>");
            MailMessage mMailMessage = new MailMessage();
            mMailMessage.From = new MailAddress("auremovic@e-clipse.com.au");

            switch (AtCallInst.ToLower())
            {
                case "fiig":
                    mMailMessage.To.Add(new MailAddress("td@fiig.com.au"));
                    mMailMessage.Subject = "At call orders - " + DateTime.Now.ToString("dd/MM/yyyy");
                    break;
                case "amm":
                    mMailMessage.To.Add(new MailAddress("admin@moneymarket.com.au"));
                    mMailMessage.Subject = "At call orders - " + DateTime.Now.ToString("dd/MM/yyyy");
                    break;
                case "other":
                    mMailMessage.To.Add(new MailAddress(""));
                    mMailMessage.Subject = "At call orders - " + DateTime.Now.ToString("dd/MM/yyyy");
                    break;
            }
            mMailMessage.Body = strBody.ToString();
            mMailMessage.IsBodyHtml = true;
            return mMailMessage;
        }

        private void CreateInstructionTableForBuyAtCall(DataTable dt, StringBuilder strBody)
        {
            strBody.AppendLine("<p>Can you please organise the following investments effective today:</p>");
            strBody.AppendLine("<p><table width=\"100%\" border=\"1\" style=\"border-color:black;\"><tr><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"></td><td colspan=\"2\" bgcolor=\"#0066CC\" style=\"color: #FFFFFF\">");
            strBody.AppendLine("<b> Account to debit</b></td><td colspan=\"4\" bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Investment details </b></td></tr><tr><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Client</b></td>");
            strBody.AppendLine("<td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Client Code</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>BSB</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Account No</b></td>");
            strBody.AppendLine("<td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Amount</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Investment Account Name</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Rate</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Institution</b></td></tr>");

            DataTable dtBuy = dt.DefaultView.ToTable(false, "Client", "Client Code", "BSB", "Account No", "Amount", "Investment Account Name", "Rate", "Institution");

            foreach (DataRow drr in dtBuy.Rows)
            {
                strBody.AppendLine("<tr>");
                foreach (string strColValue in drr.ItemArray)
                {
                    strBody.AppendLine("<td>" + strColValue + "</td>");
                }
                strBody.AppendLine("</tr>");
            }
        }

        private void CreateInstructionTableForSellAtCall(DataTable dt, StringBuilder strBody)
        {
            strBody.AppendLine("<p>Can you please organise the following redemptions effective today:</p>");
            strBody.AppendLine("<p><table width=\"100%\" border=\"1\" style=\"border-color:black;\"><tr><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"></td><td colspan=\"3\" bgcolor=\"#0066CC\" style=\"color: #FFFFFF\">");
            strBody.AppendLine("<b> Redemption details</b></td><td colspan=\"3\" bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Account to credit </b></td></tr><tr><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Client</b></td>");
            strBody.AppendLine("<td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Client Code</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Amount</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Customer No</b></td>");
            strBody.AppendLine("<td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Institution</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>BSB</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Account No</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Account Name</b></td></tr>");

            DataTable dtSell = dt.DefaultView.ToTable(false, "Client", "Client Code", "Amount", "CustomerNo", "Institution", "BSB", "Account No", "AccountName");

            foreach (DataRow drr in dtSell.Rows)
            {
                strBody.AppendLine("<tr>");
                foreach (string strColValue in drr.ItemArray)
                {
                    strBody.AppendLine("<td>" + strColValue + "</td>");
                }
                strBody.AppendLine("</tr>");
            }
        }

        private MailMessage CreateEmailForTDInstructions(DataRow[] drs, string tdAccountType)
        {
            StringBuilder strBody = new StringBuilder();
            strBody.AppendLine("<html>");
            strBody.AppendLine("<head>");
            strBody.AppendLine("</head>");
            strBody.AppendLine("<body>");
            strBody.AppendLine("<p>Dear Client Services,</p>");
            strBody.AppendLine("<p>Can you please organise the following TDs effective today:</p>");
            strBody.AppendLine("<p><table width=\"100%\" border=\"1\" style=\"border-color:black;\"><tr><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"></td><td colspan=\"2\" bgcolor=\"#0066CC\" style=\"color: #FFFFFF\">");
            strBody.AppendLine("<b> Account to debit</b></td><td colspan=\"4\" bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>TD details </b></td></tr><tr><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Client</b></td>");
            strBody.AppendLine("<td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Client Code</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>BSB</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Account No</b></td>");
            strBody.AppendLine("<td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Amount</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Term</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Rate</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Institution</b></td></tr>");
            foreach (DataRow drr in drs)
            {
                strBody.AppendLine("<tr>");
                foreach (string strColValue in drr.ItemArray)
                {
                    strBody.AppendLine("<td>" + strColValue + "</td>");
                }
                strBody.AppendLine("</tr>");
            }
            strBody.AppendLine("</table></p>");
            strBody.AppendLine("<p>If you have any questions, please do not hesitate to contact Anna Uremovic or Brett Westbrook on 02 9346 4660.</p>");
            strBody.AppendLine("<p>Kind regards</p>");
            strBody.AppendLine("<p>Anna Uremovic<br/>Head of Operations<br/><span style=\"font-family: 'Times New Roman', Times, serif; font-size: 16px; font-weight: bold\">e-Clipse Online</span><br/>3/36 Bydown Street<br/>PO Box 1899<br/>NEUTRAL BAY NSW 2089<br/><br/>Tel: (02) 9346-4686<br/>Direct: (02) 9346-4660<br/>Mobile: 0416 075 017<br/>Email: auremovic@e-clipse.com.au<br/>www.e-clipse.com.au</p>");
            strBody.AppendLine("</body>");
            strBody.AppendLine("</html>");
            MailMessage mMailMessage = new MailMessage();
            mMailMessage.From = new MailAddress(ConfigurationManager.AppSettings["TDInstructionFrom"]);

            switch (tdAccountType.ToLower())
            {
                case "fiig":
                    mMailMessage.To.Add(new MailAddress(ConfigurationManager.AppSettings["TDInstructionToFiiGTo"]));
                    mMailMessage.Subject = "TD orders - " + DateTime.Now.ToString("dd/MM/yyyy");
                    break;
                case "amm":
                    mMailMessage.To.Add(new MailAddress(ConfigurationManager.AppSettings["TDInstructionToAMMTo"]));
                    mMailMessage.Subject = "TD orders - " + DateTime.Now.ToString("dd/MM/yyyy");
                    break;
                case "other":
                    //mMailMessage.To.Add(new MailAddress(""));
                    mMailMessage.Subject = "TD orders - " + DateTime.Now.ToString("dd/MM/yyyy");
                    break;
            }
            mMailMessage.Body = strBody.ToString();
            mMailMessage.IsBodyHtml = true;
            //SmtpClient client = new SmtpClient();
            //client.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
            //client.PickupDirectoryLocation = @"C:\temp";
            //client.EnableSsl = false;
            //client.Send(mMailMessage);
            return mMailMessage;
        }

        private MailMessage CreateEmailForTDInstructions(DataTable dt, string tdAccountType)
        {
            StringBuilder strBody = new StringBuilder();
            strBody.AppendLine("<html>");
            strBody.AppendLine("<head>");
            strBody.AppendLine("</head>");
            strBody.AppendLine("<body>");
            strBody.AppendLine("<p>Dear Client Services,</p>");
            strBody.AppendLine("<p>Can you please organise the following TDs effective today:</p>");
            strBody.AppendLine("<p><table width=\"100%\" border=\"1\" style=\"border-color:black;\"><tr><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"></td><td colspan=\"2\" bgcolor=\"#0066CC\" style=\"color: #FFFFFF\">");
            strBody.AppendLine("<b> Account to debit</b></td><td colspan=\"4\" bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>TD details </b></td></tr><tr><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Client</b></td>");
            strBody.AppendLine("<td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Client Code</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>BSB</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Account No</b></td>");
            strBody.AppendLine("<td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Amount</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Term</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Rate</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Institution</b></td></tr>");
            foreach (DataRow drr in dt.Rows)
            {
                strBody.AppendLine("<tr>");
                foreach (string strColValue in drr.ItemArray)
                {
                    strBody.AppendLine("<td>" + strColValue + "</td>");
                }
                strBody.AppendLine("</tr>");
            }
            strBody.AppendLine("</table></p>");
            strBody.AppendLine("<p>If you have any questions, please do not hesitate to contact Anna Uremovic or Brett Westbrook on 02 9346 4660.</p>");
            strBody.AppendLine("<p>Kind regards</p>");
            strBody.AppendLine("<p>Anna Uremovic<br/>Head of Operations<br/><span style=\"font-family: 'Times New Roman', Times, serif; font-size: 16px; font-weight: bold\">e-Clipse Online</span><br/>3/36 Bydown Street<br/>PO Box 1899<br/>NEUTRAL BAY NSW 2089<br/><br/>Tel: (02) 9346-4686<br/>Direct: (02) 9346-4660<br/>Mobile: 0416 075 017<br/>Email: auremovic@e-clipse.com.au<br/>www.e-clipse.com.au</p>");
            strBody.AppendLine("</body>");
            strBody.AppendLine("</html>");
            MailMessage mMailMessage = new MailMessage();
            mMailMessage.From = new MailAddress("auremovic@e-clipse.com.au");

            switch (tdAccountType.ToLower())
            {
                case "fiig":
                    mMailMessage.To.Add(new MailAddress("td@fiig.com.au"));
                    mMailMessage.Subject = "TD orders - " + DateTime.Now.ToString("dd/MM/yyyy");
                    break;
                case "amm":
                    mMailMessage.To.Add(new MailAddress("admin@moneymarket.com.au"));
                    mMailMessage.Subject = "TD orders - " + DateTime.Now.ToString("dd/MM/yyyy");
                    break;
                case "other":
                    mMailMessage.To.Add(new MailAddress(""));
                    mMailMessage.Subject = "TD orders - " + DateTime.Now.ToString("dd/MM/yyyy");
                    break;
            }
            mMailMessage.Body = strBody.ToString();
            mMailMessage.IsBodyHtml = true;
            //SmtpClient client = new SmtpClient();
            //client.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
            //client.PickupDirectoryLocation = @"C:\temp";
            //client.EnableSsl = false;
            //client.Send(mMailMessage);
            return mMailMessage;
        }

        private MailMessage CreateEmailForBulkTDInstructions(DataTable dt, string tdAccountType)
        {
            var strBody = new StringBuilder();
            strBody.AppendLine("<html>");
            strBody.AppendLine("<head>");
            strBody.AppendLine("</head>");
            strBody.AppendLine("<body>");
            strBody.AppendLine("<p>Dear Client Services,</p>");
            strBody.AppendLine("<p>Can you please organise the following TD orders for e-clipse super effective today:</p>");
            strBody.AppendLine("<p><table width=\"70%\" border=\"1\" style=\"border-color:black;\"><tr><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\" colspan=\"4\"><b>TD details</b></td></tr><tr><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Amount</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\">");
            strBody.AppendLine("<b>Term</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Rate</b></td><td bgcolor=\"#0066CC\" style=\"color: #FFFFFF\"><b>Institution</b></td></tr>");
            foreach (DataRow drr in dt.Rows)
            {
                strBody.AppendLine("<tr>");
                foreach (string strColValue in drr.ItemArray)
                {
                    strBody.AppendLine("<td>" + strColValue + "</td>");
                }
                strBody.AppendLine("</tr>");
            }
            strBody.AppendLine("</table></p>");
            strBody.AppendLine("<p>Settlement will be arranged by the funds custodian JP Morgan.<br/>Please send us the contract notes for these term deposits as soon as possible.</p>");
            strBody.AppendLine("<p>If you have any questions, please do not hesitate to contact Anna Uremovic or Brett Westbrook on 02 9346 4660.</p>");
            strBody.AppendLine("<p>Kind regards</p>");
            strBody.AppendLine("<p>Anna Uremovic<br/>Head of Operations<br/><span style=\"font-family: 'Times New Roman', Times, serif; font-size: 16px; font-weight: bold\">e-Clipse Online</span><br/>3/36 Bydown Street<br/>PO Box 1899<br/>NEUTRAL BAY NSW 2089<br/><br/>Tel: (02) 9346-4686<br/>Direct: (02) 9346-4660<br/>Mobile: 0416 075 017<br/>Email: auremovic@e-clipse.com.au<br/>www.e-clipse.com.au</p>");
            strBody.AppendLine("</body>");
            strBody.AppendLine("</html>");
            var mMailMessage = new MailMessage { From = new MailAddress("auremovic@e-clipse.com.au") };

            switch (tdAccountType.ToLower())
            {
                case "fiig":
                    mMailMessage.To.Add(new MailAddress("td@fiig.com.au"));
                    mMailMessage.Subject = "TD orders - " + DateTime.Now.ToString("dd/MM/yyyy");
                    break;
                case "amm":
                    mMailMessage.To.Add(new MailAddress("admin@moneymarket.com.au"));
                    mMailMessage.Subject = "TD orders - " + DateTime.Now.ToString("dd/MM/yyyy");
                    break;
                case "other":
                    mMailMessage.To.Add(new MailAddress(""));
                    mMailMessage.Subject = "TD orders - " + DateTime.Now.ToString("dd/MM/yyyy");
                    break;
            }
            mMailMessage.Body = strBody.ToString();
            mMailMessage.IsBodyHtml = true;
            return mMailMessage;
        }

        public ICMBroker Broker
        {
            get;
            set;
        }

        public UserEntity User { get; set; }
        public ClientManagementType ClientManagementType { get; set; }

        #endregion

        public System.Collections.Generic.List<Guid> OrdersContianingOnly
        {
            get;
            set;

        }

        public string TradeDate { get; set; }
    }
}