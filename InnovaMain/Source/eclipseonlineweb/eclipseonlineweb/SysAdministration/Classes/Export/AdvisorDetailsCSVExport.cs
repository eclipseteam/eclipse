﻿using System;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Commands;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Data;
using System.Text;

namespace eclipseonlineweb.Export
{
    public class AdvisorCSVExport : IExportable
    {

        #region IExportable Members

        public System.Data.DataSet Export()
        {
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);

            ExportDS accountopeingsDs = new ExportDS();
            accountopeingsDs.Command = (int)WebCommands.ExportAdvisorDetails;
            accountopeingsDs.Unit = new OrganizationUnit
            {
                CurrentUser = User,
                
            };
            org.GetData(accountopeingsDs);
            
            Broker.ReleaseBrokerManagedComponent(org);

            return accountopeingsDs;
        }

        public byte[] GetByteArray(System.Data.DataSet data)
        {         
            FileType = "csv";
            string sbStateStreet = string.Empty;
            AdviserDetailsCSVColumns obj = new AdviserDetailsCSVColumns();
            sbStateStreet = Utilities.Datatable_to_csv(data.Tables[obj.Table.TableName], "ExportAdvisorDetails_" + DateTime.Now.ToString("dd-MM-yy") + "_.csv");

            byte[] bytes = Encoding.Unicode.GetBytes(sbStateStreet);
            return bytes;        
        }

        public string FileType
        {get;set;}
        

        public ICMBroker Broker
        {
            get;
            set;
        }

        public UserEntity User{get;set;}
        public ClientManagementType ClientManagementType { get; set; }

        #endregion
    }
}