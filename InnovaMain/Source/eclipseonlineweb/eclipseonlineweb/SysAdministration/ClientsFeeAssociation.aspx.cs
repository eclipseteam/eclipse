﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Oritax.TaxSimp.Security;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.DataSets;

namespace eclipseonlineweb.SysAdministration
{
    public partial class ClientsFeeAssociation : UMABasePage
    {
        #region Base Properties

        public override void LoadPage()
        {
            base.LoadPage();
        }

        protected override void GetBMCData()
        {
            IOrganization orgCM = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            OrganisationListingDS organisationListingDS = new OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.ClientsAndFeesAssociation;
            orgCM.GetData(organisationListingDS);

            this.PresentationData = organisationListingDS;
        }

        public override void PopulatePage(System.Data.DataSet ds)
        {

        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
          
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        #endregion

        #region Telerik Section
        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e) { }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e){
            if (e.CommandName == "Configure")
            {
                string INS = e.CommandArgument.ToString();
                Response.Redirect("~/ClientViews/ConfigureFees.aspx?INS=" + INS + "&PageName=" + "ClientFeeAssociation");
            }
        }
        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) { }
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e) { }
        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e) { }
        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e) { }
        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e) { }
        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DataView View = new DataView(this.PresentationData.Tables[ClientFeesAssocDS.CLIENTFEEASSOCIATIONSTABLE]);
            View.Sort = ClientFeesAssocDS.CLIENTNAME + " ASC";
            this.PresentationGrid.DataSource = View.ToTable();
        }
        #endregion
    }
}