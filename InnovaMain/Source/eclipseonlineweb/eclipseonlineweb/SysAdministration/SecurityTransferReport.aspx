﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="SecurityTransferReport.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.SecurityTransferReport" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <div style="text-align: right; float: right">
            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
        </div>
    </fieldset>
    <fieldset>
        <legend>Security Tranfer Report</legend>
        <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
            Width="100%" PageSize="20" AllowSorting="True" AllowMultiRowSelection="False"
            AllowPaging="True" GridLines="None" AllowFilteringByColumn="true" EnableViewState="true"
            ShowFooter="false" >
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowFilteringByColumn="True" TableLayout="Fixed" Width="100%">
                <Columns>
                    <telerik:GridBoundColumn SortExpression="Securitycode" ReadOnly="true" HeaderText="Securitycode"
                        HeaderButtonType="TextButton" DataField="Securitycode" UniqueName="Securitycode">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="Securityname" ReadOnly="true" HeaderText="Securityname"
                        HeaderButtonType="TextButton" DataField="Securityname" UniqueName="Securityname">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="“dateofearliesttransferin" ReadOnly="true"
                        HeaderText="dateofearliesttransferin" HeaderButtonType="TextButton" DataField="dateofearliesttransferin"
                        UniqueName="dateofearliesttransferin">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="“Clientcodefothistransfe" ReadOnly="true"
                        HeaderText="Client Code Transfer" HeaderButtonType="TextButton" DataField="Clientcodefothistransfe"
                        UniqueName="Clientcodefothistransfe">
                    </telerik:GridBoundColumn>
                    <telerik:GridDateTimeColumn Visible="true" UniqueName="earliestpricedate" PickerType="DatePicker"
                        HeaderText="earliest price date" HeaderStyle-BackColor="LightGray" DataField="earliestpricedate"
                        DataFormatString="{0:dd/MM/yyyy}">
                        <ItemStyle Width="85px" />
                    </telerik:GridDateTimeColumn>
                    <telerik:GridBoundColumn SortExpression="Priceatearliestpricedate" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-HorizontalAlign="Center" HeaderText="Price at earliest price date"
                        HeaderButtonType="TextButton" DataField="Priceatearliestpricedate" UniqueName="Priceatearliestpricedate">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </fieldset>
</asp:Content>
