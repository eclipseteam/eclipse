﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eclipseonlineweb.SysAdministration
{
    public partial class DownloadOrderReceipt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string filename = Request["filename"];
            switch (filename.Split('.')[1].ToLower())
            {
                case "pdf":
                    Response.ContentType = "image/pdf";
                    break;
                case "zip":
                    Response.ContentType = "application/zip";
                    break;
                case "txt":
                    Response.ContentType = "text/txt";
                    break;
            }

            Response.AppendHeader("Content-Disposition", string.Format("attachment; filename={0}", filename));
            Response.TransmitFile(string.Format("{0}\\OrderReceipts\\{1}", HttpContext.Current.Server.MapPath("~/App_Data"), filename));
            Response.End();
        }
    }
}