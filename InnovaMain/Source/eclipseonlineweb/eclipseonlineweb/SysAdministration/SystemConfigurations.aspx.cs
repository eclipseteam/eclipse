﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;

namespace eclipseonlineweb.SysAdministration
{
    public partial class SystemConfigurations : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");

            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }
        protected override void GetBMCData()
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var ds = new OrganizationalSettingsDS() { CommandType = DatasetCommandTypes.Get, };

            organization.GetData(ds);
            this.PresentationData = ds;

            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        public override void PopulatePage(DataSet ds)
        {
            if (!IsPostBack)
                LoadSetttings((OrganizationalSettingsDS)ds);
        }

        private void LoadSetttings(OrganizationalSettingsDS ds)
        {
            if (ds.OrganizationalSettingTable.Rows.Count > 0)
            {
                chkSendToSMA.Checked = (bool)ds.OrganizationalSettingTable.Rows[0][ds.OrganizationalSettingTable.SENDORDERSTOSMA];
                chkReadFromSMA.Checked = (bool)ds.OrganizationalSettingTable.Rows[0][ds.OrganizationalSettingTable.READFROMSMA];
            }

        }

        protected void BtnCreateObpObject(object sender, EventArgs e)
        {
            CreateWellKnownObject(WellKnownCM.OBP, new Guid("986E40E6-19F6-49FA-B94D-36A7C3D2558C"), "OBP");
        }
        protected void BtnCreateSecuritiyObject(object sender, EventArgs e)
        {
            CreateWellKnownObject(WellKnownCM.SecurityProvider, new Guid("6D0500F5-B1F9-4053-88BC-FF4004D9E9D0"), "SAS70Password");
        }
        protected void BtnCreateLiecenceObject(object sender, EventArgs e)
        {
            CreateWellKnownObject(WellKnownCM.Licencing, new Guid("26437F7E-10D8-4887-A462-E92A7EF3F05C"), "Licence");
        }


        private void CreateWellKnownObject(WellKnownCM wellKnownCm, Guid typeId, string name)
        {
            try
            {
                UMABroker.SaveOverride = true;


                IBrokerManagedComponent component = UMABroker.GetWellKnownBMC(wellKnownCm);

                if (component == null)
                {
                    Console.WriteLine("Creating " + name + "...\n");

                    IBrokerManagedComponent componentInstance = UMABroker.CreateComponentInstance(typeId, name);
                    lblMessage.Text = "A " + name + " has been created..\n";
                }
                else
                {
                    lblMessage.Text = "A " + name + " already exists..\n";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Creation of " + name + " failed: " + ex.Message + "\n";
                throw;
            }
            finally
            {
                if (UMABroker != null)
                {
                    UMABroker.SetComplete();
                    this.UMABroker.SetStart();
                }
            }
        }

        protected void BtnSaveOrganizationalSettings(object sender, EventArgs e)
        {
            var ds = new OrganizationalSettingsDS() { CommandType = DatasetCommandTypes.Update, };
            var row = ds.OrganizationalSettingTable.NewRow();
            row[ds.OrganizationalSettingTable.SENDORDERSTOSMA] = chkSendToSMA.Checked;
            row[ds.OrganizationalSettingTable.READFROMSMA] = chkReadFromSMA.Checked;
            ds.OrganizationalSettingTable.Rows.Add(row);
            SaveOrganizanition(ds);
        }
    }
}