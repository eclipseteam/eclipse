﻿<%@ Page Title="e-Clipse Online Portal" EnableViewState="true" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeBehind="OrderPadUtil.aspx.cs" Inherits="eclipseonlineweb.OrderPadUtil" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="c1" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="SideMenu" ContentPlaceHolderID="SidebarMenu" runat="Server">
<asp:Button ID="btnOrdercm" runat="server" OnClick="OnClick" Text="Show Order CMs"/> <br/>
<asp:Button ID="btnUnsettled" runat="server" OnClick="Unsetlled_OnClick" Text="Show Unsettled CMs"/>
  <asp:Button ID="btnOBP" runat="server" OnClick="OBPCM_OnClick" Text="Show OBP CMs"/>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
         
        
            <c1:C1GridView  ShowFilter="true" OnFiltering="Filter" RowStyle-Font-Size="Smaller"
                AllowSorting="true" OnSorting="SortMainList" FilterStyle-Font-Size="Smaller"
                HeaderStyle-Font-Size="Smaller" EnableTheming="true" Width="100%" ID="InstanceListGrid"
                CallbackSettings-Action="None"
                PagerSettings-Mode="Numeric" runat="server" AutogenerateColumns="true" AllowPaging="false"
                PageSize="15" ClientSelectionMode="None" CallbackSettings-Mode="Full"  >
                <SelectedRowStyle />
                <Columns>
       
                </Columns>
            </c1:C1GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
