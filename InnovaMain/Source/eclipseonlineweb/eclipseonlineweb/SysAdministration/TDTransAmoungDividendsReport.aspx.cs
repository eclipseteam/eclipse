﻿using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.DataSets;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class TDTransAmoungDividendsReport : UMABasePage
    {
      
       public override void LoadPage()
        {
            base.LoadPage();
            DivTranGrid.DataSource = GetTDTransactionsAmoungDividens().ToTable();
            DivTranGrid.DataBind();
           
        }

       public override bool AccessibleByUser()
       {
           DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
           if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
               return true;
           else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
               return false;
           else
               return true;
       }

       private DataView GetTDTransactionsAmoungDividens()
       {
           var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
           DIVTransactionDS dividendDs = new DIVTransactionDS();
           dividendDs.Command = (int)WebCommands.GetTransactionAmongDividents;
           dividendDs.Unit = new OrganizationUnit { CurrentUser = (Page as UMABasePage).GetCurrentUser() };
           org.GetData(dividendDs);
           DataView tddividendView = new DataView(dividendDs.Tables["Income Transactions"]);
           tddividendView.RowFilter = "InvestmentAmount>" + 0 + " or InterestPaid>" + 0 + " or InterestRate>" + 0 + "";
           tddividendView.Sort = "RecordDate DESC";
           UMABroker.ReleaseBrokerManagedComponent(org);
           return tddividendView;
       }

        protected void DIV_PageIndexChanging(object sender, C1GridViewPageEventArgs e)
        {
            DivTranGrid.DataSource = GetTDTransactionsAmoungDividens().ToTable();
            DivTranGrid.PageIndex = e.NewPageIndex;
            DivTranGrid.DataBind();
        }

        protected void FilterDIV(object sender, C1GridViewFilterEventArgs e)
        {
            e.Values[0] = ((string)e.Values[0]).Trim();
            DivTranGrid.DataSource = GetTDTransactionsAmoungDividens().ToTable();
            DivTranGrid.DataBind();
            
        }

        protected void SortDIV(object sender, C1GridViewSortEventArgs e)
        {
            DivTranGrid.DataSource = GetTDTransactionsAmoungDividens().ToTable();
            DivTranGrid.DataBind();
        }
    }
}
