﻿<%@ Page Title="" Language="C#" MasterPageFile="AdminMaster.master" AutoEventWireup="true"
    CodeBehind="ImportFinSimplicityOrders.aspx.cs" Inherits="eclipseonlineweb.ImportFinSimplicityOrders"
    EnableViewState="false" %>

<%@ Register TagPrefix="uc" TagName="Details" Src="WebControls/ImportMessageControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Import Fin Simplicity Orders For StateStreet/P2"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <table width="100%">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Select Financial Simplicity File</legend>
                            <asp:Label ID="lblFile" runat="server" Font-Bold="True" Text="File:"></asp:Label>
                            <asp:FileUpload Height="23px" ID="filMyFile" runat="server" />
                          

                            <telerik:RadButton ID="cmdSend" runat="server" Width="92px" Text="Upload" OnClick="BtnUploadClick"></telerik:RadButton>

                            <asp:RegularExpressionValidator ID="rexp" runat="server" ControlToValidate="filMyFile"
                                ErrorMessage="File type should be Excel (.xls)" ValidationExpression="(.*\.([Xx][Ll][Ss])$)"
                                ForeColor="#FF3300"></asp:RegularExpressionValidator>
                               
                                <asp:CheckBox runat="server" ID="chkSequantialNumber" Text="SSAL Instruction number"
                                onclick="document.getElementById('MainContent_MainContent_txtSequantialNumber').disabled=(!this.checked); if(!this.checked) {document.getElementById('MainContent_MainContent_txtSequantialNumber').value = '';}"  />                                
                           
                           
                           <telerik:RadTextBox runat="server" ID="txtSequantialNumber" Enabled="false"></telerik:RadTextBox>
                            
                            <asp:RegularExpressionValidator ID="reSequance" runat="server" 
                                ControlToValidate="txtSequantialNumber" ErrorMessage="Please number(s) only" 
                                ForeColor="Red" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
                        </fieldset>
                        <asp:HiddenField runat="server" ID="hf_FileName" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel runat="server" ID="PnlResults">
                            <uc:Details ID="ImportMessageControl" runat="server" />
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="cmdSend" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
