﻿<%@ Page Title="e-Clipse Online Portal" EnableViewState="true" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="AccountsFUMCompliance.aspx.cs" Inherits="eclipseonlineweb.AccountsFUMCompliance" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Chart"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
    TagPrefix="wijmo" %>
<%@ Register TagPrefix="uc1" TagName="breadcrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:breadcrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblAdministrationUsersList" Text="CLIENT HOLDINGS VS FINSIMP HOLDINGS"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <table width="100%">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Select FINSIMPLICITY Holding file</legend>
                            <table>
                                <tr>
                                    <td>
                                        <c1:C1InputDate DateFormat="dd/MMM/yyyy" ID="c1ValutionDate" runat="server" ShowTrigger="true">
                                        </c1:C1InputDate>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblFile" runat="server" Font-Bold="True" Text="File:"></asp:Label>
                                        <asp:FileUpload Width="300px" ID="filMyFile" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Button ID="cmdSend" runat="server" Width="300px" Text="Export Holding Compliance File"
                                            OnClick="BtnUploadClick" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <br />
            <telerik:RadGrid Visible="false" ID="PresentationGrid" runat="server" ShowStatusBar="true"
                AutoGenerateColumns="False" PageSize="15" AllowSorting="True" AllowMultiRowSelection="False"
                AllowPaging="false" GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true"
                AllowAutomaticInserts="True" AllowAutomaticUpdates="True" EnableViewState="true"
                ShowFooter="false">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="ActiveOrders" TableLayout="Fixed">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="ClientCid" ReadOnly="true" HeaderText="ID"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="ClientCid" UniqueName="ClientCid" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="80px" SortExpression="ClientID" ReadOnly="true"
                            HeaderText="Client ID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ClientID" UniqueName="ClientID">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="500px" SortExpression="ClientName" HeaderText="Account Name"
                            ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ClientName" UniqueName="ClientName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="20px" HeaderStyle-Width="20px" SortExpression="SecurityName"
                            HeaderText="Sec Name" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="SecurityName"
                            UniqueName="SecurityName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="500px" SortExpression="Description"
                            HeaderText="Description" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Description" UniqueName="Description">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="100px" FilterControlWidth="100px" ReadOnly="true"
                            SortExpression="TotalUnits" ShowFilterIcon="true" HeaderText="Total Holding Units"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                            DataField="Total" UniqueName="TotalUnits" DataFormatString="{0:N4}">
                            <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="100px" FilterControlWidth="100px" ReadOnly="true"
                            SortExpression="Total" ShowFilterIcon="true" HeaderText="Total Holding ($)" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" HeaderButtonType="TextButton" DataField="Total"
                            UniqueName="Total" DataFormatString="{0:C}">
                            <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="100px" FilterControlWidth="100px" ReadOnly="true"
                            SortExpression="FinsimpTotalUnits" ShowFilterIcon="true" HeaderText="FINSIMP Total Holding"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                            DataField="Total" UniqueName="FinsimpTotalUnits" DataFormatString="{0:N4}">
                            <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="100px" FilterControlWidth="100px" ReadOnly="true"
                            SortExpression="FinsimpTotalCash" ShowFilterIcon="true" HeaderText="DIFF" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" HeaderButtonType="TextButton" DataField="Total"
                            UniqueName="FinsimpTotalCash" DataFormatString="{0:N4}">
                            <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn UniqueName="selec" HeaderText="" ShowFilterIcon="false"
                            AllowFiltering="false" HeaderStyle-Width="70px">
                            <ItemTemplate>
                                <asp:LinkButton ID="Definition" runat="server" Text="Select" OnClientClick='<%#String.Format("{0}\"{1}{2}\")", "javascript:window.open(","ClientViews/ClientMainView.aspx?INS=",Eval("ClientCid")) %>'></asp:LinkButton>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="cmdSend" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
