﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    EnableEventValidation="false" AutoEventWireup="true" CodeBehind="UnlinkedAccounts.aspx.cs"
    Inherits="eclipseonlineweb.UnlinkedAccounts" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc1" TagName="breadcrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="4%">
                        <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                            Visible="False" ID="btnDownload" />
                    </td>
                    <td width="100%" class="breadcrumbgap">
                        <uc1:breadcrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <asp:Label Font-Bold="true" runat="server" ID="lblSecurities" Text="Unlinked Accounts"></asp:Label>
                        <asp:HiddenField ID="hfSecID" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <ajaxToolkit:Accordion ID="MyAccordion" runat="server" SelectedIndex="0" FadeTransitions="true"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" HeaderCssClass="accordionHeader"
        ContentCssClass="accordionContent">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                <Header>
                    <a href="" onclick="return false;" class="accordionLink" style="color: #3E3B6A">Unlinked
                        Bank Accounts</a>
                    <%--                                <asp:Button ID="BankExcelBtn" Width="150px" Text="Export to Excel" HorizontalAlign="Right" OnClick="ButtonExcel_Click"  runat="server"></asp:Button>
                    --%>
                    <asp:ImageButton runat="server" ImageUrl="~/images/excel_16.png" OnClick="ButtonExcel_Click"
                        Visible="true" ID="BankExcelBtn" />
                </Header>
                <Content>
                    <telerik:RadGrid ID="BankAccounts" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                        PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                        GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                        Width="100%" AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false"
                        OnNeedDataSource="BankAccounts_OnNeedDataSource" OnItemCommand="AccountGrid_ItemCommand">
                        <PagerStyle Mode="NumericPages"></PagerStyle>
                        <MasterTableView AutoGenerateColumns="false">
                            <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false"
                                ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                            <Columns>
                                <telerik:GridBoundColumn SortExpression="CID" ReadOnly="true" HeaderText="CID" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="CID" UniqueName="CID" Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="AccountName"
                                    HeaderStyle-Width="25%" ItemStyle-Width="25%" HeaderText="Account Name" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="AccountName" UniqueName="AccountName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                                    SortExpression="AccountType" HeaderText="Account Type" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="AccountType" UniqueName="AccountType" ReadOnly="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="80px" ReadOnly="true" HeaderStyle-Width="10%"
                                    ItemStyle-Width="10%" SortExpression="BSBNo" HeaderText="BSB No" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="false" DataField="BSBNo" UniqueName="BSBNo">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                                    SortExpression="AccountNo" HeaderText="Account No" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="AccountNo" UniqueName="AccountNo" ReadOnly="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                                    SortExpression="Institution" HeaderText="Institution" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="Institution" UniqueName="Institution" ReadOnly="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                                    SortExpression="TotalAmount" HeaderText="TotalAmount" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="TotalAmount" UniqueName="TotalAmount" ReadOnly="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                                    SortExpression="Status" HeaderText="Status" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Status" UniqueName="Status"
                                    ReadOnly="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridButtonColumn ConfirmText="Are you sure you want to delete this account from system?"
                                    ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                    <HeaderStyle Width="60px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                                </telerik:GridButtonColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <br />
                    <asp:Label ID="TextMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server">
                <Header>
                    <a href="" onclick="return false;" class="accordionLink" style="color: #3E3B6A">Unlinked
                        ASX Accounts</a>
                    <asp:ImageButton runat="server" ImageUrl="~/images/excel_16.png" OnClick="ButtonExcel_Click"
                        Visible="true" ID="ASXExcelBtn" />
                </Header>
                <Content>
                    <telerik:RadGrid ID="ASXAccounts" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                        PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                        GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                        Width="100%" AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false"
                        OnNeedDataSource="ASXAccounts_OnNeedDataSource" OnItemCommand="AccountGrid_ItemCommand">
                        <PagerStyle Mode="NumericPages"></PagerStyle>
                        <MasterTableView AutoGenerateColumns="false">
                            <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false"
                                ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                            <Columns>
                                <telerik:GridBoundColumn SortExpression="CID" ReadOnly="true" HeaderText="CID" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="CID" UniqueName="CID" Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="AccountName"
                                    HeaderStyle-Width="25%" ItemStyle-Width="25%" HeaderText="Account Name" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="AccountName" UniqueName="AccountName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="AccountDesignation"
                                    HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderText="Account Designation"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="AccountDesignation" UniqueName="AccountDesignation">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="70px" SortExpression="AccountNo" HeaderStyle-Width="15%"
                                    ItemStyle-Width="15%" HeaderText="Account No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountNo" UniqueName="AccountNo">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="70px" SortExpression="HINNumber" HeaderStyle-Width="15%"
                                    ItemStyle-Width="25%" HeaderText="HIN Number" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="HINNumber" UniqueName="HINNumber">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                                    SortExpression="UnitsHeld" HeaderText="UnitsHeld" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="UnitsHeld" UniqueName="UnitsHeld" ReadOnly="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridButtonColumn ConfirmText="Are you sure you want to delete this account from system?"
                                    ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                    <HeaderStyle Width="60px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                                </telerik:GridButtonColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPane3" runat="server">
                <Header>
                    <a href="" onclick="return false;" class="accordionLink" style="color: #3E3B6A">Unlinked
                        MIS Accounts</a>
                    <asp:ImageButton runat="server" ImageUrl="~/images/excel_16.png" OnClick="ButtonExcel_Click"
                        Visible="true" ID="MISExcelBtn" />
                </Header>
                <Content>
                    <telerik:RadGrid ID="MISAccounts" runat="server" AutoGenerateColumns="True" PageSize="20"
                        AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" GridLines="Horizontal"
                        AllowFilteringByColumn="True" EnableViewState="False" ShowFooter="True" OnNeedDataSource="MISAccounts_OnNeedDataSource"
                        OnItemCommand="AccountGrid_ItemCommand">
                        <PagerStyle Mode="NumericPages"></PagerStyle>
                        <MasterTableView AutoGenerateColumns="false">
                            <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false"
                                ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                            <Columns>
                                <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="FundCode" HeaderStyle-Width="50%"
                                    ItemStyle-Width="50%" HeaderText="Fund Code" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="FundCode" UniqueName="FundCode">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="50%" ItemStyle-Width="50%"
                                    SortExpression="FundDescription " HeaderText="Fund Description" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="FundDescription" UniqueName="FundDescription" ReadOnly="true">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
</asp:Content>
