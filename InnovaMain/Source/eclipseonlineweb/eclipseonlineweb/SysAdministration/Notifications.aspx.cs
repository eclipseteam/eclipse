﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.SM.Workflows;
using Oritax.TaxSimp.SM.Workflows.Data;

namespace eclipseonlineweb
{
    public partial class Notifications : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void GetBMCData()
        {
            IBrokerManagedComponent bmc = this.UMABroker.CreateTransientComponentInstance(new Guid("15DE2644-C1A0-41D5-8293-2794EF31840D"));
            WorkflowsDS workflowsDS = new WorkflowsDS();
            bmc.GetData(workflowsDS);
            this.PresentationData = workflowsDS;
        }

        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
        }

        public override void LoadPage()
        {
            base.LoadPage();

            if (Request.QueryString["ID"] != null)
                this.cid = Request.QueryString["ID"].ToString();
            else
                this.cid = ""; 

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
                ((SetupMaster)Master).IsAdmin = "1";
        }

        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e) { }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "download")
            {
                string[] IdIns = e.CommandArgument.ToString().Split(',');
                Response.Redirect("~/SysAdministration/Notifications.aspx?ID=" + IdIns[0].ToString() + "&INS=" + IdIns[1].ToString() +"&PageName=" + "Notification");
            }
        }

        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) { }
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e) { }
        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
         
        }

        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e) { }
        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                string downloadFileURL = "https://" + this.Request.Url.Host + this.Request.ApplicationPath + "/" + ((Telerik.Web.UI.GridDataItem)(e.Item))["FileDownLoadURLDownloadData"].Text;
                HtmlAnchor linkButton = ((Telerik.Web.UI.GridDataItem)(e.Item))["FileDownLoadURLDownload"].Controls[1] as HtmlAnchor;
                linkButton.Attributes.Add("href", downloadFileURL);
            }
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DataView View = new DataView(this.PresentationData.Tables[WorkflowsDS.NotificationsTable]);
            View.Sort = "CreatedDate DESC";
    
            if(cid != string.Empty)
                View.RowFilter ="WorkflowID ='"+cid+"'";
            
            this.PresentationGrid.DataSource = View.ToTable();
        }
    }
}
