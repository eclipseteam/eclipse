﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using eclipseonlineweb.WebControls;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Commands;

using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using Oritax.TaxSimp.Data;


namespace eclipseonlineweb
{
    public partial class ImportMacquarieDataFeed : UMABasePage
    {

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void Intialise()
        {
            ImportMessageControl.GridImportMessages.ColumnCreated += GridImportMessages_ColumnCreated;
            ImportMessageControl.GirdInvestmentCodeError.ColumnCreated += GridImportErrorMessages_ColumnCreated;
            ImportMessageControl.GridImportMessages.ItemDataBound += GridImportMessages_ItemDataBound;

            if (!IsPostBack)
            {
                ImportMessageControl.Clear();
            }

            ImportMessageControl.Completed += text =>
            {
                switch (text.ToLower())
                {
                    case "next":
                        SendData();
                        break;
                    default:
                        ClearMessageControls();
                        break;
                }
            };


        }
        public void ClearMessageControls()
        {
            ImportMessageControl.Clear();
            ImportMessageControl.TextMessage.Text = "";
        }
        private void GridImportMessages_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item).ItemIndex == -1) return;

            DataRow dr = ((DataRowView)((e.Item).DataItem)).Row;
            if (bool.Parse(dr["HasErrors"].ToString()))
            {
                e.Item.ForeColor = Color.Red;
            }
        }

        void GridImportMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {

            GridImportMessages_ColumnCreated(e);
        }

        void GridImportErrorMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            GridImportMessages_ColumnCreated(e);
        }


        protected void btnImport_OnClick(object sender, EventArgs e)
        {
            ProcessFile();
        }

        private void ProcessFile()
        {
            ImportProcessDS ds = new ImportProcessDS();
            if (this.InputEndDate.DateInput.SelectedDate.HasValue)
                ds.EndDate = this.InputEndDate.DateInput.SelectedDate.Value;
            else
                ds.EndDate = DateTime.Now;

            ds.Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.MaquarieBankTransactionsImport;
            SaveOrganizanition(ds);
            SetCompletionGird(ds);
            PresentationData = ds;
        }

        private void SetResultGird(ImportProcessDS ds)
        {
            this.lblMessageCode.Text = ds.MessageNumber;
            this.lblMessageSummary.Text = ds.MessageSummary;
            this.lblMessageDetails.Text = ds.MessageDetail;
        }

        private void ProcessFile(bool isFile)
        {
            txtIsFile.Value = isFile.ToString();
            ImportProcessDS ds = new ImportProcessDS();
            ds.Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ValidateMaquarieBankTransactions;
            ds.UseFile = isFile;
            if (isFile)
            {
                if (!ReadFromFile(ds))
                {
                    lblMessageCode.Text = "Couldn't read file or File not valid";
                    return;
                }
            }

            SaveOrganizanition(ds);

            var importFolder = Server.MapPath("~/App_Data/Import/MacquireTransactions/");
            if (!Directory.Exists(importFolder))
            {
                Directory.CreateDirectory(importFolder);
            }
            SetValidationGird(ds);
            PresentationData = ds;
            string filename = ImportMessageControl.RefreshFileID() + ".xml";
            ds.WriteXml(importFolder + filename, XmlWriteMode.WriteSchema);
            PresentationData = ds;
        }

        private void SendData()
        {
            var distributionDictory = Server.MapPath("~/App_Data/Import/MacquireTransactions/");
            if (!Directory.Exists(distributionDictory))
            {
                Directory.CreateDirectory(distributionDictory);
            }

            ImportProcessDS ds = new ImportProcessDS();

            bool useFile = bool.Parse(txtIsFile.Value);
            if (useFile)
            {
                string filename = distributionDictory + ImportMessageControl.GetFileID() + ".xml";
                ds.FileName = filename;

                ds.ReadXml(filename);
                if (ds.Tables[0].Columns.Contains("Message"))
                    ds.Tables[0].Columns.Remove("Message");
                if (ds.Tables[0].Columns.Contains("HasErrors"))
                    ds.Tables[0].Columns.Remove("HasErrors");
                if (ds.Tables[0].Columns.Contains("IsMissingItem"))
                    ds.Tables[0].Columns.Remove("IsMissingItem");
            }
            ds.UseFile = useFile;
            ds.Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.MaquarieBankTransactionsImport;
            SaveOrganizanition(ds);
            SetCompletionGird(ds);
        }

        private void SetCompletionGird(ImportProcessDS ds)
        {
            this.lblMessageCode.Text = ds.MessageNumber;
            this.lblMessageSummary.Text = ds.MessageSummary;
            this.lblMessageDetails.Text = ds.MessageDetail;
            ImportMessageControl.Clear();
            if (ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                using (DataView dv = new DataView(dt))
                {
                    ImportMessageControl.GridImportMessages.DataSource = dv;
                    ImportMessageControl.GridImportMessages.DataBind();
                }

                using (DataView dv = new DataView(dt))
                {
                    dv.RowFilter = "IsMissingItem='true'";
                    DataTable missingItems = new DataTable();
                    missingItems.Columns.Add("MissingItem");
                    DataRow dr;
                    foreach (DataRow row in dv.ToTable().Rows)
                    {
                        dr = missingItems.NewRow();
                        dr["MissingItem"] = row["AccountNumber"];
                        missingItems.Rows.Add(dr);
                    }

                    if (missingItems.Rows.Count > 0)
                    {
                        ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                        ImportMessageControl.GirdInvestmentCodeError.DataBind();

                        ImportMessageControl.TextMessage.Text = ImportMessages.sMissingAccountsIgnored;
                    }
                    else
                    {
                        ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                        ImportMessageControl.GirdInvestmentCodeError.DataBind();
                        ImportMessageControl.TextMessage.Text = "";
                    }
                }

                int successfulRows = dt.Select("HasErrors='false'").Length;
                ImportMessageControl.SetButtons(ButtonTypes.FINISH);
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sImportedSuccessfully, successfulRows, dt.Rows.Count - successfulRows), ImportMessageType.Sucess);
            }
        }

        private void SetValidationGird(ImportProcessDS ds)
        {
            this.lblMessageCode.Text = ds.MessageNumber;
            this.lblMessageSummary.Text = ds.MessageSummary;
            this.lblMessageDetails.Text = ds.MessageDetail;
            //ImportMessageControl.Clear();
            if (ds.Tables.Count > 0)
            {
                using (DataView dv = new DataView(ds.Tables[0]))
                {
                    ImportMessageControl.GridImportMessages.DataSource = dv;
                    ImportMessageControl.GridImportMessages.DataBind();
                }
                using (DataView dv = new DataView(ds.Tables[0]))
                {
                    dv.RowFilter = "IsMissingItem='true'";
                    DataTable missingItems = new DataTable();
                    missingItems.Columns.Add("MissingItem");
                    DataRow dr;
                    foreach (DataRow row in dv.ToTable().Rows)
                    {
                        dr = missingItems.NewRow();
                        dr["MissingItem"] = row["AccountNumber"];
                        missingItems.Rows.Add(dr);
                    }

                    if (missingItems.Rows.Count > 0)
                    {
                        ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                        ImportMessageControl.GirdInvestmentCodeError.DataBind();

                        ImportMessageControl.TextMessage.Text = ImportMessages.sMissingAccountsIgnored;
                    }
                    else
                    {
                        ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                        ImportMessageControl.GirdInvestmentCodeError.DataBind();
                        ImportMessageControl.TextMessage.Text = "";
                    }
                }
                int successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
                if (ds.Tables[0].Rows.Count == 0 || successfulRows == 0)
                {
                    ImportMessageControl.SetButtons(ButtonTypes.CLOSE);
                    ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sErrorsCannotContinue, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Error);
                    ImportMessageControl.TextMessage.Text = "";
                }
                else
                {
                    ImportMessageControl.SetButtons(ButtonTypes.NEXTCANCEL);
                    //sucess
                    ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sValidationCompleted, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
                }
            }

        }
        private bool ReadFromFile(ImportProcessDS ds)
        {
            bool result = false;
            if (filMyFile.PostedFile != null)
            {
                // Get a reference to PostedFile object
                HttpPostedFile myFile = filMyFile.PostedFile;

                // Get size of uploaded file
                int nFileLen = myFile.ContentLength;

                // make sure the size of the file is > 0
                if (nFileLen > 0)
                {
                    // Allocate a buffer for reading of the file
                    string fileName = Path.GetFileName(myFile.FileName);
                    var date = DateTime.Now;
                    var importFolder = Server.MapPath("~/App_Data/Import/MacquireTransactions/");
                    if (!Directory.Exists(importFolder))
                    {
                        Directory.CreateDirectory(importFolder);
                    }

                    string fileLocation = importFolder + date.ToString("ddMMyyhhmmsstt") + fileName;
                    byte[] myData = new byte[nFileLen];

                    // Read uploaded file from the Stream
                    myFile.InputStream.Read(myData, 0, nFileLen);

                    // Write data into a file
                    Utilities.WriteToFile(fileLocation, ref myData);

                    string csv = string.Empty;
                    using (StreamReader reader = new StreamReader(fileLocation, true))
                    {
                        csv = reader.ReadToEnd();
                        reader.Close();

                    }

                    ds.Tables.Add(ConversorTransactionCsv.CreateMacquireTransactioList(csv));
                    ds.FileName = fileName;

                    result = true;
                }
            }
            return result;
        }

        protected void BtnUploadFileClick(object sender, EventArgs e)
        {
            ProcessFile(true);
        }

        protected void GridImportMessages_ColumnCreated(GridColumnCreatedEventArgs e)
        {
            string colName = e.Column.UniqueName.ToLower();
            switch (colName)
            {
                case "message":
                    e.Column.HeaderText = "Message";
                    break;
                case "haserrors":
                    e.Column.HeaderText = "Has Errors";
                    e.Column.Visible = false;
                    break;
                case "ismissingitem":
                    e.Column.HeaderText = "Is Missing Item";
                    e.Column.Visible = false;
                    break;
                case "missingitem":
                    e.Column.HeaderText = "Missing Investment Code";
                    break;
                case "accountnumber":
                case "transactiondate":
                case "reversalflag":
                case "debitcredit":
                case "amount":
                case "narrative":
                case "transactiontype":
                case "systemtransactiontype":
                case "importtransactiontypedetail":
                    e.Column.Visible = true;
                    break;
                default:
                    e.Column.Visible = false;
                    break;
            }
        }
    }
}