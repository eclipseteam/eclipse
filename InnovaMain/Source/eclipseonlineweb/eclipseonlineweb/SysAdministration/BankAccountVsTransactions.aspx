﻿<%@ Page Title="e-Clipse Online Portal" EnableViewState="true" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="BankAccountVsTransactions.aspx.cs" Inherits="eclipseonlineweb.BankAccountVsTransactions" %>

<%@ Register TagPrefix="uc1" TagName="breadcrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .RadGrid_Metro .rgHeader, .RadGrid_Metro .rgHeader a
        {
            font-size: 8pt !important;
        }
        .RadGrid_Metro .rgRow a, .RadGrid_Metro .rgAltRow a, .RadGrid_Metro tr.rgEditRow a, .RadGrid_Metro .rgFooter a, .RadGrid_Metro .rgEditForm a
        {
            font-size: 8pt !important;
        }
    </style>
    <script type="text/javascript">
        function doFilter(sender, eventArgs, colUniqueName) {
            if (eventArgs.keyCode == 13) {
                eventArgs.cancelBubble = true;
                eventArgs.returnValue = false;
                if (eventArgs.stopPropagation) {
                    eventArgs.stopPropagation();
                    eventArgs.preventDefault();
                }
                var masterTableView = $find("<%: PresentationGrid.ClientID %>").get_masterTableView();
                var uniqueName = colUniqueName; // set Column UniqueName for the filtering TextBox
                masterTableView.filter(uniqueName, sender.value, Telerik.Web.UI.GridFilterFunction.CurrentKnownFunction);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                                ID="btnDownload" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:breadcrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Bank Account Holdings vs. Transactions"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid OnNeedDataSource="PresentationGrid_OnNeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true" GridLines="None"
                AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" EnableLinqExpressions="False"
                OnItemCreated="PresentationGrid_OnItemCreated">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="BankAccVsTransactions" TableLayout="Fixed" Font-Size="8" FilterExpression="([Difference] <> 0)">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <Columns>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="CLIENTCID" HeaderText="CLIENTCID" AutoPostBackOnFilter="false"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="CLIENTCID" UniqueName="CLIENTCID" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridHyperLinkColumn FilterControlWidth="50px" HeaderStyle-Width="90px" DataTextFormatString="{0}"
                            DataNavigateUrlFields="CLIENTCID" SortExpression="ClientID" UniqueName="ClientID" DataNavigateUrlFormatString="../ClientViews/ClientMainView.aspx?ins={0}"
                            HeaderText="Client ID" DataTextField="ClientID" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                            HeaderButtonType="TextButton" ShowFilterIcon="true">
                        </telerik:GridHyperLinkColumn>
                        <telerik:GridBoundColumn FilterControlWidth="330px" HeaderStyle-Width="370px" SortExpression="ClientName"
                            ReadOnly="true" HeaderText="Client Name" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ClientName"
                            UniqueName="ClientName" Visible="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="330px" HeaderStyle-Width="370px" SortExpression="Name"
                            ReadOnly="true" HeaderText="Account Name" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Name" UniqueName="Name">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" SortExpression="CustomerNO"
                            ReadOnly="true" HeaderText="Customer NO" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CustomerNO"
                            UniqueName="CustomerNO">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" SortExpression="BSB"
                            ReadOnly="true" HeaderText="BSB" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BSB" UniqueName="BSB">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" SortExpression="AccountNumber"
                            ReadOnly="true" HeaderText="Account Number" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountNumber"
                            UniqueName="AccountNumber">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ItemStyle-HorizontalAlign="Right" FilterControlWidth="60px"
                            HeaderStyle-Width="100px" ReadOnly="true" SortExpression="Holding" HeaderText="Holding"
                            AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="Holding" UniqueName="Holding" DataFormatString="{0:C}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ItemStyle-HorizontalAlign="Right" FilterControlWidth="80px"
                            HeaderStyle-Width="120px" ReadOnly="true" SortExpression="Transaction"
                            HeaderText="System Transaction" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Transaction"
                            UniqueName="Transaction" DataFormatString="{0:C}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ItemStyle-HorizontalAlign="Right" FilterControlWidth="60px"
                            HeaderStyle-Width="100px" ReadOnly="true" SortExpression="Difference" HeaderText="Difference"
                            AutoPostBackOnFilter="false" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Difference" UniqueName="Difference" CurrentFilterFunction="NotEqualTo"
                            CurrentFilterValue="0" DataFormatString="{0:C}">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
