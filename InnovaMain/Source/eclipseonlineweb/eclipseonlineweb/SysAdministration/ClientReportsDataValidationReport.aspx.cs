﻿using System;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;


namespace eclipseonlineweb.SysAdministration
{
    public partial class ClientReportsDataValidationReport : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        public override void LoadPage()
        {
            if (!IsPostBack)
            {
                InputStartDate.SelectedDate = new DateTime(DateTime.Now.Year - 1, 07, 1);
                InputEndDate.SelectedDate = new DateTime(DateTime.Now.Year, 06, 30);

            }
        }


        private void GetData()
        {
            var orgCm = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var transferInReportDs = new Oritax.TaxSimp.DataSets.ClientReportDataValidationDS();
            transferInReportDs.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            var selectedDate = this.InputStartDate.DateInput.SelectedDate;
            if (selectedDate != null)
            {
                transferInReportDs.StartDate = selectedDate.Value.Date;

            }
            if (InputEndDate.DateInput.SelectedDate != null)
            {
                transferInReportDs.EndDate = InputEndDate.DateInput.SelectedDate.Value.Date;
            }
           
            if (!chkAllClients.Checked)
            {
                transferInReportDs.Count = Convert.ToInt32(txtClientCount.Text);
            }

            transferInReportDs.Command = (int)WebCommands.GetAllClientDetails;
            if (orgCm != null)
            {

                orgCm.GetData(transferInReportDs);
                UMABroker.ReleaseBrokerManagedComponent(orgCm);

                var veiw = transferInReportDs.ClientReportShareTable.DefaultView;
                lblClientCount.Text = transferInReportDs.ClientCount.ToString();
                PresentationGrid.DataSource = veiw;

            }


        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (IsPostBack)
                GetData();
        }

        protected void GenerateReport(object sender, EventArgs e)
        {
            PresentationGrid.Rebind();
        }
    }
}