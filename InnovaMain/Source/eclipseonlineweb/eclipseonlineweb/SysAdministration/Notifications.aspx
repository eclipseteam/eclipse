﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="Notifications.aspx.cs" Inherits="eclipseonlineweb.Notifications" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="4%">
                        <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                            ID="btnDownload" />
                    </td>
                    <td width="100%" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <asp:Label Font-Bold="true" runat="server" ID="lblSecurities" Text="Notifications"></asp:Label>
                        <asp:HiddenField ID="hfSecID" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="false" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
                OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
                OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
                OnItemDataBound="PresentationGrid_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="WorkflowID" AllowMultiColumnSorting="True" Width="100%"
                    CommandItemDisplay="Top" Name="Workflows" TableLayout="Fixed" EditMode="EditForms" >
                    <CommandItemSettings  ShowAddNewRecordButton="false" ></CommandItemSettings>
                    <Columns>
                     <telerik:GridBoundColumn ReadOnly="true" Visible="false" DataField="NotificationID"
                            UniqueName="NotificationID" />
                             <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="FileDownLoadURLDownload"
                            UniqueName="FileDownLoadURLDownloadData" />
                        <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                            SortExpression="ClientID" HeaderText="Client ID" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="ClientID" UniqueName="ClientID" ReadOnly="true">
                        </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="20%" ItemStyle-Width="20%"
                            SortExpression="ClientName" HeaderText="Client Name" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="ClientName" UniqueName="ClientName" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                            SortExpression="NotificationName" HeaderText="Notification Name" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="NotificationName" UniqueName="NotificationName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="20%" ItemStyle-Width="20%"
                            SortExpression="NotificationDescription" HeaderText="Notification Desc" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="NotificationDescription" UniqueName="NotificationDescription">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="25%" ItemStyle-Width="25%"
                            SortExpression="NotificationDetails" HeaderText="Details" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="NotificationDetails" UniqueName="NotificationDetails">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                            SortExpression="CreatedDate" HeaderText="Created Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedDate" UniqueName="CreatedDate"
                            ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn UniqueName="FileDownLoadURLDownload" HeaderText="" ShowFilterIcon="false"
                            ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center" AllowFiltering="false"
                            HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <a id="Download" runat="server">
                                    <img src="../images/attach.png" /></a>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="Html"></Excel>
                </ExportSettings>
            </telerik:RadGrid>
             <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorListOfWorkflowType"
                runat="server" DropDownStyle-Width="200px">
            </telerik:GridDropDownListColumnEditor>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
