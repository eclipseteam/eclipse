﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="BulkOrders.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.BulkOrders" %>
<%@ Import Namespace="Oritax.TaxSimp.Common" %>
<%@ Import Namespace="Oritax.TaxSimp.Data" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .RadGrid_Metro .rgHeader, .RadGrid_Metro .rgHeader a
        {
            font-size: 8pt !important;
        }
        .RadGrid_Metro .rgRow a, .RadGrid_Metro .rgAltRow a, .RadGrid_Metro tr.rgEditRow a, .RadGrid_Metro .rgFooter a, .RadGrid_Metro .rgEditForm a
        {
            font-size: 8pt !important;
        }
        .RadGrid .rgDetailTable
        {
            background-color: white !important;
        }
        .rbPrimaryIcon
        {
            left: 4px !important;
            top: 0 !important;
        }
    </style>
    <script src="../Scripts/GridScripts.js" type="text/javascript"></script>
    <script type="text/javascript">
        function change(index, type) {
            var x = confirm('Are you sure you want to complete this order?');
            if (x) {
                var grid = $find("<%:PresentationGrid.ClientID %>");
                var masterTable = grid.get_masterTableView();
                traverseChildTables(masterTable, index, type);
                return true;
            } else {
                return false;
            }

        }

        function traverseChildTables(gridTableView, index, type) {
            var dataItems = gridTableView.get_dataItems();
            for (var i = 0; i < dataItems.length; i++) {
                if (dataItems[i].get_nestedViews().length > 0) {
                    var nestedView = dataItems[i].get_nestedViews()[0];
                    //here you can access the nested table's data items using nestedView.get_dataItems()
                    var row = nestedView.get_dataItems()[index];

                    if (type == 'ASX') {
                        var cvPrice = row.findElement("cvPrice");
                        ValidatorEnable(cvPrice, true);
                        var reqPrice = row.findElement("reqPrice");
                        ValidatorEnable(reqPrice, true);
                        var cvUnits = row.findElement("cvUnits");
                        ValidatorEnable(cvUnits, true);
                        var reqUnits = row.findElement("reqUnits");
                        ValidatorEnable(reqUnits, true);
                    }
                    var reqNote = row.findElement("reqNote");
                    ValidatorEnable(reqNote, true);
                }
            }
        }

        function CheckAll(checkBox, cbID) {
            CheckAllCheckBoxes(checkBox, "<%:PresentationGrid.ClientID %>", cbID);
        }

        function CheckSingle(colId,ChkID) {
            CheckSingleCheckBox(colId, "<%:PresentationGrid.ClientID %>", ChkID);
        }

        function CheckSelected(ColID) {
            return CheckSelectedCheckBoxes("<%:PresentationGrid.ClientID %>", ColID);
        }

        function CheckSelectedExport(sender, args) {
            var isSelected = CheckSelected("chkSelect");
            var isConfirmed = false;
            if (!isSelected) {
                alert("Please select some orders first.");
            }
            else {
                isConfirmed = true;
            }
            sender.set_autoPostBack(isConfirmed);
        }



        function CheckSelectedSMAExport(sender, args) {
            var isSelected = CheckSelected("chkSelect1");
            var isConfirmed = false;
            if (!isSelected) {
                alert("Please select some orders first.");
            }
            else {
                isConfirmed = true;
            }
            sender.set_autoPostBack(isConfirmed);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="55%">
                            <telerik:RadButton runat="server" ID="btnExportOrders" ToolTip="Export Orders" OnClick="btnExportOrders_OnClick"
                                OnClientClicked="CheckSelectedExport">
                                <ContentTemplate>
                                    <img src="../images/download.png" alt="" class="btnImageWithText" />
                                    <span class="riLabel">Export Order </span>
                                </ContentTemplate>
                            </telerik:RadButton>
                            &nbsp;
                            <telerik:RadButton runat="server" ID="btnShowExportedOrders" Text="Show Exported Bulk Orders Files"
                                OnClick="btnShowExportedOrders_OnClick">
                            </telerik:RadButton>
                            &nbsp;
                            <telerik:RadButton runat="server" ID="btnSubmitBulkOrders" Text="Submit Bulk Orders" OnClick="btnSubmitBulkOrders_OnClick" Visible="false">
                            </telerik:RadButton>
                            &nbsp;
                            <telerik:RadButton ID="btnUMA" runat="server" ToggleType="Radio" ButtonType="ToggleButton"
                                Text="UMA" Value="UMA" GroupName="ClientManagementType" Checked="true" OnClick="btnUMA_OnClick">
                            </telerik:RadButton>
                            <telerik:RadButton ID="btnSMA" runat="server" ToggleType="Radio" Text="Super" Value="SMA" GroupName="ClientManagementType"
                                ButtonType="ToggleButton" OnClick="btnSMA_OnClick">
                            </telerik:RadButton>
                            &nbsp;
                            <telerik:RadButton runat="server" ID="btnBulkExportSMA" Text="Export to Super" OnClick="btnBulkExportSMA_Click" OnClientClicked="CheckSelectedSMAExport">
                            </telerik:RadButton>
                        </td>
                        <td width="45%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Bulk Orders"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid OnNeedDataSource="PresentationGridNeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGridDetailTableDataBind"
                OnItemCommand="PresentationGridItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                EnableViewState="true" ShowFooter="true" OnItemDataBound="PresentationGridItemDataBound"
                OnItemCreated="PresentationGrid_OnItemCreated">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="BulkOrders" TableLayout="Fixed" Font-Size="8">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <DetailTables>
                        <telerik:GridTableView Name="BulkOrderItems" Width="100%" Font-Size="8" AllowFilteringByColumn="False">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <DetailTables>
                                <telerik:GridTableView Name="BulkOrderItemDetails" Width="100%" Font-Size="8" AllowFilteringByColumn="False">
                                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                                    <Columns>
                                        <telerik:GridBoundColumn SortExpression="BulkOrderID" ReadOnly="true" HeaderText="BulkOrderID"
                                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                            DataField="BulkOrderID" UniqueName="BulkOrderID" Display="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn SortExpression="BulkOrderItemID" ReadOnly="true" HeaderText="BulkOrderItemID"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                            HeaderButtonType="TextButton" DataField="BulkOrderItemID" UniqueName="BulkOrderItemID"
                                            Display="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn SortExpression="OrderID" ReadOnly="true" HeaderText="OrderID"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                            HeaderButtonType="TextButton" DataField="OrderID" UniqueName="OrderID" Display="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn SortExpression="OrderItemID" ReadOnly="true" HeaderText="OrderItemID"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                            HeaderButtonType="TextButton" DataField="OrderItemID" UniqueName="OrderItemID"
                                            Display="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridHyperLinkColumn DataTextFormatString="{0}" SortExpression="OrderNo"
                                            UniqueName="OrderNo" HeaderText="Order No" DataTextField="OrderNo" AutoPostBackOnFilter="true"
                                            CurrentFilterFunction="Contains" HeaderButtonType="TextButton" ShowFilterIcon="true">
                                        </telerik:GridHyperLinkColumn>
                                        <telerik:GridBoundColumn SortExpression="OrderStatus" ReadOnly="true" HeaderText="OrderStatus"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                            HeaderButtonType="TextButton" DataField="OrderStatus" UniqueName="OrderStatus"
                                            Display="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="ClientCID"
                                            SortExpression="ClientID" UniqueName="ClientID" DataNavigateUrlFormatString="../ClientViews/ClientMainView.aspx?ins={0}"
                                            HeaderText="Client ID" DataTextField="ClientID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                            HeaderButtonType="TextButton" ShowFilterIcon="true">
                                        </telerik:GridHyperLinkColumn>
                                        <telerik:GridBoundColumn SortExpression="OrderItemType" ReadOnly="true" HeaderText="Item Type"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                            HeaderButtonType="TextButton" DataField="OrderItemType" UniqueName="OrderItemType">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn SortExpression="Code" ReadOnly="true" HeaderText="Code"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                            HeaderButtonType="TextButton" DataField="Code" UniqueName="Code">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn SortExpression="Currency" ReadOnly="true" HeaderText="Currency"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                            HeaderButtonType="TextButton" DataField="Currency" UniqueName="Currency">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn SortExpression="UnitPrice" ReadOnly="true" HeaderText="Suggested Unit Price"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                            HeaderButtonType="TextButton" DataField="UnitPrice" UniqueName="UnitPrice" DataFormatString="{0:N6}">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn SortExpression="Units" ReadOnly="true" HeaderText="Suggested Units"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                            HeaderButtonType="TextButton" DataField="Units" UniqueName="Units" DataFormatString="{0:N4}">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn SortExpression="BrokerName" ReadOnly="true" HeaderText="Broker"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                            HeaderButtonType="TextButton" DataField="BrokerName" UniqueName="BrokerName">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn SortExpression="InstituteName" ReadOnly="true" HeaderText="Bank"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                            HeaderButtonType="TextButton" DataField="InstituteName" UniqueName="InstituteName">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn SortExpression="Term" ReadOnly="true" HeaderText="Term"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                            HeaderButtonType="TextButton" DataField="Term" UniqueName="Term">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn SortExpression="Rate" ReadOnly="true" HeaderText="Rate"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                            HeaderButtonType="TextButton" DataField="Rate" UniqueName="Rate" DataFormatString="{0:P}">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="120px" SortExpression="Amount"
                                            ReadOnly="true" HeaderText="Amount" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Amount" UniqueName="Amount"
                                            DataFormatString="{0:C}">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn SortExpression="ActualUnitPrice" ReadOnly="true" HeaderText="Actual Unit Price"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                            HeaderButtonType="TextButton" DataField="ActualUnitPrice" UniqueName="ActualUnitPrice"
                                            DataFormatString="{0:N6}">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn SortExpression="ActualUnits" ReadOnly="true" HeaderText="Actual Units"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                            HeaderButtonType="TextButton" DataField="ActualUnits" UniqueName="ActualUnits"
                                            DataFormatString="{0:N4}">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="ActualAmount"
                                            ReadOnly="true" HeaderText="Actual Amount" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ActualAmount"
                                            UniqueName="ActualAmount" DataFormatString="{0:C}">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </telerik:GridTableView>
                            </DetailTables>
                            <Columns>
                                <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="ID" UniqueName="ID" Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="BulkOrderID" ReadOnly="true" HeaderText="BulkOrderID"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="BulkOrderID" UniqueName="BulkOrderID"
                                    Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="OrderItemIDs" ReadOnly="true" HeaderText="OrderItemIDs"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="OrderItemIDs" UniqueName="OrderItemIDs"
                                    Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="Code" ReadOnly="true" HeaderText="Code"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="Code" UniqueName="Code">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="ExpectedUnitPrice" ReadOnly="true" HeaderText="Expected Unit Price"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="ExpectedUnitPrice" UniqueName="ExpectedUnitPrice"
                                    DataFormatString="{0:N6}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="ExpectedUnits" ReadOnly="true" HeaderText="Expected Units"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="ExpectedUnits" UniqueName="ExpectedUnits"
                                    DataFormatString="{0:N4}" Aggregate="Sum">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="ExpectedAmount" ReadOnly="true" HeaderText="Expected Amount"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="ExpectedAmount" UniqueName="ExpectedAmount"
                                    DataFormatString="{0:C}" Aggregate="Sum">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn SortExpression="ActualUnitPrice" UniqueName="ActualUnitPrice"
                                    HeaderText="Actual Unit Price" HeaderStyle-Width="92px" AllowFiltering="False"
                                    DataField="ActualUnitPrice">
                                    <ItemTemplate>
                                        <telerik:RadNumericTextBox ID="txtUnitPrice" runat="server" Width="90px" Visible='<%# !(Convert.ToBoolean(Eval("IsApportioned"))) %>'>
                                            <NumberFormat DecimalDigits="6"></NumberFormat>
                                        </telerik:RadNumericTextBox><asp:CompareValidator ID="cvPrice" runat="server" ErrorMessage="Invalid Price"
                                            ControlToValidate="txtUnitPrice" ValueToCompare="0" Display="Dynamic" SetFocusOnError="true"
                                            ForeColor="Red" Operator="GreaterThan" ValidationGroup="Complete" Enabled="false">                                            
                                        </asp:CompareValidator><asp:RequiredFieldValidator ID="reqPrice" runat="server" ErrorMessage="Enter Price"
                                            ControlToValidate="txtUnitPrice" Display="Dynamic" ForeColor="Red" ValidationGroup="Complete"
                                            Enabled="false"></asp:RequiredFieldValidator><asp:Label ID="Label1" runat="server"
                                                Text='<%# string.Format("{0:N6}",Eval("ActualUnitPrice")) %>' Visible='<%# Eval("IsApportioned") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn SortExpression="ActualUnits" UniqueName="ActualUnits"
                                    HeaderText="Actual Units" HeaderStyle-Width="97px" AllowFiltering="False" DataField="ActualUnits">
                                    <ItemTemplate>
                                        <telerik:RadNumericTextBox ID="txtUnits" runat="server" Width="95px" Visible='<%# !(Convert.ToBoolean(Eval("IsApportioned"))) %>'>
                                            <NumberFormat DecimalDigits="4"></NumberFormat>
                                        </telerik:RadNumericTextBox><asp:CompareValidator ID="cvUnits" runat="server" ErrorMessage="Invalid Units"
                                            ControlToValidate="txtUnits" ValueToCompare="0" Display="Dynamic" SetFocusOnError="true"
                                            ForeColor="Red" Operator="GreaterThan" ValidationGroup="Complete" Enabled="false"></asp:CompareValidator><asp:RequiredFieldValidator
                                                ID="reqUnits" runat="server" ErrorMessage="Enter Units" ControlToValidate="txtUnits"
                                                Display="Dynamic" ForeColor="Red" ValidationGroup="Complete" Enabled="false"></asp:RequiredFieldValidator>
                                        <asp:Label ID="Label2" runat="server" Text='<%# string.Format("{0:N4}",Eval("ActualUnits")) %>'
                                            Visible='<%# Eval("IsApportioned") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn SortExpression="ActualAmount" ReadOnly="true" HeaderText="Actual Amount"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="ActualAmount" UniqueName="ActualAmount"
                                    DataFormatString="{0:C}" Aggregate="Sum">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn UniqueName="Brokerage" HeaderText="Brokerage" AllowFiltering="False"
                                    HeaderStyle-Width="112px" SortExpression="Brokerage" DataField="Brokerage" Visible="false">
                                    <ItemTemplate>
                                        <telerik:RadNumericTextBox ID="txtBrokerage" runat="server" Width="110px" Type="Currency"
                                            Visible='<%# !(Convert.ToBoolean(Eval("IsApportioned"))) %>'>
                                        </telerik:RadNumericTextBox><asp:CompareValidator ID="cvBrokerage" runat="server"
                                            ErrorMessage="Invalid Brokerage" ControlToValidate="txtBrokerage" ValueToCompare="0"
                                            Display="Dynamic" SetFocusOnError="true" ForeColor="Red" Operator="GreaterThanEqual"
                                            ValidationGroup="Complete" Enabled="false">                                            
                                        </asp:CompareValidator><asp:Label ID="LabelBrokerage" runat="server" Text='<%# string.Format("{0:C}",Eval("Brokerage")) %>'
                                            Visible='<%# Eval("IsApportioned") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn UniqueName="Charges" HeaderText="Charges" AllowFiltering="False"
                                    HeaderStyle-Width="112px" SortExpression="Charges" DataField="Charges" Visible="false">
                                    <ItemTemplate>
                                        <telerik:RadNumericTextBox ID="txtCharges" runat="server" Width="110px" Type="Currency"
                                            Visible='<%# !(Convert.ToBoolean(Eval("IsApportioned"))) %>'>
                                        </telerik:RadNumericTextBox><asp:CompareValidator ID="cvCharges" runat="server" ErrorMessage="Invalid Charges"
                                            ControlToValidate="txtCharges" ValueToCompare="0" Display="Dynamic" SetFocusOnError="true"
                                            ForeColor="Red" Operator="GreaterThanEqual" ValidationGroup="Complete" Enabled="false">                                            
                                        </asp:CompareValidator><asp:Label ID="LabelCharges" runat="server" Text='<%# string.Format("{0:C}",Eval("Charges")) %>'
                                            Visible='<%# Eval("IsApportioned") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn UniqueName="Tax" HeaderText="Tax" AllowFiltering="False"
                                    HeaderStyle-Width="112px" SortExpression="Tax" DataField="Tax" Visible="false">
                                    <ItemTemplate>
                                        <telerik:RadNumericTextBox ID="txtTax" runat="server" Width="110px" Type="Currency"
                                            Visible='<%# !(Convert.ToBoolean(Eval("IsApportioned"))) %>'>
                                        </telerik:RadNumericTextBox><asp:CompareValidator ID="cvTax" runat="server" ErrorMessage="Invalid Tax"
                                            ControlToValidate="txtTax" ValueToCompare="0" Display="Dynamic" SetFocusOnError="true"
                                            ForeColor="Red" Operator="GreaterThanEqual" ValidationGroup="Complete" Enabled="false">                                            
                                        </asp:CompareValidator><asp:Label ID="LabelTax" runat="server" Text='<%# string.Format("{0:C}",Eval("Tax")) %>'
                                            Visible='<%# Eval("IsApportioned") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn UniqueName="GrossValue" HeaderText="Gross Value" AllowFiltering="False"
                                    HeaderStyle-Width="112px" SortExpression="GrossValue" DataField="GrossValue"
                                    Visible="false">
                                    <ItemTemplate>
                                        <telerik:RadNumericTextBox ID="txtGrossValue" runat="server" Width="110px" Type="Currency"
                                            Visible='<%# !(Convert.ToBoolean(Eval("IsApportioned"))) %>'>
                                        </telerik:RadNumericTextBox><asp:CompareValidator ID="cvGrossValue" runat="server"
                                            ErrorMessage="Invalid Gross Value" ControlToValidate="txtGrossValue" ValueToCompare="0"
                                            Display="Dynamic" SetFocusOnError="true" ForeColor="Red" Operator="GreaterThanEqual"
                                            ValidationGroup="Complete" Enabled="false">                                            
                                        </asp:CompareValidator><asp:Label ID="LabelGrossValue" runat="server" Text='<%# string.Format("{0:C}",Eval("GrossValue")) %>'
                                            Visible='<%# Eval("IsApportioned") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn SortExpression="BrokerName" ReadOnly="true" HeaderText="Broker"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="BrokerName" UniqueName="BrokerName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="InstituteName" ReadOnly="true" HeaderText="Bank"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="InstituteName" UniqueName="InstituteName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="Term" ReadOnly="true" HeaderText="Term"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="Term" UniqueName="Term">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="Rate" ReadOnly="true" HeaderText="Rate"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="Rate" UniqueName="Rate" DataFormatString="{0:P}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="Amount" ReadOnly="true" HeaderText="Amount"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="Amount" UniqueName="Amount" DataFormatString="{0:C}"
                                    Aggregate="Sum">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn UniqueName="ContractNote" HeaderText="Contract Note"
                                    AllowFiltering="False" HeaderStyle-Width="92px" SortExpression="ContractNote"
                                    DataField="ContractNote">
                                    <ItemTemplate>
                                        <telerik:RadTextBox runat="server" Width="85px" ID="txtContractNote" Visible='<%# !(Convert.ToBoolean(Eval("IsApportioned"))) %>'>
                                        </telerik:RadTextBox><asp:RequiredFieldValidator ID="reqNote" runat="server" ErrorMessage="Enter Note"
                                            ForeColor="Red" Display="Dynamic" ControlToValidate="txtContractNote" ValidationGroup="Complete"
                                            Enabled="false"></asp:RequiredFieldValidator><asp:Label ID="Label4" runat="server"
                                                Text='<%# Eval("ContractNote") %>' Visible='<%# Eval("IsApportioned") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="CreatedDate"
                                    ReadOnly="true" HeaderText="Created Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedDate" UniqueName="CreatedDate"
                                    DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="UpdatedDate"
                                    ReadOnly="true" HeaderText="Updated Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UpdatedDate" UniqueName="UpdatedDate"
                                    DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="IsApportioned" ReadOnly="true" HeaderText="Is Apportioned"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="IsApportioned" UniqueName="IsApportioned"
                                    Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderStyle-Width="85px" UniqueName="Actions" HeaderText="Actions"
                                    AllowFiltering="false">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnAdjust" runat="server" CommandName="complete" CommandArgument='<%# Eval("ID") %>'
                                            ValidationGroup="Complete" ToolTip="Complete Bulk Order" Visible='<%# !(Convert.ToBoolean(Eval("IsApportioned"))) %>'>Complete</asp:LinkButton>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </telerik:GridTableView>
                    </DetailTables>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="ID" UniqueName="ID" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="90px" SortExpression="BulkOrderID"
                            ReadOnly="true" HeaderText="Bulk Order ID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BulkOrderID" UniqueName="BulkOrderID">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Status" ReadOnly="true" HeaderText="Status"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="Status" UniqueName="Status">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="OrderType" ReadOnly="true" HeaderText="Order Type"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="OrderType" UniqueName="OrderType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="OrderAccountType" HeaderText="Account Type"
                            ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="OrderAccountType"
                            UniqueName="OrderAccountType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="OrderItemType" ShowFilterIcon="true"
                            HeaderText="Item Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            HeaderButtonType="TextButton" DataField="OrderItemType" UniqueName="OrderItemType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="OrderPreferedBy" ShowFilterIcon="true"
                            HeaderText="Prefered By" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            HeaderButtonType="TextButton" DataField="OrderPreferedBy" UniqueName="OrderPreferedBy">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="35px" HeaderStyle-Width="70px" ReadOnly="true"
                            SortExpression="SMARequestID" HeaderText="Super Request ID" AutoPostBackOnFilter="false"
                            ShowFilterIcon="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                            DataField="SMARequestID" UniqueName="SMARequestID" >
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="35px" HeaderStyle-Width="70px" ReadOnly="true"
                            SortExpression="SMACallStatus" HeaderText="Supper Call Status" AutoPostBackOnFilter="false"
                            ShowFilterIcon="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                            DataField="SMACallStatus" UniqueName="SMACallStatus"  >
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="IsExported" HeaderText="Is Exported"
                            AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                            HeaderButtonType="TextButton" DataField="IsExported" UniqueName="IsExported">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="65px" HeaderStyle-Width="100px" SortExpression="TradeDate"
                            ReadOnly="true" HeaderText="Trade Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TradeDate" UniqueName="TradeDate"
                            DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" ReadOnly="true"
                            SortExpression="CreatedDate" HeaderText="Create Date " AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="CreatedDate" UniqueName="CreatedDate" DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}"
                            DataType="System.DateTime">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" ReadOnly="true"
                            SortExpression="UpdatedDate" HeaderText="Update Date" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="UpdatedDate" UniqueName="UpdatedDate" DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}"
                            DataType="System.DateTime">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="UpdatedBy" ReadOnly="true" HeaderText="UpdatedBy"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="UpdatedBy" UniqueName="UpdatedBy" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="UpdatedByCID" ReadOnly="true" HeaderText="UpdatedByCID"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="UpdatedByCID" UniqueName="UpdatedByCID"
                            Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn UniqueName="BulkSelect" AllowFiltering="false" HeaderStyle-Width="35px">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" Visible='<%# !Convert.ToBoolean(Eval("IsExported")) %>' onclick="CheckSingle('BulkSelect','chkSelect');"
                                    ToolTip="Select Orders" />
                            </ItemTemplate>
                            <HeaderTemplate>
                                <asp:CheckBox ID="headerChkbox" runat="server" onclick="CheckAll(this,'chkSelect');" ToolTip="Select All Orders" />
                            </HeaderTemplate>
                        </telerik:GridTemplateColumn>
                         <telerik:GridTemplateColumn UniqueName="BulkSelect1" AllowFiltering="false" HeaderStyle-Width="100px" >
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect1" runat="server" ToolTip="Select" Visible='<%# btnSMA.Checked && (Eval("Status").ToString()== BulkOrderStatus.Complete.ToString())&&(Eval("SMACallStatus").ToString()== SMACallStatus.Failed.ToString()) %>' onclick="CheckSingle('BulkSelect1','chkSelect1');" />
                            </ItemTemplate>
                            <HeaderTemplate>
                                <asp:CheckBox ID="headerChkbox1" runat="server" Text="Super Export" onclick="CheckAll(this,'chkSelect1');" ToolTip="Select All Complete Orders for Super" />
                            </HeaderTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
