﻿using System;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb.SysAdministration
{
    public partial class EntityRelation : UMABasePage
    {
        private DataTable teams;
        private IBrokerManagedComponent org;

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        public override void LoadPage()
        {
            CreateHierarchy();

            RadOrgChart1.NodeDataBound +=
                new Telerik.Web.UI.OrgChartNodeDataBoundEventHandler(RadOrgChart1_NodeDataBound);

            RadOrgChart1.DataFieldID = "ParentId";
            RadOrgChart1.DataFieldParentID = "ChildId";
            RadOrgChart1.DataCollapsedField = "Collapsed";
            RadOrgChart1.DataTextField = "EntityName";
            RadOrgChart1.DataImageUrlField = "ImageUrl";
            RadOrgChart1.RenderedFields.NodeFields.Add(new Telerik.Web.UI.OrgChartRenderedField() { DataField = "EntityName" });
            RadOrgChart1.DataSource = teams;
            RadOrgChart1.DataBind();
        }

        protected void RadOrgChart1_NodeDataBound(object sender,
            Telerik.Web.UI.OrgChartNodeDataBoundEventArguments e)
        {
            #region Old Code
            //if (e.Node.ID == "2")
            //    e.Node.ColumnCount = 2;
            //if (e.Node.ID == "3")
            //    e.Node.ColumnCount = 1; 
            #endregion
        }

        private DataTable CreateHierarchy()
        {
            #region Initialze Datasource
            teams = new DataTable();
            teams.Columns.Add("ParentId");
            teams.Columns.Add("ChildId");
            teams.Columns.Add("EntityName");
            teams.Columns.Add("Description");
            teams.Columns.Add("ImageUrl");
            teams.Columns.Add("PrefixUrl");
            teams.Columns.Add("Cid");
            teams.Columns.Add("Clid");
            teams.Columns.Add("Csid");
            teams.Columns.Add("Collapsed");
            #endregion

            teams.Rows.Add(new string[] 
            { 
                "1", //ParentId
                null, //ChildId(of)
                "Innova", //EntityName
                "e-Clipse", //Description
                "../images/eclipse-small.png", //ImageUrl
                "javascript:return false;", //PrefixUrl
                null, //Cid
                null, //Clid
                null, //Csid
                "0" //Collapsed
            });

            #region Dealer Groups
            int dgId = 1;
            DealerGroupDS dgDs = PopulateDealerGroup();
            foreach (DataRow objDgDs in dgDs.DealerGroupTable.Rows)
            {
                dgId++;
                teams.Rows.Add(new string[]
                {
                    dgId.ToString(), //ParentId
                    "1", //ChildId(of)
                    "Dealer Group", //EntityName
                    objDgDs["TradingName"].ToString(), //Description
                    "../images/ic-dealer.png", //ImageUrl
                    "DealerGroupCompanyDetail.aspx?ins="+objDgDs["Cid"].ToString(), //PrefixUrl
                    objDgDs["Cid"].ToString(), //Cid
                    objDgDs["Clid"].ToString(), //Clid
                    objDgDs["CsiD"].ToString(), //Csid
                    "1" //Collapsed
                });

                #region IFA
                int ifaId = Int32.Parse(dgId.ToString() + "01");
                MembershipDS memDgDs =
                    PopulateChild(new Guid(objDgDs["cid"].ToString()));
                foreach (DataRow objMemDgDs in memDgDs.Tables["MembershipList"].Rows)
                {
                    ifaId++;
                    teams.Rows.Add(new string[]
                    {
                        ifaId.ToString(), //ParentId
                        dgId.ToString(), //ChildId(of)
                        "IFA", //EntityName
                        objMemDgDs["Name"].ToString(), //Description
                        "../images/ic-ifa.png", //ImageUrl
                        "IFACompanyDetail.aspx?ins="+objMemDgDs["Cid"].ToString(), //PrefixUrl
                        objMemDgDs["Cid"].ToString(),  //Cid
                        objMemDgDs["Clid"].ToString(), //Clid
                        objMemDgDs["Csid"].ToString(), //Csid
                        "1" //Collapsed
                    });

                    #region Advisors
                    int advId = Int32.Parse(ifaId.ToString() + "01");
                    MembershipDS memIfaDs =
                        PopulateChild(new Guid(objMemDgDs["cid"].ToString()));
                    foreach (DataRow objMemIfaDs in memIfaDs.Tables["MembershipList"].Rows)
                    {
                        advId++;
                        teams.Rows.Add(new string[]
                        {
                            advId.ToString(), //ParentId
                            ifaId.ToString(), //ChildId(of)
                            "Advisor", //EntityName
                            objMemIfaDs["Name"].ToString(), //Description
                            "../images/ic-advisor.png", //ImageUrl
                            "AdviserDetails.aspx?ins="+objMemIfaDs["Cid"].ToString(), //PrefixUrl
                            objMemIfaDs["Cid"].ToString(),  //Cid
                            objMemIfaDs["Clid"].ToString(), //Clid
                            objMemIfaDs["Csid"].ToString(), //Csid
                            "1" //Collapsed
                        });

                        #region Clients
                        int clientId = Int32.Parse(advId.ToString() + "01");
                        MembershipDS memAdvDs =
                            PopulateChild(new Guid(objMemIfaDs["cid"].ToString()));
                        foreach (DataRow objMemAdvDs in memAdvDs.Tables["MembershipList"].Rows)
                        {
                            clientId++;
                            teams.Rows.Add(new string[]
                            {
                                clientId.ToString(), //ParentId
                                advId.ToString(), //ChildId(of)
                                "Client", //EntityName
                                objMemAdvDs["Name"].ToString(), //Description
                                "../images/ic-client.png", //ImageUrl
                                "../ClientViews/ClientMainView.aspx?ins="+objMemAdvDs["Cid"].ToString(), //PrefixUrl
                                objMemAdvDs["Cid"].ToString(),  //Cid
                                objMemAdvDs["Clid"].ToString(), //Clid
                                objMemAdvDs["Csid"].ToString(), //Csid
                                "1" //Collapsed
                                });
                        }
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            #endregion

            return teams;
        }

        private DealerGroupDS PopulateDealerGroup()
        {
            #region Dealer Group
            org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);

            DealerGroupDS dealerGroupDs = new DealerGroupDS
            {
                CommandType = DatasetCommandTypes.Get,
                Command = (int)WebCommands.GetOrganizationUnitsByType
            };

            dealerGroupDs.Unit = new OrganizationUnit
            {
                CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                Type = ((int)OrganizationType.DealerGroup).ToString(),

            };
            org.GetData(dealerGroupDs);
            UMABroker.ReleaseBrokerManagedComponent(org);
            return dealerGroupDs;
            #endregion
        }

        private MembershipDS PopulateChild(Guid parentCiD)
        {
            #region Advisor
            MembershipDS memDs = new MembershipDS
            {
                CommandType = DatasetCommandTypes.GetChildren,
                Unit = new OrganizationUnit
                {
                    CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                }
            };

            var component = UMABroker.GetBMCInstance(parentCiD);
            component.GetData(memDs);
            UMABroker.ReleaseBrokerManagedComponent(component);
            return memDs;
            #endregion
        }
    }
}