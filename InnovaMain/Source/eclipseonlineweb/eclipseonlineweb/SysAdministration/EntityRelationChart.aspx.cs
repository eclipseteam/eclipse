﻿using System;
using System.Data;
using System.Globalization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb.SysAdministration
{
    public partial class EntityRelationChart : UMABasePage
    {
        private DataTable _teams;
        private IBrokerManagedComponent _org;

        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            return true;
        }

        public override void LoadPage()
        {
            CreateHierarchy();

            RadOrgChart1.NodeDataBound +=RadOrgChart1_NodeDataBound;

            RadOrgChart1.DataFieldID = "ParentId";
            RadOrgChart1.DataFieldParentID = "ChildId";
            RadOrgChart1.DataCollapsedField = "Collapsed";
            RadOrgChart1.DataTextField = "EntityName";
            RadOrgChart1.DataImageUrlField = "ImageUrl";
            RadOrgChart1.RenderedFields.NodeFields.Add(new Telerik.Web.UI.OrgChartRenderedField { DataField = "EntityName" });
            RadOrgChart1.DataSource = _teams;
            RadOrgChart1.DataBind();
        }

        protected void RadOrgChart1_NodeDataBound(object sender,
            Telerik.Web.UI.OrgChartNodeDataBoundEventArguments e)
        {
            #region Old Code
            //if (e.Node.ID == "2")
            //    e.Node.ColumnCount = 2;
            //if (e.Node.ID == "3")
            //    e.Node.ColumnCount = 1; 
            #endregion
        }

        private void CreateHierarchy()
        {
            #region Initialze Datasource
            _teams = new DataTable();
            _teams.Columns.Add("ParentId");
            _teams.Columns.Add("ChildId");
            _teams.Columns.Add("EntityName");
            _teams.Columns.Add("Description");
            _teams.Columns.Add("ImageUrl");
            _teams.Columns.Add("PrefixUrl");
            _teams.Columns.Add("Cid");
            _teams.Columns.Add("Clid");
            _teams.Columns.Add("Csid");
            _teams.Columns.Add("Collapsed");
            #endregion

            _teams.Rows.Add(new object[] 
            { 
                "1", //ParentId
                null, //ChildId(of)
                "Innova", //EntityName
                "e-Clipse", //Description
                "../images/eclipse-small.png", //ImageUrl
                "javascript:return false;", //PrefixUrl
                null, //Cid
                null, //Clid
                null, //Csid
                "0" //Collapsed
            });

            #region Dealer Groups
            int dgId = 1;
            DealerGroupDS dgDs = PopulateDealerGroup();
            foreach (DataRow objDgDs in dgDs.DealerGroupTable.Rows)
            {
                dgId++;
                _teams.Rows.Add(new string[]
                {
                    dgId.ToString(CultureInfo.InvariantCulture), //ParentId
                    "1", //ChildId(of)
                    "Dealer Group", //EntityName
                    objDgDs["TradingName"].ToString(), //Description
                    "../images/ic-dealer.png", //ImageUrl
                    "DealerGroupCompanyDetail.aspx?ins="+objDgDs["Cid"], //PrefixUrl
                    objDgDs["Cid"].ToString(), //Cid
                    objDgDs["Clid"].ToString(), //Clid
                    objDgDs["CsiD"].ToString(), //Csid
                    "1" //Collapsed
                });

                #region IFA
                int ifaId = Int32.Parse(dgId.ToString() + "01");
                MembershipDS memDgDs =
                    PopulateChild(new Guid(objDgDs["cid"].ToString()));
                foreach (DataRow objMemDgDs in memDgDs.Tables["MembershipList"].Rows)
                {
                    ifaId++;
                    _teams.Rows.Add(new string[]
                    {
                        ifaId.ToString(), //ParentId
                        dgId.ToString(), //ChildId(of)
                        "IFA", //EntityName
                        objMemDgDs["Name"].ToString(), //Description
                        "../images/ic-ifa.png", //ImageUrl
                        "IFACompanyDetail.aspx?ins="+objMemDgDs["Cid"].ToString(), //PrefixUrl
                        objMemDgDs["Cid"].ToString(),  //Cid
                        objMemDgDs["Clid"].ToString(), //Clid
                        objMemDgDs["Csid"].ToString(), //Csid
                        "1" //Collapsed
                    });

                    #region Advisors
                    int advId = Int32.Parse(ifaId.ToString() + "01");
                    MembershipDS memIfaDs =
                        PopulateChild(new Guid(objMemDgDs["cid"].ToString()));
                    foreach (DataRow objMemIfaDs in memIfaDs.Tables["MembershipList"].Rows)
                    {
                        advId++;
                        _teams.Rows.Add(new string[]
                        {
                            advId.ToString(), //ParentId
                            ifaId.ToString(), //ChildId(of)
                            "Advisor", //EntityName
                            objMemIfaDs["Name"].ToString(), //Description
                            "../images/ic-advisor.png", //ImageUrl
                            "AdviserDetails.aspx?ins="+objMemIfaDs["Cid"].ToString(), //PrefixUrl
                            objMemIfaDs["Cid"].ToString(),  //Cid
                            objMemIfaDs["Clid"].ToString(), //Clid
                            objMemIfaDs["Csid"].ToString(), //Csid
                            "1" //Collapsed
                        });

                        #region Clients
                        int clientId = Int32.Parse(advId.ToString() + "01");
                        MembershipDS memAdvDs =
                            PopulateChild(new Guid(objMemIfaDs["cid"].ToString()));
                        foreach (DataRow objMemAdvDs in memAdvDs.Tables["MembershipList"].Rows)
                        {
                            clientId++;
                            _teams.Rows.Add(new string[]
                            {
                                clientId.ToString(), //ParentId
                                advId.ToString(), //ChildId(of)
                                "Client", //EntityName
                                objMemAdvDs["Name"].ToString(), //Description
                                "../images/ic-client.png", //ImageUrl
                                "../ClientViews/ClientMainView.aspx?ins="+objMemAdvDs["Cid"].ToString(), //PrefixUrl
                                objMemAdvDs["Cid"].ToString(),  //Cid
                                objMemAdvDs["Clid"].ToString(), //Clid
                                objMemAdvDs["Csid"].ToString(), //Csid
                                "1" //Collapsed
                                });
                        }
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            #endregion
        }

        private DealerGroupDS PopulateDealerGroup()
        {
            #region Dealer Group
            _org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);

            var dealerGroupDs = new DealerGroupDS
            {
                CommandType = DatasetCommandTypes.Get,
                Command = (int)WebCommands.GetOrganizationUnitsByType
            };

            dealerGroupDs.Unit = new OrganizationUnit
            {
                CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                Type = ((int)OrganizationType.DealerGroup).ToString(),

            };
            _org.GetData(dealerGroupDs);
            UMABroker.ReleaseBrokerManagedComponent(_org);
            return dealerGroupDs;
            #endregion
        }

        private MembershipDS PopulateChild(Guid parentCiD)
        {
            #region Advisor
            MembershipDS memDs = new MembershipDS
            {
                CommandType = DatasetCommandTypes.GetChildren,
                Unit = new OrganizationUnit
                {
                    CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                }
            };

            var component = UMABroker.GetBMCInstance(parentCiD);
            component.GetData(memDs);
            UMABroker.ReleaseBrokerManagedComponent(component);
            return memDs;
            #endregion
        }
    }
}