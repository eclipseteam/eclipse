﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="Workflows.aspx.cs" Inherits="eclipseonlineweb.Workflows" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register TagPrefix="uc2" TagName="clientFilter" Src="~/Controls/ClientFilterControl.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="4%">
                        <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                            ID="btnDownload" />
                    </td>
                    <td width="100%" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <asp:Label Font-Bold="true" runat="server" ID="lblSecurities" Text="Workflows"></asp:Label>
                        <asp:HiddenField ID="hfSecID" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />

    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <uc2:clientFilter ID="clientFilter" runat="server"></uc2:clientFilter><br/>

            <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
                OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
                OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
                OnItemDataBound="PresentationGrid_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="WorkflowID" AllowMultiColumnSorting="True" Width="100%"
                    CommandItemDisplay="Top" Name="Workflows" TableLayout="Fixed" EditMode="EditForms">
                    <CommandItemSettings AddNewRecordText="Add New Workflow" ShowExportToExcelButton="true"
                        ShowExportToWordButton="true" ShowExportToPdfButton="true" ></CommandItemSettings>
                    <Columns>
                     <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="WorkflowID"
                            UniqueName="WorkflowID" />
                            <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="WorkflowType"
                            UniqueName="WorkflowType" />
                        <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="20%" ItemStyle-Width="20%"
                            SortExpression="WorkflowName" HeaderText="Workflow Name" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="WorkflowName" UniqueName="WorkflowName" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="40%" ItemStyle-Width="40%"
                            SortExpression="WorkflowDesc" HeaderText="Workflow Desc" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="WorkflowDesc" UniqueName="WorkflowDesc">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn HeaderText="Workflow Type" SortExpression="WorkflowType"
                            UniqueName="WorkflowTypeCombo" DataField="WorkflowType" Visible="false" ColumnEditorID="GridDropDownColumnEditorListOfWorkflowType" />
                        <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                            SortExpression="CreatedDate" HeaderText="Created Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedDate" UniqueName="CreatedDate"
                            ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80px" DefaultInsertValue="2012" HeaderStyle-Width="10%"
                            ItemStyle-Width="10%" SortExpression="CreatedBy" HeaderText="Created By" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="CreatedBy" UniqueName="CreatedBy" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn CommandName="Run" Text="Run Workflow" ButtonType="LinkButton"
                            HeaderStyle-Width="10%" ItemStyle-Width="10%">
                        </telerik:GridButtonColumn>
                        <telerik:GridTemplateColumn UniqueName="FeeTemplateDetails" FilterControlWidth="5%"
                            ShowFilterIcon="false">
                            <HeaderStyle Width="7%" />
                            <ItemStyle Width="7%" />
                            <ItemTemplate>
                                <asp:LinkButton ID="Definition" runat="server" Text="Details" CommandArgument='<%# Eval("WorkflowID") +"," + Eval("WorkflowID")%>'
                                    CommandName="Details">
                                </asp:LinkButton>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridButtonColumn ConfirmText="Delete these details record?" ButtonType="ImageButton"
                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="Html"></Excel>
                </ExportSettings>
            </telerik:RadGrid>
             <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorListOfWorkflowType"
                runat="server" DropDownStyle-Width="200px">
            </telerik:GridDropDownListColumnEditor>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
