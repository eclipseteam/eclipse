﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Data;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class BankAccounts : UMABasePage
    {
        private bool flag = false;

        protected override void Intialise()
        {
            base.Intialise();
            #region Bank Account control
            BankControl.Saved += (CID, CLID, CSID) =>
            {
                HideShowBankGrid(false);
                PopulateData();
            };
            BankControl.Canceled += () => HideShowBankGrid(false);
            BankControl.ValidationFailed += () => HideShowBankGrid(true);
            BankControl.SaveData += (bankCid, ds) =>
            {
                if (bankCid == Guid.Empty)
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(bankCid.ToString(), ds);
                }
                HideShowBankGrid(false);
            };
            #endregion
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected void btnDownload_Onclick(object sender, EventArgs e)
        {
            DataSet excelDataset = new DataSet();
            DataView entityView = new DataView(PresentationData.Tables[BankDetailsDS.BANKDETAILSTABLELIST]);
            entityView.Sort = "ENTITYNAME_FIELD ASC";
            DataTable entityTable = entityView.ToTable();
            entityTable.Columns.Remove("ENTITYCLID_FIELD");
            entityTable.Columns.Remove("ENTITYCSID_FIELD");
            entityTable.Columns.Remove("ENTITYCIID_FIELD");
            entityTable.Columns.Remove("ENTITYCTID_FIELD");
            entityTable.Columns.Remove("ENTITYSCTYPE_FIELD");
            entityTable.Columns.Remove("ENTITYSCSTATUS_FIELD");
            entityTable.Columns.Remove("ENTITYSORT_FIELD");
            entityTable.Columns.Remove("InstitutionID");

            excelDataset.Merge(entityTable, true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, "Accounts-FUM-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", Page.Response);
        }

        protected override void GetBMCData()
        {
            PopulateData();
        }

        private void PopulateData()
        {
            IBrokerManagedComponent iBMC = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            OrganisationListingDS ds = new OrganisationListingDS();
            ds.OrganisationListingOperationType = OrganisationListingOperationType.BankAccounts;
            iBMC.GetData(ds);
            PresentationData = ds;
            UMABroker.ReleaseBrokerManagedComponent(iBMC);
            BindControls(ds);
        }

        private void BindControls(DataSet ds)
        {
            DataView entityView = new DataView(ds.Tables[BankDetailsDS.BANKDETAILSTABLELIST]);
            entityView.Sort = "ENTITYNAME_FIELD ASC";
            gvBankAccount.DataSource = entityView.ToTable();
            gvBankAccount.DataBind();
        }

        private void BindBankAccount()
        {
            IBrokerManagedComponent iBMC = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            OrganisationListingDS ds = new OrganisationListingDS();
            ds.OrganisationListingOperationType = OrganisationListingOperationType.BankAccounts;
            iBMC.GetData(ds);
            PresentationData = ds;
            UMABroker.ReleaseBrokerManagedComponent(iBMC);
            DataView entityView = new DataView(ds.Tables[BankDetailsDS.BANKDETAILSTABLELIST]);
            entityView.Sort = "ENTITYNAME_FIELD ASC";
            gvBankAccount.DataSource = entityView.ToTable();

        }

        public override void LoadPage()
        {
            base.LoadPage();

            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
                sm.RegisterPostBackControl(btnDownload);
        }

        public override void PopulatePage(DataSet ds)
        {
            if (!IsPostBack)
            {
                DataView entityView = new DataView(ds.Tables[BankDetailsDS.BANKDETAILSTABLELIST]);
                entityView.Sort = "ENTITYNAME_FIELD ASC";
            }
        }

        protected void gvBankAccount_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            BindBankAccount();
        }

        protected void gvBankAccount_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;
            var gDataItem = e.Item as GridDataItem;
            switch (e.CommandName.ToLower())
            {
                case "editrecords":
                    {
                        Guid rowCid = new Guid(gDataItem["ENTITYCIID_FIELD"].Text);
                        BankControl.SetEntity(rowCid, true);
                        HideShowBankGrid(true);
                    }
                    break;
                case "delete":
                    {
                        string rowId = gDataItem["ENTITYCIID_FIELD"].Text;
                        DeleteInstance(rowId);
                        PopulateData();
                    }
                    break;
            }
        }

        private void HideShowBankGrid(bool show)
        {
            OVER.Visible = BankModal.Visible = show;
        }
    }
}
