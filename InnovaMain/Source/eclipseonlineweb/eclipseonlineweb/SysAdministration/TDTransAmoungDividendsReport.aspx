﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="TDTransAmoungDividendsReport.aspx.cs" Inherits="eclipseonlineweb.TDTransAmoungDividendsReport" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="breadcrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <style type="">
                    .wijmo-wijgrid .wijmo-wijgrid-headerrow .wijmo-wijgrid-headertext
                    {
                        padding: 0 !important;
                    }
                </style>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <br />
                            <br />
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td class="breadcrumbgap">
                            <uc1:breadcrumb id="BreadCrumb1" runat="server" />
                            <br />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <asp:Panel runat="server" ID="pnlDIV">
                <c1:C1GridView AlternatingRowStyle-Font-Size="X-Small" FilterStyle-Font-Size="Smaller"
                    AllowSorting="true" OnSorting="SortDIV" RowStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small"
                    ID="DivTranGrid" AllowPaging="true" PageSize="20" runat="server" AutogenerateColumns="false"
                    ShowFilter="true" OnFiltering="FilterDIV" OnPageIndexChanging="DIV_PageIndexChanging"
                    Width="100%" ShowFooter="true" ScrollMode="None">
                    <Columns>
                        <c1:C1BoundField DataField="ID" HeaderText="ID" SortExpression="ID" Visible="false" />
                        <c1:C1BoundField DataField="ClientID" Width="6%" SortExpression="ClientID" HeaderText="Client ID">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="InvestmentCode" Width="4%" SortExpression="InvestmentCode"
                            HeaderText="Code">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="TransactionType" Width="10%" SortExpression="TransactionType"
                            HeaderText="Transaction Type">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="BalanceDate" Width="6%" HeaderText="Balance Date" SortExpression="BalanceDate"
                            DataFormatString="dd/MM/yyyy">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="DividendType" Width="5%" HeaderText="Dividend Type" SortExpression="DividendType"
                            DataFormatString="dd/MM/yyyy">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="RecordDate" Width="6%" SortExpression="RecordDate" DataFormatString="dd/MM/yyyy"
                            HeaderText="Ex-Dividend Date">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="BooksCloseDate" Width="6%" HeaderText="Books Close Date"
                            SortExpression="BooksCloseDate" DataFormatString="dd/MM/yyyy">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="PaymentDate" Width="6%" SortExpression="PaymentDate"
                            DataFormatString="dd/MM/yyyy" HeaderText="Payment Date">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="UnitsOnHand" Width="4%" SortExpression="UnitsOnHand"
                            HeaderText="Units">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="CentsPerShare" Width="5%" SortExpression="CentsPerShare"
                            DataFormatString="N4" HeaderText="Cents Per Share">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="PaidDividend" Width="5%" SortExpression="PaidDividend"
                            DataFormatString="C" HeaderText="Paid Dividend">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="FrankingCredits" Width="6%" SortExpression="FrankingCredits"
                            DataFormatString="N4" HeaderText="Franking Credits (CPS)">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="TotalFrankingCredits" Width="6%" SortExpression="TotalFrankingCredits"
                            DataFormatString="C" HeaderText="Total Franking Credits ($)">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="Currency" Width="5%" HeaderText="Currency (Ccy)" SortExpression="Currency"
                            DataFormatString="dd/MM/yyyy">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="Status" Width="4%" SortExpression="Status" HeaderText="Status">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="InvestmentAmount" Width="6%" SortExpression="InvestmentAmount"
                            DataFormatString="C" HeaderText="Investment Amount">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="InterestRate" Width="5%" SortExpression="InterestRate"
                            DataFormatString="C" HeaderText="Interest Rate">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="InterestPaid" Width="5%" SortExpression="InterestPaid"
                            DataFormatString="C" HeaderText="Interest Paid">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                    </Columns>
                </c1:C1GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
