﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="ImportAmmAtCall.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.ImportAmmAtCall" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%@ register src="~/Controls/BreadCrumb.ascx" tagname="BreadCrumb" tagprefix="uc1" %>
    <%@ register src="~/SysAdministration/WebControls/ImportMessageControl.ascx" tagname="Details"
        tagprefix="uc" %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Import Client At Call Transactions (AMM)"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Import Client At Call Transactions (AMM)</legend><span class="riLabel"></span>
                            <asp:FileUpload ID="fileuploadExcel" runat="server" />
                            <asp:RegularExpressionValidator ID="rexp" runat="server" ControlToValidate="fileuploadExcel"
                                ErrorMessage="File type should be CSV (.csv)" ValidationExpression="(.*\.([Cc][Ss][Vv])$)"
                                ForeColor="#FF3300"></asp:RegularExpressionValidator>
                            <asp:Button runat="server" ID="btnImportTran" Text="Import Transactions File" OnClick="btnImportTran_Click" />
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel runat="server" ID="PnlResults">
                            <uc:Details ID="ImportMessageControl" runat="server" />
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnImportTran" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
