﻿using System;
using System.Web;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb.SysAdministration
{
    public partial class ExportReportPopup : UMABasePage
    {
        public override void LoadPage()
        {
            base.LoadPage();
            if (Request.Params["filename"] == "StateStreetCSVExport")
                ExportSSFiles();
            if (Request.Params["filename"] == "BankWestAccountOpeningCSVExport")
                ExportFilesBankwestAccountOpening();
            else
                ExportFiles();    
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        private void ExportFiles()
        {
            try
            {
                var bytes = Session["ExportFileBytes"] as byte[];
                Session.Remove("ExportFileBytes");
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                HttpContext.Current.Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
                Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
                Response.AppendHeader("Content-Type", "application/" + Request.Params["filetype"]);
                Response.AppendHeader("Content-disposition", "attachment; filename=" + Request.Params["filename"] + "." + Request.Params["filetype"]);
                Response.BinaryWrite(bytes);
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                string strScript = "<script>";
                strScript = strScript + "alert('" + ex.Message + "');";
                strScript = strScript + "</script>";
                //Page.RegisterStartupScript("ClientScript", strScript);
                ClientScript.RegisterStartupScript(GetType(), "ClientScript", strScript);
            }
        }
        private void ExportFilesBankwestAccountOpening()
        {
            try
            {
                var bytes = Session["ExportFileBytes"] as byte[];
                Session.Remove("ExportFileBytes");
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                HttpContext.Current.Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                Response.AppendHeader("Content-Disposition", "attachment;filename=Bankest Multi Account Opening Utility["+String.Format("{0:u}", DateTime.Now)+"].xlsm");
                Response.BinaryWrite(bytes);
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                string strScript = "<script>";
                strScript = strScript + "alert('" + ex.Message + "');";
                strScript = strScript + "</script>";
                //Page.RegisterStartupScript("ClientScript", strScript);
                ClientScript.RegisterStartupScript(GetType(), "ClientScript", strScript);
            }
        }

        private void ExportSSFiles()
        {
            try
            {

                var bytes = Session["ExportFileBytes"] as byte[];
                Session.Remove("ExportFileBytes");
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                HttpContext.Current.Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
                Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
                Response.AppendHeader("Content-Type", "application/" + Request.Params["filetype"]);
                Response.AppendHeader("Content-disposition", "attachment; filename=NewAccount" + DateTime.Now.ToString("yyyyMMdd") + "." + Request.Params["filetype"]);
                Response.BinaryWrite(bytes);
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                string strScript = "<script>";
                strScript = strScript + "alert('" + ex.Message + "');";
                strScript = strScript + "</script>";
                //Page.RegisterStartupScript("ClientScript", strScript);
                ClientScript.RegisterStartupScript(GetType(), "ClientScript", strScript);
            }
        }
    }
}