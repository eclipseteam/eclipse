﻿using System;
using System.IO;
using System.Web.UI;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class ImportClientDistributions : UMABasePage
    {

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void Intialise()
        {
            base.Intialise();
          
        }

        public override void LoadPage()
        {
            base.LoadPage();
        }

        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            
        }

        protected void SecListSorting(object sender, C1GridViewSortEventArgs e)
        {
           
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {

            if (FileUpload1.HasFile)
            {
                string fileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                var date = DateTime.Now;
                var distributionDictory = Server.MapPath("~/App_Data/Distrbutions/");
                if (!Directory.Exists(distributionDictory))
                {
                    Directory.CreateDirectory(distributionDictory);
                }

                string fileLocation = distributionDictory + date.ToString("ddMMyyhhmmsstt") + fileName;
                FileUpload1.SaveAs(fileLocation);

                ImportProcessDS ds=new ImportProcessDS();
                ds.Tables.Add(CSVReader.ReadCSVToDataTable(fileLocation, true));
                ds.Unit = new OrganizationUnit {CurrentUser = GetCurrentUser()};
                ds.Command =(int) WebCommands.ImportClientsDistiributions;
                ds.FileName = fileName;
                this.SaveOrganizanition(ds);

                this.SecList.DataSource = ds.Tables[0];
                this.SecList.DataBind();
                this.PresentationData = ds;


            }
        }

        private bool flag = false;

        protected void Filter(object sender, C1GridViewFilterEventArgs e)
        {
            flag = true;
        }

        protected void C1GridView1_DataBound(object sender, EventArgs e)
        {
            if (flag)
            {
                SecList.SelectedIndex = 0;
            }
        }
        
        protected void Sec_PageIndexChanging(object sender, C1GridViewPageEventArgs e)
        {
           
        }
    }
}
