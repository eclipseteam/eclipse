﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eclipseonlineweb
{
    public partial class AdminMaster : MasterPage   
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((HiddenField)Master.FindControl("hfMainMenuText")).Value = "ADMINISTRATION";
        }
    }
}