﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using eclipseonlineweb.WebControls;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class ImportMISUnitHolderBalancesDatafeed : UMABasePage
    {

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void Intialise()
        {
            ImportMessageControl.GridImportMessages.ColumnCreated += GridImportMessages_ColumnCreated;
            ImportMessageControl.GirdInvestmentCodeError.ColumnCreated += GridImportErrorMessages_ColumnCreated;
            ImportMessageControl.GridImportMessages.ItemDataBound += new GridItemEventHandler(GridImportMessages_ItemDataBound);
            if (!IsPostBack)
            {
                ImportMessageControl.Clear();
            }

            ImportMessageControl.Completed += text =>
                                                  {
                                                      switch (text.ToLower())
                                                      {
                                                          case "next":
                                                              SendData();
                                                              break;
                                                          default:
                                                              ClearMessageControls();
                                                              break;
                                                      }
                                                  };

           
        }
        public override void LoadPage()
        {
            ImportMessageControl.ShowHideSampleFileLink("~/Templates/ImportSampleFiles/PostedTransactions20121213(SS Transaction confirmations).csv");
        }
        void GridImportMessages_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item).ItemIndex != -1)
            {
                DataRow dr = ((DataRowView)((e.Item).DataItem)).Row;
                if (bool.Parse(dr["HasErrors"].ToString()))
                {
                    e.Item.ForeColor = Color.Red;
                }
            }

        }
        void GridImportMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {

            SetDataGridColumns(e);
        }

        void GridImportErrorMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            SetDataGridColumns(e);
        }

        public void BtnUploadClick(object sender, EventArgs e)
        {
                if (filMyFile.PostedFile != null)
                {
                    // Get a reference to PostedFile object
                    HttpPostedFile myFile = filMyFile.PostedFile;

                    // Get size of uploaded file
                    int nFileLen = myFile.ContentLength;

                    // make sure the size of the file is > 0
                    if (nFileLen > 0)
                    {
                        // Allocate a buffer for reading of the file
                        string fileName = Path.GetFileName(myFile.FileName);
                        var date = DateTime.Now;
                        var distinationDirectory = Server.MapPath("~/App_Data/Import/");
                        if (!Directory.Exists(distinationDirectory))
                        {
                            Directory.CreateDirectory(distinationDirectory);
                        }

                        string fileLocation = distinationDirectory + date.ToString("ddMMyyhhmmsstt") + fileName;
                        byte[] myData = new byte[nFileLen];

                        // Read uploaded file from the Stream
                        myFile.InputStream.Read(myData, 0, nFileLen);

                        // Write data into a file
                        Utilities.WriteToFile(fileLocation, ref myData);
                        string csv = string.Empty;
                        using (StreamReader reader = new StreamReader(fileLocation, true))
                        {
                            csv = reader.ReadToEnd();
                            reader.Close();
                        }
                        string PSPrice = ConversorTransactionCsv.SelectRows(csv, "");
                        DataTable dt = new DataTable();
                        ImportProcessDS ds = new ImportProcessDS();
                        ds.ExtendedProperties.Add("misName", "StateStreet");
                        dt = ConversorTransactionCsv.CreateMisUnitHoldList(PSPrice);
                        ds.Command = (int)WebCommands.ValidateMISUnitHoldImportFile;
                        ds.Tables.Add(dt);
                        ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
                        ds.FileName = fileName;
                        SaveOrganizanition(ds);
                        SetValidationGird(ds);
                        PresentationData = ds;
                        string filename = ImportMessageControl.RefreshFileID() + ".xml";
                        ds.WriteXml(distinationDirectory + filename, XmlWriteMode.WriteSchema);
                    }
                }
        }

        private void SetValidationGird(ImportProcessDS ds)
        {
            //ImportMessageControl.Clear();
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                DataRow dr;
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    dr = missingItems.NewRow();
                    dr["MissingItem"] = row["FundCode"];
                    missingItems.Rows.Add(dr);
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();

                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingFundCodeIgnored;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }

            int successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
            if (ds.Tables[0].Rows.Count == 0 || successfulRows == 0)
            {
                ImportMessageControl.SetButtons(ButtonTypes.CLOSE);
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sErrorsCannotContinue, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Error);
                ImportMessageControl.TextMessage.Text = "";
            }
            else
            {
                ImportMessageControl.SetButtons(ButtonTypes.NEXTCANCEL);
                //sucess
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sValidationCompleted, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
            }
        }

        private void SetCompletionGird(ImportProcessDS ds)
        {
            ImportMessageControl.Clear();
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }

            using (DataView dv = new DataView(ds.Tables[0]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                DataRow dr;
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    dr = missingItems.NewRow();
                    dr["MissingItem"] = row["FundCode"];
                    missingItems.Rows.Add(dr);
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();

                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingFundCodeIgnored;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }

            int successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
            ImportMessageControl.SetButtons(ButtonTypes.FINISH);
            ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sImportedSuccessfully, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
        }

        private void SendData()
        {
            var distributionDictory = Server.MapPath("~/App_Data/Import/");
            if (!Directory.Exists(distributionDictory))
            {
                Directory.CreateDirectory(distributionDictory);
            }

            string filename = distributionDictory + ImportMessageControl.GetFileID() + ".xml";
            ImportProcessDS ds = new ImportProcessDS();
           
            ds.ReadXml(filename);
            ds.Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportMISUnitHoldImportFile;
            ds.FileName = filename;
            SaveOrganizanition(ds);
            SetCompletionGird(ds);
            ImportMessageControl.SetImportSuccessFileMsg("File is imported successfully. You can see MIS Holdings Vs Transactions report from", "~/SysAdministration/MISHoldingVsTransactions.aspx");
        }

        public void ClearMessageControls()
        {
            ImportMessageControl.Clear();
            ImportMessageControl.TextMessage.Text = "";
        }

        private static void SetDataGridColumns(GridColumnCreatedEventArgs e)
        {
            string colName = e.Column.UniqueName.ToLower();
            switch (colName)
            {
                case "fundcode":
                    e.Column.HeaderText = "Fund Code";
                    break;
                case "fundname":
                    e.Column.HeaderText = "Fund Name";
                    e.Column.FilterControlWidth = Unit.Pixel(50);
                    break;
                case "account":
                    e.Column.HeaderText = "Account";
                    e.Column.FilterControlWidth = Unit.Pixel(50);
                    break;
                case "asofdate":
                    e.Column.HeaderText = "As of Date";
                    e.Column.FilterControlWidth = Unit.Pixel(170);
                    break;
                case "subaccount":
                    e.Column.HeaderText = "Sub Account";
                    break;
                case "totalshares":
                    e.Column.HeaderText = "Total Shares";
                    break;
                case "redemptionprice":
                    e.Column.HeaderText = "Redemption Price";
                    break;
                case "currentvalue":
                    e.Column.HeaderText = "Current Value";
                   break;
                case "investorcategory":
                    e.Column.HeaderText = "Investor Category";
                   break;
                case "haserrors":
                    e.Column.HeaderText = "Has Errors";
                    e.Column.Visible = false;
                    break;
                case "ismissingitem":
                    e.Column.HeaderText = "Is Missing Item";
                    e.Column.Visible = false;
                    break;
                case "missingitem":
                    e.Column.HeaderText = "Missing Fund Code(s)";
                    break;
            }
        }
    }
}