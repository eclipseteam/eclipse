﻿using System;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
namespace eclipseonlineweb.SysAdministration
{
    public partial class DealerGroupCompanyDetail : UMABasePage
    {
        protected override void Intialise()
        {
            base.Intialise();
            DealerGroupCompanyControl1.SaveData += (Cid, ds) =>
            {
                if (Cid == Guid.Empty)
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(Cid.ToString(), ds);
                }
            };
            DealerGroupCompanyControl1.Saved += (cID) => Response.Redirect(Request.Url.AbsoluteUri + "?ins=" + cID);
            BankAccounts.SaveOrg += SaveOrganizanition;
            BankAccounts.SaveUnit += SaveData;
            Directors.SaveOrg += SaveOrganizanition;
            Directors.SaveUnit += SaveData;
            Signatories.SaveOrg += SaveOrganizanition;
            Signatories.SaveUnit += SaveData;
            ShareHolders.SaveOrg += SaveOrganizanition;
            ShareHolders.SaveUnit += SaveData;
        }
       
        private void GetData()
        {
            var clientData = UMABroker.GetBMCInstance(new Guid(cid)) as OrganizationUnitCM;
            if (clientData != null)
            {
                var ds = new AddressDetailsDS();
                clientData.GetData(ds);
                PresentationData = ds;
                UMABroker.ReleaseBrokerManagedComponent(clientData);
                AddressDetailControl.FillAddressControls(ds);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cid))
            {
                var ds = AddressDetailControl.SetData();
                SaveData(cid, ds);
                AddressDetailControl.FillAddressControls(ds);
            }
        }
        public override void LoadPage()
        {
            cid = string.IsNullOrEmpty(Request.Params["ins"]) ? string.Empty : Request.QueryString["ins"];
            if (!IsPostBack)
            {
                DealerGroupCompanyControl1.SetEntity(
                    string.IsNullOrEmpty(Request.Params["ins"]) ? Guid.Empty : new Guid(Request.QueryString["ins"]),
                    Oritax.TaxSimp.CM.Group.DealerGroupEntityType.DealerGroupCompanyControl);
                if (string.IsNullOrEmpty(cid))
                {
                    SetVisibility(false);
                }
                else
                {
                    SetVisibility(true);
                    GetData();
                }
            }
        }
        private void SetVisibility(bool visible)
        {
            ApAddress.Visible = ApBankAccounts.Visible = ApDirectors.Visible = ApSignatories.Visible = ApShareHolders.Visible = ApDocuments.Visible = ApSecurity.Visible == visible;
            RadTab tabAddressHeader = RadTabStrip1.FindTabByValue("AddressHeader");
            tabAddressHeader.Visible = visible;
            RadTab tabAccountsHeader = RadTabStrip1.FindTabByValue("AccountsHeader");
            tabAccountsHeader.Visible = visible;
            RadTab tabDirectorsHeader = RadTabStrip1.FindTabByValue("DirectorsHeader");
            tabDirectorsHeader.Visible = visible;
            RadTab tabSignatoriesHeader = RadTabStrip1.FindTabByValue("SignatoriesHeader");
            tabSignatoriesHeader.Visible = visible;
            RadTab tabShareHoldersHeader = RadTabStrip1.FindTabByValue("ShareHoldersHeader");
            tabShareHoldersHeader.Visible = visible;
            RadTab tabSecHeader = RadTabStrip1.FindTabByValue("SecurityHeader");
            tabSecHeader.Visible = visible;
            RadTab tabDocumentsHeader = RadTabStrip1.FindTabByValue("DocumentsHeader");
            tabDocumentsHeader.Visible = visible;
        }
    }
}