﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="ClientAndAdvisersAssociation.aspx.cs" Inherits="eclipseonlineweb.ClientAndAdvisersAssociation" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
   
    <fieldset>
        <table width="100%">
            <tr>
                <td width="4%">
                    
                 <telerik:RadButton Height="40px" ID="btnAsscociateAllAdvisersClient" runat="server" Width="300px"
                            Text="REASSOCIATE ALL ADVISERS & CLIENTS" OnClick="btnAsscociateAllAdvisersClient_Click">
                        </telerik:RadButton>
                </td>
                <td>
                </td>
                <td width="100%" align="right">
                    <asp:Label Font-Bold="true" runat="server" ID="lblFeeRunDetails" Text="Clients And Advisers Association"></asp:Label>
                    <asp:HiddenField ID="hfSecID" runat="server" />
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
  <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
    <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
        runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
        AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
        OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
        AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
        OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
        OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
        EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
        OnItemDataBound="PresentationGrid_ItemDataBound">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView DataKeyNames="ENTITYCIID_FIELD" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
            Name="AdvisersAndClients" TableLayout="Fixed" EditMode="EditForms">
            <CommandItemSettings ShowAddNewRecordButton="false" ShowExportToExcelButton="true"
                ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
            <Columns>
             <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                    <HeaderStyle Width="2%"></HeaderStyle>
                    <ItemStyle CssClass="MyImageButton"></ItemStyle>
                </telerik:GridEditCommandColumn>
                <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ENTITYCIID_FIELD" UniqueName="ENTITYCIID_FIELD" />
                <telerik:GridBoundColumn FilterControlWidth="80px"  HeaderStyle-Width="10%" ItemStyle-Width="10%"
                    SortExpression="ClientID" HeaderText="Client ID" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="ClientID" UniqueName="ClientID" ReadOnly="true" >
                </telerik:GridBoundColumn>

                <telerik:GridBoundColumn FilterControlWidth="80px"  HeaderStyle-Width="15%" ItemStyle-Width="15%"
                    SortExpression="ENTITYNAME_FIELD" HeaderText="Client Name" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="ENTITYNAME_FIELD" UniqueName="ENTITYNAME_FIELD" ReadOnly="true" >
                </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn FilterControlWidth="80px"  HeaderStyle-Width="15%" ItemStyle-Width="15%"
                    SortExpression="AdviserID" HeaderText="Adviser ID" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="AdviserID" UniqueName="AdviserID" ReadOnly="true" >
                </telerik:GridBoundColumn>

                 <telerik:GridBoundColumn FilterControlWidth="80px"  HeaderStyle-Width="15%" ItemStyle-Width="15%"
                    SortExpression="AdviserName" HeaderText="Adviser Name" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="AdviserName" UniqueName="AdviserName" ReadOnly="true" >
                </telerik:GridBoundColumn>

                 <telerik:GridDropDownColumn HeaderText="Adviser Name" SortExpression="AdviserName"
                                UniqueName="AdviserListingCombo" DataField="AdviserName" Visible="false" ColumnEditorID="GridDropDownColumnEditorListOfAdvisers" />
            </Columns>
        </MasterTableView>
        <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
            <Excel Format="Html"></Excel>
        </ExportSettings>
    </telerik:RadGrid>
                <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorListOfAdvisers"
                    runat="server" DropDownStyle-Width="200px">
                   </telerik:GridDropDownListColumnEditor>
                   <br />
     </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
