﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="MembershipDetail.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.MembershipDetail" %>

<%@ Register Src="../Controls/OrganizationMembership.ascx" TagName="MembershipControl"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .holder
        {
            width: 100%;
            display: block;
            z-index: 6;
        }
        .content
        {
            background: #fff;
            z-index: 7; /*  padding: 28px 26px 33px 25px;*/
        }
        .popup
        {
            border-radius: 7px;
            background: #6b6a63;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 800px;
            top: 20%;
            left: 50%;
            margin-left: -400px;
            margin-top: -40px;
            z-index: 6;
        }
        .overlay
        {
            width: 100%;
            opacity: 0.65;
            height: 100%;
            left: 0; /*IE*/
            top: 0;
            text-align: center;
            z-index: 5;
            position: fixed;
            background-color: #444444;
        }
    </style>
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td>
                        <telerik:radbutton id="btnBack" runat="server" width="33px" height="33px" tooltip="Back">
                            <image imageurl="~/images/window_previous.png"></image>
                        </telerik:radbutton>
                        <telerik:radbutton id="btnAddExisting" runat="server" text="RadButton" width="33px"
                            height="32px" tooltip="Add Existing" onclick="LnkbtnAddExistngMembersClick">
                            <image imageurl="~/images/add_existing.png"></image>
                        </telerik:radbutton>
                    </td>
                    <td width="90%" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <asp:Label Font-Bold="true" runat="server" ID="lblMemberShip"></asp:Label><asp:HiddenField
                            ID="hfOrgType" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <fieldset>
        <table width="100%">
            <tr runat="server" id="parentRow">
                <td style="font-size: 12px;">Parent Name:
                    <asp:Label Font-Bold="true" runat="server" ID="lblParentName"></asp:Label></td>
                <td style="font-size: 12px;">Parent Id:
                    <asp:Label Font-Bold="true" runat="server" ID="lblParentID"></asp:Label></td>
                <td style="font-size: 12px;">Parent Type:
                    <asp:Label Font-Bold="true" runat="server" ID="lblParentType"></asp:Label></td>
            </tr>


            <tr>
                <td colspan="3">
                    <asp:Label Font-Bold="true" runat="server" ID="lblErrorMessage" ForeColor="Red"></asp:Label>
                </td>
            </tr>



            <tr>
                <td colspan="3">
                    <asp:Label Font-Bold="true" runat="server" ID="lblIncluded"></asp:Label>
                </td>
            </tr>
        </table>
    </fieldset>
    <telerik:radgrid id="gd_Membership" runat="server" autogeneratecolumns="True" pagesize="20"
        allowsorting="True" allowmultirowselection="False" allowpaging="true" gridlines="None"
        allowautomaticinserts="false" allowfilteringbycolumn="true" enableviewstate="true"
        showfooter="false" onneeddatasource="gd_Membership_OnNeedDataSource" onitemcommand="GdMembershipItemCommand"
        onitemdatabound="gd_Membership_OnItemDataBound">
        <pagerstyle mode="NumericPages"></pagerstyle>
        <mastertableview autogeneratecolumns="false" commanditemdisplay="Top" width="100%">
            <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false"
                ShowExportToWordButton="false" ShowExportToPdfButton="false"></CommandItemSettings>
            <Columns>
                <telerik:GridEditCommandColumn EditText="Edit" Visible="false" />
                <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="Cid" HeaderStyle-Width="5%"
                    ItemStyle-Width="5%" HeaderText="CID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Cid" UniqueName="Cid"
                    Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="Clid" HeaderStyle-Width="5%"
                    ItemStyle-Width="5%" HeaderText="CLID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Clid" UniqueName="Clid"
                    Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="Csid" HeaderStyle-Width="5%"
                    ItemStyle-Width="5%" HeaderText="CSID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Csid" UniqueName="Csid"
                    Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridHyperLinkColumn DataTextFormatString="{0}" SortExpression="ClientID" UniqueName="ClientID"                         
                    HeaderText="Client ID" DataTextField="ClientID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    FilterControlWidth="120px" HeaderStyle-Width="5%" HeaderButtonType="TextButton" ShowFilterIcon="true" >                    
                </telerik:GridHyperLinkColumn>                
                <telerik:GridBoundColumn FilterControlWidth="400px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                    SortExpression="Name" HeaderText="Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Name" UniqueName="Name">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="200px" HeaderStyle-Width="8%" ItemStyle-Width="8%"
                    SortExpression="Type" HeaderText="Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="false" DataField="Type" UniqueName="Type">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="100px" HeaderStyle-Width="5%" ItemStyle-Width="5%"
                    SortExpression="Status" HeaderText="Status" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Status" UniqueName="Status">
                </telerik:GridBoundColumn>
                <telerik:GridButtonColumn CommandName="Delete" Text="Delete" ButtonType="LinkButton"
                    HeaderStyle-Width="10%" ItemStyle-Width="10%">
                </telerik:GridButtonColumn>
            </Columns>
        </mastertableview>
    </telerik:radgrid>
    <div class="overlay" id="OVER" visible="False" runat="server">
    </div>
    <div id="MemershipPopup" runat="server" visible="false" class="holder">
        <div class="popup">
            <div class="content">
                <uc1:MembershipControl ID="membershipControl" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
