﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="IndividualList.aspx.cs" Inherits="eclipseonlineweb.IndividualList" %>

<%@ Register Src="../Controls/Individual.ascx" TagName="Individual" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <style>
        .holder
        {
            width: 100%;
            display: block;
            z-index: 6;
        }
        .content
        {
            background: #fff;
            z-index: 7; /*  padding: 28px 26px 33px 25px;*/
        }
        .popup
        {
            border-radius: 7px;
            background: #6b6a63;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 1000px;
            top: 10%;
            left: 40%;
            margin-left: -300px;
            margin-top: -40px;
            z-index: 6;
        }
        
        .popup1
        {
            border-radius: 7px;
            background: #6b6a63;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 1000px;
            top: 20%;
            left: 50%;
            margin-left: -400px;
            margin-top: -40px;
            z-index: 6;
        }
        .overlay
        {
            width: 100%;
            opacity: 0.65;
            height: 100%;
            left: 0; /*IE*/
            top: 0;
            text-align: center;
            z-index: 5;
            position: fixed;
            background-color: #444444;
        }
        
        .wijmo-wijgrid .wijmo-wijgrid-groupheaderrow td
        {
            background-color: #DAE6F4;
            color: #000000;
            font-size: smaller;
            font-weight: bold;
            vertical-align: middle;
        }
        .wijmo-wijgrid .wijmo-wijgrid-footerrow td
        {
            font-weight: bolder;
            text-align: right;
            font-size: smaller;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <telerik:RadButton ID="btn" runat="server" ToolTip="Create an Individual" OnClick="btnAddIndividual_Click">
                                <ContentTemplate>
                                    <img src="../images/add-icon.png" alt="" class="btnImageWithText" />
                                </ContentTemplate>
                            </telerik:RadButton>
                            &nbsp;&nbsp;&nbsp;
                            <telerik:RadButton ID="BtnRunClientIDPatch" runat="server" ToolTip="Run Individual Client ID Patch:This Patch is Required to Run because Individual ID of Old records were changing after IIS Reset"
                                OnClick="BtnRunClientID_Click">
                                <ContentTemplate>
                                    <img src="../images/database.png" alt="" class="btnImageWithText" />
                                </ContentTemplate>
                            </telerik:RadButton>
                            <telerik:RadButton ID="btnBackUp" runat="server" ToolTip="Run Individual Clients Patch:This Patch is Required to Run because Individual needs to hold references of the its Clients Shares"
                                OnClick="btnSetIndividualShares_Click">
                                <ContentTemplate>
                                    <img src="../images/database.png" alt="" class="btnImageWithText" />
                                </ContentTemplate>
                            </telerik:RadButton>
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Individuals"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <asp:Panel runat="server" ID="pnlMainGrid" Visible="true">
                <telerik:RadGrid ID="gvIndividualList" runat="server" AutoGenerateColumns="True"
                    PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true"
                    GridLines="Horizontal" AllowAutomaticUpdates="true" AllowFilteringByColumn="true"
                    OnNeedDataSource="gvIndividualBList_OnNeedDataSource" OnItemCommand="gvIndividualList_OnItemCommand"
                    EnableViewState="true" ShowFooter="True">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AutoGenerateColumns="false">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Cid" Display="false" HeaderButtonType="TextButton"
                                DataField="Cid" UniqueName="Cid">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="CLid" Display="false" HeaderButtonType="TextButton"
                                DataField="CLID" UniqueName="CLid">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="CSid" Display="False" HeaderButtonType="TextButton"
                                DataField="CSID" UniqueName="CSid">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="120px" HeaderStyle-Width="7%" ItemStyle-Width="200px"
                                SortExpression="Fullname" HeaderText="Name" AutoPostBackOnFilter="False" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Fullname" UniqueName="Fullname">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="8%" ItemStyle-Width="120px"
                                SortExpression="EmailAddress" HeaderText="Email" AutoPostBackOnFilter="False"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="EmailAddress" UniqueName="EmailAddress">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="2%" ItemStyle-Width="30px"
                                SortExpression="LicenseNumber" HeaderText="License Number" AutoPostBackOnFilter="False"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="LicenseNumber" UniqueName="LicenseNumber">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" Visible="False" SortExpression="OrganizationType"
                                HeaderText="Organization Type" AutoPostBackOnFilter="False" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="OrganizationType"
                                UniqueName="OrganizationType">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="60px" ReadOnly="true" HeaderStyle-Width="3%"
                                ItemStyle-Width="30px" SortExpression="OrganizationStatus" HeaderText="Organization Status"
                                AutoPostBackOnFilter="False" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="OrganizationStatus" UniqueName="OrganizationStatus">
                            </telerik:GridBoundColumn>
                            <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="Cid"
                                AllowFiltering="False" HeaderStyle-Width="4%" DataNavigateUrlFormatString="SecurityConfiguration.aspx?ins={0}"
                                HeaderText="Security" Text="Security Configutration">
                            </telerik:GridHyperLinkColumn>
                             <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="Cid" HeaderStyle-Width="3%"
                                AllowFiltering="False" DataNavigateUrlFormatString="IndividualClientstAssociation.aspx?ins={0}"
                                HeaderText="Clients" Text="View Clients">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridButtonColumn CommandName="EditRecords" Text="Edit" ButtonType="LinkButton"
                                HeaderTooltip="Edit" HeaderStyle-Width="2%" ItemStyle-Width="2%">
                            </telerik:GridButtonColumn>
                            <telerik:GridButtonColumn CommandName="Delete" Text="Delete" ButtonType="LinkButton"
                                ConfirmText="Are you sure you want to delete the selected record?" HeaderStyle-Width="2%"
                                ItemStyle-Width="2%">
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </asp:Panel>
            <div id="IndividualModal" runat="server" visible="false" class="holder">
                <div class="popup">
                    <div class="content">
                        <uc1:Individual ID="IndividualControl" runat="server" />
                    </div>
                </div>
            </div>
            <div class="overlay" id="OVER" visible="False" runat="server">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
