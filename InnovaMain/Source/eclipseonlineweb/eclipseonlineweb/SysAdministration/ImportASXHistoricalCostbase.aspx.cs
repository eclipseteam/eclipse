﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using eclipseonlineweb.WebControls;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class ImportASXHistoricalCostbase : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void Intialise()
        {
            ImportMessageControl.GridImportMessages.ColumnCreated += GridImportMessages_ColumnCreated;
            ImportMessageControl.GirdInvestmentCodeError.ColumnCreated += GridImportErrorMessages_ColumnCreated;
            ImportMessageControl.GridImportMessages.ItemDataBound += GridImportMessages_ItemDataBound;
            if (!IsPostBack)
            {
                ImportMessageControl.Clear();
            }

            ImportMessageControl.Completed += text =>
            {
                switch (text.ToLower())
                {
                    case "next":
                        SendData();
                        break;
                    default:
                        ClearMessageControls();
                        break;
                }
            };
        }

        void GridImportMessages_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item).ItemIndex != -1)
            {
                DataRow dr = ((DataRowView)((e.Item).DataItem)).Row;
                if (bool.Parse(dr["HasErrors"].ToString()))
                {
                    e.Item.ForeColor = Color.Red;
                }
            }

        }
        public override void LoadPage()
        {
            ImportMessageControl.ShowHideSampleFileLink("~/Templates/ImportSampleFiles/BulkTradeExport1DF7.xls");
        }
        void GridImportMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            SetDataGridColumns(e);
        }

        void GridImportErrorMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            SetDataGridColumns(e);
        }

        public void BtnUploadClick(object sender, EventArgs e)
        {
            // Check to see if file was uploaded
            if (filMyFile.PostedFile != null)
            {
                // Get a reference to PostedFile object
                HttpPostedFile myFile = filMyFile.PostedFile;

                // Get size of uploaded file
                int nFileLen = myFile.ContentLength;

                // make sure the size of the file is > 0
                if (nFileLen > 0)
                {
                    // Allocate a buffer for reading of the file
                    string fileName = Path.GetFileName(myFile.FileName);
                    hf_FileName.Value = fileName;
                    var date = DateTime.Now;
                    var distributionDictory = Server.MapPath("~/App_Data/Import/");
                    if (!Directory.Exists(distributionDictory))
                    {
                        Directory.CreateDirectory(distributionDictory);
                    }

                    string fileLocation = distributionDictory + date.ToString("ddMMyyhhmmsstt") + fileName;
                    byte[] myData = new byte[nFileLen];

                    // Read uploaded file from the Stream
                    myFile.InputStream.Read(myData, 0, nFileLen);

                    // Write data into a file
                    Utilities.WriteToFile(fileLocation, ref myData);

                    SpreadsheetGear.IWorkbook workbook = SpreadsheetGear.Factory.GetWorkbookSet().Workbooks.OpenFromStream(filMyFile.PostedFile.InputStream);
                    DataSet dss = workbook.GetDataSet(SpreadsheetGear.Data.GetDataFlags.FormattedText);

                    ImportProcessDS ds = new ImportProcessDS();
                    foreach (DataTable dt in dss.Tables)
                    {
                        ds.Tables.Add(dt.Copy());
                    }

                    ds.Unit = new OrganizationUnit
                    {
                        Name = "Order " + DateTime.Now.ToString("dd MMM, yyyy hh:mm:ss"),
                        Type = ((int)OrganizationType.Order).ToString(),
                        CurrentUser = GetCurrentUser()
                    };
                    ds.Command = (int)WebCommands.ValidateASXHistoricalCostBaseOfTransferredAssets;
                    ds.FileName = hf_FileName.Value;
                    if (!ds.ExtendedProperties.Contains("CID"))
                        ds.ExtendedProperties.Add("CID", Request.QueryString["ins"].ToString());
                    SaveOrganizanition(ds);
                    SetValidationGird(ds);
                    PresentationData = ds;
                    string filename = ImportMessageControl.RefreshFileID() + ".xml";
                    ds.WriteXml(distributionDictory + filename, XmlWriteMode.WriteSchema);
                }
            }
        }

        private void SetValidationGird(ImportProcessDS ds)
        {
            //ImportMessageControl.Clear();
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    DataRow dr = missingItems.NewRow();
                    dr["MissingItem"] = row["Client Id"];
                    missingItems.Rows.Add(dr);
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();

                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingClientAccountsIgnored;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }
            int successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
            if (ds.Tables[0].Rows.Count == 0 || successfulRows == 0)
            {
                ImportMessageControl.SetButtons(ButtonTypes.CLOSE);
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sErrorsCannotContinue, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Error);
                ImportMessageControl.TextMessage.Text = "";
            }
            else
            {
                if (ds.ExtendedProperties.Contains("Result"))
                {
                    ImportMessageControl.SetButtons(ButtonTypes.CLOSE);
                    ImportMessageControl.SetTextMessage(string.Format(ds.ExtendedProperties["Message"].ToString(), successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Error);
                    ImportMessageControl.TextMessage.Text = "";
                }
                else
                {
                    ImportMessageControl.SetButtons(ButtonTypes.NEXTCANCEL);
                    //success
                    ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sValidationCompleted, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
                }
            }
        }

        private void SetCompletionGird(ImportProcessDS ds)
        {
            ImportMessageControl.Clear();
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }

            using (DataView dv = new DataView(ds.Tables[0]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    DataRow dr = missingItems.NewRow();
                    dr["MissingItem"] = row["Client Id"];
                    missingItems.Rows.Add(dr);
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();

                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingClientAccountsIgnored;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }
            int successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
            ImportMessageControl.SetButtons(ButtonTypes.FINISH);
            ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sImportFinish, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
        }

        private void SendData()
        {
            var distributionDirectory = Server.MapPath("~/App_Data/Import/");
            if (!Directory.Exists(distributionDirectory))
            {
                Directory.CreateDirectory(distributionDirectory);
            }

            string filename = distributionDirectory + ImportMessageControl.GetFileID() + ".xml";
            ImportProcessDS ds = new ImportProcessDS();
            ds.ReadXml(filename);
            ds.Unit = new OrganizationUnit
            {
                Name = "Order " + DateTime.Now.ToString("dd MMM, yyyy hh:mm:ss"),
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = GetCurrentUser()
            };
            ds.Command = (int)WebCommands.ImportASXHistoricalCostBaseOfTransferredAssets;
            ds.FileName = hf_FileName.Value;
            SaveOrganizanition(ds);
            SetCompletionGird(ds);
            
        }

        public void ClearMessageControls()
        {
            ImportMessageControl.Clear();
            ImportMessageControl.TextMessage.Text = "";
        }

        private static void SetDataGridColumns(GridColumnCreatedEventArgs e)
        {
            string colName = e.Column.UniqueName.ToLower();
            switch (colName)
            {
                case "Client Id":
                    e.Column.HeaderText = "Client Id";
                    break;
                case "Client Name":
                    e.Column.HeaderText = "Client Name";
                    e.Column.FilterControlWidth = Unit.Pixel(50);
                    break;
                case "Investment Code":
                    e.Column.HeaderText = "Investment Code";
                    e.Column.FilterControlWidth = Unit.Pixel(50);
                    break;
                case "Transaction Date":
                    e.Column.HeaderText = "Transaction Date";
                    e.Column.FilterControlWidth = Unit.Pixel(170);
                    break;
                case "Unit Price":
                    e.Column.HeaderText = "Unit Price";
                    ((GridBoundColumn)e.Column).DataFormatString = "{0:f2}";
                    break;
                case "Units ":
                    e.Column.HeaderText = "Units";
                    ((GridBoundColumn)e.Column).DataFormatString = "{0:f4}";
                    break;
                case "Brokerage":
                    e.Column.HeaderText = "Brokerage";
                    break;
                case "Net Value":
                    e.Column.HeaderText = "Net Value";
                    break;
                case "message":
                    e.Column.HeaderText = "Message";
                    break;
                case "haserrors":
                    e.Column.HeaderText = "Has Errors";
                    e.Column.Visible = false;
                    break;
                case "ismissingitem":
                    e.Column.HeaderText = "Is Missing Item";
                    e.Column.Visible = false;
                    break;
                case "missingitem":
                    e.Column.HeaderText = "Missing Client Accounts";
                    break;
            }
        }
    }
}