﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="AdviserDetails.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.AdviserDetails" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register Src="../Controls/AdviserInformation.ascx" TagName="AdviserInformation"
    TagPrefix="uc2" %>
<%@ Register Src="../Controls/BankAccountMappingControl.ascx" TagName="BankAccountMappingControl"
    TagPrefix="uc3" %>
<%@ Register Src="../Controls/AddressDetailControl.ascx" TagName="AddressControl"
    TagPrefix="uc2" %>
<%@ Register Src="../Controls/SecurityConfigurationControl.ascx" TagName="SecurityConfigurationControl"
    TagPrefix="uc2" %>
<%@ Register TagPrefix="uc5" TagName="ContactMappingControl" Src="~/Controls/ContactMappingControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server">
                <fieldset>
                    <table width="100%">
                        <tr>
                            <td style="width: 5%">
                                <asp:ImageButton runat="server" ImageUrl="~/images/window_previous.png" ToolTip="Back"
                                    AlternateText="Back" ID="btnBack" PostBackUrl="Advisor.aspx" />
                            </td>
                            <td width="90%" class="breadcrumbgap">
                                <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                                <br />
                                <asp:Label Font-Bold="true" runat="server" ID="lblAdvisor" Text="Adviser Detail"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </asp:Panel>
            <br />
            <telerik:radtabstrip id="RadTabStrip1" runat="server" skin="Vista" multipageid="RadMultiPage1"
                selectedindex="0">
                <tabs>
                    <telerik:RadTab Text="Adviser Information" runat="server" Selected="True" Value="TabAdviserInformation">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Address" runat="server" Visible="true" Value="TabAddress">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Bank Accounts" runat="server" Visible="true" Value="TabBankAccounts">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Security" runat="server" Visible="false" Value="TabSecurity">
                    </telerik:RadTab>
                    <%--<telerik:RadTab Text="Documents" runat="server" Visible="false">
                    </telerik:RadTab>--%>
                </tabs>
            </telerik:radtabstrip>
            <telerik:radmultipage runat="server" id="RadMultiPage1" selectedindex="0">
                <telerik:radpageview runat="server" id="AdvierInfoPageView">
                    <uc2:AdviserInformation ID="AdviserInfo" runat="server" />
                </telerik:radpageview>
                <telerik:radpageview id="AddressPageView" runat="server">
                    <fieldset style="width: 98%">
                        <div style="text-align: left; float: none; border: 1px; background-color: White;
                            height: 25px; position: relative; vertical-align: middle; margin: 0; padding: 0;">
                            <telerik:radbutton id="btnSave" runat="server" width="32px" height="32px" tooltip="Save Changes"
                                onclick="BtnSaveClick">
                                <image imageurl="~/images/Save-Icon.png" />
                            </telerik:radbutton>
                        </div>
                    </fieldset>
                    <br />
                    <uc2:AddressControl ID="AddressDetailControl" runat="server" />
                </telerik:radpageview>
                <telerik:radpageview id="BankPageView" runat="server">
                    <uc3:BankAccountMappingControl ID="BankAccountMappingControl" runat="server" />
                </telerik:radpageview>
                <telerik:radpageview id="SecurityPageView" runat="server">
                    <uc2:SecurityConfigurationControl ID="SecurityConfiguration" runat="server" />
                </telerik:radpageview>
                <telerik:radpageview id="IndividualPageView" runat="server">
                    <uc5:ContactMappingControl ID="IndividualMappingControl" runat="server" IndividualsType="Individual"
                        SingleSelect="true" />
                </telerik:radpageview>
            </telerik:radmultipage>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
