﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using eclipseonlineweb.WebControls;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Commands;
using System.Web.UI;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;


namespace eclipseonlineweb
{
    public partial class ImportSMAClientDataFeed : UMABasePage
    {

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void Intialise()
        {
            ImportMessageControl.GridImportMessages.ColumnCreated += GridImportMessages_ColumnCreated;
            ImportMessageControl.GirdInvestmentCodeError.ColumnCreated += GridImportErrorMessages_ColumnCreated;
            ImportMessageControl.GridImportMessages.ItemDataBound += GridImportMessages_ItemDataBound;

            if (!IsPostBack)
            {
                ImportMessageControl.Clear();
            }

            ImportMessageControl.Completed += text =>
            {
                switch (text.ToLower())
                {
                    case "next":
                        SendData();
                        break;
                    default:
                        ClearMessageControls();
                        break;
                }
            };


        }

        public override void LoadPage()
        {
            var sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
                sm.RegisterPostBackControl(this.btnDownloadListOfAdvisers);
        }

        public void ClearMessageControls()
        {
            ImportMessageControl.Clear();
            ImportMessageControl.TextMessage.Text = "";
        }
        private void GridImportMessages_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item).ItemIndex == -1) return;

            DataRow dr = ((DataRowView)((e.Item).DataItem)).Row;
            if (bool.Parse(dr["HasErrors"].ToString()))
            {
                e.Item.ForeColor = Color.Red;
            }
        }

        void GridImportMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {

            GridImportMessages_ColumnCreated(e);
        }

        void GridImportErrorMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            GridImportMessages_ColumnCreated(e);
        }


        protected void btnImportFull_OnClick(object sender, EventArgs e)
        {
            ProcessFile(string.Empty, false);
        }

        protected void btnImportFullOneAccount_OnClick(object sender, EventArgs e)
        {
            ProcessFile(this.txtNewAccount.Text, true);
        }

        protected void btnImportLastFiveDays_OnClick(object sender, EventArgs e)
        {
            ProcessFileLastFiveDays();
        }

        protected void btnRefreshTransactionsMISData_OnClick(object sender, EventArgs e)
        {
            ProcessFileRefreshMISTransaction();
        }

        protected void btnDownloadListOfAdvisers_OnClick(object sender, EventArgs e)
        {
            this.ProcessFileAdviserList();
        }

        protected void btnMapAdvisers_OnClick(object sender, EventArgs e)
        {
            this.ProcessFileAdviserMAP();
        }

        protected void btnUpdateAccountStatus_OnClick(object sender, EventArgs e)
        {
            this.ProcessAccountStatus();
        }

        protected void btnDeleteClosedAccounts_OnClick(object sender, EventArgs e)
        {
            this.ProcessDeleteClosedAccounts();
        }

        protected void btnUpdateOrderID_OnClick(object sender, EventArgs e)
        {
            IBrokerManagedComponent user = this.UMABroker.GetBMCInstance("Administrator", "DBUser_1_1");
            IOrganization iorgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            object[] args = new Object[3] { iorgCM.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(this.UMABroker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[OrganisationListingDS.TableName]);
            view.RowFilter = OrganisationListingDS.GetRowFilterEclipseSuper();
            view.Sort = "ENTITYNAME_FIELD ASC";
            DataTable orgFiteredTable = view.ToTable();

            int count = 0;

            foreach (DataRow row in orgFiteredTable.Rows)
            {
                UMABroker.SaveOverride = true;
                UMABroker.SetStart(); 
                Guid entityCid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit orgUnit = this.UMABroker.GetBMCInstance(entityCid) as IOrganizationUnit;
                if (orgUnit.StatusType == StatusType.Active)
                {
                    SMAImportProcessDS ds = new SMAImportProcessDS();
                    ds.Variables.Add("MemberCode", orgUnit.SuperMemberID);
                    ds.Variables.Add("OrgCid", orgUnit.CID.ToString());
                    ds.Variables.Add("EclipseID", orgUnit.ClientId.ToString());

                    ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
                    ds.Command = (int)WebCommands.ImportSMASetOtherID;
                    IOrganization organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                    organization.GetData(ds);
                    count++;
                }
            }
        }

        protected void btnApplyModelUpdateAcrossEclipseSuperClient(object sender, EventArgs e)
        {
            IBrokerManagedComponent user = this.UMABroker.GetBMCInstance("Administrator", "DBUser_1_1");
            IOrganization iorgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            object[] args = new Object[3] { iorgCM.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(this.UMABroker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[OrganisationListingDS.TableName]);
            view.RowFilter = OrganisationListingDS.GetRowFilterEclipseSuper();
            view.Sort = "ENTITYNAME_FIELD ASC";
            DataTable orgFiteredTable = view.ToTable();

            int count = 0; 

            foreach (DataRow row in orgFiteredTable.Rows)
            {
                UMABroker.SaveOverride = true;
                Guid entityCid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                
                ImportProcessDS ds = new ImportProcessDS();

                IOrganizationUnit orgUnit = this.UMABroker.GetBMCInstance(entityCid) as IOrganizationUnit;
                if (orgUnit.StatusType == StatusType.Active)
                {
                    ds.Variables.Add("MemberCode", orgUnit.SuperMemberID);
                    ds.Variables.Add("OrgCid", orgUnit.CID.ToString());
                    ds.Variables.Add("EclipseID", orgUnit.ClientId.ToString());
                    ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
                    ds.Command = (int)WebCommands.ImportSMAClientPortfolioUpdate;

                    iorgCM.GetData(ds);
                    count++;
                }
            }
        }

        private void ProcessPortfolio()
        {
            ImportProcessDS ds = new ImportProcessDS();
            IOrganizationUnit orgUnit = this.UMABroker.GetBMCInstance(new Guid(this.cid)) as IOrganizationUnit;
            ds.Variables.Add("MemberCode", orgUnit.SuperMemberID);
            ds.Variables.Add("OrgCid", orgUnit.CID.ToString());
            ds.Variables.Add("EclipseID", orgUnit.ClientId.ToString());

            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportSMAClientPortfolioUpdate;
            IOrganization organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            organization.GetData(ds);
        }


        private void ProcessDeleteClosedAccounts()
        {
            SMAImportProcessDS ds = new SMAImportProcessDS();
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportSMAAccountDeleteClosedAccounts;
            IOrganization organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            organization.GetData(ds);
            SetCompletionGird(ds);
        }

        protected void btnUpdateASXFIIG_OnClick(object sender, EventArgs e)
        {
            this.ProcessASXFIGGForSMA();
        }

        private void ProcessASXFIGGForSMA()
        {
            SMAImportProcessDS ds = new SMAImportProcessDS();
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportSMAAccountAddFiiGASX;
            IOrganization organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            organization.GetData(ds);
            SetCompletionGird(ds);
        }

        private void ProcessAccountStatus()
        {
            SMAImportProcessDS ds = new SMAImportProcessDS();
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportSMAAccountStatus;
            IOrganization organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            organization.GetData(ds);
            SetCompletionGird(ds);
        }

        private void ProcessFile(string para, bool importByID)
        {
            ImportProcessDS ds = new ImportProcessDS();
            ds.ByIDPara = para;
            ds.ImportByID = importByID;
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportSMAClientMembersWithFULLData;
            IOrganization organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            organization.GetData(ds);
            SetCompletionGird(ds);
            PresentationData = ds;
        }

        private void ProcessFileLastFiveDays()
        {
            SMAImportProcessDS ds = new SMAImportProcessDS();
            ds.StartDate = new DateTime(2011,7,1);
            ds.EndDate = DateTime.Now;
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportSMATransactions;
            IOrganization organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            organization.GetData(ds);
            SetCompletionGird(ds);
            PresentationData = ds;
        }

        private void ProcessFileRefreshMISTransaction()
        {
            ImportProcessDS ds = new ImportProcessDS();
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportSMARefreshMISTransactions;

            IOrganization organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            organization.GetData(ds);
            SetCompletionGird(ds);
            PresentationData = ds;
        }

        private void ProcessFileAdviserList()
        {
            ImportProcessDS ds = new ImportProcessDS();
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportSMAAdviserList;
            IOrganization organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            organization.GetData(ds);

            DataSet excelDataset = new DataSet();
            excelDataset.Merge(ds.Tables["AdviserDetailsViewModel"], true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, "Super Adviser List ["+DateTime.Now.ToString("dd-MMM-yyyy")+"].xls", this.Page.Response);
            PresentationData = ds;
        }


        private void ProcessFileAdviserMAP()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            DBUserDetailsDS dBUserDetailsDS = new Oritax.TaxSimp.Security.DBUserDetailsDS();
            dBUserDetailsDS.UserList = true;
            objUser.GetData(dBUserDetailsDS);
            
            SMAImportProcessDS ds = new SMAImportProcessDS();
            ds.UserDetail = dBUserDetailsDS.Tables["UserDetail"]; 
            
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportSMAAdviserMAP;
            IOrganization organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            organization.GetData(ds);
            SetCompletionGird(ds);
        }

        private void SetResultGird(ImportProcessDS ds)
        {
        }

        private void SendData()
        {
            var distributionDictory = Server.MapPath("~/App_Data/Import/MacquireTransactions/");
            if (!Directory.Exists(distributionDictory))
            {
                Directory.CreateDirectory(distributionDictory);
            }
            
            ImportProcessDS ds = new ImportProcessDS();
         
          bool useFile = bool.Parse(txtIsFile.Value);
            if (useFile)
            {
                string filename = distributionDictory + ImportMessageControl.GetFileID() + ".xml";
                ds.FileName = filename;
               
                ds.ReadXml(filename);
                if (!ds.Tables[0].Columns.Contains("Message"))
                ds.Tables[0].Columns.Remove("Message");
                if (!ds.Tables[0].Columns.Contains("HasErrors"))
                ds.Tables[0].Columns.Remove("HasErrors");
                if (!ds.Tables[0].Columns.Contains("IsMissingItem"))
                ds.Tables[0].Columns.Remove("IsMissingItem");
            }
            ds.UseFile = useFile;
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.MaquarieBankTransactionsImport;
            SaveOrganizanition(ds);
            SetCompletionGird(ds);
        }

        private void SetCompletionGird(ImportProcessDS ds)
        {
            ImportMessageControl.Clear();
            try
            {
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    using (DataView dv = new DataView(dt))
                    {
                        ImportMessageControl.GridImportMessages.DataSource = dv;
                        ImportMessageControl.GridImportMessages.DataBind();
                    }

                    using (DataView dv = new DataView(dt))
                    {
                        dv.RowFilter = "IsMissingItem='true'";
                        DataTable missingItems = new DataTable();
                        missingItems.Columns.Add("MissingItem");
                        DataRow dr;
                        foreach (DataRow row in dv.ToTable().Rows)
                        {
                            dr = missingItems.NewRow();
                            dr["MissingItem"] = row["Message"];
                            missingItems.Rows.Add(dr);
                        }

                        if (missingItems.Rows.Count > 0)
                        {
                            ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                            ImportMessageControl.GirdInvestmentCodeError.DataBind();

                            ImportMessageControl.TextMessage.Text = ImportMessages.sMissingAccountsIgnored;
                        }
                        else
                        {
                            ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                            ImportMessageControl.GirdInvestmentCodeError.DataBind();
                            ImportMessageControl.TextMessage.Text = "";
                        }
                    }

                    int successfulRows = dt.Select("HasErrors='false'").Length;
                    ImportMessageControl.SetButtons(ButtonTypes.FINISH);
                    ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sImportedSuccessfully, successfulRows, dt.Rows.Count - successfulRows), ImportMessageType.Sucess);
                }
            }
            catch
            { }
        }

        private void SetValidationGird(ImportProcessDS ds)
        {
           
            if (ds.Tables.Count > 0)
            {
                using (DataView dv = new DataView(ds.Tables[0]))
                {
                    ImportMessageControl.GridImportMessages.DataSource = dv;
                    ImportMessageControl.GridImportMessages.DataBind();
                }
                using (DataView dv = new DataView(ds.Tables[0]))
                {
                    dv.RowFilter = "IsMissingItem='true'";
                    DataTable missingItems = new DataTable();
                    missingItems.Columns.Add("MissingItem");
                    DataRow dr;
                    foreach (DataRow row in dv.ToTable().Rows)
                    {
                        dr = missingItems.NewRow();
                        dr["MissingItem"] = row["Message"];
                        missingItems.Rows.Add(dr);
                    }

                    if (missingItems.Rows.Count > 0)
                    {
                        ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                        ImportMessageControl.GirdInvestmentCodeError.DataBind();

                        ImportMessageControl.TextMessage.Text = ImportMessages.sMissingAccountsIgnored;
                    }
                    else
                    {
                        ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                        ImportMessageControl.GirdInvestmentCodeError.DataBind();
                        ImportMessageControl.TextMessage.Text = "";
                    }
                }
                int successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
                if (ds.Tables[0].Rows.Count == 0 || successfulRows == 0)
                {
                    ImportMessageControl.SetButtons(ButtonTypes.CLOSE);
                    ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sErrorsCannotContinue, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Error);
                    ImportMessageControl.TextMessage.Text = "";
                }
                else
                {
                    ImportMessageControl.SetButtons(ButtonTypes.NEXTCANCEL);
                    //sucess
                    ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sValidationCompleted, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
                }
            }

        }
    
        protected void GridImportMessages_ColumnCreated(GridColumnCreatedEventArgs e)
        {
            string colName = e.Column.UniqueName.ToLower();
            switch (colName)
            {
                case "message":
                    e.Column.HeaderText = "Message";
                    break;
                case "haserrors":
                    e.Column.HeaderText = "Has Errors";
                    e.Column.Visible = false;
                    break;
                case "ismissingitem":
                    e.Column.HeaderText = "Is Missing Item";
                    e.Column.Visible = false;
                    break;
                case "missingitem":
                    e.Column.HeaderText = "Missing Investment Code";
                    break;
                case "accountnumber":
                case "transactiondate":
                case "reversalflag":
                case "debitcredit":
                case "amount":
                case "narrative":
                case "transactiontype":
                case "systemtransactiontype":
                case "importtransactiontypedetail":
                    e.Column.Visible = true;
                    break;
                default:
                    e.Column.Visible = false;
                    break;
            }
        }
    }
}