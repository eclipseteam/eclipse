﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MissingDividendsReport.aspx.cs"
    MasterPageFile="AdminMaster.master" Inherits="eclipseonlineweb.SysAdministration.MissingDividendsReport" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <fieldset>
        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
    </fieldset>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <legend>Filters</legend>
                <table>
                    <tr>
                        <td>
                            Start Date:
                        </td>
                        <td>
                            <telerik:RadDatePicker ID="dtStartDate" runat="server" Width="200px" Calendar-RangeMinDate="1/01/1900 12:00:00 AM"
                                DateInput-MinDate="1/01/1900 12:00:00 AM" MinDate="1/01/1900 12:00:00 AM">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                                </DateInput>
                            </telerik:RadDatePicker>
                        </td>
                        <td>
                            End Date:
                        </td>
                        <td>
                            <telerik:RadDatePicker ID="dtEndDate" runat="server" Width="200px" Calendar-RangeMinDate="1/01/1900 12:00:00 AM"
                                DateInput-MinDate="1/01/1900 12:00:00 AM" MinDate="1/01/1900 12:00:00 AM">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                                </DateInput>
                            </telerik:RadDatePicker>
                        </td>
                        <td>
                            <telerik:RadButton runat="server" ID="btnAddDealerGroup" ToolTip="Show Report" OnClick="btnRunReport_Click">
                                <ContentTemplate>
                                    <img src="../images/Run.jpg" alt="" class="btnImageWithText" />
                                    <span class="riLabel">Run</span>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </td>
                        <td>
                            <telerik:RadButton runat="server" ID="btnExportToExcel" ToolTip="Download as Excel" OnClick="btnExportToExcel_Click" Visible="false">
                                <ContentTemplate>
                                    <img src="../images/export_excel.png" alt="" class="btnImageWithText" />
                                    <span class="riLabel">Download</span>
                                </ContentTemplate>
                            </telerik:RadButton>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <fieldset>
                <legend>Bank Transactions</legend>
                <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                    Width="100%" PageSize="20" AllowSorting="True" AllowMultiRowSelection="False"
                    AllowPaging="True" GridLines="None" AllowFilteringByColumn="true" EnableViewState="true"
                    ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AllowFilteringByColumn="True" TableLayout="Fixed" Width="100%">
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ID" UniqueName="ID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="BankCID" ReadOnly="true" HeaderText="BankCID"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="BankCID" UniqueName="BankCID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderStyle-Width="10%" FilterControlWidth="75%" SortExpression="ClientID"
                                ReadOnly="true" HeaderText="ClientID" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ClientID" UniqueName="ClientID">
                            </telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn HeaderText="Account" Visible="false" SortExpression="AccountName"
                                UniqueName="DDLAccountList" DataField="AccountName" ColumnEditorID="GridDropDownListColumnEditorAccountList" />
                            <telerik:GridBoundColumn FilterControlWidth="45px" HeaderStyle-Width="80px" SortExpression="ImportTransactionType"
                                ReadOnly="true" HeaderText="Import Transaction" HeaderButtonType="TextButton"
                                DataField="ImportTransactionType" UniqueName="ImportTransactionType">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="75%" SortExpression="SystemTransactionType"
                                ReadOnly="true" HeaderText="System Transaction" HeaderButtonType="TextButton"
                                DataField="SystemTransactionType" UniqueName="SystemTransactionType">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="85px" SortExpression="Category"
                                ReadOnly="true" HeaderText="Category" HeaderButtonType="TextButton" DataField="Category"
                                UniqueName="Category">
                            </telerik:GridBoundColumn>
                            <telerik:GridDateTimeColumn Visible="true" UniqueName="BankTransactionDate" PickerType="DatePicker"
                                HeaderText="Transaction Date" HeaderStyle-BackColor="LightGray" DataField="BankTransactionDate" DataFormatString="{0:dd/MM/yyyy}">
                                <ItemStyle Width="85px" />
                            </telerik:GridDateTimeColumn>
                            <telerik:GridBoundColumn FilterControlWidth="75%" ReadOnly="true" SortExpression="AccountNO"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Account Number"
                                HeaderButtonType="TextButton" DataField="AccountNO" UniqueName="AccountNO">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="35px" HeaderStyle-Width="70px" ReadOnly="true"
                                SortExpression="AccountType" HeaderText="Account Type" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" HeaderButtonType="TextButton" DataField="AccountType"
                                UniqueName="AccountType">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="75%" ReadOnly="false" SortExpression="BankAmount"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Amount"
                                HeaderButtonType="TextButton" DataField="BankAmount" UniqueName="BankAmount"
                                DefaultInsertValue="0" DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="75%" ReadOnly="false" SortExpression="BankAdjustment"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" DefaultInsertValue="0"
                                HeaderText="Adjustment" HeaderButtonType="TextButton" DataField="BankAdjustment"
                                UniqueName="BankAdjustment" DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="75%" ReadOnly="true" SortExpression="AmountTotal"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" DefaultInsertValue="0"
                                HeaderText="Total" HeaderButtonType="TextButton" DataField="AmountTotal" UniqueName="AmountTotal"
                                DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="75%" ReadOnly="false" SortExpression="Comment"
                                HeaderText="Comments" HeaderButtonType="TextButton" DataField="Comment" UniqueName="Comment">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="75px" ReadOnly="true"
                                SortExpression="DividendStatus" HeaderText="Status" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" HeaderButtonType="TextButton" DataField="DividendStatus"
                                UniqueName="DividendStatus">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </fieldset>
            <fieldset>
                <legend>Dividends Transactions</legend>
                <telerik:RadGrid ID="DividendGird" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                    Width="100%" PageSize="20" AllowSorting="True" AllowMultiRowSelection="False"
                    AllowPaging="True" GridLines="None" AllowFilteringByColumn="true" EnableViewState="true"
                    ShowFooter="false" OnNeedDataSource="DividendGird_OnNeedDataSource">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView AllowFilteringByColumn="True" TableLayout="Fixed" Width="100%">
                        <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                DataField="ID" UniqueName="ID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderStyle-Width="10%" FilterControlWidth="75%" SortExpression="ClientID"
                                ReadOnly="true" HeaderText="ClientID" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ClientID" UniqueName="ClientID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderStyle-Width="5%" FilterControlWidth="55%" SortExpression="InvestmentCode"
                                ReadOnly="true" HeaderText="Code" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InvestmentCode"
                                UniqueName="InvestmentCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderStyle-Width="10%" FilterControlWidth="75%" SortExpression="TransactionType"
                                ReadOnly="true" HeaderText="Transaction Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransactionType"
                                UniqueName="TransactionType">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70%" SortExpression="BalanceDate" ReadOnly="true"
                                HeaderText="Balance Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="BalanceDate" UniqueName="BalanceDate"
                                DataFormatString="{0:dd/MM/yyyy}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70%" SortExpression="DividendType" ReadOnly="true"
                                HeaderText="Dividend Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="DividendType"
                                UniqueName="DividendType">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70%" SortExpression="RecordDate" HeaderText="Ex-Dividend Date"
                                ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="RecordDate" UniqueName="RecordDate"
                                DataFormatString="{0:dd/MM/yyyy}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="BooksCloseDate"
                                ShowFilterIcon="true" HeaderText="Books Close Date" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" HeaderButtonType="TextButton" DataField="BooksCloseDate"
                                UniqueName="BooksCloseDate" DataFormatString="{0:dd/MM/yyyy}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="PaymentDate"
                                HeaderText="Payment Date" AutoPostBackOnFilter="true" ShowFilterIcon="true" CurrentFilterFunction="Contains"
                                HeaderButtonType="TextButton" HeaderStyle-BackColor="LightGray" DataField="PaymentDate" UniqueName="PaymentDate"
                                DataFormatString="{0:dd/MM/yyyy}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="UnitsOnHand"
                                HeaderText="Amount" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UnitsOnHand" UniqueName="UnitsOnHand">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="CentsPerShare"
                                HeaderText="Cents Per Share" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CentsPerShare"
                                UniqueName="CentsPerShare" DataFormatString="{0:N4}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="PaidDividend"
                                HeaderText="Paid Dividend" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="PaidDividend"
                                UniqueName="PaidDividend" DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="FrankingCredits"
                                HeaderText="Franking Credits (CPS)" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="FrankingCredits"
                                UniqueName="FrankingCredits" DataFormatString="{0:N4}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="TotalFrankingCredits"
                                HeaderText="Total Franking Credits ($)" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TotalFrankingCredits"
                                UniqueName="TotalFrankingCredits" DataFormatString="{0:C}">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="Currency"
                                HeaderText="Currency (Ccy)" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Currency" UniqueName="Currency">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlWidth="70%" ReadOnly="true" SortExpression="Status"
                                HeaderText="Status" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Status" UniqueName="Status">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </fieldset>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
