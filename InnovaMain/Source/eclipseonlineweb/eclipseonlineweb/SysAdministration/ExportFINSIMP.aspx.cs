﻿using System;
using System.IO;
using System.Web.UI;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CalculationInterface;

namespace eclipseonlineweb
{
    public partial class ExportFINSIMP : UMABasePage
    {
        protected override void Intialise()
        {
            base.Intialise();
          
        }

        public override void LoadPage()
        {
            this.Form.SubmitDisabledControls = true;
            base.LoadPage();
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected void Export(object sender, EventArgs e)
        {
            IBrokerManagedComponent ibmc = this.UMABroker.GetWellKnownBMC(Oritax.TaxSimp.CalculationInterface.WellKnownCM.Organization);
            CSVExportDS csvExportDS = new CSVExportDS();
            ibmc.GetData(csvExportDS);
            this.btnExport.Enabled = true;
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=FinSimpHolding.csv");
            Response.ContentType = "text/csv";
            Response.Write(csvExportDS.CSVString);
            Response.End();
            
        }
    }
}
