﻿using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;


namespace eclipseonlineweb.SysAdministration
{
    public partial class SecurityTransferReport : UMABasePage
    {
        public override void LoadPage()
        {
            GetData();
        }

        private void GetData()
        {
            var orgCm = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var transferInReportDs = new Oritax.TaxSimp.DataSets.TransferInReportDS();
            transferInReportDs.Unit = new Oritax.TaxSimp.Data.OrganizationUnit {CurrentUser = GetCurrentUser()};
            transferInReportDs.Command = (int) WebCommands.GetAllClientDetails;
            if (orgCm != null) orgCm.GetData(transferInReportDs);
            PresentationData = transferInReportDs;
        }
    }
}