﻿using System;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb.SysAdministration
{
    public partial class BuyASX : UMABasePage
    {
        protected override void Intialise()
        {
            BuyASXControl.SaveData += (clientCID, ds) =>
            {
                if (clientCID == Guid.Empty.ToString())
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(clientCID, ds);
                }
            };
        }

        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            return true;
        }

        public override void LoadPage()
        {
            if (!IsPostBack)
            {
                var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
                //Setting control required properties
                BuyASXControl.IsAdmin = objUser.Administrator;
                BuyASXControl.IsAdminMenu = true;
            }
        }
    }
}