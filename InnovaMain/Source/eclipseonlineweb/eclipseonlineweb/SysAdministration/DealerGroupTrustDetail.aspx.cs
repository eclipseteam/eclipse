﻿using System;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;

namespace eclipseonlineweb.SysAdministration
{
    public partial class DealerGroupTrustDetail : UMABasePage
    {
        protected override void Intialise()
        {
            base.Intialise();
            DealerGroupTrustControl1.SaveData += (Cid, ds) =>
            {
                if (Cid == Guid.Empty)
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(Cid.ToString(), ds);
                }
            };
            DealerGroupTrustControl1.Saved += (cID) =>
            {
                Response.Redirect(Request.Url.AbsoluteUri + "?ins=" + cID);
            };
            BankAccountMappingControl.SaveOrg += SaveOrganizanition;
            BankAccountMappingControl.SaveUnit += SaveData;

            Corporates.SaveOrg += SaveOrganizanition;
            Corporates.SaveUnit += SaveData;
          
            Corporates.Filter = string.Format("Type='{0}' or Type='{1}'",OrganizationType.ClientCorporationPrivate,OrganizationType.ClientCorporationPublic);
        }
        private void GetData()
        {
            OrganizationUnitCM clientData = UMABroker.GetBMCInstance(new Guid(cid)) as OrganizationUnitCM;
            if (clientData != null)
            {
                AddressDetailsDS ds = new AddressDetailsDS();
                clientData.GetData(ds);
                PresentationData = ds;
                UMABroker.ReleaseBrokerManagedComponent(clientData);
                AddressDetailControl.FillAddressControls(ds);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cid))
            {
                AddressDetailsDS ds = AddressDetailControl.SetData();
                SaveData(cid, ds);
                AddressDetailControl.FillAddressControls(ds);
            }
        }
        public override void LoadPage()
        {
            cid = string.IsNullOrEmpty(Request.Params["ins"]) ? string.Empty : Request.QueryString["ins"];
            if (!IsPostBack)
            {
                DealerGroupTrustControl1.SetEntity(
                    string.IsNullOrEmpty(Request.Params["ins"]) ? Guid.Empty : new Guid(Request.QueryString["ins"]),
                    Oritax.TaxSimp.CM.Group.DealerGroupEntityType.DealerGroupTrustControl);
                if (string.IsNullOrEmpty(cid))
                {
                    SetVisibility(false);
                }
                else
                {
                    SetVisibility(true);
                    GetData();
                }
            }
        }
        private void SetVisibility(bool visible)
        {
            ApCorporate.Visible =  ApBankAccounts.Visible = ApAddress.Visible =ApDocuments.Visible = ApSecurity.Visible == visible;
            RadTab tabCorporateHeader = RadTabStrip1.FindTabByValue("CorporateHeader");
            tabCorporateHeader.Visible = visible;
            RadTab tabBankAccountsHeader = RadTabStrip1.FindTabByValue("BankAccountsHeader");
            tabBankAccountsHeader.Visible = visible;
            RadTab tabAddressHeader = RadTabStrip1.FindTabByValue("AddressHeader");
            tabAddressHeader.Visible = visible;
            
            RadTab tabSecHeader = RadTabStrip1.FindTabByValue("SecurityHeader");
            tabSecHeader.Visible = visible;
            RadTab tabDocumentsHeader = RadTabStrip1.FindTabByValue("DocumentsHeader");
            tabDocumentsHeader.Visible = visible;
        }        
    }
}