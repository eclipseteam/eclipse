﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Enum;
using Telerik.Web.UI;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb.SysAdministration
{
    public partial class MembershipDetail : UMABasePage
    {
        private string _cid;

        public override bool AccessibleByUser()
        {
            var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
               return true;
        }

        protected override void Intialise()
        {
            base.Intialise(); 
            membershipControl.Canceled += () => OVER.Visible = MemershipPopup.Visible = false;
            membershipControl.ValidationFailed += () =>
            {
                OVER.Visible = MemershipPopup.Visible = true;
            };
            membershipControl.Save += ds =>
            {
                SaveMembership(ds);
                OVER.Visible = MemershipPopup.Visible = false;
                CheckForErrors(ds);
                gd_Membership.Rebind();
            };
        }

        private void CheckForErrors(MembershipDS ds)
        {
            lblErrorMessage.Text = "";
            foreach (DataRow dr in ds.membershipTable.Rows)
            {
                if (dr.HasErrors)
                {
                    lblErrorMessage.Text += dr.RowError + "</br>";
                }
            }
        }

        private void SaveMembership(MembershipDS ds)
        {
            SaveData(_cid, ds);
        }

        public override void LoadPage()
        {
            _cid = Request.Params["CID"];
            SetLabels();
        }

        private void SetLabels()
       {
            membershipControl.OrgType = (OrganizationType)Enum.Parse(typeof(OrganizationType), Request.Params["orgtype"]);
            switch (membershipControl.OrgType)
            {
                case OrganizationType.DealerGroup:
                    lblMemberShip.Text = "Membership Detail - " + Request.QueryString["Name"] + " (Dealer Group)";
                    lblIncluded.Text = "Included IFA(s)";
                    btnBack.PostBackUrl = "DealerGroup.aspx";
                    btnBack.Text = btnBack.ToolTip = "Back to Dealer Group";
                    hfOrgType.Value = OrganizationType.DealerGroup.ToString();
                    parentRow.Visible = false;
                    break;
                case OrganizationType.IFA:
                    lblMemberShip.Text = "Membership Detail - " + Request.QueryString["Name"] + " (IFA)";
                    lblIncluded.Text = "Included Advisor(s)";
                    btnBack.PostBackUrl = "IFA.aspx";
                    btnBack.Text = btnBack.ToolTip = "Back to IFA";
                    hfOrgType.Value = OrganizationType.IFA.ToString();
                    parentRow.Visible = true;
                    break;
                case OrganizationType.Adviser:
                    lblMemberShip.Text = "Membership Detail - " + Request.QueryString["Name"] + " (Adviser)";
                    lblIncluded.Text = "Included Client(s)";
                    btnBack.PostBackUrl = "Advisor.aspx";
                    btnBack.Text = btnBack.ToolTip = "Back to Adviser";
                    hfOrgType.Value = OrganizationType.Adviser.ToString();
                    parentRow.Visible = true;
                    break;
            }
        }

        protected void GdMembershipItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                var ds = new MembershipDS { CommandType = DatasetCommandTypes.Delete };
                var gDataItem = e.Item as GridDataItem;
                DataRow dr = ds.membershipTable.NewRow();
                dr[ds.membershipTable.CID] = new Guid(gDataItem["Cid"].Text);
                dr[ds.membershipTable.CLID] = new Guid(gDataItem["Clid"].Text);
                dr[ds.membershipTable.CSID] = new Guid(gDataItem["Csid"].Text);
                ds.membershipTable.Rows.Add(dr);
                SaveData(_cid, ds);
                lblErrorMessage.Text = "";
                gd_Membership.Rebind();
            }
        }

        protected void LnkbtnAddExistngMembersClick(object sender, EventArgs e)
        {
            lblErrorMessage.Text = "";
            membershipControl.ResetControls();
            ShowHideExisting(true);
          
        }

        private void ShowHideExisting(bool show)
        {
            OVER.Visible = MemershipPopup.Visible = show;
        }

        protected void gd_Membership_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        /// <summary>
        /// Get Membership data
        /// </summary>
        private void GetData()
        {
            var cID = new Guid(_cid);
            var ds = new MembershipDS
                         {
                             CommandType = DatasetCommandTypes.GetChildren,
                             Unit = new OrganizationUnit
                                        {
                                            CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                                        }
                         };


            var component = UMABroker.GetBMCInstance(cID);
            component.GetData(ds);
            UMABroker.ReleaseBrokerManagedComponent(component);
           
            DataView memberShipView = new DataView(ds.membershipTable);
            memberShipView.Sort = ds.membershipTable.NAME + " ASC";
            gd_Membership.DataSource = memberShipView.ToTable();

            SetParentInfo(ds);

        }

        private void SetParentInfo(MembershipDS ds)
        {
            if (ds.parentTable.Rows.Count > 0)
            {

                lblParentName.Text = ds.parentTable.Rows[0][ds.parentTable.NAME].ToString();
                lblParentID.Text = ds.parentTable.Rows[0][ds.parentTable.CLIENTID].ToString();
                lblParentType.Text = ds.parentTable.Rows[0][ds.parentTable.TYPE].ToString();
                
            }
            else {

                lblParentName.Text =lblParentID.Text =lblParentType.Text = "--";
               
            }
        }

        protected void gd_Membership_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = e.Item as GridDataItem;
            var clinetId = ((HyperLink)dataItem["ClientID"].Controls[0]);
            var cId = dataItem["Cid"].Text;

            var uri = string.Empty;
            var orgType = (OrganizationType)Enum.Parse(typeof(OrganizationType), hfOrgType.Value);
            switch (orgType)
            {
                case OrganizationType.DealerGroup:
                    var comp = UMABroker.GetBMCInstance(new Guid(cId));
                    if (comp != null)
                    {
                        string type = comp.GetDataStream((int) CmCommand.GetType, null);
                        UMABroker.ReleaseBrokerManagedComponent(comp);
                        var ifaType = (IFAEntityType) Enum.Parse(typeof (IFAEntityType), type);
                        switch (ifaType)
                        {
                            case IFAEntityType.IFACompanyControl:
                                uri = string.Format("IFACompanyDetail.aspx?ins={0}", cId);
                                break;
                            case IFAEntityType.IFAIndividualControl:
                                uri = string.Format("IFASoleTraderDetail.aspx?ins={0}", cId);
                                break;
                            case IFAEntityType.IFAPartnershipControl:
                                uri = string.Format("IFAPartnershipDetail.aspx?ins={0}", cId);
                                break;
                            case IFAEntityType.IFATrustControl:
                                uri = string.Format("IFATrustDetail.aspx?ins={0}", cId);
                                break;
                        }
                    }
                    break;
                case OrganizationType.Adviser:
                    uri = string.Format("../ClientViews/ClientMainView.aspx?ins={0}", cId);
                    break;
                case OrganizationType.IFA:
                    uri = string.Format("AdviserDetails.aspx?ins={0}", cId);
                    break;
            }

            clinetId.NavigateUrl = uri;
        }


       
        

    }
}