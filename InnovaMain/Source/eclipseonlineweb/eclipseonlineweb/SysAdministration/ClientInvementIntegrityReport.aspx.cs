﻿using System;
using System.Data;
using System.Web.UI;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Utilities;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;


namespace eclipseonlineweb.SysAdministration
{
    public partial class ClientInvementIntegrityReport : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        public override void LoadPage()
        {
            if (!IsPostBack)
            {
                InputStartDate.SelectedDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(DateTime.Now.AddYears(-1));
                InputEndDate.SelectedDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(DateTime.Now);

            }
        }


        private void GetData()
        {
            var orgCm = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var transferInReportDs = new HoldingSummaryReportDS {AddBanktransInInvestment = true, Unit = new Oritax.TaxSimp.Data.OrganizationUnit {CurrentUser = GetCurrentUser()}};
            var selectedDate = this.InputStartDate.DateInput.SelectedDate;
            if (selectedDate != null)
            {
                transferInReportDs.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(selectedDate.Value.Date); 

            }
            if (InputEndDate.DateInput.SelectedDate != null)
            {
                transferInReportDs.EndDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(InputEndDate.DateInput.SelectedDate.Value.Date); 
            }
           
            if (!chkAllClients.Checked)
            {
                transferInReportDs.Count = Convert.ToInt32(txtClientCount.Text);
            }

            transferInReportDs.Command = (int)WebCommands.GetAllClientDetails;
            if (orgCm != null)
            {

                orgCm.GetData(transferInReportDs);
                
                UMABroker.ReleaseBrokerManagedComponent(orgCm);
                this.PresentationData = transferInReportDs;
                var dt = transferInReportDs.GetCapitalTransTableToInvestmentIntegrityTable();
                var veiw = new DataView(dt);
                veiw.Sort = dt.TRANSACTIONDATE + " DESC," + dt.SERVICETYPE + " ASC";
                PresentationGrid.DataSource = veiw;

            }


        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (IsPostBack)
                GetData();
        }

        protected void GenerateReport(object sender, EventArgs e)
        {
            PresentationGrid.Rebind();
        }

        protected void BtnReportClick(object sender, EventArgs e)
        {
            GetData();
            var ds = PresentationData as HoldingSummaryReportDS;
            DataSet excelDataset = new DataSet();
            var dt = ds.GetCapitalTransTableToInvestmentIntegrityTable();
            
            excelDataset.Tables.Add(dt);
            var dt2 = ds.Tables[HoldingSummaryReportDS.CAPITALTRANTABLE].Copy();
            foreach (DataRow row in dt2.Rows)
            {
                foreach (DataColumn column in dt2.Columns)
                {
                    if(row[column]!=DBNull.Value)
                    {
                        if(column.DataType==typeof(decimal))
                        row[column] = Math.Round((decimal)row[column],2);
                        if (column.DataType == typeof(double))
                            row[column] = Math.Round((double)row[column], 2);
                    }
                }
            }
            excelDataset.Tables.Add(dt2);
            ExcelHelper.ToExcel(excelDataset, "IntegrityReport.xls", this.Page.Response);

        }
    }
}