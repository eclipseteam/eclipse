﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="FeeDetails.aspx.cs" Inherits="eclipseonlineweb.FeeDetails" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="4%">
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDetails" Text="Fee Details"></asp:Label>
                            <asp:HiddenField ID="hfSecID" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <table>
                <tr>
                    <td>
                        <b>Products Confguration </b>
                    </td>
                    <td>
                        <telerik:RadComboBox Width="200px" ID="C1ComboBoxProductConfigType" Filter="Contains"
                            runat="server" Skin="Metro">
                            <Items>
                                <telerik:RadComboBoxItem Text="All" Value="All" Selected="true" />
                                <telerik:RadComboBoxItem Text="By Exclusions" Value="By Exclusions" />
                                <telerik:RadComboBoxItem Text="By Inclusions" Value="By Inclusions" />
                            </Items>
                        </telerik:RadComboBox>
                    </td>
                    <td>
                        <telerik:RadButton ID="btnSet" runat="server" Height="20px" Width="50px" Text="SET"
                            OnClick="btnSet_Click">
                        </telerik:RadButton>
                    </td>
                </tr>
            </table>
            <asp:Panel runat="server" ID="pnlSelectProducts">
                <h3>
                    Products Configuration ( Exclusions OR Inclusions)</h3>
                <table>
                    <tr>
                        <td>
                            <b>Select Products</b>
                        </td>
                        <td>
                            <telerik:RadComboBox CheckBoxes="true" EnableCheckAllItemsCheckBox="true" Width="400px"
                                ID="C1ComboBoxProducts" Filter="Contains" runat="server" Skin="Metro">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <telerik:RadButton ID="btnAddProducts" runat="server" Height="20px" Width="50px"
                                Text="ADD" OnClick="btnAddProductst_Click">
                            </telerik:RadButton>
                        </td>
                    </tr>
                </table>
                <br />
                <telerik:RadGrid OnNeedDataSource="GridProducts_NeedDataSource" ID="GridProducts"
                    runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                    AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="GridProducts_DetailTableDataBind"
                    OnItemCommand="GridProducts_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                    AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                    OnItemUpdated="GridProducts_ItemUpdated" OnItemDeleted="GridProducts_ItemDeleted"
                    OnItemInserted="GridProducts_ItemInserted" OnInsertCommand="GridProducts_InsertCommand"
                    EnableViewState="true" ShowFooter="true" OnItemCreated="GridProducts_ItemCreated"
                    OnItemDataBound="GridProducts_ItemDataBound">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="PROID" AllowMultiColumnSorting="True" Width="100%"
                        CommandItemDisplay="Top" Name="Products" TableLayout="Fixed" EditMode="EditForms">
                        <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                        <Columns>
                            <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="PROID" UniqueName="PROID" />
                            <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="PRONAME" HeaderStyle-Width="20%"
                                ItemStyle-Width="20%" HeaderText="Description" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="PRONAME" UniqueName="PRONAME">
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn ConfirmText="Delete these details record?" ButtonType="ImageButton"
                                CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                    <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                        <Excel Format="Html"></Excel>
                    </ExportSettings>
                </telerik:RadGrid>
                <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorModelServiceType"
                    runat="server" DropDownStyle-Width="400px" />
            </asp:Panel>
            <br />
            <br />
            <h3>
                Ongoing Fee ($)</h3>
            <telerik:RadGrid OnNeedDataSource="GridOngoingFeeValue_NeedDataSource" ID="GridOngoingFeeValue"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="GridOngoingFeeValue_DetailTableDataBind"
                OnItemCommand="GridOngoingFeeValue_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="GridOngoingFeeValue_ItemUpdated" OnItemDeleted="GridOngoingFeeValue_ItemDeleted"
                OnItemInserted="GridOngoingFeeValue_ItemInserted" OnInsertCommand="GridOngoingFeeValue_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="GridOngoingFeeValue_ItemCreated"
                OnItemDataBound="GridOngoingFeeValue_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="FeesOngoingValue" TableLayout="Fixed" EditMode="EditForms">
                    <CommandItemSettings AddNewRecordText="Add New Ongoing Fee ($)" ShowExportToExcelButton="true"
                        ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridEditCommandColumn>
                        <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ID" UniqueName="ID" />
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Description"
                            HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderText="Description" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Description" UniqueName="Description">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                            SortExpression="FeeValue" HeaderText="Fee Value" AutoPostBackOnFilter="true"
                            DataFormatString="{0:c}" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="FeeValue" UniqueName="FeeValue">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ConfirmText="Delete these details record?" ButtonType="ImageButton"
                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="Html"></Excel>
                </ExportSettings>
            </telerik:RadGrid>
            <br />
            <h3>
                Ongoing Fee (%)</h3>
            <telerik:RadGrid OnNeedDataSource="GridOngoingFeePercent_NeedDataSource" ID="GridOngoingFeePercent"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="GridOngoingFeePercent_DetailTableDataBind"
                OnItemCommand="GridOngoingFeePercent_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="GridOngoingFeePercent_ItemUpdated" OnItemDeleted="GridOngoingFeePercent_ItemDeleted"
                OnItemInserted="GridOngoingFeePercent_ItemInserted" OnInsertCommand="GridOngoingFeePercent_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="GridOngoingFeePercent_ItemCreated"
                OnItemDataBound="GridOngoingFeePercent_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="FeesOngoingPercent" TableLayout="Fixed" EditMode="EditForms">
                    <CommandItemSettings AddNewRecordText="Add New Ongoing Fee (%)" ShowExportToExcelButton="true"
                        ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridEditCommandColumn>
                        <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ID" UniqueName="ID" />
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Description"
                            HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderText="Description" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Description" UniqueName="Description">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                            SortExpression="PercentageFee" HeaderText="Fee %" AutoPostBackOnFilter="true"
                            DataFormatString="{0:n4}" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="PercentageFee" UniqueName="PercentageFee">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ConfirmText="Delete these details record?" ButtonType="ImageButton"
                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="Html"></Excel>
                </ExportSettings>
            </telerik:RadGrid>
            <br />
            <h3>
                Tiered Fee</h3>
            <telerik:RadGrid OnNeedDataSource="TieredFee_NeedDataSource" ID="TieredFee" runat="server"
                ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20" AllowSorting="True"
                AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="TieredFee_DetailTableDataBind"
                OnItemCommand="TieredFee_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="TieredFee_ItemUpdated" OnItemDeleted="TieredFee_ItemDeleted" OnItemInserted="TieredFee_ItemInserted"
                OnInsertCommand="TieredFee_InsertCommand" EnableViewState="true" ShowFooter="true"
                OnItemCreated="TieredFee_ItemCreated" OnItemDataBound="TieredFee_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="TieredFees" TableLayout="Fixed" EditMode="EditForms">
                    <CommandItemSettings AddNewRecordText="Add New Tiered Fee" ShowExportToExcelButton="true"
                        ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridEditCommandColumn>
                        <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ID" UniqueName="ID" />
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Description"
                            HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderText="Description" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Description" UniqueName="Description">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                            SortExpression="FromValue" HeaderText="From ($)" AutoPostBackOnFilter="true"
                            DataFormatString="{0:c}" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="FromValue" UniqueName="FromValue">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                            SortExpression="ToValue" HeaderText="To ($)" AutoPostBackOnFilter="true" DataFormatString="{0:c}"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="ToValue" UniqueName="ToValue">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                            SortExpression="PercentageFee" HeaderText="Fee %" AutoPostBackOnFilter="true"
                            DataFormatString="{0:n4}" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="PercentageFee" UniqueName="PercentageFee">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ConfirmText="Delete these details record?" ButtonType="ImageButton"
                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="Html"></Excel>
                </ExportSettings>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
