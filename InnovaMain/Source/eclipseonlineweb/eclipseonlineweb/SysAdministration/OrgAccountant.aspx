﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="OrgAccountant.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.OrgAccountant" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .holder
        {
            width: 100%;
            display: inline;
            z-index: 6;
        }
        .content
        {
            background: #fff;
            z-index: 7; /*  padding: 28px 26px 33px 25px;*/
        }
        .popup
        {
            border-radius: 7px;
            background: white;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 175px;
            top: 20%;
            left: 50%;
            margin-left: -400px;
            margin-top: -40px;
            z-index: 6;
        }
        
        .overlay
        {
            width: 100%;
            opacity: 0.65;
            height: 100%;
            left: 0; /*IE*/
            top: 0;
            text-align: center;
            z-index: 5;
            position: fixed;
            background-color: #444444;
        }
    </style>
    <fieldset>
        <table width="100%">
            <tr>
                <td style="width: 30%">
                    <telerik:RadButton ID="btn" ToolTip="Add New Accountant" runat="server" OnClick="lnkbtnAddAccountant_Click">
                        <ContentTemplate>
                            <img src="../images/Add-button-.png" alt="" class="btnImageWithText" />
                            <span class="riLabel">New Accountant</span>
                        </ContentTemplate>
                    </telerik:RadButton>
                </td>
                <td width="70%" class="breadcrumbgap">
                    <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                    <br />
                    <asp:Label Font-Bold="true" runat="server" ID="lblAccountant" Text="Accountant"></asp:Label>
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <telerik:RadGrid ID="gd_Accountant" runat="server" AutoGenerateColumns="True" PageSize="20"
        AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true" GridLines="Horizontal"
        AllowAutomaticInserts="true" OnInsertCommand="gd_Accountant_InsertCommand" AllowFilteringByColumn="true"
        EnableViewState="true" ShowFooter="True" OnNeedDataSource="gd_Accountant_OnNeedDataSource"
        OnItemCommand="gd_Accountant_ItemCommand" AllowAutomaticUpdates="True">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="Top" EditMode="EditForms">
            <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false"
                ShowExportToWordButton="false" ShowExportToPdfButton="false"></CommandItemSettings>
            <Columns>
                <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="Cid" ReadOnly="true"
                    HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderText="CID" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="Cid" UniqueName="Cid" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="Clid" ReadOnly="true"
                    HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderText="CLID" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="Clid" UniqueName="Clid" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="Csid" ReadOnly="true"
                    HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderText="CSID" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="Csid" UniqueName="Csid" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="ClientID" ReadOnly="true"
                    HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderText="Client ID" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="ClientID" UniqueName="ClientID">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="400px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                    SortExpression="TradingName" HeaderText="Trading Name" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="TradingName" UniqueName="TradingName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="120px" SortExpression="AccountantType"
                    HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderText="AccountantType" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="AccountantType" UniqueName="AccountantType" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="200px" HeaderStyle-Width="8%" ReadOnly="true"
                    ItemStyle-Width="8%" SortExpression="Type" HeaderText="Type" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="false" DataField="Type" UniqueName="Type"
                    Visible="False">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="100px" HeaderStyle-Width="5%" ReadOnly="true"
                    ItemStyle-Width="5%" SortExpression="Status" HeaderText="Status" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="Status" UniqueName="Status">
                </telerik:GridBoundColumn>
                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                    UniqueName="EditColumn">
                    <HeaderStyle Width="4%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn Visible="True" ConfirmText="Are you sure you want to delete this Accountant?"
                    ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                    <HeaderStyle Width="4%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                </telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <div id="OrganizationTypePopup" runat="server" visible="false" class="holder">
        <div class="popup">
            <table>
                <tr>
                    <td style="width: 100%;">
                        <table style="background-color: darkblue;">
                            <tr>
                                <td style="text-align: left; color: White; width: 100px">
                                    Select Accountant Type
                                </td>
                                <td style="text-align: right; width: 50px;">
                                    <telerik:RadButton runat="server" ID="ImageButton1" Width="16px" Height="15px" BorderStyle="None"
                                        Text="Close" OnClick="btnCancel_OnClick">
                                        <Image ImageUrl="~/images/cross_icon_normal.png"></Image>
                                    </telerik:RadButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadButton ButtonType="ToggleButton" ToggleType="Radio" runat="server" ID="rdoSoleTrader"
                            GroupName="Accountant" Text="Accountant - Sole Trader" Checked="true" AutoPostBack="False" />
                        <br />
                        <telerik:RadButton ButtonType="ToggleButton" ToggleType="Radio" runat="server" ID="rdoCompany"
                            GroupName="Accountant" Text="Accountant - Company" AutoPostBack="False" />
                        <br />
                        <telerik:RadButton ButtonType="ToggleButton" ToggleType="Radio" runat="server" ID="rdoPartnership"
                            GroupName="Accountant" Text="Accountant - Partnership" AutoPostBack="False" />
                        <br />
                        <telerik:RadButton ButtonType="ToggleButton" ToggleType="Radio" runat="server" ID="rdoTrust"
                            GroupName="Accountant" Text="Accountant - Trust" AutoPostBack="False" />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        <telerik:RadButton runat="server" Text="Next" ID="btnNext" OnClick="btnNext_OnClick" />
                        <telerik:RadButton runat="server" Text="Cancel" ID="btnCancel" OnClick="btnCancel_OnClick" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="overlay" id="OVER" visible="False" runat="server">
    </div>
</asp:Content>
