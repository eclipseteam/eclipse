﻿using System;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using Oritax.TaxSimp.CM.OrganizationUnit;

namespace eclipseonlineweb.SysAdministration
{
    public partial class DealerGroupDetail : UMABasePage
    {
        protected override void Intialise()
        {
            base.Intialise();
            DealerIndivisualControl.SaveData += (cid, ds) =>
            {
                if (cid == Guid.Empty)
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(cid.ToString(), ds);
                }
            };
            DealerIndivisualControl.Saved += (cID) => Response.Redirect(Request.Url.AbsoluteUri + "?ins=" + cID);
            BankAccountMappingControl.SaveOrg += SaveOrganizanition;
            BankAccountMappingControl.SaveUnit += SaveData;
            ContactMappingControl1.SaveOrg += SaveOrganizanition;
            ContactMappingControl1.SaveUnit += SaveData;
        }

      
        private void GetData()
        {
            var clientData = UMABroker.GetBMCInstance(new Guid(cid)) as OrganizationUnitCM;
            if (clientData != null)
            {
                var ds = new AddressDetailsDS();
                clientData.GetData(ds);
                PresentationData = ds;
                UMABroker.ReleaseBrokerManagedComponent(clientData);
                AddressDetailControl.FillAddressControls(ds);
            }
        }
        protected void BtnSaveClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cid))
            {
                var ds = AddressDetailControl.SetData();
                SaveData(cid, ds);
                AddressDetailControl.FillAddressControls(ds);
            }
        }
        public override void LoadPage()
        {
            cid = string.IsNullOrEmpty(Request.Params["ins"]) ? string.Empty : Request.QueryString["ins"];
            if (!IsPostBack)
            {
                DealerIndivisualControl.SetEntity(
                    string.IsNullOrEmpty(Request.Params["ins"]) ? Guid.Empty : new Guid(Request.QueryString["ins"]),
                    Oritax.TaxSimp.CM.Group.DealerGroupEntityType.DealerGroupIndividualControl);
                if (string.IsNullOrEmpty(cid))
                {
                    SetVisibility(false);
                }
                else
                {
                    SetVisibility(true);
                    GetData();
                }

            }
        }
        private void SetVisibility(bool visible)
        {
            ApSol.Visible = ApBankAccounts.Visible = ApAddress.Visible = ApDocuments.Visible = ApSecurity.Visible == visible;
            RadTab tabSoleTraderHeader = RadTabStrip1.FindTabByValue("SoleTraderHeader");
            tabSoleTraderHeader.Visible = visible;
            RadTab tabBankAccounHeader = RadTabStrip1.FindTabByValue("BankAccounHeader");
            tabBankAccounHeader.Visible = visible;
            RadTab tabAddressHeader = RadTabStrip1.FindTabByValue("AddressHeader");
            tabAddressHeader.Visible = visible;
            
            RadTab tabSecHeader = RadTabStrip1.FindTabByValue("SecurityHeader");
            tabSecHeader.Visible = visible;
            RadTab tabDocumentsHeader = RadTabStrip1.FindTabByValue("DocumentsHeader");
            tabDocumentsHeader.Visible = visible;
        }
    }
}