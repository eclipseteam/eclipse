﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="SecurityTransferInReport.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.SecurityTransferInReport" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <div style="text-align: right; float: right">
            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
        </div>
    </fieldset>
    <fieldset>
        <legend>Security Transfer In Report</legend>
        <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
            Width="100%" PageSize="20" AllowSorting="True" AllowMultiRowSelection="False"
            AllowPaging="True" GridLines="None" AllowFilteringByColumn="true" EnableViewState="true"
            ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowFilteringByColumn="True" TableLayout="Fixed" Width="100%">
                <Columns>
                    <telerik:GridBoundColumn SortExpression="SecurityCode" ReadOnly="true" HeaderText="Security Code"
                        HeaderButtonType="TextButton" DataField="SecurityCode" UniqueName="SecurityCode">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="SecurityName" ReadOnly="true" HeaderText="Security Name"
                        HeaderButtonType="TextButton" DataField="SecurityName" UniqueName="SecurityName">
                    </telerik:GridBoundColumn>
                    <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="ClientCID"
                        SortExpression="ClientCode" UniqueName="ClientCode" DataNavigateUrlFormatString="../ClientViews/ClientMainView.aspx?ins={0}"
                        HeaderText="Client ID" DataTextField="ClientCode" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" FilterControlWidth="50px" HeaderStyle-Width="85px"
                        HeaderButtonType="TextButton" ShowFilterIcon="true">
                    </telerik:GridHyperLinkColumn>
                    <telerik:GridDateTimeColumn Visible="true" SortExpression="EarliestTransferInDate"
                        UniqueName="EarliestTransferInDate" PickerType="DatePicker" HeaderText="Earliest Transfer-In Date"
                        HeaderStyle-BackColor="LightGray" DataField="EarliestTransferInDate" DataFormatString="{0:dd/MM/yyyy}">
                        <ItemStyle Width="85px" />
                    </telerik:GridDateTimeColumn>
                    <telerik:GridDateTimeColumn Visible="true" SortExpression="EarliestPriceDate" UniqueName="EarliestPriceDate"
                        PickerType="DatePicker" HeaderText="Earliest Price Date" HeaderStyle-BackColor="LightGray"
                        DataField="EarliestPriceDate" DataFormatString="{0:dd/MM/yyyy}">
                        <ItemStyle Width="85px" />
                    </telerik:GridDateTimeColumn>
                    <telerik:GridBoundColumn SortExpression="PriceAtEarliestPriceDate" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-HorizontalAlign="Center" HeaderText="Price at Earliest Price Date"
                        HeaderButtonType="TextButton" DataField="PriceAtEarliestPriceDate" UniqueName="PriceAtEarliestPriceDate">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="ERROR" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-HorizontalAlign="Center" HeaderText="Has Errors" HeaderButtonType="TextButton"
                        DataField="ERROR" UniqueName="ERROR">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </fieldset>
</asp:Content>
