﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;

namespace eclipseonlineweb
{
    public partial class BusinessTitle : UMABasePage
    {
        private ICMBroker umaBroker
        {
            get
            {
                return (Page as UMABasePage).UMABroker;
            }
        }

        protected override void GetBMCData()
        {
            IOrganization organization =
                this.umaBroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            BusinessTitleDS ds = new BusinessTitleDS();
            organization.GetData(ds);
            this.PresentationData = ds;

            if (ds.BTitleTable.Rows.Count == 0)
                btnDefaultBTitles.Visible = true;

            umaBroker.ReleaseBrokerManagedComponent(organization);
        }

        public override void LoadPage()
        {
            base.LoadPage();
            DBUser objUser =
                (DBUser)umaBroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
        }

        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                String _title = gDataItem["txtTitle"].Text;

                BusinessTitleDS DS = new BusinessTitleDS();
                DS.CommandType = DatasetCommandTypes.Delete;
                DataTable dt = DS.BTitleTable;
                DataRow dr = dt.NewRow();
                dr[DS.BTitleTable.TITLE] = _title;
                dt.Rows.Add(dr);

                base.SaveOrganizanition(DS);
                GetBMCData();
                this.PresentationGrid.Rebind();
            }
        }

        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            umaBroker.SaveOverride = true;
            BusinessTitleDS DS = new BusinessTitleDS();
            DS.CommandType = DatasetCommandTypes.Add;
            DataRow row = DS.Tables[DS.BTitleTable.NAME].NewRow();
            row[DS.BTitleTable.TITLE] = ((TextBox)(((GridEditableItem)(e.Item))["txtTitle"].Controls[0])).Text;
            DS.Tables[DS.BTitleTable.NAME].Rows.Add(row);

            SaveOrganizanition(DS);
            e.Canceled = true;
            e.Item.Edit = false;

            GetBMCData();
            this.PresentationGrid.Rebind();
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetBMCData();
            BusinessTitleDS DS = new BusinessTitleDS();
            DataView View = new DataView(this.PresentationData.Tables[DS.BTitleTable.NAME]);
            View.Sort = DS.BTitleTable.TITLE + " ASC";
            this.PresentationGrid.DataSource = View.ToTable();
        }

        protected void btnDefaultBTitles_Click(object sender, EventArgs e)
        {
            BusinessTitleDS DS = new BusinessTitleDS();
            DS.CommandType = DatasetCommandTypes.Add;

            string[] arr = new string[5];
            arr[0] = "Director";
            arr[1] = "Trustee";
            arr[2] = "Company Secetory";
            arr[3] = "Legal Attorney";
            arr[4] = "Others";

            foreach (string bt in arr)
            {
                DataRow row = DS.Tables[DS.BTitleTable.NAME].NewRow();
                row[DS.BTitleTable.TITLE] = bt;
                DS.Tables[DS.BTitleTable.NAME].Rows.Add(row);
            }
            
            SaveOrganizanition(DS);
            this.PresentationGrid.Rebind();
            btnDefaultBTitles.Visible = false;
        }
    }
}