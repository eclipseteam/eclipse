﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.CM.Organization.Data;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using C1.Web.Wijmo.Controls.C1Chart;
using System.Web;
using System.IO;
using System.Collections.Generic;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp;
using Oritax.TaxSimp.Data;

namespace eclipseonlineweb
{
    public partial class AccountsFUMCompliance : UMABasePage
    {
        private DataView _entityView;
        
        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
            var excelDataset = new DataSet();
            PopulateData();
            _entityView = new DataView(this.PresentationData.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE]) { Sort = SecuritySummaryReportDS.CLIENTNAME + " ASC" };
            DataTable entityTable = _entityView.ToTable();

            excelDataset.Merge(entityTable, true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, "HOLDINGvsFINSIMP HOLDING-[" + DateTime.Today.ToString("dd-MMM-yyyy")+"].xls", Page.Response);
        }
        
        protected void Filter(object sender, C1GridViewFilterEventArgs e)
        {
            e.Values[0] = ((string)e.Values[0]).Trim();
            _entityView = new DataView(this.PresentationData.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE]) { Sort = SecuritySummaryReportDS.CLIENTNAME + " ASC" };
        }

        protected override void GetBMCData()
        {
        }

        public void BtnUploadClick(object sender, EventArgs e)
        {
            // Check to see if file was uploaded
            if (filMyFile.PostedFile != null)
            {
                // Get a reference to PostedFile object
                HttpPostedFile myFile = filMyFile.PostedFile;

                // Get size of uploaded file
                int nFileLen = myFile.ContentLength;

                // make sure the size of the file is > 0
                if (nFileLen > 0)
                {
                    if (myFile.FileName.Contains(".csv"))
                        ProcessCSVUpload(myFile, nFileLen);
                    else if (myFile.FileName.Contains("xls"))
                        ProcessExcelUpload(myFile, nFileLen);
                }
            }
        }



        private void ProcessCSVUpload(HttpPostedFile myFile, int nFileLen)
        {
            // Allocate a buffer for reading of the file
            string fileName = Path.GetFileName(myFile.FileName);
            var date = DateTime.Now;
            var distributionDictory = Server.MapPath("~/App_Data/Import/");
            if (!Directory.Exists(distributionDictory))
            {
                Directory.CreateDirectory(distributionDictory);
            }

            string fileLocation = distributionDictory + date.ToString("ddMMyyhhmmsstt") + fileName;
            byte[] myData = new byte[nFileLen];

            // Read uploaded file from the Stream
            myFile.InputStream.Read(myData, 0, nFileLen);

            // Write data into a file
            Utilities.WriteToFile(fileLocation, ref myData);

            var reader = ReadAsLines(fileLocation);

            var finSimpData = new DataTable();

            //this assume the first record is filled with the column names
            var headers = reader.First().Split(',');
            foreach (var header in headers)
            {
                finSimpData.Columns.Add(header);
            }

            var records = reader.Skip(1);
            foreach (var record in records)
            {
                finSimpData.Rows.Add(record.Split(','));
            }

            PopulateData();

            foreach (DataRow row in this.PresentationData.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE].Rows)
            {
                DataRow[] selectedFinRows = finSimpData.Select("wrap_platform_client_id ='" + row[SecuritySummaryReportDS.CLIENTID].ToString() + "' AND investment_code ='" + row[SecuritySummaryReportDS.SECNAME].ToString().ToUpper() + "'");
                if (selectedFinRows.Count() > 0)
                {
                    DataRow selectedFinRow = selectedFinRows[0];

                    if (selectedFinRow != null)
                    {
                        if (row[SecuritySummaryReportDS.SECNAME].ToString().ToUpper() == "CASH")
                        {
                            decimal eclipseHlding = Convert.ToDecimal(row[SecuritySummaryReportDS.TOTAL].ToString());
                            decimal finHolding = Convert.ToDecimal(selectedFinRow["units"].ToString());

                            row[SecuritySummaryReportDS.TOTALFINSIMPUNITS] = Convert.ToDecimal(selectedFinRow["units"].ToString());
                            row[SecuritySummaryReportDS.TOTALFINSIMPCASH] = eclipseHlding - finHolding;
                        }
                        else
                        {
                            decimal eclipseHlding = Convert.ToDecimal(row[SecuritySummaryReportDS.TOTALUNITS].ToString());
                            decimal finHolding = Convert.ToDecimal(selectedFinRow["units"].ToString());

                            row[SecuritySummaryReportDS.TOTALFINSIMPUNITS] = Convert.ToDecimal(selectedFinRow["units"].ToString());
                            row[SecuritySummaryReportDS.TOTALFINSIMPCASH] = eclipseHlding - finHolding;
                        }
                    }
                }
                else
                {
                    if (row[SecuritySummaryReportDS.SECNAME].ToString().ToUpper() == "CASH")
                    {
                        decimal eclipseHlding = Convert.ToDecimal(row[SecuritySummaryReportDS.TOTAL].ToString());
                        decimal finHolding = 0;

                        row[SecuritySummaryReportDS.TOTALFINSIMPUNITS] = finHolding;
                        row[SecuritySummaryReportDS.TOTALFINSIMPCASH] = eclipseHlding - finHolding;
                    }
                    else
                    {
                        decimal eclipseHlding = Convert.ToDecimal(row[SecuritySummaryReportDS.TOTALUNITS].ToString());
                        decimal finHolding = 0;

                        row[SecuritySummaryReportDS.TOTALFINSIMPUNITS] = finHolding;
                        row[SecuritySummaryReportDS.TOTALFINSIMPCASH] = eclipseHlding - finHolding;
                    }

                }
            }

            foreach (DataRow row in finSimpData.Rows)
            {
                DataRow[] selectedeClipseRows = this.PresentationData.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE].Select(SecuritySummaryReportDS.CLIENTID + "= '" + row["wrap_platform_client_id"].ToString() + "' AND " + SecuritySummaryReportDS.SECNAME + " ='" + row["investment_code"].ToString().ToUpper() + "'");

                if (selectedeClipseRows.Count() > 0)
                {
                    DataRow selectedeClipseRow = selectedeClipseRows[0];
                    var selectedClientRow = this.PresentationData.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE].Select().Where(eclipseRow => row["wrap_platform_client_id"].ToString() == eclipseRow[SecuritySummaryReportDS.CLIENTID].ToString()).FirstOrDefault();

                    if (selectedeClipseRow == null)
                    {
                        DataRow missingRow = this.PresentationData.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE].NewRow();
                        if (selectedClientRow != null)
                        {
                            missingRow[SecuritySummaryReportDS.CLIENTCID] = selectedClientRow[SecuritySummaryReportDS.CLIENTCID];
                            missingRow[SecuritySummaryReportDS.CLIENTID] = selectedClientRow[SecuritySummaryReportDS.CLIENTID];
                            missingRow[SecuritySummaryReportDS.CLIENTNAME] = selectedClientRow[SecuritySummaryReportDS.CLIENTNAME];
                        }
                        else
                        {
                            missingRow[SecuritySummaryReportDS.CLIENTCID] = Guid.Empty;
                            missingRow[SecuritySummaryReportDS.CLIENTID] = row["wrap_platform_client_id"];
                            missingRow[SecuritySummaryReportDS.CLIENTNAME] = row["investor_name"];
                        }
                        missingRow[SecuritySummaryReportDS.SECNAME] = row["investment_code"]; ;
                        missingRow[SecuritySummaryReportDS.DESCRIPTION] = row["investment_name"]; ;
                        missingRow[SecuritySummaryReportDS.TOTAL] = 0;
                        decimal finHolding = Convert.ToDecimal(row["units"].ToString());
                        missingRow[SecuritySummaryReportDS.TOTALFINSIMPCASH] = -finHolding;
                        missingRow[SecuritySummaryReportDS.TOTALFINSIMPUNITS] = finHolding;
                        missingRow[SecuritySummaryReportDS.PRICEATDATE] = DateTime.Now;
                        this.PresentationData.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE].Rows.Add(missingRow);
                    }
                }
            }

            var excelDataset = new DataSet();
            _entityView = new DataView(this.PresentationData.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE]) { Sort = SecuritySummaryReportDS.CLIENTNAME + " ASC" };
            DataTable entityTable = _entityView.ToTable();
            entityTable.Columns.Remove(SecuritySummaryReportDS.CLIENTCID);
            excelDataset.Merge(entityTable, true, MissingSchemaAction.Add);
            ExcelHelper.ToOpenExcel(excelDataset, "HOLDINGvsFINSIMP HOLDING-[" + DateTime.Today.ToString("dd-MMM-yyyy") + "].xlsx", Page.Response);
        }

        private void ProcessExcelUpload(HttpPostedFile myFile, int nFileLen)
        {
            // Allocate a buffer for reading of the file
            string fileName = Path.GetFileName(myFile.FileName);
            var date = DateTime.Now;
            var distributionDictory = Server.MapPath("~/App_Data/Import/");
            if (!Directory.Exists(distributionDictory))
            {
                Directory.CreateDirectory(distributionDictory);
            }

            string fileLocation = distributionDictory + date.ToString("ddMMyyhhmmsstt") + fileName;
            byte[] myData = new byte[nFileLen];

            // Read uploaded file from the Stream
            myFile.InputStream.Read(myData, 0, nFileLen);

            // Write data into a file
            Utilities.WriteToFile(fileLocation, ref myData);

            SpreadsheetGear.IWorkbook workbook = SpreadsheetGear.Factory.GetWorkbook(fileLocation);

            DataSet finSimpDataDS = workbook.GetDataSet(SpreadsheetGear.Data.GetDataFlags.None);
            DataTable finSimpData = finSimpDataDS.Tables["Holdings"];
            PopulateData();

            foreach (DataRow row in this.PresentationData.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE].Rows)
            {
                if (row[SecuritySummaryReportDS.SECTYPE].ToString() != Enumeration.GetDescription(SecurityType.TD))
                {
                    DataRow[] selectedFinRows = finSimpData.Select("client_id ='" + row[SecuritySummaryReportDS.CLIENTID].ToString() + "' AND investment_code ='" + row[SecuritySummaryReportDS.SECNAME].ToString().ToUpper() + "'");
                    if (selectedFinRows.Count() > 0)
                    {
                        DataRow selectedFinRow = selectedFinRows[0];

                        if (selectedFinRow != null)
                        {
                            decimal finsimpRebalanceValue = Convert.ToDecimal(selectedFinRow["rebalance_units"].ToString());

                            if (row[SecuritySummaryReportDS.SECNAME].ToString().ToUpper() == "CASH")
                            {
                                decimal eclipseHlding = Convert.ToDecimal(row[SecuritySummaryReportDS.TOTAL].ToString());
                                decimal finHolding = Convert.ToDecimal(selectedFinRow["current_value"].ToString());

                                row[SecuritySummaryReportDS.TOTALFINSIMPUNITS] = 0;
                                row[SecuritySummaryReportDS.FINSIMPUNITPRICE] = 0;
                                row[SecuritySummaryReportDS.TOTALFINSIMPCASH] = finHolding;
                                row[SecuritySummaryReportDS.TOTALUNITSDIFF] = 0;
                                row[SecuritySummaryReportDS.TOTALCASHDIFF] = eclipseHlding - finHolding;
                            }
                            else
                            {
                                decimal eclipseHldingUnits = Convert.ToDecimal(row[SecuritySummaryReportDS.TOTALUNITS].ToString());
                                decimal eclipseHldingPrice = Convert.ToDecimal(row[SecuritySummaryReportDS.UNITPRICE].ToString());
                                decimal eclipseHldingValue = Convert.ToDecimal(row[SecuritySummaryReportDS.TOTAL].ToString());
                                decimal finUnitsHolding = Convert.ToDecimal(selectedFinRow["units"].ToString());
                                decimal finValueHolding = Convert.ToDecimal(selectedFinRow["current_value"].ToString());
                                decimal finUnitPriceHolding = Convert.ToDecimal(selectedFinRow["unit_price"].ToString());

                                row[SecuritySummaryReportDS.TOTALFINSIMPUNITS] = finUnitsHolding;
                                row[SecuritySummaryReportDS.FINSIMPUNITPRICE] = finUnitPriceHolding;
                                row[SecuritySummaryReportDS.TOTALFINSIMPCASH] = finValueHolding;
                                row[SecuritySummaryReportDS.TOTALUNITSDIFF] = eclipseHldingUnits - finUnitsHolding;
                                row[SecuritySummaryReportDS.TOTALCASHDIFF] = eclipseHldingValue - finValueHolding;
                            }

                            row[SecuritySummaryReportDS.FINSIMPREBALANCEDVALUE] = finsimpRebalanceValue;
                        }
                    }
                    else
                    {
                        if (row[SecuritySummaryReportDS.SECNAME].ToString().ToUpper() == "CASH")
                        {
                            decimal eclipseHlding = Convert.ToDecimal(row[SecuritySummaryReportDS.TOTAL].ToString());
                            decimal finHolding = 0;

                            row[SecuritySummaryReportDS.TOTALFINSIMPUNITS] = 0;
                            row[SecuritySummaryReportDS.FINSIMPREBALANCEDVALUE] = 0;
                            row[SecuritySummaryReportDS.FINSIMPUNITPRICE] = 0;
                            row[SecuritySummaryReportDS.TOTALFINSIMPCASH] = 0;
                            row[SecuritySummaryReportDS.TOTALUNITSDIFF] = 0;
                            row[SecuritySummaryReportDS.TOTALCASHDIFF] = eclipseHlding - finHolding;
                        }
                        else
                        {
                            decimal eclipseHldingUnits = Convert.ToDecimal(row[SecuritySummaryReportDS.TOTALUNITS].ToString());
                            decimal eclipseHldingValue = Convert.ToDecimal(row[SecuritySummaryReportDS.TOTAL].ToString());
                            decimal finHoldingUnits = 0;
                            decimal finHoldingValue = 0;

                            row[SecuritySummaryReportDS.TOTALFINSIMPUNITS] = 0;
                            row[SecuritySummaryReportDS.FINSIMPUNITPRICE] = 0;
                            row[SecuritySummaryReportDS.TOTALFINSIMPCASH] = 0;
                            row[SecuritySummaryReportDS.FINSIMPREBALANCEDVALUE] = 0;
                            row[SecuritySummaryReportDS.TOTALUNITSDIFF] = eclipseHldingUnits - finHoldingUnits;
                            row[SecuritySummaryReportDS.TOTALCASHDIFF] = eclipseHldingValue - finHoldingValue;
                        }
                    }
                }
            }

            foreach (DataRow row in finSimpData.Rows)
            {
                DataRow[] selectedeClipseRows = this.PresentationData.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE].Select(SecuritySummaryReportDS.CLIENTID + "= '" + row["client_id"].ToString() + "' AND " + SecuritySummaryReportDS.SECNAME + " ='" + row["investment_code"].ToString().ToUpper() + "'");

                if (selectedeClipseRows.Count() > 0)
                {
                    DataRow selectedeClipseRow = selectedeClipseRows[0];
                    var selectedClientRow = this.PresentationData.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE].Select().Where(eclipseRow => row["client_id"].ToString() == eclipseRow[SecuritySummaryReportDS.CLIENTID].ToString()).FirstOrDefault();

                    if (selectedeClipseRow == null)
                    {
                        DataRow missingRow = this.PresentationData.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE].NewRow();
                        if (selectedClientRow != null)
                        {
                            missingRow[SecuritySummaryReportDS.CLIENTCID] = selectedClientRow[SecuritySummaryReportDS.CLIENTCID];
                            missingRow[SecuritySummaryReportDS.CLIENTID] = selectedClientRow[SecuritySummaryReportDS.CLIENTID];
                            missingRow[SecuritySummaryReportDS.CLIENTNAME] = selectedClientRow[SecuritySummaryReportDS.CLIENTNAME];
                        }
                        else
                        {
                            missingRow[SecuritySummaryReportDS.CLIENTCID] = Guid.Empty;
                            missingRow[SecuritySummaryReportDS.CLIENTID] = row["wrap_platform_client_id"];
                            missingRow[SecuritySummaryReportDS.CLIENTNAME] = row["investor_name"];
                        }
                        missingRow[SecuritySummaryReportDS.SECNAME] = row["investment_code"]; ;
                        missingRow[SecuritySummaryReportDS.DESCRIPTION] = row["investment_name"]; ;
                        missingRow[SecuritySummaryReportDS.TOTAL] = 0;
                        decimal eclipseHlding = 0;
                        decimal finUnitsHolding = Convert.ToDecimal(row["units"].ToString());
                        decimal finValueHolding = Convert.ToDecimal(row["value"].ToString());
                        decimal finUnitPriceHolding = Convert.ToDecimal(row["Price"].ToString());

                        row[SecuritySummaryReportDS.TOTALFINSIMPUNITS] = finUnitsHolding;
                        row[SecuritySummaryReportDS.FINSIMPUNITPRICE] = finUnitPriceHolding;
                        row[SecuritySummaryReportDS.TOTALFINSIMPCASH] = finValueHolding;
                        row[SecuritySummaryReportDS.TOTALUNITSDIFF] = eclipseHlding - finUnitsHolding;
                        row[SecuritySummaryReportDS.TOTALCASHDIFF] = eclipseHlding - finValueHolding;
                        missingRow[SecuritySummaryReportDS.PRICEATDATE] = DateTime.Now;
                        this.PresentationData.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE].Rows.Add(missingRow);
                    }
                }
            }

            var excelDataset = new DataSet();
            _entityView = new DataView(this.PresentationData.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE]) { Sort = SecuritySummaryReportDS.CLIENTNAME + " ASC" };
            DataTable entityTable = _entityView.ToTable();
            entityTable.Columns.Remove(SecuritySummaryReportDS.CLIENTCID);
            excelDataset.Merge(entityTable, true, MissingSchemaAction.Add);
            ExcelHelper.ToOpenExcel(excelDataset, "HOLDINGvsFINSIMP HOLDING-[" + DateTime.Today.ToString("dd-MMM-yyyy") + "].xlsx", Page.Response);
        }

        static IEnumerable<string> ReadAsLines(string filename)
        {
            using (StreamReader reader = new StreamReader(filename))
                while (!reader.EndOfStream)
                    yield return reader.ReadLine();
        }

        protected void GenerateAccountsFUM(object sender, EventArgs e)
        {
            PopulateData();
        }

        private void PopulateData()
        {
            IBrokerManagedComponent iBMC = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            var ds = new OrganisationListingDS
                         {
                             OrganisationListingOperationType = OrganisationListingOperationType.ClientsSECFUM,
                             AsOfDate = c1ValutionDate.Date.HasValue ? c1ValutionDate.Date.Value : DateTime.Now
                         };
            ds.ServiceTypes = Oritax.TaxSimp.Data.ServiceTypes.DoItForMe;
            iBMC.GetData(ds);           
            PresentationData = ds;
            
            UMABroker.ReleaseBrokerManagedComponent(iBMC);
        }

        private void BindControls(DataSet ds)
        {
            _entityView = new DataView(this.PresentationData.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE]) { Sort = SecuritySummaryReportDS.CLIENTNAME + " ASC" };
            DataTable entityTable = _entityView.ToTable();
            PresentationGrid.DataSource = entityTable;
            PresentationGrid.DataBind();
        }

        public override void LoadPage()
        {
            base.LoadPage();
        }

   }
}
