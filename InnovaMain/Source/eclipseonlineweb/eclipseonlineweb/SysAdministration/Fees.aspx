﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="Fees.aspx.cs" Inherits="eclipseonlineweb.Fees" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="4%">
                        <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                            ID="btnDownload" />
                    </td>
                    <td width="100%" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <asp:Label Font-Bold="true" runat="server" ID="lblSecurities" Text="Fees"></asp:Label>
                        <asp:HiddenField ID="hfSecID" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
                OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
                OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
                OnItemDataBound="PresentationGrid_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="ID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="Fees" TableLayout="Fixed" EditMode="EditForms">
                    <CommandItemSettings AddNewRecordText="Add New Fee Template" ShowExportToExcelButton="true"
                        ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                            <HeaderStyle Width="2%"></HeaderStyle>
                            <ItemStyle CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridEditCommandColumn>
                        <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ID" UniqueName="ID" />
                        <telerik:GridDropDownColumn HeaderText="Service Type" SortExpression="SERVICESTYPE"
                            UniqueName="ModelServiceTypeEditor" DataField="SERVICESTYPE" Visible="false"
                            ColumnEditorID="GridDropDownColumnEditorModelServiceType" />
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="SERVICESTYPE"
                            ReadOnly="true" HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderText="Service Type"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="SERVICESTYPE" UniqueName="SERVICESTYPE">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="FEETYPE" ReadOnly="true"
                            HeaderStyle-Width="15%" ItemStyle-Width="15%" HeaderText="Fee Type" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="FEETYPE" UniqueName="FEETYPE">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn HeaderText="Fee Type" Visible="false" SortExpression="FEETYPE"
                            UniqueName="ServiceTypeEditor" DataField="FEETYPE" ColumnEditorID="GridDropDownColumnEditorModelServiceType" />
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="DESCRIPTION"
                            HeaderStyle-Width="15%" ItemStyle-Width="15%" HeaderText="Description" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="DESCRIPTION" UniqueName="DESCRIPTION">
                        </telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn Visible="true" UniqueName="STARTDATE" PickerType="DatePicker"
                            HeaderText="Start Date" DataField="STARTDATE" DataFormatString="{0:dd/MM/yyyy}">
                            <ItemStyle Width="10%" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridDateTimeColumn Visible="true" UniqueName="ENDDATE" PickerType="DatePicker"
                            HeaderText="End Date" DataField="ENDDATE" DataFormatString="{0:dd/MM/yyyy}">
                            <ItemStyle Width="10%" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridBoundColumn FilterControlWidth="150px" HeaderStyle-Width="25%" ItemStyle-Width="35%"
                            SortExpression="COMMENTS" HeaderText="Comments" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="COMMENTS" UniqueName="COMMENTS">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn CommandName="Details" Text="Details" ButtonType="LinkButton"
                            HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        </telerik:GridButtonColumn>
                        <telerik:GridButtonColumn CommandName="Push" Text="Push" ButtonType="LinkButton"
                            HeaderStyle-Width="4%" ItemStyle-Width="5%">
                        </telerik:GridButtonColumn>
                        <telerik:GridButtonColumn ConfirmText="Delete these details record?" ButtonType="ImageButton"
                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                            <HeaderStyle Width="2%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="Html"></Excel>
                </ExportSettings>
            </telerik:RadGrid>
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorModelServiceType"
                runat="server" DropDownStyle-Width="400px" />
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorFeeType" runat="server"
                DropDownStyle-Width="300px" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
