﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using eclipseonlineweb.WebControls;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class ImportFinSimplicityOrders : UMABasePage
    {

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void Intialise()
        {
            ImportMessageControl.GridImportMessages.ColumnCreated += GridImportMessages_ColumnCreated;
            ImportMessageControl.GirdInvestmentCodeError.ColumnCreated += GridImportErrorMessages_ColumnCreated;
            ImportMessageControl.GridImportMessages.ItemDataBound += GridImportMessages_ItemDataBound;
            if (!IsPostBack)
            {
                ImportMessageControl.Clear();
            }

            ImportMessageControl.Completed += text =>
                                                  {
                                                      switch (text.ToLower())
                                                      {
                                                          case "next":
                                                              SendData();
                                                              break;
                                                          default:
                                                              ClearMessageControls();
                                                              break;
                                                      }
                                                  };

           
        }

        void GridImportMessages_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if((e.Item).ItemIndex!=-1)
            {
              DataRow dr= ((DataRowView) ((e.Item).DataItem)).Row;
                if(bool.Parse(dr["HasErrors"].ToString()))
                {
                    e.Item.ForeColor = Color.Red;
                }
            }

        }

        public override void LoadPage()
        {
            ImportMessageControl.ShowHideSampleFileLink("~/Templates/ImportSampleFiles/Amazons Rebalance 11122012_1.xls");
            if (!Page.IsPostBack)
            {
                GetSequanceNumber();
            }
        }
        void GridImportMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {

            SetDataGridColumns(e);
        }

        void GridImportErrorMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            SetDataGridColumns(e);
        }

        private void GetSequanceNumber()
        {
            ImportProcessDS ds = new ImportProcessDS();
            ds.Unit = new OrganizationUnit
            {
                Name = "Order " + DateTime.Now.ToString("dd MMM, yyyy hh:mm:ss"),
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = GetCurrentUser()
            };
            ds.Command = (int)WebCommands.ImportFinSimplcityOrders;
            ds.ExtendedProperties["GetSquanceNumber"] = true;
            SaveOrganizanition(ds);
            txtSequantialNumber.Text = ds.ExtendedProperties["PreviouseSequanceNumber"].ToString();
            ds.ExtendedProperties.Clear();
        }
        public void BtnUploadClick(object sender, EventArgs e)
        {
            // Check to see if file was uploaded
            if (filMyFile.PostedFile != null)
            {
                // Get a reference to PostedFile object
                HttpPostedFile myFile = filMyFile.PostedFile;

                // Get size of uploaded file
                int nFileLen = myFile.ContentLength;

                // make sure the size of the file is > 0
                if (nFileLen > 0)
                {
                    // Allocate a buffer for reading of the file
                    string fileName = Path.GetFileName(myFile.FileName);
                    hf_FileName.Value = fileName;
                    var date = DateTime.Now;
                    var distributionDictory = Server.MapPath("~/App_Data/Import/");
                    if (!Directory.Exists(distributionDictory))
                    {
                        Directory.CreateDirectory(distributionDictory);
                    }
                    GetNextFilename(distributionDictory + fileName);
                    string fileLocation = distributionDictory + date.ToString("ddMMyyhhmmsstt") + fileName;
                    byte[] myData = new byte[nFileLen];

                    // Read uploaded file from the Stream
                    myFile.InputStream.Read(myData, 0, nFileLen);

                    // Write data into a file
                    Utilities.WriteToFile(fileLocation, ref myData);

                    DataSet dss = ExcelReader.ReadExeclAllSheets(fileLocation);

                    ImportProcessDS ds = new ImportProcessDS();
                    foreach (DataTable dt in dss.Tables)
                    {
                        ds.Tables.Add(dt.Copy());
                    }

                    ds.Unit = new OrganizationUnit
                    {
                        Name = "Order " + DateTime.Now.ToString("dd MMM, yyyy hh:mm:ss"),
                        Type = ((int)OrganizationType.Order).ToString(),
                        CurrentUser = GetCurrentUser()
                    };
                    ds.Command = (int)WebCommands.ValidateFinSimplcityOrders;
                    ds.FileName = hf_FileName.Value;
                    SaveOrganizanition(ds);
                    SetValidationGird(ds);
                    PresentationData = ds;
                    string filename = ImportMessageControl.RefreshFileID() + ".xml";
                    ds.WriteXml(distributionDictory + filename, XmlWriteMode.WriteSchema);
                }
            }
        }

        public static string GetNextFilename(string filename)
        {
            int i = 1;
            string dir = Path.GetDirectoryName(filename);
            string file = Path.GetFileNameWithoutExtension(filename) + "{0}";
            string extension = Path.GetExtension(filename);

            while (File.Exists(filename))
                filename = Path.Combine(dir, string.Format(file, "(" + i++ + ")") + extension);

            return filename;
        }

        private void SetValidationGird(ImportProcessDS ds)
        {
            //ImportMessageControl.Clear();
            string tableName="State Street";
            using (DataView dv = new DataView(ds.Tables[tableName]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }
            using (DataView dv = new DataView(ds.Tables[tableName]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    DataRow dr = missingItems.NewRow();
                    dr["MissingItem"] = row["Oritax Id"];
                    missingItems.Rows.Add(dr);
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();

                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingClientAccountsIgnored;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }
            int successfulRows = ds.Tables[tableName].Select("HasErrors='false'").Length;
            if (ds.Tables[tableName].Rows.Count == 0 || successfulRows == 0)
            {
                ImportMessageControl.SetButtons(ButtonTypes.CLOSE);
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sErrorsCannotContinue, successfulRows, ds.Tables[tableName].Rows.Count - successfulRows), ImportMessageType.Error);
                ImportMessageControl.TextMessage.Text = "";
            }
            else
            {
                if (ds.ExtendedProperties.Contains("Result"))
                {
                    ImportMessageControl.SetButtons(ButtonTypes.CLOSE);
                    ImportMessageControl.SetTextMessage(string.Format(ds.ExtendedProperties["Message"].ToString(), successfulRows, ds.Tables[tableName].Rows.Count - successfulRows), ImportMessageType.Error);
                    ImportMessageControl.TextMessage.Text = "";
                }
                else
                {
                    ImportMessageControl.SetButtons(ButtonTypes.NEXTCANCEL);
                    //success
                    ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sValidationCompleted, successfulRows, ds.Tables[tableName].Rows.Count - successfulRows), ImportMessageType.Sucess);
                }
            }
        }

        private void SetCompletionGird(ImportProcessDS ds)
        {
            string tableName = "State Street";
            ImportMessageControl.Clear();
            using (DataView dv = new DataView(ds.Tables[tableName]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }

            using (DataView dv = new DataView(ds.Tables[tableName]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    DataRow dr = missingItems.NewRow();
                    dr["MissingItem"] = row["Oritax Id"];
                    missingItems.Rows.Add(dr);
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();

                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingClientAccountsIgnored;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }
            int successfulRows = ds.Tables[tableName].Select("HasErrors='false'").Length;
            ImportMessageControl.SetButtons(ButtonTypes.FINISH);
            ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sImportedSuccessfully, successfulRows, ds.Tables[tableName].Rows.Count - successfulRows), ImportMessageType.Sucess);
        }

        private void SendData()
        {
            var distributionDictory = Server.MapPath("~/App_Data/Import/");
            if (!Directory.Exists(distributionDictory))
            {
                Directory.CreateDirectory(distributionDictory);
            }

            string filename = distributionDictory + ImportMessageControl.GetFileID() + ".xml";
            ImportProcessDS ds = new ImportProcessDS();

            ds.ReadXml(filename);
            ds.Unit = new OrganizationUnit
                          {
                              Name = "Order " + DateTime.Now.ToString("dd MMM, yyyy hh:mm:ss"),
                              Type = ((int)OrganizationType.Order).ToString(),
                              CurrentUser = GetCurrentUser()
                          };
            ds.Command = (int)WebCommands.ImportFinSimplcityOrders;
            ds.FileName = hf_FileName.Value;
            if (chkSequantialNumber.Checked)
            {
                ds.ExtendedProperties["SequanceNumber"] = txtSequantialNumber.Text;
            }
            SaveOrganizanition(ds);
            SetCompletionGird(ds);
        }

        public void ClearMessageControls()
        {
            ImportMessageControl.Clear();
            ImportMessageControl.TextMessage.Text = "";
        }

        private static void SetDataGridColumns(GridColumnCreatedEventArgs e)
        {
            string colName = e.Column.UniqueName.ToLower();
            switch (colName)
            {
                case "applicationid":
                    e.Column.HeaderText = "Client ID";
                    break;
                case "bsb":
                    e.Column.HeaderText = "BSB";
                    e.Column.FilterControlWidth = Unit.Pixel(50);
                    break;
                case "bch":
                    e.Column.HeaderText = "BCH";
                    e.Column.FilterControlWidth = Unit.Pixel(50);
                    break;
                case "account number":
                    e.Column.HeaderText = "Account Number";
                    e.Column.FilterControlWidth = Unit.Pixel(170);
                    break;
                case "name":
                    e.Column.HeaderText = "Account Name";
                    break;
                case "trustee":
                    e.Column.HeaderText = "Trustee";
                    break;
                case "order amount":
                    ((GridBoundColumn)e.Column).DataFormatString = "{0:f2}";
                    break;
                case "rebalancing units":
                    ((GridBoundColumn)e.Column).DataFormatString = "{0:f4}";
                    break;
                case "message":
                    e.Column.HeaderText = "Message";
                    break;
                case "haserrors":
                    e.Column.HeaderText = "Has Errors";
                    e.Column.Visible = false;
                    break;
                case "ismissingitem":
                    e.Column.HeaderText = "Is Missing Item";
                    e.Column.Visible = false;
                    break;
                case "missingitem":
                    e.Column.HeaderText = "Missing Client Accounts";
                    break;
            }
        }
    }
}