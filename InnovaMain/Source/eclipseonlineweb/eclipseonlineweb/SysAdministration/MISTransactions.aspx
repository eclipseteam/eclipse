﻿<%@ Page Title="e-Clipse Online Portal" EnableViewState="true" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="MISTransactions.aspx.cs" Inherits="eclipseonlineweb.MISTransactions" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                                ID="btnDownload" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="MIS Transactions"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <c1:C1GridView ShowFilter="true" OnFiltering="Filter" RowStyle-Font-Size="Smaller"
                AllowSorting="true" OnSorting="SortMainList" FilterStyle-Font-Size="Smaller"
                HeaderStyle-Font-Size="Smaller" EnableTheming="true" Width="100%" ID="MISAccountList"
                CallbackSettings-Action="None" OnPageIndexChanging="MISAccountList_PageIndexChanging"
                PagerSettings-Mode="Numeric" runat="server" AutogenerateColumns="False" AllowPaging="true"
                PageSize="15" ClientSelectionMode="None" CallbackSettings-Mode="Partial"
                OnSelectedIndexChanging="MISAccountList_SelectedIndexChanging">
                <SelectedRowStyle />
                <Columns>
                    <c1:C1BoundField DataField="ID" SortExpression="ID" HeaderText="MISCID" Visible="false">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1BoundField>
                    <c1:C1BoundField DataField="MISCID" SortExpression="MISCID" HeaderText="MISCID" Visible="false">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1BoundField>
                    <c1:C1BoundField DataField="ClientID" SortExpression="ClientID" HeaderText="Client ID">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1BoundField>
                    <c1:C1BoundField DataField="Code" SortExpression="Code" HeaderText="Fund Code">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1BoundField>
                    <c1:C1BoundField DataField="Description" SortExpression="Description" HeaderText="Fund Description">
                        <ItemStyle HorizontalAlign="Left" />
                    </c1:C1BoundField>
                    <c1:C1BoundField DataField="TradeDate" SortExpression="TradeDate" DataFormatString="dd/MM/yyyy"
                        HeaderText="Trade Date">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1BoundField>
                    <c1:C1BoundField DataField="TransactionType" SortExpression="TransactionType" HeaderText="Transaction Type">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1BoundField>
                    <c1:C1BoundField DataField="Shares" SortExpression="Shares" HeaderText="Shares">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1BoundField>
                    <c1:C1BoundField DataField="UnitPrice" SortExpression="UnitPrice" DataFormatString="N4"
                        HeaderText="Unit Price">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1BoundField>
                    <c1:C1BoundField DataField="Amount" SortExpression="Amount" DataFormatString="C"
                        HeaderText="Amount">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1BoundField>
                    <c1:C1CommandField ShowDeleteButton="true" DeleteText="Delete">
                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                    </c1:C1CommandField>
                </Columns>
            </c1:C1GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
