﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="DownloadAtCallReconciliationFiles.aspx.cs" Inherits="eclipseonlineweb.DownloadAtCallReconciliationFiles" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:content id="HeaderContent" runat="server" contentplaceholderid="head">
</asp:content>
<asp:content id="BodyContent" runat="server" contentplaceholderid="MainContent">
   <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="85%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="At Call Holdings Vs Transactions"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <asp:Panel runat="server" ID="Panel1" Visible="true" GroupingText="At Call Holdings Vs Transactions file list">
            <telerik:RadGrid ID="gd_Download" runat="server" AutoGenerateColumns="True" PageSize="20"
        AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true" GridLines="Horizontal"
        AllowAutomaticInserts="false" AllowFilteringByColumn="true" EnableViewState="False"
        ShowFooter="True" OnNeedDataSource="gd_Download_OnNeedDataSource" OnItemCommand="gd_DownloadGridItemCommand" Width="55%">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="Top">
            <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false"
                ShowExportToWordButton="false" ShowExportToPdfButton="false"></CommandItemSettings>
            <Columns>                
                <telerik:GridBoundColumn FilterControlWidth="300px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                    SortExpression="FileName" HeaderText="File Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="FileName" UniqueName="FileName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="130px" HeaderStyle-Width="6%" ItemStyle-Width="6%"
                    SortExpression="CreateDate" HeaderText="Created Date/Time" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="false" DataField="CreateDate" UniqueName="CreateDate">
                </telerik:GridBoundColumn>
                <telerik:GridButtonColumn CommandName="Download" Text="Download" ButtonType="LinkButton"
                    HeaderStyle-Width="10%" ItemStyle-Width="10%">
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn CommandName="Delete"  Text="Delete" ConfirmText="Are you sure to delete this file?" ConfirmTitle="Delete Selected File" ButtonType="LinkButton"
                    HeaderStyle-Width="7%" ItemStyle-Width="7%">
                </telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
           </asp:Panel>
        </ContentTemplate>
</asp:UpdatePanel>
</asp:content>