﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.CM.Organization.Data;
using CarlosAg.ExcelXmlWriter;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace eclipseonlineweb
{
    public partial class ClientAndAdvisersAssociation : UMABasePage
    {
        protected override void GetBMCData()
        {
            if (!Page.IsPostBack)
            {
                }
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected void DownloadXLS(object sender, EventArgs e)
        {

        }

        public override void LoadPage()
        {
            base.LoadPage();

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
                ((SetupMaster)Master).IsAdmin = "1";
        }

        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e) { }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "update")
            {
                if ("AdvisersAndClients".Equals(e.Item.OwnerTableView.Name))
                {
                    GridEditFormItem gdItem = (e.Item as GridEditFormItem);
                    //string productID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ProID"].ToString();
                    string adviserID = ((RadComboBox)gdItem["AdviserListingCombo"].Controls[0]).SelectedValue;
                    string clientID = ((System.Web.UI.WebControls.TextBox)(gdItem["ENTITYCIID_FIELD"].Controls[0])).Text;
                    if (clientID != string.Empty)
                    {
                        ClientAdvisersAssocDS clientAdvisersAssocDS = new Oritax.TaxSimp.DataSets.ClientAdvisersAssocDS();
                        clientAdvisersAssocDS.AdviserCID = adviserID;
                        SaveData(clientID, clientAdvisersAssocDS);
                    }
                    e.Item.Edit = false;
                    e.Canceled = true;
                    PresentationGrid.Rebind();
                }
            }
        }

        protected void btnAsscociateAllAdvisersClient_Click(object sender, EventArgs e)
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            OrganisationListingDS organisationListingDS = new Oritax.TaxSimp.CM.Organization.Data.OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.ClientsWithAdvisers;
            organization.GetData(organisationListingDS);
            this.PresentationData = organisationListingDS;
            UMABroker.ReleaseBrokerManagedComponent(organization);

            DataView View = new DataView(PresentationData.Tables[OrganisationListingDS.ADVISERANDCLIENTTABLE]);
            View.RowFilter = "IsClient='true'";
            View.Sort = "ENTITYNAME_FIELD ASC";
            DataTable adviserClientTable = View.ToTable();

            foreach(DataRow row in adviserClientTable.Rows)
            {
                string clientCID = row["ENTITYCIID_FIELD"].ToString();
                string adviserCID = row["AdviserCID"].ToString();

                if (clientCID != string.Empty)
                {
                    ClientAdvisersAssocDS clientAdvisersAssocDS = new Oritax.TaxSimp.DataSets.ClientAdvisersAssocDS();
                    clientAdvisersAssocDS.AdviserCID = adviserCID;
                    SaveData(clientCID, clientAdvisersAssocDS);
                }
            }
        }

        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) { }
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e) { }
        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
        }

        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e) { }

        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;
                GridDropDownListColumnEditor editor = null;

                if ("AdvisersAndClients".Equals(e.Item.OwnerTableView.Name))
                {
                    IDictionary<Enum, string> servicesValue = Enumeration.ToDictionary(typeof(ServiceTypes));
                    editor = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("AdviserListingCombo"));
                    editor.ComboBoxControl.Filter = RadComboBoxFilter.Contains;
                    DataView view = new DataView(this.PresentationData.Tables[OrganisationListingDS.ADVISERLISTINGTABLE]);
                    view.Sort = "ENTITYNAME_FIELD ASC";
                    DataTable adviserListingTable = view.ToTable();

                    editor.DataSource = adviserListingTable;
                    editor.DataTextField = "ENTITYNAME_FIELD";
                    editor.DataValueField = "ENTITYCIID_FIELD";
                    editor.DataBind();
                }
            }
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            OrganisationListingDS organisationListingDS = new Oritax.TaxSimp.CM.Organization.Data.OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.ClientsWithAdvisers;
            organization.GetData(organisationListingDS);
            this.PresentationData = organisationListingDS;
            UMABroker.ReleaseBrokerManagedComponent(organization);

            DataView View = new DataView(PresentationData.Tables[OrganisationListingDS.ADVISERANDCLIENTTABLE]);
            View.RowFilter = "IsClient='true'";
            View.Sort = "ENTITYNAME_FIELD ASC";
            this.PresentationGrid.DataSource = View.ToTable();
        }
    }
}
