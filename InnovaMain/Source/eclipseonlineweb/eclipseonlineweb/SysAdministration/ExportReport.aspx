﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="ExportReport.aspx.cs" Inherits="eclipseonlineweb.ExportReport" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="85%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Export"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <asp:Panel runat="server" ID="Panel1" Visible="true" GroupingText="Export">
                <table width="100%">
                    <tr>
                        <td style="width:85%;">
                            <asp:Button UseSubmitBehavior="false" runat="server" ID="btnExport" OnClick="Export"
                                Text="Export" />
                            &nbsp;&nbsp;<asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </td>
                        <td style="text-align:right; width:15%;">
                            <asp:HyperLink runat="server" ID="hyp_sampleFile" Visible="false" Target="_blank"><Font size="2px" style="text-decoration:underline">View Sample File</Font></asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
