﻿using System;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;

namespace eclipseonlineweb.SysAdministration
{
    public partial class AccountantCompanyDetail : UMABasePage
    {
        protected void btnPushAccessToClient_Click(object sender, EventArgs e)
        {
            PushDownUserAccess(new Guid(this.cid)); 
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cid))
            {
                var ds = AddressControl.SetData();
                SaveData(cid, ds);
                AddressControl.FillAddressControls(ds);
            }

        }

        protected override void Intialise()
        {
            base.Intialise();
            AccountantCompanyControl1.SaveData += (Cid, ds) =>
            {
                if (Cid == Guid.Empty)
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(Cid.ToString(), ds);
                }
            };
            AccountantCompanyControl1.Saved += (cID) => Response.Redirect(Request.Url.AbsoluteUri + "?ins=" + cID);
            BankAccountControl.SaveOrg += SaveOrganizanition;
            BankAccountControl.SaveUnit += SaveData;
            DirectorControl.SaveOrg += SaveOrganizanition;
            DirectorControl.SaveUnit += SaveData;
            SignatoryControl.SaveOrg += SaveOrganizanition;
            SignatoryControl.SaveUnit += SaveData;
            ShareHolders.SaveOrg += SaveOrganizanition;
            ShareHolders.SaveUnit += SaveData;

            ClientMappingControl.SaveOrg += SaveOrganizanition;
            ClientMappingControl.SaveUnit += SaveData;
            ClientMappingControl.OrgType = OrganizationType.Accountant;
        }

        private void GetData()
        {
            var clientData = UMABroker.GetBMCInstance(new Guid(cid)) as OrganizationUnitCM;
            if (clientData != null)
            {
                var ds = new AddressDetailsDS();
                clientData.GetData(ds);
                PresentationData = ds;
                UMABroker.ReleaseBrokerManagedComponent(clientData);
                AddressControl.FillAddressControls(ds);
            }
        }

        public override void LoadPage()
        {
            cid = string.IsNullOrEmpty(Request.Params["ins"]) ? string.Empty : Request.QueryString["ins"];
            if (!IsPostBack)
            {
                AccountantCompanyControl1.SetEntity(
                    string.IsNullOrEmpty(Request.Params["ins"]) ? Guid.Empty : new Guid(Request.QueryString["ins"]),
                    Oritax.TaxSimp.CM.Group.AccountantEntityType.AccountantCompanyControl);
                if (string.IsNullOrEmpty(cid))
                {
                    SetVisibility(false);
                }
                else
                {
                    SetVisibility(true);
                    GetData();
                }
            }
        }
        private void SetVisibility(bool visible)
        {
            rpvAddress.Visible = rpvBankAccounts.Visible = rpvDirectors.Visible = rpvSignatories.Visible = rpvShareHolders.Visible = rpvClient.Visible = rpvSecurity.Visible = rpvDocuments.Visible == visible;
            RadTab tabAddressHeader = RadTabStrip1.FindTabByValue("AddressHeader");
            tabAddressHeader.Visible = visible;
            RadTab tabDirectorsHeader = RadTabStrip1.FindTabByValue("DirectorHeader");
            tabDirectorsHeader.Visible = visible;
            RadTab tabSignatoriesHeader = RadTabStrip1.FindTabByValue("SignatoryHeader");
            tabSignatoriesHeader.Visible = visible;
            RadTab tabShareHoldersHeader = RadTabStrip1.FindTabByValue("ShareHolderHeader");
            tabShareHoldersHeader.Visible = visible;
            RadTab tabAccountsHeader = RadTabStrip1.FindTabByValue("BankAccounHeader");
            tabAccountsHeader.Visible = visible;
            RadTab tabClientHeader = RadTabStrip1.FindTabByValue("ClientHeader");
            tabClientHeader.Visible = visible;
            RadTab tabSecHeader = RadTabStrip1.FindTabByValue("SecurityHeader");
            tabSecHeader.Visible = visible;
            RadTab tabDocumentsHeader = RadTabStrip1.FindTabByValue("DocumentsHeader");
            tabDocumentsHeader.Visible = visible;
        }
    }
}