﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using eclipseonlineweb.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using System.Collections.Generic;
using System.Drawing;
using Oritax.TaxSimp.Security;
using System.Configuration;
using System.Linq;
using Oritax.TaxSimp.Utilities;

namespace eclipseonlineweb
{
    public partial class ImportTermDepositPriceList : UMABasePage
    {
        public override void LoadPage()
        {
            base.LoadPage();
            if (!IsPostBack)
            {
                dtPicker.SelectedDate = DateTime.Now;
            }
        }

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void Intialise()
        {
            ImportMessageControl.GridImportMessages.ColumnCreated += GridImportMessages_ColumnCreated;
            ImportMessageControl.GirdInvestmentCodeError.ColumnCreated += GridImportErrorMessages_ColumnCreated;
            ImportMessageControl.GridImportMessages.ItemDataBound += new GridItemEventHandler(GridImportMessages_ItemDataBound);
            if (!IsPostBack)
            {
                ImportMessageControl.Clear();
            }

            ImportMessageControl.Completed += text =>
                                                  {
                                                      switch (text.ToLower())
                                                      {
                                                          case "next":
                                                              SendData();
                                                              break;
                                                          default:
                                                              ClearMessageControls();
                                                              break;
                                                      }
                                                  };


        }

        void GridImportMessages_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item).ItemIndex != -1)
            {
                DataRow dr = ((DataRowView)((e.Item).DataItem)).Row;
                if (bool.Parse(dr["HasErrors"].ToString()))
                {
                    e.Item.ForeColor = Color.Red;
                }
            }

        }
        void GridImportMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {


            SetDataGridColumns(e);
        }

        void GridImportErrorMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {

            SetDataGridColumns(e);
        }

        public void BtnUploadClick(object sender, EventArgs e)
        {
            lblMessage.Text = "";
            bool haserrors = false;
            if (cmbSelectInstitute.SelectedValue == Guid.Empty.ToString())
            {
                lblMessage.Text = "Please select Institute.";
                haserrors = true;
            }
            if (dtPicker.SelectedDate == null)
            {
                lblMessage.Text += " Please select date";
                haserrors = true;
            }

            if (cmbSelectEffectiveFor.SelectedValue =="-1")
            {
                lblMessage.Text += "Please select Rate effective for.";
                haserrors = true;
            }

            if (haserrors)
                return;


            if (filMyFile.PostedFile != null)
            {
                // Get a reference to PostedFile object
                HttpPostedFile myFile = filMyFile.PostedFile;

                // Get size of uploaded file
                int nFileLen = myFile.ContentLength;

                // make sure the size of the file is > 0
                if (nFileLen > 0)
                {
                    // Allocate a buffer for reading of the file
                    string fileName = Path.GetFileName(myFile.FileName);
                    var date = DateTime.Now;
                    var DistinationDirectory = Server.MapPath("~/App_Data/Import/");
                    if (!Directory.Exists(DistinationDirectory))
                    {
                        Directory.CreateDirectory(DistinationDirectory);
                    }

                    string fileLocation = DistinationDirectory + date.ToString("ddMMyyhhmmsstt") + fileName;
                    byte[] myData = new byte[nFileLen];

                    // Read uploaded file from the Stream
                    myFile.InputStream.Read(myData, 0, nFileLen);

                    // Write data into a file
                    Utilities.WriteToFile(fileLocation, ref myData);
                    string csv = string.Empty;
                    using (StreamReader reader = new StreamReader(fileLocation, true))
                    {
                        csv = reader.ReadToEnd();
                        reader.Close();
                    }
                    string TDPrice = ConversorTransactionCsv.SelectRows(csv, "");
                    DataTable dt = new DataTable();
                    ImportProcessDS ds = new ImportProcessDS();
                    ds.ExtendedProperties.Add("InstituteID", cmbSelectInstitute.SelectedValue);
                    ds.ExtendedProperties.Add("RateEffectiveFor", cmbSelectEffectiveFor.SelectedValue);
                    dt = ConversorTransactionCsv.CreateTermDepositPriceList(TDPrice, dtPicker.SelectedDate.Value.ToString("dd/MM/yyyy"));
                    ds.Command = (int)WebCommands.ValidateTDPriceListDataFeedImportFile;
                    ds.Tables.Add(dt);
                    ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
                    ds.FileName = fileName;
                    SaveOrganizanition(ds);
                    SetValidationGird(ds);
                    PresentationData = ds;
                    string filename = ImportMessageControl.RefreshFileID() + ".xml";
                    ds.WriteXml(DistinationDirectory + filename, XmlWriteMode.WriteSchema);
                }
            }

            hdnCriteria.Value = "file";
            // Check to see if file was uploaded
        }

        private void SetValidationGird(ImportProcessDS ds)
        {
            //ImportMessageControl.Clear();
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                DataRow dr;
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    dr = missingItems.NewRow();
                    dr["MissingItem"] = row["Provider"];
                    missingItems.Rows.Add(dr);
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingProviderAdded;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }

            int successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
            if (ds.Tables[0].Rows.Count == 0 || successfulRows == 0)
            {
                ImportMessageControl.SetButtons(ButtonTypes.CLOSE);
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sErrorsCannotContinue, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Error);
                ImportMessageControl.TextMessage.Text = "";
            }
            else
            {
                ImportMessageControl.SetButtons(ButtonTypes.NEXTCANCEL);
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sValidationCompleted, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
            }
        }

        private void SetCompletionGird(ImportProcessDS ds)
        {
            ImportMessageControl.Clear();
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }

            using (DataView dv = new DataView(ds.Tables[0]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                DataRow dr;
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    dr = missingItems.NewRow();
                    dr["MissingItem"] = row["Provider"];
                    missingItems.Rows.Add(dr);
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingProviderAdded;

                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }

            int successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
            ImportMessageControl.SetButtons(ButtonTypes.FINISH);
            ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sImportedSuccessfully, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
        }

        private void SendData()
        {
            var distributionDictory = Server.MapPath("~/App_Data/Import/");
            if (!Directory.Exists(distributionDictory))
            {
                Directory.CreateDirectory(distributionDictory);
            }
            string filename = distributionDictory + ImportMessageControl.GetFileID() + ".xml";
            ImportProcessDS ds = new ImportProcessDS();
            ds.ReadXml(filename);
            if (ds.ExtendedProperties.Count == 0)
            {
                ds.ExtendedProperties.Add("InstituteID", cmbSelectInstitute.SelectedValue);
                ds.ExtendedProperties.Add("RateEffectiveFor", cmbSelectEffectiveFor.SelectedValue);
            }
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };

            switch (hdnCriteria.Value)
            {
                case "file":
                    ds.Command = (int)WebCommands.ImportTDPriceListFeedImportFile;
                    break;
                case "amm":
                    ds.Command = (int)WebCommands.ImportTDRates;
                    break;
            }
            ds.FileName = filename;
            SaveOrganizanition(ds);
            SetCompletionGird(ds);

            ImportMessageControl.SetImportSuccessFileMsg("You can see Rate Details", "~/Administration/TDTransaction.aspx");
        }

        public void ClearMessageControls()
        {
            ImportMessageControl.Clear();
            ImportMessageControl.TextMessage.Text = "";
        }

        private void SetDataGridColumns(GridColumnCreatedEventArgs e)
        {
            string colName = e.Column.UniqueName.ToLower();
            switch (colName)
            {
                case "rateid":
                case "providerid":
                    e.Column.Visible = false;
                break;
                case "provider":
                    e.Column.HeaderText = "Provider (Institute)";
                    break;
                case "providertype":
                    e.Column.HeaderText = "Provider Type";
                    e.Column.FilterControlWidth = Unit.Pixel(50);
                    break;
                case "splongtermrating":
                    e.Column.HeaderText = "S&P Long Term Rating";
                    e.Column.FilterControlWidth = Unit.Pixel(50);
                    break;
                case "min":
                    e.Column.HeaderText = "Min $";
                    e.Column.FilterControlWidth = Unit.Pixel(170);
                    break;
                case "max":
                    e.Column.HeaderText = "Max $";
                    break;
                case "maximumbrokeage":
                    e.Column.HeaderText = "Maximum Brokeage";
                    break;

                case "days30":
                    e.Column.HeaderText = "30-Days %";
                    break;
                case "days60":
                    e.Column.HeaderText = "60-Days %";
                    break;
                case "days90":
                    e.Column.HeaderText = "90-Days %";
                    break;

                case "days120":
                    e.Column.HeaderText = "120-Days %";
                    break;
                case "days150":
                    e.Column.HeaderText = "150-Days %";
                    break;
                case "days180":
                    e.Column.HeaderText = "180-Days %";
                    break;

                case "days270":
                    e.Column.HeaderText = "270-Days %";
                    break;
                case "years1":
                    e.Column.HeaderText = "1 Year %";
                    break;
                case "years2":
                    e.Column.HeaderText = "2 Years %";
                    break;
                case "years3":
                    e.Column.HeaderText = "3 Years %";
                    break;
                case "years4":
                    e.Column.HeaderText = "4 Years %";
                    break;
                case "years5":
                    e.Column.HeaderText = "5 Years %";
                    break;
                case "message":
                    e.Column.HeaderText = "Message";
                    break;
                case "haserrors":
                    e.Column.HeaderText = "Has Errors";
                    e.Column.Visible = false;
                    break;
                case "ismissingitem":
                    e.Column.HeaderText = "Is Missing Item";
                    e.Column.Visible = false;
                    break;
                case "missingitem":
                    e.Column.HeaderText = "Missing Providers (Institutes)";
                    break;
            }
        }

        protected override void GetBMCData()
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            FillInstitutionDropDown(organization.Institution);
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        private void FillInstitutionDropDown(List<InstitutionEntity> Institution)
        {

            string Insval = cmbSelectInstitute.SelectedValue;
            cmbSelectInstitute.ClearSelection();
            cmbSelectInstitute.DataSource = Institution;
            cmbSelectInstitute.DataTextField = "Name";
            cmbSelectInstitute.DataValueField = "ID";
            cmbSelectInstitute.DataBind();
            RadComboBoxItem item = new RadComboBoxItem();
            item.Text = "-- Select Institute --";
            item.Value = Guid.Empty.ToString();
            cmbSelectInstitute.Items.Insert(0, item);
            cmbSelectInstitute.SelectedValue = Insval;
        }

        protected void btnImport_OnClick(object sender, EventArgs e)
        {
            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            if (org != null)
            {
                var inst = org.Institution;
                var dt = GetTdRates(inst.Where(ss => ss.Name.ToLower() == "amm").ToList().First().ID);
                WriteXml(dt);
                var ds = new ImportProcessDS();
                ds.Tables.Add(dt.Copy());
                ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
                ds.Command = (int)WebCommands.ValidateTDRates;
                SaveOrganizanition(ds);
                SetValidationGird(ds);
                PresentationData = ds;
            }
            hdnCriteria.Value = "amm";
        }

        private void WriteXml(DataTable dt)
        {
            const string fileName = "AMM_TDRates.xml";
            var date = DateTime.Now;
            var distributionDictory = Server.MapPath("~/App_Data/Import/");
            if (!Directory.Exists(distributionDictory))
                Directory.CreateDirectory(distributionDictory);
            DataSet dsXml = new DataSet();
            dsXml.Tables.Add(dt);
            string fileContent = dsXml.GetXml();
            byte[] myData = new byte[fileContent.Length * sizeof(char)];
            System.Buffer.BlockCopy(fileContent.ToCharArray(), 0, myData, 0, myData.Length);
            string fileLocation =
                distributionDictory + date.ToString("ddMMyyhhmmsstt") + fileName;
            WebUtilities.Utilities.WriteToFile(fileLocation, ref myData);
            ImportMessageControl.FileID.Value = date.ToString("ddMMyyhhmmsstt") + "AMM_TDRates";
        }

        #region Old Code
        //private DataTable GetTdRates(Guid instituteId)
        //{
        //    var dt = new DataTable();
        //    dt.Columns.Add(new DataColumn("RateID", typeof(int)));
        //    dt.Columns.Add(new DataColumn("ProviderID", typeof(int)));
        //    dt.Columns.Add(new DataColumn("Provider", typeof(String)));
        //    dt.Columns.Add(new DataColumn("ProviderType", typeof(String)));
        //    dt.Columns.Add(new DataColumn("SPLongTermRating", typeof(String)));
        //    dt.Columns.Add(new DataColumn("Min", typeof(decimal)));
        //    dt.Columns.Add(new DataColumn("Max", typeof(decimal)));
        //    dt.Columns.Add(new DataColumn("MaximumBrokeage", typeof(decimal)));
        //    dt.Columns.Add(new DataColumn("Days30", typeof(decimal)));
        //    dt.Columns.Add(new DataColumn("Days60", typeof(decimal)));
        //    dt.Columns.Add(new DataColumn("Days90", typeof(decimal)));
        //    dt.Columns.Add(new DataColumn("Days120", typeof(decimal)));
        //    dt.Columns.Add(new DataColumn("Days150", typeof(decimal)));
        //    dt.Columns.Add(new DataColumn("Days180", typeof(decimal)));
        //    dt.Columns.Add(new DataColumn("Days270", typeof(decimal)));
        //    dt.Columns.Add(new DataColumn("Years1", typeof(decimal)));
        //    dt.Columns.Add(new DataColumn("Years2", typeof(decimal)));
        //    dt.Columns.Add(new DataColumn("Years3", typeof(decimal)));
        //    dt.Columns.Add(new DataColumn("Years4", typeof(decimal)));
        //    dt.Columns.Add(new DataColumn("Years5", typeof(decimal)));
        //    dt.Columns.Add(new DataColumn("Date", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Status", typeof(string)));

        //    var objTdws = (dynamic)null;
        //    var objProviderRates = (dynamic)null;

        //    if (DBConnection.Connection.ConnectionString.Contains("Test"))
        //    {
        //        objTdws = new eclipseonlineweb.AmmWs.TDWS();
        //        objProviderRates =
        //            objTdws.GetAllRates(ConfigurationManager.AppSettings["AmmTestApiKey"]);
        //    }
        //    else
        //    {
        //        objTdws = new eclipseonlineweb.AMMLive.TDWS();
        //        objProviderRates =
        //            objTdws.GetAllRates(ConfigurationManager.AppSettings["AmmProductionApiKey"]);
        //    }

        //    var orgCm = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
        //    if (orgCm != null)
        //    {
        //        var inst = orgCm.Institution;

        //        foreach (var rate in objProviderRates.ProviderRate)
        //        {
        //            if (rate.providerProductType == "Term Deposits")
        //            {
        //                var dr = dt.NewRow();
        //                dr["ProviderID"] = rate.providerID;
        //                dr["RateID"] = rate.providerRateID;
        //                dr["Provider"] = rate.providerName;
        //                dr["ProviderType"] = rate.providerType;
        //                dr["SPLongTermRating"] = string.Empty;

        //                if (!string.IsNullOrEmpty(rate.providerRateLowerLimit))
        //                    dr["Min"] = Decimal.Parse(rate.providerRateLowerLimit);

        //                if (!string.IsNullOrEmpty(rate.providerRateUpperLimit))
        //                    dr["Max"] = Decimal.Parse(rate.providerRateUpperLimit);

        //                if (!string.IsNullOrEmpty(rate.providerRateBrokerage))
        //                    dr["MaximumBrokeage"] = Decimal.Parse(rate.providerRateBrokerage);

        //                if (!string.IsNullOrEmpty(rate.provider1mRate))
        //                    dr["Days30"] = Decimal.Parse(rate.provider1mRate);

        //                if (!string.IsNullOrEmpty(rate.provider2mRate))
        //                    dr["Days60"] = Decimal.Parse(rate.provider2mRate);

        //                if (!string.IsNullOrEmpty(rate.provider3mRate))
        //                    dr["Days90"] = Decimal.Parse(rate.provider3mRate);

        //                if (!string.IsNullOrEmpty(rate.provider4mRate))
        //                    dr["Days120"] = Decimal.Parse(rate.provider4mRate);

        //                if (!string.IsNullOrEmpty(rate.provider5mRate))
        //                    dr["Days150"] = Decimal.Parse(rate.provider5mRate);

        //                if (!string.IsNullOrEmpty(rate.provider6mRate))
        //                    dr["Days180"] = Decimal.Parse(rate.provider6mRate);

        //                if (!string.IsNullOrEmpty(rate.provider9mRate))
        //                    dr["Days270"] = Decimal.Parse(rate.provider9mRate);

        //                if (!string.IsNullOrEmpty(rate.provider1yRate))
        //                    dr["Years1"] = Decimal.Parse(rate.provider1yRate);

        //                if (!string.IsNullOrEmpty(rate.provider2yRate))
        //                    dr["Years2"] = Decimal.Parse(rate.provider2yRate);

        //                if (!string.IsNullOrEmpty(rate.provider3yRate))
        //                    dr["Years3"] = Decimal.Parse(rate.provider3yRate);

        //                if (!string.IsNullOrEmpty(rate.provider4yRate))
        //                    dr["Years4"] = Decimal.Parse(rate.provider4yRate);

        //                if (!string.IsNullOrEmpty(rate.provider5yRate))
        //                    dr["Years5"] = Decimal.Parse(rate.provider5yRate);

        //                dr["Date"] = ((DateTime)dtPicker.SelectedDate).ToString("dd/MM/yyyy");
        //                dr["Status"] = "Uploaded";
        //                dt.Rows.Add(dr);
        //            }
        //        }
        //    }
        //    return dt;
        //}
        
        #endregion
        private DataTable GetTdRates(Guid instituteId)
        {
            var dt = new DataTable();
            dt.Columns.Add(new DataColumn("RateID", typeof(int)));
            dt.Columns.Add(new DataColumn("ProviderID", typeof(int)));
            dt.Columns.Add(new DataColumn("Name", typeof(String)));
            dt.Columns.Add(new DataColumn("Type", typeof(String)));
            dt.Columns.Add(new DataColumn("ID", typeof(Guid)));
            dt.Columns.Add(new DataColumn("InstituteID", typeof(Guid)));
            dt.Columns.Add(new DataColumn("Min", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Max", typeof(decimal)));
            dt.Columns.Add(new DataColumn("MaximumBrokeage", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Days30", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Days60", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Days90", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Days120", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Days150", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Days180", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Days270", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Years1", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Years2", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Years3", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Years4", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Years5", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Status", typeof(String)));
            dt.Columns.Add(new DataColumn("Date", typeof(DateTime)));
            dt.Columns.Add(new DataColumn("Ongoing", typeof(decimal)));
            var objTdws = (dynamic)null;
            var objProviderRates = (dynamic)null;

            if (DBConnection.Connection.ConnectionString.Contains("Test"))
            {
                objTdws = new eclipseonlineweb.AmmWs.TDWS();
                objProviderRates =
                    objTdws.GetAllRates(ConfigurationManager.AppSettings["AmmTestApiKey"]);
            }
            else
            {
                objTdws = new eclipseonlineweb.AMMLive.TDWS();
                objProviderRates =
                    objTdws.GetAllRates(ConfigurationManager.AppSettings["AmmProductionApiKey"]);
            }

            var orgCm = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            if (orgCm != null)
            {
                var inst = orgCm.Institution;

                foreach (var rate in objProviderRates.ProviderRate)
                {
                    if (rate.providerProductType == "Term Deposits")
                    {
                        var dr = dt.NewRow();
                        dr["RateID"] = rate.providerRateID;
                        dr["Name"] = rate.providerName;
                        dr["ProviderID"] = rate.providerID;
                        dr["Type"] = rate.providerType;
                        dr["RateID"] = rate.providerRateID;
                        dr["ID"] = Guid.NewGuid();
                        dr["InstituteID"] = instituteId;

                        if (!string.IsNullOrEmpty(rate.providerRateLowerLimit))
                            dr["Min"] = Decimal.Parse(rate.providerRateLowerLimit);

                        if (!string.IsNullOrEmpty(rate.providerRateUpperLimit))
                            dr["Max"] = Decimal.Parse(rate.providerRateUpperLimit);

                        if (!string.IsNullOrEmpty(rate.providerRateBrokerage))
                            dr["MaximumBrokeage"] = Decimal.Parse(rate.providerRateBrokerage);

                        if (!string.IsNullOrEmpty(rate.provider1mRate))
                            dr["Days30"] = Decimal.Parse(rate.provider1mRate);

                        if (!string.IsNullOrEmpty(rate.provider2mRate))
                            dr["Days60"] = Decimal.Parse(rate.provider2mRate);

                        if (!string.IsNullOrEmpty(rate.provider3mRate))
                            dr["Days90"] = Decimal.Parse(rate.provider3mRate);

                        if (!string.IsNullOrEmpty(rate.provider4mRate))
                            dr["Days120"] = Decimal.Parse(rate.provider4mRate);

                        if (!string.IsNullOrEmpty(rate.provider5mRate))
                            dr["Days150"] = Decimal.Parse(rate.provider5mRate);

                        if (!string.IsNullOrEmpty(rate.provider6mRate))
                            dr["Days180"] = Decimal.Parse(rate.provider6mRate);

                        if (!string.IsNullOrEmpty(rate.provider9mRate))
                            dr["Days270"] = Decimal.Parse(rate.provider9mRate);

                        if (!string.IsNullOrEmpty(rate.provider1yRate))
                            dr["Years1"] = Decimal.Parse(rate.provider1yRate);

                        if (!string.IsNullOrEmpty(rate.provider2yRate))
                            dr["Years2"] = Decimal.Parse(rate.provider2yRate);

                        if (!string.IsNullOrEmpty(rate.provider3yRate))
                            dr["Years3"] = Decimal.Parse(rate.provider3yRate);

                        if (!string.IsNullOrEmpty(rate.provider4yRate))
                            dr["Years4"] = Decimal.Parse(rate.provider4yRate);

                        if (!string.IsNullOrEmpty(rate.provider5yRate))
                            dr["Years5"] = Decimal.Parse(rate.provider5yRate);

                        dr["Status"] = "Uploaded";
                        dr["Date"] = ((DateTime)dtPicker.SelectedDate).ToString("dd/MM/yyyy");
                        dr["Ongoing"] = 0;
                        dt.Rows.Add(dr);
                    }
                }
            }
            return dt;
        }
    }
}