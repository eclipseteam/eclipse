<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="ClientReportsDataValidationReport.aspx.cs"
    Inherits="eclipseonlineweb.SysAdministration.ClientReportsDataValidationReport" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function SetTradingName(sender, args) {
            var txtClientCount = $find("<%: txtClientCount.ClientID %>");
            if (args.get_checked()) {
                txtClientCount.set_minValue(0);
                txtClientCount.set_value(0);

            }
            else {
                txtClientCount.set_value(5);
                txtClientCount.set_minValue(5);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <table width="100%">
            <tr>
                <td style="width: 5%">
                    <span class="riLabel">Start Date:</span><telerik:RadDatePicker ID="InputStartDate"
                        runat="server">
                        <DateInput DateFormat="dd/MM/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
                </td>
                <td style="width: 5%">
                    <span class="riLabel">End Date:</span><telerik:RadDatePicker ID="InputEndDate" runat="server">
                        <DateInput DateFormat="dd/MM/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <span class="riLabel">All Clients:</span>
                    <telerik:RadButton ID="chkAllClients" runat="server" Text="All" AutoPostBack="false"
                        OnClientCheckedChanged="SetTradingName" ToggleType="CheckBox" ButtonType="ToggleButton">
                    </telerik:RadButton>
                </td>
                <td>
                    <span class="riLabel">No. of Clients:</span>
                    <telerik:RadNumericTextBox runat="server" ID="txtClientCount" MinValue="5" ShowSpinButtons="True"
                        EnableViewState="true" Value="5">
                        <NumberFormat GroupSeparator="" DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
                </td>
                <td>
                    <br />
                    <telerik:RadButton runat="server" ID="BtnGenerateReport" OnClick="GenerateReport"
                        Text="Generate Report">
                    </telerik:RadButton>
                </td>
                <td style="width: 45%;" class="breadcrumbgap">
                    <uc1:BreadCrumb ID="BreadCrumb2" runat="server" />
                    <br />
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <fieldset>
        <legend>Client Report's Data Validation</legend>Client Checked:<asp:Label ID="lblClientCount"
            runat="server"></asp:Label>
        <br />
        <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
            Width="100%" PageSize="500" AllowSorting="True" AllowMultiRowSelection="False"
            AllowPaging="false" GridLines="None" AllowFilteringByColumn="true" EnableViewState="true"
            ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowFilteringByColumn="True" TableLayout="Fixed" Width="100%">
                <Columns>
                    <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="ClientCID"
                        SortExpression="ClientID" UniqueName="ClientID" DataNavigateUrlFormatString="../ClientViews/ClientMainView.aspx?ins={0}"
                        HeaderText="Client ID" DataTextField="ClientID" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" FilterControlWidth="75px" HeaderStyle-Width="120px"
                        HeaderButtonType="TextButton" ShowFilterIcon="true">
                    </telerik:GridHyperLinkColumn>
                    <telerik:GridBoundColumn SortExpression="ClientName" HeaderText="ClientName" HeaderButtonType="TextButton"
                        DataField="ClientName" UniqueName="ClientName" FilterControlWidth="100px" HeaderStyle-Width="200px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="ReportName" HeaderText="Report Name" HeaderButtonType="TextButton"
                        DataField="ReportName" UniqueName="ReportName" FilterControlWidth="100px" HeaderStyle-Width="175px">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn SortExpression="Message" FilterControlWidth="100px" HeaderStyle-Width="250px"
                        HeaderText="Message" HeaderButtonType="TextButton" DataField="Message" UniqueName="Message">
                        <ItemTemplate>
                            <asp:Label ID="lblRate" runat="server" Text='<%#Eval("Message").ToString().Replace(",","<br/>") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </fieldset>
</asp:Content>
