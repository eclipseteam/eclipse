﻿using System;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
namespace eclipseonlineweb.SysAdministration
{
    public partial class DesktopBrokerAccounts : UMABasePage
    {

    
                protected override void Intialise()
                {
                    base.Intialise();

                    ASXControl.Saved += (CID, CLID, CSID) =>
                                            {
                                                HideShowDesktopBrokerGrid(false);
                                                SaveASXData(Guid.Parse(SelectedClientCID.Value), CID, CLID, CSID, DatasetCommandTypes.Update);
                                            };
                    ASXControl.Canceled += () => HideShowDesktopBrokerGrid(false);
                    ASXControl.ValidationFailed += () => HideShowDesktopBrokerGrid(true);
                    ASXControl.SaveData += (asxCid, ds) =>
                        {
                        ((DesktopBrokerAccountDS) ds).RepUnitCID = Guid.Parse(SelectedClientCID.Value);
                        if (asxCid == Guid.Empty.ToString())
                        {
                            SaveOrganizanition(ds);
                        }
                        else
                        {
                            SaveData(asxCid, ds);
                        }
                        HideShowDesktopBrokerGrid(false);
                    };

            
                }

                private void HideShowDesktopBrokerGrid(bool show)
                {
                    OVER.Visible = ASXModal.Visible = show;
                }


                private DataSet SaveASXData(Guid ClientCID,Guid CID, Guid CLID, Guid CSID, DatasetCommandTypes commandType)
                {
                  
                        var ds = new DesktopBrokerAccountDS {CommandType = commandType};
                        DataRow dr = ds.DesktopBrokerAccountsTable.NewRow();
                        dr[ds.DesktopBrokerAccountsTable.CID] = CID;
                        dr[ds.DesktopBrokerAccountsTable.CLID] = CLID;
                        dr[ds.DesktopBrokerAccountsTable.CSID] = CSID;
                        ds.Tables[ds.DesktopBrokerAccountsTable.TABLENAME].Rows.Add(dr);
                        ds.RepUnitCID = ClientCID;
                        if (ClientCID != Guid.Empty)
                        {
                            SaveData(ClientCID, ds);
                        }
                    PresentationGrid.Rebind();
                   
                    return ds;

                }
        
                public override void LoadPage()
                {
            
                    if (!IsPostBack)
                    {
                        var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
                        if (objUser.Name != "Administrator" && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor || objUser.UserType == UserType.DealerGroup || objUser.UserType == UserType.IFA || objUser.UserType == UserType.Accountant))
                        {
                   
                            PresentationGrid.Columns.FindByUniqueNameSafe("DeleteColumn").Display = false;
                            PresentationGrid.Columns.FindByUniqueNameSafe("EditColumn").Display = false;
                        }
                        UMABroker.ReleaseBrokerManagedComponent(objUser);
                    }
                }
                private void GetData()
                {
                    IBrokerManagedComponent orgcm = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
                    var ds = new DesktopBrokerAccountDS{Unit = new OrganizationUnit(){CurrentUser = GetCurrentUser()},
                        Command = (int) WebCommands.GetAllClientDetails};

                    orgcm.GetData(ds);
                    PresentationData = ds;
                    var summaryView = new DataView(ds.Tables[ds.DesktopBrokerAccountsTable.TABLENAME]) { Sort = ds.DesktopBrokerAccountsTable.ACCOUNTNAME + " DESC" };
                    PresentationGrid.DataSource = summaryView;
                    UMABroker.ReleaseBrokerManagedComponent(orgcm);
                }
        
              

       
                protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
                {
                    GetData();
                }

                protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
                {
                    if (!(e.Item is GridDataItem)) return;

                    var dataItem = (GridDataItem)e.Item;
                    var clientCID = new Guid(dataItem["ClientCID"].Text);
                    SelectedClientCID.Value = clientCID.ToString();
                    var linkEntiyCID = new Guid(dataItem["CID"].Text);
                    var linkEntiyClid = new Guid(dataItem["CLID"].Text);
                    var linkEntiyCsid = new Guid(dataItem["CSID"].Text);
                    switch (e.CommandName.ToLower())
                    {
                        case "edit":
                            e.Item.Edit = false;
                            e.Canceled = true;
                            
                            ASXControl.SetEntity(linkEntiyCID);
                            HideShowDesktopBrokerGrid(true);
                            break;
                        case "delete":
                            e.Item.Edit = false;
                            e.Canceled = true;
                            var ds = SaveASXData(clientCID, linkEntiyCID, linkEntiyClid, linkEntiyCsid, DatasetCommandTypes.Delete);
                           
                            PresentationGrid.Rebind();
                          break;
                    }
                }

        protected void btnDownload_Onclick(object sender, EventArgs e)
        {
            GetData();

            DataSet excelDataset = new DataSet();
            var ds=PresentationData as DesktopBrokerAccountDS;
            DataView entityView = ds.DesktopBrokerAccountsTable.DefaultView;
            entityView.Sort = ds.DesktopBrokerAccountsTable.ACCOUNTNAME + " DESC";
            DataTable entityTable = entityView.ToTable();
            entityTable.Columns.Remove(ds.DesktopBrokerAccountsTable.CID);
            entityTable.Columns.Remove(ds.DesktopBrokerAccountsTable.CLIENTCID);
            entityTable.Columns.Remove(ds.DesktopBrokerAccountsTable.CLID);
            entityTable.Columns.Remove(ds.DesktopBrokerAccountsTable.CSID);
            excelDataset.Merge(entityTable, true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, "DesktopBrokerAccounts-FUM-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", Page.Response);
        }
    }
}