﻿<%@ Page Title="e-Clipse Online Portal" EnableViewState="true" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="ASXAccountVsTransactions.aspx.cs" Inherits="eclipseonlineweb.ASXAccountVsTransactions" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .RadGrid_Metro .rgHeader, .RadGrid_Metro .rgHeader a
        {
            font-size: 8pt !important;
        }
        .RadGrid_Metro .rgRow a, .RadGrid_Metro .rgAltRow a, .RadGrid_Metro tr.rgEditRow a, .RadGrid_Metro .rgFooter a, .RadGrid_Metro .rgEditForm a
        {
            font-size: 8pt !important;
        }
    </style>
    <script type="text/javascript">
        function doFilter(sender, eventArgs, colUniqueName) {
            if (eventArgs.keyCode == 13) {
                eventArgs.cancelBubble = true;
                eventArgs.returnValue = false;
                if (eventArgs.stopPropagation) {
                    eventArgs.stopPropagation();
                    eventArgs.preventDefault();
                }
                var masterTableView = $find("<%: PresentationGrid.ClientID %>").get_masterTableView();
                var uniqueName = colUniqueName; // set Column UniqueName for the filtering TextBox
                masterTableView.filter(uniqueName, sender.value, Telerik.Web.UI.GridFilterFunction.CurrentKnownFunction);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                                ID="btnDownload" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="ASX Broker holding vs. transactions"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid OnNeedDataSource="PresentationGrid_OnNeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true" GridLines="None"
                AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnItemCreated="PresentationGrid_OnItemCreated">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="BankAccVsTransactions" TableLayout="Fixed" Font-Size="8">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <Columns>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="CID" HeaderText="CID" AutoPostBackOnFilter="false"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="CID" UniqueName="CID" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridHyperLinkColumn FilterControlWidth="50px" HeaderStyle-Width="90px" DataTextFormatString="{0}"
                            DataNavigateUrlFields="CID" SortExpression="ClientID" UniqueName="ClientID" DataNavigateUrlFormatString="../ClientViews/ClientMainView.aspx?ins={0}"
                            HeaderText="Client ID" DataTextField="ClientID" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                            HeaderButtonType="TextButton" ShowFilterIcon="true">
                        </telerik:GridHyperLinkColumn>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="EntityClientID" HeaderText="EntityClientID"
                            AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="EntityClientID" UniqueName="EntityClientID"
                            Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="410px" HeaderStyle-Width="450px" SortExpression="TrandingName"
                            ReadOnly="true" HeaderText="Tranding Name" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TrandingName"
                            UniqueName="TrandingName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" SortExpression="ServiceTypes"
                            ReadOnly="true" HeaderText="Service Types" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ServiceTypes"
                            UniqueName="ServiceTypes">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" SortExpression="AccountNumber"
                            ReadOnly="true" HeaderText="Account Number" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountNumber"
                            UniqueName="AccountNumber">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ItemStyle-HorizontalAlign="Right" FilterControlWidth="60px"
                            HeaderStyle-Width="100px" ReadOnly="true" SortExpression="UnitHolding" HeaderText="Unit Holding"
                            AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="UnitHolding" UniqueName="UnitHolding"
                            DataFormatString="{0:N4}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ItemStyle-HorizontalAlign="Right" FilterControlWidth="90px"
                            HeaderStyle-Width="130px" ReadOnly="true" SortExpression="TransactionUnitHolding"
                            HeaderText="System Transaction Unit" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransactionUnitHolding"
                            UniqueName="TransactionUnitHolding" DataFormatString="{0:N4}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ItemStyle-HorizontalAlign="Right" FilterControlWidth="60px"
                            HeaderStyle-Width="100px" ReadOnly="true" SortExpression="Difference" HeaderText="Difference"
                            AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="Difference" UniqueName="Difference"
                            DataFormatString="{0:N4}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ItemStyle-HorizontalAlign="Right" ReadOnly="true" SortExpression="UnsettledOrder"
                            HeaderText="Unsettled Order" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UnsettledOrder"
                            UniqueName="UnsettledOrder" DataFormatString="{0:N4}" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ItemStyle-HorizontalAlign="Right" ReadOnly="true" SortExpression="Total"
                            HeaderText="Total" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Total" UniqueName="Total"
                            DataFormatString="{0:N4}" Visible="false">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
