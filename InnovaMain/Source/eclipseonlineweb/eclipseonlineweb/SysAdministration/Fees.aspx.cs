﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.Common.UMAFee;

namespace eclipseonlineweb
{
    public partial class Fees : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void GetBMCData()
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            UMAFeesDS umaFeesDS = new UMAFeesDS();
            organization.GetData(umaFeesDS);
            this.PresentationData = umaFeesDS;

            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
        }

        public override void LoadPage()
        {
            base.LoadPage();

            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
                ((SetupMaster)Master).IsAdmin = "1";
        }

        protected void PresentationGrid_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e){ }
        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "details")
            {
                string ID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"].ToString();
                Response.Redirect("FeeDetails.aspx?ID=" + ID);
            }
            if (e.CommandName.ToLower() == "push")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                Guid ID = new Guid(gDataItem["ID"].Text);
                UMAFeesDS DS = new UMAFeesDS();
                DS.DataSetOperationType = DataSetOperationType.PushUpdate;
                FeeEntity entityToUpdateBulk = new FeeEntity();
                entityToUpdateBulk.ID = ID;
                DS.FeeEntityToAddUpdate = entityToUpdateBulk;
                base.SaveOrganizanition(DS);
            }
            else if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                Guid ID = new Guid(gDataItem["ID"].Text);
                UMAFeesDS DS = new UMAFeesDS();
                DS.DataSetOperationType = DataSetOperationType.DeletSingle;
                FeeEntity entityToDel = new FeeEntity();
                entityToDel.ID = ID;
                DS.FeeEntityToAddUpdate = entityToDel; 
                base.SaveOrganizanition(DS);
                this.GetBMCData();
                this.PresentationGrid.Rebind();

            }
            else if (e.CommandName.ToLower() == "update")
            {
                GridEditFormItem EditForm = (GridEditFormItem)e.Item;
            
                UMABroker.SaveOverride = true;

                RadComboBox combo = (RadComboBox)EditForm["ServiceTypeEditor"].Controls[0];
                RadComboBox servicesCombo = (RadComboBox)EditForm["ModelServiceTypeEditor"].Controls[0];
                    
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                Guid ID = (Guid)EditForm.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"]; 

                UMAFeesDS umaFeesDS = new UMAFeesDS();
                DataTable feeTable = umaFeesDS.Tables[UMAFeesDS.FEELISTABLE];
                umaFeesDS.DataSetOperationType = DataSetOperationType.UpdateSingle;

                DataRow feeRow = feeTable.NewRow();

                feeRow[UMAFeesDS.ID] = ID;
                FeeType feeType = FeeType.None;
               
                if (combo.SelectedValue == FeeType.AccountKeeping.ToString())
                    feeType = FeeType.AccountKeeping;
                else if (combo.SelectedValue == FeeType.Adviser.ToString())
                    feeType = FeeType.Adviser;
                else if (combo.SelectedValue == FeeType.AssetMgt.ToString())
                    feeType = FeeType.AssetMgt;
                else if (combo.SelectedValue == FeeType.Dealer.ToString())
                    feeType = FeeType.Dealer;
                else if (combo.SelectedValue == FeeType.InvestmentMgr.ToString())
                    feeType = FeeType.InvestmentMgr;
                else if (combo.SelectedValue == FeeType.MDAOperator.ToString())
                    feeType = FeeType.MDAOperator;
                else if (combo.SelectedValue == FeeType.Platform.ToString())
                    feeType = FeeType.Platform;
                else if (combo.SelectedValue == FeeType.None.ToString())
                    feeType = FeeType.None;

                feeRow[UMAFeesDS.FEETYPEENUM] = feeType;

                ServiceTypes serviceType = ServiceTypes.None;

                if (servicesCombo.SelectedValue == ServiceTypes.All.ToString())
                    serviceType = ServiceTypes.All;
                else if (servicesCombo.SelectedValue == ServiceTypes.Custom.ToString())
                    serviceType = ServiceTypes.Custom;
                else if (servicesCombo.SelectedValue == ServiceTypes.DoItForMe.ToString())
                    serviceType = ServiceTypes.DoItForMe;
                else if (servicesCombo.SelectedValue == ServiceTypes.DoItWithMe.ToString())
                    serviceType = ServiceTypes.DoItWithMe;
                else if (servicesCombo.SelectedValue == ServiceTypes.DoItYourSelf.ToString())
                    serviceType = ServiceTypes.DoItYourSelf;
                else if (servicesCombo.SelectedValue == ServiceTypes.ManualAsset.ToString())
                    serviceType = ServiceTypes.ManualAsset;
                else if (servicesCombo.SelectedValue == ServiceTypes.None.ToString())
                    serviceType = ServiceTypes.None;

                feeRow[UMAFeesDS.SERVICESTYPEENUM] = serviceType;

                feeRow[UMAFeesDS.COMMENTS] = ((TextBox)(((GridEditableItem)(e.Item))["COMMENTS"].Controls[0])).Text;
                feeRow[UMAFeesDS.DESCRIPTION] = ((TextBox)(((GridEditableItem)(e.Item))["DESCRIPTION"].Controls[0])).Text;
                if (((RadDatePicker)(((GridEditableItem)(e.Item))["STARTDATE"].Controls[0])).SelectedDate.Value != null)
                    feeRow[UMAFeesDS.STARTDATE] = ((RadDatePicker)(((GridEditableItem)(e.Item))["STARTDATE"].Controls[0])).SelectedDate.Value;

                if (((RadDatePicker)(((GridEditableItem)(e.Item))["ENDDATE"].Controls[0])).SelectedDate.Value != null)
                    feeRow[UMAFeesDS.ENDDATE] = ((RadDatePicker)(((GridEditableItem)(e.Item))["ENDDATE"].Controls[0])).SelectedDate.Value;

                feeTable.Rows.Add(feeRow);

                iorg.SetData(umaFeesDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                this.GetBMCData();
                this.PresentationGrid.Rebind();
            }
        }
        protected void PresentationGrid_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e){}
        protected void PresentationGrid_ItemDeleted(object source, GridDeletedEventArgs e){}
        protected void PresentationGrid_ItemInserted(object source, GridInsertedEventArgs e){}
        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("Fees".Equals(e.Item.OwnerTableView.Name))
            {
                GridEditFormInsertItem EditForm = (GridEditFormInsertItem)e.Item;
                
                Hashtable values = new Hashtable();
                EditForm.ExtractValues(values);

                UMABroker.SaveOverride = true;
                
                RadComboBox combo = (RadComboBox)EditForm["ServiceTypeEditor"].Controls[0];

                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDS umaFeesDS = new UMAFeesDS();
                umaFeesDS.DataSetOperationType = DataSetOperationType.NewSingle;
                FeeEntity entityToAdd = new FeeEntity();

                if (combo.SelectedValue == FeeType.AccountKeeping.ToString())
                    entityToAdd.FeeType = FeeType.AccountKeeping;
                else if (combo.SelectedValue == FeeType.Adviser.ToString())
                    entityToAdd.FeeType = FeeType.Adviser;
                else if (combo.SelectedValue == FeeType.AssetMgt.ToString())
                    entityToAdd.FeeType = FeeType.AssetMgt;
                else if (combo.SelectedValue == FeeType.Dealer.ToString())
                    entityToAdd.FeeType = FeeType.Dealer;
                else if (combo.SelectedValue == FeeType.InvestmentMgr.ToString())
                    entityToAdd.FeeType = FeeType.InvestmentMgr;
                else if (combo.SelectedValue == FeeType.MDAOperator.ToString())
                    entityToAdd.FeeType = FeeType.MDAOperator;
                else if (combo.SelectedValue == FeeType.None.ToString())
                    entityToAdd.FeeType = FeeType.None;

                RadComboBox servicesCombo = (RadComboBox)EditForm["ModelServiceTypeEditor"].Controls[0];

                if (servicesCombo.SelectedValue == ServiceTypes.All.ToString())
                    entityToAdd.ServiceTypes = ServiceTypes.All;
                else if (servicesCombo.SelectedValue == ServiceTypes.Custom.ToString())
                    entityToAdd.ServiceTypes = ServiceTypes.Custom;
                else if (servicesCombo.SelectedValue == ServiceTypes.DoItForMe.ToString())
                    entityToAdd.ServiceTypes = ServiceTypes.DoItForMe;
                else if (servicesCombo.SelectedValue == ServiceTypes.DoItWithMe.ToString())
                    entityToAdd.ServiceTypes = ServiceTypes.DoItWithMe;
                else if (servicesCombo.SelectedValue == ServiceTypes.DoItYourSelf.ToString())
                    entityToAdd.ServiceTypes = ServiceTypes.DoItYourSelf;
                else if (servicesCombo.SelectedValue == ServiceTypes.ManualAsset.ToString())
                    entityToAdd.ServiceTypes = ServiceTypes.ManualAsset;
                else if (servicesCombo.SelectedValue == ServiceTypes.None.ToString())
                    entityToAdd.ServiceTypes = ServiceTypes.None;

                entityToAdd.Comments = ((TextBox)(((GridEditableItem)(e.Item))["COMMENTS"].Controls[0])).Text;
                entityToAdd.Description = ((TextBox)(((GridEditableItem)(e.Item))["DESCRIPTION"].Controls[0])).Text;

                if (((RadDatePicker)(((GridEditableItem)(e.Item))["STARTDATE"].Controls[0])).SelectedDate.Value != null)
                    entityToAdd.StartDate = ((RadDatePicker)(((GridEditableItem)(e.Item))["STARTDATE"].Controls[0])).SelectedDate.Value;

                if (((RadDatePicker)(((GridEditableItem)(e.Item))["ENDDATE"].Controls[0])).SelectedDate.Value != null)
                    entityToAdd.EndDate = ((RadDatePicker)(((GridEditableItem)(e.Item))["ENDDATE"].Controls[0])).SelectedDate.Value;

                umaFeesDS.FeeEntityToAddUpdate = entityToAdd;

                iorg.SetData(umaFeesDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                this.GetBMCData();
                this.PresentationGrid.Rebind();
            }
        }
        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e){}
        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;
                GridDropDownListColumnEditor editor = null;

                if ("Fees".Equals(e.Item.OwnerTableView.Name))
                {
                    IDictionary<Enum, string> feeServicesValue = Enumeration.ToDictionary(typeof(FeeType));
                    editor = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("ServiceTypeEditor"));
    
                    string savedValueFeeType = String.Empty;
                    string savedValueServiceType = String.Empty;

                    if (((Telerik.Web.UI.GridEditFormItem)(e.Item)).ParentItem != null)
                    {
                        savedValueFeeType = ((Telerik.Web.UI.GridEditFormItem)(e.Item)).ParentItem["FEETYPE"].Text;
                        savedValueServiceType = ((Telerik.Web.UI.GridEditFormItem)(e.Item)).ParentItem["SERVICESTYPE"].Text;
                    }
                    
                    editor.DataSource = feeServicesValue;
                    editor.DataValueField = "Key";
                    editor.DataTextField = "Value";
                    editor.DataBind();

                    if(savedValueFeeType != string.Empty)   
                        editor.ComboBoxControl.Items.FindItemByText(savedValueFeeType).Selected = true;
                   
                    IDictionary<Enum, string> servicesValue = Enumeration.ToDictionary(typeof(ServiceTypes));
                    editor = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("ModelServiceTypeEditor"));
                   
                    editor.DataSource = servicesValue;
                    editor.DataValueField = "Key";
                    editor.DataTextField = "Value";
                    editor.DataBind();

                    if (savedValueServiceType != string.Empty)   
                        editor.ComboBoxControl.Items.FindItemByText(savedValueServiceType).Selected = true;
                }
            }
        }
        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DataView View = new DataView(this.PresentationData.Tables[UMAFeesDS.FEELISTABLE]);
            View.Sort = UMAFeesDS.DESCRIPTION + " ASC";
            this.PresentationGrid.DataSource = View.ToTable();
        }
    }
}
