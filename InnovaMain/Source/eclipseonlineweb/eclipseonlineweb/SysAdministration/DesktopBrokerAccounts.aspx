﻿<%@ Page Title="" Language="C#" MasterPageFile="AdminMaster.master" AutoEventWireup="true"
    CodeBehind="DesktopBrokerAccounts.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.DesktopBrokerAccounts" %>

<%@ Register Src="~/Controls/DesktopBrokerAccountControl.ascx" TagName="DesktopBrokerAccountControl"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" runat="server">
    <style>
        .holder
        {
            width: 100%;
            display: block;
            z-index: 6;
        }
        .content
        {
            background: #fff;
            z-index: 7; /*  padding: 28px 26px 33px 25px;*/
        }
        .popup
        {
            border-radius: 7px;
            background: #6b6a63;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 1000px;
            top: 20%;
            left: 50%;
            margin-left: -400px;
            margin-top: -40px;
            z-index: 6;
        }
        
        .popup1
        {
            border-radius: 7px;
            background: #6b6a63;
            margin: 30px auto 0;
            padding: 6px;
            position: absolute;
            width: 840px;
            top: 20%;
            left: 50%;
            margin-left: -400px;
            margin-top: -40px;
            z-index: 6;
        }
        .overlay
        {
            width: 100%;
            opacity: 0.65;
            height: 100%;
            left: 0; /*IE*/
            top: 0;
            text-align: center;
            z-index: 5;
            position: fixed;
            background-color: #444444;
        }
        
        .wijmo-wijgrid .wijmo-wijgrid-groupheaderrow td
        {
            background-color: #DAE6F4;
            color: #000000;
            font-size: smaller;
            font-weight: bold;
            vertical-align: middle;
        }
        .wijmo-wijgrid .wijmo-wijgrid-footerrow td
        {
            font-weight: bolder;
            text-align: right;
            font-size: smaller;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function cancel() {
            window.close();
        }
    </script>
   
    <fieldset>
        <table width="100%">
            <tr>
             <td width="20%">
                        <telerik:RadButton runat="server" ID="btnDownload" ToolTip="Download" OnClick="btnDownload_Onclick"
                            Width="100PX">
                            <ContentTemplate>
                                <img src="../images/download.png" alt="" class="btnImageWithText" />
                                <span class="riLabel">Download</span>
                            </ContentTemplate>
                        </telerik:RadButton>
                    </td>
                <td style="width: 80%;" class="breadcrumbgap">
                    <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
        PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
        GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True" Width="100%"
        AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource"
        OnItemCommand="PresentationGrid_OnItemCommand">
        <PagerStyle Mode="NumericPages"></PagerStyle>
          <%--  <ClientSettings>
                <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="false" FrozenColumnsCount="1"></Scrolling>
            </ClientSettings>--%>
        <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
            Name="Banks" TableLayout="Fixed">
            <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
            <HeaderStyle Font-Bold="True"></HeaderStyle>
         
            <Columns>
            <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="ClientCID" 
                    SortExpression="ClientCode" UniqueName="ClientCode" DataNavigateUrlFormatString="../ClientViews/ClientMainView.aspx?ins={0}"
                    HeaderText="Client ID" DataTextField="ClientCode" AutoPostBackOnFilter="false"
                    CurrentFilterFunction="Contains" FilterControlWidth="75px" HeaderStyle-Width="120px"
                    HeaderButtonType="TextButton" ShowFilterIcon="true">
                </telerik:GridHyperLinkColumn>
                <telerik:GridBoundColumn FilterControlWidth="200px" HeaderStyle-Width="300px" SortExpression="ClientName"
                    ReadOnly="true" HeaderText="Client Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ClientName" UniqueName="ClientName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="ClientCID" ReadOnly="true" HeaderText="ClientCID"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="ClientCID" UniqueName="ClientCID" Display="false">
                </telerik:GridBoundColumn>
             <telerik:GridBoundColumn SortExpression="CID" ReadOnly="true" HeaderText="CID" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="CID" UniqueName="CID" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="CLID" ReadOnly="true" HeaderText="CLID"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="CLID" UniqueName="CLID" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="CSID" ReadOnly="true" HeaderText="CSID"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="CSID" UniqueName="CSID" Display="false">
                </telerik:GridBoundColumn>
                
                <telerik:GridBoundColumn FilterControlWidth="100px" HeaderStyle-Width="150px" SortExpression="ServiceType"
                    ReadOnly="true" HeaderText="Service Types" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ServiceType" UniqueName="ServiceType">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="200px" HeaderStyle-Width="300px" SortExpression="AccountName"
                    ReadOnly="true" HeaderText="Account Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountName" UniqueName="AccountName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderStyle-Width="120px" ReadOnly="true" SortExpression="AccountNo"
                    HeaderText="Account Number" AutoPostBackOnFilter="true" ShowFilterIcon="true"
                    CurrentFilterFunction="Contains" HeaderButtonType="TextButton" DataField="AccountNo"
                    UniqueName="AccountNo">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="160px" HeaderStyle-Width="200px" SortExpression="AccountDesignation"
                    HeaderText="Account Designation" ReadOnly="true" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="AccountDesignation" UniqueName="AccountDesignation">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="120px" SortExpression="HINNumber"
                    ReadOnly="true" HeaderText="HIN Number" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="HINNumber" UniqueName="HINNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="120px" SortExpression="UnitsHeld"
                    ReadOnly="true" HeaderText="Units Held" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UnitsHeld" UniqueName="UnitsHeld">
                </telerik:GridBoundColumn>
                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit"
                    UniqueName="EditColumn">
                    <HeaderStyle Width="60px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn ConfirmText="Are you sure you want to deassociate this account from client?"
                    ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                    <HeaderStyle  Width="60px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                </telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
    <asp:Button ID="btnCancel" runat="server" Style="display: none" />
    <asp:Button ID="btnOkay" runat="server" Style="display: none" />
    <div id="ASXModal" runat="server" visible="false" class="holder">
        <div class="popup">
            <div class="content">
                <uc1:DesktopBrokerAccountControl ID="ASXControl" runat="server" />
            </div>
        </div>
    </div>
    <div class="overlay" id="OVER" visible="False" runat="server">
    </div>
    <asp:HiddenField ID="SelectedClientCID" runat="server" />
   
</asp:Content>
