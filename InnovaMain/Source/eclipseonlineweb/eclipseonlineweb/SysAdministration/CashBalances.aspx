﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="CashBalances.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.CashBalances" %>

<%@ Register TagPrefix="uc1" TagName="breadcrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server">
                <fieldset>
                    <table width="100%">
                        <tr>
                            <td width="4%">
                                &nbsp;
                            </td>
                            <td width="100%" class="breadcrumbgap">
                                <uc1:breadcrumb ID="BreadCrumb1" runat="server" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </asp:Panel>
            <br />
            <telerik:RadGrid OnNeedDataSource="CashBalancesGrid_OnNeedDataSource" ID="CashBalancesGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" GridLines="None"
                AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnItemDataBound="CashBalancesGrid_OnItemDataBound"
                OnItemCommand="CashBalancesGrid_OnItemCommand">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="None"
                    Name="CashBalances" TableLayout="Fixed" EditMode="EditForms">
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="ID" HeaderText="ID" ReadOnly="true" Display="false"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="ID" UniqueName="ID">
                        </telerik:GridBoundColumn>
                        <telerik:GridEditCommandColumn HeaderStyle-Width="30px" ButtonType="ImageButton"
                            UniqueName="EditCommandColumn2">
                            <ItemStyle CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridEditCommandColumn>
                        <telerik:GridBoundColumn SortExpression="SuperAccountType" HeaderText="Account Type"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" ReadOnly="true" DataField="SuperAccountType" UniqueName="SuperAccountType">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn HeaderText="Account Type" SortExpression="SuperAccountType"
                            UniqueName="SuperAccountTypeEditor" DataField="SuperAccountType" Visible="false"
                            ColumnEditorID="GridDropDownColumnEditorModelSuperAccountTypeList" />
                        <telerik:GridNumericColumn SortExpression="Percentage" HeaderText="Percentage" AutoPostBackOnFilter="False"
                            CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Percentage" UniqueName="Percentage" DataFormatString="{0:P}" NumericType="Percent">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn SortExpression="MinValue" HeaderText="Min Value" AutoPostBackOnFilter="False"
                            CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="MinValue" UniqueName="MinValue" DataFormatString="{0:C}" NumericType="Currency">
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn SortExpression="MaxValue" HeaderText="Max Value" AutoPostBackOnFilter="False"
                            CurrentFilterFunction="EqualTo" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="MaxValue" UniqueName="MaxValue" DataFormatString="{0:C}" NumericType="Currency">
                        </telerik:GridNumericColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorModelSuperAccountTypeList"
                runat="server" DropDownStyle-Width="200px" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
