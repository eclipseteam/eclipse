﻿<%@ Page Title="" Language="C#" MasterPageFile="AdminMaster.master" AutoEventWireup="true"
    CodeBehind="ImportSMAClientDataFeed.aspx.cs" Inherits="eclipseonlineweb.ImportSMAClientDataFeed"
    EnableViewState="false" %>

<%@ Register TagPrefix="uc" TagName="Details" Src="WebControls/ImportMessageControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Import Super Data Feed"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <table width="100%">
                <tr>
                    <td>
                        <fieldset>
                            <asp:HiddenField runat="server" ID="txtIsFile" />
                            <legend>Import Super Data</legend>
                            <table>
                                <tr>
                                    <td>
                                        <telerik:RadButton runat="server" ID="btnImportAllNewAccount" Text="NEW ACCOUNTS & TRANSACTIONS" OnClick="btnImportFull_OnClick" Width="250px">
                                        </telerik:RadButton>
                                    </td>
                                    <td>
                                        <span class="riLabel">
                                            <p>
                                                Import all new accounts from Super with transaction data. Please note this action will only add new account with transactions. Any existing account will not be updated</p>
                                        </span>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <telerik:RadButton runat="server" ID="btnImportAllNewOneAccount" Text="NEW ACCOUNT" OnClick="btnImportFullOneAccount_OnClick" Width="250px">
                                        </telerik:RadButton>
                                        <telerik:RadTextBox runat="server" ID="txtNewAccount" Width="100px"></telerik:RadTextBox>
                                    </td>
                                    <td>
                                        <span class="riLabel">
                                            <p>
                                                Import all new accounts from Super with transaction data. Please note this action will only add new account with transactions. Any existing account will not be updated</p>
                                        </span>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <telerik:RadButton runat="server" ID="btnImportLastFiveDaysTransactions" Text="EXISTING ACCOUNTS TRANSACTIONS" OnClick="btnImportLastFiveDays_OnClick"  Width="250px">
                                        </telerik:RadButton>
                                    </td>
                                    <td>
                                        <span class="riLabel">
                                            <p>
                                                Import all last 5 days MIS transactions for all existing Super account AND Since Inception all Bank transactions. This will also attempt to update account and contact details. Please note this service will not add any new account.</p>
                                        </span>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <telerik:RadButton runat="server" ID="btnRefreshTransactionsMISData" Text="FIX & REFRESH MIS TRANSACTIONS" OnClick="btnRefreshTransactionsMISData_OnClick"  Width="250px">
                                        </telerik:RadButton>
                                    </td>
                                    <td>
                                        <span class="riLabel">
                                            <p>
                                                This is to refresh and fix MIS Fund data across the system</p>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadButton runat="server" ID="btnDownloadListOfAdvisers" Text="DOWNLOAD ADVISER LIST" OnClick="btnDownloadListOfAdvisers_OnClick"  Width="250px">
                                        </telerik:RadButton>
                                    </td>
                                    <td>
                                        <span class="riLabel">
                                            <p>
                                                Download List of Advisers</p>
                                        </span>
                                    </td>
                                </tr>

                                   <tr>
                                    <td>
                                        <telerik:RadButton runat="server" ID="RadButton1" Text="MAP ADVISERS TO CLIENT" OnClick="btnMapAdvisers_OnClick"  Width="250px">
                                        </telerik:RadButton>
                                    </td>
                                    <td>
                                        <span class="riLabel">
                                            <p>
                                                Map Advisers to existing E- Clipse Super clients</p>
                                        </span>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <telerik:RadButton runat="server" ID="btnUpdateAccountStatus" Text="UPDATE ACCOUNT STATUS" OnClick="btnUpdateAccountStatus_OnClick"  Width="250px">
                                        </telerik:RadButton>
                                    </td>
                                    <td>
                                        <span class="riLabel">
                                            <p>
                                                Update existing e-Clipse Super Account Status and remove old accounts</p>
                                        </span>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <telerik:RadButton runat="server" ID="RadButton3" Text="DELETE CLOSED ACCOUNTS" OnClick="btnDeleteClosedAccounts_OnClick"  Width="250px">
                                        </telerik:RadButton>
                                    </td>
                                    <td>
                                        <span class="riLabel">
                                            <p>
                                                Delete existing closed e-Clipse Super Account</p>
                                        </span>
                                    </td>
                                </tr>
                                
                                  <tr>
                                    <td>
                                        <telerik:RadButton runat="server" ID="RadButton12" Text="ADD ASX & FIIG" OnClick="btnUpdateASXFIIG_OnClick"  Width="250px">
                                        </telerik:RadButton>
                                    </td>
                                    <td>
                                        <span class="riLabel">
                                            <p>
                                                Add FiiG and ASX for existing accounts</p>
                                        </span>
                                    </td>
                                </tr>
                                                                  <tr>
                                    <td>
                                        <telerik:RadButton runat="server" ID="RadButton2" Text="Update DIFM & DIWM Portfolio" OnClick="btnApplyModelUpdateAcrossEclipseSuperClient"  Width="250px">
                                        </telerik:RadButton>
                                    </td>
                                    <td>
                                        <span class="riLabel">
                                            <p>
                                                Update DIFM & DIWM Portfolio</p>
                                        </span>
                                    </td>
                                </tr>

                                      <tr>
                                    <td>
                                        <telerik:RadButton runat="server" ID="RadButton4" Text="Update Set Other ID" OnClick="btnUpdateOrderID_OnClick"  Width="250px">
                                        </telerik:RadButton>
                                    </td>
                                    <td>
                                        <span class="riLabel">
                                            <p>
                                                Update Set Other ID</p>
                                        </span>
                                    </td>
                                </tr>

                            </table>
                            
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel runat="server" ID="PnlResults">
                            <uc:Details ID="ImportMessageControl" runat="server" />
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
