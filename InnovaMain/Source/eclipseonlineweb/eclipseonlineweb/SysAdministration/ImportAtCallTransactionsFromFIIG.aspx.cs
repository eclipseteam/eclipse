﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.UI;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using System.Linq;
using eclipseonlineweb.WebUtilities;
using eclipseonlineweb.WebControls;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization.Data;
using System.Configuration;



namespace eclipseonlineweb
{
    public partial class ImportAtCallTransactionsFromFIIG : UMABasePage
    {

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        private static ImportProcessDS importDS;
        private static List<string> Sheets;
        private static ImportProcessDS FinalImportDS;
        protected override void Intialise()
        {
            ImportMessageControl.GridImportMessages.ColumnCreated += GridImportMessages_ColumnCreated;
            ImportMessageControl.GirdInvestmentCodeError.ColumnCreated += GridImportErrorMessages_ColumnCreated;
            ImportMessageControl.GridImportMessages.ItemDataBound += GridImportMessages_ItemDataBound;
            if (!IsPostBack)
            {
                ImportMessageControl.Clear();
            }

            ImportMessageControl.Completed += text =>
                                                  {
                                                      switch (text.ToLower())
                                                      {
                                                          case "next":
                                                              SendData();
                                                              break;
                                                          default:
                                                              ClearMessageControls();
                                                              break;
                                                      }
                                                  };

           
        }

        private void FillSheetNameDropDown()
        {
            ddlSheetName.DataSource = Sheets;
            ddlSheetName.DataBind();
        }
        void GridImportMessages_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if((e.Item).ItemIndex!=-1)
            {
              DataRow dr= ((DataRowView) ((e.Item).DataItem)).Row;
                if(bool.Parse(dr["HasErrors"].ToString()))
                {
                    e.Item.ForeColor = Color.Red;
                }
            }

        }

        public override void LoadPage()
        {
            if (!Page.IsPostBack)
            {
                importDS = new ImportProcessDS();
                Sheets = new List<string>();
                FinalImportDS = new ImportProcessDS();
            }
            GetBMCData();
            LoadControls();
            FillSheetNameDropDown();
        }

        private void LoadControls()
        {
            var org = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            if (org.Institution != null)
            {
                var institutes = org.Institution.Where(ss => ss.ID == new Guid("a555cab5-9c7f-4be9-bdcd-2f8e04bf4537") || ss.ID == new Guid("3048b160-14e0-43c6-ba43-eca33dac267f"));
                cmbBroker.DataSource = institutes;
                cmbBroker.DataTextField = "Name";
                cmbBroker.DataValueField = "ID";
                cmbBroker.DataBind();
                cmbBroker.SelectedValue = "a555cab5-9c7f-4be9-bdcd-2f8e04bf4537";
            }
        }
        void GridImportMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {

            SetDataGridColumns(e);
        }

        void GridImportErrorMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            SetDataGridColumns(e);
        }

        private bool Validation()
        {
            bool isNotValid = false;
            if (cmbInstitution.SelectedValue == Guid.Empty.ToString())
            {
                lblMessage.Text += "Select Institution, ";
                isNotValid = true;
            }
            if(string.IsNullOrEmpty(dtPicker.SelectedDate.ToString()))
            {
                lblMessage.Text += "Select Transaction Date, ";
                isNotValid = true;
            }
            if (string.IsNullOrEmpty(cmbImportTransactionType.SelectedValue.ToString()) || cmbImportTransactionType.SelectedValue == "-1")
            {
                lblMessage.Text += "Select Import Transaction Type, ";
                isNotValid = true;
            }
            if (string.IsNullOrEmpty(cmbSystemTransType.SelectedItem.Text))
            {
                lblMessage.Text += "Select System Transaction Type, ";
                isNotValid = true;
            }
            if (string.IsNullOrEmpty(cmbBroker.SelectedValue.ToString()) || cmbBroker.SelectedValue == "-1")
            {
                lblMessage.Text += "Select Broker, ";
                isNotValid = true;
            }
            return isNotValid;
        }

        public void BtnUploadClick(object sender, EventArgs e)
        {

            if (Validation())
            {
                return;
            }
            else
            {
                    lblMessage.Text = "";
            }
            
            // Check to see if file was uploaded
            if (filMyFile.PostedFile != null)
            {
                // Get a reference to PostedFile object
                HttpPostedFile myFile = filMyFile.PostedFile;

                // Get size of uploaded file
                int nFileLen = myFile.ContentLength;


                // make sure the size of the file is > 0
                if (nFileLen > 0)
                {
                    // Allocate a buffer for reading of the file
                    string fileName = Path.GetFileName(myFile.FileName);
                    hf_FileName.Value = fileName;
                    var date = DateTime.Now;
                    var distributionDictory = Server.MapPath("~/App_Data/Import/");
                    if (!Directory.Exists(distributionDictory))
                    {
                        Directory.CreateDirectory(distributionDictory);
                    }
                    GetNextFilename(distributionDictory + fileName);
                    string fileLocation = distributionDictory + date.ToString("ddMMyyhhmmsstt") + fileName;
                    byte[] myData = new byte[nFileLen];

                    // Read uploaded file from the Stream
                    myFile.InputStream.Read(myData, 0, nFileLen);

                    // Write data into a file
                    Utilities.WriteToFile(fileLocation, ref myData);
                    DataSet dss = ExcelReader.ReadExeclAllSheets(fileLocation);

                    List<string> sheetNames = new List<string>();
                    sheetNames.Add("All");
                    for (int i = dss.Tables.Count - 1; i >= 0;i-- )
                    {
                        if (dss.Tables[i].TableName.Contains("_xlnm#"))
                            dss.Tables.Remove(dss.Tables[i]);
                        else
                            sheetNames.Add(dss.Tables[i].TableName);
                    }
                    Sheets = sheetNames;
                    FillSheetNameDropDown();
                    ImportProcessDS ds = new ImportProcessDS();
                    DataTable dt_Main = CreateTableForAtCallTransactionsImport();
                    //string monthName = string.Empty;
                    //string year = string.Empty;                    
                    foreach (DataTable dt in dss.Tables)
                    {
                       //foreach (DataColumn col in dt.Columns)
                       // {
                       // //    if(col.ColumnName.ToLower().Contains("jan"))
                       // //    {monthName = "Jan";break;}
                       // //    else if (col.ColumnName.ToLower().Contains("feb"))
                       // //    { monthName = "Feb"; break; }
                       // //    else if (col.ColumnName.ToLower().Contains("mar"))
                       // //    { monthName = "March"; break; }
                       // //    else if (col.ColumnName.ToLower().Contains("apr"))
                       // //    { monthName = "April"; break; }
                       // //    else if (col.ColumnName.ToLower().Contains("may"))
                       // //    { monthName = "May"; break; }
                       // //    else if (col.ColumnName.ToLower().Contains("jun"))
                       // //    { monthName = "June"; break; }
                       // //    else if (col.ColumnName.ToLower().Contains("jul"))
                       // //    { monthName = "July"; break; }
                       // //    else if (col.ColumnName.ToLower().Contains("aug"))
                       // //    { monthName = "Aug"; break; }
                       // //    else if (col.ColumnName.ToLower().Contains("sep"))
                       // //    { monthName = "Sep"; break; }
                       // //    else if (col.ColumnName.ToLower().Contains("oct"))
                       // //    { monthName = "Oct"; break; }
                       // //    else if (col.ColumnName.ToLower().Contains("nov"))
                       // //    { monthName = "Nov"; break; }
                       // //    else if (col.ColumnName.ToLower().Contains("dec"))
                       // //    { monthName = "Dec"; break; }
                       // //    else
                       // //        monthName = string.Empty;                            
                       // }
                        //year = dt.TableName.ToString();
                        //if (year.Contains(" "))
                        //{
                        //    year = year.Substring(year.IndexOf(" "));
                        //}                       
                        foreach (DataRow dr1 in dt.Rows)
                        {
                            DataRow dr = dt_Main.NewRow();
                            dr["ClientName"] = dr1[0];
                            dr["CustomerNumber"] = dr1[1].ToString().Replace(" ", "");
                            dr["AccNumber"] = dr1[2].ToString().Replace(" ", "");
                            //dr["Month"] = DateTime.Parse(monthName + " " + "01" + ", " + year);   
                            dr["Month"] = dtPicker.SelectedDate;
                            dr["SheetName"] = dt.TableName;
                            dr["Balance"] = dr1[3];
                            dr["BonusInterest"] = dr1[4];
                            dr["Interest"] = dr1[5];
                            dr["Amount"] = dr1[7];
                            dt_Main.Rows.Add(dr);
                        }
                        
                    }
                    DataView dv = new DataView(dt_Main);
                    dv.Sort = "Month ASC";
                    ds.Tables.Add(dv.ToTable());
                    DataTable dt_AtCallHolding = CreateAtCallHoldingTable();
                    ds.Tables.Add(dt_AtCallHolding);
                    ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit
                    {                                           
                        CurrentUser = GetCurrentUser()
                    };
                    ds.Command = (int)WebCommands.ValidatieAtCallTransactionsImportFiles;
                    ds.FileName = hf_FileName.Value;
                    SaveOrganizanition(ds);
                    SetValidationGird(ds);
                    PresentationData = ds;
                    importDS = ds;
                    FinalImportDS = ds;
                    string filename = ImportMessageControl.RefreshFileID() + ".xml";
                    ds.WriteXml(distributionDictory + filename, XmlWriteMode.WriteSchema);
                }
            }
        }        
        public static string GetNextFilename(string filename)
        {
            int i = 1;
            string dir = Path.GetDirectoryName(filename);
            string file = Path.GetFileNameWithoutExtension(filename) + "{0}";
            string extension = Path.GetExtension(filename);

            while (File.Exists(filename))
                filename = Path.Combine(dir, string.Format(file, "(" + i++ + ")") + extension);

            return filename;
        }

        private void SetValidationGird(ImportProcessDS ds)
        {
            //ImportMessageControl.Clear();
            string tableName = "AtCallTransactions";
            using (DataView dv = new DataView(ds.Tables[tableName]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }
            using (DataView dv = new DataView(ds.Tables[tableName]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    DataRow dr = missingItems.NewRow();
                    dr["MissingItem"] = row["ClientName"];
                    missingItems.Rows.Add(dr);
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();

                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingClientAccountsIgnored;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }
            int successfulRows = ds.Tables[tableName].Select("HasErrors='false'").Length;
            if (ds.Tables[tableName].Rows.Count == 0 || successfulRows == 0)
            {
                ImportMessageControl.SetButtons(ButtonTypes.CLOSE);
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sErrorsCannotContinue, successfulRows, ds.Tables[tableName].Rows.Count - successfulRows), ImportMessageType.Error);
                ImportMessageControl.TextMessage.Text = "";
            }
            else
            {
                if (ds.ExtendedProperties.Contains("Result"))
                {
                    ImportMessageControl.SetButtons(ButtonTypes.CLOSE);
                    ImportMessageControl.SetTextMessage(string.Format(ds.ExtendedProperties["Message"].ToString(), successfulRows, ds.Tables[tableName].Rows.Count - successfulRows), ImportMessageType.Error);
                    ImportMessageControl.TextMessage.Text = "";
                }
                else
                {
                    ImportMessageControl.SetButtons(ButtonTypes.NEXTCANCEL);
                    //success
                    ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sValidationCompleted, successfulRows, ds.Tables[tableName].Rows.Count - successfulRows), ImportMessageType.Sucess);
                }
            }
        }

        private void SetCompletionGird(ImportProcessDS ds)
        {
            string tableName = "AtCallTransactions";
            ImportMessageControl.Clear();
            using (DataView dv = new DataView(ds.Tables[tableName]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }

            using (DataView dv = new DataView(ds.Tables[tableName]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    DataRow dr = missingItems.NewRow();
                    dr["MissingItem"] = row["CustomerNumber"];
                    missingItems.Rows.Add(dr);
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();

                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingClientAccountsIgnored;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }
            int successfulRows = ds.Tables[tableName].Select("HasErrors='false'").Length;
            ImportMessageControl.SetButtons(ButtonTypes.FINISH);
            ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sImportedSuccessfully, successfulRows, ds.Tables[tableName].Rows.Count - successfulRows), ImportMessageType.Sucess);
        }

        private void SendData()
        {
            var distributionDictory = Server.MapPath("~/App_Data/Import/");
            if (!Directory.Exists(distributionDictory))
            {
                Directory.CreateDirectory(distributionDictory);
            }

            //string filename = distributionDictory + ImportMessageControl.GetFileID() + ".xml";
            ImportProcessDS ds = new ImportProcessDS();
            ds = FinalImportDS;
            //ds.ReadXml(filename);
            DataTable dt = ds.Tables["AtCallTransactions"];
            DataColumn dc1 = new DataColumn("InstituteID", typeof(string));
            DataColumn dc2 = new DataColumn("TranDate", typeof(DateTime));
            DataColumn dc3 = new DataColumn("SysTranType", typeof(string));
            DataColumn dc4 = new DataColumn("ImportTranType", typeof(string));
            DataColumn dc5 = new DataColumn("Broker", typeof(string));
            dc1.DefaultValue = cmbInstitution.SelectedValue;
            dc2.DefaultValue = dtPicker.SelectedDate;
            dc3.DefaultValue = cmbSystemTransType.Text;
            dc4.DefaultValue = cmbImportTransactionType.SelectedValue;
            dc5.DefaultValue = cmbBroker.SelectedValue;
            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);
            dt.Columns.Add(dc3);
            dt.Columns.Add(dc4);
            dt.Columns.Add(dc5);
            
            ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit
                          {
                              Name = "Order " + DateTime.Now.ToString("dd MMM, yyyy hh:mm:ss"),
                              Type = ((int)OrganizationType.Order).ToString(),
                              CurrentUser = GetCurrentUser()
                          };
            ds.Command = (int)WebCommands.ImportAtCallTransactionsImportFiles;
            ds.FileName = hf_FileName.Value;           
            SaveOrganizanition(ds);
            SetCompletionGird(ds);

            string HodlingID = ImportMessageControl.RefreshFileID();
            string HoldingFileName = HodlingID + ".xml";
            ds.WriteXml(distributionDictory + HoldingFileName, XmlWriteMode.WriteSchema);
            SaveReconciliationReport(ds);
            ImportMessageControl.SetImportSuccessFileMsg("File is imported successfully. You can see At Call Holding Vs Transactions report from", "~/SysAdministration/AtCallHoldingVSTransactions.aspx?ID=" + HodlingID);
        }

        private void SaveReconciliationReport(ImportProcessDS ds)
        {
            DataTable dtAtCallHoldings = ds.Tables["AtCallHoldings"];
            DataTable datatable = dtAtCallHoldings.Copy();
            if (datatable.Columns.Contains("ClientCID"))
            {
                datatable.Columns.Remove("ClientCID");
            }
            if (datatable.Columns.Contains("CID"))
            {
                datatable.Columns.Remove("CID");
            }
            if (datatable.Columns.Contains("CLID"))
            {
                datatable.Columns.Remove("CLID");
            }
            if (datatable.Columns.Contains("CSID"))
            {
                datatable.Columns.Remove("CSID");
            }           
            // Open the template workbook, which contains number formats and 
            // formulas, and get an IRange from a defined name
            string path = "~/Templates/AtCallHoldings.xls";
            SpreadsheetGear.IWorkbook workbook = SpreadsheetGear.Factory.GetWorkbook(HttpContext.Current.Server.MapPath(path));
            SpreadsheetGear.IRange range = workbook.Names["AtCallHoldingVsTransaction"].RefersToRange;
            // Insert the DataTable into the template worksheet range. The InsertCells
            // flag will cause the formatted range to be adjusted for the inserted data.
            range.CopyFromDataTable(datatable, SpreadsheetGear.Data.SetDataFlags.NoColumnHeaders);
            // Save to disk. 
            string DiskPath = Server.MapPath("~\\" + ConfigurationManager.AppSettings["SaveAtCallReconciliation"]);
            bool IsExists = System.IO.Directory.Exists(DiskPath);
            if (!IsExists)
                System.IO.Directory.CreateDirectory(DiskPath);
            DiskPath = DiskPath + "AtCallHoldingsVsTransactions_" + DateTime.Now.ToString("yyyy_MM_dd") + "_" + DateTime.Now.ToString("hhmmssFFF") + ".xls";
            workbook.SaveAs(DiskPath, SpreadsheetGear.FileFormat.Excel8);
        }

        public void ClearMessageControls()
        {
            ImportMessageControl.Clear();
            ImportMessageControl.TextMessage.Text = "";
        }

        private static void SetDataGridColumns(GridColumnCreatedEventArgs e)
        {
            string colName = e.Column.UniqueName.ToLower();
            switch (colName)
            {
                case "clientname":
                    e.Column.HeaderText = "Client Name";
                    break;
                case "customernumber":
                    e.Column.HeaderText = "Customer Number";
                    e.Column.FilterControlWidth = Unit.Pixel(50);
                    break;
                case "accnumber":
                    e.Column.HeaderText = "Acc Number";
                    e.Column.FilterControlWidth = Unit.Pixel(50);
                    break;
                case "month":
                    e.Column.HeaderText = "Month/Year";
                    e.Column.FilterControlWidth = Unit.Pixel(170);
                    ((GridBoundColumn)e.Column).DataFormatString = "{0:MMM yy}";
                    break;                
                case "balance":
                    e.Column.HeaderText = "Balance";
                    break;
                case "bonusinterest":
                    e.Column.HeaderText = "Bonus Interest";
                    break;
                case "interest":
                    ((GridBoundColumn)e.Column).DataFormatString = "{0:f2}";
                    e.Column.HeaderText = "Interest";
                    break;
                case "totalinterest":
                   ((GridBoundColumn)e.Column).DataFormatString = "{0:f2}";
                   e.Column.HeaderText = "Total Interest";
                    break;
                case "message":
                    e.Column.HeaderText = "Message";
                    break;
                case "haserrors":
                    e.Column.HeaderText = "Has Errors";
                    e.Column.Visible = false;
                    break;
                case "ismissingitem":
                    e.Column.HeaderText = "Is Missing Item";
                    e.Column.Visible = false;
                    break;
                case "missingitem":
                    e.Column.HeaderText = "Missing Client Accounts";
                    break;
            }
        }

        private DataTable CreateTableForAtCallTransactionsImport()
        {
            DataTable dt = new DataTable();
            dt.TableName = "AtCallTransactions";
            DataColumn dc = new DataColumn("ClientName", typeof(string));
            DataColumn dc1 = new DataColumn("CustomerNumber", typeof(string));
            DataColumn dc2 = new DataColumn("AccNumber", typeof(string));
            DataColumn dc3 = new DataColumn("Month", typeof(DateTime));            
            DataColumn dc4 = new DataColumn("Balance", typeof(string));
            DataColumn dc5 = new DataColumn("BonusInterest", typeof(string));
            DataColumn dc6 = new DataColumn("Interest", typeof(string));
            DataColumn dc7 = new DataColumn("Amount", typeof(string));
            DataColumn dc8 = new DataColumn("SheetName", typeof(string));
            dt.Columns.Add(dc);
            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);
            dt.Columns.Add(dc3);
            dt.Columns.Add(dc4);
            dt.Columns.Add(dc5);
            dt.Columns.Add(dc6);
            dt.Columns.Add(dc7);
            dt.Columns.Add(dc8);
            return dt;
        }
        protected override void GetBMCData()
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            FillInstitutionDropDown(organization.Institution);
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }
        
        private void FillInstitutionDropDown(List<InstitutionEntity> Institution)
        {

            string Insval = cmbInstitution.SelectedValue;
            cmbInstitution.ClearSelection();
            cmbInstitution.DataSource = Institution;
            cmbInstitution.DataTextField = "Name";
            cmbInstitution.DataValueField = "ID";
            cmbInstitution.DataBind();
            RadComboBoxItem item = new RadComboBoxItem();
            item.Text = "-- Select Institute --";
            item.Value = Guid.Empty.ToString();
            cmbInstitution.Items.Insert(0, item);
            cmbInstitution.SelectedValue = Insval;
        }
        private DataTable CreateAtCallHoldingTable()
        {
            DataTable dt = new DataTable();
            dt.TableName = "AtCallHoldings";
            DataColumn dc_ClientID = new DataColumn("ClientID", typeof(string));
            DataColumn dc_ClientCID = new DataColumn("ClientCID", typeof(Guid));
            DataColumn dc_CustNo = new DataColumn("CustomerNumber", typeof(string));
            DataColumn dc_ClientName = new DataColumn("ClientName", typeof(string));
            DataColumn dc_CID = new DataColumn("CID", typeof(Guid));
            DataColumn dc_CLID = new DataColumn("CLID", typeof(Guid));
            DataColumn dc_CSID = new DataColumn("CSID", typeof(Guid));
            DataColumn dc_Holdings = new DataColumn("Holding", typeof(decimal));
            DataColumn dc_TransHolding = new DataColumn("TransactionHolding", typeof(decimal));
            DataColumn dc_Diff = new DataColumn("Diff", typeof(decimal));
            dt.Columns.Add(dc_ClientID);
            dt.Columns.Add(dc_ClientCID);
            dt.Columns.Add(dc_CustNo);
            dt.Columns.Add(dc_ClientName);         
            dt.Columns.Add(dc_CID);
            dt.Columns.Add(dc_CLID);
            dt.Columns.Add(dc_CSID);
            dt.Columns.Add(dc_Holdings);
            dt.Columns.Add(dc_TransHolding);
            dt.Columns.Add(dc_Diff);           
            return dt;

            
        }
        protected void ddlSheetName_SelectedIndexChanged(object o, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ImportProcessDS ds = new ImportProcessDS();
            
            if (ddlSheetName.Text.Trim() == "All")
            {
                SetValidationGird(importDS);
            }
            else
            {
                ds.Tables.Add(importDS.Tables["AtCallHoldings"].Copy());
                string monthName = string.Empty;
                string year = string.Empty;
                ds.Tables.Add(importDS.Tables["AtCallTransactions"].Clone());
                monthName = ddlSheetName.Text.Substring(0, ddlSheetName.Text.IndexOf(" ")).Trim();
                year = ddlSheetName.Text;
                if (year.Contains(" "))
                {
                    year = year.Substring(year.IndexOf(" ") + 1);
                }
                string selectyquery = string.Format("SheetName='{0} {1}'", monthName, year);
                DataRow[] drs = importDS.Tables["AtCallTransactions"].Select(selectyquery);
              foreach (DataRow dr in drs)
              {
                  DataRow datarow = ds.Tables["AtCallTransactions"].NewRow();
                  datarow["ClientName"] = dr["ClientName"];
                  datarow["CustomerNumber"] = dr["CustomerNumber"];
                  datarow["AccNumber"] = dr["AccNumber"];
                  datarow["Month"] = dr["Month"];
                  datarow["SheetName"] = dr["SheetName"];
                  datarow["Balance"] = dr["Balance"];
                  datarow["BonusInterest"] = dr["BonusInterest"];
                  datarow["Interest"] = dr["Interest"];
                  datarow["Amount"] = dr["Amount"];
                  datarow["Message"] = dr["Message"];
                  datarow["HasErrors"] = dr["HasErrors"];
                  datarow["IsMissingItem"] = dr["IsMissingItem"];
                  ds.Tables["AtCallTransactions"].Rows.Add(datarow);
              }
              FinalImportDS = ds;
              SetValidationGird(ds);
            }
            
        }
        
    }
}