﻿using System;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Configuration;
using Telerik.Web.UI;

namespace eclipseonlineweb
{
    public partial class DownloadExportFiles : UMABasePage
    {

        public override void LoadPage()
        {

        }

        private DataTable get_files()
        {
            string pattern = string.Empty;
            if (Request.QueryString["type"] != null)
            {
                pattern = Request.QueryString["type"] + "*.*";
                if (btnSMA.Checked)
                    pattern = string.Format("SMA_{0}", pattern);
            }
            string path = GetPath();
            var directory = new DirectoryInfo(path);
            if (!directory.Exists)
            {
                directory.Create();
            }

            var files = directory.GetFiles(pattern);
            DataTable dt = CreateTableForExportInstructionsFiles();
            foreach (var fileInfo in files)
            {
                DataRow dr = dt.NewRow();
                dr["FileName"] = fileInfo.Name;
                dr["CreateDate"] = fileInfo.CreationTime;
                dt.Rows.Add(dr);
            }

            return dt;
        }
        protected void gd_Download_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetData();
        }

        private void GetData()
        {
            DataView dv = get_files().DefaultView;
            dv.Sort = "CreateDate desc";
            gd_Download.DataSource = dv.ToTable();
        }

        private DataTable CreateTableForExportInstructionsFiles()
        {
            var dt = new DataTable();
            var dc = new DataColumn("FileName", typeof(string));
            var dc1 = new DataColumn("CreateDate", typeof(DateTime));
            dt.Columns.Add(dc);
            dt.Columns.Add(dc1);
            return dt;
        }
        protected void gd_DownloadGridItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "download":
                    var item = (GridDataItem)e.Item;
                    string downloadfileName = item["FileName"].Text;
                    DownloadFiles(downloadfileName);
                    break;
                case "delete":
                    var item1 = (GridDataItem)e.Item;
                    string deletedfileName = item1["FileName"].Text;
                    DeleteSelectedFile(deletedfileName);
                    break;
            }
        }

        private void DeleteSelectedFile(string fileName)
        {
            string path = GetPath();
            path = path + fileName;
            File.Delete(path);
            GetData();
        }

        private void DownloadFiles(string fileName)
        {
            string path = GetPath();
            string strURL = path + fileName;
            byte[] bytes = File.ReadAllBytes(strURL);
            Session["ExportFileBytes"] = bytes;
            string script = string.Format("window.open('ExportReportPopup.aspx?fileName={0}&filetype={1}',null,'height=200,width=400,status=yes,toolbar=no,menubar=no,location=no');", fileName, "zip");
            ScriptManager.RegisterStartupScript(UpdatePanel, UpdatePanel.GetType(), "OpenNewWindow", script, true);
        }

        private string GetPath()
        {
            string path = Server.MapPath("~\\" + ConfigurationManager.AppSettings["ExportInstructionsPath"]);
            if (!path.EndsWith("\\"))
                path += "\\";
            if (btnSMA.Checked)
            {
                path += "SMA\\";
            }
            return path;
        }

        protected void btnUMA_OnClick(object sender, EventArgs e)
        {
            gd_Download.Rebind();
        }

        protected void btnSMA_OnClick(object sender, EventArgs e)
        {
            gd_Download.Rebind();
        }
    }
}