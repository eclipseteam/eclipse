﻿using Oritax.TaxSimp.Security;

namespace eclipseonlineweb.SysAdministration
{
    public partial class P2Reports : UMABasePage
    {
        public override void LoadPage()
        {
            if (!IsPostBack)
            {
                var user = GetCurrentUser();
                //Setting control required properties
                P2ReportsControl.IsAdmin = user.IsAdmin;
                P2ReportsControl.IsAdminMenu = true;
            }
        }

        protected override void Intialise()
        {
            base.Intialise();
            P2ReportsControl.SaveData += SaveData;
        }
    }


}