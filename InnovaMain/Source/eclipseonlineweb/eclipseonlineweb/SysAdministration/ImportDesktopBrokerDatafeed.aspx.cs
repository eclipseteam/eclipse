﻿using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.DesktopBrokerDataFeed;
using eclipseonlineweb.WebUtilities;
using eclipseonlineweb.WebControls;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class ImportDesktopBrokerDataFeed : UMABasePage
    {

        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void Intialise()
        {
            ImportMessageControl.GridImportMessages.ColumnCreated += GridImportMessages_ColumnCreated;
            ImportMessageControl.GirdInvestmentCodeError.ColumnCreated += GridImportErrorMessages_ColumnCreated;
            ImportMessageControl.GridImportMessages.ItemDataBound += GridImportMessages_ItemDataBound;

            if (!IsPostBack)
            {
                ImportMessageControl.Clear();
            }

            ImportMessageControl.Completed += text =>
                                                  {
                                                      switch (text.ToLower())
                                                      {
                                                          case "next":
                                                              SendData();
                                                              break;
                                                          default:
                                                              ClearMessageControls();
                                                              break;
                                                      }
                                                  };


        }
        public override void LoadPage()
        {
            ImportMessageControl.ShowHideSampleFileLink("~/Templates/ImportSampleFiles/BellDirectData (110317) - confirmation.xml");
        }

        void GridImportMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {

            SetDataGridColumns(e);
        }

        void GridImportErrorMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            SetDataGridColumns(e);
        }

        private static string GetDatafeedXml(FileInfo fileinfo)
        {
            var mergeXml = "";
            try
            {
                StreamReader reader = fileinfo.OpenText();
                string xml = reader.ReadToEnd();
                reader.Close();

                xml = xml.Replace(" xmlns=\"http://www.epistandard.com/epi/datafeed/4.0\"", "");

                var bellDirectXML = XElement.Parse(xml);

                var asxBrokers = from c in bellDirectXML.Elements("PlatformProviders")
                                     .Elements("PlatformProvider").Elements("Extracts")
                                     .Elements("Extract").Elements("Advisers").Elements("Adviser")
                                     .Elements("Accounts")
                                 select c;

                mergeXml = asxBrokers.Select(account => (from a in account.Elements("Account") select a)).Aggregate("<Datafeed> <Accounts>", (current1, accountXml) => accountXml.Aggregate(current1, (current, s) => current + s.ToString()));

                mergeXml += "</Accounts></Datafeed>";
            }
            catch (IOException)
            {
            }
            return mergeXml;
        }

        private void SetValidationGird(ImportProcessDS ds)
        {
            //ImportMessageControl.Clear();
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    if (missingItems.Select(string.Format("MissingItem='{0}'", row["InvestmentCode"])).Length == 0)
                    {
                        DataRow dr = missingItems.NewRow();
                        dr["MissingItem"] = row["InvestmentCode"];
                        missingItems.Rows.Add(dr);
                    }
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();

                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingInvestmentCodeAdded;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }

            int successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
            if (ds.Tables[0].Rows.Count == 0 || successfulRows == 0)
            {
                ImportMessageControl.SetButtons(ButtonTypes.CLOSE);
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sErrorsCannotContinue, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Error);
                ImportMessageControl.TextMessage.Text = "";
            }
            else
            {
                ImportMessageControl.SetButtons(ButtonTypes.NEXTCANCEL);
                //sucess
                ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sValidationCompleted, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
            }
        }

        private void SetCompletionGird(ImportProcessDS ds)
        {
            ImportMessageControl.Clear();
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }

            using (DataView dv = new DataView(ds.Tables[0]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                DataRow dr;
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    if (missingItems.Select(string.Format("MissingItem='{0}'", row["InvestmentCode"])).Length == 0)
                    {
                        dr = missingItems.NewRow();
                        dr["MissingItem"] = row["InvestmentCode"];
                        missingItems.Rows.Add(dr);
                    }
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();

                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingInvestmentCodeAdded;
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }

            int successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
            ImportMessageControl.SetButtons(ButtonTypes.FINISH);
            ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sImportedSuccessfully, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
        }

        private void SendData()
        {
            var distributionDictory = Server.MapPath("~/App_Data/Import/");
            if (!Directory.Exists(distributionDictory))
            {
                Directory.CreateDirectory(distributionDictory);
            }

            string filename = distributionDictory + ImportMessageControl.GetFileID() + ".xml";
            ImportProcessDS ds = new ImportProcessDS();
            string xml = string.Empty;
            using (StreamReader reader = new StreamReader(filename))
            {
                xml = reader.ReadToEnd();
            }
            ds.ExtendedProperties.Add("Xml", xml);
            ds.Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ImportDesktopBrokerDataFeedImportFile;
            ds.FileName = filename;
            SaveOrganizanition(ds);
            SetCompletionGird(ds);
            ImportMessageControl.SetImportSuccessFileMsg("File is imported successfully. You can see ASX Broker Holding Vs Transactions report from", "~/SysAdministration/ASXAccountVsTransactions.aspx");
        }

        public void ClearMessageControls()
        {
            ImportMessageControl.Clear();
            ImportMessageControl.TextMessage.Text = "";
        }

        private static void SetDataGridColumns(GridColumnCreatedEventArgs e)
        {
            string colName = e.Column.UniqueName.ToLower();
            switch (colName)
            {
                case "message":
                    e.Column.HeaderText = "Message";
                    break;
                case "haserrors":
                    e.Column.HeaderText = "Has Errors";
                    e.Column.Visible = false;
                    break;
                case "ismissingitem":
                    e.Column.HeaderText = "Is Missing Item";
                    e.Column.Visible = false;
                    break;
                case "missingitem":
                    e.Column.HeaderText = "Missing Investment Code";
                    break;
            }
        }

        protected void btnImport_OnClick(object sender, EventArgs e)
        {
            try
            {
                var importFolder = Server.MapPath("~/App_Data/Import/");
                if (!Directory.Exists(importFolder))
                {
                    Directory.CreateDirectory(importFolder);
                }

                var objDownload = new XMLDownloader
                            {
                                Url = ConfigurationManager.AppSettings["DBDataFeedURL"] + "&ReferenceID=&StartDate=&EndDate=" + DateTime.Now.ToString("dd-MM-yyyy")
                            };
                string fileLocation = objDownload.MakeRequest(importFolder);

                ProcessFile(importFolder, fileLocation);
            }
            catch (Exception)
            {
            }

        }

        protected void btnImportFiveDays_OnClick(object sender, EventArgs e)
        {
            try
            {
                var importFolder = Server.MapPath("~/App_Data/Import/");
                if (!Directory.Exists(importFolder))
                {
                    Directory.CreateDirectory(importFolder);
                }

                var objDownload = new XMLDownloader
                {
                    Url = ConfigurationManager.AppSettings["DBDataFeedURL"] + "&ReferenceID=&StartDate="+DateTime.Now.AddDays(-5).ToString("dd-MM-yyyy")+"&EndDate=" + DateTime.Now.ToString("dd-MM-yyyy")
                };
                string fileLocation = objDownload.MakeRequest(importFolder);

                ProcessFile(importFolder, fileLocation);
            }
            catch (Exception)
            {
            }

        }

        void GridImportMessages_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item).ItemIndex == -1) return;

            DataRow dr = ((DataRowView)((e.Item).DataItem)).Row;
            if (bool.Parse(dr["HasErrors"].ToString()))
            {
                e.Item.ForeColor = Color.Red;
            }
        }

        public void BtnUploadClick(object sender, EventArgs e)
        {
            // Check to see if file was uploaded
            if (filMyFile.PostedFile != null)
            {
                // Get a reference to PostedFile object
                HttpPostedFile myFile = filMyFile.PostedFile;

                // Get size of uploaded file
                int nFileLen = myFile.ContentLength;

                // make sure the size of the file is > 0
                if (nFileLen > 0)
                {
                    // Allocate a buffer for reading of the file
                    string fileName = Path.GetFileName(myFile.FileName);
                    var date = DateTime.Now;
                    var importFolder = Server.MapPath("~/App_Data/Import/");
                    if (!Directory.Exists(importFolder))
                    {
                        Directory.CreateDirectory(importFolder);
                    }

                    string fileLocation = importFolder + date.ToString("ddMMyyhhmmsstt") + fileName;
                    byte[] myData = new byte[nFileLen];

                    // Read uploaded file from the Stream
                    myFile.InputStream.Read(myData, 0, nFileLen);

                    // Write data into a file
                    Utilities.WriteToFile(fileLocation, ref myData);

                    ProcessFile(importFolder, fileLocation);
                }
            }
        }

        private void ProcessFile(string importFolder, string fileLocation)
        {
            FileInfo fileinfo = new FileInfo(fileLocation);
            string xml = GetDatafeedXml(fileinfo);

            if (string.IsNullOrEmpty(xml))
            {
                return;
            }
            // replacing & in data because it xml parser throughs exception
            xml = xml.Replace("&", "&amp;");

            ImportProcessDS ds = new ImportProcessDS();
            ds.ExtendedProperties.Add("Xml", xml);
            ds.Unit = new OrganizationUnit { CurrentUser = GetCurrentUser() };
            ds.Command = (int)WebCommands.ValidateDesktopBrokerDataFeedImportFile;
            ds.FileName = fileinfo.Name;
            SaveOrganizanition(ds);
            SetValidationGird(ds);
            PresentationData = ds;
            string filename = ImportMessageControl.RefreshFileID() + ".xml";
            using (StreamWriter writer = new StreamWriter(importFolder + filename, true))
            {
                writer.WriteLine(xml);
            }
        }
    }
}