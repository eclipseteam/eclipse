﻿using System;
using System.Linq;
using System.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.Organization;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1Gauge;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections.Generic;
using System.Collections;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.Common.UMAFee;

namespace eclipseonlineweb
{
    public partial class FeeDetails : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        string id = String.Empty;
        protected override void GetBMCData()
        {
            id = Request.QueryString["ID"].ToString();

            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
            umaFeesDetailsDS.FeeEntityID = new Guid(id);
            organization.GetData(umaFeesDetailsDS);
            this.PresentationData = umaFeesDetailsDS;

            if (umaFeesDetailsDS.FeeEntity.ProductConfgType == ProductConfgType.All)
                this.C1ComboBoxProductConfigType.Items.FindItemByValue("All").Selected = true;
            else if (umaFeesDetailsDS.FeeEntity.ProductConfgType == ProductConfgType.ByExclusions)
                this.C1ComboBoxProductConfigType.Items.FindItemByValue("By Exclusions").Selected = true;
            else if (umaFeesDetailsDS.FeeEntity.ProductConfgType == ProductConfgType.ByInclusions)
                this.C1ComboBoxProductConfigType.Items.FindItemByValue("By Inclusions").Selected = true;

            lblDetails.Text = umaFeesDetailsDS.FeeEntity.Description.ToUpper() + "- Fee Breakdown";

            if (!Page.IsPostBack)
            {
                this.C1ComboBoxProducts.DataSource = organization.Products;
                C1ComboBoxProducts.DataTextField = "Description";
                C1ComboBoxProducts.DataValueField = "ID";
                C1ComboBoxProducts.DataBind();
            }

            UMABroker.ReleaseBrokerManagedComponent(organization);
        }


        protected void DownloadXLS(object sender, ImageClickEventArgs e)
        {
        }

        public override void LoadPage()
        {
            base.LoadPage();

            if (C1ComboBoxProductConfigType.SelectedItem.Value == "All")
                pnlSelectProducts.Visible = false;
            else
                pnlSelectProducts.Visible = true;


            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (!objUser.Administrator && (objUser.UserType == UserType.Client || objUser.UserType == UserType.Advisor))
            {
                ((SetupMaster)Master).IsAdmin = "1";
            }
        }

        #region Telerik Grid Products Config

        protected void GridProducts_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e) { }
        protected void GridProducts_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                Guid rowID = new Guid(gDataItem["PROID"].Text);
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                umaFeesDetailsDS.ProductID = rowID;
                umaFeesDetailsDS.FeeOperationType = FeeOperationType.DeleteProduct;
                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.GridProducts.Rebind();
            }
        }
        protected void GridProducts_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) { }
        protected void GridProducts_ItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void GridProducts_ItemInserted(object source, GridInsertedEventArgs e) { }
        protected void GridProducts_InsertCommand(object source, GridCommandEventArgs e) { }
        protected void GridProducts_ItemCreated(object sender, GridItemEventArgs e) { }
        protected void GridProducts_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e) { }
        protected void GridProducts_NeedDataSource(object sender, GridNeedDataSourceEventArgs e) {

            DataTable proTable = this.PresentationData.Tables[UMAFeesDetailsDS.PRODUCTSTABLENAME];
            DataView proView = new DataView(proTable);
            proView.Sort = UMAFeesDetailsDS.PRONAME + " ASC";
            this.GridProducts.DataSource = proView.ToTable(); 
        }

        #endregion 

        #region Telerik Grid OngoingFee Value

        protected void GridOngoingFeeValue_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e){}
        protected void GridOngoingFeeValue_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                Guid rowID = new Guid(gDataItem["ID"].Text);
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                iorg.GetData(umaFeesDetailsDS);

                var feeEnitity = umaFeesDetailsDS.FeeEntity.OngoingFeeValueList.Where(fee => fee.ID == rowID).FirstOrDefault();
                if (feeEnitity != null)
                    umaFeesDetailsDS.FeeEntity.OngoingFeeValueList.Remove(feeEnitity);

                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.GridOngoingFeeValue.Rebind();

            }

            else if (e.CommandName.ToLower() == "update")
            {
                GridEditFormItem gDataItem = e.Item as GridEditFormItem;
                Guid rowID = new Guid(gDataItem.GetDataKeyValue("ID").ToString());
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                iorg.GetData(umaFeesDetailsDS);

                var feeEnitity = umaFeesDetailsDS.FeeEntity.OngoingFeeValueList.Where(fee => fee.ID == rowID).FirstOrDefault();

                if (feeEnitity != null)
                {
                    feeEnitity.FeeValue = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["FeeValue"].Controls[0])).Text);
                    feeEnitity.Description = ((TextBox)(((GridEditableItem)(e.Item))["Description"].Controls[0])).Text;
                    feeEnitity.Year = umaFeesDetailsDS.FeeEntity.Year;
                }

                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.GridOngoingFeeValue.Rebind();
            }
        }
        protected void GridOngoingFeeValue_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e){}
        protected void GridOngoingFeeValue_ItemDeleted(object source, GridDeletedEventArgs e){}
        protected void GridOngoingFeeValue_ItemInserted(object source, GridInsertedEventArgs e){}
        protected void GridOngoingFeeValue_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("FeesOngoingValue".Equals(e.Item.OwnerTableView.Name))
            {
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                iorg.GetData(umaFeesDetailsDS); 

                OngoingFeeValue ongoingFeeValue = new Oritax.TaxSimp.Common.UMAFee.OngoingFeeValue();
                ongoingFeeValue.FeeValue = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["FeeValue"].Controls[0])).Text);
                ongoingFeeValue.Description = ((TextBox)(((GridEditableItem)(e.Item))["Description"].Controls[0])).Text;
                ongoingFeeValue.Year = umaFeesDetailsDS.FeeEntity.Year;
                umaFeesDetailsDS.FeeEntity.OngoingFeeValueList.Add(ongoingFeeValue);

                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.GridOngoingFeeValue.Rebind();
            }
        }
        protected void GridOngoingFeeValue_ItemCreated(object sender, GridItemEventArgs e){}
        protected void GridOngoingFeeValue_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e){}
        protected void GridOngoingFeeValue_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetBMCData();

            UMAFeesDetailsDS umaFeesDetailsDS = (UMAFeesDetailsDS)this.PresentationData;
            this.GridOngoingFeeValue.DataSource = umaFeesDetailsDS.FeeEntity.OngoingFeeValueList; 
        }

        #endregion 

        #region Telerik Grid OngoingFee Percentage

        protected void GridOngoingFeePercent_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e) { }
        protected void GridOngoingFeePercent_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                Guid rowID = new Guid(gDataItem["ID"].Text);
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                iorg.GetData(umaFeesDetailsDS);

                var feeEnitity = umaFeesDetailsDS.FeeEntity.OngoingFeePercentList.Where(fee => fee.ID == rowID).FirstOrDefault();
                if (feeEnitity != null)
                    umaFeesDetailsDS.FeeEntity.OngoingFeePercentList.Remove(feeEnitity); 

                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.GridOngoingFeePercent.Rebind();

            }

            else if (e.CommandName.ToLower() == "update")
            {
                GridEditFormItem gDataItem = e.Item as GridEditFormItem;
                Guid rowID = new Guid(gDataItem.GetDataKeyValue("ID").ToString());
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                iorg.GetData(umaFeesDetailsDS);

                var feeEnitity = umaFeesDetailsDS.FeeEntity.OngoingFeePercentList.Where(fee => fee.ID == rowID).FirstOrDefault();
                
                if (feeEnitity != null)
                {
                    feeEnitity.PercentageFee = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["PercentageFee"].Controls[0])).Text);
                    feeEnitity.Description = ((TextBox)(((GridEditableItem)(e.Item))["Description"].Controls[0])).Text;
                    feeEnitity.Year = umaFeesDetailsDS.FeeEntity.Year;
                }
                
                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.GridOngoingFeePercent.Rebind();
            }
        }
        protected void GridOngoingFeePercent_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) { }
        protected void GridOngoingFeePercent_ItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void GridOngoingFeePercent_ItemInserted(object source, GridInsertedEventArgs e) { }
        protected void GridOngoingFeePercent_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("FeesOngoingPercent".Equals(e.Item.OwnerTableView.Name))
            {
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                iorg.GetData(umaFeesDetailsDS);

                OngoingFeePercent ongoingFeePercent = new Oritax.TaxSimp.Common.UMAFee.OngoingFeePercent();
                ongoingFeePercent.PercentageFee = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["PercentageFee"].Controls[0])).Text);
                ongoingFeePercent.Description = ((TextBox)(((GridEditableItem)(e.Item))["Description"].Controls[0])).Text;
                ongoingFeePercent.Year = umaFeesDetailsDS.FeeEntity.Year;
                umaFeesDetailsDS.FeeEntity.OngoingFeePercentList.Add(ongoingFeePercent);

                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.GridOngoingFeePercent.Rebind();
            }
        }
        protected void GridOngoingFeePercent_ItemCreated(object sender, GridItemEventArgs e) { }
        protected void GridOngoingFeePercent_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e) { }
        protected void GridOngoingFeePercent_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetBMCData();

            UMAFeesDetailsDS umaFeesDetailsDS = (UMAFeesDetailsDS)this.PresentationData;
            this.GridOngoingFeePercent.DataSource = umaFeesDetailsDS.FeeEntity.OngoingFeePercentList; 
        }

        #endregion 

        #region Telerik Grid Fee Tiered 

        protected void TieredFee_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e) { }
        protected void TieredFee_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem gDataItem = e.Item as GridDataItem;
                Guid rowID = new Guid(gDataItem["ID"].Text);
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                iorg.GetData(umaFeesDetailsDS);

                var feeEnitity = umaFeesDetailsDS.FeeEntity.FeeTieredRangeList.Where(fee => fee.ID == rowID).FirstOrDefault();
                if (feeEnitity != null)
                    umaFeesDetailsDS.FeeEntity.FeeTieredRangeList.Remove(feeEnitity);

                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.TieredFee.Rebind();

            }
            else if (e.CommandName.ToLower() == "update")
            {
                GridEditFormItem gDataItem = e.Item as GridEditFormItem;
                Guid rowID = (Guid)gDataItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"];

                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                iorg.GetData(umaFeesDetailsDS);

                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;

                var feeEnitity = umaFeesDetailsDS.FeeEntity.FeeTieredRangeList.Where(fee => fee.ID == rowID).FirstOrDefault();

                if (feeEnitity != null)
                {
                    feeEnitity.Description = ((TextBox)(((GridEditableItem)(e.Item))["Description"].Controls[0])).Text;
                    feeEnitity.Year = umaFeesDetailsDS.FeeEntity.Year;
                    feeEnitity.FromValue = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["FromValue"].Controls[0])).Text);
                    feeEnitity.ToValue = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["ToValue"].Controls[0])).Text);
                    feeEnitity.PercentageFee = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["PercentageFee"].Controls[0])).Text);
                }

                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;

                GetBMCData();
                this.TieredFee.Rebind();
            }
        }
        protected void TieredFee_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) { }
        protected void TieredFee_ItemDeleted(object source, GridDeletedEventArgs e) { }
        protected void TieredFee_ItemInserted(object source, GridInsertedEventArgs e) { }
        protected void TieredFee_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("TieredFees".Equals(e.Item.OwnerTableView.Name))
            {
                GridEditFormInsertItem EditForm = (GridEditFormInsertItem)e.Item;

                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                iorg.GetData(umaFeesDetailsDS);

                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                
                FeeTieredRange feeTieredRange = new FeeTieredRange();
                feeTieredRange.Description = ((TextBox)(((GridEditableItem)(e.Item))["Description"].Controls[0])).Text;
                feeTieredRange.Year = umaFeesDetailsDS.FeeEntity.Year;
                feeTieredRange.FromValue = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["FromValue"].Controls[0])).Text);
                feeTieredRange.ToValue = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["ToValue"].Controls[0])).Text);
                feeTieredRange.PercentageFee = Convert.ToDecimal(((TextBox)(((GridEditableItem)(e.Item))["PercentageFee"].Controls[0])).Text);

                umaFeesDetailsDS.FeeEntity.FeeTieredRangeList.Add(feeTieredRange);

                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Canceled = true;
                e.Item.Edit = false;
                GetBMCData();
                this.TieredFee.Rebind();
            }
        }
        protected void TieredFee_ItemCreated(object sender, GridItemEventArgs e) { }
        protected void TieredFee_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e) { }
        protected void TieredFee_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetBMCData();

            UMAFeesDetailsDS umaFeesDetailsDS = (UMAFeesDetailsDS)this.PresentationData;
            this.TieredFee.DataSource = umaFeesDetailsDS.FeeEntity.FeeTieredRangeList; 
        }

        #endregion 

        protected void btnSet_Click(object sender, EventArgs e)
        {

            if (C1ComboBoxProductConfigType.Text == "All")
            {
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                iorg.GetData(umaFeesDetailsDS);
                umaFeesDetailsDS.FeeEntity.ProductList.Clear();
                umaFeesDetailsDS.FeeEntity.ProductConfgType = ProductConfgType.All;
                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                GetBMCData();
                this.GridProducts.Rebind();
                this.pnlSelectProducts.Visible = false;
            }
            else
            {
                UMABroker.SaveOverride = true;
                IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
                umaFeesDetailsDS.FeeEntityID = new Guid(id);
                iorg.GetData(umaFeesDetailsDS);
                umaFeesDetailsDS.FeeEntity.ProductList.Clear();

                if (C1ComboBoxProductConfigType.Text == "By Exclusions")
                    umaFeesDetailsDS.FeeEntity.ProductConfgType = ProductConfgType.ByExclusions;
                if (C1ComboBoxProductConfigType.Text == "By Inclusions")
                    umaFeesDetailsDS.FeeEntity.ProductConfgType = ProductConfgType.ByInclusions;

                umaFeesDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                iorg.SetData(umaFeesDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                GetBMCData();
                this.GridProducts.Rebind();
                this.pnlSelectProducts.Visible = true; 
            }
        }

        protected void btnAddProductst_Click(object sender, EventArgs e)
        {
            UMABroker.SaveOverride = true;
            IOrganization iorg = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            UMAFeesDetailsDS umaFeesDetailsDS = new UMAFeesDetailsDS();
            umaFeesDetailsDS.FeeEntityID = new Guid(this.id);
            umaFeesDetailsDS.FeeOperationType = FeeOperationType.NewProduct;
            DataTable productTable = umaFeesDetailsDS.Tables[UMAFeesDetailsDS.PRODUCTSTABLENAME];
            
            foreach (var item in C1ComboBoxProducts.CheckedItems)
            {
                DataRow row = productTable.NewRow();
                row[UMAFeesDetailsDS.PROID] = new Guid(item.Value);
                row[UMAFeesDetailsDS.PRONAME] = item.Text;

                productTable.Rows.Add(row); 
            }

            iorg.SetData(umaFeesDetailsDS);
            
            UMABroker.SetComplete();
            UMABroker.SetStart();
            GetBMCData();
            this.GridProducts.Rebind();
        }
    }
}
