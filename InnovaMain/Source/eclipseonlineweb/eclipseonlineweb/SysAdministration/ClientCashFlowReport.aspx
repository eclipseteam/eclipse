<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="ClientCashFlowReport.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.ClientCashFlowReport" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function SetClientCount(sender, args) {
            var txtClientCount = $find("<%: txtClientCount.ClientID %>");
            if (args.get_checked()) {
                txtClientCount.set_minValue(0);
                txtClientCount.set_value(0);

            }
            else {
                txtClientCount.set_value(5);
                txtClientCount.set_minValue(5);
            }
        }
    </script>
    <style type="text/css">
        .classImage
        {
            background: url('./images/excel.png');
            background-position: 0 0;
            width: 100px;
            height: 25px;
        }
        .classHoveredImage
        {
            background-position: 0 -100px;
        }
        .classPressedImage
        {
            background-position: 0 -200px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <table width="100%" >
            <tr>
                <td style="width: 100%;" class="breadcrumbgap">
                    <uc1:BreadCrumb ID="BreadCrumb2" runat="server" />
                    <br /> <br />
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <span class="riLabel">Start Date:</span><telerik:RadDatePicker ID="InputStartDate"
                        runat="server">
                        <DateInput DateFormat="MMM - yyyy">
                        </DateInput></telerik:RadDatePicker>
            
                    <span class="riLabel">End Date:</span><telerik:RadDatePicker ID="InputEndDate" runat="server">
                        <DateInput DateFormat="MMM - yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
           
                    <span class="riLabel">All Clients:</span>
                    <telerik:RadButton ID="chkAllClients" runat="server"  AutoPostBack="false"
                        OnClientCheckedChanged="SetClientCount" ToggleType="CheckBox" ButtonType="ToggleButton">
                    </telerik:RadButton>
                
                    <span class="riLabel">No. of Clients:</span>
                    <telerik:RadNumericTextBox runat="server" ID="txtClientCount" MinValue="5" ShowSpinButtons="True"
                        EnableViewState="true" Value="5">
                        <NumberFormat GroupSeparator="" DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
               
                    <telerik:RadButton runat="server" ID="BtnGenerateReport" OnClick="GenerateReport"
                        Text="Generate Report">
                    </telerik:RadButton>
                  
                    <telerik:RadButton ID="btnReport" runat="server" OnClick="BtnReportClick" Text="Export">
                        <Icon PrimaryIconUrl="~/images/excel_16.png" PrimaryIconLeft="2" PrimaryIconTop="2"></Icon>
                    </telerik:RadButton>
                
                    &nbsp;
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <fieldset>
        <legend>Cash Flow Report</legend>
        <br />
        <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
            Width="100%" PageSize="500" AllowSorting="True" AllowMultiRowSelection="False"
            AllowPaging="false" GridLines="None" AllowFilteringByColumn="true" EnableViewState="true"
            ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView AllowFilteringByColumn="True" TableLayout="Fixed" Width="100%" GroupLoadMode="Client">
                <Columns>
                    <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="ClientCID"
                        SortExpression="ClientCode" UniqueName="ClientCode" DataNavigateUrlFormatString="../ClientViews/ClientMainView.aspx?ins={0}"
                        HeaderText="Client ID" DataTextField="ClientCode" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" FilterControlWidth="75px" HeaderStyle-Width="120px"
                        HeaderButtonType="TextButton" ShowFilterIcon="true">
                    </telerik:GridHyperLinkColumn>
                    <telerik:GridBoundColumn SortExpression="ClientName" HeaderText="ClientName" HeaderButtonType="TextButton"
                        DataField="ClientName" UniqueName="ClientName" FilterControlWidth="100px" HeaderStyle-Width="200px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="AdviserName" HeaderText="AdviserName" HeaderButtonType="TextButton"
                        DataField="AdviserName" UniqueName="AdviserName" FilterControlWidth="100px" HeaderStyle-Width="200px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="ServiceType" HeaderText="ServiceType" HeaderButtonType="TextButton"
                        DataField="ServiceType" UniqueName="ServiceType" FilterControlWidth="100px" HeaderStyle-Width="175px">
                    </telerik:GridBoundColumn>
                    <telerik:GridDateTimeColumn SortExpression="TradeDate" ReadOnly="true" PickerType="DatePicker"
                        HeaderText="Month" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" FilterControlWidth="75%" DataFormatString="{0:MMM-yyyy}"
                        FilterDateFormat="MMM-yyyy" HeaderButtonType="TextButton" DataField="TradeDate"
                        UniqueName="TradeDate">
                        <HeaderStyle Width="19%" HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" />
                    </telerik:GridDateTimeColumn>
                    <telerik:GridBoundColumn SortExpression="ReinvestOrDistributeOption" HeaderText="ReinvestOrDistributeOption"
                        HeaderButtonType="TextButton" DataField="ReinvestOrDistributeOption" UniqueName="ReinvestOrDistributeOption"
                        FilterControlWidth="100px" HeaderStyle-Width="175px" >
                    </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn SortExpression="AccountType" HeaderText="AccountType"
                        HeaderButtonType="TextButton" DataField="AccountType" UniqueName="AccountType"
                        FilterControlWidth="100px" HeaderStyle-Width="175px" >
                    </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn SortExpression="AccountName" HeaderText="AccountName"
                        HeaderButtonType="TextButton" DataField="AccountName" UniqueName="AccountName"
                        FilterControlWidth="100px" HeaderStyle-Width="175px" >
                    </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn SortExpression="OpeningBalance" HeaderText="OpeningBalance"
                        HeaderButtonType="TextButton" DataField="OpeningBalance" UniqueName="OpeningBalance"
                        FilterControlWidth="100px" HeaderStyle-Width="175px" DataFormatString="{0:N2}">
                    </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn SortExpression="TransferIn" HeaderText="TransferIn"
                        HeaderButtonType="TextButton" DataField="TransferIn" UniqueName="TransferIn"
                        FilterControlWidth="100px" HeaderStyle-Width="175px" DataFormatString="{0:N2}">
                    </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn SortExpression="TransferOut" HeaderText="TransferOut"
                        HeaderButtonType="TextButton" DataField="TransferOut" UniqueName="TransferOut"
                        FilterControlWidth="100px" HeaderStyle-Width="175px" DataFormatString="{0:N2}">
                    </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn SortExpression="Income" HeaderText="Income"
                        HeaderButtonType="TextButton" DataField="Income" UniqueName="Income"
                        FilterControlWidth="100px" HeaderStyle-Width="175px" DataFormatString="{0:N2}">
                    </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn SortExpression="Investments" HeaderText="Investments"
                        HeaderButtonType="TextButton" DataField="Investments" UniqueName="Investments"
                        FilterControlWidth="100px" HeaderStyle-Width="175px" DataFormatString="{0:N2}">
                    </telerik:GridBoundColumn>
                      <telerik:GridBoundColumn SortExpression="FeesTaxes" HeaderText="FeesTaxes"
                        HeaderButtonType="TextButton" DataField="FeesTaxes" UniqueName="FeesTaxes"
                        FilterControlWidth="100px" HeaderStyle-Width="175px" DataFormatString="{0:N2}">
                    </telerik:GridBoundColumn>
                      <telerik:GridBoundColumn SortExpression="InternalCashMovement" HeaderText="InternalCashMovement"
                        HeaderButtonType="TextButton" DataField="InternalCashMovement" UniqueName="InternalCashMovement"
                        FilterControlWidth="100px" HeaderStyle-Width="175px" DataFormatString="{0:N2}">
                    </telerik:GridBoundColumn>
                      <telerik:GridBoundColumn SortExpression="InvestmentReturn" HeaderText="InvestmentReturn"
                        HeaderButtonType="TextButton" DataField="InvestmentReturn" UniqueName="InvestmentReturn"
                        FilterControlWidth="100px" HeaderStyle-Width="175px" DataFormatString="{0:N2}">
                    </telerik:GridBoundColumn>
                      <telerik:GridBoundColumn SortExpression="ClosingBalance" HeaderText="ClosingBalance"
                        HeaderButtonType="TextButton" DataField="ClosingBalance" UniqueName="ClosingBalance"
                        FilterControlWidth="100px" HeaderStyle-Width="175px" DataFormatString="{0:N2}">
                    </telerik:GridBoundColumn>
                </Columns>
                <GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldName="TradeDate" FieldAlias="Month" FormatString="{0:MMM-yyyy}">
                            </telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldName="TradeDate" SortOrder="Descending"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>
            </MasterTableView>
        </telerik:RadGrid>
    </fieldset>
</asp:Content>
