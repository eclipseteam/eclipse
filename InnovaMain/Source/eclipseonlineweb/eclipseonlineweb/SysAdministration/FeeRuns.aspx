﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="FeeRuns.aspx.cs" Inherits="eclipseonlineweb.FeeRuns" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="Panel1" runat="server">
        <fieldset>
            <table width="100%">
                <tr>
                    <td width="4%">
                        <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                            ID="btnDownload" />
                    </td>
                    <td width="100%" class="breadcrumbgap">
                        <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                        <br />
                        <asp:Label Font-Bold="true" runat="server" ID="lblSecurities" Text="Fees"></asp:Label>
                        <asp:HiddenField ID="hfSecID" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" OnDetailTableDataBind="PresentationGrid_DetailTableDataBind"
                OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnItemUpdated="PresentationGrid_ItemUpdated" OnItemDeleted="PresentationGrid_ItemDeleted"
                OnItemInserted="PresentationGrid_ItemInserted" OnInsertCommand="PresentationGrid_InsertCommand"
                EnableViewState="true" ShowFooter="true" OnItemCreated="PresentationGrid_ItemCreated"
                OnItemDataBound="PresentationGrid_ItemDataBound">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="ENTITYCIID_FIELD" AllowMultiColumnSorting="True" Width="100%"
                    CommandItemDisplay="Top" Name="FeeRuns" TableLayout="Fixed" EditMode="EditForms">
                    <CommandItemSettings AddNewRecordText="Add New Fee Run" ShowExportToExcelButton="true"
                        ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                    <Columns>
                        <telerik:GridBoundColumn ReadOnly="true" Display="false" DataField="ENTITYCIID_FIELD"
                            UniqueName="ENTITYCIID_FIELD" />
                        <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="20%" ItemStyle-Width="20%"
                            SortExpression="ENTITYNAME_FIELD" HeaderText="Name" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="ENTITYNAME_FIELD" UniqueName="ENTITYNAME_FIELD" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                            SortExpression="SHORTNAME" HeaderText="Short Name" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="SHORTNAME" UniqueName="SHORTNAME">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                            SortExpression="RUNTYPE" HeaderText="Run Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="RUNTYPE" UniqueName="RUNTYPE"
                            ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn HeaderText="Month" SortExpression="MONTH" UniqueName="MONTHCOMBO"
                            DataField="MONTH" Visible="false" ColumnEditorID="GridDropDownColumnEditorMonth" />
                        <telerik:GridBoundColumn FilterControlWidth="80px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                            SortExpression="MONTH" HeaderText="Month" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="MONTH" UniqueName="MONTH"
                            ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80px" DefaultInsertValue="2012" HeaderStyle-Width="10%"
                            ItemStyle-Width="10%" SortExpression="YEAR" HeaderText="Year" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="YEAR" UniqueName="YEAR" ReadOnly="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="80px" Aggregate="Sum" HeaderStyle-Width="10%"
                            ItemStyle-Width="10%" SortExpression="TOTALFEES" HeaderText="Total Fees" AutoPostBackOnFilter="true"
                            DataFormatString="{0:c}" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="TOTALFEES" UniqueName="TOTALFEES" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn CommandName="Run" Text="Run Fee" ButtonType="LinkButton"
                            HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        </telerik:GridButtonColumn>
                        <telerik:GridTemplateColumn UniqueName="FeeTemplateDetails" FilterControlWidth="5%"
                            ShowFilterIcon="false">
                            <HeaderStyle Width="7%" />
                            <ItemStyle Width="7%" />
                            <ItemTemplate>
                                <asp:LinkButton ID="Definition" runat="server" Text="Details" CommandArgument='<%# Eval("ENTITYCIID_FIELD") +"," + Eval("ENTITYCIID_FIELD")%>'
                                    CommandName="Details">
                                </asp:LinkButton>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridButtonColumn ConfirmText="Delete these details record?" ButtonType="ImageButton"
                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn2">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="Html"></Excel>
                </ExportSettings>
            </telerik:RadGrid>
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorMonth" runat="server"
                DropDownStyle-Width="146px">
            </telerik:GridDropDownListColumnEditor>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
