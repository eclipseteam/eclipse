﻿<%@ Page Title="" Language="C#" MasterPageFile="AdminMaster.master" AutoEventWireup="true"
    CodeBehind="ImportTermDepositPriceList.aspx.cs" Inherits="eclipseonlineweb.ImportTermDepositPriceList"
    EnableViewState="false" %>

<%@ Register TagPrefix="uc" TagName="Details" Src="WebControls/ImportMessageControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .padl
        {
            padding-left: 90px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            var td_file = document.getElementById('td_file');
            td_file.checked = true;

            var hdnCriteria = $find("<%: hdnCriteria.ClientID %>");
            if (hdnCriteria != null && hdnCriteria.val == 'amm')
                SetControlsOnImportWebServiceClick();
        });

        function SetControlsOnImportWebServiceClick() {
            var td_service = document.getElementById('td_service');
            td_service.checked = true;
            importTD(1);
        }

        function importTD(val) {
            var fTDImportFromFile = document.getElementById('fTDImportFromFile');
            var fTDImportFromService = document.getElementById('fTDImportFromService');
            var cboInstitute = $find("<%: cmbSelectInstitute.ClientID %>");
            var cboRateEffectiveFor = $find("<%: cmbSelectEffectiveFor.ClientID %>");
            switch (val) {
                case 0:
                    fTDImportFromFile.style.display = 'inline';
                    fTDImportFromService.style.display = 'none';
                    cboInstitute.enable();
                    cboRateEffectiveFor.enable();
                    break;
                case 1:
                    fTDImportFromFile.style.display = 'none';
                    fTDImportFromService.style.display = 'inline';
                    var itm = cboInstitute.findItemByText('AMM');
                    itm.select();
                    cboInstitute.disable();
                    cboRateEffectiveFor.disable();
                    break;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td class="breadcrumbgap">
                        <fieldset>
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Import TD Price List"></asp:Label>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Import Criteria</legend>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <input type="radio" name="group" id="td_file" onchange="importTD(0)" checked="checked">File</input>&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="group" id="td_service" onchange="importTD(1)">Web Service</input>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblSelectType" runat="server" Font-Bold="True" Text="Institutions:"></asp:Label>
                                    </td>
                                    <td>
                                        <telerik:RadComboBox ID="cmbSelectInstitute" runat="server">
                                        </telerik:RadComboBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblSelectDate" CssClass="padl" runat="server" Font-Bold="True" Text="Select Date:"></asp:Label>
                                    </td>
                                    <td>
                                        <telerik:RadDatePicker ID="dtPicker" runat="server" Culture="en-AU">
                                            <DateInput ID="dtPrice" DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy" runat="server" />
                                        </telerik:RadDatePicker>
                                    </td>
                                    <td style="width: 35%">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblRateEffectiveFor" runat="server" Font-Bold="True" Text="Rate effective for :"></asp:Label>
                                    </td>
                                    <td>
                                        <telerik:RadComboBox ID="cmbSelectEffectiveFor" runat="server">
                                            <Items>
                                                <telerik:RadComboBoxItem runat="server" Selected="True" Text="---" Value="-1" />
                                                <telerik:RadComboBoxItem runat="server" Text="UMA" Value="0" />
                                                <telerik:RadComboBoxItem runat="server" Text="Super" Value="10" />
                                            </Items>
                                        </telerik:RadComboBox>
                                   </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <asp:Label ID="lblMessage" CssClass="padl" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <br />
                        <fieldset id="fTDImportFromFile">
                            <legend>File</legend>
                            <asp:Label ID="lblFile" runat="server" Font-Bold="True" Text="File:"></asp:Label>
                            <asp:FileUpload Height="23px" ID="filMyFile" runat="server" />
                            <asp:Button ID="cmdSend" runat="server" Width="92px" Text="Upload" OnClick="BtnUploadClick" />
                            <asp:RegularExpressionValidator ID="rexp" runat="server" ControlToValidate="filMyFile"
                                ErrorMessage="File type should be csv (.csv)" ValidationExpression="(.*\.([Cc][Ss][Vv])$)"
                                ForeColor="#FF3300"></asp:RegularExpressionValidator>
                        </fieldset>
                        <fieldset id="fTDImportFromService" style="display: none;">
                            <legend>Web Service</legend>
                            <asp:Button ID="btnImport" runat="server" Text="Import from AMM Web Service" OnClick="btnImport_OnClick" />
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel runat="server" ID="PnlResults">
                            <uc:Details ID="ImportMessageControl" runat="server" />
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnCriteria" runat="server" EnableViewState="true" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="cmdSend" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
