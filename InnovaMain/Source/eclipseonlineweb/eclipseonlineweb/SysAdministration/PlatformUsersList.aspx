﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="PlatformUsersList.aspx.cs" Inherits="eclipseonlineweb.PlatformUsersList" %>


<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript">
        //Resetting menu index
        localStorage['MenuIndex'] = '0';
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <%-- <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
       
        <ContentTemplate>--%>
    <fieldset>
        <table width="100%">
            <tr>
                <td width="20%">
                    <telerik:RadButton runat="server" ID="btnDownload" ToolTip="Export" OnClick="DownloadXLS"
                        Visible="false">
                        <ContentTemplate>
                            <img src="../images/download.png" alt="" class="btnImageWithText" />
                            <span class="riLabel">Export</span>
                        </ContentTemplate>
                    </telerik:RadButton>
                </td>
                <td width="100%" class="breadcrumbgap">
                    <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                    <br />
                    <asp:Label Font-Bold="true" runat="server" ID="lblAdministrationUsersList" Text="PLATFORM USERS LIST"></asp:Label>
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <fieldset>
        <legend>PLATFORM USERS</legend>
        <asp:Panel runat="server" ID="pnlMainGrid" Visible="true">
            <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
                runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnInsertCommand="PresentationGrid_InsertCommand"
                EnableViewState="true" ShowFooter="false" OnItemCreated="PresentationGrid_ItemCreated"
                OnItemDataBound="PresentationGrid_ItemDataBound">
                <ExportSettings ExportOnlyData="true">
                    <Excel Format="Biff" />
                </ExportSettings>
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="CID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="Users" TableLayout="Fixed">
                    <CommandItemSettings AddNewRecordText="Add New User" ShowExportToExcelButton="true"
                        ShowExportToWordButton="true" ShowExportToPdfButton="true"></CommandItemSettings>
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridEditCommandColumn>
                        <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="IFACID" HeaderStyle-Width="5%"
                            ItemStyle-Width="5%" HeaderText="IFACID" ReadOnly="true" Display="false" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="IFACID" UniqueName="IFACID">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="150px" SortExpression="AdviserCID" HeaderStyle-Width="5%"
                            ItemStyle-Width="5%" HeaderText="AdviserCID" ReadOnly="true" Display="false"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="AdviserCID" UniqueName="AdviserCID">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" SortExpression="Username" HeaderStyle-Width="17%"
                            ItemStyle-Width="17%" HeaderText="User Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Username" UniqueName="Username">
                        </telerik:GridBoundColumn>
                       <%-- <telerik:GridCheckBoxColumn HeaderText="Password Change" Visible="False" UniqueName="ChkChangePassword" />--%>
                        <telerik:GridBoundColumn FilterControlWidth="50px" ReadOnly="false" HeaderStyle-Width="7%"
                            ItemStyle-Width="7%" SortExpression="Password" HeaderText="Password" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" Display="false" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Password" UniqueName="Password">
                        </telerik:GridBoundColumn>
                        <telerik:GridDropDownColumn HeaderText="User Type" SortExpression="UserTypeString"
                            UniqueName="UserTypeStringEditor" DataField="UserTypeString" Visible="false"  ListValueField = "Key" ListTextField = "Value"
                            ColumnEditorID="GridDropDownColumnEditorModelUserTypeList" />
                        <telerik:GridDropDownColumn HeaderText="Adviser Name" SortExpression="AdviserID"
                            UniqueName="AdviserTypeEditor" DataField="AdviserID" Visible="false"  ColumnEditorID="GridDropDownColumnEditorModelAdviserList" ListTextField="ENTITYNAME_FIELD" ListValueField="ENTITYCIID_FIELD" />
                        <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                            SortExpression="Firstname" HeaderText="First Name" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Firstname" UniqueName="Firstname" ReadOnly="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                            SortExpression="Lastname" HeaderText="Last Name" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="Lastname" UniqueName="Lastname" ReadOnly="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="50px" ReadOnly="true" HeaderStyle-Width="8%"
                            ItemStyle-Width="8%" SortExpression="AdviserID" HeaderText="Adviser ID" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="AdviserID" UniqueName="AdviserID">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="100px" ReadOnly="true" HeaderStyle-Width="15%"
                            ItemStyle-Width="15%" SortExpression="IFANAME" HeaderText="IFA Name" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="IFANAME" UniqueName="IFANAME">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="50px" ReadOnly="true" HeaderStyle-Width="8%"
                            ItemStyle-Width="8%" SortExpression="UserTypeString" HeaderText="User Type" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                            DataField="UserTypeString" UniqueName="UserTypeString">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="120px" ReadOnly="false" HeaderStyle-Width="17%"
                            ItemStyle-Width="17%" SortExpression="EmailAddress" HeaderText="Email Address"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="EmailAddress" UniqueName="EmailAddress">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="0px" ReadOnly="true" HeaderStyle-Width="4%"
                            ItemStyle-Width="4%" SortExpression="Locked" AllowFiltering="false" HeaderText="Locked"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                            HeaderButtonType="TextButton" DataField="Locked" UniqueName="Locked">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="0px" ReadOnly="true" HeaderStyle-Width="3%"
                            ItemStyle-Width="2%" SortExpression="Active" AllowFiltering="false" HeaderText="Active"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                            HeaderButtonType="TextButton" DataField="Active" UniqueName="Active">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ConfirmText="Delete this user?" ButtonType="ImageButton"
                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="Html"></Excel>
                </ExportSettings>
            </telerik:RadGrid>
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorModelAdviserList"
                runat="server" DropDownStyle-Width="200px"/>
            <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditorModelUserTypeList"
                runat="server" DropDownStyle-Width="200px"/>
          
        </asp:Panel>
    </fieldset>
    <%--  </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
