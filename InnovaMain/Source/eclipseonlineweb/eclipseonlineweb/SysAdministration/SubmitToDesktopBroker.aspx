﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="SubmitToDesktopBroker.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.SubmitToDesktopBroker" %>

<%@ Register Src="../Controls/PendingOrders.ascx" TagName="PendingOrders" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:PendingOrders ID="PendingOrdersControl" runat="server" />
</asp:Content>
