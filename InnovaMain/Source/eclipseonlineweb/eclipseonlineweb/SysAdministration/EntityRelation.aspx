﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/SysAdministration/AdminMaster.master"
    CodeBehind="EntityRelation.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.EntityRelation" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        /** Customize the demo canvas */
        /** .qsf-demo-canvas{} */
        
        /** Customize the org chart */
        html .RadOrgChart .rocImageWrap, html .RadOrgChart .rocImageWrap img
        {
            width: 60px !important;
            height: 70px !important;
        }
        
        /** html .RadOrgChart .rocItem{} */
        /** html .RadOrgChart .rocItemContent{} */
    </style>
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>
    <div>
        <telerik:RadOrgChart ID="RadOrgChart1" runat="server" EnableCollapsing="True" LoadOnDemand="NodesAndGroups">
            <ItemTemplate>
                <table style="border-spacing: 0px; padding: 0px; background-color: #7CC6E9; color: black;">
                    <tr>
                        <td rowspan="2" style="width: 60px;">
                            <img src='<%#DataBinder.Eval(Container.DataItem, "ImageUrl")%>' alt="image" width="60px"
                                height="70px" />
                        </td>
                        <td style="padding-left: 8px; width: 100%">
                            <%#DataBinder.Eval(Container.DataItem, "EntityName")%>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 8px; width: 100%">
                            <a href='<%# DataBinder.Eval(Container.DataItem, "PrefixUrl")%>' target="_blank">
                                <%#DataBinder.Eval(Container.DataItem, "Description")%></a>
                            <asp:HiddenField runat="server" ID="hfCid" Value='<%#DataBinder.Eval(Container.DataItem, "Cid")%>' />
                            <asp:HiddenField runat="server" ID="hfClid" Value='<%#DataBinder.Eval(Container.DataItem, "Clid")%>' />
                            <asp:HiddenField runat="server" ID="hfCsid" Value='<%#DataBinder.Eval(Container.DataItem, "Csid")%>' />
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </telerik:RadOrgChart>
    </div>
</asp:Content>
