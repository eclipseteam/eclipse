﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="PendingOrdersNew.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.PendingOrdersNew" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="PresentationGrid">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PresentationGrid"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="OrderDetails" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="btnUMA">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PresentationGrid"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="OrderDetails" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                     <telerik:AjaxUpdatedControl ControlID="btnUMA" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                     <telerik:AjaxUpdatedControl ControlID="btnSMA" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSMA">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PresentationGrid"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="OrderDetails" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                     <telerik:AjaxUpdatedControl ControlID="btnUMA" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                     <telerik:AjaxUpdatedControl ControlID="btnSMA" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="OrderDetails">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PresentationGrid"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="OrderDetails" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <fieldset>
        <table width="100%">
            <tr>
                <td width="75%">
                </td>
                <td style="width: 25%;" class="breadcrumbgap">
                    <uc1:BreadCrumb ID="BreadCrumb2" runat="server" />
                    <br />
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset id="OrderDetails" runat="server" visible="false">
        <legend>Order Details</legend>
        <table width="100%">
            <tr>
                
                <td align="left">
                     <telerik:RadTextBox  runat="server" ID="TxtOrderID" Text="" ReadOnly="True" LabelWidth="120px" Width="250" Label="Order Number:" />
                </td>
                
                <td align="left">
                     <telerik:RadTextBox   ReadOnly="True" runat="server" ID="TxtStatus" Text="" LabelWidth="120px" Width="250" Label="Status:"/>
                </td>
              
                <td align="left">
                    <telerik:RadTextBox  ReadOnly="True" runat="server" ID="TxtOrderType" Text="" LabelWidth="120px" Width="250" Label="Order Type:"/>
                </td>
            </tr>
            <tr>
                
                <td align="left">
                    <telerik:RadTextBox   ReadOnly="True" runat="server" ID="TxtClientID" Text="" LabelWidth="120px" Width="250" Label="Client ID:"/>
                </td>
           
                <td align="left">
                    <telerik:RadTextBox   ReadOnly="True" runat="server" ID="TxtItemType" Text="" LabelWidth="120px" Width="250" Label="Item Type:"/>
                </td>
              
                <td align="left">
                    <telerik:RadTextBox  ReadOnly="True" runat="server" ID="TxtRequestBy" Text="" LabelWidth="120px" Width="250" Label="Requested By:" />
                </td>
            </tr>
            <tr>
           
                <td align="left">
                    <telerik:RadTextBox   ReadOnly="True" runat="server" ID="TxtOrderAccountType" Text="" LabelWidth="120px" Width="250" Label="Order Account Type:"/>
                </td>
              
                <td align="left">
                     <telerik:RadTextBox   ReadOnly="True" runat="server" ID="TxtValue" Text="" LabelWidth="120px" Width="250" Label="Value:" />
                </td>
             
                <td align="left">
                    <telerik:RadTextBox   ReadOnly="True" runat="server" ID="TxtBulkStatus" Text="" LabelWidth="120px" Width="250" Label="Bulk Status:"/>
                </td>
            </tr>
            <tr>
               
                <td align="left">
                    <telerik:RadTextBox   ReadOnly="True" runat="server" ID="TxtTradeDate" Text="" LabelWidth="120px" Width="250" Label="Trade Date:"/>
                </td>
              
                <td align="left">
                     <telerik:RadTextBox   ReadOnly="True" runat="server" ID="TxtCreatedDate" Text="" LabelWidth="120px" Width="250" Label="Created Date:"/>
                </td>
                
                <td align="left">
                   <telerik:RadTextBox   ReadOnly="True" runat="server" ID="TxtUpdatedDate" Text="" LabelWidth="120px" Width="250" Label="Updated Date:"/>
                </td>
            </tr>
            <tr>
              
                <td align="left">
                     <telerik:RadTextBox   ReadOnly="True" runat="server" ID="TxtCreatedBy" Text="" LabelWidth="120px" Width="250" Label="Created By:"/>
                </td>
                
                <td align="left">
                    <telerik:RadTextBox   ReadOnly="True" runat="server" ID="TxtSMARequestID" Text="" LabelWidth="120px" Width="250" Label="Super Request ID:" />
                </td>
                
                <td align="left">
                    <telerik:RadTextBox   ReadOnly="True" runat="server" ID="TxtSMACallStatus" Text="" LabelWidth="120px" Width="250" Label="Super Call Status:"/>
                </td>
            </tr>
        </table>
        <table width="100%">
            <tr>
                <td>
                    <telerik:RadGrid ID="SSOrderDetails" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                        PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="false"
                        GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="false"
                        AllowAutomaticInserts="True" AllowAutomaticUpdates="True" EnableViewState="true"
                        ShowFooter="false" EnableLinqExpressions="false" Visible="false">
                        <MasterTableView Width="100%" Font-Size="8" AllowFilteringByColumn="False">
                            <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <Columns>
                                <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="false"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="ID" UniqueName="ID" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="OrderID" ReadOnly="true" HeaderText="OrderID"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="OrderID" UniqueName="OrderID" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="ProductID" ReadOnly="true" HeaderText="ProductID"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="ProductID" UniqueName="ProductID" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="80px" SortExpression="FundCode"
                                    ReadOnly="true" HeaderText="Fund Code" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="FundCode" UniqueName="FundCode">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="220px" HeaderStyle-Width="260px" SortExpression="FundName"
                                    ReadOnly="true" HeaderText="Fund Name" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="FundName" UniqueName="FundName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" SortExpression="Units"
                                    ReadOnly="true" HeaderText="Units" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Units" UniqueName="Units"
                                    DataFormatString="{0:N4}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Amount"
                                    ReadOnly="true" HeaderText="Amount" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Amount" UniqueName="Amount"
                                    DataFormatString="{0:C}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="CreatedDate"
                                    ReadOnly="true" HeaderText="CreatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedDate" UniqueName="CreatedDate"
                                    DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="UpdatedDate"
                                    ReadOnly="true" HeaderText="UpdatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UpdatedDate" UniqueName="UpdatedDate"
                                    DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="ValidationMsg" ReadOnly="true" HeaderText="Validation Msg"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="ValidationMsg" UniqueName="ValidationMsg">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadGrid ID="ASXOrderDetails" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                        PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="false"
                        GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="false"
                        AllowAutomaticInserts="True" AllowAutomaticUpdates="True" EnableViewState="true"
                        ShowFooter="false" EnableLinqExpressions="false" Visible="false">
                        <MasterTableView Width="100%" Font-Size="8" AllowFilteringByColumn="False">
                            <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <Columns>
                                <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="false"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="ID" UniqueName="ID" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="OrderID" ReadOnly="true" HeaderText="OrderID"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="OrderID" UniqueName="OrderID" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="ProductID" ReadOnly="true" HeaderText="ProductID"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="ProductID" UniqueName="ProductID" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" SortExpression="InvestmentCode"
                                    ReadOnly="true" HeaderText="Investment Code" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InvestmentCode"
                                    UniqueName="InvestmentCode">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="220px" HeaderStyle-Width="260px" SortExpression="InvestmentName"
                                    ReadOnly="true" HeaderText="Investment Name" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="InvestmentName"
                                    UniqueName="InvestmentName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="60px" HeaderStyle-Width="100px" SortExpression="Units"
                                    ReadOnly="true" HeaderText="Units" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Units" UniqueName="Units"
                                    DataFormatString="{0:N4}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="ASXUnitPrice"
                                    ReadOnly="true" HeaderText="Unit Price" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ASXUnitPrice"
                                    UniqueName="ASXUnitPrice" DataFormatString="{0:N6}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Amount"
                                    ReadOnly="true" HeaderText="Amount" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Amount" UniqueName="Amount"
                                    DataFormatString="{0:C}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="CreatedDate"
                                    ReadOnly="true" HeaderText="CreatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedDate" UniqueName="CreatedDate"
                                    DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="UpdatedDate"
                                    ReadOnly="true" HeaderText="UpdatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UpdatedDate" UniqueName="UpdatedDate"
                                    DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="ValidationMsg" ReadOnly="true" HeaderText="Validation Msg"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="ValidationMsg" UniqueName="ValidationMsg">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadGrid ID="TDOrderDetails" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                        PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="false"
                        GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="false"
                        AllowAutomaticInserts="True" AllowAutomaticUpdates="True" EnableViewState="true"
                        ShowFooter="false" EnableLinqExpressions="false" Visible="false">
                        <MasterTableView Width="100%" Font-Size="8" AllowFilteringByColumn="False">
                            <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <Columns>
                                <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="false"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="ID" UniqueName="ID" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="OrderID" ReadOnly="true" HeaderText="OrderID"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="OrderID" UniqueName="OrderID" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="BrokerID" ReadOnly="true" HeaderText="BrokerID"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="BrokerID" UniqueName="BrokerID" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="BrokerName" ReadOnly="true" HeaderText="Broker Name"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="BrokerName" UniqueName="BrokerName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="BankID" ReadOnly="true" HeaderText="BankID"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="BankID" UniqueName="BankID" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="BankName" ReadOnly="true" HeaderText="Bank Name"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="BankName" UniqueName="BankName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Min"
                                    ReadOnly="true" HeaderText="Min" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Min" UniqueName="Min"
                                    DataFormatString="{0:C}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Max"
                                    ReadOnly="true" HeaderText="Max" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Max" UniqueName="Max"
                                    DataFormatString="{0:C}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="80px" SortExpression="Rating"
                                    ReadOnly="true" HeaderText="Rating" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Rating" UniqueName="Rating">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="35px" HeaderStyle-Width="75px" SortExpression="Duration"
                                    ReadOnly="true" HeaderText="Duration" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Duration" UniqueName="Duration">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="80px" SortExpression="Percentage"
                                    ReadOnly="true" HeaderText="Percentage" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Percentage" UniqueName="Percentage"
                                    DataFormatString="{0:P}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="Units" ReadOnly="true" HeaderText="Units"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="Units" UniqueName="Units" Visible="false"
                                    DataFormatString="{0:N4}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Amount"
                                    ReadOnly="true" HeaderText="Amount" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Amount" UniqueName="Amount"
                                    DataFormatString="{0:C}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="CreatedDate"
                                    ReadOnly="true" HeaderText="CreatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedDate" UniqueName="CreatedDate"
                                    DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="UpdatedDate"
                                    ReadOnly="true" HeaderText="UpdatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UpdatedDate" UniqueName="UpdatedDate"
                                    DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="ValidationMsg" ReadOnly="true" HeaderText="Validation Msg"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="ValidationMsg" UniqueName="ValidationMsg">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadGrid ID="AtCallOrderDetails" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                        PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="false"
                        GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="false"
                        AllowAutomaticInserts="True" AllowAutomaticUpdates="True" EnableViewState="true"
                        ShowFooter="false" EnableLinqExpressions="false" Visible="false">
                        <MasterTableView Width="100%" Font-Size="8" AllowFilteringByColumn="False">
                            <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <Columns>
                                <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="false"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    DataField="ID" UniqueName="ID" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="OrderID" ReadOnly="true" HeaderText="OrderID"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="OrderID" UniqueName="OrderID" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="80px" SortExpression="TransferTo"
                                    ReadOnly="true" HeaderText="Transfer To" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransferTo" UniqueName="TransferTo">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="220px" HeaderStyle-Width="100px" SortExpression="TransferAccBSB"
                                    ReadOnly="true" HeaderText="BSB" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransferAccBSB"
                                    UniqueName="TransferAccBSB">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="220px" HeaderStyle-Width="120px" SortExpression="TransferAccNumber"
                                    ReadOnly="true" HeaderText="Account No" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="TransferAccNumber"
                                    UniqueName="TransferAccNumber">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="AtCallType" ReadOnly="true" HeaderText="AtCallType"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="AtCallType" UniqueName="AtCallType"
                                    Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="BrokerID" ReadOnly="true" HeaderText="BrokerID"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="BrokerID" UniqueName="BrokerID" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="BrokerName" ReadOnly="true" HeaderText="Broker Name"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="BrokerName" UniqueName="BrokerName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="BankID" ReadOnly="true" HeaderText="BankID"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="BankID" UniqueName="BankID" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="BankName" ReadOnly="true" HeaderText="Bank Name"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="BankName" UniqueName="BankName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="AccountTier" HeaderText="Description" AutoPostBackOnFilter="false"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                                    ReadOnly="true" DataField="AccountTier" UniqueName="AccountTier">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Min"
                                    ReadOnly="true" HeaderText="Min" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Min" UniqueName="Min"
                                    DataFormatString="{0:C}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Max"
                                    ReadOnly="true" HeaderText="Max" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Max" UniqueName="Max"
                                    DataFormatString="{0:C}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="180px" SortExpression="Rate"
                                    ReadOnly="true" HeaderText="Rate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Rate" UniqueName="Rate"
                                    DataFormatString="{0:P}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" SortExpression="Amount"
                                    ReadOnly="true" HeaderText="Amount" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Amount" UniqueName="Amount"
                                    DataFormatString="{0:C}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="CreatedDate"
                                    ReadOnly="true" HeaderText="CreatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedDate" UniqueName="CreatedDate"
                                    DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn FilterControlWidth="95px" HeaderStyle-Width="135px" SortExpression="UpdatedDate"
                                    ReadOnly="true" HeaderText="UpdatedDate" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="UpdatedDate" UniqueName="UpdatedDate"
                                    DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="ValidationMsg" ReadOnly="true" HeaderText="Validation Msg"
                                    AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="ValidationMsg" UniqueName="ValidationMsg">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <fieldset>
        <table width="100%">
            <tr>
                <td width="75%">
                    <telerik:RadButton ID="btnUMA" runat="server" ToggleType="Radio" ButtonType="ToggleButton"
                        Text="UMA" Value="UMA" GroupName="ClientManagementType" Checked="true" OnClick="btnSMA_OnClick">
                    </telerik:RadButton>
                    <telerik:RadButton ID="btnSMA" runat="server" ToggleType="Radio" Text="Super" Value="SMA" GroupName="ClientManagementType"
                        ButtonType="ToggleButton" OnClick="btnSMA_OnClick">
                    </telerik:RadButton>
                </td>
                <td style="width: 25%;" class="breadcrumbgap">
                </td>
            </tr>
        </table>
        <telerik:RadGrid OnNeedDataSource="PresentationGrid_NeedDataSource" ID="PresentationGrid"
            runat="server" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="20"
            AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true" GridLines="None"
            AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
            AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" HierarchyLoadMode="Client"
            EnableLinqExpressions="false" OnSelectedIndexChanged="PresentationGrid_OnSelectedIndexChanged">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <ClientSettings EnablePostBackOnRowClick="true">
                <Selecting AllowRowSelect="true"></Selecting>
                <Scrolling AllowScroll="false" />
            </ClientSettings>
            <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                DataKeyNames="ID" Name="ActiveOrders" TableLayout="Fixed" Font-Size="8">
                <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                <PagerStyle Mode="NextPrevNumericAndAdvanced" AlwaysVisible="True"></PagerStyle>
                <Columns>
                    <%-- because we are using client side and Server side Techniques so we need to remove link (removing after discussion with Farooqe).Samee--%>
                    <telerik:GridBoundColumn FilterControlWidth="45px" HeaderStyle-Width="75px" ItemStyle-Width="75px"
                        ReadOnly="true" SortExpression="OrderID" ShowFilterIcon="true" HeaderText="Order ID"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                        DataField="OrderID" UniqueName="OrderID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="ID" ReadOnly="true" HeaderText="ID" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="ID" UniqueName="ID" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="ClientCID"
                        SortExpression="ClientID" UniqueName="ClientID" DataNavigateUrlFormatString="../ClientViews/ClientMainView.aspx?ins={0}"
                        HeaderText="Client ID" DataTextField="ClientID" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" FilterControlWidth="50px" HeaderStyle-Width="85px"
                        HeaderButtonType="TextButton" ShowFilterIcon="true">
                    </telerik:GridHyperLinkColumn>
                    <telerik:GridBoundColumn SortExpression="AccountCID" ReadOnly="true" HeaderText="AccountCID"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="AccountCID" UniqueName="AccountCID"
                        Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="200px" SortExpression="AccountNumber"
                        ReadOnly="true" HeaderText="AccountNumber" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="AccountNumber"
                        UniqueName="AccountNumber" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="85px" SortExpression="OrderAccountType"
                        HeaderText="Account Type" ReadOnly="true" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="OrderAccountType"
                        UniqueName="OrderAccountType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="45px" HeaderStyle-Width="85px" ItemStyle-Width="85px"
                        ReadOnly="true" SortExpression="OrderType" ShowFilterIcon="true" HeaderText="Order Type"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                        DataField="OrderType" UniqueName="OrderType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="35px" HeaderStyle-Width="70px" ReadOnly="true"
                        SortExpression="OrderItemType" HeaderText="Item Type" AutoPostBackOnFilter="false"
                        ShowFilterIcon="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                        DataField="OrderItemType" UniqueName="OrderItemType">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="45px" HeaderStyle-Width="85px" ReadOnly="true"
                        SortExpression="OrderPreferedBy" HeaderText="Requested By" AutoPostBackOnFilter="false"
                        ShowFilterIcon="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                        DataField="OrderPreferedBy" UniqueName="OrderPreferedBy">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="35px" HeaderStyle-Width="70px" ReadOnly="true"
                        SortExpression="SMARequestID" HeaderText="Super Request ID" AutoPostBackOnFilter="false"
                        ShowFilterIcon="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                        DataField="SMARequestID" UniqueName="SMARequestID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="35px" HeaderStyle-Width="70px" ReadOnly="true"
                        SortExpression="SMACallStatus" HeaderText="Super Call Status" AutoPostBackOnFilter="false"
                        ShowFilterIcon="true" CurrentFilterFunction="Contains" HeaderButtonType="TextButton"
                        DataField="SMACallStatus" UniqueName="SMACallStatus">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn FilterControlWidth="60px" HeaderStyle-Width="95px" ReadOnly="true"
                        SortExpression="Status" HeaderText="Status" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Status" UniqueName="Status">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="hypLinkStatus" Text='<%# Bind("Status") %>'></asp:HyperLink>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn FilterControlWidth="40px" HeaderStyle-Width="75px" SortExpression="OrderBulkStatus"
                        ReadOnly="true" HeaderText="Bulk Status" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="OrderBulkStatus"
                        UniqueName="OrderBulkStatus">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="70px" HeaderStyle-Width="110px" ReadOnly="true"
                        SortExpression="Value" HeaderText="Value" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="Value" UniqueName="Value"
                        DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="50px" HeaderStyle-Width="85px" ReadOnly="true"
                        SortExpression="TradeDate" HeaderText="Trade Date" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="TradeDate" UniqueName="TradeDate" DataFormatString="{0:dd/MM/yyyy}"
                        DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="90px" HeaderStyle-Width="90px" SortExpression="CreatedBy"
                        ReadOnly="true" HeaderText="Created By" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="CreatedBy" UniqueName="CreatedBy">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="65px" HeaderStyle-Width="100px" ReadOnly="true"
                        SortExpression="CreatedDate" HeaderText="Create Date " AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="CreatedDate" UniqueName="CreatedDate" DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}"
                        DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlWidth="65px" HeaderStyle-Width="100px" ReadOnly="true"
                        SortExpression="UpdatedDate" HeaderText="Update Date" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                        DataField="UpdatedDate" UniqueName="UpdatedDate" DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}"
                        DataType="System.DateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderStyle-Width="120px" SortExpression="ValidationMsg"
                        ReadOnly="true" HeaderText="Validation Msg" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ValidationMsg"
                        UniqueName="ValidationMsg">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="UpdatedBy" ReadOnly="true" HeaderText="UpdatedBy"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="UpdatedBy" UniqueName="UpdatedBy" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="UpdatedByCID" ReadOnly="true" HeaderText="UpdatedByCID"
                        AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        HeaderButtonType="TextButton" DataField="UpdatedByCID" UniqueName="UpdatedByCID"
                        Visible="false">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </fieldset>
</asp:Content>
