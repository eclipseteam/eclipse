﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using eclipseonlineweb.WebControls;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using System.Drawing;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public partial class ImportProductSecuritiesPriceList : UMABasePage
    {
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }

        protected override void Intialise()
        {
            ImportMessageControl.GridImportMessages.ColumnCreated += GridImportMessages_ColumnCreated;
            ImportMessageControl.GirdInvestmentCodeError.ColumnCreated += GridImportErrorMessages_ColumnCreated;
            ImportMessageControl.GridImportMessages.ItemDataBound += new GridItemEventHandler(GridImportMessages_ItemDataBound);
            if (!IsPostBack)
            {
                ImportMessageControl.Clear();
            }

            ImportMessageControl.Completed += text =>
                                                  {
                                                      switch (text.ToLower())
                                                      {
                                                          case "next":
                                                              SendData();
                                                              break;
                                                          default:
                                                              ClearMessageControls();
                                                              break;
                                                      }
                                                  };

           
        }
        void GridImportMessages_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item).ItemIndex != -1)
            {
                DataRow dr = ((DataRowView)((e.Item).DataItem)).Row;
                if (bool.Parse(dr["HasErrors"].ToString()))
                {
                    e.Item.ForeColor = Color.Red;
                }
            }

        }

        void GridImportMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {

           
            SetDataGridColumns(e);
        }

        void GridImportErrorMessages_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
           
            SetDataGridColumns(e);
        }

        public void ClearMessageControls()
        {
            ImportMessageControl.Clear();
            ImportMessageControl.TextMessage.Text = "";
        }
        public void BtnUploadClick(object sender, EventArgs e)
        {
            lblMessage.Text = "";
            bool haserrors = false;
            if (cmbProductSecurity.SelectedItem.Value == Guid.Empty.ToString())
            {
               lblMessage.Text = ImportMessages.sSelectProductSecurity;
               haserrors = true;
            }                            
            if (haserrors)
               return;
            try
            {
                if (filMyFile.PostedFile != null)
                {
                    // Get a reference to PostedFile object
                    HttpPostedFile myFile = filMyFile.PostedFile;

                      // Get size of uploaded file
                    int nFileLen = myFile.ContentLength;

                    // make sure the size of the file is > 0
                    if (nFileLen > 0)
                    {
                        // Allocate a buffer for reading of the file
                        string fileName = Path.GetFileName(myFile.FileName);
                        var date = DateTime.Now;
                        var DistinationDirectory = Server.MapPath("~/App_Data/Import/");
                        if (!Directory.Exists(DistinationDirectory))
                        {
                            Directory.CreateDirectory(DistinationDirectory);
                        }

                        string fileLocation = DistinationDirectory + date.ToString("ddMMyyhhmmsstt") + fileName;
                        byte[] myData = new byte[nFileLen];

                        // Read uploaded file from the Stream
                        myFile.InputStream.Read(myData, 0, nFileLen);

                        // Write data into a file
                        Utilities.WriteToFile(fileLocation, ref myData);
                        string csv = string.Empty;
                        using (StreamReader reader = new StreamReader(fileLocation, true))
                        {
                            csv = reader.ReadToEnd();
                            reader.Close();
                        }
                        string PSPrice = ConversorTransactionCsv.SelectRows(csv, "");
                        DataTable dt = new DataTable();
                        ImportProcessDS ds = new ImportProcessDS();
                        ds.ExtendedProperties.Add("ProductSecurityID", cmbProductSecurity.SelectedValue);
                        dt = ConversorTransactionCsv.CreateProductSecurityPriceList(PSPrice);
                        ds.Command = (int)WebCommands.ValidateProductSecurityPriceListDataFeedImportFile;                           
                       ds.Tables.Add(dt);
                       ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
                       ds.FileName = fileName;
                       SaveOrganizanition(ds);
                       SetValidationGird(ds);
                       PresentationData = ds;
                       string filename = ImportMessageControl.RefreshFileID() + ".xml";
                       ds.WriteXml(DistinationDirectory + filename, XmlWriteMode.WriteSchema);
                     }
               }
               
            }
            catch 
            {
                lblMessage.Text = ImportMessages.sFileTypeMismatch;
            }
            // Check to see if file was uploaded
           
        }
        private void SetValidationGird(ImportProcessDS ds)
        {
                //ImportMessageControl.Clear();
                using (DataView dv = new DataView(ds.Tables[0]))
                {
                    ImportMessageControl.GridImportMessages.DataSource = dv;
                    ImportMessageControl.GridImportMessages.DataBind();
                }
                using (DataView dv = new DataView(ds.Tables[0]))
                {
                    dv.RowFilter = "IsMissingItem='true'";
                    DataTable missingItems = new DataTable();
                    missingItems.Columns.Add("MissingItem");
                    DataRow dr;
                    foreach (DataRow row in dv.ToTable().Rows)
                    {
                        dr = missingItems.NewRow();
                        dr["MissingItem"] = row["StockCode"];
                        missingItems.Rows.Add(dr);
                    }

                    if (missingItems.Rows.Count > 0)
                    {
                        ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                        ImportMessageControl.GirdInvestmentCodeError.DataBind();
                        ImportMessageControl.TextMessage.Text = ImportMessages.sMissingCodeAdded;                       
                    }
                    else
                    {
                        ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                        ImportMessageControl.GirdInvestmentCodeError.DataBind();
                        ImportMessageControl.TextMessage.Text = "";
                    }
                }
                int successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
                if (ds.Tables[0].Rows.Count == 0 || successfulRows == 0)
                {
                    ImportMessageControl.SetButtons(ButtonTypes.CLOSE);
                    ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sErrorsCannotContinue, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Error);
                    ImportMessageControl.TextMessage.Text = "";
                }
                else
                {
                    ImportMessageControl.SetButtons(ButtonTypes.NEXTCANCEL);
                    //sucess
                    ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sValidationCompleted, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
                }
            }
        private void SendData()
            {
                bool haserrors = false;
                var distributionDictory = Server.MapPath("~/App_Data/Import/");
                if (cmbProductSecurity.SelectedItem.Value == Guid.Empty.ToString())
                {
                    lblMessage.Text = ImportMessages.sSelectProductSecurity;
                    haserrors = true;
                }
                if (haserrors)
                    return;
    
            if (!Directory.Exists(distributionDictory))
                {
                    Directory.CreateDirectory(distributionDictory);
                }
                string filename = distributionDictory + ImportMessageControl.GetFileID() + ".xml";
                ImportProcessDS ds = new ImportProcessDS();

                ds.ReadXml(filename);
              
                if (ds.ExtendedProperties.Contains("ProductSecurityID"))
                    ds.ExtendedProperties["ProductSecurityID"] = cmbProductSecurity.SelectedValue;
                else
                    ds.ExtendedProperties.Add("ProductSecurityID", cmbProductSecurity.SelectedValue);
                ds.Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = GetCurrentUser() };
                ds.Command = (int)WebCommands.ImportProductSecurityPriceListFeedImportFile;                        
                ds.FileName = filename;
                SaveOrganizanition(ds);
                SetCompletionGird(ds);
            }                          
        protected override void GetBMCData()
        {
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            PROSecuritiesDS pROSecuritiesDS = new Oritax.TaxSimp.DataSets.PROSecuritiesDS();
            organization.GetData(pROSecuritiesDS);            
            FillProductSecurityDropDown(pROSecuritiesDS);            
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }
        private void FillProductSecurityDropDown(PROSecuritiesDS ds)
        {
            string selval = cmbProductSecurity.SelectedValue;
            cmbProductSecurity.ClearSelection();            
            cmbProductSecurity.DataSource = ds.Tables[PROSecuritiesDS.PROSECLISTTABLE];
            cmbProductSecurity.DataTextField = "Name";
            cmbProductSecurity.DataValueField = "ID";
            cmbProductSecurity.DataBind();
            RadComboBoxItem item = new RadComboBoxItem();
            item.Text = "--Select Product Security--";
            item.Value = Guid.Empty.ToString();
            cmbProductSecurity.Items.Insert(0, item);
            cmbProductSecurity.SelectedValue = selval;
            
        }        
        private void SetDataGridColumns(GridColumnCreatedEventArgs e)
        {
            string colName = e.Column.UniqueName.ToLower();
            switch (colName)
                {
                    case "StockCode":
                        e.Column.HeaderText = "Stock Code";
                        break;
                    case "Market":
                        e.Column.HeaderText = "Market";
                        e.Column.FilterControlWidth = Unit.Pixel(50);
                        break;
                    case "Currency":
                        e.Column.HeaderText = "Currency";
                        e.Column.FilterControlWidth = Unit.Pixel(50);
                        break;
                    case "Recommendation":
                        e.Column.HeaderText = "Recommendation";
                        e.Column.FilterControlWidth = Unit.Pixel(170);
                        break;
                    case "Weighting":
                        e.Column.HeaderText = "Weighting";
                        break;
                    case "Comment":
                        e.Column.HeaderText = "Comment";
                        break;

                    case "BuyPrice":
                        e.Column.HeaderText = "Buy Price";
                        break;
                    case "SellPrice":
                        e.Column.HeaderText = "Sell Price";
                        break;
                    case "DynamicRatingOption":
                        e.Column.HeaderText = "Dynamic Rating Option";
                        break;                    
                    case "message":
                        e.Column.HeaderText = "Message";
                        break;
                    case "haserrors":
                        e.Column.HeaderText = "Has Errors";
                        e.Column.Visible = false;
                        break;
                    case "ismissingitem":
                        e.Column.HeaderText = "Is Missing Item";
                        e.Column.Visible = false;
                        break;
                    case "missingitem":
                        e.Column.HeaderText = "Missing Stock Code(s)";
                        break;
                }
            }
        private void SetCompletionGird(ImportProcessDS ds)
        {
            ImportMessageControl.Clear();
            using (DataView dv = new DataView(ds.Tables[0]))
            {
                ImportMessageControl.GridImportMessages.DataSource = dv;
                ImportMessageControl.GridImportMessages.DataBind();
            }

            using (DataView dv = new DataView(ds.Tables[0]))
            {
                dv.RowFilter = "IsMissingItem='true'";
                DataTable missingItems = new DataTable();
                missingItems.Columns.Add("MissingItem");
                DataRow dr;
                foreach (DataRow row in dv.ToTable().Rows)
                {
                    dr = missingItems.NewRow();
                    dr["MissingItem"] = row["StockCode"];
                    missingItems.Rows.Add(dr);
                }

                if (missingItems.Rows.Count > 0)
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = missingItems;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = ImportMessages.sMissingStockCodeAdded;                  
                }
                else
                {
                    ImportMessageControl.GirdInvestmentCodeError.DataSource = null;
                    ImportMessageControl.GirdInvestmentCodeError.DataBind();
                    ImportMessageControl.TextMessage.Text = "";
                }
            }
            int successfulRows = ds.Tables[0].Select("HasErrors='false'").Length;
            ImportMessageControl.SetButtons(ButtonTypes.FINISH);
            ImportMessageControl.SetTextMessage(string.Format(ImportMessages.sImportedSuccessfully, successfulRows, ds.Tables[0].Rows.Count - successfulRows), ImportMessageType.Sucess);
        }
    }
}