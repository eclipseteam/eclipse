﻿<%@ Page Title="e-Clipse Online Portal" EnableViewState="true" Language="C#" MasterPageFile="AdminMaster.master"
    AutoEventWireup="true" CodeBehind="AtCallHoldingVSTransactions.aspx.cs" Inherits="eclipseonlineweb.AtCallHoldingVSTransactions" %>


<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                                ID="btnDownload" /> &nbsp;
                           
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="At Call Holdings vs. Transactions"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>

            <br />
            <telerik:RadGrid ID="gd_AtCall" runat="server" AutoGenerateColumns="True" PageSize="20" 
        AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true" GridLines="Horizontal" AllowAutomaticInserts="false"
        AllowFilteringByColumn="true" EnableViewState="true" ShowFooter="True" OnNeedDataSource="gd_AtCall_OnNeedDataSource" >
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="Top" >
            <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false"
                ShowExportToWordButton="false" ShowExportToPdfButton="false"></CommandItemSettings>
            <Columns>  
                 <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="ClientCID"
                        SortExpression="ClientID" UniqueName="ClientID" DataNavigateUrlFormatString="../ClientViews/ClientMainView.aspx?ins={0}"
                        HeaderText="Client ID" DataTextField="ClientID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        FilterControlWidth="50px" HeaderStyle-Width="90px" HeaderButtonType="TextButton"
                        ShowFilterIcon="true">
                 </telerik:GridHyperLinkColumn> 
                <telerik:GridBoundColumn FilterControlWidth="300px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                    SortExpression="ClientName" HeaderText="Client Name" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="ClientName" UniqueName="ClientName">
                </telerik:GridBoundColumn>                         
                <telerik:GridBoundColumn FilterControlWidth="300px" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                    SortExpression="CustomerNumber" HeaderText="Customer Number" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="CustomerNumber" UniqueName="CustomerNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="200px" HeaderStyle-Width="8%"
                    ItemStyle-Width="8%" SortExpression="Holding" HeaderText="Holding" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="false" DataField="Holding" UniqueName="Holding">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlWidth="100px" HeaderStyle-Width="5%" ItemStyle-Width="5%"
                    SortExpression="TransactionHolding" HeaderText="Transactions" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="TransactionHolding" UniqueName="TransactionHolding">
                </telerik:GridBoundColumn> 
                <telerik:GridBoundColumn FilterControlWidth="100px" HeaderStyle-Width="5%" ItemStyle-Width="5%"
                    SortExpression="Diff" HeaderText="Difference" AutoPostBackOnFilter="true"
                    CurrentFilterFunction="Contains" ShowFilterIcon="true" HeaderButtonType="TextButton"
                    DataField="Diff" UniqueName="Diff">
                </telerik:GridBoundColumn>                    
                         
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
            <%--<c1:C1GridView ShowFilter="true" OnFiltering="Filter" RowStyle-Font-Size="Smaller"
                AllowSorting="true" OnSorting="SortMainList" FilterStyle-Font-Size="Smaller"
                HeaderStyle-Font-Size="Smaller" EnableTheming="true" Width="100%" ID="MISAccountList"
                CallbackSettings-Action="None" OnPageIndexChanging="MISAccountList_PageIndexChanging"
                PagerSettings-Mode="Numeric" runat="server" AutogenerateColumns="False" AllowPaging="true"
                PageSize="15" ClientSelectionMode="None" CallbackSettings-Mode="Partial" OnSelectedIndexChanging="MISAccountList_SelectedIndexChanging">
                <SelectedRowStyle />
                <Columns>                    
                    <c1:C1HyperLinkField DataTextField="ClientID" SortExpression="ClientID" HeaderText="Client ID"
                        DataNavigateUrlFields="ClientCID" DataNavigateUrlFormatString="../ClientViews/ClientMainView.aspx?ins={0}">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1HyperLinkField>
                    <c1:C1BoundField DataField="CustomerNumber" SortExpression="CustomerNumber" HeaderText="Customer Number">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1BoundField>
                   
                    <c1:C1BoundField DataField="Holding" SortExpression="Holding" DataFormatString="N4"
                        HeaderText="Holdings">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1BoundField>
                    <c1:C1BoundField DataField="TransactionHolding" SortExpression="TransactionHolding" DataFormatString="N4"
                        HeaderText="Transactions">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1BoundField>
                    <c1:C1BoundField DataField="Diff" SortExpression="Diff" HeaderText="Difference">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1BoundField>
                </Columns>
            </c1:C1GridView>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
