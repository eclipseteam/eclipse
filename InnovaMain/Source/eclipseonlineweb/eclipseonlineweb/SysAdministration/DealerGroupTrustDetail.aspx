﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    EnableEventValidation="true" AutoEventWireup="true" CodeBehind="DealerGroupTrustDetail.aspx.cs"
    Inherits="eclipseonlineweb.SysAdministration.DealerGroupTrustDetail" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register Src="../Controls/AddressDetailControl.ascx" TagName="AddressDetailControl"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="../Controls/BankAccountMappingControl.ascx" TagName="BankAccountMappingControl"
    TagPrefix="uc4" %>
<%@ Register Src="../Controls/ContactMappingControl.ascx" TagName="ContactMappingControl"
    TagPrefix="uc5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Controls/DealerGroupTrustControl.ascx" TagName="DealerGroupTrustControl"
    TagPrefix="uc2" %>
<%@ Register TagPrefix="uc2" TagName="SecurityConfigurationControl" Src="~/Controls/SecurityConfigurationControl.ascx" %>
<%@ Register TagPrefix="uc5" TagName="OrganizationUnitMappingControl" Src="~/Controls/OrganizationUnitMappingControl.ascx" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                        </td>
                        <td width="90%" align="right">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <span style="font-weight: bold;">Dealer Group</span>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:radtabstrip id="RadTabStrip1" runat="server" skin="Vista" multipageid="RadMultiPage1"
                selectedindex="0">
                <tabs>
                    <telerik:RadTab Text="Dealer Group Trust" runat="server" Selected="True" Value="DealerGroupTrustpHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Addresses" runat="server" Visible="true" Value="AddressHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Bank Accounts" runat="server" Visible="true" Value="BankAccountsHeader">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Corporate" runat="server" Visible="true" Value="CorporateHeader">
                    </telerik:RadTab>
                     <telerik:RadTab Text="Security" runat="server" Visible="true" Value="SecurityHeader">
                    </telerik:RadTab>
                     <telerik:RadTab Text="Documents" runat="server" Visible="true" Value="DocumentsHeader">
                    </telerik:RadTab>
                </tabs>
            </telerik:radtabstrip>
            <telerik:radmultipage runat="server" id="RadMultiPage1" selectedindex="0">
                <telerik:radpageview runat="server" id="ApBasic">
                    <uc2:DealerGroupTrustControl ID="DealerGroupTrustControl1" runat="server" />
                </telerik:radpageview>
                <telerik:radpageview id="ApAddress" runat="server">
                    <fieldset style="width: 98%">
                        <div style="text-align: left; float: none; border: 1px; background-color: White;
                            height: 25px; position: relative; vertical-align: middle; margin: 0; padding: 0;">
                            <asp:ImageButton ID="btnSave" AlternateText="Add Client Addresses" ToolTip="Save Changes"
                                OnClick="btnSave_Click" runat="server" ImageUrl="~/images/Save-Icon.png" />
                        </div>
                    </fieldset>
                    <br />
                    <uc1:AddressDetailControl ID="AddressDetailControl" runat="server" />
                </telerik:radpageview>
                <telerik:radpageview id="ApBankAccounts" runat="server">
                    <uc4:BankAccountMappingControl ID="BankAccountMappingControl" runat="server" />
                </telerik:radpageview>
                <telerik:radpageview id="ApCorporate" runat="server">
                <uc5:OrganizationUnitMappingControl ID="Corporates" runat="server" UnitPropertyName="Corporates" TitleText="Corporates"/>
                </telerik:radpageview>
                <telerik:radpageview id="ApSecurity" runat="server">
                    <uc2:SecurityConfigurationControl ID="SecurityConfiguration" runat="server" />
                </telerik:radpageview>
                <telerik:radpageview id="ApDocuments" runat="server">
                </telerik:radpageview>
            </telerik:radmultipage>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
