﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="SecurityConfiguration.aspx.cs" Inherits="eclipseonlineweb.SysAdministration.SecurityConfiguration" %>

<%@ Register TagPrefix="uc2" TagName="SecurityConfigurationControl" Src="~/Controls/SecurityConfigurationControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<fieldset>
<legend>Security Configurations</legend> 
    <uc2:SecurityConfigurationControl ID="SecurityConfiguration1" runat="server" />
    </fieldset>
</asp:Content>
