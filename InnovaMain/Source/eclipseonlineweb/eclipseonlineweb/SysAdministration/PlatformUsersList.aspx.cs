﻿using System;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CalculationInterface;
using Telerik.Web.UI;
using Oritax.TaxSimp;
using System.Collections.Generic;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.Organization.Data;
using System.Linq;
using System.Security.Cryptography;

namespace eclipseonlineweb
{
    public partial class PlatformUsersList : UMABasePage
    {
        bool _isExportMode;
        public override bool AccessibleByUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.Name == "auremovic@innovapm.com.au" || objUser.Name == "shepworth")
                return true;
            else if (objUser.UserType != UserType.Innova && objUser.Name != "Administrator")
                return false;
            else
                return true;
        }
        protected override void GetBMCData()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            DBUserDetailsDS dBUserDetailsDS = new DBUserDetailsDS();
            dBUserDetailsDS.UserList = true;
            objUser.GetData(dBUserDetailsDS);

            dBUserDetailsDS.Tables["UserDetail"].Columns.Add("UserTypeString");

            PresentationData = dBUserDetailsDS;
            foreach (DataRow drow in PresentationData.Tables["UserDetail"].Rows)
            {
                if (drow["UserType"].ToString() == "0")
                    drow["UserTypeString"] = "Adviser";
                else if (drow["UserType"].ToString() == "1")
                    drow["UserTypeString"] = "Accountant";
                else if (drow["UserType"].ToString() == "2")
                    drow["UserTypeString"] = "IFA";
                else if (drow["UserType"].ToString() == "3")
                    drow["UserTypeString"] = "DealerGroup";
                else if (drow["UserType"].ToString() == "4")
                    drow["UserTypeString"] = "Innova";
                else if (drow["UserType"].ToString() == "5")
                    drow["UserTypeString"] = "Client";
            }

            UMABroker.ReleaseBrokerManagedComponent(objUser);
        }

        protected void DownloadXLS(object sender, EventArgs e)
        {
            DataSet excelDataset = new DataSet();
            excelDataset.Merge(PresentationData.Tables["UserDetail"], true, MissingSchemaAction.Add);

            ExcelHelper.ToExcel(excelDataset, "User Securities-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", this.Page.Response);
        }

        protected void btnBackToListing(object sender, ImageClickEventArgs e)
        {
            this.pnlMainGrid.Visible = true;
        }

       

       

        private void GetUserListData()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            DBUserDetailsDS dBUserDetailsDS = new Oritax.TaxSimp.Security.DBUserDetailsDS();
            dBUserDetailsDS.UserList = true;
            objUser.GetData(dBUserDetailsDS);

            dBUserDetailsDS.Tables["UserDetail"].Columns.Add("UserTypeString");

            this.PresentationData = dBUserDetailsDS;
            foreach (DataRow drow in PresentationData.Tables["UserDetail"].Rows)
            {

                if (drow["UserType"].ToString() == "0")
                    drow["UserTypeString"] = "Adviser";
                else if (drow["UserType"].ToString() == "1")
                    drow["UserTypeString"] = "Accountant";
                else if (drow["UserType"].ToString() == "2")
                    drow["UserTypeString"] = "IFA";
                else if (drow["UserType"].ToString() == "3")
                    drow["UserTypeString"] = "DealerGroup";
                else if (drow["UserType"].ToString() == "4")
                    drow["UserTypeString"] = "Innova";
                else if (drow["UserType"].ToString() == "5")
                    drow["UserTypeString"] = "Client";
                else if (drow["UserType"].ToString() == "6")
                    drow["UserTypeString"] = "Services";
            }

         }

       

        public override void PopulatePage(DataSet ds)
        {
            ScriptManager sm = ScriptManager.GetCurrent(Page);

            if (sm != null)
                sm.RegisterPostBackControl(this.btnDownload);
        }

        protected void BackToSummary(object sender, EventArgs args)
        {
            this.pnlMainGrid.Visible = true;
        }

  

        #region Telerik Code

     

        protected void PresentationGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;
                GridEditManager editMan = editedItem.EditManager;
                GridDropDownListColumnEditor adviserList = null;
                GridDropDownListColumnEditor userList = null;
               
                if ("Users".Equals(e.Item.OwnerTableView.Name))
                {
                    IOrganization orgCM = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                    OrganisationListingDS organisationListingDS = new OrganisationListingDS();
                    organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.AdvisersBasicList;
                    orgCM.GetData(organisationListingDS);

                    IDictionary<Enum, string> userTypeValues = Enumeration.ToDictionary(typeof(UserType));
                    adviserList = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("AdviserTypeEditor"));
                    userList = (GridDropDownListColumnEditor)(editMan.GetColumnEditor("UserTypeStringEditor"));

                    RadComboBoxItem emptyAdviser = new RadComboBoxItem(String.Empty, String.Empty);
                    adviserList.ComboBoxControl.Filter = RadComboBoxFilter.Contains;
                    adviserList.DataSource = organisationListingDS.Tables["ADVISERLISTINGTABLE"];
                    //adviserList.DataValueField = "ENTITYCIID_FIELD";
                    //adviserList.DataTextField = "ENTITYNAME_FIELD";
                    adviserList.DataBind();
                    adviserList.ComboBoxControl.Items.Add(emptyAdviser);
                    emptyAdviser.Selected = true;

                    RadComboBoxItem emptyUser = new RadComboBoxItem(String.Empty, String.Empty);
                    userList.ComboBoxControl.Filter = RadComboBoxFilter.Contains;
                    userList.DataSource = userTypeValues;
                    //userList.DataValueField = "Key";
                    //userList.DataTextField = "Value";
                    userList.DataBind();
                    userList.ComboBoxControl.Items.Add(emptyUser);
                    emptyUser.Selected = true;


                    if (!(e.Item.DataItem is GridInsertionObject))
                    {
                        string valueCID = ((System.Data.DataRowView)(e.Item.DataItem)).Row["AdviserCID"].ToString();
                        if (valueCID != string.Empty)
                        {
                            RadComboBoxItem item = userList.ComboBoxControl.FindItemByValue(valueCID);
                            if (item != null)
                                item.Selected = true;
                        }

                        string value = (string)((System.Data.DataRowView)(e.Item.DataItem)).Row["UserTypeString"];
                        if (value != string.Empty)
                        {
                            RadComboBoxItem item = userList.ComboBoxControl.FindItemByText(value);
                            if (item != null)
                                item.Selected = true;
                        }
                    }
                }
            }
            e.Canceled = true;
        }

        protected void PresentationGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
         
            DataView usersView = new DataView(this.PresentationData.Tables["UserDetail"]);
            DataTable table = usersView.ToTable();
            table.CaseSensitive = false;

            this.PresentationGrid.DataSource = usersView.ToTable();
        }

        protected void PresentationGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "update")
            {
                if ("Users".Equals(e.Item.OwnerTableView.Name))
                {
                    DBUserDetailsDS dbUserDetailsDS = PopulateDatasetWithData(e);
                    e.Item.Edit = false;
                    e.Canceled = true;
                    UpdateAndRefresh(dbUserDetailsDS, dbUserDetailsDS.DBUserCID);
                }
            }
            else if (e.CommandName.ToLower() == "delete")
            {
                GridDataItem editForm = (GridDataItem)e.Item;
                string cidToDelete = editForm.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["CID"].ToString();

                DataRow row = this.PresentationData.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Select().Where(drow => drow[DBUserDetailsDS.USER_USERID_FIELD].ToString() == cidToDelete).FirstOrDefault();
                row.Delete();

                DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");

                e.Item.Edit = false;
                e.Canceled = true;
                this.SaveData(objUser.CID.ToString(), this.PresentationData);
                this.GetBMCData();
                this.PresentationGrid.Rebind();

            }

            else if (e.CommandName == RadGrid.ExportToWordCommandName || e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName)
            {

                PresentationGrid.MasterTableView.GetColumn("EditCommandColumn2").Visible = false;
                PresentationGrid.MasterTableView.GetColumn("Locked").Visible = false;
                PresentationGrid.MasterTableView.GetColumn("Active").Visible = false;
                PresentationGrid.MasterTableView.GetColumn("DeleteColumn").Visible = false;

                PresentationGrid.MasterTableView.GetColumn("Username").HeaderStyle.Width = Unit.Pixel(250);
                PresentationGrid.MasterTableView.GetColumn("Firstname").HeaderStyle.Width = Unit.Pixel(150);
                PresentationGrid.MasterTableView.GetColumn("Lastname").HeaderStyle.Width = Unit.Pixel(90);
                PresentationGrid.MasterTableView.GetColumn("AdviserID").HeaderStyle.Width = Unit.Pixel(90);
                PresentationGrid.MasterTableView.GetColumn("IFANAME").HeaderStyle.Width = Unit.Pixel(190);
                PresentationGrid.MasterTableView.GetColumn("UserTypeString").HeaderStyle.Width = Unit.Pixel(90);
                PresentationGrid.MasterTableView.GetColumn("EmailAddress").HeaderStyle.Width = Unit.Pixel(200);
                ConfigureExport();
                _isExportMode = true;
                PresentationGrid.Rebind();
            }

        }

        public void ConfigureExport()
        {
            PresentationGrid.ExportSettings.ExportOnlyData = true;
            PresentationGrid.ExportSettings.IgnorePaging = true;
            PresentationGrid.ExportSettings.OpenInNewWindow = true;
            PresentationGrid.ExportSettings.Pdf.PageHeight = Unit.Parse("135mm");
            PresentationGrid.ExportSettings.Pdf.PageWidth = Unit.Parse("510mm");
        }

        private void SetHeaderFormat(GridHeaderItem headerItem)
        {


            headerItem.BackColor = Color.Yellow;
            headerItem.VerticalAlign = VerticalAlign.Bottom;
            headerItem.HorizontalAlign = HorizontalAlign.Left;
            headerItem.Font.Size = new FontUnit(Unit.Point(9));
            headerItem.Font.Bold = true;
            headerItem.ForeColor = Color.Black;
            headerItem.BorderColor = Color.Red;
            headerItem.BorderWidth = 1;
            foreach (TableCell cell in headerItem.Cells)
            {
                cell.Font.Bold = true;
                cell.HorizontalAlign = HorizontalAlign.Left;
                cell.BorderColor = Color.Red;
                cell.BorderWidth = 1;
            }
        }

        private void SaveAndRefresh(DBUserDetailsDS dbUserDetailsDS)
        {
            UMABroker.SaveOverride = true;
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            objUser.SetData(dbUserDetailsDS);

            UMABroker.SetComplete();
            UMABroker.SetStart();
            this.GetBMCData();
            this.PresentationGrid.Rebind();
        }

        private void UpdateAndRefresh(DBUserDetailsDS dbUserDetailsDS, string CID)
        {
            this.SaveData(CID, dbUserDetailsDS);
            this.GetBMCData();
            this.PresentationGrid.Rebind();
        }
        
        protected void PresentationGrid_InsertCommand(object source, GridCommandEventArgs e)
        {
            if ("Users".Equals(e.Item.OwnerTableView.Name))
            {
                UMABroker.SaveOverride = true;
                DBUserDetailsDS dbUserDetailsDS = PopulateDatasetWithData(e);
                IBrokerManagedComponent bmc = UMABroker.CreateComponentInstance(new Guid(dbUserDetailsDS.DBUserCID), new Guid("3d98a779-adf0-4bf5-940d-220ee57acbd9"),
                                                                                dbUserDetailsDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0][DBUserDetailsDS.USER_USERNAME_FIELD].ToString());
                dbUserDetailsDS.IsNew = true;
                bmc.SetData(dbUserDetailsDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
                e.Item.Edit = false;
                e.Canceled = true;
                this.PresentationGrid.Rebind();
            }
        }

        private DBUserDetailsDS PopulateDatasetWithData(GridCommandEventArgs e)
        {
            var iOrgCm = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            string hashingPassword;
            var editForm = (GridEditFormItem)e.Item;
            var username = (TextBox)editForm["Username"].Controls[0];
            var firstname = (TextBox)editForm["Firstname"].Controls[0];
            var lastname = (TextBox)editForm["Lastname"].Controls[0];
            var emailAddress = (TextBox)editForm["EmailAddress"].Controls[0];
            var password = (TextBox)editForm["Password"].Controls[0];
           
            var comboAdvisers = (RadComboBox)editForm["AdviserTypeEditor"].Controls[0];
            var comboUserType = (RadComboBox)editForm["UserTypeStringEditor"].Controls[0];

            var dbUserDetailsDs = new DBUserDetailsDS();
            DataRow row = dbUserDetailsDs.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].NewRow();
            row[DBUserDetailsDS.USER_USERNAME_FIELD] = username.Text;
            row[DBUserDetailsDS.USER_FIRSTNAME_FIELD] = firstname.Text;
            row[DBUserDetailsDS.USER_LASTNAME_FIELD] = lastname.Text;
            row[DBUserDetailsDS.USER_EMAILADDRESS_FIELD] = emailAddress.Text;
            row[DBUserDetailsDS.USER_ORGANISATION_CID_FIELD] = objUser.OrganisationCID;

            if (((LinkButton)(e.CommandSource)).Text.ToLower() == "insert")
            {
                Guid newUserId = Guid.NewGuid();
                dbUserDetailsDs.DBUserCID = newUserId.ToString();
                row[DBUserDetailsDS.USER_USERID_FIELD] = newUserId;
                if (password.Text != string.Empty)
                {
                    hashingPassword = CalculateMd5Hash(password.Text);
                    //row[DBUserDetailsDS.USER_PASSWORD_FIELD] = hashingPassword;
                    row[DBUserDetailsDS.USER_PASSWORD_FIELD] = Oritax.TaxSimp.Utilities.Encryption.EncryptData(hashingPassword);
                    // row[DBUserDetailsDS.USER_PASSWORD_FIELD] = Oritax.TaxSimp.Utilities.Encryption.EncryptData(Password.Text);
                }
                else
                {
                    hashingPassword = CalculateMd5Hash("Password%01");
                    //row[DBUserDetailsDS.USER_PASSWORD_FIELD] = hashingPassword;
                    row[DBUserDetailsDS.USER_PASSWORD_FIELD] = Oritax.TaxSimp.Utilities.Encryption.EncryptData(hashingPassword);
                    //row[DBUserDetailsDS.USER_PASSWORD_FIELD] = Oritax.TaxSimp.Utilities.Encryption.EncryptData("Password%01");
                }
            }
            else
            {
                //This is for Update
               
                    if (password.Text != string.Empty)
                    {
                        hashingPassword = CalculateMd5Hash(password.Text);
                        //row[DBUserDetailsDS.USER_PASSWORD_FIELD] = hashingPassword;
                        row[DBUserDetailsDS.USER_PASSWORD_FIELD] = Oritax.TaxSimp.Utilities.Encryption.EncryptData(hashingPassword);
                        // row[DBUserDetailsDS.USER_PASSWORD_FIELD] = Oritax.TaxSimp.Utilities.Encryption.EncryptData(Password.Text);
                    }
                    else
                    {
                        hashingPassword = CalculateMd5Hash("Password%01");
                        //row[DBUserDetailsDS.USER_PASSWORD_FIELD] = hashingPassword;
                        row[DBUserDetailsDS.USER_PASSWORD_FIELD] = Oritax.TaxSimp.Utilities.Encryption.EncryptData(hashingPassword);
                        //row[DBUserDetailsDS.USER_PASSWORD_FIELD] = Oritax.TaxSimp.Utilities.Encryption.EncryptData("Password%01");
                    }
               
                dbUserDetailsDs.DBUserCID = editForm.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["CID"].ToString();
                row[DBUserDetailsDS.USER_USERID_FIELD] = editForm.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["CID"];
            }



            row[DBUserDetailsDS.USER_FAILEDATTEMPTS_FIELD] = 0;
            row[DBUserDetailsDS.USER_ACTIVE_FIELD] = 1;
            row[DBUserDetailsDS.USER_LOCKED_FIELD] = 0;
            row[DBUserDetailsDS.USER_ADMINISTRATOR_FIELD] = 0;
            row[DBUserDetailsDS.USER_DISPLAY_ZERO_PREFERENCE_FIELD] = true;

            if (comboUserType.SelectedItem.Text == "Adviser")
                row[DBUserDetailsDS.USER_USER_TYPE_ENUM_FIELD] = UserType.Advisor;
            if (comboUserType.SelectedItem.Text == "Accountant")
                row[DBUserDetailsDS.USER_USER_TYPE_ENUM_FIELD] = UserType.Accountant;
            if (comboUserType.SelectedItem.Text == "IFA")
                row[DBUserDetailsDS.USER_USER_TYPE_ENUM_FIELD] = UserType.IFA;
            if (comboUserType.SelectedItem.Text == "DealerGroup")
                row[DBUserDetailsDS.USER_USER_TYPE_ENUM_FIELD] = UserType.DealerGroup;
            if (comboUserType.SelectedItem.Text == "Innova")
            {
                row[DBUserDetailsDS.USER_USER_TYPE_ENUM_FIELD] = UserType.Innova;
                row[DBUserDetailsDS.USER_ADMINISTRATOR_FIELD] = 1;
            }
            if (comboUserType.SelectedItem.Text == "Client")
                row[DBUserDetailsDS.USER_USER_TYPE_ENUM_FIELD] = UserType.Client;
            if (comboUserType.SelectedItem.Text == "Services")
                row[DBUserDetailsDS.USER_USER_TYPE_ENUM_FIELD] = UserType.Services;

            if (comboAdvisers.SelectedItem.Value != string.Empty)
                row[DBUserDetailsDS.ADVISERCID] = comboAdvisers.SelectedItem.Value;
            dbUserDetailsDs.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows.Add(row);
            return dbUserDetailsDs;
        }

        private string CalculateMd5Hash(string input)
        {
            //MD5 md5 = MD5.Create();
            //byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            //byte[] hash = md5.ComputeHash(inputBytes);
            //var sb = new StringBuilder();
            //for (var i = 0; i < hash.Length; i++)
            //{
            //    sb.Append(hash[i].ToString("X2"));
            //}
            //return sb.ToString();
            return input;
        }

        protected void PresentationGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;
            if (item != null && item.IsInEditMode && item.ItemIndex != -1)
            {

            }
            else
                if (_isExportMode && e.Item is GridHeaderItem)
                {
                    GridHeaderItem headerItem = (GridHeaderItem)e.Item;
                    SetHeaderFormat(headerItem);
                }
        }

        private void DisplayMessage(string text)
        {
            PresentationGrid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
        }

        #endregion

    }
}
