﻿using System;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb.SysAdministration
{
    public partial class SellOrder : UMABasePage
    {
        protected override void Intialise()
        {
            SellOrderControl.SaveData += (clientCID, ds) =>
            {
                if (clientCID == Guid.Empty.ToString())
                {
                    SaveOrganizanition(ds);
                }
                else
                {
                    SaveData(clientCID, ds);
                }
            };
        }

        public override void LoadPage()
        {
            if (!IsPostBack)
            {
                var objUser = (DBUser)UMABroker.GetBMCInstance(User.Identity.Name, "DBUser_1_1");
                //Setting control required properties
                SellOrderControl.IsAdmin = objUser.Administrator;
                SellOrderControl.IsAdminMenu = true;
            }
        }
    }
}