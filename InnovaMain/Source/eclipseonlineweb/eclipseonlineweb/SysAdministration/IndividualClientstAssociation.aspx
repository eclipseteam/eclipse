﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/SysAdministration/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="IndividualClientstAssociation.aspx.cs" Inherits="eclipseonlineweb.IndividualClientstAssociation" %>

<%@ Register Src="../Controls/IndvidualClientsMappingControl.ascx" TagName="ClientsAssociationControl"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ClientHeaderInfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
   
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <fieldset>
        <table width="100%">
            <tr>
                <td style="width: 5%">
                  
                </td>
                <td style="width: 5%">
                 
                </td>
                <td style="width: 5%">
                </td>
                <td>
                </td>
                <td>
                </td>
                <td style="width: 85%;" class="breadcrumbgap">
                    <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                    <br />
                    <uc1:ClientHeaderInfo ID="ClientHeaderInfo1" runat="server" />
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <uc1:ClientsAssociationControl  ID="IndividualClientMappingControl" runat="server"></uc1:ClientsAssociationControl>
</asp:Content>
