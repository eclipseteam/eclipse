﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Utilities;
using Telerik.Web.UI;
using System.Drawing;

namespace eclipseonlineweb.WebControls
{
    public partial class ImportMessageControl : UserControl
    {
        public Action<string> Completed { get; set; }

        public DataTable GridImportMessagesDataSource { get; set; }

        public void ShowHideSampleFileLink(string filename)
        {
            if (IsTestApp())
            {
                hyp_sampleFile.Visible = true;
                hyp_sampleFile.NavigateUrl = filename;
            }
            else
            {
                hyp_sampleFile.Visible = false;
            }
        }

        public bool IsTestApp()
        {
            return DBConnection.Connection.ConnectionString.Contains("Test");
        }

        public void SetButtons(ButtonTypes value)
        {

            SetButtonVisibility(btnOK, false);
            SetButtonVisibility(btnNext, false);
            SetButtonVisibility(btnCancel, false);
            SetButtonVisibility(btnFinish, false);
            SetButtonVisibility(btnPrevious, false);
            SetButtonVisibility(btnClose, false);
            switch (value)
            {
                case ButtonTypes.CLOSE:
                    SetButtonVisibility(btnClose, true);

                    break;
                case ButtonTypes.OK:
                    SetButtonVisibility(btnOK, true);

                    break;
                case ButtonTypes.FINISH:
                    SetButtonVisibility(btnFinish, true);

                    break;
                case ButtonTypes.NEXT:
                    SetButtonVisibility(btnNext, true);

                    break;
                case ButtonTypes.OKCANCEL:
                    SetButtonVisibility(btnOK, true);
                    SetButtonVisibility(btnCancel, true);

                    break;
                case ButtonTypes.NEXTCANCEL:
                    SetButtonVisibility(btnNext, true);
                    SetButtonVisibility(btnCancel, true);

                    break;
                case ButtonTypes.FINISHCANCEL:
                    SetButtonVisibility(btnFinish, true);
                    SetButtonVisibility(btnCancel, true);

                    break;
                case ButtonTypes.PREVIOUSNEXTCANCEL:
                    SetButtonVisibility(btnPrevious, true);
                    SetButtonVisibility(btnNext, true);
                    SetButtonVisibility(btnCancel, true);

                    break;
                case ButtonTypes.NONE:
                    break;
            }
        }

        private static void SetButtonVisibility(Button btn, bool visible)
        {
            btn.Style["Display"] = visible ? "" : "none";
        }

        public void Clear()
        {
            FileID.Value = Guid.Empty.ToString();
            SetButtons(ButtonTypes.NONE);
            GridImportMessages.DataSource = null;
            GridImportMessages.DataBind();
            GirdInvestmentCodeError.DataSource = null;
            GirdInvestmentCodeError.DataBind();
            SetTextMessage("", ImportMessageType.InvestmentCodeAdded);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void SetTextMessage(string text, ImportMessageType type)
        {
            MessageStackPanelBottm1.Text = text;
            Color color = Color.Black;
            switch (type)
            {
                case ImportMessageType.Sucess:
                    color = Color.Black;
                    break;
                case ImportMessageType.Error:
                    color = Color.Red;
                    break;
                case ImportMessageType.Warning:
                    color = Color.Yellow;
                    break;
            }

            MessageStackPanelBottm1.ForeColor = color;
        }

        public void SetImportSuccessFileMsg(string labelText, string navigateURL)
        {
            dv_importSeccuess.Visible = true;
            lblImportmsg.Text = labelText;
            hyp_ASXV_VS_Trans.NavigateUrl = navigateURL;
        }


        public string RefreshFileID()
        {
            FileID.Value = Guid.NewGuid().ToString();
            return FileID.Value;
        }

        public string GetFileID()
        {
            return FileID.Value;
        }

        protected void btn_OnClick(object sender, EventArgs e)
        {
            if (Completed != null)
                Completed((sender as Button).Text);

        }
    }
    public enum ButtonTypes
    {
        OK, OKCANCEL, NEXTCANCEL, FINISHCANCEL, PREVIOUSNEXTCANCEL, FINISH, NONE, NEXT, CLOSE
    }



    public enum ImportMessageType
    {
        InvestmentCodeAdded,
        MissingInvestmentCode,
        Error,
        Sucess,
        Warning,
        InvestmentCodeDeleted,
    }
}