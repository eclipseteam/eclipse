﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImportMessageControl.ascx.cs"
    Inherits="eclipseonlineweb.WebControls.ImportMessageControl" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<link rel="stylesheet" type="text/css" href="~/Styles/style.css" />
<asp:HiddenField ID="FileID" runat="server" EnableViewState="true" />
<table style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; background: none repeat scroll 0 0 white;
    border: 1px solid #C4C4C4; border-radius: 2px 2px 2px 2px; box-shadow: 0 0 3px 0 #CDCDCD;
    width: 98.2%;">
    <tr>
        <td style="width:85%">
            <asp:Label ID="MessageStackPanelBottm1" runat="server" Font-Size="11" Font-Bold="True" /><br/>
                <div id="dv_importSeccuess" runat="server" Visible="false" style="color: #339933">
                <asp:Label ID="lblImportmsg" runat="server" Font-Size="11" Font-Bold="True"></asp:Label>                             
                <asp:HyperLink runat="server" ID="hyp_ASXV_VS_Trans"><Font size="3" style="text-decoration:underline">here</Font></asp:HyperLink>
            </div>  
        </td>
        <td style="width:15%; text-align:right">
            <asp:HyperLink runat="server" ID="hyp_sampleFile" Visible="false" Target="_blank"><Font size="2px" style="text-decoration:underline">View Sample File</Font></asp:HyperLink>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Panel ID="ButtonStackPanelBottm" runat="server" HorizontalAlign="Right">
                <asp:Button ID="btnOK" runat="server" Text="OK" Style="display: none" Width="75"
                    Height="25" OnClick="btn_OnClick" />
                <asp:Button ID="btnPrevious" runat="server" Text="Previous" Style="display: none"
                    Width="75" Height="25" OnClick="btn_OnClick" />
                <asp:Button ID="btnNext" runat="server" Text="Next" Style="display: none" Width="75"
                    Height="25" OnClick="btn_OnClick" />
                <asp:Button ID="btnFinish" runat="server" Text="Finish" Style="display: none" Width="75"
                    Height="25" OnClick="btn_OnClick" />
                <asp:Button ID="btnClose" runat="server" Text="Close" Style="display: none" Width="75"
                    Height="25" OnClick="btn_OnClick" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" Style="display: none" Width="75"
                    Height="25" OnClick="btn_OnClick" />
            </asp:Panel>
        </td>
    </tr>
</table>
<br />
<ajaxToolkit:Accordion ID="MyAccordion" runat="server" SelectedIndex="1" FadeTransitions="true"
    FramesPerSecond="40" TransitionDuration="250" AutoSize="None" HeaderCssClass="accordionHeader"
    ContentCssClass="accordionContent">
    <Panes>
        <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
            <Header>
                <a href="" onclick="return false;" class="accordionLink" style="color: #3E3B6A">Missing
                    Items</a>
            </Header>
            <Content>
                <telerik:RadGrid ID="GirdInvestmentCodeError" runat="server" AutoGenerateColumns="True"
                    PageSize="20" AllowSorting="false" AllowMultiRowSelection="False" AllowPaging="False"
                    GridLines="Horizontal" AllowFilteringByColumn="False" EnableViewState="False"
                    ShowFooter="True">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                        <Excel Format="Html"></Excel>
                    </ExportSettings>
                </telerik:RadGrid>
                <br />
                <asp:Label ID="TextMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
            </Content>
        </ajaxToolkit:AccordionPane>
        <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server">
            <Header>
                <a href="" onclick="return false;" class="accordionLink" style="color: #3E3B6A">Import
                    Messages</a>
            </Header>
            <Content>
                <telerik:RadGrid ID="GridImportMessages" runat="server" AutoGenerateColumns="True"
                    PageSize="20" AllowSorting="false" AllowMultiRowSelection="False" AllowPaging="False"
                    GridLines="Horizontal" AllowFilteringByColumn="False" EnableViewState="False"
                    ShowFooter="True">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                        <Excel Format="Html"></Excel>
                    </ExportSettings>
                    <MasterTableView>
                        <SortExpressions>
                            <telerik:GridSortExpression FieldName="HasErrors" SortOrder="Descending" />
                        </SortExpressions>
                    </MasterTableView>
                </telerik:RadGrid>
            </Content>
        </ajaxToolkit:AccordionPane>
    </Panes>
</ajaxToolkit:Accordion>
