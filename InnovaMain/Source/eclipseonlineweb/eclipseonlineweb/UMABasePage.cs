﻿using System;
using System.Collections;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.Utilities;
using System.Data;
using System.Threading;
using C1.C1Preview;
using Oritax.TaxSimp.DataSets;
using System.Configuration;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1GridView;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Security;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.Calculation;
using System.Linq;

namespace eclipseonlineweb
{
    public class UMABasePage : System.Web.UI.Page
    {
        #region Variables
        //Private Variables
        private ICMBroker umaBroker = null;
        //Component Presentation Dataset
        private DataSet presentationData;

        protected string cid = string.Empty;

        public DataSet PresentationData
        {
            get { return presentationData; }
            set { presentationData = value; }
        }

        public virtual bool AccessibleByUser()
        {
            return true;
        }
        public bool IsAdminUser()
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(this.User.Identity.Name, "DBUser_1_1");
            if (objUser.UserType == UserType.Innova || objUser.Name == "Administrator")
                return true;
            else
                return false;
        }

        public string DataGridToExcel(C1GridView dgExport)
        {
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            //create an htmltextwriter which uses the stringwriter
            System.Web.UI.HtmlTextWriter htmlWrite = new System.Web.UI.HtmlTextWriter(stringWrite);
            C1.Web.Wijmo.Controls.C1GridView.C1GridView dg = default(C1.Web.Wijmo.Controls.C1GridView.C1GridView);
            //just set the input datagrid = to the new dg grid
            dg = dgExport;

            //Make the header text bold
            dg.HeaderStyle.Font.Bold = true;

            //If needed, here's how to change colors/formatting at the component level
            dg.HeaderStyle.ForeColor = System.Drawing.Color.Black;
            dg.RowStyle.ForeColor = System.Drawing.Color.Black;

            //bind the modified datagrid
            //tell the datagrid to render itself to our htmltextwriter
            dg.AllowSorting = false;
            dg.AllowPaging = false;
            dg.AllowCustomPaging = false;

            //new code
            Control parent = dg.Parent;
            parent.Controls.Remove(dg);

            dg.RenderControl(htmlWrite);

            //new code
            parent.Controls.Add(dg);

            //output the html
            return stringWrite.ToString();
        }

        private IBrokerManagedComponent bmcComponent;

        public IBrokerManagedComponent BMCComponent
        {
            get { return bmcComponent; }
            set { bmcComponent = value; }
        }

        #endregion

        #region Properties

        public ICMBroker UMABroker
        {
            get
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (brokerSlot == null)
                    return null;

                return (ICMBroker)Thread.GetData(brokerSlot);
            }
            set
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (brokerSlot == null)
                    brokerSlot = Thread.AllocateNamedDataSlot("Broker");

                if (value == null)
                    Thread.FreeNamedDataSlot("Broker");

                Thread.SetData(brokerSlot, value);
            }
        }

        #endregion

        public void SetReportMenuForEclipseSuper(string componentInformationDisplayName)
        {
            ContentPlaceHolder cpHolder = this.Master.Controls[0].FindControl("SidebarMenu") as ContentPlaceHolder;

            Panel pnlUMAReports = (Panel)cpHolder.FindControl("pnlUMAReports");
            Panel pnlECLIPSESUPERReports = (Panel)cpHolder.FindControl("pnlECLIPSESUPERReports");

            if (componentInformationDisplayName.ToLower() == "e-clipse super")
            {
                pnlUMAReports.Visible = false;
                pnlECLIPSESUPERReports.Visible = true;
            }
            else
            {
                pnlUMAReports.Visible = true;
                pnlECLIPSESUPERReports.Visible = false;
            }
        }

        public void PushDownUserAccess(Guid parentCID)
        {
            IOrganizationUnit orgUnit = UMABroker.GetBMCInstance(parentCID) as IOrganizationUnit;
            PartyDS sourcePartyDS = new PartyDS();
            orgUnit.GetData(sourcePartyDS);
            DataTable sourceIncludedUsers = sourcePartyDS.Tables[PartyDS.INCLUDEDPARTY_TABLE];
            if (sourceIncludedUsers.Rows.Count > 0)
            {
                if (orgUnit.ClientEntity is AccountantEntity)
                {
                    AccountantEntity accountantEntity = orgUnit.ClientEntity as AccountantEntity;
                    foreach (var child in accountantEntity.Clients)
                    {
                        IBrokerManagedComponent childBMC = UMABroker.GetCMImplementation(child.Clid, child.Csid);
                        if (childBMC != null)
                        {
                            PartyDS childPartyDS = new PartyDS();
                            foreach (DataRow sourceUserRow in sourceIncludedUsers.Rows)
                            {
                                var existingParty = childPartyDS.Tables[PartyDS.INCLUDEDPARTY_TABLE].Select().Where(row => row[PartyDS.PARTYNAME_FIELD].ToString() == sourceUserRow[PartyDS.PARTYNAME_FIELD].ToString());
                                if (existingParty == null || existingParty.Count() == 0)
                                {
                                    DataRow newChildPartyRow = childPartyDS.Tables[PartyDS.INCLUDEDPARTY_TABLE].NewRow();
                                    newChildPartyRow[PartyDS.PARTYNAME_FIELD] = sourceUserRow[PartyDS.PARTYNAME_FIELD];
                                    newChildPartyRow[PartyDS.PARTYCID_FIELD] = sourceUserRow[PartyDS.PARTYCID_FIELD];
                                    childPartyDS.Tables[PartyDS.INCLUDEDPARTY_TABLE].Rows.Add(newChildPartyRow);
                                }
                            }

                            this.SaveData(childBMC.CID.ToString(), childPartyDS);
                        }
                    }
                }
            }
        }

        protected void EraseBankAccountNumber(DataTable holdingSummaryDT)
        {
            // for following product, no bank account / BSB to be revealed. 
            // INNOVA-1329 Financial Summary : bank accounts for FIIG and AMM

            if (holdingSummaryDT == null)
                return;

            if (!holdingSummaryDT.Columns.Contains("ProductName") ||
                !holdingSummaryDT.Columns.Contains("AccountNo") ||
                !holdingSummaryDT.Columns.Contains("BSB")
                )
                return;

            string[] productsErasing = { "AMM-At Call", "AMM-TD", "FiiG-At Call", "FiiG-TD" };

            holdingSummaryDT
                .AsEnumerable()
                .Where(m => productsErasing.ToList<string>().Contains(m["ProductName"]))
                .ToList()
                .ForEach
                    (
                        dr => 
                        {
                            dr["AccountNo"] = "";
                            dr["BSB"] = "";
                        }
                    );

        }

        public void BulkTransactionsOutput(string clientID, string clientName)
        {
            IBrokerManagedComponent clientData = UMABroker.GetBMCInstance(new Guid(clientID));
            HoldingRptDataSet HoldingRptDataSet = new HoldingRptDataSet();
            BankTransactionDS banktransacationDS = new BankTransactionDS();
            MISTransactionDS mISTransactionDS = new MISTransactionDS();
            ASXTransactionDS aSXTransactionDS = new ASXTransactionDS();
            DIVTransactionDS dIVTransactionDS = new DIVTransactionDS();
            TDTransactionDS tdTransactionDS = new TDTransactionDS();
            ManualTransactionDS manualTransactionDS = new ManualTransactionDS();
            ClientDistributionsDS clientDistributionsDS = new ClientDistributionsDS();
            clientData.GetData(HoldingRptDataSet);

            clientData.GetData(banktransacationDS);
            clientData.GetData(mISTransactionDS);
            clientData.GetData(aSXTransactionDS);
            clientData.GetData(dIVTransactionDS);
            clientData.GetData(tdTransactionDS);
            clientData.GetData(manualTransactionDS);
            clientData.GetData(clientDistributionsDS);

            DataSet excelDataset = new DataSet();
            excelDataset.Merge(HoldingRptDataSet, true, MissingSchemaAction.Add);
            excelDataset.Merge(banktransacationDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(mISTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(aSXTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(dIVTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(tdTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(manualTransactionDS, true, MissingSchemaAction.Add);
            excelDataset.Merge(clientDistributionsDS, true, MissingSchemaAction.Add);

            if (excelDataset.Tables.Contains("HoldingSummary"))
            {
                excelDataset.Tables["HoldingSummary"].Columns.Remove("TDID");
                excelDataset.Tables["HoldingSummary"].Columns.Remove("ProductID");
                excelDataset.Tables["HoldingSummary"].Columns.Remove("AccountCID");

                EraseBankAccountNumber(excelDataset.Tables["HoldingSummary"]);
            }

            if (excelDataset.Tables.Contains("ClientSummaryTable"))
            {
                excelDataset.Tables["ClientSummaryTable"].Columns.Remove("AdviserCID");
            }

            if (excelDataset.Tables.Contains("Income Transactions"))
            {
                excelDataset.Tables["Income Transactions"].Columns.Remove("ID");
            }

            if (excelDataset.Tables.Contains("Cash Transactions"))
            {
                excelDataset.Tables["Cash Transactions"].Columns.Remove("BankCid");
                excelDataset.Tables["Cash Transactions"].Columns.Remove("InstitutionID");
                excelDataset.Tables["Cash Transactions"].Columns.Remove("ID");
                excelDataset.Tables["Cash Transactions"].Columns.Remove("DividendID");
            }

            if (excelDataset.Tables.Contains("MIS Transactions"))
            {
                excelDataset.Tables["MIS Transactions"].Columns.Remove("ID");
                excelDataset.Tables["MIS Transactions"].Columns.Remove("MISCID");
                excelDataset.Tables["MIS Transactions"].Columns.Remove("ClientCID");
            }

            if (excelDataset.Tables.Contains("DesktopBroker Transactions"))
            {
                excelDataset.Tables["DesktopBroker Transactions"].Columns.Remove("ASXCID");
                excelDataset.Tables["DesktopBroker Transactions"].Columns.Remove("TranstactionUniqueID");
                excelDataset.Tables["DesktopBroker Transactions"].Columns.Remove("DividendID");
            }

            if (excelDataset.Tables.Contains("TDs Transactions"))
            {
                excelDataset.Tables["TDs Transactions"].Columns.Remove("ID");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("AccountName");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("DividendID");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("InstitutionID");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("Product");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("BSB");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("AccountType");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("AccountNo");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("BankCid");
                excelDataset.Tables["TDs Transactions"].Columns.Remove("BrokerID");
            }

            if (excelDataset.Tables.Contains("Manual Transactions"))
            {
                excelDataset.Tables["Manual Transactions"].Columns.Remove("ID");
                excelDataset.Tables["Manual Transactions"].Columns.Remove("InvestmentID");
            }

            if (excelDataset.Tables.Contains(ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE))
            {
                excelDataset.Tables[ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE].Columns.Remove("ID");
                excelDataset.Tables[ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE].Columns.Remove("ClientCID");
            }

            if (excelDataset.Tables.Contains("ProductBreakDown"))
                excelDataset.Tables.Remove("ProductBreakDown");
            if (excelDataset.Tables.Contains("TDBreakDown"))
                excelDataset.Tables.Remove("TDBreakDown");
            if (excelDataset.Tables.Contains("ContactTable"))
                excelDataset.Tables.Remove("ContactTable");
            if (excelDataset.Tables.Contains("TDAccountList"))
                excelDataset.Tables.Remove("TDAccountList");
            if (excelDataset.Tables.Contains("PRODUCTLIST"))
                excelDataset.Tables.Remove("PRODUCTLIST");
            if (excelDataset.Tables.Contains("Accounts"))
                excelDataset.Tables.Remove("Accounts");
            if (excelDataset.Tables.Contains("MISTranVsHolding"))
                excelDataset.Tables.Remove("MISTranVsHolding");
            if (excelDataset.Tables.Contains("BankAccountList"))
                excelDataset.Tables.Remove("BankAccountList");
            if (excelDataset.Tables.Contains("ClientAllDividends"))
                excelDataset.Tables.Remove("ClientAllDividends");

            ExcelHelper.ToExcel(excelDataset, clientName + ".xls", this.Page.Response);
        }

        protected void SetReportMenus(MenuEventArgs e)
        {
            if (e.Item.Text.Contains("BACK TO ACCOUNT HOMEPAGE"))
                Response.Redirect("~/ClientViews/ClientMainView.aspx?ins=" + this.cid);
            else if (e.Item.Text == "HOLDING & VALUATION")
                Response.Redirect(@"ClientHoldingReport.aspx?ins=" + this.cid);
            else if (e.Item.Text == "HOLDING STATEMENT")
                Response.Redirect(@"HoldingStatementReport.aspx?ins=" + this.cid);
            else if (e.Item.Text == "ASSET CLASS SUMMARY")
                Response.Redirect(@"ClientAssetSummaryReport.aspx?ins=" + this.cid);
            else if (e.Item.Text == "SECURITY SUMMARY")
                Response.Redirect(@"SecuritySummaryReport.aspx?ins=" + this.cid);
            else if (e.Item.Text == "CAPITAL MOVEMENT SUMMARY")
                Response.Redirect(@"ClientCapitalMovementReport.aspx?ins=" + this.cid);
            else if (e.Item.Text == "- PORTFOLIO SUMMARY")
                Response.Redirect(@"ClientPerformanceReport.aspx?ins=" + this.cid);
            else if (e.Item.Text == "- PORTFOLIO DETAILED")
                Response.Redirect(@"ClientPerformanceReportDetails.aspx?ins=" + this.cid);
            else if (e.Item.Text == "- INCOME vs. GROWTH")
                Response.Redirect(@"IncomeGrowthReportDetails.aspx?ins=" + this.cid);
            else if (e.Item.Text == "BANK")
                Response.Redirect(@"BankStatementReport.aspx?ins=" + this.cid);
            else if (e.Item.Text == "DIVIDEND")
                Response.Redirect(@"DividentStatement.aspx?ins=" + this.cid);
            else if (e.Item.Text == "GAINS / LOSSES")
                Response.Redirect(@"GainsLossesReport.aspx?ins=" + this.cid);
            else if (e.Item.Text == "DISTRIBUTION")
                Response.Redirect(@"DistributionStatement.aspx?ins=" + this.cid);
            else if (e.Item.Text == "TAX PACK")
                Response.Redirect(@"TaxReport.aspx?ins=" + this.cid);
            else if (e.Item.Text == "SEC HOLDINGS")
                Response.Redirect(@"ChessHoldingStatementReport.aspx?ins=" + this.cid);
            else if (e.Item.Text == "FUND HOLDINGS")
                Response.Redirect(@"DisHoldingStatementReport.aspx?ins=" + this.cid);

        }

        public virtual void ProcessReport()
        {


        }

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Intialise();
            this.InitialiseUMABroker();
            this.PageAllowedByLoggedInUser();
            this.LoadPage();
        }

        protected void PageAllowedByLoggedInUser()
        {
            if (!this.AccessibleByUser())
            {
                Response.Redirect("~/Account/PermissionDenied.aspx");
            }
        }

        protected void SaveData(string cid, DataSet ds)
        {
            SaveData(new Guid(cid), ds);
        }

        protected void SaveDataTransientComponent(Guid cid, DataSet ds)
        {
            IBrokerManagedComponent bmc = this.UMABroker.CreateTransientComponentInstance(cid);
            bmc.SetData(ds);
        }

        protected void SaveData(IBrokerManagedComponent bmc)
        {
            SetAsposeWordsLicense();
            this.UMABroker.SaveOverride = true;
            bmc.CalculateToken(true);
            this.UMABroker.SetComplete();
            this.UMABroker.SetStart();
        }

        protected void SaveData(Guid cid, DataSet ds)
        {
            SetAsposeWordsLicense();
            this.UMABroker.SaveOverride = true;
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(cid);
            clientData.SetData(ds);
            this.UMABroker.SetComplete();
            this.UMABroker.SetStart();
        }

        protected void SetAsposeWordsLicense()
        {
            Aspose.Words.License license = new Aspose.Words.License();
            license.SetLicense("Aspose.Words.lic");
        }

        public DataTable GetDataTable(SpreadsheetGear.IRange range, SpreadsheetGear.Data.GetDataFlags flags)
        {
            // Get a reference to the worksheet.
            SpreadsheetGear.IWorksheet worksheet = range.Worksheet;

            // Get a reference to all the worksheet cells.
            SpreadsheetGear.IRange cells = worksheet.Cells;

            // Get a reference to the advanced API.
            SpreadsheetGear.Advanced.Cells.IValues values =
                (SpreadsheetGear.Advanced.Cells.IValues)worksheet;

            // Create a new DataTable.
            DataTable dataTable = new DataTable();

            // Determine the row and column coordinates of the range.
            int row1 = range.Row;
            int col1 = range.Column;
            int rowCount = range.RowCount;
            int colCount = range.ColumnCount;
            int row2 = row1 + rowCount - 1;
            int col2 = col1 + colCount - 1;
            int row = row1;

            // If the first row is not used for column headers...
            if ((flags & SpreadsheetGear.Data.GetDataFlags.NoColumnHeaders) != 0)
            {
                // Create columns using simple column names.
                for (int col = col1; col <= col2; col++)
                {
                    string colName = "Column" + (col - col1 + 1);
                    dataTable.Columns.Add(colName);
                }
            }
            else
            {
                // Create columns using the first row in the range for column names.
                for (int col = col1; col <= col2; col++)
                {
                    // Use the IRange API to get formatted text.
                    string colName = cells[row, col].Text;
                    dataTable.Columns.Add(colName);
                }
                row++;
            }

            // If the DataTable column data types should be set...
            if ((flags & SpreadsheetGear.Data.GetDataFlags.NoColumnTypes) == 0 && row <= row2)
            {
                for (int col = col1; col <= col2; col++)
                {
                    // Get a reference to the DataTable column.
                    System.Data.DataColumn dataCol = dataTable.Columns[col - col1];

                    // If formatted text is to be used for all cell values...
                    if ((flags & SpreadsheetGear.Data.GetDataFlags.FormattedText) != 0)
                    {
                        // Set the data type to a string.
                        dataCol.DataType = typeof(string);
                    }
                    else
                    {
                        // Set the data type based on the type of data in the cell.
                        //
                        // Note that this will cause problems if a column does not contain
                        // consistent data types - for example a column of formulas where
                        // the first is numeric but one of the following is an error.
                        SpreadsheetGear.Advanced.Cells.IValue value = values[row, col];
                        if (value != null)
                        {
                            switch (value.Type)
                            {
                                case SpreadsheetGear.Advanced.Cells.ValueType.Number:
                                    dataCol.DataType = typeof(double);
                                    break;
                                case SpreadsheetGear.Advanced.Cells.ValueType.Text:
                                case SpreadsheetGear.Advanced.Cells.ValueType.Error:
                                    dataCol.DataType = typeof(string);
                                    break;
                                case SpreadsheetGear.Advanced.Cells.ValueType.Logical:
                                    dataCol.DataType = typeof(bool);
                                    break;
                            }
                        }
                    }
                }
            }

            // If formatted text is to be used for all cell values...
            if ((flags & SpreadsheetGear.Data.GetDataFlags.FormattedText) != 0)
            {
                // Create the row data as an array of strings.
                string[] rowData = new string[colCount];
                for (; row <= row2; row++)
                {
                    // If the row is not hidden...
                    if (!cells[row, 0].Rows.Hidden)
                    {
                        for (int col = col1; col <= col2; col++)
                        {
                            // Use the IRange API to get formatted text.
                            string text = cells[row, col].Text;
                            rowData[col - col1] = text;
                        }

                        // Add a new row using the array of formatted strings.
                        dataTable.Rows.Add(rowData);
                    }
                }
            }
            else
            {
                // Create the row data as an array of objects.
                object[] rowData = new object[colCount];
                for (; row <= row2; row++)
                {
                    // If the row is not hidden...
                    if (!cells[row, 0].Rows.Hidden)
                    {
                        for (int col = col1; col <= col2; col++)
                        {
                            // Use the advanced API to get the raw data values.
                            SpreadsheetGear.Advanced.Cells.IValue value = values[row, col];
                            object obj = null;
                            if (value != null)
                            {
                                switch (value.Type)
                                {
                                    case SpreadsheetGear.Advanced.Cells.ValueType.Number:
                                        obj = value.Number;
                                        break;
                                    case SpreadsheetGear.Advanced.Cells.ValueType.Text:
                                        obj = value.Text;
                                        break;
                                    case SpreadsheetGear.Advanced.Cells.ValueType.Logical:
                                        obj = value.Logical;
                                        break;
                                    case SpreadsheetGear.Advanced.Cells.ValueType.Error:
                                        // This will create problems if it is a column type
                                        // of double or bool.
                                        obj = "#" + value.Error.ToString().ToUpper() + "!";
                                        break;
                                }
                            }
                            rowData[col - col1] = obj;
                        }

                        // Add a new row using the array of objects.
                        dataTable.Rows.Add(rowData);
                    }
                }
            }

            // Return the DataTable.
            return dataTable;
        }

        protected void DeleteInstance(string cid)
        {
            DeleteInstance(new Guid(cid));
        }

        protected void DeleteInstance(Guid cid)
        {
            this.UMABroker.SaveOverride = true;
            this.UMABroker.DeleteCMInstance(cid);
            this.UMABroker.SetComplete();
            this.UMABroker.SetStart();
        }

        protected void SaveOrganizanition(DataSet ds)
        {
            this.UMABroker.SaveOverride = true;
            IOrganization organization = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            organization.SetData(ds);
            this.UMABroker.SetComplete();
            this.UMABroker.SetStart();
        }

        protected void SaveWellKnownComponent(DataSet ds, WellKnownCM wellKnownCm)
        {
            this.UMABroker.SaveOverride = true;
            var comp = UMABroker.GetWellKnownBMC(wellKnownCm);
            comp.SetData(ds);
            this.UMABroker.SetComplete();
            this.UMABroker.SetStart();
        }




        public static void AddFooterSecuritiesStatement(C1PrintDocument doc)
        {
            doc.PageLayouts.PrintFooterOnLastPage = true;
            RenderTable footer = new RenderTable();
            footer.Rows[0].Height = .30;
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;
            footer.Rows[1].Height = 1.5;
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.TextAlignVert = AlignVertEnum.Center;

            footer.Cells[0, 0].Text = "FOR YOUR INFORMATION";
            footer.Cells[0, 0].Style.TextAlignHorz = AlignHorzEnum.Center;
            footer.Cells[0, 0].Style.FontSize = 12;

            footer.Cells[1, 0].Text = "\nThe closing balance shown in this statement displays the transactions and holdings provided by the sponsoring broker.\n\nThe closing balance on this statement may not be the current holding balance. e-Clipse will not be liable for any financial loss incurred by any party who relies on the balance shown.\n\ne-Clipse Online Pty Ltd (ABN 70 145 358 630)\n3/36 Bydown Street, Neutral Bay, NSW 2089\nP: +61 2 9346 4686";
            footer.Cells[1, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            footer.Cells[1, 0].Style.TextAlignVert = AlignVertEnum.Top;
            footer.Cells[1, 0].Style.FontSize = 9;
            footer.Cells[1, 0].Style.FontBold = false;

            doc.PageLayouts.LastPage = new PageLayout();
            doc.PageLayouts.LastPage.PageFooter = footer;
        }

        public static void AddFooterMISStatement(C1PrintDocument doc)
        {
            doc.PageLayouts.PrintFooterOnLastPage = true;
            RenderTable footer = new RenderTable();
            footer.Rows[0].Height = .30;
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;
            footer.Rows[1].Height = 2;
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.TextAlignVert = AlignVertEnum.Center;

            footer.Cells[0, 0].Text = "FOR YOUR INFORMATION";
            footer.Cells[0, 0].Style.TextAlignHorz = AlignHorzEnum.Center;
            footer.Cells[0, 0].Style.FontSize = 12;

            footer.Cells[1, 0].Text = "\nThe balance shown in this statement is that recorded by e-Clipse at the close of each business day and is sourced through a daily Data Feed from the administrator of the Fund.\n\nSee the offer document for more details.\n\nThe closing balance shown may not be the current holding balance. e-Clipse will not be liable for any financial loss incurred by any party who relies on the balance shown without making their own adjustment for transactions.\n\ne-Clipse Online Pty Ltd (ABN 70 145 358 630)\n3/36 Bydown Street, Neutral Bay, NSW 2089\nhttp://www.e-clipse.com.au\nP: +61 2 9346 4686";
            footer.Cells[1, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            footer.Cells[1, 0].Style.TextAlignVert = AlignVertEnum.Top;
            footer.Cells[1, 0].Style.FontSize = 9;
            footer.Cells[1, 0].Style.FontBold = false;

            doc.PageLayouts.LastPage = new PageLayout();
            doc.PageLayouts.LastPage.PageFooter = footer;
        }

        public static void AddClientHeaderToReportWithAddress(C1PrintDocument doc, DataRow clientSummaryRow, string dateInfo, string reportname, string addressLine1, string addressLine2, string suburb, string postCode, string state, string country)
        {
            doc.PageLayout.PageSettings.Landscape = true;
            doc.PageLayout.PageSettings.TopMargin = .20;
            doc.PageLayout.PageSettings.LeftMargin = .60;
            doc.PageLayout.PageSettings.RightMargin = .60;

            RenderImage logo = new RenderImage();
            string imageDirectory = ConfigurationManager.AppSettings["Images"];
            logo.Image = System.Drawing.Image.FromFile(imageDirectory + "\\E-Clipse-logo-onwhite.png", true);

            RenderText cellText = new RenderText("Page [PageNo] of [PageCount]");
            cellText.Style.FontItalic = true;
            RenderTable theader = new RenderTable(doc);
            theader.Cells[0, 0].SpanRows = 3;
            theader.Cells[0, 0].RenderObject = logo;
            theader.Cells[0, 1].RenderObject = cellText;

            RenderText celltext = new RenderText(doc);
            celltext.Text = "e-Clipse Online\n";
            celltext.Style.TextAlignHorz = AlignHorzEnum.Right;
            celltext.Style.FontSize = 12;
            celltext.Style.FontBold = true;
            theader.Cells[1, 1].RenderObject = celltext;

            celltext = new RenderText(doc);
            celltext.Text = reportname + "\n";
            celltext.Style.TextColor = Color.FromArgb(46, 44, 83);
            celltext.Style.TextAlignHorz = AlignHorzEnum.Right;
            celltext.Style.FontSize = 14;
            celltext.Style.FontBold = true;
            theader.Cells[2, 1].RenderObject = celltext;

            celltext = new RenderText(doc);
            celltext.Text = dateInfo;
            celltext.Style.TextAlignHorz = AlignHorzEnum.Right;
            celltext.Style.FontSize = 12;
            celltext.Style.FontBold = true;
            theader.Cells[3, 1].RenderObject = celltext;

            celltext = new RenderText(doc);
            celltext.Text = "\n\n\n\n\n\n";
            theader.Cells[4, 1].RenderObject = celltext;

            doc.PageLayout.PageHeader = theader;
            doc.PageLayout.PageHeader.Style.TextAlignHorz = AlignHorzEnum.Right;
            doc.PageLayout.PageHeader.Style.TextAlignVert = AlignVertEnum.Center;
            doc.PageLayout.PageHeader.Height = "3cm";

            RenderTable secondHeader = new RenderTable(doc);
            secondHeader.Cells[2, 0].Text = "\n\n" + clientSummaryRow[HoldingRptDataSet.CLIENTNAME];
            secondHeader.Cells[2, 0].Style.TextAlignHorz = AlignHorzEnum.Left;

            string addressLine1And2 = string.Empty;
            if (addressLine2 == string.Empty)
                addressLine1And2 = addressLine1;
            else
                addressLine1And2 = addressLine1 + ", " + addressLine2;

            secondHeader.Cells[3, 0].Text = addressLine1And2;
            secondHeader.Cells[3, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            secondHeader.Cells[4, 0].Text = suburb +
                                            " " + state + " " + postCode;
            secondHeader.Cells[4, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            secondHeader.Cells[5, 0].Text = country;

            secondHeader.Cells[5, 0].Style.TextAlignHorz = AlignHorzEnum.Left;

            RenderText thcelltext = new RenderText(doc);
            thcelltext.Text = "\n\nInvestor Name";
            thcelltext.Style.FontBold = true;
            secondHeader.Cells[2, 1].RenderObject = thcelltext;
            secondHeader.Cells[2, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secondHeader.Cells[3, 1].Text = clientSummaryRow[HoldingRptDataSet.CLIENTNAME].ToString();
            secondHeader.Cells[3, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            thcelltext = new RenderText(doc);
            thcelltext.Text = "Client ID";
            thcelltext.Style.FontBold = true;
            secondHeader.Cells[4, 1].RenderObject = thcelltext;
            secondHeader.Cells[4, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secondHeader.Cells[5, 1].Text = clientSummaryRow[HoldingRptDataSet.CLIENTID].ToString();
            secondHeader.Cells[5, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            thcelltext = new RenderText(doc);
            thcelltext.Text = "Adviser";
            thcelltext.Style.FontBold = true;
            secondHeader.Cells[6, 1].RenderObject = thcelltext;
            secondHeader.Cells[6, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secondHeader.Cells[7, 1].Text = clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME].ToString() + "\n\n";
            secondHeader.Cells[7, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            string accountType = clientSummaryRow[HoldingRptDataSet.CLIENTTYPE].ToString();

            thcelltext = new RenderText(doc);
            thcelltext.Text = "Acount Type";
            thcelltext.Style.FontBold = true;
            secondHeader.Cells[6, 1].RenderObject = thcelltext;
            secondHeader.Cells[6, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secondHeader.Cells[7, 1].Text = accountType;
            secondHeader.Cells[7, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            doc.Body.Children.Add(secondHeader);
        }

        /// <summary>
        /// Set Title Page
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="imageName">Stored image file name at Images Root Directory</param>
        public static RenderToc AddTitleAndTOCPage(C1PrintDocument doc, string imageName)
        {
            doc.PageLayouts.FirstPage = new PageLayout(new C1PageSettings());
            doc.PageLayouts.FirstPage.PageSettings.Landscape = false;
            doc.PageLayouts.FirstPage.PageSettings.TopMargin = 0.0;
            doc.PageLayouts.FirstPage.PageSettings.LeftMargin = 0.0;
            doc.PageLayouts.FirstPage.PageSettings.RightMargin = 0.0;
            doc.PageLayouts.FirstPage.PageSettings.BottomMargin = 0.0;
            string imageDirectory = ConfigurationManager.AppSettings["Images"];
            RenderImage backgroundImage = new RenderImage();
            backgroundImage.Image = System.Drawing.Image.FromFile(imageDirectory + "\\" + "eclipseUmaTaxPack.png", true);
            backgroundImage.BreakAfter = BreakEnum.Page;
            doc.Body.Children.Add(backgroundImage);
            doc.PageLayouts.PrintHeaderOnFirstPage = false;

            RenderText tocHeader = new RenderText();
            tocHeader.Text = "\n\nTABLE OF CONTENTS\n\n\n";
            tocHeader.Style.TextColor = Color.Black;
            tocHeader.Style.FontSize = 15;
            tocHeader.Style.FontBold = true;
            tocHeader.Style.TextAlignHorz = AlignHorzEnum.Center;
            doc.Body.Children.Add(tocHeader);

            RenderToc renderTOC = new RenderToc();
            renderTOC.BreakAfter = BreakEnum.Page;
            doc.Body.Children.Add(renderTOC);

            return renderTOC;
        }

        public static void AddClientHeaderToReport(C1PrintDocument doc, DataRow clientSummaryRow, string dateInfo, string reportname)
        {
            doc.PageLayout.PageSettings.Landscape = true;
            doc.PageLayout.PageSettings.TopMargin = .20;
            doc.PageLayout.PageSettings.LeftMargin = .60;
            doc.PageLayout.PageSettings.RightMargin = .60;

            RenderImage logo = new RenderImage();
            string imageDirectory = ConfigurationManager.AppSettings["Images"];
            logo.Image = System.Drawing.Image.FromFile(imageDirectory + "\\E-Clipse-logo-onwhite.png", true);

            RenderText cellText = new RenderText("Page [PageNo] of [PageCount]");
            cellText.Style.FontItalic = true;
            RenderTable theader = new RenderTable(doc);
            theader.Cells[0, 0].SpanRows = 3;
            theader.Cells[0, 0].RenderObject = logo;
            theader.Cells[0, 1].RenderObject = cellText;

            RenderText celltext = new RenderText(doc);
            celltext.Text = "e-Clipse Online\n";
            celltext.Style.TextAlignHorz = AlignHorzEnum.Right;
            celltext.Style.FontSize = 12;
            celltext.Style.FontBold = true;
            theader.Cells[1, 1].RenderObject = celltext;

            celltext = new RenderText(doc);
            celltext.Text = reportname + "\n";
            celltext.Style.TextColor = Color.FromArgb(46, 44, 83);
            celltext.Style.TextAlignHorz = AlignHorzEnum.Right;
            celltext.Style.FontSize = 14;
            celltext.Style.FontBold = true;
            theader.Cells[2, 1].RenderObject = celltext;

            celltext = new RenderText(doc);
            celltext.Text = dateInfo;
            celltext.Style.TextAlignHorz = AlignHorzEnum.Right;
            celltext.Style.FontSize = 12;
            celltext.Style.FontBold = true;
            theader.Cells[3, 1].RenderObject = celltext;

            celltext = new RenderText(doc);
            celltext.Text = "\n\n\n\n\n\n";
            theader.Cells[4, 1].RenderObject = celltext;

            doc.PageLayout.PageHeader = theader;
            doc.PageLayout.PageHeader.Style.TextAlignHorz = AlignHorzEnum.Right;
            doc.PageLayout.PageHeader.Style.TextAlignVert = AlignVertEnum.Center;
            doc.PageLayout.PageHeader.Height = "3cm";

            RenderTable secondHeader = new RenderTable(doc);
            secondHeader.Cells[2, 0].Text = "\n\n" + clientSummaryRow[HoldingRptDataSet.CLIENTNAME];
            secondHeader.Cells[2, 0].Style.TextAlignHorz = AlignHorzEnum.Left;

            string addressLine1And2 = string.Empty;
            if (clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2].ToString() == string.Empty)
                addressLine1And2 = clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1].ToString();
            else
                addressLine1And2 = clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1].ToString() + ", " + clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2].ToString();
            secondHeader.Cells[3, 0].Text = addressLine1And2;
            secondHeader.Cells[3, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            secondHeader.Cells[4, 0].Text = clientSummaryRow[HoldingRptDataSet.SUBURB].ToString() +
                                            " " + clientSummaryRow[HoldingRptDataSet.STATE].ToString() + " " + clientSummaryRow[HoldingRptDataSet.POSTCODE].ToString();
            secondHeader.Cells[4, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            secondHeader.Cells[5, 0].Text = clientSummaryRow[HoldingRptDataSet.COUNTRY].ToString();

            secondHeader.Cells[5, 0].Style.TextAlignHorz = AlignHorzEnum.Left;

            RenderText thcelltext = new RenderText(doc);
            thcelltext.Text = "\n\nInvestor Name";
            thcelltext.Style.FontBold = true;
            secondHeader.Cells[2, 1].RenderObject = thcelltext;
            secondHeader.Cells[2, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secondHeader.Cells[3, 1].Text = clientSummaryRow[HoldingRptDataSet.CLIENTNAME].ToString();
            secondHeader.Cells[3, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            thcelltext = new RenderText(doc);
            thcelltext.Text = "Client ID";
            thcelltext.Style.FontBold = true;
            secondHeader.Cells[4, 1].RenderObject = thcelltext;
            secondHeader.Cells[4, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secondHeader.Cells[5, 1].Text = clientSummaryRow[HoldingRptDataSet.CLIENTID].ToString();
            secondHeader.Cells[5, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            thcelltext = new RenderText(doc);
            thcelltext.Text = "Adviser";
            thcelltext.Style.FontBold = true;
            secondHeader.Cells[6, 1].RenderObject = thcelltext;
            secondHeader.Cells[6, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secondHeader.Cells[7, 1].Text = clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME].ToString() + "\n\n";
            secondHeader.Cells[7, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            string accountType = clientSummaryRow[HoldingRptDataSet.CLIENTTYPE].ToString();

            thcelltext = new RenderText(doc);
            thcelltext.Text = "Acount Type";
            thcelltext.Style.FontBold = true;
            secondHeader.Cells[6, 1].RenderObject = thcelltext;
            secondHeader.Cells[6, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secondHeader.Cells[7, 1].Text = accountType;
            secondHeader.Cells[7, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            //if (accountType == "e-Clipse Super")
            //{
            //    thcelltext = new RenderText(doc);
            //    thcelltext.Text = "Acount Type";
            //    thcelltext.Style.FontBold = true;
            //    secondHeader.Cells[6, 1].RenderObject = thcelltext;
            //    secondHeader.Cells[6, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            //    secondHeader.Cells[7, 1].Text = clientSummaryRow[HoldingRptDataSet.CLIENTTYPE].ToString();
            //    secondHeader.Cells[7, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
            //}

            doc.Body.Children.Add(secondHeader);
        }

        public static void AddHeaderToReport(C1PrintDocument doc, string dateInfo, string reportname)
        {
            doc.PageLayout.PageSettings.Landscape = true;
            doc.PageLayout.PageSettings.TopMargin = .20;
            doc.PageLayout.PageSettings.LeftMargin = .60;
            doc.PageLayout.PageSettings.RightMargin = .60;

            RenderImage logo = new RenderImage();
            string imageDirectory = ConfigurationManager.AppSettings["Images"];
            logo.Image = System.Drawing.Image.FromFile(imageDirectory + "\\E-Clipse-logo-onwhite.png", true);

            RenderText cellText = new RenderText("Page [PageNo] of [PageCount]");
            cellText.Style.FontItalic = true;
            RenderTable theader = new RenderTable(doc);
            theader.Cells[0, 0].SpanRows = 3;
            theader.Cells[0, 0].RenderObject = logo;
            theader.Cells[0, 1].RenderObject = cellText;

            RenderText celltext = new RenderText(doc);
            celltext.Text = "e-Clipse Online\n";
            celltext.Style.TextAlignHorz = AlignHorzEnum.Right;
            celltext.Style.FontSize = 12;
            celltext.Style.FontBold = true;
            theader.Cells[1, 1].RenderObject = celltext;

            celltext = new RenderText(doc);
            celltext.Text = reportname + "\n";
            celltext.Style.TextColor = Color.FromArgb(46, 44, 83);
            celltext.Style.TextAlignHorz = AlignHorzEnum.Right;
            celltext.Style.FontSize = 16;
            celltext.Style.FontBold = true;
            theader.Cells[2, 1].RenderObject = celltext;

            celltext = new RenderText(doc);
            celltext.Text = dateInfo;
            celltext.Style.TextAlignHorz = AlignHorzEnum.Right;
            celltext.Style.FontSize = 12;
            celltext.Style.FontBold = true;
            theader.Cells[3, 1].RenderObject = celltext;

            celltext = new RenderText(doc);
            celltext.Text = "\n\n\n\n\n\n";
            theader.Cells[4, 1].RenderObject = celltext;

            doc.PageLayout.PageHeader = theader;
            doc.PageLayout.PageHeader.Style.TextAlignHorz = AlignHorzEnum.Right;
            doc.PageLayout.PageHeader.Style.TextAlignVert = AlignVertEnum.Center;
            doc.PageLayout.PageHeader.Height = "3cm";
        }

        public virtual Guid CMTypeGuid
        {
            get
            {
                return Guid.Empty;
            }
        }

        public string RequestInstanceID
        {
            get
            {
                if (Context.Items.Contains("RequestInstanceID"))
                    return Context.Items["RequestInstanceID"].ToString();
                else
                {
                    Context.Items.Add("RequestInstanceID", "");
                    return Context.Items["RequestInstanceID"].ToString();
                }
            }
            set
            {
                if (Context.Items.Contains("RequestInstanceID"))
                    Context.Items["RequestInstanceID"] = value;
                else
                    Context.Items.Add("RequestInstanceID", value);
            }
        }

        protected virtual void GetBMCData()
        {

        }

        public virtual void LoadPage()
        {
            this.GetBMCData();
            this.PopulatePage(this.presentationData);
        }

        public virtual void PopulatePage(DataSet ds)
        {

        }

        public void SetReportContinousProperty(C1ReportViewerToolbar toolbar)
        {
            for (int i = toolbar.Controls.Count - 1; i >= 0; i--)
            {
                string className = ((HtmlGenericControl)toolbar.Controls[i]).Attributes["class"];
                if (!string.IsNullOrEmpty(className) && className.Contains("continuousview"))
                {
                    CheckBox chkBox = (CheckBox)toolbar.Controls[i];
                    chkBox.Checked = true;
                }
            }
        }

        public UserEntity GetCurrentUser()
        {
            return GetCurrentUser(this.User.Identity.Name);
        }

        public UserEntity GetCurrentUser(string userName)
        {
            DBUser objUser = (DBUser)UMABroker.GetBMCInstance(userName, "DBUser_1_1");
            return GetCurrentUser(objUser);
        }

        public UserEntity GetCurrentUser(DBUser objUser)
        {
            UserEntity UserEntity = new UserEntity();
            UserEntity.CID = objUser.CID;
            UserEntity.CurrentUserName = objUser.Name;
            UserEntity.IsAdmin = objUser.Administrator;
            UserEntity.IsWebUser = true;
            UserEntity.UserType = (int)objUser.UserType;
            UserEntity.Email = objUser.Email;
            this.UMABroker.ReleaseBrokerManagedComponent(objUser);

            return UserEntity;
        }
        public static bool CheckAdviserForOrderPad(DBUser user)
        {
            if (user.UserType == UserType.Advisor || user.UserType == UserType.Innova)
                return true;
            else
                return false;
        }

        #region Private Methods

        private void InitialiseUMABroker()
        {
            this.UMABroker = new CMBroker(Context.User, DBConnection.Connection.ConnectionString);
            this.UMABroker.SetStart();
        }

        protected virtual void Intialise()
        {

        }

        protected override void OnUnload(EventArgs e)
        {
            base.OnUnload(e);
            this.CleanUpUMABroker();
            Thread.SetData(Thread.GetNamedDataSlot("Broker"), null);
        }

        private void CleanUpUMABroker()
        {
            if (this.umaBroker != null)
            {
                if (this.umaBroker.InTransaction)
                    this.umaBroker.SetAbort();
                else
                {
                    try
                    {
                        this.umaBroker.SetComplete();
                    }
                    catch { }
                }
                this.umaBroker.Dispose();
            }
        }

        public static bool IsTestApp()
        {
            return DBConnection.IsTestMode;
        }


        #endregion
    }
}