﻿using System;
using System.IO;
using Oritax.TaxSimp.Utilities;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.Services;
using System.Web.Security;
using System.Web;
using System.Security.Principal;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            //var Runtime = WorkflowInit.Runtime;
        }

        void Application_End(object sender, EventArgs e)
        {
        }

        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            //String cookieName = FormsAuthentication.FormsCookieName;
            //HttpCookie authCookie = Context.Request.Cookies[cookieName];

            //if (authCookie == null)
            //{
            //    return;
            //}

            //FormsAuthenticationTicket authTicket = null;
            //try
            //{
            //    authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            //}
            //catch
            //{
            //    return;
            //}

            //if (authTicket == null)
            //{
            //    return;
            //}

            //if (authTicket.Expired)
            //{
            //    UserAuthentication.LogUserOut(authTicket.Name);
            //    return;
            //}

            //if (!UserAuthentication.IsUserLoggedIn(authTicket.Name))
            //{
            //    FormsAuthentication.SignOut();
            //    Response.Redirect("Account/Login.aspx?ReturnURL=" + HttpUtility.UrlEncode(Request.Url.ToString()));
            //    return;
            //}

            //FormsAuthenticationTicket newAuthTicket = new FormsAuthenticationTicket(authTicket.Version, authTicket.Name, authTicket.IssueDate, DateTime.Now.AddMinutes(UserAuthentication.AuthenticationTimeout), false, authTicket.UserData);
            //string encryptedTicket = FormsAuthentication.Encrypt(newAuthTicket);
            //authCookie.Value = encryptedTicket;

            //Context.Response.Cookies.Set(authCookie);

            //UserAuthentication.UserRefreshedSession(authTicket.Name);

            ////When the ticket was created, the UserData property was assigned a
            ////pipe-delimited string of group names.
            //String[] groups = newAuthTicket.UserData.Split(new char[] { '|' });

            ////Create an Identity.
            //GenericIdentity id = new GenericIdentity(newAuthTicket.Name, "LdapAuthentication");

            ////This principal flows throughout the request.
            //TXPrincipal principal = new TXPrincipal(id, groups);

            //Context.User = principal;
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            if (Request.Url.Host != "localhost")
            {
                switch (Request.Url.Scheme)
                {
                    case "https":
                        Response.AddHeader("Strict-Transport-Security", "max-age=300");
                        break;
                    case "http":
                        var path = "https://" + Request.Url.Host + Request.Url.PathAndQuery;
                        Response.Status = "301 Moved Permanently";
                        Response.AddHeader("Location", path);
                        break;
                }
            }
        }

        void Application_Error(object sender, EventArgs e)
        {
            // don't want to recurse if there is an error on the error page
            if (Request.Url.PathAndQuery.IndexOf("pageerror") < 0)
            {
                string strErrorLogFile = "";
                try
                {
                    if(Context.Error!=null)
                    strErrorLogFile = Utilities.LogException(Context.Error.GetBaseException(),this.Request,this.Context);
                }
                catch
                {
                    // if there is an error, ignore it, otherwise the system will
                    // end up in a loop on the error handler.
                }
                finally
                {
                    var url = string.Format("~/PageError.aspx?ErrorLog={0}", strErrorLogFile);
                    Response.Redirect(url, false);
                }
            }

        }

        void Session_Start(object sender, EventArgs e)
        {
            Session["init"] = 0;
        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

    }
}
