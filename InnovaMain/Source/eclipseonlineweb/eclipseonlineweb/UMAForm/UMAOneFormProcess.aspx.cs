﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.DataSets;
using System.Data;
using C1.Web.Wijmo.Controls.C1ComboBox;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.CM.Organization.UMAForm;
using Aspose.Words;
using Oritax.TaxSimp.Data;
using Aspose.Words.Tables;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using eclipseonlineweb.UMAForm.HelperClass;
using System.Text;
using Oritax.TaxSimp.Security;

namespace eclipseonlineweb.UMAForm
{
    public partial class UMAOneFormProcess : UMABasePage
    {
        // ReSharper disable InconsistentNaming
        SecuritiesDS presentationDataSecuritiesDS = new SecuritiesDS();
        // ReSharper restore InconsistentNaming

        #region Account

        public string CreatedByUser
        {
            get
            {
                if (ViewState["CreatedByUser"] != null)
                    return ViewState["CreatedByUser"].ToString();
                else
                {
                    ViewState["CreatedByUser"] = string.Empty;
                    return ViewState["CreatedByUser"].ToString();
                }
            }
            set
            {

                ViewState["CreatedByUser"] = value;

            }
        }

        public string ModifiedByUser
        {
            get
            {
                if (ViewState["ModifiedByUser"] != null)
                    return ViewState["ModifiedByUser"].ToString();
                else
                {
                    ViewState["ModifiedByUser"] = string.Empty;
                    return ViewState["ModifiedByUser"].ToString();
                }
            }
            set
            {

                ViewState["ModifiedByUser"] = value;
            }
        }

        public string CreatedOnDate
        {
            get
            {
                if (ViewState["CreatedOnDate"] != null)
                    return ViewState["CreatedOnDate"].ToString();
                else
                {
                    ViewState["CreatedOnDate"] = string.Empty;
                    return ViewState["CreatedOnDate"].ToString();
                }
            }
            set
            {
                ViewState["CreatedOnDate"] = value;
            }
        }


        public string AccountType
        {
            get
            {
                if (RBAccountType.SelectedItem != null)
                    return RBAccountType.SelectedItem.Text;
                return string.Empty;
            }

            set
            {
                if (value != string.Empty)
                {
                    if (RBAccountType.Items.FindByText(value) != null)
                    {
                        RBAccountType.Items.FindByText(value).Selected = true;
                    }
                }
            }
        }

        public string AccountNO
        {
            get
            {
                return txtAccountNo.Text;
            }
            set
            {
                txtAccountNo.Text = value;
            }
        }

        public string AccountName
        {
            get
            {
                return txtAccountName.Text;
            }
            set
            {
                txtAccountName.Text = value;
            }
        }

        public string TrustName
        {
            get
            {
                return txtTrusteeName.Text;
            }
            set
            {
                txtTrusteeName.Text = value;
            }
        }
        protected Guid IFACID
        {
            get { return string.IsNullOrEmpty(cmbIFA.SelectedValue) ? Guid.Empty : Guid.Parse(cmbIFA.SelectedValue); }
            set { cmbIFA.SelectedValue = value.ToString(); }
        }


        #endregion

        #region Fee

        public double UpFrontFee
        {
            get
            {
                return txtUpfrontFee.Value;
            }
            set
            {
                txtUpfrontFee.Value = value;
            }
        }
        public double OngoingFeeAmount
        {
            get
            {
                return txtOngoingFee.Value;
            }
            set
            {
                txtOngoingFee.Value = value;
            }
        }
        public double OngoingFeePercentage
        {
            get
            {
                return txtOngoingFeePercentage.Value;
            }
            set
            {
                txtOngoingFeePercentage.Value = value;
            }
        }
        public double TierTo1
        {
            get
            {
                return txtToTier1.Value;
            }
            set
            {
                txtToTier1.Value = value;
            }
        }
        public double TierFrom1
        {
            get
            {
                return txtFromTier1.Value;
            }
            set
            {
                txtFromTier1.Value = value;
            }
        }
        public double TierPercentage1
        {
            get
            {
                return txtPercentTier1.Value;
            }
            set
            {
                txtPercentTier1.Value = value;
            }
        }
        public double TierTo2
        {
            get
            {

                return txtToTier2.Value;
            }
            set
            {
                txtToTier2.Value = value;
            }
        }
        public double TierFrom2
        {
            get
            {
                return txtFromTier2.Value;
            }
            set
            {
                txtFromTier2.Value = value;
            }
        }
        public double TierPercentage2
        {
            get
            {
                return txtPercentTier2.Value;
            }
            set
            {
                txtPercentTier2.Value = value;
            }
        }
        public double TierTo3
        {
            get
            {
                return txtToTier3.Value;
            }
            set
            {
                txtToTier3.Value = value;
            }
        }
        public double TierFrom3
        {
            get
            {
                return txtFromTier3.Value;
            }
            set
            {
                txtFromTier3.Value = value;
            }
        }
        public double TierPercentage3
        {
            get
            {
                return txtPercentTier3.Value;
            }
            set
            {
                txtPercentTier3.Value = value;
            }
        }
        public double TierTo4
        {
            get
            {
                return txtToTier4.Value;
            }
            set
            {
                txtToTier4.Value = value;
            }
        }
        public double TierFrom4
        {
            get
            {
                return txtFromTier4.Value;
            }
            set
            {
                txtFromTier4.Value = value;
            }
        }
        public double TierPercentage4
        {
            get
            {
                return txtPercentTier4.Value;
            }
            set
            {
                txtPercentTier4.Value = value;
            }
        }
        public double TierTo5
        {
            get
            {
                return txtToTier5.Value;
            }
            set
            {
                txtToTier5.Value = value;
            }
        }
        public double TierFrom5
        {
            get
            {
                return txtFromTier5.Value;
            }
            set
            {
                txtFromTier5.Value = value;
            }
        }
        public double TierPercentage5
        {
            get
            {
                return txtPercentTier5.Value;
            }
            set
            {
                txtPercentTier5.Value = value;
            }
        }

        #endregion

        #region TFN ABN ABRN

        public string TFNSig1
        {
            get
            {
                return txtTFNClient1.Text;
            }
            set
            {
                txtTFNClient1.Text = value;
            }
        }

        public string TFNSig2
        {
            get
            {
                return txtTFNClient2.Text;
            }
            set
            {
                txtTFNClient2.Text = value;
            }
        }

        public string TFNSig3
        {
            get
            {
                return txtTFNClient3.Text;
            }
            set
            {
                txtTFNClient3.Text = value;
            }
        }

        public string TFNSig4
        {
            get
            {
                return txtTFNClient4.Text;
            }
            set
            {
                txtTFNClient4.Text = value;
            }
        }

        public string TFNTrustSuperfund
        {
            get
            {
                return txtTFNTrustSuperfund.Text;
            }
            set
            {
                txtTFNTrustSuperfund.Text = value;
            }
        }

        public bool HaveABNForTaxWithHolding
        {
            get
            {
                if (rbHaveAbnYes.Checked)
                    return true;
                else
                    return false;
            }
            set
            {
                if (value)
                    rbHaveAbnYes.Checked = true;
                else
                    rbHaveAbnNo.Checked = true;
            }
        }

        public String CountryABNForTaxWithHolding
        {
            get
            {
                return TFNABNCountryList.SelectedItem.Text;
            }
            set
            {
                TFNABNCountryList.Text = value;
            }
        }

        #endregion

        #region Authority to Act

        public bool AuthorityToActEclispe
        {
            get
            { return chkSelectLimitedPowerAttorneyEclipse.Checked; }
            set
            {
                chkSelectLimitedPowerAttorneyEclipse.Checked = value;
            }
        }


        public bool AuthorityToActAMM
        {
            get
            { return chkLimitedPowerAttorneyAMM.Checked; }
            set
            { chkLimitedPowerAttorneyAMM.Checked = value; }
        }

        public bool AuthorityToActMacBank
        {
            get
            { return chkLimitedPowerAttorneyMacBankCMA.Checked; }
            set
            { chkLimitedPowerAttorneyMacBankCMA.Checked = value; }
        }

        public bool InvestorStatusWholesaleInvestor
        {
            get
            { return rbWholesaleInvestor.Checked; }
            set
            { rbWholesaleInvestor.Checked = value; }
        }

        public bool InvestorStatusSophisticatedInvestor
        {
            get
            { return rbSophisticatedInvestor.Checked; }
            set
            { rbSophisticatedInvestor.Checked = value; }
        }

        public bool InvestorStatusProfessionalInvestor
        {
            get
            { return rbProfessionalInvestor.Checked; }
            set
            { rbProfessionalInvestor.Checked = value; }
        }

        public bool InvestorStatusRetailInvestor
        {
            get
            { return rbRetailInvestor.Checked; }
            set
            { rbRetailInvestor.Checked = value; }
        }

        public bool CertificateFromAccountantIsAttached
        {
            get
            { return chkCertificateFromAccountantAttached.Checked; }
            set
            { chkCertificateFromAccountantAttached.Checked = value; }
        }

        #endregion

        #region Services
        public List<SecuritiesExclusionEntity> SecuritiesExclusionListEntityList
        {
            get
            {
                if (ViewState["SecuritiesExclusionEntityList"] == null)
                    ViewState.Add("SecuritiesExclusionEntityList", new List<SecuritiesExclusionEntity>());


                return (List<SecuritiesExclusionEntity>)ViewState["SecuritiesExclusionEntityList"];
            }
            set
            {
                if (ViewState["SecuritiesExclusionEntityList"] == null)
                    ViewState.Add("SecuritiesExclusionEntityList", new List<SecuritiesExclusionEntity>());


                ViewState["SecuritiesExclusionEntityList"] = value;
            }
        }

        public List<StateStreetEntity> StateStreetEntityList
        {
            get
            {
                if (ViewState["StateStreetEntityList"] == null)
                    ViewState.Add("StateStreetEntityList", new List<StateStreetEntity>());


                return (List<StateStreetEntity>)ViewState["StateStreetEntityList"];
            }
            set
            {
                if (ViewState["StateStreetEntityList"] == null)
                    ViewState.Add("StateStreetEntityList", new List<StateStreetEntity>());


                ViewState["StateStreetEntityList"] = value;
            }
        }

        public decimal DIYAmount
        {
            get
            {
                return Convert.ToDecimal(txtDIYInvestment.Value);
            }
            set { txtDIYInvestment.Value = Convert.ToDouble(value); }
        }

        public decimal DIWMAmount
        {
            get
            {

                return Convert.ToDecimal(txtDIWMInvestment.Value);
            }
            set { txtDIWMInvestment.Value = Convert.ToDouble(value); }
        }

        public string InvestmentProgram
        {
            get
            {
                if (C1ComboBoxInvestmentProgram.SelectedItem != null)
                    return C1ComboBoxInvestmentProgram.SelectedItem.Text;
                else
                {
                    var comboItem = C1ComboBoxInvestmentProgram.Items.Where(item => item.Text.ToLower() == C1ComboBoxInvestmentProgram.Text.ToLower()).FirstOrDefault();
                    if (comboItem != null)
                    {
                        comboItem.Selected = true;
                        return comboItem.Text;
                    }
                    else
                        return String.Empty;
                }
            }
            set
            {
                C1ComboBoxInvestmentProgram.Text = value;
            }
        }

        public string InvestmentProgramCode
        {
            get
            {
                if (C1ComboBoxInvestmentProgram.SelectedItem != null)
                    return C1ComboBoxInvestmentProgram.SelectedItem.Value;
                var comboItem = C1ComboBoxInvestmentProgram.Items.Where(item => item.Text.ToLower() == C1ComboBoxInvestmentProgram.Text.ToLower()).FirstOrDefault();
                if (comboItem != null)
                {
                    comboItem.Selected = true;
                    return comboItem.Value;
                }
                else
                    return String.Empty;
            }
            set
            {
                C1ComboBoxItem item = C1ComboBoxInvestmentProgram.Items.Where(it => it.Value == value).FirstOrDefault();
                if (item != null)
                    item.Selected = true;
            }
        }

        public decimal DIFMAmount
        {
            get
            {

                return Convert.ToDecimal(txtDIFMInvestment.Value);
            }
            set { txtDIFMInvestment.Value = Convert.ToDouble(value); }
        }

        public bool DoItYourSelf
        {
            get
            {
                return chkDIYOption.Checked;
            }
            set
            {
                chkDIYOption.Checked = value;
            }
        }

        public bool DIYBWA
        {
            get
            {
                return chkDIYOptionBWA.Checked;
            }
            set
            {
                chkDIYOptionBWA.Checked = value;
            }

        }

        public bool DIYDesktopBroker
        {
            get
            {
                return chkDIYOptionASX.Checked;
            }
            set
            {
                chkDIYOptionASX.Checked = value;
            }

        }

        public bool DIYFIIG
        {
            get
            {
                return chkDIYOptionFIIG.Checked;
            }
            set
            {
                chkDIYOptionFIIG.Checked = value;
            }

        }

        public bool DIYAMM
        {
            get
            {
                return chkDIYOptionAMMM.Checked;
            }
            set
            {
                chkDIYOptionAMMM.Checked = value;
            }
        }

        public bool DoItWithMe
        {
            get
            {
                return chkDIWNOption.Checked;
            }
            set
            {
                chkDIWNOption.Checked = value;
            }
        }

        public bool DIWNMBWA
        {
            get
            {
                return chkDIWNOptionBWA.Checked;
            }
            set
            {
                chkDIWNOptionBWA.Checked = value;
            }
        }

        public bool DIWNMDesktopBroker
        {
            get
            {
                return chkDIWNOptionASX.Checked;
            }
            set
            {
                chkDIWNOptionASX.Checked = value;
            }
        }

        public bool DIWNMFIIG
        {
            get
            {
                return chkDIWNOptionFIIG.Checked;
            }
            set
            {
                chkDIWNOptionFIIG.Checked = value;
            }
        }

        public bool DIWNMAMM
        {
            get
            {
                return chkDIWNOptionAMMM.Checked;
            }
            set
            {
                chkDIWNOptionAMMM.Checked = value;
            }
        }

        public bool DIWNMSS
        {
            get
            {
                return chkDIWNOptionStateStreet.Checked;
            }
            set
            {
                chkDIWNOptionStateStreet.Checked = value;
            }
        }

        public bool DIWMServiceManagedViaMDA
        {
            get
            {
                return chkServiceManagedViaMDA.Checked;
            }
            set
            {
                chkServiceManagedViaMDA.Checked = value;
            }
        }

        public bool DoItForhMe
        {
            get
            {
                return chkDIFMOption.Checked;
            }
            set
            {
                chkDIFMOption.Checked = value;
            }
        }

        public bool DIFNMBWA
        {
            get
            {
                return chkDIFMOptionBWA.Checked;
            }
            set
            {
                chkDIFMOptionBWA.Checked = value;
            }
        }

        public bool DIFNMDesktopBroker
        {
            get
            {
                return chkDIFMOptionASX.Checked;
            }
            set
            {
                chkDIFMOptionASX.Checked = value;
            }
        }

        public bool DIFNMSS
        {
            get
            {
                return chkDIFMOptionSS.Checked;
            }
            set
            {
                chkDIFMOptionSS.Checked = value;
            }
        }

        public bool DIFMNominatePreferences
        {
            get
            {
                return chkNominateInvestmentPreference.Checked;
            }
            set
            {
                chkNominateInvestmentPreference.Checked = value;
            }
        }


        public string MinBalance
        {
            get
            {
                return txtMinBalance.Value.ToString();
            }
            set
            {
                if (value != string.Empty)
                    txtMinBalance.Value = Convert.ToDouble(value);
            }
        }

        public string MinBalancePercentage
        {
            get
            {
                return txtMinBalancePercentage.Value.ToString();
            }
            set
            {
                if (value != string.Empty)
                    txtMinBalancePercentage.Value = Convert.ToDouble(value);
            }
        }

        public string MinTrade
        {
            get
            {
                return txtMinTrade.Value.ToString();
            }
            set
            {
                if (value != string.Empty)
                    txtMinTrade.Value = Convert.ToDouble(value);
            }
        }

        public string MinTradePercentage
        {
            get
            {
                return txtMinTradePercentage.Value.ToString();
            }
            set
            {
                if (value != string.Empty)
                    txtMinTradePercentage.Value = Convert.ToDouble(value);
            }
        }

        public bool AssignedAllocatedValueHoldSell
        {
            get
            {
                return chkAssignedAllocatedValueHoldSell.Checked;
            }
            set
            {
                chkAssignedAllocatedValueHoldSell.Checked = value;
            }

        }

        public bool DistributedAllocatedValueHoldSell
        {
            get
            {
                return chkDistributedAllocatedValueHoldSell.Checked;
            }
            set
            {
                chkDistributedAllocatedValueHoldSell.Checked = value;
            }

        }

        public bool AssignedAllocatedValueExclusion
        {
            get
            {
                return chkAssignedAllocatedValueExclusion.Checked;
            }
            set
            {
                chkAssignedAllocatedValueExclusion.Checked = value;
            }

        }

        public bool DistributedAllocatedValueExclusion
        {
            get
            {
                return chkDistributedAllocatedValueExclusion.Checked;
            }
            set
            {
                chkDistributedAllocatedValueExclusion.Checked = value;
            }

        }

        public bool AssignedAllocatedValueMinTradeHolding
        {
            get
            {
                return chkAssignedAllocatedValueMinTradeHolding.Checked;
            }
            set
            {
                chkAssignedAllocatedValueMinTradeHolding.Checked = value;
            }

        }

        public bool DistributedAllocatedValueMinTradeHolding
        {
            get
            {
                return chkDistributedAllocatedValueMinTradeHolding.Checked;
            }
            set
            {
                chkDistributedAllocatedValueMinTradeHolding.Checked = value;
            }

        }

        #endregion

        #region Platforms
        public string NameOfPlatformsBT
        {
            get
            { return txtPTName1.Text; }
            set
            { txtPTName1.Text = value; }
        }
        public string InvestorNoBT
        {
            get
            { return txtInvestor1.Text; }
            set
            { txtInvestor1.Text = value; }
        }
        public string AdviserNoBT
        {
            get
            { return txtAdviser1.Text; }
            set
            { txtAdviser1.Text = value; }
        }
        public string ProviderNoBT
        {
            get
            { return txtProvider1.Text; }
            set
            { txtProvider1.Text = value; }
        }

        public string NameOfPlatformsEclipse
        {
            get
            { return txtPlatform2.Text; }
            set
            { txtPlatform2.Text = value; }
        }
        public string InvestorNoEclipse
        {
            get
            { return txtInvestor2.Text; }
            set
            { txtInvestor2.Text = value; }
        }
        public string AdviserNoEclipse
        {
            get
            { return txtAdviser2.Text; }
            set
            { txtAdviser2.Text = value; }
        }
        public string ProviderNoEclipse
        {
            get
            { return txtProvider2.Text; }
            set
            { txtProvider2.Text = value; }
        }

        public bool MaqCMALinkBWA
        {
            get
            {

                if (rbMacquarieBankwestLink.SelectedItem != null)
                {
                    if (rbMacquarieBankwestLink.SelectedItem.Value == "1")
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            set
            {
                if (value)
                    rbMacquarieBankwestLink.Items.FindByValue("1").Selected = true;
                else
                    rbMacquarieBankwestLink.Items.FindByValue("0").Selected = true;
            }
        }
        public string MaqCMADIYDIWM
        {
            get
            {
                if (rbMacquarieDIYDIWM.SelectedItem != null)
                {
                    return rbMacquarieDIYDIWM.SelectedItem.Text;
                }
                else
                    return string.Empty;
            }
            set
            {
                if (value != String.Empty)
                    rbMacquarieDIYDIWM.Items.FindByText(value).Selected = true;
            }
        }
        public string MaqCMABSB
        {
            get
            { return InputBSB.Text; }
            set
            { InputBSB.Text = value; }
        }
        public string MaqCMAAccountNo
        {
            get
            { return InoutAccNo.Text; }
            set
            { InoutAccNo.Text = value; }
        }
        public string MaqCMAAccountName
        {
            get
            { return txtAccountNameMaq.Text; }
            set
            { txtAccountNameMaq.Text = value; }
        }
        #endregion

        #region BWA Option

        public bool DIYPhoneAccess
        {
            get
            {
                return chkPhoneAccessDIY.Checked;
            }
            set
            {
                chkPhoneAccessDIY.Checked = value;
            }
        }

        public bool DIYOnlineAccess
        {
            get
            {
                return chkOnlineAccessDIY.Checked;
            }
            set
            {
                chkOnlineAccessDIY.Checked = value;
            }
        }

        public bool DIYDebitCard
        {
            get
            {
                return chkDebitCardDIY.Checked;
            }
            set
            {
                chkDebitCardDIY.Checked = value;
            }
        }

        public bool DIYChequeBook
        {
            get
            {
                return chkCequeBook25DIY.Checked;
            }
            set
            {
                chkCequeBook25DIY.Checked = value;
            }
        }

        public bool DIYDepositBook
        {
            get
            {
                return chkDepositBookDIY.Checked;
            }
            set
            {
                chkDepositBookDIY.Checked = value;
            }
        }


        public string DIYMannerOfOperation
        {
            get
            {
                if (rbMannerOfOperationOptionsDIY.SelectedItem != null)
                    return rbMannerOfOperationOptionsDIY.SelectedItem.Value;

                return string.Empty;
            }
            set
            {
                if (value != string.Empty)
                    rbMannerOfOperationOptionsDIY.Items.FindByText(value).Selected = true;
            }
        }

        public bool DIWMPhoneAccess
        {
            get
            {
                return chkPhoneAccessDIWM.Checked;
            }
            set
            {
                chkPhoneAccessDIWM.Checked = value;
            }
        }
        public bool DIWMOnlineAccess
        {
            get
            {
                return chkOnlineAccessDIWM.Checked;
            }
            set
            {
                chkOnlineAccessDIWM.Checked = value;
            }
        }
        public bool DIWMDebitCard
        {
            get
            {
                return chkDebitCardDIWM.Checked;
            }
            set
            {
                chkDebitCardDIWM.Checked = value;
            }
        }
        public bool DIWMChequeBook
        {
            get
            {
                return chkCequeBook25DIWM.Checked;
            }
            set
            {
                chkCequeBook25DIWM.Checked = value;
            }
        }
        public bool DIWMDepositBook
        {
            get
            {
                return chkDepositBookDIWM.Checked;
            }
            set
            {
                chkDepositBookDIWM.Checked = value;
            }
        }

        public string DIWMMannerOfOperation
        {
            get
            {
                if (rbMannerOfOperationOptionsDIWM.SelectedItem != null)
                    return rbMannerOfOperationOptionsDIWM.SelectedItem.Value;

                return string.Empty;
            }
            set
            {
                if (value != string.Empty)
                    rbMannerOfOperationOptionsDIWM.Items.FindByText(value).Selected = true;
            }
        }

        #endregion Securities

        #region Chess

        public bool TransferListedSecuritiesToDesktopBroker
        {
            get
            {
                if (rbTransferSecToDescktopBroker.SelectedItem != null)
                {
                    if (rbTransferSecToDescktopBroker.Text == "Yes")
                        return true;
                    else
                        return false;
                }
                return false;
            }
            set
            {
                if (value)
                    rbTransferSecToDescktopBroker.Items.FindByText("Yes").Selected = true;
                else
                    rbTransferSecToDescktopBroker.Items.FindByText("No").Selected = true;
            }
        }
        public string ChessDIYDIWM
        {
            get
            {
                if (RBChessDIYDIWMOption.SelectedItem != null)
                    return RBChessDIYDIWMOption.SelectedItem.Value;

                return string.Empty;
            }
            set
            {
                if (value != string.Empty)
                    RBChessDIYDIWMOption.Items.FindByText(value).Selected = true;
            }
        }
        public string ChessRegisteredAccountName
        {
            get
            { return txtRegisteredAccountNameChess.Text; }
            set
            { txtRegisteredAccountNameChess.Text = value; }
        }

        public string ChessAccountDesignation
        {
            get
            {
                string ad1 = !string.IsNullOrWhiteSpace(txt_ad1.Value) ? txt_ad1.Value : " ";
                string ad2 = !string.IsNullOrWhiteSpace(txt_ad2.Value) ? txt_ad2.Value : " ";
                string ad3 = !string.IsNullOrWhiteSpace(txt_ad3.Value) ? txt_ad3.Value : " ";
                string ad4 = !string.IsNullOrWhiteSpace(txt_ad4.Value) ? txt_ad4.Value : " ";
                string ad5 = !string.IsNullOrWhiteSpace(txt_ad5.Value) ? txt_ad5.Value : " ";
                string ad6 = !string.IsNullOrWhiteSpace(txt_ad6.Value) ? txt_ad6.Value : " ";
                string ad7 = !string.IsNullOrWhiteSpace(txt_ad7.Value) ? txt_ad7.Value : " ";
                string ad8 = !string.IsNullOrWhiteSpace(txt_ad8.Value) ? txt_ad8.Value : " ";
                string ad9 = !string.IsNullOrWhiteSpace(txt_ad9.Value) ? txt_ad9.Value : " ";
                string ad10 = !string.IsNullOrWhiteSpace(txt_ad10.Value) ? txt_ad10.Value : " ";
                string ad11 = !string.IsNullOrWhiteSpace(txt_ad11.Value) ? txt_ad11.Value : " ";
                string ad12 = !string.IsNullOrWhiteSpace(txt_ad12.Value) ? txt_ad12.Value : " ";
                string ad13 = !string.IsNullOrWhiteSpace(txt_ad13.Value) ? txt_ad13.Value : " ";
                string ad14 = !string.IsNullOrWhiteSpace(txt_ad14.Value) ? txt_ad14.Value : " ";
                string ad15 = !string.IsNullOrWhiteSpace(txt_ad15.Value) ? txt_ad15.Value : " ";
                string ad16 = !string.IsNullOrWhiteSpace(txt_ad16.Value) ? txt_ad16.Value : " ";
                string ad17 = !string.IsNullOrWhiteSpace(txt_ad17.Value) ? txt_ad17.Value : " ";
                string ad18 = !string.IsNullOrWhiteSpace(txt_ad18.Value) ? txt_ad18.Value : " ";
                string ad19 = !string.IsNullOrWhiteSpace(txt_ad19.Value) ? txt_ad19.Value : " ";
                string ad20 = !string.IsNullOrWhiteSpace(txt_ad20.Value) ? txt_ad20.Value : " ";
                string ad21 = !string.IsNullOrWhiteSpace(txt_ad21.Value) ? txt_ad21.Value : " ";
                string ad22 = !string.IsNullOrWhiteSpace(txt_ad22.Value) ? txt_ad22.Value : " ";
                string ad23 = !string.IsNullOrWhiteSpace(txt_ad23.Value) ? txt_ad23.Value : " ";
                string ad24 = !string.IsNullOrWhiteSpace(txt_ad24.Value) ? txt_ad24.Value : " ";
                string ad25 = !string.IsNullOrWhiteSpace(txt_ad25.Value) ? txt_ad25.Value : " ";

                string ad26 = !string.IsNullOrWhiteSpace(txt_ad21.Value) ? txt_ad26.Value : " ";
                string ad27 = !string.IsNullOrWhiteSpace(txt_ad22.Value) ? txt_ad27.Value : " ";
                string ad28 = !string.IsNullOrWhiteSpace(txt_ad23.Value) ? txt_ad28.Value : " ";
                string ad29 = !string.IsNullOrWhiteSpace(txt_ad24.Value) ? txt_ad29.Value : " ";
                string ad30 = !string.IsNullOrWhiteSpace(txt_ad25.Value) ? txt_ad30.Value : " ";



                string designation = ad1 + ad2 + ad3 + ad4 + ad5 + ad6 + ad7 + ad8 + ad9 + ad10 + ad11 + ad12 + ad13 + ad14 + ad15 + ad16 + ad17 + ad18 + ad19 + ad20
                    + ad21 + ad22 + ad23 + ad24 + ad25 + ad26 + ad27 + ad28 + ad29 + ad30;
                return designation;
            }
            set
            {
                for (int i = 0; i < value.Length; i++)
                {
                    switch (i)
                    {
                        case 0:
                            txt_ad1.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 1:
                            txt_ad2.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 2:
                            txt_ad3.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 3:
                            txt_ad4.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 4:
                            txt_ad5.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 5:
                            txt_ad6.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 6:
                            txt_ad7.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 7:
                            txt_ad8.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 8:
                            txt_ad9.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 9:
                            txt_ad10.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 10:
                            txt_ad11.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 11:
                            txt_ad12.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 12:
                            txt_ad13.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 13:
                            txt_ad14.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 14:
                            txt_ad15.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 15:
                            txt_ad16.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 16:
                            txt_ad17.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 17:
                            txt_ad18.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 18:
                            txt_ad19.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 19:
                            txt_ad20.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 20:
                            txt_ad21.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 21:
                            txt_ad22.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 22:
                            txt_ad23.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 23:
                            txt_ad24.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                        case 24:
                            txt_ad25.Value = !string.IsNullOrWhiteSpace(value.Substring(i, 1)) ? value.Substring(i, 1) : "";
                            break;
                    }

                }
            }
        }
        public string ChessAddressLine1
        {
            get
            { return txtAddressLine1Chess.Text; }
            set
            { txtAddressLine1Chess.Text = value; }
        }
        public string ChessAddressLine2
        {
            get
            { return txtAddressLine2Chess.Text; }
            set
            { txtAddressLine2Chess.Text = value; }
        }
        public string ChessSuburb
        {
            get
            { return txtSuburbChess.Text; }
            set
            { txtSuburbChess.Text = value; }
        }
        public string ChessState
        {
            get
            {
                if (C1ComboBoxStateChess.SelectedItem != null)
                    return C1ComboBoxStateChess.SelectedItem.Text;
                else
                    return C1ComboBoxStateChess.Text;
            }
            set
            {
                C1ComboBoxStateChess.Text = value;
            }
        }
        public string ChessPostCode
        {
            get
            { return txtPostcodeChess.Text; }
            set
            { txtPostcodeChess.Text = value; }
        }
        public string ChessCountry
        {
            get
            {
                if (C1ComboBoxCountryListChess.SelectedItem != null)
                    return C1ComboBoxCountryListChess.SelectedItem.Text;
                else
                    return C1ComboBoxCountryListChess.Text;
            }
            set
            {
                C1ComboBoxCountryListChess.Text = value;
            }
        }

        public bool IssuedSponsor
        {
            get
            {
                return chkIssueSponsorChess.Checked;
            }
            set
            {
                chkIssueSponsorChess.Checked = value;
            }
        }
        public bool ExactNameMatch
        {
            get
            {
                return chkExactNameAddressMatchAbove.Checked;
            }
            set
            {
                chkExactNameAddressMatchAbove.Checked = value;
            }
        }
        public string ExactNameMatchAmount
        {
            get
            { return txtHowManyHoldingCompanyChess.Text; }
            set
            { txtHowManyHoldingCompanyChess.Text = value; }
        }
        public bool ExactNameNoMatch
        {
            get
            {
                return chkExactNameAddressNOTMatchAbove.Checked;
            }
            set
            {
                chkExactNameAddressNOTMatchAbove.Checked = value;
            }
        }
        public string ExactNameNoMatchAmount
        {
            get
            { return txtHowManyHoldingCompanyChessNotMatch.Text; }
            set
            { txtHowManyHoldingCompanyChessNotMatch.Text = value; }
        }
        public bool BrokerSponsored
        {
            get
            {
                return chkBrokerSponsoredRegistration.Checked;
            }
            set
            {
                chkBrokerSponsoredRegistration.Checked = value;
            }
        }
        public string ExistingBroker1
        {
            get
            { return txtNameOfSponsorBroker1.Text; }
            set
            { txtNameOfSponsorBroker1.Text = value; }
        }
        public string ClientNo1
        {
            get
            { return txtBrokerID1.Text; }
            set
            { txtBrokerID1.Text = value; }
        }
        public string ClientHin1
        {
            get
            { return txtClientHin1.Text; }
            set
            { txtClientHin1.Text = value; }
        }

        public string ExistingBroker2
        {
            get
            { return txtNameOfSponsorBroker2.Text; }
            set
            { txtNameOfSponsorBroker2.Text = value; }
        }
        public string ClientNo2
        {
            get
            { return txtBrokerID2.Text; }
            set
            { txtBrokerID2.Text = value; }
        }
        public string ClientHin2
        {
            get
            { return txtClientHin2.Text; }
            set
            { txtClientHin2.Text = value; }
        }

        #endregion

        #region CheckList

        public bool ProofIDCheckListItem
        {
            get
            {
                return chkProofofIDCheckListItem.Checked;
            }
            set
            {
                chkProofofIDCheckListItem.Checked = value;
            }
        }
        public bool TrustDeedChecListItem
        {
            get
            {
                return chkTrustDeedCheckListItem.Checked;
            }
            set
            {
                chkTrustDeedCheckListItem.Checked = value;
            }
        }
        public bool LetterOfAccountantChecListItem
        {
            get
            {
                return chkLetterFromYourAccountantCheckListItem.Checked;
            }
            set
            {
                chkLetterFromYourAccountantCheckListItem.Checked = value;
            }
        }
        public bool BusinessNameRegoChecListItem
        {
            get
            {
                return chkBusinessNameCheckListItem.Checked;
            }
            set
            {
                chkBusinessNameCheckListItem.Checked = value;
            }
        }
        public bool IssueSponsorChessSponsorChecListItem
        {
            get
            {
                return chkIssueSponsorHoldingsToChessCheckListItem.Checked;
            }
            set
            {
                chkIssueSponsorHoldingsToChessCheckListItem.Checked = value;
            }
        }
        public bool IssueSponsorChessSponsorHoldsingStatements
        {
            get
            {
                return chkIssuerSponsoredStatementsCheckListItem.Checked;
            }
            set
            {
                chkIssuerSponsoredStatementsCheckListItem.Checked = value;
            }
        }
        public bool OffMarketTransferChecListItem
        {
            get
            {
                return chkOffMarketTransferCheckListItem.Checked;
            }
            set
            {
                chkOffMarketTransferCheckListItem.Checked = value;
            }
        }
        public bool ChangeOFClientChecListItem
        {
            get
            {
                return chkChangeOfClientDetailsCheckListItem.Checked;
            }
            set
            {
                chkChangeOfClientDetailsCheckListItem.Checked = value;
            }
        }
        public bool BrokerToBrokerChecListItem
        {
            get
            {
                return chkBrokerToBrokerTransferClientCheckList.Checked;
            }
            set
            {
                chkBrokerToBrokerTransferClientCheckList.Checked = value;
            }
        }
        public bool ProofOfAddressChecListItem
        {
            get
            {
                return chkProofOfAddressPassportProvidedClientCheckListItem.Checked;
            }
            set
            {
                chkProofOfAddressPassportProvidedClientCheckListItem.Checked = value;
            }
        }

        #endregion

        protected void PopulateUMAForm(UMAFormEntity umaFormEntity)
        {
            #region Account
            txtUserIDAdviserID.Text = umaFormEntity.userName;
            txtLastModifiedUser.Text = umaFormEntity.modifiedByUser;
            txtCreatedDate.Text = umaFormEntity.CreatedOnDateForm;
            txtLastModifiedDate.Text = umaFormEntity.lastModifiedDateForm;


            AccountType = umaFormEntity.accountType;
            AccountNO = umaFormEntity.accountNO;
            AccountName = umaFormEntity.accountName;
            TrustName = umaFormEntity.trustName;

            IFACID = umaFormEntity.IFACID;


            if (umaFormEntity.UMAFormStatus == UMAFormStatus.Intialised)
                txtFormStatus.Text = "Intialised";
            if (umaFormEntity.UMAFormStatus == UMAFormStatus.UnderReview)
                txtFormStatus.Text = "Under review";
            if (umaFormEntity.UMAFormStatus == UMAFormStatus.Finalised)
                txtFormStatus.Text = "Finalised";



            #endregion

            #region fee

            TiredFee.Checked = umaFormEntity.TierFee;
            OngoingFee.Checked = umaFormEntity.OnGoingFee;

            UpFrontFee = Convert.ToDouble(umaFormEntity.UpFrontFee);
            OngoingFeeAmount = umaFormEntity.OngoingFeeDollar;
            OngoingFeePercentage = umaFormEntity.OngoingFeePercentage;
            TierTo1 = Convert.ToDouble(umaFormEntity.Tier1To);
            TierFrom1 = Convert.ToDouble(umaFormEntity.Tier1From);
            TierPercentage1 = Convert.ToDouble(umaFormEntity.Tier1Percentage);
            TierTo2 = Convert.ToDouble(umaFormEntity.Tier2To);
            TierFrom2 = Convert.ToDouble(umaFormEntity.Tier2From);
            TierPercentage2 = Convert.ToDouble(umaFormEntity.Tier2Percentage);
            TierTo3 = Convert.ToDouble(umaFormEntity.Tier3To);
            TierFrom3 = Convert.ToDouble(umaFormEntity.Tier3From);
            TierPercentage4 = Convert.ToDouble(umaFormEntity.Tier4Percentage);
            TierPercentage3 = Convert.ToDouble(umaFormEntity.Tier3Percentage);
            TierTo4 = Convert.ToDouble(umaFormEntity.Tier4To);
            TierFrom4 = Convert.ToDouble(umaFormEntity.Tier4From);

            TierTo5 = Convert.ToDouble(umaFormEntity.Tier5To);
            TierFrom5 = Convert.ToDouble(umaFormEntity.Tier5From);
            TierPercentage5 = Convert.ToDouble(umaFormEntity.Tier5Percentage);

            #endregion

            #region TFN ABN ABRN

            TFNSig1 = umaFormEntity.TFNSig1;
            TFNSig2 = umaFormEntity.TFNSig2;
            TFNSig3 = umaFormEntity.TFNSig3;
            TFNSig4 = umaFormEntity.TFNSig4;
            TFNTrustSuperfund = umaFormEntity.TFNTrustFund;
            HaveABNForTaxWithHolding = umaFormEntity.AppliedForABN;
            CountryABNForTaxWithHolding = umaFormEntity.NonRedidentCountry;
            txtABN.Text = umaFormEntity.ABNCompany;
            txtACN.Text = umaFormEntity.ACN;
            #endregion

            #region Authority To Act

            AuthorityToActEclispe = umaFormEntity.EclipseOnline;
            AuthorityToActAMM = umaFormEntity.AustralianMoneyMarket;
            AuthorityToActMacBank = umaFormEntity.MacquarieCMA;
            InvestorStatusWholesaleInvestor = umaFormEntity.InvestorStatusWholesaleInvestor;
            InvestorStatusSophisticatedInvestor = umaFormEntity.InvestorStatusSophisticatedInvestor;
            InvestorStatusProfessionalInvestor = umaFormEntity.InvestorStatusProfessionalInvestor;
            InvestorStatusRetailInvestor = umaFormEntity.InvestorStatusRetailInvestor;
            CertificateFromAccountantIsAttached = umaFormEntity.CertificateFromAccountantIsAttached;

            #endregion

            #region Services

            SecuritiesExclusionListEntityList = umaFormEntity.SecuritiesExclusionEntityList;
            StateStreetEntityList = umaFormEntity.StateStreetEntityList;
            DIYAmount = umaFormEntity.DIYAmount;
            DIWMAmount = umaFormEntity.DIWMAmount;
            InvestmentProgram = umaFormEntity.InvestmentProgram;
            InvestmentProgramCode = umaFormEntity.InvestmentProgramCode;
            DIFMAmount = umaFormEntity.DIFMAmount;
            DoItYourSelf = umaFormEntity.DoItYourSelf;
            DIYBWA = umaFormEntity.DIYBWA;
            DIYDesktopBroker = umaFormEntity.DIYDesktopBroker;
            DIYFIIG = umaFormEntity.DIYFIIG;
            DIYAMM = umaFormEntity.DIYAMM;
            DoItWithMe = umaFormEntity.DoItWithMe;
            DIWNMBWA = umaFormEntity.DIWNMBWA;
            DIWNMDesktopBroker = umaFormEntity.DIWNMDesktopBroker;
            DIWNMFIIG = umaFormEntity.DIWNMFIIG;
            DIWNMAMM = umaFormEntity.DIWNMAMM;
            DIWNMSS = umaFormEntity.DIWNMSS;
            DIWMServiceManagedViaMDA = umaFormEntity.DIWMServiceManagedViaMDA;
            DoItForhMe = umaFormEntity.DoItForhMe;
            DIFNMBWA = umaFormEntity.DIFNMBWA;
            DIFNMDesktopBroker = umaFormEntity.DIFNMDesktopBroker;
            DIFNMSS = umaFormEntity.DIFNMSS;
            DIFMNominatePreferences = umaFormEntity.DIFMNominatePreferences;

            MinBalance = umaFormEntity.MinBalance;
            MinBalancePercentage = umaFormEntity.MinBalancePercentage;
            MinTrade = umaFormEntity.MinTrade;
            MinTradePercentage = umaFormEntity.MinTradePercentage;

            AssignedAllocatedValueHoldSell = umaFormEntity.AssignedAllocatedValueHoldSell;
            DistributedAllocatedValueHoldSell = umaFormEntity.DistributedAllocatedValueHoldSell;
            AssignedAllocatedValueExclusion = umaFormEntity.AssignedAllocatedValueExclusion;
            DistributedAllocatedValueExclusion = umaFormEntity.DistributedAllocatedValueExclusion;
            AssignedAllocatedValueMinTradeHolding = umaFormEntity.AssignedAllocatedValueMinTradeHolding;
            DistributedAllocatedValueMinTradeHolding = umaFormEntity.DistributedAllocatedValueMinTradeHolding;

            #endregion

            #region Platforms

            NameOfPlatformsBT = umaFormEntity.NameOfPlatformsBT;
            InvestorNoBT = umaFormEntity.InvestorNoBT;
            AdviserNoBT = umaFormEntity.AdviserNoBT;
            ProviderNoBT = umaFormEntity.ProviderNoBT;

            NameOfPlatformsEclipse = umaFormEntity.NameOfPlatformsEclipse;
            InvestorNoEclipse = umaFormEntity.InvestorNoEclipse;
            AdviserNoEclipse = umaFormEntity.AdviserNoEclipse;
            ProviderNoEclipse = umaFormEntity.ProviderNoEclipse;

            MaqCMALinkBWA = umaFormEntity.MaqCMALinkBWA;
            MaqCMADIYDIWM = umaFormEntity.MaqCMADIYDIWM;
            MaqCMABSB = umaFormEntity.MaqCMABSB;
            MaqCMAAccountNo = umaFormEntity.MaqCMAAccountNo;
            MaqCMAAccountName = umaFormEntity.MaqCMAAccountName;

            #endregion

            #region BWA Option

            DIYPhoneAccess = umaFormEntity.DIYPhoneAccess;
            DIYOnlineAccess = umaFormEntity.DIYOnlineAccess;
            DIYDebitCard = umaFormEntity.DIYDebitCard;
            DIYChequeBook = umaFormEntity.DIYChequeBook;
            DIYDepositBook = umaFormEntity.DIYDepositBook;
            DIYMannerOfOperation = umaFormEntity.DIYMannerOfOperation;
            DIWMPhoneAccess = umaFormEntity.DIWMPhoneAccess;
            DIWMOnlineAccess = umaFormEntity.DIWMOnlineAccess;
            DIWMDebitCard = umaFormEntity.DIWMDebitCard;
            DIWMChequeBook = umaFormEntity.DIWMChequeBook;
            DIWMDepositBook = umaFormEntity.DIWMDepositBook;
            DIWMMannerOfOperation = umaFormEntity.DIWMMannerOfOperation;

            #endregion

            #region Chess

            TransferListedSecuritiesToDesktopBroker = umaFormEntity.TransferListedSecuritiesToDesktopBroker;
            ChessDIYDIWM = umaFormEntity.ChessDIYDIWM;
            ChessRegisteredAccountName = umaFormEntity.ChessRegisteredAccountName;
            ChessAccountDesignation = umaFormEntity.ChessAccountDesignation;
            ChessAddressLine1 = umaFormEntity.ChessAddressLine1;
            ChessAddressLine2 = umaFormEntity.ChessAddressLine2;
            ChessSuburb = umaFormEntity.ChessSuburb;
            ChessState = umaFormEntity.ChessState;
            ChessPostCode = umaFormEntity.ChessPostCode;
            ChessCountry = umaFormEntity.ChessCountry;
            IssuedSponsor = umaFormEntity.IssuedSponsor;
            ExactNameMatch = umaFormEntity.ExactNameMatch;
            ExactNameMatchAmount = umaFormEntity.ExactNameMatchAmount;
            ExactNameNoMatch = umaFormEntity.ExactNameNoMatch;
            ExactNameNoMatchAmount = umaFormEntity.ExactNameNoMatchAmount;
            BrokerSponsored = umaFormEntity.BrokerSponsored;
            ExistingBroker1 = umaFormEntity.ExistingBroker1;
            ClientNo1 = umaFormEntity.ClientNo1;
            ClientHin1 = umaFormEntity.ClientHin1;
            ExistingBroker2 = umaFormEntity.ExistingBroker2;
            ClientNo2 = umaFormEntity.ClientNo2;
            ClientHin2 = umaFormEntity.ClientHin2;

            #endregion

            #region CheckList

            ProofIDCheckListItem = umaFormEntity.ProofIDCheckListItem;
            TrustDeedChecListItem = umaFormEntity.TrustDeedChecListItem;
            LetterOfAccountantChecListItem = umaFormEntity.LetterOfAccountantChecListItem;
            BusinessNameRegoChecListItem = umaFormEntity.BusinessNameRegoChecListItem;
            IssueSponsorChessSponsorChecListItem = umaFormEntity.IssueSponsorChessSponsorChecListItem;
            IssueSponsorChessSponsorHoldsingStatements = umaFormEntity.IssueSponsorChessSponsorHoldsingStatements;
            OffMarketTransferChecListItem = umaFormEntity.OffMarketTransferChecListItem;
            ChangeOFClientChecListItem = umaFormEntity.ChangeOFClientChecListItem;
            BrokerToBrokerChecListItem = umaFormEntity.BrokerToBrokerChecListItem;
            ProofOfAddressChecListItem = umaFormEntity.ProofOfAddressChecListItem;

            #endregion

            #region Signatories

            Sig2Required.Checked = umaFormEntity.Sig2Required;
            Sig3Required.Checked = umaFormEntity.Sig3Required;
            Sig4Required.Checked = umaFormEntity.Sig4Required;

            SignatoryEntity signatoryEntity1 = umaFormEntity.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault();
            SignatoryEntity signatoryEntity2 = umaFormEntity.SignatoryEntityList.Where(sig => sig.SigNo == 2).FirstOrDefault();
            SignatoryEntity signatoryEntity3 = umaFormEntity.SignatoryEntityList.Where(sig => sig.SigNo == 3).FirstOrDefault();
            SignatoryEntity signatoryEntity4 = umaFormEntity.SignatoryEntityList.Where(sig => sig.SigNo == 4).FirstOrDefault();

            #region Signatory 1

            SignatoriesDetails1.Title = signatoryEntity1.Title;

            SignatoriesDetails1.FamilyName = signatoryEntity1.FamilyName;
            SignatoriesDetails1.GivenName = signatoryEntity1.GivenName;
            SignatoriesDetails1.MiddleName = signatoryEntity1.MiddleName;
            SignatoriesDetails1.OtherName = signatoryEntity1.OtherName;
            SignatoriesDetails1.DateOfBirth = signatoryEntity1.DateOfBirth;
            SignatoriesDetails1.CorporateTite = signatoryEntity1.CorporateTitel;

            SignatoriesDetails1.ResiAddressLine1 = signatoryEntity1.ResiAddressLine1;
            SignatoriesDetails1.ResiAddressLine2 = signatoryEntity1.ResiAddressLine2;
            SignatoriesDetails1.ResiSuburb = signatoryEntity1.ResiSuburb;
            SignatoriesDetails1.ResiState = signatoryEntity1.ResiState;
            SignatoriesDetails1.ResiPostCode = signatoryEntity1.ResiPostCode;
            SignatoriesDetails1.ResiCountry = signatoryEntity1.ResiCountry;

            SignatoriesDetails1.MailAddressLine1 = signatoryEntity1.MailAddressLine1;
            SignatoriesDetails1.MailAddressLine2 = signatoryEntity1.MailAddressLine2;
            SignatoriesDetails1.MailSuburb = signatoryEntity1.MailSuburb;
            SignatoriesDetails1.MailState = signatoryEntity1.MailState;
            SignatoriesDetails1.MailPostCode = signatoryEntity1.MailPostCode;
            SignatoriesDetails1.MailCountry = signatoryEntity1.MailCountry;

            SignatoriesDetails1.Occupation = signatoryEntity1.Occupation;
            SignatoriesDetails1.Employer = signatoryEntity1.Employer;
            SignatoriesDetails1.MainCountryOFAddressSig = signatoryEntity1.MainCountryOFAddressSig;
            SignatoriesDetails1.MobilePH = signatoryEntity1.MobilePH;
            SignatoriesDetails1.AlternativePH = signatoryEntity1.AlternativePH;
            SignatoriesDetails1.ContactPH = signatoryEntity1.ContactPH;

            SignatoriesDetails1.Fax = signatoryEntity1.Fax;
            SignatoriesDetails1.Email = signatoryEntity1.Email;

            SignatoriesDetails1.DriverLicSig = signatoryEntity1.DriverLicSig;
            SignatoriesDetails1.PassportSig = signatoryEntity1.PassportSig;
            SignatoriesDetails1.DocumentNoLic = signatoryEntity1.DocumentNoLic;
            SignatoriesDetails1.DocumentNoPassport = signatoryEntity1.DocumentNoPassport;
            SignatoriesDetails1.LicDocumentIssuer = signatoryEntity1.LicDocumentIssuer;
            SignatoriesDetails1.LicCertifiedCopyAttached = signatoryEntity1.LicCertifiedCopyAttached;
            SignatoriesDetails1.LicIssueDate = signatoryEntity1.LicIssueDate;
            SignatoriesDetails1.LicExpiryDate = signatoryEntity1.LicExpiryDate;

            SignatoriesDetails1.PassportDocumentIssuer = signatoryEntity1.PassportDocumentIssuer;
            SignatoriesDetails1.PassportCertifiedCopyAttached = signatoryEntity1.PassportCertifiedCopyAttached;
            SignatoriesDetails1.PassportIssueDate = signatoryEntity1.PassportIssueDate;
            SignatoriesDetails1.PassportExpiryDate = signatoryEntity1.PassportExpiryDate;

            #endregion

            #region Signatory 2

            SignatoriesDetails2.Title = signatoryEntity2.Title;

            SignatoriesDetails2.FamilyName = signatoryEntity2.FamilyName;
            SignatoriesDetails2.GivenName = signatoryEntity2.GivenName;
            SignatoriesDetails2.MiddleName = signatoryEntity2.MiddleName;
            SignatoriesDetails2.OtherName = signatoryEntity2.OtherName;
            SignatoriesDetails2.DateOfBirth = signatoryEntity2.DateOfBirth;
            SignatoriesDetails2.CorporateTite = signatoryEntity2.CorporateTitel;

            SignatoriesDetails2.ResiAddressLine1 = signatoryEntity2.ResiAddressLine1;
            SignatoriesDetails2.ResiAddressLine2 = signatoryEntity2.ResiAddressLine2;
            SignatoriesDetails2.ResiSuburb = signatoryEntity2.ResiSuburb;
            SignatoriesDetails2.ResiState = signatoryEntity2.ResiState;
            SignatoriesDetails2.ResiPostCode = signatoryEntity2.ResiPostCode;
            SignatoriesDetails2.ResiCountry = signatoryEntity2.ResiCountry;

            SignatoriesDetails2.MailAddressLine1 = signatoryEntity2.MailAddressLine1;
            SignatoriesDetails2.MailAddressLine2 = signatoryEntity2.MailAddressLine2;
            SignatoriesDetails2.MailSuburb = signatoryEntity2.MailSuburb;
            SignatoriesDetails2.MailState = signatoryEntity2.MailState;
            SignatoriesDetails2.MailPostCode = signatoryEntity2.MailPostCode;
            SignatoriesDetails2.MailCountry = signatoryEntity2.MailCountry;

            SignatoriesDetails2.Occupation = signatoryEntity2.Occupation;
            SignatoriesDetails2.Employer = signatoryEntity2.Employer;
            SignatoriesDetails2.MainCountryOFAddressSig = signatoryEntity2.MainCountryOFAddressSig;
            SignatoriesDetails2.ContactPH = signatoryEntity2.ContactPH;
            SignatoriesDetails2.MobilePH = signatoryEntity2.MobilePH;
            SignatoriesDetails2.AlternativePH = signatoryEntity2.AlternativePH;
            SignatoriesDetails2.Fax = signatoryEntity2.Fax;
            SignatoriesDetails2.Email = signatoryEntity2.Email;

            SignatoriesDetails2.DocumentNoLic = signatoryEntity2.DocumentNoLic;
            SignatoriesDetails2.DocumentNoPassport = signatoryEntity2.DocumentNoPassport;
            SignatoriesDetails2.DriverLicSig = signatoryEntity2.DriverLicSig;
            SignatoriesDetails2.PassportSig = signatoryEntity2.PassportSig;
            SignatoriesDetails2.LicDocumentIssuer = signatoryEntity2.LicDocumentIssuer;
            SignatoriesDetails2.LicCertifiedCopyAttached = signatoryEntity2.LicCertifiedCopyAttached;
            SignatoriesDetails2.LicIssueDate = signatoryEntity2.LicIssueDate;
            SignatoriesDetails2.LicExpiryDate = signatoryEntity2.LicExpiryDate;

            SignatoriesDetails2.PassportDocumentIssuer = signatoryEntity2.PassportDocumentIssuer;
            SignatoriesDetails2.PassportCertifiedCopyAttached = signatoryEntity2.PassportCertifiedCopyAttached;
            SignatoriesDetails2.PassportIssueDate = signatoryEntity2.PassportIssueDate;
            SignatoriesDetails2.PassportExpiryDate = signatoryEntity2.PassportExpiryDate;

            #endregion

            #region Signatory 3

            SignatoriesDetails3.Title = signatoryEntity3.Title;

            SignatoriesDetails3.FamilyName = signatoryEntity3.FamilyName;
            SignatoriesDetails3.GivenName = signatoryEntity3.GivenName;
            SignatoriesDetails3.MiddleName = signatoryEntity3.MiddleName;
            SignatoriesDetails3.OtherName = signatoryEntity3.OtherName;
            SignatoriesDetails3.DateOfBirth = signatoryEntity3.DateOfBirth;
            SignatoriesDetails3.CorporateTite = signatoryEntity3.CorporateTitel;

            SignatoriesDetails3.ResiAddressLine1 = signatoryEntity3.ResiAddressLine1;
            SignatoriesDetails3.ResiAddressLine2 = signatoryEntity3.ResiAddressLine2;
            SignatoriesDetails3.ResiSuburb = signatoryEntity3.ResiSuburb;
            SignatoriesDetails3.ResiState = signatoryEntity3.ResiState;
            SignatoriesDetails3.ResiPostCode = signatoryEntity3.ResiPostCode;
            SignatoriesDetails3.ResiCountry = signatoryEntity3.ResiCountry;

            SignatoriesDetails3.MailAddressLine1 = signatoryEntity3.MailAddressLine1;
            SignatoriesDetails3.MailAddressLine2 = signatoryEntity3.MailAddressLine2;
            SignatoriesDetails3.MailSuburb = signatoryEntity3.MailSuburb;
            SignatoriesDetails3.MailState = signatoryEntity3.MailState;
            SignatoriesDetails3.MailPostCode = signatoryEntity3.MailPostCode;
            SignatoriesDetails3.MailCountry = signatoryEntity3.MailCountry;

            SignatoriesDetails3.Occupation = signatoryEntity3.Occupation;
            SignatoriesDetails3.Employer = signatoryEntity3.Employer;
            SignatoriesDetails3.MainCountryOFAddressSig = signatoryEntity3.MainCountryOFAddressSig;
            SignatoriesDetails3.ContactPH = signatoryEntity3.ContactPH;
            SignatoriesDetails3.AlternativePH = signatoryEntity3.AlternativePH;
            SignatoriesDetails3.MobilePH = signatoryEntity3.MobilePH;
            SignatoriesDetails3.Fax = signatoryEntity3.Fax;
            SignatoriesDetails3.Email = signatoryEntity3.Email;

            SignatoriesDetails3.DocumentNoLic = signatoryEntity3.DocumentNoLic;
            SignatoriesDetails3.DocumentNoPassport = signatoryEntity3.DocumentNoPassport;
            SignatoriesDetails3.DriverLicSig = signatoryEntity3.DriverLicSig;
            SignatoriesDetails3.PassportSig = signatoryEntity3.PassportSig;
            SignatoriesDetails3.LicDocumentIssuer = signatoryEntity3.LicDocumentIssuer;
            SignatoriesDetails3.LicCertifiedCopyAttached = signatoryEntity3.LicCertifiedCopyAttached;
            SignatoriesDetails3.LicIssueDate = signatoryEntity3.LicIssueDate;
            SignatoriesDetails3.LicExpiryDate = signatoryEntity3.LicExpiryDate;

            SignatoriesDetails3.PassportDocumentIssuer = signatoryEntity3.PassportDocumentIssuer;
            SignatoriesDetails3.PassportCertifiedCopyAttached = signatoryEntity3.PassportCertifiedCopyAttached;
            SignatoriesDetails3.PassportIssueDate = signatoryEntity3.PassportIssueDate;
            SignatoriesDetails3.PassportExpiryDate = signatoryEntity3.PassportExpiryDate;

            #endregion

            #region Signatory 4

            SignatoriesDetails4.Title = signatoryEntity4.Title;

            SignatoriesDetails4.FamilyName = signatoryEntity4.FamilyName;
            SignatoriesDetails4.GivenName = signatoryEntity4.GivenName;
            SignatoriesDetails4.MiddleName = signatoryEntity4.MiddleName;
            SignatoriesDetails4.OtherName = signatoryEntity4.OtherName;
            SignatoriesDetails4.DateOfBirth = signatoryEntity4.DateOfBirth;
            SignatoriesDetails4.CorporateTite = signatoryEntity4.CorporateTitel;

            SignatoriesDetails4.ResiAddressLine1 = signatoryEntity4.ResiAddressLine1;
            SignatoriesDetails4.ResiAddressLine2 = signatoryEntity4.ResiAddressLine2;
            SignatoriesDetails4.ResiSuburb = signatoryEntity4.ResiSuburb;
            SignatoriesDetails4.ResiState = signatoryEntity4.ResiState;
            SignatoriesDetails4.ResiPostCode = signatoryEntity4.ResiPostCode;
            SignatoriesDetails4.ResiCountry = signatoryEntity4.ResiCountry;

            SignatoriesDetails4.MailAddressLine1 = signatoryEntity4.MailAddressLine1;
            SignatoriesDetails4.MailAddressLine2 = signatoryEntity4.MailAddressLine2;
            SignatoriesDetails4.MailSuburb = signatoryEntity4.MailSuburb;
            SignatoriesDetails4.MailState = signatoryEntity4.MailState;
            SignatoriesDetails4.MailPostCode = signatoryEntity4.MailPostCode;
            SignatoriesDetails4.MailCountry = signatoryEntity4.MailCountry;

            SignatoriesDetails4.Occupation = signatoryEntity4.Occupation;
            SignatoriesDetails4.Employer = signatoryEntity4.Employer;
            SignatoriesDetails4.MainCountryOFAddressSig = signatoryEntity4.MainCountryOFAddressSig;
            SignatoriesDetails4.ContactPH = signatoryEntity4.ContactPH;
            SignatoriesDetails4.AlternativePH = signatoryEntity4.AlternativePH;
            SignatoriesDetails4.Fax = signatoryEntity4.Fax;
            SignatoriesDetails4.Email = signatoryEntity4.Email;
            SignatoriesDetails4.MobilePH = signatoryEntity4.MobilePH;

            SignatoriesDetails4.DocumentNoLic = signatoryEntity4.DocumentNoLic;
            SignatoriesDetails4.DocumentNoPassport = signatoryEntity4.DocumentNoPassport;
            SignatoriesDetails4.DriverLicSig = signatoryEntity4.DriverLicSig;
            SignatoriesDetails4.PassportSig = signatoryEntity4.PassportSig;
            SignatoriesDetails4.LicDocumentIssuer = signatoryEntity4.LicDocumentIssuer;
            SignatoriesDetails4.LicCertifiedCopyAttached = signatoryEntity4.LicCertifiedCopyAttached;
            SignatoriesDetails4.LicIssueDate = signatoryEntity4.LicIssueDate;
            SignatoriesDetails4.LicExpiryDate = signatoryEntity4.LicExpiryDate;

            SignatoriesDetails4.PassportDocumentIssuer = signatoryEntity4.PassportDocumentIssuer;
            SignatoriesDetails4.PassportCertifiedCopyAttached = signatoryEntity4.PassportCertifiedCopyAttached;
            SignatoriesDetails4.PassportIssueDate = signatoryEntity4.PassportIssueDate;
            SignatoriesDetails4.PassportExpiryDate = signatoryEntity4.PassportExpiryDate;

            #endregion

            #endregion

            #region Addresses

            AddressDetails.MialingAddressLine1 = umaFormEntity.MialingAddressLine1;
            AddressDetails.MialingAddressLine2 = umaFormEntity.MialingAddressLine2;
            AddressDetails.MialingSuburb = umaFormEntity.MialingSuburb;
            AddressDetails.MialingState = umaFormEntity.MialingState;
            AddressDetails.MialingPostCode = umaFormEntity.MialingPostCode;
            AddressDetails.MialingCountry = umaFormEntity.MialingCountry;

            AddressDetails.DupMialingAddressLine1 = umaFormEntity.DupMialingAddressLine1;
            AddressDetails.DupMialingAddressLine2 = umaFormEntity.DupMialingAddressLine2;
            AddressDetails.DupMialingSuburb = umaFormEntity.DupMialingSuburb;
            AddressDetails.DupMialingState = umaFormEntity.DupMialingState;
            AddressDetails.DupMialingPostCode = umaFormEntity.DupMialingPostCode;
            AddressDetails.DupMialingCountry = umaFormEntity.DupMialingCountry;

            AddressDetails.RegAddressLine1 = umaFormEntity.RegAddressLine1;
            AddressDetails.RegAddressLine2 = umaFormEntity.RegAddressLine2;
            AddressDetails.RegSuburb = umaFormEntity.RegSuburb;
            AddressDetails.RegState = umaFormEntity.RegState;
            AddressDetails.RegPostCode = umaFormEntity.RegPostCode;
            AddressDetails.RegCountry = umaFormEntity.RegCountry;

            AddressDetails.PrincipalAddressLine1 = umaFormEntity.PrincipalAddressLine1;
            AddressDetails.PrincipalAddressLine2 = umaFormEntity.PrincipalAddressLine2;
            AddressDetails.PrincipalSuburb = umaFormEntity.PrincipalSuburb;
            AddressDetails.PrincipalState = umaFormEntity.PrincipalState;
            AddressDetails.PrincipalPostCode = umaFormEntity.PrincipalPostCode;
            AddressDetails.PrincipalCountry = umaFormEntity.PrincipalCountry;

            #endregion
            SecuritiesExclusionListEntityList = umaFormEntity.SecuritiesExclusionEntityList;
            StateStreetEntityList = umaFormEntity.StateStreetEntityList;

            ExcusionList.DataSource = SecurityExclusionTable();
            ExcusionList.DataBind();

            StateStreetList.DataSource = StateStreetPreferenceTable();
            StateStreetList.DataBind();

            UsersWorkFlow.InculdedUsers = umaFormEntity.IncludedUsers;
        }

        protected void OngoingFee_CheckedChanged(object sender, EventArgs e)
        {
            if (OngoingFee.Checked)
            {
                pnlEnableOngoing.Visible = true;
                pnlEnableTiered.Visible = false;
            }
            else if (TiredFee.Checked)
            {
                pnlEnableOngoing.Visible = false;
                pnlEnableTiered.Visible = true;
            }
        }

        public void CreatePDFDocument(string strHtml)
        {
            string strFileName = HttpContext.Current.Server.MapPath("test.pdf");
            // step 1: creation of a document-object
            iTextSharp.text.Document document = new iTextSharp.text.Document();
            // step 2:
            // we create a writer that listens to the document
            PdfWriter.GetInstance(document, new FileStream(strFileName, FileMode.Create));
            StringReader se = new StringReader(strHtml);
            HTMLWorker obj = new HTMLWorker(document);
            document.Open();
            obj.Parse(se);
            document.Close();
            ShowPdf(strFileName);
        }

        public void ShowPdf(string strFileName)
        {
            Response.ClearContent();
            Response.ClearHeaders();
            Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName);
            Response.ContentType = "application/pdf";
            Response.WriteFile(strFileName);
            Response.Flush();
            Response.Clear();
        }
        private static void SetLimitedPowerOfAttorney(UMAFormEntity form, Aspose.Words.Document document)
        {
            if (!form.EclipseOnline && !form.AustralianMoneyMarket)
            {
                Aspose.Words.Bookmark bookmarkLimitedPA = document.Range.Bookmarks["limitedPA"];
                bookmarkLimitedPA.Text = string.Empty;

                Aspose.Words.Bookmark bookmarkLimitedPAAMM = document.Range.Bookmarks["limitedPAAMM"];
                bookmarkLimitedPAAMM.Text = string.Empty;
            }
            else if (form.EclipseOnline && !form.AustralianMoneyMarket)
            {
                Aspose.Words.Bookmark bookmarkLimitedPAAMM = document.Range.Bookmarks["limitedPAAMM"];
                bookmarkLimitedPAAMM.Text = string.Empty;
            }
            else if (!form.EclipseOnline && form.AustralianMoneyMarket)
            {
                Aspose.Words.Bookmark bookmarkLimitedPA = document.Range.Bookmarks["limitedPA"];
                bookmarkLimitedPA.Text = string.Empty;
            }
        }

        private string GetINvestorStatus()
        {
            string investorStatus = string.Empty;

            if (InvestorStatusWholesaleInvestor)
                return rbWholesaleInvestor.Text;
            else if (InvestorStatusSophisticatedInvestor)
                return rbSophisticatedInvestor.Text;

            else if (InvestorStatusProfessionalInvestor)
                return rbProfessionalInvestor.Text;
            else if (InvestorStatusRetailInvestor)
                return rbRetailInvestor.Text;

            return investorStatus;

        }

        protected void EnableSig2(object sender, EventArgs args)
        {
            EnableDisableSig2();
        }

        private void EnableDisableSig2()
        {
            if (Sig2Required.Checked)
            {
                pnlSig2.Enabled = true;

                SignatoriesDetails2.ResidentialCountry.Enabled = true;
                SignatoriesDetails2.ResidentialState.Enabled = true;

                SignatoriesDetails2.MailingCountry.Enabled = true;
                SignatoriesDetails2.MailingState.Enabled = true;
            }
            else
                pnlSig2.Enabled = false;
        }

        private void EnableDisableSig3()
        {
            if (Sig3Required.Checked)
            {
                pnlSig3.Enabled = true;

                SignatoriesDetails3.ResidentialCountry.Enabled = true;
                SignatoriesDetails3.ResidentialState.Enabled = true;

                SignatoriesDetails3.MailingCountry.Enabled = true;
                SignatoriesDetails3.MailingState.Enabled = true;
            }
            else
                pnlSig3.Enabled = false;
        }

        private void EnableDisableSig4()
        {
            if (Sig4Required.Checked)
            {
                pnlSig4.Enabled = true;

                SignatoriesDetails4.ResidentialCountry.Enabled = true;
                SignatoriesDetails4.ResidentialState.Enabled = true;

                SignatoriesDetails4.MailingCountry.Enabled = true;
                SignatoriesDetails4.MailingState.Enabled = true;
            }
            else
                pnlSig4.Enabled = false;
        }

        protected void EnableSig3(object sender, EventArgs args)
        {
            EnableDisableSig3();
        }

        protected void EnableSig4(object sender, EventArgs args)
        {
            EnableDisableSig4();
        }

        protected void SetDIFMConfiguration(object sender, EventArgs args)
        {
            if (chkDIFMOption.Checked)
            {
                chkDIFMOptionBWA.Checked = true;
                chkDIFMOptionASX.Checked = true;
                chkDIFMOptionSS.Checked = true;
            }
            else
            {
                chkDIFMOptionBWA.Checked = false;
                chkDIFMOptionASX.Checked = false;
                chkDIFMOptionSS.Checked = false;
            }

        }

        private static void SetBaseDetails(UMAFormEntity form, Aspose.Words.Document document, ModelEntity configuredModel)
        {
            document.Range.Replace("[ECLIPSE_CLIENT_NAME]", form.accountName, false, false);
            document.Range.Replace("[ECLIPSE_TRUST_NAME]", form.trustName, false, false);
            document.Range.Replace("[ECLIPSE_ACCTYPE_NAME]", form.accountType, false, false);
            document.Range.Replace("[ECLIPSE_DATE]", DateTime.Today.ToString("dd/MM/yyyy"), false, false);

            document.Range.Replace("[ECLIPSE_ADDR1]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().ResiAddressLine1, false, false);
            document.Range.Replace("[ECLIPSE_ADDR2]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().ResiAddressLine2, false, false);
            document.Range.Replace("[ECLIPSE_STATE]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().ResiState, false, false);
            document.Range.Replace("[ECLIPSE_PC]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().ResiPostCode, false, false);
            document.Range.Replace("[ECLIPSE_SUBURB]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().ResiSuburb, false, false);

            document.Range.Replace("[ECLIPSE_ACCTYPE]", form.accountType, false, false);
            document.Range.Replace("[ECLIPSE_SIG1_FULLNAME]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().GivenName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().MiddleName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().FamilyName, false, false);
            document.Range.Replace("[ECLIPSE_SIG2_FULLNAME]", form.SignatoryEntityList.Where(sig => sig.SigNo == 2).FirstOrDefault().GivenName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 2).FirstOrDefault().MiddleName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 2).FirstOrDefault().FamilyName, false, false);

            if (form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().GivenName != string.Empty)
                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE1]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().CorporateTitel, false, false);
            else
                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE1]", string.Empty, false, false);
            if (form.SignatoryEntityList.Where(sig => sig.SigNo == 2).FirstOrDefault().GivenName != string.Empty)
                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE2]", form.SignatoryEntityList.Where(sig => sig.SigNo == 2).FirstOrDefault().CorporateTitel, false, false);
            else
                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE2]", string.Empty, false, false);

            document.Range.Replace("[ECLIPSE_SIG3_FULLNAME]", form.SignatoryEntityList.Where(sig => sig.SigNo == 3).FirstOrDefault().GivenName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 3).FirstOrDefault().MiddleName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 3).FirstOrDefault().FamilyName, false, false);
            document.Range.Replace("[ECLIPSE_SIG4_FULLNAME]", form.SignatoryEntityList.Where(sig => sig.SigNo == 4).FirstOrDefault().GivenName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 4).FirstOrDefault().MiddleName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 4).FirstOrDefault().FamilyName, false, false);

            if (form.SignatoryEntityList.Where(sig => sig.SigNo == 3).FirstOrDefault().GivenName != string.Empty)
                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE3]", form.SignatoryEntityList.Where(sig => sig.SigNo == 3).FirstOrDefault().CorporateTitel, false, false);
            else
                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE3]", string.Empty, false, false);
            if (form.SignatoryEntityList.Where(sig => sig.SigNo == 4).FirstOrDefault().GivenName != string.Empty)
                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE4]", form.SignatoryEntityList.Where(sig => sig.SigNo == 4).FirstOrDefault().CorporateTitel, false, false);
            else
                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE4]", string.Empty, false, false);

            document.Range.Replace("[INVESTOR_STATUS]", form.GetINvestorStatus(), false, false);
            if (configuredModel != null)
                document.Range.Replace("[PROGRAM_NAME]", configuredModel.Name, false, false);

            decimal minBalance = 0;
            decimal minTrade = 0;
            decimal minBalancePercent = 0;
            decimal minTradePercent = 0;

            if (form.MinBalance != string.Empty)
                minBalance = Convert.ToDecimal(form.MinBalance);

            if (form.MinTrade != string.Empty)
                minTrade = Convert.ToDecimal(form.MinTrade);

            if (form.MinBalancePercentage != string.Empty)
                minBalancePercent = Convert.ToDecimal(form.MinBalancePercentage);

            if (form.MinTradePercentage != string.Empty)
                minTradePercent = Convert.ToDecimal(form.MinTradePercentage);

            if (minBalance > 0)
            {
                document.Range.Replace("[ECLIPSE_BALTYPE]", "", false, false);
                document.Range.Replace("[ECLIPSE_MINBAL]", minBalance.ToString("C"), false, false);
            }
            else if (minTradePercent > 0)
            {
                document.Range.Replace("[ECLIPSE_BALTYPE]", "", false, false);
                document.Range.Replace("[ECLIPSE_MINBAL]", minTradePercent.ToString("P"), false, false);
            }

            if (minTrade > 0)
            {
                document.Range.Replace("[ECLIPSE_TRADETYPE]", "", false, false);
                document.Range.Replace("[ECLIPSE_MINTRADE]", minTrade.ToString("C"), false, false);
            }
            else if (minTradePercent > 0)
            {
                document.Range.Replace("[ECLIPSE_TRADETYPE]", "", false, false);
                document.Range.Replace("[ECLIPSE_MINTRADE]]", minTradePercent.ToString("P"), false, false);
            }


            document.Range.Replace("[ECLIPSE_HANDLE_NOTPUR_HOLD]", form.HandlingDueToHoldOrSell(), false, false);
            document.Range.Replace("[ECLIPSE_HANDLE_NOTPUR_EXCLUSION]", form.HandlingDueToExclusion(), false, false);
            document.Range.Replace("[ECLIPSE_HANDLE_NOTPUR_MINTRADE]", form.HandlingDueToMinTradeOrHoldingConstraint(), false, false);
        }

        private static void SetExclusionList(UMAFormEntity form, Aspose.Words.Document document)
        {
            if (form.SecuritiesExclusionEntityList.Count > 0)
            {
                DocumentBuilder builderExl = new DocumentBuilder(document);
                builderExl.MoveToBookmark("ECLIPSE_EXCL", false, true);
                builderExl.StartTable();

                // Insert a cell
                builderExl.InsertCell();
                builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                builderExl.CellFormat.LeftPadding = 2;
                builderExl.CellFormat.Width = 70;
                builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
                builderExl.Bold = true;
                builderExl.Write("Investment Type");

                builderExl.InsertCell();
                builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                builderExl.CellFormat.LeftPadding = 2;
                builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
                builderExl.Bold = true;
                builderExl.Write("Investment Name / Code");

                builderExl.InsertCell();
                builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                builderExl.CellFormat.LeftPadding = 2;
                builderExl.CellFormat.Orientation = TextOrientation.Horizontal;
                builderExl.Bold = true;
                builderExl.Write("Exclusion Type");

                builderExl.EndRow();

                foreach (SecuritiesExclusionEntity investmentExclusions in form.SecuritiesExclusionEntityList)
                {
                    if (investmentExclusions.Code != null)
                    {
                        // Insert a cell
                        builderExl.InsertCell();
                        builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                        builderExl.Bold = true;
                        builderExl.CellFormat.LeftPadding = 2;
                        builderExl.Write(investmentExclusions.Code);

                        builderExl.InsertCell();
                        builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                        builderExl.Write(investmentExclusions.Description);
                        builderExl.CellFormat.LeftPadding = 2;

                        builderExl.InsertCell();
                        builderExl.CellFormat.LeftPadding = 2;
                        builderExl.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                        builderExl.Write(investmentExclusions.InvestmentAction);

                        builderExl.EndRow();
                    }

                }
            }
        }

        private static void SetAmmSection(UMAFormEntity form, bool isAMM, Aspose.Words.Document document, bool isDIFM)
        {
            if (form.AustralianMoneyMarket || isDIFM)
            {

                document.Range.Replace("[ECLIPSE_LEFTBLACK_AMM]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_CLIENT_NAME_AMM]", form.accountName, false, false);
                document.Range.Replace("[ECLIPSE_TRUST_NAME_AMM]", form.trustName, false, false);
                document.Range.Replace("[ECLIPSE_ACCTYPE_NAME_AMM]", form.accountType, false, false);

                document.Range.Replace("[ECLIPSE_ADDR1_AMM]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().ResiAddressLine1, false, false);
                document.Range.Replace("[ECLIPSE_ADDR2_AMM]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().ResiAddressLine2, false, false);
                document.Range.Replace("[ECLIPSE_STATE_AMM]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().ResiState, false, false);
                document.Range.Replace("[ECLIPSE_PC_AMM]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().ResiPostCode, false, false);
                document.Range.Replace("[ECLIPSE_SUBURB_AMM]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().ResiSuburb, false, false);

                document.Range.Replace("[ECLIPSE_ACCTYPE_AMM]", form.accountType, false, false);
                document.Range.Replace("[ECLIPSE_SIG1_FULLNAME_AMM]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().GivenName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().MiddleName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().FamilyName, false, false);
                document.Range.Replace("[ECLIPSE_SIG2_FULLNAME_AMM]", form.SignatoryEntityList.Where(sig => sig.SigNo == 2).FirstOrDefault().GivenName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 2).FirstOrDefault().MiddleName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 2).FirstOrDefault().FamilyName, false, false);

                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE1_AMM]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().CorporateTitel, false, false);
                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE2_AMM]", form.SignatoryEntityList.Where(sig => sig.SigNo == 2).FirstOrDefault().CorporateTitel, false, false);

                document.Range.Replace("[ECLIPSE_SIG3_FULLNAME_AMM]", form.SignatoryEntityList.Where(sig => sig.SigNo == 3).FirstOrDefault().GivenName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 3).FirstOrDefault().MiddleName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 3).FirstOrDefault().FamilyName, false, false);
                document.Range.Replace("[ECLIPSE_SIG4_FULLNAME_AMM]", form.SignatoryEntityList.Where(sig => sig.SigNo == 4).FirstOrDefault().GivenName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 4).FirstOrDefault().MiddleName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 4).FirstOrDefault().FamilyName, false, false);

                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE3_AMM]", form.SignatoryEntityList.Where(sig => sig.SigNo == 3).FirstOrDefault().CorporateTitel, false, false);
                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE4_AMM]", form.SignatoryEntityList.Where(sig => sig.SigNo == 4).FirstOrDefault().CorporateTitel, false, false);

            }
            else
            {
                document.Range.Replace("[ECLIPSE_LEFTBLACK_AMM]", "*This form is intentionally left blank.", false, false);
                document.Range.Replace("[ECLIPSE_CLIENT_NAME_AMM]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_TRUST_NAME_AMM]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_ACCTYPE_NAME_AMM]", String.Empty, false, false);

                document.Range.Replace("[ECLIPSE_ADDR1_AMM]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_ADDR2_AMM]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_STATE_AMM]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_PC_AMM]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_SUBURB_AMM]", String.Empty, false, false);


                document.Range.Replace("[ECLIPSE_ACCTYPE_AMM]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_SIG1_FULLNAME_AMM]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_SIG2_FULLNAME_AMM]", String.Empty, false, false);

                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE1_AMM]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE2_AMM]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_SIG3_FULLNAME_AMM]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_SIG4_FULLNAME_AMM]", String.Empty, false, false);

                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE3_AMM]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE4_AMM]", String.Empty, false, false);
            }

            document.Range.Replace("[ECLIPSE_LEFTBLACK_AMM]", String.Empty, false, false);
        }

        private static void SetAsxSection(UMAFormEntity form, bool isASX, Aspose.Words.Document document)
        {
            if (isASX)
            {
                document.Range.Replace("[ECLIPSE_LEFTBLACK_DB]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_CLIENT_NAME_DB]", form.accountName, false, false);
                document.Range.Replace("[ECLIPSE_TRUST_NAME_DB]", form.trustName, false, false);
                document.Range.Replace("[ECLIPSE_ACCTYPE_NAME_DB]", form.accountType, false, false);


                if (form.ChessAddressLine1 != string.Empty)
                {
                    document.Range.Replace("[ECLIPSE_ADDR1_DB]", form.ChessAddressLine1, false, false);
                    document.Range.Replace("[ECLIPSE_ADDR2_DB]", form.ChessAddressLine2, false, false);
                    document.Range.Replace("[ECLIPSE_STATE_DB]", form.ChessState, false, false);
                    document.Range.Replace("[ECLIPSE_PC_DB]", form.ChessPostCode, false, false);
                    document.Range.Replace("[ECLIPSE_SUBURB_DB]", form.ChessSuburb, false, false);
                }
                else
                {
                    document.Range.Replace("[ECLIPSE_ADDR1_DB]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().ResiAddressLine1, false, false);
                    document.Range.Replace("[ECLIPSE_ADDR2_DB]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().ResiAddressLine2, false, false);
                    document.Range.Replace("[ECLIPSE_STATE_DB]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().ResiState, false, false);
                    document.Range.Replace("[ECLIPSE_PC_DB]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().ResiPostCode, false, false);
                    document.Range.Replace("[ECLIPSE_SUBURB_DB]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().ResiSuburb, false, false);
                }


                document.Range.Replace("[ECLIPSE_ACCTYPE_DB]", form.accountType, false, false);
                document.Range.Replace("[ECLIPSE_SIG1_FULLNAME_DB]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().GivenName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().MiddleName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().FamilyName, false, false);
                document.Range.Replace("[ECLIPSE_SIG2_FULLNAME_DB]", form.SignatoryEntityList.Where(sig => sig.SigNo == 2).FirstOrDefault().GivenName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 2).FirstOrDefault().MiddleName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 2).FirstOrDefault().FamilyName, false, false);

                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE1_DB]", form.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault().CorporateTitel, false, false);
                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE2_DB]", form.SignatoryEntityList.Where(sig => sig.SigNo == 2).FirstOrDefault().CorporateTitel, false, false);

                document.Range.Replace("[ECLIPSE_SIG3_FULLNAME_DB]", form.SignatoryEntityList.Where(sig => sig.SigNo == 3).FirstOrDefault().GivenName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 3).FirstOrDefault().MiddleName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 3).FirstOrDefault().FamilyName, false, false);
                document.Range.Replace("[ECLIPSE_SIG4_FULLNAME_DB]", form.SignatoryEntityList.Where(sig => sig.SigNo == 4).FirstOrDefault().GivenName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 4).FirstOrDefault().MiddleName + " " + form.SignatoryEntityList.Where(sig => sig.SigNo == 4).FirstOrDefault().FamilyName, false, false);

                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE3_DB]", form.SignatoryEntityList.Where(sig => sig.SigNo == 3).FirstOrDefault().CorporateTitel, false, false);
                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE4_DB]", form.SignatoryEntityList.Where(sig => sig.SigNo == 4).FirstOrDefault().CorporateTitel, false, false);

            }
            else
            {
                document.Range.Replace("[ECLIPSE_LEFTBLACK_DB]", "*This form is intentionally left blank.", false, false);
                document.Range.Replace("[ECLIPSE_CLIENT_NAME_DB]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_TRUST_NAME_DB]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_ACCTYPE_NAME_DB]", String.Empty, false, false);

                document.Range.Replace("[ECLIPSE_ADDR1_DB]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_ADDR2_DB]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_STATE_DB]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_PC_DB]", String.Empty, false, false);


                document.Range.Replace("[ECLIPSE_ACCTYPE_DB]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_SIG1_FULLNAME_DB]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_SIG2_FULLNAME_DB]", String.Empty, false, false);

                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE1_DB]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE2_DB]", String.Empty, false, false);

                document.Range.Replace("[ECLIPSE_SIG3_FULLNAME_DB]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_SIG4_FULLNAME_DB]", String.Empty, false, false);

                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE3_DB]", String.Empty, false, false);
                document.Range.Replace("[ECLIPSE_CLIENT_COM_TITLE4_DB]", String.Empty, false, false);
            }

            document.Range.Replace("[ECLIPSE_LEFTBLACK_DB]", String.Empty, false, false);
        }

        private static void AddModelTable(Aspose.Words.Document document, ModelEntity configuredModel)
        {
            if (configuredModel != null)
            {
                document.Range.Replace("[ECLIPSE_PROGRAM]", configuredModel.Name, false, false);
                document.Range.Replace("[ECLIPSE_PROGRAM_CODE]", configuredModel.ProgramCode, false, false);

                DocumentBuilder builder = new DocumentBuilder(document);
                builder.MoveToBookmark("ECLIPSE_MODEL", false, true);

                // Insert a cell
                builder.InsertCell();
                builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                builder.CellFormat.LeftPadding = 2;
                builder.CellFormat.Width = 70;
                builder.CellFormat.Orientation = TextOrientation.Horizontal;
                builder.Bold = true;
                builder.Write("Asset Class");

                builder.InsertCell();
                builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                builder.CellFormat.LeftPadding = 2;
                builder.CellFormat.Orientation = TextOrientation.Horizontal;
                builder.Bold = true;
                builder.Write("Minimum");

                builder.InsertCell();
                builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                builder.CellFormat.LeftPadding = 2;
                builder.CellFormat.Orientation = TextOrientation.Horizontal;
                builder.Bold = true;
                builder.Write("Target Weight");

                builder.InsertCell();
                builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                builder.CellFormat.LeftPadding = 2;
                builder.CellFormat.Orientation = TextOrientation.Horizontal;
                builder.Bold = true;
                builder.Write("Maximum");

                builder.EndRow();

                foreach (AssetEntity assetEntity in configuredModel.Assets)
                {
                    double totalMinAllocation = 0;
                    double totalMaxAllocation = 0;
                    foreach (ProductEntity productEntity in assetEntity.Products)
                    {
                        totalMinAllocation += productEntity.MinAllocation;
                        totalMaxAllocation += productEntity.MaxAllocation;
                    }
                    // Insert a cell
                    builder.InsertCell();
                    builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                    builder.Bold = true;
                    builder.CellFormat.LeftPadding = 2;
                    builder.Write(assetEntity.Description);

                    builder.InsertCell();
                    builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                    builder.Write(totalMinAllocation.ToString() + "%");
                    builder.CellFormat.LeftPadding = 2;

                    builder.InsertCell();
                    builder.CellFormat.LeftPadding = 2;
                    builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                    builder.Write(assetEntity.SharePercentage.ToString() + "%");

                    builder.InsertCell();
                    builder.CellFormat.LeftPadding = 2;
                    builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                    builder.Write(totalMaxAllocation.ToString() + "%");

                    builder.EndRow();
                }

                //ECLIPSE_EXCL
                // Insert a cell
                builder.InsertCell();
                builder.CellFormat.LeftPadding = 2;
                builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                builder.Bold = true;

                builder.InsertCell();
                builder.CellFormat.LeftPadding = 2;
                builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;

                builder.InsertCell();
                builder.CellFormat.LeftPadding = 2;
                builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                builder.Bold = true;
                builder.Write("100%");

                builder.EndRow();
                builder.CellFormat.LeftPadding = 2;
                builder.CellFormat.Width = 70;
                Aspose.Words.Tables.Table table = builder.EndTable();
            }
        }

        protected void DePopulateUMAForm(UMAFormEntity umaFormEntity)
        {
            #region Account

            umaFormEntity.accountType = AccountType;
            umaFormEntity.accountNO = AccountNO;
            umaFormEntity.accountName = AccountName;
            umaFormEntity.trustName = TrustName;
            umaFormEntity.IFACID = IFACID;

            umaFormEntity.userName = txtUserIDAdviserID.Text;
            umaFormEntity.modifiedByUser = txtLastModifiedUser.Text;
            umaFormEntity.CreatedOnDateForm = txtCreatedDate.Text;
            umaFormEntity.lastModifiedDateForm = txtLastModifiedDate.Text;

            if (txtFormStatus.Text == "Intialised")
                umaFormEntity.UMAFormStatus = UMAFormStatus.Intialised;
            if (txtFormStatus.Text == "Under review")
                umaFormEntity.UMAFormStatus = UMAFormStatus.Intialised;
            if (txtFormStatus.Text == "Finalised")
                umaFormEntity.UMAFormStatus = UMAFormStatus.Intialised;


            #endregion

            #region fee

            umaFormEntity.TierFee = TiredFee.Checked;
            umaFormEntity.OnGoingFee = OngoingFee.Checked;

            umaFormEntity.UpFrontFee = Convert.ToDecimal(UpFrontFee);
            umaFormEntity.OngoingFeeDollar = OngoingFeeAmount;
            umaFormEntity.OngoingFeePercentage = OngoingFeePercentage;
            umaFormEntity.Tier1To = Convert.ToDecimal(TierTo1);
            umaFormEntity.Tier1From = Convert.ToDecimal(TierFrom1);
            umaFormEntity.Tier1Percentage = Convert.ToDecimal(TierPercentage1);
            umaFormEntity.Tier2To = Convert.ToDecimal(TierTo2);
            umaFormEntity.Tier2From = Convert.ToDecimal(TierFrom2);
            umaFormEntity.Tier2Percentage = Convert.ToDecimal(TierPercentage2);
            umaFormEntity.Tier3To = Convert.ToDecimal(TierTo3);
            umaFormEntity.Tier3From = Convert.ToDecimal(TierFrom3);
            umaFormEntity.Tier3Percentage = Convert.ToDecimal(TierPercentage3);
            umaFormEntity.Tier4Percentage = Convert.ToDecimal(TierPercentage4);
            umaFormEntity.Tier4To = Convert.ToDecimal(TierTo4);
            umaFormEntity.Tier4From = Convert.ToDecimal(TierFrom4);

            umaFormEntity.Tier5To = Convert.ToDecimal(TierTo5);
            umaFormEntity.Tier5From = Convert.ToDecimal(TierFrom5);
            umaFormEntity.Tier5Percentage = Convert.ToDecimal(TierPercentage5);

            #endregion

            #region TFN ABN ABRN

            umaFormEntity.TFNSig1 = TFNSig1;
            umaFormEntity.TFNSig2 = TFNSig2;
            umaFormEntity.TFNSig3 = TFNSig3;
            umaFormEntity.TFNSig4 = TFNSig4;
            umaFormEntity.ABNCompany = txtABN.Text;
            umaFormEntity.ACN = txtACN.Text;
            umaFormEntity.TFNTrustFund = TFNTrustSuperfund;
            umaFormEntity.AppliedForABN = HaveABNForTaxWithHolding;
            umaFormEntity.NonRedidentCountry = CountryABNForTaxWithHolding;

            #endregion

            #region Authority To Act

            umaFormEntity.EclipseOnline = AuthorityToActEclispe;
            umaFormEntity.AustralianMoneyMarket = AuthorityToActAMM;
            umaFormEntity.MacquarieCMA = AuthorityToActMacBank;
            umaFormEntity.InvestorStatusWholesaleInvestor = InvestorStatusWholesaleInvestor;
            umaFormEntity.InvestorStatusSophisticatedInvestor = InvestorStatusSophisticatedInvestor;
            umaFormEntity.InvestorStatusProfessionalInvestor = InvestorStatusProfessionalInvestor;
            umaFormEntity.InvestorStatusRetailInvestor = InvestorStatusRetailInvestor;
            umaFormEntity.CertificateFromAccountantIsAttached = CertificateFromAccountantIsAttached;

            #endregion

            #region Services

            umaFormEntity.SecuritiesExclusionEntityList = SecuritiesExclusionListEntityList;
            umaFormEntity.StateStreetEntityList = StateStreetEntityList;
            umaFormEntity.DIYAmount = DIYAmount;
            umaFormEntity.DIWMAmount = DIWMAmount;
            umaFormEntity.InvestmentProgram = InvestmentProgram;
            umaFormEntity.InvestmentProgramCode = InvestmentProgramCode;
            umaFormEntity.DIFMAmount = DIFMAmount;
            umaFormEntity.DoItYourSelf = DoItYourSelf;
            umaFormEntity.DIYBWA = DIYBWA;
            umaFormEntity.DIYDesktopBroker = DIYDesktopBroker;
            umaFormEntity.DIYFIIG = DIYFIIG;
            umaFormEntity.DIYAMM = DIYAMM;
            umaFormEntity.DoItWithMe = DoItWithMe;
            umaFormEntity.DIWNMBWA = DIWNMBWA;
            umaFormEntity.DIWNMDesktopBroker = DIWNMDesktopBroker;
            umaFormEntity.DIWNMFIIG = DIWNMFIIG;
            umaFormEntity.DIWNMAMM = DIWNMAMM;
            umaFormEntity.DIWNMSS = DIWNMSS;
            umaFormEntity.DIWMServiceManagedViaMDA = DIWMServiceManagedViaMDA;
            umaFormEntity.DoItForhMe = DoItForhMe;
            umaFormEntity.DIFNMBWA = DIFNMBWA;
            umaFormEntity.DIFNMDesktopBroker = DIFNMDesktopBroker;
            umaFormEntity.DIFNMSS = DIFNMSS;
            umaFormEntity.DIFMNominatePreferences = DIFMNominatePreferences;

            umaFormEntity.MinBalance = MinBalance;
            umaFormEntity.MinBalancePercentage = MinBalancePercentage;
            umaFormEntity.MinTrade = MinTrade;
            umaFormEntity.MinTradePercentage = MinTradePercentage;

            umaFormEntity.AssignedAllocatedValueHoldSell = AssignedAllocatedValueHoldSell;
            umaFormEntity.DistributedAllocatedValueHoldSell = DistributedAllocatedValueHoldSell;
            umaFormEntity.AssignedAllocatedValueExclusion = AssignedAllocatedValueExclusion;
            umaFormEntity.DistributedAllocatedValueExclusion = DistributedAllocatedValueExclusion;
            umaFormEntity.AssignedAllocatedValueMinTradeHolding = AssignedAllocatedValueMinTradeHolding;
            umaFormEntity.DistributedAllocatedValueMinTradeHolding = DistributedAllocatedValueMinTradeHolding;

            #endregion

            #region Platforms

            umaFormEntity.NameOfPlatformsBT = NameOfPlatformsBT;
            umaFormEntity.InvestorNoBT = InvestorNoBT;
            umaFormEntity.AdviserNoBT = AdviserNoBT;
            umaFormEntity.ProviderNoBT = ProviderNoBT;

            umaFormEntity.NameOfPlatformsEclipse = NameOfPlatformsEclipse;
            umaFormEntity.InvestorNoEclipse = InvestorNoEclipse;
            umaFormEntity.AdviserNoEclipse = AdviserNoEclipse;
            umaFormEntity.ProviderNoEclipse = ProviderNoEclipse;

            umaFormEntity.MaqCMALinkBWA = MaqCMALinkBWA;
            umaFormEntity.MaqCMADIYDIWM = MaqCMADIYDIWM;
            umaFormEntity.MaqCMABSB = MaqCMABSB;
            umaFormEntity.MaqCMAAccountNo = MaqCMAAccountNo;
            umaFormEntity.MaqCMAAccountName = MaqCMAAccountName;

            #endregion

            #region BWA Option

            umaFormEntity.DIYPhoneAccess = DIYPhoneAccess;
            umaFormEntity.DIYOnlineAccess = DIYOnlineAccess;
            umaFormEntity.DIYDebitCard = DIYDebitCard;
            umaFormEntity.DIYChequeBook = DIYChequeBook;
            umaFormEntity.DIYDepositBook = DIYDepositBook;
            umaFormEntity.DIYMannerOfOperation = DIYMannerOfOperation;
            umaFormEntity.DIWMPhoneAccess = DIWMPhoneAccess;
            umaFormEntity.DIWMOnlineAccess = DIWMOnlineAccess;
            umaFormEntity.DIWMDebitCard = DIWMDebitCard;
            umaFormEntity.DIWMChequeBook = DIWMChequeBook;
            umaFormEntity.DIWMDepositBook = DIWMDepositBook;
            umaFormEntity.DIWMMannerOfOperation = DIWMMannerOfOperation;

            #endregion

            #region Chess

            umaFormEntity.TransferListedSecuritiesToDesktopBroker = TransferListedSecuritiesToDesktopBroker;
            umaFormEntity.ChessDIYDIWM = ChessDIYDIWM;
            umaFormEntity.ChessRegisteredAccountName = ChessRegisteredAccountName;
            umaFormEntity.ChessAccountDesignation = ChessAccountDesignation;
            umaFormEntity.ChessAddressLine1 = ChessAddressLine1;
            umaFormEntity.ChessAddressLine2 = ChessAddressLine2;
            umaFormEntity.ChessSuburb = ChessSuburb;
            umaFormEntity.ChessState = ChessState;
            umaFormEntity.ChessPostCode = ChessPostCode;
            umaFormEntity.ChessCountry = ChessCountry;
            umaFormEntity.IssuedSponsor = IssuedSponsor;
            umaFormEntity.ExactNameMatch = ExactNameMatch;
            umaFormEntity.ExactNameMatchAmount = ExactNameMatchAmount;
            umaFormEntity.ExactNameNoMatch = ExactNameNoMatch;
            umaFormEntity.ExactNameNoMatchAmount = ExactNameNoMatchAmount;
            umaFormEntity.BrokerSponsored = BrokerSponsored;
            umaFormEntity.ExistingBroker1 = ExistingBroker1;
            umaFormEntity.ClientNo1 = ClientNo1;
            umaFormEntity.ClientHin1 = ClientHin1;
            umaFormEntity.ExistingBroker2 = ExistingBroker2;
            umaFormEntity.ClientNo2 = ClientNo2;
            umaFormEntity.ClientHin2 = ClientHin2;

            #endregion

            #region CheckList

            umaFormEntity.ProofIDCheckListItem = ProofIDCheckListItem;
            umaFormEntity.TrustDeedChecListItem = TrustDeedChecListItem;
            umaFormEntity.LetterOfAccountantChecListItem = LetterOfAccountantChecListItem;
            umaFormEntity.BusinessNameRegoChecListItem = BusinessNameRegoChecListItem;
            umaFormEntity.IssueSponsorChessSponsorChecListItem = IssueSponsorChessSponsorChecListItem;
            umaFormEntity.IssueSponsorChessSponsorHoldsingStatements = IssueSponsorChessSponsorHoldsingStatements;
            umaFormEntity.OffMarketTransferChecListItem = OffMarketTransferChecListItem;
            umaFormEntity.ChangeOFClientChecListItem = ChangeOFClientChecListItem;
            umaFormEntity.BrokerToBrokerChecListItem = BrokerToBrokerChecListItem;
            umaFormEntity.ProofOfAddressChecListItem = ProofOfAddressChecListItem;

            #endregion

            #region Addresses

            umaFormEntity.MialingAddressLine1 = AddressDetails.MialingAddressLine1;
            umaFormEntity.MialingAddressLine2 = AddressDetails.MialingAddressLine2;
            umaFormEntity.MialingSuburb = AddressDetails.MialingSuburb;
            umaFormEntity.MialingState = AddressDetails.MialingState;
            umaFormEntity.MialingPostCode = AddressDetails.MialingPostCode;
            umaFormEntity.MialingCountry = AddressDetails.MialingCountry;

            umaFormEntity.DupMialingAddressLine1 = AddressDetails.DupMialingAddressLine1;
            umaFormEntity.DupMialingAddressLine2 = AddressDetails.DupMialingAddressLine2;
            umaFormEntity.DupMialingSuburb = AddressDetails.DupMialingSuburb;
            umaFormEntity.DupMialingState = AddressDetails.DupMialingState;
            umaFormEntity.DupMialingPostCode = AddressDetails.DupMialingPostCode;
            umaFormEntity.DupMialingCountry = AddressDetails.DupMialingCountry;

            umaFormEntity.RegAddressLine1 = AddressDetails.RegAddressLine1;
            umaFormEntity.RegAddressLine2 = AddressDetails.RegAddressLine2;
            umaFormEntity.RegSuburb = AddressDetails.RegSuburb;
            umaFormEntity.RegState = AddressDetails.RegState;
            umaFormEntity.RegPostCode = AddressDetails.RegPostCode;
            umaFormEntity.RegCountry = AddressDetails.RegCountry;

            umaFormEntity.PrincipalAddressLine1 = AddressDetails.PrincipalAddressLine1;
            umaFormEntity.PrincipalAddressLine2 = AddressDetails.PrincipalAddressLine2;
            umaFormEntity.PrincipalSuburb = AddressDetails.PrincipalSuburb;
            umaFormEntity.PrincipalState = AddressDetails.PrincipalState;
            umaFormEntity.PrincipalPostCode = AddressDetails.PrincipalPostCode;
            umaFormEntity.PrincipalCountry = AddressDetails.PrincipalCountry;

            #endregion

            #region Signatories

            umaFormEntity.Sig2Required = Sig2Required.Checked;
            umaFormEntity.Sig3Required = Sig3Required.Checked;
            umaFormEntity.Sig4Required = Sig4Required.Checked;

            SignatoryEntity signatoryEntity1 = umaFormEntity.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault();
            SignatoryEntity signatoryEntity2 = umaFormEntity.SignatoryEntityList.Where(sig => sig.SigNo == 2).FirstOrDefault();
            SignatoryEntity signatoryEntity3 = umaFormEntity.SignatoryEntityList.Where(sig => sig.SigNo == 3).FirstOrDefault();
            SignatoryEntity signatoryEntity4 = umaFormEntity.SignatoryEntityList.Where(sig => sig.SigNo == 4).FirstOrDefault();

            #region Signatory 1

            signatoryEntity1.Title = SignatoriesDetails1.Title;

            signatoryEntity1.FamilyName = SignatoriesDetails1.FamilyName;
            signatoryEntity1.GivenName = SignatoriesDetails1.GivenName;
            signatoryEntity1.MiddleName = SignatoriesDetails1.MiddleName;
            signatoryEntity1.OtherName = SignatoriesDetails1.OtherName;
            signatoryEntity1.DateOfBirth = SignatoriesDetails1.DateOfBirth;
            signatoryEntity1.CorporateTitel = SignatoriesDetails1.CorporateTite;

            signatoryEntity1.ResiAddressLine1 = SignatoriesDetails1.ResiAddressLine1;
            signatoryEntity1.ResiAddressLine2 = SignatoriesDetails1.ResiAddressLine2;
            signatoryEntity1.ResiSuburb = SignatoriesDetails1.ResiSuburb;
            signatoryEntity1.ResiState = SignatoriesDetails1.ResiState;
            signatoryEntity1.ResiPostCode = SignatoriesDetails1.ResiPostCode;
            signatoryEntity1.ResiCountry = SignatoriesDetails1.ResiCountry;

            signatoryEntity1.MailAddressLine1 = SignatoriesDetails1.MailAddressLine1;
            signatoryEntity1.MailAddressLine2 = SignatoriesDetails1.MailAddressLine2;
            signatoryEntity1.MailSuburb = SignatoriesDetails1.MailSuburb;
            signatoryEntity1.MailState = SignatoriesDetails1.MailState;
            signatoryEntity1.MailPostCode = SignatoriesDetails1.MailPostCode;
            signatoryEntity1.MailCountry = SignatoriesDetails1.MailCountry;

            signatoryEntity1.Occupation = SignatoriesDetails1.Occupation;
            signatoryEntity1.Employer = SignatoriesDetails1.Employer;
            signatoryEntity1.MainCountryOFAddressSig = SignatoriesDetails1.MainCountryOFAddressSig;
            signatoryEntity1.ContactPH = SignatoriesDetails1.ContactPH;
            signatoryEntity1.AlternativePH = SignatoriesDetails1.AlternativePH;
            signatoryEntity1.Fax = SignatoriesDetails1.Fax;
            signatoryEntity1.Email = SignatoriesDetails1.Email;

            signatoryEntity1.DriverLicSig = SignatoriesDetails1.DriverLicSig;
            signatoryEntity1.PassportSig = SignatoriesDetails1.PassportSig;
            signatoryEntity1.LicDocumentIssuer = SignatoriesDetails1.LicDocumentIssuer;
            signatoryEntity1.LicCertifiedCopyAttached = SignatoriesDetails1.LicCertifiedCopyAttached;
            signatoryEntity1.LicIssueDate = SignatoriesDetails1.LicIssueDate;
            signatoryEntity1.LicExpiryDate = SignatoriesDetails1.LicExpiryDate;
            signatoryEntity1.DocumentNoLic = SignatoriesDetails1.DocumentNoLic;
            signatoryEntity1.DocumentNoPassport = SignatoriesDetails1.DocumentNoPassport;

            signatoryEntity1.PassportDocumentIssuer = SignatoriesDetails1.PassportDocumentIssuer;
            signatoryEntity1.PassportCertifiedCopyAttached = SignatoriesDetails1.PassportCertifiedCopyAttached;
            signatoryEntity1.PassportIssueDate = SignatoriesDetails1.PassportIssueDate;
            signatoryEntity1.PassportExpiryDate = SignatoriesDetails1.PassportExpiryDate;

            #endregion

            #region Signatory 2

            signatoryEntity2.Title = SignatoriesDetails2.Title;

            signatoryEntity2.FamilyName = SignatoriesDetails2.FamilyName;
            signatoryEntity2.GivenName = SignatoriesDetails2.GivenName;
            signatoryEntity2.MiddleName = SignatoriesDetails2.MiddleName;
            signatoryEntity2.OtherName = SignatoriesDetails2.OtherName;
            signatoryEntity2.DateOfBirth = SignatoriesDetails2.DateOfBirth;
            signatoryEntity2.CorporateTitel = SignatoriesDetails2.CorporateTite;

            signatoryEntity2.ResiAddressLine1 = SignatoriesDetails2.ResiAddressLine1;
            signatoryEntity2.ResiAddressLine2 = SignatoriesDetails2.ResiAddressLine2;
            signatoryEntity2.ResiSuburb = SignatoriesDetails2.ResiSuburb;
            signatoryEntity2.ResiState = SignatoriesDetails2.ResiState;
            signatoryEntity2.ResiPostCode = SignatoriesDetails2.ResiPostCode;
            signatoryEntity2.ResiCountry = SignatoriesDetails2.ResiCountry;

            signatoryEntity2.MailAddressLine1 = SignatoriesDetails2.MailAddressLine1;
            signatoryEntity2.MailAddressLine2 = SignatoriesDetails2.MailAddressLine2;
            signatoryEntity2.MailSuburb = SignatoriesDetails2.MailSuburb;
            signatoryEntity2.MailState = SignatoriesDetails2.MailState;
            signatoryEntity2.MailPostCode = SignatoriesDetails2.MailPostCode;
            signatoryEntity2.MailCountry = SignatoriesDetails2.MailCountry;

            signatoryEntity2.Occupation = SignatoriesDetails2.Occupation;
            signatoryEntity2.Employer = SignatoriesDetails2.Employer;
            signatoryEntity2.MainCountryOFAddressSig = SignatoriesDetails2.MainCountryOFAddressSig;
            signatoryEntity2.ContactPH = SignatoriesDetails2.ContactPH;
            signatoryEntity2.AlternativePH = SignatoriesDetails2.AlternativePH;
            signatoryEntity2.Fax = SignatoriesDetails2.Fax;
            signatoryEntity2.Email = SignatoriesDetails2.Email;

            signatoryEntity2.DriverLicSig = SignatoriesDetails2.DriverLicSig;
            signatoryEntity2.PassportSig = SignatoriesDetails2.PassportSig;
            signatoryEntity2.LicDocumentIssuer = SignatoriesDetails2.LicDocumentIssuer;
            signatoryEntity2.LicCertifiedCopyAttached = SignatoriesDetails2.LicCertifiedCopyAttached;
            signatoryEntity2.LicIssueDate = SignatoriesDetails2.LicIssueDate;
            signatoryEntity2.LicExpiryDate = SignatoriesDetails2.LicExpiryDate;
            signatoryEntity2.DocumentNoLic = SignatoriesDetails2.DocumentNoLic;
            signatoryEntity2.DocumentNoPassport = SignatoriesDetails2.DocumentNoPassport;

            signatoryEntity2.PassportDocumentIssuer = SignatoriesDetails2.PassportDocumentIssuer;
            signatoryEntity2.PassportCertifiedCopyAttached = SignatoriesDetails2.PassportCertifiedCopyAttached;
            signatoryEntity2.PassportIssueDate = SignatoriesDetails2.PassportIssueDate;
            signatoryEntity2.PassportExpiryDate = SignatoriesDetails2.PassportExpiryDate;

            #endregion

            #region Signatory 3

            signatoryEntity3.Title = SignatoriesDetails3.Title;

            signatoryEntity3.FamilyName = SignatoriesDetails3.FamilyName;
            signatoryEntity3.GivenName = SignatoriesDetails3.GivenName;
            signatoryEntity3.MiddleName = SignatoriesDetails3.MiddleName;
            signatoryEntity3.OtherName = SignatoriesDetails3.OtherName;
            signatoryEntity3.DateOfBirth = SignatoriesDetails3.DateOfBirth;
            signatoryEntity3.CorporateTitel = SignatoriesDetails3.CorporateTite;

            signatoryEntity3.ResiAddressLine1 = SignatoriesDetails3.ResiAddressLine1;
            signatoryEntity3.ResiAddressLine2 = SignatoriesDetails3.ResiAddressLine2;
            signatoryEntity3.ResiSuburb = SignatoriesDetails3.ResiSuburb;
            signatoryEntity3.ResiState = SignatoriesDetails3.ResiState;
            signatoryEntity3.ResiPostCode = SignatoriesDetails3.ResiPostCode;
            signatoryEntity3.ResiCountry = SignatoriesDetails3.ResiCountry;

            signatoryEntity3.MailAddressLine1 = SignatoriesDetails3.MailAddressLine1;
            signatoryEntity3.MailAddressLine2 = SignatoriesDetails3.MailAddressLine2;
            signatoryEntity3.MailSuburb = SignatoriesDetails3.MailSuburb;
            signatoryEntity3.MailState = SignatoriesDetails3.MailState;
            signatoryEntity3.MailPostCode = SignatoriesDetails3.MailPostCode;
            signatoryEntity3.MailCountry = SignatoriesDetails3.MailCountry;

            signatoryEntity3.Occupation = SignatoriesDetails3.Occupation;
            signatoryEntity3.Employer = SignatoriesDetails3.Employer;
            signatoryEntity3.MainCountryOFAddressSig = SignatoriesDetails3.MainCountryOFAddressSig;
            signatoryEntity3.ContactPH = SignatoriesDetails3.ContactPH;
            signatoryEntity3.AlternativePH = SignatoriesDetails3.AlternativePH;
            signatoryEntity3.Fax = SignatoriesDetails3.Fax;
            signatoryEntity3.Email = SignatoriesDetails3.Email;

            signatoryEntity3.DriverLicSig = SignatoriesDetails3.DriverLicSig;
            signatoryEntity3.PassportSig = SignatoriesDetails3.PassportSig;
            signatoryEntity3.LicDocumentIssuer = SignatoriesDetails3.LicDocumentIssuer;
            signatoryEntity3.LicCertifiedCopyAttached = SignatoriesDetails3.LicCertifiedCopyAttached;
            signatoryEntity3.LicIssueDate = SignatoriesDetails3.LicIssueDate;
            signatoryEntity3.LicExpiryDate = SignatoriesDetails3.LicExpiryDate;
            signatoryEntity3.DocumentNoLic = SignatoriesDetails3.DocumentNoLic;
            signatoryEntity3.DocumentNoPassport = SignatoriesDetails3.DocumentNoPassport;

            signatoryEntity3.PassportDocumentIssuer = SignatoriesDetails3.PassportDocumentIssuer;
            signatoryEntity3.PassportCertifiedCopyAttached = SignatoriesDetails3.PassportCertifiedCopyAttached;
            signatoryEntity3.PassportIssueDate = SignatoriesDetails3.PassportIssueDate;
            signatoryEntity3.PassportExpiryDate = SignatoriesDetails3.PassportExpiryDate;

            #endregion

            #region Signatory 4

            signatoryEntity4.Title = SignatoriesDetails4.Title;

            signatoryEntity4.FamilyName = SignatoriesDetails4.FamilyName;
            signatoryEntity4.GivenName = SignatoriesDetails4.GivenName;
            signatoryEntity4.MiddleName = SignatoriesDetails4.MiddleName;
            signatoryEntity4.OtherName = SignatoriesDetails4.OtherName;
            signatoryEntity4.DateOfBirth = SignatoriesDetails4.DateOfBirth;
            signatoryEntity4.CorporateTitel = SignatoriesDetails4.CorporateTite;

            signatoryEntity4.ResiAddressLine1 = SignatoriesDetails4.ResiAddressLine1;
            signatoryEntity4.ResiAddressLine2 = SignatoriesDetails4.ResiAddressLine2;
            signatoryEntity4.ResiSuburb = SignatoriesDetails4.ResiSuburb;
            signatoryEntity4.ResiState = SignatoriesDetails4.ResiState;
            signatoryEntity4.ResiPostCode = SignatoriesDetails4.ResiPostCode;
            signatoryEntity4.ResiCountry = SignatoriesDetails4.ResiCountry;

            signatoryEntity4.MailAddressLine1 = SignatoriesDetails4.MailAddressLine1;
            signatoryEntity4.MailAddressLine2 = SignatoriesDetails4.MailAddressLine2;
            signatoryEntity4.MailSuburb = SignatoriesDetails4.MailSuburb;
            signatoryEntity4.MailState = SignatoriesDetails4.MailState;
            signatoryEntity4.MailPostCode = SignatoriesDetails4.MailPostCode;
            signatoryEntity4.MailCountry = SignatoriesDetails4.MailCountry;

            signatoryEntity4.Occupation = SignatoriesDetails4.Occupation;
            signatoryEntity4.Employer = SignatoriesDetails4.Employer;
            signatoryEntity4.MainCountryOFAddressSig = SignatoriesDetails4.MainCountryOFAddressSig;
            signatoryEntity4.ContactPH = SignatoriesDetails4.ContactPH;
            signatoryEntity4.AlternativePH = SignatoriesDetails4.AlternativePH;
            signatoryEntity4.Fax = SignatoriesDetails4.Fax;
            signatoryEntity4.Email = SignatoriesDetails4.Email;

            signatoryEntity4.DriverLicSig = SignatoriesDetails4.DriverLicSig;
            signatoryEntity4.PassportSig = SignatoriesDetails4.PassportSig;
            signatoryEntity4.LicDocumentIssuer = SignatoriesDetails4.LicDocumentIssuer;
            signatoryEntity4.LicCertifiedCopyAttached = SignatoriesDetails4.LicCertifiedCopyAttached;
            signatoryEntity4.LicIssueDate = SignatoriesDetails4.LicIssueDate;
            signatoryEntity4.LicExpiryDate = SignatoriesDetails4.LicExpiryDate;
            signatoryEntity4.DocumentNoLic = SignatoriesDetails4.DocumentNoLic;
            signatoryEntity4.DocumentNoPassport = SignatoriesDetails4.DocumentNoPassport;

            signatoryEntity4.PassportDocumentIssuer = SignatoriesDetails4.PassportDocumentIssuer;
            signatoryEntity4.PassportCertifiedCopyAttached = SignatoriesDetails4.PassportCertifiedCopyAttached;
            signatoryEntity4.PassportIssueDate = SignatoriesDetails4.PassportIssueDate;
            signatoryEntity4.PassportExpiryDate = SignatoriesDetails4.PassportExpiryDate;

            #endregion

            #endregion

            umaFormEntity.SecuritiesExclusionEntityList = SecuritiesExclusionListEntityList;
            umaFormEntity.StateStreetEntityList = StateStreetEntityList;
            umaFormEntity.IncludedUsers = UsersWorkFlow.InculdedUsers;
        }

        protected override void Intialise()
        {
            base.Intialise();
        }

        public override void LoadPage()
        {
            ExcusionList.RowCommand += new C1GridViewCommandEventHandler(ExcusionList_RowCommand);
            ExcusionList.RowDeleting += new C1GridViewDeleteEventHandler(ExcusionList_RowDeleting);

            StateStreetList.RowCommand += new C1GridViewCommandEventHandler(StateStreetList_RowCommand);
            StateStreetList.RowDeleting += new C1GridViewDeleteEventHandler(ExcusionList_RowDeleting);

            IOrganization orgCM = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            SecuritiesDS securitiesDS = new Oritax.TaxSimp.DataSets.SecuritiesDS();
            orgCM.GetData(securitiesDS);
            presentationDataSecuritiesDS = securitiesDS;

            if (!Page.IsPostBack)
            {
                IFADS ifaDs = new IFADS { CommandType = DatasetCommandTypes.Get, Command = (int)WebCommands.GetOrganizationUnitsByType };
                ifaDs.Unit = new Oritax.TaxSimp.Data.OrganizationUnit
                {
                    CurrentUser = (Page as UMABasePage).GetCurrentUser("Administrator"),
                    Type = ((int)OrganizationType.IFA).ToString(),

                };

                orgCM.GetData(ifaDs);
                cmbIFA.DataSource = ifaDs.ifaTable;
                cmbIFA.DataValueField = ifaDs.ifaTable.CID;
                cmbIFA.DataTextField = ifaDs.ifaTable.TRADINGNAME;
                cmbIFA.DataBind();

                if (!Request.Params.AllKeys.Contains("AppNo"))
                {
                    CreatedByUser = User.Identity.Name;
                    ModifiedByUser = User.Identity.Name;
                    CreatedOnDate = DateTime.Now.ToString("dd/MM/yyyy");
                    AccountNO = Oritax.TaxSimp.Utilities.UniqueID.Get9UniqueKey().ToUpper();
                    txtUserIDAdviserID.Text = User.Identity.Name;
                    txtLastModifiedUser.Text = User.Identity.Name;
                    txtCreatedDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    txtLastModifiedDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    txtFormStatus.Text = "Intialised";
                }
                else
                {
                    txtAccountNo.Text = Request.QueryString["AppNo"].ToString();
                    UMAFormDS ds = new UMAFormDS();
                    ds.AppNo = txtAccountNo.Text;
                    orgCM.GetData(ds);
                    PopulateUMAForm(ds.UMAFormEntity);
                }
                //Here we check the UserType Administrator or not
                UserEntity user = GetCurrentUser();
                UserSecurities.Visible = user.IsAdmin;
            }

            UMAFormEntity validationEntity = new UMAFormEntity();
            DePopulateUMAForm(validationEntity);
            DataTable validationTable = validationEntity.Validate();

            C1GridViewValidation.DataSource = validationTable;
            C1GridViewValidation.DataBind();

            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
            {
                sm.RegisterPostBackControl(btnPrint);
                sm.RegisterPostBackControl(btnPrintSummary);
                sm.RegisterPostBackControl(btnPrintMaq);
            }

            EnableDisableSig2();
            EnableDisableSig3();
            EnableDisableSig4();

            C1ComboBoxInvestmentProgram.DataSource = orgCM.Model.Where(model => model.ServiceType == ServiceTypes.DoItForMe && model.IsPrivate != true);
            C1ComboBoxInvestmentProgram.DataValueField = "ProgramCode";
            C1ComboBoxInvestmentProgram.DataTextField = "Name";
            C1ComboBoxInvestmentProgram.DataBind();

            C1ComboBoxInvestmentProgram.Items.OrderBy(item => item.Text);

            UMABroker.ReleaseBrokerManagedComponent(orgCM);
            base.LoadPage();

            ((Label)Master.FindControl("lblBreadCrumb")).Text = "WORKFLOW&nbsp; > &nbsp;ADD ACCOUNTS";
        }


        public void ExcusionList_RowDeleting(object sender, C1GridViewDeleteEventArgs e) { }

        public override void PopulatePage(System.Data.DataSet ds)
        {
            if (!Page.IsPostBack)
            {
                DataView SecListViewNoFunds = new DataView(presentationDataSecuritiesDS.Tables[SecuritiesDS.SECLIST]);
                SecListViewNoFunds.Sort = SecuritiesDS.CODE + " ASC";
                C1ComboBoxFundsEclusion.DataSource = SecListViewNoFunds.ToTable();
                C1ComboBoxFundsEclusion.DataBind();
            }
        }

        protected void btnAddFundsToExclusionList_Click(object sender, EventArgs e)
        {
            DataRow[] codeRow = presentationDataSecuritiesDS.Tables[SecuritiesDS.SECLIST].Select("Description='" + C1ComboBoxFundsEclusion.Text + "'");

            if (codeRow.Count() > 0)
            {
                string code = (string)codeRow[0]["Code"];
                SecuritiesExclusionEntity securitiesExclusionListEntity = new SecuritiesExclusionEntity(code, C1ComboBoxFundsEclusion.Text, C1ComboBoxFundsEclusionAction.SelectedItem.Text, (Guid)codeRow[0]["ID"]);

                SecuritiesExclusionEntity entity = (SecuritiesExclusionEntity)SecuritiesExclusionListEntityList.Where(secList => secList.Code == code).FirstOrDefault();

                if (entity != null)
                {
                    entity.Code = securitiesExclusionListEntity.Code;
                    entity.InvestmentAction = securitiesExclusionListEntity.InvestmentAction;
                    entity.Description = securitiesExclusionListEntity.Description;
                    entity.ID = securitiesExclusionListEntity.ID;
                }
                else
                    SecuritiesExclusionListEntityList.Add(securitiesExclusionListEntity);

                ExcusionList.DataSource = SecurityExclusionTable();
                ExcusionList.DataBind();
            }
        }

        protected void btnAddFundsToStateStreetList_Click(object sender, EventArgs e)
        {
            if (C1ComboBoxStateStreetSelection.SelectedItem != null)
            {
                StateStreetEntity securitiesExclusionListEntity = new StateStreetEntity(C1ComboBoxStateStreetSelection.SelectedItem.Value, C1ComboBoxStateStreetSelection.SelectedItem.Text);
                StateStreetEntity entity = (StateStreetEntity)StateStreetEntityList.Where(secList => secList.Code == C1ComboBoxStateStreetSelection.SelectedItem.Value).FirstOrDefault();

                if (entity != null)
                {
                    entity.Code = securitiesExclusionListEntity.Code;
                    entity.Description = securitiesExclusionListEntity.Description;
                }
                else
                    StateStreetEntityList.Add(securitiesExclusionListEntity);

                StateStreetList.DataSource = StateStreetPreferenceTable();
                StateStreetList.DataBind();
            }
        }

        protected void ExcusionList_RowCommand(object sender, C1GridViewCommandEventArgs e)
        {
            Guid ID = new Guid(((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[0].Text);
            string code = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[1].Text;
            string desc = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[2].Text;
            string investAction = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[3].Text;

            if (e.CommandName.ToLower() == "delete")
            {
                SecuritiesExclusionEntity entity = (SecuritiesExclusionEntity)SecuritiesExclusionListEntityList.Where(secList => secList.Code == code).FirstOrDefault();
                if (entity != null)
                    SecuritiesExclusionListEntityList.Remove(entity);
                ExcusionList.DataSource = SecurityExclusionTable();
                ExcusionList.DataBind();
            }
        }

        protected void StateStreetList_RowCommand(object sender, C1GridViewCommandEventArgs e)
        {
            string code = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[0].Text;
            string desc = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[1].Text;

            if (e.CommandName.ToLower() == "delete")
            {
                StateStreetEntity entity = (StateStreetEntity)StateStreetEntityList.Where(secList => secList.Code == code).FirstOrDefault();
                if (entity != null)
                    StateStreetEntityList.Remove(entity);
                StateStreetList.DataSource = StateStreetPreferenceTable();
                StateStreetList.DataBind();
            }
        }

        private DataTable SecurityExclusionTable()
        {
            DataTable table = new DataTable("ExclusionListTable");
            table.Columns.Add("ID", typeof(Guid));
            table.Columns.Add("Code", typeof(string));
            table.Columns.Add("Description", typeof(string));
            table.Columns.Add("InvestmentAction", typeof(string));

            foreach (SecuritiesExclusionEntity securitiesExclusionListEntity in SecuritiesExclusionListEntityList)
            {
                DataRow row = table.NewRow();
                row["ID"] = securitiesExclusionListEntity.ID;
                row["Code"] = securitiesExclusionListEntity.Code;
                row["Description"] = securitiesExclusionListEntity.Description;
                row["InvestmentAction"] = securitiesExclusionListEntity.InvestmentAction;
                table.Rows.Add(row);
            }

            return table;
        }

        private DataTable StateStreetPreferenceTable()
        {
            DataTable table = new DataTable("StateStreetListTable");
            table.Columns.Add("Code", typeof(string));
            table.Columns.Add("Description", typeof(string));

            foreach (StateStreetEntity securitiesExclusionListEntity in StateStreetEntityList)
            {
                DataRow row = table.NewRow();
                row["Code"] = securitiesExclusionListEntity.Code;
                row["Description"] = securitiesExclusionListEntity.Description;
                table.Rows.Add(row);
            }

            return table;
        }

        protected void btnValidate_Click(object sender, EventArgs e)
        {

        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            UMAFormEntity entity = new UMAFormEntity();
            DePopulateUMAForm(entity);

            string outputType = "";
            bool isAMM = false;

            if (entity.DIWNMAMM || entity.DIYAMM)
                isAMM = true;

            bool isASX = false;
            if (entity.DIYDesktopBroker || entity.DIWNMDesktopBroker || entity.DIFNMDesktopBroker)
                isASX = true;

            Aspose.Words.License license = new Aspose.Words.License();
            license.SetLicense("Aspose.Words.lic");
            Aspose.Words.Document document = null;

            string strBaseFolder = Request.PhysicalApplicationPath;
            if (!strBaseFolder.EndsWith("\\"))
                strBaseFolder += "\\";
            strBaseFolder += "UMAForm\\Output\\";

            if (entity.DoItForhMe)
            {
                outputType = "(DIFM)";

                UMABroker.SetStart();
                IOrganization iBMC = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                ModelEntity configuredModel = null;
                foreach (ModelEntity modelEntity in iBMC.Model)
                {
                    if (modelEntity.ProgramCode == entity.InvestmentProgramCode)
                    {
                        configuredModel = modelEntity;
                    }
                }

                //document = new Aspose.Words.Document(strBaseFolder + "\\UMA One Form Application (DIFM).docx");
                if (entity.DoItForhMe && entity.DIWMServiceManagedViaMDA)
                {
                    outputType = "(DIFM & DIWM MDA)";
                    document = new Aspose.Words.Document(strBaseFolder + "\\UMA One Form Application (DIFM & DIWM MDA).docx");
                }
                else
                {
                    outputType = "(DIFM Only MDA)";
                    document = new Aspose.Words.Document(strBaseFolder + "\\UMA One Form Application (DIFM Only MDA).docx");
                }

                SetLimitedPowerOfAttorney(entity, document);

                AddModelTable(document, configuredModel);

                SetBaseDetails(entity, document, configuredModel);

                SetExclusionList(entity, document);

                SetAmmSection(entity, isAMM, document, entity.DoItForhMe);

                SetAsxSection(entity, isASX, document);

                UMABroker.ReleaseBrokerManagedComponent(iBMC);
            }

            else if (entity.DIWMServiceManagedViaMDA)
            {
                outputType = "(DIWM Only MDA)";

                document = new Aspose.Words.Document(strBaseFolder + "\\UMA One Form Application (DIWM Only MDA).docx");

                SetLimitedPowerOfAttorney(entity, document);

                SetBaseDetails(entity, document, null);

                SetAmmSection(entity, isAMM, document, entity.DoItForhMe);

                SetAsxSection(entity, isASX, document);
            }

            else if (entity.DoItWithMe && entity.DoItYourSelf)
            {
                outputType = "(DIWM)";

                document = new Aspose.Words.Document(strBaseFolder + "\\UMA One Form Application (DIWM).docx");

                SetLimitedPowerOfAttorney(entity, document);

                SetBaseDetails(entity, document, null);

                SetAmmSection(entity, isAMM, document, entity.DoItForhMe);

                SetAsxSection(entity, isASX, document);
            }

            else if (entity.DoItWithMe)
            {
                outputType = "(DIWM)";

                document = new Aspose.Words.Document(strBaseFolder + "\\UMA One Form Application (DIWM).docx");

                SetLimitedPowerOfAttorney(entity, document);

                SetBaseDetails(entity, document, null);

                SetAmmSection(entity, isAMM, document, entity.DoItForhMe);

                SetAsxSection(entity, isASX, document);
            }

            else if (entity.DoItYourSelf)
            {
                outputType = "(DIY)";

                document = new Aspose.Words.Document(strBaseFolder + "\\UMA One Form Application (DIY).docx");

                SetLimitedPowerOfAttorney(entity, document);

                SetBaseDetails(entity, document, null);

                SetAmmSection(entity, isAMM, document, entity.DoItForhMe);

                SetAsxSection(entity, isASX, document);
            }

            if (document != null)
            {
                MemoryStream ms = new MemoryStream();
                document.Save(ms, Aspose.Words.SaveFormat.Pdf);
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=" + entity.accountName + "-" + outputType + ".pdf");
                Response.Buffer = true;
                Response.Clear();
                Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);
                Response.OutputStream.Flush();
                Response.End();
            }
        }

        private void ProcessFDFRequestFlattenPDF(HttpContext context)
        {

        }

        protected void btnPrintMaqClick(object sender, EventArgs e)
        {
            MemoryStream pdfFlat = new MemoryStream();

            string strBaseFolder = Request.PhysicalApplicationPath;
            if (!strBaseFolder.EndsWith("\\"))
                strBaseFolder += "\\";
            strBaseFolder += "UMAForm\\Output\\";

            string fileName = strBaseFolder + "Macquarie CMA 3rd Authority Form.pdf";

            PdfReader pdfReader = new PdfReader(fileName);
            PdfStamper pdfStamper = new PdfStamper(pdfReader, pdfFlat);

            // create and populate a string builder with each of the
            // field names available in the subject PDF
            StringBuilder sb = new StringBuilder();
            foreach (var de in pdfReader.AcroFields.Fields)
            {
                sb.Append(de.Key.ToString() + Environment.NewLine);
            }
            AcroFields pdfFormFields = pdfStamper.AcroFields;
            pdfFormFields.SetField("CMA TPA What is your account number_2", MaqCMABSB + "  " + MaqCMAAccountNo);
            pdfFormFields.SetField("CMA TPA What is your account name_2", MaqCMAAccountName);
            pdfFormFields.SetField("Name print here_7", SignatoriesDetails1.GivenName + " " + SignatoriesDetails1.MiddleName + " " + SignatoriesDetails1.FamilyName);
            pdfFormFields.SetField("Name print here_8", SignatoriesDetails2.GivenName + " " + SignatoriesDetails2.MiddleName + " " + SignatoriesDetails2.FamilyName);


            pdfStamper.FormFlattening = false;
            pdfStamper.Writer.CloseStream = false;
            pdfStamper.Close();

            pdfFlat.WriteTo(Response.OutputStream);
            pdfFlat.Close();

            byte[] docData = pdfFlat.GetBuffer(); // get the generated PDF as raw data
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-disposition", "attachment; filename=MacquarieForm3rdParty.pdf");
            Response.BinaryWrite(docData);
            Response.End();

        }

        protected void btnPrintSummary_Click(object sender, EventArgs e)
        {
            string strBaseFolder = Request.PhysicalApplicationPath;
            if (!strBaseFolder.EndsWith("\\"))
                strBaseFolder += "\\";
            strBaseFolder += "UMAForm\\UMAForm.pdf";

            UMAFormEntity umaFormEntity = new UMAFormEntity();
            DePopulateUMAForm(umaFormEntity);
            UMAFormPDF pdf = new UMAFormPDF(umaFormEntity);
            pdf.CreatePDF(strBaseFolder, Response);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveForm();

        }

        private void SaveForm()
        {
            UMAFormEntity umaFormEntity = new UMAFormEntity();
            DePopulateUMAForm(umaFormEntity);
            umaFormEntity.modifiedByUser = User.Identity.Name;
            umaFormEntity.lastModifiedDateForm = DateTime.Now.ToString("dd/MM/yyyy");
            UMABroker.SaveOverride = true;
            IBrokerManagedComponent iBMC = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            UMAFormDS ds = new UMAFormDS();
            ds.AppNo = umaFormEntity.accountNO;
            ds.UMAFormDSOperation = UMAFormDSOperation.Add;
            ds.UMAFormEntity = umaFormEntity;
            iBMC.SetData(ds);
            UMABroker.SetComplete();
        }

        private void SaveFormAndFinalised()
        {
            UMAFormEntity umaFormEntity = new UMAFormEntity();
            DePopulateUMAForm(umaFormEntity);

            DataTable valiationTable = umaFormEntity.Validate();

            if (valiationTable.Rows.Count > 0)
            {
                ModalPopupExtenderWarningValidation.Show();
            }
            else
            {
                umaFormEntity.modifiedByUser = User.Identity.Name;
                umaFormEntity.lastModifiedDateForm = DateTime.Now.ToString("dd/MM/yyyy"); ;
                umaFormEntity.UMAFormStatus = UMAFormStatus.UnderReview;

                UMABroker.SaveOverride = true;
                IBrokerManagedComponent iBMC = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
                UMAFormDS ds = new UMAFormDS();
                ds.AppNo = umaFormEntity.accountNO;
                ds.UMAFormDSOperation = UMAFormDSOperation.Add;
                ds.UMAFormEntity = umaFormEntity;
                iBMC.SetData(ds);

                if (umaFormEntity.ClientCID != Guid.Empty)
                {
                    ds = new UMAFormDS();
                    ds.AppNo = umaFormEntity.accountNO;
                    ds.UMAFormDSOperation = UMAFormDSOperation.CreateClient;
                    iBMC.SetData(ds);
                }

                UMABroker.ReleaseBrokerManagedComponent(iBMC);
                UMABroker.SetComplete();

                Response.Redirect("~/Workflows/TempAccounts.aspx");

                Response.Redirect("~/Workflows/TempAccounts.aspx");
            }
        }

        protected void btnSaveExit_Click(object sender, EventArgs e)
        {
            SaveForm();
            Response.Redirect("~/Workflows/TempAccounts.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Workflows/TempAccounts.aspx");
        }

        protected void btnFinalise_Click(object sender, EventArgs e)
        {
            SaveFormAndFinalised();


        }
    }
}