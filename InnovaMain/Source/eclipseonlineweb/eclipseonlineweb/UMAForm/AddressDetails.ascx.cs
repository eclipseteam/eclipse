﻿using System;
using Oritax.TaxSimp.Utilities;

namespace eclipseonlineweb
{
    public partial class AddressDetails : System.Web.UI.UserControl
    {
     
        public string MialingAddressLine1
        {
            get
            {
                return this.txtAddressLine1Mail.Text;
            }
            set
            {
                this.txtAddressLine1Mail.Text = value;
            }
        }

        public string MialingAddressLine2
        {
            get
            {
                return this.txtAddressLine2Mail.Text;
            }
            set
            {
                this.txtAddressLine2Mail.Text = value;
            }
        }
        public string MialingSuburb
        {
            get
            {
                return this.txtSuburbSigAddressMail.Text;
            }
            set
            {
                this.txtSuburbSigAddressMail.Text = value;
            }
        }
        public string MialingState
        {
            get
            {
                if (C1ComboBoxStateSigAddressMail.SelectedItem != null)
                    return this.C1ComboBoxStateSigAddressMail.SelectedItem.Text;
                else
                    return AusStates.ReturnShortStateCodeFromLong(this.C1ComboBoxStateSigAddressMail.Text);
            }
            set
            {
                this.C1ComboBoxStateSigAddressMail.Text = AusStates.ReturnShortStateCodeFromLong(value);
            }
        }
        public string MialingPostCode
        {
            get
            {
                return this.txtPostcodeSigAddressMail.Text;
            }
            set
            {
                this.txtPostcodeSigAddressMail.Text = value;
            }
        }
        public string MialingCountry
        {
            get
            {
                if (C1ComboBoxCountryListSigAddressMail.SelectedItem != null)
                    return this.C1ComboBoxCountryListSigAddressMail.SelectedItem.Text;
                else
                    return this.C1ComboBoxCountryListSigAddressMail.Text;
            }
            set
            {
                this.C1ComboBoxCountryListSigAddressMail.Text = value;
            }
        }

        public string DupMialingAddressLine1
        {
            get
            {
                return this.txtAddressLine1SigAddressDulplicateMailing.Text;
            }
            set
            {
                this.txtAddressLine1SigAddressDulplicateMailing.Text = value;
            }
        }
        public string DupMialingAddressLine2
        {
            get
            {
                return this.txtAddressLine2SigAddressDulplicateMailing.Text;
            }
            set
            {
                this.txtAddressLine2SigAddressDulplicateMailing.Text = value;
            }
        }
        public string DupMialingSuburb
        {
            get
            {
                return this.txtSuburbSigAddressDulplicateMailing.Text;
            }
            set
            {
                this.txtSuburbSigAddressDulplicateMailing.Text = value;
            }
        }
        public string DupMialingState
        {
            get
            {
                if (C1ComboBoxSigAddressDulplicateMailingState.SelectedItem != null)
                    return this.C1ComboBoxSigAddressDulplicateMailingState.SelectedItem.Text;
                else
                    return AusStates.ReturnShortStateCodeFromLong(this.C1ComboBoxSigAddressDulplicateMailingState.Text);
            }
            set
            {
                this.C1ComboBoxSigAddressDulplicateMailingState.Text = AusStates.ReturnShortStateCodeFromLong(value);
            }
        }
        public string DupMialingPostCode
        {
            get
            {
                return this.txtPostCodeSigAddressDulplicateMailing.Text;
            }
            set
            {
                this.txtPostCodeSigAddressDulplicateMailing.Text = value;
            }
        }
        public string DupMialingCountry
        {
            get
            {
                if (C1ComboBoxCountryListSigAddressDulplicateMailing.SelectedItem != null)
                    return this.C1ComboBoxCountryListSigAddressDulplicateMailing.SelectedItem.Text;
                else
                    return this.C1ComboBoxCountryListSigAddressDulplicateMailing.Text;
            }
            set
            {
                this.C1ComboBoxCountryListSigAddressMail.Text = value;
            }
        }

        public string RegAddressLine1
        {
            get
            {
                return this.txtAddressLine1RegisteredAddress.Text;
            }
            set
            {
                this.txtAddressLine1RegisteredAddress.Text = value;
            }
        }
        public string RegAddressLine2
        {
            get
            {
                return this.txtAddressLine2RegisteredAddress.Text;
            }
            set
            {
                this.txtAddressLine2RegisteredAddress.Text = value;
            }
        }
        public string RegSuburb
        {
            get
            {
                return this.txtSuburbRegisteredAddress.Text;
            }
            set
            {
                this.txtSuburbRegisteredAddress.Text = value;
            }
        }
        public string RegState
        {
            get
            {
                if (C1ComboBoxRegisteredAddressState.SelectedItem != null)
                    return this.C1ComboBoxRegisteredAddressState.SelectedItem.Text;
                else
                    return AusStates.ReturnShortStateCodeFromLong(this.C1ComboBoxRegisteredAddressState.Text);
            }
            set
            {
                this.C1ComboBoxRegisteredAddressState.Text = AusStates.ReturnShortStateCodeFromLong(value);
            }
        }
        public string RegPostCode
        {
            get
            {
                return this.txtPostCodeRegisteredAddress.Text;
            }
            set
            {
                this.txtPostCodeRegisteredAddress.Text = value;
            }
        }
        public string RegCountry
        {
            get
            {
                if (C1ComboBoxRegisteredAddress.SelectedItem != null)
                    return this.C1ComboBoxRegisteredAddress.SelectedItem.Text;
                else
                    return this.C1ComboBoxRegisteredAddress.Text;
            }
            set
            {
                this.C1ComboBoxRegisteredAddress.Text = value;
            }
        }

        public string PrincipalAddressLine1
        {
            get
            {
                return this.txtAddressLine1PlaceOfBusiness.Text;
            }
            set
            {
                this.txtAddressLine1PlaceOfBusiness.Text = value;
            }
        }
        public string PrincipalAddressLine2
        {
            get
            {
                return this.txtAddressLine2PlaceOfBusiness.Text;
            }
            set
            {
                this.txtAddressLine2PlaceOfBusiness.Text = value;
            }
        }
        public string PrincipalSuburb
        {
            get
            {
                return this.txtSuburbPlaceOfBusiness.Text;
            }
            set
            {
                this.txtSuburbPlaceOfBusiness.Text = value;
            }
        }
        public string PrincipalState
        {
            get
            {
                if (C1ComboBoxPlaceOfBusinessState.SelectedItem != null)
                    return this.C1ComboBoxPlaceOfBusinessState.SelectedItem.Text;
                else
                    return AusStates.ReturnShortStateCodeFromLong(this.C1ComboBoxPlaceOfBusinessState.Text);
            }
            set
            {
                this.C1ComboBoxPlaceOfBusinessState.Text = AusStates.ReturnShortStateCodeFromLong(value);
            }
        }
        public string PrincipalPostCode
        {
            get
            {
                return this.txtPostCodePlaceOfBusiness.Text;
            }
            set
            {
                this.txtPostCodePlaceOfBusiness.Text = value;
            }
        }
        public string PrincipalCountry
        {
            get
            {
                if (C1ComboBoxPlaceOfBusinessCountry.SelectedItem != null)
                    return this.C1ComboBoxPlaceOfBusinessCountry.SelectedItem.Text;
                else
                    return this.C1ComboBoxPlaceOfBusinessCountry.Text;
            }
            set
            {
                this.C1ComboBoxPlaceOfBusinessCountry.Text = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

       
    }
}