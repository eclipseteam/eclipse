﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UMAOneFormProcess.aspx.cs"
    MasterPageFile="~/UMAFormMaster.Master" Inherits="eclipseonlineweb.UMAForm.UMAOneFormProcess" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Tabs"
    TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
    TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Dialog"
    TagPrefix="cc1" %>
<%@ Register TagName="SignatoriesDetails" TagPrefix="UMA" Src="~/UMAForm/SignatoriesDetails.ascx" %>
<%@ Register TagName="AddressDetails" TagPrefix="UMA" Src="~/UMAForm/AddressDetails.ascx" %>
<%@ Register TagPrefix="uc2" TagName="SecurityWorkFlow" Src="~/Controls/SecurityWorkFlow.ascx" %>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <wijmo:C1Tabs ID="OneFormWizardTabs" runat="server" ShowOption-Blind="true" Collapsible="true"
                Width="100%">
                <ShowOption Fade="true" />
                <Pages>
                    <wijmo:C1TabPage Wrap="true" Text="1:Account" Direction="LeftToRight">
                        <table width="100%" class="MainView">
                            <tr>
                                <td>
                                    <h2>
                                        Account Type</h2>
                                    <asp:RadioButtonList RepeatColumns="2" Width="100%" runat="server" ID="RBAccountType">
                                        <asp:ListItem Text="Individual" Value="Individual"></asp:ListItem>
                                        <asp:ListItem Text="Joint" Value="Joint"></asp:ListItem>
                                        <asp:ListItem Text="Company - Public" Value="CompanyPublic"></asp:ListItem>
                                        <asp:ListItem Text="Company - Private" Value="CompanyPrivate"></asp:ListItem>
                                        <asp:ListItem Text="Partnership" Value="Partnership"></asp:ListItem>
                                        <asp:ListItem Text="Trust - Individual Trustee" Value="TrustInd"></asp:ListItem>
                                        <asp:ListItem Text="Trust - Corporate Trustee" Value="TrustCorp"></asp:ListItem>
                                        <asp:ListItem Text="Trust - Joint Trustee" Value="TrustJoint"></asp:ListItem>
                                        <asp:ListItem Text="Self Managed Super Fund (SMSF) - Individual Trustee" Value="SMSFInd"></asp:ListItem>
                                        <asp:ListItem Text="Self Managed Super Fund (SMSF) - Corporate Trustee" Value="SMSFCorp"></asp:ListItem>
                                        <asp:ListItem Text="Self Managed Super Fund (SMSF) - Joint Trustee" Value="SMSFJoint"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    <table width="100%">
                                        <tr>
                                            <td width="20%">
                                                Account No:
                                            </td>
                                            <td width="60%">
                                                <asp:TextBox CssClass="Txt-Field" Width="100%" runat="server" ID="txtAccountNo" ReadOnly="true"
                                                    BackColor="LightGray"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%">
                                                <br />
                                                Account Name:
                                            </td>
                                            <td width="60%">
                                                <br />
                                                <asp:TextBox CssClass="Txt-Field" Width="100%" runat="server" ID="txtAccountName"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%">
                                                <br />
                                                Trustee Name/s:
                                            </td>
                                            <td width="60%">
                                                <br />
                                                <asp:TextBox CssClass="Txt-Field" Width="100%" runat="server" ID="txtTrusteeName"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table width="100%">
                                        <tr>
                                            <td width="20%">
                                                Adviser Name:
                                            </td>
                                            <td width="60%">
                                                <wijmo:C1ComboBox ID="cmbIFA" Width="100%" runat="server">
                                                </wijmo:C1ComboBox>
                                            </td>
                                            <tr>
                                                <td width="20%">
                                                    Adviser / Username:
                                                </td>
                                                <td width="60%">
                                                    <asp:TextBox CssClass="Txt-Field" Width="100%" runat="server" ID="txtUserIDAdviserID"
                                                        ReadOnly="true" BackColor="LightGray"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    Created On:
                                                </td>
                                                <td width="60%">
                                                    <asp:TextBox CssClass="Txt-Field" Width="100%" runat="server" ID="txtCreatedDate"
                                                        ReadOnly="true" BackColor="LightGray"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    Last Modified By:
                                                </td>
                                                <td width="60%">
                                                    <asp:TextBox CssClass="Txt-Field" Width="100%" runat="server" ID="txtLastModifiedUser"
                                                        ReadOnly="true" BackColor="LightGray"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    Last Modified Date:
                                                </td>
                                                <td width="60%">
                                                    <asp:TextBox CssClass="Txt-Field" Width="100%" runat="server" ID="txtLastModifiedDate"
                                                        ReadOnly="true" BackColor="LightGray"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    Application Form Status:
                                                </td>
                                                <td width="60%">
                                                    <asp:TextBox CssClass="Txt-Field" Width="100%" runat="server" ID="txtFormStatus"
                                                        ReadOnly="true" BackColor="LightGray"></asp:TextBox>
                                                </td>
                                            </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage Text="2:Fee">
                        <div class="MainView">
                            <table>
                                <tr>
                                    <td>
                                        Upfront fee (p.a. inc GST):
                                    </td>
                                    <td>
                                        <wijmo:C1InputCurrency runat="server" ID="txtUpfrontFee">
                                        </wijmo:C1InputCurrency>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="MainView">
                            <asp:RadioButton Visible="false" ID="OngoingFee" runat="server" Checked="true" GroupName="fee"
                                Text="Ongoing Fee" />
                            <asp:RadioButton Visible="false" ID="TiredFee" runat="server" Checked="true" GroupName="fee"
                                Text="Tiered Fee" />
                            <br />
                            <hr />
                            <asp:Panel runat="server" ID="pnlEnableOngoing">
                                <table>
                                    <tr>
                                        <td>
                                            Ongoing fee (p.a. inc GST) $:
                                        </td>
                                        <td>
                                            <wijmo:C1InputCurrency runat="server" ID="txtOngoingFee">
                                            </wijmo:C1InputCurrency>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <b>OR</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ongoing fee (p.a. inc GST) %:
                                        </td>
                                        <td>
                                            <wijmo:C1InputPercent runat="server" ID="txtOngoingFeePercentage">
                                            </wijmo:C1InputPercent>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <hr />
                            <b>Tiered Fee</b><br />
                            <asp:Panel runat="server" ID="pnlEnableTiered">
                                <table>
                                    <tr>
                                        <td>
                                            From:
                                        </td>
                                        <td>
                                            <wijmo:C1InputCurrency runat="server" ID="txtFromTier1">
                                            </wijmo:C1InputCurrency>
                                        </td>
                                        <td>
                                            to:
                                        </td>
                                        <td>
                                            <wijmo:C1InputCurrency runat="server" ID="txtToTier1">
                                            </wijmo:C1InputCurrency>
                                        </td>
                                        <td>
                                            =:
                                        </td>
                                        <td>
                                            <wijmo:C1InputPercent runat="server" ID="txtPercentTier1">
                                            </wijmo:C1InputPercent>
                                        </td>
                                        <td>
                                            p.a. inc GST
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            From:
                                        </td>
                                        <td>
                                            <wijmo:C1InputCurrency runat="server" ID="txtFromTier2">
                                            </wijmo:C1InputCurrency>
                                        </td>
                                        <td>
                                            to:
                                        </td>
                                        <td>
                                            <wijmo:C1InputCurrency runat="server" ID="txtToTier2">
                                            </wijmo:C1InputCurrency>
                                        </td>
                                        <td>
                                            =:
                                        </td>
                                        <td>
                                            <wijmo:C1InputPercent runat="server" ID="txtPercentTier2">
                                            </wijmo:C1InputPercent>
                                        </td>
                                        <td>
                                            p.a. inc GST
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            From:
                                        </td>
                                        <td>
                                            <wijmo:C1InputCurrency runat="server" ID="txtFromTier3">
                                            </wijmo:C1InputCurrency>
                                        </td>
                                        <td>
                                            to:
                                        </td>
                                        <td>
                                            <wijmo:C1InputCurrency runat="server" ID="txtToTier3">
                                            </wijmo:C1InputCurrency>
                                        </td>
                                        <td>
                                            =:
                                        </td>
                                        <td>
                                            <wijmo:C1InputPercent runat="server" ID="txtPercentTier3">
                                            </wijmo:C1InputPercent>
                                        </td>
                                        <td>
                                            p.a. inc GST
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            From:
                                        </td>
                                        <td>
                                            <wijmo:C1InputCurrency runat="server" ID="txtFromTier4">
                                            </wijmo:C1InputCurrency>
                                        </td>
                                        <td>
                                            to:
                                        </td>
                                        <td>
                                            <wijmo:C1InputCurrency runat="server" ID="txtToTier4">
                                            </wijmo:C1InputCurrency>
                                        </td>
                                        <td>
                                            =:
                                        </td>
                                        <td>
                                            <wijmo:C1InputPercent runat="server" ID="txtPercentTier4">
                                            </wijmo:C1InputPercent>
                                        </td>
                                        <td>
                                            p.a. inc GST
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            From:
                                        </td>
                                        <td>
                                            <wijmo:C1InputCurrency runat="server" ID="txtFromTier5">
                                            </wijmo:C1InputCurrency>
                                        </td>
                                        <td>
                                            to:
                                        </td>
                                        <td>
                                            <wijmo:C1InputCurrency runat="server" ID="txtToTier5">
                                            </wijmo:C1InputCurrency>
                                        </td>
                                        <td>
                                            =:
                                        </td>
                                        <td>
                                            <wijmo:C1InputPercent runat="server" ID="txtPercentTier5">
                                            </wijmo:C1InputPercent>
                                        </td>
                                        <td>
                                            p.a. inc GST
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </div>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage Text="3:TFN/ABN">
                        <div class="MainView">
                            The TFN and ABN must be that of the Trust / Superannuation fund / Deceased Estate.
                            (Complete all relevant sections)
                            <br />
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        TFN (Investor/Signatory 1):
                                    </td>
                                    <td>
                                        <wijmo:C1InputMask ID="txtTFNClient1" Width="400px" runat="server" Mask="000-000-000"
                                            HidePromptOnLeave="true">
                                        </wijmo:C1InputMask>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        TFN (Investor/Signatory 2):
                                    </td>
                                    <td>
                                        <wijmo:C1InputMask ID="txtTFNClient2" Width="400px" runat="server" Mask="000-000-000"
                                            HidePromptOnLeave="true">
                                        </wijmo:C1InputMask>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        TFN (Investor/Signatory 3):
                                    </td>
                                    <td>
                                        <wijmo:C1InputMask ID="txtTFNClient3" Width="400px" runat="server" Mask="000-000-000"
                                            HidePromptOnLeave="true">
                                        </wijmo:C1InputMask>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        TFN (Investor/Signatory 4):
                                    </td>
                                    <td>
                                        <wijmo:C1InputMask ID="txtTFNClient4" Width="400px" runat="server" Mask="000-000-000"
                                            HidePromptOnLeave="true">
                                        </wijmo:C1InputMask>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        TFN (Trust/Superfund):
                                    </td>
                                    <td>
                                        <wijmo:C1InputMask ID="txtTFNTrustSuperfund" Width="400px" runat="server" Mask="000-000-000"
                                            HidePromptOnLeave="true">
                                        </wijmo:C1InputMask>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ABN (Company/Superfund):
                                    </td>
                                    <td>
                                        <wijmo:C1InputMask ID="txtABN" Width="400px" runat="server" Mask="00-000-000-000"
                                            HidePromptOnLeave="true">
                                        </wijmo:C1InputMask>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ACN (Company):
                                    </td>
                                    <td>
                                        <wijmo:C1InputMask ID="txtACN" Width="400px" runat="server" Mask="000-000-000" HidePromptOnLeave="true">
                                        </wijmo:C1InputMask>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            If you don't have an ABN or withholding tax payer number, have you applied for one?
                            <asp:RadioButton ID="rbHaveAbnYes" runat="server" GroupName="fee" Text="Yes" />
                            <asp:RadioButton ID="rbHaveAbnNo" runat="server" GroupName="fee" Text="No" />
                            <br />
                            <br />
                            If you are a non-resident for tax purposes, please provide your country of residence
                            <wijmo:C1ComboBox ID="TFNABNCountryList" runat="server">
                                <Items>
                                    <wijmo:C1ComboBoxItem Value="AF" Text="Afghanistan" />
                                    <wijmo:C1ComboBoxItem Value="AL" Text="Albania" />
                                    <wijmo:C1ComboBoxItem Value="DZ" Text="Algeria" />
                                    <wijmo:C1ComboBoxItem Value="AS" Text="American Samoa" />
                                    <wijmo:C1ComboBoxItem Value="AD" Text="Andorra" />
                                    <wijmo:C1ComboBoxItem Value="AO" Text="Angola" />
                                    <wijmo:C1ComboBoxItem Value="AI" Text="Anguilla" />
                                    <wijmo:C1ComboBoxItem Value="AQ" Text="Antarctica" />
                                    <wijmo:C1ComboBoxItem Value="AG" Text="Antigua And Barbuda" />
                                    <wijmo:C1ComboBoxItem Value="AR" Text="Argentina" />
                                    <wijmo:C1ComboBoxItem Value="AM" Text="Armenia" />
                                    <wijmo:C1ComboBoxItem Value="AW" Text="Aruba" />
                                    <wijmo:C1ComboBoxItem Value="AU" Text="Australia" Selected="True" />
                                    <wijmo:C1ComboBoxItem Value="AT" Text="Austria" />
                                    <wijmo:C1ComboBoxItem Value="AZ" Text="Azerbaijan" />
                                    <wijmo:C1ComboBoxItem Value="BS" Text="Bahamas" />
                                    <wijmo:C1ComboBoxItem Value="BH" Text="Bahrain" />
                                    <wijmo:C1ComboBoxItem Value="BD" Text="Bangladesh" />
                                    <wijmo:C1ComboBoxItem Value="BB" Text="Barbados" />
                                    <wijmo:C1ComboBoxItem Value="BY" Text="Belarus" />
                                    <wijmo:C1ComboBoxItem Value="BE" Text="Belgium" />
                                    <wijmo:C1ComboBoxItem Value="BZ" Text="Belize" />
                                    <wijmo:C1ComboBoxItem Value="BJ" Text="Benin" />
                                    <wijmo:C1ComboBoxItem Value="BM" Text="Bermuda" />
                                    <wijmo:C1ComboBoxItem Value="BT" Text="Bhutan" />
                                    <wijmo:C1ComboBoxItem Value="BO" Text="Bolivia" />
                                    <wijmo:C1ComboBoxItem Value="BA" Text="Bosnia And Herzegowina" />
                                    <wijmo:C1ComboBoxItem Value="BW" Text="Botswana" />
                                    <wijmo:C1ComboBoxItem Value="BV" Text="Bouvet Island" />
                                    <wijmo:C1ComboBoxItem Value="BR" Text="Brazil" />
                                    <wijmo:C1ComboBoxItem Value="IO" Text="British Indian Ocean Territory" />
                                    <wijmo:C1ComboBoxItem Value="BN" Text="Brunei Darussalam" />
                                    <wijmo:C1ComboBoxItem Value="BG" Text="Bulgaria" />
                                    <wijmo:C1ComboBoxItem Value="BF" Text="Burkina Faso" />
                                    <wijmo:C1ComboBoxItem Value="BI" Text="Burundi" />
                                    <wijmo:C1ComboBoxItem Value="KH" Text="Cambodia" />
                                    <wijmo:C1ComboBoxItem Value="CM" Text="Cameroon" />
                                    <wijmo:C1ComboBoxItem Value="CA" Text="Canada" />
                                    <wijmo:C1ComboBoxItem Value="CV" Text="Cape Verde" />
                                    <wijmo:C1ComboBoxItem Value="KY" Text="Cayman Islands" />
                                    <wijmo:C1ComboBoxItem Value="CF" Text="Central African Republic" />
                                    <wijmo:C1ComboBoxItem Value="TD" Text="Chad" />
                                    <wijmo:C1ComboBoxItem Value="CL" Text="Chile" />
                                    <wijmo:C1ComboBoxItem Value="CN" Text="China" />
                                    <wijmo:C1ComboBoxItem Value="CX" Text="Christmas Island" />
                                    <wijmo:C1ComboBoxItem Value="CC" Text="Cocos (Keeling) Islands" />
                                    <wijmo:C1ComboBoxItem Value="CO" Text="Colombia" />
                                    <wijmo:C1ComboBoxItem Value="KM" Text="Comoros" />
                                    <wijmo:C1ComboBoxItem Value="CG" Text="Congo" />
                                    <wijmo:C1ComboBoxItem Value="CK" Text="Cook Islands" />
                                    <wijmo:C1ComboBoxItem Value="CR" Text="Costa Rica" />
                                    <wijmo:C1ComboBoxItem Value="CI" Text="Cote D'Ivoire" />
                                    <wijmo:C1ComboBoxItem Value="HR" Text="Croatia (Local Name: Hrvatska)" />
                                    <wijmo:C1ComboBoxItem Value="CU" Text="Cuba" />
                                    <wijmo:C1ComboBoxItem Value="CY" Text="Cyprus" />
                                    <wijmo:C1ComboBoxItem Value="CZ" Text="Czech Republic" />
                                    <wijmo:C1ComboBoxItem Value="DK" Text="Denmark" />
                                    <wijmo:C1ComboBoxItem Value="DJ" Text="Djibouti" />
                                    <wijmo:C1ComboBoxItem Value="DM" Text="Dominica" />
                                    <wijmo:C1ComboBoxItem Value="DO" Text="Dominican Republic" />
                                    <wijmo:C1ComboBoxItem Value="TP" Text="East Timor" />
                                    <wijmo:C1ComboBoxItem Value="EC" Text="Ecuador" />
                                    <wijmo:C1ComboBoxItem Value="EG" Text="Egypt" />
                                    <wijmo:C1ComboBoxItem Value="SV" Text="El Salvador" />
                                    <wijmo:C1ComboBoxItem Value="GQ" Text="Equatorial Guinea" />
                                    <wijmo:C1ComboBoxItem Value="ER" Text="Eritrea" />
                                    <wijmo:C1ComboBoxItem Value="EE" Text="Estonia" />
                                    <wijmo:C1ComboBoxItem Value="ET" Text="Ethiopia" />
                                    <wijmo:C1ComboBoxItem Value="FK" Text="Falkland Islands (Malvinas)" />
                                    <wijmo:C1ComboBoxItem Value="FO" Text="Faroe Islands" />
                                    <wijmo:C1ComboBoxItem Value="FJ" Text="Fiji" />
                                    <wijmo:C1ComboBoxItem Value="FI" Text="Finland" />
                                    <wijmo:C1ComboBoxItem Value="FR" Text="France" />
                                    <wijmo:C1ComboBoxItem Value="GF" Text="French Guiana" />
                                    <wijmo:C1ComboBoxItem Value="PF" Text="French Polynesia" />
                                    <wijmo:C1ComboBoxItem Value="TF" Text="French Southern Territories" />
                                    <wijmo:C1ComboBoxItem Value="GA" Text="Gabon" />
                                    <wijmo:C1ComboBoxItem Value="GM" Text="Gambia" />
                                    <wijmo:C1ComboBoxItem Value="GE" Text="Georgia" />
                                    <wijmo:C1ComboBoxItem Value="DE" Text="Germany" />
                                    <wijmo:C1ComboBoxItem Value="GH" Text="Ghana" />
                                    <wijmo:C1ComboBoxItem Value="GI" Text="Gibraltar" />
                                    <wijmo:C1ComboBoxItem Value="GR" Text="Greece" />
                                    <wijmo:C1ComboBoxItem Value="GL" Text="Greenland" />
                                    <wijmo:C1ComboBoxItem Value="GD" Text="Grenada" />
                                    <wijmo:C1ComboBoxItem Value="GP" Text="Guadeloupe" />
                                    <wijmo:C1ComboBoxItem Value="GU" Text="Guam" />
                                    <wijmo:C1ComboBoxItem Value="GT" Text="Guatemala" />
                                    <wijmo:C1ComboBoxItem Value="GN" Text="Guinea" />
                                    <wijmo:C1ComboBoxItem Value="GW" Text="Guinea-Bissau" />
                                    <wijmo:C1ComboBoxItem Value="GY" Text="Guyana" />
                                    <wijmo:C1ComboBoxItem Value="HT" Text="Haiti" />
                                    <wijmo:C1ComboBoxItem Value="HM" Text="Heard And Mc Donald Islands" />
                                    <wijmo:C1ComboBoxItem Value="VA" Text="Holy See (Vatican City State)" />
                                    <wijmo:C1ComboBoxItem Value="HN" Text="Honduras" />
                                    <wijmo:C1ComboBoxItem Value="HK" Text="Hong Kong" />
                                    <wijmo:C1ComboBoxItem Value="HU" Text="Hungary" />
                                    <wijmo:C1ComboBoxItem Value="IS" Text="Icel And" />
                                    <wijmo:C1ComboBoxItem Value="IN" Text="India" />
                                    <wijmo:C1ComboBoxItem Value="ID" Text="Indonesia" />
                                    <wijmo:C1ComboBoxItem Value="IR" Text="Iran (Islamic Republic Of)" />
                                    <wijmo:C1ComboBoxItem Value="IQ" Text="Iraq" />
                                    <wijmo:C1ComboBoxItem Value="IE" Text="Ireland" />
                                    <wijmo:C1ComboBoxItem Value="IL" Text="Israel" />
                                    <wijmo:C1ComboBoxItem Value="IT" Text="Italy" />
                                    <wijmo:C1ComboBoxItem Value="JM" Text="Jamaica" />
                                    <wijmo:C1ComboBoxItem Value="JP" Text="Japan" />
                                    <wijmo:C1ComboBoxItem Value="JO" Text="Jordan" />
                                    <wijmo:C1ComboBoxItem Value="KZ" Text="Kazakhstan" />
                                    <wijmo:C1ComboBoxItem Value="KE" Text="Kenya" />
                                    <wijmo:C1ComboBoxItem Value="KI" Text="Kiribati" />
                                    <wijmo:C1ComboBoxItem Value="KP" Text="Korea, Dem People'S Republic" />
                                    <wijmo:C1ComboBoxItem Value="KR" Text="Korea, Republic Of" />
                                    <wijmo:C1ComboBoxItem Value="KW" Text="Kuwait" />
                                    <wijmo:C1ComboBoxItem Value="KG" Text="Kyrgyzstan" />
                                    <wijmo:C1ComboBoxItem Value="LA" Text="Lao People'S Dem Republic" />
                                    <wijmo:C1ComboBoxItem Value="LV" Text="Latvia" />
                                    <wijmo:C1ComboBoxItem Value="LB" Text="Lebanon" />
                                    <wijmo:C1ComboBoxItem Value="LS" Text="Lesotho" />
                                    <wijmo:C1ComboBoxItem Value="LR" Text="Liberia" />
                                    <wijmo:C1ComboBoxItem Value="LY" Text="Libyan Arab Jamahiriya" />
                                    <wijmo:C1ComboBoxItem Value="LI" Text="Liechtenstein" />
                                    <wijmo:C1ComboBoxItem Value="LT" Text="Lithuania" />
                                    <wijmo:C1ComboBoxItem Value="LU" Text="Luxembourg" />
                                    <wijmo:C1ComboBoxItem Value="MO" Text="Macau" />
                                    <wijmo:C1ComboBoxItem Value="MK" Text="Macedonia" />
                                    <wijmo:C1ComboBoxItem Value="MG" Text="Madagascar" />
                                    <wijmo:C1ComboBoxItem Value="MW" Text="Malawi" />
                                    <wijmo:C1ComboBoxItem Value="MY" Text="Malaysia" />
                                    <wijmo:C1ComboBoxItem Value="MV" Text="Maldives" />
                                    <wijmo:C1ComboBoxItem Value="ML" Text="Mali" />
                                    <wijmo:C1ComboBoxItem Value="MT" Text="Malta" />
                                    <wijmo:C1ComboBoxItem Value="MH" Text="Marshall Islands" />
                                    <wijmo:C1ComboBoxItem Value="MQ" Text="Martinique" />
                                    <wijmo:C1ComboBoxItem Value="MR" Text="Mauritania" />
                                    <wijmo:C1ComboBoxItem Value="MU" Text="Mauritius" />
                                    <wijmo:C1ComboBoxItem Value="YT" Text="Mayotte" />
                                    <wijmo:C1ComboBoxItem Value="MX" Text="Mexico" />
                                    <wijmo:C1ComboBoxItem Value="FM" Text="Micronesia, Federated States" />
                                    <wijmo:C1ComboBoxItem Value="MD" Text="Moldova, Republic Of" />
                                    <wijmo:C1ComboBoxItem Value="MC" Text="Monaco" />
                                    <wijmo:C1ComboBoxItem Value="MN" Text="Mongolia" />
                                    <wijmo:C1ComboBoxItem Value="MS" Text="Montserrat" />
                                    <wijmo:C1ComboBoxItem Value="MA" Text="Morocco" />
                                    <wijmo:C1ComboBoxItem Value="MZ" Text="Mozambique" />
                                    <wijmo:C1ComboBoxItem Value="MM" Text="Myanmar" />
                                    <wijmo:C1ComboBoxItem Value="NA" Text="Namibia" />
                                    <wijmo:C1ComboBoxItem Value="NR" Text="Nauru" />
                                    <wijmo:C1ComboBoxItem Value="NP" Text="Nepal" />
                                    <wijmo:C1ComboBoxItem Value="NL" Text="Netherlands" />
                                    <wijmo:C1ComboBoxItem Value="AN" Text="Netherlands Ant Illes" />
                                    <wijmo:C1ComboBoxItem Value="NC" Text="New Caledonia" />
                                    <wijmo:C1ComboBoxItem Value="NZ" Text="New Zealand" />
                                    <wijmo:C1ComboBoxItem Value="NI" Text="Nicaragua" />
                                    <wijmo:C1ComboBoxItem Value="NE" Text="Niger" />
                                    <wijmo:C1ComboBoxItem Value="NG" Text="Nigeria" />
                                    <wijmo:C1ComboBoxItem Value="NU" Text="Niue" />
                                    <wijmo:C1ComboBoxItem Value="NF" Text="Norfolk Island" />
                                    <wijmo:C1ComboBoxItem Value="MP" Text="Northern Mariana Islands" />
                                    <wijmo:C1ComboBoxItem Value="NO" Text="Norway" />
                                    <wijmo:C1ComboBoxItem Value="OM" Text="Oman" />
                                    <wijmo:C1ComboBoxItem Value="PK" Text="Pakistan" />
                                    <wijmo:C1ComboBoxItem Value="PW" Text="Palau" />
                                    <wijmo:C1ComboBoxItem Value="PA" Text="Panama" />
                                    <wijmo:C1ComboBoxItem Value="PG" Text="Papua New Guinea" />
                                    <wijmo:C1ComboBoxItem Value="PY" Text="Paraguay" />
                                    <wijmo:C1ComboBoxItem Value="PE" Text="Peru" />
                                    <wijmo:C1ComboBoxItem Value="PH" Text="Philippines" />
                                    <wijmo:C1ComboBoxItem Value="PN" Text="Pitcairn" />
                                    <wijmo:C1ComboBoxItem Value="PL" Text="Poland" />
                                    <wijmo:C1ComboBoxItem Value="PT" Text="Portugal" />
                                    <wijmo:C1ComboBoxItem Value="PR" Text="Puerto Rico" />
                                    <wijmo:C1ComboBoxItem Value="QA" Text="Qatar" />
                                    <wijmo:C1ComboBoxItem Value="RE" Text="Reunion" />
                                    <wijmo:C1ComboBoxItem Value="RO" Text="Romania" />
                                    <wijmo:C1ComboBoxItem Value="RU" Text="Russian Federation" />
                                    <wijmo:C1ComboBoxItem Value="RW" Text="Rwanda" />
                                    <wijmo:C1ComboBoxItem Value="KN" Text="Saint K Itts And Nevis" />
                                    <wijmo:C1ComboBoxItem Value="LC" Text="Saint Lucia" />
                                    <wijmo:C1ComboBoxItem Value="VC" Text="Saint Vincent, The Grenadines" />
                                    <wijmo:C1ComboBoxItem Value="WS" Text="Samoa" />
                                    <wijmo:C1ComboBoxItem Value="SM" Text="San Marino" />
                                    <wijmo:C1ComboBoxItem Value="ST" Text="Sao Tome And Principe" />
                                    <wijmo:C1ComboBoxItem Value="SA" Text="Saudi Arabia" />
                                    <wijmo:C1ComboBoxItem Value="SN" Text="Senegal" />
                                    <wijmo:C1ComboBoxItem Value="SC" Text="Seychelles" />
                                    <wijmo:C1ComboBoxItem Value="SL" Text="Sierra Leone" />
                                    <wijmo:C1ComboBoxItem Value="SG" Text="Singapore" />
                                    <wijmo:C1ComboBoxItem Value="SK" Text="Slovakia (Slovak Republic)" />
                                    <wijmo:C1ComboBoxItem Value="SI" Text="Slovenia" />
                                    <wijmo:C1ComboBoxItem Value="SB" Text="Solomon Islands" />
                                    <wijmo:C1ComboBoxItem Value="SO" Text="Somalia" />
                                    <wijmo:C1ComboBoxItem Value="ZA" Text="South Africa" />
                                    <wijmo:C1ComboBoxItem Value="GS" Text="South Georgia , S Sandwich Is." />
                                    <wijmo:C1ComboBoxItem Value="ES" Text="Spain" />
                                    <wijmo:C1ComboBoxItem Value="LK" Text="Sri Lanka" />
                                    <wijmo:C1ComboBoxItem Value="SH" Text="St. Helena" />
                                    <wijmo:C1ComboBoxItem Value="PM" Text="St. Pierre And Miquelon" />
                                    <wijmo:C1ComboBoxItem Value="SD" Text="Sudan" />
                                    <wijmo:C1ComboBoxItem Value="SR" Text="Suriname" />
                                    <wijmo:C1ComboBoxItem Value="SJ" Text="Svalbard, Jan Mayen Islands" />
                                    <wijmo:C1ComboBoxItem Value="SZ" Text="Sw Aziland" />
                                    <wijmo:C1ComboBoxItem Value="SE" Text="Sweden" />
                                    <wijmo:C1ComboBoxItem Value="CH" Text="Switzerland" />
                                    <wijmo:C1ComboBoxItem Value="SY" Text="Syrian Arab Republic" />
                                    <wijmo:C1ComboBoxItem Value="TW" Text="Taiwan" />
                                    <wijmo:C1ComboBoxItem Value="TJ" Text="Tajikistan" />
                                    <wijmo:C1ComboBoxItem Value="TZ" Text="Tanzania, United Republic Of" />
                                    <wijmo:C1ComboBoxItem Value="TH" Text="Thailand" />
                                    <wijmo:C1ComboBoxItem Value="TG" Text="Togo" />
                                    <wijmo:C1ComboBoxItem Value="TK" Text="Tokelau" />
                                    <wijmo:C1ComboBoxItem Value="TO" Text="Tonga" />
                                    <wijmo:C1ComboBoxItem Value="TT" Text="Trinidad And Tobago" />
                                    <wijmo:C1ComboBoxItem Value="TN" Text="Tunisia" />
                                    <wijmo:C1ComboBoxItem Value="TR" Text="Turkey" />
                                    <wijmo:C1ComboBoxItem Value="TM" Text="Turkmenistan" />
                                    <wijmo:C1ComboBoxItem Value="TC" Text="Turks And Caicos Islands" />
                                    <wijmo:C1ComboBoxItem Value="TV" Text="Tuvalu" />
                                    <wijmo:C1ComboBoxItem Value="UG" Text="Uganda" />
                                    <wijmo:C1ComboBoxItem Value="UA" Text="Ukraine" />
                                    <wijmo:C1ComboBoxItem Value="AE" Text="United Arab Emirates" />
                                    <wijmo:C1ComboBoxItem Value="GB" Text="United Kingdom" />
                                    <wijmo:C1ComboBoxItem Value="US" Text="United States" />
                                    <wijmo:C1ComboBoxItem Value="UM" Text="United States Minor Is." />
                                    <wijmo:C1ComboBoxItem Value="UY" Text="Uruguay" />
                                    <wijmo:C1ComboBoxItem Value="UZ" Text="Uzbekistan" />
                                    <wijmo:C1ComboBoxItem Value="VU" Text="Vanuatu" />
                                    <wijmo:C1ComboBoxItem Value="VE" Text="Venezuela" />
                                    <wijmo:C1ComboBoxItem Value="VN" Text="Viet Nam" />
                                    <wijmo:C1ComboBoxItem Value="VG" Text="Virgin Islands (British)" />
                                    <wijmo:C1ComboBoxItem Value="VI" Text="Virgin Islands (U.S.)" />
                                    <wijmo:C1ComboBoxItem Value="WF" Text="Wallis And Futuna Islands" />
                                    <wijmo:C1ComboBoxItem Value="EH" Text="Western Sahara" />
                                    <wijmo:C1ComboBoxItem Value="YE" Text="Yemen" />
                                    <wijmo:C1ComboBoxItem Value="YU" Text="Yugoslavia" />
                                    <wijmo:C1ComboBoxItem Value="ZR" Text="Zaire" />
                                    <wijmo:C1ComboBoxItem Value="ZM" Text="Zambia" />
                                    <wijmo:C1ComboBoxItem Value="ZW" Text="Zimbabwe" />
                                </Items>
                            </wijmo:C1ComboBox>
                        </div>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage Text="4:Authority to Act">
                        <div class="MainView">
                            <h2>
                                Authority to Act</h2>
                            <br />
                            <a style="text-decoration: underline" onclick="$('#<%:AuthorityToActDialog.ClientID%>').c1dialog('open')">
                                Please click this link to read about e-Clipse Online and Australian Money Market
                                (AMM) Authority to Act Details</a>
                            <br />
                            <br />
                            Client wishes to grant a Limited Power of Attorney to:
                            <br />
                            <br />
                            1.
                            <asp:CheckBox Text="e-clipse Online" runat="server" ID="chkSelectLimitedPowerAttorneyEclipse" /><br />
                            2.
                            <asp:CheckBox Text="Australian Money Market (AMM)" runat="server" ID="chkLimitedPowerAttorneyAMM" />
                            <asp:CheckBox Visible="false" Text="Macquarie CMA" runat="server" ID="chkLimitedPowerAttorneyMacBankCMA" /><br />
                            <br />
                            <b>* If the Client DOES NOT wish to grant a Limited Power of Attorney, DO NOT SELECT
                                ANYTHING ABOVE.<br />
                                The client will be required to complete and sign all relevant account opening forms
                                to establish their e-Clipse UMA Account</b>
                            <h2>
                                Investor Status</h2>
                            <asp:RadioButton GroupName="InvestorStatus" Text=" Wholesale Investor" runat="server"
                                ID="rbWholesaleInvestor" /><br />
                            <asp:RadioButton GroupName="InvestorStatus" Text="" runat="server" ID="rbSophisticatedInvestor" />
                            <a style="text-decoration: underline" onclick="$('#<%:SophisticatedInvestorDialog.ClientID%>').c1dialog('open')">
                                Sophisticated Investor</a><asp:CheckBox Text="Certificate from Accountant is attached"
                                    runat="server" ID="chkCertificateFromAccountantAttached" /><br />
                            <asp:RadioButton GroupName="InvestorStatus" Text=" Professional Investor" runat="server"
                                ID="rbProfessionalInvestor" /><br />
                            <asp:RadioButton GroupName="InvestorStatus" Text=" Retail Investor" runat="server"
                                ID="rbRetailInvestor" />
                            <br />
                            <br />
                            <br />
                        </div>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage Text="5:DIY/DIWM">
                        <div class="MainView">
                            <h2>
                                Services</h2>
                            <p>
                                Indicate the UMA Service and Accounts you require.</p>
                            <table>
                                <tr>
                                    <td>
                                        <asp:CheckBox Font-Bold="true" Text="DO IT YOURSELF (DIY)" runat="server" ID="chkDIYOption" />
                                        Estimated Amount to be invested $
                                    </td>
                                    <td>
                                        <wijmo:C1InputCurrency runat="server" ID="txtDIYInvestment">
                                        </wijmo:C1InputCurrency>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            1.
                            <asp:CheckBox Text="BWA CMA (for Bank Account)" runat="server" ID="chkDIYOptionBWA" /><br />
                            2.
                            <asp:CheckBox Text="Desktop Broker (for HIN)" runat="server" ID="chkDIYOptionASX" /><br />
                            3.
                            <asp:CheckBox Text="FIIG (for TDs & cash)" runat="server" ID="chkDIYOptionFIIG" /><br />
                            4.
                            <asp:CheckBox Text="Australian Money Market (for TDs & cash)" runat="server" ID="chkDIYOptionAMMM" /><br />
                            <br />
                            <hr />
                            <table>
                                <tr>
                                    <td>
                                        <asp:CheckBox Font-Bold="true" Text="DO IT WITH ME (DIWM)" runat="server" ID="chkDIWNOption" />
                                        Estimated Amount to be invested $
                                    </td>
                                    <td>
                                        <wijmo:C1InputCurrency runat="server" ID="txtDIWMInvestment">
                                        </wijmo:C1InputCurrency>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <br />
                            Specify which Accounts you need opened
                            <br />
                            1.
                            <asp:CheckBox Text="BWA CMA (for Bank Account)" runat="server" ID="chkDIWNOptionBWA" /><br />
                            2.
                            <asp:CheckBox Text="Desktop Broker (for HIN)" runat="server" ID="chkDIWNOptionASX" /><br />
                            3.
                            <asp:CheckBox Text="FIIG (for TDs & cash)" runat="server" ID="chkDIWNOptionFIIG" /><br />
                            4.
                            <asp:CheckBox Text="Australian Money Market (for TDs & cash)" runat="server" ID="chkDIWNOptionAMMM" /><br />
                            5.
                            <asp:CheckBox Text="State Street (for Innova)" runat="server" ID="chkDIWNOptionStateStreet" />
                            <br />
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <b>Add Preferred Funds</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <wijmo:C1ComboBox ID="C1ComboBoxStateStreetSelection" Width="600px" runat="server">
                                            <Items>
                                                <wijmo:C1ComboBoxItem Text="IV003-Innova Australian Share Fund (Active)-($AUD)" Value="IV003" />
                                                <wijmo:C1ComboBoxItem Text="IV010-Innova Australian Share Fund (Passive)-($AUD)"
                                                    Value="IV010" />
                                                <wijmo:C1ComboBoxItem Text="IV001-Innova Global Share Fund (Active)-($AUD)" Value="IV001" />
                                                <wijmo:C1ComboBoxItem Text="IV008-Innova Global Share Fund (Passive)-($AUD)" Value="IV008" />
                                                <wijmo:C1ComboBoxItem Text="IV002-Innova Fixed Income Fund (Active)-($AUD)" Value="IV002" />
                                                <wijmo:C1ComboBoxItem Text="IV009-Innova Fixed Income Fund (Passive)-($AUD)" Value="IV009" />
                                                <wijmo:C1ComboBoxItem Text="IV006-Innova Alternatives Fund (Liquid)-($AUD)" Value="IV006" />
                                                <wijmo:C1ComboBoxItem Text="IV018-Innova Alternatives Fund (Illiquid)-($AUD)" Value="IV018" />
                                                <wijmo:C1ComboBoxItem Text="IV005-Innova Real Assets Fund (Active)-($AUD)" Value="IV005" />
                                                <wijmo:C1ComboBoxItem Text="IV007-Innova Cash Fund (Active)-(AUD)" Value="IV007" />
                                                <wijmo:C1ComboBoxItem Text="IV013-Innova Australian Share Fund (Active)-(&#163;GBP)"
                                                    Value="IV013" />
                                                <wijmo:C1ComboBoxItem Text="IV011-Innova Global Share Fund (Active)-(&#163;GBP)"
                                                    Value="IV011" />
                                                <wijmo:C1ComboBoxItem Text="IV012-Innova Fixed Income Fund (Active)-(&#163;GBP)"
                                                    Value="IV012" />
                                                <wijmo:C1ComboBoxItem Text="IV016-Innova Alternatives Fund (Liquid)-(&#163;GBP)"
                                                    Value="IV016" />
                                                <wijmo:C1ComboBoxItem Text="IV015-Innova Real Assets Fund (Active)-(&#163;GBP)" Value="IV015" />
                                                <wijmo:C1ComboBoxItem Text="IV017-Innova Cash Fund (Active)-(&#163;GBP)" Value="IV017" />
                                            </Items>
                                        </wijmo:C1ComboBox>
                                    </td>
                                    <td>
                                        <asp:Button Text="ADD" runat="server" ID="btnAddStateStreetPref" OnClick="btnAddFundsToStateStreetList_Click" />
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <wijmo:C1GridView ShowFilter="false" RowStyle-Font-Size="Small" FilterStyle-Font-Size="Small"
                                HeaderStyle-Font-Size="Small" EnableTheming="true" Width="600px" ID="StateStreetList"
                                CallbackSettings-Action="None" runat="server" AutogenerateColumns="False" ClientSelectionMode="None"
                                CallbackSettings-Mode="Full">
                                <SelectedRowStyle />
                                <Columns>
                                    <wijmo:C1BoundField Width="20%" DataField="Code" HeaderText="Code">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField Width="60%" DataField="Description" HeaderText="Description">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </wijmo:C1BoundField>
                                    <wijmo:C1CommandField Width="20%" DeleteText="Remove" ShowDeleteButton="true">
                                    </wijmo:C1CommandField>
                                </Columns>
                            </wijmo:C1GridView>
                            <br />
                                <asp:CheckBox Text=" Client wishes to have deposit products in DIWM service managed via MDA" runat="server" ID="chkServiceManagedViaMDA" />
                            <br />
                        </div>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage Text="6:DIFM">
                        <div class="MainView">
                            <h2>
                                Services</h2>
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <asp:CheckBox OnCheckedChanged="SetDIFMConfiguration" AutoPostBack="true" Font-Bold="true"
                                            Text="DO IT FOR ME (DIFM)" runat="server" ID="chkDIFMOption" />
                                        Estimated Amount to be invested $
                                    </td>
                                    <td>
                                        <wijmo:C1InputCurrency runat="server" ID="txtDIFMInvestment">
                                        </wijmo:C1InputCurrency>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <br />
                            Please select the Investment Program
                            <br />
                            <wijmo:C1ComboBox ID="C1ComboBoxInvestmentProgram" ViewStateMode="Enabled" runat="server"
                                Width="400px">
                            </wijmo:C1ComboBox>
                            <br />
                            1. Investment Preferences
                            <asp:CheckBox Text="" Visible="false" runat="server" ID="chkNominateInvestmentPreference" /><a
                                style="text-decoration: underline"></a><br />
                            <p>
                                The Investment Preferences that have been discussed with Your adviser are:</p>
                            <ul>
                                <li>Handling of unmanaged investments:
                                    <ul>
                                        <li>
                                            <asp:CheckBox ID="chkSellTheHolding" Checked="true" runat="server" Text="Sell the holding" /></li>
                                    </ul>
                                </li>
                                <li>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chkMinBalance" Checked="true" runat="server" />The minimum balance
                                                per investment in a rebalance is $500
                                            </td>
                                            <td>
                                                <wijmo:C1InputCurrency Visible="false" Value="500" runat="server" ID="txtMinBalance">
                                                </wijmo:C1InputCurrency>
                                            </td>
                                            <td>
                                                <b>OR</b> 0.00%
                                            </td>
                                            <td>
                                                <wijmo:C1InputPercent Visible="false" runat="server" Value="0" ID="txtMinBalancePercentage">
                                                </wijmo:C1InputPercent>
                                            </td>
                                            <td>
                                                *Note: Default is $500
                                            </td>
                                        </tr>
                                    </table>
                                    <li>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="CheckBox1" Checked="true" runat="server" />
                                                    The minimum trade amount in a rebalance $2500
                                                </td>
                                                <td>
                                                    <wijmo:C1InputCurrency Visible="false" Value="2500" runat="server" ID="txtMinTrade">
                                                    </wijmo:C1InputCurrency>
                                                </td>
                                                <td>
                                                    <b>OR</b> 0.00%
                                                </td>
                                                <td>
                                                    <wijmo:C1InputPercent Visible="false" runat="server" Value="0" ID="txtMinTradePercentage">
                                                    </wijmo:C1InputPercent>
                                                </td>
                                                <td>
                                                    *Note: Default is $2500
                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                    <li>Handling of investments that are not purchased due to a hold or sell rating in the
                                        Portfolio:<br />
                                        <asp:RadioButton ID="chkAssignedAllocatedValueHoldSell" Text="Assign allocated value to cash (Default)"
                                            GroupName="HandlingInvestmentNotPurchased" runat="server" Checked="true" ViewStateMode="Enabled" /><br />
                                        <asp:RadioButton ID="chkDistributedAllocatedValueHoldSell" ViewStateMode="Enabled"
                                            Text="Distribute allocated value to other investments" GroupName="HandlingInvestmentNotPurchased"
                                            runat="server" Checked="false" />
                                    </li>
                                    <li>Handling of investments that are not purchased due to an exclusion:<br />
                                        <asp:RadioButton ID="chkAssignedAllocatedValueExclusion" Text="Assign allocated value to cash (Default)"
                                            GroupName="HandlingInvestmentExclusion" runat="server" ViewStateMode="Enabled"
                                            Checked="true" /><br />
                                        <asp:RadioButton ID="chkDistributedAllocatedValueExclusion" ViewStateMode="Enabled"
                                            Text="Distribute allocated value to other investments" GroupName="HandlingInvestmentExclusion"
                                            runat="server" Checked="false" />
                                    </li>
                                    <li>Handling of investments that are not purchased due to a minimum trade or minimum
                                        holding constraint:<br />
                                        <asp:RadioButton ID="chkAssignedAllocatedValueMinTradeHolding" ViewStateMode="Enabled"
                                            Text="Assign allocated value to cash (Default)" GroupName="HandlingInvestmentMinTrade"
                                            runat="server" Checked="true" /><br />
                                        <asp:RadioButton ID="chkDistributedAllocatedValueMinTradeHolding" ViewStateMode="Enabled"
                                            Text="Distribute allocated value to other investments" GroupName="HandlingInvestmentMinTrade"
                                            runat="server" Checked="false" />
                                    </li>
                            </ul>
                            <br />
                            2. Exclusions allow You to prevent selected investments from being purchased or
                            sold on Your behalf by the MDA operator. Listed below are the exclusions that You
                            have agreed with Your adviser.<br />
                            <br />
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <b>Add Funds / Securities to Exclusion list:</b>
                                    </td>
                                </tr>
                                <tr>
                                    <%-- <td>
                             <wijmo:C1ComboBox  ID="C1ComboBoxFundsOrSec" o OnSelectedIndexChanged="C1ComboBoxFundsOrSec_SelectedIndexChanged" runat="server" AutoPostBack="true" >
                             <Items>
                             <wijmo:C1ComboBoxItem Text="Fund"/>
                             <wijmo:C1ComboBoxItem Text="Securities"/>
                             </Items>
                             </wijmo:C1ComboBox>
                        </td>--%>
                                    <td>
                                        <wijmo:C1ComboBox ID="C1ComboBoxFundsEclusion" Width="600px" DataTextField="Description"
                                            DataValueField="ID" runat="server">
                                        </wijmo:C1ComboBox>
                                    </td>
                                    <td>
                                        <wijmo:C1ComboBox ID="C1ComboBoxFundsEclusionAction" runat="server">
                                            <Items>
                                                <wijmo:C1ComboBoxItem Text="Don't Sell" />
                                                <wijmo:C1ComboBoxItem Text="Don't Buy" />
                                                <wijmo:C1ComboBoxItem Text="Don't Sell or Buy" Selected="true" />
                                            </Items>
                                        </wijmo:C1ComboBox>
                                    </td>
                                    <td>
                                        <asp:Button Text="ADD" runat="server" ID="btnAddFundsToExclusionList" OnClick="btnAddFundsToExclusionList_Click" />
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <wijmo:C1GridView ShowFilter="false" RowStyle-Font-Size="Small" FilterStyle-Font-Size="Small"
                                HeaderStyle-Font-Size="Small" EnableTheming="true" Width="600px" ID="ExcusionList"
                                CallbackSettings-Action="None" runat="server" AutogenerateColumns="False" ClientSelectionMode="None"
                                CallbackSettings-Mode="Full">
                                <SelectedRowStyle />
                                <Columns>
                                    <wijmo:C1BoundField Visible="false" DataField="ID" HeaderText="ID">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField Visible="false" DataField="Code" HeaderText="Code">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField Width="50%" DataField="Description" HeaderText="Description">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField Width="30%" DataField="InvestmentAction" HeaderText="Investment Action">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </wijmo:C1BoundField>
                                    <wijmo:C1CommandField Width="20%" DeleteText="Remove" ShowDeleteButton="true">
                                    </wijmo:C1CommandField>
                                </Columns>
                            </wijmo:C1GridView>
                            <br />
                            3.
                            <asp:CheckBox Text="BWA CMA (for Bank Account)" runat="server" ID="chkDIFMOptionBWA" /><br />
                            4.
                            <asp:CheckBox Text="Desktop Broker (for HIN)" runat="server" ID="chkDIFMOptionASX" /><br />
                            5.
                            <asp:CheckBox Text="State Street (for Innova & P2)" runat="server" ID="chkDIFMOptionSS" /><br />
                        </div>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage Text="7:Platforms">
                        <div class="MainView">
                            <h2>
                                Other Platforms</h2>
                            <asp:Panel runat="server" ID="pnlLinkedAccounts">
                                <table>
                                    <tr>
                                        <td>
                                            Name of the platform:
                                        </td>
                                        <td>
                                            <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtPTName1" Text="BT Wrap Platform"
                                                ReadOnly="true"></asp:TextBox><br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Investor No. / Account No.:
                                        </td>
                                        <td>
                                            <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtInvestor1"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Adviser No. / Code:
                                        </td>
                                        <td>
                                            <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtAdviser1"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Provider Name:
                                        </td>
                                        <td>
                                            <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtProvider1"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            Name of the platform:
                                        </td>
                                        <td>
                                            <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtPlatform2" Text="e-Clipse Super"
                                                ReadOnly="true"></asp:TextBox><br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Investor No. / Account No.:
                                        </td>
                                        <td>
                                            <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtInvestor2"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Adviser No. / Code:
                                        </td>
                                        <td>
                                            <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtAdviser2"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Provider Name:
                                        </td>
                                        <td>
                                            <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtProvider2"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <h2>
                                    Macquarie CMA</h2>
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            Do you want to link an existing Macquarie CMA instead of opening a Bankwest CMA?
                                        </td>
                                        <td>
                                            <asp:RadioButtonList RepeatDirection="Horizontal" runat="server" ID="rbMacquarieBankwestLink">
                                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                            </asp:RadioButtonList>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            If Yes to which service (ie. DIY, DIWM) :
                                        </td>
                                        <td>
                                            <asp:RadioButtonList RepeatDirection="Horizontal" runat="server" ID="rbMacquarieDIYDIWM">
                                                <asp:ListItem Text="DIY" Value="DIY"></asp:ListItem>
                                                <asp:ListItem Text="DIWM" Value="DIY"></asp:ListItem>
                                            </asp:RadioButtonList>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            Daily data feeds will be obtained and monthly fees will be deducted from this account.
                                            <b>Please note you must not tick the "BWA CMA" check box in "5: DIY/DIWM" for that service</b><br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            BSB:
                                        </td>
                                        <td>
                                            <wijmo:C1InputMask ID="InputBSB" Width="400px" Height="15px" runat="server" Mask="000-000"
                                                HidePromptOnLeave="true">
                                            </wijmo:C1InputMask>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Account No:
                                        </td>
                                        <td>
                                            <wijmo:C1InputMask ID="InoutAccNo" Width="400px" Height="15px" runat="server" Mask="000000000"
                                                HidePromptOnLeave="true">
                                            </wijmo:C1InputMask>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Account Name:
                                        </td>
                                        <td>
                                            <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtAccountNameMaq" Width="400px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </div>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage Text="8:SIG1">
                        <div class="MainView">
                            <h2>
                                Investor / Signatories / Trustree Details 1</h2>
                            <br />
                            <br />
                            <UMA:SignatoriesDetails runat="server" ID="SignatoriesDetails1" />
                        </div>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage Text="9:SIG2">
                        <div class="MainView">
                            <h2>
                                Investor / Signatories / Trustree Details 2</h2>
                            <asp:CheckBox ID="Sig2Required" Text=" ADD Signatory 2" runat="server" OnCheckedChanged="EnableSig2"
                                AutoPostBack="true" />
                            <br />
                            <br />
                            <asp:Panel runat="server" ID="pnlSig2" Enabled="false">
                                <UMA:SignatoriesDetails runat="server" ID="SignatoriesDetails2" />
                            </asp:Panel>
                        </div>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage Text="10:SIG3">
                        <div class="MainView">
                            <h2>
                                Investor / Signatories / Trustree Details 3</h2>
                            <asp:CheckBox ID="Sig3Required" Text=" ADD Signatory 3" runat="server" OnCheckedChanged="EnableSig3"
                                AutoPostBack="true" />
                            <br />
                            <br />
                            <asp:Panel runat="server" ID="pnlSig3" Enabled="false">
                                <UMA:SignatoriesDetails runat="server" ID="SignatoriesDetails3" />
                            </asp:Panel>
                        </div>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage Text="11:SIG4">
                        <div class="MainView">
                            <h2>
                                Investor / Signatories / Trustree Details 4</h2>
                            <asp:CheckBox ID="Sig4Required" Text=" ADD Signatory 4" runat="server" OnCheckedChanged="EnableSig4"
                                AutoPostBack="true" />
                            <br />
                            <br />
                            <asp:Panel runat="server" ID="pnlSig4" Enabled="false">
                                <UMA:SignatoriesDetails runat="server" ID="SignatoriesDetails4" />
                            </asp:Panel>
                        </div>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage Text="12:BWA">
                        <div class="MainView">
                            <h1>
                                DIY - BWA CMA OPTIONS</h1>
                            <p>
                                The following selections will apply to my/our DIY BWA CMA</p>
                            <h2>
                                DIY BWA Access Facilities</h2>
                            <p>
                                Please tick ( √ ) the Access Facilities required:</p>
                            1.
                            <asp:CheckBox Text="Phone Access" runat="server" ID="chkPhoneAccessDIY" /><br />
                            2.
                            <asp:CheckBox Text="Online Access" runat="server" ID="chkOnlineAccessDIY" /><br />
                            3.
                            <asp:CheckBox Text="Debit Card" runat="server" ID="chkDebitCardDIY" /><br />
                            4.
                            <asp:CheckBox Text="Cheque Book (25 per book)" runat="server" ID="chkCequeBook25DIY" /><br />
                            5.
                            <asp:CheckBox Text="Deposit Book" runat="server" ID="chkDepositBookDIY" /><br />
                            <br />
                            <h2>
                                DIY BWA Manner Of Operation</h2>
                            <p>
                                Please select how you wish to operate your CMA by ticking ( √ ) one of the following:</p>
                            <asp:RadioButtonList RepeatDirection="Horizontal" runat="server" ID="rbMannerOfOperationOptionsDIY">
                                <asp:ListItem Text="Any one of us to sign" Value="Any one of us to sign"></asp:ListItem>
                                <asp:ListItem Text="Any two of us to sign" Value="Any two of us to sign"></asp:ListItem>
                                <asp:ListItem Text="All of us to sign" Value="All of us to sign"></asp:ListItem>
                            </asp:RadioButtonList>
                            <p>
                                Note:<br />
                                <i>1. Where you do not elect a manner of operation, BWA will default to 'All of us to
                                    sign'</i><br />
                                <i>2. Phone Access, Online Access and a Debit Card cannot be selected unless the
                                manner of operation is 'Any one of us to sign'.</p>
                            </i>
                            <br />
                            <hr />
                            <h1>
                                DIWM - BWA CMA OPTIONS</h1>
                            <p>
                                The following selections will apply to my/our DIY BWA CMA</p>
                            <h2>
                                DIWM BWA Access Facilities</h2>
                            <p>
                                Please tick ( √ ) the Access Facilities required:</p>
                            1.
                            <asp:CheckBox Text="Phone Access" runat="server" ID="chkPhoneAccessDIWM" /><br />
                            2.
                            <asp:CheckBox Text="Online Access" runat="server" ID="chkOnlineAccessDIWM" /><br />
                            3.
                            <asp:CheckBox Text="Debit Card" runat="server" ID="chkDebitCardDIWM" /><br />
                            4.
                            <asp:CheckBox Text="Cheque Book (25 per book)" runat="server" ID="chkCequeBook25DIWM" /><br />
                            5.
                            <asp:CheckBox Text="Deposit Book" runat="server" ID="chkDepositBookDIWM" /><br />
                            <br />
                            <h2>
                                DIWM BWA Manner Of Operation</h2>
                            <p>
                                Please select how you wish to operate your CMA by ticking ( √ ) one of the following:</p>
                            <asp:RadioButtonList RepeatDirection="Horizontal" runat="server" ID="rbMannerOfOperationOptionsDIWM">
                                <asp:ListItem Text="Any one of us to sign" Value="Any one of us to sign"></asp:ListItem>
                                <asp:ListItem Text="Any two of us to sign" Value="Any two of us to sign"></asp:ListItem>
                                <asp:ListItem Text="All of us to sign" Value="All of us to sign"></asp:ListItem>
                            </asp:RadioButtonList>
                            <p>
                                Note:<br />
                                <i>1. Where you do not elect a manner of operation, BWA will default to 'All of us to
                                    sign'</i><br />
                                <i>2. Phone Access, Online Access and a Debit Card cannot be selected unless the
                                manner of operation is 'Any one of us to sign'.</p>
                            </i>
                        </div>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage Text="13:Securities">
                        <div class="MainView">
                            <h2>
                                CHESS Transfer Of Securities</h2>
                            <p>
                                Do you want to transfer any existing listed securities to the new Desktop Broker
                                account to be established in your name?
                                <asp:RadioButtonList RepeatDirection="Horizontal" runat="server" ID="rbTransferSecToDescktopBroker">
                                    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                </asp:RadioButtonList>
                            </p>
                            <p>
                                If Yes to which service (ie. DIY, DIWM) :
                                <asp:RadioButtonList RepeatDirection="Horizontal" runat="server" ID="RBChessDIYDIWMOption">
                                    <asp:ListItem Text="DIY" Value="DIY"></asp:ListItem>
                                    <asp:ListItem Text="DIWM" Value="DIWM"></asp:ListItem>
                                </asp:RadioButtonList>
                            </p>
                            <hr />
                            <h2>
                                CHESS Registration Details</h2>
                            <p>
                                Please note that if you are transferring securities, The details below should be
                                EXACTLY the same as on your latest holding statement.</p>
                            <table width="100%">
                                <tr>
                                    <td width="20%">
                                        Registered Account Name:
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtRegisteredAccountNameChess"
                                            Width="90%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        Account Designation (Designation must not be more than 25 characters):
                                    </td>
                                    <td colspan="3">
                                        <input type="text" id="txt_ad1" name="txt_ad1" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="<" runat="server">
                                        <input type="text" id="txt_ad2" name="txt_ad2" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad3" name="txt_ad3" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad4" name="txt_ad4" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad5" name="txt_ad5" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad6" name="txt_ad6" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad7" name="txt_ad7" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad8" name="txt_ad8" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad9" name="txt_ad9" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad10" name="txt_ad10" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad11" name="txt_ad11" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad12" name="txt_ad12" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad13" name="txt_ad13" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad14" name="txt_ad14" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad15" name="txt_ad15" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad16" name="txt_ad16" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad17" name="txt_ad17" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad18" name="txt_ad18" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad19" name="txt_ad19" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad20" name="txt_ad20" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad21" name="txt_ad21" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad22" name="txt_ad22" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad23" name="txt_ad23" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad24" name="txt_ad24" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad25" name="txt_ad25" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad26" name="txt_ad26" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="" runat="server" />
                                        <input type="text" id="txt_ad27" name="txt_ad27" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="A" runat="server" />
                                        <input type="text" id="txt_ad28" name="txt_ad28" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="/" runat="server" />
                                        <input type="text" id="txt_ad29" name="txt_ad29" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value="C" runat="server" />
                                        <input type="text" id="txt_ad30" name="txt_ad30" size="10" maxlength="1" style="width: 9px;
                                            margin-right: 1px; padding: 0 4px" value=">" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        Address Line 1:
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtAddressLine1Chess" Width="90%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        Address Line 2:
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtAddressLine2Chess" Width="90%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        Suburb:
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtSuburbChess" Width="250px"></asp:TextBox>
                                    </td>
                                    <td>
                                        State:
                                    </td>
                                    <td>
                                        <wijmo:C1ComboBox ID="C1ComboBoxStateChess" runat="server" Width="250px">
                                            <Items>
                                                <wijmo:C1ComboBoxItem Value="ACT" Text="ACT" />
                                                <wijmo:C1ComboBoxItem Value="NSW" Text="NSW" />
                                                <wijmo:C1ComboBoxItem Value="NT" Text="NT" />
                                                <wijmo:C1ComboBoxItem Value="QLD" Text="QLD" />
                                                <wijmo:C1ComboBoxItem Value="SA" Text="SA" />
                                                <wijmo:C1ComboBoxItem Value="TAS" Text="TAS" />
                                                <wijmo:C1ComboBoxItem Value="VIC" Text="VIC" />
                                                <wijmo:C1ComboBoxItem Value="WA" Text="WA" />
                                            </Items>
                                        </wijmo:C1ComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Postcode:
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtPostcodeChess" Width="250px"></asp:TextBox>
                                    </td>
                                    <td>
                                        Country:
                                    </td>
                                    <td>
                                        <wijmo:C1ComboBox ID="C1ComboBoxCountryListChess" runat="server" Width="250px">
                                            <Items>
                                                <wijmo:C1ComboBoxItem Value="AF" Text="Afghanistan" />
                                                <wijmo:C1ComboBoxItem Value="AL" Text="Albania" />
                                                <wijmo:C1ComboBoxItem Value="DZ" Text="Algeria" />
                                                <wijmo:C1ComboBoxItem Value="AS" Text="American Samoa" />
                                                <wijmo:C1ComboBoxItem Value="AD" Text="Andorra" />
                                                <wijmo:C1ComboBoxItem Value="AO" Text="Angola" />
                                                <wijmo:C1ComboBoxItem Value="AI" Text="Anguilla" />
                                                <wijmo:C1ComboBoxItem Value="AQ" Text="Antarctica" />
                                                <wijmo:C1ComboBoxItem Value="AG" Text="Antigua And Barbuda" />
                                                <wijmo:C1ComboBoxItem Value="AR" Text="Argentina" />
                                                <wijmo:C1ComboBoxItem Value="AM" Text="Armenia" />
                                                <wijmo:C1ComboBoxItem Value="AW" Text="Aruba" />
                                                <wijmo:C1ComboBoxItem Value="AU" Text="Australia" Selected="True" />
                                                <wijmo:C1ComboBoxItem Value="AT" Text="Austria" />
                                                <wijmo:C1ComboBoxItem Value="AZ" Text="Azerbaijan" />
                                                <wijmo:C1ComboBoxItem Value="BS" Text="Bahamas" />
                                                <wijmo:C1ComboBoxItem Value="BH" Text="Bahrain" />
                                                <wijmo:C1ComboBoxItem Value="BD" Text="Bangladesh" />
                                                <wijmo:C1ComboBoxItem Value="BB" Text="Barbados" />
                                                <wijmo:C1ComboBoxItem Value="BY" Text="Belarus" />
                                                <wijmo:C1ComboBoxItem Value="BE" Text="Belgium" />
                                                <wijmo:C1ComboBoxItem Value="BZ" Text="Belize" />
                                                <wijmo:C1ComboBoxItem Value="BJ" Text="Benin" />
                                                <wijmo:C1ComboBoxItem Value="BM" Text="Bermuda" />
                                                <wijmo:C1ComboBoxItem Value="BT" Text="Bhutan" />
                                                <wijmo:C1ComboBoxItem Value="BO" Text="Bolivia" />
                                                <wijmo:C1ComboBoxItem Value="BA" Text="Bosnia And Herzegowina" />
                                                <wijmo:C1ComboBoxItem Value="BW" Text="Botswana" />
                                                <wijmo:C1ComboBoxItem Value="BV" Text="Bouvet Island" />
                                                <wijmo:C1ComboBoxItem Value="BR" Text="Brazil" />
                                                <wijmo:C1ComboBoxItem Value="IO" Text="British Indian Ocean Territory" />
                                                <wijmo:C1ComboBoxItem Value="BN" Text="Brunei Darussalam" />
                                                <wijmo:C1ComboBoxItem Value="BG" Text="Bulgaria" />
                                                <wijmo:C1ComboBoxItem Value="BF" Text="Burkina Faso" />
                                                <wijmo:C1ComboBoxItem Value="BI" Text="Burundi" />
                                                <wijmo:C1ComboBoxItem Value="KH" Text="Cambodia" />
                                                <wijmo:C1ComboBoxItem Value="CM" Text="Cameroon" />
                                                <wijmo:C1ComboBoxItem Value="CA" Text="Canada" />
                                                <wijmo:C1ComboBoxItem Value="CV" Text="Cape Verde" />
                                                <wijmo:C1ComboBoxItem Value="KY" Text="Cayman Islands" />
                                                <wijmo:C1ComboBoxItem Value="CF" Text="Central African Republic" />
                                                <wijmo:C1ComboBoxItem Value="TD" Text="Chad" />
                                                <wijmo:C1ComboBoxItem Value="CL" Text="Chile" />
                                                <wijmo:C1ComboBoxItem Value="CN" Text="China" />
                                                <wijmo:C1ComboBoxItem Value="CX" Text="Christmas Island" />
                                                <wijmo:C1ComboBoxItem Value="CC" Text="Cocos (Keeling) Islands" />
                                                <wijmo:C1ComboBoxItem Value="CO" Text="Colombia" />
                                                <wijmo:C1ComboBoxItem Value="KM" Text="Comoros" />
                                                <wijmo:C1ComboBoxItem Value="CG" Text="Congo" />
                                                <wijmo:C1ComboBoxItem Value="CK" Text="Cook Islands" />
                                                <wijmo:C1ComboBoxItem Value="CR" Text="Costa Rica" />
                                                <wijmo:C1ComboBoxItem Value="CI" Text="Cote D'Ivoire" />
                                                <wijmo:C1ComboBoxItem Value="HR" Text="Croatia (Local Name: Hrvatska)" />
                                                <wijmo:C1ComboBoxItem Value="CU" Text="Cuba" />
                                                <wijmo:C1ComboBoxItem Value="CY" Text="Cyprus" />
                                                <wijmo:C1ComboBoxItem Value="CZ" Text="Czech Republic" />
                                                <wijmo:C1ComboBoxItem Value="DK" Text="Denmark" />
                                                <wijmo:C1ComboBoxItem Value="DJ" Text="Djibouti" />
                                                <wijmo:C1ComboBoxItem Value="DM" Text="Dominica" />
                                                <wijmo:C1ComboBoxItem Value="DO" Text="Dominican Republic" />
                                                <wijmo:C1ComboBoxItem Value="TP" Text="East Timor" />
                                                <wijmo:C1ComboBoxItem Value="EC" Text="Ecuador" />
                                                <wijmo:C1ComboBoxItem Value="EG" Text="Egypt" />
                                                <wijmo:C1ComboBoxItem Value="SV" Text="El Salvador" />
                                                <wijmo:C1ComboBoxItem Value="GQ" Text="Equatorial Guinea" />
                                                <wijmo:C1ComboBoxItem Value="ER" Text="Eritrea" />
                                                <wijmo:C1ComboBoxItem Value="EE" Text="Estonia" />
                                                <wijmo:C1ComboBoxItem Value="ET" Text="Ethiopia" />
                                                <wijmo:C1ComboBoxItem Value="FK" Text="Falkland Islands (Malvinas)" />
                                                <wijmo:C1ComboBoxItem Value="FO" Text="Faroe Islands" />
                                                <wijmo:C1ComboBoxItem Value="FJ" Text="Fiji" />
                                                <wijmo:C1ComboBoxItem Value="FI" Text="Finland" />
                                                <wijmo:C1ComboBoxItem Value="FR" Text="France" />
                                                <wijmo:C1ComboBoxItem Value="GF" Text="French Guiana" />
                                                <wijmo:C1ComboBoxItem Value="PF" Text="French Polynesia" />
                                                <wijmo:C1ComboBoxItem Value="TF" Text="French Southern Territories" />
                                                <wijmo:C1ComboBoxItem Value="GA" Text="Gabon" />
                                                <wijmo:C1ComboBoxItem Value="GM" Text="Gambia" />
                                                <wijmo:C1ComboBoxItem Value="GE" Text="Georgia" />
                                                <wijmo:C1ComboBoxItem Value="DE" Text="Germany" />
                                                <wijmo:C1ComboBoxItem Value="GH" Text="Ghana" />
                                                <wijmo:C1ComboBoxItem Value="GI" Text="Gibraltar" />
                                                <wijmo:C1ComboBoxItem Value="GR" Text="Greece" />
                                                <wijmo:C1ComboBoxItem Value="GL" Text="Greenland" />
                                                <wijmo:C1ComboBoxItem Value="GD" Text="Grenada" />
                                                <wijmo:C1ComboBoxItem Value="GP" Text="Guadeloupe" />
                                                <wijmo:C1ComboBoxItem Value="GU" Text="Guam" />
                                                <wijmo:C1ComboBoxItem Value="GT" Text="Guatemala" />
                                                <wijmo:C1ComboBoxItem Value="GN" Text="Guinea" />
                                                <wijmo:C1ComboBoxItem Value="GW" Text="Guinea-Bissau" />
                                                <wijmo:C1ComboBoxItem Value="GY" Text="Guyana" />
                                                <wijmo:C1ComboBoxItem Value="HT" Text="Haiti" />
                                                <wijmo:C1ComboBoxItem Value="HM" Text="Heard And Mc Donald Islands" />
                                                <wijmo:C1ComboBoxItem Value="VA" Text="Holy See (Vatican City State)" />
                                                <wijmo:C1ComboBoxItem Value="HN" Text="Honduras" />
                                                <wijmo:C1ComboBoxItem Value="HK" Text="Hong Kong" />
                                                <wijmo:C1ComboBoxItem Value="HU" Text="Hungary" />
                                                <wijmo:C1ComboBoxItem Value="IS" Text="Icel And" />
                                                <wijmo:C1ComboBoxItem Value="IN" Text="India" />
                                                <wijmo:C1ComboBoxItem Value="ID" Text="Indonesia" />
                                                <wijmo:C1ComboBoxItem Value="IR" Text="Iran (Islamic Republic Of)" />
                                                <wijmo:C1ComboBoxItem Value="IQ" Text="Iraq" />
                                                <wijmo:C1ComboBoxItem Value="IE" Text="Ireland" />
                                                <wijmo:C1ComboBoxItem Value="IL" Text="Israel" />
                                                <wijmo:C1ComboBoxItem Value="IT" Text="Italy" />
                                                <wijmo:C1ComboBoxItem Value="JM" Text="Jamaica" />
                                                <wijmo:C1ComboBoxItem Value="JP" Text="Japan" />
                                                <wijmo:C1ComboBoxItem Value="JO" Text="Jordan" />
                                                <wijmo:C1ComboBoxItem Value="KZ" Text="Kazakhstan" />
                                                <wijmo:C1ComboBoxItem Value="KE" Text="Kenya" />
                                                <wijmo:C1ComboBoxItem Value="KI" Text="Kiribati" />
                                                <wijmo:C1ComboBoxItem Value="KP" Text="Korea, Dem People'S Republic" />
                                                <wijmo:C1ComboBoxItem Value="KR" Text="Korea, Republic Of" />
                                                <wijmo:C1ComboBoxItem Value="KW" Text="Kuwait" />
                                                <wijmo:C1ComboBoxItem Value="KG" Text="Kyrgyzstan" />
                                                <wijmo:C1ComboBoxItem Value="LA" Text="Lao People'S Dem Republic" />
                                                <wijmo:C1ComboBoxItem Value="LV" Text="Latvia" />
                                                <wijmo:C1ComboBoxItem Value="LB" Text="Lebanon" />
                                                <wijmo:C1ComboBoxItem Value="LS" Text="Lesotho" />
                                                <wijmo:C1ComboBoxItem Value="LR" Text="Liberia" />
                                                <wijmo:C1ComboBoxItem Value="LY" Text="Libyan Arab Jamahiriya" />
                                                <wijmo:C1ComboBoxItem Value="LI" Text="Liechtenstein" />
                                                <wijmo:C1ComboBoxItem Value="LT" Text="Lithuania" />
                                                <wijmo:C1ComboBoxItem Value="LU" Text="Luxembourg" />
                                                <wijmo:C1ComboBoxItem Value="MO" Text="Macau" />
                                                <wijmo:C1ComboBoxItem Value="MK" Text="Macedonia" />
                                                <wijmo:C1ComboBoxItem Value="MG" Text="Madagascar" />
                                                <wijmo:C1ComboBoxItem Value="MW" Text="Malawi" />
                                                <wijmo:C1ComboBoxItem Value="MY" Text="Malaysia" />
                                                <wijmo:C1ComboBoxItem Value="MV" Text="Maldives" />
                                                <wijmo:C1ComboBoxItem Value="ML" Text="Mali" />
                                                <wijmo:C1ComboBoxItem Value="MT" Text="Malta" />
                                                <wijmo:C1ComboBoxItem Value="MH" Text="Marshall Islands" />
                                                <wijmo:C1ComboBoxItem Value="MQ" Text="Martinique" />
                                                <wijmo:C1ComboBoxItem Value="MR" Text="Mauritania" />
                                                <wijmo:C1ComboBoxItem Value="MU" Text="Mauritius" />
                                                <wijmo:C1ComboBoxItem Value="YT" Text="Mayotte" />
                                                <wijmo:C1ComboBoxItem Value="MX" Text="Mexico" />
                                                <wijmo:C1ComboBoxItem Value="FM" Text="Micronesia, Federated States" />
                                                <wijmo:C1ComboBoxItem Value="MD" Text="Moldova, Republic Of" />
                                                <wijmo:C1ComboBoxItem Value="MC" Text="Monaco" />
                                                <wijmo:C1ComboBoxItem Value="MN" Text="Mongolia" />
                                                <wijmo:C1ComboBoxItem Value="MS" Text="Montserrat" />
                                                <wijmo:C1ComboBoxItem Value="MA" Text="Morocco" />
                                                <wijmo:C1ComboBoxItem Value="MZ" Text="Mozambique" />
                                                <wijmo:C1ComboBoxItem Value="MM" Text="Myanmar" />
                                                <wijmo:C1ComboBoxItem Value="NA" Text="Namibia" />
                                                <wijmo:C1ComboBoxItem Value="NR" Text="Nauru" />
                                                <wijmo:C1ComboBoxItem Value="NP" Text="Nepal" />
                                                <wijmo:C1ComboBoxItem Value="NL" Text="Netherlands" />
                                                <wijmo:C1ComboBoxItem Value="AN" Text="Netherlands Ant Illes" />
                                                <wijmo:C1ComboBoxItem Value="NC" Text="New Caledonia" />
                                                <wijmo:C1ComboBoxItem Value="NZ" Text="New Zealand" />
                                                <wijmo:C1ComboBoxItem Value="NI" Text="Nicaragua" />
                                                <wijmo:C1ComboBoxItem Value="NE" Text="Niger" />
                                                <wijmo:C1ComboBoxItem Value="NG" Text="Nigeria" />
                                                <wijmo:C1ComboBoxItem Value="NU" Text="Niue" />
                                                <wijmo:C1ComboBoxItem Value="NF" Text="Norfolk Island" />
                                                <wijmo:C1ComboBoxItem Value="MP" Text="Northern Mariana Islands" />
                                                <wijmo:C1ComboBoxItem Value="NO" Text="Norway" />
                                                <wijmo:C1ComboBoxItem Value="OM" Text="Oman" />
                                                <wijmo:C1ComboBoxItem Value="PK" Text="Pakistan" />
                                                <wijmo:C1ComboBoxItem Value="PW" Text="Palau" />
                                                <wijmo:C1ComboBoxItem Value="PA" Text="Panama" />
                                                <wijmo:C1ComboBoxItem Value="PG" Text="Papua New Guinea" />
                                                <wijmo:C1ComboBoxItem Value="PY" Text="Paraguay" />
                                                <wijmo:C1ComboBoxItem Value="PE" Text="Peru" />
                                                <wijmo:C1ComboBoxItem Value="PH" Text="Philippines" />
                                                <wijmo:C1ComboBoxItem Value="PN" Text="Pitcairn" />
                                                <wijmo:C1ComboBoxItem Value="PL" Text="Poland" />
                                                <wijmo:C1ComboBoxItem Value="PT" Text="Portugal" />
                                                <wijmo:C1ComboBoxItem Value="PR" Text="Puerto Rico" />
                                                <wijmo:C1ComboBoxItem Value="QA" Text="Qatar" />
                                                <wijmo:C1ComboBoxItem Value="RE" Text="Reunion" />
                                                <wijmo:C1ComboBoxItem Value="RO" Text="Romania" />
                                                <wijmo:C1ComboBoxItem Value="RU" Text="Russian Federation" />
                                                <wijmo:C1ComboBoxItem Value="RW" Text="Rwanda" />
                                                <wijmo:C1ComboBoxItem Value="KN" Text="Saint K Itts And Nevis" />
                                                <wijmo:C1ComboBoxItem Value="LC" Text="Saint Lucia" />
                                                <wijmo:C1ComboBoxItem Value="VC" Text="Saint Vincent, The Grenadines" />
                                                <wijmo:C1ComboBoxItem Value="WS" Text="Samoa" />
                                                <wijmo:C1ComboBoxItem Value="SM" Text="San Marino" />
                                                <wijmo:C1ComboBoxItem Value="ST" Text="Sao Tome And Principe" />
                                                <wijmo:C1ComboBoxItem Value="SA" Text="Saudi Arabia" />
                                                <wijmo:C1ComboBoxItem Value="SN" Text="Senegal" />
                                                <wijmo:C1ComboBoxItem Value="SC" Text="Seychelles" />
                                                <wijmo:C1ComboBoxItem Value="SL" Text="Sierra Leone" />
                                                <wijmo:C1ComboBoxItem Value="SG" Text="Singapore" />
                                                <wijmo:C1ComboBoxItem Value="SK" Text="Slovakia (Slovak Republic)" />
                                                <wijmo:C1ComboBoxItem Value="SI" Text="Slovenia" />
                                                <wijmo:C1ComboBoxItem Value="SB" Text="Solomon Islands" />
                                                <wijmo:C1ComboBoxItem Value="SO" Text="Somalia" />
                                                <wijmo:C1ComboBoxItem Value="ZA" Text="South Africa" />
                                                <wijmo:C1ComboBoxItem Value="GS" Text="South Georgia , S Sandwich Is." />
                                                <wijmo:C1ComboBoxItem Value="ES" Text="Spain" />
                                                <wijmo:C1ComboBoxItem Value="LK" Text="Sri Lanka" />
                                                <wijmo:C1ComboBoxItem Value="SH" Text="St. Helena" />
                                                <wijmo:C1ComboBoxItem Value="PM" Text="St. Pierre And Miquelon" />
                                                <wijmo:C1ComboBoxItem Value="SD" Text="Sudan" />
                                                <wijmo:C1ComboBoxItem Value="SR" Text="Suriname" />
                                                <wijmo:C1ComboBoxItem Value="SJ" Text="Svalbard, Jan Mayen Islands" />
                                                <wijmo:C1ComboBoxItem Value="SZ" Text="Sw Aziland" />
                                                <wijmo:C1ComboBoxItem Value="SE" Text="Sweden" />
                                                <wijmo:C1ComboBoxItem Value="CH" Text="Switzerland" />
                                                <wijmo:C1ComboBoxItem Value="SY" Text="Syrian Arab Republic" />
                                                <wijmo:C1ComboBoxItem Value="TW" Text="Taiwan" />
                                                <wijmo:C1ComboBoxItem Value="TJ" Text="Tajikistan" />
                                                <wijmo:C1ComboBoxItem Value="TZ" Text="Tanzania, United Republic Of" />
                                                <wijmo:C1ComboBoxItem Value="TH" Text="Thailand" />
                                                <wijmo:C1ComboBoxItem Value="TG" Text="Togo" />
                                                <wijmo:C1ComboBoxItem Value="TK" Text="Tokelau" />
                                                <wijmo:C1ComboBoxItem Value="TO" Text="Tonga" />
                                                <wijmo:C1ComboBoxItem Value="TT" Text="Trinidad And Tobago" />
                                                <wijmo:C1ComboBoxItem Value="TN" Text="Tunisia" />
                                                <wijmo:C1ComboBoxItem Value="TR" Text="Turkey" />
                                                <wijmo:C1ComboBoxItem Value="TM" Text="Turkmenistan" />
                                                <wijmo:C1ComboBoxItem Value="TC" Text="Turks And Caicos Islands" />
                                                <wijmo:C1ComboBoxItem Value="TV" Text="Tuvalu" />
                                                <wijmo:C1ComboBoxItem Value="UG" Text="Uganda" />
                                                <wijmo:C1ComboBoxItem Value="UA" Text="Ukraine" />
                                                <wijmo:C1ComboBoxItem Value="AE" Text="United Arab Emirates" />
                                                <wijmo:C1ComboBoxItem Value="GB" Text="United Kingdom" />
                                                <wijmo:C1ComboBoxItem Value="US" Text="United States" />
                                                <wijmo:C1ComboBoxItem Value="UM" Text="United States Minor Is." />
                                                <wijmo:C1ComboBoxItem Value="UY" Text="Uruguay" />
                                                <wijmo:C1ComboBoxItem Value="UZ" Text="Uzbekistan" />
                                                <wijmo:C1ComboBoxItem Value="VU" Text="Vanuatu" />
                                                <wijmo:C1ComboBoxItem Value="VE" Text="Venezuela" />
                                                <wijmo:C1ComboBoxItem Value="VN" Text="Viet Nam" />
                                                <wijmo:C1ComboBoxItem Value="VG" Text="Virgin Islands (British)" />
                                                <wijmo:C1ComboBoxItem Value="VI" Text="Virgin Islands (U.S.)" />
                                                <wijmo:C1ComboBoxItem Value="WF" Text="Wallis And Futuna Islands" />
                                                <wijmo:C1ComboBoxItem Value="EH" Text="Western Sahara" />
                                                <wijmo:C1ComboBoxItem Value="YE" Text="Yemen" />
                                                <wijmo:C1ComboBoxItem Value="YU" Text="Yugoslavia" />
                                                <wijmo:C1ComboBoxItem Value="ZR" Text="Zaire" />
                                                <wijmo:C1ComboBoxItem Value="ZM" Text="Zambia" />
                                                <wijmo:C1ComboBoxItem Value="ZW" Text="Zimbabwe" />
                                            </Items>
                                        </wijmo:C1ComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <b>Please note: This address will receive all CHESS and registry correspondence (HIN
                                            confirmation, TFN confirmation, Bank A/C confirmation, Dividend statements, Annual
                                            reports, Corporate Action Notices)</b>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <hr />
                            <h2>
                                CHESS Sponsorship Transfer</h2>
                            <p>
                                Are the securities to be transferred</p>
                            <ul>
                                <li>
                                    <asp:CheckBox ID="chkIssueSponsorChess" runat="server" Text="Issuer Sponsored - please attach copies of your Issuer Sponsored Holding Statements" /></li>
                                <ul>
                                    <li>
                                        <asp:CheckBox ID="chkExactNameAddressMatchAbove" runat="server" Text="Exact Name/Address match as above" /><br />
                                        <br />
                                        How many different company holdings
                                        <asp:TextBox CssClass="Txt-Field" runat="server" ID="txtHowManyHoldingCompanyChess"></asp:TextBox><br />
                                        <br />
                                        i. Use an Issuer Sponsored Holdings to CHESS Sponsorship Conversion Form for all
                                        matching holdings<br />
                                        <br />
                                    </li>
                                    <li>
                                        <asp:CheckBox ID="chkExactNameAddressNOTMatchAbove" runat="server" Text="Name/Address does not match the CHESS Registration Details above" /><br />
                                        <br />
                                        How many different company holdings need to be changed?<asp:TextBox CssClass="Txt-Field"
                                            runat="server" ID="txtHowManyHoldingCompanyChessNotMatch"></asp:TextBox><br />
                                        <br />
                                        i. Use Change of client details form<br />
                                        <br />
                                        ii. Use an Issuer Sponsored Holdings to CHESS Sponsorship Conversion Form for all
                                        un-matched holdings </li>
                                </ul>
                            </ul>
                            <ul>
                                <li>
                                    <asp:CheckBox ID="chkBrokerSponsoredRegistration" runat="server" Text="Broker Sponsored - please attach copies of your CHESS Registration" /></li>
                                <ul>
                                    <li>i. Use the Broker to Broker Transfer Request, for each different Broker</li>
                                    <li>ii. If names/addresses are not matched, use a Client Change of Details Form</li>
                                    <li>iii. If there is a change of Legal or Beneficial ownership, use an Off Market Transfer
                                        Form for each holding</li>
                                </ul>
                            </ul>
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        Name of existing Sponsoring Broker:
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txtNameOfSponsorBroker1" runat="server" Width="200px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Existing Broker PID:
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txtBrokerID1" runat="server" Width="200px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Client HIN:
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txtClientHin1" runat="server" Width="200px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        Name of existing Sponsoring Broker:
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txtNameOfSponsorBroker2" runat="server" Width="200px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Existing Broker PID:
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txtBrokerID2" runat="server" Width="200px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Client HIN:
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="Txt-Field" ID="txtClientHin2" runat="server" Width="200px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage Text="14.Address">
                        <div class="MainView">
                            <UMA:AddressDetails runat="server" ID="AddressDetails" />
                        </div>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage Text="15:Checklist">
                        <div class="MainView">
                            <h2>
                                UMA Document Checklist
                                <br />
                                <br />
                            </h2>
                            <asp:CheckBox Text="Proof of ID (Drivers Licence / passport) - Certified Copy" runat="server"
                                ID="chkProofofIDCheckListItem" /><br />
                            <asp:CheckBox Text="Trust Deed (If a Trust) - Certified Copy of Title Page, Contents/Recitals Page, Execution Page"
                                runat="server" ID="chkTrustDeedCheckListItem" /><br />
                            <asp:CheckBox Text="Letter from your Accountant (If a sophisticated Investor)" runat="server"
                                ID="chkLetterFromYourAccountantCheckListItem" /><br />
                            <asp:CheckBox Text="Business Name Registration Certificate (If a business account)"
                                runat="server" ID="chkBusinessNameCheckListItem" /><br />
                            <asp:CheckBox Text="Issuer Sponsored Holdings to CHESS Sponsorship Conversion " runat="server"
                                ID="chkIssueSponsorHoldingsToChessCheckListItem" /><br />
                            <asp:CheckBox Text="Issuer Sponsored Holding Statements" runat="server" ID="chkIssuerSponsoredStatementsCheckListItem" /><br />
                            <asp:CheckBox Text="Off Market Transfer" runat="server" ID="chkOffMarketTransferCheckListItem" /><br />
                            <asp:CheckBox Text="Change of Client Details Form" runat="server" ID="chkChangeOfClientDetailsCheckListItem" /><br />
                            <asp:CheckBox Text="Broker to Broker Transfer Form" runat="server" ID="chkBrokerToBrokerTransferClientCheckList" /><br />
                            <asp:CheckBox Text="Proof of Address (when passport used for ID) - copy of utilities bill, bank statement or tax assessment notice will be accepted"
                                runat="server" ID="chkProofOfAddressPassportProvidedClientCheckListItem" /><br />
                            <br />
                            <br />
                            <h2>
                                VALIDATION CHECKLIST</h2>
                            <br />
                            *Click Save to trigger validation.
                            <br />
                            <wijmo:C1GridView ShowFilter="false" RowStyle-Font-Size="Small" FilterStyle-Font-Size="Small"
                                HeaderStyle-Font-Size="Small" EnableTheming="true" Width="1000px" ID="C1GridViewValidation"
                                CallbackSettings-Action="None" runat="server" AutogenerateColumns="False" ClientSelectionMode="None"
                                CallbackSettings-Mode="Full">
                                <SelectedRowStyle />
                                <Columns>
                                    <wijmo:C1BoundField Width="10%" DataField="ErrorType" HeaderText="ErrorType">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle Height="20px" />
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField Width="20%" DataField="ShortDesc" HeaderText="ShortDesc">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle Height="20px" />
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField Width="70%" DataField="Description" HeaderText="Description">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle Height="20px" />
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </div>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage Text="15:Users" ID="UserSecurities" runat="server">
                        <div class="MainView">
                            <uc2:SecurityWorkFlow ID="UsersWorkFlow" runat="server" />
                        </div>
                    </wijmo:C1TabPage>
                </Pages>
                <ShowOption Fade="true" />
            </wijmo:C1Tabs>
            <br />
            <br />
            <table>
                <tr>
                    <td>
                        <asp:Button ID="btnSave" Font-Bold="true" runat="server" Width="190px" BackColor="#465c71"
                            ForeColor="#dde4ec" Text="SAVE" OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnSaveExit" Font-Bold="true" runat="server" Width="190px" BackColor="#465c71"
                            ForeColor="#dde4ec" Text="SAVE & EXIT" OnClick="btnSaveExit_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnPrintSummary" Font-Bold="true" runat="server" Width="190px" BackColor="#465c71"
                            ForeColor="#dde4ec" Text="PRINT SUMMARY" OnClick="btnPrintSummary_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnPrint" Font-Bold="true" runat="server" Width="190px" BackColor="#465c71"
                            ForeColor="#dde4ec" Text="PRINT FOR SIGNING" OnClick="btnPrint_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnPrintMaq" Font-Bold="true" runat="server" Width="230px" BackColor="#465c71"
                            ForeColor="#dde4ec" Text="MACQUARIE 3rd PARTY" OnClick="btnPrintMaqClick" />
                    </td>
                    <td>
                        <asp:Button ID="btnFinalise" Font-Bold="true" runat="server" Width="190px" BackColor="#465c71"
                            ForeColor="#dde4ec" Text="SUBMIT TO E-CLIPSE" OnClick="btnFinalise_Click" />
                    </td>
                </tr>
            </table>
            <cc1:C1Dialog ID="AuthorityToActDialog" runat="server" Width="700px" Height="500px"
                Modal="true" ShowOnLoad="false" Stack="True" CloseText="Close" MaintainStatesOnPostback="true"
                Title="Authority to Act">
                <Content>
                    <p>
                        The agreed limited power of attorney granted to e-Clipse and Australian Money Market
                        (AMM) is to undertake and perform the following on the clients behalf:</p>
                    <p>
                        1. apply for and open new bank and broker accounts with any chosen financial institution,
                        or any other investments;<br />
                        2. authorise for direct debit payments to be acted upon by any chosen institution
                        in order to transfer funds in the relation to new or existing accounts<br />
                        3. instruct in relation to rollovers, maturities, transfer requests of existing
                        investments;<br />
                        4. advise of changes to my/our contact details as advised from time to time;<br />
                        5. notify my/our TFN(s) or Exemption(s) in respect of any existing investment(s)
                        and or new purchase(s) made on my/our behalf.</p>
                    <br />
                    <p>
                        I/We understand that e-Clipse and AMM will provide me/us with a copy of the relevant
                        terms and conditions relating to any account which e-Clipse and AMM proposes to
                        open in my/our name prior to the account being opened. I/We am aware that I/we must
                        read and understand those terms and conditions and contact e-Clipse and AMM if I/we
                        do not agree to be bound by them.</p>
                    <p>
                        This authority is given only to act as per instructions given by me/us via the e-Clipse
                        UMA platform or by any other means of documented instruction.</p>
                </Content>
            </cc1:C1Dialog>
            <cc1:C1Dialog ID="SophisticatedInvestorDialog" runat="server" Width="700px" Height="500px"
                Modal="true" ShowOnLoad="false" Stack="True" CloseText="Close" Title="" MaintainStatesOnPostback="true">
                <Content>
                    <h2>
                        Wholesale or Retail Investors</h2>
                    <p>
                        To qualify as a Sophisticated or Professional Investor under Section 708 of the
                        Corporations Act you need to be able to meet one of the conditions below. Please
                        ensure that you include documentation to support your situation. The Investment
                        Program to be implemented for you under the Managed Account Service will need to
                        have regard to whether you are a wholesale or retail investor. A sophisticated investor
                        (see below) is a client equipped to be considered a wholesale client in relation
                        to the UMA service provided by e-Clipse. To be considered a wholesale investor as
                        defined under the Corporations Act 2001 (cth) (Act) you must:</p>
                    <p>
                        i. Be investing an amount of at least $500,000 in e-Clipse; or</p>
                    <p>
                        ii. Meet the "sophisticated investor" criteria under the Act (see below).</p>
                    <p>
                        In the absence of a certificate from a qualified accountant where the amount invested
                        is less than $500,000, we must consider you a retail investor.</p>
                    To qualify as a Sophisticated or Professional Investor under Section 708 of the
                    Corporations Act you need to be able to meet one of the conditions below.</label>
                    <h2>
                        Sophisticated Investors</h2>
                    <p>
                        To qualify as a sophisticated investor please supply a certificate provided by a
                        qualified accountant or eligible foreign professional body which states that you
                        have:</p>
                    a. net assets of at least $2.5 million; or<br />
                    b. gross income for each of the last two financial years of at least $250,000 a
                    year."<br />
                    <p>
                        The certificate must include:</p>
                    <p>
                        1. the class membership of the professional body to which the accountant belongs
                        (as outlined below)</p>
                    <p>
                        2. the date of issue</p>
                    <p>
                        3. the Chapter of Chapters of the Corporations Act under which it is issued</p>
                    <p>
                        If you control a company or trust and meet the requirements above, the company or
                        trust may also qualify for exempt status as a sophisticated investor.</p>
                    <h2>
                        Qualified Accountant</h2>
                    <p>
                        A qualified Accountant is defined in s88B of the Act as a person meeting the criteria
                        in a class declaration made by ASIC. Under ASIC's class order (CO 01/1256) you are
                        a qualified accountant if you: a. belong to one of the following professional bodies
                        in the declared membership classification (below) and; b. you comply with your body's
                        continuing professional education requirements.</p>
                    <table>
                        <tr>
                            <th style="text-align: left;">
                                Professional Bodies
                            </th>
                            <th style="text-align: left;">
                                Declared Membership Classification
                            </th>
                        </tr>
                        <tr class="odd">
                            <td height="25">
                                <label>
                                    The Institute of Chartered Accountants in Australia</label>
                            </td>
                            <td>
                                <label>
                                    CA, ACA and FCA</label>
                            </td>
                        </tr>
                        <tr class="even">
                            <td height="25">
                                <label>
                                    CPA Australia</label>
                            </td>
                            <td>
                                <label>
                                    CPA and FCPA</label>
                            </td>
                        </tr>
                        <tr class="odd">
                            <td height="25">
                                <label>
                                    National Institute of Accountants in Australia</label>
                            </td>
                            <td>
                                <label>
                                    PNA, FPNA, MINA and FINA</label>
                            </td>
                        </tr>
                    </table>
                </Content>
                <ExpandingAnimation Duration="400" />
                <CollapsingAnimation Duration="300" />
            </cc1:C1Dialog>
            <asp:ModalPopupExtender ID="ModalPopupExtenderWarningValidation" runat="server" CancelControlID="btnCancel"
                TargetControlID="btnShowPopup" PopupControlID="pnlpopup" PopupDragHandleControlID="PopupHeader"
                Drag="true" BackgroundCssClass="ModalPopupBG">
            </asp:ModalPopupExtender>
            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
            <asp:Panel ID="pnlpopup" runat="server" BackColor="White" Style="display: none">
                <div class="popup_Container">
                    <div class="popup_Titlebar" id="PopupHeader">
                        <div class="TitlebarLeft">
                            Transaction Details
                        </div>
                        <div class="TitlebarRight" onclick="cancel();">
                        </div>
                    </div>
                    <div class="PopupBody">
                        <p>
                            You will not be able to SUBMIT this application to e-Clipse until the Validation
                            fields on ‘15. Checklist’ are blank.</p>
                        <p>
                            Please click on ’15. Checklist’ to see the Validation table.</p>
                        <p>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" /></p>
                    </div>
                </div>
            </asp:Panel>
            <cc1:C1Dialog ID="InvestmentExclusionsDialog" runat="server" Width="1000px" Height="600px"
                Modal="True" ShowOnLoad="False" Stack="True" CloseText="Close" CloseOnEscape="False"
                MaintainVisibilityOnPostback="False" Show="blind" MaintainStatesOnPostback="true">
                <CaptionButtons>
                    <Pin IconClassOn="ui-icon-pin-w" IconClassOff="ui-icon-pin-s"></Pin>
                    <Refresh IconClassOn="ui-icon-refresh"></Refresh>
                    <Minimize IconClassOn="ui-icon-minus"></Minimize>
                    <Maximize IconClassOn="ui-icon-extlink"></Maximize>
                    <Close IconClassOn="ui-icon-close"></Close>
                </CaptionButtons>
                <Content>
                    <h1>
                        MANAGED DISCRETIONARY ACCOUNT AGREEMENT</h1>
                    <br />
                    <br />
                </Content>
                <ExpandingAnimation Duration="400" />
                <CollapsingAnimation Duration="300" />
            </cc1:C1Dialog>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
