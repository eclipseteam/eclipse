﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddressDetails.ascx.cs"
    Inherits="eclipseonlineweb.AddressDetails" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
    TagPrefix="wijmo" %>



<b>Account Mailing Address:</b>
<asp:Panel runat="server" ID="pnlMailingAddress">
    <table width="100%">
        <tr>
            <td width="10%">
                Address Line 1:
            </td>
            <td colspan="3">
                <asp:TextBox runat="server" ID="txtAddressLine1Mail" Text=""  Width="90%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="10%">
                Address Line 2:
            </td>
            <td colspan="3">
                <asp:TextBox runat="server" ID="txtAddressLine2Mail" Text=""  Width="90%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="10%">
                Suburb:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSuburbSigAddressMail" Text=""  Width="250px"></asp:TextBox>
            </td>
            <td>
                State:
            </td>
            <td>
                <wijmo:C1ComboBox ID="C1ComboBoxStateSigAddressMail" Text=""  runat="server" Width="250px">
                    <Items>
                        <wijmo:C1ComboBoxItem Value="ACT" Text="ACT" />
                        <wijmo:C1ComboBoxItem Value="NSW" Text="NSW"  Selected="true"/>
                        <wijmo:C1ComboBoxItem Value="NT" Text="NT" />
                        <wijmo:C1ComboBoxItem Value="QLD" Text="QLD" />
                        <wijmo:C1ComboBoxItem Value="SA" Text="SA" />
                        <wijmo:C1ComboBoxItem Value="TAS" Text="TAS" />
                        <wijmo:C1ComboBoxItem Value="VIC" Text="VIC" />
                        <wijmo:C1ComboBoxItem Value="WA" Text="WA" />
                    </Items>
                </wijmo:C1ComboBox>
            </td>
        </tr>
        <tr>
            <td>
                Postcode:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPostcodeSigAddressMail" Text="2089"   Width="250px"></asp:TextBox>
            </td>
            <td>
                Country:
            </td>
            <td>
                <wijmo:C1ComboBox ID="C1ComboBoxCountryListSigAddressMail" Text="Australia"  runat="server" Width="250px">
                    <Items>
                        <wijmo:C1ComboBoxItem Value="AF" Text="Afghanistan" />
                        <wijmo:C1ComboBoxItem Value="AL" Text="Albania" />
                        <wijmo:C1ComboBoxItem Value="DZ" Text="Algeria" />
                        <wijmo:C1ComboBoxItem Value="AS" Text="American Samoa" />
                        <wijmo:C1ComboBoxItem Value="AD" Text="Andorra" />
                        <wijmo:C1ComboBoxItem Value="AO" Text="Angola" />
                        <wijmo:C1ComboBoxItem Value="AI" Text="Anguilla" />
                        <wijmo:C1ComboBoxItem Value="AQ" Text="Antarctica" />
                        <wijmo:C1ComboBoxItem Value="AG" Text="Antigua And Barbuda" />
                        <wijmo:C1ComboBoxItem Value="AR" Text="Argentina" />
                        <wijmo:C1ComboBoxItem Value="AM" Text="Armenia" />
                        <wijmo:C1ComboBoxItem Value="AW" Text="Aruba" />
                        <wijmo:C1ComboBoxItem Value="AU" Text="Australia" Selected="True" />
                        <wijmo:C1ComboBoxItem Value="AT" Text="Austria" />
                        <wijmo:C1ComboBoxItem Value="AZ" Text="Azerbaijan" />
                        <wijmo:C1ComboBoxItem Value="BS" Text="Bahamas" />
                        <wijmo:C1ComboBoxItem Value="BH" Text="Bahrain" />
                        <wijmo:C1ComboBoxItem Value="BD" Text="Bangladesh" />
                        <wijmo:C1ComboBoxItem Value="BB" Text="Barbados" />
                        <wijmo:C1ComboBoxItem Value="BY" Text="Belarus" />
                        <wijmo:C1ComboBoxItem Value="BE" Text="Belgium" />
                        <wijmo:C1ComboBoxItem Value="BZ" Text="Belize" />
                        <wijmo:C1ComboBoxItem Value="BJ" Text="Benin" />
                        <wijmo:C1ComboBoxItem Value="BM" Text="Bermuda" />
                        <wijmo:C1ComboBoxItem Value="BT" Text="Bhutan" />
                        <wijmo:C1ComboBoxItem Value="BO" Text="Bolivia" />
                        <wijmo:C1ComboBoxItem Value="BA" Text="Bosnia And Herzegowina" />
                        <wijmo:C1ComboBoxItem Value="BW" Text="Botswana" />
                        <wijmo:C1ComboBoxItem Value="BV" Text="Bouvet Island" />
                        <wijmo:C1ComboBoxItem Value="BR" Text="Brazil" />
                        <wijmo:C1ComboBoxItem Value="IO" Text="British Indian Ocean Territory" />
                        <wijmo:C1ComboBoxItem Value="BN" Text="Brunei Darussalam" />
                        <wijmo:C1ComboBoxItem Value="BG" Text="Bulgaria" />
                        <wijmo:C1ComboBoxItem Value="BF" Text="Burkina Faso" />
                        <wijmo:C1ComboBoxItem Value="BI" Text="Burundi" />
                        <wijmo:C1ComboBoxItem Value="KH" Text="Cambodia" />
                        <wijmo:C1ComboBoxItem Value="CM" Text="Cameroon" />
                        <wijmo:C1ComboBoxItem Value="CA" Text="Canada" />
                        <wijmo:C1ComboBoxItem Value="CV" Text="Cape Verde" />
                        <wijmo:C1ComboBoxItem Value="KY" Text="Cayman Islands" />
                        <wijmo:C1ComboBoxItem Value="CF" Text="Central African Republic" />
                        <wijmo:C1ComboBoxItem Value="TD" Text="Chad" />
                        <wijmo:C1ComboBoxItem Value="CL" Text="Chile" />
                        <wijmo:C1ComboBoxItem Value="CN" Text="China" />
                        <wijmo:C1ComboBoxItem Value="CX" Text="Christmas Island" />
                        <wijmo:C1ComboBoxItem Value="CC" Text="Cocos (Keeling) Islands" />
                        <wijmo:C1ComboBoxItem Value="CO" Text="Colombia" />
                        <wijmo:C1ComboBoxItem Value="KM" Text="Comoros" />
                        <wijmo:C1ComboBoxItem Value="CG" Text="Congo" />
                        <wijmo:C1ComboBoxItem Value="CK" Text="Cook Islands" />
                        <wijmo:C1ComboBoxItem Value="CR" Text="Costa Rica" />
                        <wijmo:C1ComboBoxItem Value="CI" Text="Cote D'Ivoire" />
                        <wijmo:C1ComboBoxItem Value="HR" Text="Croatia (Local Name: Hrvatska)" />
                        <wijmo:C1ComboBoxItem Value="CU" Text="Cuba" />
                        <wijmo:C1ComboBoxItem Value="CY" Text="Cyprus" />
                        <wijmo:C1ComboBoxItem Value="CZ" Text="Czech Republic" />
                        <wijmo:C1ComboBoxItem Value="DK" Text="Denmark" />
                        <wijmo:C1ComboBoxItem Value="DJ" Text="Djibouti" />
                        <wijmo:C1ComboBoxItem Value="DM" Text="Dominica" />
                        <wijmo:C1ComboBoxItem Value="DO" Text="Dominican Republic" />
                        <wijmo:C1ComboBoxItem Value="TP" Text="East Timor" />
                        <wijmo:C1ComboBoxItem Value="EC" Text="Ecuador" />
                        <wijmo:C1ComboBoxItem Value="EG" Text="Egypt" />
                        <wijmo:C1ComboBoxItem Value="SV" Text="El Salvador" />
                        <wijmo:C1ComboBoxItem Value="GQ" Text="Equatorial Guinea" />
                        <wijmo:C1ComboBoxItem Value="ER" Text="Eritrea" />
                        <wijmo:C1ComboBoxItem Value="EE" Text="Estonia" />
                        <wijmo:C1ComboBoxItem Value="ET" Text="Ethiopia" />
                        <wijmo:C1ComboBoxItem Value="FK" Text="Falkland Islands (Malvinas)" />
                        <wijmo:C1ComboBoxItem Value="FO" Text="Faroe Islands" />
                        <wijmo:C1ComboBoxItem Value="FJ" Text="Fiji" />
                        <wijmo:C1ComboBoxItem Value="FI" Text="Finland" />
                        <wijmo:C1ComboBoxItem Value="FR" Text="France" />
                        <wijmo:C1ComboBoxItem Value="GF" Text="French Guiana" />
                        <wijmo:C1ComboBoxItem Value="PF" Text="French Polynesia" />
                        <wijmo:C1ComboBoxItem Value="TF" Text="French Southern Territories" />
                        <wijmo:C1ComboBoxItem Value="GA" Text="Gabon" />
                        <wijmo:C1ComboBoxItem Value="GM" Text="Gambia" />
                        <wijmo:C1ComboBoxItem Value="GE" Text="Georgia" />
                        <wijmo:C1ComboBoxItem Value="DE" Text="Germany" />
                        <wijmo:C1ComboBoxItem Value="GH" Text="Ghana" />
                        <wijmo:C1ComboBoxItem Value="GI" Text="Gibraltar" />
                        <wijmo:C1ComboBoxItem Value="GR" Text="Greece" />
                        <wijmo:C1ComboBoxItem Value="GL" Text="Greenland" />
                        <wijmo:C1ComboBoxItem Value="GD" Text="Grenada" />
                        <wijmo:C1ComboBoxItem Value="GP" Text="Guadeloupe" />
                        <wijmo:C1ComboBoxItem Value="GU" Text="Guam" />
                        <wijmo:C1ComboBoxItem Value="GT" Text="Guatemala" />
                        <wijmo:C1ComboBoxItem Value="GN" Text="Guinea" />
                        <wijmo:C1ComboBoxItem Value="GW" Text="Guinea-Bissau" />
                        <wijmo:C1ComboBoxItem Value="GY" Text="Guyana" />
                        <wijmo:C1ComboBoxItem Value="HT" Text="Haiti" />
                        <wijmo:C1ComboBoxItem Value="HM" Text="Heard And Mc Donald Islands" />
                        <wijmo:C1ComboBoxItem Value="VA" Text="Holy See (Vatican City State)" />
                        <wijmo:C1ComboBoxItem Value="HN" Text="Honduras" />
                        <wijmo:C1ComboBoxItem Value="HK" Text="Hong Kong" />
                        <wijmo:C1ComboBoxItem Value="HU" Text="Hungary" />
                        <wijmo:C1ComboBoxItem Value="IS" Text="Icel And" />
                        <wijmo:C1ComboBoxItem Value="IN" Text="India" />
                        <wijmo:C1ComboBoxItem Value="ID" Text="Indonesia" />
                        <wijmo:C1ComboBoxItem Value="IR" Text="Iran (Islamic Republic Of)" />
                        <wijmo:C1ComboBoxItem Value="IQ" Text="Iraq" />
                        <wijmo:C1ComboBoxItem Value="IE" Text="Ireland" />
                        <wijmo:C1ComboBoxItem Value="IL" Text="Israel" />
                        <wijmo:C1ComboBoxItem Value="IT" Text="Italy" />
                        <wijmo:C1ComboBoxItem Value="JM" Text="Jamaica" />
                        <wijmo:C1ComboBoxItem Value="JP" Text="Japan" />
                        <wijmo:C1ComboBoxItem Value="JO" Text="Jordan" />
                        <wijmo:C1ComboBoxItem Value="KZ" Text="Kazakhstan" />
                        <wijmo:C1ComboBoxItem Value="KE" Text="Kenya" />
                        <wijmo:C1ComboBoxItem Value="KI" Text="Kiribati" />
                        <wijmo:C1ComboBoxItem Value="KP" Text="Korea, Dem People'S Republic" />
                        <wijmo:C1ComboBoxItem Value="KR" Text="Korea, Republic Of" />
                        <wijmo:C1ComboBoxItem Value="KW" Text="Kuwait" />
                        <wijmo:C1ComboBoxItem Value="KG" Text="Kyrgyzstan" />
                        <wijmo:C1ComboBoxItem Value="LA" Text="Lao People'S Dem Republic" />
                        <wijmo:C1ComboBoxItem Value="LV" Text="Latvia" />
                        <wijmo:C1ComboBoxItem Value="LB" Text="Lebanon" />
                        <wijmo:C1ComboBoxItem Value="LS" Text="Lesotho" />
                        <wijmo:C1ComboBoxItem Value="LR" Text="Liberia" />
                        <wijmo:C1ComboBoxItem Value="LY" Text="Libyan Arab Jamahiriya" />
                        <wijmo:C1ComboBoxItem Value="LI" Text="Liechtenstein" />
                        <wijmo:C1ComboBoxItem Value="LT" Text="Lithuania" />
                        <wijmo:C1ComboBoxItem Value="LU" Text="Luxembourg" />
                        <wijmo:C1ComboBoxItem Value="MO" Text="Macau" />
                        <wijmo:C1ComboBoxItem Value="MK" Text="Macedonia" />
                        <wijmo:C1ComboBoxItem Value="MG" Text="Madagascar" />
                        <wijmo:C1ComboBoxItem Value="MW" Text="Malawi" />
                        <wijmo:C1ComboBoxItem Value="MY" Text="Malaysia" />
                        <wijmo:C1ComboBoxItem Value="MV" Text="Maldives" />
                        <wijmo:C1ComboBoxItem Value="ML" Text="Mali" />
                        <wijmo:C1ComboBoxItem Value="MT" Text="Malta" />
                        <wijmo:C1ComboBoxItem Value="MH" Text="Marshall Islands" />
                        <wijmo:C1ComboBoxItem Value="MQ" Text="Martinique" />
                        <wijmo:C1ComboBoxItem Value="MR" Text="Mauritania" />
                        <wijmo:C1ComboBoxItem Value="MU" Text="Mauritius" />
                        <wijmo:C1ComboBoxItem Value="YT" Text="Mayotte" />
                        <wijmo:C1ComboBoxItem Value="MX" Text="Mexico" />
                        <wijmo:C1ComboBoxItem Value="FM" Text="Micronesia, Federated States" />
                        <wijmo:C1ComboBoxItem Value="MD" Text="Moldova, Republic Of" />
                        <wijmo:C1ComboBoxItem Value="MC" Text="Monaco" />
                        <wijmo:C1ComboBoxItem Value="MN" Text="Mongolia" />
                        <wijmo:C1ComboBoxItem Value="MS" Text="Montserrat" />
                        <wijmo:C1ComboBoxItem Value="MA" Text="Morocco" />
                        <wijmo:C1ComboBoxItem Value="MZ" Text="Mozambique" />
                        <wijmo:C1ComboBoxItem Value="MM" Text="Myanmar" />
                        <wijmo:C1ComboBoxItem Value="NA" Text="Namibia" />
                        <wijmo:C1ComboBoxItem Value="NR" Text="Nauru" />
                        <wijmo:C1ComboBoxItem Value="NP" Text="Nepal" />
                        <wijmo:C1ComboBoxItem Value="NL" Text="Netherlands" />
                        <wijmo:C1ComboBoxItem Value="AN" Text="Netherlands Ant Illes" />
                        <wijmo:C1ComboBoxItem Value="NC" Text="New Caledonia" />
                        <wijmo:C1ComboBoxItem Value="NZ" Text="New Zealand" />
                        <wijmo:C1ComboBoxItem Value="NI" Text="Nicaragua" />
                        <wijmo:C1ComboBoxItem Value="NE" Text="Niger" />
                        <wijmo:C1ComboBoxItem Value="NG" Text="Nigeria" />
                        <wijmo:C1ComboBoxItem Value="NU" Text="Niue" />
                        <wijmo:C1ComboBoxItem Value="NF" Text="Norfolk Island" />
                        <wijmo:C1ComboBoxItem Value="MP" Text="Northern Mariana Islands" />
                        <wijmo:C1ComboBoxItem Value="NO" Text="Norway" />
                        <wijmo:C1ComboBoxItem Value="OM" Text="Oman" />
                        <wijmo:C1ComboBoxItem Value="PK" Text="Pakistan" />
                        <wijmo:C1ComboBoxItem Value="PW" Text="Palau" />
                        <wijmo:C1ComboBoxItem Value="PA" Text="Panama" />
                        <wijmo:C1ComboBoxItem Value="PG" Text="Papua New Guinea" />
                        <wijmo:C1ComboBoxItem Value="PY" Text="Paraguay" />
                        <wijmo:C1ComboBoxItem Value="PE" Text="Peru" />
                        <wijmo:C1ComboBoxItem Value="PH" Text="Philippines" />
                        <wijmo:C1ComboBoxItem Value="PN" Text="Pitcairn" />
                        <wijmo:C1ComboBoxItem Value="PL" Text="Poland" />
                        <wijmo:C1ComboBoxItem Value="PT" Text="Portugal" />
                        <wijmo:C1ComboBoxItem Value="PR" Text="Puerto Rico" />
                        <wijmo:C1ComboBoxItem Value="QA" Text="Qatar" />
                        <wijmo:C1ComboBoxItem Value="RE" Text="Reunion" />
                        <wijmo:C1ComboBoxItem Value="RO" Text="Romania" />
                        <wijmo:C1ComboBoxItem Value="RU" Text="Russian Federation" />
                        <wijmo:C1ComboBoxItem Value="RW" Text="Rwanda" />
                        <wijmo:C1ComboBoxItem Value="KN" Text="Saint K Itts And Nevis" />
                        <wijmo:C1ComboBoxItem Value="LC" Text="Saint Lucia" />
                        <wijmo:C1ComboBoxItem Value="VC" Text="Saint Vincent, The Grenadines" />
                        <wijmo:C1ComboBoxItem Value="WS" Text="Samoa" />
                        <wijmo:C1ComboBoxItem Value="SM" Text="San Marino" />
                        <wijmo:C1ComboBoxItem Value="ST" Text="Sao Tome And Principe" />
                        <wijmo:C1ComboBoxItem Value="SA" Text="Saudi Arabia" />
                        <wijmo:C1ComboBoxItem Value="SN" Text="Senegal" />
                        <wijmo:C1ComboBoxItem Value="SC" Text="Seychelles" />
                        <wijmo:C1ComboBoxItem Value="SL" Text="Sierra Leone" />
                        <wijmo:C1ComboBoxItem Value="SG" Text="Singapore" />
                        <wijmo:C1ComboBoxItem Value="SK" Text="Slovakia (Slovak Republic)" />
                        <wijmo:C1ComboBoxItem Value="SI" Text="Slovenia" />
                        <wijmo:C1ComboBoxItem Value="SB" Text="Solomon Islands" />
                        <wijmo:C1ComboBoxItem Value="SO" Text="Somalia" />
                        <wijmo:C1ComboBoxItem Value="ZA" Text="South Africa" />
                        <wijmo:C1ComboBoxItem Value="GS" Text="South Georgia , S Sandwich Is." />
                        <wijmo:C1ComboBoxItem Value="ES" Text="Spain" />
                        <wijmo:C1ComboBoxItem Value="LK" Text="Sri Lanka" />
                        <wijmo:C1ComboBoxItem Value="SH" Text="St. Helena" />
                        <wijmo:C1ComboBoxItem Value="PM" Text="St. Pierre And Miquelon" />
                        <wijmo:C1ComboBoxItem Value="SD" Text="Sudan" />
                        <wijmo:C1ComboBoxItem Value="SR" Text="Suriname" />
                        <wijmo:C1ComboBoxItem Value="SJ" Text="Svalbard, Jan Mayen Islands" />
                        <wijmo:C1ComboBoxItem Value="SZ" Text="Sw Aziland" />
                        <wijmo:C1ComboBoxItem Value="SE" Text="Sweden" />
                        <wijmo:C1ComboBoxItem Value="CH" Text="Switzerland" />
                        <wijmo:C1ComboBoxItem Value="SY" Text="Syrian Arab Republic" />
                        <wijmo:C1ComboBoxItem Value="TW" Text="Taiwan" />
                        <wijmo:C1ComboBoxItem Value="TJ" Text="Tajikistan" />
                        <wijmo:C1ComboBoxItem Value="TZ" Text="Tanzania, United Republic Of" />
                        <wijmo:C1ComboBoxItem Value="TH" Text="Thailand" />
                        <wijmo:C1ComboBoxItem Value="TG" Text="Togo" />
                        <wijmo:C1ComboBoxItem Value="TK" Text="Tokelau" />
                        <wijmo:C1ComboBoxItem Value="TO" Text="Tonga" />
                        <wijmo:C1ComboBoxItem Value="TT" Text="Trinidad And Tobago" />
                        <wijmo:C1ComboBoxItem Value="TN" Text="Tunisia" />
                        <wijmo:C1ComboBoxItem Value="TR" Text="Turkey" />
                        <wijmo:C1ComboBoxItem Value="TM" Text="Turkmenistan" />
                        <wijmo:C1ComboBoxItem Value="TC" Text="Turks And Caicos Islands" />
                        <wijmo:C1ComboBoxItem Value="TV" Text="Tuvalu" />
                        <wijmo:C1ComboBoxItem Value="UG" Text="Uganda" />
                        <wijmo:C1ComboBoxItem Value="UA" Text="Ukraine" />
                        <wijmo:C1ComboBoxItem Value="AE" Text="United Arab Emirates" />
                        <wijmo:C1ComboBoxItem Value="GB" Text="United Kingdom" />
                        <wijmo:C1ComboBoxItem Value="US" Text="United States" />
                        <wijmo:C1ComboBoxItem Value="UM" Text="United States Minor Is." />
                        <wijmo:C1ComboBoxItem Value="UY" Text="Uruguay" />
                        <wijmo:C1ComboBoxItem Value="UZ" Text="Uzbekistan" />
                        <wijmo:C1ComboBoxItem Value="VU" Text="Vanuatu" />
                        <wijmo:C1ComboBoxItem Value="VE" Text="Venezuela" />
                        <wijmo:C1ComboBoxItem Value="VN" Text="Viet Nam" />
                        <wijmo:C1ComboBoxItem Value="VG" Text="Virgin Islands (British)" />
                        <wijmo:C1ComboBoxItem Value="VI" Text="Virgin Islands (U.S.)" />
                        <wijmo:C1ComboBoxItem Value="WF" Text="Wallis And Futuna Islands" />
                        <wijmo:C1ComboBoxItem Value="EH" Text="Western Sahara" />
                        <wijmo:C1ComboBoxItem Value="YE" Text="Yemen" />
                        <wijmo:C1ComboBoxItem Value="YU" Text="Yugoslavia" />
                        <wijmo:C1ComboBoxItem Value="ZR" Text="Zaire" />
                        <wijmo:C1ComboBoxItem Value="ZM" Text="Zambia" />
                        <wijmo:C1ComboBoxItem Value="ZW" Text="Zimbabwe" />
                    </Items>
                </wijmo:C1ComboBox>
            </td>
        </tr>
        <tr>
        <td colspan="4" ><b>This is the address that e-Clipse online Head Office will use to send  all related correspondence for this UMA account.</b></td>
        </tr>

    </table>
</asp:Panel>
<br />
<hr />
<asp:Panel runat="server" ID="pnlDuplicateMialingAddress" Visible="true">
    <table width="100%">
        <tr>
            <td colspan="2">
                <b> Address for Bankwest Statements: </b><br />
            </td>
        </tr>
        <tr>
            <td width="10%">
                Address Line 1:
            </td>
            <td colspan="3">
                <asp:TextBox runat="server" ID="txtAddressLine1SigAddressDulplicateMailing" Width="90%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="10%">
                Address Line 2:
            </td>
            <td colspan="3">
                <asp:TextBox runat="server" ID="txtAddressLine2SigAddressDulplicateMailing" Width="90%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="10%">
                Suburb:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSuburbSigAddressDulplicateMailing" Width="250px"></asp:TextBox>
            </td>
            <td>
                State:
            </td>
            <td>
                <wijmo:C1ComboBox ID="C1ComboBoxSigAddressDulplicateMailingState" runat="server" Width="250px">
                    <Items>
                        <wijmo:C1ComboBoxItem Value="ACT" Text="ACT" />
                        <wijmo:C1ComboBoxItem Value="NSW" Text="NSW" />
                        <wijmo:C1ComboBoxItem Value="NT" Text="NT" />
                        <wijmo:C1ComboBoxItem Value="QLD" Text="QLD" />
                        <wijmo:C1ComboBoxItem Value="SA" Text="SA" />
                        <wijmo:C1ComboBoxItem Value="TAS" Text="TAS" />
                        <wijmo:C1ComboBoxItem Value="VIC" Text="VIC" />
                        <wijmo:C1ComboBoxItem Value="WA" Text="WA" />
                    </Items>
                </wijmo:C1ComboBox>
            </td>
        </tr>
        <tr>
            <td>
                Postcode:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPostCodeSigAddressDulplicateMailing" Width="250px"></asp:TextBox>
            </td>
            <td>
                Country:
            </td>
            <td>
                <wijmo:C1ComboBox ID="C1ComboBoxCountryListSigAddressDulplicateMailing" runat="server" Width="250px">
                    <Items>
                        <wijmo:C1ComboBoxItem Value="AF" Text="Afghanistan" />
                        <wijmo:C1ComboBoxItem Value="AL" Text="Albania" />
                        <wijmo:C1ComboBoxItem Value="DZ" Text="Algeria" />
                        <wijmo:C1ComboBoxItem Value="AS" Text="American Samoa" />
                        <wijmo:C1ComboBoxItem Value="AD" Text="Andorra" />
                        <wijmo:C1ComboBoxItem Value="AO" Text="Angola" />
                        <wijmo:C1ComboBoxItem Value="AI" Text="Anguilla" />
                        <wijmo:C1ComboBoxItem Value="AQ" Text="Antarctica" />
                        <wijmo:C1ComboBoxItem Value="AG" Text="Antigua And Barbuda" />
                        <wijmo:C1ComboBoxItem Value="AR" Text="Argentina" />
                        <wijmo:C1ComboBoxItem Value="AM" Text="Armenia" />
                        <wijmo:C1ComboBoxItem Value="AW" Text="Aruba" />
                        <wijmo:C1ComboBoxItem Value="AU" Text="Australia" Selected="True" />
                        <wijmo:C1ComboBoxItem Value="AT" Text="Austria" />
                        <wijmo:C1ComboBoxItem Value="AZ" Text="Azerbaijan" />
                        <wijmo:C1ComboBoxItem Value="BS" Text="Bahamas" />
                        <wijmo:C1ComboBoxItem Value="BH" Text="Bahrain" />
                        <wijmo:C1ComboBoxItem Value="BD" Text="Bangladesh" />
                        <wijmo:C1ComboBoxItem Value="BB" Text="Barbados" />
                        <wijmo:C1ComboBoxItem Value="BY" Text="Belarus" />
                        <wijmo:C1ComboBoxItem Value="BE" Text="Belgium" />
                        <wijmo:C1ComboBoxItem Value="BZ" Text="Belize" />
                        <wijmo:C1ComboBoxItem Value="BJ" Text="Benin" />
                        <wijmo:C1ComboBoxItem Value="BM" Text="Bermuda" />
                        <wijmo:C1ComboBoxItem Value="BT" Text="Bhutan" />
                        <wijmo:C1ComboBoxItem Value="BO" Text="Bolivia" />
                        <wijmo:C1ComboBoxItem Value="BA" Text="Bosnia And Herzegowina" />
                        <wijmo:C1ComboBoxItem Value="BW" Text="Botswana" />
                        <wijmo:C1ComboBoxItem Value="BV" Text="Bouvet Island" />
                        <wijmo:C1ComboBoxItem Value="BR" Text="Brazil" />
                        <wijmo:C1ComboBoxItem Value="IO" Text="British Indian Ocean Territory" />
                        <wijmo:C1ComboBoxItem Value="BN" Text="Brunei Darussalam" />
                        <wijmo:C1ComboBoxItem Value="BG" Text="Bulgaria" />
                        <wijmo:C1ComboBoxItem Value="BF" Text="Burkina Faso" />
                        <wijmo:C1ComboBoxItem Value="BI" Text="Burundi" />
                        <wijmo:C1ComboBoxItem Value="KH" Text="Cambodia" />
                        <wijmo:C1ComboBoxItem Value="CM" Text="Cameroon" />
                        <wijmo:C1ComboBoxItem Value="CA" Text="Canada" />
                        <wijmo:C1ComboBoxItem Value="CV" Text="Cape Verde" />
                        <wijmo:C1ComboBoxItem Value="KY" Text="Cayman Islands" />
                        <wijmo:C1ComboBoxItem Value="CF" Text="Central African Republic" />
                        <wijmo:C1ComboBoxItem Value="TD" Text="Chad" />
                        <wijmo:C1ComboBoxItem Value="CL" Text="Chile" />
                        <wijmo:C1ComboBoxItem Value="CN" Text="China" />
                        <wijmo:C1ComboBoxItem Value="CX" Text="Christmas Island" />
                        <wijmo:C1ComboBoxItem Value="CC" Text="Cocos (Keeling) Islands" />
                        <wijmo:C1ComboBoxItem Value="CO" Text="Colombia" />
                        <wijmo:C1ComboBoxItem Value="KM" Text="Comoros" />
                        <wijmo:C1ComboBoxItem Value="CG" Text="Congo" />
                        <wijmo:C1ComboBoxItem Value="CK" Text="Cook Islands" />
                        <wijmo:C1ComboBoxItem Value="CR" Text="Costa Rica" />
                        <wijmo:C1ComboBoxItem Value="CI" Text="Cote D'Ivoire" />
                        <wijmo:C1ComboBoxItem Value="HR" Text="Croatia (Local Name: Hrvatska)" />
                        <wijmo:C1ComboBoxItem Value="CU" Text="Cuba" />
                        <wijmo:C1ComboBoxItem Value="CY" Text="Cyprus" />
                        <wijmo:C1ComboBoxItem Value="CZ" Text="Czech Republic" />
                        <wijmo:C1ComboBoxItem Value="DK" Text="Denmark" />
                        <wijmo:C1ComboBoxItem Value="DJ" Text="Djibouti" />
                        <wijmo:C1ComboBoxItem Value="DM" Text="Dominica" />
                        <wijmo:C1ComboBoxItem Value="DO" Text="Dominican Republic" />
                        <wijmo:C1ComboBoxItem Value="TP" Text="East Timor" />
                        <wijmo:C1ComboBoxItem Value="EC" Text="Ecuador" />
                        <wijmo:C1ComboBoxItem Value="EG" Text="Egypt" />
                        <wijmo:C1ComboBoxItem Value="SV" Text="El Salvador" />
                        <wijmo:C1ComboBoxItem Value="GQ" Text="Equatorial Guinea" />
                        <wijmo:C1ComboBoxItem Value="ER" Text="Eritrea" />
                        <wijmo:C1ComboBoxItem Value="EE" Text="Estonia" />
                        <wijmo:C1ComboBoxItem Value="ET" Text="Ethiopia" />
                        <wijmo:C1ComboBoxItem Value="FK" Text="Falkland Islands (Malvinas)" />
                        <wijmo:C1ComboBoxItem Value="FO" Text="Faroe Islands" />
                        <wijmo:C1ComboBoxItem Value="FJ" Text="Fiji" />
                        <wijmo:C1ComboBoxItem Value="FI" Text="Finland" />
                        <wijmo:C1ComboBoxItem Value="FR" Text="France" />
                        <wijmo:C1ComboBoxItem Value="GF" Text="French Guiana" />
                        <wijmo:C1ComboBoxItem Value="PF" Text="French Polynesia" />
                        <wijmo:C1ComboBoxItem Value="TF" Text="French Southern Territories" />
                        <wijmo:C1ComboBoxItem Value="GA" Text="Gabon" />
                        <wijmo:C1ComboBoxItem Value="GM" Text="Gambia" />
                        <wijmo:C1ComboBoxItem Value="GE" Text="Georgia" />
                        <wijmo:C1ComboBoxItem Value="DE" Text="Germany" />
                        <wijmo:C1ComboBoxItem Value="GH" Text="Ghana" />
                        <wijmo:C1ComboBoxItem Value="GI" Text="Gibraltar" />
                        <wijmo:C1ComboBoxItem Value="GR" Text="Greece" />
                        <wijmo:C1ComboBoxItem Value="GL" Text="Greenland" />
                        <wijmo:C1ComboBoxItem Value="GD" Text="Grenada" />
                        <wijmo:C1ComboBoxItem Value="GP" Text="Guadeloupe" />
                        <wijmo:C1ComboBoxItem Value="GU" Text="Guam" />
                        <wijmo:C1ComboBoxItem Value="GT" Text="Guatemala" />
                        <wijmo:C1ComboBoxItem Value="GN" Text="Guinea" />
                        <wijmo:C1ComboBoxItem Value="GW" Text="Guinea-Bissau" />
                        <wijmo:C1ComboBoxItem Value="GY" Text="Guyana" />
                        <wijmo:C1ComboBoxItem Value="HT" Text="Haiti" />
                        <wijmo:C1ComboBoxItem Value="HM" Text="Heard And Mc Donald Islands" />
                        <wijmo:C1ComboBoxItem Value="VA" Text="Holy See (Vatican City State)" />
                        <wijmo:C1ComboBoxItem Value="HN" Text="Honduras" />
                        <wijmo:C1ComboBoxItem Value="HK" Text="Hong Kong" />
                        <wijmo:C1ComboBoxItem Value="HU" Text="Hungary" />
                        <wijmo:C1ComboBoxItem Value="IS" Text="Icel And" />
                        <wijmo:C1ComboBoxItem Value="IN" Text="India" />
                        <wijmo:C1ComboBoxItem Value="ID" Text="Indonesia" />
                        <wijmo:C1ComboBoxItem Value="IR" Text="Iran (Islamic Republic Of)" />
                        <wijmo:C1ComboBoxItem Value="IQ" Text="Iraq" />
                        <wijmo:C1ComboBoxItem Value="IE" Text="Ireland" />
                        <wijmo:C1ComboBoxItem Value="IL" Text="Israel" />
                        <wijmo:C1ComboBoxItem Value="IT" Text="Italy" />
                        <wijmo:C1ComboBoxItem Value="JM" Text="Jamaica" />
                        <wijmo:C1ComboBoxItem Value="JP" Text="Japan" />
                        <wijmo:C1ComboBoxItem Value="JO" Text="Jordan" />
                        <wijmo:C1ComboBoxItem Value="KZ" Text="Kazakhstan" />
                        <wijmo:C1ComboBoxItem Value="KE" Text="Kenya" />
                        <wijmo:C1ComboBoxItem Value="KI" Text="Kiribati" />
                        <wijmo:C1ComboBoxItem Value="KP" Text="Korea, Dem People'S Republic" />
                        <wijmo:C1ComboBoxItem Value="KR" Text="Korea, Republic Of" />
                        <wijmo:C1ComboBoxItem Value="KW" Text="Kuwait" />
                        <wijmo:C1ComboBoxItem Value="KG" Text="Kyrgyzstan" />
                        <wijmo:C1ComboBoxItem Value="LA" Text="Lao People'S Dem Republic" />
                        <wijmo:C1ComboBoxItem Value="LV" Text="Latvia" />
                        <wijmo:C1ComboBoxItem Value="LB" Text="Lebanon" />
                        <wijmo:C1ComboBoxItem Value="LS" Text="Lesotho" />
                        <wijmo:C1ComboBoxItem Value="LR" Text="Liberia" />
                        <wijmo:C1ComboBoxItem Value="LY" Text="Libyan Arab Jamahiriya" />
                        <wijmo:C1ComboBoxItem Value="LI" Text="Liechtenstein" />
                        <wijmo:C1ComboBoxItem Value="LT" Text="Lithuania" />
                        <wijmo:C1ComboBoxItem Value="LU" Text="Luxembourg" />
                        <wijmo:C1ComboBoxItem Value="MO" Text="Macau" />
                        <wijmo:C1ComboBoxItem Value="MK" Text="Macedonia" />
                        <wijmo:C1ComboBoxItem Value="MG" Text="Madagascar" />
                        <wijmo:C1ComboBoxItem Value="MW" Text="Malawi" />
                        <wijmo:C1ComboBoxItem Value="MY" Text="Malaysia" />
                        <wijmo:C1ComboBoxItem Value="MV" Text="Maldives" />
                        <wijmo:C1ComboBoxItem Value="ML" Text="Mali" />
                        <wijmo:C1ComboBoxItem Value="MT" Text="Malta" />
                        <wijmo:C1ComboBoxItem Value="MH" Text="Marshall Islands" />
                        <wijmo:C1ComboBoxItem Value="MQ" Text="Martinique" />
                        <wijmo:C1ComboBoxItem Value="MR" Text="Mauritania" />
                        <wijmo:C1ComboBoxItem Value="MU" Text="Mauritius" />
                        <wijmo:C1ComboBoxItem Value="YT" Text="Mayotte" />
                        <wijmo:C1ComboBoxItem Value="MX" Text="Mexico" />
                        <wijmo:C1ComboBoxItem Value="FM" Text="Micronesia, Federated States" />
                        <wijmo:C1ComboBoxItem Value="MD" Text="Moldova, Republic Of" />
                        <wijmo:C1ComboBoxItem Value="MC" Text="Monaco" />
                        <wijmo:C1ComboBoxItem Value="MN" Text="Mongolia" />
                        <wijmo:C1ComboBoxItem Value="MS" Text="Montserrat" />
                        <wijmo:C1ComboBoxItem Value="MA" Text="Morocco" />
                        <wijmo:C1ComboBoxItem Value="MZ" Text="Mozambique" />
                        <wijmo:C1ComboBoxItem Value="MM" Text="Myanmar" />
                        <wijmo:C1ComboBoxItem Value="NA" Text="Namibia" />
                        <wijmo:C1ComboBoxItem Value="NR" Text="Nauru" />
                        <wijmo:C1ComboBoxItem Value="NP" Text="Nepal" />
                        <wijmo:C1ComboBoxItem Value="NL" Text="Netherlands" />
                        <wijmo:C1ComboBoxItem Value="AN" Text="Netherlands Ant Illes" />
                        <wijmo:C1ComboBoxItem Value="NC" Text="New Caledonia" />
                        <wijmo:C1ComboBoxItem Value="NZ" Text="New Zealand" />
                        <wijmo:C1ComboBoxItem Value="NI" Text="Nicaragua" />
                        <wijmo:C1ComboBoxItem Value="NE" Text="Niger" />
                        <wijmo:C1ComboBoxItem Value="NG" Text="Nigeria" />
                        <wijmo:C1ComboBoxItem Value="NU" Text="Niue" />
                        <wijmo:C1ComboBoxItem Value="NF" Text="Norfolk Island" />
                        <wijmo:C1ComboBoxItem Value="MP" Text="Northern Mariana Islands" />
                        <wijmo:C1ComboBoxItem Value="NO" Text="Norway" />
                        <wijmo:C1ComboBoxItem Value="OM" Text="Oman" />
                        <wijmo:C1ComboBoxItem Value="PK" Text="Pakistan" />
                        <wijmo:C1ComboBoxItem Value="PW" Text="Palau" />
                        <wijmo:C1ComboBoxItem Value="PA" Text="Panama" />
                        <wijmo:C1ComboBoxItem Value="PG" Text="Papua New Guinea" />
                        <wijmo:C1ComboBoxItem Value="PY" Text="Paraguay" />
                        <wijmo:C1ComboBoxItem Value="PE" Text="Peru" />
                        <wijmo:C1ComboBoxItem Value="PH" Text="Philippines" />
                        <wijmo:C1ComboBoxItem Value="PN" Text="Pitcairn" />
                        <wijmo:C1ComboBoxItem Value="PL" Text="Poland" />
                        <wijmo:C1ComboBoxItem Value="PT" Text="Portugal" />
                        <wijmo:C1ComboBoxItem Value="PR" Text="Puerto Rico" />
                        <wijmo:C1ComboBoxItem Value="QA" Text="Qatar" />
                        <wijmo:C1ComboBoxItem Value="RE" Text="Reunion" />
                        <wijmo:C1ComboBoxItem Value="RO" Text="Romania" />
                        <wijmo:C1ComboBoxItem Value="RU" Text="Russian Federation" />
                        <wijmo:C1ComboBoxItem Value="RW" Text="Rwanda" />
                        <wijmo:C1ComboBoxItem Value="KN" Text="Saint K Itts And Nevis" />
                        <wijmo:C1ComboBoxItem Value="LC" Text="Saint Lucia" />
                        <wijmo:C1ComboBoxItem Value="VC" Text="Saint Vincent, The Grenadines" />
                        <wijmo:C1ComboBoxItem Value="WS" Text="Samoa" />
                        <wijmo:C1ComboBoxItem Value="SM" Text="San Marino" />
                        <wijmo:C1ComboBoxItem Value="ST" Text="Sao Tome And Principe" />
                        <wijmo:C1ComboBoxItem Value="SA" Text="Saudi Arabia" />
                        <wijmo:C1ComboBoxItem Value="SN" Text="Senegal" />
                        <wijmo:C1ComboBoxItem Value="SC" Text="Seychelles" />
                        <wijmo:C1ComboBoxItem Value="SL" Text="Sierra Leone" />
                        <wijmo:C1ComboBoxItem Value="SG" Text="Singapore" />
                        <wijmo:C1ComboBoxItem Value="SK" Text="Slovakia (Slovak Republic)" />
                        <wijmo:C1ComboBoxItem Value="SI" Text="Slovenia" />
                        <wijmo:C1ComboBoxItem Value="SB" Text="Solomon Islands" />
                        <wijmo:C1ComboBoxItem Value="SO" Text="Somalia" />
                        <wijmo:C1ComboBoxItem Value="ZA" Text="South Africa" />
                        <wijmo:C1ComboBoxItem Value="GS" Text="South Georgia , S Sandwich Is." />
                        <wijmo:C1ComboBoxItem Value="ES" Text="Spain" />
                        <wijmo:C1ComboBoxItem Value="LK" Text="Sri Lanka" />
                        <wijmo:C1ComboBoxItem Value="SH" Text="St. Helena" />
                        <wijmo:C1ComboBoxItem Value="PM" Text="St. Pierre And Miquelon" />
                        <wijmo:C1ComboBoxItem Value="SD" Text="Sudan" />
                        <wijmo:C1ComboBoxItem Value="SR" Text="Suriname" />
                        <wijmo:C1ComboBoxItem Value="SJ" Text="Svalbard, Jan Mayen Islands" />
                        <wijmo:C1ComboBoxItem Value="SZ" Text="Sw Aziland" />
                        <wijmo:C1ComboBoxItem Value="SE" Text="Sweden" />
                        <wijmo:C1ComboBoxItem Value="CH" Text="Switzerland" />
                        <wijmo:C1ComboBoxItem Value="SY" Text="Syrian Arab Republic" />
                        <wijmo:C1ComboBoxItem Value="TW" Text="Taiwan" />
                        <wijmo:C1ComboBoxItem Value="TJ" Text="Tajikistan" />
                        <wijmo:C1ComboBoxItem Value="TZ" Text="Tanzania, United Republic Of" />
                        <wijmo:C1ComboBoxItem Value="TH" Text="Thailand" />
                        <wijmo:C1ComboBoxItem Value="TG" Text="Togo" />
                        <wijmo:C1ComboBoxItem Value="TK" Text="Tokelau" />
                        <wijmo:C1ComboBoxItem Value="TO" Text="Tonga" />
                        <wijmo:C1ComboBoxItem Value="TT" Text="Trinidad And Tobago" />
                        <wijmo:C1ComboBoxItem Value="TN" Text="Tunisia" />
                        <wijmo:C1ComboBoxItem Value="TR" Text="Turkey" />
                        <wijmo:C1ComboBoxItem Value="TM" Text="Turkmenistan" />
                        <wijmo:C1ComboBoxItem Value="TC" Text="Turks And Caicos Islands" />
                        <wijmo:C1ComboBoxItem Value="TV" Text="Tuvalu" />
                        <wijmo:C1ComboBoxItem Value="UG" Text="Uganda" />
                        <wijmo:C1ComboBoxItem Value="UA" Text="Ukraine" />
                        <wijmo:C1ComboBoxItem Value="AE" Text="United Arab Emirates" />
                        <wijmo:C1ComboBoxItem Value="GB" Text="United Kingdom" />
                        <wijmo:C1ComboBoxItem Value="US" Text="United States" />
                        <wijmo:C1ComboBoxItem Value="UM" Text="United States Minor Is." />
                        <wijmo:C1ComboBoxItem Value="UY" Text="Uruguay" />
                        <wijmo:C1ComboBoxItem Value="UZ" Text="Uzbekistan" />
                        <wijmo:C1ComboBoxItem Value="VU" Text="Vanuatu" />
                        <wijmo:C1ComboBoxItem Value="VE" Text="Venezuela" />
                        <wijmo:C1ComboBoxItem Value="VN" Text="Viet Nam" />
                        <wijmo:C1ComboBoxItem Value="VG" Text="Virgin Islands (British)" />
                        <wijmo:C1ComboBoxItem Value="VI" Text="Virgin Islands (U.S.)" />
                        <wijmo:C1ComboBoxItem Value="WF" Text="Wallis And Futuna Islands" />
                        <wijmo:C1ComboBoxItem Value="EH" Text="Western Sahara" />
                        <wijmo:C1ComboBoxItem Value="YE" Text="Yemen" />
                        <wijmo:C1ComboBoxItem Value="YU" Text="Yugoslavia" />
                        <wijmo:C1ComboBoxItem Value="ZR" Text="Zaire" />
                        <wijmo:C1ComboBoxItem Value="ZM" Text="Zambia" />
                        <wijmo:C1ComboBoxItem Value="ZW" Text="Zimbabwe" />
                    </Items>
                </wijmo:C1ComboBox>
            </td>
        </tr>

        <tr>
        <td colspan="4" ><b>Original Bankwest statements are sent to e-Clipse Head Office.  If you want to receive duplicate Bankwest statements quarterly please provide this postal address</b></td>
        </tr>
    </table>
</asp:Panel>
<br /><hr />
<asp:Panel runat="server" ID="pnlRegisteredAddress" Visible="true">
    <table width="100%">
        <tr>
            <td colspan="2">
                <b>Registered Address of the  company /Corporate Trustee as per AISIC register (PO Box, RMB or c/- is not sufficient)</b><br />
            </td>
        </tr>
        <tr>
            <td width="10%">
                Address Line 1:
            </td>
            <td colspan="3">
                <asp:TextBox runat="server" ID="txtAddressLine1RegisteredAddress" Width="90%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="10%">
                Address Line 2:
            </td>
            <td colspan="3">
                <asp:TextBox runat="server" ID="txtAddressLine2RegisteredAddress" Width="90%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="10%">
                Suburb:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSuburbRegisteredAddress" Width="250px"></asp:TextBox>
            </td>
            <td>
                State:
            </td>
            <td>
                <wijmo:C1ComboBox ID="C1ComboBoxRegisteredAddressState" runat="server" Width="250px">
                    <Items>
                        <wijmo:C1ComboBoxItem Value="ACT" Text="ACT" />
                        <wijmo:C1ComboBoxItem Value="NSW" Text="NSW" />
                        <wijmo:C1ComboBoxItem Value="NT" Text="NT" />
                        <wijmo:C1ComboBoxItem Value="QLD" Text="QLD" />
                        <wijmo:C1ComboBoxItem Value="SA" Text="SA" />
                        <wijmo:C1ComboBoxItem Value="TAS" Text="TAS" />
                        <wijmo:C1ComboBoxItem Value="VIC" Text="VIC" />
                        <wijmo:C1ComboBoxItem Value="WA" Text="WA" />
                    </Items>
                </wijmo:C1ComboBox>
            </td>
        </tr>
        <tr>
            <td>
                Postcode:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPostCodeRegisteredAddress" Width="250px"></asp:TextBox>
            </td>
            <td>
                Country:
            </td>
            <td>
                <wijmo:C1ComboBox ID="C1ComboBoxRegisteredAddress" runat="server" Width="250px">
                    <Items>
                        <wijmo:C1ComboBoxItem Value="AF" Text="Afghanistan" />
                        <wijmo:C1ComboBoxItem Value="AL" Text="Albania" />
                        <wijmo:C1ComboBoxItem Value="DZ" Text="Algeria" />
                        <wijmo:C1ComboBoxItem Value="AS" Text="American Samoa" />
                        <wijmo:C1ComboBoxItem Value="AD" Text="Andorra" />
                        <wijmo:C1ComboBoxItem Value="AO" Text="Angola" />
                        <wijmo:C1ComboBoxItem Value="AI" Text="Anguilla" />
                        <wijmo:C1ComboBoxItem Value="AQ" Text="Antarctica" />
                        <wijmo:C1ComboBoxItem Value="AG" Text="Antigua And Barbuda" />
                        <wijmo:C1ComboBoxItem Value="AR" Text="Argentina" />
                        <wijmo:C1ComboBoxItem Value="AM" Text="Armenia" />
                        <wijmo:C1ComboBoxItem Value="AW" Text="Aruba" />
                        <wijmo:C1ComboBoxItem Value="AU" Text="Australia" Selected="True" />
                        <wijmo:C1ComboBoxItem Value="AT" Text="Austria" />
                        <wijmo:C1ComboBoxItem Value="AZ" Text="Azerbaijan" />
                        <wijmo:C1ComboBoxItem Value="BS" Text="Bahamas" />
                        <wijmo:C1ComboBoxItem Value="BH" Text="Bahrain" />
                        <wijmo:C1ComboBoxItem Value="BD" Text="Bangladesh" />
                        <wijmo:C1ComboBoxItem Value="BB" Text="Barbados" />
                        <wijmo:C1ComboBoxItem Value="BY" Text="Belarus" />
                        <wijmo:C1ComboBoxItem Value="BE" Text="Belgium" />
                        <wijmo:C1ComboBoxItem Value="BZ" Text="Belize" />
                        <wijmo:C1ComboBoxItem Value="BJ" Text="Benin" />
                        <wijmo:C1ComboBoxItem Value="BM" Text="Bermuda" />
                        <wijmo:C1ComboBoxItem Value="BT" Text="Bhutan" />
                        <wijmo:C1ComboBoxItem Value="BO" Text="Bolivia" />
                        <wijmo:C1ComboBoxItem Value="BA" Text="Bosnia And Herzegowina" />
                        <wijmo:C1ComboBoxItem Value="BW" Text="Botswana" />
                        <wijmo:C1ComboBoxItem Value="BV" Text="Bouvet Island" />
                        <wijmo:C1ComboBoxItem Value="BR" Text="Brazil" />
                        <wijmo:C1ComboBoxItem Value="IO" Text="British Indian Ocean Territory" />
                        <wijmo:C1ComboBoxItem Value="BN" Text="Brunei Darussalam" />
                        <wijmo:C1ComboBoxItem Value="BG" Text="Bulgaria" />
                        <wijmo:C1ComboBoxItem Value="BF" Text="Burkina Faso" />
                        <wijmo:C1ComboBoxItem Value="BI" Text="Burundi" />
                        <wijmo:C1ComboBoxItem Value="KH" Text="Cambodia" />
                        <wijmo:C1ComboBoxItem Value="CM" Text="Cameroon" />
                        <wijmo:C1ComboBoxItem Value="CA" Text="Canada" />
                        <wijmo:C1ComboBoxItem Value="CV" Text="Cape Verde" />
                        <wijmo:C1ComboBoxItem Value="KY" Text="Cayman Islands" />
                        <wijmo:C1ComboBoxItem Value="CF" Text="Central African Republic" />
                        <wijmo:C1ComboBoxItem Value="TD" Text="Chad" />
                        <wijmo:C1ComboBoxItem Value="CL" Text="Chile" />
                        <wijmo:C1ComboBoxItem Value="CN" Text="China" />
                        <wijmo:C1ComboBoxItem Value="CX" Text="Christmas Island" />
                        <wijmo:C1ComboBoxItem Value="CC" Text="Cocos (Keeling) Islands" />
                        <wijmo:C1ComboBoxItem Value="CO" Text="Colombia" />
                        <wijmo:C1ComboBoxItem Value="KM" Text="Comoros" />
                        <wijmo:C1ComboBoxItem Value="CG" Text="Congo" />
                        <wijmo:C1ComboBoxItem Value="CK" Text="Cook Islands" />
                        <wijmo:C1ComboBoxItem Value="CR" Text="Costa Rica" />
                        <wijmo:C1ComboBoxItem Value="CI" Text="Cote D'Ivoire" />
                        <wijmo:C1ComboBoxItem Value="HR" Text="Croatia (Local Name: Hrvatska)" />
                        <wijmo:C1ComboBoxItem Value="CU" Text="Cuba" />
                        <wijmo:C1ComboBoxItem Value="CY" Text="Cyprus" />
                        <wijmo:C1ComboBoxItem Value="CZ" Text="Czech Republic" />
                        <wijmo:C1ComboBoxItem Value="DK" Text="Denmark" />
                        <wijmo:C1ComboBoxItem Value="DJ" Text="Djibouti" />
                        <wijmo:C1ComboBoxItem Value="DM" Text="Dominica" />
                        <wijmo:C1ComboBoxItem Value="DO" Text="Dominican Republic" />
                        <wijmo:C1ComboBoxItem Value="TP" Text="East Timor" />
                        <wijmo:C1ComboBoxItem Value="EC" Text="Ecuador" />
                        <wijmo:C1ComboBoxItem Value="EG" Text="Egypt" />
                        <wijmo:C1ComboBoxItem Value="SV" Text="El Salvador" />
                        <wijmo:C1ComboBoxItem Value="GQ" Text="Equatorial Guinea" />
                        <wijmo:C1ComboBoxItem Value="ER" Text="Eritrea" />
                        <wijmo:C1ComboBoxItem Value="EE" Text="Estonia" />
                        <wijmo:C1ComboBoxItem Value="ET" Text="Ethiopia" />
                        <wijmo:C1ComboBoxItem Value="FK" Text="Falkland Islands (Malvinas)" />
                        <wijmo:C1ComboBoxItem Value="FO" Text="Faroe Islands" />
                        <wijmo:C1ComboBoxItem Value="FJ" Text="Fiji" />
                        <wijmo:C1ComboBoxItem Value="FI" Text="Finland" />
                        <wijmo:C1ComboBoxItem Value="FR" Text="France" />
                        <wijmo:C1ComboBoxItem Value="GF" Text="French Guiana" />
                        <wijmo:C1ComboBoxItem Value="PF" Text="French Polynesia" />
                        <wijmo:C1ComboBoxItem Value="TF" Text="French Southern Territories" />
                        <wijmo:C1ComboBoxItem Value="GA" Text="Gabon" />
                        <wijmo:C1ComboBoxItem Value="GM" Text="Gambia" />
                        <wijmo:C1ComboBoxItem Value="GE" Text="Georgia" />
                        <wijmo:C1ComboBoxItem Value="DE" Text="Germany" />
                        <wijmo:C1ComboBoxItem Value="GH" Text="Ghana" />
                        <wijmo:C1ComboBoxItem Value="GI" Text="Gibraltar" />
                        <wijmo:C1ComboBoxItem Value="GR" Text="Greece" />
                        <wijmo:C1ComboBoxItem Value="GL" Text="Greenland" />
                        <wijmo:C1ComboBoxItem Value="GD" Text="Grenada" />
                        <wijmo:C1ComboBoxItem Value="GP" Text="Guadeloupe" />
                        <wijmo:C1ComboBoxItem Value="GU" Text="Guam" />
                        <wijmo:C1ComboBoxItem Value="GT" Text="Guatemala" />
                        <wijmo:C1ComboBoxItem Value="GN" Text="Guinea" />
                        <wijmo:C1ComboBoxItem Value="GW" Text="Guinea-Bissau" />
                        <wijmo:C1ComboBoxItem Value="GY" Text="Guyana" />
                        <wijmo:C1ComboBoxItem Value="HT" Text="Haiti" />
                        <wijmo:C1ComboBoxItem Value="HM" Text="Heard And Mc Donald Islands" />
                        <wijmo:C1ComboBoxItem Value="VA" Text="Holy See (Vatican City State)" />
                        <wijmo:C1ComboBoxItem Value="HN" Text="Honduras" />
                        <wijmo:C1ComboBoxItem Value="HK" Text="Hong Kong" />
                        <wijmo:C1ComboBoxItem Value="HU" Text="Hungary" />
                        <wijmo:C1ComboBoxItem Value="IS" Text="Icel And" />
                        <wijmo:C1ComboBoxItem Value="IN" Text="India" />
                        <wijmo:C1ComboBoxItem Value="ID" Text="Indonesia" />
                        <wijmo:C1ComboBoxItem Value="IR" Text="Iran (Islamic Republic Of)" />
                        <wijmo:C1ComboBoxItem Value="IQ" Text="Iraq" />
                        <wijmo:C1ComboBoxItem Value="IE" Text="Ireland" />
                        <wijmo:C1ComboBoxItem Value="IL" Text="Israel" />
                        <wijmo:C1ComboBoxItem Value="IT" Text="Italy" />
                        <wijmo:C1ComboBoxItem Value="JM" Text="Jamaica" />
                        <wijmo:C1ComboBoxItem Value="JP" Text="Japan" />
                        <wijmo:C1ComboBoxItem Value="JO" Text="Jordan" />
                        <wijmo:C1ComboBoxItem Value="KZ" Text="Kazakhstan" />
                        <wijmo:C1ComboBoxItem Value="KE" Text="Kenya" />
                        <wijmo:C1ComboBoxItem Value="KI" Text="Kiribati" />
                        <wijmo:C1ComboBoxItem Value="KP" Text="Korea, Dem People'S Republic" />
                        <wijmo:C1ComboBoxItem Value="KR" Text="Korea, Republic Of" />
                        <wijmo:C1ComboBoxItem Value="KW" Text="Kuwait" />
                        <wijmo:C1ComboBoxItem Value="KG" Text="Kyrgyzstan" />
                        <wijmo:C1ComboBoxItem Value="LA" Text="Lao People'S Dem Republic" />
                        <wijmo:C1ComboBoxItem Value="LV" Text="Latvia" />
                        <wijmo:C1ComboBoxItem Value="LB" Text="Lebanon" />
                        <wijmo:C1ComboBoxItem Value="LS" Text="Lesotho" />
                        <wijmo:C1ComboBoxItem Value="LR" Text="Liberia" />
                        <wijmo:C1ComboBoxItem Value="LY" Text="Libyan Arab Jamahiriya" />
                        <wijmo:C1ComboBoxItem Value="LI" Text="Liechtenstein" />
                        <wijmo:C1ComboBoxItem Value="LT" Text="Lithuania" />
                        <wijmo:C1ComboBoxItem Value="LU" Text="Luxembourg" />
                        <wijmo:C1ComboBoxItem Value="MO" Text="Macau" />
                        <wijmo:C1ComboBoxItem Value="MK" Text="Macedonia" />
                        <wijmo:C1ComboBoxItem Value="MG" Text="Madagascar" />
                        <wijmo:C1ComboBoxItem Value="MW" Text="Malawi" />
                        <wijmo:C1ComboBoxItem Value="MY" Text="Malaysia" />
                        <wijmo:C1ComboBoxItem Value="MV" Text="Maldives" />
                        <wijmo:C1ComboBoxItem Value="ML" Text="Mali" />
                        <wijmo:C1ComboBoxItem Value="MT" Text="Malta" />
                        <wijmo:C1ComboBoxItem Value="MH" Text="Marshall Islands" />
                        <wijmo:C1ComboBoxItem Value="MQ" Text="Martinique" />
                        <wijmo:C1ComboBoxItem Value="MR" Text="Mauritania" />
                        <wijmo:C1ComboBoxItem Value="MU" Text="Mauritius" />
                        <wijmo:C1ComboBoxItem Value="YT" Text="Mayotte" />
                        <wijmo:C1ComboBoxItem Value="MX" Text="Mexico" />
                        <wijmo:C1ComboBoxItem Value="FM" Text="Micronesia, Federated States" />
                        <wijmo:C1ComboBoxItem Value="MD" Text="Moldova, Republic Of" />
                        <wijmo:C1ComboBoxItem Value="MC" Text="Monaco" />
                        <wijmo:C1ComboBoxItem Value="MN" Text="Mongolia" />
                        <wijmo:C1ComboBoxItem Value="MS" Text="Montserrat" />
                        <wijmo:C1ComboBoxItem Value="MA" Text="Morocco" />
                        <wijmo:C1ComboBoxItem Value="MZ" Text="Mozambique" />
                        <wijmo:C1ComboBoxItem Value="MM" Text="Myanmar" />
                        <wijmo:C1ComboBoxItem Value="NA" Text="Namibia" />
                        <wijmo:C1ComboBoxItem Value="NR" Text="Nauru" />
                        <wijmo:C1ComboBoxItem Value="NP" Text="Nepal" />
                        <wijmo:C1ComboBoxItem Value="NL" Text="Netherlands" />
                        <wijmo:C1ComboBoxItem Value="AN" Text="Netherlands Ant Illes" />
                        <wijmo:C1ComboBoxItem Value="NC" Text="New Caledonia" />
                        <wijmo:C1ComboBoxItem Value="NZ" Text="New Zealand" />
                        <wijmo:C1ComboBoxItem Value="NI" Text="Nicaragua" />
                        <wijmo:C1ComboBoxItem Value="NE" Text="Niger" />
                        <wijmo:C1ComboBoxItem Value="NG" Text="Nigeria" />
                        <wijmo:C1ComboBoxItem Value="NU" Text="Niue" />
                        <wijmo:C1ComboBoxItem Value="NF" Text="Norfolk Island" />
                        <wijmo:C1ComboBoxItem Value="MP" Text="Northern Mariana Islands" />
                        <wijmo:C1ComboBoxItem Value="NO" Text="Norway" />
                        <wijmo:C1ComboBoxItem Value="OM" Text="Oman" />
                        <wijmo:C1ComboBoxItem Value="PK" Text="Pakistan" />
                        <wijmo:C1ComboBoxItem Value="PW" Text="Palau" />
                        <wijmo:C1ComboBoxItem Value="PA" Text="Panama" />
                        <wijmo:C1ComboBoxItem Value="PG" Text="Papua New Guinea" />
                        <wijmo:C1ComboBoxItem Value="PY" Text="Paraguay" />
                        <wijmo:C1ComboBoxItem Value="PE" Text="Peru" />
                        <wijmo:C1ComboBoxItem Value="PH" Text="Philippines" />
                        <wijmo:C1ComboBoxItem Value="PN" Text="Pitcairn" />
                        <wijmo:C1ComboBoxItem Value="PL" Text="Poland" />
                        <wijmo:C1ComboBoxItem Value="PT" Text="Portugal" />
                        <wijmo:C1ComboBoxItem Value="PR" Text="Puerto Rico" />
                        <wijmo:C1ComboBoxItem Value="QA" Text="Qatar" />
                        <wijmo:C1ComboBoxItem Value="RE" Text="Reunion" />
                        <wijmo:C1ComboBoxItem Value="RO" Text="Romania" />
                        <wijmo:C1ComboBoxItem Value="RU" Text="Russian Federation" />
                        <wijmo:C1ComboBoxItem Value="RW" Text="Rwanda" />
                        <wijmo:C1ComboBoxItem Value="KN" Text="Saint K Itts And Nevis" />
                        <wijmo:C1ComboBoxItem Value="LC" Text="Saint Lucia" />
                        <wijmo:C1ComboBoxItem Value="VC" Text="Saint Vincent, The Grenadines" />
                        <wijmo:C1ComboBoxItem Value="WS" Text="Samoa" />
                        <wijmo:C1ComboBoxItem Value="SM" Text="San Marino" />
                        <wijmo:C1ComboBoxItem Value="ST" Text="Sao Tome And Principe" />
                        <wijmo:C1ComboBoxItem Value="SA" Text="Saudi Arabia" />
                        <wijmo:C1ComboBoxItem Value="SN" Text="Senegal" />
                        <wijmo:C1ComboBoxItem Value="SC" Text="Seychelles" />
                        <wijmo:C1ComboBoxItem Value="SL" Text="Sierra Leone" />
                        <wijmo:C1ComboBoxItem Value="SG" Text="Singapore" />
                        <wijmo:C1ComboBoxItem Value="SK" Text="Slovakia (Slovak Republic)" />
                        <wijmo:C1ComboBoxItem Value="SI" Text="Slovenia" />
                        <wijmo:C1ComboBoxItem Value="SB" Text="Solomon Islands" />
                        <wijmo:C1ComboBoxItem Value="SO" Text="Somalia" />
                        <wijmo:C1ComboBoxItem Value="ZA" Text="South Africa" />
                        <wijmo:C1ComboBoxItem Value="GS" Text="South Georgia , S Sandwich Is." />
                        <wijmo:C1ComboBoxItem Value="ES" Text="Spain" />
                        <wijmo:C1ComboBoxItem Value="LK" Text="Sri Lanka" />
                        <wijmo:C1ComboBoxItem Value="SH" Text="St. Helena" />
                        <wijmo:C1ComboBoxItem Value="PM" Text="St. Pierre And Miquelon" />
                        <wijmo:C1ComboBoxItem Value="SD" Text="Sudan" />
                        <wijmo:C1ComboBoxItem Value="SR" Text="Suriname" />
                        <wijmo:C1ComboBoxItem Value="SJ" Text="Svalbard, Jan Mayen Islands" />
                        <wijmo:C1ComboBoxItem Value="SZ" Text="Sw Aziland" />
                        <wijmo:C1ComboBoxItem Value="SE" Text="Sweden" />
                        <wijmo:C1ComboBoxItem Value="CH" Text="Switzerland" />
                        <wijmo:C1ComboBoxItem Value="SY" Text="Syrian Arab Republic" />
                        <wijmo:C1ComboBoxItem Value="TW" Text="Taiwan" />
                        <wijmo:C1ComboBoxItem Value="TJ" Text="Tajikistan" />
                        <wijmo:C1ComboBoxItem Value="TZ" Text="Tanzania, United Republic Of" />
                        <wijmo:C1ComboBoxItem Value="TH" Text="Thailand" />
                        <wijmo:C1ComboBoxItem Value="TG" Text="Togo" />
                        <wijmo:C1ComboBoxItem Value="TK" Text="Tokelau" />
                        <wijmo:C1ComboBoxItem Value="TO" Text="Tonga" />
                        <wijmo:C1ComboBoxItem Value="TT" Text="Trinidad And Tobago" />
                        <wijmo:C1ComboBoxItem Value="TN" Text="Tunisia" />
                        <wijmo:C1ComboBoxItem Value="TR" Text="Turkey" />
                        <wijmo:C1ComboBoxItem Value="TM" Text="Turkmenistan" />
                        <wijmo:C1ComboBoxItem Value="TC" Text="Turks And Caicos Islands" />
                        <wijmo:C1ComboBoxItem Value="TV" Text="Tuvalu" />
                        <wijmo:C1ComboBoxItem Value="UG" Text="Uganda" />
                        <wijmo:C1ComboBoxItem Value="UA" Text="Ukraine" />
                        <wijmo:C1ComboBoxItem Value="AE" Text="United Arab Emirates" />
                        <wijmo:C1ComboBoxItem Value="GB" Text="United Kingdom" />
                        <wijmo:C1ComboBoxItem Value="US" Text="United States" />
                        <wijmo:C1ComboBoxItem Value="UM" Text="United States Minor Is." />
                        <wijmo:C1ComboBoxItem Value="UY" Text="Uruguay" />
                        <wijmo:C1ComboBoxItem Value="UZ" Text="Uzbekistan" />
                        <wijmo:C1ComboBoxItem Value="VU" Text="Vanuatu" />
                        <wijmo:C1ComboBoxItem Value="VE" Text="Venezuela" />
                        <wijmo:C1ComboBoxItem Value="VN" Text="Viet Nam" />
                        <wijmo:C1ComboBoxItem Value="VG" Text="Virgin Islands (British)" />
                        <wijmo:C1ComboBoxItem Value="VI" Text="Virgin Islands (U.S.)" />
                        <wijmo:C1ComboBoxItem Value="WF" Text="Wallis And Futuna Islands" />
                        <wijmo:C1ComboBoxItem Value="EH" Text="Western Sahara" />
                        <wijmo:C1ComboBoxItem Value="YE" Text="Yemen" />
                        <wijmo:C1ComboBoxItem Value="YU" Text="Yugoslavia" />
                        <wijmo:C1ComboBoxItem Value="ZR" Text="Zaire" />
                        <wijmo:C1ComboBoxItem Value="ZM" Text="Zambia" />
                        <wijmo:C1ComboBoxItem Value="ZW" Text="Zimbabwe" />
                    </Items>
                </wijmo:C1ComboBox>
            </td>
        </tr>
    </table>
</asp:Panel>
<br /><hr />
<asp:Panel runat="server" ID="pnlPlaceOfBusinessAddress" Visible="true">
    <table width="100%">
        <tr>
            <td colspan="2">
                <b>Principal Place of the Business of the Trust/Super Fund/Company</b><br />
            </td>
        </tr>
        <tr>
            <td width="10%">
                Address Line 1:
            </td>
            <td colspan="3">
                <asp:TextBox runat="server" ID="txtAddressLine1PlaceOfBusiness" Width="90%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="10%">
                Address Line 2:
            </td>
            <td colspan="3">
                <asp:TextBox runat="server" ID="txtAddressLine2PlaceOfBusiness" Width="90%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="10%">
                Suburb:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSuburbPlaceOfBusiness" Width="250px"></asp:TextBox>
            </td>
            <td>
                State:
            </td>
            <td>
                <wijmo:C1ComboBox ID="C1ComboBoxPlaceOfBusinessState" runat="server" Width="250px">
                    <Items>
                        <wijmo:C1ComboBoxItem Value="ACT" Text="ACT" />
                        <wijmo:C1ComboBoxItem Value="NSW" Text="NSW" />
                        <wijmo:C1ComboBoxItem Value="NT" Text="NT" />
                        <wijmo:C1ComboBoxItem Value="QLD" Text="QLD" />
                        <wijmo:C1ComboBoxItem Value="SA" Text="SA" />
                        <wijmo:C1ComboBoxItem Value="TAS" Text="TAS" />
                        <wijmo:C1ComboBoxItem Value="VIC" Text="VIC" />
                        <wijmo:C1ComboBoxItem Value="WA" Text="WA" />
                    </Items>
                </wijmo:C1ComboBox>
            </td>
        </tr>
        <tr>
            <td>
                Postcode:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPostCodePlaceOfBusiness" Width="250px"></asp:TextBox>
            </td>
            <td>
                Country:
            </td>
            <td>
                <wijmo:C1ComboBox ID="C1ComboBoxPlaceOfBusinessCountry" runat="server" Width="250px">
                    <Items>
                        <wijmo:C1ComboBoxItem Value="AF" Text="Afghanistan" />
                        <wijmo:C1ComboBoxItem Value="AL" Text="Albania" />
                        <wijmo:C1ComboBoxItem Value="DZ" Text="Algeria" />
                        <wijmo:C1ComboBoxItem Value="AS" Text="American Samoa" />
                        <wijmo:C1ComboBoxItem Value="AD" Text="Andorra" />
                        <wijmo:C1ComboBoxItem Value="AO" Text="Angola" />
                        <wijmo:C1ComboBoxItem Value="AI" Text="Anguilla" />
                        <wijmo:C1ComboBoxItem Value="AQ" Text="Antarctica" />
                        <wijmo:C1ComboBoxItem Value="AG" Text="Antigua And Barbuda" />
                        <wijmo:C1ComboBoxItem Value="AR" Text="Argentina" />
                        <wijmo:C1ComboBoxItem Value="AM" Text="Armenia" />
                        <wijmo:C1ComboBoxItem Value="AW" Text="Aruba" />
                        <wijmo:C1ComboBoxItem Value="AU" Text="Australia" Selected="True" />
                        <wijmo:C1ComboBoxItem Value="AT" Text="Austria" />
                        <wijmo:C1ComboBoxItem Value="AZ" Text="Azerbaijan" />
                        <wijmo:C1ComboBoxItem Value="BS" Text="Bahamas" />
                        <wijmo:C1ComboBoxItem Value="BH" Text="Bahrain" />
                        <wijmo:C1ComboBoxItem Value="BD" Text="Bangladesh" />
                        <wijmo:C1ComboBoxItem Value="BB" Text="Barbados" />
                        <wijmo:C1ComboBoxItem Value="BY" Text="Belarus" />
                        <wijmo:C1ComboBoxItem Value="BE" Text="Belgium" />
                        <wijmo:C1ComboBoxItem Value="BZ" Text="Belize" />
                        <wijmo:C1ComboBoxItem Value="BJ" Text="Benin" />
                        <wijmo:C1ComboBoxItem Value="BM" Text="Bermuda" />
                        <wijmo:C1ComboBoxItem Value="BT" Text="Bhutan" />
                        <wijmo:C1ComboBoxItem Value="BO" Text="Bolivia" />
                        <wijmo:C1ComboBoxItem Value="BA" Text="Bosnia And Herzegowina" />
                        <wijmo:C1ComboBoxItem Value="BW" Text="Botswana" />
                        <wijmo:C1ComboBoxItem Value="BV" Text="Bouvet Island" />
                        <wijmo:C1ComboBoxItem Value="BR" Text="Brazil" />
                        <wijmo:C1ComboBoxItem Value="IO" Text="British Indian Ocean Territory" />
                        <wijmo:C1ComboBoxItem Value="BN" Text="Brunei Darussalam" />
                        <wijmo:C1ComboBoxItem Value="BG" Text="Bulgaria" />
                        <wijmo:C1ComboBoxItem Value="BF" Text="Burkina Faso" />
                        <wijmo:C1ComboBoxItem Value="BI" Text="Burundi" />
                        <wijmo:C1ComboBoxItem Value="KH" Text="Cambodia" />
                        <wijmo:C1ComboBoxItem Value="CM" Text="Cameroon" />
                        <wijmo:C1ComboBoxItem Value="CA" Text="Canada" />
                        <wijmo:C1ComboBoxItem Value="CV" Text="Cape Verde" />
                        <wijmo:C1ComboBoxItem Value="KY" Text="Cayman Islands" />
                        <wijmo:C1ComboBoxItem Value="CF" Text="Central African Republic" />
                        <wijmo:C1ComboBoxItem Value="TD" Text="Chad" />
                        <wijmo:C1ComboBoxItem Value="CL" Text="Chile" />
                        <wijmo:C1ComboBoxItem Value="CN" Text="China" />
                        <wijmo:C1ComboBoxItem Value="CX" Text="Christmas Island" />
                        <wijmo:C1ComboBoxItem Value="CC" Text="Cocos (Keeling) Islands" />
                        <wijmo:C1ComboBoxItem Value="CO" Text="Colombia" />
                        <wijmo:C1ComboBoxItem Value="KM" Text="Comoros" />
                        <wijmo:C1ComboBoxItem Value="CG" Text="Congo" />
                        <wijmo:C1ComboBoxItem Value="CK" Text="Cook Islands" />
                        <wijmo:C1ComboBoxItem Value="CR" Text="Costa Rica" />
                        <wijmo:C1ComboBoxItem Value="CI" Text="Cote D'Ivoire" />
                        <wijmo:C1ComboBoxItem Value="HR" Text="Croatia (Local Name: Hrvatska)" />
                        <wijmo:C1ComboBoxItem Value="CU" Text="Cuba" />
                        <wijmo:C1ComboBoxItem Value="CY" Text="Cyprus" />
                        <wijmo:C1ComboBoxItem Value="CZ" Text="Czech Republic" />
                        <wijmo:C1ComboBoxItem Value="DK" Text="Denmark" />
                        <wijmo:C1ComboBoxItem Value="DJ" Text="Djibouti" />
                        <wijmo:C1ComboBoxItem Value="DM" Text="Dominica" />
                        <wijmo:C1ComboBoxItem Value="DO" Text="Dominican Republic" />
                        <wijmo:C1ComboBoxItem Value="TP" Text="East Timor" />
                        <wijmo:C1ComboBoxItem Value="EC" Text="Ecuador" />
                        <wijmo:C1ComboBoxItem Value="EG" Text="Egypt" />
                        <wijmo:C1ComboBoxItem Value="SV" Text="El Salvador" />
                        <wijmo:C1ComboBoxItem Value="GQ" Text="Equatorial Guinea" />
                        <wijmo:C1ComboBoxItem Value="ER" Text="Eritrea" />
                        <wijmo:C1ComboBoxItem Value="EE" Text="Estonia" />
                        <wijmo:C1ComboBoxItem Value="ET" Text="Ethiopia" />
                        <wijmo:C1ComboBoxItem Value="FK" Text="Falkland Islands (Malvinas)" />
                        <wijmo:C1ComboBoxItem Value="FO" Text="Faroe Islands" />
                        <wijmo:C1ComboBoxItem Value="FJ" Text="Fiji" />
                        <wijmo:C1ComboBoxItem Value="FI" Text="Finland" />
                        <wijmo:C1ComboBoxItem Value="FR" Text="France" />
                        <wijmo:C1ComboBoxItem Value="GF" Text="French Guiana" />
                        <wijmo:C1ComboBoxItem Value="PF" Text="French Polynesia" />
                        <wijmo:C1ComboBoxItem Value="TF" Text="French Southern Territories" />
                        <wijmo:C1ComboBoxItem Value="GA" Text="Gabon" />
                        <wijmo:C1ComboBoxItem Value="GM" Text="Gambia" />
                        <wijmo:C1ComboBoxItem Value="GE" Text="Georgia" />
                        <wijmo:C1ComboBoxItem Value="DE" Text="Germany" />
                        <wijmo:C1ComboBoxItem Value="GH" Text="Ghana" />
                        <wijmo:C1ComboBoxItem Value="GI" Text="Gibraltar" />
                        <wijmo:C1ComboBoxItem Value="GR" Text="Greece" />
                        <wijmo:C1ComboBoxItem Value="GL" Text="Greenland" />
                        <wijmo:C1ComboBoxItem Value="GD" Text="Grenada" />
                        <wijmo:C1ComboBoxItem Value="GP" Text="Guadeloupe" />
                        <wijmo:C1ComboBoxItem Value="GU" Text="Guam" />
                        <wijmo:C1ComboBoxItem Value="GT" Text="Guatemala" />
                        <wijmo:C1ComboBoxItem Value="GN" Text="Guinea" />
                        <wijmo:C1ComboBoxItem Value="GW" Text="Guinea-Bissau" />
                        <wijmo:C1ComboBoxItem Value="GY" Text="Guyana" />
                        <wijmo:C1ComboBoxItem Value="HT" Text="Haiti" />
                        <wijmo:C1ComboBoxItem Value="HM" Text="Heard And Mc Donald Islands" />
                        <wijmo:C1ComboBoxItem Value="VA" Text="Holy See (Vatican City State)" />
                        <wijmo:C1ComboBoxItem Value="HN" Text="Honduras" />
                        <wijmo:C1ComboBoxItem Value="HK" Text="Hong Kong" />
                        <wijmo:C1ComboBoxItem Value="HU" Text="Hungary" />
                        <wijmo:C1ComboBoxItem Value="IS" Text="Icel And" />
                        <wijmo:C1ComboBoxItem Value="IN" Text="India" />
                        <wijmo:C1ComboBoxItem Value="ID" Text="Indonesia" />
                        <wijmo:C1ComboBoxItem Value="IR" Text="Iran (Islamic Republic Of)" />
                        <wijmo:C1ComboBoxItem Value="IQ" Text="Iraq" />
                        <wijmo:C1ComboBoxItem Value="IE" Text="Ireland" />
                        <wijmo:C1ComboBoxItem Value="IL" Text="Israel" />
                        <wijmo:C1ComboBoxItem Value="IT" Text="Italy" />
                        <wijmo:C1ComboBoxItem Value="JM" Text="Jamaica" />
                        <wijmo:C1ComboBoxItem Value="JP" Text="Japan" />
                        <wijmo:C1ComboBoxItem Value="JO" Text="Jordan" />
                        <wijmo:C1ComboBoxItem Value="KZ" Text="Kazakhstan" />
                        <wijmo:C1ComboBoxItem Value="KE" Text="Kenya" />
                        <wijmo:C1ComboBoxItem Value="KI" Text="Kiribati" />
                        <wijmo:C1ComboBoxItem Value="KP" Text="Korea, Dem People'S Republic" />
                        <wijmo:C1ComboBoxItem Value="KR" Text="Korea, Republic Of" />
                        <wijmo:C1ComboBoxItem Value="KW" Text="Kuwait" />
                        <wijmo:C1ComboBoxItem Value="KG" Text="Kyrgyzstan" />
                        <wijmo:C1ComboBoxItem Value="LA" Text="Lao People'S Dem Republic" />
                        <wijmo:C1ComboBoxItem Value="LV" Text="Latvia" />
                        <wijmo:C1ComboBoxItem Value="LB" Text="Lebanon" />
                        <wijmo:C1ComboBoxItem Value="LS" Text="Lesotho" />
                        <wijmo:C1ComboBoxItem Value="LR" Text="Liberia" />
                        <wijmo:C1ComboBoxItem Value="LY" Text="Libyan Arab Jamahiriya" />
                        <wijmo:C1ComboBoxItem Value="LI" Text="Liechtenstein" />
                        <wijmo:C1ComboBoxItem Value="LT" Text="Lithuania" />
                        <wijmo:C1ComboBoxItem Value="LU" Text="Luxembourg" />
                        <wijmo:C1ComboBoxItem Value="MO" Text="Macau" />
                        <wijmo:C1ComboBoxItem Value="MK" Text="Macedonia" />
                        <wijmo:C1ComboBoxItem Value="MG" Text="Madagascar" />
                        <wijmo:C1ComboBoxItem Value="MW" Text="Malawi" />
                        <wijmo:C1ComboBoxItem Value="MY" Text="Malaysia" />
                        <wijmo:C1ComboBoxItem Value="MV" Text="Maldives" />
                        <wijmo:C1ComboBoxItem Value="ML" Text="Mali" />
                        <wijmo:C1ComboBoxItem Value="MT" Text="Malta" />
                        <wijmo:C1ComboBoxItem Value="MH" Text="Marshall Islands" />
                        <wijmo:C1ComboBoxItem Value="MQ" Text="Martinique" />
                        <wijmo:C1ComboBoxItem Value="MR" Text="Mauritania" />
                        <wijmo:C1ComboBoxItem Value="MU" Text="Mauritius" />
                        <wijmo:C1ComboBoxItem Value="YT" Text="Mayotte" />
                        <wijmo:C1ComboBoxItem Value="MX" Text="Mexico" />
                        <wijmo:C1ComboBoxItem Value="FM" Text="Micronesia, Federated States" />
                        <wijmo:C1ComboBoxItem Value="MD" Text="Moldova, Republic Of" />
                        <wijmo:C1ComboBoxItem Value="MC" Text="Monaco" />
                        <wijmo:C1ComboBoxItem Value="MN" Text="Mongolia" />
                        <wijmo:C1ComboBoxItem Value="MS" Text="Montserrat" />
                        <wijmo:C1ComboBoxItem Value="MA" Text="Morocco" />
                        <wijmo:C1ComboBoxItem Value="MZ" Text="Mozambique" />
                        <wijmo:C1ComboBoxItem Value="MM" Text="Myanmar" />
                        <wijmo:C1ComboBoxItem Value="NA" Text="Namibia" />
                        <wijmo:C1ComboBoxItem Value="NR" Text="Nauru" />
                        <wijmo:C1ComboBoxItem Value="NP" Text="Nepal" />
                        <wijmo:C1ComboBoxItem Value="NL" Text="Netherlands" />
                        <wijmo:C1ComboBoxItem Value="AN" Text="Netherlands Ant Illes" />
                        <wijmo:C1ComboBoxItem Value="NC" Text="New Caledonia" />
                        <wijmo:C1ComboBoxItem Value="NZ" Text="New Zealand" />
                        <wijmo:C1ComboBoxItem Value="NI" Text="Nicaragua" />
                        <wijmo:C1ComboBoxItem Value="NE" Text="Niger" />
                        <wijmo:C1ComboBoxItem Value="NG" Text="Nigeria" />
                        <wijmo:C1ComboBoxItem Value="NU" Text="Niue" />
                        <wijmo:C1ComboBoxItem Value="NF" Text="Norfolk Island" />
                        <wijmo:C1ComboBoxItem Value="MP" Text="Northern Mariana Islands" />
                        <wijmo:C1ComboBoxItem Value="NO" Text="Norway" />
                        <wijmo:C1ComboBoxItem Value="OM" Text="Oman" />
                        <wijmo:C1ComboBoxItem Value="PK" Text="Pakistan" />
                        <wijmo:C1ComboBoxItem Value="PW" Text="Palau" />
                        <wijmo:C1ComboBoxItem Value="PA" Text="Panama" />
                        <wijmo:C1ComboBoxItem Value="PG" Text="Papua New Guinea" />
                        <wijmo:C1ComboBoxItem Value="PY" Text="Paraguay" />
                        <wijmo:C1ComboBoxItem Value="PE" Text="Peru" />
                        <wijmo:C1ComboBoxItem Value="PH" Text="Philippines" />
                        <wijmo:C1ComboBoxItem Value="PN" Text="Pitcairn" />
                        <wijmo:C1ComboBoxItem Value="PL" Text="Poland" />
                        <wijmo:C1ComboBoxItem Value="PT" Text="Portugal" />
                        <wijmo:C1ComboBoxItem Value="PR" Text="Puerto Rico" />
                        <wijmo:C1ComboBoxItem Value="QA" Text="Qatar" />
                        <wijmo:C1ComboBoxItem Value="RE" Text="Reunion" />
                        <wijmo:C1ComboBoxItem Value="RO" Text="Romania" />
                        <wijmo:C1ComboBoxItem Value="RU" Text="Russian Federation" />
                        <wijmo:C1ComboBoxItem Value="RW" Text="Rwanda" />
                        <wijmo:C1ComboBoxItem Value="KN" Text="Saint K Itts And Nevis" />
                        <wijmo:C1ComboBoxItem Value="LC" Text="Saint Lucia" />
                        <wijmo:C1ComboBoxItem Value="VC" Text="Saint Vincent, The Grenadines" />
                        <wijmo:C1ComboBoxItem Value="WS" Text="Samoa" />
                        <wijmo:C1ComboBoxItem Value="SM" Text="San Marino" />
                        <wijmo:C1ComboBoxItem Value="ST" Text="Sao Tome And Principe" />
                        <wijmo:C1ComboBoxItem Value="SA" Text="Saudi Arabia" />
                        <wijmo:C1ComboBoxItem Value="SN" Text="Senegal" />
                        <wijmo:C1ComboBoxItem Value="SC" Text="Seychelles" />
                        <wijmo:C1ComboBoxItem Value="SL" Text="Sierra Leone" />
                        <wijmo:C1ComboBoxItem Value="SG" Text="Singapore" />
                        <wijmo:C1ComboBoxItem Value="SK" Text="Slovakia (Slovak Republic)" />
                        <wijmo:C1ComboBoxItem Value="SI" Text="Slovenia" />
                        <wijmo:C1ComboBoxItem Value="SB" Text="Solomon Islands" />
                        <wijmo:C1ComboBoxItem Value="SO" Text="Somalia" />
                        <wijmo:C1ComboBoxItem Value="ZA" Text="South Africa" />
                        <wijmo:C1ComboBoxItem Value="GS" Text="South Georgia , S Sandwich Is." />
                        <wijmo:C1ComboBoxItem Value="ES" Text="Spain" />
                        <wijmo:C1ComboBoxItem Value="LK" Text="Sri Lanka" />
                        <wijmo:C1ComboBoxItem Value="SH" Text="St. Helena" />
                        <wijmo:C1ComboBoxItem Value="PM" Text="St. Pierre And Miquelon" />
                        <wijmo:C1ComboBoxItem Value="SD" Text="Sudan" />
                        <wijmo:C1ComboBoxItem Value="SR" Text="Suriname" />
                        <wijmo:C1ComboBoxItem Value="SJ" Text="Svalbard, Jan Mayen Islands" />
                        <wijmo:C1ComboBoxItem Value="SZ" Text="Sw Aziland" />
                        <wijmo:C1ComboBoxItem Value="SE" Text="Sweden" />
                        <wijmo:C1ComboBoxItem Value="CH" Text="Switzerland" />
                        <wijmo:C1ComboBoxItem Value="SY" Text="Syrian Arab Republic" />
                        <wijmo:C1ComboBoxItem Value="TW" Text="Taiwan" />
                        <wijmo:C1ComboBoxItem Value="TJ" Text="Tajikistan" />
                        <wijmo:C1ComboBoxItem Value="TZ" Text="Tanzania, United Republic Of" />
                        <wijmo:C1ComboBoxItem Value="TH" Text="Thailand" />
                        <wijmo:C1ComboBoxItem Value="TG" Text="Togo" />
                        <wijmo:C1ComboBoxItem Value="TK" Text="Tokelau" />
                        <wijmo:C1ComboBoxItem Value="TO" Text="Tonga" />
                        <wijmo:C1ComboBoxItem Value="TT" Text="Trinidad And Tobago" />
                        <wijmo:C1ComboBoxItem Value="TN" Text="Tunisia" />
                        <wijmo:C1ComboBoxItem Value="TR" Text="Turkey" />
                        <wijmo:C1ComboBoxItem Value="TM" Text="Turkmenistan" />
                        <wijmo:C1ComboBoxItem Value="TC" Text="Turks And Caicos Islands" />
                        <wijmo:C1ComboBoxItem Value="TV" Text="Tuvalu" />
                        <wijmo:C1ComboBoxItem Value="UG" Text="Uganda" />
                        <wijmo:C1ComboBoxItem Value="UA" Text="Ukraine" />
                        <wijmo:C1ComboBoxItem Value="AE" Text="United Arab Emirates" />
                        <wijmo:C1ComboBoxItem Value="GB" Text="United Kingdom" />
                        <wijmo:C1ComboBoxItem Value="US" Text="United States" />
                        <wijmo:C1ComboBoxItem Value="UM" Text="United States Minor Is." />
                        <wijmo:C1ComboBoxItem Value="UY" Text="Uruguay" />
                        <wijmo:C1ComboBoxItem Value="UZ" Text="Uzbekistan" />
                        <wijmo:C1ComboBoxItem Value="VU" Text="Vanuatu" />
                        <wijmo:C1ComboBoxItem Value="VE" Text="Venezuela" />
                        <wijmo:C1ComboBoxItem Value="VN" Text="Viet Nam" />
                        <wijmo:C1ComboBoxItem Value="VG" Text="Virgin Islands (British)" />
                        <wijmo:C1ComboBoxItem Value="VI" Text="Virgin Islands (U.S.)" />
                        <wijmo:C1ComboBoxItem Value="WF" Text="Wallis And Futuna Islands" />
                        <wijmo:C1ComboBoxItem Value="EH" Text="Western Sahara" />
                        <wijmo:C1ComboBoxItem Value="YE" Text="Yemen" />
                        <wijmo:C1ComboBoxItem Value="YU" Text="Yugoslavia" />
                        <wijmo:C1ComboBoxItem Value="ZR" Text="Zaire" />
                        <wijmo:C1ComboBoxItem Value="ZM" Text="Zambia" />
                        <wijmo:C1ComboBoxItem Value="ZW" Text="Zimbabwe" />
                    </Items>
                </wijmo:C1ComboBox>
            </td>
        </tr>
    </table>
</asp:Panel>

<br/><hr/>
<b>Please note that all Bankwest accounting opening correspondence is sent to the e-Clipse online Head Office for collation and redirection to the client.</b>