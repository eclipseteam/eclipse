﻿using System;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using Oritax.TaxSimp.CM.Organization.UMAForm;

namespace eclipseonlineweb.UMAForm.HelperClass
{
    public class UMAFormPDF
    {
        #region VARIABLES
        public UMAFormEntity UMAFormEntity { get; set; }

        private string errMsg = string.Empty;
        private string filePath = @"c:\UMAForm.pdf";//"~/PDF/UMAForm.pdf";

        private const float TABLE_WIDTH = 100f;
        private const float ROW_DETAIL_HEIGHT = 16f;
        private const float SEPARATOR_HEIGHT = 10f;

        private const string FONT = "Arial";
        private const string IMG_UNCHECK_CHKBOX = "~/images/UmaForm/check-box-uncheck-icon.png";
        private const string IMG_CHECK_CHKBOX = "~/images/UmaForm/check-box-icon.png";
        private const string IMG_UNCHECK_RADIO = "~/images/UmaForm/ui-radio-button-uncheck-icon.png";
        private const string IMG_CHECK_RADIO = "~/images/UmaForm/ui-radio-button-icon.png";

        private Font TABLE_FONT_HEADER = FontFactory.GetFont(FONT, 7f, Font.BOLD, BaseColor.WHITE);
        private Font TABLE_FONT_HEADERBLACK = FontFactory.GetFont(FONT, 7f, Font.BOLD, BaseColor.BLACK);
        private Font TABLE_FONT_IDENTIFIER = FontFactory.GetFont(FONT, 10f, Font.BOLD, COLOR_DARK_BLACK);

        private Font _typeClient = FontFactory.GetFont(FONT, 7f, Font.BOLD, COLOR_DARK_BLACK);

        private Font TABLE_FONT_DETAILS = FontFactory.GetFont(FONT, 8f, Font.NORMAL, COLOR_DARK_BLACK);
        private Font TABLE_FONT_DETAILS_UNDERLINE = FontFactory.GetFont(FONT, 8f, Font.UNDERLINE, COLOR_DARK_BLACK);
        private Font TABLE_FONT_DETAILS_ITALIC = FontFactory.GetFont(FONT, 7f, Font.ITALIC, COLOR_DARK_BLACK);

        private static readonly BaseColor TABLE_HEADER_BGCOLOR = new BaseColor(61, 59, 106);
        private static readonly BaseColor TABLE_DETAIL_BGCOLOR = new BaseColor(255, 255, 255);
        private static readonly BaseColor TABLE_DETAIL_BORDER_COLOR = BaseColor.WHITE;

        private static readonly BaseColor TEXTBOX_BGCOLOR = new BaseColor(255, 255, 255);
        private static readonly BaseColor TEXTBOX_BORDERCOLOR = new BaseColor(204, 204, 204);

        private static readonly BaseColor COLOR_DARK_BLACK = new BaseColor(0, 0, 0);
        #endregion VARIABLES

        #region CONSTRUCTOR
        public UMAFormPDF(UMAFormEntity umaFormEntity)
        {
            this.UMAFormEntity = umaFormEntity;
            filePath = @"c:\UMAForm_" + this.UMAFormEntity.accountName + ".pdf";
        }
        #endregion CONSTRUCTOR

        #region PAGE 1 : Client Details
        public PdfPTable AccountDetails()
        {
            float[] widths = new float[] { 2f, 3f, 13f, 27f, 2f, 3f, 18f, 2f, 3f, 18f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);

            tbl.AddCell(AddCell("CLIENT DETAILS", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
            tbl.AddCell(AddCell("Type of Account:", 4, 15f, _typeClient, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));


            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*ROW 1*/
            tbl.AddCell(AddCell("1:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.accountType == "Individual") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Individual", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("7:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.accountType.Contains("SMSF")) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Self Managed Super Fund (SMSF) ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("8:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.accountType.Contains("Trust -")) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Trust", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 2*/
            tbl.AddCell(AddCell("2:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.accountType == "Joint") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Joint", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Please specify the type of trustee:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Please specify the type of trustee:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 3*/
            tbl.AddCell(AddCell("3:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.accountType == "Company - Public") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Company - Public", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.accountType == "Self Managed Super Fund (SMSF) - Individual Trustee") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Individual ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.accountType == "Trust - Individual Trustee") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Individual ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 4*/
            tbl.AddCell(AddCell("4:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.accountType == "Company - Private") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Company - Private", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.accountType == "Self Managed Super Fund (SMSF) - Joint Trustee") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Joint ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.accountType == "Trust - Joint Trustee") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Joint ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 5*/
            tbl.AddCell(AddCell("5:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.accountType == "Partnership") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Partnership", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.accountType == "Self Managed Super Fund (SMSF) - Corporate Trustee") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Corporate ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.accountType == "Trust - Corporate Trustee") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Corporate ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 6*/

            tbl.AddCell(AddCell("", 6, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 7*/
            tbl.AddCell(AddCell("Account Name:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.accountName, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 8*/
            tbl.AddCell(AddCell("Trustee Name/s:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.trustName, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 9*/
            tbl.AddCell(AddCell("Account NO:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.accountNO.ToUpper(), 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            return tbl;
        }
        #endregion PAGE 1 : CLIENT DETAILS

        #region PAGE 2 : FEE STRUCTURE
        public PdfPTable FeeStructure()
        {
            float[] widths = new float[] { 13f, 5f, 16f, 3f, 8f, 8f, 2f, 15f, 0.5f, 3f, 16f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 5f };


            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);

            tbl.AddCell(AddCell("FEE STRUCTURE", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 3*/
            tbl.AddCell(AddCell("Upfront fee:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.UpFrontFee.ToString("C"), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("inc GST", 8, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 4*/
            tbl.AddCell(AddCell("Ongoing fee:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.OngoingFeeDollar.ToString("C"), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("p.a. inc GST   Or", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.OngoingFeePercentage.ToString() + " %", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("p.a. inc GST", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 5*/
            tbl.AddCell(AddCell("Or:", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 6*/
            tbl.AddCell(AddCell("Tiered fee:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("From", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.Tier1From.ToString("C"), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("To", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.Tier1To.ToString("C"), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.Tier1Percentage.ToString() + " %", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("p.a. inc GST", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 7*/
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("From", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.Tier2From.ToString("C"), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("To", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.Tier1To.ToString("C"), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.Tier2Percentage.ToString() + " %", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("p.a. inc GST", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 8*/
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("From", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.Tier3From.ToString("C"), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("To", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.Tier3To.ToString("C"), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.Tier3Percentage.ToString() + " %", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("p.a. inc GST", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 8*/
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("From", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.Tier4From.ToString("C"), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("To", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.Tier4To.ToString("C"), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.Tier4Percentage.ToString() + " %", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("p.a. inc GST", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 8*/
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("From", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.Tier5From.ToString("C"), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("To", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.Tier5To.ToString("C"), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.Tier5Percentage.ToString() + " %", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("p.a. inc GST", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            return tbl;
        }

        #endregion PAGE 2 :  FEE STRUCTURE

        #region PAGE 3: TFN / ABN / ABRN

        public PdfPTable TFNABNABRN()
        {
            float[] widths = new float[] { 8f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 5f };
            float[] highpen_padding = new float[] { 4.5f, 1f, 2f, 1f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);

            tbl.AddCell(AddCell("TFN / ABN / ABRN", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 1*/
            tbl.AddCell(AddCell("TFN (Investor/Signatory 1):", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            for (int i = 0; i < UMAFormEntity.TFNSig1.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig1.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 1:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig1.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 2:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig1.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 3:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig1.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 4:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig1.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 5:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig1.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 6:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig1.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 7:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig1.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 8:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig1.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 5, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                }
            }

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 1*/
            tbl.AddCell(AddCell("TFN (Investor/Signatory 2):", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            for (int i = 0; i < UMAFormEntity.TFNSig2.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig2.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 1:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig2.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 2:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig2.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 3:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig2.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 4:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig2.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 5:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig2.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 6:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig2.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 7:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig2.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 8:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig2.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 5, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                }
            }

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 1*/
            tbl.AddCell(AddCell("TFN (Investor/Signatory 3):", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            for (int i = 0; i < UMAFormEntity.TFNSig3.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig3.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 1:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig3.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 2:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig3.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 3:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig3.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 4:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig3.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 5:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig3.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 6:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig3.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 7:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig3.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 8:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig3.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 5, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                }
            }

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 1*/
            tbl.AddCell(AddCell("TFN (Investor/Signatory 4):", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            for (int i = 0; i < UMAFormEntity.TFNSig4.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig4.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 1:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig4.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 2:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig4.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 3:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig4.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 4:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig4.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 5:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig4.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 6:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig4.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 7:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig4.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 8:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNSig4.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 5, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                }
            }

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 1*/
            tbl.AddCell(AddCell("TFN (Trust/Superfund):", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            for (int i = 0; i < UMAFormEntity.TFNTrustFund.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNTrustFund.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 1:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNTrustFund.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 2:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNTrustFund.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 3:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNTrustFund.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 4:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNTrustFund.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 5:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNTrustFund.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 6:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNTrustFund.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 7:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNTrustFund.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 8:
                        tbl.AddCell(AddCell(UMAFormEntity.TFNTrustFund.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 5, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                }
            }

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 1*/
            tbl.AddCell(AddCell("ACN (Company):", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            for (int i = 0; i < UMAFormEntity.ACN.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 1:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 2:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 3:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 4:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 5:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 6:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 7:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 8:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 5, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                }
            }

            return tbl;
        }

        #endregion  PAGE 3: TFN / ABN / ABRN

        #region PAGE 3 : TFN / ABN / ABRN PART 2

        public PdfPTable TFNABNABRN_Part2()
        {
            float[] widths = new float[] { 8f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 5f };
            float[] highpen_padding = new float[] { 4.5f, 1f, 2f, 1f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);

            /*ROW 1*/
            tbl.AddCell(AddCell("ABN (Company/Superfund):", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            for (int i = 0; i < UMAFormEntity.ABNCompany.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        tbl.AddCell(AddCell(UMAFormEntity.ABNCompany.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 1:
                        tbl.AddCell(AddCell(UMAFormEntity.ABNCompany.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_CENTER, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 2:
                        tbl.AddCell(AddCell(UMAFormEntity.ABNCompany.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 3:
                        tbl.AddCell(AddCell(UMAFormEntity.ABNCompany.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 4:
                        tbl.AddCell(AddCell(UMAFormEntity.ABNCompany.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_CENTER, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 5:
                        tbl.AddCell(AddCell(UMAFormEntity.ABNCompany.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 6:
                        tbl.AddCell(AddCell(UMAFormEntity.ABNCompany.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 7:
                        tbl.AddCell(AddCell(UMAFormEntity.ABNCompany.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_CENTER, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 8:
                        tbl.AddCell(AddCell(UMAFormEntity.ABNCompany.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 9:
                        tbl.AddCell(AddCell(UMAFormEntity.ABNCompany.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 10:
                        tbl.AddCell(AddCell(UMAFormEntity.ABNCompany.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                }
            }

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 1*/
            tbl.AddCell(AddCell("ACN (Company):", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            for (int i = 0; i < UMAFormEntity.ACN.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 1:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_CENTER, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 2:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 3:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 4:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_CENTER, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 5:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 6:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 7:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_CENTER, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 8:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 9:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                    case 10:
                        tbl.AddCell(AddCell(UMAFormEntity.ACN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                        break;
                }
            }

            return tbl;
        }

        #endregion PAGE 3 : TFN / ABN / ABRN PART 2

        #region PAGE 3 : TFN / ABN / ABRN PART 3

        public PdfPTable TFNABNABRN_Part3()
        {
            float[] widths = new float[] { 9.5f, 0.1f, 0.5f, 0.5f, 0.5f, 2f, 4f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);

            /*ROW 1*/
            tbl.AddCell(AddCell("If you don't have an ABN or withholding tax payer number, have you applied for one?", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.AppliedForABN) ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Yes", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((!UMAFormEntity.AppliedForABN) ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("No", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 2*/
            tbl.AddCell(AddCell("If you are a non-resident for tax purposes, please provide your country of residence.", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.NonRedidentCountry, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR)); tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            return tbl;
        }

        #endregion PAGE 3 : TFN / ABN / ABRN PART 3

        #region PAGE 5: Macquarie CMA Settings

        public PdfPTable McquarieCMADetails()
        {
            float[] widths = new float[] { 9.5f, 0.1f, 0.5f, 0.5f, 0.5f, 2f, 4f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);

            /*ROW 1*/
            tbl.AddCell(AddCell("Do you want to link an existing Macquarie CMA instead of opening a Bankwest CMA?", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.MaqCMALinkBWA) ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Yes", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((!UMAFormEntity.MaqCMALinkBWA) ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("No", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            bool isMAQDIY = false;

            if (UMAFormEntity.MaqCMADIYDIWM.ToLower() == "diy")
                isMAQDIY = true;

            /*ROW 1*/
            tbl.AddCell(AddCell("If Yes to which service (ie. DIY, DIWM):", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((isMAQDIY) ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("DIY", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((!isMAQDIY) ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("DIWM", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            return tbl;
        }

        #endregion PAGE 5: Macquarie CMA Settings

        #region PAGE 3 : INVESTOR STATUS
        public PdfPTable Investor_Status()
        {
            float[] widths = new float[] { 0.15f, 1.2f, 0.15f, 4f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);

            tbl.AddCell(AddCell("INVESTOR STATUS", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));

            /*ROW 1*/
            tbl.AddCell(AddCellImage((UMAFormEntity.InvestorStatusWholesaleInvestor) ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Wholesale Investor", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 2*/
            tbl.AddCell(AddCellImage((UMAFormEntity.InvestorStatusSophisticatedInvestor) ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Sophisticated Investor", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS_UNDERLINE, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.CertificateFromAccountantIsAttached) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Certificate from Accountant is attached ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 3*/
            tbl.AddCell(AddCellImage((UMAFormEntity.InvestorStatusProfessionalInvestor) ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Professional Investor", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 4*/
            tbl.AddCell(AddCellImage((UMAFormEntity.InvestorStatusRetailInvestor) ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Retail Investor", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            return tbl;
        }
        #endregion PAGE 3 : INVESTOR STATUS

        #region PAGE 3 : AUTHORITY TO ACT
        public PdfPTable Authority_To_Act()
        {
            float[] widths = new float[] { 0.3f, 9.5f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);

            tbl.AddCell(AddCell("AUTHORITY TO ACT", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));

            tbl.AddCell(AddCell("Client wishes to grant a Limited Power of Attorney to:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 1*/
            tbl.AddCell(AddCellImage((UMAFormEntity.EclipseOnline) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("e-clipse Online", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 2*/
            tbl.AddCell(AddCellImage((UMAFormEntity.AustralianMoneyMarket) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Australian Money Market (AMM) ", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("* If the Client DOES NOT wish to grant a limited power of attorney, DO NOT SELECT ANYTHING ABOVE. The client will be require to complete and sign all", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("relavant account forms to establish their e-Clipse UMA.", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            return tbl;
        }
        #endregion PAGE 3 : AUTHORITY TO ACT

        #region PAGE 3 : SERVICES
        public PdfPTable Services()
        {
            float[] widths = new float[] { 0.4f, 0.5f, 0.4f, 3f, 1f, 2f, 4f, 4f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;
            PdfPTable tbl = CreateTable(max_column, widths);
            tbl.AddCell(AddCell("SERVICES", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Indicate the UMA Service and Accounts you require.", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*ROW 1*/
            tbl.AddCell(AddCellImage((UMAFormEntity.DoItYourSelf) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("DO IT YOURSELF (DIY)", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.DIYAmount.ToString("C"), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 2*/
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Specify which Accounts you need opened", 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 3*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.DIYBWA) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("BWA CMA (for Bank Account)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 4*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.DIYDesktopBroker) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Desktop Broker (for HIN) ", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 5*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.DIYFIIG) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("FIIG (for TDs & cash)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 6*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.DIYAMM) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Australian Money Market (for TDs & cash)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 7*/
            tbl.AddCell(AddCellImage((UMAFormEntity.DoItWithMe) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("DO IT WITH ME (DIWM)", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.DIWMAmount.ToString("C"), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 8*/
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Specify which Accounts you need opened", 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 9*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.DIWNMBWA) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("BWA CMA (for Bank Account)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 10*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.DIWNMDesktopBroker) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Desktop Broker (for HIN) ", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 11*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.DIWNMFIIG) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("FIIG (for TDs & cash)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 12*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.DIWNMAMM) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Australian Money Market (for TDs & cash)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 12*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.DIWNMSS) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("State Street (for Innova)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            if (UMAFormEntity.StateStreetEntityList.Count > 0)
            {
                tbl.AddCell(AddCell("Preferred Funds", max_column, 12F, TABLE_FONT_HEADERBLACK, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

                foreach (StateStreetEntity stateStreetEntity in UMAFormEntity.StateStreetEntityList)
                {
                    tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                    tbl.AddCell(AddCell(stateStreetEntity.Description, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                    tbl.AddCell(AddCell(stateStreetEntity.Code, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                    /*SEPARATOR*/
                    tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                }
            }

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 13*/
            tbl.AddCell(AddCellImage((UMAFormEntity.DoItForhMe) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("DO IT FOR ME (DIFM)", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.DIFMAmount.ToString("C"), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("Investment Program Name:", max_column, 12F, TABLE_FONT_HEADERBLACK, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 14*/
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.InvestmentProgram, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.InvestmentProgramCode, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));


            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 15*/
            tbl.AddCell(AddCell("Nominated Investment Preferences", max_column, 12F, TABLE_FONT_HEADERBLACK, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("The Investment Preferences that have been discussed with Your adviser are:", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCellImage((true) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Handling of unmanaged investments: Sell the holding", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCellImage((true) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("The minimum balance per investment in a rebalance is", 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));


            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("$" + UMAFormEntity.MinBalance, 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.MinBalancePercentage + "%", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));


            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCellImage((true) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("The minimum trade amount in a rebalance", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));


            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("$" + UMAFormEntity.MinTrade, 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.MinTradePercentage + "%", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));


            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));


            tbl.AddCell(AddCellImage((true) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Handling of investments that are not purchased due to a hold or sell rating in the Portfolio:. ", 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 17*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.AssignedAllocatedValueHoldSell) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Assign allocated value to cash (Default)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*ROW 18*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.DistributedAllocatedValueHoldSell) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Distribute allocated value to other investments", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCellImage((true) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Handling of investments that are not purchased due to an exclusion:", 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 17*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.AssignedAllocatedValueExclusion) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Assign allocated value to cash (Default)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 18*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.DistributedAllocatedValueExclusion) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Distribute allocated value to other investments", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCellImage((true) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Handling of investments that are not purchased due to a minimum trade or minimum holding constraint:", 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 17*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.AssignedAllocatedValueMinTradeHolding) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Assign allocated value to cash (Default)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 18*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.DistributedAllocatedValueMinTradeHolding) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Distribute allocated value to other investments", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));


            if (UMAFormEntity.StateStreetEntityList.Count > 0)
            {
                tbl.AddCell(AddCell("Investments Exclusions", max_column, 12F, TABLE_FONT_HEADERBLACK, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

                foreach (SecuritiesExclusionEntity securitiesExclusionEntity in UMAFormEntity.SecuritiesExclusionEntityList)
                {
                    tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                    tbl.AddCell(AddCell(securitiesExclusionEntity.Description, 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                    tbl.AddCell(AddCell(securitiesExclusionEntity.Code, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                    tbl.AddCell(AddCell(securitiesExclusionEntity.InvestmentAction, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                    /*SEPARATOR*/
                    tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                }
            }


            tbl.AddCell(AddCell("The following Accounts will be opened for you.", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 17*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.DIFNMBWA) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("BWA CMA (for Bank Account)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 18*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.DIFNMDesktopBroker) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Desktop Broker (for HIN)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 19*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((UMAFormEntity.DIFNMSS) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("State Street (for Innova & P2)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 20*/


            return tbl;
        }
        #endregion PAGE 3 : SERVICES

        #region PAGE 4 : Platforms Details
        public PdfPTable PlatformsDetails()
        {
            float[] widths = new float[] { 2f, 3f, 13f, 27f, 2f, 3f, 18f, 2f, 3f, 18f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);

            tbl.AddCell(AddCell("OTHER PLATFORMS", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 7*/
            tbl.AddCell(AddCell("Name of the platform", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.NameOfPlatformsBT, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Investor No. / Account No:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.InvestorNoBT, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Adviser No. / Code:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.AdviserNoBT, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Provider Name:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.ProviderNoBT, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("Name of the platform", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.NameOfPlatformsEclipse, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Investor No. / Account No:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.InvestorNoEclipse, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Adviser No. / Code:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.AdviserNoEclipse, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Provider Name:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.ProviderNoEclipse, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            return tbl;
        }
        #endregion PAGE 4 :Platforms Details

        #region PAGE 6 : Macaquarie CMA
        public PdfPTable McquarieCMA()
        {
            float[] widths = new float[] { 2f, 3f, 13f, 27f, 2f, 3f, 18f, 2f, 3f, 18f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);

            tbl.AddCell(AddCell("MACQUARIE CMA", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 7*/
            tbl.AddCell(AddCell("BSB:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.MaqCMABSB, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Account No:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.MaqCMAAccountNo, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Account Name:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(UMAFormEntity.MaqCMAAccountName, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            return tbl;
        }
        #endregion PAGE 6 : Macquarie CMA

        #region PAGE 5 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 1)
        public PdfPTable InvestorDetails_I(SignatoryEntity Form5)
        {
            float[] widths = new float[] { 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.4f, 0.3f, 0.5f, 0.7f, 2f, 5f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);

            tbl.AddCell(AddCell("INVESTOR/TRUSTEE/SIGNATORY DETAILS", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("INVESTOR/TRUSTEE/SIGNATORY " + Form5.SigNo.ToString(), max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*ROW 1*/
            tbl.AddCell(AddCellImage((Form5.Title == "Mr") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Mr", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form5.Title == "Mrs") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Mrs", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form5.Title == "Ms") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Ms", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form5.Title == "Miss") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Miss", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form5.Title == "Other") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Other", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.Title, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            return tbl;
        }
        #endregion PAGE 5 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 1)

        #region PAGE 5 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 2)
        public PdfPTable InvestorDetails_II(SignatoryEntity Form5)
        {
            float[] widths = new float[] { 1f, 0.3f, 1f, 0.3f, 0.8f, 5f, 2f, 4f, 7f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths, 1f);

            /*ROW 1*/
            tbl.AddCell(AddCell("Family Name:", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.FamilyName, 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("Given Name/s:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_CENTER, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.GivenName, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("Middle Name:", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.MiddleName, 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("Other Name/s:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_CENTER, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.GivenName, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 2*/
            tbl.AddCell(AddCell("Date of Birth:", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.DateOfBirth, 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 3*/
            tbl.AddCell(AddCell("Title if Corporate Trustee:  ", 9, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.CorporateTitel, 9, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            return tbl;
        }
        #endregion PAGE 5 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 2)

        #region PAGE 5 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 3)
        public PdfPTable InvestorDetails_III(SignatoryEntity Form5)
        {
            float[] widths = new float[] { 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.4f, 0.3f, 0.5f, 0.7f, 2f, 5f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths, 10f);


            return tbl;
        }
        #endregion PAGE 5 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 3)

        #region PAGE 5 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 4)
        public PdfPTable InvestorDetails_IV(SignatoryEntity Form5)
        {
            float[] widths = new float[] { 3f, 0.5f, 0.5f, 1f, 0.3f, 1f, 1f, 0.5f, 0.5f, 1f, 0.6f, 0.3f, 2f, 1f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);

            /*ROW 1*/
            tbl.AddCell(AddCell("Residential Address (mandatory, a PO Box, RMB or c/ - is not sufficient):", 6, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 2*/
            tbl.AddCell(AddCell("Address Line 1:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.ResiAddressLine1, 13, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 3*/
            tbl.AddCell(AddCell("Address Line 2:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.ResiAddressLine2, 13, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 4*/
            tbl.AddCell(AddCell("Suburb:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.ResiSuburb, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("State:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.ResiState, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 5*/
            tbl.AddCell(AddCell("Postcode:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.ResiPostCode, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Country:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.ResiCountry, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 5*/
            tbl.AddCell(AddCell("Mailing Address", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 2*/
            tbl.AddCell(AddCell("Address Line 1:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.MailAddressLine1, 13, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 3*/
            tbl.AddCell(AddCell("Address Line 2:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.MailAddressLine2, 13, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 4*/
            tbl.AddCell(AddCell("Suburb:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.MailSuburb, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("State:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.MailState, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 5*/
            tbl.AddCell(AddCell("Postcode:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.MailPostCode, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Country:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.MailCountry, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));


            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 6*/
            tbl.AddCell(AddCell("Occupation:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.Occupation, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Employer:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.Employer, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 7*/
            tbl.AddCell(AddCell("Main country of residence, if not Australia:", 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.MainCountryOFAddressSig, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 8*/
            tbl.AddCell(AddCell("Contact Ph:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.ContactPH, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Fax:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.Fax, 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 9*/
            tbl.AddCell(AddCell("Alternate Ph:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.AlternativePH, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Email:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.Email, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 10*/
            tbl.AddCell(AddCell("ID Type:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((!Form5.DriverLicSig) ? IMG_UNCHECK_CHKBOX : IMG_CHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Drivers License:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((!Form5.PassportSig) ? IMG_UNCHECK_CHKBOX : IMG_CHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Passport:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("", 8, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("Drivers License", max_column, 12F, TABLE_FONT_HEADERBLACK, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("Document No:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.DocumentNoLic, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*ROW 11*/
            tbl.AddCell(AddCell("Document Issuer:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.LicDocumentIssuer, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 6, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 12*/
            tbl.AddCell(AddCell("Issue Date:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.LicIssueDate, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Expiry Date:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.LicExpiryDate, 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 13*/
            tbl.AddCell(AddCell("Certified copy attached:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form5.LicCertifiedCopyAttached) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("Passport", max_column, 12F, TABLE_FONT_HEADERBLACK, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("Document No:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.DocumentNoPassport, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));


            /*ROW 11*/
            tbl.AddCell(AddCell("Document Issuer:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.PassportDocumentIssuer, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 6, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 12*/
            tbl.AddCell(AddCell("Issue Date:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.PassportIssueDate, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Expiry Date:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form5.PassportExpiryDate, 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 13*/
            tbl.AddCell(AddCell("Certified copy attached:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form5.PassportCertifiedCopyAttached) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            return tbl;
        }
        #endregion PAGE 5 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 4)

        #region PAGE 8 : DIY - BWA CMA OPTIONS (Part 1)
        public PdfPTable Page_Eight_DIY_BWA_CMA_Options_I(UMAFormEntity Form8)
        {
            float[] widths = new float[] { 0.2f, 0.8f, 0.2f, 0.8f, 0.2f, 0.2f, 0.8f, 0.2f, 1.3f, 0.2f, 3f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths, 1f);


            tbl.AddCell(AddCell("DIY - BWA CMA OPTIONS", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("The following selections will apply to my/our DIY BWA CMA", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("BWA ACCESS FACILITIES", max_column, 18f, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Please tick ( √ ) the Access Facilities required:", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form8.DIYPhoneAccess) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Phone Access", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form8.DIYOnlineAccess) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Online Access", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form8.DIYDebitCard) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Debit Card", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form8.DIYChequeBook) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Cheque Book (25 per book)", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form8.DIYDepositBook) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Deposit Book", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            return tbl;
        }
        #endregion PAGE 8 : DIY - BWA CMA OPTIONS (Part 1)

        #region PAGE 8 : DIY - BWA CMA OPTIONS (Part 2)
        public PdfPTable Page_Eight_DIY_BWA_CMA_Options_II(UMAFormEntity Form8)
        {
            float[] widths = new float[] { 0.2f, 0.8f, 0.2f, 0.8f, 0.2f, 0.2f, 0.8f, 3f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;
            PdfPTable tbl = CreateTable(max_column, widths);
            tbl.AddCell(AddCell("BWA MANNER OF OPERATION", max_column, 18f, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Please select how you wish to operate your CMA by ticking ( √ ) one of the following:", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form8.DIYMannerOfOperation == "Any one of us to sign") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Any one of us to sign", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form8.DIYMannerOfOperation == "Any two of us to sign") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Any two of us to sign", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form8.DIYMannerOfOperation == "All of us to sign") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("All of us to sign", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Note:", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("1. Where you do not elect a manner of operation, BWA will default to 'All of us to sign'.", max_column, 15f, TABLE_FONT_DETAILS_ITALIC, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("2. Phone Access, Online Access and a Debit Card cannot be selected unless the manner of operation is 'Any one of us to sign'.", max_column, 15f, TABLE_FONT_DETAILS_ITALIC, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            return tbl;
        }
        #endregion PAGE 8 : DIY - BWA CMA OPTIONS (Part 2)

        #region PAGE 8 : DIWM - BWA CMA OPTIONS (Part 1)
        public PdfPTable Page_Eight_DIWM_BWA_CMA_Options_I(UMAFormEntity Form8)
        {
            float[] widths = new float[] { 0.2f, 0.8f, 0.2f, 0.8f, 0.2f, 0.2f, 0.8f, 0.2f, 1.3f, 0.2f, 3f }; /*Array should be equal to no. of Columns*/
            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };
            int max_column = widths.Length;
            PdfPTable tbl = CreateTable(max_column, widths, 1f);
            tbl.AddCell(AddCell("DIWM - BWA CMA OPTIONS", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("The following selections will apply to my/our DIWM BWA CMA", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("BWA ACCESS FACILITIES", max_column, 18f, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Please tick ( √ ) the Access Facilities required:", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form8.DIWMPhoneAccess) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Phone Access", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form8.DIWMOnlineAccess) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Online Access", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form8.DIWMDebitCard) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Debit Card", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form8.DIWMChequeBook) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Cheque Book (25 per book)", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form8.DIWMDepositBook) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Deposit Book", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            return tbl;
        }
        #endregion PAGE 8 : DIWM - BWA CMA OPTIONS (Part 1)

        #region PAGE 8 : DIWM - BWA CMA OPTIONS (Part 2)
        public PdfPTable Page_Eight_DIWM_BWA_CMA_Options_II(UMAFormEntity Form8)
        {
            float[] widths = new float[] { 0.2f, 0.8f, 0.2f, 0.8f, 0.2f, 0.2f, 0.8f, 3f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);
            tbl.AddCell(AddCell("BWA MANNER OF OPERATION", max_column, 18f, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Please select how you wish to operate your CMA by ticking ( √ ) one of the following:", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form8.DIWMMannerOfOperation == "Any one of us to sign") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Any one of us to sign", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form8.DIWMMannerOfOperation == "Any two of us to sign") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Any two of us to sign", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form8.DIWMMannerOfOperation == "All of us to sign") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("All of us to sign", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Note:", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("1. Where you do not elect a manner of operation, BWA will default to 'All of us to sign'.", max_column, 15f, TABLE_FONT_DETAILS_ITALIC, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("2. Phone Access, Online Access and a Debit Card cannot be selected unless the manner of operation is 'Any one of us to sign'.", max_column, 15f, TABLE_FONT_DETAILS_ITALIC, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            return tbl;
        }
        #endregion PAGE 8 : DIWM - BWA CMA OPTIONS (Part 2)

        #region PAGE 9 : SECURITIES Part I
        public PdfPTable Page_Nine_SecuritiesI(UMAFormEntity Form9)
        {
            float[] widths = new float[] { 3f, 3f, 3f, 3f, 5f, 5f, 3f, 3f, 3f, 3f, 3f, 3f, 2f, 5f,  0.3f, 1f, 1f, 0.3f, 0.3f, 0.3f, 0.4f, 0.3f, 0.5f, 0.7f, 2f, 5f, 5f,
                                       1f, 1f, 1f, 0.3f, 0.3f, 0.3f, 0.4f, 0.3f, 0.5f, 0.7f, 2f, 5f,  0.3f, 1f, 1f, 0.3f, 0.3f, 0.3f, 0.4f, 0.3f, 0.5f, 0.7f, 2f, 5f, 5f,
                                       1f, 1f};

            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);
            /*ROW 1*/
            tbl.AddCell(AddCell("CHESS TRANSFER OF SECURITIES", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Do you want to transfer any existing listed securities to the new Desktop Broker account to be established in your name?", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*ROW 2*/
            tbl.AddCell(AddCell("Yes", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form9.TransferListedSecuritiesToDesktopBroker) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("No", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((!Form9.TransferListedSecuritiesToDesktopBroker) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*ROW 3*/
            tbl.AddCell(AddCell("If Yes to which service (ie. DIY, DIWM):", 7, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form9.ChessDIYDIWM, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, chk_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            return tbl;
        }
        #endregion PAGE 9 : SECURITIES Part I

        #region PAGE 9 : SECURITIES Part II
        public PdfPTable Page_Nine_SecuritiesII(UMAFormEntity Form9)
        {
            float[] widths = new float[] { 
            3f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f,
                                        0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f,
                                          0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f};

            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 5f };
            float[] highpen_padding = new float[] { 4.5f, 5f, 2f, 3f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);


            /*ROW 1*/
            tbl.AddCell(AddCell("Account Designation:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("(Designation must not be more than 25 characters)", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            for (int i = 0; i < Form9.ChessAccountDesignation.Length; i++)
            {
                tbl.AddCell(AddCell(Form9.ChessAccountDesignation.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
                tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            }

            return tbl;
        }
        #endregion PAGE 9 : SECURITIES Part II

        #region PAGE 9 : SECURITIES Part III
        public PdfPTable Page_Nine_SecuritiesIII(UMAFormEntity Form9)
        {
            float[] widths = new float[] { 2f, 3f, 13f, 27f, 2f, 3f, 18f, 2f, 3f, 18f };

            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };
            float[] highpen_padding = new float[] { 4.5f, 1f, 2f, 1f };
            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);
            tbl.AddCell(AddCell("CHESS REGISTRATION DETAILS ", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Please note that if you are transfering securities, your registered address should be EXACTLY the same as on your latest holding statement.", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("Registered Account Name:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form9.ChessRegisteredAccountName, 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("Account Designation::", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form9.ChessAccountDesignation, 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));


            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));


            tbl.AddCell(AddCell("CHESS Registration Address:", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*ROW 7*/
            tbl.AddCell(AddCell("Address Line 1:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form9.ChessAddressLine1, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 8*/
            tbl.AddCell(AddCell("Address Line 2", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form9.ChessAddressLine2, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 7*/
            tbl.AddCell(AddCell("Suburb:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form9.ChessSuburb, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("State:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form9.ChessState, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 8*/
            tbl.AddCell(AddCell("Postcode:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form9.ChessPostCode, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Country:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form9.ChessCountry, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            return tbl;
        }
        #endregion PAGE 9 : SECURITIES Part III


        #region PAGE 9 : SECURITIES Part IV
        public PdfPTable Page_Nine_SecuritiesIV(UMAFormEntity Form9)
        {
            float[] widths = new float[] { 0.4f, 0.5f, 0.4f, 3f, 1f, 2f, 4f, 4f };

            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);

            tbl.AddCell(AddCell("CHESS SPONSORSHIP TRANSFER  ", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Are the securities to be transfered.", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            /*ROW 1*/
            tbl.AddCell(AddCellImage((Form9.IssuedSponsor) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(" Issuer Sponsored - please attach copies of your Issuer Sponsored Holding Statements", 10, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 9*/
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCellImage((Form9.BrokerSponsored) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(" Broker Sponsored - please attach copies of your CHESS Registration", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            ///*ROW 10*/
            //tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            //tbl.AddCell(AddCell("i. Use the Broker to Broker Transfer Request, for each different Broker ", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            ///*ROW 11*/
            //tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            //tbl.AddCell(AddCell("ii. If names/addresses are not matched, use a Change of Details Form ", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            ///*ROW 12*/
            //tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            //tbl.AddCell(AddCell("iii. If there is a change of Legal or Beneficial ownership, use an off market transfer form for each holding ", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            ///*SEPARATOR*/
            //tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            ///*ROW 13*/

            //    tbl.AddCell(AddCellImage(IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            //    tbl.AddCell(AddCell("Name of existing Sponsoring Broker: ", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            //    tbl.AddCell(AddCell(Form9.ExistingBroker1, 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            //    tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            //    /*SEPARATOR*/
            //    tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            //    /*ROW 14*/
            //    tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            //    tbl.AddCell(AddCell("Existing Broker PID:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_CENTER, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            //    tbl.AddCell(AddCell(Form9.ClientNo1, 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            //    tbl.AddCell(AddCell("Client HIN: ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_CENTER, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            //    tbl.AddCell(AddCell(Form9.ClientHin1, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            //    /*SEPARATOR*/
            //    tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));


            //    /*ROW 13*/

            //    tbl.AddCell(AddCellImage(IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            //    tbl.AddCell(AddCell("Name of existing Sponsoring Broker: ", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            //    tbl.AddCell(AddCell(Form9.ExistingBroker2, 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            //    tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            //    /*SEPARATOR*/
            //    tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            //    /*ROW 14*/
            //    tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            //    tbl.AddCell(AddCell("Existing Broker PID:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_CENTER, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            //    tbl.AddCell(AddCell(Form9.ClientNo2, 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            //    tbl.AddCell(AddCell("Client HIN: ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_CENTER, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            //    tbl.AddCell(AddCell(Form9.ClientHin2, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            return tbl;
        }
        #endregion PAGE 9 : SECURITIES Part IV

        #region PAGE 7 : ACCOUNT CORRESPONDENCE DETAILS
        public PdfPTable Page_Seven_Account_Correspondence_Details(UMAFormEntity Form7)
        {
            float[] widths = new float[] { 3f, 5f, 0.5f, 0.3f, 1f, 5f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);

            tbl.AddCell(AddCell("ACCOUNT CORRESPONDENCE DETAILS", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));


            /*ROW 5*/
            tbl.AddCell(AddCell("Mailing Address: (Account Opening correspondence is sent to e-Clipse Online for collation and re-direction to the Client):", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 6*/
            tbl.AddCell(AddCell("Address Line 1:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.MialingAddressLine1, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 7*/
            tbl.AddCell(AddCell("Address Line 2:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.MialingAddressLine2, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 8*/
            tbl.AddCell(AddCell("Suburb:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.MialingSuburb, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("State:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.MialingState, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 9*/
            tbl.AddCell(AddCell("Postcode:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.MialingPostCode, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Country:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.MialingCountry, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 15*/
            tbl.AddCell(AddCell("Duplicate Statement Address: (for duplicate copy of Bankwest statement – Quarterly)", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 16*/
            tbl.AddCell(AddCell("Address Line 1:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.DupMialingAddressLine1, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 17*/
            tbl.AddCell(AddCell("Address Line 2:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.DupMialingAddressLine2, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 18*/
            tbl.AddCell(AddCell("Suburb:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.DupMialingSuburb, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("State:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.DupMialingState, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 19*/
            tbl.AddCell(AddCell("Postcode:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.DupMialingPostCode, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Country:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.DupMialingCountry, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));


            tbl.AddCell(AddCell("Bankwest Trust ID/AS PER ASIC REGISTER: Registered Address of the Corporate Trustee (A PO Box, RMB or c/- is not sufficent)", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 1*/
            tbl.AddCell(AddCell("Address Line 1:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.RegAddressLine1, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 2*/
            tbl.AddCell(AddCell("Address Line 2:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.RegAddressLine2, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 3*/
            tbl.AddCell(AddCell("Suburb:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.RegSuburb, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("State:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.RegState, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 4*/
            tbl.AddCell(AddCell("Postcode:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.RegPostCode, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Country:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.RegCountry, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));


            tbl.AddCell(AddCell("Bankwest Trust ID/AS PER ASIC REGISTER: Principal Place of the Business of the Trust/Super Fund/Company", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 1*/
            tbl.AddCell(AddCell("Address Line 1:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.PrincipalAddressLine1, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 2*/
            tbl.AddCell(AddCell("Address Line 2:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.PrincipalAddressLine2, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 3*/
            tbl.AddCell(AddCell("Suburb:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.PrincipalSuburb, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("State:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.PrincipalState, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 4*/
            tbl.AddCell(AddCell("Postcode:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.PrincipalPostCode, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Country:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell(Form7.PrincipalCountry, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

            /*SEPARATOR*/
            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            return tbl;
        }
        #endregion PAGE 7 : ACCOUNT CORRESPONDENCE DETAILS

        #region PAGE 10 : UMA DOCUMENT CHECKLIST
        public PdfPTable Page_Ten_UMA_Document_Checklist(UMAFormEntity Form10)
        {
            float[] widths = new float[] { 0.3f, 9.5f }; /*Array should be equal to no. of Columns*/

            /*Top, Right, Bottom, Left*/
            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

            int max_column = widths.Length;

            PdfPTable tbl = CreateTable(max_column, widths);

            tbl.AddCell(AddCell("UMA DOCUMENT CHECKLIST", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
            tbl.AddCell(AddCell("The following documents are required to establish bank and broker accounts.", max_column, 15f, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 1*/
            tbl.AddCell(AddCellImage((Form10.ProofIDCheckListItem) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Proof of ID (Drivers Licence or passport) ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 2*/
            tbl.AddCell(AddCellImage((Form10.TrustDeedChecListItem) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Trust Deed (If a Trust)", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 3*/
            tbl.AddCell(AddCellImage((Form10.LetterOfAccountantChecListItem) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Letter from your Accountant (If a sophisticated Investor)", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 4*/
            tbl.AddCell(AddCellImage((Form10.BusinessNameRegoChecListItem) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Business Name Registration Certificate (If a business account) or Certificate of Registration ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 5*/
            tbl.AddCell(AddCellImage((Form10.BrokerToBrokerChecListItem) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Broker to Broker Transfer", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 6*/
            tbl.AddCell(AddCellImage((Form10.IssueSponsorChessSponsorChecListItem) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Issuer Sponsored Holdings to CHESS Sponsorship Conversion", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 7*/
            tbl.AddCell(AddCellImage((Form10.IssueSponsorChessSponsorHoldsingStatements) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Issuer Sponsored Holding Statements", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 8*/
            tbl.AddCell(AddCellImage((Form10.OffMarketTransferChecListItem) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Off Market Transfer", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 9*/
            tbl.AddCell(AddCellImage((Form10.ChangeOFClientChecListItem) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Change of Client Details Form", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            /*ROW 9*/
            tbl.AddCell(AddCellImage((Form10.ProofOfAddressChecListItem) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            tbl.AddCell(AddCell("Proof of Address (when passport used for ID) - copy of utilities bill, bank statement or tax assessment notice will be accepted", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

            return tbl;
        }
        #endregion PAGE 10 : UMA DOCUMENT CHECKLIST

        #region CREATE PDF
        public void CreatePDF(string path, HttpResponse response)
        {
            HttpResponse Response = HttpContext.Current.Response;
            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + this.UMAFormEntity.accountName + ".pdf");
            // step 1: creation of a document-object
            //Instantiate a document object set pagesize and margin left, right, top, bottom
            Document document = new Document(PageSize.LETTER, 20, 20, 30, 30);
            // step 2: we create a writer that listens to the document
            PdfWriter writer = PdfWriter.GetInstance(document, Response.OutputStream);
            try
            {

                HttpContext Page = HttpContext.Current;
                String pdfFilePath = path;

                document.Open();

                document.Add(AccountDetails());
                document.Add(FeeStructure());
                document.Add(TFNABNABRN());
                document.Add(this.TFNABNABRN_Part2());
                document.Add(this.TFNABNABRN_Part3());
                document.Add(this.Authority_To_Act());
                document.Add(this.Investor_Status());
                document.Add(this.Services());
                document.Add(this.PlatformsDetails());
                document.Add(this.McquarieCMA());
                document.Add(this.McquarieCMADetails());

                SignatoryEntity sig1 = UMAFormEntity.SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault();

                document.Add(this.InvestorDetails_I(sig1));
                document.Add(this.InvestorDetails_II(sig1));
                // document.Add(this.InvestorDetails_III(sig1));
                document.Add(this.InvestorDetails_IV(sig1));

                if (UMAFormEntity.Sig2Required)
                {
                    SignatoryEntity sig2 = UMAFormEntity.SignatoryEntityList.Where(sig => sig.SigNo == 2).FirstOrDefault();

                    document.Add(this.InvestorDetails_I(sig2));
                    document.Add(this.InvestorDetails_II(sig2));
                    document.Add(this.InvestorDetails_III(sig2));
                    document.Add(this.InvestorDetails_IV(sig2));
                }

                if (UMAFormEntity.Sig3Required)
                {
                    SignatoryEntity sig3 = UMAFormEntity.SignatoryEntityList.Where(sig => sig.SigNo == 3).FirstOrDefault();

                    document.Add(this.InvestorDetails_I(sig3));
                    document.Add(this.InvestorDetails_II(sig3));
                    document.Add(this.InvestorDetails_III(sig3));
                    document.Add(this.InvestorDetails_IV(sig3));
                }


                if (UMAFormEntity.Sig4Required)
                {
                    SignatoryEntity sig4 = UMAFormEntity.SignatoryEntityList.Where(sig => sig.SigNo == 4).FirstOrDefault();

                    document.Add(this.InvestorDetails_I(sig4));
                    document.Add(this.InvestorDetails_II(sig4));
                    document.Add(this.InvestorDetails_III(sig4));
                    document.Add(this.InvestorDetails_IV(sig4));
                }

                document.Add(Page_Eight_DIY_BWA_CMA_Options_I(UMAFormEntity));
                document.Add(Page_Eight_DIY_BWA_CMA_Options_II(UMAFormEntity));
                document.Add(Page_Eight_DIWM_BWA_CMA_Options_I(UMAFormEntity));
                document.Add(Page_Eight_DIWM_BWA_CMA_Options_II(UMAFormEntity));
                document.Add(Page_Nine_SecuritiesI(UMAFormEntity));
                document.Add(Page_Nine_SecuritiesIII(UMAFormEntity));
                document.Add(Page_Nine_SecuritiesIV(UMAFormEntity));
                document.Add(Page_Seven_Account_Correspondence_Details(UMAFormEntity));
            }
            catch (DocumentException docEx)
            {
                errMsg = docEx.Message;
            }
            catch (IOException ioEx)
            {
                errMsg = ioEx.Message;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
            }
            finally
            {
                document.Close();
            }
        }
        #endregion CREATE PDF

        #region CREATE TABLE
        private PdfPTable CreateTable(int columns, float[] width, float after = 20f)
        {
            PdfPTable table = new PdfPTable(columns);
            table.TotalWidth = TABLE_WIDTH;
            table.WidthPercentage = TABLE_WIDTH;
            table.SetWidths(width);
            table.SpacingBefore = 0f;
            table.SpacingAfter = after;
            return table;
        }
        #endregion CREATE TABLE

        #region ADD CELL
        private PdfPCell AddCell(string label, int colSpan, float height, Font font, int alignment, float[] padding, float border, BaseColor bordercolor, BaseColor bgcolor)
        {
            /*label, colspan, size, fontstyle, color, alignment, padding, border, bordercolor, bgcolor*/
            PdfPCell cell = new PdfPCell(new Phrase(label, font));

            cell.BackgroundColor = bgcolor;
            cell.BorderColor = bordercolor;
            cell.Colspan = colSpan;
            cell.BorderWidth = border;
            cell.HorizontalAlignment = alignment;
            cell.FixedHeight = height;
            cell.PaddingTop = padding[0];
            cell.PaddingRight = padding[1];
            cell.PaddingBottom = padding[2];
            cell.PaddingLeft = padding[3];

            return cell;
        }
        #endregion ADD CELL

        #region ADD CELL IMAGE
        private PdfPCell AddCellImage(string path, int colSpan, int alignment, float[] padding, float border, BaseColor bordercolor, BaseColor bgcolor)
        {
            /*path, colspan, alignment, padding, border, bordercolor, bgcolor*/

            HttpContext Page = HttpContext.Current;
            Image image = Image.GetInstance(Page.Server.MapPath(path));

            PdfPCell cell = new PdfPCell(image);

            cell.BackgroundColor = bgcolor;
            cell.BorderColor = bordercolor;
            cell.Colspan = colSpan;
            cell.BorderWidth = border;
            cell.HorizontalAlignment = alignment;

            return cell;
        }
        #endregion ADD CELL IMAGE

    }
}