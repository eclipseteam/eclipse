﻿using System;
using System.Web;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;

namespace eclipseonlineweb
{
    public partial class DownloadForm : UMABasePage
    {
        public override void LoadPage()
        {
            try
            {
                IBrokerManagedComponent orgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
                ExportAppFormsDS ds = new ExportAppFormsDS();
                string serviceTypeStr = Request.Params["ServiceType"];
                ServiceTypes serviceType = ServiceTypes.None;

                switch (serviceTypeStr)
                { 
                    case "DIFM":
                        serviceType = ServiceTypes.DoItForMe;
                        break;
                    case "DIWM":
                        serviceType = ServiceTypes.DoItWithMe;
                        break;
                    case "DIY":
                        serviceType = ServiceTypes.DoItYourSelf;
                        break;
                    default:
                        serviceType = ServiceTypes.None;
                        break;
                }

                ds.serviceType = serviceType;
                ds.AppNo = Request.Params["AppNo"];
                orgCM.GetData(ds);
                this.UMABroker.ReleaseBrokerManagedComponent(orgCM);
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                HttpContext.Current.Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
                Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });
                Response.AppendHeader("Content-Type", "application/zip");
                Response.AppendHeader("Content-disposition", "attachment; filename=" + ds.AppNo + ".zip");
                Response.BinaryWrite(Convert.FromBase64String(ds.Contents));
                Response.Flush();
                Response.End();
            }
            
            catch(Exception ex)
            {
                string strScript = null;
                strScript = "<script>";
                strScript = strScript + "alert('" + ex.Message + "');";
                strScript = strScript + "</script>";
                ClientScript.RegisterStartupScript(GetType(), "ClientScript", strScript);
            }
        }

    }
}
