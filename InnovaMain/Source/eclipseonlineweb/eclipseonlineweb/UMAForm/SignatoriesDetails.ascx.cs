﻿using System;
using C1.Web.Wijmo.Controls.C1ComboBox;
using Oritax.TaxSimp.Utilities;
using eclipseonlineweb.WebUtilities;

// ReSharper disable CheckNamespace
namespace eclipseonlineweb
// ReSharper restore CheckNamespace
{
    public partial class SignatoriesDetails : System.Web.UI.UserControl
    {
        public string Title
        {
            get
            {
                if (rbTitle1.SelectedItem != null)
                {
                    return rbTitle1.SelectedItem.Text.ToLower() == "other" ?
                        txtOtherTitle.Text : rbTitle1.SelectedItem.Text;
                }

                return string.Empty;
            }
            set
            {
                if (value != string.Empty)
                {
                    if (rbTitle1.Items.FindByText(value) != null)
                        rbTitle1.Items.FindByText(value).Selected = true;
                    else
                    {
                        rbTitle1.Items.FindByText("Other").Selected = true;
                        txtOtherTitle.Text = value;
                    }
                }
            }
        }

        public string FamilyName
        {
            get
            {
                return txtFamilyName.Text;
            }
            set
            {
                txtFamilyName.Text = value;
            }
        }

        public string GivenName
        {
            get
            {
                return txtFirstName.Text;
            }
            set
            {
                txtFirstName.Text = value;
            }
        }
        public string MiddleName
        {
            get
            {
                return txtMiddleName.Text;
            }
            set
            {
                txtMiddleName.Text = value;
            }
        }

        public string DocumentNoPassport
        {
            get
            {
                return txtDocumentNoPassport.Text;
            }
            set
            {
                txtDocumentNoPassport.Text = value;
            }
        }

        public string DocumentNoLic
        {
            get
            {
                return txtDocumentNoLic.Text;
            }
            set
            {
                txtDocumentNoLic.Text = value;
            }
        }

        public string OtherName
        {
            get
            {
                return txtOtherNames.Text;
            }
            set
            {
                txtOtherNames.Text = value;
            }
        }

        public string DateOfBirth
        {
            get
            {
                return DateOfBirthInput.DateInput.DisplayText;
            }
            set
            {
                DateOfBirthInput.SelectedDate = Utilities.GetDateFromString(value, "dd/MM/yyyy");
                DateOfBirthInput.DateInput.DisplayText = value;
            }
        }

        public string CorporateTite
        {
            get
            {
                if (chkDirector.Checked)
                    return chkDirector.Text;
                if (chkSecretary.Checked)
                    return chkSecretary.Text;
                if (chkNotApplicable.Checked)
                    return chkSecretary.Text;
                return chkOther.Checked ?
                    txtOtherTitleCorporate.Text : string.Empty;
            }
            set
            {
                if (value == chkDirector.Text)
                    chkDirector.Checked = true;
                else if (value == chkNotApplicable.Text)
                    chkNotApplicable.Checked = true;
                else if (value == chkSecretary.Text)
                    chkSecretary.Checked = true;
                else if (value != string.Empty)
                {
                    chkOther.Checked = true;
                    txtOtherTitleCorporate.Text = value;
                }
            }
        }

        public string ResiAddressLine1
        {
            get
            {
                return txtAddressLine1.Text;
            }
            set
            {
                txtAddressLine1.Text = value;
            }
        }

        public C1ComboBox ResidentialState
        {
            get
            {
                return C1ComboBoxStateSigAddress;
            }
        }

        public C1ComboBox ResidentialCountry
        {
            get
            {
                return C1ComboBoxCountryListSigAddress;
            }
        }

        public C1ComboBox MailingState
        {
            get
            {
                return C1ComboBoxSigAddressMailingState;
            }
        }

        public C1ComboBox MailingCountry
        {
            get
            {
                return C1ComboBoxCountryListSigAddressMailing;
            }
        }


        public string ResiAddressLine2
        {
            get
            {
                return txtAddressLine2.Text;
            }
            set
            {
                txtAddressLine2.Text = value;
            }
        }
        public string ResiSuburb
        {
            get
            {
                return txtSuburbSigAddress.Text;
            }
            set
            {
                txtSuburbSigAddress.Text = value;
            }
        }
        public string ResiState
        {
            get
            {
                return C1ComboBoxStateSigAddress.SelectedItem != null ?
                    C1ComboBoxStateSigAddress.SelectedItem.Text : C1ComboBoxStateSigAddress.Text;
            }
            set
            {
                C1ComboBoxStateSigAddress.SelectedValue = AusStates.ReturnShortStateCodeFromLong(value); 
            }
        }
        public string ResiPostCode
        {
            get
            {
                return txtPostcodeSigAddress.Text;
            }
            set
            {
                txtPostcodeSigAddress.Text = value;
            }
        }
        public string ResiCountry
        {
            get
            {
                return C1ComboBoxCountryListSigAddress.SelectedItem != null ?
                    C1ComboBoxCountryListSigAddress.SelectedItem.Text : C1ComboBoxCountryListSigAddress.Text;
            }
            set
            {
                C1ComboBoxCountryListSigAddress.Text = value;
            }
        }

        public string MailAddressLine1
        {
            get
            {
                return txtAddressLine1SigAddressMailing.Text;
            }
            set
            {
                txtAddressLine1SigAddressMailing.Text = value;
            }
        }
        public string MailAddressLine2
        {
            get
            {
                return txtAddressLine2SigAddressMailing.Text;
            }
            set
            {
                txtAddressLine2SigAddressMailing.Text = value;
            }
        }

        public string MailSuburb
        {
            get
            {
                return txtSuburbSigAddressMailing.Text;
            }
            set
            {
                txtSuburbSigAddressMailing.Text = value;
            }
        }

        public string MailState
        {
            get
            {
                return C1ComboBoxSigAddressMailingState.SelectedItem != null ?
                    C1ComboBoxSigAddressMailingState.SelectedItem.Text : C1ComboBoxSigAddressMailingState.Text;
            }
            set
            {
                C1ComboBoxSigAddressMailingState.SelectedValue = AusStates.ReturnShortStateCodeFromLong(value); 
            }
        }

        public string MailPostCode
        {
            get
            {
                return txtPostCodeSigAddressMailing.Text;
            }
            set
            {
                txtPostCodeSigAddressMailing.Text = value;
            }
        }

        public string MailCountry
        {
            get
            {
                return C1ComboBoxCountryListSigAddressMailing.SelectedItem != null ?
                    C1ComboBoxCountryListSigAddressMailing.SelectedItem.Text : C1ComboBoxCountryListSigAddressMailing.Text;
            }
            set
            {
                C1ComboBoxCountryListSigAddressMailing.Text = value;
            }
        }


        public string Occupation
        {
            get
            {
                return txtOccupation.Text;
            }
            set
            {
                txtOccupation.Text = value;
            }
        }

        public string Employer
        {
            get
            {
                return txtEmployer.Text;
            }
            set
            {
                txtEmployer.Text = value;
            }
        }

        public string MainCountryOFAddressSig
        {
            get
            {
                return txtNotAustraliaResidence.Text;
            }
            set
            {
                txtNotAustraliaResidence.Text = value;
            }
        }
        public string ContactPH
        {
            get
            {
                return txtContactPH.Text;
            }
            set
            {
                txtContactPH.Text = value;
            }
        }
        public string MobilePH
        {
            get
            {
                return txtMobile.Text;
            }
            set
            {
                txtMobile.Text = value;
            }
        }
        public string AlternativePH
        {
            get
            {
                return txtAlternatePH.Text;
            }
            set
            {
                txtAlternatePH.Text = value;
            }
        }
        public string Fax
        {
            get
            {
                return txtFax.Text;
            }
            set
            {
                txtFax.Text = value;
            }
        }
        public string Email
        {
            get
            {
                return txtEmail.Text;
            }
            set
            {
                txtEmail.Text = value;
            }
        }

        public bool DriverLicSig
        {
            get
            {
                return chkDriverLic.Checked;
            }
            set
            {
                chkDriverLic.Checked = value;
            }
        }

        public bool PassportSig
        {
            get
            {
                return chkPassportID.Checked;
            }
            set
            {
                chkPassportID.Checked = value;
            }
        }

        public string LicDocumentIssuer
        {
            get
            {
                return txtDocumentIssuerDriverLic.Text;
            }
            set
            {
                txtDocumentIssuerDriverLic.Text = value;
            }
        }

        public bool LicCertifiedCopyAttached
        {
            get
            {
                return chkCertifiedCopiesAttachedDriverLic.Checked;
            }
            set
            {
                chkCertifiedCopiesAttachedDriverLic.Checked = value;
            }
        }

        public string LicIssueDate
        {
            get
            {
                return txtIssueDateDriverLic.DateInput.DisplayText;
            }
            set
            {

                txtIssueDateDriverLic.SelectedDate = Utilities.GetDateFromString(value, "dd/MM/yyyy");
                txtIssueDateDriverLic.DateInput.DisplayText = value;
            }
        }

        public string LicExpiryDate
        {
            get
            {
                return txtExpiryDateDriverLic.DateInput.DisplayText;

            }
            set
            {
                txtExpiryDateDriverLic.SelectedDate = Utilities.GetDateFromString(value, "dd/MM/yyyy");
                txtExpiryDateDriverLic.DateInput.DisplayText = value;
            }
        }


        public string PassportDocumentIssuer
        {
            get
            {
                return txtDocumentIssuerPassport.Text;
            }
            set
            {
                txtDocumentIssuerPassport.Text = value;
            }
        }

        public bool PassportCertifiedCopyAttached
        {
            get
            {
                return chkCertifiedCopiesAttachedPassport.Checked;
            }
            set
            {
                chkCertifiedCopiesAttachedPassport.Checked = value;
            }
        }

        public string PassportIssueDate
        {
            get
            {
                return txtIssueDatePassportID.DateInput.DisplayText;
            }
            set
            {
                txtIssueDatePassportID.SelectedDate = Utilities.GetDateFromString(value, "dd/MM/yyyy");
                txtIssueDatePassportID.DateInput.DisplayText = value;
            }
        }

        public string PassportExpiryDate
        {
            get
            {
                return txtExpiryDatePassportID.DateInput.DisplayText;
            }
            set
            {
                txtExpiryDatePassportID.SelectedDate = Utilities.GetDateFromString(value, "dd/MM/yyyy");
                txtExpiryDatePassportID.DateInput.DisplayText = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            chkDriverLic.CheckedChanged += chkDriverLic_CheckedChanged;
            ChkMailingAddressSig1.CheckedChanged += ChkMailingAddressSig1_CheckedChanged;
            chkPassportID.CheckedChanged += chkPassportID_CheckedChanged;
        }

        private void chkDriverLic_CheckedChanged(object sender, EventArgs e)
        {
            pnlDriverLic.Visible = chkDriverLic.Checked;
        }

        private void ChkMailingAddressSig1_CheckedChanged(object sender, EventArgs e)
        {
            pnlMialingAddressSig1.Visible = ChkMailingAddressSig1.Checked;
        }

        private void chkPassportID_CheckedChanged(object sender, EventArgs e)
        {
            pnlPassportIDDetails.Visible = chkPassportID.Checked;
        }
    }
}