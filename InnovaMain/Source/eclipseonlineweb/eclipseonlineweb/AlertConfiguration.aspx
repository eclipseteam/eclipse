﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/Home.master" EnableEventValidation="false"
    AutoEventWireup="true" CodeBehind="AlertConfiguration.aspx.cs" Inherits="eclipseonlineweb.AlertConfiguration" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register TagPrefix="uc" TagName="AlertConfigurationTabsControl" Src="~/Controls/AlertConfigurationTabsControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .RadGrid_Metro .rgHeader, .RadGrid_Metro .rgHeader a
        {
            font-size: 8pt !important;
        }
        .RadGrid_Metro .rgRow a, .RadGrid_Metro .rgAltRow a, .RadGrid_Metro tr.rgEditRow a, .RadGrid_Metro .rgFooter a, .RadGrid_Metro .rgEditForm a
        {
            font-size: 8pt !important;
        }
        .MyGridClass .rgDataDiv
        {
            height: auto !important;
        }
    </style>
    <script type="text/javascript">
        //Resetting menu index
        localStorage['MenuIndex'] = '0';
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.rgDataDiv').height('135px');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblConfig" Text="Alert Configuration"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <div id="divAlertConfig" runat="server">
                <uc:AlertConfigurationTabsControl ID="ctlAlertConfig" runat="server"></uc:AlertConfigurationTabsControl>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
