﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="ReportsMaster.master"
    AutoEventWireup="true" CodeBehind="GainsLossesReport.aspx.cs" Inherits="eclipseonlineweb.GainsLossesReport" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer"
    TagPrefix="C1ReportViewer" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
               <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <span class="riLabel">Start Date:</span><telerik:RadMonthYearPicker ID="InputStartDate"
                                runat="server">
                            </telerik:RadMonthYearPicker>
                        </td>
                        <td style="width: 5%">
                        <span class="riLabel">End Date:</span><telerik:RadMonthYearPicker ID="InputEndDate"
                                runat="server">
                            </telerik:RadMonthYearPicker>
                            
                        </td>
                         <td style="width: 5%">

                         <span class="riLabel">Cost Calculation Method:</span>
                         <telerik:RadComboBox runat="server" ID="comboBoxCostType"> </telerik:RadComboBox>
                          
                        </td>
                        <td style="width: 5%"><br />
                        <telerik:RadButton runat="server" ID="BtnGenerateReport" OnClick="GenerateReport" Text="Generate Report"></telerik:RadButton>
                            
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td></td>
                        <%--<td style="width: 45%; background-color:Yellow">
                        <p style="color:Red"><span style="color:Red" class="riLabel">Please note the 'Tax Pack' is in draft format until further notice. Please do not rely on this information for tax purposes.</span></p>
                        </td>--%>
                         <td></td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb2" runat="server" />
                            <br />
                            <uc1:clientheaderinfo ID="ClientHeaderInfo2" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <C1ReportViewer:C1ReportViewer CollapseToolsPanel="true" Cache-Enabled="false" Cache-ShareBetweenSessions="false"
                FileName="C1ReportViewerAssetSummary" runat="server" ID="C1ReportViewerAssetClass"
                Zoom="75%" Height="550px" Width="100%">
            </C1ReportViewer:C1ReportViewer>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
