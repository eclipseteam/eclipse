﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using System.Collections;
using System.Collections.ObjectModel;
using Oritax.TaxSimp.Utilities;
using Telerik.Web.UI;
using System.Configuration;

namespace eclipseonlineweb
{
    public partial class TaxPackReport : UMABasePage
    {

        private List<AssetEntity> assets = null;
        string selectCostType = string.Empty;
        Hashtable dsList = new Hashtable();

        public override void PopulatePage(DataSet ds)
        {
            this.cid = Request.QueryString["ins"].ToString();
            if (!Page.IsPostBack)
            {
                comboBoxCostType.DataSource = Enumeration.GetAll<CostTypes>();
                comboBoxCostType.DataTextField = "Value";
                comboBoxCostType.DataValueField = "Key";
                comboBoxCostType.DataBind();

                var defaultItem = comboBoxCostType.Items.Where(i => i.Text == "FIFO").FirstOrDefault();
                if (defaultItem != null)
                    defaultItem.Selected = true;
            }

            base.PopulatePage(ds);
            ReportClick();
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public C1PrintDocument MakeDoc()
        {
            PresentationData = (DataSet)this.dsList["LossAndGainsReportDS"];
            BaseClientDetails baseClientDetails = (BaseClientDetails)this.dsList["BaseClientDetails"];
            C1PrintDocument doc2 = C1ReportViewer.CreateC1PrintDocument();
            doc2.Style.BackColor = Color.Azure;

            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            RenderToc renderTOC = AddTitleAndTOCPage(doc, "eclipseUmaTaxPack.png");

            DataRow clientSummaryRow = this.PresentationData.Tables[LossAndGainsReportDS.CLIENTSUMMARYTABLE].Rows[0];

            if (baseClientDetails.SETADDRESSLINE1 == string.Empty)
                AddClientHeaderToReport(doc, clientSummaryRow, ((LossAndGainsReportDS)this.PresentationData).DateInfo(), "TAX PACK");
            else
                AddClientHeaderToReportWithAddress(doc, clientSummaryRow, ((LossAndGainsReportDS)this.PresentationData).DateInfo(), "TAX PACK", baseClientDetails.SETADDRESSLINE1, baseClientDetails.SETADDRESSLINE2, baseClientDetails.SETSUBURB, baseClientDetails.SETPOSTCODE, baseClientDetails.SETSTATE, baseClientDetails.SETCOUNTRY);


            RenderText gridHeaderTotal = new RenderText();
            if (comboBoxCostType.SelectedItem != null)
                gridHeaderTotal.Text = "*Cost Calculation Method - " + comboBoxCostType.SelectedItem.Text + "\n\n";
            else
                gridHeaderTotal.Text = "*Cost Calculation Method - " + comboBoxCostType.Text + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 8;
            gridHeaderTotal.Style.FontBold = true;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n";

            RenderText gridHeaderTotal2 = new RenderText();
            if (this.comboBoxAccType.SelectedItem != null)
                gridHeaderTotal2.Text = "*Accounting Method - " + comboBoxAccType.SelectedItem.Text + "\n\n";
            else
                gridHeaderTotal2.Text = "*Accounting Method - " + comboBoxAccType.Text + "\n\n";
            gridHeaderTotal2.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal2.Style.FontSize = 8;
            gridHeaderTotal2.Style.FontBold = true;

            RenderText lineBreakTotalHeader = new RenderText();
            lineBreakTotalHeader.Text = "\n";
            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(lineBreakTotal);
            doc.Body.Children.Add(gridHeaderTotal2);
            doc.Body.Children.Add(lineBreakTotalHeader);

            if (C1ComboBoxReports.FindItemByText("TAX SUMMARY").Checked)
            {
                SetTaxReport(doc, renderTOC);
                RenderText lineBreakPage = new RenderText();
                lineBreakPage.BreakAfter = BreakEnum.Page;
                lineBreakPage.Text = "\n";
                doc.Body.Children.Add(lineBreakPage);
            }

            if (C1ComboBoxReports.FindItemByText("INVESTMENT TRANSACTION LISTING").Checked)
            {
                SetCapitalReport((HoldingSummaryReportDS)this.dsList["HoldingSummaryReportDS"], doc, renderTOC);
                RenderText lineBreakPage = new RenderText();
                lineBreakPage.BreakAfter = BreakEnum.Page;
                lineBreakPage.Text = "";
                doc.Body.Children.Add(lineBreakPage);
            }

            if (C1ComboBoxReports.FindItemByText("GAINS & LOSSES").Checked)
            {
                GainsAndLosses(doc, renderTOC);
                RenderText lineBreakPage = new RenderText();
                lineBreakPage.BreakAfter = BreakEnum.Page;
                lineBreakPage.Text = "";
                doc.Body.Children.Add(lineBreakPage);
            }

            if (C1ComboBoxReports.FindItemByText("HOLDING SUMMARY").Checked)
            {
                HoldingSummaryReportDS holdingSummaryReportDS = (HoldingSummaryReportDS)this.dsList["HoldingSummaryReportDS"];

                //Add CASH NO RCV ITEM TO HOLDING TABLE
                ExtractHoldingSummaryReport(holdingSummaryReportDS, doc, renderTOC, "Holding Summary (Cash)", AcountingMethodType.Cash);

                if (this.comboBoxAccType.Text.ToLower() != "cash")
                {
                    var cashNRRows = ((LossAndGainsReportDS)this.dsList["LossAndGainsReportDS"]).Tables[LossAndGainsReportDS.TAXREPORTINGMATRIXTABLE].Select().Where(row => row[LossAndGainsReportDS.ACCOUNTTYPE].ToString() == "CASH NR");

                    foreach (DataRow row in cashNRRows)
                        holdingSummaryReportDS.AddSecuritySummaryRow("CASH", row[LossAndGainsReportDS.ACCOUNTNO].ToString(), "CASH NR", row[LossAndGainsReportDS.ACCOUNTNO].ToString(), DateTime.Now, 0, 0, 0, 0, 0, 0, 0, (decimal)row[LossAndGainsReportDS.DISTRIBUTION], 0, 0, (decimal)row[LossAndGainsReportDS.DISTRIBUTION], (decimal)row[LossAndGainsReportDS.DISTRIBUTION], (decimal)row[LossAndGainsReportDS.DISTRIBUTION]);

                    ExtractHoldingSummaryReport(holdingSummaryReportDS, doc, renderTOC, "Holding Summary (Includes Accrued Distribution)", AcountingMethodType.Accrual);
                }

                RenderText lineBreakPage = new RenderText();
                lineBreakPage.BreakAfter = BreakEnum.Page;
                lineBreakPage.Text = "";
                doc.Body.Children.Add(lineBreakPage);
            }

            if (C1ComboBoxReports.FindItemByText("HOLDING STATEMENTS").Checked)
            {
                SetHoldingStatement(doc, (HoldingSummaryReportDS)this.dsList["HoldingSummaryReportDS"], renderTOC);
                SetHoldingStatementSEC(doc, (HoldingSummaryReportDS)this.dsList["HoldingSummaryReportDS"], renderTOC);
                RenderText lineBreakPage = new RenderText();
                lineBreakPage.BreakAfter = BreakEnum.Page;
                lineBreakPage.Text = "";
                doc.Body.Children.Add(lineBreakPage);
            }

            if (C1ComboBoxReports.FindItemByValue("INCOME").Checked)
            {
                SetIncomeReport(doc, renderTOC);
                RenderText lineBreakPage = new RenderText();
                lineBreakPage.BreakAfter = BreakEnum.Page;
                lineBreakPage.Text = "";
                doc.Body.Children.Add(lineBreakPage);
            }

            if (C1ComboBoxReports.FindItemByText("TAX DISTRIBUTION").Checked)
            {
                SetTaxDistribution(doc, renderTOC);
                RenderText lineBreakPage = new RenderText();
                lineBreakPage.BreakAfter = BreakEnum.Page;
                lineBreakPage.Text = "";
                doc.Body.Children.Add(lineBreakPage);
            }

            doc.PageLayout.PageSettings.Landscape = true;
            AddFooter(doc);


            return doc;
        }

        private void SetTaxReport(C1PrintDocument doc, RenderToc toc)
        {
            RenderText celltext = new RenderText(doc);
            celltext.Text = "TAX REPORT" + "\n\n";
            celltext.Style.TextColor = Color.FromArgb(46, 44, 83);
            celltext.Style.TextAlignHorz = AlignHorzEnum.Left;
            celltext.Style.FontSize = 16;
            celltext.Style.FontBold = true;
            doc.Body.Children.Add(celltext);

            toc.AddItem("Tax Summary", celltext);

            PresentationData = (DataSet)this.dsList["LossAndGainsReportDS"];

            DataView secSummaryTableViewOverall = new DataView(PresentationData.Tables[LossAndGainsReportDS.TAXREPORTINGMATRIXTABLE]);
            secSummaryTableViewOverall.Sort = LossAndGainsReportDS.ACCOUNTTYPE + " ASC, " + LossAndGainsReportDS.ACCOUNTNO + " ASC";

            if (this.comboBoxAccType.Text.ToLower() != "cash")
                Tax_SummmaryTableOverall(doc, secSummaryTableViewOverall.ToTable(), "Tax Summary (Includes Accrued Distribution)", AcountingMethodType.Accrual);

            DataView viewNoCashNR = new DataView(secSummaryTableViewOverall.ToTable());
            viewNoCashNR.RowFilter = LossAndGainsReportDS.ACCOUNTTYPE + " <> 'CASH NR'";
            Tax_SummmaryTableOverall(doc, viewNoCashNR.ToTable(), "Tax Summary (Cash Basis)", AcountingMethodType.Cash);

            DataView secSummaryTableView = new DataView(PresentationData.Tables[LossAndGainsReportDS.SECURITYSUMMARYTABLEUNREALISED]);
            secSummaryTableView.Sort = LossAndGainsReportDS.SECNAME + " ASC";

            DataView interestIncomeTrans = new DataView(PresentationData.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS]);
            interestIncomeTrans.RowFilter = LossAndGainsReportDS.SYSTRANSACIONTYPE + "= 'Interest' OR " + LossAndGainsReportDS.SYSTRANSACIONTYPE + "= 'Commission Rebate'";
            interestIncomeTrans.Sort = LossAndGainsReportDS.ACCOUNTNO + " ASC";

            DataView disIncomeTrans = new DataView(PresentationData.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS]);
            disIncomeTrans.RowFilter = LossAndGainsReportDS.SYSTRANSACIONTYPE + "= 'Distribution'";
            disIncomeTrans.Sort = LossAndGainsReportDS.ACCOUNTNO + " ASC";

            DataView divIncomeTrans = new DataView(PresentationData.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS]);
            divIncomeTrans.RowFilter = LossAndGainsReportDS.SYSTRANSACIONTYPE + "= 'Dividend'";
            divIncomeTrans.Sort = LossAndGainsReportDS.ACCOUNTNO + " ASC";

            DataView rentalIncomeTrans = new DataView(PresentationData.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS]);
            rentalIncomeTrans.RowFilter = LossAndGainsReportDS.SYSTRANSACIONTYPE + "= 'Rental Income'";
            rentalIncomeTrans.Sort = LossAndGainsReportDS.ACCOUNTNO + " ASC";

            Tax_BankDetailTable(doc, interestIncomeTrans.ToTable(), "Interest", "Income");
            Tax_BankDetailTable(doc, disIncomeTrans.ToTable(), "Distribution", "Income");
            Tax_BankDetailTable(doc, divIncomeTrans.ToTable(), "Dividend", "Income");
            Tax_BankDetailTable(doc, rentalIncomeTrans.ToTable(), "Rental Income", "Income");
        }

        private void SetTaxDistribution(C1PrintDocument doc, RenderToc toc)
        {
            RenderText lineBreakTotalMainItem = new RenderText();
            doc.Body.Children.Add(lineBreakTotalMainItem);
            RenderTocItem tocItem = toc.AddItem("Tax Distributions Statements", lineBreakTotalMainItem);

            ClientTaxDistributionsDS clientTaxDistributionsDS = (ClientTaxDistributionsDS)this.dsList["ClientTaxDistributionsDS"];
            HoldingSummaryReportDS HoldingSummaryReportDS = (HoldingSummaryReportDS)this.dsList["HoldingSummaryReportDS"];
            IEnumerable<DistributionIncomeEntity> DistributionIncomeEntityList = clientTaxDistributionsDS.DistributionIncomeEntityList.Where(dis => dis.RecordDate >= HoldingSummaryReportDS.StartDate && dis.RecordDate <= HoldingSummaryReportDS.EndDate);

            var slectedDistributionStatement = DistributionIncomeEntityList.Where(dis => dis.NetCashDistribution != 0).OrderBy(dis => dis.FundCode).OrderBy(dis => dis.PaymentDate);
            var groupBySlectedDistributionStatement = slectedDistributionStatement.GroupBy(dis => dis.FundCode).OrderBy(dis => dis.Key);


            foreach (var disEnitityGroupItem in groupBySlectedDistributionStatement)
            {
                var disItem = disEnitityGroupItem.FirstOrDefault();

                RenderText gridHeader = new RenderText();
                gridHeader.Text = "Annual Taxation Statement for the year ended " + HoldingSummaryReportDS.EndDate.ToString("d MMM yyyy") + "\n For " + disItem.FundName + " - " + disItem.FundCode + "\n";
                gridHeader.Style.TextColor = Color.Black;
                gridHeader.Style.FontSize = 12;
                gridHeader.Style.FontBold = true;
                gridHeader.Style.TextAlignHorz = AlignHorzEnum.Center;
                doc.Body.Children.Add(gridHeader);

                RenderTocItem tocItemSub = toc.AddItem(disItem.FundName + " - " + disItem.FundCode, gridHeader);
                tocItemSub.Level = 2;

                List<DistributionIncomeEntity> filteredList = slectedDistributionStatement.Where(dis => dis.FundCode == disEnitityGroupItem.Key).ToList();
                SetComponentHeaderTable(doc, filteredList);

                Dictionary<int, double> grossDistributionTotals = new Dictionary<int, double>();
                Dictionary<int, double> withholdingTaxTotals = new Dictionary<int, double>();
                Dictionary<int, double> netDistributionTotals = new Dictionary<int, double>();
                Dictionary<int, double> australianFrankingCredit = new Dictionary<int, double>();
                Dictionary<int, double> foriegnIncomeOffset = new Dictionary<int, double>();
                bool processedOffsets = false;
                SetComponentTable(ref processedOffsets, false, doc, ComponentCategory.Australian_Income, filteredList, grossDistributionTotals, withholdingTaxTotals, australianFrankingCredit, foriegnIncomeOffset);
                SetComponentTable(ref processedOffsets, false, doc, ComponentCategory.Foreign_Income, filteredList, grossDistributionTotals, withholdingTaxTotals, australianFrankingCredit, foriegnIncomeOffset);
                SetComponentTable(ref processedOffsets, false, doc, ComponentCategory.Capital_Gains, filteredList, grossDistributionTotals, withholdingTaxTotals, australianFrankingCredit, foriegnIncomeOffset);
                RenderText lineBreakPageIncome = new RenderText();
                lineBreakPageIncome.BreakAfter = BreakEnum.Page;
                lineBreakPageIncome.Text = "\n";
                doc.Body.Children.Add(lineBreakPageIncome);
                SetComponentTable(ref processedOffsets, true, doc, ComponentCategory.Other_Non_Assessable_Amount, filteredList, grossDistributionTotals, withholdingTaxTotals, australianFrankingCredit, foriegnIncomeOffset);

                RenderTable componentTableTotal = new RenderTable();
                componentTableTotal.Style.FontSize = 8;
                componentTableTotal.RowGroups[0, 1].PageHeader = true;
                componentTableTotal.Rows[0].Height = .2;
                componentTableTotal.Rows[0].Style.TextAlignHorz = AlignHorzEnum.Left;
                componentTableTotal.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                componentTableTotal.Cells[0, 0].Text = "Gross Cash Distribution";
                componentTableTotal.Rows[0].Style.FontSize = 8;
                componentTableTotal.Cols[0].Width = 8;

                componentTableTotal.Cells[1, 0].Text = "Less: Withholding tax";
                componentTableTotal.Rows[1].Style.FontSize = 8;
                componentTableTotal.Cols[1].Width = 8;

                componentTableTotal.Cells[2, 0].Text = "Net Cash Distribution";
                componentTableTotal.Rows[2].Style.FontSize = 8;
                componentTableTotal.Rows[2].Style.FontBold = true;
                componentTableTotal.Cols[2].Width = 8;

                for (int i = 1; i <= 5; i++)
                    componentTableTotal.Cols[i].Width = 3;
                componentTableTotal.Cols[6].Width = .2;

                SetTotalGridStyle(componentTableTotal, 2, 0);

                foreach (var dictItem in grossDistributionTotals)
                {
                    componentTableTotal.Cells[0, dictItem.Key].Text = dictItem.Value.ToString("C");
                    componentTableTotal.Cells[0, dictItem.Key].Style.TextAlignHorz = AlignHorzEnum.Right;
                    componentTableTotal.Cols[dictItem.Key].Width = 3;

                    if (netDistributionTotals.ContainsKey(dictItem.Key))
                        netDistributionTotals[dictItem.Key] += dictItem.Value;
                    else
                        netDistributionTotals.Add(dictItem.Key, dictItem.Value);
                }


                foreach (var dictItem in withholdingTaxTotals)
                {
                    componentTableTotal.Cells[1, dictItem.Key].Text = dictItem.Value.ToString("C");
                    componentTableTotal.Cols[dictItem.Key].Width = 3;
                    componentTableTotal.Cells[1, dictItem.Key].Style.TextAlignHorz = AlignHorzEnum.Right;

                    if (netDistributionTotals.ContainsKey(dictItem.Key))
                        netDistributionTotals[dictItem.Key] += dictItem.Value;
                    else
                        netDistributionTotals.Add(dictItem.Key, dictItem.Value);
                }

                foreach (var dictItem in netDistributionTotals)
                {
                    componentTableTotal.Cells[2, dictItem.Key].Text = dictItem.Value.ToString("C");
                    componentTableTotal.Cols[dictItem.Key].Width = 3;
                    componentTableTotal.Cells[2, dictItem.Key].Style.TextAlignHorz = AlignHorzEnum.Right;
                }


                RenderTable componentTableTaxOffSetAndIncome = new RenderTable();
                componentTableTaxOffSetAndIncome.Style.FontSize = 8;
                componentTableTaxOffSetAndIncome.RowGroups[0, 1].PageHeader = true;
                componentTableTaxOffSetAndIncome.Rows[0].Height = .2;
                componentTableTaxOffSetAndIncome.Rows[0].Style.TextAlignHorz = AlignHorzEnum.Left;
                componentTableTaxOffSetAndIncome.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                componentTableTaxOffSetAndIncome.Cells[0, 0].Text = "Tax Offsets";
                componentTableTaxOffSetAndIncome.Rows[0].Style.FontSize = 8;
                componentTableTaxOffSetAndIncome.Rows[0].Style.FontBold = true;
                componentTableTaxOffSetAndIncome.Cols[0].Width = 8;

                componentTableTaxOffSetAndIncome.Cells[1, 0].Text = "Australian franking credit";
                componentTableTaxOffSetAndIncome.Rows[1].Style.FontSize = 8;
                componentTableTaxOffSetAndIncome.Cols[1].Width = 8;

                componentTableTaxOffSetAndIncome.Cells[2, 0].Text = "Foreign income tax offset";
                componentTableTaxOffSetAndIncome.Rows[2].Style.FontSize = 8;
                componentTableTaxOffSetAndIncome.Cols[2].Width = 8;

                for (int i = 1; i <= 5; i++)
                    componentTableTaxOffSetAndIncome.Cols[i].Width = 3;
                componentTableTaxOffSetAndIncome.Cols[6].Width = .2;

                SetTotalGridStyle(componentTableTaxOffSetAndIncome, 2, 1);

                foreach (var dictItem in australianFrankingCredit)
                {
                    componentTableTaxOffSetAndIncome.Cells[1, dictItem.Key].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                    componentTableTaxOffSetAndIncome.Cells[1, 6].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                    if (dictItem.Key == 1)
                        componentTableTaxOffSetAndIncome.Cells[1, dictItem.Key].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
                    componentTableTaxOffSetAndIncome.Cells[1, 6].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);

                    componentTableTaxOffSetAndIncome.Cells[1, dictItem.Key].Text = dictItem.Value.ToString("C");
                    componentTableTaxOffSetAndIncome.Cols[dictItem.Key].Width = 3;
                    componentTableTaxOffSetAndIncome.Cells[1, dictItem.Key].Style.TextAlignHorz = AlignHorzEnum.Right;
                }

                foreach (var dictItem in foriegnIncomeOffset)
                {
                    componentTableTaxOffSetAndIncome.Cells[2, dictItem.Key].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                    componentTableTaxOffSetAndIncome.Cells[2, 6].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                    if (dictItem.Key == 1)
                        componentTableTaxOffSetAndIncome.Cells[2, dictItem.Key].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
                    componentTableTaxOffSetAndIncome.Cells[2, 6].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);

                    componentTableTaxOffSetAndIncome.Cells[2, dictItem.Key].Text = dictItem.Value.ToString("C");
                    componentTableTaxOffSetAndIncome.Cols[dictItem.Key].Width = 3;
                    componentTableTaxOffSetAndIncome.Cells[2, dictItem.Key].Style.TextAlignHorz = AlignHorzEnum.Right;
                }

                RenderText lineBreakTotal = new RenderText();
                lineBreakTotal.Text = "\n";
                RenderText lineBreakTotal2 = new RenderText();
                lineBreakTotal2.Text = "\n";
                doc.Body.Children.Add(componentTableTotal);
                doc.Body.Children.Add(lineBreakTotal2);
                doc.Body.Children.Add(componentTableTaxOffSetAndIncome);
                RenderText lineBreakPage = new RenderText();
                lineBreakPage.BreakAfter = BreakEnum.Page;
                lineBreakPage.Text = "\n";
                doc.Body.Children.Add(lineBreakPage);
            }
        }

        private static void SetTotalGridStyle(RenderTable componentTableTotal, int rowCount, int startRowPos)
        {
            for (int col = 1; col <= 5; col++)
            {
                for (int row = startRowPos; row <= rowCount; row++)
                {
                    if (row == startRowPos)
                    {
                        componentTableTotal.Cells[row, col].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                        componentTableTotal.Cells[row, 6].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                    }
                    if (col == 1)
                        componentTableTotal.Cells[row, col].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
                    if (row == rowCount)
                    {
                        componentTableTotal.Cells[row, col].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                        componentTableTotal.Cells[row, 6].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                    }
                    componentTableTotal.Cells[row, 6].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
                }
            }
        }

        private static void SetComponentTableTotal(string category, C1PrintDocument doc, DistributionIncomeEntity slectedDistributionStatement, IEnumerable<DistributionComponent> distributionComponentList)
        {
            RenderTable componentTable = new RenderTable();
            componentTable.Style.FontSize = 8;
            componentTable.RowGroups[0, 1].PageHeader = true;
            componentTable.Rows[0].Height = .2;
            componentTable.Rows[0].Style.FontBold = true;
            componentTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            componentTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            componentTable.Rows[0].Style.FontBold = true;
            componentTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            componentTable.Cells[0, 0].Text = "Category";
            componentTable.Cells[0, 1].Text = "Distribution Paid";
            componentTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
            componentTable.Cells[0, 2].Text = "Tax Paid / Offset";
            componentTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
            componentTable.Cells[0, 3].Text = "Withholding / Non-Resident Tax Paid";
            componentTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            componentTable.Cells[0, 4].Text = "Gross Distribution Amount";
            componentTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            componentTable.Rows[0].Style.FontSize = 7;
            componentTable.Cols[0].Width = 7.5;
            componentTable.Cols[1].Width = 5;
            componentTable.Cols[2].Width = 5;
            componentTable.Cols[3].Width = 5;
            componentTable.Cols[4].Width = 4;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = category + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n";
            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(componentTable);
            doc.Body.Children.Add(lineBreakTotal);

            int totalIndex = 1;

            componentTable.Rows[totalIndex].Height = .2;
            componentTable.Rows[totalIndex].Style.FontBold = true;
            componentTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            componentTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            componentTable.Rows[totalIndex].Style.FontBold = true;
            componentTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;
            componentTable.Cells[totalIndex, 0].Text = "TOTAL";

            componentTable.Cells[totalIndex, 1].Text = distributionComponentList.Sum(dis => dis.Unit_Amount).Value.ToString("C");
            componentTable.Cells[totalIndex, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            componentTable.Cells[totalIndex, 2].Text = distributionComponentList.Sum(dis => dis.Autax_Credit).Value.ToString("C");
            componentTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

            componentTable.Cells[totalIndex, 3].Text = distributionComponentList.Sum(dis => dis.Tax_WithHeld).Value.ToString("C");
            componentTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

            componentTable.Cells[totalIndex, 4].Text = distributionComponentList.Sum(dis => dis.GrossDistribution).ToString("C");
            componentTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
        }

        private void SetHoldingStatement(C1PrintDocument doc, HoldingSummaryReportDS ds, RenderToc toc)
        {
            RenderText celltext = new RenderText(doc);
            celltext.Text = "HOLDING STATEMENTS" + "\n\n";
            celltext.Style.TextColor = Color.FromArgb(46, 44, 83);
            celltext.Style.TextAlignHorz = AlignHorzEnum.Left;
            celltext.Style.FontSize = 16;
            celltext.Style.FontBold = true;
            doc.Body.Children.Add(celltext);
            toc.AddItem("Holding Statements - Managed Funds", celltext);

            IEnumerable<IGrouping<object, DataRow>> groupedRows = ds.misTransactionDetailsTable.Select().OrderBy(row => row[ds.misTransactionDetailsTable.SECCODE]).GroupBy(row => row[ds.misTransactionDetailsTable.SECCODE]);

            foreach (var rows in groupedRows)
            {
                decimal holdingValue = rows.Sum(r => decimal.Parse(r[ds.misTransactionDetailsTable.UNITSAMTBALANCE].ToString()));
                if (rows.Count() > 0 && holdingValue != 0)
                {
                    RenderTable secSumTable = new RenderTable();
                    secSumTable.RowGroups[0, 1].PageHeader = true;
                    RenderTable secSummaryTotalTable = new RenderTable();
                    secSummaryTotalTable.Style.FontSize = 8;
                    secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
                    secSummaryTotalTable.Rows[0].Height = .2;
                    secSummaryTotalTable.Rows[0].Style.FontBold = true;
                    secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                    secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                    secSummaryTotalTable.Rows[0].Style.FontBold = true;
                    secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                    secSummaryTotalTable.Cells[0, 0].Text = "Date";
                    secSummaryTotalTable.Cells[0, 1].Text = "Transaction Type";
                    secSummaryTotalTable.Cells[0, 2].Text = "Units";
                    secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 3].Text = "Unit Price";
                    secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 4].Text = "Transaction Value";
                    secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 5].Text = "Unit Balance";
                    secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 6].Text = "Holding Balance ($)";
                    secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                    secSummaryTotalTable.Cols[0].Width = 4;
                    secSummaryTotalTable.Cols[1].Width = 4;
                    secSummaryTotalTable.Cols[2].Width = 4;
                    secSummaryTotalTable.Cols[3].Width = 4;
                    secSummaryTotalTable.Cols[4].Width = 4;
                    secSummaryTotalTable.Cols[5].Width = 4;
                    secSummaryTotalTable.Cols[6].Width = 4;


                    RenderText gridHeaderTotal = new RenderText();
                    gridHeaderTotal.Text = rows.Key.ToString().ToUpper() + "   -   " + rows.FirstOrDefault()[ds.misTransactionDetailsTable.SECCODEDESC].ToString().ToUpper() + "\n\n";
                    gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
                    gridHeaderTotal.Style.FontSize = 11;
                    gridHeaderTotal.Style.FontBold = true;

                    RenderText lineBreakTotal = new RenderText();
                    lineBreakTotal.Text = "\n\n\n";

                    int totalIndex = 1;

                    foreach (DataRow secRow in rows)
                    {
                        secSummaryTotalTable.Cells[totalIndex, 0].Text = ((DateTime)secRow[ds.misTransactionDetailsTable.TRANSACTIONDATE]).ToString("dd/MM/yyyy");
                        secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[ds.misTransactionDetailsTable.TRANSTYPE].ToString();

                        if (secRow[ds.misTransactionDetailsTable.UNITS] != null & secRow[ds.misTransactionDetailsTable.UNITS] != DBNull.Value)
                            secSummaryTotalTable.Cells[totalIndex, 2].Text = Convert.ToDecimal(secRow[ds.misTransactionDetailsTable.UNITS]).ToString("N2");
                        else
                            secSummaryTotalTable.Cells[totalIndex, 2].Text = "-";
                        secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                        if (secRow[ds.misTransactionDetailsTable.UNITSPRICE] != null & secRow[ds.misTransactionDetailsTable.UNITSPRICE] != DBNull.Value)
                            secSummaryTotalTable.Cells[totalIndex, 3].Text = Convert.ToDecimal(secRow[ds.misTransactionDetailsTable.UNITSPRICE]).ToString("N4");
                        else
                            secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                        secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                        if (secRow[ds.misTransactionDetailsTable.UNITSAMT] != null & secRow[ds.misTransactionDetailsTable.UNITSAMT] != DBNull.Value)
                            secSummaryTotalTable.Cells[totalIndex, 4].Text = Convert.ToDecimal(secRow[ds.misTransactionDetailsTable.UNITSAMT]).ToString("C");
                        else
                            secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";
                        secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                        if (secRow[ds.misTransactionDetailsTable.UNITSBALANCE] != null & secRow[ds.misTransactionDetailsTable.UNITSBALANCE] != DBNull.Value)
                            secSummaryTotalTable.Cells[totalIndex, 5].Text = Convert.ToDecimal(secRow[ds.misTransactionDetailsTable.UNITSBALANCE]).ToString("N2");
                        else
                            secSummaryTotalTable.Cells[totalIndex, 5].Text = "-";
                        secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                        if (secRow[ds.misTransactionDetailsTable.UNITSAMTBALANCE] != null & secRow[ds.misTransactionDetailsTable.UNITSAMTBALANCE] != DBNull.Value)
                            secSummaryTotalTable.Cells[totalIndex, 6].Text = Convert.ToDecimal(secRow[ds.misTransactionDetailsTable.UNITSAMTBALANCE]).ToString("C");
                        else
                            secSummaryTotalTable.Cells[totalIndex, 6].Text = "-";
                        secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                        totalIndex++;
                    }

                    secSummaryTotalTable.Rows[totalIndex].Height = .2;
                    secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
                    secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                    secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                    secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
                    secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;
                    doc.PageLayout.PageSettings.Landscape = true;
                    doc.PageLayout.PageSettings.TopMargin = .20;
                    doc.PageLayout.PageSettings.LeftMargin = .20;
                    doc.PageLayout.PageSettings.RightMargin = .20;

                    RenderText lineBreakPage = new RenderText();
                    doc.Body.Children.Add(lineBreakPage);

                    DataView incomeView = new DataView(PresentationData.Tables[LossAndGainsReportDS.INCOMEBREAKDOWN]);
                    incomeView.Sort = LossAndGainsReportDS.SECTYPE + " ASC";
                    DataTable sortedIncomeView = incomeView.ToTable();

                    RenderText gridHeaderTotal2 = new RenderText();
                    gridHeaderTotal2.Text = rows.Key.ToString().ToUpper() + "   -   " + rows.FirstOrDefault()[ds.misTransactionDetailsTable.SECCODEDESC].ToString().ToUpper() + " SUMMARY\n\n";
                    gridHeaderTotal2.Style.TextColor = Color.FromArgb(46, 44, 83);
                    gridHeaderTotal2.Style.FontSize = 11;
                    gridHeaderTotal2.Style.FontBold = true;


                    RenderTable holdingStatementSummaryTable = new RenderTable();
                    holdingStatementSummaryTable.Style.FontSize = 8;
                    holdingStatementSummaryTable.RowGroups[0, 1].PageHeader = true;
                    holdingStatementSummaryTable.Rows[0].Height = .2;
                    holdingStatementSummaryTable.Rows[0].Style.FontBold = true;
                    holdingStatementSummaryTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                    holdingStatementSummaryTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                    holdingStatementSummaryTable.Rows[0].Style.FontBold = true;
                    holdingStatementSummaryTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;


                    holdingStatementSummaryTable.Cells[0, 0].Text = "Opening Balance ($)";
                    holdingStatementSummaryTable.Cells[0, 0].Style.TextAlignHorz = AlignHorzEnum.Right;
                    holdingStatementSummaryTable.Cells[0, 1].Text = "Opening Units";
                    holdingStatementSummaryTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                    holdingStatementSummaryTable.Cells[0, 2].Text = "Adjustments Up";
                    holdingStatementSummaryTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                    holdingStatementSummaryTable.Cells[0, 3].Text = "Adjustments Down";
                    holdingStatementSummaryTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                    holdingStatementSummaryTable.Cells[0, 4].Text = "Purchase";
                    holdingStatementSummaryTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                    holdingStatementSummaryTable.Cells[0, 5].Text = "Redemption";
                    holdingStatementSummaryTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                    holdingStatementSummaryTable.Cells[0, 6].Text = "Closing Units";
                    holdingStatementSummaryTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                    holdingStatementSummaryTable.Cells[0, 7].Text = "Closing Balance ($)";
                    holdingStatementSummaryTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    holdingStatementSummaryTable.Cols[0].Width = 4;
                    holdingStatementSummaryTable.Cols[1].Width = 4;
                    holdingStatementSummaryTable.Cols[2].Width = 4;
                    holdingStatementSummaryTable.Cols[3].Width = 4;
                    holdingStatementSummaryTable.Cols[4].Width = 4;
                    holdingStatementSummaryTable.Cols[5].Width = 4;
                    holdingStatementSummaryTable.Cols[6].Width = 4;
                    holdingStatementSummaryTable.Cols[7].Width = 4;

                    decimal openingBalance = 0;
                    Decimal.TryParse(rows.Where(row => row[ds.misTransactionDetailsTable.TRANSTYPE].ToString() == "Opening Balance").FirstOrDefault()[ds.misTransactionDetailsTable.UNITSAMT].ToString(), out openingBalance);

                    decimal openingBalanceUnits = 0;
                    Decimal.TryParse(rows.Where(row => row[ds.misTransactionDetailsTable.TRANSTYPE].ToString() == "Opening Balance").FirstOrDefault()[ds.misTransactionDetailsTable.UNITS].ToString(), out openingBalanceUnits);

                    decimal closingBalance = 0;
                    Decimal.TryParse(rows.Where(row => row[ds.misTransactionDetailsTable.TRANSTYPE].ToString() == "Closing Balance").FirstOrDefault()[ds.misTransactionDetailsTable.UNITSAMT].ToString(), out closingBalance);

                    decimal closingBalanceUnits = 0;
                    Decimal.TryParse(rows.Where(row => row[ds.misTransactionDetailsTable.TRANSTYPE].ToString() == "Closing Balance").FirstOrDefault()[ds.misTransactionDetailsTable.UNITS].ToString(), out closingBalanceUnits);

                    decimal purchaseBalance = Convert.ToDecimal(rows.Where(row => row[ds.misTransactionDetailsTable.TRANSTYPE].ToString() == "Purchase" || row[ds.misTransactionDetailsTable.TRANSTYPE].ToString() == "Application").Sum(row => Convert.ToDecimal(row[ds.misTransactionDetailsTable.UNITSAMT])));
                    decimal purchaseBalanceUnits = Convert.ToDecimal(rows.Where(row => row[ds.misTransactionDetailsTable.TRANSTYPE].ToString() == "Purchase" || row[ds.misTransactionDetailsTable.TRANSTYPE].ToString() == "Application").Sum(row => Convert.ToDecimal(row[ds.misTransactionDetailsTable.UNITS])));

                    decimal redemptionBalance = Convert.ToDecimal(rows.Where(row => row[ds.misTransactionDetailsTable.TRANSTYPE].ToString() == "Redemption").Sum(row => Convert.ToDecimal(row[ds.misTransactionDetailsTable.UNITSAMT])));
                    decimal redemptionBalanceUnits = Convert.ToDecimal(rows.Where(row => row[ds.misTransactionDetailsTable.TRANSTYPE].ToString() == "Redemption").Sum(row => Convert.ToDecimal(row[ds.misTransactionDetailsTable.UNITS])));

                    decimal adjustmentUpBalance = Convert.ToDecimal(rows.Where(row => row[ds.misTransactionDetailsTable.TRANSTYPE].ToString() == "Adjustment Up").Sum(row => Convert.ToDecimal(row[ds.misTransactionDetailsTable.UNITSAMT])));
                    decimal adjustmentUpBalanceUnits = Convert.ToDecimal(rows.Where(row => row[ds.misTransactionDetailsTable.TRANSTYPE].ToString() == "Adjustment Up").Sum(row => Convert.ToDecimal(row[ds.misTransactionDetailsTable.UNITS])));

                    decimal adjustmentDownBalance = Convert.ToDecimal(rows.Where(row => row[ds.misTransactionDetailsTable.TRANSTYPE].ToString() == "Adjustment Down").Sum(row => Convert.ToDecimal(row[ds.misTransactionDetailsTable.UNITSAMT])));
                    decimal adjustmentDownBalanceUnits = Convert.ToDecimal(rows.Where(row => row[ds.misTransactionDetailsTable.TRANSTYPE].ToString() == "Adjustment Down").Sum(row => Convert.ToDecimal(row[ds.misTransactionDetailsTable.UNITS])));

                    holdingStatementSummaryTable.Cells[1, 0].Text = openingBalance.ToString("C");
                    holdingStatementSummaryTable.Cells[1, 0].Style.TextAlignHorz = AlignHorzEnum.Right;
                    holdingStatementSummaryTable.Cells[1, 1].Text = openingBalanceUnits.ToString("N4");
                    holdingStatementSummaryTable.Cells[1, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

                    holdingStatementSummaryTable.Cells[1, 2].Text = adjustmentUpBalanceUnits.ToString("N4");
                    holdingStatementSummaryTable.Cells[1, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                    holdingStatementSummaryTable.Cells[1, 3].Text = adjustmentDownBalanceUnits.ToString("N4");
                    holdingStatementSummaryTable.Cells[1, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    holdingStatementSummaryTable.Cells[1, 4].Text = purchaseBalance.ToString("N4");
                    holdingStatementSummaryTable.Cells[1, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                    holdingStatementSummaryTable.Cells[1, 5].Text = redemptionBalanceUnits.ToString("N4");
                    holdingStatementSummaryTable.Cells[1, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                    holdingStatementSummaryTable.Cells[1, 6].Text = closingBalanceUnits.ToString("N4");
                    holdingStatementSummaryTable.Cells[1, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                    holdingStatementSummaryTable.Cells[1, 7].Text = closingBalance.ToString("C");
                    holdingStatementSummaryTable.Cells[1, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    holdingStatementSummaryTable.Rows[3].Height = .2;
                    holdingStatementSummaryTable.Rows[3].Style.FontBold = true;
                    holdingStatementSummaryTable.Rows[3].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                    holdingStatementSummaryTable.Rows[3].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                    holdingStatementSummaryTable.Rows[3].Style.FontBold = true;
                    holdingStatementSummaryTable.Rows[3].Style.TextAlignVert = AlignVertEnum.Center;

                    if (rows.Key.ToString().ToUpper() == "IV004")
                    {
                        doc.Body.Children.Add(gridHeaderTotal2);
                        doc.Body.Children.Add(holdingStatementSummaryTable);

                        RenderText lineBreakPage3 = new RenderText();

                        lineBreakPage3.Text = "\n";
                        doc.Body.Children.Add(lineBreakPage3);
                    }
                    doc.Body.Children.Add(gridHeaderTotal);
                    doc.Body.Children.Add(secSummaryTotalTable);
                    doc.Body.Children.Add(lineBreakTotal);
                    IEnumerable<DataRow> selecteddRows = sortedIncomeView.Select().Where(row => row[LossAndGainsReportDS.SECNAME].ToString() == rows.Key.ToString());
                    if (selecteddRows.Count() > 0)
                    {
                        RenderText gridHeaderTotalIncome = new RenderText();
                        gridHeaderTotalIncome.Text = "Distribution Details\n\n";
                        gridHeaderTotalIncome.Style.TextColor = Color.FromArgb(46, 44, 83);
                        gridHeaderTotalIncome.Style.FontSize = 11;
                        gridHeaderTotalIncome.Style.FontBold = true;
                        doc.Body.Children.Add(gridHeaderTotalIncome);

                        RenderTable incomeBreakdownOverall = SetIncomeReconSummary(selecteddRows);
                        doc.Body.Children.Add(incomeBreakdownOverall);
                    }

                    RenderText lineBreakPage2 = new RenderText();
                    lineBreakPage2.BreakAfter = BreakEnum.Page;
                    lineBreakPage2.Text = "\n";
                    doc.Body.Children.Add(lineBreakPage2);
                }
            }
        }

        private void SetHoldingStatementSEC(C1PrintDocument doc, HoldingSummaryReportDS ds, RenderToc toc)
        {
            RenderText celltext = new RenderText(doc);
            celltext.Text = "HOLDING STATEMENTS ASX SECURITIES" + "\n\n";
            celltext.Style.TextColor = Color.FromArgb(46, 44, 83);
            celltext.Style.TextAlignHorz = AlignHorzEnum.Left;
            celltext.Style.FontSize = 16;
            celltext.Style.FontBold = true;
            doc.Body.Children.Add(celltext);
            toc.AddItem("Holding Statements - Securities", celltext);

            IEnumerable<IGrouping<object, DataRow>> groupedRows = ds.transactiondetailstable.DefaultView.ToTable(true).Select().OrderBy(row => row[ds.transactiondetailstable.TRANSACTIONDATE]).GroupBy(row => row[ds.transactiondetailstable.SECCODE]);

            foreach (var rows in groupedRows)
            {
                if (rows.Count() > 0)
                {
                    RenderTable secSumTable = new RenderTable();
                    secSumTable.RowGroups[0, 1].PageHeader = true;
                    RenderTable secSummaryTotalTable = new RenderTable();
                    secSummaryTotalTable.Style.FontSize = 8;
                    secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
                    secSummaryTotalTable.Rows[0].Height = .2;
                    secSummaryTotalTable.Rows[0].Style.FontBold = true;
                    secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                    secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                    secSummaryTotalTable.Rows[0].Style.FontBold = true;
                    secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                    secSummaryTotalTable.Cells[0, 0].Text = "Date";
                    secSummaryTotalTable.Cells[0, 1].Text = "Transaction Type";

                    secSummaryTotalTable.Cells[0, 2].Text = "Transaction ID";
                    secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Left;
                    secSummaryTotalTable.Cells[0, 3].Text = "Units";
                    secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 4].Text = "Holding Balance";
                    secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    secSummaryTotalTable.Cols[0].Width = 4;
                    secSummaryTotalTable.Cols[1].Width = 4;
                    secSummaryTotalTable.Cols[2].Width = 4;
                    secSummaryTotalTable.Cols[3].Width = 4;
                    secSummaryTotalTable.Cols[4].Width = 4;

                    RenderText gridHeaderTotal = new RenderText();
                    gridHeaderTotal.Text = rows.Key.ToString().ToUpper() + "\n\n";
                    gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
                    gridHeaderTotal.Style.FontSize = 11;
                    gridHeaderTotal.Style.FontBold = true;

                    RenderText lineBreakTotal = new RenderText();
                    lineBreakTotal.Text = "\n\n\n";

                    int totalIndex = 1;

                    foreach (DataRow secRow in rows)
                    {
                        secSummaryTotalTable.Cells[totalIndex, 0].Text = ((DateTime)secRow[ds.transactiondetailstable.TRANSACTIONDATE]).ToString("dd/MM/yyyy");
                        secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[ds.transactiondetailstable.TRANSTYPE].ToString();

                        if (secRow[ds.transactiondetailstable.TRANSDISID] != null & secRow[ds.transactiondetailstable.TRANSDISID] != DBNull.Value)
                            secSummaryTotalTable.Cells[totalIndex, 2].Text = secRow[ds.transactiondetailstable.TRANSDISID].ToString();
                        else
                            secSummaryTotalTable.Cells[totalIndex, 2].Text = "-";
                        secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                        if (secRow[ds.transactiondetailstable.UNITS] != null & secRow[ds.transactiondetailstable.UNITS] != DBNull.Value)
                            secSummaryTotalTable.Cells[totalIndex, 3].Text = Convert.ToDecimal(secRow[ds.transactiondetailstable.UNITS]).ToString("N4");
                        else
                            secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                        secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                        if (secRow[ds.transactiondetailstable.UNITSBALANCE] != null & secRow[ds.transactiondetailstable.UNITSBALANCE] != DBNull.Value)
                            secSummaryTotalTable.Cells[totalIndex, 4].Text = Convert.ToDecimal(secRow[ds.transactiondetailstable.UNITSBALANCE]).ToString("N4");
                        else
                            secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";
                        secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;


                        totalIndex++;
                    }

                    secSummaryTotalTable.Rows[totalIndex].Height = .2;
                    secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
                    secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                    secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                    secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
                    secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;
                    doc.PageLayout.PageSettings.Landscape = true;
                    doc.PageLayout.PageSettings.TopMargin = .20;
                    doc.PageLayout.PageSettings.LeftMargin = .20;
                    doc.PageLayout.PageSettings.RightMargin = .20;
                    doc.Body.Children.Add(gridHeaderTotal);
                    doc.Body.Children.Add(secSummaryTotalTable);
                    RenderText lineBreakPage2 = new RenderText();
                    lineBreakPage2.BreakAfter = BreakEnum.Page;
                    lineBreakPage2.Text = "\n";
                    doc.Body.Children.Add(lineBreakPage2);
                }
            }
        }

        private void SetComponentTable(ref bool processedOffSets, bool useHeader, C1PrintDocument doc, ComponentCategory componentCategory, List<DistributionIncomeEntity> filteredList, Dictionary<int, double> totalTotals, Dictionary<int, double> withholdingTaxTotals, Dictionary<int, double> australianFrankingCredit, Dictionary<int, double> foriegnIncomeOffset)
        {
            RenderTable componentTable = new RenderTable();
            componentTable.Style.FontSize = 8;
            componentTable.RowGroups[0, 1].PageHeader = true;
            componentTable.Rows[0].Height = .2;
            if (useHeader)
                componentTable.Rows[0].Height = .3;
            componentTable.Rows[0].Style.FontBold = true;
            componentTable.Rows[0].Style.FontBold = true;
            componentTable.Rows[0].Style.TextAlignHorz = AlignHorzEnum.Left;
            componentTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;
            componentTable.Cells[0, 0].Text = Enumeration.GetDescription(componentCategory);
            componentTable.Rows[0].Style.FontSize = 8;
            componentTable.Cols[0].Width = 8;

            var disItemType = filteredList.FirstOrDefault();
            List<DistributionComponent> componentsFromListDesc = new List<DistributionComponent>();

            foreach (var filteredListItem in filteredList)
            {
                var componentsFromListDescList = filteredListItem.Components.Where(comp => comp.Category == componentCategory).OrderBy(comp => comp.Decription);

                foreach (var item in componentsFromListDescList)
                {
                    var existingItem = componentsFromListDesc.Where(t => t.Decription == item.Decription).FirstOrDefault(); 
                    if(existingItem == null)
                        componentsFromListDesc.Add(item); 
                }
            }
            
            for (int i = 1; i <= 5; i++)
                componentTable.Cols[i].Width = 3;

            componentTable.Cols[6].Width = .2;

            int rowIndex = 1;
            int columnIndex = 1;

            if (useHeader)
            {
                foreach (var item in filteredList)
                {
                    if (this.comboBoxAccType.Text.ToLower() == "cash")
                        componentTable.Cells[0, columnIndex].Text = "Distribution (" + item.PaymentDate.Value.ToString("dd/MM/yyyy") + ")";
                    else
                        componentTable.Cells[0, columnIndex].Text = "Distribution (" + item.RecordDate.Value.ToString("dd/MM/yyyy") + ")";

                    componentTable.Cells[0, columnIndex].Style.TextAlignHorz = AlignHorzEnum.Right;
                    componentTable.Cols[columnIndex].Width = 3;
                    columnIndex++;
                }
                componentTable.Cells[0, 5].Text = "Total";
                componentTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                columnIndex = 1;
            }

            foreach (DistributionComponent disComp in componentsFromListDesc)
            {
                componentTable.Cells[rowIndex, 0].Text = disComp.Decription;
                rowIndex++;
            }

            columnIndex = 1;
            Dictionary<int, double> totals = new Dictionary<int, double>();
            int columnCount = filteredList.Count();
            int check = filteredList.SelectMany(q => q.Components).Count();

            double withholdingTotal = 0;
            double grossDistributionTotal = 0;
            double taxOffSetTotal = 0;
            double foreignIncomeTotal = 0;

            foreach (var disItem in filteredList)
            {
                var componentsFromList = disItem.Components.Where(comp => comp.Category == componentCategory).OrderBy(comp => comp.Decription);
                componentTable.Cols[columnIndex].Width = 3;
                rowIndex = 1;
                int rowount = componentsFromList.Count();

                foreach (DistributionComponent disComp in componentsFromList)
                {
                    bool found = false;
                    int tRowIndex = 0; 

                    for (int tRow = 0; tRow <= componentTable.Rows.Count; tRow++)
                    {
                        C1.C1Preview.TableCell tCell = componentTable.Cells.FindCell(tRow, 0);
                        
                        if (tCell != null)
                        {
                            if (tCell.Text.ToLower() == disComp.Decription.ToLower())
                            {
                                tRowIndex = tRow;
                                found = true;
                            }

                            if (found)
                            {
                                if (disComp.Unit_Amount.HasValue)
                                    componentTable.Cells[tRowIndex, columnIndex].Text = disComp.Unit_Amount.Value.ToString("C");
                                else
                                    componentTable.Cells[tRowIndex, columnIndex].Text = "-";
                                componentTable.Cells[tRowIndex, columnIndex].Style.TextAlignHorz = AlignHorzEnum.Right;
                                rowIndex++;
                                found = false;
                            }
                        }
                    }
                }

                double total = componentsFromList.Sum(dis => dis.Unit_Amount).Value;
                double taxWithHeld = componentsFromList.Sum(dis => dis.Tax_WithHeld).Value;

                totals.Add(columnIndex, total);

                if (totalTotals.ContainsKey(columnIndex))
                    totalTotals[columnIndex] += total;
                else
                    totalTotals.Add(columnIndex, total);

                grossDistributionTotal += total;

                if (withholdingTaxTotals.ContainsKey(columnIndex))
                    withholdingTaxTotals[columnIndex] += taxWithHeld;
                else
                    withholdingTaxTotals.Add(columnIndex, taxWithHeld);

                withholdingTotal += taxWithHeld;


                var ausIncomeComponents = disItem.Components.Where(comp => comp.Category == ComponentCategory.Australian_Income);
                var foreignIncomeComponents = disItem.Components.Where(comp => comp.Category == ComponentCategory.Foreign_Income);
                var capitalGains = disItem.Components.Where(comp => comp.Category == ComponentCategory.Capital_Gains);
                var nonassComponents = disItem.Components.Where(comp => comp.Category == ComponentCategory.Other_Non_Assessable_Amount);

                double frankingCredit = ausIncomeComponents.Sum(comp => comp.Autax_Credit.HasValue ? comp.Autax_Credit.Value : 0);
                double foreignSourceIncomeTaxOffset = foreignIncomeComponents.Sum(forgn => forgn.Autax_Credit.HasValue ? forgn.Autax_Credit.Value : 0);
                double ausFrankingCredit = foreignIncomeComponents.Where(forgn => forgn.ComponentType == 899).Sum(comp => comp.GrossDistribution);

                if (australianFrankingCredit.ContainsKey(columnIndex))
                    australianFrankingCredit[columnIndex] = (frankingCredit + ausFrankingCredit);
                else
                    australianFrankingCredit.Add(columnIndex, (frankingCredit + ausFrankingCredit));

                taxOffSetTotal += (frankingCredit + ausFrankingCredit);

                if (foriegnIncomeOffset.ContainsKey(columnIndex))
                    foriegnIncomeOffset[columnIndex] = foreignSourceIncomeTaxOffset;
                else
                    foriegnIncomeOffset.Add(columnIndex, foreignSourceIncomeTaxOffset);

                foreignIncomeTotal += foreignSourceIncomeTaxOffset;

                for (int col = 1; col <= 5; col++)
                {
                    for (int row = 1; row <= rowIndex - 1; row++)
                    {
                        if (row == 1)
                        {
                            componentTable.Cells[row, col].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                            componentTable.Cells[row, 6].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                        }
                        if (col == 1)
                            componentTable.Cells[row, col].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
                        if (row == rowIndex - 1)
                        {
                            componentTable.Cells[row, col].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                            componentTable.Cells[row, 6].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                        }
                        componentTable.Cells[row, 6].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
                    }
                }
                columnIndex++;
            }

            if (totalTotals.ContainsKey(5))
                totalTotals[5] += grossDistributionTotal;
            else
                totalTotals.Add(5, grossDistributionTotal);

            if (withholdingTaxTotals.ContainsKey(5))
                withholdingTaxTotals[5] += withholdingTotal;
            else
                withholdingTaxTotals.Add(5, withholdingTotal);

            if (!processedOffSets)
            {
                if (australianFrankingCredit.ContainsKey(5))
                    australianFrankingCredit[5] += taxOffSetTotal;
                else
                    australianFrankingCredit.Add(5, taxOffSetTotal);

                if (foriegnIncomeOffset.ContainsKey(5))
                    foriegnIncomeOffset[5] += foreignIncomeTotal;
                else
                    foriegnIncomeOffset.Add(5, foreignIncomeTotal);

                processedOffSets = true;
            }

            var components = from component in filteredList
                             select new
                             {
                                 DistributionComponents = component.Components.Where(comp => comp.Category == componentCategory),
                             };

            ObservableCollection<DistributionComponent> componentsList = new ObservableCollection<DistributionComponent>();

            foreach (var comp in components)
            {
                foreach (DistributionComponent disComp in comp.DistributionComponents)
                {
                    componentsList.Add(disComp);
                }
            }

            var componentGroup = componentsList.OrderBy(c => c.Decription).GroupBy(c => c.Decription);
            rowIndex = 1;

            foreach (var compGroupItem in componentGroup)
            {
                double total = compGroupItem.Sum(comp => comp.Unit_Amount.HasValue ? comp.Unit_Amount.Value : 0);
                componentTable.Cells[rowIndex, 5].Text = total.ToString("C");
                componentTable.Cells[rowIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                rowIndex++;

                if (totals.ContainsKey(5))
                    totals[5] += total;
                else
                    totals.Add(5, total);
            }

            componentTable.Rows[rowIndex].Height = .2;
            componentTable.Rows[rowIndex].Style.FontBold = true;
            componentTable.Rows[rowIndex].Style.FontBold = true;
            componentTable.Rows[rowIndex].Style.TextAlignVert = AlignVertEnum.Center;
            componentTable.Cells[rowIndex, 0].Text = "Total for " + Enumeration.GetDescription(componentCategory);


            foreach (var dictItem in totals)
            {
                componentTable.Cells[rowIndex, dictItem.Key].Text = dictItem.Value.ToString("C");
                componentTable.Cells[rowIndex, dictItem.Key].Style.TextAlignHorz = AlignHorzEnum.Right;
            }

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n";
            doc.Body.Children.Add(componentTable);
            doc.Body.Children.Add(lineBreakTotal);
        }

        private void SetComponentHeaderTable(C1PrintDocument doc, List<DistributionIncomeEntity> distributionIncomeEntityList)
        {
            int columnCount = distributionIncomeEntityList.Count();

            RenderTable componentTable = new RenderTable();
            componentTable.Style.FontSize = 8;
            componentTable.RowGroups[0, 1].PageHeader = true;
            componentTable.Rows[0].Height = .4;
            componentTable.Rows[0].Style.FontBold = true;
            componentTable.Rows[0].Style.FontBold = true;
            componentTable.Rows[0].Style.TextAlignHorz = AlignHorzEnum.Right;
            componentTable.Cells[0, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            componentTable.Cells[0, 0].Text = "Cash Distribution components";
            componentTable.Cols[0].Width = 8;

            int columnIndex = 1;

            for (int i = 1; i <= 5; i++)
                componentTable.Cols[i].Width = 3;

            componentTable.Cols[6].Width = .2;
            componentTable.Cells[0, 5].Text = "Total";

            foreach (var item in distributionIncomeEntityList)
            {
                if (this.comboBoxAccType.Text.ToLower() == "cash")
                    componentTable.Cells[0, columnIndex].Text = "Distribution (" + item.PaymentDate.Value.ToString("dd/MM/yyyy") + ")";
                else
                    componentTable.Cells[0, columnIndex].Text = "Distribution (" + item.RecordDate.Value.ToString("dd/MM/yyyy") + ")";

                componentTable.Cells[0, columnIndex].Style.TextAlignHorz = AlignHorzEnum.Right;
                componentTable.Cols[columnIndex].Width = 3;
                columnIndex++;
            }

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n";
            RenderText lineBreakTotal2 = new RenderText();
            lineBreakTotal.Text = "\n\n";
            doc.Body.Children.Add(lineBreakTotal);
            doc.Body.Children.Add(componentTable);
            doc.Body.Children.Add(lineBreakTotal2);
        }

        private void GainsAndLosses(C1PrintDocument doc, RenderToc toc)
        {
            RenderText celltext = new RenderText(doc);
            celltext.Text = "GAINS AND LOSSES REPORT" + "\n\n";
            celltext.Style.TextColor = Color.FromArgb(46, 44, 83);
            celltext.Style.TextAlignHorz = AlignHorzEnum.Left;
            celltext.Style.FontSize = 16;
            celltext.Style.FontBold = true;
            doc.Body.Children.Add(celltext);
            toc.AddItem("Gains & Losses", celltext);

            PresentationData = (DataSet)this.dsList["LossAndGainsReportDS"];

            DataView secSummaryTableView = new DataView(PresentationData.Tables[LossAndGainsReportDS.SECURITYSUMMARYTABLEUNREALISED]);
            secSummaryTableView.Sort = LossAndGainsReportDS.SECNAME + " ASC";

            DataView secSummaryTableViewOverall = new DataView(PresentationData.Tables[LossAndGainsReportDS.GAINSLOSSESSUMMARYTABLE]);
            secSummaryTableViewOverall.Sort = LossAndGainsReportDS.ACCOUNTTYPE + " ASC";

            GainsAndLosses_SummmaryTableOverall(doc, secSummaryTableViewOverall.ToTable());

            var accountGroup = secSummaryTableView.ToTable().Select().GroupBy(rows => rows[LossAndGainsReportDS.ACCNO]);
            foreach (var account in accountGroup)
                GainsAndLosses_UnRealisedDetailTable(doc, account);

            DataView secSummaryTableViewRealised = new DataView(PresentationData.Tables[LossAndGainsReportDS.SECURITYSUMMARYTABLEREALISED]);
            secSummaryTableViewRealised.Sort = LossAndGainsReportDS.SECNAME + " ASC";

            var accountGroupRealised = secSummaryTableViewRealised.ToTable().Select().GroupBy(rows => rows[LossAndGainsReportDS.ACCNO]);
            foreach (var account in accountGroupRealised)
                GainsAndLosses_RealisedDetailTable(doc, account);
        }

        private static void ExtractHoldingSummaryReport(HoldingSummaryReportDS ds, C1PrintDocument doc, RenderToc toc, string header, AcountingMethodType accountingMethod)
        {
            RenderText celltext = new RenderText(doc);
            celltext.Text = header.ToUpper() + "\n\n";
            celltext.Style.TextColor = Color.FromArgb(46, 44, 83);
            celltext.Style.TextAlignHorz = AlignHorzEnum.Left;
            celltext.Style.FontSize = 16;
            celltext.Style.FontBold = true;
            doc.Body.Children.Add(celltext);
            toc.AddItem(header, celltext);

            DataView secSummaryTableView = new DataView(ds.securitySummaryTable);
            secSummaryTableView.Sort = ds.securitySummaryTable.SECNAME + " ASC";

            DataTable secFilteredTable = secSummaryTableView.ToTable();

            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.Style.FontSize = 7;
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Code";
            secSummaryTotalTable.Cells[0, 1].Text = "Description";
            secSummaryTotalTable.Cells[0, 2].Text = "Type";
            secSummaryTotalTable.Cells[0, 3].Text = "Op. Units";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 4].Text = "Op. Price";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 5].Text = "Op. Bal.";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 6].Text = "Purchase";
            secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 7].Text = "Sale";
            secSummaryTotalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 8].Text = "Adj. Up";
            secSummaryTotalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 9].Text = "Adj. Down";
            secSummaryTotalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 10].Text = "Movement";
            secSummaryTotalTable.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 11].Text = "Cls. Units";
            secSummaryTotalTable.Cells[0, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 12].Text = "Cls. Price";
            secSummaryTotalTable.Cells[0, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 13].Text = "Cls. Balance";
            secSummaryTotalTable.Cells[0, 13].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 7;
            secSummaryTotalTable.Cols[2].Width = 2;
            secSummaryTotalTable.Cols[3].Width = 2;
            secSummaryTotalTable.Cols[4].Width = 2;
            secSummaryTotalTable.Cols[5].Width = 2;
            secSummaryTotalTable.Cols[6].Width = 2;
            secSummaryTotalTable.Cols[7].Width = 2;
            secSummaryTotalTable.Cols[8].Width = 2;
            secSummaryTotalTable.Cols[9].Width = 2;
            secSummaryTotalTable.Cols[10].Width = 2;
            secSummaryTotalTable.Cols[11].Width = 2;
            secSummaryTotalTable.Cols[12].Width = 2;
            secSummaryTotalTable.Cols[13].Width = 2;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            decimal openingBalTotal = 0;
            decimal closingBalTotal = 0;
            decimal applicationTotal = 0;
            decimal redemptionTotal = 0;
            decimal adjustmentUpTotal = 0;
            decimal adjustmentDownTotal = 0;
            decimal movementTotal = 0;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            foreach (DataRow secRow in secFilteredTable.Rows)
            {
                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[ds.securitySummaryTable.SECNAME].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[ds.securitySummaryTable.DESCRIPTION].ToString();
                secSummaryTotalTable.Cells[totalIndex, 2].Text = secRow[ds.securitySummaryTable.TYPE].ToString();

                if (secRow[ds.securitySummaryTable.OPENINGUNITSBAL] != null & secRow[ds.securitySummaryTable.OPENINGUNITSBAL] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = Convert.ToDecimal(secRow[ds.securitySummaryTable.OPENINGUNITSBAL]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[ds.securitySummaryTable.OPENINGUNITPRICE] != null & secRow[ds.securitySummaryTable.OPENINGUNITPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = Convert.ToDecimal(secRow[ds.securitySummaryTable.OPENINGUNITPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal openingBal = Convert.ToDecimal(secRow[ds.securitySummaryTable.OPENINGBALANCE]);
                secSummaryTotalTable.Cells[totalIndex, 5].Text = openingBal.ToString("C");
                openingBalTotal += openingBal;
                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal application = 0;

                if (secRow[ds.securitySummaryTable.APPLICATION] != DBNull.Value)
                    application = Convert.ToDecimal(secRow[ds.securitySummaryTable.APPLICATION]);

                secSummaryTotalTable.Cells[totalIndex, 6].Text = application.ToString("C");
                applicationTotal += application;
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal redemption = 0;

                if (secRow[ds.securitySummaryTable.REDEMPTION] != DBNull.Value)
                    redemption = Convert.ToDecimal(secRow[ds.securitySummaryTable.REDEMPTION]);

                secSummaryTotalTable.Cells[totalIndex, 7].Text = redemption.ToString("C");
                redemptionTotal += redemption;
                secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal adjUp = 0;

                if (secRow[ds.securitySummaryTable.ADJUSTMENTUP] != DBNull.Value)
                    adjUp = Convert.ToDecimal(secRow[ds.securitySummaryTable.ADJUSTMENTUP]);

                secSummaryTotalTable.Cells[totalIndex, 8].Text = adjUp.ToString("C");
                adjustmentUpTotal += adjUp;
                secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal adjDown = 0;

                if (secRow[ds.securitySummaryTable.ADJUSTMENTDOWN] != DBNull.Value)
                    adjDown = Convert.ToDecimal(secRow[ds.securitySummaryTable.ADJUSTMENTDOWN]);

                secSummaryTotalTable.Cells[totalIndex, 9].Text = adjDown.ToString("C");
                adjustmentDownTotal += adjDown;
                secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal movement = 0;

                if (accountingMethod == AcountingMethodType.Cash)
                    movement = Convert.ToDecimal(secRow[ds.securitySummaryTable.MOVEMENT]);
                else if (accountingMethod == AcountingMethodType.Accrual)
                    movement = Convert.ToDecimal(secRow[ds.securitySummaryTable.MOVEMENTADJ]);

                secSummaryTotalTable.Cells[totalIndex, 10].Text = movement.ToString("C");
                movementTotal += movement;
                secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[ds.securitySummaryTable.CLOSINGUNITS] != null & secRow[ds.securitySummaryTable.CLOSINGUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = Convert.ToDecimal(secRow[ds.securitySummaryTable.CLOSINGUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[ds.securitySummaryTable.CLOSINGUNITPRICE] != null & secRow[ds.securitySummaryTable.CLOSINGUNITPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 12].Text = Convert.ToDecimal(secRow[ds.securitySummaryTable.CLOSINGUNITPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 12].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 12].Style.TextAlignHorz = AlignHorzEnum.Right;


                decimal closingBal = 0;

                if (accountingMethod == AcountingMethodType.Cash)
                    closingBal = Convert.ToDecimal(secRow[ds.securitySummaryTable.CLOSINGBAL]);
                else if (accountingMethod == AcountingMethodType.Accrual)
                    closingBal = Convert.ToDecimal(secRow[ds.securitySummaryTable.CLOSINGBALADJ]);

                secSummaryTotalTable.Cells[totalIndex, 13].Text = closingBal.ToString("C");
                closingBalTotal += closingBal;
                secSummaryTotalTable.Cells[totalIndex, 13].Style.TextAlignHorz = AlignHorzEnum.Right;


                totalIndex++;
            }

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            secSummaryTotalTable.Cells[totalIndex, 1].Text = "";
            secSummaryTotalTable.Cells[totalIndex, 5].Text = openingBalTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[totalIndex, 6].Text = applicationTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[totalIndex, 7].Text = redemptionTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[totalIndex, 8].Text = adjustmentUpTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[totalIndex, 9].Text = adjustmentDownTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[totalIndex, 10].Text = movementTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[totalIndex, 13].Text = closingBalTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 13].Style.TextAlignHorz = AlignHorzEnum.Right;
        }

        private static void SetCapitalReport(HoldingSummaryReportDS ds, C1PrintDocument doc, RenderToc toc)
        {
            RenderText celltext = new RenderText(doc);
            celltext.Text = "INVESTMENT TRANSACTION LISTING" + "\n\n";
            celltext.Style.TextColor = Color.FromArgb(46, 44, 83);
            celltext.Style.TextAlignHorz = AlignHorzEnum.Left;
            celltext.Style.FontSize = 16;
            celltext.Style.FontBold = true;
            doc.Body.Children.Add(celltext);

            DataView secSummaryTableView = new DataView(ds.Tables[HoldingSummaryReportDS.CAPITALTRANTABLE]);
            secSummaryTableView.Sort = HoldingSummaryReportDS.SECODE + " ASC";

            DataTable secFilteredTable = secSummaryTableView.ToTable();

            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.Style.FontSize = 6;
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Code";
            secSummaryTotalTable.Cells[0, 1].Text = "Security Name";
            secSummaryTotalTable.Cells[0, 2].Text = "Product Type";
            secSummaryTotalTable.Cells[0, 3].Text = "Transaction Type";
            secSummaryTotalTable.Cells[0, 4].Text = "Account No.";
            secSummaryTotalTable.Cells[0, 5].Text = "Trade Date";

            secSummaryTotalTable.Cells[0, 6].Text = "Settlement Date";

            secSummaryTotalTable.Cells[0, 7].Text = "Units";
            secSummaryTotalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 8].Text = "Trade Price";
            secSummaryTotalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 9].Text = "Gross Value";
            secSummaryTotalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 10].Text = "Fees";
            secSummaryTotalTable.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 11].Text = "Net Value";
            secSummaryTotalTable.Cells[0, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 7;
            secSummaryTotalTable.Cols[2].Width = 2.5;
            secSummaryTotalTable.Cols[3].Width = 3.5;
            secSummaryTotalTable.Cols[4].Width = 2.5;
            secSummaryTotalTable.Cols[5].Width = 3;
            secSummaryTotalTable.Cols[6].Width = 3;
            secSummaryTotalTable.Cols[7].Width = 2.5;
            secSummaryTotalTable.Cols[8].Width = 2.5;
            secSummaryTotalTable.Cols[9].Width = 2.5;
            secSummaryTotalTable.Cols[10].Width = 2.5;
            secSummaryTotalTable.Cols[11].Width = 2.5;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Investment Transactions\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            toc.AddItem("Investment Transaction Listing", gridHeaderTotal);
            int totalIndex = 1;

            foreach (DataRow secRow in secFilteredTable.Rows)
            {
                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[HoldingSummaryReportDS.SECODE].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[HoldingSummaryReportDS.SECNAME].ToString();
                secSummaryTotalTable.Cells[totalIndex, 2].Text = secRow[HoldingSummaryReportDS.PROTYPE].ToString();
                secSummaryTotalTable.Cells[totalIndex, 3].Text = secRow[HoldingSummaryReportDS.TRANTYPE].ToString();
                secSummaryTotalTable.Cells[totalIndex, 4].Text = secRow[HoldingSummaryReportDS.ACCNO].ToString();
                secSummaryTotalTable.Cells[totalIndex, 5].Text = ((DateTime)secRow[HoldingSummaryReportDS.TRADEDATE]).ToString("dd/MM/yyyy");
                secSummaryTotalTable.Cells[totalIndex, 6].Text = ((DateTime)secRow[HoldingSummaryReportDS.SETTLEDATE]).ToString("dd/MM/yyyy");

                if (secRow[HoldingSummaryReportDS.UNITS] != null & secRow[HoldingSummaryReportDS.UNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 7].Text = Convert.ToDecimal(secRow[HoldingSummaryReportDS.UNITS]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 7].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[HoldingSummaryReportDS.TRADEPRICE] != null & secRow[HoldingSummaryReportDS.TRADEPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 8].Text = Convert.ToDecimal(secRow[HoldingSummaryReportDS.TRADEPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 8].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;


                if (secRow[HoldingSummaryReportDS.GROSSVAL] != null & secRow[HoldingSummaryReportDS.GROSSVAL] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = Convert.ToDecimal(secRow[HoldingSummaryReportDS.GROSSVAL]).ToString("C");
                else
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[HoldingSummaryReportDS.FEES] != null & secRow[HoldingSummaryReportDS.FEES] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 10].Text = Convert.ToDecimal(secRow[HoldingSummaryReportDS.FEES]).ToString("C");
                else
                    secSummaryTotalTable.Cells[totalIndex, 10].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[HoldingSummaryReportDS.NETVALUE] != null & secRow[HoldingSummaryReportDS.NETVALUE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = Convert.ToDecimal(secRow[HoldingSummaryReportDS.NETVALUE]).ToString("C");
                else
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

                totalIndex++;
            }

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;
        }

        private static void Tax_UnRealisedDetailTable(C1PrintDocument doc, IGrouping<object, DataRow> account)
        {
            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.Style.FontSize = 7;
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Code";
            secSummaryTotalTable.Cells[0, 1].Text = "Description";
            secSummaryTotalTable.Cells[0, 2].Text = "Opening";
            secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 3].Text = "Mvt";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 4].Text = "Closing";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 5].Text = "Avg. Cost Price";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 6].Text = "Trade Val ($)";
            secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 7].Text = "Fees";
            secSummaryTotalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 8].Text = "Cost Val ($)";
            secSummaryTotalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 9].Text = "Current Price";
            secSummaryTotalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 10].Text = "Curent Val ($)";
            secSummaryTotalTable.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 11].Text = "Gains / Losses";
            secSummaryTotalTable.Cells[0, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 9;
            secSummaryTotalTable.Cols[2].Width = 2;
            secSummaryTotalTable.Cols[3].Width = 2;
            secSummaryTotalTable.Cols[4].Width = 2;
            secSummaryTotalTable.Cols[5].Width = 3;
            secSummaryTotalTable.Cols[6].Width = 3;
            secSummaryTotalTable.Cols[7].Width = 2;
            secSummaryTotalTable.Cols[8].Width = 3;
            secSummaryTotalTable.Cols[9].Width = 3;
            secSummaryTotalTable.Cols[10].Width = 3;
            secSummaryTotalTable.Cols[11].Width = 3;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Unrealised Gains & Losses - " + account.Key.ToString().ToUpper() + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            decimal grandTotal = 0;
            decimal grandTotalUnrealisedValue = 0;
            decimal tradeTotalValue = 0;
            decimal grandProfitLoss = 0;


            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            foreach (DataRow secRow in account)
            {
                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[LossAndGainsReportDS.SECNAME].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[LossAndGainsReportDS.DESCRIPTION].ToString();
                if (secRow[LossAndGainsReportDS.OPENINGBALUNITS] != null & secRow[LossAndGainsReportDS.OPENINGBALUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.OPENINGBALUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.MOVEMENTUNITS] != null & secRow[LossAndGainsReportDS.MOVEMENTUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.MOVEMENTUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.CLOSINGBALUNITS] != null & secRow[LossAndGainsReportDS.CLOSINGBALUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.CLOSINGBALUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.UNREALISEDCOSTPRICE] != null & secRow[LossAndGainsReportDS.UNREALISEDCOSTPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISEDCOSTPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;


                decimal tradeValue = 0;
                if (secRow[LossAndGainsReportDS.TRADEVALUE] != null & secRow[LossAndGainsReportDS.TRADEVALUE] != DBNull.Value)
                    tradeValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.TRADEVALUE]);
                else
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 6].Text = tradeValue.ToString("C");
                tradeTotalValue += tradeValue;
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal fees = 0;
                if (secRow[LossAndGainsReportDS.FEE] != null & secRow[LossAndGainsReportDS.FEE] != DBNull.Value)
                    fees = Convert.ToDecimal(secRow[LossAndGainsReportDS.FEE]);
                else
                    secSummaryTotalTable.Cells[totalIndex, 7].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 7].Text = fees.ToString("C");
                secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;


                decimal unrealisedCostValue = 0;
                if (secRow[LossAndGainsReportDS.UNREALISEDCOSTVALUE] != null & secRow[LossAndGainsReportDS.UNREALISEDCOSTVALUE] != DBNull.Value)
                    unrealisedCostValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISEDCOSTVALUE]);
                else
                    secSummaryTotalTable.Cells[totalIndex, 8].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 8].Text = unrealisedCostValue.ToString("C");
                grandTotalUnrealisedValue += unrealisedCostValue;
                secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;


                if (secRow[LossAndGainsReportDS.UNITPRICE] != null & secRow[LossAndGainsReportDS.UNITPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNITPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
                decimal total = Convert.ToDecimal(secRow[LossAndGainsReportDS.TOTAL]);
                secSummaryTotalTable.Cells[totalIndex, 10].Text = total.ToString("C");
                grandTotal += total;

                secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal unrealisedProfitLoss = 0;
                if (secRow[LossAndGainsReportDS.UNREALISEDPROFITLOSS] != null & secRow[LossAndGainsReportDS.UNREALISEDPROFITLOSS] != DBNull.Value)
                {
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISEDPROFITLOSS]).ToString("C");
                    unrealisedProfitLoss = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISEDPROFITLOSS]);
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
                if (unrealisedProfitLoss > 0)
                    secSummaryTotalTable.Cells[totalIndex, 11].Style.TextColor = Color.Green;
                else
                    secSummaryTotalTable.Cells[totalIndex, 11].Style.TextColor = Color.Red;

                grandProfitLoss += unrealisedProfitLoss;

                totalIndex++;
            }

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            secSummaryTotalTable.Cells[totalIndex, 1].Text = "";
            secSummaryTotalTable.Cells[totalIndex, 6].Text = tradeTotalValue.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 8].Text = grandTotalUnrealisedValue.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 10].Text = grandTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

            if (grandProfitLoss > 0)
                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextColor = Color.Green;
            else
                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextColor = Color.Red;

            secSummaryTotalTable.Cells[totalIndex, 11].Text = grandProfitLoss.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
        }

        private static void Tax_RealisedDetailTable(C1PrintDocument doc, IGrouping<object, DataRow> account)
        {
            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.Style.FontSize = 7;
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Sec. Code";
            secSummaryTotalTable.Cells[0, 1].Text = "Description";
            secSummaryTotalTable.Cells[0, 2].Text = "Sold Units";
            secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 3].Text = "Avg. Cost Price";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 4].Text = "Cost Value ($)";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 5].Text = "Sale Value ($)";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 6].Text = "Gains / Losses";
            secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 9;
            secSummaryTotalTable.Cols[2].Width = 3;
            secSummaryTotalTable.Cols[3].Width = 3;
            secSummaryTotalTable.Cols[4].Width = 3;
            secSummaryTotalTable.Cols[5].Width = 3;
            secSummaryTotalTable.Cols[6].Width = 3;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Realised Gains & Losses - " + account.Key.ToString().ToUpper() + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            decimal grandTotal = 0;
            decimal grandTotalrealisedValue = 0;
            decimal grandProfitLoss = 0;


            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            foreach (DataRow secRow in account)
            {
                string secCode = secRow[LossAndGainsReportDS.SECNAME].ToString();

                //IV004 && IV014 have special col names
                if (secCode == "IV004" || secCode == "IV014")
                {
                    secSummaryTotalTable.Cells[0, 0].Text = "Sec. Code";
                    secSummaryTotalTable.Cells[0, 1].Text = "Description";
                    secSummaryTotalTable.Cells[0, 2].Text = "Total Units";
                    secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 3].Text = "Avg. Cost Price";
                    secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 4].Text = "Cost Value ($)";
                    secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 5].Text = "Current Value ($)";
                    secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 6].Text = "Gains / Losses";
                    secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                }

                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[LossAndGainsReportDS.SECNAME].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[LossAndGainsReportDS.DESCRIPTION].ToString();

                if (secRow[LossAndGainsReportDS.SOLDUNITS] != null & secRow[LossAndGainsReportDS.SOLDUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.SOLDUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.REALISEDCOSTPRICE] != null & secRow[LossAndGainsReportDS.REALISEDCOSTPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISEDCOSTPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                decimal realisedCostValue = 0;
                if (secRow[LossAndGainsReportDS.REALISEDCOSTVALUE] != null & secRow[LossAndGainsReportDS.REALISEDCOSTVALUE] != DBNull.Value)
                    realisedCostValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISEDCOSTVALUE]);
                else
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 4].Text = realisedCostValue.ToString("C");
                grandTotalrealisedValue += realisedCostValue;
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal saleProceeds = Convert.ToDecimal(secRow[LossAndGainsReportDS.SALESPROCEED]);
                secSummaryTotalTable.Cells[totalIndex, 5].Text = saleProceeds.ToString("C");
                grandTotal += saleProceeds;

                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal realisedProfitLoss = 0;
                if (secRow[LossAndGainsReportDS.REALISEDPROFITLOSS] != null & secRow[LossAndGainsReportDS.REALISEDPROFITLOSS] != DBNull.Value)
                {
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISEDPROFITLOSS]).ToString("C");
                    realisedProfitLoss = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISEDPROFITLOSS]);
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                if (realisedProfitLoss > 0)
                    secSummaryTotalTable.Cells[totalIndex, 6].Style.TextColor = Color.Green;
                else
                    secSummaryTotalTable.Cells[totalIndex, 6].Style.TextColor = Color.Red;

                grandProfitLoss += realisedProfitLoss;

                totalIndex++;
            }


            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            secSummaryTotalTable.Cells[totalIndex, 1].Text = "";
            secSummaryTotalTable.Cells[totalIndex, 4].Text = grandTotalrealisedValue.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 5].Text = grandTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

            if (grandProfitLoss > 0)
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextColor = Color.Green;
            else
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextColor = Color.Red;

            secSummaryTotalTable.Cells[totalIndex, 6].Text = grandProfitLoss.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
        }

        private static void Tax_BankDetailTable(C1PrintDocument doc, DataTable table, string sysCategory, string category)
        {
            if (table.Rows.Count > 0)
            {
                RenderTable secSumTable = new RenderTable();
                secSumTable.RowGroups[0, 1].PageHeader = true;
                RenderTable secSummaryTotalTable = new RenderTable();
                secSummaryTotalTable.Style.FontSize = 7;
                secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
                secSummaryTotalTable.Rows[0].Height = .2;
                secSummaryTotalTable.Rows[0].Style.FontBold = true;
                secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                secSummaryTotalTable.Rows[0].Style.FontBold = true;
                secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                secSummaryTotalTable.Cells[0, 0].Text = "Account Name";
                secSummaryTotalTable.Cells[0, 1].Text = "Account NO";
                secSummaryTotalTable.Cells[0, 2].Text = "Transaction Date";
                secSummaryTotalTable.Cells[0, 3].Text = "Description";
                secSummaryTotalTable.Cells[0, 4].Text = "Amount";
                secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                secSummaryTotalTable.Cols[0].Width = 9;
                secSummaryTotalTable.Cols[1].Width = 3;
                secSummaryTotalTable.Cols[2].Width = 3;
                secSummaryTotalTable.Cols[3].Width = 5;
                secSummaryTotalTable.Cols[4].Width = 3;


                RenderText gridHeaderTotal = new RenderText();
                gridHeaderTotal.Text = category.ToUpper() + " - " + sysCategory.ToUpper() + " Transactions\n\n";
                gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeaderTotal.Style.FontSize = 11;
                gridHeaderTotal.Style.FontBold = true;

                decimal grandTotal = 0;

                RenderText lineBreakTotal = new RenderText();
                lineBreakTotal.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeaderTotal);
                doc.Body.Children.Add(secSummaryTotalTable);
                doc.Body.Children.Add(lineBreakTotal);
                int totalIndex = 1;

                DataView sortedView = new DataView(table);
                sortedView.Sort = LossAndGainsReportDS.ACCOUNTNO + " ASC";
                sortedView.RowFilter = LossAndGainsReportDS.TOTAL + " <> 0";
                DataTable sortedTable = sortedView.ToTable();

                foreach (DataRow secRow in sortedTable.Rows)
                {
                    secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[LossAndGainsReportDS.ACCOUNTNAME].ToString();
                    secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[LossAndGainsReportDS.ACCOUNTNO].ToString();
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = ((DateTime)secRow[LossAndGainsReportDS.TRANSACTIONDATE]).ToString("dd/MM/yyyy");
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = secRow[LossAndGainsReportDS.DESCRIPTION].ToString();

                    decimal totalValue = 0;

                    if (secRow[LossAndGainsReportDS.TOTAL] != null & secRow[LossAndGainsReportDS.TOTAL] != DBNull.Value)
                    {
                        totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.TOTAL]);
                        secSummaryTotalTable.Cells[totalIndex, 4].Text = totalValue.ToString("C");
                    }
                    else
                        secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";
                    secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    grandTotal += totalValue;

                    totalIndex++;
                }

                secSummaryTotalTable.Rows[totalIndex].Height = .2;
                secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
                secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
                secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

                secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
                secSummaryTotalTable.Cells[totalIndex, 1].Text = "";
                secSummaryTotalTable.Cells[totalIndex, 4].Text = grandTotal.ToString("C");
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            }
        }

        private static void Tax_SummmaryTableOverall(C1PrintDocument doc, DataTable overallSummary, string header, AcountingMethodType acountingMethodType)
        {
            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Style.FontSize = 7;

            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Acc Type";
            secSummaryTotalTable.Cells[0, 1].Text = "Account NO";

            secSummaryTotalTable.Cells[0, 2].Text = "Opening";
            secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 3].Text = "Investment";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 4].Text = "Tran. In/Out";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 5].Text = "Interest";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 6].Text = "Rental";
            secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 7].Text = "Distribution";
            secSummaryTotalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 8].Text = "Dividend";
            secSummaryTotalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 9].Text = "Expenses";
            secSummaryTotalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 10].Text = "Change in Val.";
            secSummaryTotalTable.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 11].Text = "Closing";
            secSummaryTotalTable.Cells[0, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 12].Text = "Unrealised";
            secSummaryTotalTable.Cells[0, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 13].Text = "Realised";
            secSummaryTotalTable.Cells[0, 13].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 4;
            secSummaryTotalTable.Cols[2].Width = 2.5;
            secSummaryTotalTable.Cols[3].Width = 2.5;
            secSummaryTotalTable.Cols[4].Width = 2.5;
            secSummaryTotalTable.Cols[5].Width = 2.5;
            secSummaryTotalTable.Cols[6].Width = 2.5;
            secSummaryTotalTable.Cols[7].Width = 2.5;
            secSummaryTotalTable.Cols[8].Width = 2.5;
            secSummaryTotalTable.Cols[9].Width = 2.5;
            secSummaryTotalTable.Cols[10].Width = 2.5;
            secSummaryTotalTable.Cols[11].Width = 2.5;
            secSummaryTotalTable.Cols[12].Width = 2.5;
            secSummaryTotalTable.Cols[13].Width = 2.5;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = header + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 12;
            gridHeaderTotal.Style.FontBold = true;

            decimal openingBalTotal = 0;
            decimal investTotal = 0;
            decimal transferInTotal = 0;
            decimal transferOutTotal = 0;
            decimal changeInMktValTotal = 0;
            decimal interestTotal = 0;
            decimal rentalTotal = 0;
            decimal disTotal = 0;
            decimal divTotal = 0;
            decimal expTotal = 0;
            decimal realisedTotal = 0;
            decimal unRealisedTotal = 0;
            decimal closingTotal = 0;

            RenderText lineBreakTotal2 = new RenderText();
            lineBreakTotal2.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal2);
            int totalIndex = 1;

            foreach (DataRow secRow in overallSummary.Rows)
            {
                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[LossAndGainsReportDS.ACCOUNTTYPE].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[LossAndGainsReportDS.ACCOUNTNO].ToString();

                decimal totalValue = 0;

                if (secRow[LossAndGainsReportDS.OPENINGBALANCE] != null & secRow[LossAndGainsReportDS.OPENINGBALANCE] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.OPENINGBALANCE]);
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                openingBalTotal += totalValue;


                totalValue = 0;

                if (secRow[LossAndGainsReportDS.INVESTMENT] != null & secRow[LossAndGainsReportDS.INVESTMENT] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.INVESTMENT]);
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                investTotal += totalValue;

                totalValue = 0;
                decimal transgerIn = 0;
                decimal transferOut = 0;
                if (secRow[LossAndGainsReportDS.TRANSFERIN] != null & secRow[LossAndGainsReportDS.TRANSFERIN] != DBNull.Value)
                    transgerIn = Convert.ToDecimal(secRow[LossAndGainsReportDS.TRANSFERIN]);
                if (secRow[LossAndGainsReportDS.TRANSFEROUT] != null & secRow[LossAndGainsReportDS.TRANSFEROUT] != DBNull.Value)
                    transferOut = Convert.ToDecimal(secRow[LossAndGainsReportDS.TRANSFEROUT]);

                secSummaryTotalTable.Cells[totalIndex, 4].Text = (transgerIn + transferOut).ToString("C");
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                transferInTotal += (transgerIn + transferOut);

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.INTEREST] != null & secRow[LossAndGainsReportDS.INTEREST] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.INTEREST]);
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                interestTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.RENTAL] != null & secRow[LossAndGainsReportDS.RENTAL] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.RENTAL]);
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                rentalTotal += totalValue;

                totalValue = 0;
                if (acountingMethodType == AcountingMethodType.Accrual)
                {
                    if (secRow[LossAndGainsReportDS.DISTRIBUTIONADJ] != null & secRow[LossAndGainsReportDS.DISTRIBUTIONADJ] != DBNull.Value)
                    {
                        totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.DISTRIBUTIONADJ]);
                        secSummaryTotalTable.Cells[totalIndex, 7].Text = totalValue.ToString("C");
                    }
                    else
                        secSummaryTotalTable.Cells[totalIndex, 7].Text = "-";
                    secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                }
                else if (acountingMethodType == AcountingMethodType.Cash)
                {
                    if (secRow[LossAndGainsReportDS.DISTRIBUTION] != null & secRow[LossAndGainsReportDS.DISTRIBUTION] != DBNull.Value)
                    {
                        totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.DISTRIBUTION]);
                        secSummaryTotalTable.Cells[totalIndex, 7].Text = totalValue.ToString("C");
                    }
                    else
                        secSummaryTotalTable.Cells[totalIndex, 7].Text = "-";
                    secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                }

                disTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.DIVIDEND] != null & secRow[LossAndGainsReportDS.DIVIDEND] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.DIVIDEND]);
                    secSummaryTotalTable.Cells[totalIndex, 8].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 8].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                divTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.EXPENSES] != null & secRow[LossAndGainsReportDS.EXPENSES] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.EXPENSES]);
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                expTotal += totalValue;


                totalValue = 0;

                if (acountingMethodType == AcountingMethodType.Accrual)
                {
                    if (secRow[LossAndGainsReportDS.CLOSINGADJBALANCE] != null & secRow[LossAndGainsReportDS.CLOSINGADJBALANCE] != DBNull.Value)
                    {
                        totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.CLOSINGADJBALANCE]);
                        secSummaryTotalTable.Cells[totalIndex, 11].Text = totalValue.ToString("C");
                    }
                    else
                        secSummaryTotalTable.Cells[totalIndex, 11].Text = "-";
                    secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
                }
                else if (acountingMethodType == AcountingMethodType.Cash)
                {
                    if (secRow[LossAndGainsReportDS.CLOSINGBALANCE] != null & secRow[LossAndGainsReportDS.CLOSINGBALANCE] != DBNull.Value)
                    {
                        totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.CLOSINGBALANCE]);
                        secSummaryTotalTable.Cells[totalIndex, 11].Text = totalValue.ToString("C");
                    }
                    else
                        secSummaryTotalTable.Cells[totalIndex, 11].Text = "-";
                    secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
                }

                closingTotal += totalValue;


                totalValue = 0;

                if (secRow[LossAndGainsReportDS.CHANGEINMKT] != null & secRow[LossAndGainsReportDS.CHANGEINMKT] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.CHANGEINMKT]);
                    secSummaryTotalTable.Cells[totalIndex, 10].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 10].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                changeInMktValTotal += totalValue;


                totalValue = 0;

                if (secRow[LossAndGainsReportDS.UNREALISED] != null & secRow[LossAndGainsReportDS.UNREALISED] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISED]);
                    secSummaryTotalTable.Cells[totalIndex, 12].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 12].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

                unRealisedTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.REALISED] != null & secRow[LossAndGainsReportDS.REALISED] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISED]);
                    secSummaryTotalTable.Cells[totalIndex, 13].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 13].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 13].Style.TextAlignHorz = AlignHorzEnum.Right;

                realisedTotal += totalValue;


                totalIndex++;
            }

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            secSummaryTotalTable.Cells[totalIndex, 1].Text = "";

            secSummaryTotalTable.Cells[totalIndex, 2].Text = openingBalTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 3].Text = investTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 4].Text = (transferInTotal + transferOutTotal).ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 5].Text = interestTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 6].Text = rentalTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 7].Text = disTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 8].Text = divTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 9].Text = expTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 10].Text = changeInMktValTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 11].Text = closingTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 12].Text = unRealisedTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 12].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 13].Text = realisedTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 13].Style.TextAlignHorz = AlignHorzEnum.Right;
        }

        public static void AddFooter(C1PrintDocument doc)
        {
            doc.PageLayouts.PrintFooterOnLastPage = true;
            RenderTable footer = new RenderTable();

            footer.Rows[0].Height = .30;
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;
            footer.Rows[1].Height = 2.5;
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.TextAlignVert = AlignVertEnum.Center;

            footer.Cells[0, 0].Text = "FOR YOUR INFORMATION";
            footer.Cells[0, 0].Style.TextAlignHorz = AlignHorzEnum.Center;
            footer.Cells[0, 0].Style.FontSize = 12;

            footer.Cells[1, 0].Text = "\n1. This report provides an estimate of the realised and unrealised gains and losses of the portfolio for the period.  It does not constitute tax advice, nor an actual taxation outcome.  The report is provided for general information and is not intended to be considered as comprehensive tax information or relied upon without reference to an adviser and/or accountant.\n\nTax law depends upon an investors specific circumstances and the assumptions made in  this report may not be relevant to the investor." +
                                        "\n\n2. Information has been provided on a cash rather than accrual basis.  Income listed on the report are amounts that have been received as deposits in the linked CMA account and correctly identified as an income transaction." +
                                        "\n\n3. This report is provided by e-Clipse Online Pty Limited ABN 70 145 358 630, AFSL 357 306 (“e-Clipse”) and is based on information provided to e-Clipse by third parties.  Whilst every reasonable effort has been made by e-Clipse to ensure its accuracy, neither e-Clipse nor any of its related entities guarantee its accuracy nor accept any liability for any errors or omissions." +
                                        "\n\ne-Clipse Online Pty Ltd (ABN 70 145 358 630)\n3/36 Bydown Street, Neutral Bay, NSW 2089\nhttp://www.e-clipse.com.au\nP: +61 2 9346 4686";
            footer.Cells[1, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            footer.Cells[1, 0].Style.TextAlignVert = AlignVertEnum.Top;
            footer.Cells[1, 0].Style.FontSize = 9;
            footer.Cells[1, 0].Style.FontBold = false;

            doc.PageLayouts.LastPage = new PageLayout();
            doc.PageLayouts.LastPage.PageFooter = footer;
        }

        private static void GainsAndLosses_UnRealisedDetailTable(C1PrintDocument doc, IGrouping<object, DataRow> account)
        {
            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.Style.FontSize = 7;
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Code";
            secSummaryTotalTable.Cells[0, 1].Text = "Description";
            secSummaryTotalTable.Cells[0, 2].Text = "Opening";
            secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 3].Text = "Mvt";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 4].Text = "Closing";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 5].Text = "Avg. Cost Price";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 6].Text = "Trade Val ($)";
            secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 7].Text = "Fees";
            secSummaryTotalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 8].Text = "Cost Val ($)";
            secSummaryTotalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 9].Text = "Current Price";
            secSummaryTotalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 10].Text = "Curent Val ($)";
            secSummaryTotalTable.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 11].Text = "Gains / Losses";
            secSummaryTotalTable.Cells[0, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 9;
            secSummaryTotalTable.Cols[2].Width = 2;
            secSummaryTotalTable.Cols[3].Width = 2;
            secSummaryTotalTable.Cols[4].Width = 2;
            secSummaryTotalTable.Cols[5].Width = 3;
            secSummaryTotalTable.Cols[6].Width = 3;
            secSummaryTotalTable.Cols[7].Width = 2;
            secSummaryTotalTable.Cols[8].Width = 3;
            secSummaryTotalTable.Cols[9].Width = 3;
            secSummaryTotalTable.Cols[10].Width = 3;
            secSummaryTotalTable.Cols[11].Width = 3;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Unrealised Gains & Losses - " + account.Key.ToString().ToUpper() + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            decimal grandTotal = 0;
            decimal grandTotalUnrealisedValue = 0;
            decimal tradeTotalValue = 0;
            decimal grandProfitLoss = 0;


            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            foreach (DataRow secRow in account)
            {
                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[LossAndGainsReportDS.SECNAME].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[LossAndGainsReportDS.DESCRIPTION].ToString();
                if (secRow[LossAndGainsReportDS.OPENINGBALUNITS] != null & secRow[LossAndGainsReportDS.OPENINGBALUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.OPENINGBALUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.MOVEMENTUNITS] != null & secRow[LossAndGainsReportDS.MOVEMENTUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.MOVEMENTUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.CLOSINGBALUNITS] != null & secRow[LossAndGainsReportDS.CLOSINGBALUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.CLOSINGBALUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.UNREALISEDCOSTPRICE] != null & secRow[LossAndGainsReportDS.UNREALISEDCOSTPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISEDCOSTPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;


                decimal tradeValue = 0;
                if (secRow[LossAndGainsReportDS.TRADEVALUE] != null & secRow[LossAndGainsReportDS.TRADEVALUE] != DBNull.Value)
                    tradeValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.TRADEVALUE]);
                else
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 6].Text = tradeValue.ToString("C");
                tradeTotalValue += tradeValue;
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal fees = 0;
                if (secRow[LossAndGainsReportDS.FEE] != null & secRow[LossAndGainsReportDS.FEE] != DBNull.Value)
                    fees = Convert.ToDecimal(secRow[LossAndGainsReportDS.FEE]);
                else
                    secSummaryTotalTable.Cells[totalIndex, 7].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 7].Text = fees.ToString("C");
                secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;


                decimal unrealisedCostValue = 0;
                if (secRow[LossAndGainsReportDS.UNREALISEDCOSTVALUE] != null & secRow[LossAndGainsReportDS.UNREALISEDCOSTVALUE] != DBNull.Value)
                    unrealisedCostValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISEDCOSTVALUE]);
                else
                    secSummaryTotalTable.Cells[totalIndex, 8].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 8].Text = unrealisedCostValue.ToString("C");
                grandTotalUnrealisedValue += unrealisedCostValue;
                secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;


                if (secRow[LossAndGainsReportDS.UNITPRICE] != null & secRow[LossAndGainsReportDS.UNITPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNITPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
                decimal total = Convert.ToDecimal(secRow[LossAndGainsReportDS.TOTAL]);
                secSummaryTotalTable.Cells[totalIndex, 10].Text = total.ToString("C");
                grandTotal += total;

                secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal unrealisedProfitLoss = 0;
                if (secRow[LossAndGainsReportDS.UNREALISEDPROFITLOSS] != null & secRow[LossAndGainsReportDS.UNREALISEDPROFITLOSS] != DBNull.Value)
                {
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISEDPROFITLOSS]).ToString("C");
                    unrealisedProfitLoss = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISEDPROFITLOSS]);
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
                if (unrealisedProfitLoss > 0)
                    secSummaryTotalTable.Cells[totalIndex, 11].Style.TextColor = Color.Green;
                else
                    secSummaryTotalTable.Cells[totalIndex, 11].Style.TextColor = Color.Red;

                grandProfitLoss += unrealisedProfitLoss;

                totalIndex++;
            }

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            secSummaryTotalTable.Cells[totalIndex, 1].Text = "";
            secSummaryTotalTable.Cells[totalIndex, 6].Text = tradeTotalValue.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 8].Text = grandTotalUnrealisedValue.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 10].Text = grandTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

            if (grandProfitLoss > 0)
                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextColor = Color.Green;
            else
                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextColor = Color.Red;

            secSummaryTotalTable.Cells[totalIndex, 11].Text = grandProfitLoss.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
        }

        private static void GainsAndLosses_RealisedDetailTable(C1PrintDocument doc, IGrouping<object, DataRow> account)
        {
            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.Style.FontSize = 7;
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Sec. Code";
            secSummaryTotalTable.Cells[0, 1].Text = "Description";
            secSummaryTotalTable.Cells[0, 2].Text = "Sold Units";
            secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 3].Text = "Avg. Cost Price";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 4].Text = "Cost Value ($)";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 5].Text = "Sale Value ($)";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 6].Text = "Gains / Losses";
            secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 9;
            secSummaryTotalTable.Cols[2].Width = 3;
            secSummaryTotalTable.Cols[3].Width = 3;
            secSummaryTotalTable.Cols[4].Width = 3;
            secSummaryTotalTable.Cols[5].Width = 3;
            secSummaryTotalTable.Cols[6].Width = 3;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Realised Gains & Losses - " + account.Key.ToString().ToUpper() + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            decimal grandTotal = 0;
            decimal grandTotalrealisedValue = 0;
            decimal grandProfitLoss = 0;


            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            foreach (DataRow secRow in account)
            {
                string secCode = secRow[LossAndGainsReportDS.SECNAME].ToString();

                //IV004 && IV014 have special col names
                if (secCode == "IV004" || secCode == "IV014")
                {
                    secSummaryTotalTable.Cells[0, 0].Text = "Sec. Code";
                    secSummaryTotalTable.Cells[0, 1].Text = "Description";
                    secSummaryTotalTable.Cells[0, 2].Text = "Total Units";
                    secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 3].Text = "Avg. Cost Price";
                    secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 4].Text = "Cost Value ($)";
                    secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 5].Text = "Current Value ($)";
                    secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 6].Text = "Gains / Losses";
                    secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                }

                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[LossAndGainsReportDS.SECNAME].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[LossAndGainsReportDS.DESCRIPTION].ToString();

                if (secRow[LossAndGainsReportDS.SOLDUNITS] != null & secRow[LossAndGainsReportDS.SOLDUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.SOLDUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.REALISEDCOSTPRICE] != null & secRow[LossAndGainsReportDS.REALISEDCOSTPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISEDCOSTPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                decimal realisedCostValue = 0;
                if (secRow[LossAndGainsReportDS.REALISEDCOSTVALUE] != null & secRow[LossAndGainsReportDS.REALISEDCOSTVALUE] != DBNull.Value)
                    realisedCostValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISEDCOSTVALUE]);
                else
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 4].Text = realisedCostValue.ToString("C");
                grandTotalrealisedValue += realisedCostValue;
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal saleProceeds = Convert.ToDecimal(secRow[LossAndGainsReportDS.SALESPROCEED]);
                secSummaryTotalTable.Cells[totalIndex, 5].Text = saleProceeds.ToString("C");
                grandTotal += saleProceeds;

                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal realisedProfitLoss = 0;
                if (secRow[LossAndGainsReportDS.REALISEDPROFITLOSS] != null & secRow[LossAndGainsReportDS.REALISEDPROFITLOSS] != DBNull.Value)
                {
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISEDPROFITLOSS]).ToString("C");
                    realisedProfitLoss = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISEDPROFITLOSS]);
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                if (realisedProfitLoss > 0)
                    secSummaryTotalTable.Cells[totalIndex, 6].Style.TextColor = Color.Green;
                else
                    secSummaryTotalTable.Cells[totalIndex, 6].Style.TextColor = Color.Red;

                grandProfitLoss += realisedProfitLoss;

                totalIndex++;
            }

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            secSummaryTotalTable.Cells[totalIndex, 1].Text = "";
            secSummaryTotalTable.Cells[totalIndex, 4].Text = grandTotalrealisedValue.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 5].Text = grandTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

            if (grandProfitLoss > 0)
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextColor = Color.Green;
            else
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextColor = Color.Red;

            secSummaryTotalTable.Cells[totalIndex, 6].Text = grandProfitLoss.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
        }

        private static void GainsAndLosses_SummmaryTableOverall(C1PrintDocument doc, DataTable overallSummary)
        {
            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.Style.FontSize = 7;
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Account Type";
            secSummaryTotalTable.Cells[0, 1].Text = "Account Name";
            secSummaryTotalTable.Cells[0, 2].Text = "Account NO";
            secSummaryTotalTable.Cells[0, 3].Text = "Realised Gains / Losses";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 4].Text = "Unrealised Gains / Losses";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 5].Text = "Total";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 9;
            secSummaryTotalTable.Cols[2].Width = 3;
            secSummaryTotalTable.Cols[3].Width = 3;
            secSummaryTotalTable.Cols[4].Width = 3;
            secSummaryTotalTable.Cols[5].Width = 3;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Realised & Unrealised Gains & Losses Summary\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            decimal realisedTotal = 0;
            decimal unRealisedTotal = 0;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            DataView sortedView = new DataView(overallSummary);
            sortedView.Sort = LossAndGainsReportDS.ACCOUNTNO + " ASC";
            sortedView.RowFilter = LossAndGainsReportDS.REALISED + " <> 0 OR " + LossAndGainsReportDS.UNREALISED + " <> 0 ";
            DataTable sortedTable = sortedView.ToTable();

            foreach (DataRow secRow in sortedTable.Rows)
            {
                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[LossAndGainsReportDS.ACCOUNTTYPE].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[LossAndGainsReportDS.ACCOUNTNAME].ToString();
                secSummaryTotalTable.Cells[totalIndex, 2].Text = secRow[LossAndGainsReportDS.ACCOUNTNO].ToString();

                decimal realisedValue = 0;

                if (secRow[LossAndGainsReportDS.REALISED] != null & secRow[LossAndGainsReportDS.REALISED] != DBNull.Value)
                {
                    realisedValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISED]);
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = realisedValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;


                decimal unRealisedValue = 0;

                if (secRow[LossAndGainsReportDS.UNREALISED] != null & secRow[LossAndGainsReportDS.UNREALISED] != DBNull.Value)
                {
                    unRealisedValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISED]);
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = unRealisedValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                secSummaryTotalTable.Cells[totalIndex, 5].Text = (unRealisedValue + realisedValue).ToString("C");
                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                realisedTotal += realisedValue;
                unRealisedTotal += unRealisedValue;

                totalIndex++;
            }

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            secSummaryTotalTable.Cells[totalIndex, 1].Text = "";
            secSummaryTotalTable.Cells[totalIndex, 3].Text = realisedTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 4].Text = unRealisedTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 5].Text = (unRealisedTotal + realisedTotal).ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

            if (realisedTotal > 0)
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextColor = Color.Green;
            else
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextColor = Color.Red;

            if (unRealisedTotal > 0)
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextColor = Color.Green;
            else
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextColor = Color.Red;

            secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
        }

        private void SetIncomeReport(C1PrintDocument doc, RenderToc toc)
        {
            RenderText celltext = new RenderText(doc);
            celltext.Text = "INCOME REPORT" + "\n\n";
            celltext.Style.TextColor = Color.FromArgb(46, 44, 83);
            celltext.Style.TextAlignHorz = AlignHorzEnum.Left;
            celltext.Style.FontSize = 16;
            celltext.Style.FontBold = true;
            doc.Body.Children.Add(celltext);
            toc.AddItem("Income & Expense", celltext);

            DataView secSummaryTableViewOverall = new DataView(PresentationData.Tables[LossAndGainsReportDS.TAXREPORTINGMATRIXTABLE]);
            secSummaryTableViewOverall.Sort = LossAndGainsReportDS.ACCOUNTTYPE + " ASC";

            Income_SummmaryTableOverall(doc, secSummaryTableViewOverall.ToTable(), PresentationData.Tables[LossAndGainsReportDS.INCOMEBREAKDOWN], PresentationData.Tables[LossAndGainsReportDS.EXPENSEBREAKDOWN], PresentationData.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS]);

            DataView incomeSummaryView = new DataView(PresentationData.Tables[LossAndGainsReportDS.INCOMEBREAKDOWN]);
            incomeSummaryView.Sort = LossAndGainsReportDS.SECTYPE + " ASC";
        }

        private static void Income_SummmaryTableOverall(C1PrintDocument doc, DataTable overallSummary, DataTable incomeBreakDown, DataTable expenseBreakDown, DataTable bankAccountIncomes)
        {
            RenderTable secSumTableIncomeSection = new RenderTable();
            secSumTableIncomeSection.RowGroups[0, 1].PageHeader = true;
            secSumTableIncomeSection.Style.FontSize = 8;
            secSumTableIncomeSection.Rows[0].Height = .2;
            secSumTableIncomeSection.Rows[0].Style.FontBold = true;
            secSumTableIncomeSection.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSumTableIncomeSection.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSumTableIncomeSection.Rows[0].Style.FontBold = true;
            secSumTableIncomeSection.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSumTableIncomeSection.Cells[0, 0].Text = "Income Category";
            secSumTableIncomeSection.Cells[0, 1].Text = "Amount";
            secSumTableIncomeSection.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            RenderTable secSumTableExpenseSection = new RenderTable();
            secSumTableExpenseSection.RowGroups[0, 1].PageHeader = true;
            secSumTableExpenseSection.Style.FontSize = 8;
            secSumTableExpenseSection.Rows[0].Height = .2;
            secSumTableExpenseSection.Rows[0].Style.FontBold = true;
            secSumTableExpenseSection.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSumTableExpenseSection.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSumTableExpenseSection.Rows[0].Style.FontBold = true;
            secSumTableExpenseSection.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSumTableExpenseSection.Cells[0, 0].Text = "Expense Category";
            secSumTableExpenseSection.Cells[0, 1].Text = "Amount";
            secSumTableExpenseSection.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            RenderTable incomeTableBySec = new RenderTable();
            incomeTableBySec.RowGroups[0, 1].PageHeader = true;
            incomeTableBySec.Style.FontSize = 8;
            incomeTableBySec.Rows[0].Height = .2;
            incomeTableBySec.Rows[0].Style.FontBold = true;
            incomeTableBySec.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            incomeTableBySec.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            incomeTableBySec.Rows[0].Style.FontBold = true;
            incomeTableBySec.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            incomeTableBySec.Cells[0, 0].Text = "Sec";
            incomeTableBySec.Cells[0, 1].Text = "Income Category";
            incomeTableBySec.Cells[0, 2].Text = "Amount";
            incomeTableBySec.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

            RenderTable incomeBreakdownOverall = new RenderTable();
            incomeBreakdownOverall.Style.FontSize = 6;
            incomeBreakdownOverall.RowGroups[0, 1].PageHeader = true;
            incomeBreakdownOverall.Rows[0].Height = .2;
            incomeBreakdownOverall.Rows[0].Style.FontBold = true;
            incomeBreakdownOverall.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            incomeBreakdownOverall.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            incomeBreakdownOverall.Rows[0].Style.FontBold = true;
            incomeBreakdownOverall.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            incomeBreakdownOverall.Cells[0, 0].Text = "Desc.";
            incomeBreakdownOverall.Cells[0, 1].Text = "Sec. Code";
            incomeBreakdownOverall.Cells[0, 2].Text = "Inc. Type";

            incomeBreakdownOverall.Cells[0, 3].Text = "Record Date";
            incomeBreakdownOverall.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Center;

            incomeBreakdownOverall.Cells[0, 4].Text = "Payment Date";
            incomeBreakdownOverall.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Center;
            incomeBreakdownOverall.Cells[0, 5].Text = "Source";
            incomeBreakdownOverall.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Center;

            incomeBreakdownOverall.Cells[0, 6].Text = "Assc. Acc.";
            incomeBreakdownOverall.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Center;

            incomeBreakdownOverall.Cells[0, 7].Text = "Trans. Date";
            incomeBreakdownOverall.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Center;

            incomeBreakdownOverall.Cells[0, 8].Text = "$ Per Share";
            incomeBreakdownOverall.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

            incomeBreakdownOverall.Cells[0, 9].Text = "Franked";
            incomeBreakdownOverall.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

            incomeBreakdownOverall.Cells[0, 10].Text = "Unfranked";
            incomeBreakdownOverall.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

            incomeBreakdownOverall.Cells[0, 11].Text = "Franking Credit";
            incomeBreakdownOverall.Cells[0, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

            incomeBreakdownOverall.Cells[0, 12].Text = "Income Paid";
            incomeBreakdownOverall.Cells[0, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

            incomeBreakdownOverall.Cols[0].Width = 7.5;
            incomeBreakdownOverall.Cols[1].Width = 2.2;
            incomeBreakdownOverall.Cols[2].Width = 2.2;
            incomeBreakdownOverall.Cols[3].Width = 2.2;
            incomeBreakdownOverall.Cols[4].Width = 2.2;
            incomeBreakdownOverall.Cols[5].Width = 2;
            incomeBreakdownOverall.Cols[6].Width = 2;
            incomeBreakdownOverall.Cols[7].Width = 2.2;
            incomeBreakdownOverall.Cols[8].Width = 2.2;
            incomeBreakdownOverall.Cols[9].Width = 2.2;
            incomeBreakdownOverall.Cols[10].Width = 2.2;
            incomeBreakdownOverall.Cols[11].Width = 2.5;
            incomeBreakdownOverall.Cols[12].Width = 2;

            DataView incomeView = new DataView(incomeBreakDown);
            incomeView.Sort = LossAndGainsReportDS.SECTYPE + " ASC, " + LossAndGainsReportDS.PAYMENTDATE + " ASC";
            DataTable sortedIncomeView = incomeView.ToTable();

            int totalIndexIncome = 1;
            decimal incomeTotal = 0;
            decimal frankedTotal = 0;
            decimal unfrankedTotal = 0;
            decimal frankingCreditTotal = 0;

            foreach (DataRow secRow in sortedIncomeView.Rows)
            {
                incomeBreakdownOverall.Cells[totalIndexIncome, 0].Text = secRow[LossAndGainsReportDS.DESCRIPTION].ToString();
                incomeBreakdownOverall.Cells[totalIndexIncome, 1].Text = secRow[LossAndGainsReportDS.SECNAME].ToString();

                incomeBreakdownOverall.Cells[totalIndexIncome, 2].Text = secRow[LossAndGainsReportDS.INCOMETYPE].ToString();

                if (secRow[LossAndGainsReportDS.RECORDDATE] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 3].Text = "-";
                else
                    incomeBreakdownOverall.Cells[totalIndexIncome, 3].Text = ((DateTime)secRow[LossAndGainsReportDS.RECORDDATE]).ToString("dd/MM/yyyy");
                incomeBreakdownOverall.Cells[totalIndexIncome, 3].Style.TextAlignHorz = AlignHorzEnum.Center;

                if (secRow[LossAndGainsReportDS.PAYMENTDATE] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 4].Text = "-";
                else
                    incomeBreakdownOverall.Cells[totalIndexIncome, 4].Text = ((DateTime)secRow[LossAndGainsReportDS.PAYMENTDATE]).ToString("dd/MM/yyyy");
                incomeBreakdownOverall.Cells[totalIndexIncome, 4].Style.TextAlignHorz = AlignHorzEnum.Center;

                incomeBreakdownOverall.Cells[totalIndexIncome, 5].Text = secRow[LossAndGainsReportDS.SOURCE].ToString();
                incomeBreakdownOverall.Cells[totalIndexIncome, 5].Style.TextAlignHorz = AlignHorzEnum.Center;

                incomeBreakdownOverall.Cells[totalIndexIncome, 6].Text = secRow[LossAndGainsReportDS.CASH_BANKACC].ToString();

                if (secRow[LossAndGainsReportDS.CASH_PAYMENTDATE] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 7].Text = "-";
                else
                    incomeBreakdownOverall.Cells[totalIndexIncome, 7].Text = ((DateTime)secRow[LossAndGainsReportDS.CASH_PAYMENTDATE]).ToString("dd/MM/yyyy");
                incomeBreakdownOverall.Cells[totalIndexIncome, 7].Style.TextAlignHorz = AlignHorzEnum.Center;

                if (secRow[LossAndGainsReportDS.DIVAMOUNTPERSHARE] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 8].Text = "-";
                else
                    incomeBreakdownOverall.Cells[totalIndexIncome, 8].Text = ((decimal)secRow[LossAndGainsReportDS.DIVAMOUNTPERSHARE]).ToString("N4");
                incomeBreakdownOverall.Cells[totalIndexIncome, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal frankedAmount = 0;

                if (secRow[LossAndGainsReportDS.FRANKEDAMOUNT] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 9].Text = "-";
                else
                {
                    frankedAmount = (decimal)secRow[LossAndGainsReportDS.FRANKEDAMOUNT];
                    incomeBreakdownOverall.Cells[totalIndexIncome, 9].Text = frankedAmount.ToString("C");
                }

                incomeBreakdownOverall.Cells[totalIndexIncome, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
                frankedTotal += frankedAmount;

                decimal unFrankedAmount = 0;

                if (secRow[LossAndGainsReportDS.UNFRANKEDAMOUNT] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 10].Text = "-";
                else
                {
                    unFrankedAmount = (decimal)secRow[LossAndGainsReportDS.UNFRANKEDAMOUNT];
                    incomeBreakdownOverall.Cells[totalIndexIncome, 10].Text = unFrankedAmount.ToString("C");
                }

                incomeBreakdownOverall.Cells[totalIndexIncome, 10].Style.TextAlignHorz = AlignHorzEnum.Right;
                unfrankedTotal += unFrankedAmount;

                decimal frankingCreditAmount = 0;

                if (secRow[LossAndGainsReportDS.FRANKINGCREDIT] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 11].Text = "-";
                else
                {
                    frankingCreditAmount = (decimal)secRow[LossAndGainsReportDS.FRANKINGCREDIT];
                    incomeBreakdownOverall.Cells[totalIndexIncome, 11].Text = frankingCreditAmount.ToString("C");
                }
                incomeBreakdownOverall.Cells[totalIndexIncome, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
                frankingCreditTotal += frankingCreditAmount;


                if (secRow[LossAndGainsReportDS.INCPAID] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 12].Text = "-";
                else
                {
                    incomeBreakdownOverall.Cells[totalIndexIncome, 12].Text = ((decimal)secRow[LossAndGainsReportDS.INCPAID]).ToString("C");
                    incomeTotal += (decimal)secRow[LossAndGainsReportDS.INCPAID];
                }
                incomeBreakdownOverall.Cells[totalIndexIncome, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

                totalIndexIncome++;
            }

            incomeBreakdownOverall.Rows[totalIndexIncome].Height = .2;
            incomeBreakdownOverall.Rows[totalIndexIncome].Style.FontBold = true;
            incomeBreakdownOverall.Rows[totalIndexIncome].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            incomeBreakdownOverall.Rows[totalIndexIncome].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            incomeBreakdownOverall.Rows[totalIndexIncome].Style.FontBold = true;
            incomeBreakdownOverall.Rows[totalIndexIncome].Style.TextAlignVert = AlignVertEnum.Center;

            incomeBreakdownOverall.Cells[totalIndexIncome, 0].Text = "TOTAL";

            incomeBreakdownOverall.Cells[totalIndexIncome, 9].Text = frankedTotal.ToString("C");
            incomeBreakdownOverall.Cells[totalIndexIncome, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
            incomeBreakdownOverall.Cells[totalIndexIncome, 10].Text = unfrankedTotal.ToString("C");
            incomeBreakdownOverall.Cells[totalIndexIncome, 10].Style.TextAlignHorz = AlignHorzEnum.Right;
            incomeBreakdownOverall.Cells[totalIndexIncome, 11].Text = frankingCreditTotal.ToString("C");
            incomeBreakdownOverall.Cells[totalIndexIncome, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

            incomeBreakdownOverall.Cells[totalIndexIncome, 12].Text = incomeTotal.ToString("C");
            incomeBreakdownOverall.Cells[totalIndexIncome, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

            #region Expense BreakDown

            RenderTable expenseBreakdownOverall = new RenderTable();
            expenseBreakdownOverall.Style.FontSize = 6;
            expenseBreakdownOverall.RowGroups[0, 1].PageHeader = true;
            expenseBreakdownOverall.Rows[0].Height = .2;
            expenseBreakdownOverall.Rows[0].Style.FontBold = true;
            expenseBreakdownOverall.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            expenseBreakdownOverall.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            expenseBreakdownOverall.Rows[0].Style.FontBold = true;
            expenseBreakdownOverall.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            expenseBreakdownOverall.Cells[0, 0].Text = "BSB";
            expenseBreakdownOverall.Cells[0, 1].Text = "Account No";
            expenseBreakdownOverall.Cells[0, 2].Text = "Tran. Date";
            expenseBreakdownOverall.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Center;
            expenseBreakdownOverall.Cells[0, 3].Text = "Tran. Type";
            expenseBreakdownOverall.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Center;
            expenseBreakdownOverall.Cells[0, 4].Text = "Tran. Desc.";
            expenseBreakdownOverall.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Center;
            expenseBreakdownOverall.Cells[0, 5].Text = "Category";
            expenseBreakdownOverall.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Center;
            expenseBreakdownOverall.Cells[0, 6].Text = "Amount";
            expenseBreakdownOverall.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
            expenseBreakdownOverall.Cells[0, 7].Text = "GST";
            expenseBreakdownOverall.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
            expenseBreakdownOverall.Cells[0, 8].Text = "Total";
            expenseBreakdownOverall.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
            expenseBreakdownOverall.Cells[0, 9].Text = "Comment";
            expenseBreakdownOverall.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Center;

            expenseBreakdownOverall.Cols[0].Width = 2.2;
            expenseBreakdownOverall.Cols[1].Width = 2.2;
            expenseBreakdownOverall.Cols[2].Width = 2.2;
            expenseBreakdownOverall.Cols[3].Width = 3;
            expenseBreakdownOverall.Cols[4].Width = 3;
            expenseBreakdownOverall.Cols[5].Width = 3;
            expenseBreakdownOverall.Cols[6].Width = 2;
            expenseBreakdownOverall.Cols[7].Width = 2.2;
            expenseBreakdownOverall.Cols[8].Width = 2.2;
            expenseBreakdownOverall.Cols[9].Width = 4;

            DataView expenseView = new DataView(expenseBreakDown);
            expenseView.Sort = LossAndGainsReportDS.SYSTEMTRANSACTIONTYPE + " ASC, " + LossAndGainsReportDS.PAYMENTDATE + " ASC";
            DataTable sortedExpenseView = expenseView.ToTable();

            int totalIndexExpense = 1;
            decimal expenseTotal = 0;
            decimal gstTotal = 0;
            decimal totolTotal = 0;

            foreach (DataRow secRow in sortedExpenseView.Rows)
            {
                expenseBreakdownOverall.Cells[totalIndexExpense, 0].Text = secRow[LossAndGainsReportDS.BSB].ToString();
                expenseBreakdownOverall.Cells[totalIndexExpense, 1].Text = secRow[LossAndGainsReportDS.CASH_BANKACC].ToString();

                if (secRow[LossAndGainsReportDS.PAYMENTDATE] is DBNull)
                    expenseBreakdownOverall.Cells[totalIndexExpense, 2].Text = "-";
                else
                    expenseBreakdownOverall.Cells[totalIndexExpense, 2].Text = ((DateTime)secRow[LossAndGainsReportDS.PAYMENTDATE]).ToString("dd/MM/yyyy");
                expenseBreakdownOverall.Cells[totalIndexExpense, 2].Style.TextAlignHorz = AlignHorzEnum.Center;

                expenseBreakdownOverall.Cells[totalIndexExpense, 3].Text = secRow[LossAndGainsReportDS.TRANSACTIONTYPE].ToString();
                expenseBreakdownOverall.Cells[totalIndexExpense, 3].Style.TextAlignHorz = AlignHorzEnum.Center;

                expenseBreakdownOverall.Cells[totalIndexExpense, 4].Text = secRow[LossAndGainsReportDS.SYSTEMTRANSACTIONTYPE].ToString();
                expenseBreakdownOverall.Cells[totalIndexExpense, 4].Style.TextAlignHorz = AlignHorzEnum.Center;

                expenseBreakdownOverall.Cells[totalIndexExpense, 5].Text = secRow[LossAndGainsReportDS.TRANCATEGORY].ToString();
                expenseBreakdownOverall.Cells[totalIndexExpense, 5].Style.TextAlignHorz = AlignHorzEnum.Center;

                expenseBreakdownOverall.Cells[totalIndexExpense, 6].Text = ((decimal)secRow[LossAndGainsReportDS.EXPENSE]).ToString("c");
                expenseBreakdownOverall.Cells[totalIndexExpense, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                expenseTotal += (decimal)secRow[LossAndGainsReportDS.EXPENSE];

                expenseBreakdownOverall.Cells[totalIndexExpense, 7].Text = ((decimal)secRow[LossAndGainsReportDS.GST]).ToString("c");
                expenseBreakdownOverall.Cells[totalIndexExpense, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                gstTotal += (decimal)secRow[LossAndGainsReportDS.GST];

                expenseBreakdownOverall.Cells[totalIndexExpense, 8].Text = ((decimal)secRow[LossAndGainsReportDS.TOTAL]).ToString("c");
                expenseBreakdownOverall.Cells[totalIndexExpense, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
                totolTotal += (decimal)secRow[LossAndGainsReportDS.TOTAL];

                expenseBreakdownOverall.Cells[totalIndexExpense, 9].Text = secRow[LossAndGainsReportDS.COMMENT].ToString();
                expenseBreakdownOverall.Cells[totalIndexExpense, 9].Style.TextAlignHorz = AlignHorzEnum.Center;

                totalIndexExpense++;
            }

            expenseBreakdownOverall.Rows[totalIndexExpense].Height = .2;
            expenseBreakdownOverall.Rows[totalIndexExpense].Style.FontBold = true;
            expenseBreakdownOverall.Rows[totalIndexExpense].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            expenseBreakdownOverall.Rows[totalIndexExpense].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            expenseBreakdownOverall.Rows[totalIndexExpense].Style.FontBold = true;
            expenseBreakdownOverall.Rows[totalIndexExpense].Style.TextAlignVert = AlignVertEnum.Center;

            expenseBreakdownOverall.Cells[totalIndexExpense, 0].Text = "TOTAL";

            expenseBreakdownOverall.Cells[totalIndexExpense, 6].Text = expenseTotal.ToString("C");
            expenseBreakdownOverall.Cells[totalIndexExpense, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
            expenseBreakdownOverall.Cells[totalIndexExpense, 7].Text = gstTotal.ToString("C");
            expenseBreakdownOverall.Cells[totalIndexExpense, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
            expenseBreakdownOverall.Cells[totalIndexExpense, 8].Text = totolTotal.ToString("C");
            expenseBreakdownOverall.Cells[totalIndexExpense, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

            #endregion

            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Style.FontSize = 7;

            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Acc Type";
            secSummaryTotalTable.Cells[0, 1].Text = "Account NO";

            secSummaryTotalTable.Cells[0, 2].Text = "Opening";
            secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 3].Text = "Investment";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 4].Text = "Tran. In/Out";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 5].Text = "Interest";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 6].Text = "Rental";
            secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 7].Text = "Distribution";
            secSummaryTotalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 8].Text = "Dividend";
            secSummaryTotalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 9].Text = "Expenses";
            secSummaryTotalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 10].Text = "Change in Val.";
            secSummaryTotalTable.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 11].Text = "Closing";
            secSummaryTotalTable.Cells[0, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 12].Text = "Unrealised";
            secSummaryTotalTable.Cells[0, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 13].Text = "Realised";
            secSummaryTotalTable.Cells[0, 13].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 4;
            secSummaryTotalTable.Cols[2].Width = 2.5;
            secSummaryTotalTable.Cols[3].Width = 2.5;
            secSummaryTotalTable.Cols[4].Width = 2.5;
            secSummaryTotalTable.Cols[5].Width = 2.5;
            secSummaryTotalTable.Cols[6].Width = 2.5;
            secSummaryTotalTable.Cols[7].Width = 2.5;
            secSummaryTotalTable.Cols[8].Width = 2.5;
            secSummaryTotalTable.Cols[9].Width = 2.5;
            secSummaryTotalTable.Cols[10].Width = 2.5;
            secSummaryTotalTable.Cols[11].Width = 2.5;
            secSummaryTotalTable.Cols[12].Width = 2.5;
            secSummaryTotalTable.Cols[13].Width = 2.5;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Income Summary\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 10;
            gridHeaderTotal.Style.FontBold = true;

            RenderText gridHeaderTotalExpense = new RenderText();
            gridHeaderTotalExpense.Text = "Expense Summary\n\n";
            gridHeaderTotalExpense.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotalExpense.Style.FontSize = 10;
            gridHeaderTotalExpense.Style.FontBold = true;

            RenderText gridHeaderIncomeTransactions = new RenderText();
            gridHeaderIncomeTransactions.Text = "Income Tranactions\n\n";
            gridHeaderIncomeTransactions.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderIncomeTransactions.Style.FontSize = 10;
            gridHeaderIncomeTransactions.Style.FontBold = true;

            RenderText gridHeaderExpenseTransactions = new RenderText();
            gridHeaderExpenseTransactions.Text = "Expense Transactions\n\n";
            gridHeaderExpenseTransactions.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderExpenseTransactions.Style.FontSize = 10;
            gridHeaderExpenseTransactions.Style.FontBold = true;

            RenderText gridHeaderTotal2 = new RenderText();
            gridHeaderTotal2.Text = "Unrealised / Realised Gains & Losses Summary\n\n";
            gridHeaderTotal2.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal2.Style.FontSize = 10;
            gridHeaderTotal2.Style.FontBold = true;

            decimal openingBalTotal = 0;
            decimal investTotal = 0;
            decimal transferInTotal = 0;
            decimal transferOutTotal = 0;
            decimal changeInMktValTotal = 0;
            decimal interestTotal = 0;
            decimal rentalTotal = 0;
            decimal disTotal = 0;
            decimal divTotal = 0;
            decimal expTotal = 0;
            decimal realisedTotal = 0;
            decimal unRealisedTotal = 0;
            decimal closingTotal = 0;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n";

            RenderText lineBreakTotal2 = new RenderText();
            lineBreakTotal2.Text = "\n\n\n";

            RenderText lineBreakTotal3 = new RenderText();
            lineBreakTotal3.Text = "\n\n";

            RenderText lineBreakTotal4 = new RenderText();
            lineBreakTotal4.Text = "\n\n";

            RenderText lineBreakTotal5 = new RenderText();
            lineBreakTotal5.Text = "\n\n";

            RenderText lineBreakTotal6 = new RenderText();
            lineBreakTotal6.Text = "\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSumTableIncomeSection);
            doc.Body.Children.Add(lineBreakTotal4);
            doc.Body.Children.Add(gridHeaderTotalExpense);
            doc.Body.Children.Add(secSumTableExpenseSection);
            doc.Body.Children.Add(lineBreakTotal3);
            doc.Body.Children.Add(gridHeaderIncomeTransactions);
            doc.Body.Children.Add(incomeBreakdownOverall);
            doc.Body.Children.Add(lineBreakTotal5);
            doc.Body.Children.Add(gridHeaderExpenseTransactions);
            doc.Body.Children.Add(expenseBreakdownOverall);
            doc.Body.Children.Add(lineBreakTotal6);
            doc.Body.Children.Add(lineBreakTotal);
            doc.Body.Children.Add(gridHeaderTotal2);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal2);
            int totalIndex = 1;

            foreach (DataRow secRow in overallSummary.Rows)
            {
                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[LossAndGainsReportDS.ACCOUNTTYPE].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[LossAndGainsReportDS.ACCOUNTNO].ToString();

                decimal totalValue = 0;

                if (secRow[LossAndGainsReportDS.OPENINGBALANCE] != null & secRow[LossAndGainsReportDS.OPENINGBALANCE] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.OPENINGBALANCE]);
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                openingBalTotal += totalValue;


                totalValue = 0;

                if (secRow[LossAndGainsReportDS.INVESTMENT] != null & secRow[LossAndGainsReportDS.INVESTMENT] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.INVESTMENT]);
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                investTotal += totalValue;

                totalValue = 0;
                decimal transgerIn = 0;
                decimal transferOut = 0;
                if (secRow[LossAndGainsReportDS.TRANSFERIN] != null & secRow[LossAndGainsReportDS.TRANSFERIN] != DBNull.Value)
                    transgerIn = Convert.ToDecimal(secRow[LossAndGainsReportDS.TRANSFERIN]);
                if (secRow[LossAndGainsReportDS.TRANSFEROUT] != null & secRow[LossAndGainsReportDS.TRANSFEROUT] != DBNull.Value)
                    transferOut = Convert.ToDecimal(secRow[LossAndGainsReportDS.TRANSFEROUT]);

                secSummaryTotalTable.Cells[totalIndex, 4].Text = (transgerIn + transferOut).ToString("C");
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                transferInTotal += (transgerIn + transferOut);

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.INTEREST] != null & secRow[LossAndGainsReportDS.INTEREST] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.INTEREST]);
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                interestTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.RENTAL] != null & secRow[LossAndGainsReportDS.RENTAL] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.RENTAL]);
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                rentalTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.DISTRIBUTIONADJ] != null & secRow[LossAndGainsReportDS.DISTRIBUTIONADJ] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.DISTRIBUTIONADJ]);
                    secSummaryTotalTable.Cells[totalIndex, 7].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 7].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                disTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.DIVIDEND] != null & secRow[LossAndGainsReportDS.DIVIDEND] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.DIVIDEND]);
                    secSummaryTotalTable.Cells[totalIndex, 8].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 8].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                divTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.EXPENSES] != null & secRow[LossAndGainsReportDS.EXPENSES] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.EXPENSES]);
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                expTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.CLOSINGADJBALANCE] != null & secRow[LossAndGainsReportDS.CLOSINGADJBALANCE] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.CLOSINGADJBALANCE]);
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

                closingTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.CHANGEINMKT] != null & secRow[LossAndGainsReportDS.CHANGEINMKT] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.CHANGEINMKT]);
                    secSummaryTotalTable.Cells[totalIndex, 10].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 10].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                changeInMktValTotal += totalValue;


                totalValue = 0;

                if (secRow[LossAndGainsReportDS.UNREALISED] != null & secRow[LossAndGainsReportDS.UNREALISED] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISED]);
                    secSummaryTotalTable.Cells[totalIndex, 12].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 12].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

                unRealisedTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.REALISED] != null & secRow[LossAndGainsReportDS.REALISED] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISED]);
                    secSummaryTotalTable.Cells[totalIndex, 13].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 13].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 13].Style.TextAlignHorz = AlignHorzEnum.Right;

                realisedTotal += totalValue;


                totalIndex++;
            }

            var groupedExpenses = expenseBreakDown.Select().GroupBy(row => row[LossAndGainsReportDS.SYSTEMTRANSACTIONTYPE]);
            int expenseItemCount = 1;
            decimal expenseSummaryTotal = 0;

            foreach (var expenseItem in groupedExpenses)
            {
                secSumTableExpenseSection.Cells[expenseItemCount, 0].Text = expenseItem.Key.ToString();
                decimal expenseItemTotal = expenseItem.Sum(row => (decimal)row[LossAndGainsReportDS.TOTAL]);
                secSumTableExpenseSection.Cells[expenseItemCount, 1].Text = expenseItemTotal.ToString("C");
                secSumTableExpenseSection.Cells[expenseItemCount, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

                expenseSummaryTotal += expenseItemTotal;
                expenseItemCount++;
            }

            secSumTableExpenseSection.Rows[expenseItemCount].Height = .2;
            secSumTableExpenseSection.Rows[expenseItemCount].Style.FontBold = true;
            secSumTableExpenseSection.Rows[expenseItemCount].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSumTableExpenseSection.Rows[expenseItemCount].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSumTableExpenseSection.Rows[expenseItemCount].Style.FontBold = true;
            secSumTableExpenseSection.Rows[expenseItemCount].Style.TextAlignVert = AlignVertEnum.Center;

            secSumTableExpenseSection.Cells[expenseItemCount, 0].Text = "TOTAL EXPENSE";
            secSumTableExpenseSection.Cells[expenseItemCount, 1].Text = expenseSummaryTotal.ToString("C");
            secSumTableExpenseSection.Cells[expenseItemCount, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSumTableIncomeSection.Cells[1, 0].Text = "Interest";
            secSumTableIncomeSection.Cells[1, 1].Text = interestTotal.ToString("C");
            secSumTableIncomeSection.Cells[1, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSumTableIncomeSection.Cells[2, 0].Text = "Dividend";
            secSumTableIncomeSection.Cells[2, 1].Text = divTotal.ToString("C");
            secSumTableIncomeSection.Cells[2, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            decimal totalDistribution = bankAccountIncomes.Select().Where(row => row[LossAndGainsReportDS.SYSTRANSACIONTYPE].ToString() == "Distribution").Sum(row => Math.Round(decimal.Parse(row[LossAndGainsReportDS.TOTAL].ToString()), 2));

            secSumTableIncomeSection.Cells[3, 0].Text = "Distribution";
            secSumTableIncomeSection.Cells[3, 1].Text = totalDistribution.ToString("C");
            secSumTableIncomeSection.Cells[3, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSumTableIncomeSection.Cells[4, 0].Text = "Rental";
            secSumTableIncomeSection.Cells[4, 1].Text = rentalTotal.ToString("C");
            secSumTableIncomeSection.Cells[4, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSumTableIncomeSection.Cells[5, 0].Text = "Realised Gains (Securities & Funds)";
            secSumTableIncomeSection.Cells[5, 1].Text = realisedTotal.ToString("C");
            secSumTableIncomeSection.Cells[5, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSumTableIncomeSection.Rows[6].Height = .2;
            secSumTableIncomeSection.Rows[6].Style.FontBold = true;
            secSumTableIncomeSection.Rows[6].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSumTableIncomeSection.Rows[6].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSumTableIncomeSection.Rows[6].Style.FontBold = true;
            secSumTableIncomeSection.Rows[6].Style.TextAlignVert = AlignVertEnum.Center;

            secSumTableIncomeSection.Cells[6, 0].Text = "TOTAL INCOME";
            secSumTableIncomeSection.Cells[6, 1].Text = (realisedTotal + totalDistribution + divTotal + interestTotal + rentalTotal).ToString("C");
            secSumTableIncomeSection.Cells[6, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            secSummaryTotalTable.Cells[totalIndex, 1].Text = "";

            secSummaryTotalTable.Cells[totalIndex, 2].Text = openingBalTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 3].Text = investTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 4].Text = (transferInTotal + transferOutTotal).ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 5].Text = interestTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 6].Text = rentalTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 7].Text = disTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 8].Text = divTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 9].Text = expTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 10].Text = changeInMktValTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 11].Text = closingTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 12].Text = unRealisedTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 12].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 13].Text = realisedTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 13].Style.TextAlignHorz = AlignHorzEnum.Right;
        }
        private static RenderTable SetIncomeReconSummary(IEnumerable<DataRow> incomeBreakDownRows)
        {
            RenderTable incomeBreakdownOverall = new RenderTable();
            incomeBreakdownOverall.Style.FontSize = 6;
            incomeBreakdownOverall.RowGroups[0, 1].PageHeader = true;
            incomeBreakdownOverall.Rows[0].Height = .2;
            incomeBreakdownOverall.Rows[0].Style.FontBold = true;
            incomeBreakdownOverall.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            incomeBreakdownOverall.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            incomeBreakdownOverall.Rows[0].Style.FontBold = true;
            incomeBreakdownOverall.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            incomeBreakdownOverall.Cells[0, 0].Text = "Desc.";
            incomeBreakdownOverall.Cells[0, 1].Text = "Sec. Code";
            incomeBreakdownOverall.Cells[0, 2].Text = "Inc. Type";

            incomeBreakdownOverall.Cells[0, 3].Text = "Record Date";
            incomeBreakdownOverall.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Center;

            incomeBreakdownOverall.Cells[0, 4].Text = "Payment Date";
            incomeBreakdownOverall.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Center;
            incomeBreakdownOverall.Cells[0, 5].Text = "Source";
            incomeBreakdownOverall.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Center;

            incomeBreakdownOverall.Cells[0, 6].Text = "Assc. Acc.";
            incomeBreakdownOverall.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Center;

            incomeBreakdownOverall.Cells[0, 7].Text = "Trans. Date";
            incomeBreakdownOverall.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Center;

            incomeBreakdownOverall.Cells[0, 8].Text = "Net DPU";
            incomeBreakdownOverall.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

            incomeBreakdownOverall.Cells[0, 9].Text = "Units on Hand";
            incomeBreakdownOverall.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

            incomeBreakdownOverall.Cells[0, 10].Text = "Franking Credit";
            incomeBreakdownOverall.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

            incomeBreakdownOverall.Cells[0, 11].Text = "Income Paid";
            incomeBreakdownOverall.Cells[0, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

            incomeBreakdownOverall.Cols[0].Width = 7.5;
            incomeBreakdownOverall.Cols[1].Width = 2.2;
            incomeBreakdownOverall.Cols[2].Width = 2.2;
            incomeBreakdownOverall.Cols[3].Width = 2.2;
            incomeBreakdownOverall.Cols[4].Width = 2.2;
            incomeBreakdownOverall.Cols[5].Width = 2;
            incomeBreakdownOverall.Cols[6].Width = 2;
            incomeBreakdownOverall.Cols[7].Width = 2.2;
            incomeBreakdownOverall.Cols[8].Width = 2.2;
            incomeBreakdownOverall.Cols[9].Width = 2.2;
            incomeBreakdownOverall.Cols[10].Width = 2.2;
            incomeBreakdownOverall.Cols[11].Width = 2.5;

            int totalIndexIncome = 1;
            decimal incomeTotal = 0;
            foreach (DataRow secRow in incomeBreakDownRows)
            {
                incomeBreakdownOverall.Cells[totalIndexIncome, 0].Text = secRow[LossAndGainsReportDS.DESCRIPTION].ToString();
                incomeBreakdownOverall.Cells[totalIndexIncome, 1].Text = secRow[LossAndGainsReportDS.SECNAME].ToString();

                incomeBreakdownOverall.Cells[totalIndexIncome, 2].Text = secRow[LossAndGainsReportDS.INCOMETYPE].ToString();

                if (secRow[LossAndGainsReportDS.RECORDDATE] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 3].Text = "-";
                else
                    incomeBreakdownOverall.Cells[totalIndexIncome, 3].Text = ((DateTime)secRow[LossAndGainsReportDS.RECORDDATE]).ToString("dd/MM/yyyy");
                incomeBreakdownOverall.Cells[totalIndexIncome, 3].Style.TextAlignHorz = AlignHorzEnum.Center;

                if (secRow[LossAndGainsReportDS.PAYMENTDATE] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 4].Text = "-";
                else
                    incomeBreakdownOverall.Cells[totalIndexIncome, 4].Text = ((DateTime)secRow[LossAndGainsReportDS.PAYMENTDATE]).ToString("dd/MM/yyyy");
                incomeBreakdownOverall.Cells[totalIndexIncome, 4].Style.TextAlignHorz = AlignHorzEnum.Center;

                incomeBreakdownOverall.Cells[totalIndexIncome, 5].Text = secRow[LossAndGainsReportDS.SOURCE].ToString();
                incomeBreakdownOverall.Cells[totalIndexIncome, 5].Style.TextAlignHorz = AlignHorzEnum.Center;

                incomeBreakdownOverall.Cells[totalIndexIncome, 6].Text = secRow[LossAndGainsReportDS.CASH_BANKACC].ToString();

                if (secRow[LossAndGainsReportDS.CASH_PAYMENTDATE] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 7].Text = "-";
                else
                    incomeBreakdownOverall.Cells[totalIndexIncome, 7].Text = ((DateTime)secRow[LossAndGainsReportDS.CASH_PAYMENTDATE]).ToString("dd/MM/yyyy");
                incomeBreakdownOverall.Cells[totalIndexIncome, 7].Style.TextAlignHorz = AlignHorzEnum.Center;

                if (secRow[LossAndGainsReportDS.NETDPU] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 8].Text = "-";
                else
                    incomeBreakdownOverall.Cells[totalIndexIncome, 8].Text = ((decimal)secRow[LossAndGainsReportDS.NETDPU]).ToString("N6");
                incomeBreakdownOverall.Cells[totalIndexIncome, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.UNITSHELD] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 9].Text = "-";
                else
                    incomeBreakdownOverall.Cells[totalIndexIncome, 9].Text = ((decimal)secRow[LossAndGainsReportDS.UNITSHELD]).ToString("N4");
                incomeBreakdownOverall.Cells[totalIndexIncome, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.FRANKINGCREDIT] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 10].Text = "-";
                else
                    incomeBreakdownOverall.Cells[totalIndexIncome, 10].Text = ((decimal)secRow[LossAndGainsReportDS.FRANKINGCREDIT]).ToString("C");
                incomeBreakdownOverall.Cells[totalIndexIncome, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.INCPAID] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 11].Text = "-";
                else
                {
                    incomeBreakdownOverall.Cells[totalIndexIncome, 11].Text = ((decimal)secRow[LossAndGainsReportDS.INCPAID]).ToString("C");
                    incomeTotal += (decimal)secRow[LossAndGainsReportDS.INCPAID];
                }
                incomeBreakdownOverall.Cells[totalIndexIncome, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

                totalIndexIncome++;
            }

            incomeBreakdownOverall.Rows[totalIndexIncome].Height = .2;
            incomeBreakdownOverall.Rows[totalIndexIncome].Style.FontBold = true;
            incomeBreakdownOverall.Rows[totalIndexIncome].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            incomeBreakdownOverall.Rows[totalIndexIncome].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            incomeBreakdownOverall.Rows[totalIndexIncome].Style.FontBold = true;
            incomeBreakdownOverall.Rows[totalIndexIncome].Style.TextAlignVert = AlignVertEnum.Center;

            incomeBreakdownOverall.Cells[totalIndexIncome, 0].Text = "TOTAL";
            incomeBreakdownOverall.Cells[totalIndexIncome, 11].Text = incomeTotal.ToString("C");
            incomeBreakdownOverall.Cells[totalIndexIncome, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
            return incomeBreakdownOverall;
        }

        private void GetReportData()
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            IOrganization orgData = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            this.assets = orgData.Assets;
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);

            BaseClientDetails baseClientDetails = new Oritax.TaxSimp.DataSets.BaseClientDetails();
            baseClientDetails.UseAddressEntity = true;
            baseClientDetails.AddressKey = LBLAddressKey.Text;

            LossAndGainsReportDS lossAndGainsReportDS = new LossAndGainsReportDS();
            ClientTaxDistributionsDS clientTaxDistributionsDS = new ClientTaxDistributionsDS();
            HoldingSummaryReportDS holdingSummaryReportDS = new HoldingSummaryReportDS();

            if (comboBoxAccType.SelectedValue == "Accrual")
                lossAndGainsReportDS.AcountingMethod = AcountingMethodType.Accrual;
            else if (comboBoxAccType.SelectedValue == "Mixed")
                lossAndGainsReportDS.AcountingMethod = AcountingMethodType.Mixed;
            else
                lossAndGainsReportDS.AcountingMethod = AcountingMethodType.Cash;

            if (this.InputStartDate.DateInput.SelectedDate.HasValue)
            {
                lossAndGainsReportDS.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(this.InputStartDate.DateInput.SelectedDate.Value);
                holdingSummaryReportDS.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(this.InputStartDate.DateInput.SelectedDate.Value);
            }

            if (this.InputEndDate.DateInput.SelectedDate.HasValue)
            {
                lossAndGainsReportDS.EndDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(this.InputEndDate.DateInput.SelectedDate.Value);
                holdingSummaryReportDS.EndDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(this.InputEndDate.DateInput.SelectedDate.Value);
            }

            clientData.GetData(lossAndGainsReportDS);
            clientData.GetData(clientTaxDistributionsDS);
            clientData.GetData(holdingSummaryReportDS);
            clientData.GetData(baseClientDetails);

            dsList.Clear();

            dsList.Add("HoldingSummaryReportDS", holdingSummaryReportDS);
            dsList.Add("ClientTaxDistributionsDS", clientTaxDistributionsDS);
            dsList.Add("LossAndGainsReportDS", lossAndGainsReportDS);
            dsList.Add("BaseClientDetails", baseClientDetails);

            AddressList.DataSource = baseClientDetails.AddressList;
            AddressList.DataValueField = "Key";
            AddressList.DataTextField = "Value";
            AddressList.DataBind();

            if (this.LBLAddressKey.Text != string.Empty)
            {
                var selectedItem = AddressList.Items.Where(item => item.Value == LBLAddressKey.Text).FirstOrDefault();
                selectedItem.Selected = true;
            }

            comboBoxCostType.Text = lossAndGainsReportDS.CostTypes.ToString();
            InputEndDate.DbSelectedDate = lossAndGainsReportDS.EndDate;
            InputStartDate.DbSelectedDate = lossAndGainsReportDS.StartDate;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            this.UMABroker.ReleaseBrokerManagedComponent(orgData);
        }

        protected void RadComboBox1_DataBound(object sender, EventArgs e)
        {
        }

        protected void RadComboBox1_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
        }

        protected void Button1_Click(object sender, System.EventArgs e)
        {
        }

        protected void RadComboBox1_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
        }

        protected void GenerateReport(object sender, EventArgs e)
        {
            ReportClick();
        }

        protected void ReportClick()
        {
            if (AddressList.SelectedItem != null)
                LBLAddressKey.Text = AddressList.SelectedItem.Value;

            GetReportData();

            string reportName = "Report_" + Guid.NewGuid().ToString();
            C1ReportViewer.RegisterDocument(reportName, MakeDoc);
            this.C1ReportViewerAssetClass.FileName = reportName;
            C1ReportViewerAssetClass.ReportName = reportName;
        }
    }
}
