﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.C1ComboBox;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Telerik.Web.UI;
using Oritax.TaxSimp.Utilities;

namespace eclipseonlineweb
{
    public struct BankTransaction
    {
        public string ID { get; set; }
        public DateTime TransactionDate { get; set; }
        public string SystemTransactionType { get; set; }
        /// <summary>
        /// Withdrawal or Deposit
        /// </summary>
        public string ImportTransactionType { get; set; }
        public string ExternalReferenceID { get; set; }

        public string Category { get; set; }
        public double Amount { get; set; }
        public double Balance { get; set; }
        public string AccountNo { get; set; }
        public string Comment { get; set; }
        public string ServiceType { get; set; }
        public int Rank { get; set; }
    }

    public class BankStatement
    {
        private SortedList<string, List<BankTransaction>> ACCOUNT_WISE_TRANS = new SortedList<string, List<BankTransaction>>();

        public BankStatement(DataRowCollection data)
        {
            foreach (DataRow row in data)
            {
                AddTrans(new BankTransaction
                             {
                                 //ID = row["ID"].ToString(),
                                 Amount = Convert.ToDouble(row["Amount"].ToString()),
                                 AccountNo = row["AccountNo"].ToString(),
                                 Comment = row["Comment"].ToString(),
                                 ImportTransactionType = row["ImportTransactionType"].ToString(),
                                 ServiceType = row["ServiceType"].ToString(),
                                 SystemTransactionType = row["SystemTransactionType"].ToString(),
                                 ExternalReferenceID = row["ExternalReferenceID"].ToString(),
                                 Category = row["Category"].ToString(),
                                 TransactionDate = DateTime.Parse(row["BankTransactionDate"].ToString()),
                                 Balance = Convert.ToDouble(row["Balance"].ToString()),
                                 Rank =  int.Parse(row["Rank"].ToString())

                             });
            }
        }

        public IList<string> AccountNoList
        {
            get { return ACCOUNT_WISE_TRANS.Keys; }
        }

        private void AddTrans(BankTransaction trans)
        {
            if (!ACCOUNT_WISE_TRANS.ContainsKey(trans.AccountNo))
                ACCOUNT_WISE_TRANS[trans.AccountNo] = new List<BankTransaction>();

            ACCOUNT_WISE_TRANS[trans.AccountNo].Add(trans);
        }

        public List<BankTransaction> GetAccountTransactions(string accountNo)
        {
            return ACCOUNT_WISE_TRANS[accountNo].OrderBy(tr => tr.TransactionDate).ToList();
        }
    }

    public partial class BankStatementReport : UMABasePage
    {
        private bool SinceInception = false; 

        private void FillAccountCombo()
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            BankTransactionDS ds = new BankTransactionDS();

            if (InputStartDate.DateInput.SelectedDate.HasValue)
                ds.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(InputStartDate.DateInput.SelectedDate.Value);
            if (InputEndDate.DateInput.SelectedDate.HasValue)
                ds.EndDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(InputEndDate.DateInput.SelectedDate.Value);

            clientData.GetData(ds);

            foreach (DataRow row in ds.Tables[BankTransactionDS.BANKACCOUNTLIST].Rows)
            {
                var lst = new RadComboBoxItem
                {
                    Text = row[BankTransactionDS.BANKACCOUNTNO] + " - " + row[BankTransactionDS.BANKACCOUNTNAME],
                    Value = row[BankTransactionDS.BANKACCOUNTNO].ToString()
                };
                comboAccounts.Items.Add(lst);
            }
            comboAccounts.SelectedIndex = 0;
        }

        public override void PopulatePage(DataSet ds)
        {
            this.cid = Request.QueryString["ins"];
            base.PopulatePage(ds);

            if (!Page.IsPostBack)
                FillAccountCombo();
            
            BankStmtReportClick();
        }

        private DataSet FilterDataSet(DataSet ds)
        {
            var newDS = new DataSet();
             
            if (comboAccounts.SelectedItem != null)
            {
                var strExpr = string.Format("{0}={1}", BankTransactionDS.BANKACCOUNTNO, comboAccounts.SelectedItem.Value);

                var dv = ds.Tables["Cash Transactions"].DefaultView;
                dv.RowFilter = strExpr;
                var newDT = dv.ToTable();
                newDS.Tables.Add(ds.Tables[0].Copy());
                newDS.Tables.Add(ds.Tables[1].Copy());
                newDS.Tables.Add(ds.Tables[2].Copy());
                newDS.Tables.Add(newDT);
             
            }

            return newDS;
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public C1PrintDocument MakeDoc()
        {
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            DataRow clientSummaryRow = this.PresentationData.Tables[0].Rows[0];

            string startDate = string.Empty; 
            string endDate = string.Empty;

            if (SinceInception)
                startDate = "Since Inception";
            else
                startDate = this.InputStartDate.DateInput.SelectedDate.Value.ToString("dd/MM/yyyy");
            
            if(!this.InputEndDate.DateInput.SelectedDate.HasValue)
                endDate = DateTime.Now.ToString("dd/MM/yyyy");
            else
                endDate = this.InputEndDate.DateInput.SelectedDate.Value.ToString("dd/MM/yyyy");

            string dateInfo = startDate + " - " + endDate;
            AddClientHeaderToReport(doc, clientSummaryRow, dateInfo, "BANK STATEMENT REPORT");//NB. Can't get DateInfo like bellow
            var manRows = this.PresentationData.Tables["Cash Transactions"].Rows;
            SetBankStmtGrid(doc, manRows);
           
            return doc;
        }

        public void SetBankStmtGrid(C1PrintDocument doc, DataRowCollection difmRows)
        {
            int index = 1;

            if (difmRows.Count > 0)
            {
                //Convert data to meaningful business objects
                BankStatement stm = new BankStatement(difmRows);
              
                //Get and loop thru all acc/nos for this client
                IList<string> AccountNos = stm.AccountNoList;
                foreach (string AccountNo in AccountNos)
                {
                    RenderTable bankStmtTable = new RenderTable();

                    bankStmtTable.Style.FontSize = 8;
                    bankStmtTable.Rows[0].SplitBehavior = SplitBehaviorEnum.SplitNewPage;
                    bankStmtTable.Rows[0].Height = .2;
                    bankStmtTable.Rows[0].Style.FontBold = true;
                    bankStmtTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                    bankStmtTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                    bankStmtTable.Rows[0].Style.FontBold = true;
                    bankStmtTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;
                    
                    bankStmtTable.RowGroups[0, 1].PageHeader = true; 

                    bankStmtTable.Cells[0, 0].Text = "Date";
                    bankStmtTable.Cells[0, 1].Text = "Description";
                    bankStmtTable.Cells[0, 2].Text = "Category";
                    bankStmtTable.Cells[0, 3].Text = "System Transaction";
                    bankStmtTable.Cells[0, 4].Text = "Withdrawals ($)";
                    bankStmtTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                    bankStmtTable.Cells[0, 5].Text = "Deposits ($)";
                    bankStmtTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                    bankStmtTable.Cells[0, 6].Text = "Balance ($)";
                    bankStmtTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                    bankStmtTable.Cols[0].Width = 1.5;
                    bankStmtTable.Cols[1].Width = 4;
                    bankStmtTable.Cols[2].Width = 2;
                    bankStmtTable.Cols[3].Width = 3.5;
                    bankStmtTable.Cols[4].Width = 1.5;
                    bankStmtTable.Cols[5].Width = 1.5;
                    bankStmtTable.Cols[6].Width = 2;

                    List<BankTransaction> trans = stm.GetAccountTransactions(AccountNo);
                    
                    if (trans.Count > 0)
                    {
                        var firstTransaction = trans.OrderBy(tran => tran.Rank).FirstOrDefault(); 

                        BankTransaction transactionOpening = new BankTransaction();
                        transactionOpening.TransactionDate = firstTransaction.TransactionDate;
                        transactionOpening.Comment = "Opening Balance";
                        transactionOpening.Balance = firstTransaction.Balance - firstTransaction.Amount;

                        trans.Insert(0, transactionOpening);

                        var lastTransaction = trans.OrderByDescending(tran => tran.Rank).FirstOrDefault();

                        BankTransaction transactionClosing = new BankTransaction();
                        transactionClosing.TransactionDate = lastTransaction.TransactionDate;
                        transactionClosing.Comment = "Closing Balance";
                        transactionClosing.Balance = lastTransaction.Balance;

                        trans.Insert(trans.Count, transactionClosing); 
                    }

                    foreach (var bankTransaction in trans)
                    {
                        if (bankTransaction.Comment == "Closing Balance" || bankTransaction.Comment == "Opening Balance")
                        {
                            bankStmtTable.Cells[index, 1].Text = bankTransaction.Comment;
                            bankStmtTable.Cells[index, 1].Style.FontBold = true;
                            bankStmtTable.Rows[index].Height = .3;
                            bankStmtTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                            bankStmtTable.Cells[index, 6].Text = bankTransaction.Balance.ToString("C");
                            bankStmtTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                            bankStmtTable.Cells[index, 6].Style.FontBold = true; 
                        }
                        else
                        {
                            bankStmtTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                            bankStmtTable.Rows[index].Height = .3;
                            bankStmtTable.Cells[index, 0].Text = bankTransaction.TransactionDate.ToString("dd/MM/yyyy");
                            bankStmtTable.Cells[index, 1].Text = bankTransaction.Comment;

                            bankStmtTable.Cells[index, 2].Text = bankTransaction.Category;
                            bankStmtTable.Cells[index, 3].Text = bankTransaction.SystemTransactionType;

                            bankStmtTable.Cells[index, 4].Text = bankTransaction.ImportTransactionType == "Withdrawal" ? bankTransaction.Amount.ToString("C") : "";
                            bankStmtTable.Cells[index, 5].Text = bankTransaction.ImportTransactionType == "Withdrawal" ? "" : bankTransaction.Amount.ToString("C");


                            bankStmtTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                            bankStmtTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                            bankStmtTable.Cells[index, 6].Text = bankTransaction.Balance.ToString("C");
                            bankStmtTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                        }
                        index++;
                    }

                    RenderText gridHeader = new RenderText();
                    gridHeader.Text = "Account No - " + AccountNo + "\n\n";
                    gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                    gridHeader.Style.FontSize = 11;
                    gridHeader.Style.FontBold = true;

                    RenderText lineBreak = new RenderText();
                    lineBreak.Text = "\n\n\n";

                    doc.Body.Children.Add(gridHeader);
                    doc.Body.Children.Add(bankStmtTable);
                    doc.Body.Children.Add(lineBreak);
                }

            }

        }

        protected void GenerateReport(object sender, EventArgs e)
        {
            BankStmtReportClick();
        }

        protected void BankStmtReportClick()
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            BankTransactionDS ds = new BankTransactionDS();

            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);

            if (InputStartDate.DateInput.SelectedDate.HasValue)
                ds.StartDate = InputStartDate.DateInput.SelectedDate.Value;
            else
            {
                SinceInception = true;
                ds.IsSinceInception = true;
            }
            if (InputEndDate.DateInput.SelectedDate.HasValue)
                ds.EndDate = InputEndDate.DateInput.SelectedDate.Value;

            clientData.GetData(ds);
            this.PresentationData = FilterDataSet(ds);
            InputEndDate.DbSelectedDate = ds.EndDate;
            InputStartDate.DbSelectedDate = ds.StartDate; 
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            string reportName = "Report_" + Guid.NewGuid().ToString();
            if (this.PresentationData.Tables[0].Rows.Count > 0)
            {

                C1ReportViewer.RegisterDocument(reportName, MakeDoc);
                C1ReportViewer1.FileName = reportName;
                C1ReportViewer1.ReportName = reportName;
            }
        }
    }
}
