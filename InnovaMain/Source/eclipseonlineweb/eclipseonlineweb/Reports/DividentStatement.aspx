﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="ReportsMaster.master"
    AutoEventWireup="true" CodeBehind="DividentStatement.aspx.cs" Inherits="eclipseonlineweb.DividentStatement" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer"
    TagPrefix="C1ReportViewer" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <asp:Panel runat="server" ID="pnlDIV">
                <c1:C1GridView AlternatingRowStyle-Font-Size="X-Small" FilterStyle-Font-Size="Smaller"
                    AllowSorting="true" OnSorting="SortDIV" RowStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small"
                    ID="DivTranGrid" runat="server" AutogenerateColumns="false" ShowFilter="true"
                    OnFiltering="FilterDIV" Width="100%" ShowFooter="true" ScrollMode="None">
                    <Columns>
                        <c1:C1BoundField DataField="ID" HeaderText="ID" Visible="false" />
                        <c1:C1BoundField DataField="InvestmentCode" Visible="false" SortExpression="InvestmentCode"
                            HeaderText="">
                            <ItemStyle HorizontalAlign="Center" />
                            <GroupInfo OutlineMode="StartExpanded" Position="Header" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="RecordDate" SortExpression="RecordDate" DataFormatString="dd/MM/yyyy"
                            HeaderText="Record Date">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="PaymentDate" SortExpression="PaymentDate" DataFormatString="dd/MM/yyyy"
                            HeaderText="Payment Date">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="UnitsOnHand" SortExpression="UnitsOnHand" HeaderText="Units">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="CentsPerShare" SortExpression="CentsPerShare" DataFormatString="N4"
                            HeaderText="Cents Per Share">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="FrankingCredits" SortExpression="FrankingCredits" DataFormatString="N4"
                            HeaderText="Franking Credits (CPS)">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="TotalFrankingCredits" SortExpression="TotalFrankingCredits"
                            Aggregate="Sum" DataFormatString="C" HeaderText="Total Franking Credits ($)">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="PaidDividend" SortExpression="PaidDividend" Aggregate="Sum"
                            DataFormatString="C" HeaderText="Paid Dividend">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1ButtonField Width="10%" Text="Report" ButtonType="Link" CommandName="Details">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1ButtonField>
                    </Columns>
                </c1:C1GridView>
            </asp:Panel>
            <div>
                <C1ReportViewer:C1ReportViewer CollapseToolsPanel="true" Visible="false" Cache-Enabled="false"
                    Cache-ShareBetweenSessions="false" FileName="InMemoryBasicTable" runat="server"
                    ID="C1ReportViewerDividentStatement" Height="550px" Width="100%" Zoom="75%">
                </C1ReportViewer:C1ReportViewer>
            </div>
            <asp:Label runat="server" ID="lblDivID" Text="" Visible="true"></asp:Label>
            <asp:Label runat="server" ID="lblRecordDate" Text="" Visible="true"></asp:Label>
            <asp:Label runat="server" ID="lblUnits" Text="" Visible="true"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
