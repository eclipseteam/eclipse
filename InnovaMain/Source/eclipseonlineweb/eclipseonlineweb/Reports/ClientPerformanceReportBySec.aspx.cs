﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Utilities;
using Telerik.Web.UI;

namespace eclipseonlineweb
{
    public partial class ClientPerformanceReportBySec : UMABasePage
    {

        public override void LoadPage()
        {
            this.cid = Request.QueryString["ins"].ToString();

            if (!this.IsPostBack)
                this.PopulatePage(this.PresentationData);

            ScriptManager sm = ScriptManager.GetCurrent(Page);

            
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        protected void quickReports_ItemClick(object sender, RadMenuEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "1M":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetOneMonthDate();
                        ClientPerformanceReportClick(ds);
                        break;
                    }
                case "3M":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetThreeMonthDate();
                        ClientPerformanceReportClick(ds);
                        break;
                    }
                case "6M":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetSixMonthDate();
                        ClientPerformanceReportClick(ds);
                        break;
                    }
                case "12M":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetTwelveMonthDate();
                        ClientPerformanceReportClick(ds);
                        break;
                    }
                case "LFY":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetLastFinancialYear();
                        ClientPerformanceReportClick(ds);
                        break;
                    }
                case "CFY":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetCurrentFinancialYear();
                        ClientPerformanceReportClick(ds);
                        break;
                    }
            }
        }

        public override void PopulatePage(DataSet ds)
        {
            base.PopulatePage(ds);
            ClientPerformanceReportClick();
        }

        public static void AddFooter(C1PrintDocument doc)
        {
            RenderTable footer = new RenderTable();
            footer.BreakBefore = BreakEnum.Page;
            footer.Rows[0].Height = .30;
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;
            footer.Rows[1].Height = 2.5;
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.TextAlignVert = AlignVertEnum.Center;

            footer.Cells[0, 0].Text = "FOR YOUR INFORMATION";
            footer.Cells[0, 0].Style.TextAlignHorz = AlignHorzEnum.Center;
            footer.Cells[0, 0].Style.FontSize = 12;

            footer.Cells[1, 0].Text = "\n1. The “Movement” column includes all cash movement in and out of your portfolio.  The capital movement summary report on the portal will provide a breakdown of this amount between income, expenses, transfer in and out’s etc." +
                                        "\n\n2. Portfolio performance is calculated using the Modified Dietz Method and are net of fees and charges shown in the Capital Flows table.  Under this method of calculation individual cash flows over the period are weighted according to when they occur to provide an approximation of the actual return.  When returns are volatile or cash flows are large in relation to the size of the portfolio, the accuracy of this approximation will be reduced.\n\nIncome transactions such as interest, dividends, distributions and rent are not included in the cash flows as they are included in the change of investment value to determine the return on your portfolio.\n\nPast performance is not a reliable indicator of future performance." +
                                        "\n\n3. Returns shown in this reports are not annualised. The performance numbers are therefore the return for the period the report has been run." +
                                        "\n\n4. This report is provided by e-Clipse Online Pty Limited ABN 70 145 358 630, AFSL 357 306 (“e-Clipse”) and is based on information provided to e-Clipse by third parties.  Whilst every reasonable effort has been made by e-Clipse to ensure its accuracy, neither e-Clipse nor any of its related entities guarantee its accuracy nor accept any liability for any errors or omissions." +
                                        "\n\ne-Clipse Online Pty Ltd (ABN 70 145 358 630)\n3/36 Bydown Street, Neutral Bay, NSW 2089\nhttp://www.e-clipse.com.au\nP: +61 2 9346 4686";
            footer.Cells[1, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            footer.Cells[1, 0].Style.TextAlignVert = AlignVertEnum.Top;
            footer.Cells[1, 0].Style.FontSize = 9;
            footer.Cells[1, 0].Style.FontBold = false;

            doc.PageLayouts.LastPage = new PageLayout();
            doc.PageLayouts.LastPage.Document.Body.Children.Add(footer);
        }

        public C1PrintDocument MakeDocPerformance()
        {
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            DataRow clientSummaryRow = this.PresentationData.Tables[CapitalReportDS.CLIENTSUMMARYTABLE].Rows[0];

            ((CapitalReportDS)this.PresentationData).StartDate = WebUtilities.Utilities.FirstDayOfMonthFromDateTime(((CapitalReportDS)this.PresentationData).StartDate);
            ((CapitalReportDS)this.PresentationData).EndDate = WebUtilities.Utilities.LastDayOfMonthFromDateTime(((CapitalReportDS)this.PresentationData).EndDate);

            AddClientHeaderToReport(doc, clientSummaryRow, ((CapitalReportDS)this.PresentationData).DateInfo(), "SECURITY PERFORMANCE SUMMARY");

            DataTable capitalSummaryTable = this.PresentationData.Tables[CapitalReportDS.CAPITALFLOWSUMMARY];
            DataTable performSummaryTable = this.PresentationData.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC];
            DataTable capitalSummaryCatTable = this.PresentationData.Tables[CapitalReportDS.CAPITALFLOWSUMMARYCAT];
            DataTable bankTranTable = this.PresentationData.Tables[CapitalReportDS.BANKTRANSACTIONSTABLE];
            DataTable asxTranTable = this.PresentationData.Tables[CapitalReportDS.ASXTRANSTABLE];

            var capitalSummaryCatRows = capitalSummaryCatTable.Select().OrderByDescending(rows => rows[CapitalReportDS.MONTH]);
            var bankTranTableRows = bankTranTable.Select().OrderByDescending(rows => rows[CapitalReportDS.BANKTRANSDATE]);
            var asxTranTableRows = asxTranTable.Select().OrderByDescending(rows => rows[CapitalReportDS.TRADEDATE]);

            var difmRows = capitalSummaryTable.Select("ServiceType = 'Do It For Me'").OrderBy(rows => rows[CapitalReportDS.ASSETNAME]);
            var diyRows = capitalSummaryTable.Select("ServiceType = 'Do It Yourself'").OrderBy(rows => rows[CapitalReportDS.ASSETNAME]);
            var diwmRows = capitalSummaryTable.Select("ServiceType = 'Do It With Me'").OrderBy(rows => rows[CapitalReportDS.ASSETNAME]);
            var manRows = capitalSummaryTable.Select("ServiceType = 'Manual Asset'").OrderBy(rows => rows[CapitalReportDS.ASSETNAME]);

            SetPerformanceSummaryGrid(doc, performSummaryTable, "SECURITY PERFORMANCE SUMMARY (" + ((CapitalReportDS)this.PresentationData).DateInfo() + ")");
            AddFooter(doc);
            return doc;
        }

        public void SetPerformanceSummaryGrid(C1PrintDocument doc, DataTable performSummaryTable, string serviceName)
        {
            int index = 1;

            decimal opBalSummaryTotal = 0;
            decimal clsSummaryTotal = 0;
            decimal appSummaryTotal = 0;
            decimal redSummaryTotal = 0;
            decimal expenseTotal = 0;
            decimal transferInOutTotal = 0;
            decimal movExcIncSummaryTotal = 0;
            decimal chgMktValSummaryTotal = 0;
            decimal incomeSummaryTotal = 0;
            decimal IncomeTotal = 0;

            if (performSummaryTable.Rows.Count > 0)
            {
                RenderTable performTable = new RenderTable();
                performTable.RowGroups[0, 1].PageHeader = true;
                performTable.Style.FontSize = 7;

                performTable.Rows[0].Height = .2;
                performTable.Rows[0].Style.FontBold = true;
                performTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                performTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                performTable.Rows[0].Style.FontBold = true;
                performTable.Rows[0].Style.FontSize = 7;
                performTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                performTable.Cells[0, 0].Text = "Description";
                performTable.Cells[0, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
                performTable.Cells[0, 1].Text = "TYPE";
                performTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Center;

                performTable.Cells[0, 2].Text = "Op. Bal.";
                performTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[0, 3].Text = "Sale / Pur.";
                performTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[0, 4].Text = "Trans. In / Out";
                performTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[0, 5].Text = "Fees, Taxes";
                performTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[0, 6].Text = "Other Mvt.";
                performTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[0, 7].Text = "Mkt. Val. Chg.";
                performTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[0, 8].Text = "Cls. Bal.";
                performTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[0, 9].Text = "Income";
                performTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[0, 10].Text = "Growth %";
                performTable.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[0, 11].Text = "Income %";
                performTable.Cells[0, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[0, 12].Text = "Overall %";
                performTable.Cells[0, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

                performTable.Cols[0].Width = 4;
                performTable.Cols[1].Width = 0.5;
                performTable.Cols[2].Width = 1.2;
                performTable.Cols[3].Width = 1.2;
                performTable.Cols[4].Width = 1.2;
                performTable.Cols[5].Width = 1.2;
                performTable.Cols[6].Width = 1.2;
                performTable.Cols[7].Width = 1.2;
                performTable.Cols[8].Width = 1.2;
                performTable.Cols[9].Width = 1.2;
                performTable.Cols[10].Width = 0.8;
                performTable.Cols[11].Width = 0.8;
                performTable.Cols[12].Width = 0.8;

                foreach (DataRow row in performSummaryTable.Select().OrderBy(row => row[PerfReportDS.DESCRIPTION]))
                {
                    string type = String.Empty;

                    if (row[PerfReportDS.TYPE] != System.DBNull.Value)
                        type = (string)row[PerfReportDS.TYPE];

                    if (row[PerfReportDS.OPENINGBAL] != System.DBNull.Value)
                        opBalSummaryTotal += (decimal)row[PerfReportDS.OPENINGBAL];
                    if (row[PerfReportDS.CLOSINGBALANCE] != System.DBNull.Value)
                        clsSummaryTotal += (decimal)row[PerfReportDS.CLOSINGBALANCE];
                    appSummaryTotal += (decimal)row[PerfReportDS.APPLICATION];
                    redSummaryTotal += (decimal)row[PerfReportDS.REDEMPTION];
                    transferInOutTotal += (decimal)row[PerfReportDS.TRANSFEROUT];
                    expenseTotal += (decimal)row[PerfReportDS.EXPENSE];

                    if (row[PerfReportDS.TYPE].ToString().ToUpper() == "CASH" || row[PerfReportDS.TYPE].ToString().ToUpper() == "TDS")
                        movExcIncSummaryTotal += ((decimal)row[PerfReportDS.INTERNALCASHMOVEMENT] + (decimal)row[PerfReportDS.OTHER] + (decimal)row[PerfReportDS.INCOME]);
                    else
                        movExcIncSummaryTotal += ((decimal)row[PerfReportDS.INTERNALCASHMOVEMENT] + (decimal)row[PerfReportDS.OTHER]);

                    IncomeTotal += ((decimal)row[PerfReportDS.INCOME]);

                    performTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    performTable.Rows[index].Height = .3;

                    performTable.Cells[index, 0].Text = row[PerfReportDS.DESCRIPTION].ToString();
                    performTable.Cells[index, 1].Text = row[PerfReportDS.TYPE].ToString().ToUpper();
                    performTable.Cells[index, 1].Style.TextAlignHorz = AlignHorzEnum.Center;
                    performTable.Cells[index, 2].Text = ((decimal)row[PerfReportDS.OPENINGBAL]).ToString("C");
                    performTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                    performTable.Cells[index, 3].Text = ((decimal)row[PerfReportDS.APPLICATION] + (decimal)row[PerfReportDS.REDEMPTION]).ToString("C");
                    performTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    performTable.Cells[index, 4].Text = ((decimal)row[PerfReportDS.TRANSFEROUT]).ToString("C");
                    performTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    performTable.Cells[index, 5].Text = ((decimal)row[PerfReportDS.EXPENSE]).ToString("C");
                    performTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                    if (row[PerfReportDS.TYPE].ToString().ToUpper() == "CASH" || row[PerfReportDS.TYPE].ToString().ToUpper() == "TDS")
                        performTable.Cells[index, 6].Text = ((decimal)row[PerfReportDS.INTERNALCASHMOVEMENT] + (decimal)row[PerfReportDS.OTHER] + (decimal)row[PerfReportDS.INCOME]).ToString("C");
                    else
                        performTable.Cells[index, 6].Text = ((decimal)row[PerfReportDS.INTERNALCASHMOVEMENT] + (decimal)row[PerfReportDS.OTHER]).ToString("C");

                    performTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                    performTable.Cells[index, 7].Text = ((decimal)row[PerfReportDS.CHANGEININVESTMENTVALUE]).ToString("C");
                    performTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                    chgMktValSummaryTotal += (decimal)row[PerfReportDS.CHANGEININVESTMENTVALUE];

                    if (row[PerfReportDS.CLOSINGBALANCE] != System.DBNull.Value)
                        performTable.Cells[index, 8].Text = ((decimal)row[PerfReportDS.CLOSINGBALANCE]).ToString("C");
                    else
                        performTable.Cells[index, 8].Text = "-";
                    performTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                    performTable.Cells[index, 9].Text = ((decimal)row[PerfReportDS.INCOME]).ToString("C");
                    performTable.Cells[index, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
                    incomeSummaryTotal += (decimal)row[PerfReportDS.INCOME];

                    performTable.Cells[index, 10].Text = row[PerfReportDS.PERPERIOD].ToString();
                    performTable.Cells[index, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                    performTable.Cells[index, 11].Text = row[PerfReportDS.PERPERIODDIFF].ToString();
                    performTable.Cells[index, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

                    performTable.Cells[index, 12].Text = row[PerfReportDS.PERPERIODINCOME].ToString();
                    performTable.Cells[index, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

                    index++;
                }


                performTable.Rows[index].Height = .2;
                performTable.Rows[index].Style.FontBold = true;
                performTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                performTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                performTable.Rows[index].Style.FontBold = true;
                performTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                performTable.Cells[index, 0].Text = "TOTAL";
                performTable.Cells[index, 0].Style.TextAlignHorz = AlignHorzEnum.Left;

                performTable.Cells[index, 2].Text = opBalSummaryTotal.ToString("C");
                performTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                performTable.Cells[index, 3].Text = (appSummaryTotal + redSummaryTotal).ToString("C");
                performTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                performTable.Cells[index, 4].Text = transferInOutTotal.ToString("C");
                performTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                performTable.Cells[index, 5].Text = expenseTotal.ToString("C");
                performTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[index, 6].Text = movExcIncSummaryTotal.ToString("C");
                performTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                performTable.Cells[index, 7].Text = chgMktValSummaryTotal.ToString("C");
                performTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                performTable.Cells[index, 8].Text = clsSummaryTotal.ToString("C");
                performTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[index, 9].Text = incomeSummaryTotal.ToString("C");
                performTable.Cells[index, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(performTable);
                doc.Body.Children.Add(lineBreak);
            }

        }

        protected void GeneratePerformanceReport(object sender, EventArgs e)
        {
            ClientPerformanceReportClick();
        }

        protected void ClientPerformanceReportClick(PerfReportDS ds)
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"].ToString()));
            clientData.GetData(ds);
            this.PresentationData = ds;
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);
            string reportName = "Report_" + Guid.NewGuid().ToString();
            C1ReportViewer.RegisterDocument(reportName, MakeDocPerformance);
            C1ReportViewerCapitalFlow.FileName = reportName;
            C1ReportViewerCapitalFlow.ReportName = reportName;
            InputEndDate.DbSelectedDate = ds.EndDate;
            InputStartDate.DbSelectedDate = ds.StartDate;
            UMABroker.ReleaseBrokerManagedComponent(clientData);    
        }
        protected void ClientPerformanceReportClick()
        {
            PerfReportDS ds = new PerfReportDS();
            ds.IsRange = true;

            if (InputEndDate.DateInput.SelectedDate.HasValue)
                 ds.EndDate =  AccountingFinancialYear.LastDayOfMonthFromDateTime(this.InputEndDate.DateInput.SelectedDate.Value);

            if (InputStartDate.DateInput.SelectedDate.HasValue)
                 ds.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(this.InputStartDate.DateInput.SelectedDate.Value);

            ClientPerformanceReportClick(ds); 
        }
    }
}
