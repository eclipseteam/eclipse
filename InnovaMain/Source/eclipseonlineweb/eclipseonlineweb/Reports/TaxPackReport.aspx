﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="ReportsMaster.master"
    AutoEventWireup="true" CodeBehind="TaxPackReport.aspx.cs" Inherits="eclipseonlineweb.TaxPackReport" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer"
    TagPrefix="C1ReportViewer" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                            <span class="riLabel">Start Date:</span><telerik:RadMonthYearPicker Width="110px"
                                ID="InputStartDate" runat="server">
                            </telerik:RadMonthYearPicker>
                        </td>
                        <td>
                            <span class="riLabel">End Date:</span><telerik:RadMonthYearPicker ID="InputEndDate"
                                Width="110px" runat="server">
                            </telerik:RadMonthYearPicker>
                        </td>
                        <td>
                            <span class="riLabel">Acc. Method:</span>
                            <telerik:RadComboBox runat="server" ID="comboBoxAccType" Width="100px">
                                <Items>
                                    <telerik:RadComboBoxItem Value="Accrual" Text="Accrual" />
                                    <telerik:RadComboBoxItem Value="Cash" Selected="false" Text="Cash" />
                                    <telerik:RadComboBoxItem Value="Mixed" Selected="true" Text="Mixed" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <span class="riLabel">Cost Method:</span>
                            <telerik:RadComboBox runat="server" ID="comboBoxCostType" Width="100px">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <span class="riLabel">Select Reports:</span>
                            <telerik:RadComboBox CheckBoxes="true" EnableCheckAllItemsCheckBox="true" Width="250px"
                                Text="SELECT REPORT(S)" ID="C1ComboBoxReports" Filter="Contains" runat="server"
                                Skin="Metro">
                                <Items>
                                    <telerik:RadComboBoxItem Text="INVESTMENT TRANSACTION LISTING" Value="INVESTMENT TRANSACTION LISTING"
                                        Checked="true" />
                                    <telerik:RadComboBoxItem Text="GAINS & LOSSES" Value="GAINS & LOSSES" Checked="true" />
                                    <telerik:RadComboBoxItem Text="HOLDING SUMMARY" Value="HOLDING SUMMARY" Checked="true" />
                                    <telerik:RadComboBoxItem Text="HOLDING STATEMENTS" Value="HOLDING STATEMENTS" Checked="true" />
                                    <telerik:RadComboBoxItem Text="INCOME & EXPENSE" Value="INCOME" Checked="true" />
                                    <telerik:RadComboBoxItem Text="TAX SUMMARY" Value="TAX DISTRIBUTION" Checked="true" />
                                    <telerik:RadComboBoxItem Text="TAX DISTRIBUTION" Value="TAX DISTRIBUTION" Checked="true" />
                                    <telerik:RadComboBoxItem Text="TAX DISTRIBUTION - DETAILS" Value="TAX DISTRIBUTION - DETAILS"
                                        Checked="true" />
                                    <telerik:RadComboBoxItem Text="DIVIDENDS" Value="DIVIDEND - SUMMARY" Checked="true" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                        <td>
                        </td>
                        <td>
                            <span class="riLabel">Addresses:</span>
                            <telerik:RadComboBox runat="server" ID="AddressList" Height="190px" Width="420px"
                                HighlightTemplatedItems="true" OnDataBound="RadComboBox1_DataBound" OnItemDataBound="RadComboBox1_ItemDataBound"
                                OnItemsRequested="RadComboBox1_ItemsRequested">
                                <ItemTemplate>
                                    <ul>
                                        <li class="col1"><b>
                                            <%# DataBinder.Eval(Container.DataItem, "Key") %></b></li>
                                        <li class="col2">
                                            <%# DataBinder.Eval(Container.DataItem, "Value") %></li>
                                    </ul>
                                </ItemTemplate>
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <br />
                            <telerik:RadButton runat="server" ID="BtnGenerateReport" OnClick="GenerateReport"
                                Text="Generate Report">
                            </telerik:RadButton>
                        </td>
                        <td>
                        </td>
                        <td style="width: 65%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <C1ReportViewer:C1ReportViewer CollapseToolsPanel="true" Cache-Enabled="false" Cache-ShareBetweenSessions="false"
                FileName="C1ReportViewerAssetSummary" runat="server" ID="C1ReportViewerAssetClass"
                Zoom="75%" Height="550px" Width="100%">
            </C1ReportViewer:C1ReportViewer>
            <asp:Label runat="server" Visible="false" ID="LBLAddressKey"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
