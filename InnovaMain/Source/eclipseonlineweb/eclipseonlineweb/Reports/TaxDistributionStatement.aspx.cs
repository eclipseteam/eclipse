﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;

namespace eclipseonlineweb
{
    public partial class TaxDistributionStatement : UMABasePage
    {

        protected override void Intialise()
        {
            base.Intialise();
            this.DisTranGrid.RowCommand += new C1GridViewCommandEventHandler(DisTranGrid_RowCommand);
          
        }

        private void DisTranGrid_RowCommand(object sender, C1GridViewCommandEventArgs e)
        {
            var grid = (C1GridView)sender;
            int index = int.Parse(e.CommandArgument.ToString());
            string fundCode = grid.Rows[index].Cells[0].Text;
            string startDate = grid.Rows[index].Cells[2].Text;
            string endDate = grid.Rows[index].Cells[3].Text;

            this.cid = Request.QueryString["ins"].ToString();

            if (e.CommandName.ToLower() == "details")
            {
                C1ReportViewerDividentStatement.Visible = true;
                DisTranGrid.Visible = false;
                Response.Redirect("~/Reports/TaxDistributionStatementReport.aspx?ins=" + this.cid + "&FundCode=" + fundCode + "&StartDate=" + startDate + "&EndDate=" + endDate);
            }
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public override void LoadPage()
        {
            this.cid = Request.QueryString["ins"].ToString();

            DIS_Click();

            ScriptManager sm = ScriptManager.GetCurrent(Page);
        }

        protected void DIS_Click()
        {
            this.cid = Request.QueryString["ins"].ToString();
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);
            ClientTaxDistributionsDS disDS = new ClientTaxDistributionsDS();
            clientData.GetData(disDS);
            this.PresentationData = disDS;
            
            if (disDS.Tables.Count > 0)
            {
                DataView divView = new DataView(disDS.Tables[ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE]);
                divView.Sort = "FinYearStartDate DESC";
               
                this.DisTranGrid.DataSource = divView.ToTable();
                this.DisTranGrid.DataBind();
            }

            this.UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void DIS_PageIndexChanging(object sender, C1GridViewPageEventArgs e)
        {
            this.cid = Request.QueryString["ins"].ToString();
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            ClientDistributionsDS ds = new ClientDistributionsDS();
            clientData.GetData(ds);
            DataView summaryView = new DataView(ds.Tables["Income Transactions"]);
            summaryView.Sort = "FinYearStartDate DESC";
            this.DisTranGrid.DataSource = summaryView.ToTable();

            this.DisTranGrid.PageIndex = e.NewPageIndex;
            DisTranGrid.DataBind();

            this.UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void FilterDIS(object sender, C1GridViewFilterEventArgs e)
        {
            this.cid = Request.QueryString["ins"].ToString();
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            ClientDistributionsDS ds = new ClientDistributionsDS();
            clientData.GetData(ds);

            e.Values[0] = ((string)e.Values[0]).Trim();

            DataView summaryView = new DataView(ds.Tables["Income Transactions"]);
            summaryView.Sort = "FinYearStartDate DESC";
       
            this.DisTranGrid.DataSource = summaryView.ToTable();
            this.DisTranGrid.DataBind();

            this.UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void SortDIS(object sender, C1GridViewSortEventArgs e)
        {
            this.cid = Request.QueryString["ins"].ToString();
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            ClientDistributionsDS ds = new ClientDistributionsDS();
            clientData.GetData(ds);

            DataView summaryView = new DataView(ds.Tables["Income Transactions"]);
            summaryView.Sort = "RecordDate DESC";
       
            this.DisTranGrid.DataSource = summaryView.ToTable();
            this.DisTranGrid.DataBind();

            this.UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

       
    }
}
