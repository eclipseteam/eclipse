﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Utilities;

namespace eclipseonlineweb
{
    public partial class ClientCapitalMovementReport : UMABasePage
    {
        public override void PopulatePage(DataSet ds)
        {
            this.cid = Request.QueryString["ins"].ToString();
            base.PopulatePage(ds);
            ClientCapitalReportClick();
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public C1PrintDocument MakeDocCapital()
        {
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            DataRow clientSummaryRow = this.PresentationData.Tables[CapitalReportDS.CLIENTSUMMARYTABLE].Rows[0];

            ((CapitalReportDS)this.PresentationData).StartDate = WebUtilities.Utilities.FirstDayOfMonthFromDateTime(((CapitalReportDS)this.PresentationData).StartDate);
            ((CapitalReportDS)this.PresentationData).EndDate = WebUtilities.Utilities.LastDayOfMonthFromDateTime(((CapitalReportDS)this.PresentationData).EndDate);

            AddClientHeaderToReport(doc, clientSummaryRow, ((CapitalReportDS)this.PresentationData).DateInfo(),"CAPITAL MOVEMENT REPORT");

            DataTable capitalSummaryTable = this.PresentationData.Tables[CapitalReportDS.CAPITALFLOWSUMMARY];
            DataTable capitalSummaryCatTable = this.PresentationData.Tables[CapitalReportDS.CAPITALFLOWSUMMARYCAT];
            DataTable bankTranTable = this.PresentationData.Tables[CapitalReportDS.BANKTRANSACTIONSTABLE];
            DataTable asxTranTable = this.PresentationData.Tables[CapitalReportDS.ASXTRANSTABLE];

            var capitalSummaryCatRows = capitalSummaryCatTable.Select().OrderByDescending(rows => rows[CapitalReportDS.MONTH]);
            var bankTranTableRows = bankTranTable.Select().OrderByDescending(rows => rows[CapitalReportDS.BANKTRANSDATE]).OrderBy(rows => rows[CapitalReportDS.CAPITALMOVEMENTCAT]).OrderBy(rows => rows[CapitalReportDS.BANKTRANSDATE]);
            var asxTranTableRows = asxTranTable.Select().OrderByDescending(rows => rows[CapitalReportDS.TRADEDATE]);

            var difmRows = capitalSummaryTable.Select("ServiceType = 'Do It For Me'").OrderBy(rows => rows[CapitalReportDS.ASSETNAME]);
            var diyRows = capitalSummaryTable.Select("ServiceType = 'Do It Yourself'").OrderBy(rows => rows[CapitalReportDS.ASSETNAME]);
            var diwmRows = capitalSummaryTable.Select("ServiceType = 'Do It With Me'").OrderBy(rows => rows[CapitalReportDS.ASSETNAME]);
            var manRows = capitalSummaryTable.Select("ServiceType = 'Manual Asset'").OrderBy(rows => rows[CapitalReportDS.ASSETNAME]);

            SetCapitalCatSummaryGrid(doc, capitalSummaryCatRows, "CAPITAL MOVEMENT SUMMARY");

            SetCapitalGrid(doc, difmRows, "CAPITAL MOVEMENT SUMMARY - DO IT FOR ME", ((CapitalReportDS)this.PresentationData).DateInfo());
            SetCapitalGrid(doc, diyRows, "CAPITAL MOVEMENT SUMMARY - DO IT YOURSELF", ((CapitalReportDS)this.PresentationData).DateInfo());
            SetCapitalGrid(doc, diwmRows, "CAPITAL MOVEMENT SUMMARY - DO IT WITH ME", ((CapitalReportDS)this.PresentationData).DateInfo());
            SetCapitalGrid(doc, manRows, "CAPITAL MOVEMENT SUMMARY - MANUAL ASSETS", ((CapitalReportDS)this.PresentationData).DateInfo());

            
            var groupedRows = this.PresentationData.Tables[CapitalReportDS.TRANBREAKDOWNTABLE].Select().OrderByDescending(row => (DateTime)row[CapitalReportDS.TRADEDATE]).GroupBy(row => row[CapitalReportDS.MAJORCATEGORY]);

            foreach (var tranCategory in groupedRows)
            {
                SetTransBreakDownGrid(doc, tranCategory, tranCategory.Key.ToString().ToUpper());
            }

            AddFooterCapMvtStatement(doc);

            return doc;
        }

        public static void AddFooterCapMvtStatement(C1PrintDocument doc)
        {
            doc.PageLayouts.PrintFooterOnLastPage = true;
            RenderTable footer = new RenderTable();
            footer.Rows[0].Height = .30;
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;
            footer.Rows[1].Height = 2.5;
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.TextAlignVert = AlignVertEnum.Center;

            footer.Cells[0, 0].Text = "FOR YOUR INFORMATION";
            footer.Cells[0, 0].Style.TextAlignHorz = AlignHorzEnum.Center;
            footer.Cells[0, 0].Style.FontSize = 12;

            footer.Cells[1, 0].Text = "\n1. The value of any unitised securities held in your portfolio (e.g. shares & managed funds) is determined based on the latest available exit price of each respective security at the date your portfolio is being valued."+
                                        "\n\n2. Capital movements are based on cash flows in and out of your portfolio or between accounts in your portfolio.  This report also provides an indication of the growth of your portfolio (whether it be positive or negative) as a result of market movement." +
                                        "\n\n3. This report is provided by e-Clipse Online Pty Limited ABN 70 145 358 630, AFSL 357 306 (“e-Clipse”) and is based on information provided to e-Clipse by third parties.  Whilst every reasonable effort has been made by e-Clipse to ensure its accuracy, neither e-Clipse nor any of its related entities guarantee its accuracy nor accept any liability for any errors or omissions." +
                                        "\n\ne-Clipse Online Pty Ltd (ABN 70 145 358 630)\n3/36 Bydown Street, Neutral Bay, NSW 2089\nhttp://www.e-clipse.com.au\nP: +61 2 9346 4686";
            footer.Cells[1, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            footer.Cells[1, 0].Style.TextAlignVert = AlignVertEnum.Top;
            footer.Cells[1, 0].Style.FontSize = 9;
            footer.Cells[1, 0].Style.FontBold = false;

            doc.PageLayouts.LastPage = new PageLayout();
            doc.PageLayouts.LastPage.PageFooter = footer;
        }

        public void SetCapitalCatSummaryGrid(C1PrintDocument doc, IOrderedEnumerable<DataRow> rows, string serviceName)
        {
            int index = 1;

            if (rows.Count() > 0)
            {
                RenderTable capitalTable = new RenderTable();
                capitalTable.RowGroups[0, 1].PageHeader = true;
            
                double OpeningBalance = 0;
                double TransferInOut = 0;
                double Income = 0;
                double AppicationRedemption = 0;
                double TaxInOut = 0;
                double Expense = 0;
                double InternalCashMovement = 0;
                double ClosingBalance = 0;
                double ChangeInInvestmentValue = 0;

                capitalTable.Style.FontSize = 8;

                capitalTable.Rows[0].Height = .2;
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.FontSize = 7;
                capitalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[0, 0].Text = "Month";
                capitalTable.Cells[0, 1].Text = "Opening Balance";
                capitalTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 2].Text = "Transfer In/Out";
                capitalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 3].Text = "Income";
                capitalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 4].Text = "Investments";
                capitalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 5].Text = "Tax & Expenses";
                capitalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 6].Text = "Internal Cash Mvt";
                capitalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 7].Text = "Change in Mkt. Val.";
                capitalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 8].Text = "Closing Balance";
                capitalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
             
                capitalTable.Cols[0].Width = .5;
                capitalTable.Cols[1].Width = 1.2;
                capitalTable.Cols[2].Width = 1.2;
                capitalTable.Cols[3].Width = 1.2;
                capitalTable.Cols[4].Width = 1.2;
                capitalTable.Cols[5].Width = 1.2;
                capitalTable.Cols[6].Width = 1.2;
                capitalTable.Cols[7].Width = 1.2;
                capitalTable.Cols[8].Width = 1.2;
             
                foreach (DataRow row in rows)
                {
                    capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    capitalTable.Rows[index].Height = .3;
                    capitalTable.Cells[index, 0].Text = ((DateTime)row[CapitalReportDS.MONTH]).ToString("MMM-yy").ToUpper();

                    OpeningBalance += Convert.ToDouble(row[CapitalReportDS.OPENINGBAL]);
                    TransferInOut += Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]);
                    Income += Convert.ToDouble(row[CapitalReportDS.INCOME]);
                    AppicationRedemption += Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION]);
                    TaxInOut += Convert.ToDouble(row[CapitalReportDS.TAXINOUT]);
                    Expense += Convert.ToDouble(row[CapitalReportDS.EXPENSE]);
                    InternalCashMovement += Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]);
                    ClosingBalance += Convert.ToDouble(row[CapitalReportDS.CLOSINGBALANCE]);
                    ChangeInInvestmentValue += Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]);

                    capitalTable.Cells[index, 1].Text = Convert.ToDouble(row[CapitalReportDS.OPENINGBAL]).ToString("C");
                    capitalTable.Cells[index, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 2].Text = Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]).ToString("C");
                    capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 3].Text = Convert.ToDouble(row[CapitalReportDS.INCOME]).ToString("C");
                    capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 4].Text = Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION]).ToString("C");
                    capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 5].Text = Convert.ToDouble(row[CapitalReportDS.EXPENSE]).ToString("C");
                    capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 6].Text = Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]).ToString("C");
                    capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 7].Text = Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]).ToString("C");
                    capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 8].Text = Convert.ToDouble(row[CapitalReportDS.CLOSINGBALANCE]).ToString("C");
                    capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                    index++;
                }
                
                capitalTable.Rows[index].Height = .2;
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[index, 0].Text = "TOTAL";

                capitalTable.Cells[index, 2].Text = TransferInOut.ToString("C");
                capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 3].Text = Income.ToString("C");
                capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 4].Text = AppicationRedemption.ToString("C");
                capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 5].Text = Expense.ToString("C");
                capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 6].Text = InternalCashMovement.ToString("C");
                capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 7].Text = ChangeInInvestmentValue.ToString("C");
                capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
            
                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(capitalTable);
                doc.Body.Children.Add(lineBreak);
            }

        }

        public void SetPerformanceSummaryGrid(C1PrintDocument doc, IOrderedEnumerable<DataRow> rows, string serviceName)
        {
            int index = 1;

            if (rows.Count() > 0)
            {
                RenderTable capitalTable = new RenderTable();
                capitalTable.RowGroups[0, 1].PageHeader = true;
                double OpeningBalance = 0;
                double Movement = 0;
                double ClosingBalance = 0;
                double ChangeInInvestmentValue = 0;

                capitalTable.Style.FontSize = 8;

                capitalTable.Rows[0].Height = .2;
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.FontSize = 7;
                capitalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[0, 0].Text = "Month";
                capitalTable.Cells[0, 1].Text = "Opening Balance";
                capitalTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 2].Text = "Movement";
                capitalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 3].Text = "Closing Balance";
                capitalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 4].Text = "Change in Inv";
                capitalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 5].Text = "Sum CF";
                capitalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 6].Text = "Sum Weighted CF";
                capitalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 7].Text = "Mod Dietz";
                capitalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cols[0].Width = .5;
                capitalTable.Cols[1].Width = 1.2;
                capitalTable.Cols[2].Width = 1.2;
                capitalTable.Cols[3].Width = 1.2;
                capitalTable.Cols[4].Width = 1.2;
                capitalTable.Cols[5].Width = 1.2;
                capitalTable.Cols[6].Width = 1.2;
                capitalTable.Cols[7].Width = 1.2;

                foreach (DataRow row in rows)
                {

                    capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    capitalTable.Rows[index].Height = .3;
                    capitalTable.Cells[index, 0].Text = ((DateTime)row[CapitalReportDS.MONTH]).ToString("MMM-yy").ToUpper();

                    OpeningBalance += Convert.ToDouble(row[CapitalReportDS.OPENINGBAL]);
                    Movement += Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]);
                    Movement += Convert.ToDouble(row[CapitalReportDS.INCOME]);
                    Movement += Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION]);
                    Movement += Convert.ToDouble(row[CapitalReportDS.TAXINOUT]);
                    Movement += Convert.ToDouble(row[CapitalReportDS.EXPENSE]);
                    Movement += Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]);
                    ClosingBalance += Convert.ToDouble(row[CapitalReportDS.CLOSINGBALANCE]);
                    ChangeInInvestmentValue += Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]);

                    double movement = Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]) + Convert.ToDouble(row[CapitalReportDS.INCOME]) + Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION])
                                       + Convert.ToDouble(row[CapitalReportDS.TAXINOUT]) + Convert.ToDouble(row[CapitalReportDS.EXPENSE]) + Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]);

                    capitalTable.Cells[index, 1].Text = Convert.ToDouble(row[CapitalReportDS.OPENINGBAL]).ToString("C");
                    capitalTable.Cells[index, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 2].Text = movement.ToString("C");
                    capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 3].Text = Convert.ToDouble(row[CapitalReportDS.CLOSINGBALANCE]).ToString("C");
                    capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 4].Text = Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]).ToString("C");
                    capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    if (row[CapitalReportDS.SUMCF] != null && row[CapitalReportDS.SUMCF].ToString() != String.Empty)
                        capitalTable.Cells[index, 5].Text = Convert.ToDouble(row[CapitalReportDS.SUMCF]).ToString("N2");
                    else
                        capitalTable.Cells[index, 5].Text = Convert.ToDouble(0).ToString("P2");
                    capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                    if (row[CapitalReportDS.SUMWEIGHTCF] != null && row[CapitalReportDS.SUMWEIGHTCF].ToString() != String.Empty)
                        capitalTable.Cells[index, 6].Text = Convert.ToDouble(row[CapitalReportDS.SUMWEIGHTCF]).ToString("N2");
                    else
                        capitalTable.Cells[index, 6].Text = Convert.ToDouble(0).ToString("P2");
                    capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                    if (row[CapitalReportDS.MODDIETZ] != null && row[CapitalReportDS.MODDIETZ].ToString() != String.Empty)
                        capitalTable.Cells[index, 7].Text = Convert.ToDouble(row[CapitalReportDS.MODDIETZ]).ToString("P2");
                    else
                        capitalTable.Cells[index, 7].Text = Convert.ToDouble(0).ToString("P2");
                    capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    index++;
                }


                capitalTable.Rows[index].Height = .2;
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[index, 0].Text = "TOTAL";

                capitalTable.Cells[index, 2].Text = Movement.ToString("C");
                capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(capitalTable);
                doc.Body.Children.Add(lineBreak);
            }

        }

        public void SetCapitalGrid(C1PrintDocument doc, IOrderedEnumerable<DataRow> difmRows, string serviceName, string dateinfo)
        {
            int index = 1;

            if (difmRows.Count() > 0)
            {
                RenderTable capitalTable = new RenderTable();
                capitalTable.RowGroups[0, 1].PageHeader = true;
               
                decimal openingBalanceTotal = 0;
                decimal transferInOutTotal = 0;
                decimal incomeTotal = 0;
                decimal investmentsTotal = 0;
                decimal taxExpenseTotal = 0;
                decimal intCashMovTotal = 0;
                decimal mktValCahangeTotal = 0;
                decimal closingBalanceTotal = 0;
                
                capitalTable.Style.FontSize = 7;

                capitalTable.Rows[0].Height = .2;
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.FontSize = 7;
                capitalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[0, 0].Text = "Asset Name";
                capitalTable.Cells[0, 1].Text = "Product Name";
                capitalTable.Cells[0, 2].Text = "Account No";
                capitalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Center;
                capitalTable.Cells[0, 3].Text = "Opn. Bal.";
                capitalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 4].Text = "Trans. In/Out";
                capitalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 5].Text = "Income";
                capitalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 6].Text = "Investments";
                capitalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 7].Text = "Tax & Exp.";
                capitalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 8].Text = "Int. Cash Mvt.";
                capitalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 9].Text = "Mkt Val. Chg.";
                capitalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 10].Text = "Cls. Bal.";
                capitalTable.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cols[0].Width = .8;
                capitalTable.Cols[1].Width = .8;
                capitalTable.Cols[2].Width = .8;
                capitalTable.Cols[3].Width = 1;
                capitalTable.Cols[4].Width = 1;
                capitalTable.Cols[5].Width = 1;
                capitalTable.Cols[6].Width = 1;
                capitalTable.Cols[7].Width = 1;
                capitalTable.Cols[8].Width = 1;
                capitalTable.Cols[9].Width = 1;
                capitalTable.Cols[10].Width = 1;
  
                foreach (DataRow row in difmRows)
                {
                    capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    capitalTable.Rows[index].Height = .3;
                    capitalTable.Cells[index, 0].Text = row[CapitalReportDS.ASSETNAME].ToString();
                    capitalTable.Cells[index, 1].Text = row[CapitalReportDS.PRODUCTNAME].ToString();
                    capitalTable.Cells[index, 2].Text = row[CapitalReportDS.ACCOUNTNO].ToString();
                    capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Center;

                    decimal openingBalance = 0;
                    decimal transferInOut = 0;
                    decimal income = 0;
                    decimal investments = 0;
                    decimal taxExpense = 0;
                    decimal intCashMov = 0;
                    decimal mktValCahange = 0;
                    decimal closingBalance = 0;

                    openingBalance += (decimal)row[CapitalReportDS.OPENINGBAL];
                    transferInOut += (decimal)row[CapitalReportDS.TRANSFEROUT];
                    income += (decimal)row[CapitalReportDS.INCOME];
                    investments += (decimal)row[CapitalReportDS.APPLICATIONREDEMPTION];
                    taxExpense += (decimal)row[CapitalReportDS.TAXINOUT];
                    taxExpense += (decimal)row[CapitalReportDS.EXPENSE  ];
                    intCashMov += (decimal)row[CapitalReportDS.INTERNALCASHMOVEMENT];
                    mktValCahange += (decimal)row[CapitalReportDS.CHANGEININVESTMENTVALUE];
                    closingBalance += (decimal)row[CapitalReportDS.CLOSINGBALANCE];

                    openingBalanceTotal += openingBalance;
                    transferInOutTotal += transferInOut;
                    incomeTotal += income;
                    investmentsTotal += investments;
                    taxExpenseTotal += taxExpense;
                    intCashMovTotal += intCashMov;
                    mktValCahangeTotal += mktValCahange;
                    closingBalanceTotal += closingBalance;

                    capitalTable.Cells[index, 3].Text = openingBalance.ToString("C");
                    capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 4].Text = transferInOut.ToString("C");
                    capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 5].Text = income.ToString("C");
                    capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 6].Text = investments.ToString("C");
                    capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 7].Text = taxExpense.ToString("C");
                    capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 8].Text = intCashMov.ToString("C");
                    capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 9].Text = mktValCahange.ToString("C");
                    capitalTable.Cells[index, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 10].Text = closingBalance.ToString("C");
                    capitalTable.Cells[index, 10].Style.TextAlignHorz = AlignHorzEnum.Right;
                    index++;
                }

                capitalTable.Rows[index].Height = .2;
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[index, 0].Text = "TOTAL";
                capitalTable.Cells[index, 3].Text = openingBalanceTotal.ToString("C");
                capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 4].Text = transferInOutTotal.ToString("C");
                capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 5].Text = incomeTotal.ToString("C");
                capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 6].Text = investmentsTotal.ToString("C");
                capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 7].Text = taxExpenseTotal.ToString("C");
                capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 8].Text = intCashMovTotal.ToString("C");
                capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 9].Text = mktValCahangeTotal.ToString("C");
                capitalTable.Cells[index, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 10].Text = closingBalanceTotal.ToString("C");
                capitalTable.Cells[index, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + " - " + dateinfo + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(capitalTable);
                doc.Body.Children.Add(lineBreak);
            }

        }

        public void SetTransBreakDownGrid(C1PrintDocument doc, IGrouping<object, DataRow> rows, string serviceName)
        {
            int index = 1;
            double total = 0;
            if (rows.Count() > 0)
            {
                RenderTable capitalTable = new RenderTable();
                capitalTable.RowGroups[0, 1].PageHeader = true;
                capitalTable.Style.FontSize = 6;

                capitalTable.Rows[0].Height = .2;
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.FontSize = 6;
                capitalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[0, 0].Text = CapitalReportDS.TRANSACTIONTYPE;
                capitalTable.Cells[0, 1].Text = CapitalReportDS.ACCOUNTNO;
                capitalTable.Cells[0, 2].Text = CapitalReportDS.CATEGORY;
                capitalTable.Cells[0, 3].Text = CapitalReportDS.MAJORCATEGORY;
                capitalTable.Cells[0, 4].Text = CapitalReportDS.COMMENT;
                capitalTable.Cells[0, 5].Text = CapitalReportDS.ACCOUNTNO;
                capitalTable.Cells[0, 6].Text = CapitalReportDS.TRADEDATE;
                capitalTable.Cells[0, 7].Text = CapitalReportDS.AMOUNTTOTAL;
                capitalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cols[0].Width = 3;
                capitalTable.Cols[1].Width = 3;
                capitalTable.Cols[2].Width = 3;
                capitalTable.Cols[3].Width = 3;
                capitalTable.Cols[4].Width = 8;
                capitalTable.Cols[5].Width = 3;
                capitalTable.Cols[6].Width = 3;
                capitalTable.Cols[7].Width = 3;

                foreach (DataRow row in rows)
                {
                    capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    capitalTable.Rows[index].Height = .3;
                    capitalTable.Cells[index, 0].Text = row[CapitalReportDS.TRANSACTIONTYPE].ToString();
                    capitalTable.Cells[index, 1].Text = row[CapitalReportDS.ACCOUNTNO].ToString();
                    capitalTable.Cells[index, 2].Text = row[CapitalReportDS.CATEGORY].ToString();
                    capitalTable.Cells[index, 3].Text = row[CapitalReportDS.MAJORCATEGORY].ToString();
                    capitalTable.Cells[index, 4].Text = row[CapitalReportDS.COMMENT].ToString();
                    capitalTable.Cells[index, 5].Text = row[CapitalReportDS.ACCOUNTNO].ToString();
                    capitalTable.Cells[index, 6].Text = ((DateTime)row[CapitalReportDS.TRADEDATE]).ToString("dd/MM/yyyy");
                    double amount = Convert.ToDouble(row[CapitalReportDS.AMOUNTTOTAL]);
                    total += amount;
                    capitalTable.Cells[index, 7].Text = amount.ToString("C");
                    capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                    index++;
                }

                capitalTable.Rows[index].Height = .2;
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                capitalTable.Cells[index, 7].Text = total.ToString("C");
                capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(capitalTable);
                doc.Body.Children.Add(lineBreak);
            }

        }

        public void SetCapitalTransactionsGrid(C1PrintDocument doc, IOrderedEnumerable<DataRow> banktransactions, IOrderedEnumerable<DataRow> desktopTransactions, string month)
        {
            int index = 1;

            if (banktransactions.Count() > 0)
            {
                RenderTable capitalTable = new RenderTable();
                capitalTable.RowGroups[0, 1].PageHeader = true;
                capitalTable.Style.FontSize = 6;

                capitalTable.Rows[0].Height = .2;
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.FontSize = 6;
                capitalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[0, 0].Text = "Import Transaction";
                capitalTable.Cells[0, 1].Text = "System Transaction";
                capitalTable.Cells[0, 2].Text = "Category";
                capitalTable.Cells[0, 3].Text = "Transaction Date";
                capitalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Center;
                capitalTable.Cells[0, 4].Text = "Account No";
                capitalTable.Cells[0, 5].Text = "Account Type";
                capitalTable.Cells[0, 6].Text = "Comments";
                capitalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Left;
                capitalTable.Cells[0, 7].Text = "Amount";
                capitalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 8].Text = "Adjustment";
                capitalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 9].Text = "Total";
                capitalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cols[0].Width = 2;
                capitalTable.Cols[1].Width = 1.2;
                capitalTable.Cols[2].Width = 1;
                capitalTable.Cols[3].Width = 1.2;
                capitalTable.Cols[4].Width = 3;
                capitalTable.Cols[5].Width = .8;
                capitalTable.Cols[6].Width = 1;
                capitalTable.Cols[7].Width = 1;
                capitalTable.Cols[8].Width = 1;
                capitalTable.Cols[9].Width = 3;

                foreach (DataRow row in banktransactions)
                {

                    capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    capitalTable.Rows[index].Height = .3;
                    capitalTable.Cells[index, 0].Text = row[CapitalReportDS.IMPTRANTYPE].ToString();
                    capitalTable.Cells[index, 1].Text = row[CapitalReportDS.SYSTRANTYPE].ToString();
                    capitalTable.Cells[index, 2].Text = row[CapitalReportDS.CATEGORY].ToString();
                    capitalTable.Cells[index, 3].Text = ((DateTime)row[CapitalReportDS.BANKTRANSDATE]).ToString("dd/MMM/yyyy");
                    capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Center;
                    capitalTable.Cells[index, 4].Text = row[CapitalReportDS.ACCOUNTNO].ToString();
                    capitalTable.Cells[index, 5].Text = row[CapitalReportDS.ACCOUNTTYPE].ToString();

                    capitalTable.Cells[index, 6].Text = Convert.ToDouble(row[CapitalReportDS.BANKAMOUNT]).ToString("C");
                    capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 7].Text = Convert.ToDouble(row[CapitalReportDS.BANKADJUSTMENT]).ToString("C");
                    capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 8].Text = Convert.ToDouble(row[CapitalReportDS.AMOUNTTOTAL]).ToString("C");
                    capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
                    capitalTable.Cells[index, 9].Text = row[CapitalReportDS.COMMENT].ToString();
                    index++;
                }

                capitalTable.Rows[index].Height = .2;
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = "" + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(capitalTable);
                doc.Body.Children.Add(lineBreak);
            }

        }

        public void SetCapitalASXTransactionsGrid(C1PrintDocument doc, IOrderedEnumerable<DataRow> desktopBrokerTransactions, string serviceName)
        {
            int index = 1;

            if (desktopBrokerTransactions.Count() > 0)
            {
                RenderTable capitalTable = new RenderTable();
                capitalTable.RowGroups[0, 1].PageHeader = true;
                capitalTable.Style.FontSize = 6;

                capitalTable.Rows[0].Height = .2;
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.FontSize = 6;
                capitalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[0, 0].Text = "Account No";
                capitalTable.Cells[0, 1].Text = "External Ref ID";
                capitalTable.Cells[0, 2].Text = "Investment Code";
                capitalTable.Cells[0, 3].Text = "Transaction Type";
                capitalTable.Cells[0, 4].Text = "Transaction Category";
                capitalTable.Cells[0, 5].Text = "Trade Date";
                capitalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Left;
                capitalTable.Cells[0, 6].Text = "Transaction Month";
                capitalTable.Cells[0, 7].Text = "Contract Note";
                capitalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Left;
                capitalTable.Cells[0, 8].Text = "Units";
                capitalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 9].Text = "Net Value";
                capitalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cols[0].Width = 1;
                capitalTable.Cols[1].Width = 1;
                capitalTable.Cols[2].Width = 1;
                capitalTable.Cols[3].Width = 1.5;
                capitalTable.Cols[4].Width = 1.5;
                capitalTable.Cols[5].Width = 1;
                capitalTable.Cols[6].Width = 1;
                capitalTable.Cols[7].Width = 1;
                capitalTable.Cols[8].Width = 1;
                capitalTable.Cols[9].Width = 1;

                foreach (DataRow row in desktopBrokerTransactions)
                {
                    capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    capitalTable.Rows[index].Height = .3;
                    capitalTable.Cells[index, 0].Text = row[CapitalReportDS.ACCOUNTNO].ToString();
                    capitalTable.Cells[index, 1].Text = row[CapitalReportDS.FPSID].ToString();
                    capitalTable.Cells[index, 2].Text = row[CapitalReportDS.INVESMENTCODE].ToString();
                    capitalTable.Cells[index, 3].Text = row[CapitalReportDS.TRANSACTIONTYPE].ToString();
                    capitalTable.Cells[index, 4].Text = row[CapitalReportDS.CAPITALMOVEMENTCAT].ToString();
                    capitalTable.Cells[index, 5].Text = ((DateTime)row[CapitalReportDS.TRADEDATE]).ToString("dd/MMM/yyyy");
                    capitalTable.Cells[index, 6].Text = row[CapitalReportDS.MONTH].ToString();
                    capitalTable.Cells[index, 7].Text = row[CapitalReportDS.NARRATIVE].ToString();
                    capitalTable.Cells[index, 8].Text = row[CapitalReportDS.UNITS].ToString();
                    capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
                    capitalTable.Cells[index, 9].Text = Convert.ToDouble(row[CapitalReportDS.NETVALUE]).ToString("N2");
                    capitalTable.Cells[index, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
                    index++;
                }

                capitalTable.Rows[index].Height = .2;
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(capitalTable);
                doc.Body.Children.Add(lineBreak);
            }

        }

        protected void GenerateCapitalReport(object sender, EventArgs e)
        {
           ClientCapitalReportClick(); 
        }

        protected void ClientCapitalReportClick()
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"].ToString()));
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);
            CapitalReportDS ds = new CapitalReportDS();

            if (InputEndDate.DateInput.SelectedDate.HasValue)
                 ds.EndDate =  AccountingFinancialYear.LastDayOfMonthFromDateTime(this.InputEndDate.DateInput.SelectedDate.Value);

            if (InputStartDate.DateInput.SelectedDate.HasValue)
                 ds.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(this.InputStartDate.DateInput.SelectedDate.Value);

            clientData.GetData(ds);
            this.PresentationData = ds;

            DataRow clientSummaryRow = ds.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0];

            string reportName = "Report_" + Guid.NewGuid().ToString();
            C1ReportViewer.RegisterDocument(reportName, MakeDocCapital);
            C1ReportViewerCapitalFlow.FileName = reportName;
            C1ReportViewerCapitalFlow.ReportName = reportName;
            InputEndDate.DbSelectedDate = ds.EndDate;
            InputStartDate.DbSelectedDate = ds.StartDate; 
            UMABroker.ReleaseBrokerManagedComponent(clientData);   
        }
    }
}
