﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eclipseonlineweb
{
    public partial class ReportsMaster : MasterPage
    {
        public string cid = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            cid = Request.QueryString["ins"];
            ((HiddenField)Master.FindControl("hfMainMenuText")).Value = "HOME&nbsp; > &nbsp;REPORTS";
        }
    }
}