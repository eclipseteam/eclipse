﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="ReportsMaster.master"
    AutoEventWireup="true" CodeBehind="ChessHoldingStatementReport.aspx.cs" Inherits="eclipseonlineweb.ChessHoldingStatementReport" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                      <td style="width: 5%">
                            <span class="riLabel">Start Date:</span><telerik:RadDatePicker ID="InputStartDate"
                                runat="server">
                            </telerik:RadDatePicker>
                        </td>
                        <td style="width: 5%">
                        <span class="riLabel">End Date:</span><telerik:RadDatePicker ID="InputEndDate"
                                runat="server">
                            </telerik:RadDatePicker>
                            
                        </td>
                        <td style="width: 5%"><br />
                        <telerik:RadButton runat="server" ID="BtnGenerateReport" OnClick="GenerateReport" Text="Generate Report"></telerik:RadButton>
                            
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <div id="MainView">
                <asp:Panel runat="server" ID="pnlDIV">
                    <c1:C1GridView AlternatingRowStyle-Font-Size="X-Small" FilterStyle-Font-Size="Smaller"
                        AllowSorting="true" OnSorting="SortGrid" RowStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small"
                        ID="InfoGrid" runat="server" AutogenerateColumns="false" ShowFilter="true" OnFiltering="FilterGrid"
                        Width="100%" ShowFooter="true" ScrollMode="None">
                        <Columns>
                            <c1:C1BoundField Width="5%" DataField="SecurityName" SortExpression="SecurityName"
                                HeaderText="Code">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </c1:C1BoundField>
                            <c1:C1BoundField Width="31%" DataField="Description" SortExpression="Description"
                                HeaderText="Description">
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" />
                            </c1:C1BoundField>
                            <c1:C1BoundField Width="8%" DataField="OpeningBalanceUnits" SortExpression="OpeningBalanceUnits"
                                DataFormatString="N0" HeaderText="Opening Units">
                                <HeaderStyle HorizontalAlign="Right" />
                                <ItemStyle HorizontalAlign="Right" />
                            </c1:C1BoundField>
                            <c1:C1BoundField Width="8%" DataField="OpeningUnitPrice" SortExpression="OpeningUnitPrice"
                                DataFormatString="N4" HeaderText="Unit Price">
                                <HeaderStyle HorizontalAlign="Right" />
                                <ItemStyle HorizontalAlign="Right" />
                            </c1:C1BoundField>
                            <c1:C1BoundField Width="8%" DataField="OpeningBalance" Aggregate="Sum" SortExpression="OpeningBalance"
                                HeaderText="Opening Bal." DataFormatString="C">
                                <ItemStyle HorizontalAlign="Right" />
                                <HeaderStyle HorizontalAlign="Right" />
                            </c1:C1BoundField>
                            <c1:C1BoundField Width="8%" DataField="Movement" Aggregate="Sum" SortExpression="Movement"
                                HeaderText="Movement" DataFormatString="C">
                                <ItemStyle HorizontalAlign="Right" />
                                <HeaderStyle HorizontalAlign="Right" />
                            </c1:C1BoundField>
                            <c1:C1BoundField Width="8%" DataField="ClosingUnits" SortExpression="ClosingUnits"
                                DataFormatString="N0" HeaderText="Cls. Units">
                                <HeaderStyle HorizontalAlign="Right" />
                                <ItemStyle HorizontalAlign="Right" />
                            </c1:C1BoundField>
                            <c1:C1BoundField Width="8%" DataField="ClosingUnitPrice" SortExpression="ClosingUnitPrice"
                                DataFormatString="N4" HeaderText="Unit Price">
                                <HeaderStyle HorizontalAlign="Right" />
                                <ItemStyle HorizontalAlign="Right" />
                            </c1:C1BoundField>
                            <c1:C1BoundField Width="8%" DataField="ClosingBalance" Aggregate="Sum" SortExpression="ClosingBalance"
                                HeaderText="Cls. Bal" DataFormatString="C">
                                <ItemStyle HorizontalAlign="Right" />
                                <HeaderStyle HorizontalAlign="Right" />
                            </c1:C1BoundField>
                            <c1:C1ButtonField Width="8%" Text="Report" ButtonType="Link" CommandName="Details">
                                <ItemStyle HorizontalAlign="Center" />
                            </c1:C1ButtonField>
                        </Columns>
                    </c1:C1GridView>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
