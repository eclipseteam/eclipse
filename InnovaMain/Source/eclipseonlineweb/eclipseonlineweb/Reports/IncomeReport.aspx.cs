﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using Oritax.TaxSimp.Utilities;

namespace eclipseonlineweb
{
    public partial class IncomeReport : UMABasePage
    {
       
        private List<AssetEntity> assets = null;
        string selectCostType = string.Empty; 

        public override void PopulatePage(DataSet ds)
        {
            this.cid = Request.QueryString["ins"].ToString();
            base.PopulatePage(ds);
            ReportClick();
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public static void AddFooter(C1PrintDocument doc)
        {
            doc.PageLayouts.PrintFooterOnLastPage = true;
            RenderTable footer = new RenderTable();
            footer.BreakBefore = BreakEnum.Page;
            footer.Rows[0].Height = .30;
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;
            footer.Rows[1].Height = 2.5;
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.TextAlignVert = AlignVertEnum.Center;

            footer.Cells[0, 0].Text = "FOR YOUR INFORMATION";
            footer.Cells[0, 0].Style.TextAlignHorz = AlignHorzEnum.Center;
            footer.Cells[0, 0].Style.FontSize = 12;

            footer.Cells[1, 0].Text = "\n1. This report provides an estimate of the realised and unrealised gains and losses of the portfolio for the period.  It does not constitute tax advice, nor an actual taxation outcome.  The report is provided for general information and is not intended to be considered as comprehensive tax information or relied upon without reference to an adviser and/or accountant.\n\nTax law depends upon an investors specific circumstances and the assumptions made in  this report may not be relevant to the investor." +
                                        "\n\n2. Information has been provided on a cash rather than accrual basis.  Income listed on the report are amounts that have been received as deposits in the linked CMA account and correctly identified as an income transaction." +
                                        "\n\n3. This report is provided by e-Clipse Online Pty Limited ABN 70 145 358 630, AFSL 357 306 (“e-Clipse”) and is based on information provided to e-Clipse by third parties.  Whilst every reasonable effort has been made by e-Clipse to ensure its accuracy, neither e-Clipse nor any of its related entities guarantee its accuracy nor accept any liability for any errors or omissions." +
                                        "\n\ne-Clipse Online Pty Ltd (ABN 70 145 358 630)\n3/36 Bydown Street, Neutral Bay, NSW 2089\nhttp://www.e-clipse.com.au\nP: +61 2 9346 4686";
            footer.Cells[1, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            footer.Cells[1, 0].Style.TextAlignVert = AlignVertEnum.Top;
            footer.Cells[1, 0].Style.FontSize = 9;
            footer.Cells[1, 0].Style.FontBold = false;

            doc.PageLayouts.LastPage = new PageLayout();
            doc.PageLayouts.LastPage.Document.Body.Children.Add(footer);
        }

        public C1PrintDocument MakeDoc()
        {
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            DataRow clientSummaryRow = this.PresentationData.Tables[LossAndGainsReportDS.CLIENTSUMMARYTABLE].Rows[0];
            AddClientHeaderToReport(doc, clientSummaryRow, ((LossAndGainsReportDS)this.PresentationData).DateInfo(), "INCOME & EXPENSE REPORT");
            RenderText gridHeaderTotal = new RenderText();
            if(this.comboBoxAccType.SelectedItem != null)
                gridHeaderTotal.Text = "*Accounting Method - " + comboBoxAccType.SelectedItem.Text + "\n\n";
            else
                gridHeaderTotal.Text = "*Accounting Method - " + comboBoxAccType.Text + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 8;
            gridHeaderTotal.Style.FontBold = true;
            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(lineBreakTotal);

            SetIncomeReport(doc);

            AddFooter(doc);
            return doc;
        }

        private void SetIncomeReport(C1PrintDocument doc)
        {
            DataView secSummaryTableViewOverall = new DataView(PresentationData.Tables[LossAndGainsReportDS.TAXREPORTINGMATRIXTABLE]);
            secSummaryTableViewOverall.Sort = LossAndGainsReportDS.ACCOUNTTYPE + " ASC, " + LossAndGainsReportDS.ACCOUNTNO + " ASC";

            SummmaryTableOverall(doc, secSummaryTableViewOverall.ToTable(), PresentationData.Tables[LossAndGainsReportDS.INCOMEBREAKDOWN], PresentationData.Tables[LossAndGainsReportDS.EXPENSEBREAKDOWN]);

            DataView incomeSummaryView = new DataView(PresentationData.Tables[LossAndGainsReportDS.INCOMEBREAKDOWN]);
            incomeSummaryView.Sort = LossAndGainsReportDS.SECTYPE + " ASC";
        }

        private static void SummmaryTableOverall(C1PrintDocument doc, DataTable overallSummary, DataTable incomeBreakDown, DataTable expenseBreakDown)
        {
            RenderTable secSumTableIncomeSection = new RenderTable();
            secSumTableIncomeSection.RowGroups[0, 1].PageHeader = true;
            secSumTableIncomeSection.Style.FontSize = 8;
            secSumTableIncomeSection.Rows[0].Height = .2;
            secSumTableIncomeSection.Rows[0].Style.FontBold = true;
            secSumTableIncomeSection.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSumTableIncomeSection.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSumTableIncomeSection.Rows[0].Style.FontBold = true;
            secSumTableIncomeSection.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSumTableIncomeSection.Cells[0, 0].Text = "Income Category";
            secSumTableIncomeSection.Cells[0, 1].Text = "Amount";
            secSumTableIncomeSection.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            RenderTable secSumTableExpenseSection = new RenderTable();
            secSumTableExpenseSection.RowGroups[0, 1].PageHeader = true;
            secSumTableExpenseSection.Style.FontSize = 8;
            secSumTableExpenseSection.Rows[0].Height = .2;
            secSumTableExpenseSection.Rows[0].Style.FontBold = true;
            secSumTableExpenseSection.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSumTableExpenseSection.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSumTableExpenseSection.Rows[0].Style.FontBold = true;
            secSumTableExpenseSection.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSumTableExpenseSection.Cells[0, 0].Text = "Expense Category";
            secSumTableExpenseSection.Cells[0, 1].Text = "Amount";
            secSumTableExpenseSection.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            RenderTable incomeTableBySec = new RenderTable();
            incomeTableBySec.RowGroups[0, 1].PageHeader = true;
            incomeTableBySec.Style.FontSize = 8;
            incomeTableBySec.Rows[0].Height = .2;
            incomeTableBySec.Rows[0].Style.FontBold = true;
            incomeTableBySec.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            incomeTableBySec.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            incomeTableBySec.Rows[0].Style.FontBold = true;
            incomeTableBySec.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            incomeTableBySec.Cells[0, 0].Text = "Sec";
            incomeTableBySec.Cells[0, 1].Text = "Income Category";
            incomeTableBySec.Cells[0, 2].Text = "Amount";
            incomeTableBySec.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
 
            RenderTable incomeBreakdownOverall = new RenderTable();
            incomeBreakdownOverall.Style.FontSize = 6;
            incomeBreakdownOverall.RowGroups[0, 1].PageHeader = true;
            incomeBreakdownOverall.Rows[0].Height = .2;
            incomeBreakdownOverall.Rows[0].Style.FontBold = true;
            incomeBreakdownOverall.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            incomeBreakdownOverall.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            incomeBreakdownOverall.Rows[0].Style.FontBold = true;
            incomeBreakdownOverall.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            incomeBreakdownOverall.Cells[0, 0].Text = "Desc.";
            incomeBreakdownOverall.Cells[0, 1].Text = "Sec. Code";
            incomeBreakdownOverall.Cells[0, 2].Text = "Inc. Type";

            incomeBreakdownOverall.Cells[0, 3].Text = "Record Date";
            incomeBreakdownOverall.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Center;

            incomeBreakdownOverall.Cells[0, 4].Text = "Payment Date";
            incomeBreakdownOverall.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Center;
            incomeBreakdownOverall.Cells[0, 5].Text = "Source";
            incomeBreakdownOverall.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Center;

            incomeBreakdownOverall.Cells[0, 6].Text = "Assc. Acc.";
            incomeBreakdownOverall.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Center;

            incomeBreakdownOverall.Cells[0, 7].Text = "Trans. Date";
            incomeBreakdownOverall.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Center;

            incomeBreakdownOverall.Cells[0, 8].Text = "$ Per Share";
            incomeBreakdownOverall.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

            incomeBreakdownOverall.Cells[0, 9].Text = "Franked";
            incomeBreakdownOverall.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

            incomeBreakdownOverall.Cells[0, 10].Text = "Unfranked";
            incomeBreakdownOverall.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

            incomeBreakdownOverall.Cells[0, 11].Text = "Franking Credit";
            incomeBreakdownOverall.Cells[0, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

            incomeBreakdownOverall.Cells[0, 12].Text = "Income Paid";
            incomeBreakdownOverall.Cells[0, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

            incomeBreakdownOverall.Cols[0].Width = 7.5;
            incomeBreakdownOverall.Cols[1].Width = 2.2;
            incomeBreakdownOverall.Cols[2].Width = 2.2;
            incomeBreakdownOverall.Cols[3].Width = 2.2;
            incomeBreakdownOverall.Cols[4].Width = 2.2;
            incomeBreakdownOverall.Cols[5].Width = 2;
            incomeBreakdownOverall.Cols[6].Width = 2;
            incomeBreakdownOverall.Cols[7].Width = 2.2;
            incomeBreakdownOverall.Cols[8].Width = 2.2;
            incomeBreakdownOverall.Cols[9].Width = 2.2;
            incomeBreakdownOverall.Cols[10].Width = 2.2;
            incomeBreakdownOverall.Cols[11].Width = 2.5;
            incomeBreakdownOverall.Cols[12].Width = 2;
          
            DataView incomeView = new DataView(incomeBreakDown);
            incomeView.Sort = LossAndGainsReportDS.SECTYPE +" ASC, " + LossAndGainsReportDS.PAYMENTDATE + " ASC";
            DataTable sortedIncomeView = incomeView.ToTable();

            int totalIndexIncome = 1;
            decimal incomeTotal = 0;
            decimal frankedTotal = 0;
            decimal unfrankedTotal = 0;
            decimal frankingCreditTotal = 0; 

            foreach(DataRow secRow in sortedIncomeView.Rows)
            {
                incomeBreakdownOverall.Cells[totalIndexIncome, 0].Text = secRow[LossAndGainsReportDS.DESCRIPTION].ToString();
                incomeBreakdownOverall.Cells[totalIndexIncome, 1].Text = secRow[LossAndGainsReportDS.SECNAME].ToString();

                incomeBreakdownOverall.Cells[totalIndexIncome, 2].Text = secRow[LossAndGainsReportDS.INCOMETYPE].ToString();

                if (secRow[LossAndGainsReportDS.RECORDDATE] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 3].Text = "-";
                else
                    incomeBreakdownOverall.Cells[totalIndexIncome, 3].Text = ((DateTime)secRow[LossAndGainsReportDS.RECORDDATE]).ToString("dd/MM/yyyy");
                incomeBreakdownOverall.Cells[totalIndexIncome, 3].Style.TextAlignHorz = AlignHorzEnum.Center;

                if (secRow[LossAndGainsReportDS.PAYMENTDATE] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 4].Text = "-";
                else
                    incomeBreakdownOverall.Cells[totalIndexIncome, 4].Text = ((DateTime)secRow[LossAndGainsReportDS.PAYMENTDATE]).ToString("dd/MM/yyyy");
                incomeBreakdownOverall.Cells[totalIndexIncome, 4].Style.TextAlignHorz = AlignHorzEnum.Center;
    
                incomeBreakdownOverall.Cells[totalIndexIncome, 5].Text = secRow[LossAndGainsReportDS.SOURCE].ToString();
                incomeBreakdownOverall.Cells[totalIndexIncome, 5].Style.TextAlignHorz = AlignHorzEnum.Center;

                incomeBreakdownOverall.Cells[totalIndexIncome, 6].Text = secRow[LossAndGainsReportDS.CASH_BANKACC].ToString();

                if (secRow[LossAndGainsReportDS.CASH_PAYMENTDATE] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 7].Text = "-";
                else
                    incomeBreakdownOverall.Cells[totalIndexIncome, 7].Text = ((DateTime)secRow[LossAndGainsReportDS.CASH_PAYMENTDATE]).ToString("dd/MM/yyyy");
                incomeBreakdownOverall.Cells[totalIndexIncome, 7].Style.TextAlignHorz = AlignHorzEnum.Center;

                if (secRow[LossAndGainsReportDS.DIVAMOUNTPERSHARE] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 8].Text = "-";
                else
                    incomeBreakdownOverall.Cells[totalIndexIncome, 8].Text = ((decimal)secRow[LossAndGainsReportDS.DIVAMOUNTPERSHARE]).ToString("N4");
                incomeBreakdownOverall.Cells[totalIndexIncome, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal frankedAmount = 0;

                if (secRow[LossAndGainsReportDS.FRANKEDAMOUNT] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 9].Text = "-";
                else
                {
                    frankedAmount = (decimal)secRow[LossAndGainsReportDS.FRANKEDAMOUNT];
                    incomeBreakdownOverall.Cells[totalIndexIncome, 9].Text = frankedAmount.ToString("C");
                }

                incomeBreakdownOverall.Cells[totalIndexIncome, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
                frankedTotal += frankedAmount;

                decimal unFrankedAmount = 0;

                if (secRow[LossAndGainsReportDS.UNFRANKEDAMOUNT] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 10].Text = "-";
                else
                {
                    unFrankedAmount = (decimal)secRow[LossAndGainsReportDS.UNFRANKEDAMOUNT];
                    incomeBreakdownOverall.Cells[totalIndexIncome, 10].Text = unFrankedAmount.ToString("C");
                }

                incomeBreakdownOverall.Cells[totalIndexIncome, 10].Style.TextAlignHorz = AlignHorzEnum.Right;
                unfrankedTotal += unFrankedAmount;

                decimal frankingCreditAmount = 0;

                if (secRow[LossAndGainsReportDS.FRANKINGCREDIT] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 11].Text = "-";
                else
                {
                    frankingCreditAmount = (decimal)secRow[LossAndGainsReportDS.FRANKINGCREDIT];
                    incomeBreakdownOverall.Cells[totalIndexIncome, 11].Text = frankingCreditAmount.ToString("C");
                }
                incomeBreakdownOverall.Cells[totalIndexIncome, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
                frankingCreditTotal += frankingCreditAmount;


                if (secRow[LossAndGainsReportDS.INCPAID] is DBNull)
                    incomeBreakdownOverall.Cells[totalIndexIncome, 12].Text = "-";
                else
                {
                    incomeBreakdownOverall.Cells[totalIndexIncome, 12].Text = ((decimal)secRow[LossAndGainsReportDS.INCPAID]).ToString("C");
                    incomeTotal += (decimal)secRow[LossAndGainsReportDS.INCPAID];
                }
                incomeBreakdownOverall.Cells[totalIndexIncome, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

                totalIndexIncome++;
            }

            incomeBreakdownOverall.Rows[totalIndexIncome].Height = .2;
            incomeBreakdownOverall.Rows[totalIndexIncome].Style.FontBold = true;
            incomeBreakdownOverall.Rows[totalIndexIncome].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            incomeBreakdownOverall.Rows[totalIndexIncome].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            incomeBreakdownOverall.Rows[totalIndexIncome].Style.FontBold = true;
            incomeBreakdownOverall.Rows[totalIndexIncome].Style.TextAlignVert = AlignVertEnum.Center;

            incomeBreakdownOverall.Cells[totalIndexIncome, 0].Text = "TOTAL";

            incomeBreakdownOverall.Cells[totalIndexIncome, 9].Text = frankedTotal.ToString("C");
            incomeBreakdownOverall.Cells[totalIndexIncome, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
            incomeBreakdownOverall.Cells[totalIndexIncome, 10].Text = unfrankedTotal.ToString("C");
            incomeBreakdownOverall.Cells[totalIndexIncome, 10].Style.TextAlignHorz = AlignHorzEnum.Right;
            incomeBreakdownOverall.Cells[totalIndexIncome, 11].Text = frankingCreditTotal.ToString("C");
            incomeBreakdownOverall.Cells[totalIndexIncome, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

            incomeBreakdownOverall.Cells[totalIndexIncome, 12].Text = incomeTotal.ToString("C");
            incomeBreakdownOverall.Cells[totalIndexIncome, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

            #region Expense BreakDown

            RenderTable expenseBreakdownOverall = new RenderTable();
            expenseBreakdownOverall.Style.FontSize = 6;
            expenseBreakdownOverall.RowGroups[0, 1].PageHeader = true;
            expenseBreakdownOverall.Rows[0].Height = .2;
            expenseBreakdownOverall.Rows[0].Style.FontBold = true;
            expenseBreakdownOverall.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            expenseBreakdownOverall.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            expenseBreakdownOverall.Rows[0].Style.FontBold = true;
            expenseBreakdownOverall.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            expenseBreakdownOverall.Cells[0, 0].Text = "BSB";
            expenseBreakdownOverall.Cells[0, 1].Text = "Account No";
            expenseBreakdownOverall.Cells[0, 2].Text = "Tran. Date";
            expenseBreakdownOverall.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Center;
            expenseBreakdownOverall.Cells[0, 3].Text = "Tran. Type";
            expenseBreakdownOverall.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Center;
            expenseBreakdownOverall.Cells[0, 4].Text = "Tran. Desc.";
            expenseBreakdownOverall.Cells[0, 4 ].Style.TextAlignHorz = AlignHorzEnum.Center;
            expenseBreakdownOverall.Cells[0, 5].Text = "Category";
            expenseBreakdownOverall.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Center;
            expenseBreakdownOverall.Cells[0, 6].Text = "Amount";
            expenseBreakdownOverall.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
            expenseBreakdownOverall.Cells[0, 7].Text = "GST";
            expenseBreakdownOverall.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
            expenseBreakdownOverall.Cells[0, 8].Text = "Total";
            expenseBreakdownOverall.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
            expenseBreakdownOverall.Cells[0, 9].Text = "Comment";
            expenseBreakdownOverall.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Center;

            expenseBreakdownOverall.Cols[0].Width = 2.2;
            expenseBreakdownOverall.Cols[1].Width = 2.2;
            expenseBreakdownOverall.Cols[2].Width = 2.2;
            expenseBreakdownOverall.Cols[3].Width = 3;
            expenseBreakdownOverall.Cols[4].Width = 3;
            expenseBreakdownOverall.Cols[5].Width = 3;
            expenseBreakdownOverall.Cols[6].Width = 2;
            expenseBreakdownOverall.Cols[7].Width = 2.2;
            expenseBreakdownOverall.Cols[8].Width = 2.2;
            expenseBreakdownOverall.Cols[9].Width = 4;
            
            DataView expenseView = new DataView(expenseBreakDown);
            expenseView.Sort = LossAndGainsReportDS.SYSTEMTRANSACTIONTYPE + " ASC, " + LossAndGainsReportDS.PAYMENTDATE + " ASC";
            DataTable sortedExpenseView = expenseView.ToTable();

            int totalIndexExpense = 1;
            decimal expenseTotal = 0;
            decimal gstTotal = 0;
            decimal totolTotal = 0;

            foreach (DataRow secRow in sortedExpenseView.Rows)
            {
                expenseBreakdownOverall.Cells[totalIndexExpense, 0].Text = secRow[LossAndGainsReportDS.BSB].ToString();
                expenseBreakdownOverall.Cells[totalIndexExpense, 1].Text = secRow[LossAndGainsReportDS.CASH_BANKACC].ToString();

                if (secRow[LossAndGainsReportDS.PAYMENTDATE] is DBNull)
                    expenseBreakdownOverall.Cells[totalIndexExpense, 2].Text = "-";
                else
                    expenseBreakdownOverall.Cells[totalIndexExpense, 2].Text = ((DateTime)secRow[LossAndGainsReportDS.PAYMENTDATE]).ToString("dd/MM/yyyy");
                expenseBreakdownOverall.Cells[totalIndexExpense, 2].Style.TextAlignHorz = AlignHorzEnum.Center;

                expenseBreakdownOverall.Cells[totalIndexExpense, 3].Text = secRow[LossAndGainsReportDS.TRANSACTIONTYPE].ToString();
                expenseBreakdownOverall.Cells[totalIndexExpense, 3].Style.TextAlignHorz = AlignHorzEnum.Center;

                expenseBreakdownOverall.Cells[totalIndexExpense, 4].Text = secRow[LossAndGainsReportDS.SYSTEMTRANSACTIONTYPE].ToString();
                expenseBreakdownOverall.Cells[totalIndexExpense, 4].Style.TextAlignHorz = AlignHorzEnum.Center;

                expenseBreakdownOverall.Cells[totalIndexExpense, 5].Text = secRow[LossAndGainsReportDS.TRANCATEGORY].ToString();
                expenseBreakdownOverall.Cells[totalIndexExpense, 5].Style.TextAlignHorz = AlignHorzEnum.Center;

                expenseBreakdownOverall.Cells[totalIndexExpense, 6].Text = ((decimal)secRow[LossAndGainsReportDS.EXPENSE]).ToString("c");
                expenseBreakdownOverall.Cells[totalIndexExpense, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                expenseTotal += (decimal)secRow[LossAndGainsReportDS.EXPENSE]; 
                
                expenseBreakdownOverall.Cells[totalIndexExpense, 7].Text = ((decimal)secRow[LossAndGainsReportDS.GST]).ToString("c");
                expenseBreakdownOverall.Cells[totalIndexExpense, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                gstTotal += (decimal)secRow[LossAndGainsReportDS.GST]; 

                expenseBreakdownOverall.Cells[totalIndexExpense, 8].Text = ((decimal)secRow[LossAndGainsReportDS.TOTAL]).ToString("c");
                expenseBreakdownOverall.Cells[totalIndexExpense, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
                totolTotal += (decimal)secRow[LossAndGainsReportDS.TOTAL]; 

                expenseBreakdownOverall.Cells[totalIndexExpense, 9].Text = secRow[LossAndGainsReportDS.COMMENT].ToString();
                expenseBreakdownOverall.Cells[totalIndexExpense, 9].Style.TextAlignHorz = AlignHorzEnum.Center;

                totalIndexExpense++;
            }

            expenseBreakdownOverall.Rows[totalIndexExpense].Height = .2;
            expenseBreakdownOverall.Rows[totalIndexExpense].Style.FontBold = true;
            expenseBreakdownOverall.Rows[totalIndexExpense].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            expenseBreakdownOverall.Rows[totalIndexExpense].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            expenseBreakdownOverall.Rows[totalIndexExpense].Style.FontBold = true;
            expenseBreakdownOverall.Rows[totalIndexExpense].Style.TextAlignVert = AlignVertEnum.Center;

            expenseBreakdownOverall.Cells[totalIndexExpense, 0].Text = "TOTAL";

            expenseBreakdownOverall.Cells[totalIndexExpense, 6].Text = expenseTotal.ToString("C");
            expenseBreakdownOverall.Cells[totalIndexExpense, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
            expenseBreakdownOverall.Cells[totalIndexExpense, 7].Text = gstTotal.ToString("C");
            expenseBreakdownOverall.Cells[totalIndexExpense, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
            expenseBreakdownOverall.Cells[totalIndexExpense, 8].Text = totolTotal.ToString("C");
            expenseBreakdownOverall.Cells[totalIndexExpense, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

            #endregion 

            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Style.FontSize = 7;

            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Acc Type";
            secSummaryTotalTable.Cells[0, 1].Text = "Account NO";

            secSummaryTotalTable.Cells[0, 2].Text = "Opening";
            secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 3].Text = "Investment";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 4].Text = "Tran. In/Out";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 5].Text = "Interest";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 6].Text = "Rental";
            secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 7].Text = "Distribution";
            secSummaryTotalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 8].Text = "Dividend";
            secSummaryTotalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 9].Text = "Expenses";
            secSummaryTotalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 10].Text = "Change in Val.";
            secSummaryTotalTable.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 11].Text = "Closing";
            secSummaryTotalTable.Cells[0, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 12].Text = "Unrealised";
            secSummaryTotalTable.Cells[0, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 13].Text = "Realised";
            secSummaryTotalTable.Cells[0, 13].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 4;
            secSummaryTotalTable.Cols[2].Width = 2.5;
            secSummaryTotalTable.Cols[3].Width = 2.5;
            secSummaryTotalTable.Cols[4].Width = 2.5;
            secSummaryTotalTable.Cols[5].Width = 2.5;
            secSummaryTotalTable.Cols[6].Width = 2.5;
            secSummaryTotalTable.Cols[7].Width = 2.5;
            secSummaryTotalTable.Cols[8].Width = 2.5;
            secSummaryTotalTable.Cols[9].Width = 2.5;
            secSummaryTotalTable.Cols[10].Width = 2.5;
            secSummaryTotalTable.Cols[11].Width = 2.5;
            secSummaryTotalTable.Cols[12].Width = 2.5;
            secSummaryTotalTable.Cols[13].Width = 2.5;
            
            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Income Summary\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 10;
            gridHeaderTotal.Style.FontBold = true;

            RenderText gridHeaderTotalExpense = new RenderText();
            gridHeaderTotalExpense.Text = "Expense Summary\n\n";
            gridHeaderTotalExpense.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotalExpense.Style.FontSize = 10;
            gridHeaderTotalExpense.Style.FontBold = true;

            RenderText gridHeaderIncomeTransactions = new RenderText();
            gridHeaderIncomeTransactions.Text = "Income Tranactions\n\n";
            gridHeaderIncomeTransactions.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderIncomeTransactions.Style.FontSize = 10;
            gridHeaderIncomeTransactions.Style.FontBold = true;

            RenderText gridHeaderExpenseTransactions = new RenderText();
            gridHeaderExpenseTransactions.Text = "Expense Transactions\n\n";
            gridHeaderExpenseTransactions.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderExpenseTransactions.Style.FontSize = 10;
            gridHeaderExpenseTransactions.Style.FontBold = true;

            RenderText gridHeaderTotal2 = new RenderText();
            gridHeaderTotal2.Text = "Unrealised / Realised Gains & Losses Summary\n\n";
            gridHeaderTotal2.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal2.Style.FontSize = 10;
            gridHeaderTotal2.Style.FontBold = true;

            decimal openingBalTotal = 0;
            decimal investTotal = 0;
            decimal transferInTotal = 0;
            decimal transferOutTotal = 0;
            decimal changeInMktValTotal = 0;
            decimal interestTotal = 0;
            decimal rentalTotal = 0;
            decimal disTotal = 0;
            decimal divTotal = 0;
            decimal expTotal = 0;
            decimal realisedTotal = 0;
            decimal unRealisedTotal = 0;
            decimal closingTotal = 0;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n";

            RenderText lineBreakTotal2 = new RenderText();
            lineBreakTotal2.Text = "\n\n\n";

            RenderText lineBreakTotal3 = new RenderText();
            lineBreakTotal3.Text = "\n\n";

            RenderText lineBreakTotal4 = new RenderText();
            lineBreakTotal4.Text = "\n\n";

            RenderText lineBreakTotal5 = new RenderText();
            lineBreakTotal5.Text = "\n\n";

            RenderText lineBreakTotal6 = new RenderText();
            lineBreakTotal6.Text = "\n\n";
            
            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSumTableIncomeSection);
            doc.Body.Children.Add(lineBreakTotal4);
            doc.Body.Children.Add(gridHeaderTotalExpense);
            doc.Body.Children.Add(secSumTableExpenseSection);
            doc.Body.Children.Add(lineBreakTotal3);
            doc.Body.Children.Add(gridHeaderIncomeTransactions);
            doc.Body.Children.Add(incomeBreakdownOverall);
            doc.Body.Children.Add(lineBreakTotal5);
            doc.Body.Children.Add(gridHeaderExpenseTransactions);
            doc.Body.Children.Add(expenseBreakdownOverall);
            doc.Body.Children.Add(lineBreakTotal6);
            doc.Body.Children.Add(lineBreakTotal);
            doc.Body.Children.Add(gridHeaderTotal2);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal2);
            int totalIndex = 1;

            foreach (DataRow secRow in overallSummary.Rows)
            {
                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[LossAndGainsReportDS.ACCOUNTTYPE].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[LossAndGainsReportDS.ACCOUNTNO].ToString();

                decimal totalValue = 0;

                if (secRow[LossAndGainsReportDS.OPENINGBALANCE] != null & secRow[LossAndGainsReportDS.OPENINGBALANCE] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.OPENINGBALANCE]);
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                openingBalTotal += totalValue;


                totalValue = 0;

                if (secRow[LossAndGainsReportDS.INVESTMENT] != null & secRow[LossAndGainsReportDS.INVESTMENT] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.INVESTMENT]);
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                investTotal += totalValue;

                totalValue = 0;
                decimal transgerIn = 0; 
                decimal transferOut = 0; 
                if (secRow[LossAndGainsReportDS.TRANSFERIN] != null & secRow[LossAndGainsReportDS.TRANSFERIN] != DBNull.Value)
                    transgerIn = Convert.ToDecimal(secRow[LossAndGainsReportDS.TRANSFERIN]);
                if (secRow[LossAndGainsReportDS.TRANSFEROUT] != null & secRow[LossAndGainsReportDS.TRANSFEROUT] != DBNull.Value)
                    transferOut = Convert.ToDecimal(secRow[LossAndGainsReportDS.TRANSFEROUT]);

                secSummaryTotalTable.Cells[totalIndex, 4].Text = (transgerIn + transferOut).ToString("C");
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                transferInTotal += (transgerIn + transferOut);

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.INTEREST] != null & secRow[LossAndGainsReportDS.INTEREST] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.INTEREST]);
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                interestTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.RENTAL] != null & secRow[LossAndGainsReportDS.RENTAL] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.RENTAL]);
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                rentalTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.DISTRIBUTION] != null & secRow[LossAndGainsReportDS.DISTRIBUTION] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.DISTRIBUTION]);
                    secSummaryTotalTable.Cells[totalIndex, 7].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 7].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                disTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.DIVIDEND] != null & secRow[LossAndGainsReportDS.DIVIDEND] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.DIVIDEND]);
                    secSummaryTotalTable.Cells[totalIndex, 8].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 8].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                divTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.EXPENSES] != null & secRow[LossAndGainsReportDS.EXPENSES] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.EXPENSES]);
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                expTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.CLOSINGBALANCE] != null & secRow[LossAndGainsReportDS.CLOSINGBALANCE] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.CLOSINGBALANCE]);
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

                closingTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.CHANGEINMKT] != null & secRow[LossAndGainsReportDS.CHANGEINMKT] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.CHANGEINMKT]);
                    secSummaryTotalTable.Cells[totalIndex, 10].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 10].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                changeInMktValTotal += totalValue;

                
                totalValue = 0;

                if (secRow[LossAndGainsReportDS.UNREALISED] != null & secRow[LossAndGainsReportDS.UNREALISED] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISED]);
                    secSummaryTotalTable.Cells[totalIndex, 12].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 12].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

                unRealisedTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.REALISED] != null & secRow[LossAndGainsReportDS.REALISED] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISED]);
                    secSummaryTotalTable.Cells[totalIndex, 13].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 13].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 13].Style.TextAlignHorz = AlignHorzEnum.Right;
                
                realisedTotal += totalValue;


                totalIndex++;
            }

            var groupedExpenses = expenseBreakDown.Select().GroupBy(row => row[LossAndGainsReportDS.SYSTEMTRANSACTIONTYPE]);
            int expenseItemCount = 1;
            decimal expenseSummaryTotal = 0; 

            foreach (var expenseItem in groupedExpenses)
            {
                secSumTableExpenseSection.Cells[expenseItemCount, 0].Text = expenseItem.Key.ToString();
                decimal expenseItemTotal = expenseItem.Sum(row => (decimal)row[LossAndGainsReportDS.TOTAL]);
                secSumTableExpenseSection.Cells[expenseItemCount, 1].Text = expenseItemTotal.ToString("C");
                secSumTableExpenseSection.Cells[expenseItemCount, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

                expenseSummaryTotal += expenseItemTotal; 
                expenseItemCount++;
            }

            secSumTableExpenseSection.Rows[expenseItemCount].Height = .2;
            secSumTableExpenseSection.Rows[expenseItemCount].Style.FontBold = true;
            secSumTableExpenseSection.Rows[expenseItemCount].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSumTableExpenseSection.Rows[expenseItemCount].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSumTableExpenseSection.Rows[expenseItemCount].Style.FontBold = true;
            secSumTableExpenseSection.Rows[expenseItemCount].Style.TextAlignVert = AlignVertEnum.Center;

            secSumTableExpenseSection.Cells[expenseItemCount, 0].Text = "TOTAL EXPENSE";
            secSumTableExpenseSection.Cells[expenseItemCount, 1].Text = expenseSummaryTotal.ToString("C");
            secSumTableExpenseSection.Cells[expenseItemCount, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSumTableIncomeSection.Cells[1, 0].Text = "Interest";
            secSumTableIncomeSection.Cells[1, 1].Text = interestTotal.ToString("C");
            secSumTableIncomeSection.Cells[1, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSumTableIncomeSection.Cells[2, 0].Text = "Dividend";
            secSumTableIncomeSection.Cells[2, 1].Text =  divTotal.ToString("C");
            secSumTableIncomeSection.Cells[2, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSumTableIncomeSection.Cells[3, 0].Text = "Distribution";
            secSumTableIncomeSection.Cells[3, 1].Text = disTotal.ToString("C");
            secSumTableIncomeSection.Cells[3, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSumTableIncomeSection.Cells[4, 0].Text = "Rental";
            secSumTableIncomeSection.Cells[4, 1].Text = rentalTotal.ToString("C");
            secSumTableIncomeSection.Cells[4, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSumTableIncomeSection.Cells[5, 0].Text = "Realised Gains (Securities & Funds)";
            secSumTableIncomeSection.Cells[5, 1].Text =  realisedTotal.ToString("C");
            secSumTableIncomeSection.Cells[5, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSumTableIncomeSection.Rows[6].Height = .2;
            secSumTableIncomeSection.Rows[6].Style.FontBold = true;
            secSumTableIncomeSection.Rows[6].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSumTableIncomeSection.Rows[6].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSumTableIncomeSection.Rows[6].Style.FontBold = true;
            secSumTableIncomeSection.Rows[6].Style.TextAlignVert = AlignVertEnum.Center;

            secSumTableIncomeSection.Cells[6, 0].Text = "TOTAL INCOME";
            secSumTableIncomeSection.Cells[6, 1].Text = (realisedTotal + disTotal + divTotal + interestTotal + rentalTotal).ToString("C");
            secSumTableIncomeSection.Cells[6, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            secSummaryTotalTable.Cells[totalIndex, 1].Text = "";

            secSummaryTotalTable.Cells[totalIndex, 2].Text = openingBalTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 3].Text = investTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 4].Text = (transferInTotal+transferOutTotal).ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 5].Text = interestTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 6].Text = rentalTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 7].Text = disTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 8].Text = divTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 9].Text = expTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 10].Text = changeInMktValTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 11].Text = closingTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 12].Text = unRealisedTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 12].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 13].Text = realisedTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 13].Style.TextAlignHorz = AlignHorzEnum.Right;
        }

        protected void GenerateReport(object sender, EventArgs e)
        {
            ReportClick(); 
        }

        protected void ReportClick()
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"].ToString()));
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            this.assets = organization.Assets;
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);
            LossAndGainsReportDS ds = new LossAndGainsReportDS();

            if (comboBoxAccType.SelectedValue == "Accrual")
                ds.AcountingMethod = AcountingMethodType.Accrual; 
            else if (comboBoxAccType.SelectedValue == "Mixed")
                ds.AcountingMethod = AcountingMethodType.Mixed;
            else
                ds.AcountingMethod = AcountingMethodType.Cash; 
           
            if (InputStartDate.DateInput.SelectedDate.HasValue)
                 ds.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(this.InputStartDate.DateInput.SelectedDate.Value);

            if (InputEndDate.DateInput.SelectedDate.HasValue)
                 ds.EndDate =  AccountingFinancialYear.LastDayOfMonthFromDateTime(this.InputEndDate.DateInput.SelectedDate.Value);

            clientData.GetData(ds);
            this.PresentationData = ds;

            DataRow clientSummaryRow = ds.Tables[LossAndGainsReportDS.CLIENTSUMMARYTABLE].Rows[0];

            string reportName = "Report_" + Guid.NewGuid().ToString();
            C1ReportViewer.RegisterDocument(reportName, MakeDoc);
            this.C1ReportViewerAssetClass.FileName = reportName;
            C1ReportViewerAssetClass.ReportName = reportName;
            InputEndDate.DbSelectedDate = ds.EndDate;
            InputStartDate.DbSelectedDate = ds.StartDate; 
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }
    }
}
