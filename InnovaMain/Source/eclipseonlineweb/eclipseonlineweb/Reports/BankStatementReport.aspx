﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="ReportsMaster.master"
    AutoEventWireup="true" CodeBehind="BankStatementReport.aspx.cs" Inherits="eclipseonlineweb.BankStatementReport" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer"
    TagPrefix="C1ReportViewer" %>
<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" Assembly="C1.Web.Wijmo.Controls.4, Version=4.0.20121.56, Culture=neutral, PublicKeyToken=9b75583953471eea" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                      <td style="width: 5%">
                            <span class="riLabel">Start Date:</span><telerik:RadDatePicker ID="InputStartDate" Width="120px"
                                runat="server">
                            </telerik:RadDatePicker>
                        </td>
                        <td style="width: 5%">
                        <span class="riLabel">End Date:</span><telerik:RadDatePicker ID="InputEndDate" Width="120px"
                                runat="server">
                            </telerik:RadDatePicker>
                            
                        </td>
                         <td style="width: 5%">

                         <span class="riLabel">Bank Accounts:</span>
                         <telerik:RadComboBox runat="server" ID="comboAccounts" Width="440px"> </telerik:RadComboBox>
                          
                        </td>
                        <td style="width: 5%"><br />
                        <telerik:RadButton runat="server" ID="BtnGenerateReport" OnClick="GenerateReport" Text="Generate Report"></telerik:RadButton>
                            
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 80%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <C1ReportViewer:C1ReportViewer CollapseToolsPanel="true" Cache-Enabled="false" Cache-ShareBetweenSessions="false"
                FileName="InMemoryBasicTable" runat="server" ID="C1ReportViewer1" Height="550px"
                Width="100%" Zoom="75%">
            </C1ReportViewer:C1ReportViewer>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
