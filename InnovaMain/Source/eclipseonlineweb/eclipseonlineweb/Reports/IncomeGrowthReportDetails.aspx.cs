﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Utilities;
using Telerik.Web.UI;

namespace eclipseonlineweb
{
    public partial class IncomeGrowthReportDetails : UMABasePage
    {
        public override void LoadPage()
        {
            this.cid = Request.QueryString["ins"].ToString();

            if (!this.IsPostBack)
                this.PopulatePage(this.PresentationData);

            ScriptManager sm = ScriptManager.GetCurrent(Page);

            
        }


        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public override void PopulatePage(DataSet ds)
        {
            base.PopulatePage(ds);
            ClientPerformanceReportClick();
        }

        public C1PrintDocument MakeDocPerformance()
        {
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            DataRow clientSummaryRow = this.PresentationData.Tables[CapitalReportDS.CLIENTSUMMARYTABLE].Rows[0];
            AddClientHeaderToReport(doc, clientSummaryRow, ((CapitalReportDS)this.PresentationData).DateInfo(),"INCOME vs. GROWTH REPORT");

            DataTable capitalSummaryTable = this.PresentationData.Tables[CapitalReportDS.CAPITALFLOWSUMMARY];
            DataTable perInomevsGrowthYearTable = this.PresentationData.Tables[PerfReportDS.PERFSUMMARYINCOMEGROWTHTABLE];
            DataTable performSummaryTable = this.PresentationData.Tables[PerfReportDS.PERFSUMMARYTABLE];
            DataTable capitalSummaryCatTable = this.PresentationData.Tables[CapitalReportDS.CAPITALFLOWSUMMARYCAT];
            DataTable bankTranTable = this.PresentationData.Tables[CapitalReportDS.BANKTRANSACTIONSTABLE];
            DataTable asxTranTable = this.PresentationData.Tables[CapitalReportDS.ASXTRANSTABLE];

            var capitalSummaryCatRows = capitalSummaryCatTable.Select().OrderByDescending(rows => rows[CapitalReportDS.MONTH]);
            var perInomevsGrowthYearRows = perInomevsGrowthYearTable.Select().OrderByDescending(rows => rows[PerfReportDS.ENDDATE]);

            SetPerformanceDetailsGridYear(doc, perInomevsGrowthYearRows, "INCOME vs. GROWTH YEARLY");
            SetPerformanceDetailsGrid(doc, capitalSummaryCatRows, "INCOME vs. GROWTH MONTHLY");

            AddFooter(doc);

            return doc;
        }

        protected void quickReports_ItemClick(object sender, RadMenuEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "1M":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetOneMonthDate();
                        SetReport(ds);
                        break;
                    }
                case "3M":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetThreeMonthDate();
                        SetReport(ds);
                        break;
                    }
                case "6M":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetSixMonthDate();
                        SetReport(ds);
                        break;
                    }
                case "12M":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetTwelveMonthDate();
                        SetReport(ds);
                        break;
                    }
                case "LFY":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetLastFinancialYear();
                        SetReport(ds);
                        break;
                    }
                case "CFY":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetCurrentFinancialYear();
                        SetReport(ds);
                        break;
                    }
                case "L5Y":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetFiveFinancialYear();
                        SetReport(ds);
                        break;
                    }
            }
        }

        public void SetPerformanceDetailsGrid(C1PrintDocument doc, IOrderedEnumerable<DataRow> rows, string serviceName)
        {
            int index = 1;

            if (rows.Count() > 0)
            {
                RenderTable capitalTable = new RenderTable();
                capitalTable.RowGroups[0, 1].PageHeader = true;
                double OpeningBalance = 0;
                double Movement = 0;
                double SalePurchase = 0;
                double TransferInOut = 0;
                double FeesExpenses = 0;
                double Income = 0;
                double ClosingBalance = 0;
                double ChangeInInvestmentValue = 0;
                double IncomeTotal = 0;

                capitalTable.Style.FontSize = 7;

                capitalTable.Rows[0].Height = .2;
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.FontSize = 6;
                capitalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[0, 0].Text = "Month";
                capitalTable.Cells[0, 1].Text = "Opening Balance";
                capitalTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 2].Text = "Sale / Pur.";
                capitalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 3].Text = "Transfer In / Out";
                capitalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 4].Text = "Fees, Taxes & Exp.";
                capitalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 5].Text = "Other Mvt.";
                capitalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 6].Text = "Income";
                capitalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 7].Text = "Change in Mkt. Val.";
                capitalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 8].Text = "Closing Balance";
                capitalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 9].Text = "Income Return";
                capitalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 10].Text = "Growth Return";
                capitalTable.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 11].Text = "Overall Return";
                capitalTable.Cells[0, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cols[0].Width = .5;
                capitalTable.Cols[1].Width = 1.2;
                capitalTable.Cols[2].Width = 1.2;
                capitalTable.Cols[3].Width = 1.2;
                capitalTable.Cols[4].Width = 1.2;
                capitalTable.Cols[5].Width = 1.2;
                capitalTable.Cols[6].Width = 1.2;
                capitalTable.Cols[7].Width = 1.2;
                capitalTable.Cols[8].Width = 1.2;
                capitalTable.Cols[9].Width = 1.2;
                capitalTable.Cols[10].Width = 1.2;
                capitalTable.Cols[11].Width = 1.2;
             
                var closingRow = rows.FirstOrDefault()[CapitalReportDS.CLOSINGBALANCE];
                var openingRow = rows.LastOrDefault()[CapitalReportDS.OPENINGBAL];

                OpeningBalance = Convert.ToDouble(openingRow);
                ClosingBalance = Convert.ToDouble(closingRow);

                foreach (DataRow row in rows)
                {

                    capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    capitalTable.Rows[index].Height = .3;
                    capitalTable.Cells[index, 0].Text = ((DateTime)row[CapitalReportDS.MONTH]).ToString("MMM-yy").ToUpper();

                    TransferInOut += Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]);
                    FeesExpenses += Convert.ToDouble(row[CapitalReportDS.EXPENSE]);
                    SalePurchase += Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION]);
                    Movement += Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]);
                    ChangeInInvestmentValue += Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]);
                    double movement = Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]);
                    Income += Convert.ToDouble(row[CapitalReportDS.INCOME]);

                    capitalTable.Cells[index, 1].Text = Convert.ToDouble(row[CapitalReportDS.OPENINGBAL]).ToString("C");
                    capitalTable.Cells[index, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 2].Text = Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION]).ToString("C");
                    capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 3].Text = Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]).ToString("C");
                    capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 4].Text = Convert.ToDouble(row[CapitalReportDS.EXPENSE]).ToString("C");
                    capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 5].Text = movement.ToString("C");
                    capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                    double income = Convert.ToDouble(row[CapitalReportDS.INCOME]);
                    capitalTable.Cells[index, 6].Text = income.ToString("C");
                    capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                    IncomeTotal += income;

                    double chgInvest = Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]);
                    capitalTable.Cells[index, 7].Text = chgInvest.ToString("C");
                    capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    double cls = Convert.ToDouble(row[CapitalReportDS.CLOSINGBALANCE]);
                    capitalTable.Cells[index, 8].Text = cls.ToString("C");
                    capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                    if (row[CapitalReportDS.INCOMERETURN] != null && row[CapitalReportDS.INCOMERETURN].ToString() != String.Empty)
                        capitalTable.Cells[index, 9].Text = Convert.ToDouble(row[CapitalReportDS.INCOMERETURN]).ToString("P4");
                    else
                        capitalTable.Cells[index, 9].Text = Convert.ToDouble(0).ToString("P4");
                    capitalTable.Cells[index, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                    if (row[CapitalReportDS.GROWTHRETURN] != null && row[CapitalReportDS.GROWTHRETURN].ToString() != String.Empty)
                        capitalTable.Cells[index, 10].Text = Convert.ToDouble(row[CapitalReportDS.GROWTHRETURN]).ToString("P4");
                    else
                        capitalTable.Cells[index, 10].Text = Convert.ToDouble(0).ToString("P4");
                    capitalTable.Cells[index, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                    if (row[CapitalReportDS.OVERALLRETURN] != null && row[CapitalReportDS.OVERALLRETURN].ToString() != String.Empty)
                        capitalTable.Cells[index, 11].Text = Convert.ToDouble(row[CapitalReportDS.OVERALLRETURN]).ToString("P4");
                    else
                        capitalTable.Cells[index, 11].Text = Convert.ToDouble(0).ToString("P4");
                    capitalTable.Cells[index, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

                    index++;
                }


                capitalTable.Rows[index].Height = .2;
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[index, 0].Text = "TOTAL";
                capitalTable.Cells[index, 1].Text = OpeningBalance.ToString("C");
                capitalTable.Cells[index, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 2].Text = SalePurchase.ToString("C");
                capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 3].Text = TransferInOut.ToString("C");
                capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 4].Text = FeesExpenses.ToString("C");
                capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 5].Text = Movement.ToString("C");
                capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 6].Text = IncomeTotal.ToString("C");
                capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 7].Text = ChangeInInvestmentValue.ToString("C");
                capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 8].Text = ClosingBalance.ToString("C");
                capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(capitalTable);
                doc.Body.Children.Add(lineBreak);
            }
        }

        public static void AddFooter(C1PrintDocument doc)
        {
            doc.PageLayouts.PrintFooterOnLastPage = true;
            RenderTable footer = new RenderTable();
            footer.BreakBefore = BreakEnum.Page;
            footer.Rows[0].Height = .30;
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;
            footer.Rows[1].Height = 2.5;
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.TextAlignVert = AlignVertEnum.Center;

            footer.Cells[0, 0].Text = "FOR YOUR INFORMATION";
            footer.Cells[0, 0].Style.TextAlignHorz = AlignHorzEnum.Center;
            footer.Cells[0, 0].Style.FontSize = 12;

            footer.Cells[1, 0].Text = "\n1. The “Movement” column includes all cash movement in and out of your portfolio.  The capital movement summary report on the portal will provide a breakdown of this amount between income, expenses, transfer in and out’s etc." +
                                        "\n\n2. Portfolio performance is calculated using the Modified Dietz Method and are net of fees and charges shown in the Capital Flows table.  Under this method of calculation individual cash flows over the period are weighted according to when they occur to provide an approximation of the actual return.  When returns are volatile or cash flows are large in relation to the size of the portfolio, the accuracy of this approximation will be reduced.\n\nIncome transactions such as interest, dividends, distributions and rent are not included in the cash flows as they are included in the change of investment value to determine the return on your portfolio.\n\nPast performance is not a reliable indicator of future performance." +
                                       "\n\n3. Returns shown in this reports are not annualised. The performance numbers are therefore the return for the period the report has been run." +
                                        "\n\n4. This report is provided by e-Clipse Online Pty Limited ABN 70 145 358 630, AFSL 357 306 (“e-Clipse”) and is based on information provided to e-Clipse by third parties.  Whilst every reasonable effort has been made by e-Clipse to ensure its accuracy, neither e-Clipse nor any of its related entities guarantee its accuracy nor accept any liability for any errors or omissions." +
                                        "\n\ne-Clipse Online Pty Ltd (ABN 70 145 358 630)\n3/36 Bydown Street, Neutral Bay, NSW 2089\nhttp://www.e-clipse.com.au\nP: +61 2 9346 4686";
            footer.Cells[1, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            footer.Cells[1, 0].Style.TextAlignVert = AlignVertEnum.Top;
            footer.Cells[1, 0].Style.FontSize = 9;
            footer.Cells[1, 0].Style.FontBold = false;
            doc.PageLayouts.LastPage = new PageLayout();
            doc.PageLayouts.LastPage.Document.Body.Children.Add(footer);
        }

        public void SetPerformanceDetailsGridYear(C1PrintDocument doc, IOrderedEnumerable<DataRow> rows, string serviceName)
        {
            int index = 1;

            if (rows.Count() > 0)
            {
                RenderTable capitalTable = new RenderTable();
                capitalTable.RowGroups[0, 1].PageHeader = true;
                capitalTable.Style.FontSize = 6;

                capitalTable.Rows[0].Height = .2;
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.FontSize = 6;
                capitalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[0, 0].Text = "Description";
                capitalTable.Cells[0, 1].Text = "Start Date";
                capitalTable.Cells[0, 2].Text = "End Date";
                capitalTable.Cells[0, 3].Text = "Opening Balance";
                capitalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 4].Text = "Income";
                capitalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 5].Text = "Movement";
                capitalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 6].Text = "Change in Mkt. Val.";
                capitalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 7].Text = "Closing Balance";
                capitalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 8].Text = "Income Return";
                capitalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 9].Text = "Growth Return";
                capitalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 10].Text = "Overall Return";
                capitalTable.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cols[0].Width = .8;
                capitalTable.Cols[1].Width = .8;
                capitalTable.Cols[2].Width = .8;
                capitalTable.Cols[3].Width = 1.2;
                capitalTable.Cols[4].Width = 1.2;
                capitalTable.Cols[5].Width = 1.2;
                capitalTable.Cols[6].Width = 1.2;
                capitalTable.Cols[7].Width = 1.2;
                capitalTable.Cols[8].Width = 1.2;
                capitalTable.Cols[9].Width = 1.2;
                capitalTable.Cols[10].Width = 1.2;

                foreach (DataRow row in rows)
                {

                    capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    capitalTable.Rows[index].Height = .3;
                    capitalTable.Cells[index, 0].Text = row[PerfReportDS.DESCRIPTION].ToString();
                    capitalTable.Cells[index, 1].Text = ((DateTime)row[PerfReportDS.STARTDATE]).ToString("MMM-yy").ToUpper();
                    capitalTable.Cells[index, 2].Text = ((DateTime)row[PerfReportDS.ENDDATE]).ToString("MMM-yy").ToUpper();

                    capitalTable.Cells[index, 3].Text = Convert.ToDouble(row[PerfReportDS.OPENINGBAL]).ToString("C");
                    capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 4].Text = Convert.ToDouble(row[PerfReportDS.INCOME]).ToString("C");
                    capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 5].Text = Convert.ToDouble(row[PerfReportDS.MOVEMENTEXCLINCOME]).ToString("C");
                    capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 6].Text = Convert.ToDouble(row[PerfReportDS.CHANGEININVESTMENTVALUE]).ToString("C");
                    capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 7].Text = Convert.ToDouble(row[PerfReportDS.CLOSINGBALANCE]).ToString("C");
                    capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    if (row[CapitalReportDS.INCOMERETURN] != null && row[PerfReportDS.INCOMERETURN].ToString() != String.Empty)
                        capitalTable.Cells[index, 8].Text = Convert.ToDouble(row[PerfReportDS.INCOMERETURN]).ToString("P4");
                    else
                        capitalTable.Cells[index, 8].Text = Convert.ToDouble(0).ToString("P4");
                    capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                    if (row[CapitalReportDS.GROWTHRETURN] != null && row[PerfReportDS.GROWTHRETURN].ToString() != String.Empty)
                        capitalTable.Cells[index, 9].Text = Convert.ToDouble(row[PerfReportDS.GROWTHRETURN]).ToString("P4");
                    else
                        capitalTable.Cells[index, 9].Text = Convert.ToDouble(0).ToString("P4");
                    capitalTable.Cells[index, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                    if (row[CapitalReportDS.OVERALLRETURN] != null && row[PerfReportDS.OVERALLRETURN].ToString() != String.Empty)
                        capitalTable.Cells[index, 10].Text = Convert.ToDouble(row[PerfReportDS.OVERALLRETURN]).ToString("P4");
                    else
                        capitalTable.Cells[index, 10].Text = Convert.ToDouble(0).ToString("P4");
                    capitalTable.Cells[index, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                    index++;
                }


                capitalTable.Rows[index].Height = .2;
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(capitalTable);
                doc.Body.Children.Add(lineBreak);
            }

        }

        protected void GeneratePerformanceReport(object sender, EventArgs e)
        {
            PerfReportDS ds = new PerfReportDS();
            if(InputStartDate.DateInput.SelectedDate.HasValue)
                 ds.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(this.InputStartDate.DateInput.SelectedDate.Value);
            if (InputEndDate.DateInput.SelectedDate.HasValue)
                 ds.EndDate =  AccountingFinancialYear.LastDayOfMonthFromDateTime(this.InputEndDate.DateInput.SelectedDate.Value);

            SetReport(ds);
        }

        private void SetReport(PerfReportDS ds)
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"].ToString()));
            clientData.GetData(ds);
            this.PresentationData = ds;
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);
            DataRow clientSummaryRow = ds.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0];
            string reportName = "Report_" + Guid.NewGuid().ToString();
            C1ReportViewer.RegisterDocument(reportName, MakeDocPerformance);
            ReportViewer.FileName = reportName;
            ReportViewer.ReportName = reportName;
            InputEndDate.DbSelectedDate = ds.EndDate;
            InputStartDate.DbSelectedDate = ds.StartDate; 
            UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void ClientPerformanceReportClick()
        {
            PerfReportDS ds = new PerfReportDS();

            if (InputEndDate.DateInput.SelectedDate.HasValue)
                ds.EndDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(this.InputEndDate.DateInput.SelectedDate.Value);
            else
                ds.EndDate = DateTime.Now;
           
            if (InputStartDate.DateInput.SelectedDate.HasValue)
                ds.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(this.InputStartDate.DateInput.SelectedDate.Value);
            else
                ds.StartDate = DateTime.Now.AddYears(-5);
           
            SetReport(ds);      
        }
    }
}
