﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;

namespace eclipseonlineweb
{
    public partial class ClientAssetSummaryReport : UMABasePage
    {
        private List<AssetEntity> assets = null;
      
        public override void PopulatePage(DataSet ds)
        {
            base.PopulatePage(ds);
            this.cid = Request.QueryString["ins"].ToString();
            ClientAssetsReportClick();
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);

        }

        public C1PrintDocument MakeDocAssetSummary()
        {
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            var ds = this.PresentationData as HoldingRptDataSet;
            DataRow clientSummaryRow = ds.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0];
            AddClientHeaderToReport(doc, clientSummaryRow, ds.DateInfo(), "ASSET CLASS SUMMARY REPORT");

            DataView holdingSummaryTableView = new DataView(ds.HoldingSummaryTable);
            holdingSummaryTableView.Sort = ds.HoldingSummaryTable.ASSETNAME + "," + ds.HoldingSummaryTable.PRODUCTNAME + " ASC";

            var assetGroups = holdingSummaryTableView.ToTable().Select().GroupBy(row => row[ds.HoldingSummaryTable.ASSETNAME]);

            RenderTable assetSumTable = new RenderTable();
            assetSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable assetSummaryTotalTable = new RenderTable();
            assetSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            assetSummaryTotalTable.Style.FontSize = 8;

            assetSummaryTotalTable.Rows[0].Height = .2;
            assetSummaryTotalTable.Rows[0].Style.FontBold = true;
            assetSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            assetSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            assetSummaryTotalTable.Rows[0].Style.FontBold = true;
            assetSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            assetSummaryTotalTable.Cells[0, 0].Text = "Asset Code";
            assetSummaryTotalTable.Cells[0, 1].Text = "Description";
            assetSummaryTotalTable.Cells[0, 2].Text = "Allocation (%)";
            assetSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
            assetSummaryTotalTable.Cells[0, 3].Text = "Holding ($)";
            assetSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

            assetSummaryTotalTable.Cols[0].Width = 2;
            assetSummaryTotalTable.Cols[1].Width = 8;
            assetSummaryTotalTable.Cols[2].Width = 4;
            assetSummaryTotalTable.Cols[3].Width = 4;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Asset Class Summary\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            double grandTotal = 0;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(assetSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            foreach (var assetGroup in assetGroups)
            {
                if (assetGroup.Count() > 0)
                {
                    foreach (DataRow row in assetGroup)
                        grandTotal += Convert.ToDouble(row[ds.HoldingSummaryTable.HOLDING]);
                }
            }


            foreach (var assetGroup in assetGroups)
            {
                if (assetGroup.Count() > 0)
                {
                    int index = 1;
                    string assetClass = string.Empty;
                    //Deploy header
                    RenderTable assetTable = new RenderTable();
                    assetTable.RowGroups[0, 1].PageHeader = true;
                    double totalHolding = 0;

                    assetTable.Style.FontSize = 8;

                    assetTable.Rows[0].Height = .2;
                    assetTable.Rows[0].Style.FontBold = true;
                    assetTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                    assetTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                    assetTable.Rows[0].Style.FontBold = true;
                    assetTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                    assetTable.Cells[0, 0].Text = "Product Name";
                    assetTable.Cells[0, 1].Text = "Account NO";
                    assetTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Left;
                    assetTable.Cells[0, 2].Text = "Description";
                    assetTable.Cells[0, 3].Text = "Allocation (%)";
                    assetTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                    assetTable.Cells[0, 4].Text = "Holding ($)";
                    assetTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    assetTable.Cols[0].Width = 1.5;
                    assetTable.Cols[1].Width = 1.2;
                    assetTable.Cols[2].Width = 4.3;
                    assetTable.Cols[3].Width = .8;
                    assetTable.Cols[4].Width = .8;

                    foreach (DataRow row in assetGroup)
                        totalHolding += Convert.ToDouble(row[ds.HoldingSummaryTable.HOLDING]);

                    foreach (DataRow row in assetGroup)
                    {
                        assetClass = row[ds.HoldingSummaryTable.ASSETNAME].ToString();

                        assetTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                        assetTable.Rows[index].Height = .3;
                        assetTable.Cells[index, 0].Text = row[ds.HoldingSummaryTable.PRODUCTNAME].ToString();
                        assetTable.Cells[index, 1].Text = row[ds.HoldingSummaryTable.ACCOUNTNO].ToString();
                        assetTable.Cells[index, 1].Style.TextAlignHorz = AlignHorzEnum.Left;
                        assetTable.Cells[index, 2].Text = row[ds.HoldingSummaryTable.DESCRIPTION].ToString();
                        if (double.IsNaN(Convert.ToDouble(row[ds.HoldingSummaryTable.HOLDING]) / totalHolding))
                            assetTable.Cells[index, 3].Text = "-";
                        else
                            assetTable.Cells[index, 3].Text = ((Convert.ToDouble(row[ds.HoldingSummaryTable.HOLDING]) / totalHolding)).ToString("P2");
                        assetTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                        assetTable.Cells[index, 4].Text = Convert.ToDouble(row[ds.HoldingSummaryTable.HOLDING]).ToString("C");
                        assetTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                        index++;
                    }

                    assetTable.Rows[index].Height = .2;
                    assetTable.Rows[index].Style.FontBold = true;
                    assetTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                    assetTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                    assetTable.Rows[index].Style.FontBold = true;
                    assetTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                    assetTable.Cells[index, 0].Text = "TOTAL";

                    assetTable.Cells[index, 4].Text = totalHolding.ToString("C");
                    assetTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    RenderText gridHeader = new RenderText();

                    string assetName = string.Empty;

                    if (assetClass == "ManualAsset")
                        assetName = "Manual Assets\n\n";
                    else
                        assetName = this.assets.Where(asset => asset.Name == assetClass).FirstOrDefault().Description + "\n\n";

                    gridHeader.Text = assetName;
                    gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                    gridHeader.Style.FontSize = 11;
                    gridHeader.Style.FontBold = true;

                    RenderText lineBreak = new RenderText();
                    lineBreak.Text = "\n\n\n";

                    assetSummaryTotalTable.Cells[totalIndex, 0].Text = assetClass;
                    assetSummaryTotalTable.Cells[totalIndex, 1].Text = assetName;

                    if (double.IsNaN(totalHolding / grandTotal))
                        assetSummaryTotalTable.Cells[totalIndex, 2].Text = "-";
                    else
                        assetSummaryTotalTable.Cells[totalIndex, 2].Text = (totalHolding / grandTotal).ToString("P2");

                    assetSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                    assetSummaryTotalTable.Cells[totalIndex, 3].Text = totalHolding.ToString("C");
                    assetSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    doc.Body.Children.Add(gridHeader);
                    doc.Body.Children.Add(assetTable);
                    doc.Body.Children.Add(lineBreak);

                    totalIndex++;
                }
            }

            assetSummaryTotalTable.Rows[totalIndex].Height = .2;
            assetSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            assetSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            assetSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            assetSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            assetSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            assetSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            assetSummaryTotalTable.Cells[totalIndex, 1].Text = "";
            assetSummaryTotalTable.Cells[totalIndex, 3].Text = grandTotal.ToString("C");
            assetSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
        
            return doc;
        }

        public void SetAssetsSummaryGrid(C1PrintDocument doc, IOrderedEnumerable<DataRow> rows, string serviceName)
        {

            int index = 1;

            if (rows.Count() > 0)
            {
                RenderTable capitalTable = new RenderTable();
                capitalTable.RowGroups[0, 1].PageHeader = true;
                double OpeningBalance = 0;
                double TransferInOut = 0;
                double Income = 0;
                double AppicationRedemption = 0;
                double TaxInOut = 0;
                double Expense = 0;
                double InternalCashMovement = 0;
                double ClosingBalance = 0;
                double ChangeInInvestmentValue = 0;

                capitalTable.Style.FontSize = 8;

                capitalTable.Rows[0].Height = .2;
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.FontSize = 7;
                capitalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[0, 0].Text = "Month";
                capitalTable.Cells[0, 1].Text = "Opening Balance";
                capitalTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 2].Text = "Transfer In/Out";
                capitalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 3].Text = "Income";
                capitalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 4].Text = "Invesments";
                capitalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 5].Text = "Tax & Expenses";
                capitalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 6].Text = "Internal Cash Mvt";
                capitalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 7].Text = "Closing Balance";
                capitalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 8].Text = "Change in Investment";
                capitalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cols[0].Width = .5;
                capitalTable.Cols[1].Width = 1.2;
                capitalTable.Cols[2].Width = 1.2;
                capitalTable.Cols[3].Width = 1.2;
                capitalTable.Cols[4].Width = 1.2;
                capitalTable.Cols[5].Width = 1.2;
                capitalTable.Cols[6].Width = 1.2;
                capitalTable.Cols[7].Width = 1.2;
                capitalTable.Cols[8].Width = 1.5;

                foreach (DataRow row in rows)
                {

                    capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    capitalTable.Rows[index].Height = .3;
                    capitalTable.Cells[index, 0].Text = ((DateTime)row[CapitalReportDS.MONTH]).ToString("MMM-yy").ToUpper();

                    OpeningBalance += Convert.ToDouble(row[CapitalReportDS.OPENINGBAL]);
                    TransferInOut += Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]);
                    Income += Convert.ToDouble(row[CapitalReportDS.INCOME]);
                    AppicationRedemption += Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION]);
                    TaxInOut += Convert.ToDouble(row[CapitalReportDS.TAXINOUT]);
                    Expense += Convert.ToDouble(row[CapitalReportDS.EXPENSE]);
                    InternalCashMovement += Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]);
                    ClosingBalance += Convert.ToDouble(row[CapitalReportDS.CLOSINGBALANCE]);
                    ChangeInInvestmentValue += Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]);

                    capitalTable.Cells[index, 1].Text = Convert.ToDouble(row[CapitalReportDS.OPENINGBAL]).ToString("C");
                    capitalTable.Cells[index, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 2].Text = Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]).ToString("C");
                    capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 3].Text = Convert.ToDouble(row[CapitalReportDS.INCOME]).ToString("C");
                    capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 4].Text = Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION]).ToString("C");
                    capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 5].Text = Convert.ToDouble(row[CapitalReportDS.EXPENSE]).ToString("C");
                    capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 6].Text = Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]).ToString("C");
                    capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 7].Text = Convert.ToDouble(row[CapitalReportDS.CLOSINGBALANCE]).ToString("C");
                    capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 8].Text = Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]).ToString("C");
                    capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;


                    index++;
                }


                capitalTable.Rows[index].Height = .2;
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[index, 0].Text = "TOTAL";

                capitalTable.Cells[index, 2].Text = TransferInOut.ToString("C");
                capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 3].Text = Income.ToString("C");
                capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 4].Text = AppicationRedemption.ToString("C");
                capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 5].Text = Expense.ToString("C");
                capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 6].Text = InternalCashMovement.ToString("C");
                capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(capitalTable);
                doc.Body.Children.Add(lineBreak);
            }

        }

        protected void GenerateAssetSummaryReport(object sender, EventArgs e)
        {
            ClientAssetsReportClick();
        }

        protected void ClientAssetsReportClick()
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"].ToString()));
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            this.assets = organization.Assets;
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);
            HoldingRptDataSet ds = new HoldingRptDataSet();
            if (this.InputValutationDate.SelectedDate.HasValue)
                ds.ValuationDate = this.InputValutationDate.SelectedDate.Value;
            clientData.GetData(ds);
            this.PresentationData = ds;

            DataRow clientSummaryRow = ds.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0];

            string reportName = "Report_" + Guid.NewGuid().ToString();
            C1ReportViewer.RegisterDocument(reportName, MakeDocAssetSummary);
            C1ReportViewerAssetClass.FileName = reportName;
            C1ReportViewerAssetClass.ReportName = reportName;
            InputValutationDate.DbSelectedDate = ds.ValuationDate;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }
    }
}
