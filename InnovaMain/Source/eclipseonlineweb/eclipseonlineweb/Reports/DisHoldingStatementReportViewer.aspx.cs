﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;

namespace eclipseonlineweb
{
    public partial class DisHoldingStatementReportViewer : UMABasePage
    {
       
        private List<AssetEntity> assets = null;
        public override void PopulatePage(DataSet ds)
        {
            if (!Page.IsPostBack)
            {
                string secCode = Request.QueryString["SecurityCode"].ToString();
                string startDate = Request.QueryString["StartDate"].ToString();
                string endDate = Request.QueryString["EndDate"].ToString();
                string secDesc = Request.QueryString["SecDesc"].ToString();

                this.lblSecDesc.Text = secDesc;
                this.lblSecCode.Text = secCode;


                DateTime parsedDate = DateTime.Now;

                if (DateTime.TryParse(startDate, out parsedDate))
                    InputStartDate.DbSelectedDate = DateTime.Parse(startDate);

                if (DateTime.TryParse(endDate, out parsedDate))
                    InputEndDate.DbSelectedDate = DateTime.Parse(endDate);
            }

            this.cid = Request.QueryString["ins"].ToString();
            base.PopulatePage(ds);
            ReportClick();
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public C1PrintDocument MakeDoc()
        {
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            var ds = this.PresentationData as HoldingSummaryReportDS;
            DataRow clientSummaryRow = ds.Tables[HoldingSummaryReportDS.CLIENTSUMMARYTABLE].Rows[0];
            AddClientHeaderToReport(doc, clientSummaryRow, ds.DateInfo(), "HOLDING STATEMENT");

            SetHoldingStatement(doc, ds);

            AddFooterMISStatement(doc);
            return doc;
        }

        private void SetHoldingStatement(C1PrintDocument doc, HoldingSummaryReportDS ds)
        {
            DataView secSummaryTableView = new DataView(ds.misTransactionDetailsTable);
            secSummaryTableView.Sort = ds.misTransactionDetailsTable.TRANSACTIONDATE + " ASC";
            secSummaryTableView.RowFilter = ds.misTransactionDetailsTable.SECCODE + "= '" + this.lblSecCode.Text + "'";

            DataTable secFilteredTable = secSummaryTableView.ToTable();

            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.Style.FontSize = 8;
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Date";
            secSummaryTotalTable.Cells[0, 1].Text = "Transaction Type";
            secSummaryTotalTable.Cells[0, 2].Text = "Units";
            secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 3].Text = "Unit Price";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 4].Text = "Holding ($)";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 5].Text = "Holding Balance";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 6].Text = "Holding Balance ($)";
            secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 4;
            secSummaryTotalTable.Cols[1].Width = 4;
            secSummaryTotalTable.Cols[2].Width = 4;
            secSummaryTotalTable.Cols[3].Width = 4;
            secSummaryTotalTable.Cols[4].Width = 4;
            secSummaryTotalTable.Cols[5].Width = 4;
            secSummaryTotalTable.Cols[6].Width = 4;


            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = this.lblSecCode.Text.ToUpper() + "   -   " + this.lblSecDesc.Text.ToUpper() + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";
            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            foreach (DataRow secRow in secFilteredTable.Rows)
            {
                secSummaryTotalTable.Cells[totalIndex, 0].Text = ((DateTime)secRow[ds.misTransactionDetailsTable.TRANSACTIONDATE]).ToString("dd/MM/yyyy");
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[ds.misTransactionDetailsTable.TRANSTYPE].ToString();

                if (secRow[ds.misTransactionDetailsTable.UNITS] != null & secRow[ds.misTransactionDetailsTable.UNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = Convert.ToDecimal(secRow[ds.misTransactionDetailsTable.UNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[ds.misTransactionDetailsTable.UNITSPRICE] != null & secRow[ds.misTransactionDetailsTable.UNITSPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = Convert.ToDecimal(secRow[ds.misTransactionDetailsTable.UNITSPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[ds.misTransactionDetailsTable.UNITSAMT] != null & secRow[ds.misTransactionDetailsTable.UNITSAMT] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = Convert.ToDecimal(secRow[ds.misTransactionDetailsTable.UNITSAMT]).ToString("C");
                else
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[ds.misTransactionDetailsTable.UNITSBALANCE] != null & secRow[ds.misTransactionDetailsTable.UNITSBALANCE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = Convert.ToDecimal(secRow[ds.misTransactionDetailsTable.UNITSBALANCE]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[ds.misTransactionDetailsTable.UNITSAMTBALANCE] != null & secRow[ds.misTransactionDetailsTable.UNITSAMTBALANCE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = Convert.ToDecimal(secRow[ds.misTransactionDetailsTable.UNITSAMTBALANCE]).ToString("C");
                else
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                totalIndex++;
            }

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;
            doc.PageLayout.PageSettings.Landscape = false;
            doc.PageLayout.PageSettings.TopMargin = .20;
            doc.PageLayout.PageSettings.LeftMargin = .20;
            doc.PageLayout.PageSettings.RightMargin = .20;
        }

        protected void GenerateReport(object sender, EventArgs e)
        {
            ReportClick();
        }

        protected void ReportClick()
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"].ToString()));
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            this.assets = organization.Assets;
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);
            HoldingSummaryReportDS ds = new HoldingSummaryReportDS();
            if (this.InputEndDate.DateInput.SelectedDate.HasValue)
                ds.EndDate = this.InputEndDate.DateInput.SelectedDate.Value;

            if (this.InputStartDate.DateInput.SelectedDate.HasValue)
                ds.StartDate = this.InputStartDate.DateInput.SelectedDate.Value;

            clientData.GetData(ds);
            this.PresentationData = ds;
            InputEndDate.DbSelectedDate = ds.EndDate;
            InputStartDate.DbSelectedDate = ds.StartDate;
            DataRow clientSummaryRow = ds.Tables[HoldingSummaryReportDS.CLIENTSUMMARYTABLE].Rows[0];
            string reportName = "Report_" + Guid.NewGuid().ToString();
            C1ReportViewer.RegisterDocument(reportName, MakeDoc);
            ReportViewer.FileName = reportName;
            ReportViewer.ReportName = reportName;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }
    }
}
