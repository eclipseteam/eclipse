﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="ReportsMaster.master"
    AutoEventWireup="true" CodeBehind="ClientPerformanceReportBySecB.aspx.cs" Inherits="eclipseonlineweb.ClientPerformanceReportBySecB" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer"
    TagPrefix="C1ReportViewer" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <span class="riLabel">Start Date:</span><telerik:RadMonthYearPicker ID="InputStartDate"
                                runat="server">
                            </telerik:RadMonthYearPicker>
                        </td>
                        <td style="width: 5%">
                        <span class="riLabel">End Date:</span><telerik:RadMonthYearPicker ID="InputEndDate"
                                runat="server">
                            </telerik:RadMonthYearPicker>
                            
                        </td>
                        <td style="width: 5%"><br />
                        <telerik:RadButton runat="server" ID="BtnGenerateReport" OnClick="GeneratePerformanceReport" Text="Generate Report"></telerik:RadButton>
                            
                        </td>
                        <td>
                            <br />
                            <telerik:RadMenu ID="quickReports" runat="server" EnableRoundedCorners="true" OnItemClick="quickReports_ItemClick"
                                EnableShadows="true">
                                <Items>
                                    <telerik:RadMenuItem Text="QUICK GENERATE">
                                        <Items>
                                            <telerik:RadMenuItem Value="1M" Text="One Month" />
                                            <telerik:RadMenuItem Value="3M" Text="Three Months" />
                                            <telerik:RadMenuItem Value="6M" Text="Six Months" />
                                            <telerik:RadMenuItem Value="12M" Text="Twelve Months" />
                                            <telerik:RadMenuItem Value="LFY" Text="Last Financial Year" />
                                            <telerik:RadMenuItem Value="CFY" Text="Current Financial Year" />
                                        </Items>
                                    </telerik:RadMenuItem>
                                </Items>
                            </telerik:RadMenu>
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <C1ReportViewer:C1ReportViewer CollapseToolsPanel="true" Cache-Enabled="false" Cache-ShareBetweenSessions="false"
                FileName="C1ReportViewerCapitalFlow" runat="server" ID="C1ReportViewerCapitalFlow"
                Zoom="75%" Height="550px" Width="100%">
            </C1ReportViewer:C1ReportViewer>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
