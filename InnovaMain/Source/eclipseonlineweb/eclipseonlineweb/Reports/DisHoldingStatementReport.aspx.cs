﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;

namespace eclipseonlineweb
{
    public partial class DisHoldingStatementReport : UMABasePage
    {
       
        private List<AssetEntity> assets = null;

        protected override void Intialise()
        {
            base.Intialise();
            this.InfoGrid.RowCommand += new C1GridViewCommandEventHandler(InfoGrid_RowCommand);

        }

        private void InfoGrid_RowCommand(object sender, C1GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "details")
            {
                var grid = (C1GridView)sender;
                int index = int.Parse(e.CommandArgument.ToString());
                string secCode = grid.Rows[index].Cells[0].Text;
                string startDate = this.InputStartDate.DateInput.SelectedDate.Value.ToString("dd/MM/yyyy");
                string endDate = this.InputEndDate.DateInput.SelectedDate.Value.ToString("dd/MM/yyyy");
                string secDesc = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[1].Text;
                this.cid = Request.QueryString["ins"].ToString();

                Response.Redirect("~/Reports/DisHoldingStatementReportViewer.aspx?ins=" + this.cid + "&SecDesc=" + secDesc + "&SecurityCode=" + secCode + "&StartDate=" + startDate + "&EndDate=" + endDate);
            }
        }

        public override void PopulatePage(DataSet ds)
        {
            this.cid = Request.QueryString["ins"].ToString();
            base.PopulatePage(ds);
            LoadReportData();
        }

        protected void LoadReportData()
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            this.assets = organization.Assets;
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);
            HoldingSummaryReportDS ds = new HoldingSummaryReportDS();

            if (this.InputEndDate.DateInput.SelectedDate.HasValue)
                ds.EndDate = this.InputEndDate.DateInput.SelectedDate.Value;

            if (this.InputStartDate.DateInput.SelectedDate.HasValue)
                ds.StartDate = this.InputStartDate.DateInput.SelectedDate.Value;

            clientData.GetData(ds);
            this.PresentationData = ds;
            DataRow clientSummaryRow = ds.Tables[HoldingSummaryReportDS.CLIENTSUMMARYTABLE].Rows[0];

            DataView listedSecurities = new DataView(ds.securitySummaryTable);
            listedSecurities.RowFilter = ds.securitySummaryTable.TYPE + "= 'FUND'";
            this.InfoGrid.DataSource = listedSecurities.ToTable();
            this.InfoGrid.DataBind();
            InputEndDate.DbSelectedDate = ds.EndDate;
            InputStartDate.DbSelectedDate = ds.StartDate;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        protected void Grid_PageIndexChanging(object sender, C1GridViewPageEventArgs e)
        {
           
        }

        protected void GenerateReport(object sender, EventArgs e)
        {
            LoadReportData(); 
        }

        protected void FilterGrid(object sender, C1GridViewFilterEventArgs e)
        {
            IBrokerManagedComponent clientData = GetData();
            LoadReportData(); 
            this.UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        private IBrokerManagedComponent GetData()
        {
            this.cid = Request.QueryString["ins"].ToString();
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            HoldingSummaryReportDS ds = new HoldingSummaryReportDS();
            if (this.InputEndDate.DateInput.SelectedDate.HasValue)
                ds.EndDate = this.InputEndDate.DateInput.SelectedDate.Value;

            if (this.InputStartDate.DateInput.SelectedDate.HasValue)
                ds.StartDate = this.InputStartDate.DateInput.SelectedDate.Value;

            clientData.GetData(ds);
            this.PresentationData = ds;
            return clientData;
        }

        protected void SortGrid(object sender, C1GridViewSortEventArgs e)
        {
            IBrokerManagedComponent clientData = GetData();
            LoadReportData();
            this.UMABroker.ReleaseBrokerManagedComponent(clientData);
        }
    }
}
