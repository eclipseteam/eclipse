﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;

namespace eclipseonlineweb
{
    public partial class DividentStatementReport : UMABasePage
    {

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public override void LoadPage()
        {
            this.cid = Request.QueryString["ins"].ToString();
            lblDivID.Text = Request.QueryString["DivID"].ToString();
            lblRecordDate.Text = Request.QueryString["RecordDate"].ToString();
            this.lblUnits.Text = Request.QueryString["Units"].ToString();
   
            DIV_Click();
           
            ScriptManager sm = ScriptManager.GetCurrent(Page);
        }

        protected void DIV_Click()
        {
            this.cid = Request.QueryString["ins"].ToString();
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);
            DIVTransactionDS divDS = new DIVTransactionDS();
            clientData.GetData(divDS);
            this.PresentationData = divDS;
            
            string reportName = "Report_" + Guid.NewGuid().ToString();
            C1ReportViewer.RegisterDocument(reportName, MakeDoc);
            this.C1ReportViewerDividentStatement.FileName = reportName;
            C1ReportViewerDividentStatement.ReportName = reportName;

            this.UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        public C1PrintDocument MakeDoc()
        {
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            DataRow clientSummaryRow = this.PresentationData.Tables[BaseClientDetails.CLIENTSUMMARYTABLE].Rows[0];

            AddClientHeaderToReport(doc, clientSummaryRow, "", "Dividend Statement");//NB. Can't get DateInfo like bellow
            AddFooterSecuritiesStatement(doc);
            var manRows = this.PresentationData.Tables["Income Transactions"].Select("ID='" + lblDivID.Text + "' AND UnitsOnHand ='" + this.lblUnits.Text + "'");

            RenderTable divStmtTable = new RenderTable();
            divStmtTable.RowGroups[0, 1].PageHeader = true;
            divStmtTable.Style.FontSize = 12;
            divStmtTable.Rows[0].SplitBehavior = SplitBehaviorEnum.SplitNewPage;
            divStmtTable.Rows[0].Height = .3;
            divStmtTable.Rows[0].Style.FontBold = true;
            divStmtTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            divStmtTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            divStmtTable.Rows[0].Style.FontBold = true;
            divStmtTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            divStmtTable.Cells[0, 0].Text = "";
            divStmtTable.Cells[0, 1].Text = "";
            

            divStmtTable.Cells[1, 0].Text = "Investment Code";
            divStmtTable.Cells[1, 0].Style.FontBold = true;
            divStmtTable.Cells[1, 1].Text = manRows[0]["InvestmentCode"].ToString();
            divStmtTable.Cells[1, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
            divStmtTable.Rows[1].Height = .3;
            divStmtTable.Rows[1].Style.TextAlignVert = AlignVertEnum.Center;

            divStmtTable.Cells[2, 0].Text = "Record Date";
            divStmtTable.Cells[2, 0].Style.FontBold = true;
            divStmtTable.Cells[2, 1].Text = Convert.ToDateTime(manRows[0]["RecordDate"]).ToString("dd/MM/yyyy");
            divStmtTable.Cells[2, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
            divStmtTable.Rows[2].Height = .3;
            divStmtTable.Rows[2].Style.TextAlignVert = AlignVertEnum.Center;

            divStmtTable.Cells[3, 0].Text = "Payable Date";
            divStmtTable.Cells[3, 0].Style.FontBold = true;
            divStmtTable.Cells[3, 1].Text = Convert.ToDateTime(manRows[0]["PaymentDate"]).ToString("dd/MM/yyyy"); 
            divStmtTable.Cells[3, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
            divStmtTable.Rows[3].Height = .3;
            divStmtTable.Rows[3].Style.TextAlignVert = AlignVertEnum.Center;

            divStmtTable.Cells[4, 0].Text = "Share Holding";
            divStmtTable.Cells[4, 0].Style.FontBold = true;
            divStmtTable.Cells[4, 1].Text = manRows[0]["UnitsOnHand"].ToString();
            divStmtTable.Cells[4, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
            divStmtTable.Rows[4].Height = .3;
            divStmtTable.Rows[4].Style.TextAlignVert = AlignVertEnum.Center;

            divStmtTable.Cells[5, 0].Text = "Dividend Rate";
            divStmtTable.Cells[5, 0].Style.FontBold = true;
            divStmtTable.Cells[5, 1].Text = Convert.ToDecimal(manRows[0]["CentsPerShare"]).ToString("N4");
            divStmtTable.Cells[5, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
            divStmtTable.Rows[5].Height = .3;
            divStmtTable.Rows[5].Style.TextAlignVert = AlignVertEnum.Center;

            divStmtTable.Cells[6, 0].Text = "Franked Dividend Payable";
            divStmtTable.Cells[6, 0].Style.FontBold = true;
            divStmtTable.Cells[6, 1].Text = Convert.ToDecimal(manRows[0]["FrankedAmount"]).ToString("C");
            divStmtTable.Cells[6, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
            divStmtTable.Rows[6].Height = .3;
            divStmtTable.Rows[6].Style.TextAlignVert = AlignVertEnum.Center;

            divStmtTable.Cells[7, 0].Text = "Unfranked Dividend Payable";
            divStmtTable.Cells[7, 0].Style.FontBold = true;
            divStmtTable.Cells[7, 1].Text = Convert.ToDecimal(manRows[0]["UnfrankedAmount"]).ToString("C");
            divStmtTable.Cells[7, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
            divStmtTable.Rows[7].Height = .3;
            divStmtTable.Rows[7].Style.TextAlignVert = AlignVertEnum.Center;

            divStmtTable.Cells[8, 0].Text = "Franking Credits";
            divStmtTable.Cells[8, 0].Style.FontBold = true;
            divStmtTable.Cells[8, 1].Text = Convert.ToDecimal(manRows[0]["TotalFrankingCredits"]).ToString("C");
            divStmtTable.Cells[8, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
            divStmtTable.Rows[8].Height = .3;
            divStmtTable.Rows[8].Style.TextAlignVert = AlignVertEnum.Center;

            divStmtTable.Cells[9, 0].Text = "Tax Withheld";
            divStmtTable.Cells[9, 0].Style.FontBold = true;
            divStmtTable.Cells[9, 1].Text = (0).ToString("C");
            divStmtTable.Cells[9, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
            divStmtTable.Rows[9].Height = .3;
            divStmtTable.Rows[9].Style.TextAlignVert = AlignVertEnum.Center;

            divStmtTable.Cells[10, 0].Text = "Dividend Amount";
            divStmtTable.Cells[10, 0].Style.FontBold = true;
            divStmtTable.Cells[10, 1].Text = Convert.ToDecimal(manRows[0]["PaidDividend"]).ToString("C");
            divStmtTable.Cells[10, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
            divStmtTable.Rows[10].Height = .3;
            divStmtTable.Rows[10].Style.TextAlignVert = AlignVertEnum.Center;

            divStmtTable.Rows[11].SplitBehavior = SplitBehaviorEnum.SplitNewPage;
            divStmtTable.Rows[11].Height = .3;
            divStmtTable.Rows[11].Style.FontBold = true;
            divStmtTable.Rows[11].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            divStmtTable.Rows[11].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            divStmtTable.Rows[11].Style.FontBold = true;
            divStmtTable.Rows[11].Style.TextAlignVert = AlignVertEnum.Center;

            doc.Body.Children.Add(divStmtTable);
            doc.PageLayout.PageSettings.Landscape = false;
            doc.PageLayout.PageSettings.TopMargin = .20;
            doc.PageLayout.PageSettings.LeftMargin = .20;
            doc.PageLayout.PageSettings.RightMargin = .20;
            return doc;
        }
    }
}
