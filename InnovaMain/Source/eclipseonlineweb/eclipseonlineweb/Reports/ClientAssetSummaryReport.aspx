﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="ReportsMaster.master"
    AutoEventWireup="true" CodeBehind="ClientAssetSummaryReport.aspx.cs" Inherits="eclipseonlineweb.ClientAssetSummaryReport" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer"
    TagPrefix="C1ReportViewer" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                 <table width="100%">
                    <tr>
                        <td style="width: 5%">
                            <span class="riLabel">Valuation Date:</span>
                             <telerik:RadDatePicker ID="InputValutationDate" runat="server"></telerik:RadDatePicker>
                        </td>
                        
                        <td style="width: 5%"><br />
                        <telerik:RadButton runat="server" ID="BtnGenerateReport" OnClick="GenerateAssetSummaryReport" Text="Generate Report"></telerik:RadButton>
                            
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <C1ReportViewer:C1ReportViewer CollapseToolsPanel="true" Cache-Enabled="false" Cache-ShareBetweenSessions="false"
                FileName="C1ReportViewerAssetSummary" runat="server" ID="C1ReportViewerAssetClass"
                Zoom="75%" Height="550px" Width="100%">
            </C1ReportViewer:C1ReportViewer>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
