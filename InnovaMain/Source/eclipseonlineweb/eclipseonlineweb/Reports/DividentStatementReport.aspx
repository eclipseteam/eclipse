﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="ReportsMaster.master"
    AutoEventWireup="true" CodeBehind="DividentStatementReport.aspx.cs" Inherits="eclipseonlineweb.DividentStatementReport" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer"
    TagPrefix="C1ReportViewer" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <div>
                <C1ReportViewer:C1ReportViewer CollapseToolsPanel="true" Visible="true" Cache-Enabled="false"
                    Cache-ShareBetweenSessions="false" FileName="InMemoryBasicTable" runat="server"
                    ID="C1ReportViewerDividentStatement" Height="550px" Width="100%" Zoom="75%">
                </C1ReportViewer:C1ReportViewer>
            </div>
            <asp:Label runat="server" ID="lblDivID" Text="" Visible="false"></asp:Label>
            <asp:Label runat="server" ID="lblRecordDate" Text="" Visible="false"></asp:Label>
            <asp:Label runat="server" ID="lblUnits" Text="" Visible="false"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
