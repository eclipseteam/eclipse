﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;

namespace eclipseonlineweb
{
    public partial class ChessHoldingStatementReport : UMABasePage
    {
        private List<AssetEntity> assets = null;

        protected override void Intialise()
        {
            base.Intialise();
            this.InfoGrid.RowCommand += new C1GridViewCommandEventHandler(InfoGrid_RowCommand);

        }

        private void InfoGrid_RowCommand(object sender, C1GridViewCommandEventArgs e)
        {
            var grid = (C1GridView)sender;
            int index = int.Parse(e.CommandArgument.ToString());
            string secCode = grid.Rows[index].Cells[0].Text;
            string startDate = this.InputStartDate.DateInput.SelectedDate.Value.ToString("dd/MM/yyyy");
            string endDate = InputEndDate.DateInput.SelectedDate.Value.ToString("dd/MM/yyyy");
            string secDesc = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[1].Text;
            this.cid = Request.QueryString["ins"].ToString();

            if (e.CommandName.ToLower() == "details")
            {
                Response.Redirect("~/Reports/ChessHoldingStatementReportViewer.aspx?ins=" + this.cid + "&SecDesc=" + secDesc + "&SecurityCode=" + secCode + "&StartDate=" + startDate + "&EndDate=" + endDate);
            }
        }

        public override void PopulatePage(DataSet ds)
        {
            this.cid = Request.QueryString["ins"].ToString();
            base.PopulatePage(ds);
            ChessStatementsClick();
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        protected void ChessStatementsClick()
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"].ToString()));
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            this.assets = organization.Assets;
            HoldingSummaryReportDS ds = new HoldingSummaryReportDS();

            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);

            if (InputEndDate.DateInput.SelectedDate.HasValue)
                ds.EndDate = InputEndDate.DateInput.SelectedDate.Value;

            if (this.InputStartDate.DateInput.SelectedDate.HasValue)
                ds.StartDate = this.InputStartDate.DateInput.SelectedDate.Value;

            clientData.GetData(ds);
            this.PresentationData = ds;
            InputEndDate.DbSelectedDate = ds.EndDate;
            InputStartDate.DbSelectedDate = ds.StartDate; 
            DataView listedSecurities = new DataView(ds.securitySummaryTable);
            listedSecurities.RowFilter = ds.securitySummaryTable.TYPE + "= 'SEC'";
            this.InfoGrid.DataSource = listedSecurities.ToTable();
            this.InfoGrid.DataBind();
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }

        protected void GenerateReport(object sender, EventArgs e)
        {
            ChessStatementsClick(); 
        }

        protected void FilterGrid(object sender, C1GridViewFilterEventArgs e)
        {
            IBrokerManagedComponent clientData = GetData();
            ChessStatementsClick();
            this.UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        private IBrokerManagedComponent GetData()
        {
            this.cid = Request.QueryString["ins"].ToString();
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            HoldingSummaryReportDS ds = new HoldingSummaryReportDS();
            if (InputEndDate.DateInput.SelectedDate.HasValue)
                ds.EndDate = InputEndDate.DateInput.SelectedDate.Value;

            if (this.InputStartDate.DateInput.SelectedDate.HasValue)
                ds.StartDate = this.InputStartDate.DateInput.SelectedDate.Value;

            clientData.GetData(ds);
            this.PresentationData = ds;
            return clientData;
        }

        protected void SortGrid(object sender, C1GridViewSortEventArgs e)
        {
            IBrokerManagedComponent clientData = GetData();
            ChessStatementsClick();
            this.UMABroker.ReleaseBrokerManagedComponent(clientData);
        }
    }
}
