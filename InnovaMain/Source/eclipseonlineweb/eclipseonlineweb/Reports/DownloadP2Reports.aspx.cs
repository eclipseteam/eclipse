﻿using System;
using System.Linq;
using Oritax.TaxSimp.FileImport;

namespace eclipseonlineweb.Reports
{
    public partial class DownloadP2Reports : UMABasePage
    {
        public override void LoadPage()
        {
            base.LoadPage();

            Guid bmcId = new Guid(Request["bmcid"]);

            byte[] downloadStream;
            string path;

            this.GetFile(bmcId, out downloadStream, out path);
            this.DownloadFile(downloadStream, this.GetName(path), this.GetExtention(path));
        }

        private string GetName(string path)
        {
            string name = path;
            if (path.Contains('\\'))
            {
                name = path.Substring(path.LastIndexOf('\\') + 1);
            }
            return name;
        }

        private string GetExtention(string path)
        {
            string ext = path.Substring(path.LastIndexOf('.') + 1);
            return ext;
        }

        private void GetFile(Guid bmcId, out byte[] stream, out string path)
        {
            FileImportPersistentDS fds = new FileImportPersistentDS();
            var fi = this.UMABroker.GetBMCInstance(bmcId);
            fi.GetData(fds);

            stream = (byte[])
                fds.Tables[FileImportPersistentDS.BLOBPERSIST_TABLE].Rows[0][FileImportPersistentDS.BLOBPERSIST_PERSISTENT_DATA_FIELD];
            path = (string) fds.Tables[FileImportPersistentDS.FILEIMPORT_TABLE].Rows[0][FileImportPersistentDS.FILEIMPORT_ORIGINPATH_FIELD];
           
            UMABroker.ReleaseBrokerManagedComponent(fi);

        }

        private void DownloadFile(byte[] stream, string fileName, string fileExt)
        {
            string contentType;
            switch (fileExt.ToLower())
            {
                case "pdf":
                    contentType = "application/pdf";
                    break;
                case "txt":
                    contentType = "text/plain";
                    break;
                case "doc":
                case "rtf":
                case "docx":
                    contentType = "Application/msword";
                    break;
                case "xls":
                case "xlsx":
                    contentType = "Application/x-msexcel";
                    break;
                case "jpg":
                case "jpeg":
                    contentType = "image/jpeg";
                    break;
                case "gif":
                    contentType = "image/GIF";
                    break;
                default:
                    contentType = "application/octet-stream";
                    break;
            }

            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.AppendHeader("Content-Type", contentType);
            Response.AppendHeader("Content-disposition", "attachment; filename=" + fileName);
            Response.BinaryWrite(stream);
            Response.Flush();
            Response.End();
        }

    }
}