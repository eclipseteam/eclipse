﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="ReportsMaster.master"
    AutoEventWireup="true" CodeBehind="ChessHoldingStatementReportViewer.aspx.cs"
    Inherits="eclipseonlineweb.ChessHoldingStatementReportViewer" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer"
    TagPrefix="C1ReportViewer" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                       <td style="width: 5%">
                            <span class="riLabel">Start Date:</span><telerik:RadDatePicker ID="InputStartDate"
                                runat="server">
                            </telerik:RadDatePicker>
                        </td>
                        <td style="width: 5%">
                        <span class="riLabel">End Date:</span><telerik:RadDatePicker ID="InputEndDate"
                                runat="server">
                            </telerik:RadDatePicker>
                            
                        </td>
                        <td style="width: 5%"><br />
                        <telerik:RadButton runat="server" ID="BtnGenerateReport" OnClick="GenerateReport" Text="Generate Report"></telerik:RadButton>
                        </td>
                        <td>
                        </td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <C1ReportViewer:C1ReportViewer CollapseToolsPanel="true" Cache-Enabled="false" Cache-ShareBetweenSessions="false"
                FileName="ReportViewer" runat="server" ID="ReportViewer" Zoom="75%" Height="550px"
                Width="100%">
            </C1ReportViewer:C1ReportViewer>
            <asp:Label Text="" ID="lblSecCode" Visible="false" runat="server"></asp:Label>
            <asp:Label Text="" ID="lblSecDesc" Visible="false" runat="server"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
