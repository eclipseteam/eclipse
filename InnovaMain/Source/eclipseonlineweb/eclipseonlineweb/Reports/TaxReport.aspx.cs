﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using Oritax.TaxSimp.Utilities;

namespace eclipseonlineweb
{
    public partial class TaxReport : UMABasePage
    {
       
        private List<AssetEntity> assets = null;
        string selectCostType = string.Empty; 
        public override void PopulatePage(DataSet ds)
        {
            if (!Page.IsPostBack)
            {
                comboBoxCostType.DataSource = Enumeration.GetAll<CostTypes>();
                comboBoxCostType.DataTextField = "Value";
                comboBoxCostType.DataValueField = "Key";
                comboBoxCostType.DataBind();

                var defaultItem = comboBoxCostType.Items.Where(i => i.Text == "FIFO").FirstOrDefault();
                if (defaultItem != null)
                    defaultItem.Selected = true; 
            }

            this.cid = Request.QueryString["ins"].ToString();
            ReportClick();
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public static void AddFooter(C1PrintDocument doc)
        {
            doc.PageLayouts.PrintFooterOnLastPage = true;
            RenderTable footer = new RenderTable();

            footer.Rows[0].Height = .30;
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;
            footer.Rows[1].Height = 2.5;
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.TextAlignVert = AlignVertEnum.Center;

            footer.Cells[0, 0].Text = "FOR YOUR INFORMATION";
            footer.Cells[0, 0].Style.TextAlignHorz = AlignHorzEnum.Center;
            footer.Cells[0, 0].Style.FontSize = 12;

            footer.Cells[1, 0].Text = "\n1. This report provides an estimate of the realised and unrealised gains and losses of the portfolio for the period.  It does not constitute tax advice, nor an actual taxation outcome.  The report is provided for general information and is not intended to be considered as comprehensive tax information or relied upon without reference to an adviser and/or accountant.\n\nTax law depends upon an investors specific circumstances and the assumptions made in  this report may not be relevant to the investor." +
                                        "\n\n2. Information has been provided on a cash rather than accrual basis.  Income listed on the report are amounts that have been received as deposits in the linked CMA account and correctly identified as an income transaction." +
                                        "\n\n3. This report is provided by e-Clipse Online Pty Limited ABN 70 145 358 630, AFSL 357 306 (“e-Clipse”) and is based on information provided to e-Clipse by third parties.  Whilst every reasonable effort has been made by e-Clipse to ensure its accuracy, neither e-Clipse nor any of its related entities guarantee its accuracy nor accept any liability for any errors or omissions." +
                                        "\n\ne-Clipse Online Pty Ltd (ABN 70 145 358 630)\n3/36 Bydown Street, Neutral Bay, NSW 2089\nhttp://www.e-clipse.com.au\nP: +61 2 9346 4686";
            footer.Cells[1, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            footer.Cells[1, 0].Style.TextAlignVert = AlignVertEnum.Top;
            footer.Cells[1, 0].Style.FontSize = 9;
            footer.Cells[1, 0].Style.FontBold = false;

            doc.PageLayouts.LastPage = new PageLayout();
            doc.PageLayouts.LastPage.PageFooter = footer;
        }

        public C1PrintDocument MakeDoc()
        {
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            DataRow clientSummaryRow = this.PresentationData.Tables[LossAndGainsReportDS.CLIENTSUMMARYTABLE].Rows[0];
            AddClientHeaderToReport(doc, clientSummaryRow, ((LossAndGainsReportDS)this.PresentationData).DateInfo(), "TAX REPORT");
            RenderText gridHeaderTotal = new RenderText();
            if(comboBoxCostType.SelectedItem != null)
                gridHeaderTotal.Text = "*Cost Calculation Method - " + comboBoxCostType.SelectedItem.Text + "\n\n";
            else
                gridHeaderTotal.Text = "*Cost Calculation Method - " + comboBoxCostType.Text + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 8;
            gridHeaderTotal.Style.FontBold = true;
            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(lineBreakTotal);

            SetTaxReport(doc);
            AddFooter(doc);
            return doc;
        }

        private void SetTaxReport(C1PrintDocument doc)
        {
            DataView secSummaryTableViewOverall = new DataView(PresentationData.Tables[LossAndGainsReportDS.TAXREPORTINGMATRIXTABLE]);
            secSummaryTableViewOverall.Sort = LossAndGainsReportDS.ACCOUNTTYPE + " ASC";

            SummmaryTableOverall(doc, secSummaryTableViewOverall.ToTable());

            DataView secSummaryTableView = new DataView(PresentationData.Tables[LossAndGainsReportDS.SECURITYSUMMARYTABLEUNREALISED]);
            secSummaryTableView.Sort = LossAndGainsReportDS.SECNAME + " ASC";

            var accountGroup = secSummaryTableView.ToTable().Select().GroupBy(rows => rows[LossAndGainsReportDS.ACCNO]);
            foreach (var account in accountGroup)
                UnRealisedDetailTable(doc, account);

            DataView secSummaryTableViewRealised = new DataView(PresentationData.Tables[LossAndGainsReportDS.SECURITYSUMMARYTABLEREALISED]);
            secSummaryTableViewRealised.Sort = LossAndGainsReportDS.SECNAME + " ASC";

            var accountGroupRealised = secSummaryTableViewRealised.ToTable().Select().GroupBy(rows => rows[LossAndGainsReportDS.ACCNO]);
            foreach (var account in accountGroupRealised)
                RealisedDetailTable(doc, account);

            DataView interestIncomeTrans = new DataView(PresentationData.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS]);
            interestIncomeTrans.RowFilter = LossAndGainsReportDS.SYSTRANSACIONTYPE + "= 'Interest' OR " + LossAndGainsReportDS.SYSTRANSACIONTYPE + "= 'Commission Rebate'";
            interestIncomeTrans.Sort = LossAndGainsReportDS.ACCOUNTNO + " ASC";

            DataView disIncomeTrans = new DataView(PresentationData.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS]);
            disIncomeTrans.RowFilter = LossAndGainsReportDS.SYSTRANSACIONTYPE + "= 'Distribution'";
            disIncomeTrans.Sort = LossAndGainsReportDS.ACCOUNTNO + " ASC";

            DataView divIncomeTrans = new DataView(PresentationData.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS]);
            divIncomeTrans.RowFilter = LossAndGainsReportDS.SYSTRANSACIONTYPE + "= 'Dividend'";
            divIncomeTrans.Sort = LossAndGainsReportDS.ACCOUNTNO + " ASC";

            DataView rentalIncomeTrans = new DataView(PresentationData.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS]);
            rentalIncomeTrans.RowFilter = LossAndGainsReportDS.SYSTRANSACIONTYPE + "= 'Rental Income'";
            rentalIncomeTrans.Sort = LossAndGainsReportDS.ACCOUNTNO + " ASC";

            BankDetailTable(doc, interestIncomeTrans.ToTable(), "Interest", "Income");
            BankDetailTable(doc, disIncomeTrans.ToTable(), "Distribution", "Income");
            BankDetailTable(doc, divIncomeTrans.ToTable(), "Dividend", "Income");
            BankDetailTable(doc, rentalIncomeTrans.ToTable(), "Rental Income", "Income");
        }

        private static void UnRealisedDetailTable(C1PrintDocument doc, IGrouping<object, DataRow> account)
        {
            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.Style.FontSize = 7;
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Code";
            secSummaryTotalTable.Cells[0, 1].Text = "Description";
            secSummaryTotalTable.Cells[0, 2].Text = "Opening";
            secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 3].Text = "Mvt";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 4].Text = "Closing";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 5].Text = "Avg. Cost Price";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 6].Text = "Trade Val ($)";
            secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 7].Text = "Fees";
            secSummaryTotalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 8].Text = "Cost Val ($)";
            secSummaryTotalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 9].Text = "Current Price";
            secSummaryTotalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 10].Text = "Curent Val ($)";
            secSummaryTotalTable.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 11].Text = "Gains / Losses";
            secSummaryTotalTable.Cells[0, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 9;
            secSummaryTotalTable.Cols[2].Width = 2;
            secSummaryTotalTable.Cols[3].Width = 2;
            secSummaryTotalTable.Cols[4].Width = 2;
            secSummaryTotalTable.Cols[5].Width = 3;
            secSummaryTotalTable.Cols[6].Width = 3;
            secSummaryTotalTable.Cols[7].Width = 2;
            secSummaryTotalTable.Cols[8].Width = 3;
            secSummaryTotalTable.Cols[9].Width = 3;
            secSummaryTotalTable.Cols[10].Width = 3;
            secSummaryTotalTable.Cols[11].Width = 3;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Unrealised Gains & Losses - " + account.Key.ToString().ToUpper() + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            decimal grandTotal = 0;
            decimal grandTotalUnrealisedValue = 0;
            decimal tradeTotalValue = 0;
            decimal grandProfitLoss = 0;


            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            foreach (DataRow secRow in account)
            {
                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[LossAndGainsReportDS.SECNAME].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[LossAndGainsReportDS.DESCRIPTION].ToString();
                if (secRow[LossAndGainsReportDS.OPENINGBALUNITS] != null & secRow[LossAndGainsReportDS.OPENINGBALUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.OPENINGBALUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.MOVEMENTUNITS] != null & secRow[LossAndGainsReportDS.MOVEMENTUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.MOVEMENTUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.CLOSINGBALUNITS] != null & secRow[LossAndGainsReportDS.CLOSINGBALUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.CLOSINGBALUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.UNREALISEDCOSTPRICE] != null & secRow[LossAndGainsReportDS.UNREALISEDCOSTPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISEDCOSTPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;


                decimal tradeValue = 0;
                if (secRow[LossAndGainsReportDS.TRADEVALUE] != null & secRow[LossAndGainsReportDS.TRADEVALUE] != DBNull.Value)
                    tradeValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.TRADEVALUE]);
                else
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 6].Text = tradeValue.ToString("C");
                tradeTotalValue += tradeValue;
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal fees = 0;
                if (secRow[LossAndGainsReportDS.FEE] != null & secRow[LossAndGainsReportDS.FEE] != DBNull.Value)
                    fees = Convert.ToDecimal(secRow[LossAndGainsReportDS.FEE]);
                else
                    secSummaryTotalTable.Cells[totalIndex, 7].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 7].Text = fees.ToString("C");
                secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;


                decimal unrealisedCostValue = 0;
                if (secRow[LossAndGainsReportDS.UNREALISEDCOSTVALUE] != null & secRow[LossAndGainsReportDS.UNREALISEDCOSTVALUE] != DBNull.Value)
                    unrealisedCostValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISEDCOSTVALUE]);
                else
                    secSummaryTotalTable.Cells[totalIndex, 8].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 8].Text = unrealisedCostValue.ToString("C");
                grandTotalUnrealisedValue += unrealisedCostValue;
                secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;


                if (secRow[LossAndGainsReportDS.UNITPRICE] != null & secRow[LossAndGainsReportDS.UNITPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNITPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
                decimal total = Convert.ToDecimal(secRow[LossAndGainsReportDS.TOTAL]);
                secSummaryTotalTable.Cells[totalIndex, 10].Text = total.ToString("C");
                grandTotal += total;

                secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal unrealisedProfitLoss = 0;
                if (secRow[LossAndGainsReportDS.UNREALISEDPROFITLOSS] != null & secRow[LossAndGainsReportDS.UNREALISEDPROFITLOSS] != DBNull.Value)
                {
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISEDPROFITLOSS]).ToString("C");
                    unrealisedProfitLoss = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISEDPROFITLOSS]);
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
                if (unrealisedProfitLoss > 0)
                    secSummaryTotalTable.Cells[totalIndex, 11].Style.TextColor = Color.Green;
                else
                    secSummaryTotalTable.Cells[totalIndex, 11].Style.TextColor = Color.Red;

                grandProfitLoss += unrealisedProfitLoss;

                totalIndex++;
            }

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            secSummaryTotalTable.Cells[totalIndex, 1].Text = "";
            secSummaryTotalTable.Cells[totalIndex, 6].Text = tradeTotalValue.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 8].Text = grandTotalUnrealisedValue.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 10].Text = grandTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

            if (grandProfitLoss > 0)
                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextColor = Color.Green;
            else
                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextColor = Color.Red;

            secSummaryTotalTable.Cells[totalIndex, 11].Text = grandProfitLoss.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
        }

        private static void RealisedDetailTable(C1PrintDocument doc, IGrouping<object, DataRow> account)
        {
            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.Style.FontSize = 7;
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Sec. Code";
            secSummaryTotalTable.Cells[0, 1].Text = "Description";
            secSummaryTotalTable.Cells[0, 2].Text = "Sold Units";
            secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 3].Text = "Avg. Cost Price";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 4].Text = "Cost Value ($)";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 5].Text = "Sale Value ($)";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 6].Text = "Gains / Losses";
            secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 9;
            secSummaryTotalTable.Cols[2].Width = 3;
            secSummaryTotalTable.Cols[3].Width = 3;
            secSummaryTotalTable.Cols[4].Width = 3;
            secSummaryTotalTable.Cols[5].Width = 3;
            secSummaryTotalTable.Cols[6].Width = 3;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Realised Gains & Losses - " + account.Key.ToString().ToUpper() + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            decimal grandTotal = 0;
            decimal grandTotalrealisedValue = 0;
            decimal grandProfitLoss = 0;


            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            foreach (DataRow secRow in account)
            {
                string secCode = secRow[LossAndGainsReportDS.SECNAME].ToString();

                //IV004 && IV014 have special col names
                if (secCode == "IV004" || secCode == "IV014")
                {
                    secSummaryTotalTable.Cells[0, 0].Text = "Sec. Code";
                    secSummaryTotalTable.Cells[0, 1].Text = "Description";
                    secSummaryTotalTable.Cells[0, 2].Text = "Total Units";
                    secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 3].Text = "Avg. Cost Price";
                    secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 4].Text = "Cost Value ($)";
                    secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 5].Text = "Current Value ($)";
                    secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 6].Text = "Gains / Losses";
                    secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                }

                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[LossAndGainsReportDS.SECNAME].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[LossAndGainsReportDS.DESCRIPTION].ToString();

                if (secRow[LossAndGainsReportDS.SOLDUNITS] != null & secRow[LossAndGainsReportDS.SOLDUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.SOLDUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.REALISEDCOSTPRICE] != null & secRow[LossAndGainsReportDS.REALISEDCOSTPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISEDCOSTPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                decimal realisedCostValue = 0;
                if (secRow[LossAndGainsReportDS.REALISEDCOSTVALUE] != null & secRow[LossAndGainsReportDS.REALISEDCOSTVALUE] != DBNull.Value)
                    realisedCostValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISEDCOSTVALUE]);
                else
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 4].Text = realisedCostValue.ToString("C");
                grandTotalrealisedValue += realisedCostValue;
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal saleProceeds = Convert.ToDecimal(secRow[LossAndGainsReportDS.SALESPROCEED]);
                secSummaryTotalTable.Cells[totalIndex, 5].Text = saleProceeds.ToString("C");
                grandTotal += saleProceeds;

                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal realisedProfitLoss = 0;
                if (secRow[LossAndGainsReportDS.REALISEDPROFITLOSS] != null & secRow[LossAndGainsReportDS.REALISEDPROFITLOSS] != DBNull.Value)
                {
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISEDPROFITLOSS]).ToString("C");
                    realisedProfitLoss = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISEDPROFITLOSS]);
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                if (realisedProfitLoss > 0)
                    secSummaryTotalTable.Cells[totalIndex, 6].Style.TextColor = Color.Green;
                else
                    secSummaryTotalTable.Cells[totalIndex, 6].Style.TextColor = Color.Red;

                grandProfitLoss += realisedProfitLoss;

                totalIndex++;
            }
            

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            secSummaryTotalTable.Cells[totalIndex, 1].Text = "";
            secSummaryTotalTable.Cells[totalIndex, 4].Text = grandTotalrealisedValue.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 5].Text = grandTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

            if (grandProfitLoss > 0)
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextColor = Color.Green;
            else
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextColor = Color.Red;

            secSummaryTotalTable.Cells[totalIndex, 6].Text = grandProfitLoss.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
        }

        private static void BankDetailTable(C1PrintDocument doc, DataTable table, string sysCategory, string category)
        {
            if (table.Rows.Count > 0)
            {
                RenderTable secSumTable = new RenderTable();
                secSumTable.RowGroups[0, 1].PageHeader = true;
                RenderTable secSummaryTotalTable = new RenderTable();
                secSummaryTotalTable.Style.FontSize = 7;
                secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
                secSummaryTotalTable.Rows[0].Height = .2;
                secSummaryTotalTable.Rows[0].Style.FontBold = true;
                secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                secSummaryTotalTable.Rows[0].Style.FontBold = true;
                secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                secSummaryTotalTable.Cells[0, 0].Text = "Account Name";
                secSummaryTotalTable.Cells[0, 1].Text = "Account NO";
                secSummaryTotalTable.Cells[0, 2].Text = "Transaction Date";
                secSummaryTotalTable.Cells[0, 3].Text = "Description";
                secSummaryTotalTable.Cells[0, 4].Text = "Amount";
                secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                secSummaryTotalTable.Cols[0].Width = 9;
                secSummaryTotalTable.Cols[1].Width = 3;
                secSummaryTotalTable.Cols[2].Width = 3;
                secSummaryTotalTable.Cols[3].Width = 5;
                secSummaryTotalTable.Cols[4].Width = 3;


                RenderText gridHeaderTotal = new RenderText();
                gridHeaderTotal.Text = category.ToUpper() + " - " + sysCategory.ToUpper() + " Transactions\n\n";
                gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeaderTotal.Style.FontSize = 11;
                gridHeaderTotal.Style.FontBold = true;

                decimal grandTotal = 0;

                RenderText lineBreakTotal = new RenderText();
                lineBreakTotal.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeaderTotal);
                doc.Body.Children.Add(secSummaryTotalTable);
                doc.Body.Children.Add(lineBreakTotal);
                int totalIndex = 1;

                foreach (DataRow secRow in table.Rows)
                {
                    secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[LossAndGainsReportDS.ACCOUNTNAME].ToString();
                    secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[LossAndGainsReportDS.ACCOUNTNO].ToString();
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = ((DateTime)secRow[LossAndGainsReportDS.TRANSACTIONDATE]).ToString("dd/MM/yyyy");
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = secRow[LossAndGainsReportDS.DESCRIPTION].ToString();

                    decimal totalValue = 0;

                    if (secRow[LossAndGainsReportDS.TOTAL] != null & secRow[LossAndGainsReportDS.TOTAL] != DBNull.Value)
                    {
                        totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.TOTAL]);
                        secSummaryTotalTable.Cells[totalIndex, 4].Text = totalValue.ToString("C");
                    }
                    else
                        secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";
                    secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    grandTotal += totalValue;

                    totalIndex++;
                }

                secSummaryTotalTable.Rows[totalIndex].Height = .2;
                secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
                secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
                secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

                secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
                secSummaryTotalTable.Cells[totalIndex, 1].Text = "";
                secSummaryTotalTable.Cells[totalIndex, 4].Text = grandTotal.ToString("C");
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            }
        }

        private static void SummmaryTableOverall(C1PrintDocument doc, DataTable overallSummary)
        {
            RenderTable secSumTableIncomeSection = new RenderTable();
            secSumTableIncomeSection.RowGroups[0, 1].PageHeader = true;
            secSumTableIncomeSection.Rows[0].Height = .2;
            secSumTableIncomeSection.Rows[0].Style.FontBold = true;
            secSumTableIncomeSection.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSumTableIncomeSection.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSumTableIncomeSection.Rows[0].Style.FontBold = true;
            secSumTableIncomeSection.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSumTableIncomeSection.Cells[0, 0].Text = "Income Category";
            secSumTableIncomeSection.Cells[0, 1].Text = "Amount";
            secSumTableIncomeSection.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Style.FontSize = 7;

            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Acc Type";
            secSummaryTotalTable.Cells[0, 1].Text = "Account NO";

            secSummaryTotalTable.Cells[0, 2].Text = "Opening";
            secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 3].Text = "Investment";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 4].Text = "Tran. In/Out";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 5].Text = "Interest";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 6].Text = "Rental";
            secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 7].Text = "Distribution";
            secSummaryTotalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 8].Text = "Dividend";
            secSummaryTotalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 9].Text = "Expenses";
            secSummaryTotalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 10].Text = "Change in Val.";
            secSummaryTotalTable.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 11].Text = "Closing";
            secSummaryTotalTable.Cells[0, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 12].Text = "Unrealised";
            secSummaryTotalTable.Cells[0, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 13].Text = "Realised";
            secSummaryTotalTable.Cells[0, 13].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 4;
            secSummaryTotalTable.Cols[2].Width = 2.5;
            secSummaryTotalTable.Cols[3].Width = 2.5;
            secSummaryTotalTable.Cols[4].Width = 2.5;
            secSummaryTotalTable.Cols[5].Width = 2.5;
            secSummaryTotalTable.Cols[6].Width = 2.5;
            secSummaryTotalTable.Cols[7].Width = 2.5;
            secSummaryTotalTable.Cols[8].Width = 2.5;
            secSummaryTotalTable.Cols[9].Width = 2.5;
            secSummaryTotalTable.Cols[10].Width = 2.5;
            secSummaryTotalTable.Cols[11].Width = 2.5;
            secSummaryTotalTable.Cols[12].Width = 2.5;
            secSummaryTotalTable.Cols[13].Width = 2.5;
            
        

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Tax Summary\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 12;
            gridHeaderTotal.Style.FontBold = true;

            decimal openingBalTotal = 0;
            decimal investTotal = 0;
            decimal transferInTotal = 0;
            decimal transferOutTotal = 0;
            decimal changeInMktValTotal = 0;
            decimal interestTotal = 0;
            decimal rentalTotal = 0;
            decimal disTotal = 0;
            decimal divTotal = 0;
            decimal expTotal = 0;
            decimal realisedTotal = 0;
            decimal unRealisedTotal = 0;
            decimal closingTotal = 0;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            RenderText lineBreakTotal2 = new RenderText();
            lineBreakTotal2.Text = "\n\n\n";


            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSumTableIncomeSection);
            doc.Body.Children.Add(lineBreakTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal2);
            int totalIndex = 1;

            foreach (DataRow secRow in overallSummary.Rows)
            {
                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[LossAndGainsReportDS.ACCOUNTTYPE].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[LossAndGainsReportDS.ACCOUNTNO].ToString();

                decimal totalValue = 0;

                if (secRow[LossAndGainsReportDS.OPENINGBALANCE] != null & secRow[LossAndGainsReportDS.OPENINGBALANCE] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.OPENINGBALANCE]);
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                openingBalTotal += totalValue;


                totalValue = 0;

                if (secRow[LossAndGainsReportDS.INVESTMENT] != null & secRow[LossAndGainsReportDS.INVESTMENT] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.INVESTMENT]);
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                investTotal += totalValue;

                totalValue = 0;
                decimal transgerIn = 0; 
                decimal transferOut = 0; 
                if (secRow[LossAndGainsReportDS.TRANSFERIN] != null & secRow[LossAndGainsReportDS.TRANSFERIN] != DBNull.Value)
                    transgerIn = Convert.ToDecimal(secRow[LossAndGainsReportDS.TRANSFERIN]);
                if (secRow[LossAndGainsReportDS.TRANSFEROUT] != null & secRow[LossAndGainsReportDS.TRANSFEROUT] != DBNull.Value)
                    transferOut = Convert.ToDecimal(secRow[LossAndGainsReportDS.TRANSFEROUT]);

                secSummaryTotalTable.Cells[totalIndex, 4].Text = (transgerIn + transferOut).ToString("C");
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                transferInTotal += (transgerIn + transferOut);

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.INTEREST] != null & secRow[LossAndGainsReportDS.INTEREST] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.INTEREST]);
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                interestTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.RENTAL] != null & secRow[LossAndGainsReportDS.RENTAL] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.RENTAL]);
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                rentalTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.DISTRIBUTION] != null & secRow[LossAndGainsReportDS.DISTRIBUTION] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.DISTRIBUTION]);
                    secSummaryTotalTable.Cells[totalIndex, 7].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 7].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                disTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.DIVIDEND] != null & secRow[LossAndGainsReportDS.DIVIDEND] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.DIVIDEND]);
                    secSummaryTotalTable.Cells[totalIndex, 8].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 8].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                divTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.EXPENSES] != null & secRow[LossAndGainsReportDS.EXPENSES] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.EXPENSES]);
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                expTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.CLOSINGBALANCE] != null & secRow[LossAndGainsReportDS.CLOSINGBALANCE] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.CLOSINGBALANCE]);
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

                closingTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.CHANGEINMKT] != null & secRow[LossAndGainsReportDS.CHANGEINMKT] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.CHANGEINMKT]);
                    secSummaryTotalTable.Cells[totalIndex, 10].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 10].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                changeInMktValTotal += totalValue;

                
                totalValue = 0;

                if (secRow[LossAndGainsReportDS.UNREALISED] != null & secRow[LossAndGainsReportDS.UNREALISED] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISED]);
                    secSummaryTotalTable.Cells[totalIndex, 12].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 12].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

                unRealisedTotal += totalValue;

                totalValue = 0;

                if (secRow[LossAndGainsReportDS.REALISED] != null & secRow[LossAndGainsReportDS.REALISED] != DBNull.Value)
                {
                    totalValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISED]);
                    secSummaryTotalTable.Cells[totalIndex, 13].Text = totalValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 13].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 13].Style.TextAlignHorz = AlignHorzEnum.Right;
                
                realisedTotal += totalValue;


                totalIndex++;
            }

            secSumTableIncomeSection.Cells[1, 0].Text = "Interest";
            secSumTableIncomeSection.Cells[1, 1].Text = interestTotal.ToString("C");
            secSumTableIncomeSection.Cells[1, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSumTableIncomeSection.Cells[2, 0].Text = "Dividend";
            secSumTableIncomeSection.Cells[2, 1].Text =  divTotal.ToString("C");
            secSumTableIncomeSection.Cells[2, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSumTableIncomeSection.Cells[3, 0].Text = "Distribution";
            secSumTableIncomeSection.Cells[3, 1].Text = disTotal.ToString("C");
            secSumTableIncomeSection.Cells[3, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSumTableIncomeSection.Cells[4, 0].Text = "Rental";
            secSumTableIncomeSection.Cells[4, 1].Text = rentalTotal.ToString("C");
            secSumTableIncomeSection.Cells[4, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSumTableIncomeSection.Cells[5, 0].Text = "Realised Gains (Securities & Funds)";
            secSumTableIncomeSection.Cells[5, 1].Text =  realisedTotal.ToString("C");
            secSumTableIncomeSection.Cells[5, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSumTableIncomeSection.Rows[6].Height = .2;
            secSumTableIncomeSection.Rows[6].Style.FontBold = true;
            secSumTableIncomeSection.Rows[6].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSumTableIncomeSection.Rows[6].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSumTableIncomeSection.Rows[6].Style.FontBold = true;
            secSumTableIncomeSection.Rows[6].Style.TextAlignVert = AlignVertEnum.Center;

            secSumTableIncomeSection.Cells[6, 0].Text = "TOTAL INCOME";
            secSumTableIncomeSection.Cells[6, 1].Text = (realisedTotal + disTotal + divTotal + interestTotal + rentalTotal).ToString("C");
            secSumTableIncomeSection.Cells[6, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            secSummaryTotalTable.Cells[totalIndex, 1].Text = "";

            secSummaryTotalTable.Cells[totalIndex, 2].Text = openingBalTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 3].Text = investTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 4].Text = (transferInTotal+transferOutTotal).ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 5].Text = interestTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 6].Text = rentalTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 7].Text = disTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 8].Text = divTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 9].Text = expTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 10].Text = changeInMktValTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 11].Text = closingTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 12].Text = unRealisedTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 12].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 13].Text = realisedTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 13].Style.TextAlignHorz = AlignHorzEnum.Right;
        }

        protected void GenerateReport(object sender, EventArgs e)
        {
            ReportClick();
        }

        protected void ReportClick()
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"].ToString()));
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            this.assets = organization.Assets;
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);
            LossAndGainsReportDS ds = new LossAndGainsReportDS();

            if (comboBoxCostType.SelectedItem != null)
            {
                if (comboBoxCostType.SelectedItem.Text == "LIFO")
                    ds.CostTypes = CostTypes.LIFO;
                if (comboBoxCostType.SelectedItem.Text == "AVG")
                    ds.CostTypes = CostTypes.AVG;
            }

            if (this.InputStartDate.DateInput.SelectedDate.HasValue)
                ds.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(this.InputStartDate.DateInput.SelectedDate.Value);

            if (this.InputEndDate.DateInput.SelectedDate.HasValue)
                ds.EndDate =  AccountingFinancialYear.LastDayOfMonthFromDateTime(this.InputEndDate.DateInput.SelectedDate.Value);

            clientData.GetData(ds);
            this.PresentationData = ds;

            DataRow clientSummaryRow = ds.Tables[LossAndGainsReportDS.CLIENTSUMMARYTABLE].Rows[0];

            string reportName = "Report_" + Guid.NewGuid().ToString();
            C1ReportViewer.RegisterDocument(reportName, MakeDoc);
            this.C1ReportViewerAssetClass.FileName = reportName;
            C1ReportViewerAssetClass.ReportName = reportName;
            InputEndDate.DbSelectedDate = ds.EndDate;
            InputStartDate.DbSelectedDate = ds.StartDate; 
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }
    }
}
