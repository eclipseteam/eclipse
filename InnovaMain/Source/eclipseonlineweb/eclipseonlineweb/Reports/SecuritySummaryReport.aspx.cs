﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;

namespace eclipseonlineweb
{
    public partial class SecuritySummaryReport : UMABasePage
    {

        private List<AssetEntity> assets = null;

        public override void PopulatePage(DataSet ds)
        {
            this.cid = Request.QueryString["ins"].ToString();
            base.PopulatePage(ds);
            ReportClick();
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public C1PrintDocument MakeDoc()
        {
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            DataRow clientSummaryRow = this.PresentationData.Tables[SecuritySummaryReportDS.CLIENTSUMMARYTABLE].Rows[0];
            AddClientHeaderToReport(doc, clientSummaryRow, ((SecuritySummaryReportDS)this.PresentationData).DateInfo(), "SECURITY SUMMARY REPORT");

            DataView secSummaryTableView = new DataView(PresentationData.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE]);
            secSummaryTableView.Sort = SecuritySummaryReportDS.SECNAME + " ASC";
            //filtering 0 value securities 
            secSummaryTableView.RowFilter = string.Format("CONVERT(Isnull({0},''), System.String) <> '0'", SecuritySummaryReportDS.TOTALUNITS);
            DataTable secFilteredTable = secSummaryTableView.ToTable();

            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.Style.FontSize = 8;
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Security Code";
            secSummaryTotalTable.Cells[0, 1].Text = "Description";
            secSummaryTotalTable.Cells[0, 2].Text = "Total Units";
            secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 3].Text = "Unit Price";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 4].Text = "Total ($)";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 5].Text = "Price At Date";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Center;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 8;
            secSummaryTotalTable.Cols[2].Width = 3;
            secSummaryTotalTable.Cols[3].Width = 3;
            secSummaryTotalTable.Cols[4].Width = 3;
            secSummaryTotalTable.Cols[5].Width = 3;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Security Summary\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            decimal grandTotal = 0;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            foreach (DataRow secRow in secFilteredTable.Rows)
            {
                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[SecuritySummaryReportDS.SECNAME].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[SecuritySummaryReportDS.DESCRIPTION].ToString();
                if (secRow[SecuritySummaryReportDS.TOTALUNITS] != null & secRow[SecuritySummaryReportDS.TOTALUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = Convert.ToDecimal(secRow[SecuritySummaryReportDS.TOTALUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                if (secRow[SecuritySummaryReportDS.UNITPRICE] != null & secRow[SecuritySummaryReportDS.UNITPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = Convert.ToDecimal(secRow[SecuritySummaryReportDS.UNITPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                decimal total = Convert.ToDecimal(secRow[SecuritySummaryReportDS.TOTAL]);
                secSummaryTotalTable.Cells[totalIndex, 4].Text = total.ToString("C");
                grandTotal += total;

                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                secSummaryTotalTable.Cells[totalIndex, 5].Text = Convert.ToDateTime(secRow[SecuritySummaryReportDS.PRICEATDATE]).ToString("dd-MMM-yyyy");
                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Center;

                totalIndex++;
            }

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            secSummaryTotalTable.Cells[totalIndex, 1].Text = "";
            secSummaryTotalTable.Cells[totalIndex, 4].Text = grandTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

            return doc;
        }

        public void SetSecuritySummaryGrid(C1PrintDocument doc, IOrderedEnumerable<DataRow> rows, string serviceName)
        {

            int index = 1;

            if (rows.Count() > 0)
            {
                RenderTable capitalTable = new RenderTable();
                capitalTable.RowGroups[0, 1].PageHeader = true;
                double OpeningBalance = 0;
                double TransferInOut = 0;
                double Income = 0;
                double AppicationRedemption = 0;
                double TaxInOut = 0;
                double Expense = 0;
                double InternalCashMovement = 0;
                double ClosingBalance = 0;
                double ChangeInInvestmentValue = 0;

                capitalTable.Style.FontSize = 8;

                capitalTable.Rows[0].Height = .2;
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.FontSize = 7;
                capitalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[0, 0].Text = "Month";
                capitalTable.Cells[0, 1].Text = "Opening Balance";
                capitalTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 2].Text = "Transfer In/Out";
                capitalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 3].Text = "Income";
                capitalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 4].Text = "Invesments";
                capitalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 5].Text = "Tax & Expenses";
                capitalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 6].Text = "Internal Cash Mvt";
                capitalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 7].Text = "Closing Balance";
                capitalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 8].Text = "Change in Investment";
                capitalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cols[0].Width = .5;
                capitalTable.Cols[1].Width = 1.2;
                capitalTable.Cols[2].Width = 1.2;
                capitalTable.Cols[3].Width = 1.2;
                capitalTable.Cols[4].Width = 1.2;
                capitalTable.Cols[5].Width = 1.2;
                capitalTable.Cols[6].Width = 1.2;
                capitalTable.Cols[7].Width = 1.2;
                capitalTable.Cols[8].Width = 1.5;

                foreach (DataRow row in rows)
                {

                    capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    capitalTable.Rows[index].Height = .3;
                    capitalTable.Cells[index, 0].Text = ((DateTime)row[CapitalReportDS.MONTH]).ToString("MMM-yy").ToUpper();

                    OpeningBalance += Convert.ToDouble(row[CapitalReportDS.OPENINGBAL]);
                    TransferInOut += Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]);
                    Income += Convert.ToDouble(row[CapitalReportDS.INCOME]);
                    AppicationRedemption += Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION]);
                    TaxInOut += Convert.ToDouble(row[CapitalReportDS.TAXINOUT]);
                    Expense += Convert.ToDouble(row[CapitalReportDS.EXPENSE]);
                    InternalCashMovement += Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]);
                    ClosingBalance += Convert.ToDouble(row[CapitalReportDS.CLOSINGBALANCE]);
                    ChangeInInvestmentValue += Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]);

                    capitalTable.Cells[index, 1].Text = Convert.ToDouble(row[CapitalReportDS.OPENINGBAL]).ToString("C");
                    capitalTable.Cells[index, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 2].Text = Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]).ToString("C");
                    capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 3].Text = Convert.ToDouble(row[CapitalReportDS.INCOME]).ToString("C");
                    capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 4].Text = Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION]).ToString("C");
                    capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 5].Text = Convert.ToDouble(row[CapitalReportDS.EXPENSE]).ToString("C");
                    capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 6].Text = Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]).ToString("C");
                    capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 7].Text = Convert.ToDouble(row[CapitalReportDS.CLOSINGBALANCE]).ToString("C");
                    capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 8].Text = Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]).ToString("C");
                    capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;


                    index++;
                }


                capitalTable.Rows[index].Height = .2;
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[index, 0].Text = "TOTAL";

                capitalTable.Cells[index, 2].Text = TransferInOut.ToString("C");
                capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 3].Text = Income.ToString("C");
                capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 4].Text = AppicationRedemption.ToString("C");
                capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 5].Text = Expense.ToString("C");
                capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 6].Text = InternalCashMovement.ToString("C");
                capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(capitalTable);
                doc.Body.Children.Add(lineBreak);
            }

        }

        protected void GenerateAssetSummaryReport(object sender, EventArgs e)
        {
            ReportClick();
        }

        protected void ReportClick()
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"].ToString()));
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            this.assets = organization.Assets;
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);
            SecuritySummaryReportDS ds = new SecuritySummaryReportDS();
            if (this.InputValutationDate.SelectedDate.HasValue)
                ds.ValuationDate = this.InputValutationDate.SelectedDate.Value;
            clientData.GetData(ds);
            this.PresentationData = ds;
            DataRow clientSummaryRow = ds.Tables[SecuritySummaryReportDS.CLIENTSUMMARYTABLE].Rows[0];
            string reportName = "Report_" + Guid.NewGuid().ToString();
            C1ReportViewer.RegisterDocument(reportName, MakeDoc);
            this.C1ReportViewerAssetClass.FileName = reportName;
            C1ReportViewerAssetClass.ReportName = reportName;
            InputValutationDate.DbSelectedDate = ds.ValuationDate;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }
    }
}
