﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp;
using Oritax.TaxSimp.Utilities;

namespace eclipseonlineweb
{
    public partial class GainsLossesReport : UMABasePage
    {
       
        private List<AssetEntity> assets = null;
        string selectCostType = string.Empty; 

        public override void PopulatePage(DataSet ds)
        {
            base.PopulatePage(ds);
 
            if (!Page.IsPostBack)
            {
                comboBoxCostType.DataSource = Enumeration.GetAll<CostTypes>();
                comboBoxCostType.DataTextField = "Value";
                comboBoxCostType.DataValueField = "Key";
                comboBoxCostType.DataBind();

                var defaultItem = comboBoxCostType.Items.Where(i => i.Text == "FIFO").FirstOrDefault();
                if (defaultItem != null)
                    defaultItem.Selected = true; 
            }

            this.cid = Request.QueryString["ins"].ToString();
            ReportClick();
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public C1PrintDocument MakeDoc()
        {
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            DataRow clientSummaryRow = this.PresentationData.Tables[LossAndGainsReportDS.CLIENTSUMMARYTABLE].Rows[0];
            AddClientHeaderToReport(doc, clientSummaryRow, ((LossAndGainsReportDS)this.PresentationData).DateInfo(), "GAINS AND LOSSES REPORT");

            RenderText gridHeaderTotal = new RenderText();
            if(comboBoxCostType.SelectedItem != null)
                gridHeaderTotal.Text = "*Cost Calculation Method - " + comboBoxCostType.SelectedItem.Text + "\n\n";
            else
                gridHeaderTotal.Text = "*Cost Calculation Method - " + comboBoxCostType.Text + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 8;
            gridHeaderTotal.Style.FontBold = true;
            
            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(lineBreakTotal);

            DataView secSummaryTableView = new DataView(PresentationData.Tables[LossAndGainsReportDS.SECURITYSUMMARYTABLEUNREALISED]);
            secSummaryTableView.Sort = LossAndGainsReportDS.SECNAME + " ASC";

            DataView secSummaryTableViewOverall = new DataView(PresentationData.Tables[LossAndGainsReportDS.GAINSLOSSESSUMMARYTABLE]);
            secSummaryTableViewOverall.Sort = LossAndGainsReportDS.ACCOUNTTYPE + " ASC";

            SummmaryTableOverall(doc, secSummaryTableViewOverall.ToTable()); 

            var accountGroup = secSummaryTableView.ToTable().Select().GroupBy(rows => rows[LossAndGainsReportDS.ACCNO]);
            foreach (var account in accountGroup)
                UnRealisedDetailTable(doc, account);

            DataView secSummaryTableViewRealised = new DataView(PresentationData.Tables[LossAndGainsReportDS.SECURITYSUMMARYTABLEREALISED]);
            secSummaryTableViewRealised.Sort = LossAndGainsReportDS.SECNAME + " ASC";

            var accountGroupRealised = secSummaryTableViewRealised.ToTable().Select().GroupBy(rows => rows[LossAndGainsReportDS.ACCNO]);
            foreach (var account in accountGroupRealised)
            RealisedDetailTable(doc, account);
            AddFooter(doc);
            return doc;
        }

        public static void AddFooter(C1PrintDocument doc)
        {
            doc.PageLayouts.PrintFooterOnLastPage = true;
            RenderTable footer = new RenderTable();
            footer.BreakBefore = BreakEnum.Page;
            footer.Rows[0].Height = .30;
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Left =     new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;
            footer.Rows[1].Height = 2.5;
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.TextAlignVert = AlignVertEnum.Center;

            footer.Cells[0, 0].Text = "FOR YOUR INFORMATION";
            footer.Cells[0, 0].Style.TextAlignHorz = AlignHorzEnum.Center;
            footer.Cells[0, 0].Style.FontSize = 12;

            footer.Cells[1, 0].Text = "\n1. This report provides an estimate of the realised and unrealised gains and losses of the portfolio for the period.  It does not constitute tax advice, nor an actual taxation outcome.  The report is provided for general information and is not intended to be considered as comprehensive tax information or relied upon without reference to an adviser and/or accountant.\n\nTax law depends upon an investors specific circumstances and the assumptions made in  this report may not be relevant to the investor." +
                                        "\n\n2. Information has been provided on a cash rather than accrual basis.  Income listed on the report are amounts that have been received as deposits in the linked CMA account and correctly identified as an income transaction." +
                                        "\n\n3. This report is provided by e-Clipse Online Pty Limited ABN 70 145 358 630, AFSL 357 306 (“e-Clipse”) and is based on information provided to e-Clipse by third parties.  Whilst every reasonable effort has been made by e-Clipse to ensure its accuracy, neither e-Clipse nor any of its related entities guarantee its accuracy nor accept any liability for any errors or omissions." +
                                        "\n\ne-Clipse Online Pty Ltd (ABN 70 145 358 630)\n3/36 Bydown Street, Neutral Bay, NSW 2089\nhttp://www.e-clipse.com.au\nP: +61 2 9346 4686";
            footer.Cells[1, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            footer.Cells[1, 0].Style.TextAlignVert = AlignVertEnum.Top;
            footer.Cells[1, 0].Style.FontSize = 9;
            footer.Cells[1, 0].Style.FontBold = false;
            doc.PageLayouts.LastPage = new PageLayout();
            doc.PageLayouts.LastPage.Document.Body.Children.Add(footer);
        }

        private static void UnRealisedDetailTable(C1PrintDocument doc, IGrouping<object, DataRow> account)
        {
            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.Style.FontSize = 7;
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Code";
            secSummaryTotalTable.Cells[0, 1].Text = "Description";
            secSummaryTotalTable.Cells[0, 2].Text = "Opening";
            secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 3].Text = "Mvt";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 4].Text = "Closing";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 5].Text = "Avg. Cost Price";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 6].Text = "Trade Val ($)";
            secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 7].Text = "Fees";
            secSummaryTotalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 8].Text = "Cost Val ($)";
            secSummaryTotalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 9].Text = "Current Price";
            secSummaryTotalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 10].Text = "Curent Val ($)";
            secSummaryTotalTable.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 11].Text = "Gains / Losses";
            secSummaryTotalTable.Cells[0, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 9;
            secSummaryTotalTable.Cols[2].Width = 2;
            secSummaryTotalTable.Cols[3].Width = 2;
            secSummaryTotalTable.Cols[4].Width = 2;
            secSummaryTotalTable.Cols[5].Width = 3;
            secSummaryTotalTable.Cols[6].Width = 3;
            secSummaryTotalTable.Cols[7].Width = 2;
            secSummaryTotalTable.Cols[8].Width = 3;
            secSummaryTotalTable.Cols[9].Width = 3;
            secSummaryTotalTable.Cols[10].Width = 3;
            secSummaryTotalTable.Cols[11].Width = 3;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Unrealised Gains & Losses - " + account.Key.ToString().ToUpper() + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            decimal grandTotal = 0;
            decimal grandTotalUnrealisedValue = 0;
            decimal tradeTotalValue = 0;
            decimal grandProfitLoss = 0;


            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            foreach (DataRow secRow in account)
            {
                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[LossAndGainsReportDS.SECNAME].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[LossAndGainsReportDS.DESCRIPTION].ToString();
                if (secRow[LossAndGainsReportDS.OPENINGBALUNITS] != null & secRow[LossAndGainsReportDS.OPENINGBALUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.OPENINGBALUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.MOVEMENTUNITS] != null & secRow[LossAndGainsReportDS.MOVEMENTUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.MOVEMENTUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.CLOSINGBALUNITS] != null & secRow[LossAndGainsReportDS.CLOSINGBALUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.CLOSINGBALUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.UNREALISEDCOSTPRICE] != null & secRow[LossAndGainsReportDS.UNREALISEDCOSTPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISEDCOSTPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 5].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;


                decimal tradeValue = 0;
                if (secRow[LossAndGainsReportDS.TRADEVALUE] != null & secRow[LossAndGainsReportDS.TRADEVALUE] != DBNull.Value)
                    tradeValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.TRADEVALUE]);
                else
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 6].Text = tradeValue.ToString("C");
                tradeTotalValue += tradeValue;
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal fees = 0;
                if (secRow[LossAndGainsReportDS.FEE] != null & secRow[LossAndGainsReportDS.FEE] != DBNull.Value)
                    fees = Convert.ToDecimal(secRow[LossAndGainsReportDS.FEE]);
                else
                    secSummaryTotalTable.Cells[totalIndex, 7].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 7].Text = fees.ToString("C");
                secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;


                decimal unrealisedCostValue = 0;
                if (secRow[LossAndGainsReportDS.UNREALISEDCOSTVALUE] != null & secRow[LossAndGainsReportDS.UNREALISEDCOSTVALUE] != DBNull.Value)
                    unrealisedCostValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISEDCOSTVALUE]);
                else
                    secSummaryTotalTable.Cells[totalIndex, 8].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 8].Text = unrealisedCostValue.ToString("C");
                grandTotalUnrealisedValue += unrealisedCostValue;
                secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;


                if (secRow[LossAndGainsReportDS.UNITPRICE] != null & secRow[LossAndGainsReportDS.UNITPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNITPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
                decimal total = Convert.ToDecimal(secRow[LossAndGainsReportDS.TOTAL]);
                secSummaryTotalTable.Cells[totalIndex, 10].Text = total.ToString("C");
                grandTotal += total;

                secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal unrealisedProfitLoss = 0;
                if (secRow[LossAndGainsReportDS.UNREALISEDPROFITLOSS] != null & secRow[LossAndGainsReportDS.UNREALISEDPROFITLOSS] != DBNull.Value)
                {
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISEDPROFITLOSS]).ToString("C");
                    unrealisedProfitLoss = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISEDPROFITLOSS]);
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
                if (unrealisedProfitLoss > 0)
                    secSummaryTotalTable.Cells[totalIndex, 11].Style.TextColor = Color.Green;
                else
                    secSummaryTotalTable.Cells[totalIndex, 11].Style.TextColor = Color.Red;

                grandProfitLoss += unrealisedProfitLoss;

                totalIndex++;
            }

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            secSummaryTotalTable.Cells[totalIndex, 1].Text = "";
            secSummaryTotalTable.Cells[totalIndex, 6].Text = tradeTotalValue.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 8].Text = grandTotalUnrealisedValue.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 10].Text = grandTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

            if (grandProfitLoss > 0)
                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextColor = Color.Green;
            else
                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextColor = Color.Red;

            secSummaryTotalTable.Cells[totalIndex, 11].Text = grandProfitLoss.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;
        }

        private static void RealisedDetailTable(C1PrintDocument doc, IGrouping<object, DataRow> account)
        {
            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.Style.FontSize = 7;
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Sec. Code";
            secSummaryTotalTable.Cells[0, 1].Text = "Description";
            secSummaryTotalTable.Cells[0, 2].Text = "Sold Units";
            secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 3].Text = "Avg. Cost Price";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 4].Text = "Cost Value ($)";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 5].Text = "Sale Value ($)";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 6].Text = "Gains / Losses";
            secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 9;
            secSummaryTotalTable.Cols[2].Width = 3;
            secSummaryTotalTable.Cols[3].Width = 3;
            secSummaryTotalTable.Cols[4].Width = 3;
            secSummaryTotalTable.Cols[5].Width = 3;
            secSummaryTotalTable.Cols[6].Width = 3;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Realised Gains & Losses - " + account.Key.ToString().ToUpper() + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            decimal grandTotal = 0;
            decimal grandTotalrealisedValue = 0;
            decimal grandProfitLoss = 0;


            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            foreach (DataRow secRow in account)
            {
                string secCode = secRow[LossAndGainsReportDS.SECNAME].ToString();

                //IV004 && IV014 have special col names
                if (secCode == "IV004" || secCode == "IV014")
                {
                    secSummaryTotalTable.Cells[0, 0].Text = "Sec. Code";
                    secSummaryTotalTable.Cells[0, 1].Text = "Description";
                    secSummaryTotalTable.Cells[0, 2].Text = "Total Units";
                    secSummaryTotalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 3].Text = "Avg. Cost Price";
                    secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 4].Text = "Cost Value ($)";
                    secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 5].Text = "Current Value ($)";
                    secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                    secSummaryTotalTable.Cells[0, 6].Text = "Gains / Losses";
                    secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                }

                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[LossAndGainsReportDS.SECNAME].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[LossAndGainsReportDS.DESCRIPTION].ToString();

                if (secRow[LossAndGainsReportDS.SOLDUNITS] != null & secRow[LossAndGainsReportDS.SOLDUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.SOLDUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 2].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[LossAndGainsReportDS.REALISEDCOSTPRICE] != null & secRow[LossAndGainsReportDS.REALISEDCOSTPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISEDCOSTPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                decimal realisedCostValue = 0;
                if (secRow[LossAndGainsReportDS.REALISEDCOSTVALUE] != null & secRow[LossAndGainsReportDS.REALISEDCOSTVALUE] != DBNull.Value)
                    realisedCostValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISEDCOSTVALUE]);
                else
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 4].Text = realisedCostValue.ToString("C");
                grandTotalrealisedValue += realisedCostValue;
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal saleProceeds = Convert.ToDecimal(secRow[LossAndGainsReportDS.SALESPROCEED]);
                secSummaryTotalTable.Cells[totalIndex, 5].Text = saleProceeds.ToString("C");
                grandTotal += saleProceeds;

                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal realisedProfitLoss = 0;
                if (secRow[LossAndGainsReportDS.REALISEDPROFITLOSS] != null & secRow[LossAndGainsReportDS.REALISEDPROFITLOSS] != DBNull.Value)
                {
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISEDPROFITLOSS]).ToString("C");
                    realisedProfitLoss = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISEDPROFITLOSS]);
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 6].Text = "-";

                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                if (realisedProfitLoss > 0)
                    secSummaryTotalTable.Cells[totalIndex, 6].Style.TextColor = Color.Green;
                else
                    secSummaryTotalTable.Cells[totalIndex, 6].Style.TextColor = Color.Red;

                grandProfitLoss += realisedProfitLoss;

                totalIndex++;
            }

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            secSummaryTotalTable.Cells[totalIndex, 1].Text = "";
            secSummaryTotalTable.Cells[totalIndex, 4].Text = grandTotalrealisedValue.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 5].Text = grandTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

            if (grandProfitLoss > 0)
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextColor = Color.Green;
            else
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextColor = Color.Red;

            secSummaryTotalTable.Cells[totalIndex, 6].Text = grandProfitLoss.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
        }

     
        private static void SummmaryTableOverall(C1PrintDocument doc, DataTable overallSummary)
        {
            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.Style.FontSize = 7;
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Account Type";
            secSummaryTotalTable.Cells[0, 1].Text = "Account Name";
            secSummaryTotalTable.Cells[0, 2].Text = "Account NO";
            secSummaryTotalTable.Cells[0, 3].Text = "Realised Gains / Losses";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 4].Text = "Unrealised Gains / Losses";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 5].Text = "Total";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
      
            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 9;
            secSummaryTotalTable.Cols[2].Width = 3;
            secSummaryTotalTable.Cols[3].Width = 3;
            secSummaryTotalTable.Cols[4].Width = 3;
            secSummaryTotalTable.Cols[5].Width = 3;
      
            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Realised & Unrealised Gains & Losses Summary\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            decimal realisedTotal = 0;
            decimal unRealisedTotal = 0;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            DataView sortedView = new DataView(overallSummary);
            sortedView.Sort = LossAndGainsReportDS.ACCOUNTNO + " ASC";
            sortedView.RowFilter = LossAndGainsReportDS.REALISED + " <> 0 OR " + LossAndGainsReportDS.UNREALISED + " <> 0 ";
            DataTable sortedTable = sortedView.ToTable();

            foreach (DataRow secRow in sortedTable.Rows)
            {
                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[LossAndGainsReportDS.ACCOUNTTYPE].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[LossAndGainsReportDS.ACCOUNTNAME].ToString();
                secSummaryTotalTable.Cells[totalIndex, 2].Text = secRow[LossAndGainsReportDS.ACCOUNTNO].ToString();

                decimal realisedValue = 0;

                if (secRow[LossAndGainsReportDS.REALISED] != null & secRow[LossAndGainsReportDS.REALISED] != DBNull.Value)
                {
                    realisedValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.REALISED]);
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = realisedValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;


                decimal unRealisedValue = 0;

                if (secRow[LossAndGainsReportDS.UNREALISED] != null & secRow[LossAndGainsReportDS.UNREALISED] != DBNull.Value)
                {
                    unRealisedValue = Convert.ToDecimal(secRow[LossAndGainsReportDS.UNREALISED]);
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = unRealisedValue.ToString("C");
                }
                else
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                secSummaryTotalTable.Cells[totalIndex, 5].Text = (unRealisedValue+realisedValue).ToString("C");
                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                realisedTotal += realisedValue;
                unRealisedTotal += unRealisedValue;
                
                totalIndex++;
            }

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            secSummaryTotalTable.Cells[totalIndex, 1].Text = "";
            secSummaryTotalTable.Cells[totalIndex, 3].Text = realisedTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 4].Text = unRealisedTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 5].Text = (unRealisedTotal+realisedTotal).ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

            if (realisedTotal > 0)
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextColor = Color.Green;
            else
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextColor = Color.Red;

            if (unRealisedTotal > 0)
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextColor = Color.Green;
            else
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextColor = Color.Red;

            secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
        }

        public void SetSecuritySummaryGrid(C1PrintDocument doc, IOrderedEnumerable<DataRow> rows, string serviceName)
        {
      
            int index = 1;

            if (rows.Count() > 0)
            {
                RenderTable capitalTable = new RenderTable();
                capitalTable.RowGroups[0, 1].PageHeader = true;
                double OpeningBalance = 0;
                double TransferInOut = 0;
                double Income = 0;
                double AppicationRedemption = 0;
                double TaxInOut = 0;
                double Expense = 0;
                double InternalCashMovement = 0;
                double ClosingBalance = 0;
                double ChangeInInvestmentValue = 0;

                capitalTable.Style.FontSize = 8;

                capitalTable.Rows[0].Height = .2;
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.FontSize = 7;
                capitalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[0, 0].Text = "Month";
                capitalTable.Cells[0, 1].Text = "Opening Balance";
                capitalTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 2].Text = "Transfer In/Out";
                capitalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 3].Text = "Income";
                capitalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 4].Text = "Invesments";
                capitalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 5].Text = "Tax & Expenses";
                capitalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 6].Text = "Internal Cash Mvt";
                capitalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 7].Text = "Closing Balance";
                capitalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 8].Text = "Change in Investment";
                capitalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cols[0].Width = .5;
                capitalTable.Cols[1].Width = 1.2;
                capitalTable.Cols[2].Width = 1.2;
                capitalTable.Cols[3].Width = 1.2;
                capitalTable.Cols[4].Width = 1.2;
                capitalTable.Cols[5].Width = 1.2;
                capitalTable.Cols[6].Width = 1.2;
                capitalTable.Cols[7].Width = 1.2;
                capitalTable.Cols[8].Width = 1.5;

                foreach (DataRow row in rows)
                {

                    capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    capitalTable.Rows[index].Height = .3;
                    capitalTable.Cells[index, 0].Text = ((DateTime)row[CapitalReportDS.MONTH]).ToString("MMM-yy").ToUpper();

                    OpeningBalance += Convert.ToDouble(row[CapitalReportDS.OPENINGBAL]);
                    TransferInOut += Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]);
                    Income += Convert.ToDouble(row[CapitalReportDS.INCOME]);
                    AppicationRedemption += Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION]);
                    TaxInOut += Convert.ToDouble(row[CapitalReportDS.TAXINOUT]);
                    Expense += Convert.ToDouble(row[CapitalReportDS.EXPENSE]);
                    InternalCashMovement += Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]);
                    ClosingBalance += Convert.ToDouble(row[CapitalReportDS.CLOSINGBALANCE]);
                    ChangeInInvestmentValue += Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]);

                    capitalTable.Cells[index, 1].Text = Convert.ToDouble(row[CapitalReportDS.OPENINGBAL]).ToString("C");
                    capitalTable.Cells[index, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 2].Text = Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]).ToString("C");
                    capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 3].Text = Convert.ToDouble(row[CapitalReportDS.INCOME]).ToString("C");
                    capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 4].Text = Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION]).ToString("C");
                    capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 5].Text = Convert.ToDouble(row[CapitalReportDS.EXPENSE]).ToString("C");
                    capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 6].Text = Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]).ToString("C");
                    capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 7].Text = Convert.ToDouble(row[CapitalReportDS.CLOSINGBALANCE]).ToString("C");
                    capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 8].Text = Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]).ToString("C");
                    capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;


                    index++;
                }


                capitalTable.Rows[index].Height = .2;
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[index, 0].Text = "TOTAL";

                capitalTable.Cells[index, 2].Text = TransferInOut.ToString("C");
                capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 3].Text = Income.ToString("C");
                capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 4].Text = AppicationRedemption.ToString("C");
                capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 5].Text = Expense.ToString("C");
                capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 6].Text = InternalCashMovement.ToString("C");
                capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(capitalTable);
                doc.Body.Children.Add(lineBreak);
            }

        }

        protected void GenerateReport(object sender, EventArgs e)
        {
          
        }

        protected void ReportClick()
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"].ToString()));
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            this.assets = organization.Assets;
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);
            LossAndGainsReportDS ds = new LossAndGainsReportDS();

            if (comboBoxCostType.SelectedItem != null)
            {

                if (comboBoxCostType.SelectedItem.Text == "LIFO")
                    ds.CostTypes = CostTypes.LIFO;
                if (comboBoxCostType.SelectedItem.Text == "AVG")
                    ds.CostTypes = CostTypes.AVG;
            }
            if (this.InputStartDate.DateInput.SelectedDate.HasValue)
                ds.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(this.InputStartDate.DateInput.SelectedDate.Value);

            if (this.InputEndDate.DateInput.SelectedDate.HasValue)
                ds.EndDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(this.InputEndDate.DateInput.SelectedDate.Value);

            clientData.GetData(ds);
            this.PresentationData = ds;

            DataRow clientSummaryRow = ds.Tables[LossAndGainsReportDS.CLIENTSUMMARYTABLE].Rows[0];

            string reportName = "Report_" + Guid.NewGuid().ToString();
            C1ReportViewer.RegisterDocument(reportName, MakeDoc);
            this.C1ReportViewerAssetClass.FileName = reportName;
            C1ReportViewerAssetClass.ReportName = reportName;
            InputEndDate.DbSelectedDate = ds.EndDate;
            InputStartDate.DbSelectedDate = ds.StartDate; 
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }
    }
}
