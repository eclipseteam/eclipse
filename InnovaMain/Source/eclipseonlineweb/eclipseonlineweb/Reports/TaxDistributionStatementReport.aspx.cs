﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using System.Collections.ObjectModel;
using System.Globalization;

namespace eclipseonlineweb
{
    public partial class TaxDistributionStatementReport : UMABasePage
    {

        string fundCode = string.Empty;
        DateTime startDate = DateTime.Now;
        DateTime endDate = DateTime.Now;

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public override void LoadPage()
        {
            this.cid = Request.QueryString["ins"].ToString();
            fundCode = Request.QueryString["FundCode"].ToString();
            startDate = DateTime.ParseExact(Request.QueryString["StartDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            endDate = DateTime.ParseExact(Request.QueryString["EndDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DIS_Click();
            
            ScriptManager sm = ScriptManager.GetCurrent(Page);
        }

        protected void DIS_Click()
        {
            this.cid = Request.QueryString["ins"].ToString();
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);
            ClientTaxDistributionsDS disDS = new ClientTaxDistributionsDS();
            clientData.GetData(disDS);
            this.PresentationData = disDS;
            
            if (disDS.Tables.Count > 0)
            {
                string reportName = "Report_" + Guid.NewGuid().ToString();
                C1ReportViewer.RegisterDocument(reportName, MakeDoc);
                this.C1ReportViewerDisStatement.FileName = reportName;
                C1ReportViewerDisStatement.ReportName = reportName;
            }

            this.UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        public C1PrintDocument MakeDoc()
        {
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            DataRow clientSummaryRow = this.PresentationData.Tables[BaseClientDetails.CLIENTSUMMARYTABLE].Rows[0];

            var taxRows = this.PresentationData.Tables[ClientTaxDistributionsDS.DISTAXTABLE].Select().Where(row => row[ClientTaxDistributionsDS.FUNDCODE].ToString() == this.fundCode && (DateTime)row[ClientTaxDistributionsDS.STARTDATE] == this.startDate && (DateTime)row[ClientTaxDistributionsDS.ENDDATE] == this.endDate);

            SetTaxDistribution(doc, clientSummaryRow, taxRows);
            return doc;
        }

        private void SetTaxDistribution(C1PrintDocument doc, DataRow clientSummaryRow, IEnumerable<DataRow> taxRows)
        {
            ObservableCollection<DistributionIncomeEntity> DistributionIncomeEntityList = ((ClientTaxDistributionsDS)this.PresentationData).DistributionIncomeEntityList;
            AddClientHeaderToReport(doc, clientSummaryRow, "", "Tax Distribution Statement");//NB. Can't get DateInfo like bellow

            var slectedDistributionStatement = DistributionIncomeEntityList.Where(dis => dis.FundCode == this.fundCode && dis.RecordDate >= this.startDate && dis.RecordDate <= this.endDate);

            RenderTable divStmtTaxTable = new RenderTable();
            divStmtTaxTable.RowGroups[0, 1].PageHeader = true;
            divStmtTaxTable.Style.FontSize = 10;
            divStmtTaxTable.Rows[0].SplitBehavior = SplitBehaviorEnum.SplitNewPage;
            divStmtTaxTable.Rows[0].Height = .3;
            divStmtTaxTable.Rows[0].Style.FontBold = true;
            divStmtTaxTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            divStmtTaxTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            divStmtTaxTable.Rows[0].Style.FontBold = true;
            divStmtTaxTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            divStmtTaxTable.Cols[0].Width = 4;
            divStmtTaxTable.Cols[1].Width = 4;
            divStmtTaxTable.Cols[2].Width = 2;

            divStmtTaxTable.Cells[0, 0].Text = "Tax Return (Supplementary Section)";
            divStmtTaxTable.Cells[0, 1].Text = "Amount";
            divStmtTaxTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
            divStmtTaxTable.Cells[0, 2].Text = "Tax Return label";
            divStmtTaxTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Center;

            int count = 1;

            foreach (DataRow row in taxRows)
            {
                divStmtTaxTable.Cells[count, 0].Text = row[ClientTaxDistributionsDS.TAXLABELDESC].ToString();
                divStmtTaxTable.Cells[count, 1].Text = ((double)row[ClientTaxDistributionsDS.AMOUNT]).ToString("C");
                divStmtTaxTable.Cells[count, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                divStmtTaxTable.Cells[count, 2].Text = row[ClientTaxDistributionsDS.TAXLABEL].ToString();
                divStmtTaxTable.Cells[count, 2].Style.TextAlignHorz = AlignHorzEnum.Center;

                count++;
            }

            RenderText gridHeader = new RenderText();
            gridHeader.Text = "SUMMARY OF TAX INFORMATION FOR " + endDate.Year.ToString()+ " TAX RETURN\n";
            gridHeader.Style.TextColor = Color.Black;
            gridHeader.Style.FontSize = 12;
            gridHeader.Style.FontBold = true;
            gridHeader.Style.TextAlignHorz = AlignHorzEnum.Center;

            RenderText gridHeader2 = new RenderText();
            gridHeader2.Text = "(Supplementary Section) Items\n\n";
            gridHeader2.Style.TextColor = Color.Black;
            gridHeader2.Style.FontSize = 10;
            gridHeader2.Style.FontBold = true;
            gridHeader2.Style.TextAlignHorz = AlignHorzEnum.Center;

            RenderText gridHeader3 = new RenderText();
            gridHeader3.Text = "This statement specifies how the responsible entity has, for tax purposes, classified the various components of its distributions to investors.  Investors should consult with their professional advisors on all tax related issues. This statement should not be read as giving taxation advice.";
            gridHeader3.Style.TextColor = Color.Black;
            gridHeader3.Style.FontSize = 8;
            gridHeader3.Style.FontBold = true;
            gridHeader3.Style.TextAlignHorz = AlignHorzEnum.Center;

            RenderText gridHeader4 = new RenderText();
            gridHeader4.Text = "The above summary shows the taxable components of the distributions received from your investments during the financial year.  Details of the distributions are attached.\n";
            gridHeader4.Style.TextColor = Color.Black;
            gridHeader4.Style.FontSize = 8;
            gridHeader4.Style.FontBold = true;
            gridHeader4.Style.TextAlignHorz = AlignHorzEnum.Center;

            RenderText lineBreakTotal4 = new RenderText();
            lineBreakTotal4.Text = "-----------------------------------------------------------------------------------------------";
            lineBreakTotal4.Style.TextAlignHorz = AlignHorzEnum.Center;

            RenderText gridHeader5 = new RenderText();
            gridHeader5.Text = "The above tax return label references are an excerpt from the TaxPack "+this.endDate.Year.ToString()+" Supplementary published by the Australian Taxation Office and is copyright Commonwealth of Australia, reproduced by permission.\n\n\n\n\n\n";
            gridHeader5.Style.TextColor = Color.Black;
            gridHeader5.Style.FontSize = 6;
            gridHeader5.Style.FontBold = true;
            gridHeader5.Style.TextAlignHorz = AlignHorzEnum.Center;

            RenderText lineBreakTotal3 = new RenderText();
            lineBreakTotal3.Text = "\n";
            doc.Body.Children.Add(gridHeader);
            doc.Body.Children.Add(gridHeader2);
            doc.Body.Children.Add(divStmtTaxTable);
            doc.Body.Children.Add(lineBreakTotal3);
            doc.Body.Children.Add(gridHeader3);
            doc.Body.Children.Add(lineBreakTotal4);
            doc.Body.Children.Add(gridHeader4);
            doc.Body.Children.Add(gridHeader5);

            if (slectedDistributionStatement != null && slectedDistributionStatement.Count() > 0)
            {

                RenderTable divStmtTable = new RenderTable();
                divStmtTable.RowGroups[0, 1].PageHeader = true;
                divStmtTable.Style.FontSize = 10;
                divStmtTable.Rows[0].SplitBehavior = SplitBehaviorEnum.SplitNewPage;
                divStmtTable.Rows[0].Height = .3;
                divStmtTable.Rows[0].Style.FontBold = true;
                divStmtTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                divStmtTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                divStmtTable.Rows[0].Style.FontBold = true;
                divStmtTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                divStmtTable.Cells[0, 0].Text = "Distribution Summary Details";
                divStmtTable.Cells[0, 1].Text = "";

                divStmtTable.Cells[1, 0].Text = "Fund Code";
                divStmtTable.Cells[1, 0].Style.FontBold = true;
                divStmtTable.Cells[1, 1].Text = slectedDistributionStatement.FirstOrDefault().FundCode;
                divStmtTable.Cells[1, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                divStmtTable.Rows[1].Height = .3;
                divStmtTable.Rows[1].Style.TextAlignVert = AlignVertEnum.Center;

                divStmtTable.Cells[2, 0].Text = "Fund Name";
                divStmtTable.Cells[2, 0].Style.FontBold = true;
                divStmtTable.Cells[2, 1].Text = slectedDistributionStatement.FirstOrDefault().FundName;
                divStmtTable.Cells[2, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                divStmtTable.Rows[2].Height = .3;
                divStmtTable.Rows[2].Style.TextAlignVert = AlignVertEnum.Center;

                divStmtTable.Cells[3, 0].Text = "Financial Year";
                divStmtTable.Cells[3, 0].Style.FontBold = true;
                divStmtTable.Cells[3, 1].Text = this.startDate.ToString("dd/MM/yyyy") + " - " + this.endDate.ToString("dd/MM/yyyy");
                divStmtTable.Cells[3, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                divStmtTable.Rows[3].Height = .3;
                divStmtTable.Rows[3].Style.TextAlignVert = AlignVertEnum.Center;

                divStmtTable.Cells[5, 0].Text = "Shares";
                divStmtTable.Cells[5, 0].Style.FontBold = true;
                divStmtTable.Cells[5, 1].Text = slectedDistributionStatement.Sum(dis => dis.RecodDate_Shares).ToString("N2");
                divStmtTable.Cells[5, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                divStmtTable.Rows[5].Height = .3;
                divStmtTable.Rows[5].Style.TextAlignVert = AlignVertEnum.Center;

                divStmtTable.Cells[6, 0].Text = "Gross DPU";
                divStmtTable.Cells[6, 0].Style.FontBold = true;
                divStmtTable.Cells[6, 1].Text = slectedDistributionStatement.Sum(dis => dis.GrossDistributionDPU).ToString("N4");
                divStmtTable.Cells[6, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                divStmtTable.Rows[6].Height = .3;
                divStmtTable.Rows[6].Style.TextAlignVert = AlignVertEnum.Center;

                divStmtTable.Cells[7, 0].Text = "Net DPU";
                divStmtTable.Cells[7, 0].Style.FontBold = true;
                divStmtTable.Cells[7, 1].Text = slectedDistributionStatement.Sum(dis => dis.NetDistributionDPU).ToString("N4");
                divStmtTable.Cells[7, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                divStmtTable.Rows[7].Height = .3;
                divStmtTable.Rows[7].Style.TextAlignVert = AlignVertEnum.Center;

                divStmtTable.Cells[8, 0].Text = "Net Cash Distribution";
                divStmtTable.Cells[8, 0].Style.FontBold = true;
                divStmtTable.Cells[8, 1].Text = slectedDistributionStatement.Sum(dis => dis.NetCashDistribution.Value).ToString("C");
                divStmtTable.Cells[8, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                divStmtTable.Rows[8].Height = .3;
                divStmtTable.Rows[8].Style.TextAlignVert = AlignVertEnum.Center;

                divStmtTable.Style.FontSize = 10;
                divStmtTable.Rows[9].SplitBehavior = SplitBehaviorEnum.SplitNewPage;
                divStmtTable.Rows[9].Height = .3;
                divStmtTable.Rows[9].Style.FontBold = true;
                divStmtTable.Rows[9].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                divStmtTable.Rows[9].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                divStmtTable.Rows[9].Style.FontBold = true;
                divStmtTable.Rows[9].Style.TextAlignVert = AlignVertEnum.Center;

                RenderText lineBreakTotal2 = new RenderText();
                lineBreakTotal2.Text = "\n";
                doc.Body.Children.Add(divStmtTable);
                doc.Body.Children.Add(lineBreakTotal2);



                var ausIncomeComponents = from component in slectedDistributionStatement
                                          select new
                                          {
                                              DistributionComponents = component.Components.Where(comp => comp.Category == ComponentCategory.Australian_Income),
                                          };

                ObservableCollection<DistributionComponent> ausComponents = new ObservableCollection<DistributionComponent>();

                foreach (var comp in ausIncomeComponents)
                {
                    foreach (DistributionComponent disComp in comp.DistributionComponents)
                    {
                        ausComponents.Add(disComp);
                    }
                }

                SetComponentTable("Australian Income", doc, slectedDistributionStatement.FirstOrDefault(), ausComponents.OrderBy(dis => dis.Decription));


                var foreignIncomeComponents = from component in slectedDistributionStatement
                                              select new
                                              {
                                                  DistributionComponents = component.Components.Where(comp => comp.Category == ComponentCategory.Foreign_Income),
                                              };

                ObservableCollection<DistributionComponent> foreignComponents = new ObservableCollection<DistributionComponent>();

                foreach (var comp in foreignIncomeComponents)
                {
                    foreach (DistributionComponent disComp in comp.DistributionComponents)
                    {
                        foreignComponents.Add(disComp);
                    }
                }


                SetComponentTable("Foreign Income", doc, slectedDistributionStatement.FirstOrDefault(), foreignComponents.OrderBy(dis => dis.Decription));

                var capitalGains = from component in slectedDistributionStatement
                                   select new
                                   {
                                       DistributionComponents = component.Components.Where(comp => comp.Category == ComponentCategory.Capital_Gains),
                                   };

                ObservableCollection<DistributionComponent> capitalGainsComponents = new ObservableCollection<DistributionComponent>();

                foreach (var comp in capitalGains)
                {
                    foreach (DistributionComponent disComp in comp.DistributionComponents)
                    {
                        capitalGainsComponents.Add(disComp);
                    }
                }

                SetComponentTable("Capital Gains", doc, slectedDistributionStatement.FirstOrDefault(), capitalGainsComponents.OrderBy(dis => dis.Decription));

                var nonassComponents = from component in slectedDistributionStatement
                                       select new
                                       {
                                           DistributionComponents = component.Components.Where(comp => comp.Category == ComponentCategory.Other_Non_Assessable_Amount),
                                       };

                ObservableCollection<DistributionComponent> nonassComponentssComponents = new ObservableCollection<DistributionComponent>();

                foreach (var comp in nonassComponents)
                {
                    foreach (DistributionComponent disComp in comp.DistributionComponents)
                    {
                        nonassComponentssComponents.Add(disComp);
                    }
                }


                SetComponentTable("Other Non-Assessable Amount", doc, slectedDistributionStatement.FirstOrDefault(), nonassComponentssComponents.OrderBy(dis => dis.Decription));
            }
        }

        private static void SetComponentTable(string category, C1PrintDocument doc, DistributionIncomeEntity slectedDistributionStatement, IEnumerable<DistributionComponent> distributionComponentList)
        {
            RenderTable componentTable = new RenderTable();
            componentTable.Style.FontSize = 8;
            componentTable.RowGroups[0, 1].PageHeader = true;
            componentTable.Rows[0].Height = .2;
            componentTable.Rows[0].Style.FontBold = true;
            componentTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            componentTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            componentTable.Rows[0].Style.FontBold = true;
            componentTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            componentTable.Cells[0, 0].Text = "Category";
            componentTable.Cells[0, 1].Text = "Distribution Paid";
            componentTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
            componentTable.Cells[0, 2].Text = "Tax Paid / Offset";
            componentTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
            componentTable.Cells[0, 3].Text = "Withholding / Non-Resident Tax Paid";
            componentTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            componentTable.Cells[0, 4].Text = "Gross Distribution Amount";
            componentTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            componentTable.Rows[0].Style.FontSize = 7;
            componentTable.Cols[0].Width = 7;
            componentTable.Cols[1].Width = 5;
            componentTable.Cols[2].Width = 5;
            componentTable.Cols[3].Width = 5;
            componentTable.Cols[4].Width = 4;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = category + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n";
            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(componentTable);
            doc.Body.Children.Add(lineBreakTotal);

            int totalIndex = 1;

            foreach (DistributionComponent disComp in distributionComponentList)
            {
                componentTable.Cells[totalIndex, 0].Text = disComp.Decription;
                if(disComp.Unit_Amount.HasValue)
                    componentTable.Cells[totalIndex, 1].Text = disComp.Unit_Amount.Value.ToString("C");
                else
                    componentTable.Cells[totalIndex, 1].Text = "-";

                componentTable.Cells[totalIndex, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (disComp.Autax_Credit.HasValue)
                    componentTable.Cells[totalIndex, 2].Text = disComp.Autax_Credit.Value.ToString("C");
                else
                    componentTable.Cells[totalIndex, 1].Text = "-";

                componentTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                
                if (disComp.Tax_WithHeld.HasValue)
                 componentTable.Cells[totalIndex, 3].Text = disComp.Tax_WithHeld.Value.ToString("C");
                else
                   componentTable.Cells[totalIndex, 1].Text = "-";
                componentTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                componentTable.Cells[totalIndex, 4].Text = disComp.GrossDistribution.ToString("C");
                componentTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                totalIndex++;
            }

            componentTable.Rows[totalIndex].Height = .2;
            componentTable.Rows[totalIndex].Style.FontBold = true;
            componentTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            componentTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            componentTable.Rows[totalIndex].Style.FontBold = true;
            componentTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;
            componentTable.Cells[totalIndex, 0].Text = "TOTAL";

            componentTable.Cells[totalIndex, 1].Text = distributionComponentList.Sum(dis => dis.Unit_Amount).Value.ToString("C");
            componentTable.Cells[totalIndex, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            componentTable.Cells[totalIndex, 2].Text = distributionComponentList.Sum(dis => dis.Autax_Credit).Value.ToString("C");
            componentTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

            componentTable.Cells[totalIndex, 3].Text = distributionComponentList.Sum(dis => dis.Tax_WithHeld).Value.ToString("C");
            componentTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

            componentTable.Cells[totalIndex, 4].Text = distributionComponentList.Sum(dis => dis.GrossDistribution).ToString("C");
            componentTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
        }
    }
}
