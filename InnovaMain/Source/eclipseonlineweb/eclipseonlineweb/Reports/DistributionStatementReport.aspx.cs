﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using System.Collections.ObjectModel;

namespace eclipseonlineweb
{
    public partial class DistributionStatementReport : UMABasePage
    {
      
        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public override void LoadPage()
        {
            this.cid = Request.QueryString["ins"].ToString();
            lblDisID.Text = Request.QueryString["DisID"].ToString();
            DIS_Click();
            
            ScriptManager sm = ScriptManager.GetCurrent(Page);
        }

        protected void DIS_Click()
        {
            this.cid = Request.QueryString["ins"].ToString();
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);
            ClientDistributionsDS disDS = new ClientDistributionsDS();
            clientData.GetData(disDS);
            this.PresentationData = disDS;
            
            if (disDS.Tables.Count > 0)
            {
                DataView divView = new DataView(disDS.Tables[ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE]);
                divView.Sort = "RecordDate DESC";

                string reportName = "Report_" + Guid.NewGuid().ToString();
                C1ReportViewer.RegisterDocument(reportName, MakeDoc);
                this.C1ReportViewerDisStatement.FileName = reportName;
                C1ReportViewerDisStatement.ReportName = reportName;

            }

            this.UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        public C1PrintDocument MakeDoc()
        {
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            DataRow clientSummaryRow = this.PresentationData.Tables[BaseClientDetails.CLIENTSUMMARYTABLE].Rows[0];
            ObservableCollection<DistributionIncomeEntity> DistributionIncomeEntityList = ((ClientDistributionsDS)this.PresentationData).DistributionIncomeEntityList; 
            AddClientHeaderToReport(doc, clientSummaryRow, "", "Distribution Statement");//NB. Can't get DateInfo like bellow
            
            var slectedDistributionStatement = DistributionIncomeEntityList.Where(dis => dis.ID == new Guid(this.lblDisID.Text)).FirstOrDefault();
           
            if (slectedDistributionStatement != null)
            {
                RenderTable divStmtTable = new RenderTable();
                divStmtTable.RowGroups[0, 1].PageHeader = true;
                divStmtTable.Style.FontSize = 10;
                divStmtTable.Rows[0].SplitBehavior = SplitBehaviorEnum.SplitNewPage;
                divStmtTable.Rows[0].Height = .3;
                divStmtTable.Rows[0].Style.FontBold = true;
                divStmtTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                divStmtTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                divStmtTable.Rows[0].Style.FontBold = true;
                divStmtTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                divStmtTable.Cells[0, 0].Text = "Summary Details";
                divStmtTable.Cells[0, 1].Text = "";

                divStmtTable.Cells[1, 0].Text = "Fund Code";
                divStmtTable.Cells[1, 0].Style.FontBold = true;
                divStmtTable.Cells[1, 1].Text = slectedDistributionStatement.FundCode;
                divStmtTable.Cells[1, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                divStmtTable.Rows[1].Height = .3;
                divStmtTable.Rows[1].Style.TextAlignVert = AlignVertEnum.Center;

                divStmtTable.Cells[2, 0].Text = "Fund Name";
                divStmtTable.Cells[2, 0].Style.FontBold = true;
                divStmtTable.Cells[2, 1].Text = slectedDistributionStatement.FundName;
                divStmtTable.Cells[2, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                divStmtTable.Rows[2].Height = .3;
                divStmtTable.Rows[2].Style.TextAlignVert = AlignVertEnum.Center;

                divStmtTable.Cells[3, 0].Text = "Record Date";
                divStmtTable.Cells[3, 0].Style.FontBold = true;
                divStmtTable.Cells[3, 1].Text = slectedDistributionStatement.RecordDate.Value.ToString("dd/MM/yyyy");
                divStmtTable.Cells[3, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                divStmtTable.Rows[3].Height = .3;
                divStmtTable.Rows[3].Style.TextAlignVert = AlignVertEnum.Center;

                divStmtTable.Cells[4, 0].Text = "Payment Date";
                divStmtTable.Cells[4, 0].Style.FontBold = true;
                divStmtTable.Cells[4, 1].Text = slectedDistributionStatement.PaymentDate.Value.ToString("dd/MM/yyyy");
                divStmtTable.Cells[4, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                divStmtTable.Rows[4].Height = .3;
                divStmtTable.Rows[4].Style.TextAlignVert = AlignVertEnum.Center;

                divStmtTable.Cells[5, 0].Text = "Shares";
                divStmtTable.Cells[5, 0].Style.FontBold = true;
                divStmtTable.Cells[5, 1].Text = slectedDistributionStatement.RecodDate_Shares.ToString("N4");
                divStmtTable.Cells[5, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                divStmtTable.Rows[5].Height = .3;
                divStmtTable.Rows[5].Style.TextAlignVert = AlignVertEnum.Center;

                divStmtTable.Cells[6, 0].Text = "Gross DPU";
                divStmtTable.Cells[6, 0].Style.FontBold = true;
                divStmtTable.Cells[6, 1].Text = slectedDistributionStatement.GrossDistributionDPU.ToString("N6");
                divStmtTable.Cells[6, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                divStmtTable.Rows[6].Height = .3;
                divStmtTable.Rows[6].Style.TextAlignVert = AlignVertEnum.Center;

                divStmtTable.Cells[7, 0].Text = "Net DPU";
                divStmtTable.Cells[7, 0].Style.FontBold = true;
                divStmtTable.Cells[7, 1].Text = slectedDistributionStatement.NetDistributionDPU.ToString("N6");
                divStmtTable.Cells[7, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                divStmtTable.Rows[7].Height = .3;
                divStmtTable.Rows[7].Style.TextAlignVert = AlignVertEnum.Center;

                divStmtTable.Cells[8, 0].Text = "Net Cash Distribution";
                divStmtTable.Cells[8, 0].Style.FontBold = true;
                divStmtTable.Cells[8, 1].Text = slectedDistributionStatement.NetCashDistribution.Value.ToString("C");
                divStmtTable.Cells[8, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                divStmtTable.Rows[8].Height = .3;
                divStmtTable.Rows[8].Style.TextAlignVert = AlignVertEnum.Center;

                divStmtTable.Style.FontSize = 10;
                divStmtTable.Rows[9].SplitBehavior = SplitBehaviorEnum.SplitNewPage;
                divStmtTable.Rows[9].Height = .3;
                divStmtTable.Rows[9].Style.FontBold = true;
                divStmtTable.Rows[9].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                divStmtTable.Rows[9].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                divStmtTable.Rows[9].Style.FontBold = true;
                divStmtTable.Rows[9].Style.TextAlignVert = AlignVertEnum.Center;

                RenderText lineBreakTotal2 = new RenderText();
                lineBreakTotal2.Text = "\n";
                doc.Body.Children.Add(divStmtTable);
                doc.Body.Children.Add(lineBreakTotal2);

                var ausIncomeComponents = slectedDistributionStatement.Components.Where(comp => comp.Category == ComponentCategory.Australian_Income);
                ausIncomeComponents.OrderBy(comp => comp.Decription);
                SetComponentTable("Australian Income (" + slectedDistributionStatement.FundCode + ")", doc, slectedDistributionStatement, ausIncomeComponents);

                var foreignIncomeComponents = slectedDistributionStatement.Components.Where(comp => comp.Category == ComponentCategory.Foreign_Income);
                foreignIncomeComponents.OrderBy(comp => comp.Decription);
                SetComponentTable("Foreign Income (" + slectedDistributionStatement.FundCode + ")", doc, slectedDistributionStatement, foreignIncomeComponents);

                var capitalGains = slectedDistributionStatement.Components.Where(comp => comp.Category == ComponentCategory.Capital_Gains);
                foreignIncomeComponents.OrderBy(comp => comp.Decription);
                SetComponentTable("Capital Gains (" + slectedDistributionStatement.FundCode + ")", doc, slectedDistributionStatement, capitalGains);

                var nonassComponents = slectedDistributionStatement.Components.Where(comp => comp.Category == ComponentCategory.Other_Non_Assessable_Amount);
                nonassComponents.OrderBy(comp => comp.Decription);
                SetComponentTable("Other Non-Assessable Amount (" + slectedDistributionStatement.FundCode + ")", doc, slectedDistributionStatement, nonassComponents);

                var totals = slectedDistributionStatement.Components;
                nonassComponents.OrderBy(comp => comp.Decription);
                SetComponentTableTotal("Total (" + slectedDistributionStatement.FundCode + ")", doc, slectedDistributionStatement, totals);
            }

            return doc;
        }

        private static void SetComponentTableTotal(string category, C1PrintDocument doc, DistributionIncomeEntity slectedDistributionStatement, IEnumerable<DistributionComponent> distributionComponentList)
        {
            RenderTable componentTable = new RenderTable();
            componentTable.Style.FontSize = 8;
            componentTable.RowGroups[0, 1].PageHeader = true;
            componentTable.Rows[0].Height = .2;
            componentTable.Rows[0].Style.FontBold = true;
            componentTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            componentTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            componentTable.Rows[0].Style.FontBold = true;
            componentTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            componentTable.Cells[0, 0].Text = "Category";
            componentTable.Cells[0, 1].Text = "Distribution Paid";
            componentTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
            componentTable.Cells[0, 2].Text = "Tax Paid / Offset";
            componentTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
            componentTable.Cells[0, 3].Text = "Withholding / Non-Resident Tax Paid";
            componentTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            componentTable.Cells[0, 4].Text = "Gross Distribution Amount";
            componentTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            componentTable.Rows[0].Style.FontSize = 7;
            componentTable.Cols[0].Width = 7.5;
            componentTable.Cols[1].Width = 5;
            componentTable.Cols[2].Width = 5;
            componentTable.Cols[3].Width = 5;
            componentTable.Cols[4].Width = 4;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = category + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n";
            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(componentTable);
            doc.Body.Children.Add(lineBreakTotal);

            int totalIndex = 1;

            componentTable.Rows[totalIndex].Height = .2;
            componentTable.Rows[totalIndex].Style.FontBold = true;
            componentTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            componentTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            componentTable.Rows[totalIndex].Style.FontBold = true;
            componentTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;
            componentTable.Cells[totalIndex, 0].Text = "TOTAL";

            componentTable.Cells[totalIndex, 1].Text = distributionComponentList.Sum(dis => dis.Unit_Amount).Value.ToString("C");
            componentTable.Cells[totalIndex, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            componentTable.Cells[totalIndex, 2].Text = distributionComponentList.Sum(dis => dis.Autax_Credit).Value.ToString("C");
            componentTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

            componentTable.Cells[totalIndex, 3].Text = distributionComponentList.Sum(dis => dis.Tax_WithHeld).Value.ToString("C");
            componentTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

            componentTable.Cells[totalIndex, 4].Text = distributionComponentList.Sum(dis => dis.GrossDistribution).ToString("C");
            componentTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
        }

        private static void SetComponentTable(string category, C1PrintDocument doc, DistributionIncomeEntity slectedDistributionStatement, IEnumerable<DistributionComponent> distributionComponentList)
        {
            RenderTable componentTable = new RenderTable();
            componentTable.Style.FontSize = 8;
            componentTable.RowGroups[0, 1].PageHeader = true;
            componentTable.Rows[0].Height = .2;
            componentTable.Rows[0].Style.FontBold = true;
            componentTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            componentTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            componentTable.Rows[0].Style.FontBold = true;
            componentTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            componentTable.Cells[0, 0].Text = "Category";
            componentTable.Cells[0, 1].Text = "Distribution Paid";
            componentTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
            componentTable.Cells[0, 2].Text = "Tax Paid / Offset";
            componentTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
            componentTable.Cells[0, 3].Text = "Withholding / Non-Resident Tax Paid";
            componentTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            componentTable.Cells[0, 4].Text = "Gross Distribution Amount";
            componentTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            componentTable.Rows[0].Style.FontSize = 7;
            componentTable.Cols[0].Width = 7;
            componentTable.Cols[1].Width = 5;
            componentTable.Cols[2].Width = 5;
            componentTable.Cols[3].Width = 5;
            componentTable.Cols[4].Width = 4;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = category + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n";
            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(componentTable);
            doc.Body.Children.Add(lineBreakTotal);

            int totalIndex = 1;

            foreach (DistributionComponent disComp in distributionComponentList)
            {
                componentTable.Cells[totalIndex, 0].Text = disComp.Decription;
                if(disComp.Unit_Amount.HasValue)
                    componentTable.Cells[totalIndex, 1].Text = disComp.Unit_Amount.Value.ToString("C");
                else
                    componentTable.Cells[totalIndex, 1].Text = "-";

                componentTable.Cells[totalIndex, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (disComp.Autax_Credit.HasValue)
                    componentTable.Cells[totalIndex, 2].Text = disComp.Autax_Credit.Value.ToString("C");
                else
                    componentTable.Cells[totalIndex, 2].Text = "-";

                componentTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                
                if (disComp.Tax_WithHeld.HasValue)
                 componentTable.Cells[totalIndex, 3].Text = disComp.Tax_WithHeld.Value.ToString("C");
                else
                   componentTable.Cells[totalIndex, 3].Text = "-";
                componentTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                componentTable.Cells[totalIndex, 4].Text = disComp.GrossDistribution.ToString("C");
                componentTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                totalIndex++;
            }

            componentTable.Rows[totalIndex].Height = .2;
            componentTable.Rows[totalIndex].Style.FontBold = true;
            componentTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            componentTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            componentTable.Rows[totalIndex].Style.FontBold = true;
            componentTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;
            componentTable.Cells[totalIndex, 0].Text = "TOTAL";

            componentTable.Cells[totalIndex, 1].Text = distributionComponentList.Sum(dis => dis.Unit_Amount).Value.ToString("C");
            componentTable.Cells[totalIndex, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            componentTable.Cells[totalIndex, 2].Text = distributionComponentList.Sum(dis => dis.Autax_Credit).Value.ToString("C");
            componentTable.Cells[totalIndex, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

            componentTable.Cells[totalIndex, 3].Text = distributionComponentList.Sum(dis => dis.Tax_WithHeld).Value.ToString("C");
            componentTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

            componentTable.Cells[totalIndex, 4].Text = distributionComponentList.Sum(dis => dis.GrossDistribution).ToString("C");
            componentTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
        }
    }
}
