﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Utilities;
using Telerik.Web.UI;

namespace eclipseonlineweb
{
    public partial class ClientPerformanceReportDetails : UMABasePage
    {
        public override void LoadPage()
        {
            this.cid = Request.QueryString["ins"].ToString();

            if (!this.IsPostBack)
                this.PopulatePage(this.PresentationData);

            ScriptManager sm = ScriptManager.GetCurrent(Page);
        }


        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);

        }

        public override void PopulatePage(DataSet ds)
        {
            base.PopulatePage(ds);
            ClientPerformanceReportClick();
        }

        public static void AddFooter(C1PrintDocument doc)
        {
            doc.PageLayouts.PrintFooterOnLastPage = true;
            RenderTable footer = new RenderTable();
            footer.BreakBefore = BreakEnum.Page;
            footer.Rows[0].Height = .30;
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;
            footer.Rows[1].Height = 2.5;
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.TextAlignVert = AlignVertEnum.Center;

            footer.Cells[0, 0].Text = "FOR YOUR INFORMATION";
            footer.Cells[0, 0].Style.TextAlignHorz = AlignHorzEnum.Center;
            footer.Cells[0, 0].Style.FontSize = 12;

            footer.Cells[1, 0].Text = "\n1. The “Movement” column includes all cash movement in and out of your portfolio.  The capital movement summary report on the portal will provide a breakdown of this amount between income, expenses, transfer in and out’s etc." +
                                        "\n\n2. Portfolio performance is calculated using the Modified Dietz Method and are net of fees and charges shown in the Capital Flows table.  Under this method of calculation individual cash flows over the period are weighted according to when they occur to provide an approximation of the actual return.  When returns are volatile or cash flows are large in relation to the size of the portfolio, the accuracy of this approximation will be reduced.\n\nIncome transactions such as interest, dividends, distributions and rent are not included in the cash flows as they are included in the change of investment value to determine the return on your portfolio.\n\nPast performance is not a reliable indicator of future performance." +
                                       "\n\n3. Returns shown in this reports are not annualised. The performance numbers are therefore the return for the period the report has been run." +
                                        "\n\n4. This report is provided by e-Clipse Online Pty Limited ABN 70 145 358 630, AFSL 357 306 (“e-Clipse”) and is based on information provided to e-Clipse by third parties.  Whilst every reasonable effort has been made by e-Clipse to ensure its accuracy, neither e-Clipse nor any of its related entities guarantee its accuracy nor accept any liability for any errors or omissions." +
                                        "\n\ne-Clipse Online Pty Ltd (ABN 70 145 358 630)\n3/36 Bydown Street, Neutral Bay, NSW 2089\nhttp://www.e-clipse.com.au\nP: +61 2 9346 4686";
            footer.Cells[1, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            footer.Cells[1, 0].Style.TextAlignVert = AlignVertEnum.Top;
            footer.Cells[1, 0].Style.FontSize = 9;
            footer.Cells[1, 0].Style.FontBold = false;
            doc.PageLayouts.LastPage = new PageLayout();
            doc.PageLayouts.LastPage.Document.Body.Children.Add(footer);
        }

        public C1PrintDocument MakeDocPerformance()
        {
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            DataRow clientSummaryRow = this.PresentationData.Tables[PerfReportDS.CLIENTSUMMARYTABLE].Rows[0];

            ((PerfReportDS)this.PresentationData).StartDate = WebUtilities.Utilities.FirstDayOfMonthFromDateTime(((PerfReportDS)this.PresentationData).StartDate);
            ((PerfReportDS)this.PresentationData).EndDate = WebUtilities.Utilities.LastDayOfMonthFromDateTime(((PerfReportDS)this.PresentationData).EndDate);

            AddClientHeaderToReport(doc, clientSummaryRow, ((PerfReportDS)this.PresentationData).DateInfo(), "PORTFOLIO PERFORMANCE DETAILED");

            DataTable capitalSummaryTable = this.PresentationData.Tables[PerfReportDS.CAPITALFLOWSUMMARY];
            DataTable performSummaryTable = this.PresentationData.Tables[PerfReportDS.PERFSUMMARYTABLE];

            DataTable capitalSummaryCatTable = this.PresentationData.Tables[PerfReportDS.CAPITALFLOWSUMMARYCAT];
            var capitalSummaryCatRows = capitalSummaryCatTable.Select().OrderByDescending(rows => rows[PerfReportDS.MONTH]);

            DataTable perfDIFM = this.PresentationData.Tables[PerfReportDS.DIFMPERFTABLE];
            var perfDIFMRows = perfDIFM.Select().OrderByDescending(rows => rows[PerfReportDS.MONTH]);

            DataTable perfDIWM = this.PresentationData.Tables[PerfReportDS.DIWMPERFTABLE];
            var perfDIWMRows = perfDIWM.Select().OrderByDescending(rows => rows[PerfReportDS.MONTH]);

            DataTable perfDIY = this.PresentationData.Tables[PerfReportDS.DIYPERFTABLE];
            var perfDIYRows = perfDIY.Select().OrderByDescending(rows => rows[PerfReportDS.MONTH]);

            DataTable perfMAN = this.PresentationData.Tables[PerfReportDS.MANPERFTABLE];
            var perfMANRows = perfMAN.Select().OrderByDescending(rows => rows[PerfReportDS.MONTH]);

            DataTable bankTranTable = this.PresentationData.Tables[PerfReportDS.BANKTRANSACTIONSTABLE];
            DataTable asxTranTable = this.PresentationData.Tables[PerfReportDS.ASXTRANSTABLE];

            var bankTranTableRows = bankTranTable.Select().OrderByDescending(rows => rows[PerfReportDS.BANKTRANSDATE]);
            var asxTranTableRows = asxTranTable.Select().OrderByDescending(rows => rows[PerfReportDS.TRADEDATE]);

            var difmRows = capitalSummaryTable.Select("ServiceType = 'Do It For Me'").OrderBy(rows => rows[PerfReportDS.ASSETNAME]);
            var diyRows = capitalSummaryTable.Select("ServiceType = 'Do It Yourself'").OrderBy(rows => rows[PerfReportDS.ASSETNAME]);
            var diwmRows = capitalSummaryTable.Select("ServiceType = 'Do It With Me'").OrderBy(rows => rows[PerfReportDS.ASSETNAME]);
            var manRows = capitalSummaryTable.Select("ServiceType = 'Manual Asset'").OrderBy(rows => rows[PerfReportDS.ASSETNAME]);

            SetPerformanceSummaryGrid(doc, performSummaryTable, "PERFORMANCE SUMMARY");
            SetPerformanceDetailsGrid(doc, capitalSummaryCatRows, "PERFORMANCE DETAILS");

            if (perfDIYRows.Count() > 0)
                SetPerformanceDetailsGridServiceType(doc, perfDIYRows, "PERFORMANCE DETAILS - DO IT YOURSELF");
            if (perfDIWMRows.Count() > 0)
                SetPerformanceDetailsGridServiceType(doc, perfDIWMRows, "PERFORMANCE DETAILS - DO IT WITH ME");
            if (perfDIFMRows.Count() > 0)
                SetPerformanceDetailsGridServiceType(doc, perfDIFMRows, "PERFORMANCE DETAILS - DO IT FOR ME");
            if (perfMANRows.Count() > 0)
                SetPerformanceDetailsGridServiceType(doc, perfMANRows, "PERFORMANCE DETAILS - MANUAL ASSETS");

            AddFooter(doc);
            return doc;
        }

        public void SetPerformanceDetailsGrid(C1PrintDocument doc, IOrderedEnumerable<DataRow> rows, string serviceName)
        {
            int index = 1;

            if (rows.Count() > 0)
            {
                RenderTable capitalTable = new RenderTable();
                capitalTable.RowGroups[0, 1].PageHeader = true;
                double OpeningBalance = 0;
                double SalePurchase = 0;
                double TransferInOut = 0;
                double FeesExpenses = 0;
                double Movement = 0;
                double ClosingBalance = 0;
                double ChangeInInvestmentValue = 0;
                double IncomeTotal = 0;
                capitalTable.Style.FontSize = 8;

                capitalTable.Rows[0].Height = .2;
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.FontSize = 7;
                capitalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[0, 0].Text = "Month";
                capitalTable.Cells[0, 1].Text = "Opening Balance";
                capitalTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 2].Text = "Sale / Pur.";
                capitalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 3].Text = "Transfer In / Out";
                capitalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 4].Text = "Fees, Taxes & Exp.";
                capitalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 5].Text = "Other Mvt.";
                capitalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 6].Text = "Income";
                capitalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 7].Text = "Change in Mkt. Val.";
                capitalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 8].Text = "Closing Balance";
                capitalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 9].Text = "Return";
                capitalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cols[0].Width = .5;
                capitalTable.Cols[1].Width = 1.2;
                capitalTable.Cols[2].Width = 1.2;
                capitalTable.Cols[3].Width = 1.2;
                capitalTable.Cols[4].Width = 1.2;
                capitalTable.Cols[5].Width = 1.2;
                capitalTable.Cols[6].Width = 1.2;
                capitalTable.Cols[7].Width = 1.2;
                capitalTable.Cols[8].Width = 1.2;
                capitalTable.Cols[9].Width = 1.2;

                var closingRow = rows.FirstOrDefault()[CapitalReportDS.CLOSINGBALANCE];
                var openingRow = rows.LastOrDefault()[CapitalReportDS.OPENINGBAL];

                OpeningBalance = Convert.ToDouble(openingRow);
                ClosingBalance = Convert.ToDouble(closingRow);

                foreach (DataRow row in rows)
                {

                    capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    capitalTable.Rows[index].Height = .3;
                    capitalTable.Cells[index, 0].Text = ((DateTime)row[CapitalReportDS.MONTH]).ToString("MMM-yy").ToUpper();

                    TransferInOut += Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]);
                    SalePurchase += Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION]);
                    FeesExpenses += Convert.ToDouble(row[CapitalReportDS.EXPENSE]);
                    Movement += Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]);
                    ChangeInInvestmentValue += Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]);

                    double movement = Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]);

                    capitalTable.Cells[index, 1].Text = Convert.ToDouble(row[CapitalReportDS.OPENINGBAL]).ToString("C");
                    capitalTable.Cells[index, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 2].Text = Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION]).ToString("C");
                    capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 3].Text = Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]).ToString("C");
                    capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 4].Text = Convert.ToDouble(row[CapitalReportDS.EXPENSE]).ToString("C");
                    capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 5].Text = movement.ToString("C");
                    capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                    double income = Convert.ToDouble(row[CapitalReportDS.INCOME]);
                    capitalTable.Cells[index, 6].Text = income.ToString("C");
                    capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                    IncomeTotal += income;

                    double chgInvest = Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]);
                    capitalTable.Cells[index, 7].Text = chgInvest.ToString("C");
                    capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    double cls = Convert.ToDouble(row[CapitalReportDS.CLOSINGBALANCE]);
                    capitalTable.Cells[index, 8].Text = cls.ToString("C");
                    capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                    if (row[CapitalReportDS.MODDIETZ] != null && row[CapitalReportDS.MODDIETZ].ToString() != String.Empty)
                        capitalTable.Cells[index, 9].Text = Convert.ToDouble(row[CapitalReportDS.MODDIETZ]).ToString("P2");
                    else
                        capitalTable.Cells[index, 9].Text = Convert.ToDouble(0).ToString("P2");
                    capitalTable.Cells[index, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                    index++;
                }


                capitalTable.Rows[index].Height = .2;
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[index, 0].Text = "TOTAL";
                capitalTable.Cells[index, 1].Text = OpeningBalance.ToString("C");
                capitalTable.Cells[index, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 2].Text = SalePurchase.ToString("C");
                capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 3].Text = TransferInOut.ToString("C");
                capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 4].Text = FeesExpenses.ToString("C");
                capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 5].Text = Movement.ToString("C");
                capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 6].Text = IncomeTotal.ToString("C");
                capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 7].Text = ChangeInInvestmentValue.ToString("C");
                capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 8].Text = ClosingBalance.ToString("C");
                capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(capitalTable);
                doc.Body.Children.Add(lineBreak);
            }

        }
        public void SetPerformanceSummaryGrid(C1PrintDocument doc, DataTable performSummaryTable, string serviceName)
        {
            int index = 1;

            if (performSummaryTable.Rows.Count > 0)
            {
                RenderTable performTable = new RenderTable();
                performTable.RowGroups[0, 1].PageHeader = true;
                performTable.Style.FontSize = 8;

                performTable.Rows[0].Height = .2;
                performTable.Rows[0].Style.FontBold = true;
                performTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                performTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                performTable.Rows[0].Style.FontBold = true;
                performTable.Rows[0].Style.FontSize = 7;
                performTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                performTable.Cells[0, 0].Text = "Description";
                performTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Left;
                performTable.Cells[0, 1].Text = "1 Month";
                performTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[0, 2].Text = "3 Month";
                performTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[0, 3].Text = "6 Month";
                performTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[0, 4].Text = "1 Year";
                performTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[0, 5].Text = "2 Years";
                performTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[0, 6].Text = "3 Years";
                performTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                performTable.Cells[0, 7].Text = "5 Years";
                performTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                performTable.Cols[0].Width = 1.5;
                performTable.Cols[1].Width = 1.2;
                performTable.Cols[2].Width = 1.2;
                performTable.Cols[3].Width = 1.2;
                performTable.Cols[4].Width = 1.2;
                performTable.Cols[5].Width = 1.2;
                performTable.Cols[6].Width = 1.2;
                performTable.Cols[7].Width = 1.2;

                foreach (DataRow row in performSummaryTable.Rows)
                {

                    performTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    performTable.Rows[index].Height = .3;

                    performTable.Cells[index, 0].Text = row[PerfReportDS.DESCRIPTION].ToString();
                    performTable.Cells[index, 1].Text = row[PerfReportDS.ONEMONTH].ToString();
                    performTable.Cells[index, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                    performTable.Cells[index, 2].Text = row[PerfReportDS.THREEMONTH].ToString();
                    performTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                    performTable.Cells[index, 3].Text = row[PerfReportDS.SIXMONTH].ToString();
                    performTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                    performTable.Cells[index, 4].Text = row[PerfReportDS.ONEYEAR].ToString();
                    performTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                    performTable.Cells[index, 5].Text = row[PerfReportDS.TWOYEAR].ToString();
                    performTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                    performTable.Cells[index, 6].Text = row[PerfReportDS.THREEYEAR].ToString();
                    performTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                    performTable.Cells[index, 7].Text = row[PerfReportDS.FIVEYEAR].ToString();
                    performTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    index++;
                }


                performTable.Rows[index].Height = .2;
                performTable.Rows[index].Style.FontBold = true;
                performTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                performTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                performTable.Rows[index].Style.FontBold = true;
                performTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(performTable);
                doc.Body.Children.Add(lineBreak);
            }

        }

        public void SetPerformanceDetailsGridServiceType(C1PrintDocument doc, IOrderedEnumerable<DataRow> rows, string serviceName)
        {
            int index = 1;

            if (rows.Count() > 0)
            {
                RenderTable capitalTable = new RenderTable();
                capitalTable.RowGroups[0, 1].PageHeader = true;

                double OpeningBalance = 0;
                double SalePurchase = 0;
                double TransferInOut = 0;
                double FeesExpenses = 0;
                double Movement = 0;
                double ClosingBalance = 0;
                double ChangeInInvestmentValue = 0;
                double IncomeTotal = 0;
                capitalTable.Style.FontSize = 8;

                capitalTable.Rows[0].Height = .2;
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.FontSize = 7;
                capitalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[0, 0].Text = "Month";
                capitalTable.Cells[0, 1].Text = "Opening Balance";
                capitalTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 2].Text = "Sale / Pur.";
                capitalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 3].Text = "Transfer In / Out";
                capitalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 4].Text = "Fees, Taxes & Exp.";
                capitalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 5].Text = "Other Mvt.";
                capitalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 6].Text = "Income";
                capitalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 7].Text = "Change in Mkt. Val.";
                capitalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 8].Text = "Closing Balance";
                capitalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 9].Text = "Return";
                capitalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cols[0].Width = .5;
                capitalTable.Cols[1].Width = 1.2;
                capitalTable.Cols[2].Width = 1.2;
                capitalTable.Cols[3].Width = 1.2;
                capitalTable.Cols[4].Width = 1.2;
                capitalTable.Cols[5].Width = 1.2;
                capitalTable.Cols[6].Width = 1.2;
                capitalTable.Cols[7].Width = 1.2;
                capitalTable.Cols[8].Width = 1.2;
                capitalTable.Cols[9].Width = 1.2;


                var closingRow = rows.FirstOrDefault()[CapitalReportDS.CLOSINGBALANCE];
                var openingRow = rows.LastOrDefault()[CapitalReportDS.OPENINGBAL];

                OpeningBalance = Convert.ToDouble(openingRow);
                ClosingBalance = Convert.ToDouble(closingRow);

                foreach (DataRow row in rows)
                {
                    capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    capitalTable.Rows[index].Height = .3;
                    capitalTable.Cells[index, 0].Text = ((DateTime)row[CapitalReportDS.MONTH]).ToString("MMM-yy").ToUpper();

                    Movement += Convert.ToDouble(row[PerfReportDS.INTERNALCASHMOVEMENT]);
                    double appRedemption = Convert.ToDouble(row[PerfReportDS.APPLICATION]) + Convert.ToDouble(row[PerfReportDS.REDEMPTION]);
                    SalePurchase += appRedemption;
                    TransferInOut += Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]);

                    ChangeInInvestmentValue += Convert.ToDouble(row[PerfReportDS.CHANGEININVESTMENTVALUE]);

                    double movement = Convert.ToDouble(row[PerfReportDS.INTERNALCASHMOVEMENT]);

                    capitalTable.Cells[index, 1].Text = Convert.ToDouble(row[CapitalReportDS.OPENINGBAL]).ToString("C");
                    capitalTable.Cells[index, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 2].Text = appRedemption.ToString("C");
                    capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 3].Text = Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]).ToString("C");
                    capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 4].Text = Convert.ToDouble(row[CapitalReportDS.EXPENSE]).ToString("C");
                    capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 5].Text = movement.ToString("C");
                    capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                    double income = Convert.ToDouble(row[CapitalReportDS.INCOME]);
                    capitalTable.Cells[index, 6].Text = income.ToString("C");
                    capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                    IncomeTotal += income;

                    double chgInvest = Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]);
                    capitalTable.Cells[index, 7].Text = chgInvest.ToString("C");
                    capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    double cls = Convert.ToDouble(row[CapitalReportDS.CLOSINGBALANCE]);
                    capitalTable.Cells[index, 8].Text = cls.ToString("C");
                    capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                    if (row[CapitalReportDS.MODDIETZ] != null && row[CapitalReportDS.MODDIETZ].ToString() != String.Empty)
                        capitalTable.Cells[index, 9].Text = Convert.ToDouble(row[CapitalReportDS.MODDIETZ]).ToString("P2");
                    else
                        capitalTable.Cells[index, 9].Text = Convert.ToDouble(0).ToString("P2");
                    capitalTable.Cells[index, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                    index++;
                }


                capitalTable.Rows[index].Height = .2;
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[index, 0].Text = "TOTAL";
                capitalTable.Cells[index, 1].Text = OpeningBalance.ToString("C");
                capitalTable.Cells[index, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 2].Text = SalePurchase.ToString("C");
                capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 3].Text = TransferInOut.ToString("C");
                capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 4].Text = FeesExpenses.ToString("C");
                capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 5].Text = Movement.ToString("C");
                capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 6].Text = IncomeTotal.ToString("C");
                capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 7].Text = ChangeInInvestmentValue.ToString("C");
                capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[index, 8].Text = ClosingBalance.ToString("C");
                capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(capitalTable);
                doc.Body.Children.Add(lineBreak);
            }

        }

        protected void quickReports_ItemClick(object sender, RadMenuEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "1M":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetOneMonthDate();
                        ClientPerformanceReportClick(ds);
                        break;
                    }
                case "3M":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetThreeMonthDate();
                        ClientPerformanceReportClick(ds);
                        break;
                    }
                case "6M":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetSixMonthDate();
                        ClientPerformanceReportClick(ds);
                        break;
                    }
                case "12M":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetTwelveMonthDate();
                        ClientPerformanceReportClick(ds);
                        break;
                    }
                case "LFY":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetLastFinancialYear();
                        ClientPerformanceReportClick(ds);
                        break;
                    }
                case "CFY":
                    {

                        PerfReportDS ds = new PerfReportDS();
                        ds.SetCurrentFinancialYear();
                        ClientPerformanceReportClick(ds);
                        break;
                    }
            }
        }

        protected void GeneratePerformanceReport(object sender, EventArgs e)
        {
            ClientPerformanceReportClick();
        }

        protected void ClientPerformanceReportClick(PerfReportDS ds)
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"].ToString()));
            clientData.GetData(ds);
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);
            this.PresentationData = ds;
            string reportName = "Report_" + Guid.NewGuid().ToString();
            C1ReportViewer.RegisterDocument(reportName, MakeDocPerformance);
            C1ReportViewerCapitalFlow.FileName = reportName;
            C1ReportViewerCapitalFlow.ReportName = reportName;
            InputEndDate.DbSelectedDate = ds.EndDate;
            InputStartDate.DbSelectedDate = ds.StartDate;
            UMABroker.ReleaseBrokerManagedComponent(clientData);    
        }

        protected void ClientPerformanceReportClick()
        {
            PerfReportDS ds = new PerfReportDS();
           
            if (InputEndDate.DateInput.SelectedDate.HasValue)
                ds.EndDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(this.InputEndDate.DateInput.SelectedDate.Value);

            if (InputStartDate.DateInput.SelectedDate.HasValue)
                ds.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(this.InputStartDate.DateInput.SelectedDate.Value);

            ClientPerformanceReportClick(ds);
        }
    }
}
