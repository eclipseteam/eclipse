﻿namespace eclipseonlineweb.Reports
{
    public partial class P2Reports : UMABasePage
    {
        public override void LoadPage()
        {
            cid = Request.QueryString["ins"];
            if (IsPostBack) return;
            var user = GetCurrentUser();
            P2ReportsControl.IsAdmin = user.IsAdmin;
            P2ReportsControl.IsAdminMenu = false;
            //using client Id from queryString because user.CID can contain Admin CID if logged in through Admin
            P2ReportsControl.ClientCID = cid;
        }

        protected override void Intialise()
        {
            base.Intialise();
            P2ReportsControl.SaveData += SaveData;
        }
    }
}