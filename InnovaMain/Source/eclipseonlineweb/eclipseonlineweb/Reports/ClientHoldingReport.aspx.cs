﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;

namespace eclipseonlineweb
{
    public partial class ClientHoldingReport : UMABasePage
    {
       
        private List<AssetEntity> assets = null;

        public override void PopulatePage(DataSet ds)
        {
            base.PopulatePage(ds);
            this.cid = Request.QueryString["ins"].ToString();
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            ClientHoldingReportClick();
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public C1PrintDocument MakeDoc()
        {
            
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            var ds = PresentationData as HoldingRptDataSet;

            DataRow clientSummaryRow = ds.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0];
            AddClientHeaderToReport(doc, clientSummaryRow, ds.DateInfo(), "HOLDING REPORT");

            var difmRows = ds.HoldingSummaryTable.Select("ServiceType = 'Do It For Me'").OrderBy(rows => rows[ds.HoldingSummaryTable.ASSETNAME]);
            var diyRows = ds.HoldingSummaryTable.Select("ServiceType = 'Do It Yourself'").OrderBy(rows => rows[ds.HoldingSummaryTable.ASSETNAME]);
            var diwmRows = ds.HoldingSummaryTable.Select("ServiceType = 'Do It With Me'").OrderBy(rows => rows[ds.HoldingSummaryTable.ASSETNAME]);
            var manRows = ds.HoldingSummaryTable.Select("ServiceType = 'Manual Asset'").OrderBy(rows => rows[ds.HoldingSummaryTable.ASSETNAME]);

            SetHoldingGridTotals(doc, difmRows, diyRows, diwmRows, manRows, ds.HoldingSummaryTable, string.Empty);

            SetHoldingGrid(doc, difmRows, ds, "DO IT FOR ME");
            SetHoldingGrid(doc, diyRows, ds, "DO IT YOURSELF");
            SetHoldingGrid(doc, diwmRows, ds, "DO IT WITH ME");
            SetHoldingGrid(doc, manRows, ds, "MANUAL ASSETS");
         
            return doc;
        }

        public void SetHoldingGrid(C1PrintDocument doc, IOrderedEnumerable<DataRow> difmRows, HoldingRptDataSet ds, string serviceName)
        {
            int index = 1;

            if (difmRows.Count() > 0)
            {
                RenderTable holdingTable = new RenderTable();
                holdingTable.RowGroups[0, 1].PageHeader = true;
                double totalHolding = 0;

                holdingTable.Style.FontSize = 8;

                holdingTable.Rows[0].Height = .2;
                holdingTable.Rows[0].Style.FontBold = true;
                holdingTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                holdingTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                holdingTable.Rows[0].Style.FontBold = true;
                holdingTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;
                holdingTable.RowGroups[0, 1].PageHeader = true;
                holdingTable.Cells[0, 0].Text = "Asset Name";
                holdingTable.Cells[0, 1].Text = "Product Name";
                holdingTable.Cells[0, 2].Text = "Description";
                holdingTable.Cells[0, 3].Text = "Account NO";
                holdingTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Center;
                holdingTable.Cells[0, 4].Text = "Holding ($)";
                holdingTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                holdingTable.Cols[0].Width = .8;
                holdingTable.Cols[1].Width = 1.8;
                holdingTable.Cols[2].Width = 4.8;
                holdingTable.Cols[3].Width = .9;
                holdingTable.Cols[4].Width = .8;

                foreach (DataRow row in difmRows)
                {
                    holdingTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    holdingTable.Rows[index].Height = .3;
                    holdingTable.Cells[index, 0].Text = row[ds.HoldingSummaryTable.ASSETNAME].ToString();
                    holdingTable.Cells[index, 1].Text = row[ds.HoldingSummaryTable.PRODUCTNAME].ToString();
                    holdingTable.Cells[index, 2].Text = row[ds.HoldingSummaryTable.DESCRIPTION].ToString();
                    holdingTable.Cells[index, 3].Text = row[ds.HoldingSummaryTable.ACCOUNTNO].ToString();
                    holdingTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Center;
                    totalHolding += Convert.ToDouble(row[ds.HoldingSummaryTable.HOLDING]);
                    holdingTable.Cells[index, 4].Text = Convert.ToDouble(row[ds.HoldingSummaryTable.HOLDING]).ToString("C");
                    holdingTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                    index++;
                }

                holdingTable.Rows[index].Height = .2;
                holdingTable.Rows[index].Style.FontBold = true;
                holdingTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                holdingTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                holdingTable.Rows[index].Style.FontBold = true;
                holdingTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                holdingTable.Cells[index, 0].Text = "TOTAL";

                holdingTable.Cells[index, 4].Text = totalHolding.ToString("C");
                holdingTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = "HOLDING SUMMARY - " + serviceName + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(holdingTable);
                doc.Body.Children.Add(lineBreak);
            }

        }

        public void SetHoldingGridTotals(C1PrintDocument doc, IOrderedEnumerable<DataRow> difmRows, IOrderedEnumerable<DataRow> diyRows, IOrderedEnumerable<DataRow> diwmRows, IOrderedEnumerable<DataRow> manRows, HoldingSummaryTable holdingSummaryTable, string serviceName)
        {
            int index = 1;
            double grandTotal = 0;

            RenderTable holdingTable = new RenderTable();
            holdingTable.RowGroups[0, 1].PageHeader = true;
            holdingTable.Style.FontSize = 8;

            holdingTable.Rows[0].Height = .2;
            holdingTable.Rows[0].Style.FontBold = true;
            holdingTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            holdingTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            holdingTable.Rows[0].Style.FontBold = true;
            holdingTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;
            holdingTable.RowGroups[0, 1].PageHeader = true;
            holdingTable.Cells[0, 0].Text = "Service Code";
            holdingTable.Cells[0, 1].Text = "Service Description";
            holdingTable.Cells[0, 2].Text = "Holding ($)";
            holdingTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

            holdingTable.Cols[0].Width = 2;
            holdingTable.Cols[1].Width = 4;
            holdingTable.Cols[2].Width = 4;

            if (difmRows.Count() > 0)
            {
                double totalHolding = 0;

                holdingTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                holdingTable.Rows[index].Height = .3;
                holdingTable.Cells[index, 0].Text = "DIFM";
                holdingTable.Cells[index, 1].Text = "Do It For Me";

                foreach (DataRow row in difmRows)
                {
                    totalHolding += Convert.ToDouble(row[holdingSummaryTable.HOLDING]);
                    grandTotal += Convert.ToDouble(row[holdingSummaryTable.HOLDING]);
                }

                holdingTable.Cells[index, 2].Text = totalHolding.ToString("C");
                holdingTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                index++;
            }

            if (diyRows.Count() > 0)
            {
                double totalHolding = 0;

                holdingTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                holdingTable.Rows[index].Height = .3;
                holdingTable.Cells[index, 0].Text = "DIY";
                holdingTable.Cells[index, 1].Text = "Do It Yourself";

                foreach (DataRow row in diyRows)
                {
                    totalHolding += Convert.ToDouble(row[holdingSummaryTable.HOLDING]);
                    grandTotal += Convert.ToDouble(row[holdingSummaryTable.HOLDING]);
                }
                
                holdingTable.Cells[index, 2].Text = totalHolding.ToString("C");
                holdingTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                index++;
            }

            if (diwmRows.Count() > 0)
            {
                double totalHolding = 0;

                holdingTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                holdingTable.Rows[index].Height = .3;
                holdingTable.Cells[index, 0].Text = "DIWM";
                holdingTable.Cells[index, 1].Text = "Do It With Me";

                foreach (DataRow row in diwmRows)
                {
                    totalHolding += Convert.ToDouble(row[holdingSummaryTable.HOLDING]);
                    grandTotal += Convert.ToDouble(row[holdingSummaryTable.HOLDING]);
                }
                holdingTable.Cells[index, 2].Text = totalHolding.ToString("C");
                holdingTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                index++;
            }

            if (manRows.Count() > 0)
            {
                double totalHolding = 0;

                holdingTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                holdingTable.Rows[index].Height = .3;
                holdingTable.Cells[index, 0].Text = "MANUAL";
                holdingTable.Cells[index, 1].Text = "Manual Assets";
            
                foreach (DataRow row in manRows)
                {
                    totalHolding += Convert.ToDouble(row[holdingSummaryTable.HOLDING]);
                    grandTotal += Convert.ToDouble(row[holdingSummaryTable.HOLDING]);
                }
                holdingTable.Cells[index, 2].Text = totalHolding.ToString("C");
                holdingTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                index++;
            }

            holdingTable.Rows[index].Height = .2;
            holdingTable.Rows[index].Style.FontBold = true;
            holdingTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            holdingTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            holdingTable.Rows[index].Style.FontBold = true;
            holdingTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

            holdingTable.Cells[index, 0].Text = "TOTAL";

            holdingTable.Cells[index, 2].Text = grandTotal.ToString("C");
            holdingTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

            RenderText gridHeader = new RenderText();
            gridHeader.Text = "HOLDING SUMMARY\n\n";
            gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeader.Style.FontSize = 11;
            gridHeader.Style.FontBold = true;

            RenderText lineBreak = new RenderText();
            lineBreak.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeader);
            doc.Body.Children.Add(holdingTable);
            doc.Body.Children.Add(lineBreak);
        }

        protected void GenerateValuationReport(object sender, EventArgs e)
        {
            ClientHoldingReportClick();
        }
        
        protected void ClientHoldingReportClick()
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"].ToString()));
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            this.assets = organization.Assets;

            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);

            HoldingRptDataSet ds = new HoldingRptDataSet();
            if (this.InputValutationDate.SelectedDate.HasValue)
                ds.ValuationDate = this.InputValutationDate.SelectedDate.Value;
            clientData.GetData(ds);
            this.PresentationData = ds;
            DataRow clientSummaryRow = ds.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0];
            string reportName = "Report_" + Guid.NewGuid().ToString();
            C1ReportViewer.RegisterDocument(reportName, MakeDoc);
            C1ReportViewer1.FileName = reportName;
            C1ReportViewer1.ReportName = reportName;
            InputValutationDate.DbSelectedDate = ds.ValuationDate;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }
    }
}
