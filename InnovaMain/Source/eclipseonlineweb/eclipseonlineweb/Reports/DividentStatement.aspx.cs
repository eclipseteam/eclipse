﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.Web.Wijmo.Controls.C1GridView;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;

namespace eclipseonlineweb
{
    public partial class DividentStatement : UMABasePage
    {

        protected override void Intialise()
        {
            base.Intialise();
            this.DivTranGrid.RowCommand += new C1GridViewCommandEventHandler(DivTranGrid_RowCommand);
        }

        private void DivTranGrid_RowCommand(object sender, C1GridViewCommandEventArgs e)
        {
            var grid = (C1GridView)sender;
            int index = int.Parse(e.CommandArgument.ToString());
            lblDivID.Text = grid.Rows[index].Cells[0].Text;
            lblRecordDate.Text = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[2].Text;
            this.lblUnits.Text = ((C1GridView)sender).Rows[Convert.ToInt32(e.CommandArgument)].Cells[4].Text;

            this.cid = Request.QueryString["ins"].ToString();

            if (e.CommandName.ToLower() == "details")
            {
                C1ReportViewerDividentStatement.Visible = true;
                DivTranGrid.Visible = false;
                Response.Redirect("~/Reports/DividentStatementReport.aspx?ins=" + this.cid + "&DivID=" + lblDivID.Text + "&RecordDate=" + lblRecordDate.Text + "&Units=" + this.lblUnits.Text);
            }
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public override void LoadPage()
        {
            this.cid = Request.QueryString["ins"].ToString();
            DIV_Click();
            ScriptManager sm = ScriptManager.GetCurrent(Page);
        }

        protected void DIV_Click()
        {
            this.cid = Request.QueryString["ins"].ToString();
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);
            DIVTransactionDS divDS = new DIVTransactionDS();
            clientData.GetData(divDS);
            this.PresentationData = divDS;
            
            if (divDS.Tables.Count > 0)
            {
                DataView divView = new DataView(divDS.Tables["Income Transactions"]);
                divView.Sort = "RecordDate DESC";
                divView.RowFilter = "TransactionType = 'Dividend Accrual'"; 

                this.DivTranGrid.DataSource = divView.ToTable();
                this.DivTranGrid.DataBind();
            }

            this.UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void DIV_PageIndexChanging(object sender, C1GridViewPageEventArgs e)
        {
            this.cid = Request.QueryString["ins"].ToString();
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            DIVTransactionDS ds = new DIVTransactionDS();
            clientData.GetData(ds);
            DataView summaryView = new DataView(ds.Tables["Income Transactions"]);
            summaryView.Sort = "RecordDate DESC";
            summaryView.RowFilter = "TransactionType = 'Dividend Accrual'";
            this.DivTranGrid.DataSource = summaryView.ToTable();

            this.DivTranGrid.PageIndex = e.NewPageIndex;
            DivTranGrid.DataBind();

            this.UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void FilterDIV(object sender, C1GridViewFilterEventArgs e)
        {
            this.cid = Request.QueryString["ins"].ToString();
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            DIVTransactionDS ds = new DIVTransactionDS();
            clientData.GetData(ds);

            e.Values[0] = ((string)e.Values[0]).Trim();

            DataView summaryView = new DataView(ds.Tables["Income Transactions"]);
            summaryView.Sort = "RecordDate DESC";
            summaryView.RowFilter = "TransactionType = 'Dividend Accrual'";

            this.DivTranGrid.DataSource = summaryView.ToTable();
            this.DivTranGrid.DataBind();

            this.UMABroker.ReleaseBrokerManagedComponent(clientData);
        }

        protected void SortDIV(object sender, C1GridViewSortEventArgs e)
        {
            this.cid = Request.QueryString["ins"].ToString();
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            DIVTransactionDS ds = new DIVTransactionDS();
            clientData.GetData(ds);

            DataView summaryView = new DataView(ds.Tables["Income Transactions"]);
            summaryView.Sort = "RecordDate DESC";
            summaryView.RowFilter = "TransactionType = 'Dividend Accrual'";

            this.DivTranGrid.DataSource = summaryView.ToTable();
            this.DivTranGrid.DataBind();

            this.UMABroker.ReleaseBrokerManagedComponent(clientData);
        }
    }
}
