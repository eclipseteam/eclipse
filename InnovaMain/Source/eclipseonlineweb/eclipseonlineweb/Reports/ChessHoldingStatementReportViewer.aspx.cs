﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;

namespace eclipseonlineweb
{
    public partial class ChessHoldingStatementReportViewer : UMABasePage
    {
        private List<AssetEntity> assets = null;

        public override void PopulatePage(DataSet ds)
        {
            if (!Page.IsPostBack)
            {
                string secCode = Request.QueryString["SecurityCode"].ToString();
                string startDate = Request.QueryString["StartDate"].ToString();
                string endDate = Request.QueryString["EndDate"].ToString();
                string secDesc = Request.QueryString["SecDesc"].ToString();

                this.lblSecDesc.Text = secDesc;
                this.lblSecCode.Text = secCode;


                DateTime parsedDate = DateTime.Now;

                if (DateTime.TryParse(startDate, out parsedDate))
                    InputStartDate.DbSelectedDate = DateTime.Parse(startDate);

                if (DateTime.TryParse(endDate, out parsedDate))
                    InputEndDate.DbSelectedDate = DateTime.Parse(endDate);
            }

            this.cid = Request.QueryString["ins"].ToString();
            base.PopulatePage(ds);
            ReportClick();
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public C1PrintDocument MakeDoc()
        {
          var ds=  this.PresentationData as HoldingSummaryReportDS;
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            DataRow clientSummaryRow = this.PresentationData.Tables[HoldingSummaryReportDS.CLIENTSUMMARYTABLE].Rows[0];
            AddClientHeaderToReport(doc, clientSummaryRow, ds.DateInfo(), "HOLDING STATEMENT");
            AddFooterSecuritiesStatement(doc);
            DataView secSummaryTableView = new DataView(ds.transactiondetailstable);
            secSummaryTableView.Sort = ds.transactiondetailstable.TRANSACTIONDATE + " ASC";
            secSummaryTableView.RowFilter = ds.transactiondetailstable.SECCODE + "= '" + this.lblSecCode.Text + "'";

            DataTable secFilteredTable = secSummaryTableView.ToTable();

            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true; 
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Style.FontSize = 8;

            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Date";
            secSummaryTotalTable.Cells[0, 1].Text = "Transaction Type";
            secSummaryTotalTable.Cells[0, 2].Text = "Transaction ID";
            secSummaryTotalTable.Cells[0, 3].Text = "Units";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 4].Text = "Holding Bal.";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 4;
            secSummaryTotalTable.Cols[1].Width = 4;
            secSummaryTotalTable.Cols[2].Width = 4;
            secSummaryTotalTable.Cols[3].Width = 4;
            secSummaryTotalTable.Cols[4].Width = 4;

            RenderText gridHeaderTotalBroker = new RenderText();
            gridHeaderTotalBroker.Text = "Sponsoring Broker: = 'Desktop Broker'\n\n";
            gridHeaderTotalBroker.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotalBroker.Style.FontSize = 11;
            gridHeaderTotalBroker.Style.FontBold = true;

            RenderText hinTotalBroker = new RenderText();
            hinTotalBroker.Text = "HIN#: = 'Desktop Broker'\n\n";
            hinTotalBroker.Style.TextColor = Color.FromArgb(46, 44, 83);
            hinTotalBroker.Style.FontSize = 11;
            hinTotalBroker.Style.FontBold = true;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = this.lblSecCode.Text.ToUpper() + "   -   " + this.lblSecDesc.Text.ToUpper() + "\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";
            doc.Body.Children.Add(gridHeaderTotalBroker);
        //    doc.Body.Children.Add(hinTotalBroker);
            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            foreach (DataRow secRow in secFilteredTable.Rows)
            {
                secSummaryTotalTable.Cells[totalIndex, 0].Text = ((DateTime)secRow[ds.transactiondetailstable.TRANSACTIONDATE]).ToString("dd/MM/yyyy");
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[ds.transactiondetailstable.TRANSTYPE].ToString();
                secSummaryTotalTable.Cells[totalIndex, 2].Text = secRow[ds.transactiondetailstable.TRANSDISID].ToString();

                if (secRow[ds.transactiondetailstable.UNITS] != null & secRow[ds.transactiondetailstable.UNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = Convert.ToDecimal(secRow[ds.transactiondetailstable.UNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[ds.transactiondetailstable.UNITSBALANCE] != null & secRow[ds.transactiondetailstable.UNITSBALANCE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = Convert.ToDecimal(secRow[ds.transactiondetailstable.UNITSBALANCE]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                totalIndex++;
            }

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;
            doc.PageLayout.PageSettings.Landscape = false;
            doc.PageLayout.PageSettings.TopMargin = .20;
            doc.PageLayout.PageSettings.LeftMargin = .20;
            doc.PageLayout.PageSettings.RightMargin = .20;
            return doc;
        }

        protected void GenerateReport(object sender, EventArgs e)
        {
            ReportClick();
        }

        protected void ReportClick()
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(cid));
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            this.assets = organization.Assets;

            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);

            HoldingSummaryReportDS ds = new HoldingSummaryReportDS();
            if (this.InputEndDate.DateInput.SelectedDate.HasValue)
                ds.EndDate = this.InputEndDate.DateInput.SelectedDate.Value;
           
            if (this.InputStartDate.DateInput.SelectedDate.HasValue)
                ds.StartDate = this.InputStartDate.DateInput.SelectedDate.Value;
           
            clientData.GetData(ds);
            this.PresentationData = ds;
            InputEndDate.DbSelectedDate = ds.EndDate;
            InputStartDate.DbSelectedDate = ds.StartDate; 
            DataRow clientSummaryRow = ds.Tables[HoldingSummaryReportDS.CLIENTSUMMARYTABLE].Rows[0];
            string reportName = "Report_" + Guid.NewGuid().ToString();
            C1ReportViewer.RegisterDocument(reportName, MakeDoc);
            ReportViewer.FileName = reportName;
            ReportViewer.ReportName = reportName;
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }
    }
}
