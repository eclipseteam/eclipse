﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="ReportsMaster.master"
    AutoEventWireup="true" CodeBehind="TaxDistributionStatement.aspx.cs" Inherits="eclipseonlineweb.TaxDistributionStatement" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="c1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer"
    TagPrefix="C1ReportViewer" %>
<%@ Register TagPrefix="uc1" TagName="clientheaderinfo" Src="~/Controls/ClientHeaderInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 5%">
                        </td>
                       <td></td>
                        <%--<td style="width: 45%; background-color:Yellow">
                        <p style="color:Red"><span style="color:Red" class="riLabel">Please note the 'Tax Pack' is in draft format until further notice. Please do not rely on this information for tax purposes.</span></p>
                        </td>--%>
                         <td></td>
                        <td style="width: 85%;" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <uc1:clientheaderinfo ID="ClientHeaderInfo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <asp:Panel runat="server" ID="pnlDIS">
                <c1:C1GridView AlternatingRowStyle-Font-Size="X-Small" FilterStyle-Font-Size="Smaller"
                    AllowSorting="true" OnSorting="SortDIS" RowStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small"
                    ID="DisTranGrid" runat="server" AutogenerateColumns="false" ShowFilter="true"
                    OnFiltering="FilterDIS" Width="100%" ShowFooter="true" ScrollMode="None">
                    <Columns>
                        <c1:C1BoundField DataField="FundCode" SortExpression="FundCode" HeaderText="Fund Code">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="FundName" SortExpression="FundName" HeaderText="FundName">
                            <ItemStyle HorizontalAlign="Left" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="FinYearStartDate" SortExpression="FinYearStartDate" HeaderText="Start Date">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="FinYearEndDate" SortExpression="FinYearEndDate" HeaderText="End Date">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1BoundField DataField="NetCashDistribution" SortExpression="NetCashDistribution"
                            HeaderText="Net Cash Distribution">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1BoundField>
                        <c1:C1ButtonField Width="10%" Text="Report" ButtonType="Link" Visible="true" CommandName="Details">
                            <ItemStyle HorizontalAlign="Center" />
                        </c1:C1ButtonField>
                    </Columns>
                </c1:C1GridView>
            </asp:Panel>
            <div>
                <C1ReportViewer:C1ReportViewer CollapseToolsPanel="true" Visible="false" Cache-Enabled="false"
                    Cache-ShareBetweenSessions="false" FileName="InMemoryBasicTable" runat="server"
                    ID="C1ReportViewerDividentStatement" Height="550px" Width="100%" Zoom="75%">
                </C1ReportViewer:C1ReportViewer>
            </div>
            <asp:Label runat="server" ID="lblDivID" Text="" Visible="false"></asp:Label>
            <asp:Label runat="server" ID="lblRecordDate" Text="" Visible="false"></asp:Label>
            <asp:Label runat="server" ID="lblUnits" Text="" Visible="false"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
