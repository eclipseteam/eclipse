﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Utilities;

namespace eclipseonlineweb
{
    public partial class HoldingStatementReport : UMABasePage
    {
       
        private List<AssetEntity> assets = null;

        public override void PopulatePage(DataSet ds)
        {
            this.cid = Request.QueryString["ins"].ToString();

            base.PopulatePage(ds);
            ReportClick();
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public C1PrintDocument MakeDoc()
        {
          var ds=  this.PresentationData as HoldingSummaryReportDS;
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            DataRow clientSummaryRow = this.PresentationData.Tables[HoldingSummaryReportDS.CLIENTSUMMARYTABLE].Rows[0];
            AddClientHeaderToReport(doc, clientSummaryRow, ds.DateInfo(), "HOLDING STATEMENT");
            ExtractHoldingSummaryReport(ds, doc);
            return doc;
        }

        private static void ExtractHoldingSummaryReport(HoldingSummaryReportDS ds, C1PrintDocument doc)
        {
            DataView secSummaryTableView = new DataView(ds.securitySummaryTable);
            secSummaryTableView.Sort = ds.securitySummaryTable.SECNAME + " ASC";

            DataTable secFilteredTable = secSummaryTableView.ToTable();

            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true;
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.Style.FontSize = 7;
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true;
            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Code";
            secSummaryTotalTable.Cells[0, 1].Text = "Description";
            secSummaryTotalTable.Cells[0, 2].Text = "Type";
            secSummaryTotalTable.Cells[0, 3].Text = "Op. Units";
            secSummaryTotalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 4].Text = "Op. Price";
            secSummaryTotalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
            secSummaryTotalTable.Cells[0, 5].Text = "Op. Bal.";
            secSummaryTotalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 6].Text = "Purchase";
            secSummaryTotalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 7].Text = "Sale";
            secSummaryTotalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 8].Text = "Adj. Up";
            secSummaryTotalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 9].Text = "Adj. Down";
            secSummaryTotalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 10].Text = "Movement";
            secSummaryTotalTable.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 11].Text = "Cls. Units";
            secSummaryTotalTable.Cells[0, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 12].Text = "Cls. Price";
            secSummaryTotalTable.Cells[0, 12].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 13].Text = "Cls. Balance";
            secSummaryTotalTable.Cells[0, 13].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 7;
            secSummaryTotalTable.Cols[2].Width = 2;
            secSummaryTotalTable.Cols[3].Width = 2;
            secSummaryTotalTable.Cols[4].Width = 2;
            secSummaryTotalTable.Cols[5].Width = 2;
            secSummaryTotalTable.Cols[6].Width = 2;
            secSummaryTotalTable.Cols[7].Width = 2;
            secSummaryTotalTable.Cols[8].Width = 2;
            secSummaryTotalTable.Cols[9].Width = 2;
            secSummaryTotalTable.Cols[10].Width = 2;
            secSummaryTotalTable.Cols[11].Width = 2;
            secSummaryTotalTable.Cols[12].Width = 2;
            secSummaryTotalTable.Cols[13].Width = 2;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            decimal openingBalTotal = 0;
            decimal closingBalTotal = 0;
            decimal applicationTotal = 0;
            decimal redemptionTotal = 0;
            decimal adjustmentUpTotal = 0;
            decimal adjustmentDownTotal = 0;
            decimal movementTotal = 0;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            foreach (DataRow secRow in secFilteredTable.Rows)
            {
                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[ds.securitySummaryTable.SECNAME].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[ds.securitySummaryTable.DESCRIPTION].ToString();
                secSummaryTotalTable.Cells[totalIndex, 2].Text = secRow[ds.securitySummaryTable.TYPE].ToString();

                if (secRow[ds.securitySummaryTable.OPENINGUNITSBAL] != null & secRow[ds.securitySummaryTable.OPENINGUNITSBAL] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = Convert.ToDecimal(secRow[ds.securitySummaryTable.OPENINGUNITSBAL]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 3].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[ds.securitySummaryTable.OPENINGUNITPRICE] != null & secRow[ds.securitySummaryTable.OPENINGUNITPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = Convert.ToDecimal(secRow[ds.securitySummaryTable.OPENINGUNITPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 4].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal openingBal = Convert.ToDecimal(secRow[ds.securitySummaryTable.OPENINGBALANCE]);
                secSummaryTotalTable.Cells[totalIndex, 5].Text = openingBal.ToString("C");
                openingBalTotal += openingBal;
                secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal application = 0;

                if (secRow[ds.securitySummaryTable.APPLICATION] != DBNull.Value)
                    application = Convert.ToDecimal(secRow[ds.securitySummaryTable.APPLICATION]);

                secSummaryTotalTable.Cells[totalIndex, 6].Text = application.ToString("C");
                applicationTotal += application;
                secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal redemption = 0;

                if (secRow[ds.securitySummaryTable.REDEMPTION] != DBNull.Value)
                    redemption = Convert.ToDecimal(secRow[ds.securitySummaryTable.REDEMPTION]);

                secSummaryTotalTable.Cells[totalIndex, 7].Text = redemption.ToString("C");
                redemptionTotal += redemption;
                secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal adjUp = 0;

                if (secRow[ds.securitySummaryTable.ADJUSTMENTUP] != DBNull.Value)
                    adjUp = Convert.ToDecimal(secRow[ds.securitySummaryTable.ADJUSTMENTUP]);

                secSummaryTotalTable.Cells[totalIndex, 8].Text = adjUp.ToString("C");
                adjustmentUpTotal += adjUp;
                secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal adjDown = 0;

                if (secRow[ds.securitySummaryTable.ADJUSTMENTDOWN] != DBNull.Value)
                    adjDown = Convert.ToDecimal(secRow[ds.securitySummaryTable.ADJUSTMENTDOWN]);

                secSummaryTotalTable.Cells[totalIndex, 9].Text = adjDown.ToString("C");
                adjustmentDownTotal += adjDown;
                secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                decimal movement = 0;
                movement = Convert.ToDecimal(secRow[ds.securitySummaryTable.MOVEMENT]);
                
                secSummaryTotalTable.Cells[totalIndex, 10].Text = movement.ToString("C");
                movementTotal += movement;
                secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[ds.securitySummaryTable.CLOSINGUNITS] != null & secRow[ds.securitySummaryTable.CLOSINGUNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = Convert.ToDecimal(secRow[ds.securitySummaryTable.CLOSINGUNITS]).ToString("N2");
                else
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[ds.securitySummaryTable.CLOSINGUNITPRICE] != null & secRow[ds.securitySummaryTable.CLOSINGUNITPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 12].Text = Convert.ToDecimal(secRow[ds.securitySummaryTable.CLOSINGUNITPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 12].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 12].Style.TextAlignHorz = AlignHorzEnum.Right;


                decimal closingBal = 0;
                closingBal = Convert.ToDecimal(secRow[ds.securitySummaryTable.CLOSINGBAL]);
            
                secSummaryTotalTable.Cells[totalIndex, 13].Text = closingBal.ToString("C");
                closingBalTotal += closingBal;
                secSummaryTotalTable.Cells[totalIndex, 13].Style.TextAlignHorz = AlignHorzEnum.Right;


                totalIndex++;
            }

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[totalIndex, 0].Text = "TOTAL";
            secSummaryTotalTable.Cells[totalIndex, 1].Text = "";
            secSummaryTotalTable.Cells[totalIndex, 5].Text = openingBalTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[totalIndex, 6].Text = applicationTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[totalIndex, 7].Text = redemptionTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[totalIndex, 8].Text = adjustmentUpTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[totalIndex, 9].Text = adjustmentDownTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[totalIndex, 10].Text = movementTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[totalIndex, 13].Text = closingBalTotal.ToString("C");
            secSummaryTotalTable.Cells[totalIndex, 13].Style.TextAlignHorz = AlignHorzEnum.Right;
        }

        protected void GenerateAssetSummaryReport(object sender, EventArgs e)
        {
            ReportClick();
        }

        protected void ReportClick()
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"].ToString()));
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            this.assets = organization.Assets;
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);
            HoldingSummaryReportDS ds = new HoldingSummaryReportDS();
            if (InputStartDate.DateInput.SelectedDate.HasValue)
                 ds.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(this.InputStartDate.DateInput.SelectedDate.Value);

            if (InputEndDate.DateInput.SelectedDate.HasValue)
                 ds.EndDate =  AccountingFinancialYear.LastDayOfMonthFromDateTime(this.InputEndDate.DateInput.SelectedDate.Value);

            clientData.GetData(ds);
            this.PresentationData = ds;
            DataRow clientSummaryRow = ds.Tables[HoldingSummaryReportDS.CLIENTSUMMARYTABLE].Rows[0];
            string reportName = "Report_" + Guid.NewGuid().ToString();
            C1ReportViewer.RegisterDocument(reportName, MakeDoc);
            ReportViewer.FileName = reportName;
            ReportViewer.ReportName = reportName;
            InputEndDate.DbSelectedDate = ds.EndDate;
            InputStartDate.DbSelectedDate = ds.StartDate; 
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }
    }
}
